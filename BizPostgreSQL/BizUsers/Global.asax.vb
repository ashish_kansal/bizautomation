Imports System.Web
Imports System.Web.SessionState
Imports System.Security.Cryptography
Imports System.Data.SqlClient
Imports System.Text
Public Class [Global]
    Inherits System.Web.HttpApplication

#Region " Component Designer Generated Code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container()
    End Sub

#End Region

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application is started
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        If Request.ServerVariables("HTTPS") = "off" Then
            Session("siteType") = "http:"
        Else
            Session("siteType") = "https:"
        End If
        Session("User") = ConfigurationManager.AppSettings("User")
        Dim objBusinessClass As New BusinessClass
        Dim strLicence As String()
        Try
            Dim passPhrase As String
            Dim saltValue As String
            Dim hashAlgorithm As String
            Dim passwordIterations As Integer
            Dim initVector As String
            Dim keySize As Integer

            passPhrase = "AnoopCarlBizAutoWillWinNithya768@@@@#"        ' can be any string
            saltValue = "s@AnoopCarl234358cnbvjfdhgidjigfvf1231943"        ' can be any string
            hashAlgorithm = "SHA1"             ' can be "MD5"
            passwordIterations = 2                  ' can be any number
            initVector = "FNitsD4Ab@Anup@@" ' must be 16 bytes
            keySize = 256                ' can be 192 or 128
            strLicence = objBusinessClass.Decrypt(ConfigurationManager.AppSettings("LicenceKey"), _
                                            passPhrase, _
                                            saltValue, _
                                            hashAlgorithm, _
                                            passwordIterations, _
                                            initVector, _
                                            keySize).Split("~")
            ' strLicence = Cryptographer.DecryptSymmetric(symmProvider, ConfigurationManager.AppSettings("LicenceKey")).Split("~")
        Catch ex As Exception
            Throw ex
        End Try
        If strLicence.Length = 0 Then
            Session("NoOfLicence") = 0
        Else
            Session("NoOfLicence") = strLicence(2)
        End If


    End Sub


    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires at the beginning of each request
    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires upon attempting to authenticate the use
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when an error occurs
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session ends
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application ends
    End Sub

End Class
