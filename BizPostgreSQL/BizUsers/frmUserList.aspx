<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmUserList.aspx.vb" Inherits="BizUsers.frmUserList" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>frmUserList</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="biz.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function GetUsers()
		{

			var str='';
	
		for(var i=0; i<dgUserAssociation.rows.length-1; i++)
			{
				if (document.all['dgUserAssociation__ctl' + (i+2)+ '_chkCheck'].checked==true)
				{
				str=str+'1,'
				}
				else
				{
				str=str+'0,'
				}
			}
			document.Form1.txtValue.value=str
		}
		function Cancel()
		{
			window.location.href("Default.aspx")
			return false;
		}
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table width="100%">
				<tr>
					<td align="right"><IMG src="images/biz.JPG" width="326" height="63">
					</td>
				</tr>
			</table>
			<table align="center" width="100%">
				<tr>
					<td class="normal1" align="center"><asp:literal id="litMessage" Runat="server" EnableViewState="False"></asp:literal></td>
				</tr>
			</table>
			<table>
			<tr>
			<td class=normal1>
			Organization
			</td>
			<td colspan=8>
			<asp:DropDownList ID=ddlOrg runat=server CssClass=normal1 Width=200></asp:DropDownList>
			</td>
			</tr>
			<tr>
			<td class=normal1>
			Display Name
			</td>
			<td>
			<asp:TextBox ID=txtFirstName runat=server ></asp:TextBox>
			</td>
			<td class=normal1>
			Mail Nickname
			</td>
			<td>
			<asp:TextBox ID=txtMailNickName runat=server ></asp:TextBox>
			</td >
			<td class=normal1>
			Email Address
			</td>
			<td>
			<asp:TextBox ID=txtEmail runat=server ></asp:TextBox>
			</td>
			<td>
			<asp:Button CssClass=button  ID=btnGo  Width="50" Text="Go" runat=server />
			</td>
			</tr>
			</table>
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				
				<tr>
					<td vAlign="bottom">
						<table class="TabStyle" borderColor="black" cellSpacing="0" border="1">
							<tr>
								<td class="Text_bold_White" noWrap height="23">&nbsp;&nbsp;&nbsp;User 
									Association&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td align="center" class="normal1">
						No of License :
						<asp:label ID="lblLicence" Runat="server"></asp:label>
					</td>
					<td align="right">
						<asp:button id="btnAdd" CssClass="button" Runat="server" Text="Add Users To BizAutomation"></asp:button>
						<asp:button id="btnCancel" CssClass="button" Runat="server" Text="Back" Width="50"></asp:button>
					</td>
				</tr>
				<tr>
					<td colSpan="3"><asp:table id="tblUser" Runat="server" BorderWidth="1" BorderColor="black" GridLines="None"
							Width="100%" CellSpacing="0" CellPadding="0" Height="350">
							<asp:TableRow>
								<asp:TableCell VerticalAlign="Top">
									<asp:datagrid id="dgUserAssociation" runat="server" CssClass="dg" Width="100%" AutoGenerateColumns="False">
										<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
										<ItemStyle CssClass="is"></ItemStyle>
										<HeaderStyle CssClass="hs"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn HeaderText="Sl No">
												<ItemTemplate>
													<%# Container.ItemIndex +1 %>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="DisplayName" HeaderText="Display Name"></asp:BoundColumn>
											<asp:BoundColumn DataField="UserName" HeaderText="User Name"></asp:BoundColumn>
											<asp:BoundColumn DataField="Mailname" HeaderText="Mail Name"></asp:BoundColumn>
											<asp:BoundColumn DataField="mail" HeaderText="Email Address"></asp:BoundColumn>
											<asp:TemplateColumn>
												<ItemTemplate>
													<asp:CheckBox ID="chkCheck" Runat="server"></asp:CheckBox>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid>
								</asp:TableCell>
							</asp:TableRow>
						</asp:table></td>
				</tr>
			</table>
			
			<asp:textbox id="txtValue" style="DISPLAY: none" Runat="server"></asp:textbox></form>
	</body>
</HTML>

