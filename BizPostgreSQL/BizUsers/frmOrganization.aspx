<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmOrganization.aspx.vb" Inherits="BizUsers.frmOrganization" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Organization</title>
    <LINK href="biz.css" type="text/css" rel="stylesheet">
</head>
<body>
    <form id="form1" runat="server">
    <table width="100%">
				<tr>
					<td align="right"><IMG src="images/biz.JPG" width="326" height="63">
					</td>
				</tr>
			</table>
	
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td vAlign="bottom">
						<table class="TabStyle" borderColor="black" cellSpacing="0" border="1">
							<tr>
								<td class="Text_bold_White" noWrap height="23">&nbsp;&nbsp;&nbsp;Organization&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td align="right">
						<asp:button id="btnClose" Runat="server" CssClass="button" Text="Close"></asp:button></td>
				</tr>
			</table>
			<asp:table id="Table2" CellPadding="0" CellSpacing="0" BorderWidth="1" Runat="server" Width="100%"
				BorderColor="black" GridLines="None" Height="300">
				<asp:TableRow>
					<asp:TableCell VerticalAlign="Top">
						<table width="80%" align=center >
							<tr>
								<td>
									<br>
								</td>
							</tr>
							<tr>
								<td class="normal1" align="right">
									Organization
								</td>
								<td>
									<asp:TextBox ID="txtOrganization" Runat="server" CssClass="signup"></asp:TextBox>
								</td>
						
								<td>
									<asp:Button ID="btnAdd" Runat="server" Text="Add" Width="50" CssClass="button"></asp:Button>
								</td>
							</tr>
							<tr>
								<td>
									<br>
								</td>
							</tr>
						</table>
						<table width="100%" CellPadding="0" CellSpacing="0">
							<tr>
								<td colSpan="5">
									<asp:datagrid id="dgOrganization" runat="server" CssClass="dg" BorderColor="white" AutoGenerateColumns="False"
										AllowSorting="True" Width="100%">
										<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
										<ItemStyle CssClass="is"></ItemStyle>
										<HeaderStyle CssClass="hs"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<ItemTemplate>
													<asp:LinkButton runat="server" Text="Edit" CommandName="Edit" ID="lnkbtnEdt"></asp:LinkButton>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:LinkButton ID="lnkbtnUpdt" runat="server" Text="Update" CommandName="Update"></asp:LinkButton>&nbsp;
													<asp:LinkButton runat="server" Text="Cancel" CommandName="Cancel" ID="lnkbtnCncl"></asp:LinkButton>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn>
												<ItemTemplate>
													<asp:Label ID="lblOrgID" Runat="server" Visible=False Text= '<%# DataBinder.Eval(Container,"DataItem.numDomainId") %>'>
													</asp:Label>
													<%# Container.ItemIndex +1 %>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Organization" ItemStyle-HorizontalAlign=Left >
												<ItemTemplate>
													<asp:Label ID="lblOrg" Runat="server" Text= '<%# DataBinder.Eval(Container,"DataItem.vcDomainName") %>'>
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:TextBox ID="txtEOrg" Runat="server" CssClass="signup" Text= '<%# DataBinder.Eval(Container,"DataItem.vcDomainName") %>'>
													</asp:TextBox>
												</EditItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid></td>
							</tr>
						</table>
					</asp:TableCell>
				</asp:TableRow>
			</asp:table>
			<table width="100%">
				<tr>
					<td class="normal4" align="center">
						<asp:Literal ID="litMessage" Runat="server" EnableViewState=False ></asp:Literal>
					</td>
				</tr>
			</table>
    </form>
</body>
</html>