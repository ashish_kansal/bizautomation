Partial Public Class frmOrganization
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadDeatils()
        End If
    End Sub

    Sub LoadDeatils()
        Dim objBusinessClass As New BusinessClass
        objBusinessClass.byteMode = 2
        dgOrganization.DataSource = objBusinessClass.ManageDomain(ConfigurationManager.AppSettings("ConnectionString"))
        dgOrganization.DataBind()
    End Sub

    Protected Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Response.Redirect("default.aspx")
    End Sub

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim objBusinessClass As New BusinessClass
        objBusinessClass.byteMode = 0
        objBusinessClass.DomainName = txtOrganization.Text
        objBusinessClass.ManageDomain(ConfigurationManager.AppSettings("ConnectionString"))
        LoadDeatils()
    End Sub

    Private Sub dgOrganization_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgOrganization.EditCommand
        Try
            dgOrganization.EditItemIndex = e.Item.ItemIndex
            Call LoadDeatils()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgOrganization_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgOrganization.ItemCommand
        Try
            If e.CommandName = "Cancel" Then
                dgOrganization.EditItemIndex = e.Item.ItemIndex
                dgOrganization.EditItemIndex = -1
                Call LoadDeatils()
            End If
            If e.CommandName = "Update" Then
                Dim objBusinessClass As New BusinessClass
                objBusinessClass.byteMode = 1
                objBusinessClass.DomainName = CType(e.Item.FindControl("txtEOrg"), TextBox).Text
                objBusinessClass.DomianID = CType(e.Item.FindControl("lblOrgID"), Label).Text
                objBusinessClass.ManageDomain(ConfigurationManager.AppSettings("ConnectionString"))
                dgOrganization.EditItemIndex = -1
                LoadDeatils()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class