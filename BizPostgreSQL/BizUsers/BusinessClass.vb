Imports System.Data.SqlClient
Imports System
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography
Public Class BusinessClass
    Private _DomainName As String
    Private _strUserString As String
    Private _ActivateFlag As Short
    Private _UserName As String
    Private _UserID As Long
    Private _NoofLicence As Long
    Private _EmailID As String
    Private _DisplayName As String
    Private _MailName As String
    Private _DomianID As Long
    Private _byteMode As Short

    Public Property byteMode() As Short
        Get
            Return _byteMode
        End Get
        Set(ByVal Value As Short)
            _byteMode = Value
        End Set
    End Property

    Public Property DomianID() As Long
        Get
            Return _DomianID
        End Get
        Set(ByVal Value As Long)
            _DomianID = Value
        End Set
    End Property

    Public Property MailName() As String
        Get
            Return _MailName
        End Get
        Set(ByVal Value As String)
            _MailName = Value
        End Set
    End Property

    Public Property DisplayName() As String
        Get
            Return _DisplayName
        End Get
        Set(ByVal Value As String)
            _DisplayName = Value
        End Set
    End Property

    Public Property EmailID() As String
        Get
            Return _EmailID
        End Get
        Set(ByVal Value As String)
            _EmailID = Value
        End Set
    End Property

    Public Property NoofLicence() As Long
        Get
            Return _NoofLicence
        End Get
        Set(ByVal Value As Long)
            _NoofLicence = Value
        End Set
    End Property

    Public Property UserID() As Long
        Get
            Return _UserID
        End Get
        Set(ByVal Value As Long)
            _UserID = Value
        End Set
    End Property

    Public Property UserName() As String
        Get
            Return _UserName
        End Get
        Set(ByVal Value As String)
            _UserName = Value
        End Set
    End Property

    Public Property ActivateFlag() As Short
        Get
            Return _ActivateFlag
        End Get
        Set(ByVal Value As Short)
            _ActivateFlag = Value
        End Set
    End Property

    Public Property StrUserString() As String
        Get
            Return _StrUserString
        End Get
        Set(ByVal Value As String)
            _StrUserString = Value
        End Set
    End Property

    Public Property DomainName() As String
        Get
            Return _DomainName
        End Get
        Set(ByVal Value As String)
            _DomainName = Value
        End Set
    End Property

#Region "Constructor"


    Public Sub New()
    End Sub



#End Region

    Public Function GetUserList(ByVal connString As String) As DataTable
        Try
            Dim arParms() As SqlParameter = New SqlParameter(0) {}
            Dim ds As DataSet
            arParms(0) = New SqlParameter("@DomainID", SqlDbType.VarChar, 100)
            arParms(0).Value = _DomianID

            ds = SqlDAL.ExecuteDataset(connString, "usp_GetUsersListForActivationChange", arParms)

            Return ds.Tables(0)
        Catch ex As Exception
            ' Log exception details
            Throw ex
        End Try
    End Function

    Public Function SaveUserStatus(ByVal connString As String) As Long
        Try
            Dim arParms() As SqlParameter = New SqlParameter(2) {}

            arParms(0) = New SqlParameter("@numUserID", SqlDbType.BigInt)
            arParms(0).Value = _UserID

            arParms(1) = New SqlParameter("@Active", SqlDbType.TinyInt)
            arParms(1).Value = _ActivateFlag

            arParms(2) = New SqlParameter("@noOfLicences", SqlDbType.BigInt)
            arParms(2).Value = _NoofLicence


            Return SqlDAL.ExecuteScalar(connString, "USP_SaveUserStatus", arParms)
        Catch ex As Exception
            ' Log exception details
            Throw ex
        End Try
    End Function

    Public Function GetUsers(ByVal connString As String) As DataTable
        Try
            Dim arParms() As SqlParameter = New SqlParameter(0) {}
            Dim ds As DataSet
            arParms(0) = New SqlParameter("@bitActivateFlag", SqlDbType.TinyInt)
            arParms(0).Value = _ActivateFlag

            ds = SqlDAL.ExecuteDataset(connString, "USP_GetUsersFromBiz", arParms)
            Return ds.Tables(0)
        Catch ex As Exception
            ' Log exception details
            Throw ex
        End Try
    End Function

    Public Function CreateNewUsers(ByVal connString As String) As Long
        Try
            Dim arParms() As SqlParameter = New SqlParameter(5) {}
            arParms(0) = New SqlParameter("@vcUserName", SqlDbType.VarChar, 100)
            arParms(0).Value = _UserName

            arParms(1) = New SqlParameter("@vcDomain", SqlDbType.VarChar, 100)
            arParms(1).Value = _DomainName

            arParms(2) = New SqlParameter("@noOfLicences", SqlDbType.BigInt)
            arParms(2).Value = _NoofLicence

            arParms(3) = New SqlParameter("@vcEmailID", SqlDbType.VarChar, 100)
            arParms(3).Value = _EmailID


            arParms(4) = New SqlParameter("@vcDisplayName", SqlDbType.VarChar, 100)
            arParms(4).Value = _DisplayName

            arParms(5) = New SqlParameter("@vcMailName", SqlDbType.VarChar, 100)
            arParms(5).Value = _MailName

            Return SqlDAL.ExecuteScalar(connString, "USP_CreateUsers", arParms)

        Catch ex As Exception
            ' Log exception details
            Throw ex
        End Try
    End Function

    Public Function ManageDomain(ByVal connString As String) As DataTable
        Try
            Dim arParms() As SqlParameter = New SqlParameter(2) {}


            arParms(0) = New SqlParameter("@byteMode", SqlDbType.TinyInt)
            arParms(0).Value = _byteMode

            arParms(1) = New SqlParameter("@vcDomainName", SqlDbType.VarChar, 50)
            arParms(1).Value = _DomainName

            arParms(2) = New SqlParameter("@numDomainId", SqlDbType.BigInt)
            arParms(2).Value = _DomianID

            Return SqlDAL.ExecuteDataset(connString, "USP_ManageDomain", arParms).Tables(0)

        Catch ex As Exception
            ' Log exception details
            Throw ex
        End Try
    End Function

    Public Function Encrypt(ByVal plainText As String, _
                              ByVal passPhrase As String, _
                              ByVal saltValue As String, _
                              ByVal hashAlgorithm As String, _
                              ByVal passwordIterations As Integer, _
                              ByVal initVector As String, _
                              ByVal keySize As Integer) _
                      As String

        ' Convert strings into byte arrays.
        ' Let us assume that strings only contain ASCII codes.
        ' If strings include Unicode characters, use Unicode, UTF7, or UTF8 
        ' encoding.
        Dim initVectorBytes As Byte()
        initVectorBytes = Encoding.ASCII.GetBytes(initVector)

        Dim saltValueBytes As Byte()
        saltValueBytes = Encoding.ASCII.GetBytes(saltValue)

        ' Convert our plaintext into a byte array.
        ' Let us assume that plaintext contains UTF8-encoded characters.
        Dim plainTextBytes As Byte()
        plainTextBytes = Encoding.UTF8.GetBytes(plainText)

        ' First, we must create a password, from which the key will be derived.
        ' This password will be generated from the specified passphrase and 
        ' salt value. The password will be created using the specified hash 
        ' algorithm. Password creation can be done in several iterations.
        Dim password As PasswordDeriveBytes
        password = New PasswordDeriveBytes(passPhrase, _
                                           saltValueBytes, _
                                           hashAlgorithm, _
                                           passwordIterations)

        ' Use the password to generate pseudo-random bytes for the encryption
        ' key. Specify the size of the key in bytes (instead of bits).
        Dim keyBytes As Byte()
        keyBytes = password.GetBytes(keySize / 8)

        ' Create uninitialized Rijndael encryption object.
        Dim symmetricKey As RijndaelManaged
        symmetricKey = New RijndaelManaged()

        ' It is reasonable to set encryption mode to Cipher Block Chaining
        ' (CBC). Use default options for other symmetric key parameters.
        symmetricKey.Mode = CipherMode.CBC

        ' Generate encryptor from the existing key bytes and initialization 
        ' vector. Key size will be defined based on the number of the key 
        ' bytes.
        Dim encryptor As ICryptoTransform
        encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes)

        ' Define memory stream which will be used to hold encrypted data.
        Dim memoryStream As MemoryStream
        memoryStream = New MemoryStream()

        ' Define cryptographic stream (always use Write mode for encryption).
        Dim cryptoStream As CryptoStream
        cryptoStream = New CryptoStream(memoryStream, _
                                        encryptor, _
                                        CryptoStreamMode.Write)
        ' Start encrypting.
        cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length)

        ' Finish encrypting.
        cryptoStream.FlushFinalBlock()

        ' Convert our encrypted data from a memory stream into a byte array.
        Dim cipherTextBytes As Byte()
        cipherTextBytes = memoryStream.ToArray()

        ' Close both streams.
        memoryStream.Close()
        cryptoStream.Close()

        ' Convert encrypted data into a base64-encoded string.
        Dim cipherText As String
        cipherText = Convert.ToBase64String(cipherTextBytes)

        ' Return encrypted string.
        Encrypt = cipherText
    End Function

    ' <summary>
    ' Decrypts specified ciphertext using Rijndael symmetric key algorithm.
    ' </summary>
    ' <param name="cipherText">
    ' Base64-formatted ciphertext value.
    ' </param>
    ' <param name="passPhrase">
    ' Passphrase from which a pseudo-random password will be derived. The 
    ' derived password will be used to generate the encryption key. 
    ' Passphrase can be any string. In this example we assume that this 
    ' passphrase is an ASCII string.
    ' </param>
    ' <param name="saltValue">
    ' Salt value used along with passphrase to generate password. Salt can 
    ' be any string. In this example we assume that salt is an ASCII string.
    ' </param>
    ' <param name="hashAlgorithm">
    ' Hash algorithm used to generate password. Allowed values are: "MD5" and
    ' "SHA1". SHA1 hashes are a bit slower, but more secure than MD5 hashes.
    ' </param>
    ' <param name="passwordIterations">
    ' Number of iterations used to generate password. One or two iterations
    ' should be enough.
    ' </param>
    ' <param name="initVector">
    ' Initialization vector (or IV). This value is required to encrypt the 
    ' first block of plaintext data. For RijndaelManaged class IV must be 
    ' exactly 16 ASCII characters long.
    ' </param>
    ' <param name="keySize">
    ' Size of encryption key in bits. Allowed values are: 128, 192, and 256. 
    ' Longer keys are more secure than shorter keys.
    ' </param>
    ' <returns>
    ' Decrypted string value.
    ' </returns>
    ' <remarks>
    ' Most of the logic in this function is similar to the Encrypt 
    ' logic. In order for decryption to work, all parameters of this function
    ' - except cipherText value - must match the corresponding parameters of 
    ' the Encrypt function which was called to generate the 
    ' ciphertext.
    ' </remarks>
    Public Function Decrypt(ByVal cipherText As String, _
                                   ByVal passPhrase As String, _
                                   ByVal saltValue As String, _
                                   ByVal hashAlgorithm As String, _
                                   ByVal passwordIterations As Integer, _
                                   ByVal initVector As String, _
                                   ByVal keySize As Integer) _
                           As String

        ' Convert strings defining encryption key characteristics into byte
        ' arrays. Let us assume that strings only contain ASCII codes.
        ' If strings include Unicode characters, use Unicode, UTF7, or UTF8
        ' encoding.
        Dim initVectorBytes As Byte()
        initVectorBytes = Encoding.ASCII.GetBytes(initVector)

        Dim saltValueBytes As Byte()
        saltValueBytes = Encoding.ASCII.GetBytes(saltValue)

        ' Convert our ciphertext into a byte array.
        Dim cipherTextBytes As Byte()
        cipherTextBytes = Convert.FromBase64String(cipherText)

        ' First, we must create a password, from which the key will be 
        ' derived. This password will be generated from the specified 
        ' passphrase and salt value. The password will be created using
        ' the specified hash algorithm. Password creation can be done in
        ' several iterations.
        Dim password As PasswordDeriveBytes
        password = New PasswordDeriveBytes(passPhrase, _
                                           saltValueBytes, _
                                           hashAlgorithm, _
                                           passwordIterations)

        ' Use the password to generate pseudo-random bytes for the encryption
        ' key. Specify the size of the key in bytes (instead of bits).
        Dim keyBytes As Byte()
        keyBytes = password.GetBytes(keySize / 8)

        ' Create uninitialized Rijndael encryption object.
        Dim symmetricKey As RijndaelManaged
        symmetricKey = New RijndaelManaged()

        ' It is reasonable to set encryption mode to Cipher Block Chaining
        ' (CBC). Use default options for other symmetric key parameters.
        symmetricKey.Mode = CipherMode.CBC

        ' Generate decryptor from the existing key bytes and initialization 
        ' vector. Key size will be defined based on the number of the key 
        ' bytes.
        Dim decryptor As ICryptoTransform
        decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes)

        ' Define memory stream which will be used to hold encrypted data.
        Dim memoryStream As MemoryStream
        memoryStream = New MemoryStream(cipherTextBytes)

        ' Define memory stream which will be used to hold encrypted data.
        Dim cryptoStream As CryptoStream
        cryptoStream = New CryptoStream(memoryStream, _
                                        decryptor, _
                                        CryptoStreamMode.Read)

        ' Since at this point we don't know what the size of decrypted data
        ' will be, allocate the buffer long enough to hold ciphertext;
        ' plaintext is never longer than ciphertext.
        Dim plainTextBytes As Byte()
        ReDim plainTextBytes(cipherTextBytes.Length)

        ' Start decrypting.
        Dim decryptedByteCount As Integer
        decryptedByteCount = cryptoStream.Read(plainTextBytes, _
                                               0, _
                                               plainTextBytes.Length)

        ' Close both streams.
        memoryStream.Close()
        cryptoStream.Close()

        ' Convert decrypted data into a string. 
        ' Let us assume that the original plaintext string was UTF8-encoded.
        Dim plainText As String
        plainText = Encoding.UTF8.GetString(plainTextBytes, _
                                            0, _
                                            decryptedByteCount)

        ' Return decrypted string.
        Decrypt = plainText
    End Function

End Class
