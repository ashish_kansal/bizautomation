
Imports ActiveDs
Imports System.Data.SqlClient
Imports Microsoft.Win32
Imports Microsoft.Win32.Registry
Imports Microsoft.Win32.RegistryKey


Module modUsers

    Public g_strDomainName As String
    Public g_lngDomainID As Long
    Public Const BA_NEW_FOLDERNAME = "BizAutomation"
    Public Const BA_NEW_SUB_FOLDERNAME = "Schema"
    Public Const BA_FILE_NAME = "go.asp"
    Private Const conBACRMRegKey As String = "SOFTWARE\BACRM"


    Private Structure RegData
        Public Value As String
        Public Data As String
        Public Overrides Function ToString() As String
            Return Me.Value
        End Function
    End Structure



    Public Function fn_CreateFolder(ByRef strLocalPathOfParentFolder As String, ByRef strNewFolderName As String, ByRef strNewSubFolderName As String, ByRef strFileNPath As String, ByRef strFileName As String, ByVal strExServer As String, ByVal strNames As String, ByVal path As String, ByVal siteType As String, ByVal MailName As String) As Boolean

        '================================================================================
        ' Purpose: This function will create the Exchange Folder (name passed as parameter)
        '           for the user (name passed as parameter). After the folder is created,
        '           it will create a hidden subfolder (named Schema) and copy the ASP file
        '           passed as Parameter into this subfolder. It will set the schema to open
        '           the ASP file when the end user clicks on the Folder from within OWA.
        '
        ' Parameters:
        '   g_strDomainName = Domain Name.
        '   strLocalPathOfParentFolder = The path "MBX/UserName/...." where the new folder should be created.
        '   strNewFolderName = The new Folder name.
        '   strNewSubFolderName = The New Sub Folder Name.
        '   strFileNPath = The ASP File that needs to be copied.
        '   strFileName = The name of the ASP File.
        '
        ' History
        ' Ver   Date        Author              Reason
        ' 1     05/06/2003  Anup Jishnu         Created
        ' 2     03/02/2004  Vishwanath          Modified to Convert it into WebDAV
        ' Non Compliance (any deviation from standards)
        '   No deviations from the standards.
        '================================================================================

        Dim strFolderurl As String
        Dim strSubFolderurl As String
        Dim myHttpWebRequest As System.Net.HttpWebRequest
        Dim myHttpWebResponse As System.Net.HttpWebResponse
        Dim strQuery As String
        Dim strRegisterform As String
        Dim strfilepath As String
        Dim strCopyFile As String
        Dim strgetName() As String
        Dim strUserName As String

        Try
            'Getting the username from the String
            strgetName = Split(strLocalPathOfParentFolder, "/")
            strUserName = strgetName(1)

            '*************************************************************
            ' Creating the Main Folder
            '*************************************************************
            strFolderurl = siteType & "//" & strExServer & "/exchange/" & strUserName & "/" & strNewFolderName & "/"
            ' Create a 'HttpWebRequest' object with the specified url. 


            myHttpWebRequest = CType(System.Net.WebRequest.Create(strFolderurl), System.Net.HttpWebRequest)
            ' Initialize variables.
            'myHttpWebRequest.Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim cred As System.Net.NetworkCredential = New System.Net.NetworkCredential("superuser", ConfigurationManager.AppSettings("superuser"), ConfigurationManager.AppSettings("ExchangeDomainName"))
            Dim credcache As System.Net.CredentialCache = New System.Net.CredentialCache
            credcache.Add(New Uri(strFolderurl), "Basic", cred)

            myHttpWebRequest.Credentials = credcache

            myHttpWebRequest.Method = "MKCOL"
            myHttpWebResponse = CType(myHttpWebRequest.GetResponse(), System.Net.HttpWebResponse)


            '************************************************************
            ' URL for the Sub Folder to be created
            '************************************************************
            strSubFolderurl = siteType & "//" & strExServer & "/exchange/" & strUserName & "/" & strNewFolderName & "/" & strNewSubFolderName & "/"

            '************************************************************
            ' Configuring the main folder
            '************************************************************
            strQuery = "<?xml version=""1.0""?>" & _
           "<d:propertyupdate xmlns:d=""DAV:"" xmlns:ex=""urn:schemas-microsoft-com:exch-data:"">" & _
            "<d:set>" & _
            "<d:prop> " & _
            "<d:contentclass>NewBACRM:content-classes:BACRM</d:contentclass>" & _
            "<ex:schema-collection-ref>" & strSubFolderurl & "</ex:schema-collection-ref>" & _
            "" & _
            "</d:prop>" & _
            "</d:set>" & _
            "</d:propertyupdate>"

            If configuration(strFolderurl, strQuery) = False Then Return False

            '*************************************************************
            ' Creating the Sub Folder
            '*************************************************************
            ' Create a 'HttpWebRequest' object with the specified url. 
            myHttpWebRequest = CType(System.Net.WebRequest.Create(strSubFolderurl), System.Net.HttpWebRequest)
            myHttpWebRequest = CType(System.Net.WebRequest.Create(strSubFolderurl), System.Net.HttpWebRequest)
            ' Initialize variables.
            'myHttpWebRequest.Credentials = System.Net.CredentialCache.DefaultCredentials
            'Dim cred As System.Net.NetworkCredential = New System.Net.NetworkCredential("superuser", ConfigurationManager.AppSettings("superuser"), ConfigurationManager.AppSettings("EmailSuffix").Split(".")(0))
            'Dim credcache As System.Net.CredentialCache = New System.Net.CredentialCache
            credcache.Add(New Uri(strSubFolderurl), "Basic", cred)

            myHttpWebRequest.Credentials = credcache

            myHttpWebRequest.Method = "MKCOL"
            myHttpWebResponse = CType(myHttpWebRequest.GetResponse(), System.Net.HttpWebResponse)
            myHttpWebResponse.Close()

            '****************************************************************
            ' Hidding the Sub Folder
            '****************************************************************
            strSubFolderurl = siteType & "//" & strExServer & "/exchange/" & strUserName & "/" & strNewFolderName & "/" & strNewSubFolderName
            strQuery = "<?xml version=""1.0""?>" & _
                       "<d:propertyupdate xmlns:d=""DAV:"">" & _
                        "<d:set>" & _
                        "<d:prop> " & _
                        "<d:contentclass>urn:content-classes:schemafld</d:contentclass>" & _
                        "<d:ishidden>1</d:ishidden>" & _
                        "</d:prop>" & _
                        "</d:set>" & _
                        "</d:propertyupdate>"

            If configuration(strSubFolderurl, strQuery) = False Then Return False

            '***************************************************************
            'Configuration of the Folder
            '**************************************************************
            strQuery = "<?xml version=""1.0""?>" & _
           "<d:propertyupdate xmlns:d=""DAV:"" xmlns:ex=""urn:schemas-microsoft-com:exch-data:"">" & _
            "<d:set>" & _
            "<d:prop> " & _
            "<ex:schema-collection-ref>" & strSubFolderurl & "</ex:schema-collection-ref>" & _
            "</d:prop>" & _
            "</d:set>" & _
            "</d:propertyupdate>"

            If configuration(strFolderurl, strQuery) = False Then Return False

            Dim strFileURL As String

            strFileURL = ConfigurationManager.AppSettings("exchangePath") & "/" & MailName & "/bizautomation/schema/go.asp"
            '*********************************************************************
            'Registration of the Form
            '**********************************************************************
            strRegisterform = "<?xml version=""1.0""?>" & _
                                "<g:propertyupdate xmlns:g=""DAV:"" " & _
                                "xmlns:form=""urn:schemas-microsoft-com:office:forms"">" & _
                                "<g:set>" & _
                                "<g:prop>" & _
                                "<g:contentclass>urn:schemas-microsoft-com:office:forms#registration</g:contentclass>" & _
                                "<form:contentclass>NewBACRM:content-classes:BACRM</form:contentclass>" & _
                                "<form:request>GET</form:request>" & _
                                "<form:cmd>*</form:cmd>" & _
                                "<form:binding>server</form:binding>" & _
                                "<form:formurl>" & strFileURL & "</form:formurl>" & _
                                "<form:executeurl>" & strFileURL & "</form:executeurl>" & _
                                "<form:platform>*</form:platform>" & _
                                "<form:executeparameters>*</form:executeparameters>" & _
                                "<form:contentstate>*</form:contentstate>" & _
                                "<form:browser>*</form:browser>" & _
                                "<form:majorver>*</form:majorver>" & _
                                "<form:minorver>*</form:minorver>" & _
                                "<form:version>*</form:version>" & _
                                "<form:messagestate>*</form:messagestate>" & _
                                "<form:language>*</form:language>" & _
                                "</g:prop>" & _
                                "</g:set>" & _
                                "</g:propertyupdate>"
            strfilepath = strSubFolderurl & "/BACRM.reg"
            If configuration(strfilepath, strRegisterform) = False Then Return False
            'strCopyFile = "<%" & vbCrLf
            'strCopyFile = strCopyFile & "Dim LSservername" & vbCrLf
            'strCopyFile = strCopyFile & "Dim siteType" & vbCrLf
            'strCopyFile = strCopyFile & "If Request.ServerVariables(" & """HTTPS" & """) = " & """off" & """ Then" & vbCrLf
            'strCopyFile = strCopyFile & "siteType= " & """http:" & """ " & vbCrLf
            'strCopyFile = strCopyFile & "else" & vbCrLf
            'strCopyFile = strCopyFile & "siteType= " & """https:" & """ " & vbCrLf
            'strCopyFile = strCopyFile & "end if" & vbCrLf
            'strCopyFile = strCopyFile & "LSservername = request.servervariables(" & """SERVER_NAME" & """)" & vbCrLf
            'strCopyFile = strCopyFile & "LSservername = siteType & " & """//" & """ &  LSservername %>" & vbCrLf
            'strCopyFile = strCopyFile & "<html>" & vbCrLf
            'strCopyFile = strCopyFile & "<body onLoad=" & """location.href='<%=LSservername%>" & path & "'" & """>" & vbCrLf
            'strCopyFile = strCopyFile & "</body></html>"

            'strCopyFile = "<%" & vbCrLf
            'strCopyFile = strCopyFile & "Dim LSservername" & vbCrLf
            'strCopyFile = strCopyFile & "Dim siteType" & vbCrLf
            'strCopyFile = strCopyFile & "siteType= " & """http:" & """ " & vbCrLf
            'strCopyFile = strCopyFile & "LSservername = request.servervariables(" & """SERVER_NAME" & """)" & vbCrLf
            'strCopyFile = strCopyFile & "LSservername = siteType & " & """//" & """ &  LSservername %>" & vbCrLf
            'strCopyFile = strCopyFile & "<html>" & vbCrLf
            'strCopyFile = strCopyFile & "<body onLoad=" & """location.href='<%=LSservername%>" & path & "'" & """>" & vbCrLf
            'strCopyFile = strCopyFile & "</body></html>"

            'strCopyFile = "<%" & vbCrLf
            'strCopyFile = strCopyFile & "Dim LSservername" & vbCrLf
            'strCopyFile = strCopyFile & "LSservername = request.servervariables(" & """SERVER_NAME" & """) %>" & vbCrLf
            'strCopyFile = strCopyFile & "<html>" & vbCrLf
            'strCopyFile = strCopyFile & "<body onLoad=" & """location.href='" & path & "'" & """ > " & vbCrLf
            'strCopyFile = strCopyFile & "</body></html>"

            strCopyFile = "<%" & vbCrLf
            strCopyFile = strCopyFile & "Dim LSservername" & vbCrLf
            strCopyFile = strCopyFile & "LSservername = request.servervariables(" & """SERVER_NAME" & """) %>" & vbCrLf
            strCopyFile = strCopyFile & "<html>" & vbCrLf
            strCopyFile = strCopyFile & "<body onLoad='' > " & vbCrLf
            strCopyFile = strCopyFile & "<script language='javascript'>" & vbCrLf
            strCopyFile = strCopyFile & " document.location.href = '" & ConfigurationManager.AppSettings("Path") & "?ClientMachineUTCTimeOffset='+new Date().getTimezoneOffset();"
            strCopyFile = strCopyFile & "</script></body></html>"

            strfilepath = strSubFolderurl & "/go.asp"
            '*** To create the go.asp file runtime.
            If generateFile(strfilepath, strCopyFile) = False Then Return False

            Return True
        Catch ex As Exception
            Throw ex
            Return False
        End Try
    End Function

    Public Function configuration(ByVal strURL, ByVal strApptRequest) As Boolean
        '================================================================================
        ' Purpose: This function is Called from function fn_InsertOWAActionItem and fn_UpdateOWAActionItem for creating New Action Item
        '
        ' History
        ' Ver   Date        Author            Reason
        ' 1     20/01/2004  Vishwanath K      Created
        '
        ' Non Compliance (any deviation from standards)
        '   No deviations from the standards.   
        '================================================================================

        Dim bytes() As Byte

        Dim strWebRequest As System.Net.HttpWebRequest
        Dim strWebResponse As System.Net.HttpWebResponse
        Dim RequestStream As System.IO.Stream
        Dim ResponseStream As System.IO.Stream
        Try

            strWebRequest = CType(System.Net.WebRequest.Create(strURL), System.Net.HttpWebRequest)

            'Add the network credential to the request.
            ' strWebRequest.Credentials = System.Net.CredentialCache.DefaultCredentials

            Dim cred As System.Net.NetworkCredential = New System.Net.NetworkCredential("superuser", ConfigurationManager.AppSettings("superuser"), ConfigurationManager.AppSettings("ExchangeDomainName"))
            Dim credcache As System.Net.CredentialCache = New System.Net.CredentialCache
            credcache.Add(New Uri(strURL), "Basic", cred)

            strWebRequest.Credentials = credcache

            'Specify the search method.
            strWebRequest.Method = "PROPPATCH"


            'SEt the content type header.
            strWebRequest.ContentType = "text/xml"

            'Encode the body using UTF-8.
            bytes = System.Text.Encoding.UTF8.GetBytes(strApptRequest)

            'Set the content header length. This must be done before writing data to the 
            'request stream.
            strWebRequest.ContentLength = bytes.Length

            'Get a reference to the request stream.
            RequestStream = strWebRequest.GetRequestStream()

            'Write the message body to the request stream.
            RequestStream.Write(bytes, 0, bytes.Length)

            'Close the Stream object to release the connection for further use.
            RequestStream.Close()


            'Send the SEARCH method request and get the response from the server.
            strWebResponse = strWebRequest.GetResponse()

            ResponseStream = strWebResponse.GetResponseStream
            ResponseStream.Close()
            strWebResponse.Close()

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function generateFile(ByVal strURL, ByVal strApptRequest) As Boolean
        '================================================================================
        ' Purpose: This function is used by function fn_CreateFolder to create File.
        '
        ' History
        ' Ver   Date        Author            Reason
        ' 1     20/01/2004  Vishwanath K      Created
        '
        ' Non Compliance (any deviation from standards)
        '   No deviations from the standards.   
        '================================================================================

        Dim bytes() As Byte
        Dim strWebRequest As System.Net.HttpWebRequest
        Dim strWebResponse As System.Net.HttpWebResponse
        Dim RequestStream As System.IO.Stream
        Dim ResponseStream As System.IO.Stream
        Try

            strWebRequest = CType(System.Net.HttpWebRequest.Create(strURL), System.Net.HttpWebRequest)

            'Add the network credential to the request.
            'strWebRequest.Credentials = System.Net.CredentialCache.DefaultCredentials

            Dim cred As System.Net.NetworkCredential = New System.Net.NetworkCredential("superuser", ConfigurationManager.AppSettings("superuser"), ConfigurationManager.AppSettings("ExchangeDomainName"))
            Dim credcache As System.Net.CredentialCache = New System.Net.CredentialCache
            credcache.Add(New Uri(strURL), "Basic", cred)

            strWebRequest.Credentials = credcache

            'Specify the search method.
            strWebRequest.Method = "PUT"

            'SEt the content type header.
            strWebRequest.Headers.Set("Translate", "f")
            strWebRequest.ContentType = "text/plain"

            'Encode the body using UTF-8.
            bytes = System.Text.Encoding.UTF8.GetBytes(strApptRequest)

            'Set the content header length. This must be done before writing data to the 
            'request stream.
            strWebRequest.ContentLength = bytes.Length

            'Get a reference to the request stream.
            RequestStream = strWebRequest.GetRequestStream()

            'Write the message body to the request stream.
            RequestStream.Write(bytes, 0, bytes.Length)

            'Close the Stream object to release the connection for further use.
            RequestStream.Close()



            'Send the SEARCH method request and get the response from the server.
            strWebResponse = strWebRequest.GetResponse()

            ResponseStream = strWebResponse.GetResponseStream
            ResponseStream.Close()
            strWebResponse.Close()

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function fn_UserAddedToBACRM(ByVal strMailID As String, ByVal strUserName As String, ByVal strExServer As String, ByVal siteType As String) As String
        '================================================================================
        ' Purpose: This function will check whether the "BizAutomation" Folder exists or not.
        ' History
        ' Ver   Date        Author              Reason
        ' 1     03/02/2004  Vishwanath          Created
        '
        ' Non Compliance (any deviation from standards)
        '   No deviations from the standards.
        '================================================================================
        Dim strWebRequest As System.Net.HttpWebRequest
        Dim strWebResponse As System.Net.HttpWebResponse
        Dim strRootURI As String
        Dim strQuery As String
        Dim bytes() As Byte
        Dim RequestStream As System.IO.Stream
        Dim ResponseStream As System.IO.Stream
        Dim xmlReader As System.Xml.XmlTextReader
        Dim bizFlag As String

        Try
            'Initialize Variables
            strRootURI = siteType & "//" & strExServer & "/exchange/" & strMailID & "/"
            strQuery = "<?xml version=""1.0""?>" & _
                       "<D:searchrequest xmlns:D = ""DAV:"" >" & _
                        "<D:sql>SELECT " & AddQuotes("DAV:displayname") & ", " & AddQuotes("DAV:href") & _
                        " FROM scope('hierarchical traversal of " & AddQuotes(strRootURI) & "')" & _
                        " WHERE " & AddQuotes("DAV:isfolder") & "=True</D:sql></D:searchrequest>"

            'Create the search HttpWebRequest object.
            strWebRequest = CType(System.Net.WebRequest.Create(strRootURI), System.Net.HttpWebRequest)

            'Add the network credential to the request.
            'strWebRequest.Credentials = System.Net.CredentialCache.DefaultCredentials
            Dim cred As System.Net.NetworkCredential = New System.Net.NetworkCredential("superuser", ConfigurationManager.AppSettings("superuser"), ConfigurationManager.AppSettings("ExchangeDomainName"))
            Dim credcache As System.Net.CredentialCache = New System.Net.CredentialCache
            credcache.Add(New Uri(strRootURI), "Basic", cred)

            strWebRequest.Credentials = credcache

            'Specify the search method.
            strWebRequest.Method = "SEARCH"


            'SEt the content type header.
            strWebRequest.ContentType = "text/xml"

            'Encode the body using UTF-8.
            bytes = System.Text.Encoding.UTF8.GetBytes(strQuery)

            'Set the content header length. This must be done before writing data to the 
            'request stream.
            strWebRequest.ContentLength = bytes.Length

            'Get a reference to the request stream.
            RequestStream = strWebRequest.GetRequestStream()

            'Write the message body to the request stream.
            RequestStream.Write(bytes, 0, bytes.Length)

            'Close the Stream object to release the connection for further use.
            RequestStream.Close()


            'Send the SEARCH method request and get the response from the server.
            'response = request.GetResponse
            strWebResponse = strWebRequest.GetResponse()


            'Get the XML Response stream.
            ResponseStream = strWebResponse.GetResponseStream()

            'Create the XMLTestReader object from the XML response stream.
            xmlReader = New System.Xml.XmlTextReader(ResponseStream)

            bizFlag = "False"
            'Read through the XML response, node by node.
            While (xmlReader.Read())
                'Look for the opening DAV:href node. The DAV: namespace is 
                'typically assigned the a: prefix in the XML response body.
                If xmlReader.Name = "a:displayname" Then
                    'Advance the reader to the text node.
                    xmlReader.Read()

                    If xmlReader.Value = "Calendar" Then
                        Exit While
                    End If

                    'Display the value of the DAV:href text node.
                    If xmlReader.Value = "BizAutomation" Then
                        bizFlag = "True"
                        Return bizFlag
                        Exit While
                    End If

                    'Advance the reader to the closing DAV:href node.
                    xmlReader.Read()
                End If
            End While
            Return bizFlag
            'Clean UP
            xmlReader.Close()
            ResponseStream.Close()
            strWebResponse.Close()
        Catch ex As Exception
            'Catch the exceptions. Any error codes from the SEARCH method requests
            'on the server will be caught here, also.
            'Throw ex
            Return False
        End Try
    End Function

    Public Function CheckUserInDB(ByVal strUserName As String) As Boolean
        'This is for Checking with the database
        CheckUserInDB = True
        Dim cnnUser As New SqlConnection(fn_GetConnectionString)
        Dim cmdUser As New SqlCommand("select count(*) from usermaster where vcUserName='" & strUserName & "' and numDomainID=" & g_lngDomainID, cnnUser)
        Dim intFlag As Integer
        cmdUser.Connection = cnnUser
        cnnUser.Open()
        intFlag = cmdUser.ExecuteScalar()
        cmdUser.Dispose()
        cnnUser.Close()
        cnnUser.Dispose()

        If intFlag <= 0 Then
            CheckUserInDB = False
        End If

    End Function

    Function AddQuotes(ByVal strValue As String) As String
        Const QUOTE = """"
        AddQuotes = QUOTE & Replace(strValue, QUOTE, QUOTE & QUOTE) & QUOTE
    End Function




    Public Function fn_GetConnectionString() As String
        fn_GetConnectionString = ConfigurationManager.AppSettings("ConnectionString")
    End Function

    Public Sub sb_SaveUsersInSQL(ByVal strUserID As String, ByVal strUserFullName As String, ByVal lngTerritoryID As Long, ByVal strGroups As String)
        On Error Resume Next
        If strUserID <> "Administrator" Then
            'Response.Write("<P>" & strUserID & "<BR>" & strUserFullName & "<BR>" & lngTerritoryID & "<BR>" & strGroups)
            Dim cnnAddUser As New SqlConnection(fn_GetConnectionString)
            Dim cmdAddUser As New SqlCommand("EXEC usp_AddNewUser @vcUserName='" & strUserID & "', @vcUserFullName='" & strUserFullName & "', @numDomainID=" & g_lngDomainID & ", @numCreatedBy=1 , @bintCreatedDate=" & Format(Now(), "yyyyMMddhhmmss") & ", @numTerritoryID=" & lngTerritoryID, cnnAddUser)
            Dim lngUserID As Long
            Dim aryUserGroups() As String
            Dim intLoopCounter As Integer

            'Add the new user and get its ID.
            cmdAddUser.Connection = cnnAddUser
            cnnAddUser.Open()
            lngUserID = cmdAddUser.ExecuteScalar()
            cmdAddUser.Dispose()

            'Loop thru the Group IDs and Fill the Group ID and User ID in the UserGroup table.
            aryUserGroups = strGroups.Split(",")
            For intLoopCounter = 0 To UBound(aryUserGroups)
                cmdAddUser.CommandText = "EXEC usp_UpdateUserGroups @numGroupID=" & aryUserGroups(intLoopCounter) & ", @numUserID=" & lngUserID
                cmdAddUser.ExecuteNonQuery()
            Next

            cmdAddUser.Dispose()
            cnnAddUser.Close()

            cmdAddUser = Nothing
            cnnAddUser = Nothing
        End If
    End Sub

    Public Sub sb_GetDomainID()

        Dim cnnDomain As New SqlConnection(fn_GetConnectionString)
        Dim cmdDomain As New SqlCommand

        cmdDomain.CommandText = "SELECT numDomainID FROM Domain WHERE UPPER(vcDomainName)=UPPER('" & Left(g_strDomainName, InStr(g_strDomainName, ".", CompareMethod.Binary) - 1) & "')"
        cmdDomain.Connection = cnnDomain
        cnnDomain.Open()
        g_lngDomainID = cmdDomain.ExecuteScalar

        cmdDomain.Dispose()
        cnnDomain.Close()

        cmdDomain = Nothing
        cnnDomain = Nothing

    End Sub

    Public Sub sb_FillTerritory(ByVal ddlTarget As DropDownList)
        Dim cnnTerritory As New SqlConnection(fn_GetConnectionString)
        Dim cmdTerritory As New SqlCommand
        Dim drdrTerritory As SqlDataReader

        cmdTerritory.CommandText = "SELECT numTerID, vcTerName FROM TerritoryMaster WHERE numDomainID=" & g_lngDomainID
        cmdTerritory.Connection = cnnTerritory
        cnnTerritory.Open()

        drdrTerritory = cmdTerritory.ExecuteReader

        While drdrTerritory.Read
            ddlTarget.Items.Add(New ListItem(drdrTerritory("vcTerName"), drdrTerritory("numTerID")))
        End While

        drdrTerritory.Close()
        cmdTerritory.Dispose()
        cnnTerritory.Close()

        drdrTerritory = Nothing
        cmdTerritory = Nothing
        cnnTerritory = Nothing

    End Sub

    Public Sub sb_UserGroup(ByVal tblTarget As Table)
        Dim cnnUserGroups As New SqlConnection(fn_GetConnectionString)
        Dim cmdUserGroups As New SqlCommand
        Dim drdrUserGroups As SqlDataReader
        Dim intColCounter As Int16
        Dim intRowCounter As Integer
        Dim tcellUserGroup As TableCell
        Dim trowUserGroup As TableRow

        cmdUserGroups.CommandText = "SELECT numGroupID, vcGroupName FROM AuthenticationGroupMaster WHERE numDomainID=" & g_lngDomainID
        cmdUserGroups.Connection = cnnUserGroups
        cnnUserGroups.Open()

        drdrUserGroups = cmdUserGroups.ExecuteReader
        intColCounter = 0
        intRowCounter = 0

        While drdrUserGroups.Read
            If intColCounter = 0 Then
                trowUserGroup = New TableRow
            End If

            If intRowCounter Mod 2 = 0 Then
                trowUserGroup.CssClass = "tr1"
            Else
                trowUserGroup.CssClass = "tr2"
            End If


            tcellUserGroup = New TableCell
            tcellUserGroup.Controls.Add(New LiteralControl("<input type='checkbox' name='chkUserGroups' id='chkUserGroups' value='" & drdrUserGroups("numGroupID") & "'>" & drdrUserGroups("vcGroupName")))
            tcellUserGroup.HorizontalAlign = HorizontalAlign.Left
            tcellUserGroup.Width = tcellUserGroup.Width.Percentage(25)
            trowUserGroup.Cells.Add(tcellUserGroup)
            intColCounter = intColCounter + 1

            If intColCounter = 4 Then
                tblTarget.Rows.Add(trowUserGroup)
                intColCounter = 0
            End If
        End While
        If intColCounter < 4 And intColCounter > 0 Then
            tblTarget.Rows.Add(trowUserGroup)
            intColCounter = 0
        End If

        drdrUserGroups.Close()
        cmdUserGroups.Dispose()
        cnnUserGroups.Close()

        drdrUserGroups = Nothing
        cmdUserGroups = Nothing
        cnnUserGroups = Nothing
    End Sub

End Module
