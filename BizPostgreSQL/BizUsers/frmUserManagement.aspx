<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmUserManagement.aspx.vb" Inherits="BizUsers.frmUserManagement" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>frmUserManagement</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="biz.css" type="text/css" rel="stylesheet">
		<script language="javascript">
		function GetStatus()
		{
		var str='';
	
		for(var i=0; i<dgUsers.rows.length-1; i++)
			{
				if (document.all['dgUsers__ctl' + (i+2)+ '_chkActivate'].checked==true)
				{
				str=str+'1,'
				}
				else
				{
				str=str+'0,'
				}
			}
			document.Form1.txtValue.value=str
		}
		function Cancel()
		{
			window.location.href("Default.aspx")
			return false;
		}
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table width="100%">
				<tr>
					<td align="right"><IMG src="images/biz.JPG" width="326" height="63">
					</td>
				</tr>
			</table>
			<table>
			<tr>
			<td class=normal1>
			Organization
			</td>
			<td colspan=8>
			<asp:DropDownList ID=ddlOrg Width=200 runat=server CssClass=normal1 AutoPostBack=true  ></asp:DropDownList>
			</td>
			</tr>
			</table> 
			<br />
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				
				<tr>
					<td vAlign="bottom">
						<table class="TabStyle" borderColor="black" cellSpacing="0" border="1">
							<tr>
								<td class="Text_bold_White" noWrap height="23">&nbsp;&nbsp;&nbsp;User 
									Activation/Deactivation&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td align="center" class="normal1">
						No of License :<asp:label ID="lblLicence" Runat="server"></asp:label>
					</td>
					<td align="right"><asp:button id="btnSave" Runat="server" Text="Save" CssClass="button"></asp:button>
						<asp:button id="btnSaveClose" Runat="server" Text="Save &amp; Close" CssClass="button"></asp:button>
						<asp:button id="btnCancel" Runat="server" Text="Back" Width="50" CssClass="button"></asp:button>
					</td>
				</tr>
				<tr>
					<td colSpan="3"><asp:table id="tblTeams" Runat="server" Height="350" BorderColor="black" GridLines="None" Width="100%"
							CellSpacing="0" CellPadding="0" BorderWidth="1">
							<asp:TableRow>
								<asp:TableCell VerticalAlign="Top">
									<asp:datagrid id="dgUsers" runat="server" CssClass="dg" Width="100%" AutoGenerateColumns="False">
										<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
										<ItemStyle CssClass="is"></ItemStyle>
										<HeaderStyle CssClass="hs"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn HeaderText="Sl No">
												<ItemTemplate>
													<%# Container.ItemIndex +1 %>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="vcUserName" HeaderText="User Name"></asp:BoundColumn>
											<asp:BoundColumn DataField="vcUserDesc" HeaderText="Display Name"></asp:BoundColumn>
											<asp:BoundColumn DataField="vcMailNickName" HeaderText="Mail Nick Name"></asp:BoundColumn>
											<asp:BoundColumn DataField="Email" HeaderText="Email"></asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Activation Status">
												<ItemTemplate>
													<asp:Label ID=lblActivate Runat=server style="display:none" Text='<%# DataBinder.Eval(Container.DataItem, "Active") %>'>
													</asp:Label>
													<asp:CheckBox ID="chkActivate" Runat="server"></asp:CheckBox>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid>
								</asp:TableCell>
							</asp:TableRow>
						</asp:table></td>
				</tr>
			</table>
			<table width="100%" align="center">
				<tr>
					<td class="normal1" align="center"><asp:literal id="litMessage" Runat="server"></asp:literal></td>
				</tr>
			</table>
			<asp:textbox id="txtValue" style="DISPLAY: none" Runat="server"></asp:textbox></form>
	</body>
</HTML>
