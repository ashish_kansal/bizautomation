Imports System.DirectoryServices
Partial Class frmUserList
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.dirSearcher = New System.DirectoryServices.DirectorySearcher
        '
        'dirSearcher
        '
        Me.dirSearcher.ClientTimeout = System.TimeSpan.Parse("-00:00:00.0000001")

        Me.dirSearcher.PageSize = 100
        Me.dirSearcher.ServerTimeLimit = System.TimeSpan.Parse("-00:00:00.0000001")
        Me.dirSearcher.SizeLimit = 1000
        Me.dirSearcher.PropertiesToLoad.Add("name")
        Me.dirSearcher.PropertiesToLoad.Add("samaccountname")
        Me.dirSearcher.PropertiesToLoad.Add("mailnickname")
        Me.dirSearcher.PropertiesToLoad.Add("description")
        Me.dirSearcher.PropertiesToLoad.Add("mail")
        Me.dirSearcher.SearchScope = SearchScope.Subtree

    End Sub

    Dim strExServer As String
    Dim strUserName As String
    Dim strSuccessUsers As String, strUnsuccessUsers As String
    Protected WithEvents dirSearcher As System.DirectoryServices.DirectorySearcher
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Dim intUsers As Integer
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            strExServer = ConfigurationManager.AppSettings("DomainServerName")
            strUserName = Request.ServerVariables("AUTH_USER")
            If IsPostBack = False Then
                Dim objBusinessClass As New BusinessClass
                objBusinessClass.byteMode = 2
                ddlOrg.DataSource = objBusinessClass.ManageDomain(ConfigurationManager.AppSettings("ConnectionString"))
                ddlOrg.DataTextField = "vcDomainName"
                ddlOrg.DataValueField = "numDomainId"
                ddlOrg.DataBind()
                lblLicence.Text = Session("NoOfLicence")
            End If
            btnAdd.Attributes.Add("onclick", "return GetUsers()")
            btnCancel.Attributes.Add("onclick", "return Cancel()")

        Catch ex As Exception

            Response.Write(ex)
        End Try
    End Sub



    Private Sub sb_GetUserList()

        Dim dtUsersinBiz As New DataTable
        Dim objBusiness As New BusinessClass
        objBusiness.ActivateFlag = 1
        dtUsersinBiz = objBusiness.GetUsers(ConfigurationManager.AppSettings("ConnectionString"))

        Dim dtUsers As New DataTable
        Dim dr As DataRow
        dtUsers.Columns.Add("DisplayName")
        dtUsers.Columns.Add("UserName")
        dtUsers.Columns.Add("Mailname")
        dtUsers.Columns.Add("MBXName")
        dtUsers.Columns.Add("Description")
        dtUsers.Columns.Add("mail")


        On Error Resume Next
        Dim strFullName As String

        Dim colAdsSearchResult As System.DirectoryServices.SearchResultCollection
        Dim rsltADS As System.DirectoryServices.SearchResult
        Me.dirSearcher.Filter = "(&(&(|(&(objectCategory=person)(objectSid=*)(mailnickname=*" & IIf(txtMailNickName.Text = "", "", txtMailNickName.Text & "*") & ")(mail=*" & IIf(txtEmail.Text = "", "", txtEmail.Text & "*") & ")" & IIf(txtFirstName.Text = "", "", "(name=*" & txtFirstName.Text & "*)") & "(!samAccountType:1.2.840.113556.1.4.8" & _
                       "04:=3))(&(objectCategory=person)(!objectSid=*))(&(objectCategory=group)(groupTyp" & _
                       "e:1.2.840.113556.1.4.804:=14)))(& (mailnickname=*) (| (&(objectCategory=person)(" & _
                       "objectClass=user)(|(homeMDB=*)(msExchHomeServerName=*))) ))))"
        colAdsSearchResult = dirSearcher.FindAll
        For Each rsltADS In colAdsSearchResult

            strFullName = rsltADS.Properties("name")(0).ToString()
            If InStr(1, strFullName, "aspnet", CompareMethod.Text) = 0 _
                And InStr(1, strFullName, "SystemMailbox", CompareMethod.Text) = 0 _
                And InStr(1, strFullName, "Internet Guest Account", CompareMethod.Text) = 0 _
                And InStr(1, strFullName, "Launch IIS Process Account", CompareMethod.Text) = 0 _
                And InStr(1, strFullName, "TsInternetUser", CompareMethod.Text) = 0 _
                And InStr(1, strFullName, "Application Center Test Account", CompareMethod.Text) = 0 _
                And InStr(1, strFullName, "SQLDebugger", CompareMethod.Text) = 0 _
                And InStr(1, strFullName, "ACTUser", CompareMethod.Text) = 0 _
                And InStr(1, strFullName, "Guest", CompareMethod.Text) = 0 _
                And InStr(1, strFullName, "IUSR_", CompareMethod.Text) = 0 _
                And InStr(1, strFullName, "IWAM_", CompareMethod.Text) = 0 _
                And InStr(1, strFullName, "MPFClient", CompareMethod.Text) = 0 _
                And InStr(1, strFullName, "MPFService", CompareMethod.Text) = 0 _
                And InStr(1, strFullName, "MPSPriv", CompareMethod.Text) = 0 _
                And InStr(1, strFullName, "SQLDebugger", CompareMethod.Text) = 0 _
                And InStr(1, strFullName, "VUSR_", CompareMethod.Text) = 0 Then


                Dim i, k As Integer
                If fn_CheckUserMailBox(rsltADS.Properties("proxyaddresses"), "") = "False" Then
                    For i = 0 To dtUsersinBiz.Rows.Count - 1
                        If rsltADS.Properties("mailnickname")(0).ToString() = dtUsersinBiz.Rows(i).Item("vcMailNickName") Then
                            k = 1
                        End If
                    Next
                    If k <> 1 Then
                        dr = dtUsers.NewRow
                        dr("DisplayName") = rsltADS.Properties("name")(0).ToString()
                        dr("UserName") = rsltADS.Properties("samaccountname")(0).ToString()
                        dr("Mailname") = rsltADS.Properties("mailnickname")(0).ToString()
                        dr("MBXName") = "MBX/" & rsltADS.Properties("name")(0).ToString()
                        If Not rsltADS.Properties("mail")(0) Is Nothing Then
                            dr("mail") = rsltADS.Properties("mail")(0)
                        Else
                            dr("mail") = ""
                        End If
                        dtUsers.Rows.Add(dr)
                    End If
                    If k = 1 Then
                        k = 0
                    End If
                End If
            End If

        Next
        Session("NewUsers") = dtUsers
        dgUserAssociation.DataSource = dtUsers
        dgUserAssociation.DataBind()
    End Sub


    Private Function fn_CheckUserMailBox(ByVal objProxyIds As Object, ByRef strUserFolderID As String) As String
        On Error Resume Next
        Dim intCounter As Integer

        fn_CheckUserMailBox = "True"
        For intCounter = 0 To objProxyIds.Count
            If Left(objProxyIds(intCounter).ToString(), 5).ToUpper = "SMTP:" And InStr(objProxyIds(intCounter).ToString, g_strDomainName, CompareMethod.Text) > 0 Then
                strUserFolderID = Mid(objProxyIds(intCounter).ToString, 6, InStr(objProxyIds(intCounter).ToString, "@", CompareMethod.Text) - 6)

                ' Trace.Warn("Checkin DB", CheckUserInDB(strUserFolderID).ToString())
                If fn_UserAddedToBACRM(strUserFolderID, strUserFolderID, strExServer, Session("siteType")) = "True" And CheckUserInDB(strUserFolderID) = True Then

                    'Trace.Warn("Testing true", "Folder Pass for " & strUserFolderID)

                    fn_CheckUserMailBox = "True"
                    Exit For
                Else
                    fn_CheckUserMailBox = "False"
                    'Trace.Warn("Testing False", "Folder Pass for " & strUserFolderID)
                    Exit For
                End If
            End If
        Next
    End Function

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            Dim strMessage As String
            Dim dtUsersNew As New DataTable
            Dim i As Integer
            dtUsersNew = Session("NewUsers")
            Dim str As String()
            str = txtValue.Text.TrimEnd(",").Split(",")
            If str.Length <> dtUsersNew.Rows.Count Then
                Exit Sub
            End If
            For i = 0 To dtUsersNew.Rows.Count - 1
                If str(i) = 1 Then
                    intUsers = CreateUsers(dtUsersNew.Rows(i).Item("Mailname"), dtUsersNew.Rows(i).Item("mail"), dtUsersNew.Rows(i).Item("DisplayName"), dtUsersNew.Rows(i).Item("UserName"))
                    If intUsers = -1 Then
                        strMessage = "You have added maximum no of users that is allowed as per the licence."
                        Exit For
                    End If

                End If
            Next
            If strSuccessUsers <> "" Then
                litMessage.Text = "<P>Successfully Created User Details in BACRM for the following users:" & strSuccessUsers
            End If

            If strUnsuccessUsers <> "" Then
                litMessage.Text = litMessage.Text & "<P>Creation of User Details in BACRM for the following users failed:" & strUnsuccessUsers
            End If
            litMessage.Text = litMessage.Text & "<br>" & strMessage
            sb_GetUserList()

        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Function CreateUsers(ByVal MailName As String, ByVal MailID As String, ByVal displayName As String, ByVal UserName As String) As Integer
        Try

            Dim objBusiness As New BusinessClass

            objBusiness.UserName = UserName
            objBusiness.MailName = MailName
            objBusiness.DisplayName = displayName
            objBusiness.EmailID = MailID
            objBusiness.NoofLicence = Session("NoOfLicence")
            objBusiness.DomainName = ddlOrg.SelectedItem.Text
            intUsers = objBusiness.CreateNewUsers(ConfigurationManager.AppSettings("ConnectionString"))
            If intUsers <> -1 Then
                Dim strFileNPath As String = Server.MapPath(BA_FILE_NAME)
                Dim strLocalPathOfParentFolder As String = "MBX/" & MailID
                If fn_UserAddedToBACRM(MailID, UserName, strExServer, Session("siteType")) = "True" Then
                    strSuccessUsers = strSuccessUsers & "<BR>" & UserName
                Else
                    If fn_CreateFolder(strLocalPathOfParentFolder, BA_NEW_FOLDERNAME, BA_NEW_SUB_FOLDERNAME, strFileNPath, BA_FILE_NAME, strExServer, MailID, ConfigurationManager.AppSettings("Path"), Session("siteType"), MailName) = False Then
                        strUnsuccessUsers = strUnsuccessUsers & "<BR>" & UserName

                    Else
                        strSuccessUsers = strSuccessUsers & "<BR>" & UserName
                    End If
                End If
            End If
            Return intUsers

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            sb_GetUserList()
        Catch ex As Exception
            Response.Write(ex)
        End Try

    End Sub



End Class
