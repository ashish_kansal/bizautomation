Partial Class frmUserManagement
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnClose As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not Session("Message") Is Nothing Then
                litMessage.Text = Session("Message")
                Session("Message") = Nothing
            End If
            If Not IsPostBack Then
                Dim objBusinessClass As New BusinessClass
                objBusinessClass.byteMode = 2
                ddlOrg.DataSource = objBusinessClass.ManageDomain(ConfigurationManager.AppSettings("ConnectionString"))
                ddlOrg.DataTextField = "vcDomainName"
                ddlOrg.DataValueField = "numDomainId"
                ddlOrg.DataBind()
                GetUsers()
                lblLicence.Text = Session("NoOfLicence")
            End If
            btnSave.Attributes.Add("onclick", "return GetStatus()")
            btnSaveClose.Attributes.Add("onclick", "return GetStatus()")
            btnCancel.Attributes.Add("onclick", "return Cancel()")
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Sub GetUsers()
        Try


            Dim objBusiness As New BusinessClass
            Dim dtUsers As New DataTable
            objBusiness.DomianID = ddlOrg.SelectedValue
            dtUsers = objBusiness.GetUserList(ConfigurationManager.AppSettings("ConnectionString"))
            Session("Users") = dtUsers
            dgUsers.DataSource = dtUsers
            dgUsers.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub Save()
        Try


            Dim dtUsers As New DataTable
            Dim dsNewUser As New DataTable
            dsNewUser.Columns.Add("UserID")
            dsNewUser.Columns.Add("Active")
            Dim i As Integer
            dtUsers = Session("Users")

            Dim strExServer As String = ConfigurationManager.AppSettings("DomainServerName")
            Dim strFileNPath As String = Server.MapPath(BA_FILE_NAME)
            Dim strUserName As String = Request.ServerVariables("AUTH_USER")

            Dim str As String()
            str = txtValue.Text.TrimEnd(",").Split(",")
            Dim objBusiness As New BusinessClass
            Dim intUsers As Integer
            For i = 0 To dtUsers.Rows.Count - 1
                Dim strLocalPathOfParentFolder As String = "MBX/" & dtUsers.Rows(i).Item("vcEmailID")
                objBusiness.ActivateFlag = str(i)
                objBusiness.UserID = dtUsers.Rows(i).Item("numUserId")
                objBusiness.NoofLicence = Session("NoOfLicence")
                intUsers = objBusiness.SaveUserStatus(ConfigurationManager.AppSettings("ConnectionString"))

                If intUsers > Session("NoOfLicence") Then
                    Session("Message") = "You have added maximum no of users as per the licence."
                    Response.Redirect("frmUserManagement.aspx")
                End If
                'dr = dsNewUser.NewRow
                'dr("UserID") = dtUsers.Rows(i).Item("numUserId")
                'dr("Active") = str(i)
                'dsNewUser.Rows.Add(dr)
            Next
            'Dim strUser As String
            'Dim ds As New DataSet
            'dsNewUser.TableName = "Table"
            'ds.Tables.Add(dsNewUser)
            'strUser = ds.GetXml()
            'ds.Tables.Remove("Table")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgUsers_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgUsers.ItemDataBound
        Try


            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim chk As CheckBox
                Dim lbl As Label
                chk = e.Item.FindControl("chkActivate")
                lbl = e.Item.FindControl("lblActivate")
                If lbl.Text = "True" Then
                    chk.Checked = True
                Else
                    chk.Checked = False
                End If

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Save()
            GetUsers()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            Save()
            Response.Redirect("Default.aspx")

        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlOrg_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOrg.SelectedIndexChanged
        GetUsers()
    End Sub
End Class