<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Default.aspx.vb" Inherits="BizUsers.DefaultPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML>
	<HEAD>
		<title>BizAutomation CRM [User Administration]</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="biz.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="frmDefault" method="post" runat="server">
			<table width="100%">
				<tr>
					<td align="right">
						<IMG src="images/biz.JPG" width="326" height="63">
					</td>
				</tr>
			</table>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<table align="center">
				<tr>
					<td>
						<h4>Welcome to BizAutomation CRM User Administration Section</h4>
					</td>
				</tr>
				<tr>
					<td><h5>Select Your Choice by clicking on the links below:</h5>
					</td>
				</tr>
				<tr>
					<td class="normal1"><a href="frmUserList.aspx"><u>User Association</u></a>(Associate 
						Users to BACRM to enable them to use BACRM)
					</td>
				</tr>
				<tr>
					<td class="normal1"><a href="frmUserManagement.aspx"><u>User Activation</u></a> (Activate/Deactivate 
						Users associated with BACRM. This is related to Licence/s purchased.)
					</td>
				</tr>
				<tr>
					<td class="normal1"><a href="frmOrganization.aspx"><u>Configure Organization</u></a>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>