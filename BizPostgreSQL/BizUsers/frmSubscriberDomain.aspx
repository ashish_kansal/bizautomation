﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSubscriberDomain.aspx.vb" Inherits="BizUsers.frmSubscriberDomain" %>

<%@ Register src="../BACRMUI/include/calandar.ascx" tagname="calendar" tagprefix="bizcalendar" %>
<%@ Register assembly="Infragistics2.WebUI.Misc.v9.1, Version=9.1.20091.1015, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.WebUI.Misc" tagprefix="igmisc" %>


<%@ Register assembly="Infragistics2.WebUI.UltraWebNavigator.v9.1, Version=9.1.20091.1015, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb" namespace="Infragistics.WebUI.UltraWebNavigator" tagprefix="ignav" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
    <style type="text/css">

.TabStyle
{
	background-image: url('../BACRMUI/images/ig_tab_winXPs3.gif');
	padding-left: 10px;
	color: white;
	font-family:Arial;
	font-size: 11px;
	font-weight: bold;
	height: 23px;
}

INPUT.button
{
	    border: 1px inset black;
            color: black;
	        font-weight: bold;
	background: url('../BACRMUI/images/LightBlue.jpg') repeat-x;
	        font-size: 8pt;
	        cursor: hand;
	        text-align: center;
}
.aspTable
{
	background: url('../BACRMUI/images/v.gif') repeat-x;
        }
.normal1
{
	font-family:Arial;
	font-size: 8pt;
	font-style: normal;
	font-variant: normal;
	font-weight: normal;
	color: #000000;
}
select.signup
{
	font-family:Arial;
	font-size: 8pt;
	color: Black;
	height: 15pt;
}


.signup
{
	font-family:Arial;
	font-size: 8pt;
	color: Black;
}
.normal4
{
	font-family:Arial;
	font-size: 8pt;
	font-style: normal;
	font-variant: normal;
	font-weight: normal;
	color: red;
}
    </style>
</head>
<body>
    <form id="form2" runat="server" method="post">
    <br />
    <table id="Table1" height="2" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td valign="bottom">
                <table class="TabStyle">
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;Multi Company &nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td align="right">
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save &amp; Close">
                </asp:Button>
                <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" Width="50">
                </asp:Button>
            </td>
        </tr>
    </table>
    <table id="Table2" height="2" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td>
                <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
                    Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None">
                    <asp:TableRow>
                        <asp:TableCell>
                            <br>
                            <table cellspacing="2" cellpadding="0" width="650" border="0">
                                <tr>
                                    <td class="normal1" align="right">
                                        New Domain&nbsp;
                                    </td>
                                    <td>
                                        <asp:DropDownList CssClass="signup" ID="ddlDomain" runat="server" Width="200" AutoPostBack="True">
                                            <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        
                                    </td>
                                    <td>
                                        
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="normal1" align="right">
                                        Parent Domain
                                    </td>
                                    <td colspan="3">
                                        <asp:DropDownList CssClass="signup" ID="ddlParentDomain" runat="server" Width="200">
                                            <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>  
                                
                                <tr>
                                
                                <td align="center" class="aspTable" rowspan="3" >
                                <ignav:UltraWebTree ID="ultraTreeDomain" runat="server">
                                </ignav:UltraWebTree>            
                                </td>
                                
                                </tr>
                                                              
                            </table>
                            <br />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </td>
        </tr>
        <tr align="center">
            <td class="normal4" valign="middle">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
        </table>
        <asp:HiddenField ID="HdnumListItemId" runat="server" />
    </form>
    </body>
</html>
