﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports System.Configuration
Imports Elasticsearch.Net
Imports Nest
Imports BACRM.BusinessLogic.Item
Imports System
Imports System.Dynamic
Imports Newtonsoft.Json
Imports System.Reflection
Imports System.Linq

Public Class ElasticSearchBizCartUpdateIndexedData
    Public Sub UpdateElasticSearchData()
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Dim ds As New DataSet
            Dim dataAdatpter As Npgsql.NpgsqlDataAdapter
            Dim i As Int32 = 0

            WriteMessage("ElasticSearchDataSync: Get Modified Records Start")
            Using connection As New Npgsql.NpgsqlConnection(connString)
                connection.Open()

                Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                    Dim sqlCommand As New Npgsql.NpgsqlCommand
                    sqlCommand.Connection = connection
                    sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})
                    sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur2", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})
                    sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur3", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})
                    sqlCommand.CommandTimeout = 300
                    sqlCommand.CommandText = "USP_ElasticSearchBizCart_Get"
                    sqlCommand.CommandType = CommandType.StoredProcedure
                    sqlCommand.ExecuteNonQuery()

                    For Each parm As Npgsql.NpgsqlParameter In sqlCommand.Parameters
                        If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                            If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                dataAdatpter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                ds.Tables.Add(parm.Value.ToString())
                                dataAdatpter.Fill(ds.Tables(i))
                                i += 1
                            End If
                        End If
                    Next

                    objTransaction.Commit()
                End Using

                connection.Close()
            End Using
            WriteMessage("ElasticSearchDataSync: Get Modified Records End")

            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                If ds.Tables.Count > 0 Then  'Inserted Records
                    Dim uniqueCols As DataTable = ds.Tables(0).DefaultView.ToTable(True, "numDomainID")

                    For Each drDomain As DataRow In uniqueCols.Rows
                        Dim itemRecords As DataRow() = ds.Tables(0).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='Item'").ToArray()
                        ElasticSearchManageItems(connString, drDomain("numDomainID"), itemRecords, "Insert")

                        Dim CustomFieldRecords As DataRow() = ds.Tables(0).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='CustomField'").ToArray()
                        ElasticSearchManageCustomFields(connString, drDomain("numDomainID"), CustomFieldRecords, "Insert")
                    Next
                End If

                If ds.Tables.Count > 1 Then  'Updated Records
                    Dim uniqueCols As DataTable = ds.Tables(1).DefaultView.ToTable(True, "numDomainID")

                    For Each drDomain As DataRow In uniqueCols.Rows
                        Dim itemRecords As DataRow() = ds.Tables(1).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='Item'").ToArray()
                        ElasticSearchManageItems(connString, drDomain("numDomainID"), itemRecords, "Update")

                        Dim CustomFieldRecords As DataRow() = ds.Tables(1).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='CustomField'").ToArray()
                        ElasticSearchManageCustomFields(connString, drDomain("numDomainID"), CustomFieldRecords, "Update")
                    Next
                End If

                If ds.Tables.Count > 2 Then  'Deleted Records
                    Dim uniqueCols As DataTable = ds.Tables(2).DefaultView.ToTable(True, "numDomainID")

                    For Each drDomain As DataRow In uniqueCols.Rows
                        Dim itemRecords As DataRow() = ds.Tables(2).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='Item'").ToArray()
                        ElasticSearchManageItems(connString, drDomain("numDomainID"), itemRecords, "Delete")

                        Dim CustomFieldRecords As DataRow() = ds.Tables(2).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='CustomField'").ToArray()
                        ElasticSearchManageCustomFields(connString, drDomain("numDomainID"), CustomFieldRecords, "Delete")
                    Next
                End If
            End If
        Catch ex As Exception
            BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, 0, 0, 0, Nothing)
            WriteMessage("UpdateElasticSearchData Exception: " & ex.Message)
            'DO NOT THROW EXCEPTION
        End Try
    End Sub

    Private Sub ElasticSearchManageItems(ByVal connString As String, ByVal DomainID As Long, ByVal itemRecords As DataRow(), ByVal vcType As String)
        Try
            If Not itemRecords Is Nothing AndAlso itemRecords.Length > 0 Then
                Dim _index As String = "biz_cart_" & DomainID & "_item"
                If vcType = "Delete" Then
                    ElasticSearchDeleteRecords(_index, itemRecords)
                Else
                    Dim itemIndex As Int32
                    For itemIndex = 0 To itemRecords.Count() - 1
                        Dim dsItem As New DataSet
                        Using connection As New Npgsql.NpgsqlConnection(connString)
                            Dim sqlCommand As New Npgsql.NpgsqlCommand
                            sqlCommand.Connection = connection
                            sqlCommand.CommandTimeout = 300
                            sqlCommand.CommandType = CommandType.StoredProcedure
                            sqlCommand.CommandText = "usp_elasticsearchbizcart_getitems"
                            sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = DomainID})
                            sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_vcitemcodes", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Text, .Value = itemRecords(itemIndex)("vcValue")})
                            sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})
                            sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur2", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})
                            sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur3", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                            Dim j As Int32 = 0
                            Dim da As Npgsql.NpgsqlDataAdapter
                            connection.Open()
                            Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                                sqlCommand.ExecuteNonQuery()

                                For Each parm As Npgsql.NpgsqlParameter In sqlCommand.Parameters
                                    If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                        If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                            Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                            da = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                            dsItem.Tables.Add(parm.Value.ToString())
                                            da.Fill(dsItem.Tables(j))
                                            j += 1
                                        End If
                                    End If
                                Next

                                objTransaction.Commit()
                            End Using
                            connection.Close()
                        End Using

                        Dim lstWarehouseItems As List(Of ESItemWarehouseDetails) = New List(Of ESItemWarehouseDetails)
                        lstWarehouseItems = ConvertDataTable(Of ESItemWarehouseDetails)(dsItem.Tables(1))

                        Dim lstItemCategories As List(Of ESItemCategoryDetails) = New List(Of ESItemCategoryDetails)
                        lstItemCategories = ConvertDataTable(Of ESItemCategoryDetails)(dsItem.Tables(2))

                        AppendIdColumn(dsItem.Tables(0), "numItemCode", True)
                        AppendOtherListColumns(dsItem.Tables(0), lstWarehouseItems, lstItemCategories)

                        If vcType = "Insert" Then
                            ElasticSearchInsertRecords(_index, dsItem.Tables(0))
                        ElseIf vcType = "Update" Then
                            ElasticSearchUpdateRecords(_index, dsItem.Tables(0))
                        End If
                    Next
                End If

                Dim dsItem_fields As New DataSet
                Using connection As New Npgsql.NpgsqlConnection(connString)
                    Dim sqlAdapter As Npgsql.NpgsqlDataAdapter

                    Dim sqlCommand As New Npgsql.NpgsqlCommand
                    sqlCommand.Connection = connection
                    sqlCommand.CommandTimeout = 300
                    sqlCommand.CommandType = CommandType.StoredProcedure
                    sqlCommand.CommandText = "usp_elasticsearchbizcart_getitemcustomfield"
                    sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = DomainID})
                    sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                    Dim i As Int32 = 0
                    connection.Open()
                    Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                        sqlCommand.ExecuteNonQuery()

                        For Each parm As Npgsql.NpgsqlParameter In sqlCommand.Parameters
                            If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                    Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                    sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                    dsItem_fields.Tables.Add(parm.Value.ToString())
                                    sqlAdapter.Fill(dsItem_fields.Tables(i))
                                    i += 1
                                End If
                            End If
                        Next

                        objTransaction.Commit()
                    End Using
                    connection.Close()
                End Using

                AppendIdColumn(dsItem_fields.Tables(0), "numFormFieldId")
                ElasticSearchInsertRecords(_index + "_fields", dsItem_fields.Tables(0))
            End If
        Catch ex As Exception
            BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, DomainID, 0, 0, Nothing)
            WriteMessage("ElasticSearchManageItems Exception: " & ex.Message)
        End Try
    End Sub

    Private Sub ElasticSearchManageCustomFields(ByVal connString As String, ByVal DomainID As Long, ByVal itemRecords As DataRow(), ByVal vcType As String)
        Try
            If Not itemRecords Is Nothing AndAlso itemRecords.Length > 0 Then
                Dim _index As String = "biz_cart_" & DomainID & "_item"
                If vcType = "Delete" Then
                    ElasticSearchDeleteCustomFieldRecords(_index, itemRecords)
                ElseIf vcType = "Insert" Then
                    ElasticSearchInsertCustomFieldRecords(_index, itemRecords)
                ElseIf vcType = "Update" Then
                    ElasticSearchUpdateCustomFieldRecords(_index, itemRecords)
                End If

                Dim dsItem_fields As New DataSet
                Using connection As New Npgsql.NpgsqlConnection(connString)
                    Dim sqlAdapter As Npgsql.NpgsqlDataAdapter

                    Dim sqlCommand As New Npgsql.NpgsqlCommand
                    sqlCommand.Connection = connection
                    sqlCommand.CommandTimeout = 300
                    sqlCommand.CommandType = CommandType.StoredProcedure
                    sqlCommand.CommandText = "usp_elasticsearchbizcart_getitemcustomfield"
                    sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = DomainID})
                    sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                    Dim i As Int32 = 0
                    connection.Open()
                    Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                        sqlCommand.ExecuteNonQuery()

                        For Each parm As Npgsql.NpgsqlParameter In sqlCommand.Parameters
                            If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                    Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                    sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                    dsItem_fields.Tables.Add(parm.Value.ToString())
                                    sqlAdapter.Fill(dsItem_fields.Tables(i))
                                    i += 1
                                End If
                            End If
                        Next

                        objTransaction.Commit()
                    End Using
                    connection.Close()
                End Using

                AppendIdColumn(dsItem_fields.Tables(0), "numFormFieldId")
                ElasticSearchInsertRecords(_index + "_fields", dsItem_fields.Tables(0))
            End If
        Catch ex As Exception
            BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, DomainID, 0, 0, Nothing)
            WriteMessage("ElasticSearchManageItems Exception: " & ex.Message)
            'DO NOT THROW EXCEPTION
        End Try
    End Sub


    Private Sub ElasticSearchInsertRecords(ByVal indexName As String, ByVal dt As DataTable)
        Try
            Dim settings As New Nest.ConnectionSettings(New Uri(ConfigurationManager.AppSettings("ElasticSearchURL")))
            settings.DisableDirectStreaming(True)
            Dim client As New ElasticClient(settings)

            If indexName.EndsWith("_fields") Then
                client.DeleteIndex(indexName)
            End If

            Dim isValid As Boolean = False
            Dim result As IExistsResponse = client.IndexExists(New IndexExistsRequest(Indices.Parse(indexName)))

            If Not result.Exists Then
                Dim createIndexResponse As Nest.ICreateIndexResponse =
                    client.CreateIndex(indexName, Function(c)
                                                      Return c.Settings(Function(s) s.NumberOfShards(1).NumberOfReplicas(0) _
                                                            .Analysis(Function(a) a.Analyzers(Function(anl) anl.Custom("default", Function(ca) ca.Tokenizer("whitespace").Filters(New List(Of String)() From {"lowercase"}))))) _
                                                            .Mappings(Function(ms)
                                                                          Return ms.Map(Of Object)(Function(m) m.DynamicTemplates(Function(x)
                                                                                                                                      Return x.DynamicTemplate("test", Function(t) t.Match("Search_*").Mapping(Function(mp) mp.Text(Function(tx) tx.Index(True))))
                                                                                                                                  End Function) _
                                                                                    .Properties(Function(p) p.Text(Function(s) s.Name("id").Index(False)).Nested(Of Object)(Function(s) s.Name("lstJsonWarehouseDetails").Enabled(True)).Nested(Of Object)(Function(s) s.Name("lstJsonItemCategories").Enabled(True))))
                                                                      End Function)
                                                  End Function)

                isValid = createIndexResponse.IsValid
            Else
                isValid = True
            End If

            If isValid Then
                Dim descriptor As New BulkDescriptor()
                For Each drItem As DataRow In dt.Rows
                    descriptor.Index(Of Object)(Function(op) op.Index(String.Concat(indexName)).Document(DataRowToJson(drItem)).Id(drItem("id").ToString()))
                Next
                Dim res = client.Bulk(descriptor)
            End If
        Catch ex As Exception
            BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, indexName.Split("_")(0), 0, 0, Nothing)
            WriteMessage("ElasticSearchInsertRecords Exception: " & ex.Message)
            'DO NOT THROW EXCEPTION
        End Try
    End Sub

    Private Sub ElasticSearchUpdateRecords(ByVal indexName As String, ByVal dt As DataTable)
        Try
            Dim settings As New Nest.ConnectionSettings(New Uri(ConfigurationManager.AppSettings("ElasticSearchURL")))
            Dim client As New ElasticClient(settings)

            Dim result As IExistsResponse = client.IndexExists(New IndexExistsRequest(Indices.Parse(indexName)))

            If result.Exists Then
                For Each drItem As DataRow In dt.Rows
                    Dim response As IDeleteResponse = client.Delete(Of Object)(drItem("id").ToString(), Function(d) d.Index(indexName))
                    If response.IsValid Then
                        Dim descriptor As New BulkDescriptor()
                        descriptor.Index(Of Object)(Function(op) op.Index(String.Concat(indexName)).Document(DataRowToJson(drItem)).Id(drItem("id").ToString()))
                        Dim res = client.Bulk(descriptor)
                    End If
                    'Dim updateResponse As IUpdateResponse(Of Object) = client.Update(Of Object)(DocumentPath(Of Object).Id(drItem("id").ToString()), Function(u) u.Index(indexName).DocAsUpsert(True).Doc(DataRowToJson(drItem)))
                Next
            End If
        Catch ex As Exception
            BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, indexName.Split("_")(0), 0, 0, Nothing)
            WriteMessage("ElasticSearchUpdateRecords Exception: " & ex.Message)
            'DO NOT THROW EXCEPTION
        End Try
    End Sub

    Private Sub ElasticSearchDeleteRecords(ByVal indexName As String, ByVal arrRows As DataRow())
        Try
            Dim settings As New Nest.ConnectionSettings(New Uri(ConfigurationManager.AppSettings("ElasticSearchURL")))
            Dim client As New ElasticClient(settings)

            Dim result As IExistsResponse = client.IndexExists(New IndexExistsRequest(Indices.Parse(indexName)))

            If result.Exists Then
                For Each drItem As DataRow In arrRows
                    Dim response As IDeleteResponse = client.Delete(Of Object)(drItem("vcValue").ToString(), Function(d) d.Index(indexName))
                Next
            End If
        Catch ex As Exception
            BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, indexName.Split("_")(0), 0, 0, Nothing)
            WriteMessage("ElasticSearchDeleteRecords Exception: " & ex.Message)
            'DO NOT THROW EXCEPTION
        End Try
    End Sub

    Private Sub ElasticSearchInsertCustomFieldRecords(ByVal indexName As String, ByVal arrRows As DataRow())
        Try
            Dim settings As New Nest.ConnectionSettings(New Uri(ConfigurationManager.AppSettings("ElasticSearchURL")))
            Dim client As New ElasticClient(settings)

            Dim result As IExistsResponse = client.IndexExists(New IndexExistsRequest(Indices.Parse(indexName)))

            If result.Exists Then
                For Each drItem As DataRow In arrRows
                    client.Map(Of Object)(Function(d) d.Index(indexName).Properties(Function(p) p.Text(Function(t) t.Name(drItem("vcValue").ToString()))))
                Next
                client.Reindex(Of Object)(indexName, indexName)
            End If
        Catch ex As Exception
            BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, indexName.Split("_")(0), 0, 0, Nothing)
            WriteMessage("ElasticSearchDeleteRecords Exception: " & ex.Message)
            'DO NOT THROW EXCEPTION
        End Try
    End Sub

    Private Sub ElasticSearchUpdateCustomFieldRecords(ByVal indexName As String, ByVal arrRows As DataRow())
        Try
            Dim settings As New Nest.ConnectionSettings(New Uri(ConfigurationManager.AppSettings("ElasticSearchURL")))
            Dim client As New ElasticClient(settings)

            Dim result As IExistsResponse = client.IndexExists(New IndexExistsRequest(Indices.Parse(indexName)))

            If result.Exists Then
                For Each drItem As DataRow In arrRows
                    Dim strValue As String = drItem("vcValue").ToString()
                    Dim oldField As String = strValue.Split("||")(0)
                    Dim newField As String = strValue.Split("||")(2)
                    Dim response As IPutPipelineResponse = client.PutPipeline("", Function(d) d.Description("").Processors(Function(p) p.Rename(Of Object)(Function(r) r.Field(newField).TargetField(oldField))))

                    If response.IsValid Then
                        client.Reindex(Of Object)(indexName, indexName)
                    End If
                Next
            End If
        Catch ex As Exception
            BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, indexName.Split("_")(0), 0, 0, Nothing)
            WriteMessage("ElasticSearchDeleteRecords Exception: " & ex.Message)
            'DO NOT THROW EXCEPTION
        End Try
    End Sub

    Private Sub ElasticSearchDeleteCustomFieldRecords(ByVal indexName As String, ByVal arrRows As DataRow())
        Try
            Dim settings As New Nest.ConnectionSettings(New Uri(ConfigurationManager.AppSettings("ElasticSearchURL")))
            Dim client As New ElasticClient(settings)

            Dim result As IExistsResponse = client.IndexExists(New IndexExistsRequest(Indices.Parse(indexName)))

            If result.Exists Then
                For Each drItem As DataRow In arrRows
                    Dim strValue As String = drItem("vcValue").ToString()
                    client.Update(Of Object)(indexName, Function(d) d.Index(indexName).Script(Function(s) s.Inline("ctx._source.remove('" + strValue + "')")))

                Next
            End If
        Catch ex As Exception
            BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, indexName.Split("_")(0), 0, 0, Nothing)
            WriteMessage("ElasticSearchDeleteRecords Exception: " & ex.Message)
            'DO NOT THROW EXCEPTION
        End Try
    End Sub

    Private Function DataRowToJson(datarow As DataRow) As Object
        Try
            Dim dict As New Dictionary(Of String, Object)()
            Dim jsonSetting As JsonSerializerSettings = New JsonSerializerSettings() With {
                                                                .ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                                                                .NullValueHandling = NullValueHandling.Include
                                                             }

            For Each col As DataColumn In datarow.Table.Columns
                If col.ColumnName.StartsWith("lstJson") Then
                    If IsDBNull(datarow(col)) Then
                        dict.Add(col.ColumnName, JsonConvert.DeserializeObject(""))
                    Else
                        dict.Add(col.ColumnName, JsonConvert.DeserializeObject(datarow(col).ToString()))
                    End If
                ElseIf IsDBNull(datarow(col)) Then
                    If col.ColumnName.StartsWith("bit") Then
                        dict.Add(col.ColumnName, False)
                    Else
                        dict.Add(col.ColumnName, String.Empty)
                    End If
                ElseIf col.ColumnName.ToLower() = "numitemcode" Then
                    dict.Add(col.ColumnName, datarow(col).ToString())
                ElseIf col.ColumnName.ToLower() = "vcattributevalues" Then
                    dict.Add(col.ColumnName, JsonConvert.DeserializeObject(Convert.ToString(datarow(col))))
                Else
                    dict.Add(col.ColumnName, datarow(col))
                End If
            Next

            Dim json As String = JsonConvert.SerializeObject(dict, Formatting.None, jsonSetting)

            Return JsonConvert.DeserializeObject(Of ExpandoObject)(json, jsonSetting)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function AppendIdColumn(ByRef dt As DataTable, ByVal IDColumnName As String, Optional ByVal IsItemTable As Boolean = False)
        If dt.Columns.Contains("id") = False Then
            Dim colNew As DataColumn = New DataColumn("id", Type.GetType("System.String"))
            dt.Columns.Add(colNew)

            Dim i As Integer
            For i = 0 To dt.Rows.Count - 1
                Dim ColValue As String = dt.Rows(i)(IDColumnName).ToString()
                dt.Rows(i)("id") = ColValue
            Next

        End If
    End Function

    Private Sub AppendOtherListColumns(ByRef dt As DataTable, ByRef lstWarehouseItems As List(Of ESItemWarehouseDetails), ByRef lstItemCategories As List(Of ESItemCategoryDetails))
        Dim i As Integer
        Dim jsonSetting As JsonSerializerSettings = New JsonSerializerSettings() With {
                                                                .ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                                                                .NullValueHandling = NullValueHandling.Include
                                                             }

        If dt.Columns.Contains("lstJsonWarehouseDetails") = False Then
            Dim colNew As DataColumn = New DataColumn("lstJsonWarehouseDetails", Type.GetType("System.String"))
            dt.Columns.Add(colNew)

            For i = 0 To dt.Rows.Count - 1
                Dim ItemId As String = dt.Rows(i)("numItemCode").ToString()

                If lstWarehouseItems.Any(Function(x) x.numItemID = ItemId) Then
                    dt.Rows(i)("lstJsonWarehouseDetails") = JsonConvert.SerializeObject(lstWarehouseItems.FindAll(Function(x) x.numItemID = ItemId), Formatting.None, jsonSetting)
                End If
            Next
        End If

        If dt.Columns.Contains("lstJsonItemCategories") = False Then
            Dim colNew As DataColumn = New DataColumn("lstJsonItemCategories", Type.GetType("System.String"))
            dt.Columns.Add(colNew)

            For i = 0 To dt.Rows.Count - 1
                Dim ItemId As String = dt.Rows(i)("numItemCode").ToString()
                If lstItemCategories.Any(Function(x) x.numItemID = ItemId) Then
                    dt.Rows(i)("lstJsonItemCategories") = JsonConvert.SerializeObject(lstItemCategories.FindAll(Function(x) x.numItemID = ItemId), Formatting.None, jsonSetting)
                End If
            Next
        End If
    End Sub

    Private Function AppendItemDetailsColumn(ByRef dt As DataTable, ByRef dtItem As DataTable)
        If dt.Columns.Contains("txtDesc") = False Then
            Dim colNew As DataColumn = New DataColumn("txtDesc", Type.GetType("System.String"))
            Dim colNew2 As DataColumn = New DataColumn("txtShortDesc", Type.GetType("System.String"))
            dt.Columns.Add(colNew)
            dt.Columns.Add(colNew2)

            Dim i As Integer
            For i = 0 To dt.Rows.Count - 1
                Dim ItemId As String = dt.Rows(i)("numItemCode").ToString()
                Dim dr As DataRow

                dr = dtItem.Select("numItemCode = " + ItemId + "").FirstOrDefault()
                If dr IsNot Nothing Then
                    dt.Rows(i)("txtDesc") = dr("txtDesc")
                    dt.Rows(i)("txtShortDesc") = dr("txtShortDesc")
                End If
            Next

        End If
    End Function

    Private Shared Function ConvertDataTable(Of T)(ByVal dt As DataTable) As List(Of T)
        Dim data As List(Of T) = New List(Of T)()

        For Each row As DataRow In dt.Rows
            Dim item As T = GetItem(Of T)(row)
            data.Add(item)
        Next

        Return data
    End Function

    Private Shared Function GetItem(Of T)(ByVal dr As DataRow) As T
        Dim temp As Type = GetType(T)
        Dim obj As T = Activator.CreateInstance(Of T)()

        For Each column As DataColumn In dr.Table.Columns

            For Each pro As PropertyInfo In temp.GetProperties()

                If pro.Name = column.ColumnName Then
                    If IsDBNull(dr(column.ColumnName)) Then
                        pro.SetValue(obj, Nothing, Nothing)
                    Else
                        pro.SetValue(obj, dr(column.ColumnName), Nothing)
                    End If

                Else
                    Continue For
                End If
            Next
        Next

        Return obj
    End Function

    Private Shared Sub WriteMessage(ByVal Message As String)
        Try
            BACRM.BusinessLogic.WebAPI.WebAPI.WriteMessage(Message, ConfigurationManager.AppSettings("LogFilePath"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub ElasticSearchWipeOutData()
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Dim ds As New DataSet

            WriteMessage("WipeDataElasticSearch: Get Disable Domain Start")

            Using connection As New Npgsql.NpgsqlConnection(connString)
                Dim sqlAdapter As Npgsql.NpgsqlDataAdapter

                Dim sqlCommand As New Npgsql.NpgsqlCommand
                sqlCommand.Connection = connection
                sqlCommand.CommandTimeout = 300
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.CommandText = "usp_elasticsearchbizcart_getdisabledomain"
                sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                Dim i As Int32 = 0
                connection.Open()
                Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                    sqlCommand.ExecuteNonQuery()

                    For Each parm As Npgsql.NpgsqlParameter In sqlCommand.Parameters
                        If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                            If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                ds.Tables.Add(parm.Value.ToString())
                                sqlAdapter.Fill(ds.Tables(i))
                                i += 1
                            End If
                        End If
                    Next

                    objTransaction.Commit()
                End Using
                connection.Close()
            End Using
            WriteMessage("WipeDataElasticSearch: Get Disable Domain End")

            If ds.Tables.Count > 0 And ds.Tables(0).Rows.Count > 0 Then
                Dim itemIndex As Integer
                For itemIndex = 0 To ds.Tables(0).Rows.Count - 1
                    Dim DomainID As Integer = Convert.ToInt32(ds.Tables(0).Rows(itemIndex)("numDomainID"))
                    ElasticSearchWipeOutIndex(String.Format("biz_cart_{0}_item", DomainID))

                    WriteMessage("WipeDataElasticSearch: Get Modified Records Start")
                    Using connection As New Npgsql.NpgsqlConnection(connString)
                        Dim sqlCommand As New Npgsql.NpgsqlCommand
                        sqlCommand.Connection = connection
                        sqlCommand.CommandType = CommandType.StoredProcedure
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = DomainID})
                        sqlCommand.CommandText = "usp_elasticsearchbizcart_disabledomain"

                        connection.Open()
                        sqlCommand.ExecuteNonQuery()
                        connection.Close()
                    End Using
                    WriteMessage("WipeDataElasticSearch: Get Modified Records End")
                Next
            End If


        Catch ex As Exception
            BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, 0, 0, 0, Nothing)
            WriteMessage("WipeDataElasticSearch Exception: " & ex.Message)
        End Try
    End Sub


    Private Sub ElasticSearchWipeOutIndex(ByVal indexName As String)
        Try
            Dim settings As New Nest.ConnectionSettings(New Uri(ConfigurationManager.AppSettings("ElasticSearchURL")))
            settings.DisableDirectStreaming(True)
            Dim client As New ElasticClient(settings)

            Dim result As IExistsResponse = client.IndexExists(New IndexExistsRequest(Indices.Parse(indexName)))
            If Not result.Exists Then
                client.DeleteIndex(indexName)
            End If

            result = client.IndexExists(New IndexExistsRequest(Indices.Parse(indexName + "_fields")))
            If Not result.Exists Then
                client.DeleteIndex(indexName)
            End If

        Catch ex As Exception
            BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, indexName.Split("_")(0), 0, 0, Nothing)
            WriteMessage("ElasticSearchWipeOutIndex Exception: " & ex.Message)
        End Try
    End Sub

End Class
