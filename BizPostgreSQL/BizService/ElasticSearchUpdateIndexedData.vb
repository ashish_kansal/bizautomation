﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports System.Configuration
Imports Elasticsearch.Net
Imports Nest
Imports Newtonsoft.Json
Imports System.Collections.Generic

Public Class ElasticSearchUpdateIndexedData
    Public Sub ElasticSearchReindexAllData()
        Try
            WriteMessage("ElasticSearchReindexAllData: Reindex of all data Start")

            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim dsDomains As New DataSet
            Using connection As New Npgsql.NpgsqlConnection(connString)
                Dim sqlAdapter As Npgsql.NpgsqlDataAdapter

                Dim sqlCommand As New Npgsql.NpgsqlCommand
                sqlCommand.Connection = connection
                sqlCommand.CommandTimeout = 300
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.CommandText = "usp_elasticsearch_getdomainsforreindex"
                sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                Dim i As Int32 = 0
                connection.Open()
                Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                    sqlCommand.ExecuteNonQuery()

                    For Each parm As Npgsql.NpgsqlParameter In sqlCommand.Parameters
                        If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                            If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                dsDomains.Tables.Add(parm.Value.ToString())
                                sqlAdapter.Fill(dsDomains.Tables(i))
                                i += 1
                            End If
                        End If
                    Next

                    objTransaction.Commit()
                End Using
                connection.Close()
            End Using

            If Not dsDomains Is Nothing AndAlso dsDomains.Tables.Count > 0 AndAlso dsDomains.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In dsDomains.Tables(0).Rows
                    Try
                        CreateActionItemIndexByDomain(connString, dr("numDomainID"))
                        WriteMessage("ElasticSearchReindexAllData completed for Domain:" & Convert.ToString(dr("numDomainID")) & " Module:ActionItem")
                    Catch ex As Exception
                        BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, DomainID, 0, 0, Nothing)
                        WriteMessage("ElasticSearchReindexAllData Error occurred for Domain:" & Convert.ToString(dr("numDomainID")) & " Module:ActionItem")
                    End Try

                    Try
                        CreateBizDocIndexByDomain(connString, dr("numDomainID"))
                        WriteMessage("ElasticSearchReindexAllData completed for Domain:" & Convert.ToString(dr("numDomainID")) & " Module:BizDoc")
                    Catch ex As Exception
                        BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, DomainID, 0, 0, Nothing)
                        WriteMessage("ElasticSearchReindexAllData Error occurred for Domain:" & Convert.ToString(dr("numDomainID")) & " Module:BizDoc")
                    End Try

                    Try
                        CreateCaseIndexByDomain(connString, dr("numDomainID"))
                        WriteMessage("ElasticSearchReindexAllData completed for Domain:" & Convert.ToString(dr("numDomainID")) & " Module:Case")
                    Catch ex As Exception
                        BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, DomainID, 0, 0, Nothing)
                        WriteMessage("ElasticSearchReindexAllData Error occurred for Domain:" & Convert.ToString(dr("numDomainID")) & " Module:Case")
                    End Try

                    Try
                        CreateContactIndexByDomain(connString, dr("numDomainID"))
                        WriteMessage("ElasticSearchReindexAllData completed for Domain:" & Convert.ToString(dr("numDomainID")) & " Module:Contact")
                    Catch ex As Exception
                        BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, DomainID, 0, 0, Nothing)
                        WriteMessage("ElasticSearchReindexAllData Error occurred for Domain:" & Convert.ToString(dr("numDomainID")) & " Module:Contact")
                    End Try

                    Try
                        CreateGeneralLedgerIndexByDomain(connString, dr("numDomainID"))
                        WriteMessage("ElasticSearchReindexAllData completed for Domain:" & Convert.ToString(dr("numDomainID")) & " Module:GeneralLedger")
                    Catch ex As Exception
                        BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, DomainID, 0, 0, Nothing)
                        WriteMessage("ElasticSearchReindexAllData Error occurred for Domain:" & Convert.ToString(dr("numDomainID")) & " Module:GeneralLedger")
                    End Try

                    Try
                        CreateItemIndexByDomain(connString, dr("numDomainID"))
                        WriteMessage("ElasticSearchReindexAllData completed for Domain:" & Convert.ToString(dr("numDomainID")) & " Module:Item")
                    Catch ex As Exception
                        BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, DomainID, 0, 0, Nothing)
                        WriteMessage("ElasticSearchReindexAllData Error occurred for Domain:" & Convert.ToString(dr("numDomainID")) & " Module:Item")
                    End Try

                    Try
                        CreateKnowledgebaseIndexByDomain(connString, dr("numDomainID"))
                        WriteMessage("ElasticSearchReindexAllData completed for Domain:" & Convert.ToString(dr("numDomainID")) & " Module:Knowledgebase")
                    Catch ex As Exception
                        BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, DomainID, 0, 0, Nothing)
                        WriteMessage("ElasticSearchReindexAllData Error occurred for Domain:" & Convert.ToString(dr("numDomainID")) & " Module:Knowledgebase")
                    End Try

                    Try
                        CreateOppOrderIndexByDomain(connString, dr("numDomainID"))
                        WriteMessage("ElasticSearchReindexAllData completed for Domain:" & Convert.ToString(dr("numDomainID")) & " Module:Opp/Order")
                    Catch ex As Exception
                        BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, DomainID, 0, 0, Nothing)
                        WriteMessage("ElasticSearchReindexAllData Error occurred for Domain:" & Convert.ToString(dr("numDomainID")) & " Module:Opp/Order")
                    End Try

                    Try
                        CreateOrganizationIndexByDomain(connString, dr("numDomainID"))
                        WriteMessage("ElasticSearchReindexAllData completed for Domain:" & Convert.ToString(dr("numDomainID")) & " Module:Organization")
                    Catch ex As Exception
                        BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, DomainID, 0, 0, Nothing)
                        WriteMessage("ElasticSearchReindexAllData Error occurred for Domain:" & Convert.ToString(dr("numDomainID")) & " Module:Organization")
                    End Try

                    Try
                        CreateProjectIndexByDomain(connString, dr("numDomainID"))
                        WriteMessage("ElasticSearchReindexAllData completed for Domain:" & Convert.ToString(dr("numDomainID")) & " Module:Project")
                    Catch ex As Exception
                        BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, DomainID, 0, 0, Nothing)
                        WriteMessage("ElasticSearchReindexAllData Error occurred for Domain:" & Convert.ToString(dr("numDomainID")) & " Module:Project")
                    End Try

                    Try
                        CreateReturnIndexByDomain(connString, dr("numDomainID"))
                        WriteMessage("ElasticSearchReindexAllData completed for Domain:" & Convert.ToString(dr("numDomainID")) & " Module:Return")
                    Catch ex As Exception
                        BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, DomainID, 0, 0, Nothing)
                        WriteMessage("ElasticSearchReindexAllData Error occurred for Domain:" & Convert.ToString(dr("numDomainID")) & " Module:Return")
                    End Try
                Next
            End If

            WriteMessage("ElasticSearchReindexAllData: Reindex of all data End")
        Catch ex As Exception
            BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, 0, 0, 0, Nothing)
            WriteMessage("ElasticSearchReindexAllData Exception: " & ex.Message)
        End Try
    End Sub


    Private Sub CreateActionItemIndexByDomain(ByVal connString As String, domainID As Integer)
        Try
            Dim ds As New DataSet

            Using connection As New Npgsql.NpgsqlConnection(connString)
                Dim sqlAdapter As Npgsql.NpgsqlDataAdapter

                Dim sqlCommand As New Npgsql.NpgsqlCommand
                sqlCommand.Connection = connection
                sqlCommand.CommandTimeout = 300
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.CommandText = "usp_elasticsearch_getactionitems"
                sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = domainID})
                sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_vcactionitemids", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Text, .Value = DBNull.Value})
                sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                Dim i As Int32 = 0
                connection.Open()
                Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                    sqlCommand.ExecuteNonQuery()

                    For Each parm As Npgsql.NpgsqlParameter In sqlCommand.Parameters
                        If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                            If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                ds.Tables.Add(parm.Value.ToString())
                                sqlAdapter.Fill(ds.Tables(i))
                                i += 1
                            End If
                        End If
                    Next

                    objTransaction.Commit()
                End Using
                connection.Close()
            End Using

            If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Dim table As DataTable = ds.Tables(0)
                Dim settings As New Nest.ConnectionSettings(New Uri(ConfigurationManager.AppSettings("ElasticSearchURL")))
                Dim client As New ElasticClient(settings)
                Dim result = client.IndexExists(New IndexExistsRequest(Indices.Parse(String.Concat(domainID, "_actionitem"))))

                If result.Exists Then
                    client.DeleteIndex(New DeleteIndexDescriptor(Indices.Parse(String.Concat(domainID, "_actionitem"))))
                    client.Refresh(New RefreshRequest(Indices.All))
                End If

                Dim createIndexResponse As ICreateIndexResponse = client.CreateIndex(String.Concat(domainID, "_actionitem"), Function(c) c.Settings(Function(s) s.NumberOfShards(1).NumberOfReplicas(0).Analysis(Function(a) a.Analyzers(Function(anl) anl.Custom("default", Function(ca) ca.Tokenizer("keyword").Filters(New List(Of String)() From {"lowercase"}))))).Mappings(Function(ms) ms.Map(Of Object)(Function(m) m.DynamicTemplates(Function(x) x.DynamicTemplate("test", Function(t) t.Match("Search_*").Mapping(Function(mp) mp.Text(Function(tx) tx.Index(True))))).Properties(Function(p) p.Number(Function(s) s.Name("id").Index(False)).Text(Function(s) s.Name("displaytext").Index(False)).Text(Function(s) s.Name("text").Index(False)).Text(Function(s) s.Name("url").Index(False))))))

                If createIndexResponse.IsValid Then
                    Dim descriptor As New BulkDescriptor()

                    For Each dr As DataRow In table.Rows
                        descriptor.Index(Of Object)(Function(op) op.Index(String.Concat(domainID, "_actionitem")).Document(DataRowToJson(dr)).Id(dr("id").ToString()))
                    Next

                    Dim res = client.Bulk(descriptor)
                Else
                    Throw New Exception(createIndexResponse.ServerError.[Error].ToString())
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CreateBizDocIndexByDomain(ByVal connString As String, domainID As Integer)
        Try
            Dim ds As New DataSet

            Using connection As New Npgsql.NpgsqlConnection(connString)
                Dim sqlAdapter As Npgsql.NpgsqlDataAdapter

                Dim sqlCommand As New Npgsql.NpgsqlCommand
                sqlCommand.Connection = connection
                sqlCommand.CommandTimeout = 300
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.CommandText = "usp_elasticsearch_getbizdocs"
                sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = domainID})
                sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_vcoppbizdocids", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Text, .Value = DBNull.Value})
                sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                Dim i As Int32 = 0
                connection.Open()
                Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                    sqlCommand.ExecuteNonQuery()

                    For Each parm As Npgsql.NpgsqlParameter In sqlCommand.Parameters
                        If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                            If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                ds.Tables.Add(parm.Value.ToString())
                                sqlAdapter.Fill(ds.Tables(i))
                                i += 1
                            End If
                        End If
                    Next

                    objTransaction.Commit()
                End Using
                connection.Close()
            End Using

            If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Dim table As DataTable = ds.Tables(0)

                Dim settings As New Nest.ConnectionSettings(New Uri(ConfigurationManager.AppSettings("ElasticSearchURL")))
                Dim client As New ElasticClient(settings)
                Dim result = client.IndexExists(New IndexExistsRequest(Indices.Parse(String.Concat(domainID, "_bizdoc"))))

                If result.Exists Then
                    client.DeleteIndex(New DeleteIndexDescriptor(Indices.Parse(String.Concat(domainID, "_bizdoc"))))
                    client.Refresh(New RefreshRequest(Indices.All))
                End If

                Dim createIndexResponse As ICreateIndexResponse = client.CreateIndex(String.Concat(domainID, "_bizdoc"), Function(c) c.Settings(Function(s) s.NumberOfShards(1).NumberOfReplicas(0).Analysis(Function(a) a.Analyzers(Function(anl) anl.Custom("default", Function(ca) ca.Tokenizer("keyword").Filters(New List(Of String)() From {"lowercase"}))))).Mappings(Function(ms) ms.Map(Of Object)(Function(m) m.DynamicTemplates(Function(x) x.DynamicTemplate("test", Function(t) t.Match("Search_*").Mapping(Function(mp) mp.Text(Function(tx) tx.Index(True))))).Properties(Function(p) p.Number(Function(s) s.Name("id").Index(False)).Text(Function(s) s.Name("displaytext").Index(False)).Text(Function(s) s.Name("text").Index(False)).Text(Function(s) s.Name("url").Index(False))))))

                If createIndexResponse.IsValid Then
                    Dim descriptor As New BulkDescriptor()

                    For Each dr As DataRow In table.Rows
                        descriptor.Index(Of Object)(Function(op) op.Index(String.Concat(domainID, "_bizdoc")).Document(DataRowToJson(dr)).Id(dr("id").ToString()))
                    Next

                    Dim res = client.Bulk(descriptor)
                Else
                    Throw New Exception(createIndexResponse.ServerError.[Error].ToString())
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CreateCaseIndexByDomain(ByVal connString As String, domainID As Integer)
        Try
            Dim ds As New DataSet

            Using connection As New Npgsql.NpgsqlConnection(connString)
                Dim sqlAdapter As Npgsql.NpgsqlDataAdapter

                Dim sqlCommand As New Npgsql.NpgsqlCommand
                sqlCommand.Connection = connection
                sqlCommand.CommandTimeout = 300
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.CommandText = "usp_elasticsearch_getcases"
                sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = domainID})
                sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_vccaseids", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Text, .Value = DBNull.Value})
                sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                Dim i As Int32 = 0
                connection.Open()
                Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                    sqlCommand.ExecuteNonQuery()

                    For Each parm As Npgsql.NpgsqlParameter In sqlCommand.Parameters
                        If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                            If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                ds.Tables.Add(parm.Value.ToString())
                                sqlAdapter.Fill(ds.Tables(i))
                                i += 1
                            End If
                        End If
                    Next

                    objTransaction.Commit()
                End Using
                connection.Close()
            End Using

            If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Dim table As DataTable = ds.Tables(0)

                Dim settings As New Nest.ConnectionSettings(New Uri(ConfigurationManager.AppSettings("ElasticSearchURL")))
                Dim client As New ElasticClient(settings)
                Dim result = client.IndexExists(New IndexExistsRequest(Indices.Parse(String.Concat(domainID, "_case"))))

                If result.Exists Then
                    client.DeleteIndex(New DeleteIndexDescriptor(Indices.Parse(String.Concat(domainID, "_case"))))
                    client.Refresh(New RefreshRequest(Indices.All))
                End If

                Dim createIndexResponse = client.CreateIndex(String.Concat(domainID, "_case"), Function(c) c.Settings(Function(s) s.NumberOfShards(1).NumberOfReplicas(0).Analysis(Function(a) a.Analyzers(Function(anl) anl.Custom("default", Function(ca) ca.Tokenizer("keyword").Filters(New List(Of String)() From {"lowercase"}))))).Mappings(Function(ms) ms.Map(Of Object)(Function(m) m.DynamicTemplates(Function(x) x.DynamicTemplate("test", Function(t) t.Match("Search_*").Mapping(Function(mp) mp.Text(Function(tx) tx.Index(True))))).Properties(Function(p) p.Number(Function(s) s.Name("id").Index(False)).Text(Function(s) s.Name("displaytext").Index(False)).Text(Function(s) s.Name("text").Index(False)).Text(Function(s) s.Name("url").Index(False))))))

                If createIndexResponse.IsValid Then
                    Dim descriptor As New BulkDescriptor()

                    For Each dr As DataRow In table.Rows
                        descriptor.Index(Of Object)(Function(op) op.Index(String.Concat(domainID, "_case")).Document(DataRowToJson(dr)).Id(dr("id").ToString()))
                    Next

                    Dim res = client.Bulk(descriptor)
                Else
                    Throw New Exception(createIndexResponse.ServerError.[Error].ToString())
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CreateContactIndexByDomain(ByVal connString As String, domainID As Integer)
        Try
            Dim ds As New DataSet

            Using connection As New Npgsql.NpgsqlConnection(connString)
                Dim sqlAdapter As Npgsql.NpgsqlDataAdapter

                Dim sqlCommand As New Npgsql.NpgsqlCommand
                sqlCommand.Connection = connection
                sqlCommand.CommandTimeout = 300
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.CommandText = "usp_elasticsearch_getcontacts"
                sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = domainID})
                sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_vccontactids", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Text, .Value = DBNull.Value})
                sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                Dim i As Int32 = 0
                connection.Open()
                Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                    sqlCommand.ExecuteNonQuery()

                    For Each parm As Npgsql.NpgsqlParameter In sqlCommand.Parameters
                        If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                            If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                ds.Tables.Add(parm.Value.ToString())
                                sqlAdapter.Fill(ds.Tables(i))
                                i += 1
                            End If
                        End If
                    Next

                    objTransaction.Commit()
                End Using
                connection.Close()
            End Using

            If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Dim table As DataTable = ds.Tables(0)

                Dim settings As New Nest.ConnectionSettings(New Uri(ConfigurationManager.AppSettings("ElasticSearchURL")))
                Dim client As New ElasticClient(settings)
                Dim result = client.IndexExists(New IndexExistsRequest(Indices.Parse(String.Concat(domainID, "_contact"))))

                If result.Exists Then
                    client.DeleteIndex(New DeleteIndexDescriptor(Indices.Parse(String.Concat(domainID, "_contact"))))
                    client.Refresh(New RefreshRequest(Indices.All))
                End If

                Dim createIndexResponse = client.CreateIndex(String.Concat(domainID, "_contact"), Function(c) c.Settings(Function(s) s.NumberOfShards(1).NumberOfReplicas(0).Analysis(Function(a) a.Analyzers(Function(anl) anl.Custom("default", Function(ca) ca.Tokenizer("keyword").Filters(New List(Of String)() From {"lowercase"}))))).Mappings(Function(ms) ms.Map(Of Object)(Function(m) m.DynamicTemplates(Function(x) x.DynamicTemplate("test", Function(t) t.Match("Search_*").Mapping(Function(mp) mp.Text(Function(tx) tx.Index(True))))).Properties(Function(p) p.Number(Function(s) s.Name("id").Index(False)).Text(Function(s) s.Name("displaytext").Index(False)).Text(Function(s) s.Name("text").Index(False)).Text(Function(s) s.Name("url").Index(False))))))

                If createIndexResponse.IsValid Then
                    Dim descriptor As New BulkDescriptor()

                    For Each dr As DataRow In table.Rows
                        descriptor.Index(Of Object)(Function(op) op.Index(String.Concat(domainID, "_contact")).Document(DataRowToJson(dr)).Id(dr("id").ToString()))
                    Next

                    Dim res = client.Bulk(descriptor)
                Else
                    Throw New Exception(createIndexResponse.ServerError.[Error].ToString())
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CreateGeneralLedgerIndexByDomain(ByVal connString As String, domainID As Integer)
        Try
            Dim ds As New DataSet
            Using connection As New Npgsql.NpgsqlConnection(connString)
                Dim sqlAdapter As Npgsql.NpgsqlDataAdapter

                Dim sqlCommand As New Npgsql.NpgsqlCommand
                sqlCommand.Connection = connection
                sqlCommand.CommandTimeout = 300
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.CommandText = "usp_elasticsearch_getgeneralledger"
                sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = domainID})
                sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                Dim i As Int32 = 0
                connection.Open()
                Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                    sqlCommand.ExecuteNonQuery()

                    For Each parm As Npgsql.NpgsqlParameter In sqlCommand.Parameters
                        If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                            If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                ds.Tables.Add(parm.Value.ToString())
                                sqlAdapter.Fill(ds.Tables(i))
                                i += 1
                            End If
                        End If
                    Next

                    objTransaction.Commit()
                End Using
                connection.Close()
            End Using

            If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Dim table As DataTable = ds.Tables(0)

                Dim settings As New Nest.ConnectionSettings(New Uri(ConfigurationManager.AppSettings("ElasticSearchURL")))
                Dim client As New ElasticClient(settings)
                Dim result = client.IndexExists(New IndexExistsRequest(Indices.Parse(String.Concat(domainID, "_generalledger"))))

                If result.Exists Then
                    client.DeleteIndex(New DeleteIndexDescriptor(Indices.Parse(String.Concat(domainID, "_generalledger"))))
                    client.Refresh(New RefreshRequest(Indices.All))
                End If

                Dim createIndexResponse As ICreateIndexResponse = client.CreateIndex(String.Concat(domainID, "_generalledger"), Function(c) c.Settings(Function(s) s.NumberOfShards(1).NumberOfReplicas(0).Analysis(Function(a) a.Analyzers(Function(anl) anl.Custom("default", Function(ca) ca.Tokenizer("keyword").Filters(New List(Of String)() From {"lowercase"}))))).Mappings(Function(ms) ms.Map(Of Object)(Function(m) m.DynamicTemplates(Function(x) x.DynamicTemplate("test", Function(t) t.Match("Search_*").Mapping(Function(mp) mp.Text(Function(tx) tx.Index(True))))).Properties(Function(p) p.Number(Function(s) s.Name("id").Index(False)).Text(Function(s) s.Name("displaytext").Index(False)).Text(Function(s) s.Name("text").Index(False)).Text(Function(s) s.Name("url").Index(False)).Date(Function(d) d.Name("SearchDate_EntryDate")).Number(Function(n) n.Name("SearchNumber_Amount"))))))

                If createIndexResponse.IsValid Then
                    Dim descriptor As New BulkDescriptor()
                    For Each dr As DataRow In table.Rows
                        descriptor.Index(Of Object)(Function(op) op.Index(String.Concat(domainID, "_generalledger")).Document(DataRowToJson(dr)).Id(dr("id").ToString()))
                    Next

                    Dim res = client.Bulk(descriptor)
                Else
                    Throw New Exception(createIndexResponse.ServerError.[Error].ToString())
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CreateItemIndexByDomain(ByVal connString As String, domainID As Integer)
        Try
            Dim ds As New DataSet
            Dim sqlAdapter As Npgsql.NpgsqlDataAdapter

            Using connection As New Npgsql.NpgsqlConnection(connString)
                Dim cmd As New Npgsql.NpgsqlCommand()
                cmd.Connection = connection
                cmd.CommandType = System.Data.CommandType.StoredProcedure
                cmd.CommandText = "usp_elasticsearch_getitems"
                cmd.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = domainID})
                cmd.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_vcitemcodes", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Text, .Value = DBNull.Value})
                cmd.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                Dim i As Int32 = 0
                connection.Open()
                Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                    cmd.ExecuteNonQuery()

                    For Each parm As Npgsql.NpgsqlParameter In cmd.Parameters
                        If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                            If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                ds.Tables.Add(parm.Value.ToString())
                                sqlAdapter.Fill(ds.Tables(i))
                                i += 1
                            End If
                        End If
                    Next

                    objTransaction.Commit()
                End Using
                connection.Close()


                If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Dim table As DataTable = ds.Tables(0)

                    Dim settings As New Nest.ConnectionSettings(New Uri(ConfigurationManager.AppSettings("ElasticSearchURL")))
                    Dim client As New ElasticClient(settings)
                    Dim result = client.IndexExists(New IndexExistsRequest(Indices.Parse(String.Concat(domainID, "_item"))))

                    If result.Exists Then
                        client.DeleteIndex(New DeleteIndexDescriptor(Indices.Parse(String.Concat(domainID, "_item"))))
                        client.Refresh(New RefreshRequest(Indices.All))
                    End If

                    Dim createIndexResponse = client.CreateIndex(String.Concat(domainID, "_item"), Function(c) c.Settings(Function(s) s.NumberOfShards(1).NumberOfReplicas(0).Analysis(Function(a) a.Analyzers(Function(anl) anl.Custom("default", Function(ca) ca.Tokenizer("keyword").Filters(New List(Of String)() From {"lowercase"}))))).Mappings(Function(ms) ms.Map(Of Object)(Function(m) m.DynamicTemplates(Function(x) x.DynamicTemplate("test", Function(t) t.Match("Search_*").Mapping(Function(mp) mp.Text(Function(tx) tx.Index(True))))).Properties(Function(p) p.Number(Function(s) s.Name("id").Index(False)).Text(Function(s) s.Name("displaytext").Index(False)).Text(Function(s) s.Name("text").Index(False)).Text(Function(s) s.Name("url").Index(False))))))

                    If createIndexResponse.IsValid Then
                        Dim descriptor As New BulkDescriptor()

                        For Each dr As DataRow In table.Rows
                            descriptor.Index(Of Object)(Function(op) op.Index(String.Concat(domainID, "_item")).Document(DataRowToJson(dr)).Id(dr("id").ToString()))
                        Next

                        Dim res = client.Bulk(descriptor)
                    Else
                        Throw New Exception(createIndexResponse.ServerError.[Error].ToString())
                    End If
                End If
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CreateKnowledgebaseIndexByDomain(ByVal connString As String, domainID As Integer)
        Try
            Dim ds As New DataSet
            Using connection As New Npgsql.NpgsqlConnection(connString)
                Dim sqlAdapter As Npgsql.NpgsqlDataAdapter

                Dim sqlCommand As New Npgsql.NpgsqlCommand
                sqlCommand.Connection = connection
                sqlCommand.CommandTimeout = 300
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.CommandText = "usp_elasticsearch_getknowledgebase"
                sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = domainID})
                sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_vcsolnid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Text, .Value = DBNull.Value})
                sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                Dim i As Int32 = 0
                connection.Open()
                Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                    sqlCommand.ExecuteNonQuery()

                    For Each parm As Npgsql.NpgsqlParameter In sqlCommand.Parameters
                        If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                            If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                ds.Tables.Add(parm.Value.ToString())
                                sqlAdapter.Fill(ds.Tables(i))
                                i += 1
                            End If
                        End If
                    Next

                    objTransaction.Commit()
                End Using
                connection.Close()
            End Using

            If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Dim table As DataTable = ds.Tables(0)

                Dim settings As New Nest.ConnectionSettings(New Uri(ConfigurationManager.AppSettings("ElasticSearchURL")))
                Dim client As New ElasticClient(settings)
                Dim result = client.IndexExists(New IndexExistsRequest(Indices.Parse(String.Concat(domainID, "_knowledgebase"))))

                If result.Exists Then
                    client.DeleteIndex(New DeleteIndexDescriptor(Indices.Parse(String.Concat(domainID, "_knowledgebase"))))
                    client.Refresh(New RefreshRequest(Indices.All))
                End If

                Dim createIndexResponse = client.CreateIndex(String.Concat(domainID, "_knowledgebase"), Function(c) c.Settings(Function(s) s.NumberOfShards(1).NumberOfReplicas(0).Analysis(Function(a) a.Analyzers(Function(anl) anl.Custom("default", Function(ca) ca.Tokenizer("keyword").Filters(New List(Of String)() From {"lowercase"}))))).Mappings(Function(ms) ms.Map(Of Object)(Function(m) m.DynamicTemplates(Function(x) x.DynamicTemplate("test", Function(t) t.Match("Search_*").Mapping(Function(mp) mp.Text(Function(tx) tx.Index(True))))).Properties(Function(p) p.Number(Function(s) s.Name("id").Index(False)).Text(Function(s) s.Name("displaytext").Index(False)).Text(Function(s) s.Name("text").Index(False)).Text(Function(s) s.Name("url").Index(False))))))

                If createIndexResponse.IsValid Then
                    Dim descriptor As New BulkDescriptor()

                    For Each dr As DataRow In table.Rows
                        descriptor.Index(Of Object)(Function(op) op.Index(String.Concat(domainID, "_knowledgebase")).Document(DataRowToJson(dr)).Id(dr("id").ToString()))
                    Next

                    Dim res = client.Bulk(descriptor)
                Else
                    Throw New Exception(createIndexResponse.ServerError.[Error].ToString())
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CreateOppOrderIndexByDomain(ByVal connString As String, domainID As Integer)
        Try
            Dim ds As New DataSet
            Using connection As New Npgsql.NpgsqlConnection(connString)
                Dim sqlAdapter As Npgsql.NpgsqlDataAdapter

                Dim sqlCommand As New Npgsql.NpgsqlCommand
                sqlCommand.Connection = connection
                sqlCommand.CommandTimeout = 300
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.CommandText = "usp_elasticsearch_getopporders"
                sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = domainID})
                sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_vcoppids", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Text, .Value = DBNull.Value})
                sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                Dim i As Int32 = 0
                connection.Open()
                Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                    sqlCommand.ExecuteNonQuery()

                    For Each parm As Npgsql.NpgsqlParameter In sqlCommand.Parameters
                        If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                            If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                ds.Tables.Add(parm.Value.ToString())
                                sqlAdapter.Fill(ds.Tables(i))
                                i += 1
                            End If
                        End If
                    Next

                    objTransaction.Commit()
                End Using
                connection.Close()
            End Using

            If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Dim table As DataTable = ds.Tables(0)

                Dim settings As New Nest.ConnectionSettings(New Uri(ConfigurationManager.AppSettings("ElasticSearchURL")))
                Dim client As New ElasticClient(settings)
                Dim result = client.IndexExists(New IndexExistsRequest(Indices.Parse(String.Concat(domainID, "_opporder"))))

                If result.Exists Then
                    client.DeleteIndex(New DeleteIndexDescriptor(Indices.Parse(String.Concat(domainID, "_opporder"))))
                    client.Refresh(New RefreshRequest(Indices.All))
                End If

                Dim createIndexResponse = client.CreateIndex(String.Concat(domainID, "_opporder"), Function(c) c.Settings(Function(s) s.NumberOfShards(1).NumberOfReplicas(0).Analysis(Function(a) a.Analyzers(Function(anl) anl.Custom("default", Function(ca) ca.Tokenizer("keyword").Filters(New List(Of String)() From {"lowercase"}))))).Mappings(Function(ms) ms.Map(Of Object)(Function(m) m.DynamicTemplates(Function(x) x.DynamicTemplate("test", Function(t) t.Match("Search_*").Mapping(Function(mp) mp.Text(Function(tx) tx.Index(True))))).Properties(Function(p) p.Number(Function(s) s.Name("id").Index(False)).Text(Function(s) s.Name("displaytext").Index(False)).Text(Function(s) s.Name("text").Index(False)).Text(Function(s) s.Name("url").Index(False))))))

                If createIndexResponse.IsValid Then
                    Dim descriptor As New BulkDescriptor()

                    For Each dr As DataRow In table.Rows
                        descriptor.Index(Of Object)(Function(op) op.Index(String.Concat(domainID, "_opporder")).Document(DataRowToJson(dr)).Id(dr("id").ToString()))
                    Next

                    Dim res = client.Bulk(descriptor)
                Else
                    Throw New Exception(createIndexResponse.ServerError.Error.ToString())
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CreateOrganizationIndexByDomain(ByVal connString As String, domainID As Integer)
        Try
            Dim ds As New DataSet
            Dim sqlAdapter As Npgsql.NpgsqlDataAdapter
            Using connection As New Npgsql.NpgsqlConnection(connString)
                Dim cmd As New Npgsql.NpgsqlCommand()
                cmd.Connection = connection
                cmd.CommandType = System.Data.CommandType.StoredProcedure
                cmd.CommandText = "usp_elasticsearch_getorganizations"
                cmd.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = domainID})
                cmd.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_vccompanyids", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Text, .Value = DBNull.Value})
                cmd.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                Dim i As Int32 = 0
                connection.Open()
                Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                    cmd.ExecuteNonQuery()

                    For Each parm As Npgsql.NpgsqlParameter In cmd.Parameters
                        If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                            If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                ds.Tables.Add(parm.Value.ToString())
                                sqlAdapter.Fill(ds.Tables(i))
                                i += 1
                            End If
                        End If
                    Next

                    objTransaction.Commit()
                End Using
                connection.Close()

                If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Dim table As DataTable = ds.Tables(0)

                    Dim settings As New Nest.ConnectionSettings(New Uri(ConfigurationManager.AppSettings("ElasticSearchURL")))
                    Dim client As New ElasticClient(settings)
                    Dim result = client.IndexExists(New IndexExistsRequest(Indices.Parse(String.Concat(domainID, "_organization"))))

                    If result.Exists Then
                        client.DeleteIndex(New DeleteIndexDescriptor(Indices.Parse(String.Concat(domainID, "_organization"))))
                        client.Refresh(New RefreshRequest(Indices.All))
                    End If

                    Dim createIndexResponse = client.CreateIndex(String.Concat(domainID, "_organization"), Function(c) c.Settings(Function(s) s.NumberOfShards(1).NumberOfReplicas(0).Analysis(Function(a) a.Analyzers(Function(anl) anl.Custom("default", Function(ca) ca.Tokenizer("keyword").Filters(New List(Of String)() From {"lowercase"}))))).Mappings(Function(ms) ms.Map(Of Object)(Function(m) m.DynamicTemplates(Function(x) x.DynamicTemplate("test", Function(t) t.Match("Search_*").Mapping(Function(mp) mp.Text(Function(tx) tx.Index(True))))).Properties(Function(p) p.Number(Function(s) s.Name("id").Index(False)).Text(Function(s) s.Name("displaytext").Index(False)).Text(Function(s) s.Name("text").Index(False)).Text(Function(s) s.Name("url").Index(False))))))

                    If createIndexResponse.IsValid Then
                        Dim descriptor As New BulkDescriptor()

                        For Each dr As DataRow In table.Rows
                            descriptor.Index(Of Object)(Function(op) op.Index(String.Concat(domainID, "_organization")).Document(DataRowToJson(dr)).Id(dr("id").ToString()))
                        Next

                        Dim res = client.Bulk(descriptor)
                    Else
                        Throw New Exception(createIndexResponse.ServerError.[Error].ToString())
                    End If
                End If
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CreateProjectIndexByDomain(ByVal connString As String, domainID As Integer)
        Try
            Dim ds As New DataSet
            Using connection As New Npgsql.NpgsqlConnection(connString)
                Dim sqlAdapter As Npgsql.NpgsqlDataAdapter

                Dim sqlCommand As New Npgsql.NpgsqlCommand
                sqlCommand.Connection = connection
                sqlCommand.CommandTimeout = 300
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.CommandText = "usp_elasticsearch_getprojects"
                sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = domainID})
                sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_vcprojectids", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Text, .Value = DBNull.Value})
                sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                Dim i As Int32 = 0
                connection.Open()
                Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                    sqlCommand.ExecuteNonQuery()

                    For Each parm As Npgsql.NpgsqlParameter In sqlCommand.Parameters
                        If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                            If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                ds.Tables.Add(parm.Value.ToString())
                                sqlAdapter.Fill(ds.Tables(i))
                                i += 1
                            End If
                        End If
                    Next

                    objTransaction.Commit()
                End Using
                connection.Close()
            End Using

            If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Dim table As DataTable = ds.Tables(0)

                Dim settings As New Nest.ConnectionSettings(New Uri(ConfigurationManager.AppSettings("ElasticSearchURL")))
                Dim client As New ElasticClient(settings)
                Dim result = client.IndexExists(New IndexExistsRequest(Indices.Parse(String.Concat(domainID, "_project"))))

                If result.Exists Then
                    client.DeleteIndex(New DeleteIndexDescriptor(Indices.Parse(String.Concat(domainID, "_project"))))
                    client.Refresh(New RefreshRequest(Indices.All))
                End If

                Dim createIndexResponse = client.CreateIndex(String.Concat(domainID, "_project"), Function(c) c.Settings(Function(s) s.NumberOfShards(1).NumberOfReplicas(0).Analysis(Function(a) a.Analyzers(Function(anl) anl.Custom("default", Function(ca) ca.Tokenizer("keyword").Filters(New List(Of String)() From {"lowercase"}))))).Mappings(Function(ms) ms.Map(Of Object)(Function(m) m.DynamicTemplates(Function(x) x.DynamicTemplate("test", Function(t) t.Match("Search_*").Mapping(Function(mp) mp.Text(Function(tx) tx.Index(True))))).Properties(Function(p) p.Number(Function(s) s.Name("id").Index(False)).Text(Function(s) s.Name("displaytext").Index(False)).Text(Function(s) s.Name("text").Index(False)).Text(Function(s) s.Name("url").Index(False))))))

                If createIndexResponse.IsValid Then
                    Dim descriptor As New BulkDescriptor()

                    For Each dr As DataRow In table.Rows
                        descriptor.Index(Of Object)(Function(op) op.Index(String.Concat(domainID, "_project")).Document(DataRowToJson(dr)).Id(dr("id").ToString()))
                    Next

                    Dim res = client.Bulk(descriptor)
                Else
                    Throw New Exception(createIndexResponse.ServerError.[Error].ToString())
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CreateReturnIndexByDomain(ByVal connString As String, domainID As Integer)
        Try
            Dim ds As New DataSet
            Using connection As New Npgsql.NpgsqlConnection(connString)
                Dim sqlAdapter As Npgsql.NpgsqlDataAdapter

                Dim sqlCommand As New Npgsql.NpgsqlCommand
                sqlCommand.Connection = connection
                sqlCommand.CommandTimeout = 300
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.CommandText = "usp_elasticsearch_getreturns"
                sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = domainID})
                sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_vcreturnheaderids", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Text, .Value = DBNull.Value})
                sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                Dim i As Int32 = 0
                connection.Open()
                Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                    sqlCommand.ExecuteNonQuery()

                    For Each parm As Npgsql.NpgsqlParameter In sqlCommand.Parameters
                        If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                            If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                ds.Tables.Add(parm.Value.ToString())
                                sqlAdapter.Fill(ds.Tables(i))
                                i += 1
                            End If
                        End If
                    Next

                    objTransaction.Commit()
                End Using
                connection.Close()
            End Using

            If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Dim table As DataTable = ds.Tables(0)

                Dim settings As New Nest.ConnectionSettings(New Uri(ConfigurationManager.AppSettings("ElasticSearchURL")))
                Dim client As New ElasticClient(settings)
                Dim result = client.IndexExists(New IndexExistsRequest(Indices.Parse(String.Concat(domainID, "_return"))))

                If result.Exists Then
                    client.DeleteIndex(New DeleteIndexDescriptor(Indices.Parse(String.Concat(domainID, "_return"))))
                    client.Refresh(New RefreshRequest(Indices.All))
                End If

                Dim createIndexResponse = client.CreateIndex(String.Concat(domainID, "_return"), Function(c) c.Settings(Function(s) s.NumberOfShards(1).NumberOfReplicas(0).Analysis(Function(a) a.Analyzers(Function(anl) anl.Custom("default", Function(ca) ca.Tokenizer("keyword").Filters(New List(Of String)() From {"lowercase"}))))).Mappings(Function(ms) ms.Map(Of Object)(Function(m) m.DynamicTemplates(Function(x) x.DynamicTemplate("test", Function(t) t.Match("Search_*").Mapping(Function(mp) mp.Text(Function(tx) tx.Index(True))))).Properties(Function(p) p.Number(Function(s) s.Name("id").Index(False)).Text(Function(s) s.Name("displaytext").Index(False)).Text(Function(s) s.Name("text").Index(False)).Text(Function(s) s.Name("url").Index(False))))))

                If createIndexResponse.IsValid Then
                    Dim descriptor As New BulkDescriptor()

                    For Each dr As DataRow In table.Rows
                        descriptor.Index(Of Object)(Function(op) op.Index(String.Concat(domainID, "_return")).Document(DataRowToJson(dr)).Id(dr("id").ToString()))
                    Next

                    Dim res = client.Bulk(descriptor)
                Else
                    Throw New Exception(createIndexResponse.ServerError.[Error].ToString())
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub UpdateElasticSearchData()
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Dim ds As New DataSet

            WriteMessage("ElasticSearchDataSync: Get Modified Records Start")
            Using connection As New Npgsql.NpgsqlConnection(connString)
                Dim sqlAdapter As Npgsql.NpgsqlDataAdapter

                Dim sqlCommand As New Npgsql.NpgsqlCommand
                sqlCommand.Connection = connection
                sqlCommand.CommandType = CommandType.StoredProcedure
                sqlCommand.CommandText = "USP_ElasticSearchModifiedRecords_Get"
                sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})
                sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur2", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})
                sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur3", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})
                Dim i As Int32 = 0
                connection.Open()
                Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                    sqlCommand.ExecuteNonQuery()

                    For Each parm As Npgsql.NpgsqlParameter In sqlCommand.Parameters
                        If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                            If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                ds.Tables.Add(parm.Value.ToString())
                                sqlAdapter.Fill(ds.Tables(i))
                                i += 1
                            End If
                        End If
                    Next

                    objTransaction.Commit()
                End Using
                connection.Close()
            End Using
            WriteMessage("ElasticSearchDataSync: Get Modified Records End")

            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                If ds.Tables.Count > 0 Then  'Inserted Records
                    Dim uniqueCols As DataTable = ds.Tables(0).DefaultView.ToTable(True, "numDomainID")

                    For Each drDomain As DataRow In uniqueCols.Rows
                        Dim actionItemRecords As DataRow() = ds.Tables(0).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='ActionItem'").ToArray()
                        Dim bizDocRecords As DataRow() = ds.Tables(0).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='BizDoc'").ToArray()
                        Dim caseRecords As DataRow() = ds.Tables(0).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='Case'").ToArray()
                        Dim itemRecords As DataRow() = ds.Tables(0).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='Item'").ToArray()
                        Dim knowledgeBaseRecords As DataRow() = ds.Tables(0).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='KnowledgeBase'").ToArray()
                        Dim organizationRecords As DataRow() = ds.Tables(0).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='Organization'").ToArray()
                        Dim orderRecords As DataRow() = ds.Tables(0).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='Opp/Order'").ToArray()
                        Dim projectRecords As DataRow() = ds.Tables(0).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='Project'").ToArray()
                        Dim returnRecords As DataRow() = ds.Tables(0).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='Return'").ToArray()
                        Dim contactRecords As DataRow() = ds.Tables(0).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='Contact'").ToArray()
                        Dim emailRecords As DataRow() = ds.Tables(0).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='Email'").ToArray()

                        ElasticSearchManageActionItems(connString, drDomain("numDomainID"), actionItemRecords, "Insert")
                        ElasticSearchManageCases(connString, drDomain("numDomainID"), caseRecords, "Insert")
                        ElasticSearchManageBizDocs(connString, drDomain("numDomainID"), bizDocRecords, "Insert")
                        ElasticSearchManageItems(connString, drDomain("numDomainID"), itemRecords, "Insert")
                        ElasticSearchManageKnowledgeBases(connString, drDomain("numDomainID"), knowledgeBaseRecords, "Insert")
                        ElasticSearchManageOrganizations(connString, drDomain("numDomainID"), organizationRecords, "Insert")
                        ElasticSearchManageOrders(connString, drDomain("numDomainID"), orderRecords, "Insert")
                        ElasticSearchManageProjects(connString, drDomain("numDomainID"), projectRecords, "Insert")
                        ElasticSearchManageReturns(connString, drDomain("numDomainID"), returnRecords, "Insert")
                        ElasticSearchManageContacts(connString, drDomain("numDomainID"), contactRecords, "Insert")
                        ElasticSearchManageEmails(connString, drDomain("numDomainID"), emailRecords, "Insert")
                    Next
                End If

                If ds.Tables.Count > 1 Then  'Updated Records
                    Dim uniqueCols As DataTable = ds.Tables(1).DefaultView.ToTable(True, "numDomainID")

                    For Each drDomain As DataRow In uniqueCols.Rows
                        Dim actionItemRecords As DataRow() = ds.Tables(1).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='ActionItem'").ToArray()
                        Dim bizDocRecords As DataRow() = ds.Tables(1).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='BizDoc'").ToArray()
                        Dim caseRecords As DataRow() = ds.Tables(1).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='Case'").ToArray()
                        Dim itemRecords As DataRow() = ds.Tables(1).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='Item'").ToArray()
                        Dim knowledgeBaseRecords As DataRow() = ds.Tables(1).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='KnowledgeBase'").ToArray()
                        Dim organizationRecords As DataRow() = ds.Tables(1).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='Organization'").ToArray()
                        Dim orderRecords As DataRow() = ds.Tables(1).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='Opp/Order'").ToArray()
                        Dim projectRecords As DataRow() = ds.Tables(1).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='Project'").ToArray()
                        Dim returnRecords As DataRow() = ds.Tables(1).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='Return'").ToArray()
                        Dim contactRecords As DataRow() = ds.Tables(1).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='Contact'").ToArray()

                        ElasticSearchManageActionItems(connString, drDomain("numDomainID"), actionItemRecords, "Update")
                        ElasticSearchManageCases(connString, drDomain("numDomainID"), caseRecords, "Update")
                        ElasticSearchManageBizDocs(connString, drDomain("numDomainID"), bizDocRecords, "Update")
                        ElasticSearchManageItems(connString, drDomain("numDomainID"), itemRecords, "Update")
                        ElasticSearchManageKnowledgeBases(connString, drDomain("numDomainID"), knowledgeBaseRecords, "Update")
                        ElasticSearchManageOrganizations(connString, drDomain("numDomainID"), organizationRecords, "Update")
                        ElasticSearchManageOrders(connString, drDomain("numDomainID"), orderRecords, "Update")
                        ElasticSearchManageProjects(connString, drDomain("numDomainID"), projectRecords, "Update")
                        ElasticSearchManageReturns(connString, drDomain("numDomainID"), returnRecords, "Update")
                        ElasticSearchManageContacts(connString, drDomain("numDomainID"), contactRecords, "Update")
                    Next
                End If

                If ds.Tables.Count > 2 Then  'Deleted Records
                    Dim uniqueCols As DataTable = ds.Tables(2).DefaultView.ToTable(True, "numDomainID")

                    For Each drDomain As DataRow In uniqueCols.Rows
                        Dim actionItemRecords As DataRow() = ds.Tables(2).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='ActionItem'").ToArray()
                        Dim bizDocRecords As DataRow() = ds.Tables(2).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='BizDoc'").ToArray()
                        Dim caseRecords As DataRow() = ds.Tables(2).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='Case'").ToArray()
                        Dim itemRecords As DataRow() = ds.Tables(2).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='Item'").ToArray()
                        Dim knowledgeBaseRecords As DataRow() = ds.Tables(2).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='KnowledgeBase'").ToArray()
                        Dim organizationRecords As DataRow() = ds.Tables(2).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='Organization'").ToArray()
                        Dim orderRecords As DataRow() = ds.Tables(2).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='Opp/Order'").ToArray()
                        Dim projectRecords As DataRow() = ds.Tables(2).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='Project'").ToArray()
                        Dim returnRecords As DataRow() = ds.Tables(2).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='Return'").ToArray()
                        Dim contactRecords As DataRow() = ds.Tables(2).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='Contact'").ToArray()
                        Dim emailRecords As DataRow() = ds.Tables(2).Select("numDomainID=" & drDomain("numDomainID") & " AND vcModule='Email'").ToArray()

                        ElasticSearchManageActionItems(connString, drDomain("numDomainID"), actionItemRecords, "Delete")
                        ElasticSearchManageCases(connString, drDomain("numDomainID"), caseRecords, "Delete")
                        ElasticSearchManageBizDocs(connString, drDomain("numDomainID"), bizDocRecords, "Delete")
                        ElasticSearchManageItems(connString, drDomain("numDomainID"), itemRecords, "Delete")
                        ElasticSearchManageKnowledgeBases(connString, drDomain("numDomainID"), knowledgeBaseRecords, "Delete")
                        ElasticSearchManageOrganizations(connString, drDomain("numDomainID"), organizationRecords, "Delete")
                        ElasticSearchManageOrders(connString, drDomain("numDomainID"), orderRecords, "Delete")
                        ElasticSearchManageProjects(connString, drDomain("numDomainID"), projectRecords, "Delete")
                        ElasticSearchManageReturns(connString, drDomain("numDomainID"), returnRecords, "Delete")
                        ElasticSearchManageContacts(connString, drDomain("numDomainID"), contactRecords, "Delete")
                        ElasticSearchManageEmails(connString, drDomain("numDomainID"), emailRecords, "Delete")
                    Next
                End If
            End If
        Catch ex As Exception
            BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, 0, 0, 0, Nothing)
            WriteMessage("UpdateElasticSearchData Exception: " & ex.Message)
            'DO NOT THROW EXCEPTION
        End Try
    End Sub

    Private Sub ElasticSearchManageActionItems(ByVal connString As String, ByVal DomainID As Long, ByVal actionItemRecords As DataRow(), ByVal vcType As String)
        Try
            If Not actionItemRecords Is Nothing AndAlso actionItemRecords.Length > 0 Then

                If vcType = "Delete" Then
                    ElasticSearchDeleteRecords(DomainID & "_actionitem", actionItemRecords)
                Else
                    Dim dsActionItems As New DataSet
                    Using connection As New Npgsql.NpgsqlConnection(connString)
                        Dim sqlAdapter As Npgsql.NpgsqlDataAdapter

                        Dim sqlCommand As New Npgsql.NpgsqlCommand
                        sqlCommand.Connection = connection
                        sqlCommand.CommandTimeout = 300
                        sqlCommand.CommandType = CommandType.StoredProcedure
                        sqlCommand.CommandText = "usp_elasticsearch_getactionitems"
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = DomainID})
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_vcactionitemids", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Text, .Value = String.Join(",", actionItemRecords.Select(Function(x) x("numRecordID")))})
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                        Dim i As Int32 = 0
                        connection.Open()
                        Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                            sqlCommand.ExecuteNonQuery()

                            For Each parm As Npgsql.NpgsqlParameter In sqlCommand.Parameters
                                If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                    If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                        Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                        sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                        dsActionItems.Tables.Add(parm.Value.ToString())
                                        sqlAdapter.Fill(dsActionItems.Tables(i))
                                        i += 1
                                    End If
                                End If
                            Next

                            objTransaction.Commit()
                        End Using
                        connection.Close()
                    End Using

                    If Not dsActionItems Is Nothing AndAlso Not dsActionItems.Tables.Count > 0 AndAlso dsActionItems.Tables(0).Rows.Count > 0 Then
                        If vcType = "Insert" Then
                            ElasticSearchInsertRecords(DomainID & "_actionitem", dsActionItems.Tables(0))
                        ElseIf vcType = "Update" Then
                            ElasticSearchUpdateRecords(DomainID & "_actionitem", dsActionItems.Tables(0))
                        End If
                    End If
                End If

            End If
        Catch ex As Exception
            BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, DomainID, 0, 0, Nothing)
            WriteMessage("ElasticSearchManageActionItems Exception: " & ex.Message)
            'DO NOT THROW EXCEPTION
        End Try
    End Sub

    Private Sub ElasticSearchManageBizDocs(ByVal connString As String, ByVal DomainID As Long, ByVal bizDocRecords As DataRow(), ByVal vcType As String)
        Try
            If Not bizDocRecords Is Nothing AndAlso bizDocRecords.Length > 0 Then
                If vcType = "Delete" Then
                    ElasticSearchDeleteRecords(DomainID & "_bizdoc", bizDocRecords)
                Else
                    Dim dsBizDoc As New DataSet
                    Using connection As New Npgsql.NpgsqlConnection(connString)
                        Dim sqlAdapter As Npgsql.NpgsqlDataAdapter

                        Dim sqlCommand As New Npgsql.NpgsqlCommand
                        sqlCommand.Connection = connection
                        sqlCommand.CommandTimeout = 300
                        sqlCommand.CommandType = CommandType.StoredProcedure
                        sqlCommand.CommandText = "usp_elasticsearch_getbizdocs"
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = DomainID})
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_vcoppbizdocids", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Text, .Value = String.Join(",", bizDocRecords.Select(Function(x) x("numRecordID")))})
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                        Dim i As Int32 = 0
                        connection.Open()
                        Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                            sqlCommand.ExecuteNonQuery()

                            For Each parm As Npgsql.NpgsqlParameter In sqlCommand.Parameters
                                If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                    If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                        Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                        sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                        dsBizDoc.Tables.Add(parm.Value.ToString())
                                        sqlAdapter.Fill(dsBizDoc.Tables(i))
                                        i += 1
                                    End If
                                End If
                            Next

                            objTransaction.Commit()
                        End Using
                        connection.Close()
                    End Using

                    If Not dsBizDoc Is Nothing AndAlso dsBizDoc.Tables.Count > 0 AndAlso dsBizDoc.Tables(0).Rows.Count > 0 Then
                        If vcType = "Insert" Then
                            ElasticSearchInsertRecords(DomainID & "_bizdoc", dsBizDoc.Tables(0))
                        ElseIf vcType = "Update" Then
                            ElasticSearchUpdateRecords(DomainID & "_bizdoc", dsBizDoc.Tables(0))
                        End If
                    End If
                End If

            End If
        Catch ex As Exception
            BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, DomainID, 0, 0, Nothing)
            WriteMessage("ElasticSearchManageBizDocs Exception: " & ex.Message)
            'DO NOT THROW EXCEPTION
        End Try
    End Sub

    Private Sub ElasticSearchManageCases(ByVal connString As String, ByVal DomainID As Long, ByVal caseRecords As DataRow(), ByVal vcType As String)
        Try
            If Not caseRecords Is Nothing AndAlso caseRecords.Length > 0 Then
                If vcType = "Delete" Then
                    ElasticSearchDeleteRecords(DomainID & "_case", caseRecords)
                Else
                    Dim dsCase As New DataSet
                    Using connection As New Npgsql.NpgsqlConnection(connString)
                        Dim sqlAdapter As Npgsql.NpgsqlDataAdapter

                        Dim sqlCommand As New Npgsql.NpgsqlCommand
                        sqlCommand.Connection = connection
                        sqlCommand.CommandTimeout = 300
                        sqlCommand.CommandType = CommandType.StoredProcedure
                        sqlCommand.CommandText = "usp_elasticsearch_getcases"
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = DomainID})
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_vccaseids", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Text, .Value = String.Join(",", caseRecords.Select(Function(x) x("numRecordID")))})
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                        Dim i As Int32 = 0
                        connection.Open()
                        Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                            sqlCommand.ExecuteNonQuery()

                            For Each parm As Npgsql.NpgsqlParameter In sqlCommand.Parameters
                                If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                    If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                        Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                        sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                        dsCase.Tables.Add(parm.Value.ToString())
                                        sqlAdapter.Fill(dsCase.Tables(i))
                                        i += 1
                                    End If
                                End If
                            Next

                            objTransaction.Commit()
                        End Using
                        connection.Close()
                    End Using

                    If Not dsCase Is Nothing AndAlso dsCase.Tables.Count > 0 AndAlso dsCase.Tables(0).Rows.Count > 0 Then
                        If vcType = "Insert" Then
                            ElasticSearchInsertRecords(DomainID & "_case", dsCase.Tables(0))
                        ElseIf vcType = "Update" Then
                            ElasticSearchUpdateRecords(DomainID & "_case", dsCase.Tables(0))
                        End If
                    End If
                End If

            End If
        Catch ex As Exception
            BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, DomainID, 0, 0, Nothing)
            WriteMessage("ElasticSearchManageCases Exception: " & ex.Message)
            'DO NOT THROW EXCEPTION
        End Try
    End Sub

    Private Sub ElasticSearchManageKnowledgeBases(ByVal connString As String, ByVal DomainID As Long, ByVal KnowledgeBaseRecords As DataRow(), ByVal vcType As String)
        Try
            If Not KnowledgeBaseRecords Is Nothing AndAlso KnowledgeBaseRecords.Length > 0 Then
                If vcType = "Delete" Then
                    ElasticSearchDeleteRecords(DomainID & "_knowledgebase", KnowledgeBaseRecords)
                Else
                    Dim dsKnowledgeBase As New DataSet
                    Using connection As New Npgsql.NpgsqlConnection(connString)
                        Dim sqlAdapter As Npgsql.NpgsqlDataAdapter

                        Dim sqlCommand As New Npgsql.NpgsqlCommand
                        sqlCommand.Connection = connection
                        sqlCommand.CommandTimeout = 300
                        sqlCommand.CommandType = CommandType.StoredProcedure
                        sqlCommand.CommandText = "usp_elasticsearch_getknowledgebase"
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = DomainID})
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_vcsolnid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Text, .Value = String.Join(",", KnowledgeBaseRecords.Select(Function(x) x("numRecordID")))})
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                        Dim i As Int32 = 0
                        connection.Open()
                        Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                            sqlCommand.ExecuteNonQuery()

                            For Each parm As Npgsql.NpgsqlParameter In sqlCommand.Parameters
                                If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                    If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                        Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                        sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                        dsKnowledgeBase.Tables.Add(parm.Value.ToString())
                                        sqlAdapter.Fill(dsKnowledgeBase.Tables(i))
                                        i += 1
                                    End If
                                End If
                            Next

                            objTransaction.Commit()
                        End Using
                        connection.Close()
                    End Using

                    If Not dsKnowledgeBase Is Nothing AndAlso dsKnowledgeBase.Tables.Count > 0 AndAlso dsKnowledgeBase.Tables(0).Rows.Count > 0 Then
                        If vcType = "Insert" Then
                            ElasticSearchInsertRecords(DomainID & "_knowledgebase", dsKnowledgeBase.Tables(0))
                        ElseIf vcType = "Update" Then
                            ElasticSearchUpdateRecords(DomainID & "_knowledgebase", dsKnowledgeBase.Tables(0))
                        End If
                    End If
                End If

            End If
        Catch ex As Exception
            BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, DomainID, 0, 0, Nothing)
            WriteMessage("ElasticSearchManageKnowledgeBases Exception: " & ex.Message)
            'DO NOT THROW EXCEPTION
        End Try
    End Sub

    Private Sub ElasticSearchManageItems(ByVal connString As String, ByVal DomainID As Long, ByVal itemRecords As DataRow(), ByVal vcType As String)
        Try
            If Not itemRecords Is Nothing AndAlso itemRecords.Length > 0 Then
                If vcType = "Delete" Then
                    ElasticSearchDeleteRecords(DomainID & "_item", itemRecords)
                Else
                    Dim dsItem As New DataSet
                    Dim sqlAdapter As Npgsql.NpgsqlDataAdapter
                    Using connection As New Npgsql.NpgsqlConnection(connString)
                        Dim sqlCommand As New Npgsql.NpgsqlCommand
                        sqlCommand.Connection = connection
                        sqlCommand.CommandType = CommandType.StoredProcedure
                        sqlCommand.CommandText = "usp_elasticsearch_getitems"
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = DomainID})
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_vcitemcodes", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Text, .Value = String.Join(",", itemRecords.Select(Function(x) x("numRecordID")))})
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                        Dim i As Int32 = 0
                        connection.Open()
                        Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                            sqlCommand.ExecuteNonQuery()

                            For Each parm As Npgsql.NpgsqlParameter In sqlCommand.Parameters
                                If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                    If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                        Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                        sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                        dsItem.Tables.Add(parm.Value.ToString())
                                        sqlAdapter.Fill(dsItem.Tables(i))
                                        i += 1
                                    End If
                                End If
                            Next

                            objTransaction.Commit()
                        End Using
                        connection.Close()
                    End Using

                    If Not dsItem Is Nothing AndAlso dsItem.Tables.Count > 0 AndAlso dsItem.Tables(0).Rows.Count > 0 Then
                        If vcType = "Insert" Then
                            ElasticSearchInsertRecords(DomainID & "_item", dsItem.Tables(0))
                        ElseIf vcType = "Update" Then
                            ElasticSearchUpdateRecords(DomainID & "_item", dsItem.Tables(0))
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, DomainID, 0, 0, Nothing)
            WriteMessage("ElasticSearchManageItems Exception: " & ex.Message)
            'DO NOT THROW EXCEPTION
        End Try
    End Sub

    Private Sub ElasticSearchManageOrganizations(ByVal connString As String, ByVal DomainID As Long, ByVal organizationRecords As DataRow(), ByVal vcType As String)
        Try
            If Not organizationRecords Is Nothing AndAlso organizationRecords.Length > 0 Then
                If vcType = "Delete" Then
                    ElasticSearchDeleteRecords(DomainID & "_organization", organizationRecords)
                Else
                    Dim dsOrganization As New DataSet
                    Dim sqlAdapter As Npgsql.NpgsqlDataAdapter
                    Using connection As New Npgsql.NpgsqlConnection(connString)
                        Dim sqlCommand As New Npgsql.NpgsqlCommand
                        sqlCommand.Connection = connection
                        sqlCommand.CommandType = CommandType.StoredProcedure
                        sqlCommand.CommandText = "usp_elasticsearch_getorganizations"
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = DomainID})
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_vccompanyids", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Text, .Value = String.Join(",", organizationRecords.Select(Function(x) x("numRecordID")))})
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})


                        Dim i As Int32 = 0
                        connection.Open()
                        Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                            sqlCommand.ExecuteNonQuery()

                            For Each parm As Npgsql.NpgsqlParameter In sqlCommand.Parameters
                                If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                    If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                        Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                        sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                        dsOrganization.Tables.Add(parm.Value.ToString())
                                        sqlAdapter.Fill(dsOrganization.Tables(i))
                                        i += 1
                                    End If
                                End If
                            Next

                            objTransaction.Commit()
                        End Using
                        connection.Close()
                    End Using

                    If Not dsOrganization Is Nothing AndAlso dsOrganization.Tables.Count > 0 AndAlso dsOrganization.Tables(0).Rows.Count > 0 Then
                        If vcType = "Insert" Then
                            ElasticSearchInsertRecords(DomainID & "_organization", dsOrganization.Tables(0))
                        ElseIf vcType = "Update" Then
                            ElasticSearchUpdateRecords(DomainID & "_organization", dsOrganization.Tables(0))
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, DomainID, 0, 0, Nothing)
            WriteMessage("ElasticSearchManageOrganizations Exception: " & ex.Message)
            'DO NOT THROW EXCEPTION
        End Try
    End Sub

    Private Sub ElasticSearchManageOrders(ByVal connString As String, ByVal DomainID As Long, ByVal orderRecords As DataRow(), ByVal vcType As String)
        Try
            If Not orderRecords Is Nothing AndAlso orderRecords.Length > 0 Then
                If vcType = "Delete" Then
                    ElasticSearchDeleteRecords(DomainID & "_opporder", orderRecords)
                Else
                    Dim dsOrganization As New DataSet
                    Using connection As New Npgsql.NpgsqlConnection(connString)
                        Dim sqlAdapter As Npgsql.NpgsqlDataAdapter

                        Dim sqlCommand As New Npgsql.NpgsqlCommand
                        sqlCommand.Connection = connection
                        sqlCommand.CommandTimeout = 300
                        sqlCommand.CommandType = CommandType.StoredProcedure
                        sqlCommand.CommandText = "usp_elasticsearch_getopporders"
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = DomainID})
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_vcoppids", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Text, .Value = String.Join(",", orderRecords.Select(Function(x) x("numRecordID")))})
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                        Dim i As Int32 = 0
                        connection.Open()
                        Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                            sqlCommand.ExecuteNonQuery()

                            For Each parm As Npgsql.NpgsqlParameter In sqlCommand.Parameters
                                If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                    If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                        Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                        sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                        dsOrganization.Tables.Add(parm.Value.ToString())
                                        sqlAdapter.Fill(dsOrganization.Tables(i))
                                        i += 1
                                    End If
                                End If
                            Next

                            objTransaction.Commit()
                        End Using
                        connection.Close()
                    End Using

                    If Not dsOrganization Is Nothing AndAlso dsOrganization.Tables.Count > 0 AndAlso dsOrganization.Tables(0).Rows.Count > 0 Then
                        If vcType = "Insert" Then
                            ElasticSearchInsertRecords(DomainID & "_opporder", dsOrganization.Tables(0))
                        ElseIf vcType = "Update" Then
                            ElasticSearchUpdateRecords(DomainID & "_opporder", dsOrganization.Tables(0))
                        End If
                    End If
                End If

            End If
        Catch ex As Exception
            BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, DomainID, 0, 0, Nothing)
            WriteMessage("ElasticSearchManageOrders Exception: " & ex.Message)
            'DO NOT THROW EXCEPTION
        End Try
    End Sub

    Private Sub ElasticSearchManageProjects(ByVal connString As String, ByVal DomainID As Long, ByVal projectRecords As DataRow(), ByVal vcType As String)
        Try
            If Not projectRecords Is Nothing AndAlso projectRecords.Length > 0 Then
                If vcType = "Delete" Then
                    ElasticSearchDeleteRecords(DomainID & "_project", projectRecords)
                Else
                    Dim dsProject As New DataSet
                    Using connection As New Npgsql.NpgsqlConnection(connString)
                        Dim sqlAdapter As Npgsql.NpgsqlDataAdapter

                        Dim sqlCommand As New Npgsql.NpgsqlCommand
                        sqlCommand.Connection = connection
                        sqlCommand.CommandTimeout = 300
                        sqlCommand.CommandType = CommandType.StoredProcedure
                        sqlCommand.CommandText = "usp_elasticsearch_getprojects"
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = DomainID})
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_vcprojectids", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Text, .Value = String.Join(",", projectRecords.Select(Function(x) x("numRecordID")))})
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                        Dim i As Int32 = 0
                        connection.Open()
                        Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                            sqlCommand.ExecuteNonQuery()

                            For Each parm As Npgsql.NpgsqlParameter In sqlCommand.Parameters
                                If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                    If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                        Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                        sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                        dsProject.Tables.Add(parm.Value.ToString())
                                        sqlAdapter.Fill(dsProject.Tables(i))
                                        i += 1
                                    End If
                                End If
                            Next

                            objTransaction.Commit()
                        End Using
                        connection.Close()
                    End Using

                    If Not dsProject Is Nothing AndAlso dsProject.Tables.Count > 0 AndAlso dsProject.Tables(0).Rows.Count > 0 Then
                        If vcType = "Insert" Then
                            ElasticSearchInsertRecords(DomainID & "_project", dsProject.Tables(0))
                        ElseIf vcType = "Update" Then
                            ElasticSearchUpdateRecords(DomainID & "_project", dsProject.Tables(0))
                        End If
                    End If
                End If

            End If
        Catch ex As Exception
            BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, DomainID, 0, 0, Nothing)
            WriteMessage("ElasticSearchManageProjects Exception: " & ex.Message)
            'DO NOT THROW EXCEPTION
        End Try
    End Sub

    Private Sub ElasticSearchManageReturns(ByVal connString As String, ByVal DomainID As Long, ByVal returnRecords As DataRow(), ByVal vcType As String)
        Try
            If Not returnRecords Is Nothing AndAlso returnRecords.Length > 0 Then
                If vcType = "Delete" Then
                    ElasticSearchDeleteRecords(DomainID & "_return", returnRecords)
                Else
                    Dim dsReturn As New DataSet
                    Using connection As New Npgsql.NpgsqlConnection(connString)
                        Dim sqlAdapter As Npgsql.NpgsqlDataAdapter

                        Dim sqlCommand As New Npgsql.NpgsqlCommand
                        sqlCommand.Connection = connection
                        sqlCommand.CommandTimeout = 300
                        sqlCommand.CommandType = CommandType.StoredProcedure
                        sqlCommand.CommandText = "usp_elasticsearch_getreturns"
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = DomainID})
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_vcreturnheaderids", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Text, .Value = String.Join(",", returnRecords.Select(Function(x) x("numRecordID")))})
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                        Dim i As Int32 = 0
                        connection.Open()
                        Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                            sqlCommand.ExecuteNonQuery()

                            For Each parm As Npgsql.NpgsqlParameter In sqlCommand.Parameters
                                If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                    If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                        Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                        sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                        dsReturn.Tables.Add(parm.Value.ToString())
                                        sqlAdapter.Fill(dsReturn.Tables(i))
                                        i += 1
                                    End If
                                End If
                            Next

                            objTransaction.Commit()
                        End Using
                        connection.Close()
                    End Using

                    If Not dsReturn Is Nothing AndAlso dsReturn.Tables.Count > 0 AndAlso dsReturn.Tables(0).Rows.Count > 0 Then
                        If vcType = "Insert" Then
                            ElasticSearchInsertRecords(DomainID & "_return", dsReturn.Tables(0))
                        ElseIf vcType = "Update" Then
                            ElasticSearchUpdateRecords(DomainID & "_return", dsReturn.Tables(0))
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, DomainID, 0, 0, Nothing)
            WriteMessage("ElasticSearchManageReturns Exception: " & ex.Message)
            'DO NOT THROW EXCEPTION
        End Try
    End Sub

    Private Sub ElasticSearchManageContacts(ByVal connString As String, ByVal DomainID As Long, ByVal contactRecords As DataRow(), ByVal vcType As String)
        Try
            If Not contactRecords Is Nothing AndAlso contactRecords.Length > 0 Then
                If vcType = "Delete" Then
                    ElasticSearchDeleteRecords(DomainID & "_contact", contactRecords)
                Else
                    Dim dsContact As New DataSet
                    Using connection As New Npgsql.NpgsqlConnection(connString)
                        Dim sqlAdapter As Npgsql.NpgsqlDataAdapter

                        Dim sqlCommand As New Npgsql.NpgsqlCommand
                        sqlCommand.Connection = connection
                        sqlCommand.CommandTimeout = 300
                        sqlCommand.CommandType = CommandType.StoredProcedure
                        sqlCommand.CommandText = "usp_elasticsearch_getcontacts"
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = DomainID})
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_vccontactids", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Text, .Value = String.Join(",", contactRecords.Select(Function(x) x("numRecordID")))})
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                        Dim i As Int32 = 0
                        connection.Open()
                        Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                            sqlCommand.ExecuteNonQuery()

                            For Each parm As Npgsql.NpgsqlParameter In sqlCommand.Parameters
                                If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                    If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                        Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                        sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                        dsContact.Tables.Add(parm.Value.ToString())
                                        sqlAdapter.Fill(dsContact.Tables(i))
                                        i += 1
                                    End If
                                End If
                            Next

                            objTransaction.Commit()
                        End Using
                        connection.Close()
                    End Using

                    If Not dsContact Is Nothing AndAlso dsContact.Tables.Count > 0 AndAlso dsContact.Tables(0).Rows.Count > 0 Then
                        If vcType = "Insert" Then
                            ElasticSearchInsertRecords(DomainID & "_contact", dsContact.Tables(0))
                        ElseIf vcType = "Update" Then
                            ElasticSearchUpdateRecords(DomainID & "_contact", dsContact.Tables(0))
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, DomainID, 0, 0, Nothing)
            WriteMessage("ElasticSearchManageContacts Exception: " & ex.Message)
            'DO NOT THROW EXCEPTION
        End Try
    End Sub

    Private Sub ElasticSearchManageEmails(ByVal connString As String, ByVal DomainID As Long, ByVal emailRecords As DataRow(), ByVal vcType As String)
        Try
            If Not emailRecords Is Nothing AndAlso emailRecords.Length > 0 Then
                If vcType = "Delete" Then
                    ElasticSearchDeleteRecords(DomainID & "_email", emailRecords)
                Else
                    Dim dsEmail As New DataSet
                    Using connection As New Npgsql.NpgsqlConnection(connString)
                        Dim sqlCommand As New Npgsql.NpgsqlCommand
                        sqlCommand.Connection = connection
                        sqlCommand.CommandType = CommandType.StoredProcedure
                        sqlCommand.CommandText = "usp_elasticsearch_getemails"
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = DomainID})
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numcurrentpage", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, .Value = 0})
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numpagesize", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, .Value = 0})
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_vcemailhistoryids", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Text, .Value = String.Join(",", emailRecords.Select(Function(x) x("numRecordID")))})
                        sqlCommand.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "swv_refcur", .Direction = ParameterDirection.InputOutput, .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, .Value = DBNull.Value})

                        Dim i As Int32 = 0
                        Dim sqlAdapter As Npgsql.NpgsqlDataAdapter
                        connection.Open()
                        Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                            sqlCommand.ExecuteNonQuery()

                            For Each parm As Npgsql.NpgsqlParameter In sqlCommand.Parameters
                                If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                    If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                        Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                        sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                        dsEmail.Tables.Add(parm.Value.ToString())
                                        sqlAdapter.Fill(dsEmail.Tables(i))
                                        i += 1
                                    End If
                                End If
                            Next

                            objTransaction.Commit()
                        End Using
                        connection.Close()
                    End Using

                    If Not dsEmail Is Nothing AndAlso dsEmail.Tables.Count > 0 AndAlso dsEmail.Tables(0).Rows.Count > 0 Then
                        If vcType = "Insert" Then
                            ElasticSearchInsertRecords(DomainID & "_email", dsEmail.Tables(0))
                        ElseIf vcType = "Update" Then
                            ElasticSearchUpdateRecords(DomainID & "_email", dsEmail.Tables(0))
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, DomainID, 0, 0, Nothing)
            WriteMessage("ElasticSearchManageEmails Exception: " & ex.Message)
            'DO NOT THROW EXCEPTION
        End Try
    End Sub



    Private Sub ElasticSearchInsertRecords(ByVal indexName As String, ByVal dt As DataTable)
        Try
            Dim settings As New Nest.ConnectionSettings(New Uri(ConfigurationManager.AppSettings("ElasticSearchURL")))
            Dim client As New ElasticClient(settings)
            Dim isValid As Boolean = False
            Dim result As IExistsResponse = client.IndexExists(New IndexExistsRequest(Indices.Parse(indexName)))

            If Not result.Exists Then
                Dim createIndexResponse As Nest.ICreateIndexResponse
                If indexName.Contains("_email") Then
                    createIndexResponse = client.CreateIndex(indexName, Function(c) c.Settings(Function(s) s.NumberOfShards(1).NumberOfReplicas(0).Analysis(Function(a) a.Analyzers(Function(anl) anl.Custom("default", Function(ca) ca.Tokenizer("keyword").Filters(New List(Of String)() From {"lowercase"}))))).Mappings(Function(ms) ms.Map(Of Object)(Function(m) m.DynamicTemplates(Function(x) x.DynamicTemplate("test", Function(t) t.Match("Search_*").Mapping(Function(mp) mp.Text(Function(tx) tx.Index(True))))).Properties(Function(p) p.Number(Function(s) s.Name("id").Index(False)).Text(Function(s) s.Name("displaytext").Index(False)).Text(Function(s) s.Name("text").Index(False)).Text(Function(s) s.Name("url").Index(False)).Date(Function(d) d.Name("SearchDate_dtReceivedOn"))))))
                Else
                    createIndexResponse = client.CreateIndex(indexName, Function(c) c.Settings(Function(s) s.NumberOfShards(1).NumberOfReplicas(0).Analysis(Function(a) a.Analyzers(Function(anl) anl.Custom("default", Function(ca) ca.Tokenizer("keyword").Filters(New List(Of String)() From {"lowercase"}))))).Mappings(Function(ms) ms.Map(Of Object)(Function(m) m.DynamicTemplates(Function(x) x.DynamicTemplate("test", Function(t) t.Match("Search_*").Mapping(Function(mp) mp.Text(Function(tx) tx.Index(True))))).Properties(Function(p) p.Number(Function(s) s.Name("id").Index(False)).Text(Function(s) s.Name("displaytext").Index(False)).Text(Function(s) s.Name("text").Index(False)).Text(Function(s) s.Name("url").Index(False))))))
                End If

                'Dim createIndexResponse As Nest.ICreateIndexResponse = client.CreateIndex(indexName, Function(c) c.Settings(Function(s) s.NumberOfShards(1).NumberOfReplicas(0).Analysis(Function(a) a.Analyzers(Function(anl) anl.Custom("default", Function(ca) ca.Tokenizer("keyword").Filters(New List(Of String)() From {"lowercase"}))))).Mappings(Function(ms) ms.Map(Of Object)(Function(m) m.Properties(Function(p) p.Number(Function(s) s.Name("id").Index(False)).Text(Function(s) s.Name("displaytext").Index(False)).Text(Function(s) s.Name("text").Index(False)).Text(Function(s) s.Name("url").Index(False))))))
                isValid = createIndexResponse.IsValid
            Else
                isValid = True
            End If

            If isValid Then
                Dim descriptor As New BulkDescriptor()
                For Each drItem As DataRow In dt.Rows
                    descriptor.Index(Of Object)(Function(op) op.Index(String.Concat(indexName)).Document(DataRowToJson(drItem)).Id(drItem("id").ToString()))
                Next
                Dim res = client.Bulk(descriptor)
            End If
        Catch ex As Exception
            BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, indexName.Split("_")(0), 0, 0, Nothing)
            WriteMessage("ElasticSearchInsertRecords Exception: " & ex.Message)
            'DO NOT THROW EXCEPTION
        End Try
    End Sub

    Private Sub ElasticSearchUpdateRecords(ByVal indexName As String, ByVal dt As DataTable)
        Try
            Dim settings As New Nest.ConnectionSettings(New Uri(ConfigurationManager.AppSettings("ElasticSearchURL")))
            Dim client As New ElasticClient(settings)

            Dim result As IExistsResponse = client.IndexExists(New IndexExistsRequest(Indices.Parse(indexName)))

            If result.Exists Then
                For Each drItem As DataRow In dt.Rows
                    Dim updateResponse As IUpdateResponse(Of Object) = client.Update(Of Object)(DocumentPath(Of Object).Id(drItem("id").ToString()), Function(u) u.Index(indexName).DocAsUpsert(True).Doc(DataRowToJson(drItem)))
                Next
            End If
        Catch ex As Exception
            BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, indexName.Split("_")(0), 0, 0, Nothing)
            WriteMessage("ElasticSearchUpdateRecords Exception: " & ex.Message)
            'DO NOT THROW EXCEPTION
        End Try
    End Sub

    Private Sub ElasticSearchDeleteRecords(ByVal indexName As String, ByVal arrRows As DataRow())
        Try
            Dim settings As New Nest.ConnectionSettings(New Uri(ConfigurationManager.AppSettings("ElasticSearchURL")))
            Dim client As New ElasticClient(settings)

            Dim result As IExistsResponse = client.IndexExists(New IndexExistsRequest(Indices.Parse(indexName)))

            If result.Exists Then
                For Each drItem As DataRow In arrRows
                    Dim response As IDeleteResponse = client.Delete(Of Object)(drItem("numRecordID").ToString(), Function(d) d.Index(indexName))
                Next
            End If
        Catch ex As Exception
            BACRM.BusinessLogic.Common.ExceptionModule.ExceptionPublish(ex, indexName.Split("_")(0), 0, 0, Nothing)
            WriteMessage("ElasticSearchDeleteRecords Exception: " & ex.Message)
            'DO NOT THROW EXCEPTION
        End Try
    End Sub

    Private Function DataRowToJson(datarow As DataRow) As Object
        Try
            Dim dict As New Dictionary(Of String, Object)()

            For Each col As DataColumn In datarow.Table.Columns
                dict.Add(col.ColumnName, datarow(col))
            Next

            Dim json As String = JsonConvert.SerializeObject(dict, Formatting.None, New JsonSerializerSettings() With {.ReferenceLoopHandling = ReferenceLoopHandling.Ignore})

            Return JsonConvert.DeserializeObject(Of Object)(json)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Shared Sub WriteMessage(ByVal Message As String)
        Try
            BACRM.BusinessLogic.WebAPI.WebAPI.WriteMessage(Message, System.Configuration.ConfigurationManager.AppSettings("LogFilePath"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class
