Imports System.Data.SqlClient
Imports System.Configuration
Imports System
Imports System.Xml
Imports System.IO
Imports System.Threading
Imports System.Threading.Tasks
Imports BACRM.BusinessLogic.WebAPI
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting
Imports System.Array
Imports BACRM.BusinessLogic.Opportunities

Public Class Service1
    Private Shared m_closingEvent As New ManualResetEvent(False)
    Private Shared m_tasksList As New List(Of Task)()
    Private Const M_POLL_TIME As Integer = 60000
    Dim tokenSource As New CancellationTokenSource()
    Dim cancellationToken As CancellationToken = tokenSource.Token
    Dim scheduler As TaskScheduler = TaskScheduler.Default

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        Try
            'Debugger.Launch()
            ServiceStart()
        Catch ex As Exception
            WriteMessage(ex.Message)
        End Try
    End Sub

    Protected Function ReadAppSettingInterval(ByVal interval As Double, ByVal intbyte As Integer) As Double
        '   Dim interval As Double = 600000, tempInterval 'initialize to ten minutes.


        Dim intervalMin As String = String.Empty
        If intbyte = 1 Then
            intervalMin = System.Configuration.ConfigurationManager.AppSettings("Timer1")
        ElseIf intbyte = 2 Then
            intervalMin = System.Configuration.ConfigurationManager.AppSettings("Timer2")
        End If

        If IsNumeric(intervalMin) Then
            interval = Convert.ToDouble(Convert.ToInt32(intervalMin) * 60000)
        End If

        Return interval
    End Function

    Protected Overrides Sub OnStop()
        RequestAdditionalTime(TimeSpan.FromSeconds(60).Milliseconds)

        tokenSource.Cancel()
        m_closingEvent.Set()
        If m_tasksList.Count > 0 Then
            Dim arTasks As Task() = m_tasksList.ToArray()
            Task.WaitAll(arTasks, Timeout.Infinite)
            m_tasksList.Clear()
        End If
    End Sub

    Protected Overrides Sub OnContinue()
        ServiceStart()
    End Sub

    Protected Overrides Sub OnPause()
        RequestAdditionalTime(TimeSpan.FromSeconds(60).Milliseconds)
        tokenSource.Cancel()
        m_closingEvent.Set()
        If m_tasksList.Count > 0 Then
            Dim arTasks As Task() = m_tasksList.ToArray()
            Task.WaitAll(arTasks, Timeout.Infinite)
            m_tasksList.Clear()
        End If

    End Sub

    Private Sub ServiceStart()
        Try
            m_closingEvent.Reset()

            Dim tskRecurringEvents As Task = Task.Factory.StartNew(New Action(AddressOf RecurringEvents), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskRecurringEvents)

            Dim tskActionItemEmail As Task = Task.Factory.StartNew(New Action(AddressOf ActionItemEmail), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskActionItemEmail)

            'Not IN USE NOW
            'Dim tskCularisWakeup As Task = Task.Factory.StartNew(New Action(AddressOf CularisWakeup), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            'm_tasksList.Add(tskCularisWakeup)

            Dim tskECampaignWakeup As Task = Task.Factory.StartNew(New Action(AddressOf ECampaignWakeup), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskECampaignWakeup)

            Dim tskGetNewMailsWakeup As Task = Task.Factory.StartNew(New Action(AddressOf GetNewMailsWakeup), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskGetNewMailsWakeup)

            'DO NOT UNCOMMENT FOLLOWING METHOD. ITS FECHING OLD EMAILS
            'Dim tskGetOldMailsWakeup As Task = Task.Factory.StartNew(New Action(AddressOf GetOldMailWakeUp), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            'm_tasksList.Add(tskGetOldMailsWakeup)

            Dim tskSynchronizeDeletedMailWakeup As Task = Task.Factory.StartNew(New Action(AddressOf SynchronizeDeletedMailWakeup), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskSynchronizeDeletedMailWakeup)

            Dim tskGoogleToBizConact As Task = Task.Factory.StartNew(New Action(AddressOf GoogleToBizConact), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskGoogleToBizConact)

            Dim tskGoogleToBizCalendar As Task = Task.Factory.StartNew(New Action(AddressOf GoogleToBizCalendar), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskGoogleToBizCalendar)

            Dim tskBizToGoogleCalendar As Task = Task.Factory.StartNew(New Action(AddressOf BizToGoogleCalendar), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskBizToGoogleCalendar)

            Dim tskUpdateLuceneIndex As Task = Task.Factory.StartNew(New Action(AddressOf UpdateLuceneIndex), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskUpdateLuceneIndex)

            Dim tskUpdateDealAmount As Task = Task.Factory.StartNew(New Action(AddressOf UpdateDealAmountCall), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskUpdateDealAmount)

            Dim tskUpdateBankAccountBalance As Task = Task.Factory.StartNew(New Action(AddressOf UpdateBankAccountBalance), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskUpdateBankAccountBalance)

            Dim tskImport As Task = Task.Factory.StartNew(New Action(AddressOf ImportData), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskImport)

            Dim tskRollback As Task = Task.Factory.StartNew(New Action(AddressOf RollBackImportData), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskRollback)

            Dim tskOpportunityAutomationTimer As Task = Task.Factory.StartNew(New Action(AddressOf OpportunityAutomationExecution), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskOpportunityAutomationTimer)

            Dim tskUpdateCaseStatus As Task = Task.Factory.StartNew(New Action(AddressOf UpdateCaseStatus), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskUpdateCaseStatus)

            Dim tskWFExecutionTimer As Task = Task.Factory.StartNew(New Action(AddressOf WFExecution), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskWFExecutionTimer)

            Dim tskBizRecurrenceTimer As Task = Task.Factory.StartNew(New Action(AddressOf ExecuteBizRecurrence), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskBizRecurrenceTimer)

            Dim tskCustomReportEmail As Task = Task.Factory.StartNew(New Action(AddressOf EmailCustomQueryReport), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskCustomReportEmail)

            Dim tskGetCurrencyConversionRate As Task = Task.Factory.StartNew(New Action(AddressOf GetCurrencyConversionRate), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskGetCurrencyConversionRate)

            Dim tskMassSalesOrder As Task = Task.Factory.StartNew(New Action(AddressOf BulkOrderSalesExecution), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskMassSalesOrder)

            Dim tskUpdateRecordHistory As Task = Task.Factory.StartNew(New Action(AddressOf UpdateRecordHistory), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskUpdateRecordHistory)

            Dim tskUpdateElasticSearchData As Task = Task.Factory.StartNew(New Action(AddressOf UpdateElasticSearchData), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskUpdateElasticSearchData)

            Dim tskElasticSearchReindexData As Task = Task.Factory.StartNew(New Action(AddressOf ElasticSearchReindexData), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskElasticSearchReindexData)

            Dim tskUpdateElasticBizCartSearchData As Task = Task.Factory.StartNew(New Action(AddressOf UpdateBizCartElasticSearchData), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskUpdateElasticBizCartSearchData)

            Dim tskBizCartElasticSearchWipeOutOldData As Task = Task.Factory.StartNew(New Action(AddressOf BizCartElasticSearchWipeOutOldData), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskBizCartElasticSearchWipeOutOldData)

            Dim tskDashboardCommissionReportCalculation As Task = Task.Factory.StartNew(New Action(AddressOf DashboardCommissionReportCalculation), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskDashboardCommissionReportCalculation)

            Dim tskEDIQueue As Task = Task.Factory.StartNew(New Action(AddressOf ProcessEDIQueue), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskEDIQueue)

            Dim tskWFDateFields As Task = Task.Factory.StartNew(New Action(AddressOf WFDateFieldsDataSync), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskWFDateFields)

            Dim tskOrderShippingTracking As Task = Task.Factory.StartNew(New Action(AddressOf OrderShippingTracking), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskOrderShippingTracking)

            Dim tskAutoCalculateOrderShippingRate As Task = Task.Factory.StartNew(New Action(AddressOf AutoCalculateOrderShippingRate), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskAutoCalculateOrderShippingRate)

            Dim tskScheduledReportsGroupDataCache As Task = Task.Factory.StartNew(New Action(AddressOf ScheduledReportsGroupDataCache), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskScheduledReportsGroupDataCache)

            Dim tskEmailScheduledReportsGroup As Task = Task.Factory.StartNew(New Action(AddressOf EmailScheduledReportsGroup), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskEmailScheduledReportsGroup)

            Dim tskDashboardReportDataCache As Task = Task.Factory.StartNew(New Action(AddressOf DashboardReportDataCache), cancellationToken, TaskCreationOptions.PreferFairness, scheduler)
            m_tasksList.Add(tskDashboardReportDataCache)

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub RecurringEvents()
        Try
            Dim recurringEvents = ConfigurationManager.AppSettings("RecurringEvents")
            Dim WaitTime As Integer = M_POLL_TIME * recurringEvents   'Take value from Config file 

            While Not m_closingEvent.WaitOne(WaitTime)
                Try
                    Dim objRecurring As New RecurringEvents
                    WriteMessage("-------------Recurring Events Starts ------------" + DateTime.Now)
                    objRecurring.CreateRecurringBizDocs()
                    WriteMessage("-------------Recurring Events Ends ------------" + DateTime.Now)
                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString())
                End Try
            End While
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString())
        End Try
    End Sub

    Private Sub ActionItemEmail()
        Try
            Dim actionItemEmailEvents = ConfigurationManager.AppSettings("ActionItemEmail")
            Dim WaitTime As Integer = M_POLL_TIME * actionItemEmailEvents   'Take value from Config file 

            While Not m_closingEvent.WaitOne(WaitTime)
                Try
                    Dim objAlert As New ScheduledAlert
                    Dim result As Boolean
                    result = objAlert.ActItemsMail()
                    WriteMessage("ActItemsMail Done @ " + DateTime.Now)
                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString())
                End Try
            End While
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString())
        End Try
    End Sub

    Dim strToken As String = String.Empty
    Dim strOrders As String = String.Empty

    'NOT IN USE NOW
    'Private Sub CularisWakeup()
    '    Try
    '        Dim cularisWakeupTime = ConfigurationManager.AppSettings("CularisWakeupTime")
    '        Dim WaitTime As Integer = M_POLL_TIME * cularisWakeupTime   'Take value from Config file 

    '        While Not m_closingEvent.WaitOne(WaitTime)
    '            Try
    '                Dim objAPI As New WebAPI
    '                Dim dtAPI As DataTable = objAPI.GetWebApi()
    '                Dim client As CularisClient

    '                For Each dr As DataRow In dtAPI.Rows
    '                    If dr("bitEnableAPI") = True Then
    '                        strToken = dr("vcFourthFldValue").ToString()
    '                        If strToken.Length > 0 Then
    '                            client = New CularisClient
    '                            With client
    '                                .APIURL = dr("vcFirstFldValue").ToString()
    '                                .Token = strToken
    '                                .SectionKey = dr("vcThirdFldValue").ToString()
    '                                .DomainID = dr("numDomainId").ToString()
    '                                .WebApiId = dr("WebApiId").ToString()
    '                                If dr("vcTenthFldValue").ToString().Length > 0 Then
    '                                    .DateCreated = CDate(dr("vcTenthFldValue"))
    '                                Else
    '                                    .DateCreated = "" 'get all products for first time 
    '                                End If
    '                                .DateModified = ""
    '                                .AddLatestProducts() 'Add Newly Created Products
    '                                .DateCreated = ""
    '                                If dr("vcTenthFldValue").ToString().Length > 0 Then
    '                                    .DateModified = CDate(dr("vcTenthFldValue"))
    '                                Else
    '                                    .DateModified = ""
    '                                End If
    '                                .AddLatestProducts() ' add modified products 

    '                                ''Get Orders and Create Sales Order in Biz
    '                                .order_id = dr("LastInsertedOrderID").ToString()
    '                                strOrders = .GetOrders()
    '                                .SaveOrders(strOrders)
    '                            End With
    '                        End If
    '                    End If
    '                Next
    '            Catch ex As Exception
    '                WriteMessage(ex.Message)
    '                WriteMessage(ex.StackTrace.ToString())
    '            End Try
    '        End While
    '    Catch ex As Exception
    '        WriteMessage(ex.Message)
    '        WriteMessage(ex.StackTrace.ToString())
    '    End Try
    'End Sub

    Private Shared Sub WriteMessage(ByVal Message As String)
        Try
            WebAPI.WriteMessage(Message, System.Configuration.ConfigurationManager.AppSettings("LogFilePath"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ECampaignWakeup()
        Try
            Dim eCampaignWakeupTime = ConfigurationManager.AppSettings("ECampaignWakeupTime")
            Dim WaitTime As Integer = M_POLL_TIME * eCampaignWakeupTime   'Take value from Config file 

            While Not m_closingEvent.WaitOne(WaitTime)
                Try
                    Dim objAlert As New ScheduledAlert
                    WriteMessage("Sending ECampaign Mails to Queue")
                    objAlert.SendECampaignMailsAmazon()
                    objAlert.CreateECampaignActionItems()
                    objAlert.ECampaignUpdateFollowUp()
                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString())
                End Try
            End While
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString())
        End Try
    End Sub

    Private Sub GetOldMailWakeUp()
        Try
            Dim getOldMailsWakeupTime = ConfigurationManager.AppSettings("GetOldMailsWakeupTime")
            Dim WaitTime As Integer = M_POLL_TIME * getOldMailsWakeupTime   'Take value from Config file 

            While Not m_closingEvent.WaitOne(WaitTime)
                Try
                    Dim objAlert As New ScheduledAlert
                    Try
                        WriteMessage("Email Archive Process Started.")
                        objAlert.ArchiveOldEmails()
                    Catch ex As Exception
                        WriteMessage(ex.Message)
                        WriteMessage(ex.StackTrace.ToString())
                    Finally
                        WriteMessage("Email Archive Process Finished.")
                    End Try

                    WriteMessage("Fetching IMAP Enabled User Details")

                    objAlert.GetOldMails()
                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString())
                End Try
            End While
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString())
        End Try
    End Sub

    Private Sub GetNewMailsWakeup()
        Try
            Dim getNewMailsWakeupTime = CCommon.ToInteger(ConfigurationManager.AppSettings("GetNewMailsWakeupTime"))
            Dim WaitTime As Integer = M_POLL_TIME * getNewMailsWakeupTime   'Take value from Config file 

            Do
                Try
                    Dim objAlert As New ScheduledAlert
                    Try
                        WriteMessage("Email Archive Process Started.")
                        objAlert.ArchiveOldEmails()
                    Catch ex As Exception
                        WriteMessage(ex.Message)
                        WriteMessage(ex.StackTrace.ToString())
                    Finally
                        WriteMessage("Email Archive Process Finished.")
                    End Try

                    WriteMessage("Fetching IMAP Enabled User Details")
                    objAlert.GetNewMails()
                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString())
                End Try
            Loop While Not m_closingEvent.WaitOne(WaitTime)
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString())
        End Try
    End Sub

    Private Sub SynchronizeDeletedMailWakeup()
        Try
            Dim getNewMailsWakeupTime = ConfigurationManager.AppSettings("SynchronizeDeletedMailWakeupTime")
            Dim WaitTime As Integer = M_POLL_TIME * getNewMailsWakeupTime   'Take value from Config file 

            While Not m_closingEvent.WaitOne(WaitTime)
                Try
                    Dim objAlert As New ScheduledAlert
                    WriteMessage("Started Synchronizing Inbox with Email Account")
                    objAlert.SyncDeletedMails(System.Configuration.ConfigurationManager.AppSettings("SynchronizeLastDeletedMailCount"))
                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString())
                End Try
            End While
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString())
        End Try
    End Sub

    Private Sub GoogleToBizConact()
        Try
            Dim googleContactsSync = ConfigurationManager.AppSettings("GoogleContactsSync")
            Dim WaitTime As Integer = M_POLL_TIME * googleContactsSync   'Take value from Config file 

            While Not m_closingEvent.WaitOne(WaitTime)
                Try
                    Dim objAlert As New ScheduledAlert
                    objAlert.GoogleToBizConact()
                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString())
                End Try
            End While
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString())
        End Try
    End Sub

    Private Sub GoogleToBizCalendar()
        Try
            Dim googleCalendarSync = ConfigurationManager.AppSettings("GoogleCalendarSync")
            Dim WaitTime As Integer = M_POLL_TIME * googleCalendarSync   'Take value from Config file 

            Do
                Try
                    Dim objAlert As New ScheduledAlert
                    objAlert.GoogleToBizCalendar()
                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString())
                End Try
            Loop While Not m_closingEvent.WaitOne(WaitTime)
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString())
        End Try
    End Sub

    Private Sub BizToGoogleCalendar()
        Try
            Dim googleCalendarSync = ConfigurationManager.AppSettings("GoogleCalendarSync")
            Dim WaitTime As Integer = M_POLL_TIME * googleCalendarSync   'Take value from Config file 

            While Not m_closingEvent.WaitOne(WaitTime)
                Try
                    Dim objAlert As New ScheduledAlert
                    objAlert.BizToGoogleCalendar()
                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString())
                End Try
            End While
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString())
        End Try
    End Sub

    Private Sub UpdateLuceneIndex()
        Try
            Dim updateLuceneIndex = ConfigurationManager.AppSettings("UpdateLuceneIndex")
            Dim WaitTime As Integer = M_POLL_TIME * updateLuceneIndex   'Take value from Config file 

            While Not m_closingEvent.WaitOne(WaitTime)
                Try
                    Dim objSearch As New CSearch()
                    Dim dt As DataTable

                    Dim dtSites As DataTable = BACRM.BusinessLogic.ShioppingCart.Sites.GetSitesAll()
                    For Each dr As DataRow In dtSites.Rows

                        objSearch.DomainID = CCommon.ToLong(dr("numDomainID"))
                        objSearch.SiteID = CCommon.ToLong(dr("numSiteID"))
                        objSearch.byteMode = 0
                        If Not Directory.Exists(CCommon.GetLuceneIndexPath(objSearch.DomainID, objSearch.SiteID)) Then
                            objSearch.AddUpdateMode = 1
                            dt = objSearch.GetDataForIndexing()
                            If dt.Rows.Count > 0 Then
                                SearchLucene.IndexData(CCommon.GetLuceneIndexPath(objSearch.DomainID, objSearch.SiteID), dt, IndexType.Items)
                                WriteMessage("Index Created sucessfully for DomainID" & objSearch.DomainID & " SiteID:" & objSearch.SiteID)
                            End If

                        Else
                            Try
                                Dim strErrorMessage As String = ""
                                SearchLucene.ReIndexAllWebsiteOfDomain(objSearch.DomainID, objSearch.SiteID, strErrorMessage)

                                If strErrorMessage.Length > 0 Then
                                    Throw New Exception("Error occured while building index for DomainID" & objSearch.DomainID & " SiteID:" & objSearch.SiteID)
                                End If
                            Catch ex As Exception
                                WriteMessage(ex.Message)
                                WriteMessage(ex.StackTrace.ToString())
                            End Try
                        End If
                    Next
                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString())
                End Try
            End While
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString())
        End Try
    End Sub

    'Private Sub DataImportUtilityTimer_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles DataImportUtilityTimer.Elapsed
    '    Try
    '        Dim objImport As New DataImporterClass
    '        objImport.StartImport()
    '    Catch ex As Exception
    '        WriteMessage(ex.Message)
    '        WriteMessage(ex.StackTrace.ToString())
    '    End Try
    'End Sub

    'Private Sub DataRollBackUtilityTimer_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles DataRollBackUtilityTimer.Elapsed
    '    Try
    '        Dim objImport As New DataImporterClass
    '        objImport.RollBackImportData()
    '    Catch ex As Exception
    '        WriteMessage(ex.Message)
    '        WriteMessage(ex.StackTrace.ToString)
    '    End Try
    'End Sub       

    Private Sub ImportData()
        Try
            Dim ImportTime = ConfigurationManager.AppSettings("DataImportUtility")
            Dim WaitTime As Integer = M_POLL_TIME * ImportTime   'Take value from Config file 

            While Not m_closingEvent.WaitOne(WaitTime)

                Try
                    Dim objImport As New DataImporterClass
                    objImport.StartImport()

                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString)
                End Try

            End While
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString)
        End Try
    End Sub

    Private Sub RollBackImportData()
        Try
            Dim Rollbacktime = ConfigurationManager.AppSettings("DataRollBackUtility")
            Dim WaitTime As Integer = M_POLL_TIME * Rollbacktime   'Take value from Config file 

            While Not m_closingEvent.WaitOne(WaitTime)

                Try
                    Dim objImport As New DataImporterClass
                    objImport.RollBackImportData()
                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString)
                End Try

            End While
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString)
        End Try
    End Sub

    Private Sub UpdateDealAmountCall()
        Try
            Dim ts1 As New TimeSpan(24, 0, 0)
            Dim ts2 As TimeSpan = ts1 - DateTime.Now.TimeOfDay
            Dim WaitTime As Integer = ts2.TotalMilliseconds
            WriteMessage("Task Updating Deal Amount waits for " + CCommon.ToString(WaitTime) + " Milliseconds from " + DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss:fff"))
            WriteMessage("******** Accounting Diagnosis is started. *********")

            If WaitTime < 0 Then
                Dim tsnew As New TimeSpan(24, 0, 0)
                WaitTime = tsnew.TotalMilliseconds + WaitTime
            End If

            While Not m_closingEvent.WaitOne(WaitTime)
                Try
                    Dim tskStartTime As New TimeSpan
                    tskStartTime = DateTime.Now.TimeOfDay
                    WriteMessage("Task Updating Deal Amount Started at " + DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss:fff"))
                    WaitTime = (1440 * M_POLL_TIME)
                    Dim objAlert As New ScheduledAlert
                    objAlert.UpdateDealAmount()
                    objAlert = Nothing
                    WriteMessage("Task Updating Deal Amount ended at " + DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss:fff"))
                    WriteMessage("******** Accounting Diagnosis is completed at " + DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss:fff") & ". Look for generated xml files. *********")

                    Dim TimeConsumed As New TimeSpan
                    TimeConsumed = DateTime.Now.TimeOfDay - tskStartTime
                    WaitTime = (1440 * M_POLL_TIME) - TimeConsumed.TotalMilliseconds

                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString())
                End Try

            End While

        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString())
        End Try

    End Sub

    Private Sub UpdateBankAccountBalance()
        Try
            '
            'Dim WaitTime As Integer = ts2.TotalMilliseconds
            Dim ts1 As New TimeSpan(24, 0, 0)
            Dim ts2 As TimeSpan = ts1 - DateTime.Now.TimeOfDay
            Dim WaitTime As Integer = ts2.TotalMilliseconds
            If WaitTime < 0 Then
                Dim tsnew As New TimeSpan(24, 0, 0)
                WaitTime = tsnew.TotalMilliseconds + WaitTime
            End If

            WriteMessage("Task Updating Bank Acocunt Balance  waits for " + CCommon.ToString(WaitTime) + " Milliseconds from " + DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss:fff"))

            While Not m_closingEvent.WaitOne(WaitTime)

                Try
                    WriteMessage("Task Updating Bank Acocunt Balance started at " + DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss:fff"))
                    Dim tskStartTime As New TimeSpan
                    tskStartTime = DateTime.Now.TimeOfDay
                    WaitTime = 1440 * M_POLL_TIME
                    Dim objAlert As New ScheduledAlert
                    objAlert.UpdateBankAccountBalance()
                    objAlert = Nothing
                    WriteMessage("Task Updating Bank Account Balance ended at " + DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss:fff"))
                    Dim tskTimeConsumed As New TimeSpan
                    tskTimeConsumed = DateTime.Now.TimeOfDay - tskStartTime
                    WaitTime = (1440 * M_POLL_TIME) - tskTimeConsumed.TotalMilliseconds

                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString())
                End Try

            End While

        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString())
        End Try

    End Sub

    'Private Sub OpportunityAutomationTimer_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles OpportunityAutomationTimer.Elapsed
    '    Try
    '        Dim objAlert As New ScheduledAlert
    '        objAlert.OpportunityAutomationExecution()
    '    Catch ex As Exception
    '        WriteMessage(ex.Message)
    '        WriteMessage(ex.StackTrace.ToString())
    '    End Try
    'End Sub

    Private Sub OpportunityAutomationExecution()
        Try
            Dim Rollbacktime = ConfigurationManager.AppSettings("OpportunityAutomation")
            Dim WaitTime As Integer = M_POLL_TIME * Rollbacktime   'Take value from Config file 

            While Not m_closingEvent.WaitOne(WaitTime)

                Try
                    Dim objAlert As New ScheduledAlert
                    objAlert.OpportunityAutomationExecution()
                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString)
                End Try

            End While
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString)
        End Try
    End Sub

    Private Sub UpdateCaseStatus()
        Try
            Dim WaitTime As Integer = 24 * 60 * 60 * 1000
            While Not m_closingEvent.WaitOne(WaitTime)
                Try

                    ' Update Case Status As Per Today's Date
                    WriteMessage("Updating Case Status As Per Today's Date")

                    Dim objCase As New BACRM.BusinessLogic.Case.CCases
                    objCase.UpdateCaseStatus()

                    WriteMessage("Updating Case Status is Finished.")
                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString)
                End Try

            End While
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString)
        End Try
    End Sub

    Private Sub WFExecution()
        Try
            Dim Rollbacktime = ConfigurationManager.AppSettings("WFExecution")
            Dim WaitTime As Integer = M_POLL_TIME * Rollbacktime   'Take value from Config file 

            While Not m_closingEvent.WaitOne(WaitTime)

                Try
                    Dim objAlert As New ScheduledAlert
                    objAlert.WFExecution()
                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString)
                End Try

            End While
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString)
        End Try
    End Sub

    Private Sub ExecuteBizRecurrence()
        Try
            Dim WaitTime As Integer = M_POLL_TIME * CCommon.ToInteger(ConfigurationManager.AppSettings("BizRecurrence"))

            Do
                Try
                    Dim objAlert As New ScheduledAlert
                    objAlert.BizRecurrence()
                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString)
                End Try
            Loop While Not m_closingEvent.WaitOne(WaitTime)

        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString)
        End Try
    End Sub

    Private Sub EmailCustomQueryReport()
        Try
            Dim WaitTime As Integer = M_POLL_TIME * CCommon.ToInteger(ConfigurationManager.AppSettings("EmailCustomQueryReport"))

            Do
                Try
                    Dim objAlert As New ScheduledAlert
                    objAlert.EmailCustomQueryReport()
                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString)
                End Try
            Loop While Not m_closingEvent.WaitOne(WaitTime)
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString)
        End Try
    End Sub

    Private Sub GetCurrencyConversionRate()
        Try
            Dim WaitTime As Integer = M_POLL_TIME * CCommon.ToInteger(ConfigurationManager.AppSettings("CurrencyConversionRate"))

            Do
                Try
                    Dim objAlert As New ScheduledAlert
                    objAlert.GetCurrencyConversionRate()
                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString)
                End Try
            Loop While Not m_closingEvent.WaitOne(WaitTime)
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString)
        End Try
    End Sub

    Private Sub BulkOrderSalesExecution()
        Try
            Dim WaitTime As Integer = M_POLL_TIME * CCommon.ToInteger(ConfigurationManager.AppSettings("MassSalesOrder"))

            Do
                Try
                    Dim objAlert As New ScheduledAlert
                    objAlert.BulkOrderSalesExecution()
                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString)
                End Try
            Loop While Not m_closingEvent.WaitOne(WaitTime)
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString)
        End Try
    End Sub

    Private Sub UpdateRecordHistory()
        Try
            Dim WaitTime As Integer = M_POLL_TIME * CCommon.ToInteger(ConfigurationManager.AppSettings("RecordHistoryRate"))

            Do
                Try
                    Dim objAlert As New ScheduledAlert
                    objAlert.UpdateRecordHistory()
                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString)
                End Try
            Loop While Not m_closingEvent.WaitOne(WaitTime)
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString)
        End Try
    End Sub

    Private Sub UpdateElasticSearchData()
        Try
            Dim WaitTime As Integer = M_POLL_TIME * CCommon.ToInteger(ConfigurationManager.AppSettings("ElasticSearchDataUpdateRate"))

            Do
                Try
                    Dim objElasticSearchDataSync As New ElasticSearchUpdateIndexedData
                    objElasticSearchDataSync.UpdateElasticSearchData()
                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString)
                End Try
            Loop While Not m_closingEvent.WaitOne(WaitTime)
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString)
        End Try
    End Sub

    Private Sub ElasticSearchReindexData()
        Try
            Dim WaitTime As Integer = M_POLL_TIME * CCommon.ToInteger(ConfigurationManager.AppSettings("ElasticSearchReindexRate"))

            Do
                Try
                    Dim objElasticSearchDataSync As New ElasticSearchUpdateIndexedData
                    objElasticSearchDataSync.ElasticSearchReindexAllData()
                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString)
                End Try
            Loop While Not m_closingEvent.WaitOne(WaitTime)
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString)
        End Try
    End Sub

    Private Sub UpdateBizCartElasticSearchData()
        Try
            Dim WaitTime As Integer = M_POLL_TIME * CCommon.ToInteger(ConfigurationManager.AppSettings("ElasticSearchBizCartIndexRate"))

            Do
                Try
                    Dim objElasticBizCartSearchDataSync As New ElasticSearchBizCartUpdateIndexedData
                    objElasticBizCartSearchDataSync.UpdateElasticSearchData()
                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString)
                End Try
            Loop While Not m_closingEvent.WaitOne(WaitTime)
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString)
        End Try
    End Sub

    Private Sub BizCartElasticSearchWipeOutOldData()
        Try
            Dim WaitTime As Integer = M_POLL_TIME * CCommon.ToInteger(ConfigurationManager.AppSettings("ElasticSearchBizCartIndexWipeData"))

            Do
                Try
                    Dim objElasticBizCartSearchDataSync As New ElasticSearchBizCartUpdateIndexedData
                    objElasticBizCartSearchDataSync.ElasticSearchWipeOutData()
                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString)
                End Try
            Loop While Not m_closingEvent.WaitOne(WaitTime)
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString)
        End Try
    End Sub

    Private Sub DashboardCommissionReportCalculation()
        Try
            Dim WaitTime As Integer = M_POLL_TIME * CCommon.ToInteger(ConfigurationManager.AppSettings("DashboardCommissionReportCalculationRate"))

            Do
                Try
                    Dim objScheduleAlert As New ScheduledAlert
                    objScheduleAlert.RecalculateCommissionForDashboardReport()
                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString)
                End Try
            Loop While Not m_closingEvent.WaitOne(WaitTime)
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString)
        End Try
    End Sub

    Private Sub ProcessEDIQueue()
        Try
            Dim WaitTime As Integer = M_POLL_TIME * CCommon.ToInteger(ConfigurationManager.AppSettings("EDIQueueExecution"))

            Do
                Try
                    WriteMessage("EDI Queue processing started")
                    Dim objEDIQueue As New BACRM.BusinessLogic.Opportunities.EDIQueue
                    objEDIQueue.Process()
                    WriteMessage("EDI Queue processing completed")
                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString)
                End Try
            Loop While Not m_closingEvent.WaitOne(WaitTime)

        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString)
        End Try
    End Sub


    Private Sub WFDateFieldsDataSync()
        Try
            Dim WaitTime As Integer = M_POLL_TIME * CCommon.ToInteger(ConfigurationManager.AppSettings("WFDateFieldsExecutionTime"))

            Do
                Try
                    Dim objCommon As New CCommon
                    objCommon.WFDateFieldsDataSync()

                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString)
                End Try
            Loop While Not m_closingEvent.WaitOne(WaitTime)
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString)
        End Try
    End Sub

    Public Sub OrderShippingTracking()
        Try
            Dim WaitTime As Integer = M_POLL_TIME * CCommon.ToInteger(ConfigurationManager.AppSettings("OrderShippingTrackingExecutionTime"))

            Do
                Try
                    WriteMessage("Order tracking detail update started")
                    Dim objScheduleAlert As New ScheduledAlert
                    objScheduleAlert.UpdateOrderTackingDetail()
                    WriteMessage("Order tracking detail update completed")
                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString)
                End Try
            Loop While Not m_closingEvent.WaitOne(WaitTime)
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString)
        End Try
    End Sub

    Public Sub AutoCalculateOrderShippingRate()
        Try
            Dim WaitTime As Integer = M_POLL_TIME * CCommon.ToInteger(ConfigurationManager.AppSettings("AutoCalculateOrderShippingRateTime"))

            Do
                Try
                    WriteMessage("Auto calculate order shipping rate started")
                    Dim objShippingCalculation As New ShippingCalculation
                    objShippingCalculation.AutoCalculateOrderShippingRate()
                    WriteMessage("Auto calculate order shipping rate started completed")
                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString)
                End Try
            Loop While Not m_closingEvent.WaitOne(WaitTime)
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString)
        End Try
    End Sub

    Public Sub EmailScheduledReportsGroup()
        Try
            Dim WaitTime As Integer = M_POLL_TIME * CCommon.ToInteger(ConfigurationManager.AppSettings("EmailScheduledReportsGroupTime"))

            Do
                Try
                    WriteMessage("Email schedule reports group data cache started")
                    Dim objSRG As New BACRM.BusinessLogic.Reports.ScheduledReportsGroup
                    objSRG.EmailScheduledReportsGroup(False)
                    WriteMessage("Email schedule reports group data cache completed")
                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString)
                End Try
            Loop While Not m_closingEvent.WaitOne(WaitTime)
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString)
        End Try
    End Sub

    Public Sub ScheduledReportsGroupDataCache()
        Try
            Dim WaitTime As Integer = M_POLL_TIME * CCommon.ToInteger(ConfigurationManager.AppSettings("ScheduledReportsGroupDataCacheTime"))

            Do
                Try
                    WriteMessage("Scheduled reports group data cache started")
                    Dim objSRG As New BACRM.BusinessLogic.Reports.ScheduledReportsGroup
                    objSRG.StartDataCache(False, 2)
                    WriteMessage("Scheduled reports group data cache completed")
                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString)
                End Try
            Loop While Not m_closingEvent.WaitOne(WaitTime)
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString)
        End Try
    End Sub

    Public Sub DashboardReportDataCache()
        Try
            Dim WaitTime As Integer = M_POLL_TIME * CCommon.ToInteger(ConfigurationManager.AppSettings("DashboardReportDataCacheTime"))

            Do
                Try
                    WriteMessage("Dashboard reports data cache started")
                    Dim objReportManage As New BACRM.BusinessLogic.CustomReports.CustomReportsManage
                    objReportManage.StartDashboardReportDataCache()
                    WriteMessage("Dashboard reports data cache completed")
                Catch ex As Exception
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace.ToString)
                End Try
            Loop While Not m_closingEvent.WaitOne(WaitTime)
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString)
        End Try
    End Sub
End Class
