﻿Namespace GoogleApp
    Public Class Common

        Private _UserName As String
        Public Property UserName() As String
            Get
                Return _UserName
            End Get
            Set(ByVal value As String)
                _UserName = value
            End Set
        End Property

        Private _Password As String
        Public Property Password() As String
            Get
                Return _Password
            End Get
            Set(ByVal value As String)
                _Password = value
            End Set
        End Property

        Private _DomainID As Long
        Public Property DomainID() As Long
            Get
                Return _DomainID
            End Get
            Set(ByVal value As Long)
                _DomainID = value
            End Set
        End Property


        Private _UserCntID As Long
        Public Property UserCntID() As Long
            Get
                Return _UserCntID
            End Get
            Set(ByVal value As Long)
                _UserCntID = value
            End Set
        End Property

    End Class
End Namespace