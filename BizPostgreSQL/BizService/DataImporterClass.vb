﻿'Option Explicit On
'Option Strict On

Imports System.IO
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Account
Imports LumenWorks.Framework.IO.Csv
Imports LumenWorks.Framework.Tests.Unit
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.WebAPI
Imports System.Reflection
Imports System.Collections.Generic
Imports System.Web
Imports System.Text
Imports System.Threading
Imports System.Threading.Tasks
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Accounting


Public Class DataImporterClass
    Inherits BACRM.BusinessLogic.CBusinessBase

#Region "OBJECT DECLARATION"

    Dim objImport As New ImportWizard
    Dim objLead As CLeads
    Dim objCommon As New CCommon

    Private Shared m_closingEvent As New ManualResetEvent(False)
    Private Shared m_tasksList As New List(Of Task)()

#End Region

#Region "GLOBAL VARIABLES"

    Dim intImportFileID As Long
    'Dim intDomainId As Long
    'Dim UserCntID As Long
    Dim lngMasterID As Long
    Protected strImportFileName As String = ""
    Private IsSingleOrder As Boolean
    Private IsExistingOrganization As Boolean
    Private DivisionID As Long
    Private ContactID As Long
    Private CompanyName As String
    Private ContactFirstName As String
    Private ContactLastName As String
    Private ContactEmail As String
    Private IsMatchOrganizationID As Boolean
    Private MatchFieldID As Long

    Dim dataTableDestination As New DataTable
    Dim dataTableHeading As New DataTable
    Dim strUpdateValues As String
    Dim Mode As Short
    Dim dsDropDownData As New DataSet
    Dim strErrorDetail As New StringBuilder

    Dim lngWaitMiliSeconds As Long = CCommon.ToLong(Configuration.WebConfigurationManager.AppSettings("WaitMiliSeconds"))
    Dim lngWaitRecords As Long = CCommon.ToLong(Configuration.WebConfigurationManager.AppSettings("WaitRecords"))

    Dim strEmail As String = ""
    Dim strName As String = ""
    Dim dtStartTime As DateTime
    Dim lngCSVRow As Long = 0
    Dim lngStartRow As Long = 0
    Dim tintImportType As Short

#End Region

#Region "PROCESSING DATA IMPORT REQUEST FROM DATABASE"

    'Select TOP 1 row from Import File Master table
    'Process & Update Data as per .csv data file has generate.
    'Wait for defined time & repeat the same procedure.

    'Use ImportWizard class to make every operation

    Public Sub StartImport()
        Try
            WriteMessage("Import Process Started.")
            'TO DEBUG : TEMP CODE CHECK BLOCK
            'For i As Long = 0 To 10000000000000
            '    WriteMessage("dsfdsfds")
            'Next
            '---------------------------------------------------

            Dim dsImport As New DataSet

            dsImport = objImport.GetImportFileID()
            If dsImport IsNot Nothing AndAlso dsImport.Tables.Count > 0 AndAlso dsImport.Tables(0).Rows.Count > 0 Then
                dtStartTime = DateTime.Now
                objImport.ImportFileID = CCommon.ToLong(dsImport.Tables(0).Rows(0)("intImportFileID"))
                objImport.ImportFileName = CCommon.ToString(dsImport.Tables(0).Rows(0)("vcImportFileName"))
                objImport.ImportMasterID = CCommon.ToInteger(dsImport.Tables(0).Rows(0)("numMasterID"))
                objImport.DomainId = CCommon.ToLong(dsImport.Tables(0).Rows(0)("numDomainID"))
                objImport.UserContactID = CCommon.ToLong(dsImport.Tables(0).Rows(0)("numUserContactID"))

                IsSingleOrder = CCommon.ToBool(dsImport.Tables(0).Rows(0)("bitSingleOrder"))
                IsExistingOrganization = CCommon.ToBool(dsImport.Tables(0).Rows(0)("bitExistingOrganization"))
                DivisionID = CCommon.ToLong(dsImport.Tables(0).Rows(0)("numDivisionID"))
                ContactID = CCommon.ToLong(dsImport.Tables(0).Rows(0)("numContactID"))
                CompanyName = CCommon.ToString(dsImport.Tables(0).Rows(0)("vcCompanyName"))
                ContactFirstName = CCommon.ToString(dsImport.Tables(0).Rows(0)("vcContactFirstName"))
                ContactLastName = CCommon.ToString(dsImport.Tables(0).Rows(0)("vcContactLastName"))
                ContactEmail = CCommon.ToString(dsImport.Tables(0).Rows(0)("vcContactEmail"))
                IsMatchOrganizationID = CCommon.ToBool(dsImport.Tables(0).Rows(0)("bitMatchOrganizationID"))
                MatchFieldID = CCommon.ToLong(dsImport.Tables(0).Rows(0)("numMatchFieldID"))

                lngStartRow = CCommon.ToLong(dsImport.Tables(0).Rows(0)("numProcessedCSVRowNumber"))

                intImportFileID = objImport.ImportFileID
                DomainID = CCommon.ToLong(objImport.DomainId)
                UserCntID = CCommon.ToLong(objImport.UserContactID)
                lngMasterID = CCommon.ToLong(objImport.ImportMasterID)
                strImportFileName = CCommon.ToString(objImport.ImportFileName)

                objLead = New CLeads
                objLead.ContactID = UserCntID
                objLead.ShowAll = 0
                Dim dtEmail As DataTable = objLead.GetContactInfoPromote()
                If dtEmail IsNot Nothing AndAlso dtEmail.Rows.Count > 0 Then
                    strEmail = CCommon.ToString(dtEmail.Rows(0)("vcEmail"))
                    strName = CCommon.ToString(dtEmail.Rows(0)("Name"))
                Else
                    WriteMessage("User email is not available.")
                End If


                WriteMessage("Import Process Started for :" & objImport.ImportFileName)
                WriteMessage("Import ID :" & intImportFileID)
                WriteMessage("File :" & objImport.ImportFileName)
                WriteMessage("DomainID :" & DomainID)
                WriteMessage("UserContactID :" & UserCntID)
                WriteMessage("Start Time :" & dtStartTime)
                ImportAndSaveImportData()
            Else
                WriteMessage("No records available to import.")
                Exit Sub
            End If
            WriteMessage("Import Process Completed.")
        Catch ex As Exception
            WriteMessage("Domain ID : " & DomainID)
            WriteMessage("User Contact ID : " & UserCntID)
            WriteMessage(ex.Message)
        End Try
    End Sub

#End Region

#Region "CREATE/SAVE DATA"

    Private Sub ImportAndSaveImportData()

        Try
            Dim strException As String = ""
            Dim strStackTrace As String = ""

            Dim dataTable As New DataTable
            Dim dsData As New DataSet
            Dim numRows As Integer
            Dim intCnt As Integer
            Dim itemLinkingID As Integer
            Dim dataRowHeading As DataRow

            dataTableHeading.Columns.Add("FormFieldID").SetOrdinal(0)
            dataTableHeading.Columns.Add("FormFieldName").SetOrdinal(1)
            dataTableHeading.Columns.Add("ImportFieldID").SetOrdinal(2)
            dataTableHeading.Columns.Add("dbFieldName").SetOrdinal(3)
            dataTableHeading.Columns.Add("vcAssociatedControlType").SetOrdinal(4)
            dataTableHeading.Columns.Add("bitDefault").SetOrdinal(5)
            dataTableHeading.Columns.Add("vcFieldType").SetOrdinal(6)
            dataTableHeading.Columns.Add("vcListItemType").SetOrdinal(7)
            dataTableHeading.Columns.Add("vcLookBackTableName").SetOrdinal(8)
            dataTableHeading.Columns.Add("vcOrigDBColumnName").SetOrdinal(9)
            dataTableHeading.Columns.Add("vcPropertyName").SetOrdinal(10)
            dataTableHeading.Columns.Add("bitCustomField").SetOrdinal(11)
            dataTableHeading.Columns.Add("IsPrimary").SetOrdinal(12)

            objImport.DomainId = DomainID
            objImport.ImportFileID = intImportFileID
            objImport.UserContactID = UserCntID
            objImport.MasterID = lngMasterID
            dsData = objImport.LoadMappedData()

            itemLinkingID = CCommon.ToShort(dsData.Tables(0).Rows(0)("tintItemLinkingID"))
            tintImportType = CCommon.ToShort(dsData.Tables(0).Rows(0)("tintImportType"))

            dataTable = dsData.Tables(1)
            numRows = dataTable.Rows.Count()

            For intCnt = 0 To numRows - 1
                dataRowHeading = dataTableHeading.NewRow
                dataRowHeading("FormFieldID") = dataTable.Rows(intCnt).Item("FormFieldID")
                dataRowHeading("FormFieldName") = dataTable.Rows(intCnt).Item("FormFieldName")
                dataRowHeading("ImportFieldID") = dataTable.Rows(intCnt).Item("ImportFieldID")
                dataRowHeading("dbFieldName") = dataTable.Rows(intCnt).Item("dbFieldName")
                dataRowHeading("vcAssociatedControlType") = dataTable.Rows(intCnt).Item("vcAssociatedControlType")

                dataRowHeading("bitDefault") = dataTable.Rows(intCnt).Item("bitDefault")
                dataRowHeading("vcFieldType") = dataTable.Rows(intCnt).Item("vcFieldType")
                dataRowHeading("vcListItemType") = dataTable.Rows(intCnt).Item("vcListItemType")
                dataRowHeading("vcLookBackTableName") = dataTable.Rows(intCnt).Item("vcLookBackTableName")
                dataRowHeading("vcOrigDBColumnName") = dataTable.Rows(intCnt).Item("vcOrigDBColumnName")
                dataRowHeading("vcPropertyName") = dataTable.Rows(intCnt).Item("vcPropertyName")
                dataRowHeading("bitCustomField") = dataTable.Rows(intCnt).Item("bitCustomField")

                If (CCommon.ToString(dataTable.Rows(intCnt).Item("dbFieldName")) = "numItemCode" AndAlso lngMasterID = CCommon.ToInteger(IMPORT_TYPE.Item) AndAlso tintImportType = 1 AndAlso itemLinkingID = 1) OrElse
                    (CCommon.ToString(dataTable.Rows(intCnt).Item("dbFieldName")) = "vcSKU" AndAlso lngMasterID = CCommon.ToInteger(IMPORT_TYPE.Item) AndAlso tintImportType = 1 AndAlso itemLinkingID = 2) OrElse
                    (CCommon.ToString(dataTable.Rows(intCnt).Item("dbFieldName")) = "numBarCodeId" AndAlso lngMasterID = CCommon.ToInteger(IMPORT_TYPE.Item) AndAlso tintImportType = 1 AndAlso itemLinkingID = 3) OrElse
                    (CCommon.ToString(dataTable.Rows(intCnt).Item("dbFieldName")) = "vcModelID" AndAlso lngMasterID = CCommon.ToInteger(IMPORT_TYPE.Item) AndAlso tintImportType = 1 AndAlso itemLinkingID = 4) OrElse
                    (CCommon.ToString(dataTable.Rows(intCnt).Item("dbFieldName")) = "vcItemName" AndAlso lngMasterID = CCommon.ToInteger(IMPORT_TYPE.Item) AndAlso tintImportType = 1 AndAlso itemLinkingID = 5) OrElse
                   (CCommon.ToString(dataTable.Rows(intCnt).Item("dbFieldName")) = "numWareHouseItemID" AndAlso lngMasterID = CCommon.ToInteger(IMPORT_TYPE.Item_WareHouse)) OrElse
                   (CCommon.ToString(dataTable.Rows(intCnt).Item("dbFieldName")) = "numItemImageId" AndAlso lngMasterID = CCommon.ToInteger(IMPORT_TYPE.Item_Images)) OrElse
                   (CCommon.ToString(dataTable.Rows(intCnt).Item("dbFieldName")) = "numVendorID" AndAlso lngMasterID = CCommon.ToInteger(IMPORT_TYPE.Item_Vendor)) OrElse
                   (CCommon.ToString(dataTable.Rows(intCnt).Item("dbFieldName")) = "numItemDetailID" AndAlso lngMasterID = CCommon.ToInteger(IMPORT_TYPE.Item_Assembly)) OrElse
                   (CCommon.ToString(dataTable.Rows(intCnt).Item("dbFieldName")) = "vcNonBizCompanyID" AndAlso lngMasterID = CCommon.ToInteger(IMPORT_TYPE.Organization) AndAlso tintImportType = 1 AndAlso itemLinkingID = 1) OrElse
                   (CCommon.ToString(dataTable.Rows(intCnt).Item("dbFieldName")) = "numDivisionID" AndAlso lngMasterID = CCommon.ToInteger(IMPORT_TYPE.Organization) AndAlso tintImportType = 1 AndAlso itemLinkingID = 2) OrElse
                   (CCommon.ToString(dataTable.Rows(intCnt).Item("dbFieldName")) = "vcNonBizContactID" AndAlso lngMasterID = CCommon.ToInteger(IMPORT_TYPE.Contact) AndAlso tintImportType = 1 AndAlso (itemLinkingID = 1 Or itemLinkingID = 5 Or itemLinkingID = 7)) OrElse
                   (CCommon.ToString(dataTable.Rows(intCnt).Item("dbFieldName")) = "numContactID" AndAlso lngMasterID = CCommon.ToInteger(IMPORT_TYPE.Contact) AndAlso tintImportType = 1 AndAlso (itemLinkingID = 2 Or itemLinkingID = 6 Or itemLinkingID = 8)) OrElse
                   (CCommon.ToString(dataTable.Rows(intCnt).Item("dbFieldName")) = "numAddressID" AndAlso lngMasterID = CCommon.ToInteger(IMPORT_TYPE.Address_Details)) OrElse
                   (CCommon.ToString(dataTable.Rows(intCnt).Item("dbFieldName")) = "vcNonBizContactID" AndAlso lngMasterID = CCommon.ToInteger(IMPORT_TYPE.Organization_Correspondence)) Then
                    dataRowHeading("IsPrimary") = 1
                Else
                    dataRowHeading("IsPrimary") = 0
                End If

                dataTableHeading.Rows.Add(dataRowHeading)
            Next

            Dim drHead As DataRow
            dataTableDestination.Columns.Add("SrNo")
            dataTableDestination.Columns("SrNo").AutoIncrement = True
            dataTableDestination.Columns("SrNo").AutoIncrementSeed = 1
            dataTableDestination.Columns("SrNo").AutoIncrementStep = 1

            Dim intColCnt As Integer = 0
            For Each drHead In dataTableHeading.Rows
                If dataTableDestination.Columns.Contains(CCommon.ToString(drHead.Item("FormFieldID") & "_" & drHead.Item("dbFieldName"))) = True Then
                    Continue For
                Else
                    dataTableDestination.Columns.Add(CCommon.ToString(drHead.Item("FormFieldID") & "_" & drHead.Item("dbFieldName")))
                End If
            Next

            Dim arryList() As String
            Dim strdllValue As String = ""
            Dim intArryValue As Integer = 0
            Dim fileLocation As String = CCommon.GetDocumentPhysicalPath(CCommon.ToLong(objImport.DomainId)) & objImport.ImportFileName


            If System.IO.File.Exists(fileLocation) Then
                Dim streamReader As StreamReader
                Dim intCount As Integer = intCnt

                streamReader = File.OpenText(fileLocation)
                dataTableDestination.Clear()

                Dim csv As CsvReader = New CsvReader(streamReader, True)
                Dim dataRowDestination As DataRow

                Try
                    csv.MissingFieldAction = MissingFieldAction.ReplaceByEmpty
                    csv.SupportsMultiline = True

                    For intCntNew As Integer = 0 To dataTableHeading.Rows.Count - 1
                        strdllValue = strdllValue & "," & CCommon.ToString(dataTableHeading.Rows(intCntNew)("ImportFieldID"))
                    Next

                    strdllValue = strdllValue.TrimStart(CChar(","))
                    strdllValue = strdllValue.TrimEnd(CChar(","))
                    arryList = Split(strdllValue, ",")

                    Dim i1000 As Integer = 0
                    While (csv.ReadNextRecord())
                        Dim intEmpty As Integer = 0
                        dataRowDestination = dataTableDestination.NewRow
                        intCount = 0

                        Dim intCustom As Integer = 0
                        For Each drHead In dataTableHeading.Rows
                            intArryValue = CType(CDbl(arryList(intCount)) - 1, Integer)
                            dataRowDestination(DirectCast(drHead.Item("FormFieldID") & "_" & drHead.Item("dbFieldName"), String)) = csv(intArryValue).ToString
                            intCount += 1
                            If csv(intArryValue).ToString = "" Then
                                intEmpty += 1
                            End If
                        Next

                        If intEmpty <> arryList.Length Then
                            dataTableDestination.Rows.Add(dataRowDestination)
                        End If
                        i1000 = i1000 + 1
                    End While

                    Dim dtDataHold As New DataTable
                    dtDataHold = dataTableDestination.Copy

                    objImport.ImportFileID = intImportFileID
                    objImport.DomainId = DomainID
                    objImport.Mode = 1
                    dsDropDownData = objImport.GetDropDownData("")


                    Dim strFormFieldName As String = ""
                    Dim strFormFieldID As Object = Nothing
                    Dim vcFieldtype As Char
                    Dim vcAssociatedControlType As String = ""
                    Dim strValueString As String = ""
                    Dim strImportType As String = ""

                    If dsDropDownData IsNot Nothing AndAlso dsDropDownData.Tables.Count > 0 Then

                        For Each dr As DataRow In dataTableHeading.Rows
                            vcFieldtype = dr("vcFieldType")
                            If vcFieldtype = "R" Then
                                vcAssociatedControlType = dr("vcAssociatedControlType")
                                strFormFieldName = CCommon.ToString(dr("dbFieldName"))
                                strFormFieldID = CCommon.ToLong(dr("FormFieldID"))
                                Select Case vcAssociatedControlType
                                    Case "SelectBox"
                                        If dtDataHold.Columns.Contains(strFormFieldID & "_" & strFormFieldName) = True Then
                                            For intRow As Integer = 0 To dtDataHold.Rows.Count - 1
                                                If dsDropDownData.Tables.Count > 1 Then
                                                    Dim drRow() As DataRow

                                                    strValueString = CCommon.ToString(dtDataHold.Rows(intRow)(strFormFieldID & "_" & strFormFieldName)).Trim().ToLower().Replace("'", "''")
                                                    If strFormFieldName = "numGrpID" AndAlso (strValueString.ToLower.Contains("my") = True OrElse
                                                                                           strValueString.ToLower.Contains("myleads") = True OrElse
                                                                                           strValueString.ToLower.Contains("my leads") = True) Then

                                                        strValueString = "My Leads"
                                                    ElseIf strFormFieldName = "numGrpID" AndAlso (strValueString.ToLower.Contains("web") = True OrElse
                                                                                               strValueString.ToLower.Contains("webleads") = True OrElse
                                                                                               strValueString.ToLower.Contains("web leads") = True) Then

                                                        strValueString = "WebLeads"
                                                    ElseIf strFormFieldName = "numGrpID" AndAlso (strValueString.ToLower.Contains("public") = True OrElse
                                                                                               strValueString.ToLower.Contains("publicleads") = True OrElse
                                                                                               strValueString.ToLower.Contains("public leads") = True) Then

                                                        strValueString = "PublicLeads"
                                                    End If

                                                    If strFormFieldName = "vcExportToAPI" OrElse strFormFieldName = "numCategoryID" Then
                                                        strValueString = SetValues(strValueString, ",", dsDropDownData.Tables(1).Select("vcFieldID = '" & (strFormFieldID & "_" & strFormFieldName) & "'").CopyToDataTable())
                                                        dtDataHold.Rows(intRow)(strFormFieldID & "_" & strFormFieldName) = strValueString
                                                    ElseIf strFormFieldName = "" Or strFormFieldName = "" Or strFormFieldName = "" Or strFormFieldName = "" Then
                                                    Else
                                                        drRow = dsDropDownData.Tables(1).Select("vcFieldID = '" & (strFormFieldID & "_" & strFormFieldName & "' AND Value = '" & CCommon.ToString(strValueString) & "'"))
                                                        If drRow.Length > 0 Then
                                                            dtDataHold.Rows(intRow)(strFormFieldID & "_" & strFormFieldName) = CCommon.ToString(drRow(0)("Key"))
                                                        Else
                                                            If strFormFieldName = "numShipState" Or strFormFieldName = "numBillState" Or strFormFieldName = "numState" Or strFormFieldName = "State" Or strFormFieldName = "numShippingService" Then
                                                                drRow = dsDropDownData.Tables(1).Select("vcFieldID = '" & (strFormFieldID & "_" & strFormFieldName & "' AND vcAbbreviations = '" & CCommon.ToString(strValueString) & "'"))

                                                                If drRow.Length > 0 Then
                                                                    dtDataHold.Rows(intRow)(strFormFieldID & "_" & strFormFieldName) = CCommon.ToString(drRow(0)("Key"))
                                                                Else
                                                                    dtDataHold.Rows(intRow)(strFormFieldID & "_" & strFormFieldName) = 0
                                                                End If
                                                            Else
                                                                dtDataHold.Rows(intRow)(strFormFieldID & "_" & strFormFieldName) = 0
                                                                End If
                                                            End If
                                                        End If
                                                End If

                                                dtDataHold.AcceptChanges()
                                            Next
                                        End If
                                End Select
                            End If
                        Next
                    End If

                    If dtDataHold IsNot Nothing AndAlso dtDataHold.Rows.Count > 0 Then

                        If lngMasterID = IMPORT_TYPE.Item Then
                            ImportItems(dtDataHold, itemLinkingID)
                        ElseIf lngMasterID = IMPORT_TYPE.Organization Then
                            ImportOrganizations(dtDataHold, itemLinkingID)
                        ElseIf lngMasterID = IMPORT_TYPE.Contact Then
                            ImportContacts(dtDataHold, itemLinkingID)
                        ElseIf lngMasterID = IMPORT_TYPE.Organization_Correspondence Then
                            ImportOrganizationCorrespondence(dtDataHold)
                        ElseIf lngMasterID = IMPORT_TYPE.Item_WareHouse Then
                            ImportItemWarehouse(dtDataHold)
                        ElseIf lngMasterID = IMPORT_TYPE.Item_Vendor Then
                            ImportItemVendors(dtDataHold)
                        ElseIf lngMasterID = IMPORT_TYPE.Item_Images Then
                            ImportItemImages(dtDataHold)
                        ElseIf lngMasterID = IMPORT_TYPE.Item_Assembly Then
                            ImportKitSubItems(dtDataHold)
                        ElseIf lngMasterID = IMPORT_TYPE.Address_Details Then
                            ImportAddressDetails(dtDataHold)
                        ElseIf lngMasterID = IMPORT_TYPE.SalesOrder Then
                            ImportOrder(dtDataHold, 1, itemLinkingID)
                        ElseIf lngMasterID = IMPORT_TYPE.PurchaseOrder Then
                            ImportOrder(dtDataHold, 2, itemLinkingID)
                        ElseIf lngMasterID = IMPORT_TYPE.JournalEntries Then
                            ImportAccountingEntries(dtDataHold)
                        End If
                    Else
                        WriteMessage("Not able to process csv.")
                        WriteMessage("Error Message: Number of rows to import is 0")
                        WriteMessage("Error StackTrace:")
                        UpdateImportFileStatus(0, 0, 0, "", "", "")
                    End If
                Catch ex As Exception
                    WriteMessage("Not able to process csv. File read Errors.")
                    WriteMessage("Error Message: " & ex.Message)
                    WriteMessage("Error StackTrace: " & ex.StackTrace.ToString())
                    UpdateImportFileStatus(0, 0, 0, "", "", "")
                Finally
                    streamReader.Close()
                End Try
            Else
                WriteMessage("File doesn't exists. Please import this file again.")
                objImport = New ImportWizard(intImportFileID, DomainID)
                With objImport
                    .ImportFileID = intImportFileID
                    .ImportFileName = objImport.ImportFileName
                    .MasterID = objImport.ImportMasterID
                    .RecordAdded = 0
                    .RecordUpdated = 0
                    .Errors = 0
                    .Duplicates = 0
                    .HistoryID = 0
                    .CreatedDate = objImport.CreatedDate
                    .ImportDate = DateTime.Now
                    .DomainId = DomainID
                    .UserContactID = UserCntID
                    .Status = enmImportDataStatus.Import_Completed
                    .hasSendEmail = True
                    .MappedData = Nothing
                    .Mode = 1
                    .History_Added_Value = If(.RecordAdded > 0 Or .RecordUpdated > 0, 0, "")
                    .HistoryDateTime = DateTime.Now
                    .SaveImportMappedData()

                    If .hasSendEmail = True Then

                        Try
                            Dim objSendMail As New Email
                            objSendMail.SendSystemEmail("Import File Statistics : ", _
                                                        "<p>File doesn't exists.</p>", _
                                                        "", _
                                                        System.Configuration.ConfigurationManager.AppSettings("FromAddress"), _
                                                        strEmail, strName)
                        Catch ex As Exception

                            WriteMessage("Domain ID : " & DomainID)
                            WriteMessage("User Contact ID : " & UserCntID)
                            WriteMessage("Error ocuured while sending import report email.")
                        End Try
                    End If
                End With
            End If
        Catch ex As Exception
            WriteMessage("Not able to process csv. Unknown error occurred.")
            WriteMessage("Error Message: " & ex.Message)
            WriteMessage("Error StackTrace: " & ex.StackTrace.ToString())
            UpdateImportFileStatus(0, 0, 0, "", "", "")

            WriteMessage("Main Method Error.")
            WriteMessage(ex.Message)
            WriteMessage("Error StackTrace: " & ex.StackTrace.ToString())

            Throw ex
        End Try
    End Sub

#End Region

#Region "SET PROPERTIES"

    Public Sub SetProperty(ByRef objClassObject As Object, ByVal sPropertyName As String, ByVal sPropertyValue As String)
        Try
            Dim theType As Type = objClassObject.GetType
            Dim PropertyItem As PropertyInfo = theType.GetProperty(sPropertyName, BindingFlags.Public Or BindingFlags.Instance)
            Dim fillObject As Object = objClassObject

            With PropertyItem
                If PropertyItem.Name = sPropertyName Then                                           'Compare the class property name with that of the db column name
                    Select Case PropertyItem.PropertyType.ToString()                                'Selecting the datataype of teh property
                        Case "System.String" : PropertyItem.SetValue(fillObject, sPropertyValue, Nothing)

                        Case "System.Int64"                          'Integer type properties
                            If IsNumeric(sPropertyValue) Then PropertyItem.SetValue(fillObject, CCommon.ToLong(sPropertyValue), Nothing) 'Typecasting to Int64 before setting the value

                        Case "System.Int32"                          'Integer type properties
                            If IsNumeric(sPropertyValue) Then PropertyItem.SetValue(fillObject, CCommon.ToInteger(sPropertyValue), Nothing) 'Typecasting to Int32 before setting the value

                        Case "System.Int16"                          'Integer type(Short - Int16) properties
                            If sPropertyValue.ToString().ToLower().Contains("yes") OrElse _
                               sPropertyValue.ToString().ToLower().Contains("y") OrElse _
                               sPropertyValue.ToString().ToLower().Contains("true") OrElse _
                               sPropertyValue.ToString().ToLower().Contains("1") Then
                                PropertyItem.SetValue(fillObject, CShort(1), Nothing)

                            ElseIf sPropertyValue.ToString().ToLower().Contains("no") OrElse _
                                   sPropertyValue.ToString().ToLower().Contains("n") OrElse _
                                   sPropertyValue.ToString().ToLower().Contains("false") OrElse _
                                   sPropertyValue.ToString().ToLower().Contains("0") Then
                                PropertyItem.SetValue(fillObject, CShort(0), Nothing)

                            Else
                                If IsNumeric(sPropertyValue) Then PropertyItem.SetValue(fillObject, CShort(sPropertyValue), Nothing) 'Typecasting to integer before setting the value

                            End If

                        Case "System.Double"                                                        'Double type properties
                            If IsNumeric(sPropertyValue) Then PropertyItem.SetValue(fillObject, CDbl(sPropertyValue), Nothing) 'Typecasting to Double before setting the value

                        Case "System.Decimal"                                                        'Double type properties
                            If IsNumeric(sPropertyValue) Then PropertyItem.SetValue(fillObject, CDec(sPropertyValue), Nothing) 'Typecasting to Decimal before setting the value

                        Case "System.DateTime"                                                        'Date type properties
                            If IsDate(sPropertyValue) Then PropertyItem.SetValue(fillObject, CDate(sPropertyValue), Nothing) 'Typecasting to DateTime before setting the value

                        Case "System.Boolean"                                                       'Boolean properties
                            If sPropertyValue.ToString().ToLower() = "yes" OrElse _
                                sPropertyValue.ToString().ToLower() = "y" OrElse _
                                sPropertyValue.ToString().ToLower() = "true" OrElse _
                                sPropertyValue.ToString().ToLower() = "1" Then

                                sPropertyValue = True
                                If sPropertyName.Trim.ToLower() = "NoTax".ToLower() Then
                                    sPropertyValue = False
                                End If
                            Else
                                sPropertyValue = False
                                If sPropertyName.Trim.ToLower() = "NoTax".ToLower() Then
                                    sPropertyValue = True
                                End If
                            End If

                            If IsBoolean(sPropertyValue) Then PropertyItem.SetValue(fillObject, CBool(sPropertyValue), Nothing) 'Typecasting to boolean before setting the value

                        Case "BACRM.BusinessLogic.Contacts.CContacts+enmAddressType"
                            PropertyItem.SetValue(fillObject, CType(sPropertyValue, BACRM.BusinessLogic.Contacts.CContacts.enmAddressType), Nothing)

                        Case "BACRM.BusinessLogic.Contacts.CContacts+enmAddressOf"
                            PropertyItem.SetValue(fillObject, CType(sPropertyValue, BACRM.BusinessLogic.Contacts.CContacts.enmAddressOf), Nothing)

                    End Select
                    Exit Sub
                End If
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub SetProperties(ByRef objLeadBoxData As FormGenericLeadBox, ByVal sPropertyName As String, ByVal sPropertyValue As String)
        Try
            Dim theType As Type = objLeadBoxData.GetType
            Dim myProperties() As PropertyInfo = theType.GetProperties((BindingFlags.Public Or BindingFlags.Instance))
            Dim fillObject As Object = objLeadBoxData
            Dim PropertyItem As PropertyInfo
            For Each PropertyItem In myProperties                                                       'Loop thru Writeonly public properties
                With PropertyItem
                    If PropertyItem.Name = sPropertyName Then                                           'Compare the class property name with that of the db column name
                        Select Case PropertyItem.PropertyType.ToString()                                'Selecting the datataype of teh property
                            Case "System.String" : PropertyItem.SetValue(fillObject, sPropertyValue, Nothing)
                            Case "System.Int64", "System.Int32", "System.Int16"                         'Integer type properties
                                If IsNumeric(sPropertyValue) Then PropertyItem.SetValue(fillObject, CCommon.ToInteger(sPropertyValue), Nothing) 'Typecasting to integer before setting the value
                            Case "System.Boolean"                                                       'Boolean properties
                                If IsBoolean(sPropertyValue) Then PropertyItem.SetValue(fillObject, CBool(sPropertyValue), Nothing) 'Typecasting to boolean before setting the value
                        End Select
                        Exit Sub
                    End If
                End With
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function IsBoolean(ByVal sTrg As String) As Boolean
        Try
            Dim intValue As Boolean = Convert.ToBoolean(sTrg)                                   'Try Casting to a boolean
        Catch Ex As InvalidCastException
            Return False                                                                        'if it throws an error then return false to indicate that it is non boolean
        End Try
        Return True                                                                             'Test passed, its booelan
    End Function

#End Region

#Region "WRITE MESSAGE"

    Private Sub WriteMessage(ByVal Message As String)
        Try
            WebAPI.WriteMessage(Message, System.Configuration.ConfigurationManager.AppSettings("LogFilePath"))

            If Not String.IsNullOrEmpty(strImportFileName) AndAlso DomainID > 0 Then
                WriteImportMessage(Message, CCommon.GetDocumentPhysicalPath(DomainID) & "Import_Log_" & intImportFileID)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub WriteImportMessage(ByVal Message As String, ByVal LogFilePath As String)
        Dim TimeStamp As String = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString() + " --- "
        Dim FileName As String = LogFilePath + ".txt"

        Dim nTries As Integer = 0
        While True
            Try
                nTries = nTries + 1
                Dim sw As New StreamWriter(FileName, True)
                sw.WriteLine(TimeStamp + Message)
                sw.Flush()
                sw.Close()
                sw = Nothing
                Exit Sub
            Catch ex As Exception
                If nTries >= 5 Then
                    Return
                End If
                Threading.Thread.Sleep(500)
            End Try

        End While

    End Sub

#End Region

#Region "DATA ROLLBACK UTILITY SUBROUTINES/FUNCTIONS"

    Public Sub RollBackImportData()
        Try

            WriteMessage("RollBack Process Started.")
            'TO DEBUG : TEMP CODE CHECK BLOCK
            'For i As Long = 0 To 1000000000
            '    WriteMessage("dsfdsfds")
            'Next
            '---------------------------------------------------

            Dim strMsgDetail As String = ""
            Dim dsRollback As New DataSet
            Dim intRlbkCnt As Integer = 0
            Dim objClass As Object
            Dim intRecords As Integer = 0
            Dim strItemId As String()

            objImport = New ImportWizard
            dsRollback = objImport.GetImportDataForRollBack()

            If dsRollback IsNot Nothing AndAlso dsRollback.Tables.Count > 0 AndAlso dsRollback.Tables(0).Rows.Count > 0 Then

                'Defining counter to process records as per need
                Dim intCounter As Long = 0

                For intRlbkCnt = 0 To dsRollback.Tables(0).Rows.Count - 1

                    intImportFileID = CCommon.ToLong(dsRollback.Tables(0).Rows(intRlbkCnt)("intImportFileID"))
                    DomainID = CCommon.ToLong(dsRollback.Tables(0).Rows(intRlbkCnt)("numDomainID"))
                    UserCntID = CCommon.ToLong(dsRollback.Tables(0).Rows(intRlbkCnt)("numUserContactID"))
                    lngMasterID = CCommon.ToLong(dsRollback.Tables(0).Rows(intRlbkCnt)("numMasterID"))
                    strImportFileName = CCommon.ToString(dsRollback.Tables(0).Rows(intRlbkCnt)("vcImportFileName"))

                    objLead = New CLeads
                    objLead.ContactID = UserCntID
                    objLead.ShowAll = 0
                    Dim dtEmail As DataTable = objLead.GetContactInfoPromote()
                    If dtEmail IsNot Nothing AndAlso dtEmail.Rows.Count > 0 Then
                        strEmail = CCommon.ToString(dtEmail.Rows(0)("vcEmail"))
                        strName = CCommon.ToString(dtEmail.Rows(0)("Name"))
                    Else
                        WriteMessage("User email is not available.")
                    End If

                    WriteMessage("Import File ID : " & intImportFileID)
                    WriteMessage("RollBack File Name : " & strImportFileName)
                    WriteMessage("Domain ID : " & DomainID)
                    WriteMessage("User Contact ID : " & UserCntID)

                    If lngMasterID = IMPORT_TYPE.Item Then
                        objClass = New CItems

                        strItemId = dsRollback.Tables(0).Rows(intRlbkCnt)("ItemCodes").Split(",")

                        For i As Integer = 0 To strItemId.Length - 1

                            If intCounter = lngWaitRecords Then
                                intCounter = 0
                                WriteMessage("Thread Sleep for " & (lngWaitMiliSeconds) & " miliseconds.")
                                Thread.Sleep(lngWaitMiliSeconds)
                            End If

                            Try
                                objClass.ItemCode = CCommon.ToLong(strItemId(i))
                                'objClass.ItemCode = CCommon.ToSqlDate(strItemId(i))
                                objClass.DomainID = DomainID
                                Dim dtItemImages As DataTable
                                dtItemImages = objClass.ImageItemDetails()
                                If objClass.DeleteItems = False Then
                                    intRecords = intRecords + 1
                                    'WriteMessage("Dependent Records Exists. Cannot Be deleted.")

                                    strErrorDetail.Append("<p>" & "Dependent Records Exists. Cannot Be deleted." & "</p>")
                                    WriteMessage("Dependent Records Exists. Cannot Be deleted.")
                                    'WriteMessage(strErrorDetail)
                                Else
                                    If dtItemImages.Rows.Count > 0 Then
                                        For Each row As DataRow In dtItemImages.Rows
                                            If row("vcPathForImage") <> "" Then
                                                If File.Exists(CCommon.GetDocumentPhysicalPath(DomainID) & row("vcPathForImage")) Then
                                                    File.Delete(CCommon.GetDocumentPhysicalPath(DomainID) & row("vcPathForImage"))
                                                End If
                                            End If

                                            If row("vcPathForTImage") <> "" Then
                                                If File.Exists(CCommon.GetDocumentPhysicalPath(DomainID) & row("vcPathForTImage")) Then
                                                    File.Delete(CCommon.GetDocumentPhysicalPath(DomainID) & row("vcPathForTImage"))
                                                End If
                                            End If
                                        Next
                                    End If
                                End If

                                WriteMessage("-----------------------")
                                WriteMessage("Item ID : " & CCommon.ToLong(strItemId(i)))
                                WriteMessage("-----------------------")

                            Catch ex As Exception
                                WriteMessage("----- Error Details ----")
                                WriteMessage("Item Error ID : " & CCommon.ToLong(strItemId(i)))
                                WriteMessage("Error Message: " & ex.Message)
                                WriteMessage("Error StackTrace: " & ex.StackTrace.ToString())
                                WriteMessage("-----------------------")

                                strErrorDetail.Append("<p>" & "-----------------------" & "</p>" & _
                                                      "<p>" & "Item Error ID : " & CCommon.ToLong(strItemId(i)) & "</p>" & _
                                                      "<p>" & ex.Message.ToString() & ":" & ex.StackTrace.ToString() & "</p>" & _
                                                      "<p>" & "-----------------------" & "</p>")
                            End Try

                            intCounter = intCounter + 1
                        Next

                    ElseIf lngMasterID = CCommon.ToInteger(IMPORT_TYPE.Item_WareHouse) Then

                        objClass = New CItems
                        strItemId = dsRollback.Tables(0).Rows(intRlbkCnt)("ItemCodes").Split(",")
                        For i As Integer = 0 To strItemId.Length - 1

                            If intCounter = lngWaitRecords Then
                                intCounter = 0
                                WriteMessage("Thread Sleep for " & (lngWaitMiliSeconds) & " miliseconds.")
                                Thread.Sleep(lngWaitMiliSeconds)
                            End If

                            objClass.WareHouseItemID = CCommon.ToLong(strItemId(i))
                            objClass.DomainID = DomainID
                            objClass.byteMode = 3

                            Try
                                objClass.AddUpdateWareHouseForItems()

                                WriteMessage("-----------------------")
                                WriteMessage("Item ID : " & CCommon.ToLong(strItemId(i)))
                                WriteMessage("-----------------------")

                            Catch ex As Exception
                                intRecords = intRecords + 1
                                WriteMessage("----- Error Details ----")
                                WriteMessage("Item Error ID : " & CCommon.ToLong(strItemId(i)))
                                WriteMessage("Error Message: " & ex.Message)
                                WriteMessage("Error StackTrace: " & ex.StackTrace.ToString())
                                WriteMessage("-----------------------")

                                strErrorDetail.Append("<p>" & "-----------------------" & "</p>" & _
                                                      "<p>" & "Item Error ID : " & CCommon.ToLong(strItemId(i)) & "</p>" & _
                                                      "<p>" & ex.Message.ToString() & ":" & ex.StackTrace.ToString() & "</p>" & _
                                                      "<p>" & "-----------------------" & "</p>")
                            End Try

                            intCounter = intCounter + 1
                        Next

                    ElseIf lngMasterID = CCommon.ToInteger(IMPORT_TYPE.Item_Images) Then

                        objClass = New CItems
                        strItemId = dsRollback.Tables(0).Rows(intRlbkCnt)("ItemCodes").Split(",")
                        For i As Integer = 0 To strItemId.Length - 1

                            If intCounter = lngWaitRecords Then
                                intCounter = 0
                                WriteMessage("Thread Sleep for " & (lngWaitMiliSeconds) & " miliseconds.")
                                Thread.Sleep(lngWaitMiliSeconds)
                            End If

                            Dim blnResult As Boolean = False
                            objClass.DomainID = DomainID
                            objClass.numItemImageId = CCommon.ToLong(strItemId(i))
                            Try
                                blnResult = objClass.DeleteItemImages(strItemId(i))

                                WriteMessage("-----------------------")
                                WriteMessage("Item ID : " & CCommon.ToLong(strItemId(i)))
                                WriteMessage("-----------------------")

                            Catch ex As Exception
                                intRecords = intRecords + 1
                                WriteMessage("----- Error Details ----")
                                WriteMessage("Item Error ID : " & CCommon.ToLong(strItemId(i)))
                                WriteMessage("Error Message: " & ex.Message)
                                WriteMessage("Error StackTrace: " & ex.StackTrace.ToString())
                                WriteMessage("-----------------------")

                                strErrorDetail.Append("<p>" & "-----------------------" & "</p>" & _
                                                      "<p>" & "Item Error ID : " & CCommon.ToLong(strItemId(i)) & "</p>" & _
                                                      "<p>" & ex.Message.ToString() & ":" & ex.StackTrace.ToString() & "</p>" & _
                                                      "<p>" & "-----------------------" & "</p>")
                            End Try

                            intCounter = intCounter + 1
                        Next

                    ElseIf lngMasterID = CCommon.ToInteger(IMPORT_TYPE.Item_Assembly) Then

                        objClass = New CItems
                        strItemId = dsRollback.Tables(0).Rows(intRlbkCnt)("ItemCodes").Split(",")
                        For i As Integer = 0 To strItemId.Length - 1

                            If intCounter = lngWaitRecords Then
                                intCounter = 0
                                WriteMessage("Thread Sleep for " & (lngWaitMiliSeconds) & " miliseconds.")
                                Thread.Sleep(lngWaitMiliSeconds)
                            End If

                            Dim blnResult As Boolean = False
                            objClass.ItemDetailID = CCommon.ToLong(strItemId(i))
                            Try
                                blnResult = objClass.DeleteKitItems()

                                WriteMessage("-----------------------")
                                WriteMessage("Item ID : " & CCommon.ToLong(strItemId(i)))
                                WriteMessage("-----------------------")

                            Catch ex As Exception
                                intRecords = intRecords + 1
                                WriteMessage("----- Error Details ----")
                                WriteMessage("Item Error ID : " & CCommon.ToLong(strItemId(i)))
                                WriteMessage("Error Message: " & ex.Message)
                                WriteMessage("Error StackTrace: " & ex.StackTrace.ToString())
                                WriteMessage("-----------------------")

                                strErrorDetail.Append("<p>" & "-----------------------" & "</p>" & _
                                                        "<p>" & "Item Error ID : " & CCommon.ToLong(strItemId(i)) & "</p>" & _
                                                        "<p>" & ex.Message.ToString() & ":" & ex.StackTrace.ToString() & "</p>" & _
                                                        "<p>" & "-----------------------" & "</p>")

                            End Try

                            intCounter = intCounter + 1
                        Next

                    ElseIf lngMasterID = CCommon.ToInteger(IMPORT_TYPE.Item_Vendor) Then

                        objClass = New CItems
                        strItemId = dsRollback.Tables(0).Rows(intRlbkCnt)("ItemCodes").Split(",")
                        For i As Integer = 0 To strItemId.Length - 1

                            If intCounter = lngWaitRecords Then
                                intCounter = 0
                                WriteMessage("Thread Sleep for " & (lngWaitMiliSeconds) & " miliseconds.")
                                Thread.Sleep(lngWaitMiliSeconds)
                            End If

                            objClass.VendorTcode = CCommon.ToLong(strItemId(i))
                            objClass.mode = True
                            Try
                                objClass.ManageItemVendor()

                                WriteMessage("-----------------------")
                                WriteMessage("Item ID : " & CCommon.ToLong(strItemId(i)))
                                WriteMessage("-----------------------")

                            Catch ex As Exception
                                intRecords = intRecords + 1
                                WriteMessage("----- Error Details ----")
                                WriteMessage("Item Error ID : " & CCommon.ToLong(strItemId(i)))
                                WriteMessage("Error Message: " & ex.Message)
                                WriteMessage("Error StackTrace: " & ex.StackTrace.ToString())
                                WriteMessage("-----------------------")

                                strErrorDetail.Append("<p>" & "-----------------------" & "</p>" & _
                                                      "<p>" & "Item Error ID : " & CCommon.ToLong(strItemId(i)) & "</p>" & _
                                                      "<p>" & ex.Message.ToString() & ":" & ex.StackTrace.ToString() & "</p>" & _
                                                      "<p>" & "-----------------------" & "</p>")
                            End Try

                            intCounter = intCounter + 1
                        Next

                    ElseIf lngMasterID = CCommon.ToInteger(IMPORT_TYPE.Organization) Then

                        objClass = New CAccounts
                        strItemId = dsRollback.Tables(0).Rows(intRlbkCnt)("ItemCodes").Split(",")

                        For i As Integer = 0 To strItemId.Length - 1

                            If intCounter = lngWaitRecords Then
                                intCounter = 0
                                WriteMessage("Thread Sleep for " & (lngWaitMiliSeconds) & " miliseconds.")
                                Thread.Sleep(lngWaitMiliSeconds)
                            End If

                            Dim blnResult As Boolean = False
                            objClass.DivisionID = CCommon.ToLong(strItemId(i))
                            objClass.DomainID = DomainID
                            Try
                                Dim strResult As String = ""
                                strResult = objClass.DeleteOrg()

                                If strResult = "" Then
                                    blnResult = True
                                Else
                                    WriteMessage(strResult)
                                End If

                                WriteMessage("-----------------------")
                                WriteMessage("Item ID : " & CCommon.ToLong(strItemId(i)))
                                WriteMessage("-----------------------")

                            Catch ex As Exception
                                intRecords = intRecords + 1
                                WriteMessage("----- Error Details ----")
                                WriteMessage("Item Error ID : " & CCommon.ToLong(strItemId(i)))
                                WriteMessage("Error Message: " & ex.Message)
                                WriteMessage("Error StackTrace: " & ex.StackTrace.ToString())
                                WriteMessage("-----------------------")

                                strErrorDetail.Append("<p>" & "-----------------------" & "</p>" & _
                                                      "<p>" & "Item Error ID : " & CCommon.ToLong(strItemId(i)) & "</p>" & _
                                                      "<p>" & ex.Message.ToString() & ":" & ex.StackTrace.ToString() & "</p>" & _
                                                      "<p>" & "-----------------------" & "</p>")
                            End Try

                            intCounter = intCounter + 1
                        Next

                    ElseIf lngMasterID = CCommon.ToInteger(IMPORT_TYPE.Organization_Correspondence) Then
                        objClass = New ActionItem
                        strItemId = dsRollback.Tables(0).Rows(intRlbkCnt)("ItemCodes").Split(",")

                        If objClass Is Nothing Then objClass = New ActionItem
                        For i As Integer = 0 To strItemId.Length - 1

                            If intCounter = lngWaitRecords Then
                                intCounter = 0
                                WriteMessage("Thread Sleep for " & (lngWaitMiliSeconds) & " miliseconds.")
                                Thread.Sleep(lngWaitMiliSeconds)
                            End If

                            Dim blnResult As Boolean = False
                            objClass.CommID = CCommon.ToLong(strItemId(i))
                            Try
                                blnResult = objClass.DeleteActionItem()

                                WriteMessage("-----------------------")
                                WriteMessage("Item ID : " & CCommon.ToLong(strItemId(i)))
                                WriteMessage("-----------------------")

                            Catch ex As Exception
                                intRecords = intRecords + 1
                                WriteMessage("----- Error Details ----")
                                WriteMessage("Item Error ID : " & CCommon.ToLong(strItemId(i)))
                                WriteMessage("Error Message: " & ex.Message)
                                WriteMessage("Error StackTrace: " & ex.StackTrace.ToString())
                                WriteMessage("-----------------------")

                                strErrorDetail.Append("<p>" & "-----------------------" & "</p>" & _
                                                        "<p>" & "Item Error ID : " & CCommon.ToLong(strItemId(i)) & "</p>" & _
                                                        "<p>" & ex.Message.ToString() & ":" & ex.StackTrace.ToString() & "</p>" & _
                                                        "<p>" & "-----------------------" & "</p>")

                            End Try

                            intCounter = intCounter + 1
                        Next
                    ElseIf lngMasterID = CCommon.ToInteger(IMPORT_TYPE.Contact) Then
                        objClass = New CContacts
                        strItemId = dsRollback.Tables(0).Rows(intRlbkCnt)("ItemCodes").Split(",")
                        If objClass Is Nothing Then objClass = New CContacts

                        For i As Integer = 0 To strItemId.Length - 1
                            If intCounter = lngWaitRecords Then
                                intCounter = 0
                                WriteMessage("Thread Sleep for " & (lngWaitMiliSeconds) & " miliseconds.")
                                Thread.Sleep(lngWaitMiliSeconds)
                            End If

                            Dim blnResult As Boolean = False
                            objClass.ContactID = CCommon.ToLong(strItemId(i))
                            Try
                                objClass.RollbackImportedContact()



                                WriteMessage("-----------------------")
                                WriteMessage("Contact ID : " & CCommon.ToLong(strItemId(i)))
                                WriteMessage("-----------------------")

                            Catch ex As Exception
                                intRecords = intRecords + 1
                                WriteMessage("----- Error Details ----")
                                WriteMessage("Error Contact ID : " & CCommon.ToLong(strItemId(i)))
                                WriteMessage("Error Message: " & ex.Message)
                                WriteMessage("Error StackTrace: " & ex.StackTrace.ToString())
                                WriteMessage("-----------------------")

                                strErrorDetail.Append("<p>" & "-----------------------" & "</p>" & _
                                                      "<p>" & "Error Contact ID : " & CCommon.ToLong(strItemId(i)) & "</p>" & _
                                                      "<p>" & ex.Message.ToString() & ":" & ex.StackTrace.ToString() & "</p>" & _
                                                      "<p>" & "-----------------------" & "</p>")
                            End Try

                            intCounter = intCounter + 1
                        Next
                    ElseIf lngMasterID = CCommon.ToInteger(IMPORT_TYPE.SalesOrder) Or lngMasterID = CCommon.ToInteger(IMPORT_TYPE.PurchaseOrder) Then
                        strItemId = dsRollback.Tables(0).Rows(intRlbkCnt)("ItemCodes").Split(",")

                        For i As Integer = 0 To strItemId.Length - 1
                            If intCounter = lngWaitRecords Then
                                intCounter = 0
                                WriteMessage("Thread Sleep for " & (lngWaitMiliSeconds) & " miliseconds.")
                                Thread.Sleep(lngWaitMiliSeconds)
                            End If

                            Dim blnResult As Boolean = False
                            Dim lngOppId As Long = CCommon.ToLong(strItemId(i))
                            Try
                                Dim objOpport As New BACRM.BusinessLogic.Opportunities.COpportunities
                                With objOpport
                                    .OpportID = lngOppId
                                    .DomainID = DomainID
                                    .UserCntID = UserCntID
                                End With
                                Dim lintCount As Integer
                                lintCount = objOpport.GetAuthoritativeOpportunityCount()
                                If lintCount = 0 Then
                                    Dim objAdmin As New CAdmin
                                    objAdmin.DomainID = DomainID
                                    objAdmin.ModeType = 0
                                    objAdmin.ProjectID = lngOppId

                                    objAdmin.RemoveStagePercentageDetails()

                                    objOpport.DelOpp()
                                Else
                                    Throw New Exception("You cannot delete Order for which Authoritative BizDocs is present")
                                End If

                                WriteMessage("-----------------------")
                                WriteMessage("Contact ID : " & CCommon.ToLong(strItemId(i)))
                                WriteMessage("-----------------------")

                            Catch ex As Exception
                                intRecords = intRecords + 1
                                WriteMessage("----- Error Details ----")
                                WriteMessage("Error Contact ID : " & CCommon.ToLong(strItemId(i)))
                                If ex.Message = "DEPENDANT" Then
                                    WriteMessage("Error Message: Can not remove record, Your option is to remove Time and Expense associated with all stages from ""milestone & stages"" sub-tab and try again.")
                                ElseIf ex.Message = "RETURN_EXIST" Then
                                    WriteMessage("Error Message: Return exists against order.record can not be deleted.")
                                ElseIf ex.Message = "CASE DEPENDANT" Then
                                    WriteMessage("Error Message: Dependent case exists. Cannot be Deleted.")
                                ElseIf ex.Message = "CreditBalance DEPENDANT" Then
                                    WriteMessage("Error Message: Credit balance of current order is being used. Your option is used to credit balance of Organization from Accunting sub tab.")
                                ElseIf ex.Message = "OppItems DEPENDANT" Then
                                    WriteMessage("Error Message: Items already used. Cannot be Deleted.")
                                ElseIf ex.Message = "FY_CLOSED" Then
                                    WriteMessage("Error Message: This transaction can not be posted,Because transactions date belongs to closed financial year.")
                                ElseIf ex.Message = "RECURRING ORDER OR BIZDOC" Then
                                    WriteMessage("Error Message: Can not remove record because recurrence is set on order or on bizdoc within order.")
                                ElseIf ex.Message = "RECURRED ORDER" Then
                                    WriteMessage("Error Message: Can not remove record because it is recurred from another order")
                                ElseIf ex.Message.Contains("INVENTORY IM-BALANCE") Then
                                    WriteMessage("Error Message: BizAutomation.com has detected a problem. Wait a few seconds and attempt to delete order again")
                                ElseIf ex.Message.Contains("FULFILLED_ITEMS") Then
                                    WriteMessage("Error Message: Can not delete record because contains fulfilled items. Your option is to delete fulfillment order bizdoc.")
                                ElseIf ex.Message.Contains("SERIAL/LOT#_USED") Then
                                    WriteMessage("Error Message: Purchased serial item is used in any sales order or purchase Lot# do not have enough qty left becasuse of used in any sales order.")
                                Else
                                    WriteMessage("Error Message: " & ex.Message)
                                    WriteMessage("Error StackTrace: " & ex.StackTrace.ToString())
                                End If


                                WriteMessage("-----------------------")

                                strErrorDetail.Append("<p>" & "-----------------------" & "</p>" & _
                                                      "<p>" & "Error Contact ID : " & CCommon.ToLong(strItemId(i)) & "</p>" & _
                                                      "<p>" & ex.Message.ToString() & ":" & ex.StackTrace.ToString() & "</p>" & _
                                                      "<p>" & "-----------------------" & "</p>")
                            End Try

                            intCounter = intCounter + 1
                        Next
                    ElseIf lngMasterID = CCommon.ToInteger(IMPORT_TYPE.JournalEntries) Then
                        strItemId = dsRollback.Tables(0).Rows(intRlbkCnt)("ItemCodes").Split(",")
                        Dim objJournalEntry As New JournalEntry
                        objJournalEntry.DomainID = DomainID
                        objJournalEntry.UserCntID = UserCntID

                        For i As Integer = 0 To strItemId.Length - 1
                            If intCounter = lngWaitRecords Then
                                intCounter = 0
                                WriteMessage("Thread Sleep for " & (lngWaitMiliSeconds) & " miliseconds.")
                                Thread.Sleep(lngWaitMiliSeconds)
                            End If

                            Dim blnResult As Boolean = False

                            Try
                                objJournalEntry.JournalId = CCommon.ToLong(strItemId(i))
                                objJournalEntry.DeleteJournalEntryDetails()
               
                                WriteMessage("-----------------------")
                                WriteMessage("Journal ID : " & CCommon.ToLong(strItemId(i)))
                                WriteMessage("-----------------------")
                            Catch ex As Exception
                                intRecords = intRecords + 1
                                WriteMessage("----- Error Details ----")
                                WriteMessage("Error Journal ID : " & CCommon.ToLong(strItemId(i)))
                                WriteMessage("Error Message: " & ex.Message)
                                WriteMessage("Error StackTrace: " & ex.StackTrace.ToString())
                                WriteMessage("-----------------------")

                                strErrorDetail.Append("<p>" & "-----------------------" & "</p>" & _
                                                      "<p>" & "Error Journal ID : " & CCommon.ToLong(strItemId(i)) & "</p>" & _
                                                      "<p>" & ex.Message.ToString() & ":" & ex.StackTrace.ToString() & "</p>" & _
                                                      "<p>" & "-----------------------" & "</p>")
                            End Try

                            intCounter = intCounter + 1
                        Next
                    End If

                    objImport = New ImportWizard(intImportFileID, DomainID)
                    With objImport
                        .ImportFileID = intImportFileID
                        .ImportFileName = objImport.ImportFileName
                        .MasterID = objImport.ImportMasterID
                        .CreatedDate = objImport.CreatedDate
                        .ImportDate = DateTime.Now
                        .DomainId = DomainID
                        .UserContactID = UserCntID
                        .hasSendEmail = True 'If(intErrors = 0, True, False)
                        .MappedData = Nothing
                        .Mode = 1
                        .History_Added_Value = ""
                        .HistoryDateTime = DateTime.Now
                    End With

                    If intRecords <= 0 Then
                        objImport.ImportFileID = intImportFileID
                        objImport.DomainId = DomainID
                        objImport.UserContactID = UserCntID

                        objImport.Status = enmImportDataStatus.Import_Completed
                        objImport.SaveImportMappedData()
                        Try
                            intRecords = objImport.DeleteMappedFileData()

                            WriteMessage("-----------------------")
                            WriteMessage("Import File ID : " & intImportFileID)
                            WriteMessage("RollBack File Name : " & strImportFileName)
                            WriteMessage("Domain ID : " & DomainID)
                            WriteMessage("User Contact ID : " & UserCntID)
                            WriteMessage("-----------------------")

                        Catch ex As Exception
                            intRecords = intRecords + 1

                            WriteMessage("----- Error Details ----")
                            'WriteMessage("Import File ID : " & intImportFileID)
                            'WriteMessage("RollBack File Name : " & strImportFileName)
                            'WriteMessage("Domain ID : " & DomainID)
                            'WriteMessage("User Contact ID : " & UserCntID)
                            WriteMessage("Error Message: " & ex.Message)
                            WriteMessage("Error StackTrace: " & ex.StackTrace.ToString())
                            WriteMessage("-----------------------")

                            strErrorDetail.Append("<p>" & "-----------------------" & "</p>" & _
                                                  "<p>" & ex.Message.ToString() & ":" & ex.StackTrace.ToString() & "</p>" & _
                                                  "<p>" & "-----------------------" & "</p>")
                        End Try

                        'If intRecords > 0 Then
                        '    If File.Exists(CCommon.GetDocumentPhysicalPath(DomainID) & strImportFileName) Then
                        '        Try
                        '            File.Delete(CCommon.GetDocumentPhysicalPath(DomainID) & strImportFileName)
                        '        Catch ex As Exception
                        '            WriteMessage("Error occurred while deleting file.")
                        '            WriteMessage("Error Message: " & ex.Message)
                        '            WriteMessage("Error StackTrace: " & ex.StackTrace.ToString())
                        '        End Try

                        '    End If
                        '    WriteMessage("Records deleted successfully.")
                        'End If
                        intRecords = 0
                        strItemId = {""}
                    Else
                        objImport.Status = enmImportDataStatus.Import_Rollback_Incomplete
                        objImport.SaveImportMappedData()
                    End If

                    If String.IsNullOrEmpty(strErrorDetail.ToString()) = False Then
                        Try
                            Dim strCompanyMail As String = ""
                            Dim strCompanyName As String = ""

                            strCompanyMail = "bug@bizautomation.com"
                            strCompanyName = "BizAutomation.com"

                            'TO DO : Comment below code to use exact functionality in future.
                            If CCommon.ToBool(System.Configuration.ConfigurationManager.AppSettings("IsDebugMode")) = True Then
                                strCompanyMail = "manish@bizautomation.com"
                                strCompanyName = "BizAutomation.com"
                                strEmail = "manishanjara20@gmail.com"
                            End If
                            '-----------------------------------------
                            Dim objSendMail As New Email
                            objSendMail.SendSystemEmail("Rollback File Failed : ", _
                                                        "<p>Import File ID : " & intImportFileID & "</p>" & _
                                                        "<p>Import File Name : " & strImportFileName & "</p>" & _
                                                        "<p>Domain ID : " & DomainID & "</p>" & _
                                                        "<p>User Contact ID : " & UserCntID & "</p>" & _
                                                        "<p>Errors occurred. Kindly check log file.</p>" & _
                                                        "<p>Error Details: " & strErrorDetail.ToString() & "</p>", _
                                                        strEmail, _
                                                        System.Configuration.ConfigurationManager.AppSettings("FromAddress"), _
                                                        strCompanyMail, strCompanyName)
                        Catch ex As Exception

                            WriteMessage("Domain ID : " & DomainID)
                            WriteMessage("User Contact ID : " & UserCntID)
                            WriteMessage(ex.Message.ToString())
                            WriteMessage("Error occurred while sending bug/error email to company.")
                        End Try
                    End If
                Next
            Else

                WriteMessage("No records available to rollback.")
                Exit Sub

            End If
            WriteMessage("RollBack Process Completed.")
        Catch ex As Exception
            WriteMessage("Error Message: " & ex.Message)
            WriteMessage("Error StackTrace: " & ex.StackTrace.ToString())
            Throw ex
        End Try
    End Sub
#End Region

    Private Function SetValues(ByVal strSplitString As String, ByVal splitChar As String, ByVal dtTable As DataTable) As String
        Dim strResult As String = ""
        Try
            For Each item As String In strSplitString.Split(splitChar)
                Dim drRow() As DataRow = dtTable.Select("Value ='" & item.Trim() & "'")
                If drRow.Length > 0 Then
                    strResult = CCommon.ToString(drRow(0)("Key")).Trim() & "," & strResult
                End If
            Next

            strResult = strResult.TrimEnd(",")
        Catch ex As Exception
            Throw ex
        End Try

        Return strResult
    End Function

    Public Sub ImportItems(ByVal dtDataHold As DataTable, ByVal linkingID As Short)
        Dim intInserts As Integer = 0
        Dim intUpdates As Integer = 0
        Dim intErrors As Integer = 0
        Dim isInsert As Boolean = False
        Dim isItemDetailSaved As Boolean = False
        Dim isVendorDetailSaved As Boolean = False
        Dim isPriceLevelDetailSaved As Boolean = False

        Dim strGetPrimaryColumnName As String = "ItemCode"
        Dim strFormFieldName As String = ""
        Dim strFormFieldID As Object = Nothing
        Dim vcAssociatedControlType As String = ""
        Dim strValueString As String = ""
        Dim strImportType As String = "Item"
        Dim PropertyItem As PropertyInfo = Nothing
        Dim objClass As Object = Nothing
        Dim strPropertyName As String
        Dim objItemSave As MethodInfo = Nothing
        Dim objItemValue As New Object
        Dim intCounter As Long = 0
        Dim strAddedItemCodes As String = ""
        Dim strUpdatedItemCodes As String = ""

        Try
            Dim dtCustomField As DataTable

            For intC As Integer = If(lngStartRow = 0, 0, lngStartRow - 1) To dtDataHold.Rows.Count - 1
                lngCSVRow = intC + 2
                isItemDetailSaved = False
                isVendorDetailSaved = False
                isPriceLevelDetailSaved = False

                Try
                    If intCounter = lngWaitRecords Then
                        intCounter = 0
                        WriteMessage("Thread Sleep for " & (lngWaitMiliSeconds) & " miliseconds.")
                        Thread.Sleep(lngWaitMiliSeconds)
                    End If

                    dtCustomField = New DataTable()
                    dtCustomField.Columns.Add("Fld_ID").SetOrdinal(0)
                    dtCustomField.Columns.Add("Name").SetOrdinal(1)
                    dtCustomField.Columns.Add("Fld_Value").SetOrdinal(2)
                    dtCustomField.TableName = "CusFlds"
                    dtCustomField.AcceptChanges()

                    objClass = New CItems

                    If linkingID = 2 Then
                        If dtDataHold.Columns.Contains("281_vcSKU") Then dtDataHold.Columns("281_vcSKU").SetOrdinal(0)
                    ElseIf linkingID = 3 Then
                        If dtDataHold.Columns.Contains("203_numBarCodeId") Then dtDataHold.Columns("203_numBarCodeId").SetOrdinal(0)
                    ElseIf linkingID = 4 Then
                        If dtDataHold.Columns.Contains("193_vcModelID") Then dtDataHold.Columns("193_vcModelID").SetOrdinal(0)
                    ElseIf linkingID = 5 Then
                        If dtDataHold.Columns.Contains("237_vcWHSKU") Then dtDataHold.Columns("237_vcWHSKU").SetOrdinal(0)
                    ElseIf linkingID = 6 Then
                        If dtDataHold.Columns.Contains("236_vcBarCode") Then dtDataHold.Columns("236_vcBarCode").SetOrdinal(0)
                    Else
                        If dtDataHold.Columns.Contains("211_numItemCode") Then dtDataHold.Columns("211_numItemCode").SetOrdinal(0)
                    End If

                    Dim dvTempToSort As DataView
                    dvTempToSort = dataTableHeading.DefaultView
                    dvTempToSort.Sort = "IsPrimary DESC"
                    dataTableHeading = dvTempToSort.ToTable()


                    For intHead As Integer = 0 To dataTableHeading.Rows.Count - 1
                        If dataTableHeading.Rows(intHead)("dbFieldName") Is DBNull.Value OrElse dataTableHeading.Rows(intHead)("dbFieldName") = "" Then
                            Continue For
                        End If

                        strFormFieldName = dataTableHeading.Rows(intHead)("dbFieldName")
                        strFormFieldID = dataTableHeading.Rows(intHead)("FormFieldID")
                        strPropertyName = dataTableHeading.Rows(intHead)("vcPropertyName")

                        Dim objPropValue As Object = Nothing

                        If CBool(dataTableHeading.Rows(intHead)("bitCustomField")) = True Then
                            Dim drCustom As DataRow
                            drCustom = dtCustomField.NewRow
                            drCustom("Fld_ID") = strFormFieldID
                            drCustom("Name") = strFormFieldName

                            If CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "checkbox" Then
                                If CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "true" OrElse _
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "yes" OrElse _
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "1" OrElse _
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "y" Then
                                    drCustom("Fld_Value") = 1
                                ElseIf CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "false" OrElse _
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "no" OrElse _
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "0" OrElse _
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "n" Then
                                    drCustom("Fld_Value") = 0
                                End If
                            Else
                                drCustom("Fld_Value") = dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)
                            End If

                            dtCustomField.Rows.Add(drCustom)
                            dtCustomField.AcceptChanges()
                        Else
                            objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                        End If

                        If strFormFieldName = "numItemCode" AndAlso linkingID = 1 Then
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = DomainID
                                Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(lngMasterID, linkingID, CCommon.ToString(dtDataHold.Rows(intC)("211_numItemCode")))
                                If Not dtLinkingData Is Nothing Then
                                    objClass.ItemCode = CCommon.ToLong(dtLinkingData.Rows(0)("numItemCode"))
                                    objClass.DomainID = DomainID
                                    objClass.ClientTimeZoneOffset = 0
                                    objClass.GetItemDetailForImport()
                                End If
                            Catch ex As Exception
                                If tintImportType = 1 Then
                                    Throw
                                End If
                            End Try
                        ElseIf strFormFieldName = "vcSKU" AndAlso linkingID = 2 Then
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = DomainID
                                Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(lngMasterID, linkingID, CCommon.ToString(dtDataHold.Rows(intC)("281_vcSKU")))
                                If Not dtLinkingData Is Nothing Then
                                    objClass.ItemCode = CCommon.ToLong(dtLinkingData.Rows(0)("numItemCode"))
                                    objClass.DomainID = DomainID
                                    objClass.ClientTimeZoneOffset = 0
                                    objClass.GetItemDetailForImport()
                                End If
                            Catch ex As Exception
                                If tintImportType = 1 Then
                                    Throw
                                End If
                            End Try
                        ElseIf strFormFieldName = "numBarCodeId" AndAlso linkingID = 3 Then
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = DomainID
                                Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(lngMasterID, linkingID, CCommon.ToString(dtDataHold.Rows(intC)("203_numBarCodeId")))
                                If Not dtLinkingData Is Nothing Then
                                    objClass.ItemCode = CCommon.ToLong(dtLinkingData.Rows(0)("numItemCode"))
                                    objClass.DomainID = DomainID
                                    objClass.ClientTimeZoneOffset = 0
                                    objClass.GetItemDetailForImport()
                                End If
                            Catch ex As Exception
                                If tintImportType = 1 Then
                                    Throw
                                End If
                            End Try
                        ElseIf strFormFieldName = "vcModelID" AndAlso linkingID = 4 Then
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = DomainID
                                Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(lngMasterID, linkingID, CCommon.ToString(dtDataHold.Rows(intC)("193_vcModelID")))
                                If Not dtLinkingData Is Nothing Then
                                    objClass.ItemCode = CCommon.ToLong(dtLinkingData.Rows(0)("numItemCode"))
                                    objClass.DomainID = DomainID
                                    objClass.ClientTimeZoneOffset = 0
                                    objClass.GetItemDetailForImport()
                                Else
                                    Throw New Exception("Item_Not_Found_With_Given_ModelID")
                                End If
                            Catch ex As Exception
                                If tintImportType = 1 Then
                                    Throw
                                End If
                            End Try
                        End If

                        If CCommon.ToString(strPropertyName).Length > 0 Then
                            If strPropertyName.Contains("PriceRuleType") = True Then
                                If CCommon.ToString(objPropValue).ToLower() = "deduct from list price" Then
                                    objPropValue = 1
                                ElseIf CCommon.ToString(objPropValue).ToLower() = "add to primary vendor cost" Then
                                    objPropValue = 2
                                ElseIf CCommon.ToString(objPropValue).ToLower() = "named price" Then
                                    objPropValue = 3
                                End If
                            ElseIf strPropertyName.Contains("PriceDiscountType") = True Then
                                If CCommon.ToString(objPropValue).ToLower() = "percentage" Then
                                    objPropValue = 1
                                ElseIf CCommon.ToString(objPropValue).ToLower() = "flat amount" Then
                                    objPropValue = 2
                                ElseIf CCommon.ToString(objPropValue).ToLower() = "named price" Then
                                    objPropValue = 3
                                End If
                            End If

                            SetProperty(objClass, strPropertyName, objPropValue)
                        End If
                    Next


                    SetProperty(objClass, "DomainID", DomainID)
                    SetProperty(objClass, "UserCntID", UserCntID)


                    If tintImportType = 1 AndAlso CCommon.ToLong(DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).ItemCode) = 0 Then 'Update
                        Throw New Exception("ITEM_NOT_FOUND")
                    ElseIf tintImportType = 2 Then
                        DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).ItemCode = 0
                    End If

                    If CCommon.ToLong(DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).ItemCode) > 0 Then
                        isInsert = False
                    Else
                        isInsert = True
                    End If

                    Dim objTempItem As New CItems
                    objTempItem.DomainID = CCommon.ToLong(DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).DomainID)
                    objTempItem.ItemCode = CCommon.ToLong(DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).ItemCode)

                    If DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).Assembly = True Then
                        DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).KitParent = False
                    End If

                    If CCommon.ToBool(DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).IsMatrix) Then
                        Dim groupID As Long = CCommon.ToLong(DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).ItemGroupID)
                        If groupID > 0 Then
                            Dim dtItemExistingAttributes As DataTable = Nothing

                            'CHECK IF REQUIRED ATTRIBUTES ARE PROVIDED TO CREATE ITEM LEVEL MATRIX ITEM
                            Dim objItem As New CItems
                            objItem.DomainID = DomainID
                            objItem.ItemGroupID = groupID
                            Dim dsAttributes As DataSet = objItem.GetItemGroups()

                            'Get Item Existing Attributes
                            If CCommon.ToLong(DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).ItemCode) > 0 Then
                                objItem.ItemCode = CCommon.ToLong(DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).ItemCode)
                                dtItemExistingAttributes = objItem.GetItemAttributes()
                            End If


                            If Not dsAttributes Is Nothing AndAlso dsAttributes.Tables.Count > 2 AndAlso dsAttributes.Tables(2).Rows.Count > 0 Then
                                Dim isDataProvided As Boolean = True

                                Dim dtCusTable As New DataTable
                                dtCusTable.Columns.Add("Fld_ID")
                                dtCusTable.Columns.Add("Fld_Value")
                                dtCusTable.TableName = "CusFlds"
                                Dim drCusRow As DataRow

                                For Each drAttr As DataRow In dsAttributes.Tables(2).Rows
                                    drCusRow = dtCusTable.NewRow
                                    drCusRow("Fld_ID") = drAttr("fld_id")

                                    If dtCustomField.Select("Fld_ID=" & drAttr("Fld_id")).Length > 0 Then
                                        Dim arrDR As DataRow() = dtCustomField.Select("Fld_ID=" & drAttr("Fld_id"))

                                        If CCommon.ToLong(arrDR(0)("Fld_Value")) > 0 Then
                                            drCusRow("Fld_Value") = CCommon.ToLong(arrDR(0)("Fld_Value"))
                                        Else
                                            isDataProvided = False
                                            Exit For
                                        End If
                                    ElseIf Not dtItemExistingAttributes Is Nothing AndAlso dtItemExistingAttributes.Select("fld_id=" & drAttr("Fld_id")).Length > 0 Then
                                        Dim arrDR As DataRow() = dtItemExistingAttributes.Select("fld_id=" & drAttr("Fld_id"))
                                        drCusRow("Fld_Value") = CCommon.ToLong(arrDR(0)("FLD_Value"))
                                    Else
                                        isDataProvided = False
                                        Exit For
                                    End If

                                    dtCusTable.Rows.Add(drCusRow)
                                    dtCusTable.AcceptChanges()
                                Next

                                If isDataProvided Then
                                    Dim ds1 As New DataSet
                                    ds1.Tables.Add(dtCusTable)
                                    SetProperty(objClass, "ItemAttributes", ds1.GetXml)
                                Else
                                    'All attributes are not provided
                                    SetProperty(objClass, "IsMatrix", False)
                                    SetProperty(objClass, "ItemGroupID", 0)
                                End If

                            Else
                                'Item Group is not configured
                                SetProperty(objClass, "IsMatrix", False)
                                SetProperty(objClass, "ItemGroupID", 0)
                            End If
                        Else
                            SetProperty(objClass, "IsMatrix", False)
                        End If
                    End If

                    Using objTransaction As New System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted, .Timeout = System.Transactions.TransactionManager.MaximumTimeout})
                        objItemSave = objClass.GetType().GetMethod("SaveItemForImport")
                        objItemValue = objItemSave.Invoke(objClass, New Object() {})
                        isItemDetailSaved = True

                        If CCommon.ToDouble(DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).VendorID) > 0 Then
                            objItemSave = objClass.GetType().GetMethod("ManageItemVendor")
                            objItemValue = objItemSave.Invoke(objClass, New Object() {})
                        End If
                        isVendorDetailSaved = True

                        'Add Price Level Details for Item
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%monPriceLevel1'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel1 = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%monPriceLevel2'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel2 = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%monPriceLevel3'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel3 = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%monPriceLevel4'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel4 = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%monPriceLevel5'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel5 = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%monPriceLevel6'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel6 = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%monPriceLevel7'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel7 = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%monPriceLevel8'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel8 = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%monPriceLevel9'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel9 = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%monPriceLevel10'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel10 = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%monPriceLevel11'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel11 = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%monPriceLevel12'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel12 = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%monPriceLevel13'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel13 = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%monPriceLevel14'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel14 = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%monPriceLevel15'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel15 = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%monPriceLevel16'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel16 = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%monPriceLevel17'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel17 = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%monPriceLevel18'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel18 = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%monPriceLevel19'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel19 = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%monPriceLevel20'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel20 = -1
                        End If


                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel1FromQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel1QtyFrom = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel1ToQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel1QtyTo = -1
                        End If

                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel2FromQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel2QtyFrom = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel2ToQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel2QtyTo = -1
                        End If

                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel3FromQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel3QtyFrom = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel3ToQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel3QtyTo = -1
                        End If

                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel4FromQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel4QtyFrom = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel4ToQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel4QtyTo = -1
                        End If

                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel5FromQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel5QtyFrom = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel5ToQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel5QtyTo = -1
                        End If

                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel6FromQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel6QtyFrom = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel6ToQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel6QtyTo = -1
                        End If

                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel7FromQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel7QtyFrom = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel7ToQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel7QtyTo = -1
                        End If

                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel8FromQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel8QtyFrom = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel8ToQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel8QtyTo = -1
                        End If

                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel9FromQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel9QtyFrom = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel9ToQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel9QtyTo = -1
                        End If

                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel10FromQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel10QtyFrom = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel10ToQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel10QtyTo = -1
                        End If

                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel11FromQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel11QtyFrom = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel11ToQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel11QtyTo = -1
                        End If

                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel12FromQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel12QtyFrom = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel12ToQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel12QtyTo = -1
                        End If

                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel13FromQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel13QtyFrom = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel13ToQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel13QtyTo = -1
                        End If

                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel14FromQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel14QtyFrom = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel14ToQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel14QtyTo = -1
                        End If

                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel15FromQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel15QtyFrom = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel15ToQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel15QtyTo = -1
                        End If

                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel16FromQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel16QtyFrom = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel16ToQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel16QtyTo = -1
                        End If

                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel17FromQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel17QtyFrom = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel17ToQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel17QtyTo = -1
                        End If

                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel18FromQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel18QtyFrom = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel18ToQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel18QtyTo = -1
                        End If

                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel19FromQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel19QtyFrom = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel19ToQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel19QtyTo = -1
                        End If

                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel20FromQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel20QtyFrom = -1
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%intPriceLevel20ToQty'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel20QtyTo = -1
                        End If

                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%tintRuleType'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceRuleType = -1
                        End If

                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%tintDiscountType'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceDiscountType = -1
                        End If

                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%vcPriceLevel1Name'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel1Name = "-1"
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%vcPriceLevel2Name'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel2Name = "-1"
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%vcPriceLevel3Name'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel3Name = "-1"
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%vcPriceLevel4Name'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel4Name = "-1"
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%vcPriceLevel5Name'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel5Name = "-1"
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%vcPriceLevel6Name'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel6Name = "-1"
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%vcPriceLevel7Name'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel7Name = "-1"
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%vcPriceLevel8Name'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel8Name = "-1"
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%vcPriceLevel9Name'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel9Name = "-1"
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%vcPriceLevel10Name'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel10Name = "-1"
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%vcPriceLevel11Name'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel11Name = "-1"
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%vcPriceLevel12Name'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel12Name = "-1"
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%vcPriceLevel13Name'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel13Name = "-1"
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%vcPriceLevel14Name'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel14Name = "-1"
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%vcPriceLevel15Name'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel15Name = "-1"
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%vcPriceLevel16Name'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel16Name = "-1"
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%vcPriceLevel17Name'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel17Name = "-1"
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%vcPriceLevel18Name'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel18Name = "-1"
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%vcPriceLevel19Name'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel19Name = "-1"
                        End If
                        If dataTableHeading.Select("vcOrigDBColumnName LIKE '%vcPriceLevel20Name'").Length = 0 Then
                            DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).PriceLevel20Name = "-1"
                        End If

                        objItemSave = objClass.GetType().GetMethod("ImportItemsPriceLevel")
                        objItemValue = objItemSave.Invoke(objClass, New Object() {})
                        isPriceLevelDetailSaved = True

                        If dtCustomField.Rows.Count > 0 Then
                            For intCustomFieldCnt As Integer = 0 To dtCustomField.Rows.Count - 1

                                objImport.RecId = CCommon.ToLong(objClass.GetType().GetProperty(strGetPrimaryColumnName).GetValue(objClass, Nothing))
                                objImport.FldId = CCommon.ToLong(dtCustomField.Rows(intCustomFieldCnt)("Fld_ID"))
                                objImport.FldValue = CCommon.ToString(dtCustomField.Rows(intCustomFieldCnt)("Fld_Value"))
                                objImport.UpdateCustomeField() 'Function just delete(if exist) and Inserts value

                            Next
                            dtCustomField.Rows.Clear()
                        End If

                        objTransaction.Complete()
                    End Using

                    objCommon.Mode = 36
                    objCommon.UpdateRecordID = intImportFileID
                    objCommon.UpdateValueID = lngCSVRow
                    objCommon.DomainID = DomainID
                    objCommon.UpdateSingleFieldValue()

                    Dim lngItemCode As Long = objClass.GetType().GetProperty(strGetPrimaryColumnName).GetValue(objClass, Nothing)


                    Dim message As String
                    If isInsert Then
                        'message = "CSV Row number: " & lngCSVRow & " - Item inserted with " & strGetPrimaryColumnName & ":" & lngItemCode
                        strAddedItemCodes = lngItemCode & "," & strAddedItemCodes
                    Else
                        'message = "CSV Row number: " & lngCSVRow & " - Item updated with " & strGetPrimaryColumnName & ":" & lngItemCode
                        strUpdatedItemCodes = lngItemCode & "," & strUpdatedItemCodes
                    End If
                    'WriteMessage(message)


                    If isInsert Then
                        intInserts = intInserts + 1
                    Else
                        intUpdates = intUpdates + 1
                    End If

                    Try
                        objImport = New ImportWizard(intImportFileID, DomainID)
                        With objImport
                            .ImportFileID = intImportFileID
                            .SaveImportFileReport(lngCSVRow, message, lngItemCode, "", "")
                        End With
                    Catch exM As Exception
                        'DO NOT THROW ERROR
                    End Try
                Catch ex As Exception
                    Dim errorMessage As String

                    If (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ITEM_NAME_NOT_SELECTED")) Or ex.Message.Contains("ITEM_NAME_NOT_SELECTED") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Item Name is Empty)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ITEM_TYPE_NOT_SELECTED")) Or ex.Message.Contains("ITEM_TYPE_NOT_SELECTED") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Item Type is Empty or Invalid)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("INVALID_COGS_ACCOUNT")) Or ex.Message.Contains("INVALID_COGS_ACCOUNT") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Invalid Cost of Goods Sold Account)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("INVALID_ASSET_ACCOUNT")) Or ex.Message.Contains("INVALID_ASSET_ACCOUNT") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Invalid Asset Account)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("INVALID_INCOME_ACCOUNT")) Or ex.Message.Contains("INVALID_INCOME_ACCOUNT") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Invalid Income Account)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ITEM_NOT_FOUND_WITH_GIVEN_ITEMCODE")) Or ex.Message.Contains("ITEM_NOT_FOUND_WITH_GIVEN_ITEMCODE") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (No item matched to item code for update)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("MULTIPLE_ITEMS_FOUND_WITH_GIVEN_ITEMCODE")) Or ex.Message.Contains("MULTIPLE_ITEMS_FOUND_WITH_GIVEN_ITEMCODE") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Multiple items matched to item code for update)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ITEM_NOT_FOUND_WITH_GIVEN_SKU")) Or ex.Message.Contains("ITEM_NOT_FOUND_WITH_GIVEN_SKU") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (No item matched to SKU for update)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("MULTIPLE_ITEMS_FOUND_WITH_GIVEN_SKU")) Or ex.Message.Contains("MULTIPLE_ITEMS_FOUND_WITH_GIVEN_SKU") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Multiple items matched to SKU for update)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ITEM_NOT_FOUND_WITH_GIVEN_UPC")) Or ex.Message.Contains("ITEM_NOT_FOUND_WITH_GIVEN_UPC") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (No item matched to UPC for update)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("MULTIPLE_ITEMS_NOT_FOUND_WITH_GIVEN_UPC")) Or ex.Message.Contains("MULTIPLE_ITEMS_NOT_FOUND_WITH_GIVEN_UPC") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Multiple items matched to UPC for update)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ITEM_NOT_FOUND_WITH_GIVEN_ModelID")) Or ex.Message.Contains("ITEM_NOT_FOUND_WITH_GIVEN_ModelID") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (No item matched to Model ID for update)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("MULTIPLE_ITEMS_NOT_FOUND_WITH_GIVEN_ModelID")) Or ex.Message.Contains("MULTIPLE_ITEMS_NOT_FOUND_WITH_GIVEN_ModelID") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Multiple items matched to Model ID for update)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ITEM_NOT_FOUND")) Or ex.Message.Contains("ITEM_NOT_FOUND") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (No item matched to Linking ID)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("INVALID_PRICE_RULE_TYPE")) Or ex.Message.Contains("INVALID_PRICE_RULE_TYPE") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Invalid Price Rule Type)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("INVALID_PRICE_LEVEL_DISCOUNT_TYPE")) Or ex.Message.Contains("INVALID_PRICE_LEVEL_DISCOUNT_TYPE") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Invalid Price Level Discount Type)"
                    Else
                        If Not isItemDetailSaved Then
                            errorMessage = "CSV Row number: " & lngCSVRow & " - Unknown error occured while saving item details"
                        ElseIf Not isVendorDetailSaved Then
                            errorMessage = "CSV Row number: " & lngCSVRow & " - Unknown error occured while saving vendor details"
                        ElseIf Not isPriceLevelDetailSaved Then
                            errorMessage = "CSV Row number: " & lngCSVRow & " - Unknown error occured while saving item price level details"
                        Else
                            errorMessage = "CSV Row number: " & lngCSVRow & " - Unknown error occured however item details is saved successfully"
                        End If
                    End If

                    WriteMessage(errorMessage)

                    Try
                        objImport = New ImportWizard(intImportFileID, DomainID)
                        With objImport
                            .ImportFileID = intImportFileID
                            .SaveImportFileReport(lngCSVRow, errorMessage, 0, If(Not ex.InnerException Is Nothing, ex.InnerException.Message, ex.Message), If(Not ex.InnerException Is Nothing, ex.InnerException.StackTrace, ex.StackTrace))
                        End With
                    Catch exM As Exception
                        'DO NOT THROW ERROR
                    End Try

                    intErrors = intErrors + 1
                End Try

                intCounter = intCounter + 1
            Next

            strAddedItemCodes = strAddedItemCodes.Trim(",")
            strUpdatedItemCodes = strUpdatedItemCodes.Trim(",")

            UpdateImportFileStatus(intInserts, intUpdates, intErrors, strAddedItemCodes, strUpdatedItemCodes, strImportType)

            WriteMessage("End Time :" & DateTime.Now)
            WriteMessage("Import Duration (In Seconds) :" & DateDiff(DateInterval.Second, dtStartTime, DateTime.Now))
            WriteMessage(strImportType & " records created: " & intInserts & If(strAddedItemCodes.Length > 0, " : " & strAddedItemCodes, ""))
            WriteMessage(strImportType & " records Updated: " & intUpdates & If(strUpdatedItemCodes.Length > 0, " : " & strUpdatedItemCodes, ""))
            WriteMessage(strImportType & " records have Errors: " & intErrors)
        Catch ex As Exception

        End Try
    End Sub

    Public Sub ImportOrganizations(ByVal dtDataHold As DataTable, ByVal linkingID As Short)
        Dim intInserts As Integer = 0
        Dim intUpdates As Integer = 0
        Dim intErrors As Integer = 0
        Dim isInsert As Boolean = False

        Dim strGetPrimaryColumnName As String = "DivisionID"
        Dim strFormFieldName As String = ""
        Dim strFormFieldID As Object = Nothing
        Dim vcAssociatedControlType As String = ""
        Dim strValueString As String = ""
        Dim strImportType As String = "Organization"
        Dim PropertyItem As PropertyInfo = Nothing
        Dim objClass As Object = Nothing
        Dim strPropertyName As String
        Dim objItemSave As MethodInfo = Nothing
        Dim objItemValue As New Object
        Dim intCounter As Long = 0
        Dim strAddedOrganizationCodes As String = ""
        Dim strUpdatedOrganizationCodes As String = ""

        Try
            Dim dtCustomField As New DataTable
            dtCustomField.Columns.Add("Fld_ID").SetOrdinal(0)
            dtCustomField.Columns.Add("Name").SetOrdinal(1)
            dtCustomField.Columns.Add("Fld_Value").SetOrdinal(2)
            dtCustomField.TableName = "CusFlds"
            dtCustomField.AcceptChanges()

            For intC As Integer = If(lngStartRow = 0, 0, lngStartRow - 1) To dtDataHold.Rows.Count - 1
                lngCSVRow = intC + 2
                Try
                    If intCounter = lngWaitRecords Then
                        intCounter = 0
                        WriteMessage("Thread Sleep for " & (lngWaitMiliSeconds) & " miliseconds.")
                        Thread.Sleep(lngWaitMiliSeconds)
                    End If

                    objClass = New CLeads
                    If linkingID = 1 Then
                        If dtDataHold.Columns.Contains("380_vcNonBizCompanyID") Then dtDataHold.Columns("380_vcNonBizCompanyID").SetOrdinal(0)
                    Else
                        If dtDataHold.Columns.Contains("539_numDivisionID") Then dtDataHold.Columns("539_numDivisionID").SetOrdinal(0)
                    End If

                    Dim dvTempToSort As DataView
                    dvTempToSort = dataTableHeading.DefaultView
                    dvTempToSort.Sort = "IsPrimary DESC"
                    dataTableHeading = dvTempToSort.ToTable()


                    For intHead As Integer = 0 To dataTableHeading.Rows.Count - 1
                        If dataTableHeading.Rows(intHead)("dbFieldName") Is DBNull.Value OrElse dataTableHeading.Rows(intHead)("dbFieldName") = "" Then
                            Continue For
                        End If

                        strFormFieldName = dataTableHeading.Rows(intHead)("dbFieldName")
                        strFormFieldID = dataTableHeading.Rows(intHead)("FormFieldID")
                        strPropertyName = dataTableHeading.Rows(intHead)("vcPropertyName")

                        Dim objPropValue As Object = Nothing

                        If CBool(dataTableHeading.Rows(intHead)("bitCustomField")) = True Then
                            Dim drCustom As DataRow
                            drCustom = dtCustomField.NewRow
                            drCustom("Fld_ID") = strFormFieldID
                            drCustom("Name") = strFormFieldName

                            If CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "checkbox" Then
                                If CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "true" OrElse _
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "yes" OrElse _
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "1" OrElse _
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "y" Then
                                    drCustom("Fld_Value") = 1
                                ElseIf CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "false" OrElse _
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "no" OrElse _
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "0" OrElse _
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "n" Then
                                    drCustom("Fld_Value") = 0
                                End If
                            Else
                                drCustom("Fld_Value") = dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)
                            End If

                            dtCustomField.Rows.Add(drCustom)
                            dtCustomField.AcceptChanges()
                        Else
                            objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                        End If


                        If strFormFieldName = "vcNonBizCompanyID" AndAlso linkingID = 1 Then
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = DomainID
                                Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(lngMasterID, linkingID, CCommon.ToString(dtDataHold.Rows(intC)("380_vcNonBizCompanyID")))
                                If Not dtLinkingData Is Nothing Then
                                    objClass.DivisionID = CCommon.ToLong(dtLinkingData.Rows(0)("numDivisionID"))
                                    objClass.DomainID = DomainID
                                    objClass.GetOrganizationDetailForImport()
                                End If
                            Catch ex As Exception
                                If tintImportType = 1 Then
                                    Throw
                                End If
                            End Try
                        ElseIf strFormFieldName = "numDivisionID" AndAlso linkingID = 2 Then
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = DomainID
                                Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(lngMasterID, linkingID, CCommon.ToString(dtDataHold.Rows(intC)("539_numDivisionID")))
                                If Not dtLinkingData Is Nothing Then
                                    objClass.DivisionID = CCommon.ToLong(dtLinkingData.Rows(0)("numDivisionID"))
                                    objClass.DomainID = DomainID
                                    objClass.GetOrganizationDetailForImport()
                                End If
                            Catch ex As Exception
                                If tintImportType = 1 Then
                                    Throw
                                End If
                            End Try
                        End If

                        If CCommon.ToString(strPropertyName).Length > 0 Then
                            SetProperty(objClass, strPropertyName, objPropValue)
                        End If
                    Next


                    SetProperty(objClass, "DomainID", DomainID)
                    SetProperty(objClass, "UserCntID", UserCntID)


                    If tintImportType = 1 AndAlso CCommon.ToLong(DirectCast(objClass, BACRM.BusinessLogic.Leads.CLeads).DivisionID) = 0 Then 'Update
                        Throw New Exception("ORGANIZATION_NOT_FOUND")
                    ElseIf tintImportType = 2 Then
                        DirectCast(objClass, BACRM.BusinessLogic.Leads.CLeads).DivisionID = 0
                    End If

                    If CCommon.ToLong(DirectCast(objClass, BACRM.BusinessLogic.Leads.CLeads).DivisionID) > 0 Then
                        isInsert = False
                    Else
                        isInsert = True
                    End If

                    If objClass.DivisionID = 0 Then
                        If Not String.IsNullOrEmpty(objClass.NonBizCompanyID) AndAlso Not String.IsNullOrEmpty(objClass.CompanyName) AndAlso CCommon.ToLong(objClass.CompanyType) > 0 AndAlso CCommon.ToInteger(objClass.CRMType) > 0 Then
                            Dim dtLinkingData As DataTable = Nothing
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = DomainID
                                dtLinkingData = objCom.GetRecordByLinkingID(lngMasterID, 1, objClass.NonBizCompanyID)
                            Catch ex As Exception
                                'DO NOT THROW ERROR BECAUSE LOGIC IS JUST FOR CHECKING WHETHER OTHER RECORS DO NOT EXISTS
                            End Try

                            If dtLinkingData Is Nothing Then
                                Using objTransaction As New System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted, .Timeout = System.Transactions.TransactionManager.MaximumTimeout})
                                    objItemSave = objClass.GetType().GetMethod("CreateRecordCompanyInfo")
                                    objItemValue = objItemSave.Invoke(objClass, New Object() {})

                                    objClass.CompanyID = CType(objItemValue, Long)
                                    objClass.UpdateDefaultTax = False
                                    objItemSave = Nothing
                                    objItemSave = objClass.GetType().GetMethod("CreateRecordDivisionsInfo")
                                    objItemValue = objItemSave.Invoke(objClass, New Object() {})

                                    If objClass.NonBizCompanyID IsNot Nothing AndAlso CCommon.ToString(objClass.NonBizCompanyID) <> "" Then
                                        objItemSave = Nothing
                                        objItemSave = objClass.GetType().GetMethod("ManageImportOrganizationContacts")
                                        objItemValue = objItemSave.Invoke(objClass, New Object() {objClass.DivisionID})
                                    End If

                                    If dtCustomField.Rows.Count > 0 Then
                                        For intCustomFieldCnt As Integer = 0 To dtCustomField.Rows.Count - 1

                                            objImport.RecId = CCommon.ToLong(objClass.GetType().GetProperty(strGetPrimaryColumnName).GetValue(objClass, Nothing))
                                            objImport.FldId = CCommon.ToLong(dtCustomField.Rows(intCustomFieldCnt)("Fld_ID"))
                                            objImport.FldValue = CCommon.ToString(dtCustomField.Rows(intCustomFieldCnt)("Fld_Value"))
                                            objImport.UpdateCustomeField() 'Function just delete(if exist) and Inserts value

                                        Next
                                        dtCustomField.Rows.Clear()
                                    End If

                                    objTransaction.Complete()
                                End Using
                            Else
                                Throw New Exception("USED_NON_BIZ_COMPANY_ID")
                            End If
                        Else
                            Throw New Exception("REQUIRED_FIELDS_NOT_ADDED")
                        End If
                    Else
                        Using objTransaction As New System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted, .Timeout = System.Transactions.TransactionManager.MaximumTimeout})
                            objItemSave = objClass.GetType().GetMethod("SaveOrganizationForImport")
                            objItemValue = objItemSave.Invoke(objClass, New Object() {})

                            If dtCustomField.Rows.Count > 0 Then
                                For intCustomFieldCnt As Integer = 0 To dtCustomField.Rows.Count - 1

                                    objImport.RecId = CCommon.ToLong(objClass.GetType().GetProperty(strGetPrimaryColumnName).GetValue(objClass, Nothing))
                                    objImport.FldId = CCommon.ToLong(dtCustomField.Rows(intCustomFieldCnt)("Fld_ID"))
                                    objImport.FldValue = CCommon.ToString(dtCustomField.Rows(intCustomFieldCnt)("Fld_Value"))
                                    objImport.UpdateCustomeField() 'Function just delete(if exist) and Inserts value

                                Next
                                dtCustomField.Rows.Clear()
                            End If

                            objTransaction.Complete()
                        End Using
                    End If

                    objCommon.Mode = 36
                    objCommon.UpdateRecordID = intImportFileID
                    objCommon.UpdateValueID = lngCSVRow
                    objCommon.DomainID = DomainID
                    objCommon.UpdateSingleFieldValue()

                    Dim lngDivisionID As Long = objClass.GetType().GetProperty(strGetPrimaryColumnName).GetValue(objClass, Nothing)


                    Dim message As String
                    If isInsert Then
                        'message = "CSV Row number: " & lngCSVRow & " - Organization inserted with " & strGetPrimaryColumnName & ":" & lngDivisionID
                        strAddedOrganizationCodes = lngDivisionID & "," & strAddedOrganizationCodes
                    Else
                        'message = "CSV Row number: " & lngCSVRow & " - Organization updated with " & strGetPrimaryColumnName & ":" & lngDivisionID
                        strUpdatedOrganizationCodes = lngDivisionID & "," & strUpdatedOrganizationCodes
                    End If
                    'WriteMessage(message)


                    If isInsert Then
                        intInserts = intInserts + 1
                    Else
                        intUpdates = intUpdates + 1
                    End If

                    Try
                        objImport = New ImportWizard(intImportFileID, DomainID)
                        With objImport
                            .ImportFileID = intImportFileID
                            .SaveImportFileReport(lngCSVRow, message, lngDivisionID, "", "")
                        End With
                    Catch exM As Exception
                        'DO NOT THROW ERROR
                    End Try
                Catch ex As Exception
                    Dim errorMessage As String

                    If ex.Message.Contains("REQUIRED_FIELDS_NOT_ADDED") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("REQUIRED_FIELDS_NOT_ADDED")) Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Non Biz Company ID, Organization Name, Relationship and Relationship Type can not be blank.)"
                    ElseIf ex.Message.Contains("USED_NON_BIZ_COMPANY_ID") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("USED_NON_BIZ_COMPANY_ID")) Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Non Biz Company ID already used for other organization)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ORGANIZATION_NOT_FOUND_WITH_GIVEN_NON BIZ COMPANY ID")) Or ex.Message.Contains("ORGANIZATION_NOT_FOUND_WITH_GIVEN_NON BIZ COMPANY ID") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (No organization matched to Non Biz Company ID for update)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("MULTIPLE_ORGANIZATIONS_FOUND_WITH_GIVEN_NON BIZ COMPANY ID")) Or ex.Message.Contains("MULTIPLE_ORGANIZATIONS_FOUND_WITH_GIVEN_NON BIZ COMPANY ID") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Multiple organizations matched to Non Biz Company ID for update)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ORGANIZATION_NOT_FOUND_WITH_GIVEN_ORGANIZATION ID")) Or ex.Message.Contains("ORGANIZATION_NOT_FOUND_WITH_GIVEN_ORGANIZATION ID") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (No organization matched to Organization ID for update)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("MULTIPLE_ORGANIZATIONS_FOUND_WITH_GIVEN_ORGANIZATION ID")) Or ex.Message.Contains("MULTIPLE_ORGANIZATIONS_FOUND_WITH_GIVEN_ORGANIZATION ID") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Multiple organizations matched to Organization ID for update)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ORGANIZATION_NOT_FOUND")) Or ex.Message.Contains("ORGANIZATION_NOT_FOUND") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (No organization matched to Linking ID)"
                    Else
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Unknown error occured"
                    End If

                    WriteMessage(errorMessage)

                    Try
                        objImport = New ImportWizard(intImportFileID, DomainID)
                        With objImport
                            .ImportFileID = intImportFileID
                            .SaveImportFileReport(lngCSVRow, errorMessage, 0, If(Not ex.InnerException Is Nothing, ex.InnerException.Message, ex.Message), If(Not ex.InnerException Is Nothing, ex.InnerException.StackTrace, ex.StackTrace))
                        End With
                    Catch exM As Exception
                        'DO NOT THROW ERROR
                    End Try

                    intErrors = intErrors + 1
                End Try

                intCounter = intCounter + 1
            Next

            strAddedOrganizationCodes = strAddedOrganizationCodes.Trim(",")
            strUpdatedOrganizationCodes = strUpdatedOrganizationCodes.Trim(",")

            UpdateImportFileStatus(intInserts, intUpdates, intErrors, strAddedOrganizationCodes, strUpdatedOrganizationCodes, strImportType)

            WriteMessage("End Time :" & DateTime.Now)
            WriteMessage("Import Duration (In Seconds) :" & DateDiff(DateInterval.Second, dtStartTime, DateTime.Now))
            WriteMessage(strImportType & " records created: " & intInserts & If(strAddedOrganizationCodes.Length > 0, " : " & strAddedOrganizationCodes, ""))
            WriteMessage(strImportType & " records Updated: " & intUpdates & If(strUpdatedOrganizationCodes.Length > 0, " : " & strUpdatedOrganizationCodes, ""))
            WriteMessage(strImportType & " records have Errors: " & intErrors)
        Catch ex As Exception

        End Try
    End Sub

    Public Sub ImportContacts(ByVal dtDataHold As DataTable, ByVal linkingID As Short)
        Dim intInserts As Integer = 0
        Dim intUpdates As Integer = 0
        Dim intErrors As Integer = 0
        Dim isInsert As Boolean = False

        Dim strGetPrimaryColumnName As String = "ContactID"
        Dim strFormFieldName As String = ""
        Dim strFormFieldID As Object = Nothing
        Dim vcAssociatedControlType As String = ""
        Dim strValueString As String = ""
        Dim strImportType As String = "Contact"
        Dim PropertyItem As PropertyInfo = Nothing
        Dim objClass As Object = Nothing
        Dim strPropertyName As String
        Dim objItemSave As MethodInfo = Nothing
        Dim objItemValue As New Object
        Dim intCounter As Long = 0
        Dim strAddedContactIds As String = ""
        Dim strUpdatedContactIds As String = ""

        Try
            Dim dtCustomField As New DataTable
            dtCustomField.Columns.Add("Fld_ID").SetOrdinal(0)
            dtCustomField.Columns.Add("Name").SetOrdinal(1)
            dtCustomField.Columns.Add("Fld_Value").SetOrdinal(2)
            dtCustomField.TableName = "CusFlds"
            dtCustomField.AcceptChanges()

            For intC As Integer = If(lngStartRow = 0, 0, lngStartRow - 1) To dtDataHold.Rows.Count - 1
                Try
                    lngCSVRow = intC + 2

                    If intCounter = lngWaitRecords Then
                        intCounter = 0
                        WriteMessage("Thread Sleep for " & (lngWaitMiliSeconds) & " miliseconds.")
                        Thread.Sleep(lngWaitMiliSeconds)
                    End If

                    objClass = New CContacts
                    strGetPrimaryColumnName = "ContactID"
                    If objItemSave Is Nothing Then objItemSave = objClass.GetType().GetMethod("SaveContactForImport")
                    strImportType = "Contact"

                    If linkingID = 1 Or linkingID = 5 Or linkingID = 7 Then
                        If dtDataHold.Columns.Contains("386_vcNonBizContactID") Then dtDataHold.Columns("386_vcNonBizContactID").SetOrdinal(0)
                    Else
                        If dtDataHold.Columns.Contains("860_numContactID") Then dtDataHold.Columns("860_numContactID").SetOrdinal(0)
                    End If

                    Dim dvTempToSort As DataView
                    dvTempToSort = dataTableHeading.DefaultView
                    dvTempToSort.Sort = "IsPrimary DESC"
                    dataTableHeading = dvTempToSort.ToTable()


                    For intHead As Integer = 0 To dataTableHeading.Rows.Count - 1
                        If dataTableHeading.Rows(intHead)("dbFieldName") Is DBNull.Value OrElse dataTableHeading.Rows(intHead)("dbFieldName") = "" Then
                            Continue For
                        End If

                        strFormFieldName = dataTableHeading.Rows(intHead)("dbFieldName")
                        strFormFieldID = dataTableHeading.Rows(intHead)("FormFieldID")
                        strPropertyName = dataTableHeading.Rows(intHead)("vcPropertyName")

                        Dim objPropValue As Object = Nothing

                        If CBool(dataTableHeading.Rows(intHead)("bitCustomField")) = True Then
                            Dim drCustom As DataRow
                            drCustom = dtCustomField.NewRow
                            drCustom("Fld_ID") = strFormFieldID
                            drCustom("Name") = strFormFieldName

                            If CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "checkbox" Then
                                If CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "true" OrElse _
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "yes" OrElse _
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "1" OrElse _
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "y" Then
                                    drCustom("Fld_Value") = 1
                                ElseIf CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "false" OrElse _
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "no" OrElse _
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "0" OrElse _
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "n" Then
                                    drCustom("Fld_Value") = 0
                                End If
                            Else
                                drCustom("Fld_Value") = dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)
                            End If

                            dtCustomField.Rows.Add(drCustom)
                            dtCustomField.AcceptChanges()
                        Else
                            objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                        End If


                        If strFormFieldName = "vcNonBizContactID" AndAlso linkingID = 1 Then
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = DomainID
                                Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(lngMasterID, linkingID, CCommon.ToString(dtDataHold.Rows(intC)("386_vcNonBizContactID")))
                                If Not dtLinkingData Is Nothing Then
                                    objClass.ContactID = CCommon.ToLong(dtLinkingData.Rows(0)("numContactID"))
                                    objClass.DomainID = DomainID
                                    objClass.ClientTimeZoneOffset = 0 'Session("ClientMachineUTCTimeOffset")
                                    objClass.GetContactDetailForImport()
                                End If
                            Catch ex As Exception
                                If tintImportType = 1 Then
                                    Throw
                                End If
                            End Try
                        ElseIf strFormFieldName = "vcNonBizContactID" AndAlso (linkingID = 5 Or linkingID = 7) Then
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = DomainID
                                Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(lngMasterID, 1, CCommon.ToString(dtDataHold.Rows(intC)("386_vcNonBizContactID")))
                                If Not dtLinkingData Is Nothing Then
                                    objClass.ContactID = CCommon.ToLong(dtLinkingData.Rows(0)("numContactID"))
                                    objClass.DomainID = DomainID
                                    objClass.ClientTimeZoneOffset = 0 'Session("ClientMachineUTCTimeOffset")
                                    objClass.GetContactDetailForImport()
                                End If
                            Catch ex As Exception
                                'DO NOT THROW ERROR BECAUSE linking ID 5 and 7 are for INSERT & UPDATE
                            End Try
                        ElseIf strFormFieldName = "numContactID" AndAlso linkingID = 2 Then
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = DomainID
                                Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(lngMasterID, linkingID, CCommon.ToString(dtDataHold.Rows(intC)("860_numContactID")))
                                If Not dtLinkingData Is Nothing Then
                                    objClass.ContactID = CCommon.ToLong(dtLinkingData.Rows(0)("numContactID"))
                                    objClass.DomainID = DomainID
                                    objClass.ClientTimeZoneOffset = 0 'Session("ClientMachineUTCTimeOffset")
                                    objClass.GetContactDetailForImport()
                                End If
                            Catch ex As Exception
                                If tintImportType = 1 Then
                                    Throw
                                End If
                            End Try
                        ElseIf strFormFieldName = "numContactID" AndAlso (linkingID = 6 Or linkingID = 8) Then
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = DomainID
                                Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(lngMasterID, 2, CCommon.ToString(dtDataHold.Rows(intC)("860_numContactID")))
                                If Not dtLinkingData Is Nothing Then
                                    objClass.ContactID = CCommon.ToLong(dtLinkingData.Rows(0)("numContactID"))
                                    objClass.DomainID = DomainID
                                    objClass.ClientTimeZoneOffset = 0 'Session("ClientMachineUTCTimeOffset")
                                    objClass.GetContactDetailForImport()
                                End If
                            Catch ex As Exception
                                'DO NOT THROW ERROR BECAUSE linking ID 6 and 8 are for INSERT & UPDATE
                            End Try
                        End If


                        If strFormFieldName = "vcNonBizCompanyID" AndAlso linkingID = 3 Then
                            Dim objCom As New CCommon
                            objCom.DomainID = DomainID
                            Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(133, 1, CCommon.ToString(dtDataHold.Rows(intC)("380_vcNonBizCompanyID")))
                            If Not dtLinkingData Is Nothing Then
                                objClass.DivisionID = CCommon.ToLong(dtLinkingData.Rows(0)("numDivisionID"))
                                objClass.CompanyID = CCommon.ToLong(dtLinkingData.Rows(0)("numCompanyID"))
                            End If
                            SetProperty(objClass, strPropertyName, objPropValue)
                        ElseIf strFormFieldName = "vcNonBizCompanyID" AndAlso (linkingID = 5 Or linkingID = 6) AndAlso objClass.DivisionID = 0 Then
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = DomainID
                                Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(133, 1, CCommon.ToString(dtDataHold.Rows(intC)("380_vcNonBizCompanyID")))
                                If Not dtLinkingData Is Nothing Then
                                    objClass.DivisionID = CCommon.ToLong(dtLinkingData.Rows(0)("numDivisionID"))
                                    objClass.CompanyID = CCommon.ToLong(dtLinkingData.Rows(0)("numCompanyID"))
                                End If
                            Catch ex As Exception
                                'DO NOT THROW ERROR BECAUSE linking ID 6 and 8 are for INSERT & UPDATE
                            End Try
                            SetProperty(objClass, strPropertyName, objPropValue)
                        ElseIf strFormFieldName = "numDivisionID" AndAlso linkingID = 4 Then
                            Dim objCom As New CCommon
                            objCom.DomainID = DomainID
                            Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(133, 2, CCommon.ToString(dtDataHold.Rows(intC)("539_numDivisionID")))
                            If Not dtLinkingData Is Nothing Then
                                objClass.DivisionID = CCommon.ToLong(dtLinkingData.Rows(0)("numDivisionID"))
                                objClass.CompanyID = CCommon.ToLong(dtLinkingData.Rows(0)("numCompanyID"))
                            End If
                            SetProperty(objClass, strPropertyName, objPropValue)
                        ElseIf strFormFieldName = "numDivisionID" AndAlso (linkingID = 7 Or linkingID = 8) AndAlso objClass.DivisionID = 0 Then
                            Try
                                Dim objCom As New CCommon
                                objCom.DomainID = DomainID
                                Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(133, 2, CCommon.ToString(dtDataHold.Rows(intC)("539_numDivisionID")))
                                If Not dtLinkingData Is Nothing Then
                                    objClass.DivisionID = CCommon.ToLong(dtLinkingData.Rows(0)("numDivisionID"))
                                    objClass.CompanyID = CCommon.ToLong(dtLinkingData.Rows(0)("numCompanyID"))
                                End If
                            Catch ex As Exception
                                'DO NOT THROW ERROR BECAUSE linking ID 6 and 8 are for INSERT & UPDATE
                            End Try
                            SetProperty(objClass, strPropertyName, objPropValue)
                        ElseIf CCommon.ToString(strPropertyName).Length > 0 Then
                            SetProperty(objClass, strPropertyName, objPropValue)
                        End If
                    Next


                    SetProperty(objClass, "DomainID", DomainID)
                    SetProperty(objClass, "UserCntID", UserCntID)


                    If tintImportType = 1 AndAlso CCommon.ToLong(DirectCast(objClass, BACRM.BusinessLogic.Contacts.CContacts).ContactID) = 0 Then 'Update
                        Throw New Exception("CONTACT_NOT_FOUND")
                    ElseIf tintImportType = 2 Then
                        DirectCast(objClass, BACRM.BusinessLogic.Contacts.CContacts).ContactID = 0
                    End If

                    If CCommon.ToLong(DirectCast(objClass, BACRM.BusinessLogic.Contacts.CContacts).ContactID) > 0 Then
                        isInsert = False
                    Else
                        isInsert = True
                    End If

                    If objClass.ContactID = 0 Then
                        If objClass.DivisionID = 0 Then
                            Throw New Exception("INVALID_LINKING_ID")
                        Else
                            If Not String.IsNullOrEmpty(objClass.NonBizContactID) AndAlso Not String.IsNullOrEmpty(objClass.FirstName) AndAlso Not String.IsNullOrEmpty(objClass.LastName) Then
                                Dim dtLinkingData As DataTable = Nothing
                                Try
                                    Dim objCom As New CCommon
                                    objCom.DomainID = DomainID
                                    dtLinkingData = objCom.GetRecordByLinkingID(lngMasterID, 1, objClass.NonBizContactID)
                                Catch ex As Exception
                                    'DO NOT THROW ERROR BECAUSE IT IS JUST FOR CHECKING WHETHER RECORD EXISTS ARE NOT WITH THAT LINKING ID
                                End Try

                                If dtLinkingData Is Nothing Then
                                    Using objTransaction As New System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted, .Timeout = System.Transactions.TransactionManager.MaximumTimeout})
                                        If tintImportType = 2 Then
                                            If linkingID = 9 Then
                                                Dim objLeads As New CLeads
                                                objLeads.DomainID = DomainID
                                                objLeads.UserCntID = UserCntID
                                                objLeads.FirstName = CCommon.ToString(dtDataHold.Rows(intC)("51_vcFirstName"))
                                                objLeads.LastName = CCommon.ToString(dtDataHold.Rows(intC)("52_vcLastName"))
                                                objLeads.CRMType = 2
                                                objLeads.CompanyType = 46
                                                objLeads.Profile = 0

                                                Try
                                                    objLeads.CompanyID = objLeads.CreateRecordCompanyInfo()
                                                    objLeads.DivisionID = objLeads.CreateRecordDivisionsInfo()

                                                    objClass.CompanyID = objLeads.CompanyID
                                                    objClass.DivisionID = objLeads.DivisionID
                                                Catch ex As Exception
                                                    Throw New Exception("ERROR_OCCURRED_WHILE_CREATING_ORGANIZATION")
                                                End Try
                                            End If
                                        End If

                                        objItemSave = objClass.GetType().GetMethod("SaveContactForImport")
                                        objItemSave.Invoke(objClass, New Object() {})

                                        If objClass.NonBizContactID IsNot Nothing AndAlso CCommon.ToString(objClass.NonBizContactID) <> "" Then
                                            objItemSave = Nothing
                                            objItemSave = objClass.GetType().GetMethod("ManageImportActionItemReference")
                                            objItemSave.Invoke(objClass, New Object() {objClass.ContactID})
                                        End If

                                        If dtCustomField.Rows.Count > 0 Then
                                            For intCustomFieldCnt As Integer = 0 To dtCustomField.Rows.Count - 1

                                                objImport.RecId = CCommon.ToLong(objClass.GetType().GetProperty(strGetPrimaryColumnName).GetValue(objClass, Nothing))
                                                objImport.FldId = CCommon.ToLong(dtCustomField.Rows(intCustomFieldCnt)("Fld_ID"))
                                                objImport.FldValue = CCommon.ToString(dtCustomField.Rows(intCustomFieldCnt)("Fld_Value"))
                                                objImport.UpdateCustomeField() 'Function just delete(if exist) and Inserts value

                                            Next
                                            dtCustomField.Rows.Clear()
                                        End If

                                        objTransaction.Complete()
                                    End Using
                                Else
                                    Throw New Exception("USED_NON_BIZ_CONTACT_ID")
                                End If
                            Else
                                Throw New Exception("REQUIRED_FIELD_MISSING_VALUES")
                            End If
                        End If
                    Else
                        Using objTransaction As New System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted, .Timeout = System.Transactions.TransactionManager.MaximumTimeout})
                            objItemSave = objClass.GetType().GetMethod("SaveContactForImport")
                            objItemValue = objItemSave.Invoke(objClass, New Object() {})

                            If dtCustomField.Rows.Count > 0 Then
                                For intCustomFieldCnt As Integer = 0 To dtCustomField.Rows.Count - 1

                                    objImport.RecId = CCommon.ToLong(objClass.GetType().GetProperty(strGetPrimaryColumnName).GetValue(objClass, Nothing))
                                    objImport.FldId = CCommon.ToLong(dtCustomField.Rows(intCustomFieldCnt)("Fld_ID"))
                                    objImport.FldValue = CCommon.ToString(dtCustomField.Rows(intCustomFieldCnt)("Fld_Value"))
                                    objImport.UpdateCustomeField() 'Function just delete(if exist) and Inserts value

                                Next
                                dtCustomField.Rows.Clear()
                            End If

                            objTransaction.Complete()
                        End Using
                    End If

                    objCommon.Mode = 36
                    objCommon.UpdateRecordID = intImportFileID
                    objCommon.UpdateValueID = lngCSVRow
                    objCommon.DomainID = DomainID
                    objCommon.UpdateSingleFieldValue()

                    Dim lngContactID As Long = objClass.GetType().GetProperty(strGetPrimaryColumnName).GetValue(objClass, Nothing)

                    Dim message As String
                    If isInsert Then
                        'message = "CSV Row number: " & lngCSVRow & " - Contact inserted with " & strGetPrimaryColumnName & ":" & lngContactID
                        strAddedContactIds = lngContactID & "," & strAddedContactIds
                    Else
                        'message = "CSV Row number: " & lngCSVRow & " - Contact updated with " & strGetPrimaryColumnName & ":" & lngContactID
                        strUpdatedContactIds = lngContactID & "," & strUpdatedContactIds
                    End If
                    'WriteMessage(message)


                    If isInsert Then
                        intInserts = intInserts + 1
                    Else
                        intUpdates = intUpdates + 1
                    End If

                    Try
                        objImport = New ImportWizard(intImportFileID, DomainID)
                        With objImport
                            .ImportFileID = intImportFileID
                            .SaveImportFileReport(lngCSVRow, message, lngContactID, "", "")
                        End With
                    Catch exM As Exception
                        'DO NOT THROW ERROR
                    End Try
                Catch ex As Exception
                    Dim errorMessage As String

                    If ex.Message.Contains("INVALID_LINKING_ID") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("INVALID_LINKING_ID")) Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Invalid or missing Non Biz Company ID / Organization ID for adding new contact)"
                    ElseIf ex.Message.Contains("REQUIRED_FIELD_MISSING_VALUES") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("REQUIRED_FIELD_MISSING_VALUES")) Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Non Biz Contact ID, Contact First Name and Contact Last Name are required for adding new contact)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("CONTACT_NOT_FOUND_WITH_GIVEN_NON BIZ CONTACT ID")) Or ex.Message.Contains("CONTACT_NOT_FOUND_WITH_GIVEN_NON BIZ CONTACT ID") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (No contact matched to Non Biz Contact ID for update)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("MULTIPLE_CONTACTS_FOUND_WITH_GIVEN_NON BIZ CONTACT ID")) Or ex.Message.Contains("MULTIPLE_CONTACTS_FOUND_WITH_GIVEN_NON BIZ CONTACT ID") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Multiple contacts matched to Non Biz Contact ID for update)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("CONTACT_NOT_FOUND_WITH_GIVEN_CONTACT ID")) Or ex.Message.Contains("CONTACT_NOT_FOUND_WITH_GIVEN_CONTACT ID") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (No contact matched to Contact ID for update)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("MULTIPLE_CONTACTS_FOUND_WITH_GIVEN_CONTACT ID")) Or ex.Message.Contains("MULTIPLE_CONTACTS_FOUND_WITH_GIVEN_CONTACT ID") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Multiple concats matched to Contact ID for update)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("CONTACT_NOT_FOUND")) Or ex.Message.Contains("CONTACT_NOT_FOUND") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (No contact matched to Linking ID)"
                    ElseIf ex.Message.Contains("ERROR_OCCURRED_WHILE_CREATING_ORGANIZATION") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured while creating organization for contact"
                    ElseIf ex.Message.Contains("USED_NON_BIZ_CONTACT_ID") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("USED_NON_BIZ_CONTACT_ID")) Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Non Biz Contact ID already used for other contact)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ORGANIZATION_NOT_FOUND_WITH_GIVEN_NON BIZ COMPANY ID")) Or ex.Message.Contains("ORGANIZATION_NOT_FOUND_WITH_GIVEN_NON BIZ COMPANY ID") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (No organization matched to Non Biz Company ID for adding new contact)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("MULTIPLE_ORGANIZATIONS_FOUND_WITH_GIVEN_NON BIZ COMPANY ID")) Or ex.Message.Contains("MULTIPLE_ORGANIZATIONS_FOUND_WITH_GIVEN_NON BIZ COMPANY ID") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Multiple organizations matched to Non Biz Company ID for adding new contact)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ORGANIZATION_NOT_FOUND_WITH_GIVEN_ORGANIZATION ID")) Or ex.Message.Contains("ORGANIZATION_NOT_FOUND_WITH_GIVEN_ORGANIZATION ID") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (No organization matched to Organization ID for adding new contact)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("MULTIPLE_ORGANIZATIONS_FOUND_WITH_GIVEN_ORGANIZATION ID")) Or ex.Message.Contains("MULTIPLE_ORGANIZATIONS_FOUND_WITH_GIVEN_ORGANIZATION ID") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Multiple organizations matched to Organization ID for adding new contact)"
                    Else
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Unknown error occured"
                    End If

                    WriteMessage(errorMessage)

                    Try
                        objImport = New ImportWizard(intImportFileID, DomainID)
                        With objImport
                            .ImportFileID = intImportFileID
                            .SaveImportFileReport(lngCSVRow, errorMessage, 0, If(Not ex.InnerException Is Nothing, ex.InnerException.Message, ex.Message), If(Not ex.InnerException Is Nothing, ex.InnerException.StackTrace, ex.StackTrace))
                        End With
                    Catch exM As Exception
                        'DO NOT THROW ERROR
                    End Try

                    intErrors = intErrors + 1
                End Try

                intCounter = intCounter + 1
            Next

            strAddedContactIds = strAddedContactIds.Trim(",")
            strUpdatedContactIds = strUpdatedContactIds.Trim(",")

            UpdateImportFileStatus(intInserts, intUpdates, intErrors, strAddedContactIds, strUpdatedContactIds, strImportType)

            WriteMessage("End Time :" & DateTime.Now)
            WriteMessage("Import Duration (In Seconds) :" & DateDiff(DateInterval.Second, dtStartTime, DateTime.Now))
            WriteMessage(strImportType & " records created: " & intInserts & If(strAddedContactIds.Length > 0, " : " & strAddedContactIds, ""))
            WriteMessage(strImportType & " records Updated: " & intUpdates & If(strUpdatedContactIds.Length > 0, " : " & strUpdatedContactIds, ""))
            WriteMessage(strImportType & " records have Errors: " & intErrors)
        Catch ex As Exception

        End Try
    End Sub

    Public Sub ImportOrganizationCorrespondence(ByVal dtDataHold As DataTable)
        Dim intInserts As Integer = 0
        Dim intUpdates As Integer = 0
        Dim intErrors As Integer = 0
        Dim isInsert As Boolean = False

        Dim strGetPrimaryColumnName As String = "CommID"
        Dim strFormFieldName As String = ""
        Dim strFormFieldID As Object = Nothing
        Dim vcAssociatedControlType As String = ""
        Dim strValueString As String = ""
        Dim strImportType As String = "Organization Correspondence"
        Dim PropertyItem As PropertyInfo = Nothing
        Dim objClass As Object = Nothing
        Dim strPropertyName As String
        Dim objItemSave As MethodInfo = Nothing
        Dim objItemValue As New Object
        Dim intCounter As Long = 0
        Dim strAddedOrganizationCorrespondenceCodes As String = ""

        Try
            Dim dtCustomField As New DataTable
            dtCustomField.Columns.Add("Fld_ID").SetOrdinal(0)
            dtCustomField.Columns.Add("Name").SetOrdinal(1)
            dtCustomField.Columns.Add("Fld_Value").SetOrdinal(2)
            dtCustomField.TableName = "CusFlds"
            dtCustomField.AcceptChanges()

            For intC As Integer = If(lngStartRow = 0, 0, lngStartRow - 1) To dtDataHold.Rows.Count - 1
                lngCSVRow = intC + 2

                If intCounter = lngWaitRecords Then
                    intCounter = 0
                    WriteMessage("Thread Sleep for " & (lngWaitMiliSeconds) & " miliseconds.")
                    Thread.Sleep(lngWaitMiliSeconds)
                End If

                Try
                    objClass = New ActionItem
                    If dtDataHold.Columns.Contains("386_vcNonBizContactID") Then
                        dtDataHold.Columns("386_vcNonBizContactID").SetOrdinal(0)
                    Else
                        Throw New Exception("NON_BIZ_CONTACT_ID_NOT_ADDED_IN_CSV")
                    End If

                    Dim dvTempToSort As DataView
                    dvTempToSort = dataTableHeading.DefaultView
                    dvTempToSort.Sort = "IsPrimary DESC"
                    dataTableHeading = dvTempToSort.ToTable()


                    For intHead As Integer = 0 To dataTableHeading.Rows.Count - 1
                        If dataTableHeading.Rows(intHead)("dbFieldName") Is DBNull.Value OrElse dataTableHeading.Rows(intHead)("dbFieldName") = "" Then
                            Continue For
                        End If

                        strFormFieldName = dataTableHeading.Rows(intHead)("dbFieldName")
                        strFormFieldID = dataTableHeading.Rows(intHead)("FormFieldID")
                        strPropertyName = dataTableHeading.Rows(intHead)("vcPropertyName")

                        Dim objPropValue As Object = Nothing

                        If CBool(dataTableHeading.Rows(intHead)("bitCustomField")) = True Then
                            Dim drCustom As DataRow
                            drCustom = dtCustomField.NewRow
                            drCustom("Fld_ID") = strFormFieldID
                            drCustom("Name") = strFormFieldName

                            If CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "checkbox" Then
                                If CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "true" OrElse _
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "yes" OrElse _
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "1" OrElse _
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "y" Then
                                    drCustom("Fld_Value") = 1
                                ElseIf CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "false" OrElse _
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "no" OrElse _
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "0" OrElse _
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "n" Then
                                    drCustom("Fld_Value") = 0
                                End If
                            Else
                                drCustom("Fld_Value") = dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)
                            End If

                            dtCustomField.Rows.Add(drCustom)
                            dtCustomField.AcceptChanges()
                        Else
                            objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                        End If

                        If CCommon.ToString(strPropertyName).Length > 0 Then
                            SetProperty(objClass, strPropertyName, objPropValue)
                        End If
                    Next

                    SetProperty(objClass, "DomainID", DomainID)
                    SetProperty(objClass, "UserCntID", UserCntID)


                    isInsert = True

                    'Load Contact ID and Div ID from Non Biz Contact ID
                    '--------------------------------------------------------------------
                    Dim objLeads As New CLeads
                    objLeads.DomainID = DomainID
                    objLeads.byteMode = 1
                    objLeads.NonBizContactID = objClass.NonBizContactID
                    objLeads.ManageImportActionItemReference()
                    '--------------------------------------------------------------------

                    Dim objCommon As New CCommon
                    Dim dtDomainDetail As New DataTable
                    objCommon.DomainID = objLeads.DomainID
                    dtDomainDetail = objCommon.GetDomainDetails()

                    objClass.StartTime = Convert.ToDateTime(objClass.DueDate)
                    objClass.EndTime = Convert.ToDateTime(objClass.DueDate)

                    objClass.DivisionID = objLeads.DivisionID
                    objClass.ContactID = objLeads.ContactID

                    objItemSave = objClass.GetType().GetMethod("SaveCommunicationinfo")
                    If CCommon.ToString(objClass.Details).Length > 0 Then
                        objItemValue = objItemSave.Invoke(objClass, New Object() {})
                    Else
                        Throw New Exception("Details/Comments_NOT_PROVIDED")
                    End If

                    objCommon.Mode = 36
                    objCommon.UpdateRecordID = intImportFileID
                    objCommon.UpdateValueID = lngCSVRow
                    objCommon.DomainID = DomainID
                    objCommon.UpdateSingleFieldValue()

                    Dim lngCommID As Long = objClass.GetType().GetProperty(strGetPrimaryColumnName).GetValue(objClass, Nothing)
                    strAddedOrganizationCorrespondenceCodes = lngCommID & "," & strAddedOrganizationCorrespondenceCodes

                    Dim message As String = "CSV Row number: " & lngCSVRow & " - Organization Correspondence inserted."
                    WriteMessage(message)

                    intInserts = intInserts + 1

                    Try
                        objImport = New ImportWizard(intImportFileID, DomainID)
                        With objImport
                            .ImportFileID = intImportFileID
                            .SaveImportFileReport(lngCSVRow, message, lngCommID, "", "")
                        End With
                    Catch exM As Exception
                        'DO NOT THROW ERROR
                    End Try
                Catch ex As Exception
                    Dim errorMessage As String = ""

                    If ex.Message.Contains("Details/Comments_NOT_PROVIDED") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("Details/Comments_NOT_PROVIDED")) Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Details/Comments is required)"
                    ElseIf ex.Message.Contains("COMPANY_NOT_FOUND") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("COMPANY_NOT_FOUND")) Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Organization not found)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("CONTACT_NOT_FOUND")) Or ex.Message.Contains("CONTACT_NOT_FOUND") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Contact not found)"
                    ElseIf ex.Message.Contains("NON_BIZ_CONTACT_ID_NOT_ADDED_IN_CSV") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("NON_BIZ_CONTACT_ID_NOT_ADDED_IN_CSV")) Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Non Biz Contact ID is required)"
                    Else
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Unknown error occured"
                    End If

                    WriteMessage(errorMessage)

                    Try
                        objImport = New ImportWizard(intImportFileID, DomainID)
                        With objImport
                            .ImportFileID = intImportFileID
                            .SaveImportFileReport(lngCSVRow, errorMessage, 0, If(Not ex.InnerException Is Nothing, ex.InnerException.Message, ex.Message), If(Not ex.InnerException Is Nothing, ex.InnerException.StackTrace, ex.StackTrace))
                        End With
                    Catch exM As Exception
                        'DO NOT THROW ERROR
                    End Try

                    intErrors = intErrors + 1
                End Try

                intCounter = intCounter + 1
            Next

            strAddedOrganizationCorrespondenceCodes = strAddedOrganizationCorrespondenceCodes.Trim(",")

            UpdateImportFileStatus(intInserts, intUpdates, intErrors, strAddedOrganizationCorrespondenceCodes, "", strImportType)

            WriteMessage("End Time :" & DateTime.Now)
            WriteMessage("Import Duration (In Seconds) :" & DateDiff(DateInterval.Second, dtStartTime, DateTime.Now))
            WriteMessage(strImportType & " records created: " & intInserts & " : " & strAddedOrganizationCorrespondenceCodes)
            WriteMessage(strImportType & " records Updated: " & intUpdates)
            WriteMessage(strImportType & " records have Errors: " & intErrors)
        Catch ex As Exception

        End Try
    End Sub

    Public Sub ImportItemWarehouse(ByVal dtDataHold As DataTable)
        Dim intInserts As Integer = 0
        Dim intUpdates As Integer = 0
        Dim intErrors As Integer = 0
        Dim isInsert As Boolean = False

        Dim strGetPrimaryColumnName As String = "WareHouseItemID"
        Dim strFormFieldName As String = ""
        Dim strFormFieldID As Object = Nothing
        Dim vcAssociatedControlType As String = ""
        Dim strValueString As String = ""
        Dim strImportType As String = "Item Warehouse"
        Dim PropertyItem As PropertyInfo = Nothing
        Dim objClass As Object = Nothing
        Dim strPropertyName As String
        Dim objItemSave As MethodInfo = Nothing
        Dim objItemValue As New Object
        Dim intCounter As Long = 0
        Dim strAddedWarehouseItemIds As String = ""
        Dim strUpdatedWarehouseItemIds As String = ""

        Try
            Dim strItemIDs As String = ""
            Dim dtItem As New DataTable
            If dtDataHold.Columns.Contains("211_numItemCode") Then
                dtDataHold.Columns("211_numItemCode").SetOrdinal(0)

                For Each dr As DataRow In dtDataHold.Rows
                    If CCommon.ToLong(dr("211_numItemCode")) > 0 Then
                        strItemIDs = strItemIDs & "," & CCommon.ToString(dr("211_numItemCode"))
                    End If
                Next
            Else
                strItemIDs = "0"
            End If

            strItemIDs = strItemIDs.Trim(",")
            Dim objItem As New CItems
            With objItem
                .strItemIDs = strItemIDs
                .DomainID = DomainID
                dtItem = .GetItemOnHandAvgCost()
            End With


            Dim dtCustomField As New DataTable
            dtCustomField.Columns.Add("Fld_ID").SetOrdinal(0)
            dtCustomField.Columns.Add("Name").SetOrdinal(1)
            dtCustomField.Columns.Add("Fld_Value").SetOrdinal(2)
            dtCustomField.TableName = "CusFlds"
            dtCustomField.AcceptChanges()

            For intC As Integer = If(lngStartRow = 0, 0, lngStartRow - 1) To dtDataHold.Rows.Count - 1
                Try
                    lngCSVRow = intC + 2

                    If intCounter = lngWaitRecords Then
                        intCounter = 0
                        WriteMessage("Thread Sleep for " & (lngWaitMiliSeconds) & " miliseconds.")
                        Thread.Sleep(lngWaitMiliSeconds)
                    End If


                    objClass = New CItems
                    If objItemSave Is Nothing Then objItemSave = objClass.GetType().GetMethod("AddUpdateWareHouseForItems")
                    If dtDataHold.Columns.Contains("235_numWareHouseItemID") Then
                        dtDataHold.Columns("235_numWareHouseItemID").SetOrdinal(0)
                    End If

                    If (tintImportType = 1 Or tintImportType = 3) AndAlso Not dtDataHold.Columns.Contains("235_numWareHouseItemID") Then
                        Throw New Exception("Warehouse_Item_ID_REQUIRED")
                    End If

                    If (tintImportType = 1 Or tintImportType = 3) AndAlso Not dtDataHold.Columns.Contains("211_numItemCode") Then
                        Throw New Exception("ItemID_COLUMN_REQUIRED")
                    End If

                    Dim dvTempToSort As DataView
                    dvTempToSort = dataTableHeading.DefaultView
                    dvTempToSort.Sort = "IsPrimary DESC"
                    dataTableHeading = dvTempToSort.ToTable()


                    For intHead As Integer = 0 To dataTableHeading.Rows.Count - 1
                        If dataTableHeading.Rows(intHead)("dbFieldName") Is DBNull.Value OrElse dataTableHeading.Rows(intHead)("dbFieldName") = "" Then
                            Continue For
                        End If

                        strFormFieldName = dataTableHeading.Rows(intHead)("dbFieldName")
                        strFormFieldID = dataTableHeading.Rows(intHead)("FormFieldID")
                        strPropertyName = dataTableHeading.Rows(intHead)("vcPropertyName")

                        Dim objPropValue As Object = Nothing

                        If CBool(dataTableHeading.Rows(intHead)("bitCustomField")) = True Then
                            Dim drCustom As DataRow
                            drCustom = dtCustomField.NewRow
                            drCustom("Fld_ID") = strFormFieldID
                            drCustom("Name") = strFormFieldName

                            If CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "checkbox" Then
                                If CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "true" OrElse _
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "yes" OrElse _
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "1" OrElse _
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "y" Then
                                    drCustom("Fld_Value") = 1
                                ElseIf CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "false" OrElse _
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "no" OrElse _
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "0" OrElse _
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "n" Then
                                    drCustom("Fld_Value") = 0
                                End If
                            Else
                                drCustom("Fld_Value") = dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)
                            End If

                            dtCustomField.Rows.Add(drCustom)
                            dtCustomField.AcceptChanges()
                        Else
                            objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                        End If


                        If strFormFieldName = "numWareHouseItemID" AndAlso (tintImportType = 1 Or tintImportType = 3) Then
                            objClass.WareHouseItemID = CCommon.ToLong(objPropValue)
                            objClass.DomainID = DomainID
                            objClass.GetWarehouseItemDetails()

                            objPropValue = CCommon.ToLong(objClass.WareHouseItemID)
                        End If


                        If CCommon.ToString(strPropertyName).Length > 0 Then
                            If strPropertyName.Contains("OnHand") = True Then
                                objPropValue = If(CCommon.ToLong(objPropValue) < 0, 0, objPropValue)
                            End If

                            SetProperty(objClass, strPropertyName, objPropValue)
                        End If
                    Next


                    SetProperty(objClass, "DomainID", DomainID)
                    SetProperty(objClass, "UserCntID", UserCntID)


                    If tintImportType = 1 AndAlso CCommon.ToLong(DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).WareHouseItemID) = 0 Then 'Update
                        Throw New Exception("WAREHOUSE_DETAIL_NOT_FOUND")
                    ElseIf tintImportType = 2 Then
                        DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).WareHouseItemID = 0
                    End If

                    If dtCustomField.Rows.Count > 0 Then
                        Dim ds As New DataSet
                        ds.Tables.Add(dtCustomField.Copy)
                        SetProperty(objClass, "strFieldList", ds.GetXml)
                        ds.Tables.Clear()
                    End If

                    If CCommon.ToLong(DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).WareHouseItemID) > 0 Then
                        isInsert = False
                    Else
                        isInsert = True
                    End If

                    Using objTransaction As New System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted, .Timeout = System.Transactions.TransactionManager.MaximumTimeout})
                        objItemValue = objItemSave.Invoke(objClass, New Object() {})

                        If dtCustomField.Rows.Count > 0 Then
                            For intCustomFieldCnt As Integer = 0 To dtCustomField.Rows.Count - 1

                                objImport.RecId = CCommon.ToLong(objClass.GetType().GetProperty(strGetPrimaryColumnName).GetValue(objClass, Nothing))
                                objImport.FldId = CCommon.ToLong(dtCustomField.Rows(intCustomFieldCnt)("Fld_ID"))
                                objImport.FldValue = CCommon.ToString(dtCustomField.Rows(intCustomFieldCnt)("Fld_Value"))
                                objImport.UpdateCustomeField() 'Function just delete(if exist) and Inserts value

                            Next
                            dtCustomField.Rows.Clear()
                        End If

                        objTransaction.Complete()
                    End Using


                    Try
                        Dim dblOldOnHand As Double = 0
                        Dim dvItem As DataView = dtItem.DefaultView
                        dvItem.RowFilter = "numItemCode = " & CCommon.ToLong(DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).ItemCode) & " AND numWareHouseItemID = " & CCommon.ToLong(DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).WareHouseItemID)
                        If dvItem.Count > 0 Then
                            dblOldOnHand = CCommon.ToDouble(dvItem(0)("numOnHand"))
                        End If

                        Dim dtItemDetail As DataTable = DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).ItemDetails
                        If dtItemDetail IsNot Nothing AndAlso dtItemDetail.Rows.Count > 0 Then
                            If CCommon.ToBool(dtItemDetail.Rows(0)("bitOppOrderExists")) Then
                                WriteMessage("CSV Row number: " & lngCSVRow & " - Item is used in Opportunity or Orders so you cannot update item Onhand/Avg.cost details.")
                            Else
                                If CCommon.ToBool(dtItemDetail.Rows(0)("bitLotNo")) = True Then
                                    'Adding General Ledger Entry 
                                    Dim dblNewOnHand As Double = CCommon.ToLong(objClass.GetType().GetProperty("Quantity").GetValue(objClass, Nothing))
                                    Dim dblNewAverageCost As Double = CCommon.ToDecimal(dtItemDetail.Rows(0)("monAverageCost"))

                                    If (dblOldOnHand = 0) AndAlso (dblNewOnHand > 0) Then
                                        DirectCast(objClass, CItems).MakeItemQtyAdjustmentJournal(CCommon.ToDouble(DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).ItemCode), _
                                                                                                  objClass.GetType().GetProperty("ItemName").GetValue(objClass, Nothing).ToString, _
                                                                                                  dblNewOnHand, _
                                                                                                  dblNewAverageCost, _
                                                                                                  CCommon.ToLong(dtItemDetail.Rows(0)("numAssetChartAcntId")), _
                                                                                                  objClass.GetType().GetProperty("UserCntID").GetValue(objClass, Nothing), _
                                                                                                  objClass.GetType().GetProperty("DomainID").GetValue(objClass, Nothing))
                                    End If
                                ElseIf CCommon.ToBool(dtItemDetail.Rows(0)("bitSerialized")) = True Then
                                    Dim dblNewOnHand As Double = 1 'CCommon.ToLong(objClass.GetType().GetProperty("Quantity").GetValue(objClass, Nothing))
                                    Dim dblNewAverageCost As Double = CCommon.ToDecimal(dtItemDetail.Rows(0)("monAverageCost"))

                                    If (dblOldOnHand = 0) AndAlso (dblNewOnHand > 0) Then
                                        DirectCast(objClass, CItems).MakeItemQtyAdjustmentJournal(CCommon.ToDouble(DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).ItemCode), _
                                                                                                  objClass.GetType().GetProperty("ItemName").GetValue(objClass, Nothing).ToString, _
                                                                                                  1, _
                                                                                                  dblNewAverageCost, _
                                                                                                  CCommon.ToLong(dtItemDetail.Rows(0)("numAssetChartAcntId")), _
                                                                                                  objClass.GetType().GetProperty("UserCntID").GetValue(objClass, Nothing), _
                                                                                                  objClass.GetType().GetProperty("DomainID").GetValue(objClass, Nothing))
                                    End If
                                Else
                                    Dim dblNewOnHand As Double = If(CCommon.ToLong(objClass.GetType().GetProperty("OnHand").GetValue(objClass, Nothing)) < 0, 0, CCommon.ToLong(objClass.GetType().GetProperty("OnHand").GetValue(objClass, Nothing)))
                                    Dim dblNewAverageCost As Double = CCommon.ToDecimal(objClass.GetType().GetProperty("AverageCost").GetValue(objClass, Nothing))

                                    If (dblOldOnHand = 0) AndAlso (dblNewOnHand > 0) Then
                                        DirectCast(objClass, CItems).MakeItemQtyAdjustmentJournal(CCommon.ToDouble(DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).ItemCode), _
                                                                                                  objClass.GetType().GetProperty("ItemName").GetValue(objClass, Nothing).ToString, _
                                                                                                  dblNewOnHand, _
                                                                                                  dblNewAverageCost, _
                                                                                                  CCommon.ToLong(dtItemDetail.Rows(0)("numAssetChartAcntId")), _
                                                                                                  objClass.GetType().GetProperty("UserCntID").GetValue(objClass, Nothing), _
                                                                                                  objClass.GetType().GetProperty("DomainID").GetValue(objClass, Nothing))
                                    End If

                                End If
                            End If
                        End If
                    Catch ex As Exception
                        'DO NOT THROW ERROR
                    End Try


                    objCommon.Mode = 36
                    objCommon.UpdateRecordID = intImportFileID
                    objCommon.UpdateValueID = lngCSVRow
                    objCommon.DomainID = DomainID
                    objCommon.UpdateSingleFieldValue()

                    Dim lngWarehouseItemID As Long = objClass.GetType().GetProperty(strGetPrimaryColumnName).GetValue(objClass, Nothing)

                    Dim message As String
                    If isInsert Then
                        'message = "CSV Row number: " & lngCSVRow & " - Item Warehouse inserted with " & strGetPrimaryColumnName & ":" & lngWarehouseItemID
                        strAddedWarehouseItemIds = lngWarehouseItemID & "," & strAddedWarehouseItemIds
                    Else
                        'message = "CSV Row number: " & lngCSVRow & " - Item Warehouse updated with " & strGetPrimaryColumnName & ":" & lngWarehouseItemID
                        strUpdatedWarehouseItemIds = lngWarehouseItemID & "," & strUpdatedWarehouseItemIds
                    End If
                    'WriteMessage(message)


                    If isInsert Then
                        intInserts = intInserts + 1
                    Else
                        intUpdates = intUpdates + 1
                    End If

                    Try
                        objImport = New ImportWizard(intImportFileID, DomainID)
                        With objImport
                            .ImportFileID = intImportFileID
                            .SaveImportFileReport(lngCSVRow, message, lngWarehouseItemID, "", "")
                        End With
                    Catch exM As Exception
                        'DO NOT THROW ERROR
                    End Try
                Catch ex As Exception
                    Dim errorMessage As String

                    If ex.Message.Contains("Warehouse_Item_ID_REQUIRED") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("Warehouse_Item_ID_REQUIRED")) Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Warehouse Item ID column is not added to CSV)"
                    ElseIf ex.Message.Contains("WAREHOUSE_DETAIL_NOT_FOUND") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("WAREHOUSE_DETAIL_NOT_FOUND")) Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Invalid Warehouse Item ID)"
                    ElseIf ex.Message.Contains("ItemID_COLUMN_REQUIRED") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ItemID_COLUMN_REQUIRED")) Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Item ID column is required to insert warehouse)"
                    ElseIf ex.Message.Contains("INVALID_ITEM_CODE") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("INVALID_ITEM_CODE")) Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Invalid Item Code)"
                    Else
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Unknown error occured"
                    End If

                    WriteMessage(errorMessage)

                    Try
                        objImport = New ImportWizard(intImportFileID, DomainID)
                        With objImport
                            .ImportFileID = intImportFileID
                            .SaveImportFileReport(lngCSVRow, errorMessage, 0, If(Not ex.InnerException Is Nothing, ex.InnerException.Message, ex.Message), If(Not ex.InnerException Is Nothing, ex.InnerException.StackTrace, ex.StackTrace))
                        End With
                    Catch exM As Exception
                        'DO NOT THROW ERROR
                    End Try

                    intErrors = intErrors + 1
                End Try

                intCounter = intCounter + 1
            Next

            strAddedWarehouseItemIds = strAddedWarehouseItemIds.Trim(",")
            strUpdatedWarehouseItemIds = strUpdatedWarehouseItemIds.Trim(",")

            UpdateImportFileStatus(intInserts, intUpdates, intErrors, strAddedWarehouseItemIds, strUpdatedWarehouseItemIds, strImportType)

            WriteMessage("End Time :" & DateTime.Now)
            WriteMessage("Import Duration (In Seconds) :" & DateDiff(DateInterval.Second, dtStartTime, DateTime.Now))
            WriteMessage(strImportType & " records created: " & intInserts & If(strAddedWarehouseItemIds.Length > 0, " : " & strAddedWarehouseItemIds, ""))
            WriteMessage(strImportType & " records Updated: " & intUpdates & If(strUpdatedWarehouseItemIds.Length > 0, " : " & strUpdatedWarehouseItemIds, ""))
            WriteMessage(strImportType & " records have Errors: " & intErrors)
        Catch ex As Exception

        End Try
    End Sub

    Public Sub ImportItemImages(ByVal dtDataHold As DataTable)
        Dim intInserts As Integer = 0
        Dim intUpdates As Integer = 0
        Dim intErrors As Integer = 0
        Dim isInsert As Boolean = False

        Dim strGetPrimaryColumnName As String = "numItemImageId"
        Dim strFormFieldName As String = ""
        Dim strFormFieldID As Object = Nothing
        Dim vcAssociatedControlType As String = ""
        Dim strValueString As String = ""
        Dim strImportType As String = "Item Images"
        Dim PropertyItem As PropertyInfo = Nothing
        Dim objClass As Object = Nothing
        Dim strPropertyName As String
        Dim objItemSave As MethodInfo = Nothing
        Dim objItemValue As New Object
        Dim intCounter As Long = 0
        Dim strAddedItemImageIds As String = ""
        Dim strUpdatedItemImageIds As String = ""

        Try
            Dim dtCustomField As New DataTable
            dtCustomField.Columns.Add("Fld_ID").SetOrdinal(0)
            dtCustomField.Columns.Add("Name").SetOrdinal(1)
            dtCustomField.Columns.Add("Fld_Value").SetOrdinal(2)
            dtCustomField.TableName = "CusFlds"
            dtCustomField.AcceptChanges()

            For intC As Integer = If(lngStartRow = 0, 0, lngStartRow - 1) To dtDataHold.Rows.Count - 1
                Try
                    lngCSVRow = intC + 2

                    If intCounter = lngWaitRecords Then
                        intCounter = 0
                        WriteMessage("Thread Sleep for " & (lngWaitMiliSeconds) & " miliseconds.")
                        Thread.Sleep(lngWaitMiliSeconds)
                    End If

                    objClass = New CItems
                    If objItemSave Is Nothing Then objItemSave = objClass.GetType().GetMethod("AddUpdateImageItem")
                    If dtDataHold.Columns.Contains("381_numItemImageId") Then
                        dtDataHold.Columns("381_numItemImageId").SetOrdinal(0)
                    ElseIf dtDataHold.Columns.Contains("211_numItemCode") Then
                        dtDataHold.Columns("211_numItemCode").SetOrdinal(0)
                    End If


                    If tintImportType = 1 AndAlso Not dtDataHold.Columns.Contains("381_numItemImageId") Then
                        Throw New Exception("ITEM_IMAGE_ID_REQUIRED")
                    ElseIf Not dtDataHold.Columns.Contains("211_numItemCode") Then
                        Throw New Exception("ITEM_CODE_REQUIRED")
                    End If

                    Dim dvTempToSort As DataView
                    dvTempToSort = dataTableHeading.DefaultView
                    dvTempToSort.Sort = "IsPrimary DESC"
                    dataTableHeading = dvTempToSort.ToTable()


                    For intHead As Integer = 0 To dataTableHeading.Rows.Count - 1
                        If dataTableHeading.Rows(intHead)("dbFieldName") Is DBNull.Value OrElse dataTableHeading.Rows(intHead)("dbFieldName") = "" Then
                            Continue For
                        End If

                        strFormFieldName = dataTableHeading.Rows(intHead)("dbFieldName")
                        strFormFieldID = dataTableHeading.Rows(intHead)("FormFieldID")
                        strPropertyName = dataTableHeading.Rows(intHead)("vcPropertyName")

                        Dim objPropValue As Object = Nothing

                        If CBool(dataTableHeading.Rows(intHead)("bitCustomField")) = True Then
                            Dim drCustom As DataRow
                            drCustom = dtCustomField.NewRow
                            drCustom("Fld_ID") = strFormFieldID
                            drCustom("Name") = strFormFieldName

                            If CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "checkbox" Then
                                If CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "true" OrElse _
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "yes" OrElse _
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "1" OrElse _
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "y" Then
                                    drCustom("Fld_Value") = 1
                                ElseIf CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "false" OrElse _
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "no" OrElse _
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "0" OrElse _
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "n" Then
                                    drCustom("Fld_Value") = 0
                                End If
                            Else
                                drCustom("Fld_Value") = dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)
                            End If

                            dtCustomField.Rows.Add(drCustom)
                            dtCustomField.AcceptChanges()
                        Else
                            objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                        End If


                        If strFormFieldName = "numItemImageId" Then
                            objClass.numItemImageId = CCommon.ToLong(objPropValue)
                            objClass.DomainID = DomainID
                            objClass.GetItemImageDetails()

                            objPropValue = CCommon.ToLong(objClass.numItemImageId)
                        End If

                        If CCommon.ToString(strPropertyName).Length > 0 Then
                            SetProperty(objClass, strPropertyName, objPropValue)
                        End If
                    Next


                    SetProperty(objClass, "DomainID", DomainID)
                    SetProperty(objClass, "UserCntID", UserCntID)


                    If tintImportType = 1 AndAlso CCommon.ToLong(DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).numItemImageId) = 0 Then 'Update
                        Throw New Exception("INVALID_ITEM_IMAGE_ID")
                    ElseIf tintImportType = 2 Then 'Insert
                        DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).numItemImageId = 0
                    End If

                    If CCommon.ToLong(DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).numItemImageId) > 0 Then
                        isInsert = False
                    Else
                        isInsert = True
                    End If

                    If tintImportType = 2 Or (tintImportType = 3 AndAlso DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).numItemImageId = 0) Then
                        Dim objCom As New CCommon
                        objCom.DomainID = DomainID
                        Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(20, 1, CCommon.ToString(dtDataHold.Rows(intC)("211_numItemCode")))
                    End If

                    Dim strPath As String = CCommon.ToString(CType(objClass, CItems).PathForImage)
                    If CCommon.CheckForImage(strPath) = True AndAlso CType(objClass, CItems).IsImage = True Then
                        CType(objClass, CItems).IsImage = True
                    Else
                        CType(objClass, CItems).IsImage = False
                    End If

                    objItemValue = objItemSave.Invoke(objClass, New Object() {})

                    objCommon.Mode = 36
                    objCommon.UpdateRecordID = intImportFileID
                    objCommon.UpdateValueID = lngCSVRow
                    objCommon.DomainID = DomainID
                    objCommon.UpdateSingleFieldValue()

                    Dim lngItemImageID As Long = objClass.GetType().GetProperty(strGetPrimaryColumnName).GetValue(objClass, Nothing)

                    Dim message As String
                    If isInsert Then
                        'message = "CSV Row number: " & lngCSVRow & " - Item image inserted with " & strGetPrimaryColumnName & ":" & lngItemImageID
                        strAddedItemImageIds = lngItemImageID & "," & strAddedItemImageIds
                    Else
                        'message = "CSV Row number: " & lngCSVRow & " - Item image updated with " & strGetPrimaryColumnName & ":" & lngItemImageID
                        strUpdatedItemImageIds = lngItemImageID & "," & strUpdatedItemImageIds
                    End If
                    'WriteMessage(message)


                    If isInsert Then
                        intInserts = intInserts + 1
                    Else
                        intUpdates = intUpdates + 1
                    End If

                    Try
                        objImport = New ImportWizard(intImportFileID, DomainID)
                        With objImport
                            .ImportFileID = intImportFileID
                            .SaveImportFileReport(lngCSVRow, message, lngItemImageID, "", "")
                        End With
                    Catch exM As Exception
                        'DO NOT THROW ERROR
                    End Try
                Catch ex As Exception
                    Dim errorMessage As String

                    If ex.Message.Contains("ITEM_IMAGE_ID_REQUIRED") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ITEM_IMAGE_ID_REQUIRED")) Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Item Image ID is reqired to update item images)"
                    ElseIf ex.Message.Contains("INVALID_ITEM_IMAGE_ID") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("INVALID_ITEM_IMAGE_ID")) Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Invalid Item Image ID)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ITEM_CODE_REQUIRED")) Or ex.Message.Contains("ITEM_CODE_REQUIRED") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Item Code is required to insert new item images)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ITEM_NOT_FOUND_WITH_GIVEN_ITEMCODE")) Or ex.Message.Contains("ITEM_NOT_FOUND_WITH_GIVEN_ITEMCODE") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (No item matched to item code for update)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("MULTIPLE_ITEMS_FOUND_WITH_GIVEN_ITEMCODE")) Or ex.Message.Contains("MULTIPLE_ITEMS_FOUND_WITH_GIVEN_ITEMCODE") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Multiple items matched to item code for update)"
                    Else
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Unknown error occured"
                    End If

                    WriteMessage(errorMessage)

                    Try
                        objImport = New ImportWizard(intImportFileID, DomainID)
                        With objImport
                            .ImportFileID = intImportFileID
                            .SaveImportFileReport(lngCSVRow, errorMessage, 0, If(Not ex.InnerException Is Nothing, ex.InnerException.Message, ex.Message), If(Not ex.InnerException Is Nothing, ex.InnerException.StackTrace, ex.StackTrace))
                        End With
                    Catch exM As Exception
                        'DO NOT THROW ERROR
                    End Try

                    intErrors = intErrors + 1
                End Try

                intCounter = intCounter + 1
            Next

            strAddedItemImageIds = strAddedItemImageIds.Trim(",")
            strUpdatedItemImageIds = strUpdatedItemImageIds.Trim(",")

            UpdateImportFileStatus(intInserts, intUpdates, intErrors, strAddedItemImageIds, strUpdatedItemImageIds, strImportType)

            WriteMessage("End Time :" & DateTime.Now)
            WriteMessage("Import Duration (In Seconds) :" & DateDiff(DateInterval.Second, dtStartTime, DateTime.Now))
            WriteMessage(strImportType & " records created: " & intInserts & If(strAddedItemImageIds.Length > 0, " : " & strAddedItemImageIds, ""))
            WriteMessage(strImportType & " records Updated: " & intUpdates & If(strUpdatedItemImageIds.Length > 0, " : " & strUpdatedItemImageIds, ""))
            WriteMessage(strImportType & " records have Errors: " & intErrors)
        Catch ex As Exception

        End Try
    End Sub

    Public Sub ImportItemVendors(ByVal dtDataHold As DataTable)
        Dim intInserts As Integer = 0
        Dim intUpdates As Integer = 0
        Dim intErrors As Integer = 0
        Dim isInsert As Boolean = False

        Dim strGetPrimaryColumnName As String = "VendorTcode"
        Dim strFormFieldName As String = ""
        Dim strFormFieldID As Object = Nothing
        Dim vcAssociatedControlType As String = ""
        Dim strValueString As String = ""
        Dim strImportType As String = "Item Vendor"
        Dim PropertyItem As PropertyInfo = Nothing
        Dim objClass As Object = Nothing
        Dim strPropertyName As String
        Dim objItemSave As MethodInfo = Nothing
        Dim objItemValue As New Object
        Dim intCounter As Long = 0
        Dim strAddedItemVendorIds As String = ""
        Dim strUpdatedItemVendorIds As String = ""

        Try
            Dim dtCustomField As New DataTable
            dtCustomField.Columns.Add("Fld_ID").SetOrdinal(0)
            dtCustomField.Columns.Add("Name").SetOrdinal(1)
            dtCustomField.Columns.Add("Fld_Value").SetOrdinal(2)
            dtCustomField.TableName = "CusFlds"
            dtCustomField.AcceptChanges()

            For intC As Integer = If(lngStartRow = 0, 0, lngStartRow - 1) To dtDataHold.Rows.Count - 1
                Try
                    lngCSVRow = intC + 2

                    If intCounter = lngWaitRecords Then
                        intCounter = 0
                        WriteMessage("Thread Sleep for " & (lngWaitMiliSeconds) & " miliseconds.")
                        Thread.Sleep(lngWaitMiliSeconds)
                    End If

                    objClass = New CItems
                    If objItemSave Is Nothing Then objItemSave = objClass.GetType().GetMethod("ManageItemVendor")
                    SetProperty(objClass, "mode", False)

                    If dtDataHold.Columns.Contains("349_numVendorID") Then
                        dtDataHold.Columns("349_numVendorID").SetOrdinal(0)
                    ElseIf dtDataHold.Columns.Contains("211_numItemCode") Then
                        dtDataHold.Columns("211_numItemCode").SetOrdinal(0)
                    End If


                    If tintImportType = 1 AndAlso Not dtDataHold.Columns.Contains("349_numVendorID") Then
                        Throw New Exception("VENDOR_REQUIRED")
                    ElseIf Not dtDataHold.Columns.Contains("211_numItemCode") Then
                        Throw New Exception("ITEM_CODE_REQUIRED")
                    End If

                    Dim dvTempToSort As DataView
                    dvTempToSort = dataTableHeading.DefaultView
                    dvTempToSort.Sort = "IsPrimary DESC"
                    dataTableHeading = dvTempToSort.ToTable()


                    For intHead As Integer = 0 To dataTableHeading.Rows.Count - 1
                        If dataTableHeading.Rows(intHead)("dbFieldName") Is DBNull.Value OrElse dataTableHeading.Rows(intHead)("dbFieldName") = "" Then
                            Continue For
                        End If

                        strFormFieldName = dataTableHeading.Rows(intHead)("dbFieldName")
                        strFormFieldID = dataTableHeading.Rows(intHead)("FormFieldID")
                        strPropertyName = dataTableHeading.Rows(intHead)("vcPropertyName")

                        Dim objPropValue As Object = Nothing

                        If CBool(dataTableHeading.Rows(intHead)("bitCustomField")) = True Then
                            Dim drCustom As DataRow
                            drCustom = dtCustomField.NewRow
                            drCustom("Fld_ID") = strFormFieldID
                            drCustom("Name") = strFormFieldName

                            If CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "checkbox" Then
                                If CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "true" OrElse _
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "yes" OrElse _
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "1" OrElse _
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "y" Then
                                    drCustom("Fld_Value") = 1
                                ElseIf CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "false" OrElse _
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "no" OrElse _
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "0" OrElse _
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "n" Then
                                    drCustom("Fld_Value") = 0
                                End If
                            Else
                                drCustom("Fld_Value") = dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)
                            End If

                            dtCustomField.Rows.Add(drCustom)
                            dtCustomField.AcceptChanges()
                        Else
                            objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                        End If


                        If strFormFieldName = "numVendorID" AndAlso lngMasterID = CCommon.ToInteger(IMPORT_TYPE.Item_Vendor) Then
                            Dim dr As DataRow() = dataTableHeading.Select("dbFieldName='numItemCode'")

                            If Not dr Is Nothing AndAlso dr.Length > 0 Then
                                objClass.ItemCode = CCommon.ToLong(dtDataHold.Rows(intC)(dr(0)("FormFieldID") & "_" & dr(0)("dbFieldName")))
                            End If

                            objClass.VendorID = CCommon.ToLong(objPropValue)
                            objClass.DomainID = DomainID
                            objClass.GetItemVendorDetails()
                        End If

                        If CCommon.ToString(strPropertyName).Length > 0 Then
                            SetProperty(objClass, strPropertyName, objPropValue)
                        End If
                    Next


                    SetProperty(objClass, "DomainID", DomainID)
                    SetProperty(objClass, "UserCntID", UserCntID)

                    If CCommon.ToString(DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).SerialNo).Contains(",") Then
                        Throw New Exception("MULTIPLE_SERIAL_NO")
                    End If


                    If tintImportType = 1 AndAlso CCommon.ToLong(DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).VendorTcode) = 0 Then 'Update
                        Throw New Exception("INVALID_ITEM_VENDOR")
                    ElseIf tintImportType = 2 Then 'Insert
                        DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).VendorTcode = 0
                    End If

                    If CCommon.ToLong(DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).VendorTcode) > 0 Then
                        isInsert = False
                    Else
                        isInsert = True
                    End If

                    If tintImportType = 2 Or (tintImportType = 3 AndAlso CCommon.ToLong(objClass.VendorTcode) = 0) Then
                        Dim objCom As New CCommon
                        objCom.DomainID = DomainID
                        Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(20, 1, CCommon.ToLong(dtDataHold.Rows(intC)("211_numItemCode")))
                    ElseIf tintImportType = 1 AndAlso CCommon.ToLong(objClass.VendorTcode) = 0 Then
                        Throw New Exception("INVALID_ITEM_OR_VENDOR")
                    End If

                    objItemValue = objItemSave.Invoke(objClass, New Object() {})

                    objCommon.Mode = 36
                    objCommon.UpdateRecordID = intImportFileID
                    objCommon.UpdateValueID = lngCSVRow
                    objCommon.DomainID = DomainID
                    objCommon.UpdateSingleFieldValue()

                    Dim lngItemVendorID As Long = objClass.GetType().GetProperty(strGetPrimaryColumnName).GetValue(objClass, Nothing)


                    Dim message As String
                    If isInsert Then
                        'message = "CSV Row number: " & lngCSVRow & " - Item vendor inserted with " & strGetPrimaryColumnName & ":" & lngItemVendorID
                        strAddedItemVendorIds = lngItemVendorID & "," & strAddedItemVendorIds
                    Else
                        'message = "CSV Row number: " & lngCSVRow & " - Item vendor updated with " & strGetPrimaryColumnName & ":" & lngItemVendorID
                        strUpdatedItemVendorIds = lngItemVendorID & "," & strUpdatedItemVendorIds
                    End If
                    'WriteMessage(message)


                    If isInsert Then
                        intInserts = intInserts + 1
                    Else
                        intUpdates = intUpdates + 1
                    End If

                    Try
                        objImport = New ImportWizard(intImportFileID, DomainID)
                        With objImport
                            .ImportFileID = intImportFileID
                            .SaveImportFileReport(lngCSVRow, message, lngItemVendorID, "", "")
                        End With
                    Catch exM As Exception
                        'DO NOT THROW ERROR
                    End Try
                Catch ex As Exception
                    Dim errorMessage As String

                    If ex.Message.Contains("VENDOR_REQUIRED") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("VENDOR_REQUIRED")) Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Vendor is reqired to update item vendor(s))"
                    ElseIf ex.Message.Contains("INVALID_ITEM_VENDOR") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("INVALID_ITEM_VENDOR")) Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Invalid vendor)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ITEM_CODE_REQUIRED")) Or ex.Message.Contains("ITEM_CODE_REQUIRED") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Item Code is required to insert vendor(s) for items)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ITEM_NOT_FOUND_WITH_GIVEN_ITEMCODE")) Or ex.Message.Contains("ITEM_NOT_FOUND_WITH_GIVEN_ITEMCODE") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (No item matched to item code for update)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("MULTIPLE_ITEMS_FOUND_WITH_GIVEN_ITEMCODE")) Or ex.Message.Contains("MULTIPLE_ITEMS_FOUND_WITH_GIVEN_ITEMCODE") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Multiple items matched to item code for update)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("INVALID_ITEM_OR_VENDOR")) Or ex.Message.Contains("INVALID_ITEM_OR_VENDOR") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (No record found to update with given item and vendor)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("MULTIPLE_SERIAL_NO")) Or ex.Message.Contains("MULTIPLE_SERIAL_NO") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (You can add one serial no per row)"
                    Else
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Unknown error occured"
                    End If

                    WriteMessage(errorMessage)

                    Try
                        objImport = New ImportWizard(intImportFileID, DomainID)
                        With objImport
                            .ImportFileID = intImportFileID
                            .SaveImportFileReport(lngCSVRow, errorMessage, 0, If(Not ex.InnerException Is Nothing, ex.InnerException.Message, ex.Message), If(Not ex.InnerException Is Nothing, ex.InnerException.StackTrace, ex.StackTrace))
                        End With
                    Catch exM As Exception
                        'DO NOT THROW ERROR
                    End Try

                    intErrors = intErrors + 1
                End Try

                intCounter = intCounter + 1
            Next

            strAddedItemVendorIds = strAddedItemVendorIds.Trim(",")
            strUpdatedItemVendorIds = strUpdatedItemVendorIds.Trim(",")

            UpdateImportFileStatus(intInserts, intUpdates, intErrors, strAddedItemVendorIds, strUpdatedItemVendorIds, strImportType)

            WriteMessage("End Time :" & DateTime.Now)
            WriteMessage("Import Duration (In Seconds) :" & DateDiff(DateInterval.Second, dtStartTime, DateTime.Now))
            WriteMessage(strImportType & " records created: " & intInserts & If(strAddedItemVendorIds.Length > 0, " : " & strAddedItemVendorIds, ""))
            WriteMessage(strImportType & " records Updated: " & intUpdates & If(strUpdatedItemVendorIds.Length > 0, " : " & strUpdatedItemVendorIds, ""))
            WriteMessage(strImportType & " records have Errors: " & intErrors)
        Catch ex As Exception

        End Try
    End Sub

    Public Sub ImportAddressDetails(ByVal dtDataHold As DataTable)
        Dim intInserts As Integer = 0
        Dim intUpdates As Integer = 0
        Dim intErrors As Integer = 0
        Dim isInsert As Boolean = False

        Dim strGetPrimaryColumnName As String = "AddressID"
        Dim strFormFieldName As String = ""
        Dim strFormFieldID As Object = Nothing
        Dim vcAssociatedControlType As String = ""
        Dim strValueString As String = ""
        Dim strImportType As String = "Address Details"
        Dim PropertyItem As PropertyInfo = Nothing
        Dim objClass As Object = Nothing
        Dim strPropertyName As String
        Dim objItemSave As MethodInfo = Nothing
        Dim objItemValue As New Object
        Dim intCounter As Long = 0
        Dim strAddedItemVendorIds As String = ""
        Dim strUpdatedItemVendorIds As String = ""

        Try
            Dim dtCustomField As New DataTable
            dtCustomField.Columns.Add("Fld_ID").SetOrdinal(0)
            dtCustomField.Columns.Add("Name").SetOrdinal(1)
            dtCustomField.Columns.Add("Fld_Value").SetOrdinal(2)
            dtCustomField.TableName = "CusFlds"
            dtCustomField.AcceptChanges()

            For intC As Integer = If(lngStartRow = 0, 0, lngStartRow - 1) To dtDataHold.Rows.Count - 1
                lngCSVRow = intC + 2

                If intCounter = lngWaitRecords Then
                    intCounter = 0
                    WriteMessage("Thread Sleep for " & (lngWaitMiliSeconds) & " miliseconds.")
                    Thread.Sleep(lngWaitMiliSeconds)
                End If

                objClass = New CContacts
                If objItemSave Is Nothing Then objItemSave = objClass.GetType().GetMethod("ManageAddress")

                If dtDataHold.Columns.Contains("529_numAddressID") Then
                    dtDataHold.Columns("529_numAddressID").SetOrdinal(0)
                End If


                If tintImportType = 1 AndAlso Not dtDataHold.Columns.Contains("529_numAddressID") Then
                    Throw New Exception("ADDRESSID_REQUIRED")
                End If

                Dim dvTempToSort As DataView
                dvTempToSort = dataTableHeading.DefaultView
                dvTempToSort.Sort = "IsPrimary DESC"
                dataTableHeading = dvTempToSort.ToTable()

                Try
                    For intHead As Integer = 0 To dataTableHeading.Rows.Count - 1
                        If dataTableHeading.Rows(intHead)("dbFieldName") Is DBNull.Value OrElse dataTableHeading.Rows(intHead)("dbFieldName") = "" Then
                            Continue For
                        End If

                        strFormFieldName = dataTableHeading.Rows(intHead)("dbFieldName")
                        strFormFieldID = dataTableHeading.Rows(intHead)("FormFieldID")
                        strPropertyName = dataTableHeading.Rows(intHead)("vcPropertyName")

                        Dim objPropValue As Object = Nothing

                        If CBool(dataTableHeading.Rows(intHead)("bitCustomField")) = True Then
                            Dim drCustom As DataRow
                            drCustom = dtCustomField.NewRow
                            drCustom("Fld_ID") = strFormFieldID
                            drCustom("Name") = strFormFieldName

                            If CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "checkbox" Then
                                If CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "true" OrElse _
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "yes" OrElse _
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "1" OrElse _
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "y" Then
                                    drCustom("Fld_Value") = 1
                                ElseIf CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "false" OrElse _
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "no" OrElse _
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "0" OrElse _
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "n" Then
                                    drCustom("Fld_Value") = 0
                                End If
                            Else
                                drCustom("Fld_Value") = dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)
                            End If

                            dtCustomField.Rows.Add(drCustom)
                            dtCustomField.AcceptChanges()
                        Else
                            objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                        End If


                        If strPropertyName.Contains("AddressType") = True Then
                            If CCommon.ToString(objPropValue).ToLower().Contains("bill") Then
                                objPropValue = CContacts.enmAddressType.BillTo
                            Else
                                objPropValue = CContacts.enmAddressType.ShipTo
                            End If

                        ElseIf strPropertyName.Contains("AddresOf") = True AndAlso lngMasterID = IMPORT_TYPE.Address_Details Then
                            If CCommon.ToString(objPropValue).ToLower().Contains("org") Then
                                objPropValue = CContacts.enmAddressOf.Organization

                            ElseIf CCommon.ToString(objPropValue).ToLower().Contains("con") Then
                                objPropValue = CContacts.enmAddressOf.Contact
                                CType(objClass, CContacts).AddressType = CContacts.enmAddressType.None
                            End If
                        End If

                        If CCommon.ToString(strPropertyName).Length > 0 Then
                            SetProperty(objClass, strPropertyName, objPropValue)
                        End If
                    Next


                    SetProperty(objClass, "DomainID", DomainID)
                    SetProperty(objClass, "UserCntID", UserCntID)


                    If tintImportType = 1 AndAlso CCommon.ToLong(DirectCast(objClass, BACRM.BusinessLogic.Contacts.CContacts).AddressID) = 0 Then 'Update
                        Throw New Exception("INVALID_ADDRESS_ID")
                    ElseIf tintImportType = 2 Then 'Insert
                        DirectCast(objClass, BACRM.BusinessLogic.Contacts.CContacts).AddressID = 0
                    End If

                    If CCommon.ToLong(DirectCast(objClass, BACRM.BusinessLogic.Contacts.CContacts).AddressID) > 0 Then
                        isInsert = False
                    Else
                        isInsert = True
                    End If

                    If tintImportType = 2 Then
                        If CType(objClass, CContacts).AddresOf = CContacts.enmAddressOf.Organization Then
                            Dim objCom As New CCommon
                            objCom.DomainID = DomainID
                            Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(133, 2, CCommon.ToString(dtDataHold.Rows(intC)("531_RecordID")))
                        ElseIf CType(objClass, CContacts).AddresOf = CContacts.enmAddressOf.Contact Then
                            Dim objCom As New CCommon
                            objCom.DomainID = DomainID
                            Dim dtLinkingData As DataTable = objCom.GetRecordByLinkingID(134, 2, CCommon.ToString(dtDataHold.Rows(intC)("531_RecordID")))
                        End If
                    End If

                    objItemValue = objItemSave.Invoke(objClass, New Object() {})

                    objCommon.Mode = 36
                    objCommon.UpdateRecordID = intImportFileID
                    objCommon.UpdateValueID = lngCSVRow
                    objCommon.DomainID = DomainID
                    objCommon.UpdateSingleFieldValue()

                    Dim lngItemVendorID As Long = objClass.GetType().GetProperty(strGetPrimaryColumnName).GetValue(objClass, Nothing)

                    Dim message As String
                    If isInsert Then
                        'message = "CSV Row number: " & lngCSVRow & " - Address Details inserted with " & strGetPrimaryColumnName & ":" & lngItemVendorID
                        strAddedItemVendorIds = lngItemVendorID & "," & strAddedItemVendorIds
                    Else
                        'message = "CSV Row number: " & lngCSVRow & " - Address Details updated with " & strGetPrimaryColumnName & ":" & lngItemVendorID
                        strUpdatedItemVendorIds = lngItemVendorID & "," & strUpdatedItemVendorIds
                    End If
                    'WriteMessage(message)


                    If isInsert Then
                        intInserts = intInserts + 1
                    Else
                        intUpdates = intUpdates + 1
                    End If

                    Try
                        objImport = New ImportWizard(intImportFileID, DomainID)
                        With objImport
                            .ImportFileID = intImportFileID
                            .SaveImportFileReport(lngCSVRow, message, lngItemVendorID, "", "")
                        End With
                    Catch exM As Exception
                        'DO NOT THROW ERROR
                    End Try
                Catch ex As Exception
                    Dim errorMessage As String

                    If ex.Message.Contains("ADDRESSID_REQUIRED") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ADDRESSID_REQUIRED")) Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Address ID is reqired to update address)"
                    ElseIf ex.Message.Contains("INVALID_ADDRESS_ID") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("INVALID_ADDRESS_ID")) Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Invalid Address ID)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ORGANIZATION_NOT_FOUND_WITH_GIVEN_ORGANIZATION ID")) Or ex.Message.Contains("ORGANIZATION_NOT_FOUND_WITH_GIVEN_ORGANIZATION ID") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (No organization matched to record id for insert)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("MULTIPLE_ORGANIZATIONS_FOUND_WITH_GIVEN_ORGANIZATION ID")) Or ex.Message.Contains("MULTIPLE_ORGANIZATIONS_FOUND_WITH_GIVEN_ORGANIZATION ID") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Multiple organizations matched to record id for insert)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("CONTACT_NOT_FOUND_WITH_GIVEN_CONTACT ID")) Or ex.Message.Contains("CONTACT_NOT_FOUND_WITH_GIVEN_CONTACT ID") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (No contact matched to record id for insert)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("MULTIPLE_CONTACTS_FOUND_WITH_GIVEN_CONTACT ID")) Or ex.Message.Contains("MULTIPLE_CONTACTS_FOUND_WITH_GIVEN_CONTACT ID") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Multiple contacts matched to record id for insert)"
                    Else
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Unknown error occured"
                    End If

                    WriteMessage(errorMessage)

                    Try
                        objImport = New ImportWizard(intImportFileID, DomainID)
                        With objImport
                            .ImportFileID = intImportFileID
                            .SaveImportFileReport(lngCSVRow, errorMessage, 0, If(Not ex.InnerException Is Nothing, ex.InnerException.Message, ex.Message), If(Not ex.InnerException Is Nothing, ex.InnerException.StackTrace, ex.StackTrace))
                        End With
                    Catch exM As Exception
                        'DO NOT THROW ERROR
                    End Try

                    intErrors = intErrors + 1
                End Try

                intCounter = intCounter + 1
            Next

            strAddedItemVendorIds = strAddedItemVendorIds.Trim(",")
            strUpdatedItemVendorIds = strUpdatedItemVendorIds.Trim(",")

            UpdateImportFileStatus(intInserts, intUpdates, intErrors, strAddedItemVendorIds, strUpdatedItemVendorIds, strImportType)

            WriteMessage("End Time :" & DateTime.Now)
            WriteMessage("Import Duration (In Seconds) :" & DateDiff(DateInterval.Second, dtStartTime, DateTime.Now))
            WriteMessage(strImportType & " records created: " & intInserts & If(strAddedItemVendorIds.Length > 0, " : " & strAddedItemVendorIds, ""))
            WriteMessage(strImportType & " records Updated: " & intUpdates & If(strUpdatedItemVendorIds.Length > 0, " : " & strUpdatedItemVendorIds, ""))
            WriteMessage(strImportType & " records have Errors: " & intErrors)
        Catch ex As Exception

        End Try
    End Sub

    Public Sub ImportKitSubItems(ByVal dtDataHold As DataTable)
        Dim intInserts As Integer = 0
        Dim intUpdates As Integer = 0
        Dim intErrors As Integer = 0
        Dim isInsert As Boolean = False

        Dim strGetPrimaryColumnName As String = "ItemDetailID"
        Dim strFormFieldName As String = ""
        Dim strFormFieldID As Object = Nothing
        Dim vcAssociatedControlType As String = ""
        Dim strValueString As String = ""
        Dim strImportType As String = "Item Assembly"
        Dim PropertyItem As PropertyInfo = Nothing
        Dim objClass As Object = Nothing
        Dim strPropertyName As String
        Dim objItemSave As MethodInfo = Nothing
        Dim objItemValue As New Object
        Dim intCounter As Long = 0
        Dim strAddedItemDetailIds As String = ""
        Dim strUpdatedItemDetailIds As String = ""
        Try
            Dim dtCustomField As New DataTable
            dtCustomField.Columns.Add("Fld_ID").SetOrdinal(0)
            dtCustomField.Columns.Add("Name").SetOrdinal(1)
            dtCustomField.Columns.Add("Fld_Value").SetOrdinal(2)
            dtCustomField.TableName = "CusFlds"
            dtCustomField.AcceptChanges()

            For intC As Integer = If(lngStartRow = 0, 0, lngStartRow - 1) To dtDataHold.Rows.Count - 1
                Try
                    lngCSVRow = intC + 2

                    If intCounter = lngWaitRecords Then
                        intCounter = 0
                        WriteMessage("Thread Sleep for " & (lngWaitMiliSeconds) & " miliseconds.")
                        Thread.Sleep(lngWaitMiliSeconds)
                    End If

                    objClass = New CItems
                    If objItemSave Is Nothing Then objItemSave = objClass.GetType().GetMethod("InsertAssemblyItems")
                    If dtDataHold.Columns.Contains("385_numItemDetailID") Then dtDataHold.Columns("385_numItemDetailID").SetOrdinal(0)

                    If tintImportType = 1 AndAlso Not dtDataHold.Columns.Contains("385_numItemDetailID") Then
                        Throw New Exception("ITEM_DETAIL_ID_REQUIRED")
                    End If

                    Dim dvTempToSort As DataView
                    dvTempToSort = dataTableHeading.DefaultView
                    dvTempToSort.Sort = "IsPrimary DESC"
                    dataTableHeading = dvTempToSort.ToTable()


                    For intHead As Integer = 0 To dataTableHeading.Rows.Count - 1
                        If dataTableHeading.Rows(intHead)("dbFieldName") Is DBNull.Value OrElse dataTableHeading.Rows(intHead)("dbFieldName") = "" Then
                            Continue For
                        End If

                        strFormFieldName = dataTableHeading.Rows(intHead)("dbFieldName")
                        strFormFieldID = dataTableHeading.Rows(intHead)("FormFieldID")
                        strPropertyName = dataTableHeading.Rows(intHead)("vcPropertyName")

                        Dim objPropValue As Object = Nothing

                        If CBool(dataTableHeading.Rows(intHead)("bitCustomField")) = True Then
                            Dim drCustom As DataRow
                            drCustom = dtCustomField.NewRow
                            drCustom("Fld_ID") = strFormFieldID
                            drCustom("Name") = strFormFieldName

                            If CCommon.ToString(dataTableHeading.Rows(intHead)("vcAssociatedControlType")).ToLower() = "checkbox" Then
                                If CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "true" OrElse _
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "yes" OrElse _
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "1" OrElse _
                                 CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "y" Then
                                    drCustom("Fld_Value") = 1
                                ElseIf CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "false" OrElse _
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "no" OrElse _
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "0" OrElse _
                                    CCommon.ToString(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)).ToLower = "n" Then
                                    drCustom("Fld_Value") = 0
                                End If
                            Else
                                drCustom("Fld_Value") = dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName)
                            End If

                            dtCustomField.Rows.Add(drCustom)
                            dtCustomField.AcceptChanges()
                        Else
                            objPropValue = CType(dtDataHold.Rows(intC)(strFormFieldID & "_" & strFormFieldName), Object)
                        End If

                        If strFormFieldName = "numItemDetailID" Then
                            objClass.ItemDetailID = CCommon.ToLong(objPropValue)
                            objClass.DomainID = DomainID
                            objClass.GetKitSubItemDetails()
                        End If

                        If CCommon.ToString(strPropertyName).Length > 0 Then
                            SetProperty(objClass, strPropertyName, objPropValue)
                        End If
                    Next

                    SetProperty(objClass, "DomainID", DomainID)
                    SetProperty(objClass, "UserCntID", UserCntID)

                    If tintImportType = 1 AndAlso CCommon.ToLong(DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).ItemDetailID) = 0 Then 'Update
                        Throw New Exception("INVALID_ITEM_DETAIL_ID")
                    ElseIf tintImportType = 2 Then 'Insert
                        DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).ItemDetailID = 0
                    End If

                    If CCommon.ToLong(DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).ItemDetailID) > 0 Then
                        isInsert = False
                    Else
                        isInsert = True
                    End If

                    If tintImportType = 2 AndAlso CCommon.ToLong(DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).ItemCode) = 0 Or CCommon.ToLong(DirectCast(objClass, BACRM.BusinessLogic.Item.CItems).ChildItemID) = 0 Then
                        Throw New Exception("INVALID_ITEM")
                    End If

                    objItemValue = objItemSave.Invoke(objClass, New Object() {})

                    objCommon.Mode = 36
                    objCommon.UpdateRecordID = intImportFileID
                    objCommon.UpdateValueID = lngCSVRow
                    objCommon.DomainID = DomainID
                    objCommon.UpdateSingleFieldValue()

                    Dim lngItemVendorID As Long = objClass.GetType().GetProperty(strGetPrimaryColumnName).GetValue(objClass, Nothing)


                    Dim message As String
                    If isInsert Then
                        'message = "CSV Row number: " & lngCSVRow & " - Item Assembly/Kit inserted with " & strGetPrimaryColumnName & ":" & lngItemVendorID
                        strAddedItemDetailIds = lngItemVendorID & "," & strAddedItemDetailIds
                    Else
                        'message = "CSV Row number: " & lngCSVRow & " - Item Assembly/Kit updated with " & strGetPrimaryColumnName & ":" & lngItemVendorID
                        strUpdatedItemDetailIds = lngItemVendorID & "," & strUpdatedItemDetailIds
                    End If

                    'WriteMessage(message)


                    If isInsert Then
                        intInserts = intInserts + 1
                    Else
                        intUpdates = intUpdates + 1
                    End If

                    Try
                        objImport = New ImportWizard(intImportFileID, DomainID)
                        With objImport
                            .ImportFileID = intImportFileID
                            .SaveImportFileReport(lngCSVRow, message, lngItemVendorID, "", "")
                        End With
                    Catch exM As Exception
                        'DO NOT THROW ERROR
                    End Try
                Catch ex As Exception
                    Dim errorMessage As String

                    If ex.Message.Contains("ITEM_DETAIL_ID_REQUIRED") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("ITEM_DETAIL_ID_REQUIRED")) Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Item Detail ID is required for update)"
                    ElseIf ex.Message.Contains("INVALID_ITEM_DETAIL_ID") Or (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("INVALID_ITEM_DETAIL_ID")) Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Invalid item detail id.)"
                    ElseIf (Not ex.InnerException Is Nothing AndAlso ex.InnerException.Message.Contains("INVALID_ITEM")) Or ex.Message.Contains("INVALID_ITEM") Then
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Error occured (Invalid item name or child item name)"
                    Else
                        errorMessage = "CSV Row number: " & lngCSVRow & " - Unknown error occured"
                    End If

                    WriteMessage(errorMessage)

                    Try
                        objImport = New ImportWizard(intImportFileID, DomainID)
                        With objImport
                            .ImportFileID = intImportFileID
                            .SaveImportFileReport(lngCSVRow, errorMessage, 0, If(Not ex.InnerException Is Nothing, ex.InnerException.Message, ex.Message), If(Not ex.InnerException Is Nothing, ex.InnerException.StackTrace, ex.StackTrace))
                        End With
                    Catch exM As Exception
                        'DO NOT THROW ERROR
                    End Try

                    intErrors = intErrors + 1
                End Try

                intCounter = intCounter + 1
            Next

            strAddedItemDetailIds = strAddedItemDetailIds.Trim(",")
            strUpdatedItemDetailIds = strUpdatedItemDetailIds.Trim(",")

            UpdateImportFileStatus(intInserts, intUpdates, intErrors, strAddedItemDetailIds, strUpdatedItemDetailIds, strImportType)

            WriteMessage("End Time :" & DateTime.Now)
            WriteMessage("Import Duration (In Seconds) :" & DateDiff(DateInterval.Second, dtStartTime, DateTime.Now))
            WriteMessage(strImportType & " records created: " & intInserts & If(strAddedItemDetailIds.Length > 0, " : " & strAddedItemDetailIds, ""))
            WriteMessage(strImportType & " records Updated: " & intUpdates & If(strUpdatedItemDetailIds.Length > 0, " : " & strUpdatedItemDetailIds, ""))
            WriteMessage(strImportType & " records have Errors: " & intErrors)
        Catch ex As Exception

        End Try
    End Sub

    Public Sub ImportOrder(ByVal dtDataHold As DataTable, ByVal oppType As Short, ByVal linkingID As Short)
        Dim intInserts As Integer = 0
        Dim intErrors As Integer = 0
        Dim strImportType As String = If(oppType = 1, "Sales Order", "Purchase Order")
        Dim strAddedOrderIds As String = ""

        Try
            Dim objItem As CItems
            Dim objOrderImport As OrderImport
            Dim objOrderImportLineItem As OrderImportLineItem

            Dim listInvalidCustomer As New List(Of String)
            Dim listInvalidItems As New List(Of String)
            Dim listValidCustomers As New List(Of Tuple(Of String, Long, Long))
            Dim listValidWaehouses As New List(Of Tuple(Of Long, Long, Long, Long))
            Dim listCSVRows As New List(Of OrderImportLineItem)

            Dim listOrders As New List(Of OrderImport)

            Try
                Dim isValidConfiguration As Boolean = True
                Dim errorMessage As String = ""

                If linkingID = 189 And Not dtDataHold.Columns.Contains("189_vcItemName") Then
                    isValidConfiguration = False
                    errorMessage = "Item Name is selected to pick items but it is not added in csv file or mapped to import order(s)."
                ElseIf linkingID = 281 AndAlso Not dtDataHold.Columns.Contains("281_vcSKU") Then
                    isValidConfiguration = False
                    errorMessage = "SKU is selected to pick items but it is not added in csv file or mapped to import order(s)."
                ElseIf linkingID = 203 AndAlso Not dtDataHold.Columns.Contains("203_numBarCodeId") Then
                    isValidConfiguration = False
                    errorMessage = "UPC is selected to pick items but it is not added in csv file or mapped for import."
                ElseIf linkingID = 899 AndAlso Not dtDataHold.Columns.Contains("899_CustomerPartNo") Then
                    isValidConfiguration = False
                    errorMessage = "Customer Part# is selected to pick items but it is not added in csv file or mapped to import order(s)."
                ElseIf linkingID = 211 AndAlso Not dtDataHold.Columns.Contains("211_numItemCode") Then
                    isValidConfiguration = False
                    errorMessage = "Item ID is selected to pick items but it is not added in csv file or mapped to import order(s)."
                ElseIf linkingID = 291 AndAlso Not dtDataHold.Columns.Contains("291_vcPartNo") Then
                    isValidConfiguration = False
                    errorMessage = "Vendor Part# is selected to pick items but it is not added in csv file or mapped to import order(s)."
                End If

                If Not dtDataHold.Columns.Contains("258_numUnitHour") Then
                    isValidConfiguration = False
                    errorMessage = "Units is required to import order(s)."
                End If
                If Not dtDataHold.Columns.Contains("259_monPrice") Then
                    isValidConfiguration = False
                    errorMessage = "Unit Price is required to import order(s)."
                End If
                If Not dtDataHold.Columns.Contains("293_numWarehouseID") Then
                    isValidConfiguration = False
                    errorMessage = "WareHouse is required to import order(s)."
                End If

                If isValidConfiguration Then
                    If IsSingleOrder Then
                        If IsExistingOrganization Then
                            If CCommon.ToLong(DivisionID) > 0 AndAlso CCommon.ToLong(ContactID) > 0 Then
                                objOrderImport = New OrderImport
                                objOrderImport.DivisionID = CCommon.ToLong(DivisionID)
                                objOrderImport.ContactID = CCommon.ToLong(ContactID)
                                listOrders.Add(objOrderImport)
                            Else
                                isValidConfiguration = False
                                errorMessage = "Organization and Contact needs to be selected when order needs to be imported for existing organization."
                            End If
                        Else
                            If Not String.IsNullOrEmpty(CompanyName) AndAlso Not String.IsNullOrEmpty(ContactFirstName) AndAlso Not String.IsNullOrEmpty(ContactLastName) Then
                                objOrderImport = New OrderImport
                                objOrderImport.CompanyName = CompanyName
                                objOrderImport.ContactFirstName = ContactFirstName
                                objOrderImport.ContactLastName = ContactLastName
                                objOrderImport.ContactEmail = ContactEmail
                                listOrders.Add(objOrderImport)
                            Else
                                isValidConfiguration = False
                                errorMessage = "Organization Name, Contact First & Last Name are required when order needs to be imported for new organization."
                            End If

                        End If
                    Else
                        If IsMatchOrganizationID Then
                            If Not dtDataHold.Columns.Contains("539_numDivisionID") Then
                                isValidConfiguration = False
                                errorMessage = "Organization ID is required when it is used to pick customer to import order(s)"
                            End If
                        Else
                            If MatchFieldID = 0 Then
                                isValidConfiguration = False
                                errorMessage = "Second match field with Organization Name is required to pick customer to import order(s)"
                            ElseIf Not dtDataHold.Columns.Contains("3_vcCompanyName") Then
                                isValidConfiguration = False
                                errorMessage = "Organization Name is required to pick customer to import order(s)"
                            Else
                                Dim isMatchFieldMapped As Boolean = False

                                For Each column As DataColumn In dtDataHold.Columns
                                    If column.ColumnName.StartsWith(MatchFieldID & "_") Then
                                        isMatchFieldMapped = True
                                        Exit For
                                    End If
                                Next

                                If Not isMatchFieldMapped Then
                                    isValidConfiguration = False
                                    errorMessage = "Organization and Contact needs to be selected when order needs to be imported for existing organization."
                                End If
                            End If
                        End If
                    End If
                End If

                If isValidConfiguration Then
                    Dim dtAccount As DataTable
                    For Each dr As DataRow In dtDataHold.Rows
                        lngCSVRow = lngCSVRow + 1

                        Try
                            If IsSingleOrder Then
                                objOrderImport = listOrders.Item(0)
                            Else
                                If IsMatchOrganizationID Then
                                    If listInvalidCustomer.Where(Function(x) x = CCommon.ToString(dr("539_numDivisionID"))).Count = 0 Then
                                        If listValidCustomers.Exists(Function(x) x.Item1 = CCommon.ToString(dr("539_numDivisionID"))) Then
                                            Dim tempTuple As Tuple(Of String, Long, Long) = listValidCustomers.First(Function(x) x.Item1 = CCommon.ToString(dr("539_numDivisionID")))
                                            objOrderImport = listOrders.Find(Function(x) x.DivisionID = tempTuple.Item2 AndAlso x.ContactID = tempTuple.Item3)
                                        Else
                                            Dim objAccount As New CAccounts
                                            objAccount.DivisionID = CCommon.ToLong(dr("539_numDivisionID"))
                                            dtAccount = objAccount.GetDefaultSettingValue("numPrimaryContact")

                                            If Not dtAccount Is Nothing AndAlso dtAccount.Rows.Count > 0 Then
                                                objOrderImport = New OrderImport
                                                objOrderImport.DivisionID = CCommon.ToLong(dr("539_numDivisionID"))
                                                objOrderImport.ContactID = CCommon.ToLong(dtAccount.Rows(0)("numContactID"))
                                                listOrders.Add(objOrderImport)
                                                listValidCustomers.Add(New Tuple(Of String, Long, Long)(CCommon.ToString(dr("539_numDivisionID")), objOrderImport.DivisionID, objOrderImport.ContactID))
                                            Else
                                                listInvalidCustomer.Add(CCommon.ToString(dr("539_numDivisionID")))
                                                listCSVRows.Add(New OrderImportLineItem() With {.CSVRow = lngCSVRow, .IsValid = False, .ErrorMessage = "CSV Row number: " & lngCSVRow & " - Customer does not exists"})
                                                Continue For
                                            End If
                                        End If
                                    Else
                                        listCSVRows.Add(New OrderImportLineItem() With {.CSVRow = lngCSVRow, .IsValid = False, .ErrorMessage = "CSV Row number: " & lngCSVRow & " - Customer does not exists"})
                                        Continue For
                                    End If
                                Else
                                    Dim vcMatchFieldValue As String = ""

                                    Select Case MatchFieldID
                                        Case 14
                                            vcMatchFieldValue = CCommon.ToString(dr("14_numAssignedTo"))
                                        Case 28
                                            vcMatchFieldValue = CCommon.ToString(dr("28_numCompanyIndustry"))
                                        Case 539
                                            vcMatchFieldValue = CCommon.ToString(dr("539_numDivisionID"))
                                        Case 3
                                            vcMatchFieldValue = CCommon.ToString(dr("3_vcCompanyName"))
                                        Case 10
                                            vcMatchFieldValue = CCommon.ToString(dr("10_vcComPhone"))
                                        Case 5
                                            vcMatchFieldValue = CCommon.ToString(dr("5_vcProfile"))
                                        Case 30
                                            vcMatchFieldValue = CCommon.ToString(dr("30_numStatusID"))
                                        Case 6
                                            vcMatchFieldValue = CCommon.ToString(dr("6_numCompanyType"))
                                        Case 451
                                            vcMatchFieldValue = CCommon.ToString(dr("451_tintCRMType"))
                                        Case 21
                                            vcMatchFieldValue = CCommon.ToString(dr("21_numTerID"))
                                        Case 535
                                            vcMatchFieldValue = CCommon.ToString(dr("535_vcOppRefOrderNo"))
                                    End Select

                                    If listInvalidCustomer.Where(Function(x) x = CCommon.ToString(dr("3_vcCompanyName")) & "#" & vcMatchFieldValue).Count = 0 Then
                                        If listValidCustomers.Exists(Function(x) x.Item1 = CCommon.ToString(dr("3_vcCompanyName")) & "#" & vcMatchFieldValue) Then
                                            Dim tempTuple As Tuple(Of String, Long, Long) = listValidCustomers.First(Function(x) x.Item1 = CCommon.ToString(dr("3_vcCompanyName")) & "#" & vcMatchFieldValue)
                                            objOrderImport = listOrders.Find(Function(x) x.MatchID = CCommon.ToString(dr("3_vcCompanyName")) & "#" & vcMatchFieldValue AndAlso x.DivisionID = tempTuple.Item2 AndAlso x.ContactID = tempTuple.Item3)
                                        Else
                                            Try
                                                Dim objAccount As New CAccounts
                                                objAccount.DomainID = DomainID
                                                dtAccount = objAccount.OrderImportGetCustomer(CCommon.ToString(dr("3_vcCompanyName")), MatchFieldID, vcMatchFieldValue)

                                                If Not dtAccount Is Nothing AndAlso dtAccount.Rows.Count > 0 Then
                                                    objOrderImport = New OrderImport
                                                    objOrderImport.MatchID = CCommon.ToString(dr("3_vcCompanyName")) & "#" & vcMatchFieldValue
                                                    objOrderImport.DivisionID = CCommon.ToLong(dtAccount.Rows(0)("numDivisionID"))
                                                    objOrderImport.ContactID = CCommon.ToLong(dtAccount.Rows(0)("numContactID"))
                                                    listOrders.Add(objOrderImport)
                                                    listValidCustomers.Add(New Tuple(Of String, Long, Long)(CCommon.ToString(dr("3_vcCompanyName")) & "#" & vcMatchFieldValue, objOrderImport.DivisionID, objOrderImport.ContactID))
                                                Else
                                                    listInvalidCustomer.Add(CCommon.ToString(dr("3_vcCompanyName")) & "#" & vcMatchFieldValue)
                                                    listCSVRows.Add(New OrderImportLineItem() With {.CSVRow = lngCSVRow, .IsValid = False, .ErrorMessage = "CSV Row number: " & lngCSVRow & " - Customer does not exists"})
                                                    Continue For
                                                End If
                                            Catch ex As Exception
                                                listInvalidCustomer.Add(CCommon.ToString(dr("3_vcCompanyName")) & "#" & vcMatchFieldValue)
                                                listCSVRows.Add(New OrderImportLineItem() With {.CSVRow = lngCSVRow, .IsValid = False, .ErrorMessage = "CSV Row number: " & lngCSVRow & " - Error occurred while finding Customer:" & ex.Message})
                                                Continue For
                                            End Try
                                        End If
                                    Else
                                        listCSVRows.Add(New OrderImportLineItem() With {.CSVRow = lngCSVRow, .IsValid = False, .ErrorMessage = "CSV Row number: " & lngCSVRow & " - Customer does not exists"})
                                        Continue For
                                    End If
                                End If
                            End If

                            If Not objOrderImport Is Nothing Then
                                If objOrderImport.LineItems Is Nothing Then
                                    objOrderImport.LineItems = New List(Of OrderImportLineItem)
                                End If
                            End If

                            Dim searchField As String = ""
                            Dim searchText As String = ""
                            If linkingID = 189 Then
                                searchText = CCommon.ToString(dr("189_vcItemName"))
                                searchField = "Item Name"
                            ElseIf linkingID = 281 Then
                                searchText = CCommon.ToString(dr("281_vcSKU"))
                                searchField = "SKU"
                            ElseIf linkingID = 203 Then
                                searchText = CCommon.ToString(dr("203_numBarCodeId"))
                                searchField = "UPC"
                            ElseIf linkingID = 899 Then
                                searchText = CCommon.ToString(dr("899_CustomerPartNo"))
                                searchField = "Customer Part#"
                            ElseIf linkingID = 211 Then
                                searchText = CCommon.ToString(dr("211_numItemCode"))
                                searchField = "Item ID"
                            ElseIf linkingID = 291 Then
                                searchText = CCommon.ToString(dr("291_vcPartNo"))
                                searchField = "Vendor Part#"
                            Else
                                listCSVRows.Add(New OrderImportLineItem() With {.CSVRow = lngCSVRow, .IsValid = False, .ErrorMessage = "CSV Row number: " & lngCSVRow & " - Field not selected to pick item(s) for order."})
                                Continue For
                            End If

                            objItem = New CItems
                            Dim dtBizItem As DataTable = objItem.SearchItemByFieldAndText(DomainID, objOrderImport.DivisionID, linkingID, searchText)

                            objOrderImportLineItem = New OrderImportLineItem
                            objOrderImportLineItem.CSVRow = lngCSVRow

                            If Not dtBizItem Is Nothing AndAlso dtBizItem.Rows.Count > 0 Then
                                objOrderImportLineItem.ItemCode = CCommon.ToLong(dtBizItem.Rows(0)("numItemCode"))
                                objOrderImportLineItem.ItemName = CCommon.ToString(dtBizItem.Rows(0)("vcItemName"))
                                objOrderImportLineItem.ModelID = CCommon.ToString(dtBizItem.Rows(0)("vcModelID"))
                                objOrderImportLineItem.KitParent = CCommon.ToBool(dtBizItem.Rows(0)("bitKitParent"))
                                objOrderImportLineItem.ItemType = CCommon.ToString(dtBizItem.Rows(0)("charItemType"))
                                objOrderImportLineItem.Units = CCommon.ToLong(dr("258_numUnitHour"))
                                objOrderImportLineItem.UnitPrice = CCommon.ToDouble(dr("259_monPrice"))
                                objOrderImportLineItem.ItemDescription = CCommon.ToString(dtBizItem.Rows(0)("txtItemDesc"))
                                objOrderImportLineItem.SKU = CCommon.ToString(dtBizItem.Rows(0)("vcSKU"))
                                objOrderImportLineItem.Cost = CCommon.ToDouble(dtBizItem.Rows(0)("monCost"))
                            Else
                                listCSVRows.Add(New OrderImportLineItem() With {.CSVRow = lngCSVRow, .IsValid = False, .ErrorMessage = "CSV Row number: " & lngCSVRow & " - Item does not exists"})
                                Continue For
                            End If

                            Dim isValidData As Boolean

                            For Each drHead As DataRow In dataTableHeading.Rows
                                isValidData = True

                                Dim fieldID As String = CCommon.ToString(drHead("FormFieldID"))
                                Dim dbColumnName As String = CCommon.ToString(drHead("dbFieldName"))
                                Select Case CCommon.ToLong(drHead("FormFieldID"))
                                    Case 217 'Ship To Street
                                        objOrderImport.ShipToStreet = CCommon.ToString(dr(fieldID & "_" & dbColumnName))
                                    Case 218 'Ship To City
                                        objOrderImport.ShipToCity = CCommon.ToString(dr(fieldID & "_" & dbColumnName))
                                    Case 219 'Ship To State
                                        objOrderImport.ShipToState = CCommon.ToLong(dr(fieldID & "_" & dbColumnName))
                                    Case 220 'Ship To Postal Code
                                        objOrderImport.ShipToPostalCode = CCommon.ToString(dr(fieldID & "_" & dbColumnName))
                                    Case 221 'Ship To Country
                                        objOrderImport.ShipToCountry = CCommon.ToLong(dr(fieldID & "_" & dbColumnName))
                                    Case 222 'Bill To Street
                                        objOrderImport.BillToStreet = CCommon.ToString(dr(fieldID & "_" & dbColumnName))
                                    Case 223 'Bill To City
                                        objOrderImport.BillToCity = CCommon.ToString(dr(fieldID & "_" & dbColumnName))
                                    Case 224 'Bill To State
                                        objOrderImport.BillToState = CCommon.ToLong(dr(fieldID & "_" & dbColumnName))
                                    Case 225 'Bill To Postal Code
                                        objOrderImport.BillToPostalCode = CCommon.ToString(dr(fieldID & "_" & dbColumnName))
                                    Case 226 'Bill To Country
                                        objOrderImport.BillToCountry = CCommon.ToLong(dr(fieldID & "_" & dbColumnName))
                                    Case 100 'Assigned To
                                        If CCommon.ToLong(dr(fieldID & "_" & dbColumnName)) > 0 Then
                                            objOrderImport.AssignedTo = CCommon.ToLong(dr(fieldID & "_" & dbColumnName))
                                        Else
                                            objOrderImport.AssignedTo = UserCntID
                                        End If
                                    Case 101 'Order Status
                                        objOrderImport.OrderStatus = CCommon.ToLong(dr(fieldID & "_" & dbColumnName))
                                    Case 233 'Warehouse Location
                                        objOrderImportLineItem.WarehouseLocationID = CCommon.ToLong(dr(fieldID & "_" & dbColumnName))
                                    Case 257 'Drop Ship
                                        objOrderImportLineItem.IsDropship = CCommon.ToBool(dr(fieldID & "_" & dbColumnName))
                                    Case 262 'Discount
                                        objOrderImportLineItem.Discount = CCommon.ToDouble(dr(fieldID & "_" & dbColumnName))
                                    Case 293 'Warehouse Name
                                        objOrderImportLineItem.WarehouseID = CCommon.ToLong(dr(fieldID & "_" & dbColumnName))
                                    Case 306 'UOM
                                        objOrderImportLineItem.UOM = CCommon.ToLong(dr(fieldID & "_" & dbColumnName))
                                    Case 771 'Ship Via
                                        objOrderImport.ShipVia = CCommon.ToLong(dr(fieldID & "_" & dbColumnName))
                                        'Case 40751 'Local Database ShipVia
                                        '    objOrderImport.ShipVia = CCommon.ToLong(dr(fieldID & "_" & dbColumnName))
                                    Case 902 'Shipping Service
                                        objOrderImport.ShippingService = CCommon.ToLong(dr(fieldID & "_" & dbColumnName))
                                        'Case 50885 'Local Database Shipping Service
                                        '    objOrderImport.ShippingService = CCommon.ToLong(dr(fieldID & "_" & dbColumnName))
                                    Case 914 'Is Work Order
                                        objOrderImportLineItem.IsWorkOrder = CCommon.ToBool(dr(fieldID & "_" & dbColumnName))
                                        'Case 50898 'Local Database Is Work Order
                                        '    objOrderImportLineItem.IsWorkOrder = CCommon.ToBool(dr(fieldID & "_" & dbColumnName))
                                    Case 915 'Discount Type
                                        If CCommon.ToString(dr(fieldID & "_" & dbColumnName)).ToLower() = "flat amount" Then
                                            objOrderImportLineItem.DiscountType = False
                                        Else
                                            objOrderImportLineItem.DiscountType = True
                                        End If
                                        'Case 50899 'Local Database Discount Type
                                        '    If CCommon.ToString(dr(fieldID & "_" & dbColumnName)).ToLower() = "flat amount" Then
                                        '        objOrderImportLineItem.DiscountType = False
                                        '    Else
                                        '        objOrderImportLineItem.DiscountType = True
                                        '    End If
                                    Case 742 'Cost
                                        objOrderImportLineItem.Cost = CCommon.ToDouble(dr(fieldID & "_" & dbColumnName))
                                    Case 535 'Customer PO#
                                        objOrderImport.CustomerPO = CCommon.ToString(dr(fieldID & "_" & dbColumnName))
                                    Case 870 'Inclusion Details (ID may be different in your local database)
                                        Try
                                            objOrderImportLineItem.KitChilds = CCommon.ToString(objItem.ValidateKitChildsOrderImport(DomainID, objOrderImportLineItem.ItemCode, CCommon.ToString(dr(fieldID & "_" & dbColumnName))))
                                            objOrderImportLineItem.IsHasKitAsChild = True
                                        Catch ex As Exception
                                            isValidData = False

                                            If ex.Message.Contains("INVALID_INCLISION_DETAIL") Then
                                                listCSVRows.Add(New OrderImportLineItem() With {.CSVRow = lngCSVRow, .IsValid = False, .ErrorMessage = "CSV Row number: " & lngCSVRow & " - Invalid inclusion detail"})
                                                Exit For
                                            ElseIf ex.Message.Contains("CHILD_KIT_VALUES_NOT_PROVIDED") Then
                                                listCSVRows.Add(New OrderImportLineItem() With {.CSVRow = lngCSVRow, .IsValid = False, .ErrorMessage = "CSV Row number: " & lngCSVRow & " - Required child kits are not included."})
                                                Exit For
                                            ElseIf ex.Message.Contains("KIT_ITEM_NOT_FOUND") Then
                                                listCSVRows.Add(New OrderImportLineItem() With {.CSVRow = lngCSVRow, .IsValid = False, .ErrorMessage = "CSV Row number: " & lngCSVRow & " - Kit item not found."})
                                                Exit For
                                            ElseIf ex.Message.Contains("MULTIPLE_KIT_ITEM_FOUND") Then
                                                listCSVRows.Add(New OrderImportLineItem() With {.CSVRow = lngCSVRow, .IsValid = False, .ErrorMessage = "CSV Row number: " & lngCSVRow & " - Multiple items found with kit item."})
                                                Exit For
                                            ElseIf ex.Message.Contains("KIT_CHILD_ITEM_NOT_FOUND") Then
                                                listCSVRows.Add(New OrderImportLineItem() With {.CSVRow = lngCSVRow, .IsValid = False, .ErrorMessage = "CSV Row number: " & lngCSVRow & " - Kit child item not found."})
                                                Exit For
                                            ElseIf ex.Message.Contains("MULTIPLE_KIT_CHILD_ITEM_FOUND") Then
                                                listCSVRows.Add(New OrderImportLineItem() With {.CSVRow = lngCSVRow, .IsValid = False, .ErrorMessage = "CSV Row number: " & lngCSVRow & " - Multiple items found wiht kit child item."})
                                                Exit For
                                            ElseIf ex.Message.Contains("INVALID_KIT_ITEM") Then
                                                listCSVRows.Add(New OrderImportLineItem() With {.CSVRow = lngCSVRow, .IsValid = False, .ErrorMessage = "CSV Row number: " & lngCSVRow & " - Invlaid kit item."})
                                                Exit For
                                            ElseIf ex.Message.Contains("INVALID_KIT_CHILD_ITEM") Then
                                                listCSVRows.Add(New OrderImportLineItem() With {.CSVRow = lngCSVRow, .IsValid = False, .ErrorMessage = "CSV Row number: " & lngCSVRow & " - Invlaid kit child item."})
                                                Exit For
                                            Else
                                                listCSVRows.Add(New OrderImportLineItem() With {.CSVRow = lngCSVRow, .IsValid = False, .ErrorMessage = "CSV Row number: " & lngCSVRow & " - Unknow error occured in validating inclusion details"})
                                                Exit For
                                            End If
                                        End Try

                                End Select
                            Next

                            If isValidData Then
                                If objOrderImportLineItem.ItemType = "P" And Not objOrderImportLineItem.IsDropship Then 'Inventory Item
                                    If objOrderImportLineItem.WarehouseID > 0 Then
                                        If listValidWaehouses.Exists(Function(x) x.Item1 = objOrderImportLineItem.ItemCode AndAlso x.Item2 = objOrderImportLineItem.WarehouseID AndAlso x.Item3 = objOrderImportLineItem.WarehouseLocationID) Then
                                            Dim tempWarehouse As Tuple(Of Long, Long, Long, Long) = listValidWaehouses.First(Function(x) x.Item1 = objOrderImportLineItem.ItemCode AndAlso x.Item2 = objOrderImportLineItem.WarehouseID AndAlso x.Item3 = objOrderImportLineItem.WarehouseLocationID)
                                            objOrderImportLineItem.WarehouseItemID = tempWarehouse.Item4
                                        Else
                                            objItem = New CItems
                                            objItem.DomainID = DomainID
                                            objItem.ItemCode = objOrderImportLineItem.ItemCode
                                            objItem.WarehouseID = objOrderImportLineItem.WarehouseID
                                            objItem.WarehouseLocationID = objOrderImportLineItem.WarehouseLocationID
                                            Dim dtWarehouse As DataTable = objItem.GetOneWarehouseByItemExternalInternalLocation()

                                            If Not dtWarehouse Is Nothing AndAlso dtWarehouse.Rows.Count > 0 Then
                                                objOrderImportLineItem.WarehouseItemID = CCommon.ToLong(dtWarehouse.Rows(0)("numWareHouseItemID"))
                                                listValidWaehouses.Add(New Tuple(Of Long, Long, Long, Long)(objOrderImportLineItem.ItemCode, objOrderImportLineItem.WarehouseID, objOrderImportLineItem.WarehouseLocationID, CCommon.ToLong(dtWarehouse.Rows(0)("numWareHouseItemID"))))
                                            Else
                                                listCSVRows.Add(New OrderImportLineItem() With {.CSVRow = lngCSVRow, .IsValid = False, .ErrorMessage = "CSV Row number: " & lngCSVRow & " - Warehouse does not exists."})
                                                Continue For
                                            End If
                                        End If
                                    Else
                                        listCSVRows.Add(New OrderImportLineItem() With {.CSVRow = lngCSVRow, .IsValid = False, .ErrorMessage = "CSV Row number: " & lngCSVRow & " - Warehouse does not exists."})
                                        Continue For
                                    End If
                                Else
                                    objOrderImportLineItem.WarehouseItemID = 0
                                End If

                                objOrderImport.LineItems.Add(objOrderImportLineItem)
                            End If
                        Catch ex As Exception
                            listCSVRows.Add(New OrderImportLineItem() With {.CSVRow = lngCSVRow, .IsValid = False, .ErrorMessage = "CSV Row number: " & lngCSVRow & " - Unknown error occured - " & ex.Message})
                        End Try
                    Next


                    'Start Creating Orders
                    If Not listOrders Is Nothing AndAlso listOrders.Count > 0 Then
                        Dim isNewOrganizationCreated As Boolean = True
                        Dim newOrganizationErrorMessage As String = ""

                        If IsSingleOrder AndAlso Not IsExistingOrganization Then
                            Try
                                Dim objLeads As New CLeads
                                With objLeads
                                    .CompanyName = listOrders(0).CompanyName
                                    .CustName = listOrders(0).CompanyName
                                    .CRMType = 1
                                    .DivisionName = "-"
                                    .LeadBoxFlg = 1
                                    .UserCntID = UserCntID
                                    .FirstName = listOrders(0).ContactFirstName
                                    .LastName = listOrders(0).ContactLastName
                                    .ContactPhone = ""
                                    .PhoneExt = ""
                                    .Email = listOrders(0).ContactEmail
                                    .DomainID = DomainID
                                    .ContactType = 0 'commented by chintan, contact type 70 is obsolete. default contact type is 0
                                    .PrimaryContact = True
                                    .CompanyType = 46
                                    .UpdateDefaultTax = False
                                End With
                                objLeads.CompanyID = objLeads.CreateRecordCompanyInfo
                                Dim tempCustomer As OrderImport = listOrders(0)

                                tempCustomer.DivisionID = objLeads.CreateRecordDivisionsInfo
                                objLeads.DivisionID = listOrders(0).DivisionID
                                tempCustomer.ContactID = objLeads.CreateRecordAddContactInfo()

                                Try
                                    'Added By Sachin Sadhu||Date:22ndMay12014
                                    'Purpose :To Added Organization data in work Flow queue based on created Rules
                                    '          Using Change tracking
                                    Dim objWfA As New BACRM.BusinessLogic.Workflow.Workflow()
                                    objWfA.DomainID = DomainID
                                    objWfA.UserCntID = UserCntID
                                    objWfA.RecordID = tempCustomer.DivisionID
                                    objWfA.SaveWFOrganizationQueue()
                                    'end of code

                                    'Added By Sachin Sadhu||Date:24thJuly2014
                                    'Purpose :To Added Contact data in work Flow queue based on created Rules
                                    '         Using Change tracking
                                    Dim objWF As New BACRM.BusinessLogic.Workflow.Workflow()
                                    objWfA.DomainID = DomainID
                                    objWfA.UserCntID = UserCntID
                                    objWF.RecordID = tempCustomer.ContactID
                                    objWF.SaveWFContactQueue()
                                    ' ss//end of code
                                Catch ex As Exception
                                    'DO NOT THROW ERROR
                                End Try
                            Catch ex As Exception
                                isNewOrganizationCreated = False
                                newOrganizationErrorMessage = ex.Message
                            End Try
                        End If

                        If isNewOrganizationCreated Then
                            For Each objOrderImport In listOrders
                                Try
                                    If Not objOrderImport.LineItems Is Nothing AndAlso objOrderImport.LineItems.Count > 0 Then
                                        Dim objOpportunity As New BACRM.BusinessLogic.Opportunities.MOpportunity
                                        objOpportunity.OpportunityId = 0
                                        objOpportunity.ContactID = objOrderImport.ContactID
                                        objOpportunity.DivisionID = objOrderImport.DivisionID

                                        objOpportunity.OppType = oppType
                                        objOpportunity.DealStatus = 1
                                        objOpportunity.ReleaseDate = DateAdd(DateInterval.Minute, -420, Date.UtcNow)
                                        objOpportunity.UserCntID = UserCntID
                                        objOpportunity.EstimatedCloseDate = DateAdd(DateInterval.Minute, -420, Date.UtcNow)
                                        objOpportunity.PublicFlag = 0
                                        objOpportunity.DomainID = DomainID
                                        objOpportunity.OpportunityName = ""

                                        If CCommon.ToLong(objOrderImport.BillToCountry) > 0 AndAlso CCommon.ToLong(objOrderImport.BillToState) > 0 AndAlso Not String.IsNullOrEmpty(objOrderImport.BillToStreet) AndAlso Not String.IsNullOrEmpty(objOrderImport.BillToPostalCode) Then
                                            objOpportunity.BillStreet = objOrderImport.BillToStreet
                                            objOpportunity.BillCity = objOrderImport.BillToCity
                                            objOpportunity.BillState = objOrderImport.BillToState
                                            objOpportunity.BillCountry = objOrderImport.BillToCountry
                                            objOpportunity.BillPostal = objOrderImport.BillToPostalCode
                                            objOpportunity.BillToType = 2
                                        End If

                                        If CCommon.ToLong(objOrderImport.ShipToCountry) > 0 AndAlso CCommon.ToLong(objOrderImport.ShipToState) > 0 AndAlso Not String.IsNullOrEmpty(objOrderImport.ShipToStreet) AndAlso Not String.IsNullOrEmpty(objOrderImport.ShipToPostalCode) Then
                                            objOpportunity.ShipStreet = objOrderImport.ShipToStreet
                                            objOpportunity.ShipCity = objOrderImport.ShipToCity
                                            objOpportunity.ShipState = objOrderImport.ShipToState
                                            objOpportunity.ShipCountry = objOrderImport.ShipToCountry
                                            objOpportunity.ShipPostal = objOrderImport.ShipToPostalCode
                                            objOpportunity.ShipToType = 2
                                        End If

                                        objOpportunity.CouponCode = ""
                                        objOpportunity.Discount = 0
                                        objOpportunity.boolDiscType = False

                                        objOpportunity.AssignedTo = objOrderImport.AssignedTo
                                        objOpportunity.OrderStatus = objOrderImport.OrderStatus
                                        objOpportunity.OppRefOrderNo = objOrderImport.CustomerPO

                                        Dim dsTemp As DataSet = CreateSet()
                                        Dim objCommon As New CCommon
                                        Dim i As Integer = 0

                                        For Each objLineItem As OrderImportLineItem In objOrderImport.LineItems
                                            i = i + 1

                                            dsTemp = AddItemtoDataTable(objCommon, dsTemp, True, objLineItem.ItemType, "", objLineItem.IsDropship, objLineItem.KitParent, objLineItem.ItemCode,
                                                                objLineItem.Units,
                                                                objLineItem.UnitPrice,
                                                                objLineItem.ItemDescription,
                                                                objLineItem.WarehouseItemID, objLineItem.ItemName, "", 0,
                                                                objLineItem.ModelID, objLineItem.UOM, "",
                                                                1, "Sales", objLineItem.DiscountType, objLineItem.Discount, "",
                                                                "", "",
                                                                objLineItem.IsWorkOrder,
                                                                "",
                                                                "",
                                                                0,
                                                                0,
                                                                0,
                                                                0,
                                                                vcBaseUOMName:="",
                                                                numPrimaryVendorID:=CCommon.ToLong(0),
                                                                strSKU:=objLineItem.SKU, primaryVendorCost:=0,
                                                                salePrice:=objLineItem.UnitPrice,
                                                                numMaxWOQty:=0, vcAttributes:="", vcAttributeIDs:="", numContainer:=0, numContainerQty:=0,
                                                                numItemClassification:=0,
                                                                numPromotionID:=0,
                                                                IsPromotionTriggered:=False,
                                                                vcPromotionDetail:="",
                                                                numSortOrder:=i, numCost:=objLineItem.Cost,
                                                                itemReleaseDate:=Date.Today.ToString("MM/dd/yyyy"), ShipFromLocation:="",
                                                                numShipToAddressID:=0,
                                                                ShipToFullAddress:="",
                                                                InclusionDetail:="",
                                                                OnHandAllocation:="",
                                                                KitChildItems:=objLineItem.KitChilds,
                                                                isHasKitAsChild:=objLineItem.IsHasKitAsChild)  'OnHandAllocation)
                                        Next
                                        'TODO: Item XML
                                        objOpportunity.strItems = "<?xml version=""1.0"" encoding=""iso-8859-1"" ?>" & dsTemp.GetXml

                                        Dim objAdmin As New CAdmin
                                        Dim dtBillingTerms As DataTable
                                        objAdmin.DivisionID = objOrderImport.DivisionID
                                        dtBillingTerms = objAdmin.GetBillingTerms()

                                        objOpportunity.AssignedTo = CCommon.ToLong(dtBillingTerms.Rows(0).Item("numAssignedTo"))
                                        objOpportunity.boolBillingTerms = IIf(dtBillingTerms.Rows(0).Item("tintBillingTerms") = 1, True, False)
                                        objOpportunity.BillingDays = dtBillingTerms.Rows(0).Item("numBillingDays")
                                        objOpportunity.boolInterestType = IIf(dtBillingTerms.Rows(0).Item("tintInterestType") = 1, True, False)
                                        objOpportunity.Interest = dtBillingTerms.Rows(0).Item("fltInterest")

                                        If objOrderImport.ShipVia > 0 Then
                                            objOpportunity.intUsedShippingCompany = objOrderImport.ShipVia
                                        Else
                                            objOpportunity.intUsedShippingCompany = CCommon.ToLong(dtBillingTerms.Rows(0).Item("intShippingCompany"))
                                        End If

                                        If objOrderImport.ShippingService > 0 Then
                                            objOpportunity.ShippingService = objOrderImport.ShippingService
                                        Else
                                            objOpportunity.ShippingService = CCommon.ToLong(dtBillingTerms.Rows(0).Item("numDefaultShippingServiceID"))
                                        End If

                                        objOpportunity.CampaignID = objOpportunity.GetCampaignForOrganization()


                                        If objOrderImport.LineItems.Exists(Function(x) x.WarehouseID > 0) Then
                                            objOpportunity.ShipFromLocation = objOrderImport.LineItems.First(Function(x) x.WarehouseID > 0).WarehouseID
                                            objOpportunity.WillCallWarehouseID = objOrderImport.LineItems.First(Function(x) x.WarehouseID > 0).WarehouseID
                                        End If

                                        objOpportunity.ClientTimeZoneOffset = 420


                                        Dim arrOutPut() As String = objOpportunity.Save
                                        Dim lngOppId As Long = CCommon.ToLong(arrOutPut(0))
                                        Dim oppName As String = CCommon.ToString(arrOutPut(1))

                                        strAddedOrderIds = strAddedOrderIds & lngOppId & ","

                                        For Each objLineItem As OrderImportLineItem In objOrderImport.LineItems
                                            listCSVRows.Add(New OrderImportLineItem() With {.CSVRow = objLineItem.CSVRow, .IsValid = True, .ErrorMessage = "CSV Row number: " & objLineItem.CSVRow & " - Item added in sales order " & oppName})
                                        Next

                                        Try
                                            objOpportunity.OpportunityId = lngOppId
                                            objOpportunity.UpdateOpportunityLinkingDtls()

                                            ''Added By Sachin Sadhu||Date:29thApril12014
                                            ''Purpose :To Added Opportunity data in work Flow queue based on created Rules
                                            ''          Using Change tracking
                                            Dim objWfA As New BACRM.BusinessLogic.Workflow.Workflow
                                            objWfA.DomainID = DomainID
                                            objWfA.UserCntID = UserCntID
                                            objWfA.RecordID = lngOppId
                                            objWfA.SaveWFOrderQueue()
                                            'end of code
                                        Catch ex As Exception
                                            'DO NOT THROW ERROR
                                        End Try
                                    End If
                                Catch ex As Exception
                                    For Each objLineItem As OrderImportLineItem In objOrderImport.LineItems
                                        listCSVRows.Add(New OrderImportLineItem() With {.CSVRow = objLineItem.CSVRow, .IsValid = False, .ErrorMessage = "CSV Row number: " & objLineItem.CSVRow & " - Unknown error occurred: " & ex.Message})
                                    Next
                                End Try
                            Next
                        Else
                            For Each objLineItem As OrderImportLineItem In listOrders(0).LineItems
                                listCSVRows.Add(New OrderImportLineItem() With {.CSVRow = objLineItem.CSVRow, .IsValid = False, .ErrorMessage = "CSV Row number: " & objLineItem.CSVRow & " - Not able to create new organization: " & newOrganizationErrorMessage})
                            Next
                        End If
                    End If

                    Try
                        intInserts = 0
                        intErrors = 0
                        For Each objLineItem As OrderImportLineItem In listCSVRows.OrderBy(Function(x) x.CSVRow)
                            objImport = New ImportWizard(intImportFileID, DomainID)
                            With objImport
                                .ImportFileID = intImportFileID
                                .SaveImportFileReport(objLineItem.CSVRow, objLineItem.ErrorMessage, 0, "", "")
                            End With

                            If objLineItem.IsValid Then
                                intInserts = intInserts + 1
                            Else
                                WriteMessage(objLineItem.ErrorMessage)
                                intErrors = intErrors + 1
                            End If
                        Next
                    Catch ex As Exception
                        'DO NOT THROW ERROR
                    End Try
                Else
                    WriteMessage(strImportType & ": Not able to process csv. " & errorMessage)
                End If
            Catch ex As Exception
                WriteMessage(strImportType & ": Not able to process csv. Unknown error occurred: " & ex.Message)
            End Try


            strAddedOrderIds = strAddedOrderIds.Trim(",")

            UpdateImportFileStatus(intInserts, 0, intErrors, strAddedOrderIds, "", strImportType)

            WriteMessage("End Time :" & DateTime.Now)
            WriteMessage("Import Duration (In Seconds) :" & DateDiff(DateInterval.Second, dtStartTime, DateTime.Now))
            WriteMessage(strImportType & " records created: " & intInserts & If(strAddedOrderIds.Length > 0, " : " & strAddedOrderIds, ""))
            WriteMessage(strImportType & " records have Errors: " & intErrors)
        Catch ex As Exception
            WriteMessage(strImportType & ": Not able to process csv. Unknown error occurred: " & ex.Message)

            UpdateImportFileStatus(intInserts, 0, intErrors, strAddedOrderIds, "", strImportType)

            WriteMessage("End Time :" & DateTime.Now)
            WriteMessage("Import Duration (In Seconds) :" & DateDiff(DateInterval.Second, dtStartTime, DateTime.Now))
            WriteMessage(strImportType & " records created: " & intInserts & If(strAddedOrderIds.Length > 0, " : " & strAddedOrderIds, ""))
            WriteMessage(strImportType & " records have Errors: " & intErrors)
        End Try
    End Sub

    Public Sub ImportAccountingEntries(ByVal dtDataHold As DataTable)
        Dim intInserts As Integer = 0
        Dim intErrors As Integer = 0
        Dim strImportType As String = "Accounting Entries"
        Dim strAddedJournalIds As String = ""
        Dim listCSVRows As New List(Of Tuple(Of Integer, Boolean, String))
        Dim listValidCustomers As New List(Of Tuple(Of String, Long))
        Dim listInvalidCustomer As New List(Of String)
        lngCSVRow = 0

        Try
            Dim isValidConfiguration As Boolean = True
            Dim errorMessage As String = ""

            If Not dtDataHold.Columns.Contains("931_datEntry_Date") Then
                isValidConfiguration = False
                errorMessage = "Date is required to import accounting entrie(s)."
            End If
            If Not dtDataHold.Columns.Contains("932_numChartAcntId") Then
                isValidConfiguration = False
                errorMessage = "Chart of Accounts to Credit is required to import accounting entrie(s)."
            End If
            If Not dtDataHold.Columns.Contains("933_numChartAcntId") Then
                isValidConfiguration = False
                errorMessage = "Chart of Accounts to Debit is required to import accounting entrie(s)."
            End If
            If Not dtDataHold.Columns.Contains("934_numAmount") Then
                isValidConfiguration = False
                errorMessage = "Amount is required to import accounting entrie(s)."
            End If

            If isValidConfiguration Then
                Dim entryDate As Date
                Dim dtAccount As DataTable
                Dim objJEHeader As New JournalEntryHeader
                Dim objJEList As New JournalEntryCollection
                Dim objJECredit As New JournalEntryNew()
                Dim objJEDebit As New JournalEntryNew()

                For Each dr As DataRow In dtDataHold.Rows
                    lngCSVRow = lngCSVRow + 1

                    Try
                        objJEHeader = New JournalEntryHeader
                        With objJEHeader
                            .JournalId = 0
                            .RecurringId = 0
                            .Description = ""
                            .CheckId = 0
                            .CashCreditCardId = 0
                            .ChartAcntId = 0
                            .OppId = 0
                            .OppBizDocsId = 0
                            .DepositId = 0
                            .IsOpeningBalance = 0
                            .LastRecurringDate = Date.Now
                            .NoTransactions = 0
                            .CategoryHDRID = 0
                            .ReturnID = 0
                            .CheckHeaderID = 0
                            .BillID = 0
                            .BillPaymentID = 0
                            .UserCntID = UserCntID
                            .DomainID = DomainID
                            .JournalReferenceNo = 0
                        End With

                        objJEList = New JournalEntryCollection

                        objJECredit = New JournalEntryNew()
                        objJECredit.TransactionId = 0
                        objJECredit.DebitAmt = 0
                        objJECredit.MainDeposit = 0
                        objJECredit.MainCheck = 0
                        objJECredit.MainCashCredit = 0
                        objJECredit.OppitemtCode = 0
                        objJECredit.BizDocItems = ""
                        objJECredit.Reference = ""
                        objJECredit.PaymentMethod = 0
                        objJECredit.Reconcile = False
                        objJECredit.CurrencyID = 0
                        objJECredit.FltExchangeRate = 0
                        objJECredit.TaxItemID = 0
                        objJECredit.BizDocsPaymentDetailsId = 0
                        objJECredit.ContactID = 0
                        objJECredit.ItemID = 0
                        objJECredit.ProjectID = 0
                        objJECredit.ClassID = 0
                        objJECredit.CommissionID = 0
                        objJECredit.ReconcileID = 0
                        objJECredit.Cleared = 0
                        objJECredit.ReferenceType = enmReferenceType.DirectJournal   'Direct Journal
                        objJECredit.ReferenceID = 0 'Journal ID

                        objJEDebit = New JournalEntryNew()
                        objJEDebit.TransactionId = 0
                        objJEDebit.CreditAmt = 0
                        objJEDebit.MainDeposit = 0
                        objJEDebit.MainCheck = 0
                        objJEDebit.MainCashCredit = 0
                        objJEDebit.OppitemtCode = 0
                        objJEDebit.BizDocItems = ""
                        objJEDebit.Reference = ""
                        objJEDebit.PaymentMethod = 0
                        objJEDebit.Reconcile = False
                        objJEDebit.CurrencyID = 0
                        objJEDebit.FltExchangeRate = 0
                        objJEDebit.TaxItemID = 0
                        objJEDebit.BizDocsPaymentDetailsId = 0
                        objJEDebit.ContactID = 0
                        objJEDebit.ItemID = 0
                        objJEDebit.ProjectID = 0
                        objJEDebit.ClassID = 0
                        objJEDebit.CommissionID = 0
                        objJEDebit.ReconcileID = 0
                        objJEDebit.Cleared = 0
                        objJEDebit.ReferenceType = enmReferenceType.DirectJournal   'Direct Journal
                        objJEDebit.ReferenceID = 0

                        Dim isValidData As Boolean = True

                        For Each drHead As DataRow In dataTableHeading.Rows
                            Dim fieldID As String = CCommon.ToString(drHead("FormFieldID"))
                            Dim dbColumnName As String = CCommon.ToString(drHead("dbFieldName"))
                            Select Case CCommon.ToLong(drHead("FormFieldID"))
                                Case 3 'Vendor
                                    If Not String.IsNullOrEmpty(CCommon.ToString(dr(fieldID & "_" & dbColumnName)).Trim()) Then
                                        If listInvalidCustomer.Exists(Function(x) x.ToLower() = CCommon.ToString(dr(fieldID & "_" & dbColumnName)).ToLower()) Then
                                            listCSVRows.Add(New Tuple(Of Integer, Boolean, String)(lngCSVRow, False, "CSV Row number: " & lngCSVRow & " - Customer does not exists"))
                                            isValidData = False
                                            Continue For
                                        ElseIf listValidCustomers.Exists(Function(x) x.Item1.ToLower() = CCommon.ToString(dr(fieldID & "_" & dbColumnName)).ToLower()) Then
                                            objJECredit.CustomerId = listValidCustomers.First(Function(x) x.Item1.ToLower() = CCommon.ToString(dr(fieldID & "_" & dbColumnName)).ToLower()).Item2
                                            objJEDebit.CustomerId = listValidCustomers.First(Function(x) x.Item1.ToLower() = CCommon.ToString(dr(fieldID & "_" & dbColumnName)).ToLower()).Item2
                                        Else
                                            Dim objAccount As New CAccounts
                                            objAccount.DomainID = DomainID
                                            dtAccount = objAccount.OrderImportGetCustomer(CCommon.ToString(dr("3_vcCompanyName")), 3, "")

                                            If Not dtAccount Is Nothing AndAlso dtAccount.Rows.Count > 0 Then
                                                objJECredit.CustomerId = CCommon.ToLong(dtAccount.Rows(0)("numDivisionID"))
                                                objJEDebit.CustomerId = CCommon.ToLong(dtAccount.Rows(0)("numDivisionID"))
                                                listValidCustomers.Add(New Tuple(Of String, Long)(CCommon.ToString(dr("3_vcCompanyName")), CCommon.ToLong(dtAccount.Rows(0)("numDivisionID"))))
                                            Else
                                                listInvalidCustomer.Add(CCommon.ToString(dr("3_vcCompanyName")))
                                                listCSVRows.Add(New Tuple(Of Integer, Boolean, String)(lngCSVRow, False, "CSV Row number: " & lngCSVRow & " - Customer does not exists"))
                                                isValidData = False
                                                Continue For
                                            End If
                                        End If
                                    End If
                                Case 931 'Date
                                    If String.IsNullOrEmpty(dr(fieldID & "_" & dbColumnName)) Then
                                        listCSVRows.Add(New Tuple(Of Integer, Boolean, String)(lngCSVRow, False, "CSV Row number: " & lngCSVRow & " - Invalid Date"))
                                        isValidData = False
                                        Continue For
                                    ElseIf Not DateTime.TryParseExact(CCommon.ToString(dr(fieldID & "_" & dbColumnName)), "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, entryDate) Then
                                        listCSVRows.Add(New Tuple(Of Integer, Boolean, String)(lngCSVRow, False, "CSV Row number: " & lngCSVRow & " - Invalid Date"))
                                        isValidData = False
                                        Continue For
                                    Else
                                        objJEHeader.EntryDate = CDate(entryDate.ToString("MM/dd/yyyy") & " 12:00:00")
                                    End If
                                Case 932 'Chart of Accounts to Credit
                                    If CCommon.ToLong(dr(fieldID & "_" & dbColumnName)) > 0 Then
                                        objJECredit.ChartAcntId = CCommon.ToLong(dr(fieldID & "_" & dbColumnName))
                                    Else
                                        listCSVRows.Add(New Tuple(Of Integer, Boolean, String)(lngCSVRow, False, "CSV Row number: " & lngCSVRow & " - Invalid Chart of Accounts to Credit"))
                                        isValidData = False
                                        Continue For
                                    End If
                                Case 933 'Chart of Accounts to Debit
                                    If CCommon.ToLong(dr(fieldID & "_" & dbColumnName)) > 0 Then
                                        objJEDebit.ChartAcntId = CCommon.ToLong(dr(fieldID & "_" & dbColumnName))
                                    Else
                                        listCSVRows.Add(New Tuple(Of Integer, Boolean, String)(lngCSVRow, False, "CSV Row number: " & lngCSVRow & " - Invalid Chart of Accounts to Debit"))
                                        isValidData = False
                                        Continue For
                                    End If
                                Case 934 'Amount
                                    objJEHeader.Amount = CCommon.ToDecimal(CCommon.ToString(dr(fieldID & "_" & dbColumnName)).Replace("$", "").Replace(",", ""))
                                    objJECredit.CreditAmt = CCommon.ToDecimal(CCommon.ToString(dr(fieldID & "_" & dbColumnName)).Replace("$", "").Replace(",", ""))
                                    objJEDebit.DebitAmt = CCommon.ToDecimal(CCommon.ToString(dr(fieldID & "_" & dbColumnName)).Replace("$", "").Replace(",", ""))
                                Case 935 'Memo
                                    objJECredit.Description = CCommon.ToString(dr(fieldID & "_" & dbColumnName))
                                    objJEDebit.Description = CCommon.ToString(dr(fieldID & "_" & dbColumnName))
                                Case 409 'Class
                                    objJECredit.ClassID = CCommon.ToLong(dr(fieldID & "_" & dbColumnName))
                                    objJEDebit.ClassID = CCommon.ToLong(dr(fieldID & "_" & dbColumnName))
                            End Select
                        Next

                        If isValidData Then
                            Using objTransactionScope As New System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted, .Timeout = System.Transactions.TransactionManager.MaximumTimeout})
                                Dim lntJournalId As Integer = objJEHeader.Save()

                                If lntJournalId > 0 Then
                                    objJEList.Add(objJECredit)
                                    objJEList.Add(objJEDebit)

                                    objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lntJournalId, DomainID)
                                End If

                                strAddedJournalIds = strAddedJournalIds & lntJournalId & ","
                                listCSVRows.Add(New Tuple(Of Integer, Boolean, String)(lngCSVRow, True, "CSV Row number: " & lngCSVRow & " - Record imported successfully."))

                                objTransactionScope.Complete()
                            End Using
                        End If
                    Catch ex As Exception
                        listCSVRows.Add(New Tuple(Of Integer, Boolean, String)(lngCSVRow, False, "CSV Row number: " & lngCSVRow & " - Unknown error occured - " & ex.Message))
                    End Try
                Next

                Try
                    intInserts = 0
                    intErrors = 0
                    For Each objLineItem As Tuple(Of Integer, Boolean, String) In listCSVRows.OrderBy(Function(x) x.Item1)
                        objImport = New ImportWizard(intImportFileID, DomainID)
                        With objImport
                            .ImportFileID = intImportFileID
                            .SaveImportFileReport(objLineItem.Item1, objLineItem.Item3, 0, "", "")
                        End With

                        If objLineItem.Item2 Then
                            intInserts = intInserts + 1
                        Else
                            WriteMessage(objLineItem.Item3)
                            intErrors = intErrors + 1
                        End If
                    Next
                Catch ex As Exception
                    'DO NOT THROW ERROR
                End Try
            Else
                WriteMessage(strImportType & ": Not able to process csv. " & errorMessage)
            End If

            strAddedJournalIds = strAddedJournalIds.Trim(",")

            UpdateImportFileStatus(intInserts, 0, intErrors, strAddedJournalIds, "", strImportType)

            WriteMessage("End Time :" & DateTime.Now)
            WriteMessage("Import Duration (In Seconds) :" & DateDiff(DateInterval.Second, dtStartTime, DateTime.Now))
            WriteMessage(strImportType & " records created: " & intInserts & If(strAddedJournalIds.Length > 0, " : " & strAddedJournalIds, ""))
            WriteMessage(strImportType & " records have Errors: " & intErrors)
        Catch ex As Exception
            WriteMessage(strImportType & ": Not able to process csv. Unknown error occurred: " & ex.Message)

            UpdateImportFileStatus(intInserts, 0, intErrors, strAddedJournalIds, "", strImportType)

            WriteMessage("End Time :" & DateTime.Now)
            WriteMessage("Import Duration (In Seconds) :" & DateDiff(DateInterval.Second, dtStartTime, DateTime.Now))
            WriteMessage(strImportType & " records created: " & intInserts & If(strAddedJournalIds.Length > 0, " : " & strAddedJournalIds, ""))
            WriteMessage(strImportType & " records have Errors: " & intErrors)
        End Try
    End Sub

    Private Function CreateSet() As DataSet
        Try
            Dim dsTemp As New DataSet
            Dim dtItem As New DataTable
            Dim dtSerItem As New DataTable
            Dim dtChildItems As New DataTable
            dtItem.Columns.Add("numoppitemtCode", System.Type.GetType("System.Int32"))
            dtItem.Columns.Add("numItemCode")
            dtItem.Columns.Add("numItemClassification")
            dtItem.Columns.Add("numUnitHour", GetType(Decimal))
            dtItem.Columns.Add("monPrice", GetType(Decimal))
            dtItem.Columns.Add("monSalePrice", GetType(Decimal))
            dtItem.Columns.Add("numUOM")
            dtItem.Columns.Add("vcUOMName")
            dtItem.Columns.Add("UOMConversionFactor")

            dtItem.Columns.Add("monTotAmount", GetType(Decimal))
            dtItem.Columns.Add("numSourceID")
            dtItem.Columns.Add("vcItemDesc")
            dtItem.Columns.Add("vcModelID")
            dtItem.Columns.Add("numWarehouseID")
            dtItem.Columns.Add("vcItemName")
            dtItem.Columns.Add("Warehouse")
            dtItem.Columns.Add("numWarehouseItmsID")
            dtItem.Columns.Add("ItemType")
            dtItem.Columns.Add("Attributes")
            dtItem.Columns.Add("AttributeIDs")
            dtItem.Columns.Add("Op_Flag")
            dtItem.Columns.Add("bitWorkOrder")
            dtItem.Columns.Add("numMaxWorkOrderQty")
            dtItem.Columns.Add("vcInstruction")
            dtItem.Columns.Add("bintCompliationDate")
            dtItem.Columns.Add("numWOAssignedTo")

            dtItem.Columns.Add("DropShip", GetType(Boolean))
            dtItem.Columns.Add("bitDiscountType", GetType(Boolean))
            dtItem.Columns.Add("fltDiscount", GetType(Decimal))
            dtItem.Columns.Add("TotalDiscountAmount", GetType(Decimal))
            dtItem.Columns.Add("monTotAmtBefDiscount", GetType(Decimal))

            dtItem.Columns.Add("bitTaxable0")
            dtItem.Columns.Add("Tax0", GetType(Decimal))
            dtItem.Columns.Add("TotalTax", GetType(Decimal))

            dtItem.Columns.Add("numVendorWareHouse")
            dtItem.Columns.Add("numShipmentMethod")
            dtItem.Columns.Add("numSOVendorId")

            dtItem.Columns.Add("numProjectID")
            dtItem.Columns.Add("numProjectStageID")
            dtItem.Columns.Add("charItemType")
            dtItem.Columns.Add("numToWarehouseItemID") 'added by chintan for stock transfer
            dtItem.Columns.Add("bitIsAuthBizDoc", GetType(Boolean))
            dtItem.Columns.Add("numUnitHourReceived")
            dtItem.Columns.Add("numQtyShipped")
            dtItem.Columns.Add("vcBaseUOMName")
            dtItem.Columns.Add("numVendorID", System.Type.GetType("System.Int32"))
            dtItem.Columns.Add("fltItemWeight")
            dtItem.Columns.Add("IsFreeShipping")
            dtItem.Columns.Add("fltHeight")
            dtItem.Columns.Add("fltWidth")
            dtItem.Columns.Add("fltLength")
            dtItem.Columns.Add("vcSKU")
            dtItem.Columns.Add("bitCouponApplied")
            dtItem.Columns.Add("bitItemPriceApprovalRequired")
            dtItem.Columns.Add("bitHasKitAsChild", GetType(Boolean))
            dtItem.Columns.Add("KitChildItems", GetType(String))
            dtItem.Columns.Add("numContainer")
            dtItem.Columns.Add("numContainerQty")
            dtItem.Columns.Add("numPromotionID")
            dtItem.Columns.Add("bitPromotionTriggered")
            dtItem.Columns.Add("vcPromotionDetail")
            dtItem.Columns.Add("numCost", GetType(Decimal))
            dtItem.Columns.Add("numSortOrder")
            dtItem.Columns.Add("vcVendorNotes")
            dtItem.Columns.Add("CustomerPartNo")
            dtItem.Columns.Add("ItemReleaseDate")
            dtItem.Columns.Add("Location")
            dtItem.Columns.Add("numShipToAddressID")
            dtItem.Columns.Add("ShipToFullAddress")
            dtItem.Columns.Add("InclusionDetail")
            dtItem.Columns.Add("OnHandAllocation")

            dtSerItem.Columns.Add("numWarehouseItmsDTLID")
            dtSerItem.Columns.Add("numWItmsID")
            dtSerItem.Columns.Add("numoppitemtCode", System.Type.GetType("System.Int32"))
            dtSerItem.Columns.Add("vcSerialNo")
            dtSerItem.Columns.Add("Comments")
            dtSerItem.Columns.Add("Attributes")

            dtChildItems.Columns.Add("numOppChildItemID", System.Type.GetType("System.Int32"))
            dtChildItems.Columns.Add("numoppitemtCode", System.Type.GetType("System.Int32"))
            dtChildItems.Columns.Add("numItemCode")
            dtChildItems.Columns.Add("numQtyItemsReq")
            dtChildItems.Columns.Add("vcItemName")
            dtChildItems.Columns.Add("monListPrice")
            dtChildItems.Columns.Add("UnitPrice")
            dtChildItems.Columns.Add("charItemType")
            dtChildItems.Columns.Add("txtItemDesc")
            dtChildItems.Columns.Add("numWarehouseItmsID")
            dtChildItems.Columns.Add("Op_Flag")
            dtChildItems.Columns.Add("ItemType")
            dtChildItems.Columns.Add("Attr")
            dtChildItems.Columns.Add("vcWareHouse")

            dtItem.TableName = "Item"
            dtSerItem.TableName = "SerialNo"
            dtChildItems.TableName = "ChildItems"

            dsTemp.Tables.Add(dtItem)
            dsTemp.Tables.Add(dtSerItem)
            dsTemp.Tables.Add(dtChildItems)
            dtItem.PrimaryKey = New DataColumn() {dsTemp.Tables(0).Columns("numoppitemtCode")}
            dtChildItems.PrimaryKey = New DataColumn() {dsTemp.Tables(2).Columns("numOppChildItemID")}

            Return dsTemp
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Shared Function AddItemtoDataTable(ByRef objCommon As BACRM.BusinessLogic.Common.CCommon,
                            ByRef dsTemp As DataSet,
                            ByVal AddMode As Boolean,
                            ByVal ItemTypeValue As Char,
                            ByVal ItemTypeText As String,
                            ByVal DropShip As Boolean,
                            ByVal IsKit As String,
                            ByVal ItemCode As Long,
                            ByVal Units As Double,
                            ByVal Price As Decimal,
                            ByVal Desc As String,
                            ByVal WareHouseItemID As Long,
                            ByVal ItemName As String,
                            ByVal Warehouse As String,
                            ByVal OppItemID As Long,
                            ByVal ModelId As String,
                            ByVal UOM As Long,
                            ByVal vcUOMName As String,
                            ByVal UOMConversionFactor As Decimal,
                            ByVal PageType As String,
                            ByVal IsDiscountInPer As Boolean,
                            ByVal Discount As Decimal,
                            ByVal Taxable As String,
                            ByVal Tax As String,
                            ByVal CustomerPartNo As String,
                            Optional ByVal bitWorkOrder As Boolean = False,
                            Optional ByVal vcInstruction As String = "",
                            Optional ByVal bintCompliationDate As String = "",
                            Optional ByVal numWOAssignedTo As Long = 0,
                            Optional ByVal numVendorWareHouse As Long = 0,
                            Optional ByVal numShipmentMethod As Long = 0,
                            Optional ByVal numSOVendorId As Long = 0,
                            Optional ByVal numProjectID As Long = 0,
                            Optional ByVal numProjectStageID As Long = 0,
                            Optional ByVal bDirectAdd As Boolean = False,
                            Optional ByVal ToWarehouseItemID As Long = 0,
                            Optional ByVal IsPOS As Boolean = False,
                            Optional ByVal vcBaseUOMName As String = "",
                            Optional ByVal numPrimaryVendorID As Long = 0,
                            Optional ByVal fltItemWeight As Double = 0,
                            Optional ByVal IsFreeShipping As Boolean = False,
                            Optional ByVal fltHeight As Double = 0,
                            Optional ByVal fltWidth As Double = 0,
                            Optional ByVal fltLength As Double = 0,
                            Optional ByVal strSKU As String = "",
                            Optional ByVal dtItemColumnTableInfo As DataTable = Nothing,
                            Optional ByVal primaryVendorCost As Decimal = 0,
                            Optional ByVal salePrice As Decimal = 0,
                            Optional ByVal numMaxWOQty As Integer = 0,
                            Optional ByVal vcAttributes As String = "",
                            Optional ByVal vcAttributeIDs As String = "",
                            Optional ByVal numContainer As Integer = -1,
                            Optional ByVal numContainerQty As Integer = -1,
                            Optional ByVal numItemClassification As Long = 0,
                            Optional ByVal numPromotionID As Long = 0,
                            Optional ByVal IsPromotionTriggered As Boolean = False,
                            Optional ByVal vcPromotionDetail As String = "",
                            Optional ByVal numSortOrder As Long = 0,
                            Optional ByVal numCost As Double = 0,
                            Optional ByVal vcVendorNotes As String = "",
                            Optional ByVal itemReleaseDate As String = "",
                            Optional ByVal ShipFromLocation As String = "",
                            Optional ByVal numShipToAddressID As Long = 0,
                            Optional ByVal ShipToFullAddress As String = "",
                            Optional ByVal InclusionDetail As String = "",
                            Optional ByVal OnHandAllocation As String = "",
                            Optional ByVal KitChildItems As String = "",
                            Optional ByVal isHasKitAsChild As Boolean = False
                             ) As DataSet
        Try

            Dim dtItem As DataTable
            Dim dtSerItem As DataTable
            Dim dtChildItems As DataTable

            dtItem = dsTemp.Tables(0)
            dtSerItem = dsTemp.Tables(1)
            dtChildItems = dsTemp.Tables(2)

            If Not dtItem.Columns.Contains("fltItemWeight") Then dtItem.Columns.Add("fltItemWeight")

            If Not dtItem.Columns.Contains("IsFreeShipping") Then dtItem.Columns.Add("IsFreeShipping")
            If Not dtItem.Columns.Contains("fltHeight") Then dtItem.Columns.Add("fltHeight")
            If Not dtItem.Columns.Contains("fltWidth") Then dtItem.Columns.Add("fltWidth")
            If Not dtItem.Columns.Contains("fltLength") Then dtItem.Columns.Add("fltLength")
            If Not dtItem.Columns.Contains("numSortOrder") Then dtItem.Columns.Add("numSortOrder")


            Dim dr As DataRow
            Dim lngOppItemId As Long

            If AddMode = True Then
                dr = dtItem.NewRow
                dr("numSortOrder") = numSortOrder
                dr("Op_Flag") = 1

                Dim objOpportunity As New BACRM.BusinessLogic.Opportunities.MOpportunity
                Dim maxOppItemID As Long = objOpportunity.GetMaxOppItemID()

                Dim rnd As New Random
                Dim tempOppItemID As Long
                Do
                    tempOppItemID = rnd.Next(maxOppItemID, maxOppItemID + 20000)
                Loop While dtItem.Select("numoppitemtCode=" & tempOppItemID).Length > 0

                dr("numoppitemtCode") = tempOppItemID
                dr("numItemCode") = ItemCode
                dr("vcModelID") = ModelId

                ' dr("CustomerPartNo") = CustomerPartNo

                dr("ItemType") = ItemTypeText
                dr("vcItemName") = ItemName

                If numProjectID > 0 AndAlso numProjectStageID > 0 Then
                    dr("numProjectID") = numProjectID
                    dr("numProjectStageID") = numProjectStageID
                Else
                    dr("numProjectID") = 0
                    dr("numProjectStageID") = 0
                End If

                dr("charItemType") = ItemTypeValue
                dr("bitIsAuthBizDoc") = False
                dr("numUnitHourReceived") = 0
                dr("numQtyShipped") = 0
                dr("numItemClassification") = numItemClassification
            ElseIf IsPOS = True Then
                If OppItemID > 0 Then
                    dr = dtItem.Rows.Find(OppItemID)
                    lngOppItemId = dr("numoppitemtCode")

                End If

            Else
                If OppItemID > 0 Then
                    dr = dtItem.Rows.Find(OppItemID)
                    lngOppItemId = dr("numoppitemtCode")
                End If
            End If

            If dtItem.Columns.Contains("CustomerPartNo") Then
                dr("CustomerPartNo") = CustomerPartNo
            End If

            If dtItem.Columns.Contains("numMaxWorkOrderQty") Then
                dr("numMaxWorkOrderQty") = numMaxWOQty
            End If

            If dtItem.Columns.Contains("ItemReleaseDate") Then
                dr("ItemReleaseDate") = itemReleaseDate
            End If

            If dtItem.Columns.Contains("Location") Then
                dr("Location") = ShipFromLocation
            End If

            If dtItem.Columns.Contains("numShipToAddressID") Then
                dr("numShipToAddressID") = numShipToAddressID
            End If

            If dtItem.Columns.Contains("ShipToFullAddress") Then
                dr("ShipToFullAddress") = ShipToFullAddress
            End If

            If dtItem.Columns.Contains("InclusionDetail") Then
                dr("InclusionDetail") = InclusionDetail
            End If

            If dtItem.Columns.Contains("OnHandAllocation") Then
                dr("OnHandAllocation") = OnHandAllocation
            End If

            If dtItem.Columns.Contains("KitChildItems") Then
                dr("KitChildItems") = KitChildItems
            End If

            If dtItem.Columns.Contains("bitHasKitAsChild") Then
                dr("bitHasKitAsChild") = isHasKitAsChild
            End If

            'Strat - Changed By Sandeep
            'Reason - Following code is removed from above if condition because it is also required when item is updated.
            dr("bitWorkOrder") = bitWorkOrder

            If bitWorkOrder = True Then
                dr("vcInstruction") = vcInstruction
                dr("bintCompliationDate") = bintCompliationDate
                dr("numWOAssignedTo") = numWOAssignedTo
            End If
            'End - Changed By Sandeep

            dr("vcItemDesc") = Replace(Desc, "'", "''")

            If dtItem.Columns.Contains("vcVendorNotes") Then
                dr("vcVendorNotes") = vcVendorNotes
            End If
            dr("numUnitHour") = Units

            If dtItem.Columns.Contains("numOrigUnitHour") Then
                dr("numOrigUnitHour") = Units
            End If

            dr("monPrice") = Price

            If dtItem.Columns.Contains("monSalePrice") Then
                dr("monSalePrice") = salePrice
            Else
                salePrice = Price
            End If

            dr("numUOM") = UOM
            dr("vcUOMName") = vcUOMName
            dr("UOMConversionFactor") = UOMConversionFactor
            dr("monTotAmtBefDiscount") = Math.Truncate(10000 * CCommon.ToDouble((Units * UOMConversionFactor) * Price)) / 10000
            dr("vcBaseUOMName") = vcBaseUOMName

            If dtItem.Columns.Contains("bitItemPriceApprovalRequired") Then
                dr("bitItemPriceApprovalRequired") = False
            End If

            dr("bitDiscountType") = IIf(IsDiscountInPer = True, 0, 1)
            dr("fltDiscount") = Discount

            If IsDiscountInPer = True Then
                If Discount = 0 Then
                    dr("monTotAmount") = Math.Truncate(10000 * CCommon.ToDouble((Units * UOMConversionFactor) * salePrice)) / 10000
                Else
                    dr("monTotAmount") = Math.Truncate(10000 * (CCommon.ToDouble(dr("monTotAmtBefDiscount")) - CCommon.ToDouble(dr("monTotAmtBefDiscount") * dr("fltDiscount") / 100))) / 10000
                End If
            Else
                If Discount = 0 Then
                    dr("monTotAmount") = Math.Truncate(10000 * CCommon.ToDouble((Units * UOMConversionFactor) * salePrice)) / 10000
                Else
                    dr("monTotAmount") = CCommon.ToDouble(dr("monTotAmtBefDiscount") - dr("fltDiscount"))
                End If
            End If

            If dtItem.Columns.Contains("TotalDiscountAmount") Then
                If Not IsDiscountInPer Then
                    dr("TotalDiscountAmount") = dr("fltDiscount")
                Else
                    If dr("monTotAmount") > dr("monTotAmtBefDiscount") Then
                        dr("TotalDiscountAmount") = 0
                    Else
                        dr("TotalDiscountAmount") = dr("monTotAmtBefDiscount") - dr("monTotAmount")
                    End If
                End If
            End If

            dr("numVendorWareHouse") = numVendorWareHouse
            dr("numShipmentMethod") = numShipmentMethod
            dr("numSOVendorId") = numSOVendorId
            dr("vcSKU") = strSKU

            If ItemTypeValue = "P" Then
                If DropShip = False Then
                    dr("Warehouse") = Warehouse
                    dr("numWarehouseItmsID") = WareHouseItemID
                    If dtItem.Columns.Contains("Attributes") Then
                        dr("Attributes") = objCommon.GetAttributesForWarehouseItem(dr("numWarehouseItmsID"), False)
                    End If
                    dr("DropShip") = False
                    dr("numToWarehouseItemID") = ToWarehouseItemID
                    dr("numVendorID") = 0
                Else
                    dr("numWarehouseID") = DBNull.Value
                    dr("numWarehouseItmsID") = DBNull.Value
                    dr("Warehouse") = ""
                    If dtItem.Columns.Contains("Attributes") Then
                        dr("Attributes") = ""
                    End If
                    dr("DropShip") = True
                    dr("numVendorID") = numPrimaryVendorID
                End If
            Else
                dr("numWarehouseID") = DBNull.Value
                dr("numWarehouseItmsID") = DBNull.Value
                dr("numVendorID") = numPrimaryVendorID
                dr("DropShip") = DropShip
                If AddMode Then
                    If dtItem.Columns.Contains("Attributes") Then
                        dr("Attributes") = vcAttributes
                    End If

                    If dtItem.Columns.Contains("AttributeIDs") Then
                        dr("AttributeIDs") = vcAttributeIDs
                    End If
                End If
            End If
            If numContainer <> -1 Then
                If dtItem.Columns.Contains("numContainer") Then
                    dr("numContainer") = numContainer
                End If
            End If
            If numContainerQty <> -1 Then
                If dtItem.Columns.Contains("numContainerQty") Then
                    dr("numContainerQty") = numContainerQty

                End If
            End If

            If dtItem.Columns.Contains("numPromotionID") Then
                dr("numPromotionID") = numPromotionID
            End If
            If dtItem.Columns.Contains("bitPromotionTriggered") Then
                dr("bitPromotionTriggered") = IsPromotionTriggered
            End If
            If dtItem.Columns.Contains("vcPromotionDetail") Then
                dr("vcPromotionDetail") = vcPromotionDetail
            End If
            If dtItem.Columns.Contains("numCost") Then
                dr("numCost") = numCost
            End If

            If AddMode = True Then
                dtItem.Rows.Add(dr)
            End If

            If bDirectAdd = False Then
                If dtChildItems.Rows.Count > 0 Then
                    For i As Int32 = 0 To dtChildItems.Rows.Count - 1
                        If (dsTemp.Tables(0).Select("numItemCode='" & dtChildItems.Rows(i)("numItemCode") & "'").Length = 0) Then
                            dr = dtItem.NewRow

                            dr("Op_Flag") = 1
                            dr("numoppitemtCode") = CType(IIf(IsDBNull(dtItem.Compute("MAX(numoppitemtCode)", "")), 0, dtItem.Compute("MAX(numoppitemtCode)", "")), Integer) + 1 'dtItem.Rows.Count + 1
                            dr("numItemCode") = dtChildItems.Rows(i)("numItemCode")

                            If IsDBNull(dtChildItems.Rows(i)("numQtyItemsReq")) Then
                                dr("numUnitHour") = 0
                            ElseIf dtChildItems.Rows(i)("numQtyItemsReq").ToString.Length = 0 Then
                                dr("numUnitHour") = 0
                            Else
                                dr("numUnitHour") = Math.Abs(CCommon.ToDecimal(dtChildItems.Rows(i)("numQtyItemsReq").ToString()))
                            End If

                            dr("monPrice") = CCommon.ToDecimal(dtChildItems.Rows(i)("monListPrice").ToString())
                            If dtItem.Columns.Contains("monSalePrice") Then
                                dr("monSalePrice") = CCommon.ToDecimal(dtChildItems.Rows(i)("monListPrice").ToString())
                            End If

                            dr("bitDiscountType") = False
                            dr("fltDiscount") = 0
                            dr("monTotAmtBefDiscount") = IIf(IsDBNull(dr("numUnitHour")), 0, dr("numUnitHour")) * IIf(IsDBNull(dr("monPrice")), 0, dr("monPrice"))

                            dr("monTotAmount") = dr("monTotAmtBefDiscount") - dr("fltDiscount")

                            If PageType = "Sales" Then
                                dr("Tax0") = 0
                                dr("bitTaxable0") = False
                            End If

                            dr("vcItemDesc") = dtChildItems.Rows(i)("txtItemDesc")

                            If dtChildItems.Rows(i)("numWarehouseItmsID") <> 0 Then
                                dr("numWarehouseItmsID") = dtChildItems.Rows(i)("numWarehouseItmsID")
                                dr("Warehouse") = dtChildItems.Rows(i)("vcWareHouse")
                            Else
                                dr("numWarehouseItmsID") = DBNull.Value
                                dr("Warehouse") = DBNull.Value
                            End If

                            dr("DropShip") = False
                            dr("bitIsAuthBizDoc") = False
                            dr("numUnitHourReceived") = 0
                            dr("numQtyShipped") = 0

                            dr("vcItemName") = dtChildItems.Rows(i)("vcItemName")
                            dr("ItemType") = dtChildItems.Rows(i)("charItemType")

                            dr("UOMConversionFactor") = 1
                            dr("numUOM") = 0
                            dr("vcUOMName") = ""
                            dr("bitWorkOrder") = False
                            dr("vcBaseUOMName") = ""

                            dr("numVendorWareHouse") = "0"
                            dr("numShipmentMethod") = "0"
                            dr("numSOVendorId") = "0"

                            If numProjectID > 0 AndAlso numProjectStageID > 0 Then
                                dr("numProjectID") = numProjectID
                                dr("numProjectStageID") = numProjectStageID
                            Else
                                dr("numProjectID") = 0
                                dr("numProjectStageID") = 0
                            End If
                            dr("charItemType") = dtChildItems.Rows(i)("charItemType")

                            dtItem.Rows.Add(dr)
                        End If
                    Next
                End If
            End If

            dr.AcceptChanges()
            dsTemp.AcceptChanges()

            If PageType = "Sales" Or AddMode = False Then
                If dtSerItem.ParentRelations.Count = 0 Then dsTemp.Relations.Add("Item", dsTemp.Tables(0).Columns("numoppitemtCode"), dsTemp.Tables(1).Columns("numoppitemtCode"))
            End If

            Return dsTemp
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UpdateImportFileStatus(ByVal recordInserted As Integer, ByVal recordUpdated As Integer, ByVal recordErrorOcurred As Integer, ByVal addedRecords As String, ByVal updatedRecords As String, ByVal strImportType As String)
        Try
            objImport = New ImportWizard(intImportFileID, DomainID)
            With objImport
                .ImportFileID = intImportFileID
                .ImportFileName = objImport.ImportFileName
                .MasterID = objImport.ImportMasterID
                .RecordAdded = recordInserted
                .RecordUpdated = recordUpdated
                .Errors = recordErrorOcurred
                .Duplicates = 0
                .HistoryID = 0
                .CreatedDate = objImport.CreatedDate
                .ImportDate = DateTime.Now
                .DomainId = DomainID
                .UserContactID = UserCntID
                .Status = enmImportDataStatus.Import_Completed
                .hasSendEmail = True
                .MappedData = Nothing
                .Mode = 1
                .History_Added_Value = If(.RecordAdded > 0 Or .RecordUpdated > 0, addedRecords, "")
                .HistoryDateTime = DateTime.Now
                .SaveImportMappedData()

                If .hasSendEmail = True Then

                    Try
                        Dim objSendMail As New Email
                        objSendMail.SendSystemEmail("Import File Statistics : ", _
                                                    "<p>" & strImportType & " records created: " & recordInserted & If(addedRecords.Length > 0, " : " & addedRecords, "") & "</p>" & _
                                                    "<p>" & strImportType & " records Updated: " & recordUpdated & If(updatedRecords.Length > 0, " : " & updatedRecords, "") & "</p>" & _
                                                    "<p>" & strImportType & " records have Errors: " & recordErrorOcurred & "</p>" & _
                                                    "<p>If found errors, please contact to administrator.</p>", _
                                                    "", _
                                                    System.Configuration.ConfigurationManager.AppSettings("FromAddress"), _
                                                    strEmail, strName)
                    Catch ex As Exception

                        WriteMessage("Domain ID : " & DomainID)
                        WriteMessage("User Contact ID : " & UserCntID)
                        WriteMessage("Error ocuured while sending import report email.")
                    End Try
                End If
            End With
        Catch ex As Exception
            'DO NOT TROW ERROR
        End Try
    End Sub

    Private Class OrderImport
        Public Property MatchID As String

        Public Property DivisionID As Long
        Public Property ContactID As Long
        Public Property AssignedTo As Long
        Public Property OrderStatus As Long
        Public Property CustomerPO As String

        Public Property CompanyName As String
        Public Property ContactFirstName As String
        Public Property ContactLastName As String
        Public Property ContactEmail As String

        Public Property BillToStreet As String
        Public Property BillToCity As String
        Public Property BillToState As Long
        Public Property BillToPostalCode As String
        Public Property BillToCountry As Long

        Public Property ShipToStreet As String
        Public Property ShipToCity As String
        Public Property ShipToState As Long
        Public Property ShipToPostalCode As String
        Public Property ShipToCountry As Long

        Public Property ShipVia As Long
        Public Property ShippingService As Long

        Public Property IsValid As Boolean
        Public Property ErrorMessage As String

        Public Property LineItems As List(Of OrderImportLineItem)
    End Class

    Private Class OrderImportLineItem
        Public Property ItemCode As Long
        Public Property ItemName As String
        Public Property ItemDescription As String
        Public Property ModelID As String
        Public Property ItemType As String
        Public Property SKU As String
        Public Property KitParent As Boolean
        Public Property Units As Double
        Public Property UnitPrice As Double
        Public Property WarehouseItemID As Long
        Public Property WarehouseID As Long
        Public Property WarehouseLocationID As Long
        Public Property IsWorkOrder As Boolean
        Public Property IsDropship As Boolean
        Public Property UOM As Long
        Public Property Discount As Double
        Public Property DiscountType As Boolean
        Public Property Cost As Double
        Public Property IsHasKitAsChild As Boolean
        Public Property KitChilds As String

        Public Property CSVRow As Long
        Public Property IsValid As Boolean
        Public Property ErrorMessage As String
    End Class
End Class
