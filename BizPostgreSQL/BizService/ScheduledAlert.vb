﻿Imports BACRM.BusinessLogic.Notification
Imports MailBee.SmtpMail
Imports System.Configuration
Imports System.IO
Imports MailBee
Imports MailBee.Security
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.WebAPI
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Opportunities
Imports System.Threading
Imports System.Data.SqlClient

Imports nsoftware.InEBank
Imports BACRM.BusinessLogic.Workflow
Imports System.Xml
Imports System.Text
Imports System.Net.Mail
Imports System.Text.RegularExpressions
Imports Amazon
Imports Amazon.SimpleEmail
Imports Amazon.SimpleEmail.Model
Imports System.Reflection
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Outlook
Imports System.Net
Imports BACRM.BusinessLogic.ItemShipping

Public Class ScheduledAlert
    Dim ObjNotification As Notification
    Private m As Smtp = Nothing

    'Action Items Scheduling
    Public Function ActItemsMail() As Boolean
        Smtp.LicenseKey = ConfigurationManager.AppSettings("SMTPLicense")
        Try
            If ObjNotification Is Nothing Then
                ObjNotification = New Notification
            End If
            Dim objEmail As New Email
            Dim dataTable As DataTable
            Dim smtpserver As SmtpServer
            ObjNotification.interval = ConfigurationManager.AppSettings("Timer2")
            dataTable = ObjNotification.getActItemsMail
            Dim strSentComm As String = ""
            For Each Erow As DataRow In dataTable.Rows
                m = New Smtp
                smtpserver = New SmtpServer

                'smtpserver.Name = ConfigurationManager.AppSettings("SMTPServer")
                objEmail.GetSMTPDetails(smtpserver, Long.Parse(Erow.Item("numDomainID").ToString()))
                smtpserver.SslProtocol = MailBee.Security.SecurityProtocol.TlsAuto
                m.SmtpServers.Add(smtpserver)
                m.ThrowExceptions = False
                If File.Exists("c:\aspNetEmail.log") = True Then
                    m.Log.Filename = "C:\aspNetEmail.log"
                End If
                'Get From Email Details based on DomainID
                objEmail.GetFromEmailDetails(m, Long.Parse(Erow.Item("numDomainID").ToString()))
                'm.From.DisplayName = "Administrator"
                'm.From.AsString = "Administrator@bizautomation.com"
                m.To.AsString = IIf(IsDBNull(Erow.Item("vcEmail")), "", Erow.Item("vcEmail"))
                m.Subject = IIf(IsDBNull(Erow.Item("vcSubject")), "", Erow.Item("vcSubject"))
                m.BodyHtmlText = IIf(IsDBNull(Erow.Item("vcBody")), "", Erow.Item("vcBody"))
                m.Send()
                If strSentComm = "" Then
                    strSentComm = Erow.Item("numCommId")
                Else
                    strSentComm = strSentComm & "," & Erow.Item("numCommId")
                End If

            Next
            If strSentComm <> "" Then

            End If
            ObjNotification.StrCommIds = strSentComm
            ObjNotification.UpdateActItems()

            'getMileStonesAlerts
            dataTable = ObjNotification.getMileStonesAlerts

            For Each Erow As DataRow In dataTable.Rows
                m = New Smtp
                smtpserver = New SmtpServer
                objEmail.GetSMTPDetails(smtpserver, Long.Parse(Erow.Item("numDomainID").ToString()))
                smtpserver.SslProtocol = MailBee.Security.SecurityProtocol.TlsAuto
                m.SmtpServers.Add(smtpserver)
                If File.Exists("c:\aspNetEmail.log") = True Then
                    m.Log.Filename = "C:\aspNetEmail.log"
                End If
                m.ThrowExceptions = False
                m.From.AsString = IIf(IsDBNull(Erow.Item("vcFrom")), "", Erow.Item("vcFrom"))
                m.To.AsString = IIf(IsDBNull(Erow.Item("vcTo")), "", Erow.Item("vcTo"))
                m.Cc.AsString = IIf(IsDBNull(Erow.Item("vcCC")), "", Erow.Item("vcCC"))
                m.Subject = IIf(IsDBNull(Erow.Item("vcSubject")), "", Erow.Item("vcSubject"))
                m.BodyHtmlText = IIf(IsDBNull(Erow.Item("vcBody")), "", Erow.Item("vcBody"))
                m.Send()
            Next
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Sub SendAlertMails()
        Smtp.LicenseKey = ConfigurationManager.AppSettings("SMTPLicense")
        Try
            If ObjNotification Is Nothing Then
                ObjNotification = New Notification
            End If
            Dim sb As New System.Text.StringBuilder
            Dim dataTable As DataTable
            dataTable = ObjNotification.getEmailAlerts()
            Dim objCommon As New CCommon
            WriteMessage("Total No of alerts to send: " + dataTable.Rows.Count.ToString())
            For Each Erow As DataRow In dataTable.Rows
                m = New Smtp
                Dim smtpserver As SmtpServer = New SmtpServer
                If Erow.Item("bitPSMTPServer") = True Then
                    smtpserver.AuthMethods = AuthenticationMethods.Auto
                    smtpserver.AuthOptions = AuthenticationOptions.PreferSimpleMethods
                    smtpserver.AccountName = Erow.Item("vcPSMTPUserName")
                    Dim strPassword As String
                    strPassword = objCommon.Decrypt(Erow.Item("vcPSmtpPassword"))
                    smtpserver.Password = strPassword
                End If
                If Erow.Item("numPSMTPPort") <> 0 Then
                    smtpserver.Port = Erow.Item("numPSMTPPort")
                End If
                If Erow.Item("bitPSMTPSSL") = True Then
                    smtpserver.SslMode = SslStartupMode.UseStartTls
                End If
                smtpserver.SslProtocol = MailBee.Security.SecurityProtocol.TlsAuto
                m.SmtpServers.Add(smtpserver)

                If File.Exists("c:\aspNetEmail.log") = True Then
                    m.Log.Filename = "C:\aspNetEmail.log"
                End If
                m.ThrowExceptions = False
                m.From.DisplayName = "Administrator"
                m.From.AsString = "Administrator@bizautomation.com"
                'm.FromName = "Administrator"
                m.To.AsString = IIf(IsDBNull(Erow.Item("vcTo")), "", Erow.Item("vcTo")) '"chitnanprajapati@gmail.com"
                m.Cc.AsString = IIf(IsDBNull(Erow.Item("vcCC")), "", Erow.Item("vcCC"))
                m.Subject = IIf(IsDBNull(Erow.Item("vcSubject")), "", Erow.Item("vcSubject"))
                m.BodyHtmlText = IIf(IsDBNull(Erow.Item("vcBody")), "", Erow.Item("vcBody"))
                ' Attempt to connect.
                m.Connect()

                m.Send()
                m.Disconnect()


                '' Display the host name of the server the connection was established with.
                'sb.Append("Connected to " + m.SmtpServers(m.GetCurrentSmtpServerIndex()).Name)
                '' Make sure all the recipients are ok.
                'If m.TestSend(SendFailureThreshold.AllRecipientsFailed) <> TestSendResult.OK Then
                '    sb.Append("No recipients can receive the message.recipients:" + m.To.AsString)
                '    ObjNotification.DeliveryStatus = DeliveryStatus.Failed
                'Else
                '    ' Show refused recipients if they are exists
                '    If m.GetRefusedRecipients().Count > 0 Then
                '        sb.Append("The following recipients failed: " & m.GetRefusedRecipients().ToString())
                '        ObjNotification.DeliveryStatus = DeliveryStatus.Failed
                '    Else
                '        sb.Append("All recipients are ok. Will send the message now.")

                '        ' Send e-mail. If it cannot be delivered, bounce will
                '        ' arrive to bounce@domain3.com, not to joe@domain1.com

                '        m.Send()
                '        ObjNotification.DeliveryStatus = DeliveryStatus.Sent
                '        'm.Send("bounce@domain.com", CType(Nothing, String))
                '        sb.Append("Sent to: " + m.GetAcceptedRecipients().ToString())
                '    End If
                'End If
                '' Disconnect from the server
                'm.Disconnect()

                ''update status of mail 
                'If Not IsDBNull(Erow.Item("numConECampDTLID")) Then
                '    ObjNotification.numConECampDTLID = Erow.Item("numConECampDTLID")
                '    ObjNotification.EmailLog = sb.ToString()
                '    ObjNotification.UpdateECampaignAlertStatus()
                'End If
                sb.Remove(0, sb.Length)
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub SendECampaignMailsAmazon()
        Try
            Dim strError As String = ""
            Dim mailMessage As MailMessage
            Dim dataTable As DataTable
            If ObjNotification Is Nothing Then
                ObjNotification = New Notification
            End If

            dataTable = ObjNotification.getECampaignMails()
            WriteMessage("Total No of Mails Fetched : " + dataTable.Rows.Count.ToString())
            Dim count As Int16 = 0

            For Each row As DataRow In dataTable.Rows
                If Not String.IsNullOrEmpty(row("vcAWSDomain")) AndAlso
                   Not String.IsNullOrEmpty(row("vcAWSAccessKey")) AndAlso
                   Not String.IsNullOrEmpty(row("vcAWSSecretKey")) AndAlso
                   Not String.IsNullOrEmpty(row("vcFrom")) Then

                    mailMessage = New MailMessage()

                    Try
                        Dim body As String = IIf(IsDBNull(row.Item("vcBody")), "", row.Item("vcBody"))
                        body = body & "<img style='display:none'  width ='0' height='0' src='" & ConfigurationManager.AppSettings("EmailTracking") & "?&numConECampDTLID=" & row.Item("numConECampDTLID") & "' />"
                        body = ECampaignReplaceLinks(body, CCommon.ToLong(row.Item("numConECampDTLID")))

                        mailMessage.To.Add(IIf(IsDBNull(row.Item("vcTo")), "", row.Item("vcTo")))
                        mailMessage.From = New MailAddress(row.Item("vcFrom"))
                        mailMessage.Subject = IIf(IsDBNull(row.Item("vcSubject")), "", row.Item("vcSubject"))
                        mailMessage.SubjectEncoding = Encoding.UTF8

                        Dim plainView As AlternateView = AlternateView.CreateAlternateViewFromString(Regex.Replace(body, "<(.|\n)*?>", String.Empty), Encoding.UTF8, "text/plain")
                        mailMessage.AlternateViews.Add(plainView)

                        Dim htmlView As AlternateView = AlternateView.CreateAlternateViewFromString(body, Encoding.UTF8, "text/html")
                        mailMessage.AlternateViews.Add(htmlView)


                        Dim rawMessage As New RawMessage()
                        Using memoryStream As MemoryStream = ConvertMailMessageToMemoryStream(mailMessage)
                            rawMessage.Data = memoryStream
                        End Using

                        Dim toArray() As String = IIf(IsDBNull(row.Item("vcTo")), "", row.Item("vcTo")).Split(",")
                        Dim request As New SendRawEmailRequest()
                        request.RawMessage = rawMessage
                        request.Source = row.Item("vcFrom")
                        request.Destinations = toArray.ToList()

                        Dim region As RegionEndpoint = RegionEndpoint.USWest2
                        Dim client As New AmazonSimpleEmailServiceClient(CCommon.ToString(row("vcAWSAccessKey")), CCommon.ToString(row("vcAWSSecretKey")), region)


                        If count = CCommon.ToInteger(ConfigurationManager.AppSettings("AmazonSESMaxPerSec")) Then
                            count = 0
                            System.Threading.Thread.Sleep(1000)
                        End If

                        Dim response As SendRawEmailResponse = client.SendRawEmail(request)

                        If Not IsDBNull(row.Item("numConECampDTLID")) Then
                            ObjNotification.numConECampDTLID = row.Item("numConECampDTLID")
                            ObjNotification.EmailLog = "Email sent sucessfully"
                            ObjNotification.DeliveryStatus = DeliveryStatus.Sent
                            ObjNotification.Mode = 0
                            ObjNotification.UpdateECampaignAlertStatus()
                        End If
                    Catch exSES As AmazonSimpleEmailServiceException
                        If exSES.ErrorCode <> "500" AndAlso exSES.ErrorCode <> "400" AndAlso exSES.ErrorCode <> "503" Then
                            LogECampaignError(CCommon.ToLong(row.Item("numConECampDTLID")), exSES.Message)
                        Else
                            Throw exSES
                        End If
                    Catch ex As Exception
                        LogECampaignError(CCommon.ToLong(row.Item("numConECampDTLID")), ex.Message)
                    Finally
                        count = count + 1
                        mailMessage.Dispose()
                    End Try
                Else
                    LogECampaignError(CCommon.ToLong(row.Item("numConECampDTLID")), "Amazon Simple Email Service (Amazon SES) account detail is not configured.")
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function ECampaignReplaceLinks(ByVal body As String, ByVal numConECampDTLID As Long) As String
        Try
            'Identifying all links and replace original links with Biz Links and store it. 
            Dim objCampaign As New Campaign
            Dim r As Regex = New Regex("href\s*=\s*(?:""(?<1>[^""]*)""|(?<1>\S+))")
            Dim match As MatchCollection = r.Matches(body)
            Dim strXml As String = "<LinkRecords>"
            Dim EachLink As Match
            For Each EachLink In match
                Dim strEachLink As String = CCommon.ToString(EachLink).Replace("href", "").Replace("=", "").Replace("""", "")
                If Not strEachLink.ToLower().StartsWith("#") And Not strEachLink.ToLower().StartsWith("mailto") Then
                    strXml = strXml + "<Table>"
                    strXml = strXml + "<vcOriginalLink>" + strEachLink + "</vcOriginalLink>"
                    strXml = strXml + "</Table>"
                End If
            Next
            strXml = strXml + "</LinkRecords>"

            If strXml.Contains("<vcOriginalLink>") Then
                objCampaign.strBroadcastingLink = strXml
                objCampaign.ConEmailCampID = numConECampDTLID
                Dim dt As DataTable = objCampaign.SaveConECampaignDTLLink()


                If dt.Rows.Count > 0 Then
                    For Each dr As DataRow In dt.Rows
                        If body.Contains(CCommon.ToString(dr("vcOriginalLink"))) Then
                            body = body.Replace(CCommon.ToString(dr("vcOriginalLink")), ConfigurationManager.AppSettings("EmailTracking") & "?ConECampDTLID=" & CCommon.ToString(numConECampDTLID) & "&LinkID=" & dr("numLinkId"))
                        End If
                    Next
                End If
            End If

            Return body
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub LogECampaignError(ByVal numConECampDTLID As Long, ByVal message As String)
        Try
            Dim strError As String = "Sending Failed,ID=" & numConECampDTLID & " Error:" & message
            WriteMessage(strError)

            If numConECampDTLID > 0 Then
                ObjNotification.numConECampDTLID = numConECampDTLID
                ObjNotification.EmailLog = strError
                ObjNotification.DeliveryStatus = DeliveryStatus.Failed
                ObjNotification.Mode = 0
                ObjNotification.UpdateECampaignAlertStatus()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Function ConvertMailMessageToMemoryStream(message As MailMessage) As MemoryStream
        Try
            Dim assembly As Assembly = GetType(SmtpClient).Assembly
            Dim mailWriterType As Type = assembly.[GetType]("System.Net.Mail.MailWriter")
            Dim fileStream As New MemoryStream()

            Dim mailWriterContructor As ConstructorInfo = mailWriterType.GetConstructor(BindingFlags.Instance Or BindingFlags.NonPublic, Nothing, New Type() {GetType(Stream)}, Nothing)
            Dim mailWriter As Object = mailWriterContructor.Invoke(New Object() {fileStream})

            Dim sendMethod As MethodInfo = GetType(MailMessage).GetMethod("Send", BindingFlags.Instance Or BindingFlags.NonPublic)
            sendMethod.Invoke(message, BindingFlags.Instance Or BindingFlags.NonPublic, Nothing, New Object() {mailWriter, True, True}, Nothing)

            Dim closeMethod As MethodInfo = mailWriter.[GetType]().GetMethod("Close", BindingFlags.Instance Or BindingFlags.NonPublic)
            closeMethod.Invoke(mailWriter, BindingFlags.Instance Or BindingFlags.NonPublic, Nothing, New Object() {}, Nothing)

            Return fileStream
        Catch ex As Exception
            Throw
        End Try

    End Function

    Public Sub SendEcampaignMails()
        Smtp.LicenseKey = ConfigurationManager.AppSettings("SMTPLicense")
        Dim dataTable As DataTable
        Dim filename As String = String.Empty
        Dim objCommon As New CCommon
        If ObjNotification Is Nothing Then
            ObjNotification = New Notification
        End If
        Dim strError As String = ""
        Dim objServer As SmtpServer
        Try
            dataTable = ObjNotification.getECampaignMails()
            WriteMessage("Total No of Mails Fetched : " + dataTable.Rows.Count.ToString())

            For Each Erow As DataRow In dataTable.Rows
                'if credentials are valid
                If CBool(Erow("bitSMTPServer")) = True Then

                    m = New Smtp
                    strError = ""
                    Try
                        'Add SMTP server
                        objServer = New SmtpServer()
                        objServer.Name = Erow("vcSMTPServer").ToString
                        If CBool(Erow("bitSMTPAuth")) = True Then
                            objServer.AuthMethods = AuthenticationMethods.Auto
                            objServer.AuthOptions = AuthenticationOptions.PreferSimpleMethods
                            objServer.AccountName = Erow("vcFrom").ToString
                            objServer.Password = objCommon.Decrypt(Erow("vcSmtpPassword").ToString)
                        End If
                        If CInt(Erow("numSMTPPort")) <> 0 Then
                            objServer.Port = CInt(Erow("numSMTPPort"))
                        End If
                        If CBool(Erow("bitSMTPSSL")) = True Then
                            objServer.SslMode = SslStartupMode.UseStartTls
                        End If
                        objServer.SslProtocol = MailBee.Security.SecurityProtocol.TlsAuto
                        m.SmtpServers.Add(objServer)

                        If File.Exists("c:\aspNetEmail.log") = True Then
                            m.Log.Filename = "C:\aspNetEmail.log"
                            m.Log.Enabled = True
                        End If
                        m.ThrowExceptions = False
                        m.From.DisplayName = Erow.Item("vcFrom")
                        m.From.AsString = Erow.Item("vcFrom")
                        m.To.AsString = IIf(IsDBNull(Erow.Item("vcTo")), "", Erow.Item("vcTo"))
                        m.Cc.AsString = IIf(IsDBNull(Erow.Item("vcCC")), "", Erow.Item("vcCC"))
                        m.Subject = IIf(IsDBNull(Erow.Item("vcSubject")), "", Erow.Item("vcSubject"))
                        m.BodyHtmlText = IIf(IsDBNull(Erow.Item("vcBody")), "", Erow.Item("vcBody"))
                        m.Send()
                    Catch ex As Exception
                        strError = "Sending Failed,ID=" & Erow.Item("numConECampDTLID") & " Error:" & ex.Message
                        WriteMessage(strError)
                    End Try
                    'update status of mail 
                    If Not IsDBNull(Erow.Item("numConECampDTLID")) Then
                        ObjNotification.numConECampDTLID = Erow.Item("numConECampDTLID")
                        ObjNotification.EmailLog = If(strError.Length > 1, strError, "Email sent sucessfully")
                        ObjNotification.DeliveryStatus = DeliveryStatus.Sent
                        ObjNotification.Mode = 0
                        ObjNotification.UpdateECampaignAlertStatus()
                    End If

                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub CreateECampaignActionItems()
        If ObjNotification Is Nothing Then
            ObjNotification = New Notification
        End If
        Dim dt As DataTable
        Dim objActionItem As New ActionItem
        Dim dtTemplate As DataTable
        Dim lngCommId As Long
        Dim strError As String = ""
        Try
            dt = ObjNotification.getECampaignActionItems()
            WriteMessage("Create Action Items from Drip Campaign- Fetched records :" & dt.Rows.Count.ToString())
            For Each dr As DataRow In dt.Rows
                objActionItem.RowID = dr("numActionItemTemplate")
                dtTemplate = objActionItem.LoadThisActionItemTemplateData()
                If dtTemplate.Rows.Count > 0 Then
                    Try
                        With objActionItem
                            .CommID = 0
                            .Task = GetTask(dtTemplate.Rows(0)("Type"))
                            .ContactID = dr("numContactID")
                            .DivisionID = dr("numDivisionId")
                            .Details = ""
                            .AssignedTo = dr("numRecOwner")
                            .UserCntID = dr("numRecOwner")
                            .DomainID = dr("numDomainID")
                            If .Task = 973 Then
                                .BitClosed = 1
                            Else : .BitClosed = 0
                            End If
                            .CalendarName = ""
                            If .Task = 973 Or .Task = 974 Then
                                .StartTime = dr("intStartDate")
                                .EndTime = DateAdd(DateInterval.Day, 1, .StartTime)
                            Else
                                .StartTime = dr("intStartDate")
                                If Not IsDBNull(dtTemplate.Rows(0)("DueDays")) Then
                                    .EndTime = DateAdd(DateInterval.Day, dtTemplate.Rows(0)("DueDays"), .StartTime)
                                End If
                            End If
                            .Activity = dtTemplate.Rows(0)("Activity")
                            .Status = dtTemplate.Rows(0)("Priority")
                            .Snooze = 0
                            .SnoozeStatus = 0
                            .Remainder = 5
                            .RemainderStatus = 1
                            .ClientTimeZoneOffset = 0
                            .bitOutlook = 0
                            .SendEmailTemplate = dtTemplate.Rows(0)("bitSendEmailTemp")
                            .EmailTemplate = dtTemplate.Rows(0)("numEmailTemplate")
                            .Hours = dtTemplate.Rows(0)("tintHours")
                            .Alert = dtTemplate.Rows(0)("bitAlert")
                            '.ActivityId = lngActivityId
                            '.CaseID = GetQueryStringVal(Request.QueryString("enc"), "CaseID")
                            '.CaseTimeId = CInt(txtCaseTimeID.Text)
                            '.CaseExpId = CInt(txtCaseExpenseID.Text)
                        End With
                        lngCommId = objActionItem.SaveCommunicationinfo()
                    Catch ex As Exception
                        strError = "Creation of Action Item Failed,ID=" & dr.Item("numConECampDTLID") & " Error:" & ex.Message
                        WriteMessage(strError)
                    End Try
                    'update status of mail
                    If Not IsDBNull(dr.Item("numConECampDTLID")) Then
                        ObjNotification.numConECampDTLID = dr.Item("numConECampDTLID")
                        ObjNotification.EmailLog = If(strError.Length > 1, strError, "Action Item created sucessfully")
                        ObjNotification.Mode = 2
                        ObjNotification.DeliveryStatus = If(strError.Length > 1, DeliveryStatus.Failed, DeliveryStatus.Sent)
                        ObjNotification.UpdateECampaignAlertStatus()
                    End If

                    WriteMessage("Saved New Action Item : CommunicationID=" & lngCommId)
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub ECampaignUpdateFollowUp()

        If ObjNotification Is Nothing Then
            ObjNotification = New Notification
        End If
        Dim objAcc As New BACRM.BusinessLogic.Account.CAccounts
        Dim dt As DataTable
        Try
            dt = ObjNotification.getECampaignContactFollowUpList()
            WriteMessage("Updating FollowUp status for Drip Campaign- Fetched records :" & dt.Rows.Count.ToString())
            For Each dr As DataRow In dt.Rows

                'Change followup status only if option selected is not -- Select One --
                If CCommon.ToInteger(dr("numFollowUpID")) > 0 Then
                    objAcc.ContactID = dr("numContactID")
                    objAcc.FollowUpStatus = dr("numFollowUpID")
                    objAcc.UpdateFollowUpStatus()
                    WriteMessage("Changed FollowUp Status to " & objAcc.FollowUpStatus.ToString & " for ContactID " & objAcc.ContactID.ToString)
                End If

                ''Mark Followup flag as updated
                If Not IsDBNull(dr("numConECampDTLID")) Then
                    ObjNotification.numConECampDTLID = dr("numConECampDTLID")
                    ObjNotification.Mode = 1
                    ObjNotification.UpdateECampaignAlertStatus()
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Function GetTask(ByVal TaskName As String) As Long
        Select Case TaskName.ToLower()
            Case "communication"
                Return 971
            Case "task"
                Return 972
            Case "notes"
                Return 973
            Case "follow-up anytime"
                Return 974
            Case "sales call"
                Return 975
            Case Else
                Return 972
        End Select
    End Function
    Public Sub GetNewMails()
        Try
            Dim objEmail As New Email
            Dim objUserAccess As New UserAccess
            Dim dtUsers As DataTable
            Dim strMessage As String

            dtUsers = objUserAccess.GetImapEnabledUsers(0)
            For i As Integer = 0 To dtUsers.Rows.Count - 1
                Try
                    strMessage = objEmail.GetNewEmails(dtUsers.Rows(i)("numUserCntId").ToString(), dtUsers.Rows(i)("numDomainId").ToString(), "")
                Catch ex As Exception
                    WriteMessage("UserCntID:" & dtUsers.Rows(i)("numUserCntId").ToString() & " DomainID:" & dtUsers.Rows(i)("numDomainId").ToString() & ",Error Message:" & ex.Message + " " + ex.StackTrace.ToString())
                End Try
                WriteMessage("UserCntID:" & dtUsers.Rows(i)("numUserCntId").ToString() & " DomainID:" & dtUsers.Rows(i)("numDomainId").ToString() & ",Message:" & strMessage)
            Next

            If CCommon.ToBool(ConfigurationManager.AppSettings("IsDebugMode")) = False Then
                'Get users to whom we need to send notification mail about error in last 10 attempt.
                dtUsers = objUserAccess.GetImapEnabledUsers(1)
                Dim strEmailBody As New System.Text.StringBuilder
                strEmailBody.Append("Dear customer, <br/> ")
                strEmailBody.Append("<br />")
                strEmailBody.Append("Your IMAP account has been disabled in BizAutomation.<br />")
                strEmailBody.Append("<br />")
                strEmailBody.Append("Our server have been trying hard to get e-mails out of #EMAIL# from last one and half hours but failed.<br />")
                strEmailBody.Append("Your option is to validate your IMAP details again into BizAutomation then you will be ready to go !!.<br />")
                strEmailBody.Append("We are sorry for the inconvenience.<br />")
                strEmailBody.Append("<br />")
                strEmailBody.Append("<br />")
                strEmailBody.Append("this is automated mail from BizAutomation.<br />")
                strEmailBody.Append("please do not respond to this email.<br />")
                strEmailBody.Append("<br />")
                strEmailBody.Append("BizAutomation.com<br />")
                strEmailBody.Append("One system for your <strong><em><span style='color: #00b050;'>ENTIRE</span></em></strong>")
                strEmailBody.Append("business<br />")
                strEmailBody.Append("U.S.(888)-407-4781<br />")
                strEmailBody.Append("Outside U.S. (408)-786-5116<br />")
                strEmailBody.Append("<br />")
                strEmailBody.Append("<br />")

                Dim body As String = ""
                For i As Integer = 0 To dtUsers.Rows.Count - 1
                    body = strEmailBody.ToString()
                    body = body.Replace("#EMAIL#", dtUsers.Rows(i)("vcEmailID").ToString())
                    If objEmail.SendSystemEmail("BizAutomation Notification: Your IMAP account has been disabled in BizAutomation", body, "", "noreply@bizautomation.com", dtUsers.Rows(i)("vcEmailID").ToString(), "BizAutomation", "") = True Then
                        'Set bitNotified to 1
                        objUserAccess.DomainID = dtUsers.Rows(i)("numDomainId").ToString()
                        objUserAccess.ContactID = dtUsers.Rows(i)("numUserCntId").ToString()
                        objUserAccess.byteMode = 2
                        objUserAccess.ManageImapDtls()
                    End If

                    WriteMessage("Notification mail has been sent to UserCntID:" & dtUsers.Rows(i)("numUserCntId").ToString() & "," & dtUsers.Rows(i)("vcEmailID").ToString() & " DomainID:" & dtUsers.Rows(i)("numDomainId").ToString())
                Next
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub GetOldMails()
        Try
            Dim objEmail As New Email
            Dim objUserAccess As New UserAccess
            Dim dtUsers As DataTable
            Dim strMessage As String

            dtUsers = objUserAccess.GetImapEnabledUsers(0)
            For i As Integer = 0 To dtUsers.Rows.Count - 1
                Try
                    strMessage = objEmail.GetOldEmails(dtUsers.Rows(i)("numUserCntId").ToString(), dtUsers.Rows(i)("numDomainId").ToString())
                Catch ex As Exception
                    WriteMessage("UserCntID:" & dtUsers.Rows(i)("numUserCntId").ToString() & " DomainID:" & dtUsers.Rows(i)("numDomainId").ToString() & ",Error Message:" & ex.Message + " " + ex.StackTrace.ToString())
                End Try
                WriteMessage("UserCntID:" & dtUsers.Rows(i)("numUserCntId").ToString() & " DomainID:" & dtUsers.Rows(i)("numDomainId").ToString() & ",Message:" & strMessage)
            Next

            If CCommon.ToBool(ConfigurationManager.AppSettings("IsDebugMode")) = False Then
                'Get users to whom we need to send notification mail about error in last 10 attempt.
                dtUsers = objUserAccess.GetImapEnabledUsers(1)
                Dim strEmailBody As New System.Text.StringBuilder
                strEmailBody.Append("Dear customer, <br/> ")
                strEmailBody.Append("<br />")
                strEmailBody.Append("Your IMAP account has been disabled in BizAutomation.<br />")
                strEmailBody.Append("<br />")
                strEmailBody.Append("Our server have been trying hard to get e-mails out of #EMAIL# from last one and half hours but failed.<br />")
                strEmailBody.Append("Your option is to validate your IMAP details again into BizAutomation then you will be ready to go !!.<br />")
                strEmailBody.Append("We are sorry for the inconvenience.<br />")
                strEmailBody.Append("<br />")
                strEmailBody.Append("<br />")
                strEmailBody.Append("this is automated mail from BizAutomation.<br />")
                strEmailBody.Append("please do not respond to this email.<br />")
                strEmailBody.Append("<br />")
                strEmailBody.Append("BizAutomation.com<br />")
                strEmailBody.Append("One system for your <strong><em><span style='color: #00b050;'>ENTIRE</span></em></strong>")
                strEmailBody.Append("business<br />")
                strEmailBody.Append("U.S.(888)-407-4781<br />")
                strEmailBody.Append("Outside U.S. (408)-786-5116<br />")
                strEmailBody.Append("<br />")
                strEmailBody.Append("<br />")

                Dim body As String = ""
                For i As Integer = 0 To dtUsers.Rows.Count - 1
                    body = strEmailBody.ToString()
                    body = body.Replace("#EMAIL#", dtUsers.Rows(i)("vcEmailID").ToString())
                    If objEmail.SendSystemEmail("BizAutomation Notification: Your IMAP account has been disabled in BizAutomation", body, "", "noreply@bizautomation.com", dtUsers.Rows(i)("vcEmailID").ToString(), "BizAutomation", "") = True Then
                        'Set bitNotified to 1
                        objUserAccess.DomainID = dtUsers.Rows(i)("numDomainId").ToString()
                        objUserAccess.ContactID = dtUsers.Rows(i)("numUserCntId").ToString()
                        objUserAccess.byteMode = 2
                        objUserAccess.ManageImapDtls()
                    End If

                    WriteMessage("Notification mail has been sent to UserCntID:" & dtUsers.Rows(i)("numUserCntId").ToString() & "," & dtUsers.Rows(i)("vcEmailID").ToString() & " DomainID:" & dtUsers.Rows(i)("numDomainId").ToString())
                Next
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub GoogleToBizConact()
        Try
            Dim objUserAccess As New UserAccess
            Dim dtUsers As DataTable
            Dim strMessage As String
            Dim objCommon As New GoogleContact

            dtUsers = objUserAccess.GetImapEnabledUsers(2)
            For i As Integer = 0 To dtUsers.Rows.Count - 1
                Try
                    strMessage = objCommon.GetContacts(dtUsers.Rows(i)("numDomainId").ToString())
                Catch ex As Exception
                    WriteMessage("GoogleToBizConact UserCntID: DomainID:" & dtUsers.Rows(i)("numDomainId").ToString() & ",Error Message:" & ex.Message + " " + ex.StackTrace.ToString())
                End Try
                WriteMessage("GoogleToBizConact UserCntID: DomainID:" & dtUsers.Rows(i)("numDomainId").ToString() & ",Message:" & strMessage)
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub GoogleToBizCalendar()
        Try
            Dim objUserAccess As New UserAccess
            Dim dtUsers As DataTable
            Dim strMessage As String
            Dim objCommon As New GoogleCalendar

            dtUsers = objUserAccess.GetImapEnabledUsers(3)
            For i As Integer = 0 To dtUsers.Rows.Count - 1
                Try
                    If CCommon.ToString(dtUsers.Rows(i)("vcImapServerUrl")).ToLower().Contains("office365") Then
                        strMessage = objCommon.Office365CalendarToBiz(dtUsers.Rows(i)("numUserCntId").ToString(), dtUsers.Rows(i)("numDomainId").ToString(), System.Configuration.ConfigurationManager.AppSettings("TimeZone"))
                    Else
                        strMessage = objCommon.GoogleCalendarToBiz(dtUsers.Rows(i)("numUserCntId").ToString(), dtUsers.Rows(i)("numDomainId").ToString(), System.Configuration.ConfigurationManager.AppSettings("TimeZone"))
                    End If

                Catch ex As Exception
                    WriteMessage("GoogleToBizCalendar UserCntID:" & dtUsers.Rows(i)("numUserCntId").ToString() & " DomainID:" & dtUsers.Rows(i)("numDomainId").ToString() & ",Error Message:" & ex.Message + " " + ex.StackTrace.ToString())
                End Try
                WriteMessage("GoogleToBizCalendar UserCntID:" & dtUsers.Rows(i)("numUserCntId").ToString() & " DomainID:" & dtUsers.Rows(i)("numDomainId").ToString() & ",Message:" & strMessage)
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub BizToGoogleCalendar()
        Try
            Dim objUserAccess As New UserAccess
            Dim dtUsers As DataTable
            Dim strMessage As String
            Dim objCommon As New GoogleCalendar

            dtUsers = objUserAccess.GetImapEnabledUsers(4)
            For i As Integer = 0 To dtUsers.Rows.Count - 1
                Try
                    If CCommon.ToString(dtUsers.Rows(i)("vcImapServerUrl")).ToLower().Contains("office365") Then
                        strMessage = objCommon.BizToOffice365Calendar(dtUsers.Rows(i)("numUserCntId").ToString(), dtUsers.Rows(i)("numDomainId").ToString(), System.Configuration.ConfigurationManager.AppSettings("TimeZone"))
                    Else
                        strMessage = objCommon.BizToGoogleCalendar(dtUsers.Rows(i)("numUserCntId").ToString(), dtUsers.Rows(i)("numDomainId").ToString(), System.Configuration.ConfigurationManager.AppSettings("TimeZone"))
                    End If
                Catch ex As Exception
                    WriteMessage("BizToGoogleCalendar UserCntID:" & dtUsers.Rows(i)("numUserCntId").ToString() & " DomainID:" & dtUsers.Rows(i)("numDomainId").ToString() & ",Error Message:" & ex.Message + " " + ex.StackTrace.ToString())
                End Try
                WriteMessage("BizToGoogleCalendar UserCntID:" & dtUsers.Rows(i)("numUserCntId").ToString() & " DomainID:" & dtUsers.Rows(i)("numDomainId").ToString() & ",Message:" & strMessage)
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Public Sub BizToGoogleEmailTimer()
    '    Try
    '        Dim objUserAccess As New UserAccess
    '        Dim dtUsers As DataTable
    '        Dim strMessage As String
    '        Dim objCommon As New GoogleMail

    '        dtUsers = objUserAccess.GetImapEnabledUsers(6)
    '        For i As Integer = 0 To dtUsers.Rows.Count - 1
    '            Try
    '                strMessage = objCommon.BizToGoogleSPAM(dtUsers.Rows(i)("numUserCntId").ToString(), dtUsers.Rows(i)("numDomainId").ToString())
    '            Catch ex As Exception
    '                WriteMessage("BizToGoogleEmail UserCntID:" & dtUsers.Rows(i)("numUserCntId").ToString() & " DomainID:" & dtUsers.Rows(i)("numDomainId").ToString() & ",Error Message:" & ex.Message + " " + ex.StackTrace.ToString())
    '            End Try
    '            WriteMessage("BizToGoogleEmail UserCntID:" & dtUsers.Rows(i)("numUserCntId").ToString() & " DomainID:" & dtUsers.Rows(i)("numDomainId").ToString() & ",Message:" & strMessage)
    '        Next
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub
    'Public Sub UpdateExistingMailDetails()
    '    Try
    '        Dim objEmail As New Email
    '        Dim objUserAccess As New UserAccess
    '        Dim dtUsers As DataTable
    '        Dim strMessage As String
    '        Dim dt As DataTable

    '        dtUsers = objUserAccess.GetImapEnabledUsers()
    '        For i As Integer = 0 To dtUsers.Rows.Count - 1
    '            Try
    '                dt = objEmail.ManageMessageIndexSet(1, "", dtUsers.Rows(i)("numUserCntId").ToString())
    '                If dt.Rows.Count > 0 Then
    '                    strMessage = objEmail.UpdateExistingMailDetails(dtUsers.Rows(i)("numUserCntId").ToString(), dtUsers.Rows(i)("numDomainId").ToString(), dt)
    '                End If
    '            Catch ex As Exception
    '                WriteMessage("Temp:UserCntID:" & dtUsers.Rows(i)("numUserCntId").ToString() & " DomainID:" & dtUsers.Rows(i)("numDomainId").ToString() & ",Error Message:" & ex.Message + " " + ex.StackTrace.ToString())
    '            End Try
    '            WriteMessage("Temp:UserCntID:" & dtUsers.Rows(i)("numUserCntId").ToString() & " DomainID:" & dtUsers.Rows(i)("numDomainId").ToString() & ",Message:" & strMessage)
    '        Next

    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub
    'will fetch last 20 mails and verify against database
    Public Sub SyncDeletedMails(ByVal NoOfMail As Integer)
        Try
            Dim objEmail As New Email
            Dim objUserAccess As New UserAccess
            Dim dtUsers As DataTable
            Dim strMessage As String

            dtUsers = objUserAccess.GetImapEnabledUsers(0)
            For i As Integer = 0 To dtUsers.Rows.Count - 1
                Try
                    strMessage = objEmail.SyncMailbox(NoOfMail, dtUsers.Rows(i)("numUserCntId").ToString(), dtUsers.Rows(i)("numDomainId").ToString())
                Catch ex As Exception
                    WriteMessage("UserCntID:" & dtUsers.Rows(i)("numUserCntId").ToString() & " DomainID:" & dtUsers.Rows(i)("numDomainId").ToString() & ",Error Message:" & ex.Message + " " + ex.StackTrace.ToString())
                End Try
                WriteMessage("UserCntID:" & dtUsers.Rows(i)("numUserCntId").ToString() & " DomainID:" & dtUsers.Rows(i)("numDomainId").ToString() & ",Message:" & strMessage)
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Below method will keep DealAmount in OpportunityBizDocs Table Updated Daily basis 
    'this implementation is done in order to gain performance for A/R aging Summery report Which is 20 sec as on 31/08/2009

    'Added By : Manish Anjara
    'This function will also Diagnose the accounting balance as per need & create xml files as per domain.
    Public Sub UpdateDealAmount()
        Try
            'this function takes 4-5 minits to complete .. so not using existing dataaccess layer which times out
            Dim conn As New Npgsql.NpgsqlConnection(ConfigurationManager.AppSettings("ConnectionString").ToString()) 'in seconds 
            conn.Open()
            Dim cmd As New Npgsql.NpgsqlCommand()
            cmd.Connection = conn
            cmd.CommandText = "usp_updatedealamount"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandTimeout = 2000 'don't if this is seconds or mili seconds
            cmd.ExecuteNonQuery()
            conn.Close()
            WriteMessage("******** Deal Amount Has been Updated for the day*********")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Enum DeliveryStatus
        Failed
        Sent
    End Enum
    Private Shared Sub WriteMessage(ByVal Message As String)
        Try
            WebAPI.WriteMessage(Message, System.Configuration.ConfigurationManager.AppSettings("LogFilePath"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub UpdateBankAccountBalance()
        Try
            Dim dt As DataTable
            Dim objEBanking As New EBanking
            objEBanking.BankDetailID = 0
            objEBanking.TintMode = 1
            dt = objEBanking.GetBankDetails()
            For Each dr As DataRow In dt.Rows

                Try
                    If CCommon.ToString(dr("vcAccountType")) = "Credit Card" Then
                        objEBanking.GetCreditCardStatement(dr)
                    Else
                        objEBanking.GetBankStatement(dr)
                    End If

                Catch ex As Exception
                    WriteMessage("Error Reading Account Balance for Account No : " & CCommon.ToString(dr("vcAccountNumber")))
                    WriteMessage(ex.Message)
                    WriteMessage(ex.StackTrace)
                End Try

            Next

        Catch ex As Exception
            Throw ex

        End Try

    End Sub

    Public Sub OpportunityAutomationExecution()
        Try
            WriteMessage("OpportunityAutomationExecution Process Started.")

            Dim objOA As New OpportunityAutomation
            objOA.OpportunityAutomationExecution()

            WriteMessage("OpportunityAutomationExecution Process Finished.")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub BulkOrderSalesExecution()
        Try
            WriteMessage("BulkOrderSalesExecution Process Started.")

            Dim objOpp As New MOpportunity

            objOpp.RunBulkOrderSalesBackground()

            WriteMessage("BulkOrderSalesExecution Process Finished.")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub WFExecution()
        Try
            WriteMessage("WFExecution Process Started.")

            Dim objWF As New WorkFlowExecution
            'Added by sachin
            Dim objWFM As New Workflow
            Dim dtQueue As DataTable = objWFM.GetWorkFlowQueue()
            Dim dtDateFieldWF As DataTable = objWFM.GetDateFieldWorkFlows()
            Dim dtARAgingWF As DataTable = objWFM.GetARAgingWorkFlows()

            If dtQueue.Rows.Count > 0 Or dtDateFieldWF.Rows.Count > 0 Or dtARAgingWF.Rows.Count > 0 Then

                Dim dtCopy As DataTable = dtQueue.Copy()
                Dim ds1 As New DataSet()
                ds1.Tables.Add(dtCopy)
                Dim strDirectory As String = CCommon.GetDocumentPhysicalPath() & "WFA"
                If Not Directory.Exists(strDirectory) Then
                    Directory.CreateDirectory(strDirectory)
                End If
                Dim objStreamWriter As New StreamWriter(strDirectory & "\" & "WFALogs.xml", False)
                ds1.WriteXml(objStreamWriter)
                objStreamWriter.Close()

                objWF.WFExecution()
                'Directory.Delete(strDirectory)
            Else
                Return
            End If
            'end of code by sachin
            WriteMessage("WFExecution Process Finished.")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub BizRecurrence()
        Try
            WriteMessage("BizRecurrence Process Started.")

            Dim objBizRecurrence As New BizRecurrence
            objBizRecurrence.Execute()

        Catch ex As Exception
            Throw
        Finally
            WriteMessage("BizRecurrence Process Finished.")
        End Try
    End Sub

    ''' <summary>
    ''' Moves emails which are more than 180 days old to Email Archive folder
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub ArchiveOldEmails()
        Try
            Dim objOutlook As New COutlook
            objOutlook.ArchiveEmail("All")
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub EmailCustomQueryReport()
        Try
            Dim objCustomQueryReport As New CustomQueryReport
            Dim dt As DataTable = objCustomQueryReport.GetForEmailReport()

            If Not dt Is Nothing AndAlso dt.Rows.Count() > 0 Then
                For Each dr As DataRow In dt.Rows
                    Try
                        Dim strCss As String = ""
                        Dim strFileName As String = ""
                        Dim strReportHtml As String = ""
                        Dim strFilePhysicalLocation As String = ""

                        Dim reportID As Long = CCommon.ToLong(dr("numReportID"))
                        Dim reportName As String = CCommon.ToString(dr("vcReportName"))
                        Dim numReportDomainID As Long = CCommon.ToLong(dr("numReportDomainID"))
                        Dim tintEmailFrequency As Int16 = CCommon.ToInteger(dr("tintEmailFrequency"))

                        Dim objCustomQuery As New CustomQueryReport
                        objCustomQuery.ReportID = reportID
                        Dim dtReportData As DataTable = objCustomQuery.Execute()

                        If Not dtReportData Is Nothing AndAlso dtReportData.Rows.Count > 0 Then
                            Dim strReportDataBuilder As New System.Text.StringBuilder
                            strReportDataBuilder.Append("<table id='tblReportData' style='width:100%'>")

                            'HEADER
                            strReportDataBuilder.Append("<tr>")
                            For Each myColumn As DataColumn In dtReportData.Columns
                                strReportDataBuilder.Append("<th>")
                                strReportDataBuilder.Append(myColumn.ColumnName)
                                strReportDataBuilder.Append("</th>")
                            Next
                            strReportDataBuilder.Append("</tr>")

                            For i As Int16 = 1 To dtReportData.Rows.Count
                                Dim myRow As DataRow = dtReportData.Rows(i - 1)

                                strReportDataBuilder.Append("<tr class='" & If(i Mod 2 = 0, "Row" & If(i = dtReportData.Rows.Count, " LastRow", ""), "AltRow" & If(i = dtReportData.Rows.Count, " LastRow", "")) & "'>")
                                For Each myColumn As DataColumn In dtReportData.Columns
                                    strReportDataBuilder.Append("<td>")
                                    strReportDataBuilder.Append(CCommon.ToString(myRow(myColumn.ColumnName)))
                                    strReportDataBuilder.Append("</td>")
                                Next
                                strReportDataBuilder.Append("</tr>")
                            Next

                            strReportDataBuilder.Append("</table>")

                            strCss = CCommon.ToString(dr("vcCSS"))

                            If Not String.IsNullOrEmpty(strCss) Then
                                strReportHtml = "<html><head><title></title><style type='text/css'>" & strCss & "</style></head><body><table style=""width:100%""><tr><td style=""text-align:center"">" & reportName & "</td><tr><td>" + strReportDataBuilder.ToString() + "</td></tr></table></body></html>"
                            Else
                                strReportHtml = "<html><head><title></title><style type='text/css'>#tblReportData {margin: 0;padding: 0px;border-spacing: 0px;border-collapse: collapse !important;border-color: #dad8d8 !important;font-family: 'Bookman Old Style';} #tblReportData tr td {border-top: 1px solid #fff;line-height: 20px;border: 1px solid #dad8d8;padding-left: 7px;font-size: 13px;} #tblReportData th {height: 25px;background: #e5e5e5;border: 1px solid #dad8d8;font-size: 14px;font-weight: bold;text-align: center;}  #tblReportData tr.Row {background-color: #f2f2f2;} #tblReportData tr.AltRow {background-color: #fff;}</style></head><body><table style=""width:100%""><tr><td style=""text-align:center"">" & reportName & "</td><tr><td>" + strReportDataBuilder.ToString() + "</td></tr></table></body></html>"
                            End If
                        Else
                            strReportHtml = "<html><head><title></title><style type='text/css'>#tblReportData {margin: 0;padding: 0px;border-spacing: 0px;border-collapse: collapse !important;border-color: #dad8d8 !important;font-family: 'Bookman Old Style';} #tblReportData tr td {border-top: 1px solid #fff;line-height: 20px;border: 1px solid #dad8d8;padding-left: 7px;font-size: 13px;} #tblReportData th {height: 25px;background: #e5e5e5;border: 1px solid #dad8d8;font-size: 14px;font-weight: bold;text-align: center;}  #tblReportData tr.Row {background-color: #f2f2f2;} #tblReportData tr.AltRow {background-color: #fff;}</style></head><body><table style=""width:100%""><tr><td style=""text-align:center"">" & reportName & "</td><tr><td>No Data</td></tr></table></body></html>"
                        End If

                        Try
                            Dim objHTMLToPDF As New HTMLToPDF
                            strFileName = objHTMLToPDF.ConvertHTML2PDF(strReportHtml, numReportDomainID)
                            strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(numReportDomainID) & strFileName

                            Dim objEmail As New Email
                            objEmail.SendCustomReportEmail(reportName, "", "", "noreply@bizautomation.com", CCommon.ToString(dr("vcEmailTo")), strFileName, DomainID:=numReportDomainID)

                            'LOG EMAIL SENT DATE
                            objCustomQueryReport = New CustomQueryReport
                            objCustomQueryReport.ReportID = reportID
                            objCustomQueryReport.EmailFrequency = tintEmailFrequency
                            objCustomQueryReport.EmailDate = CCommon.ToSqlDate(dr("dtDateToSend"))

                            objCustomQueryReport.LogEmailNotification()
                        Catch ex As Exception
                            WriteMessage("Custom Query Report Email Notification - Message: " & ex.Message)
                            WriteMessage("Custom Query Report Email Notification - StackTrace: " & ex.StackTrace.ToString)
                        Finally
                            Try
                                Dim file As New System.IO.FileInfo(strFilePhysicalLocation)
                                file.Delete()
                            Catch ex As Exception

                            End Try
                        End Try
                    Catch ex As Exception
                        WriteMessage("Custom Query Report Email Notification - Message: " & ex.Message)
                        WriteMessage("Custom Query Report Email Notification - StackTrace: " & ex.StackTrace.ToString)
                    End Try
                Next
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub GetCurrencyConversionRate()
        Try
            Dim objCurrencyConversionStatus As New CurrencyConversionStatus
            objCurrencyConversionStatus.GetCurrencyConversionStatus()


            'FIRST CHECK WHETHER RATES ARE FETCHED TODAY SUCCESSFULLY. 
            'IsSucceed FLAG IS MARKED TO TRUE IF RATES ARE UPDATES SUCCESSFULLY. SO IT MAY HAPPEN THAT LASTEXECUTED DATE IS OF TODAY BUT HAVEN'T SUCCEED.
            'IF NOT FETCHED SUCCESSFULLY TODAY THEN GET PRICE FROM YAHOO FINANCE API AND UPDATE IT FOR EACH DOMAIN
            If Not objCurrencyConversionStatus.LastExecutedDate Is Nothing AndAlso _
                (objCurrencyConversionStatus.LastExecutedDate <> Date.Now.Date Or Not objCurrencyConversionStatus.IsSucceed) Then

                WriteMessage("Currency Conversion Rated  Fetch - Start")

                ' GET ALL DOMAIN IDS GROUP BY BASE CURRENCY SO THAT WE DO NOT HAVE TO CALL YAHOO API FOR EACH DOMAIN
                Dim dt As DataTable = objCurrencyConversionStatus.GetDataToFetchCurrencyConversion()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    For Each dr As DataRow In dt.Rows
                        Dim vcDomainIDs As String = CCommon.ToString(dr("vcDomainIDs"))
                        Dim vcCurrencyCodes As String = CCommon.ToString(dr("vcCurrencyCodes"))

                        Dim strRequest As New StringBuilder
                        strRequest.Append("http://query.yahooapis.com/v1/public/yql?q=select * from yahoo.finance.xchange where pair in (""")
                        strRequest.Append(vcCurrencyCodes.Replace(",", ""","""))
                        strRequest.Append(""")&env=store://datatables.org/alltableswithkeys")


                        Dim objRequest As HttpWebRequest
                        objRequest = HttpWebRequest.Create(strRequest.ToString())

                        Try
                            Dim response As HttpWebResponse = objRequest.GetResponse()

                            If response.StatusCode = HttpStatusCode.OK Then
                                'UPDATE CURRENCT CONVERSION RATES
                                Using sr As New StreamReader(response.GetResponseStream())
                                    Dim responseJson As String = sr.ReadToEnd()

                                    Try
                                        Dim objCurrency As New CurrencyRates
                                        Dim doc As New XmlDocument()
                                        doc.LoadXml(responseJson)

                                        Dim m_nodelist As XmlNodeList = doc.SelectNodes("/query/results/rate")

                                        For Each m_node As XmlNode In m_nodelist
                                            'Get the Gender Attribute Value
                                            Dim currency As String = m_node("Name").InnerText
                                            Dim rate As Double = 0

                                            Double.TryParse(m_node("Rate").InnerText, rate)

                                            'UPDATE CURRECY CONVERSION RATES TO RESPECTIVE DOMAIN
                                            If Not currency = "N/A" Then
                                                Dim vcBaseCurrency As String = currency.Split("/")(1)
                                                Dim vcToCurrency As String = currency.Split("/")(0)

                                                objCurrency.UpdateExchangenRates(vcToCurrency, rate, vcDomainIDs)
                                            End If
                                        Next
                                    Catch ex As Exception
                                        UpdateCurrencyConversionStatus(objCurrencyConversionStatus.NoOfTimesTried, False)
                                        Throw
                                    End Try
                                End Using
                            Else
                                UpdateCurrencyConversionStatus(objCurrencyConversionStatus.NoOfTimesTried, False)
                            End If
                        Catch ex As Exception
                            UpdateCurrencyConversionStatus(objCurrencyConversionStatus.NoOfTimesTried, False)
                            Throw
                        End Try
                    Next

                    UpdateCurrencyConversionStatus(objCurrencyConversionStatus.NoOfTimesTried, True)
                End If

                WriteMessage("Currency Conversion Rated  Fetch - End")
            End If
        Catch ex As Exception
            WriteMessage("Currency Conversion Rated  Fetch - Error Message: " & ex.Message)
            WriteMessage("Currency Conversion Rated  Fetch - StackTrace: " & ex.StackTrace.ToString)
        End Try
    End Sub

    Private Sub UpdateCurrencyConversionStatus(ByVal noOfTimesTried As Int32, ByVal isSucceed As Boolean)
        Try
            Dim objCurrencyConversionStatus As New CurrencyConversionStatus

            objCurrencyConversionStatus.LastExecutedDate = DateTime.Now.Date
            objCurrencyConversionStatus.NoOfTimesTried = noOfTimesTried + 1
            objCurrencyConversionStatus.IsSucceed = isSucceed

            If Not isSucceed AndAlso objCurrencyConversionStatus.NoOfTimesTried Mod 5 = 0 Then
                'SENT EMAIL TO ADMINISTRATOR
                Try
                    Dim objEmail As New Email
                    If objEmail.SendCustomReportEmail("Fetching Currency Exchange Rate Failed", "Fetching Currency Exchange Rate Failed", "", "noreply@bizautomation.com", ConfigurationManager.AppSettings("CurrencyConversionRateFailed"), "", DomainID:=1) Then
                        objCurrencyConversionStatus.IsFailureNotificationSent = True
                    Else
                        objCurrencyConversionStatus.IsFailureNotificationSent = False
                    End If
                Catch ex As Exception
                    objCurrencyConversionStatus.IsFailureNotificationSent = False
                End Try
            Else
                objCurrencyConversionStatus.NoOfTimesTried = 1
                objCurrencyConversionStatus.IsFailureNotificationSent = False
            End If

            objCurrencyConversionStatus.SaveConversionStatus()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub UpdateRecordHistory()
        Dim objRecordHistory As New RecordHistory

        Try
            WriteMessage("Record modification history - Start")
            objRecordHistory.FetchDataFromCDC()
            objRecordHistory.UpdateRecordHistoryStatus(True)
        Catch ex As Exception
            WriteMessage("Record modification history - Error Message: " & ex.Message)
            WriteMessage("Record modification history - StackTrace: " & ex.StackTrace.ToString)
            Try
                Dim dtStatus As DataTable = objRecordHistory.GetRecordHistoryStatus()
                objRecordHistory.UpdateRecordHistoryStatus(False)
                If Not dtStatus Is Nothing AndAlso dtStatus.Rows.Count > 0 Then
                    Dim intNoOfTimesTried As Int32 = CCommon.ToInteger(dtStatus.Rows(0)("intNoOfTimesTried"))

                    If intNoOfTimesTried = 150 Then
                        Dim objEmail As New Email
                        objEmail.SendCustomReportEmail("Fetching Data From CDC Failed", "Fetching Data From CDC Failed", "", "noreply@bizautomation.com", "sandeep@bizautomation.com", "", DomainID:=1)
                    End If
                End If
            Catch exInner As Exception
                'DO NOT THROW ERROR
            End Try
        End Try
    End Sub

    Public Sub RecalculateCommissionForDashboardReport()
        Try
            Using conn As New Npgsql.NpgsqlConnection(ConfigurationManager.AppSettings("ConnectionString").ToString().TrimEnd(";"))
                conn.Open()
                Dim cmd As New Npgsql.NpgsqlCommand()
                cmd.Connection = conn
                cmd.CommandText = "usp_reportlistmaster_calculatecomission"
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.Add(New Npgsql.NpgsqlParameter() With {.ParameterName = "v_numdomainid", .NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, .Value = DBNull.Value})
                cmd.CommandTimeout = 2000 'don't if this is seconds or mili seconds
                cmd.ExecuteNonQuery()
                conn.Close()
            End Using
        Catch ex As Exception
            WriteMessage("DashboardCommissionReportCalculation - Error Message: " & ex.Message)
            WriteMessage("DashboardCommissionReportCalculation - StackTrace: " & ex.StackTrace.ToString)
        End Try
    End Sub

    Public Sub UpdateOrderTackingDetail()
        Try
            Dim objShippingReport As New ShippingReport
            Dim dt As DataTable = objShippingReport.GetForTrackingDetailUpdate()

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                Dim isDelivered As Boolean = False
                Dim trackingDetail As String = ""

                For Each dr As DataRow In dt.Rows
                    Try
                        If CCommon.ToLong(dr("numShippingCompany")) = 91 Then 'FedEx
                            If Not String.IsNullOrEmpty(CCommon.ToString(dr("FedexServerURL"))) _
                                AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dr("FedexDeveloperKey"))) _
                                AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dr("FedexPassword"))) _
                                AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dr("FedexAccountNumber"))) _
                                AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dr("FedexMeterNumber"))) Then

                                isDelivered = False
                                trackingDetail = ""

                                Dim track As New nsoftware.InShip.Fedextrack
                                track.FedExAccount.Server = CCommon.ToString(dr("FedexServerURL"))
                                track.FedExAccount.DeveloperKey = CCommon.ToString(dr("FedexDeveloperKey"))
                                track.FedExAccount.Password = CCommon.ToString(dr("FedexPassword"))
                                track.FedExAccount.AccountNumber = CCommon.ToString(dr("FedexAccountNumber"))
                                track.FedExAccount.MeterNumber = CCommon.ToString(dr("FedexMeterNumber"))
                                track.IdentifierType = nsoftware.InShip.FedextrackIdentifierTypes.fitTrackingNumberOrDoorTag
                                track.TrackShipment(CCommon.ToString(dr("vcTrackingNumber")))
                                track.PackageIndex = 0

                                If track.Config("Warning") = "" Then
                                    isDelivered = If(String.IsNullOrEmpty(track.PackageDeliveryDate), False, True)

                                    If track.TrackEvents.Count > 0 Then
                                        trackingDetail = track.TrackEvents(0).Status
                                    End If                                
                                End If

                                objShippingReport.ShippingReportID = CCommon.ToLong(dr("numShippingReportId"))
                                objShippingReport.IsDelivered = isDelivered
                                objShippingReport.TrackingDetail = trackingDetail
                                objShippingReport.UpdateTrackingDetail()
                            End If
                        ElseIf CCommon.ToLong(dr("numShippingCompany")) = 88 Then 'UPS
                            If Not String.IsNullOrEmpty(CCommon.ToString(dr("UPSServerURL"))) _
                                AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dr("UPSAccessKey"))) _
                                AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dr("UPSUserID"))) _
                                AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dr("UPSPassword"))) Then

                                isDelivered = False
                                trackingDetail = ""

                                Dim track As New nsoftware.InShip.Upstrack
                                track.UPSAccount.Server = CCommon.ToString(dr("UPSServerURL"))
                                track.UPSAccount.AccessKey = CCommon.ToString(dr("UPSAccessKey"))
                                track.UPSAccount.UserId = CCommon.ToString(dr("UPSUserID"))
                                track.UPSAccount.Password = CCommon.ToString(dr("UPSPassword"))
                                track.IdentifierType = nsoftware.InShip.UpstrackIdentifierTypes.uitPackageTrackingNumber
                                track.TrackShipment(CCommon.ToString(dr("vcTrackingNumber")))

                                If track.Config("Warning").Length = 0 Then
                                    If track.TrackEvents.Count > 0 Then
                                        trackingDetail = track.TrackEvents(0).Status

                                        If track.TrackEvents(0).Status.ToLower() = "delivered" Then
                                            isDelivered = True
                                        End If
                                    End If
                                End If

                                objShippingReport.ShippingReportID = CCommon.ToLong(dr("numShippingReportId"))
                                objShippingReport.IsDelivered = isDelivered
                                objShippingReport.TrackingDetail = trackingDetail
                                objShippingReport.UpdateTrackingDetail()
                            End If
                        ElseIf CCommon.ToLong(dr("numShippingCompany")) = 90 Then 'USPS
                            If Not String.IsNullOrEmpty(CCommon.ToString(dr("USPSServerURL"))) _
                                AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dr("USPSUserID"))) _
                                AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dr("USPSPassword"))) Then

                                isDelivered = False
                                trackingDetail = ""

                                Dim track As New nsoftware.InShip.Uspstrack
                                track.USPSAccount.Server = CCommon.ToString(dr("USPSServerURL"))
                                track.USPSAccount.UserId = CCommon.ToString(dr("USPSUserID"))
                                track.USPSAccount.Password = CCommon.ToString(dr("USPSPassword"))
                                track.TrackShipment(CCommon.ToString(dr("vcTrackingNumber")))

                                If track.TrackEvents.Count > 0 Then
                                    trackingDetail = track.TrackEvents(0).Status

                                    If track.TrackEvents(0).Status.ToLower().Contains("delivered") Then
                                        isDelivered = True
                                    End If
                                End If

                                objShippingReport.ShippingReportID = CCommon.ToLong(dr("numShippingReportId"))
                                objShippingReport.IsDelivered = isDelivered
                                objShippingReport.TrackingDetail = trackingDetail
                                objShippingReport.UpdateTrackingDetail()
                            End If
                        End If
                    Catch ex As Exception
                        objShippingReport.ShippingReportID = CCommon.ToLong(dr("numShippingReportId"))
                        objShippingReport.IsDelivered = False
                        objShippingReport.TrackingDetail = ""
                        objShippingReport.UpdateTrackingDetail()
                    End Try
                Next
            End If
        Catch ex As Exception
            WriteMessage("OrderShippingTracking - Error Message: " & ex.Message)
            WriteMessage("OrderShippingTracking - StackTrace: " & ex.StackTrace.ToString)
        End Try
    End Sub
End Class
