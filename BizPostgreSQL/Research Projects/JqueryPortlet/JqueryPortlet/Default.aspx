﻿<%@ Page Title="Home Page" Language="vb" MasterPageFile="~/Site.Master" AutoEventWireup="false"
    CodeBehind="Default.aspx.vb" Inherits="JqueryPortlet._Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        $(function () {
            $(".column").sortable({
                connectWith: '.column'
            });

            $(".portlet").addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
			.find(".portlet-header")
				.addClass("ui-widget-header ui-corner-all")
				.prepend('<span class="ui-icon ui-icon-minusthick"></span>')
				.end()
			.find(".portlet-content");

            $(".portlet-header .ui-icon").click(function () {
                $(this).toggleClass("ui-icon-minusthick").toggleClass("ui-icon-plusthick");
                $(this).parents(".portlet:first").find(".portlet-content").toggle();
            });

            $(".column").disableSelection();
        });
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <style type="text/css">
        .column
        {
            width: 300px;
            float: left;
            padding-bottom: 100px;
        }
        .portlet
        {
            margin: 0 1em 1em 0;
        }
        .portlet-header
        {
            margin: 0.3em;
            padding-bottom: 4px;
            padding-left: 0.2em;
        }
        .portlet-header .ui-icon
        {
            float: right;
        }
        .portlet-content
        {
            padding: 0.4em;
        }
        .ui-sortable-placeholder
        {
            border: 1px dotted black;
            visibility: visible !important;
            height: 50px !important;
        }
        .ui-sortable-placeholder *
        {
            visibility: hidden;
        }
    </style>
    <div class="demo">
        <div class="column">
            <div class="portlet">
                <div class="portlet-header">
                    Open Projects</div>
                <div class="portlet-content">
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit</div>
            </div>
            <div class="portlet">
                <div class="portlet-header">
                    News</div>
                <div class="portlet-content">
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit</div>
            </div>
        </div>
        <div class="column">
            <div class="portlet">
                <div class="portlet-header">
                    Shopping</div>
                <div class="portlet-content">
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit</div>
            </div>
        </div>
        <div class="column">
            <div class="portlet">
                <div class="portlet-header">
                    Links</div>
                <div class="portlet-content">
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit</div>
            </div>
            <div class="portlet">
                <div class="portlet-header">
                    Images</div>
                <div class="portlet-content">
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit</div>
            </div>
        </div>
    </div>
    <!-- End demo -->
    <div class="demo-description">
        <p>
            Enable portlets (styled divs) as sortables and use the <code>connectWith</code>
            option to allow sorting between columns.
        </p>
    </div>
    <!-- End demo-description -->
    <h2>
        Welcome to ASP.NET!
    </h2>
    <p>
        To learn more about ASP.NET visit <a href="http://www.asp.net" title="ASP.NET Website">
            www.asp.net</a>.
    </p>
    <p>
        You can also find <a href="http://go.microsoft.com/fwlink/?LinkID=152368&amp;clcid=0x409"
            title="MSDN ASP.NET Docs">documentation on ASP.NET at MSDN</a>.
    </p>
</asp:Content>
