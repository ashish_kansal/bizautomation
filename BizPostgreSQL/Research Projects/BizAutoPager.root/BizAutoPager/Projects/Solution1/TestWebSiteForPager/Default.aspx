﻿<%@ Page Title="Home Page" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="false"
    CodeFile="Default.aspx.vb" Inherits="_Default" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <title></title>
    <link href="Styles/NewUiStyle.css" rel="stylesheet" type="text/css" />
    <script src="Scripts/jquery.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        $(document).ready(function () {
        });

        function fn_doPage(pgno, currentPageName, btnName) {
            console.log($('#' + currentPageName + ''));
            console.log($('#' + btnName + ''));
            $('#' + currentPageName + '').val(pgno);
            if ($('#' + currentPageName + '').val() != 0 || $('#' + currentPageName + '').val() > 0) {
                document.forms[0].submit();
                $('#' + btnName + '').trigger('click');

                ////$('#btnGo1').trigger('click');
            }
            //$('#hdnPageIndex').val(pgno);
            //if ($('#hdnPageIndex').val() != 0 || $('#hdnPageIndex').val() > 0) {
            //    document.forms[0].submit();
            //    ////$('#btnGo1').trigger('click');
            //}
        }
    </script>
    <asp:Button ID="btnGo1" runat="server"/>
    <webdiyer:AspNetPager ID="AspNetPager1" runat="server" Direction="RightToLeft" HorizontalAlign="Right"
        LayoutType="div" UrlPaging="false" CssClass="pagn" ShowMoreButtons="true" ShowPageIndexBox="Never"
        AlwaysShow="true" AlwaysShowFirstLastPageNumber="true" MoreButtonType="Text"
        ShowCustomInfoSection="Left" CustomInfoHTML="Records %startrecordindex% to %endrecordindex% of %recordcount%" ShowFirstLast="true" ShowPrevNext="true"
         CurrentPageWebControlClientID="hdnPageIndex" PostbackButtonClientID="btnGo1" >
    </webdiyer:AspNetPager>
    <asp:HiddenField ID="hdnPageIndex" runat="server" ClientIDMode="Static" />
    <p>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="numStateID"
            AllowPaging="true" PageSize="5">
            <Columns>
                <asp:BoundField DataField="numStateID" HeaderText="numStateID" InsertVisible="False"
                    ReadOnly="True" SortExpression="numStateID" />
                <asp:BoundField DataField="numCountryID" HeaderText="numCountryID" SortExpression="numCountryID" />
                <asp:BoundField DataField="vcState" HeaderText="vcState" SortExpression="vcState" />
                <asp:BoundField DataField="numCreatedBy" HeaderText="numCreatedBy" SortExpression="numCreatedBy" />
                <asp:BoundField DataField="bintCreatedDate" HeaderText="bintCreatedDate" SortExpression="bintCreatedDate" />
                <asp:BoundField DataField="numModifiedBy" HeaderText="numModifiedBy" SortExpression="numModifiedBy" />
                <asp:BoundField DataField="bintModifiedDate" HeaderText="bintModifiedDate" SortExpression="bintModifiedDate" />
                <asp:BoundField DataField="numDomainID" HeaderText="numDomainID" SortExpression="numDomainID" />
                <asp:CheckBoxField DataField="constFlag" HeaderText="constFlag" SortExpression="constFlag" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TotalBiz2009ConnectionString %>"
            SelectCommand="SELECT TOP 50 * FROM State"></asp:SqlDataSource>
    </p>
</asp:Content>
