﻿
Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            AspNetPager1.RecordCount = 50
            AspNetPager1.PageSize = GridView1.PageSize
        End If

        Dim intPageIndex As Integer = IIf(hdnPageIndex.Value = "" OrElse hdnPageIndex.Value Is Nothing, 0, hdnPageIndex.Value)
        GridView1.PageIndex = IIf(intPageIndex = 0, 0, intPageIndex - 1)
        AspNetPager1.CurrentPageIndex = IIf(hdnPageIndex.Value = "" OrElse hdnPageIndex.Value Is Nothing, 0, hdnPageIndex.Value)

        GridView1.DataSource = SqlDataSource1
        GridView1.DataBind()
    End Sub

    Protected Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click

    End Sub

End Class
