﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Xml.XPath;
namespace BACRMAPIClient.Cularis
{
    public partial class GetOrders : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //ReadXML();
        }

        protected void btnlogin_Click(object sender, EventArgs e)
        {
            string strAPIURL = txtAPIURL.Text.Trim() + "?method=" + btnlogin.Text + "&user=" + txtUserKey.Text.Trim() + "&section=" + txtSectionKey.Text.Trim();
            string url = strAPIURL;
            //Create GET Request
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            string proxy = null;
            req.Method = "GET";
            req.ContentType = "application/x-www-form-urlencoded";
            req.Proxy = new WebProxy(proxy, true); // ignore for local addresses
            req.CookieContainer = new CookieContainer(); // enable cookies

            HttpWebResponse res = (HttpWebResponse)req.GetResponse();

            Stream resst = res.GetResponseStream();
            StreamReader sr = new StreamReader(resst);
            //Read the Resonse
            string response = sr.ReadToEnd();
            //Write Row XML as Output
            txtResponse.Text = response;
            if (response.IndexOf("<token>") > 0)
            {
                int start = response.IndexOf("<token>", StringComparison.OrdinalIgnoreCase);
                int end = response.IndexOf("</token>", StringComparison.OrdinalIgnoreCase);
                //Response.Write(response.Substring(start + 7, 12));
                txtToken.Text = response.Substring(start + 7, 12);
            }
            //Response.Write("<b>Response XML</b>" + HttpUtility.HtmlEncode(response) + "<br>");

            //XmlDocument xDoc = new XmlDocument();
            //xDoc.Load(sr.ReadToEnd());
            //XmlNodeList Token= xDoc.GetElementsByTagName("token");
            //Response.Write(Token[0].InnerText);

            // Serialization
            //API objAPI = new API();
            //objAPI.token = "asdas";
            //objAPI.status = "ok";
            //objAPI.response = "sdas";
            //XmlSerializer s = new XmlSerializer(typeof(API));
            //TextWriter w = new StreamWriter(@"c:\API.xml");
            //s.Serialize(w, objAPI);
            //w.Close();


            //XmlSerializer s = new XmlSerializer(typeof(API));
            //API objLogin;
            //objLogin = (API)s.Deserialize(new StringReader(response));
            //Response.Write("<br><b>Token:</b>" + objLogin.response + "<br>");
            //Response.Write("<b>ReturnUrl:</b>" + objLogin.status + "<br>");
            //Response.Write("<b>IsValid:</b>" + objLogin.token + "<br>");
        }

        protected void btnGetOrders_Click(object sender, EventArgs e)
        {
            string strAPIURL = txtAPIURL.Text.Trim() + "?method=" + btnGetOrders.Text + "&token=" + txtToken.Text.Trim() + "&section=" + txtSectionKey.Text.Trim() + "&date=20090404190400";
            Response.Write(strAPIURL);
            //20080704130000";
            //20090404190400
            string url = strAPIURL;
            //Create GET Request
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            string proxy = null;
            req.Method = "GET";
            req.ContentType = "application/x-www-form-urlencoded";
            req.Proxy = new WebProxy(proxy, true); // ignore for local addresses
            req.CookieContainer = new CookieContainer(); // enable cookies

            HttpWebResponse res = (HttpWebResponse)req.GetResponse();

            Stream resst = res.GetResponseStream();
            StreamReader sr = new StreamReader(resst);
            //Read the Resonse
            string response = sr.ReadToEnd();
            //Write Row XML as Output
            txtResponse.Text = response;
            //WriteToFile(response);
            //DataSet ds = new DataSet();
            //ds.ReadXml(@"c:\API.xml", XmlReadMode.Auto);
            //GridView1.DataSource = ds.Tables["order"];
            //GridView1.DataBind();
            //Response.Write("<b>Response XML</b>" + HttpUtility.HtmlEncode(response) + "<br>");

        }

        protected void btnAddOrder_Click(object sender, EventArgs e)
        {
            //http://www.aecageco.cularis.com/api/rest/?method=orders.add_order&token=H5O727C1s5b4&section=B0U3sxywE4oH&processor=chintan&amount=100.50&number=123456&billing_email=chintan@bizautomation.com&item:abc123=1
            StringBuilder data = new StringBuilder();
            data.Append("&token=" + txtToken.Text.Trim());
            data.Append("&section=" + txtSectionKey.Text.Trim());
            data.Append("&processor=chintan&amount=100.50&number=" + DateTime.Now.Second * DateTime.Now.Millisecond);
            data.Append("&billing_email=chintanprajapati@gmail.com&item:123=5");
            data.Append("&billing_first=billingfirst&billing_last=lastName&billing_phone=27606461&billing_address_1=13,Keval Kunj Soc,ghatlodia 380061&billing_city=ahmedabad&billing_region=region&billing_country=india&billing_postal_code=380061&type=cash&code=OrderCode");
            data.Append("&message=OrderMessage");
            data.Append("&approval=OrderApprovalStatus");
            data.Append("&transaction=TransactionID");
            data.Append("&transaction_type=visa");
            data.Append("&currency=USD");
            data.Append("&tax=12.5");
            data.Append("&shipping=2.5");
            data.Append("&fee=1.0");
            data.Append("&charged=120.0");
            data.Append("&tracking=FEDEX065");
            data.Append("&status=new");//"new", "flag", "shipped", "process", or "archive".
            string strAPIURL = txtAPIURL.Text.Trim() + "?method=" + btnAddOrder.Text + data.ToString();

            Uri address = new Uri(strAPIURL);

            // Create the web request
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(address);

            // Set type to POST
            request.Method = "POST";
            //request.ContentType = "application/x-www-form-urlencoded";
            request.ContentType = "text/xml";
            // Create the data we want to send



            // Create a byte array of the data we want to send
            byte[] byteData = UTF8Encoding.UTF8.GetBytes(data.ToString());

            // Set the content length in the request headers
            request.ContentLength = byteData.Length;

            // Write data
            using (Stream postStream = request.GetRequestStream())
            {
                postStream.Write(byteData, 0, byteData.Length);
                postStream.Close();
            }

            // Get response
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                // Get the response stream
                StreamReader reader = new StreamReader(response.GetResponseStream());

                txtResponse.Text = reader.ReadToEnd();
            }

        }

        protected void btnGetUsers_Click(object sender, EventArgs e)
        {
            string strAPIURL = txtAPIURL.Text.Trim() + "?method=" + btnGetUsers.Text + "&token=" + txtToken.Text.Trim() + "&section=" + txtSectionKey.Text.Trim() + "&group=1";
            string url = strAPIURL;
            //Create GET Request
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            string proxy = null;
            req.Method = "GET";
            req.ContentType = "application/x-www-form-urlencoded";
            req.Proxy = new WebProxy(proxy, true); // ignore for local addresses
            req.CookieContainer = new CookieContainer(); // enable cookies

            HttpWebResponse res = (HttpWebResponse)req.GetResponse();

            Stream resst = res.GetResponseStream();
            StreamReader sr = new StreamReader(resst);
            //Read the Resonse
            string response = sr.ReadToEnd();
            //Write Row XML as Output
            txtResponse.Text = response;
            DataSet dst = new DataSet();


            //GridView1.DataSource= 
        }

        protected void btnGetProducts_Click(object sender, EventArgs e)
        {
            string strAPIURL = txtAPIURL.Text.Trim() + "?method=" + btnGetProducts.Text + "&token=" + txtToken.Text.Trim() + "&section=" + txtSectionKey.Text.Trim();
            string url = strAPIURL;
            //Create GET Request
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            string proxy = null;
            req.Method = "GET";
            req.ContentType = "application/x-www-form-urlencoded";
            req.Proxy = new WebProxy(proxy, true); // ignore for local addresses
            req.CookieContainer = new CookieContainer(); // enable cookies

            HttpWebResponse res = (HttpWebResponse)req.GetResponse();

            Stream resst = res.GetResponseStream();
            StreamReader sr = new StreamReader(resst);
            //Read the Resonse
            string response = sr.ReadToEnd();
            //Write Row XML as Output
            txtResponse.Text = response;
            DataSet dst = new DataSet();
            WriteToFile(response);
            DataSet ds = new DataSet();
            ds.ReadXml(@"c:\API.xml", XmlReadMode.Auto);
            GridView1.DataSource = ds.Tables["Product"];
            GridView1.DataBind();
        }
        private void WriteToFile(string strData)
        {
            // Specify file, instructions, and privelegdes
            FileStream file = new FileStream(@"c:\API.xml", FileMode.OpenOrCreate, FileAccess.Write);

            // Create a new stream to write to the file
            StreamWriter sw = new StreamWriter(file);

            // Write a string to the file
            sw.Write(strData);

            // Close StreamWriter
            sw.Close();

            // Close file
            file.Close();
        }

        private void ReadXML()
        {
            StreamReader re = File.OpenText(@"D:/Documents and Settings/Chintan/Desktop/api.xml");
            string input = null;
            string strXML = string.Empty;
            while ((input = re.ReadLine()) != null)
            {
                strXML += input;
            }
            re.Close();

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(strXML);
            XmlNodeList elemList = doc.GetElementsByTagName("order");
            string output = string.Empty;
            for (int i = 0; i < elemList.Count; i++)
            {
                output += elemList[i].InnerXml;
                Response.Write("<br>" + elemList[i].Attributes["id"].InnerXml + "<br>");
                XmlNode xn = elemList[i];
                //for (int j = 0; j < xn.ChildNodes.Count; j++)
                //{
                Response.Write(xn["number"].InnerXml + "<br>");
                Response.Write(xn["approval"].InnerXml + "<br>");
                txtResponse.Text = xn["billing"].InnerXml;
                Response.Write(xn["billing"].Attributes["id"].InnerXml);

                //}
            }
            //txtResponse.Text = output;
        }
    }
    public class API
    {
        public string response { get; set; }
        public string status { get; set; }
        public string token { get; set; }
    }
}
