﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GetOrders.aspx.cs" Inherits="BACRMAPIClient.Cularis.GetOrders"
    ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <a href="http://www.aecageco.cularis.com/api/rest/?method=auth.login&amp;user=xGrlz5gLzN8M&amp;section=B0U3sxywE4oH">
            http://www.aecageco.cularis.com/api/rest/?method=auth.login&amp;user=xGrlz5gLzN8M&amp;section=B0U3sxywE4oH</a><br />
        <br />
        <a href="http://www.aecageco.cularis.com/api/rest/?method=orders.get_orders&amp;section=B0U3sxywE4oH&amp;token=51Yk3bvbK85R">
            http://www.aecageco.cularis.com/api/rest/?method=orders.get_orders&amp;section=B0U3sxywE4oH&amp;token=51Yk3bvbK85R</a><br />
        API URL<br />
        <asp:TextBox ID="txtAPIURL" runat="server" Text="http://www.aecageco.cularis.com/api/rest/"
            Width="264px">http://www.aecageco.cularis.com/api/rest/</asp:TextBox>
        <br />
        User Key<br />
        <asp:TextBox ID="txtUserKey" runat="server">xGrlz5gLzN8M</asp:TextBox>
        <br />
        Section Key<br />
        <asp:TextBox ID="txtSectionKey" runat="server" Text="B0U3sxywE4oH">B0U3sxywE4oH</asp:TextBox>
    </div>
    <asp:Button ID="btnlogin" runat="server" OnClick="btnlogin_Click" Text="auth.login" />
    &nbsp;Token<asp:TextBox ID="txtToken" runat="server"></asp:TextBox>
    <p>
        <asp:Button ID="btnGetOrders" runat="server" OnClick="btnGetOrders_Click" Text="orders.get_orders" />
        <asp:Button ID="btnAddOrder" runat="server" OnClick="btnAddOrder_Click" Text="orders.add_order" />
        <asp:Button ID="btnGetUsers" runat="server" OnClick="btnGetUsers_Click" Text="users.get_users" />
        <asp:Button ID="btnGetProducts" runat="server" Text="products.get_products" 
            onclick="btnGetProducts_Click" />
    </p>
    <p>
        Response from Server</p>
    <asp:TextBox ID="txtResponse" runat="server" Height="122px" TextMode="MultiLine"
        Width="572px"></asp:TextBox>
    <asp:GridView ID="GridView1" runat="server">
    </asp:GridView>
    </form>
</body>
</html>
