﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Configuration;

namespace BACRMAPIClient
{
    public class BizLogin
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool IsValid { get; set; }
        public string Token { get; set; }
        public string ReturnURL { get; set; }
        public string CurrentDate { get; set; }
        public string DomainID { get; set; }
    }
    public class error
    {
        public string msg;
        public string request;
    }
    public partial class LoginClassic : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            //Call The URL Where Service is located with appropriate parameter
            string url = ConfigurationManager.AppSettings.Get("SiteURL") + "/BizLogin.svc/Login?user=" + TextBox2.Text + "&pass=" + TextBox3.Text;
            //Create GET Request
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            string proxy = null;
            req.Method = "GET";
            req.ContentType = "application/x-www-form-urlencoded";
            req.Proxy = new WebProxy(proxy, true); // ignore for local addresses
            req.CookieContainer = new CookieContainer(); // enable cookies

            HttpWebResponse res = (HttpWebResponse)req.GetResponse();

            Stream resst = res.GetResponseStream();
            StreamReader sr = new StreamReader(resst);
            //Read the Resonse
            string response = sr.ReadToEnd();
            //Write Row XML as Output
            Response.Write("<b>Response XML</b>" +HttpUtility.HtmlEncode(response) + "<br>");

            try
            {
                //try to Deserialize Response
                XmlSerializer s = new XmlSerializer(typeof(BizLogin));
                BizLogin objLogin;
                objLogin = (BizLogin)s.Deserialize(new StringReader(response));
                Response.Write("<br><b>Token:</b>" + objLogin.Token + "<br>");
                Response.Write("<b>ReturnUrl:</b>" + objLogin.ReturnURL + "<br>");
                Response.Write("<b>IsValid:</b>" + objLogin.IsValid + "<br>");
            }
            catch (Exception)
            {
                // Deserializa Error Message
                XmlSerializer s = new XmlSerializer(typeof(error));
                error objerror;
                objerror = (error)s.Deserialize(new StringReader(response));
                Response.Write("<br><b>Message:</b>" + objerror.msg + "<br>");
                Response.Write("<br><b>Requested URL:</b>" + objerror.request + "<br>");
            }
        }
    }
}
