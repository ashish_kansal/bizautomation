﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using Microsoft.ServiceModel.Web;
using System.ServiceModel.Web;

namespace BACRMClient
{
    [ServiceContract]
    public interface IBizClient
    {
        [WebHelp(Comment = "Returns the Token if authenticated, which will be used for subsequent requests")]
        [WebGet(UriTemplate = "Login?user={UserName}&pass={Password}")]
        [OperationContract]
        BizLogin AuthenticateUser(string UserName, string Password);


    }
    public class BizClient : ClientBase<IBizClient>, IBizClient
    {

        public BizLogin AuthenticateUser(string UserName, string Password)
        {
            return this.Channel.AuthenticateUser(UserName,Password);
        }

    }

}
