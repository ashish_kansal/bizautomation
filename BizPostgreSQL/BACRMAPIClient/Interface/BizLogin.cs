﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using Microsoft.ServiceModel.Web;
using System.Net;
using BACRM.BusinessLogic.Common;
using System.Data;
using System.Web.SessionState;
namespace BACRMClient
{
    [DataContract(Namespace = "")]
    [Serializable]
    public class BizLogin
    {
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public bool IsValid { get; set; }
        [DataMember]
        public string Token { get; set; }
        [DataMember]
        public string ReturnURL { get; set; }
        [DataMember]
        public string CurrentDate { get; set; }

    }
    //public enum FaultCode
    //{
    //    [EnumMember]
    //    ERROR,
    //    [EnumMember]
    //    INCORRECT_PARAMETER
    //}
    [DataContract]
    public class error
    {

        //[DataMember]
        //public FaultCode errorcode;
        [DataMember]
        public string msg;
        [DataMember]
        public string request;

    }

}
