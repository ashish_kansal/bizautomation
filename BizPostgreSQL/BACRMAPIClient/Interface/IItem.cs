﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.ServiceModel.Web;
using System.ServiceModel.Web;
using System.ServiceModel;
using BACRM.BusinessLogic.Item;

namespace BACRMClient
{
    [ServiceContract]
    public interface IItem
    {
        [WebHelp(Comment = "Adds new Item to Inventory,and returns Created Item ID")]
        [WebInvoke(RequestFormat = WebMessageFormat.Xml,
            ResponseFormat = WebMessageFormat.Xml,
            Method = "POST",
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "AddItem?Token={Token}")]
        [FaultContract(typeof(error))]
        [OperationContract]
        string AddItem(CItems objItem, string Token);

        [WebHelp(Comment = "Returns a Item when ItemCode is found")]
        [WebGet(UriTemplate = "Items?ItemCode={ItemCode}&Token={Token}")]
        [FaultContract(typeof(error))]
        [OperationContract]
        CItems GetItem(int ItemCode, string Token);

        [WebHelp(Comment = "Get All Items Inventory,Returns Data as a string")]
        [WebInvoke(RequestFormat = WebMessageFormat.Xml,
            ResponseFormat = WebMessageFormat.Xml,
            Method = "POST",
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "GetItems?Token={Token}")]
        [FaultContract(typeof(error))]
        [OperationContract]
        string GetItems(CItems objItems, string Token);

        [WebHelp(Comment = "Delete item from inventory")]
        [WebGet(UriTemplate = "DeleteItem?ItemCode={ItemCode}&Token={Token}")]
        [FaultContract(typeof(FaultException))]
        [OperationContract]
        bool DeleteItem(int ItemCode, string Token);

        [WebInvoke(RequestFormat = WebMessageFormat.Xml,
            ResponseFormat = WebMessageFormat.Xml,
            Method = "POST",
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "CreateOpportunity?Token={Token}&Source={OpportunitySource}")]
        [OperationContract]
        [FaultContract(typeof(error))]
        CItems CreateOpportunity(string Token, CItems objItem, string OpportunitySource);


        [WebHelp(Comment = "Get Item recommended price based on old orders,returns amount")]
        [WebInvoke(RequestFormat = WebMessageFormat.Xml,
            ResponseFormat = WebMessageFormat.Xml,
            Method = "POST",
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "ItemPriceRec?Token={Token}")]
        [FaultContract(typeof(error))]
        [OperationContract]
        string GetItemPriceRec(CItems objItem, string Token);

        [WebHelp(Comment = "Get Item Status from Inventory")]
        [WebGet(UriTemplate = "ItemStatus?ItemCode={ItemCode}&WarehouseID={WarehouseID}&DomainID={DomainID}&Token={Token}")]
        [FaultContract(typeof(error))]
        [OperationContract]
        string GetInventoryStatus(int ItemCode, long WarehouseID, long DomainID, string Token);
    }
    public class ItemClient : ClientBase<IItem>, IItem
    {
        public CItems GetItem(int ItemCode, string Token)
        {
            return this.Channel.GetItem(ItemCode, Token);
        }

        public bool DeleteItem(int ItemCode, string Token)
        {
            return this.Channel.DeleteItem(ItemCode, Token);
        }

        public CItems CreateOpportunity(string Token, CItems objItem, string OpportunitySource)
        {
            return this.Channel.CreateOpportunity(Token,objItem,OpportunitySource);
        }

        public string GetItemPriceRec(CItems objItem, string Token)
        {
            return this.Channel.GetItemPriceRec(objItem,Token);
        }

        public string GetInventoryStatus(int ItemCode, long WarehouseID, long DomainID, string Token)
        {
            return this.Channel.GetInventoryStatus(ItemCode,WarehouseID,DomainID,Token);
        }

        public string AddItem(CItems objItem, string Token)
        {
            return this.Channel.AddItem(objItem,Token);
        }

        public string GetItems(CItems objItems, string Token)
        {
            return this.Channel.GetItems(objItems, Token);
        }

    }
}
