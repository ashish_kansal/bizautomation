﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BACRM.BusinessLogic.Leads;
using Microsoft.ServiceModel.Web;
using System.ServiceModel.Web;

namespace BACRMClient
{
    // NOTE: If you change the interface name "IRelationShip" here, you must also update the reference to "IRelationShip" in Web.config.
    [ServiceContract]
    public interface IRelationShipClient
    {
        [WebHelp(Comment = "Adds the Lead")]
        [WebInvoke(RequestFormat = WebMessageFormat.Xml,
            ResponseFormat = WebMessageFormat.Xml,
            Method = "POST",
            BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "AddRelationship?Token={Token}")]
        [OperationContract]
        [FaultContract(typeof(error))]
        CLeads AddLead(string Token, CLeads objLead);
    }
    public class RelationShipClient : ClientBase<IRelationShipClient>, IRelationShipClient
    {

        public CLeads AddLead(string Token, CLeads objLead)
        {
            return this.Channel.AddLead(Token, objLead);
        }

    }
}
