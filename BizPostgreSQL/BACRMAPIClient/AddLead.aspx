﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddLead.aspx.cs" Inherits="BACRMClient.AddLead" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <fieldset>
        <legend>Add Relationship</legend>
        <asp:RadioButtonList ID="radRelationship" runat="server" RepeatDirection="Vertical">
            <asp:ListItem Text="Lead" Value="0" Selected="True"></asp:ListItem>
            <asp:ListItem Text="Prospect" Value="1"></asp:ListItem>
            <asp:ListItem Text="Account" Value="2"></asp:ListItem>
            <asp:ListItem Text="Franchises" Value="1"></asp:ListItem>
        </asp:RadioButtonList>
        FirstName<asp:TextBox ID="txtFirstName" runat="server" Text="chintan"></asp:TextBox><br />
        LastName<asp:TextBox ID="txtLastName" runat="server" Text="Prajapati"></asp:TextBox><br />
        CompanyName<asp:TextBox ID="txtCompName" runat="server" Text="Biz"></asp:TextBox><br />
        RelationShip(CompanyType)
        <asp:DropDownList ID="ddlRelationShip" runat="server">
            <asp:ListItem Text="Customer" Value="46" Selected="True"> </asp:ListItem>
            <asp:ListItem Text="Franchises" Value="3969"> </asp:ListItem>
        </asp:DropDownList>
        <br />
        Division<asp:TextBox ID="txtDivisionID" runat="server" Text="0"></asp:TextBox><br />
        Token<asp:TextBox ID="txtToken" runat="server" Text="MJps5EoBJ11me8SVlZi39GFOVv%2bWh7ux7dSSsEFi8aL6%2bymmCfysj%2bSJwzk%2fDafipQnj7FYvPEbOLi%2ffkOHBFg%3d%3d"></asp:TextBox>
        <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="Add lead" />
    </fieldset>
    </form>
</body>
</html>
