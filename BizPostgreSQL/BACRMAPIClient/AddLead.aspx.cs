﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BACRM.BusinessLogic.Leads;
using System.ServiceModel;
namespace BACRMClient
{
    public partial class AddLead : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {

            try
            {
                if (txtToken.Text.Length > 0)
                {
                    RelationShipClient client = new RelationShipClient();

                    CLeads objLead = new CLeads();

                    //Mandatory Parameters
                    objLead.CompanyName = txtCompName.Text;
                    objLead.FirstName = txtFirstName.Text;
                    objLead.LastName = txtLastName.Text;
                    objLead.DomainID = 1;
                    objLead.UserCntID = 1;
                    objLead.DivisionID = int.Parse(txtDivisionID.Text);
                    objLead.CRMType = int.Parse(radRelationship.SelectedValue);
                    //***************Lead Specific parameter mandatory for creating lead************
                    objLead.GroupID = 5; //MyLead Or Public Lead or Web Lead


                    objLead.ContactType = 70;
                    objLead.CompanyType = long.Parse(ddlRelationShip.SelectedValue);
                    //Decision Maker= 68
                    //Primary Contact= 70
                    //Employee= 92
                    
                    

                    //Common Parameters
                    //objLead.How = 227;
                    //objLead.TerritoryID = 979;
                    //objLead.CustName = "test";
                    

                    //CRMType = 0 ->Lead
                    //CRMType = 1 ->Prospect or Franchisees 
                    //CRMType = 2 ->Account
                    

                    //objLead.DivisionName = "-";
                    //objLead.LeadBoxFlg = 1;
                    //objLead.UserCntID = 1;
                    //objLead.Country = "600";
                    //objLead.SCountry = 600;

                    //objLead.Phone = "27606461";
                    //objLead.PhoneExt = "079";
                    //objLead.ComPhone = "27606461";
                    //objLead.Email = "chintanprajapati123@gmail.com";
                    //objLead.DomainID = 78;
                    //objLead.ContactType = 70;
                    //objLead.Profile = 670;
                    //objLead.WebSite = "www.zoomasp.net";
                    //objLead.Comments = "this is lead";
                    //objLead.CompanyID = 0;



                    objLead = client.AddLead(txtToken.Text.Trim(), objLead);
                    client.Close();
                    Response.Write("New Company ID=" + objLead.CompanyID + "Division ID=" + objLead.DivisionID + "Contact ID =" + objLead.ContactID);

                }

            }
            catch (FaultException<error> faultEx)
            {

                Response.Write(faultEx.Detail.msg);

                Response.Write(faultEx.Detail.request);
            }
            catch (FaultException ex)
            {
                Response.Write(ex.Reason);
            }

            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }




    }
}
