﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Net;
using System.IO;
using System.Xml.Serialization;
using BACRM.BusinessLogic.Item;
using System.Text;
namespace BACRMAPIClient
{
    public partial class Items : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnGet_Click(object sender, EventArgs e)
        {
            string url = ConfigurationManager.AppSettings.Get("SiteURL") + "/Item.svc/Items?ItemCode=" + txtItemCode.Text + "&Token=" + txtToken.Text;
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            string proxy = null;
            req.Method = "GET";
            req.ContentType = "application/x-www-form-urlencoded";
            req.Proxy = new WebProxy(proxy, true); // ignore for local addresses
            req.CookieContainer = new CookieContainer(); // enable cookies

            HttpWebResponse res = (HttpWebResponse)req.GetResponse();

            Stream resst = res.GetResponseStream();
            StreamReader sr = new StreamReader(resst);
            string response = sr.ReadToEnd();
            txtXml.Text = response;
            //Response.Write("<b>XML</b>" + HttpUtility.HtmlEncode(response) + "<br>");
            // Deserialization
            //XmlSerializer s = new XmlSerializer(typeof(BizLogin));
            //BizLogin objLogin;
            //objLogin = (BizLogin)s.Deserialize(new StringReader(response));

            //Response.Write("<br><b>Token:</b>" + objLogin.Token + "<br>");
            //Response.Write("<b>ReturnUrl:</b>" + objLogin.ReturnURL + "<br>");
            //Response.Write("<b>IsValid:</b>" + objLogin.IsValid + "<br>");
        }

        protected void btnPriceRec_Click(object sender, EventArgs e)
        {
            CItems objItem = new CItems();
            objItem.ItemCode = int.Parse(txtItemCode.Text);
            objItem.NoofUnits = long.Parse(txtUnits.Text);
            //objItem.OppId = 0;
            //objItem.DivisionID = 0;
            objItem.DomainID = 1;
            objItem.byteMode = 1;
            //objItem.Amount = 0;
            //objItem.WareHouseItemID = 0;

            BACRMClient.ItemClient client = new BACRMClient.ItemClient();
            string Amount = client.GetItemPriceRec(objItem, txtToken.Text);
            txtXml.Text = Amount;
            //UsingPostRequest();
        }

        private void UsingPostRequest()
        {
            CItems objItem = new CItems();
            objItem.ItemCode = int.Parse(txtItemCode.Text);
            objItem.NoofUnits = long.Parse(txtUnits.Text);
            objItem.OppId = 0;
            objItem.DivisionID = 0;
            objItem.DomainID = 1;
            objItem.byteMode = 1;
            objItem.Amount = 0;
            objItem.WareHouseItemID = 0;
            Common objCommon = new Common();
            String ItemData = objCommon.SerializeObject(objItem, typeof(CItems));


            string url = ConfigurationManager.AppSettings.Get("SiteURL") + "/Item.svc/ItemPriceRec?Token=" + txtToken.Text;
            Uri address = new Uri(url);

            // Create the web request
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(address);

            // Set type to POST
            request.Method = "POST";
            //request.ContentType = "application/x-www-form-urlencoded";
            request.ContentType = "text/xml";
            // Create the data we want to send


            StringBuilder data = new StringBuilder();
            data.Append("Token=" + HttpUtility.UrlEncode(txtToken.Text));
            //data.Append("&context=" + HttpUtility.UrlEncode(context));
            //data.Append("&query=" + HttpUtility.UrlEncode(query));
            //data.Append("objItem=" + HttpUtility.UrlEncode(ItemData));

            // Create a byte array of the data we want to send
            byte[] byteData = UTF8Encoding.UTF8.GetBytes(data.ToString());

            // Set the content length in the request headers
            request.ContentLength = byteData.Length;

            // Write data
            using (Stream postStream = request.GetRequestStream())
            {
                postStream.Write(byteData, 0, byteData.Length);
                postStream.Close();
            }

            // Get response
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                // Get the response stream
                StreamReader reader = new StreamReader(response.GetResponseStream());

                Response.Write(reader.ReadToEnd());
            }
        }

        protected void btnInventoryStatus_Click(object sender, EventArgs e)
        {
            BACRMClient.ItemClient client = new BACRMClient.ItemClient();
            string strInventoryStatus = client.GetInventoryStatus(int.Parse(txtItemCode.Text), 0, 1, txtToken.Text);
            txtXml.Text = strInventoryStatus;
        }

        protected void btnAddItem_Click(object sender, EventArgs e)
        {
            try
            {
                BACRMClient.ItemClient client = new BACRMClient.ItemClient();
                CItems objItem = new CItems();
                //required parameter
                objItem.ItemCode = txtItemCode1.Text == "" ? 0 : int.Parse(txtItemCode1.Text);
                objItem.DomainID = 1;
                objItem.UserCntId = 1;
                objItem.ItemName = txtItemName.Text;
                objItem.ItemType = ddlItemType.SelectedValue;
                objItem.IncomeChartAcntId = int.Parse(txtIncomeAccount.Text);
                objItem.AssetChartAcntId = int.Parse(txtAssetAccount.Text);
                objItem.COGSChartAcntId = int.Parse(txtCOGs.Text);
                //Extra parameter
                objItem.ItemDesc = "";
                objItem.ListPrice = 0;
                objItem.ItemClassification = 0;//917
                objItem.ModelID = "ZX";
                objItem.KitParent = false;
                objItem.bitSerialized = false;
                objItem.Taxable = false;
                objItem.SKU = "";
                objItem.ItemGroupID = 0;
                objItem.AverageCost = 0;
                objItem.LabourCost = 0;
                //objItem.DateEntered = default(DateTime);//DateTime.Parse("02-12-2009");
                objItem.VendorID = 0;

                objItem.Weight = 0;
                objItem.Height = 0;
                objItem.Length = 0;
                objItem.Width = 0;
                objItem.FreeShipping = false;
                objItem.AllowBackOrder = true;
                objItem.UnitofMeasure = "U";//Units=U Check for others

                //objItem.strChildItems = ds.GetXml();
                objItem.ShowDeptItem = false;
                objItem.ShowDeptItemDesc = false;
                objItem.CalPriceBasedOnIndItems = false;
                objItem.Assembly = false;

                string strItemCode = client.AddItem(objItem, txtToken.Text);
                txtItemCode1.Text = strItemCode;
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected void btnGetItems_Click(object sender, EventArgs e)
        {
            BACRMClient.ItemClient client = new BACRMClient.ItemClient();
            CItems objItems = new CItems();
            objItems.UserCntId = 1;
            objItems.PageSize = txtPageSize.Text;
            objItems.ItemType = "P";
            objItems.TotalRecords = 0;
            objItems.ItemGroupID = 0;
            objItems.ItemClassification = 0; //ddlFilter.SelectedItem.Value;
            objItems.CurrentPage = txtCurrentpage.Text;
            txtXml.Text = client.GetItems(objItems, txtToken.Text);

            //if (!string.IsNullOrEmpty(ddlSearch.SelectedItem.Value) & !string.IsNullOrEmpty(txtSearch.Text))
            //{
            //    objItems.KeyWord = ddlSearch.SelectedItem.Value + " like '%" + txtSearch.Text.Trim + "%'";
            //}
            //else
            //{
            //    objItems.KeyWord = "";
            //}


            ////if (Page == "Services")
            //objItems.ItemType = "S";
            ////else if (Page == "Kits")
            //objItems.KitParent = true;
            ////else if (Page == "Inventory Items")
            //objItems.ItemType = "P";
            ////else if (Page == "Non-Inventory Items")
            //objItems.ItemType = "N";
            ////else if (Page == "Serialized Items")
            //objItems.bitSerialized = true;
            ////else if (Page == "Assembly")
            //{
            //objItems.Assembly = true;
            //objItems.KitParent = true;
            //}


        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            BACRMClient.ItemClient client = new BACRMClient.ItemClient();
            client.DeleteItem(int.Parse(txtItemCode1.Text), txtToken.Text);
        }
    }
}
