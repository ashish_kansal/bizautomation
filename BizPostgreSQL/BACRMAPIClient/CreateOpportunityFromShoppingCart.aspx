﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateOpportunityFromShoppingCart.aspx.cs"
    Inherits="BACRMClient.CreateOpportunityFromShoppingCart" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <br />
        DomainID<asp:TextBox ID="txtDomainID" runat="server">1</asp:TextBox>
        <br />
        CartItems:
        <asp:TextBox ID="txtCartItems" runat="server" TextMode="MultiLine" 
            Text="<NewDataSet><Item><numoppitemtCode>1</numoppitemtCode><numItemCode>14</numItemCode><numUnitHour>1</numUnitHour><monPrice>10.00</monPrice><monTotAmount>10.00</monTotAmount><vcItemDesc /><numWarehouseID /><vcItemName>Shirt</vcItemName><ItemType /><Weight /><Op_Flag>1</Op_Flag></Item><Item><numoppitemtCode>2</numoppitemtCode><numItemCode>99</numItemCode><numUnitHour>1</numUnitHour><monPrice>525</monPrice><monTotAmount>525</monTotAmount><vcItemDesc /><numWarehouseID >185</numWarehouseID><vcItemName>Unisez</vcItemName><ItemType /><Weight /><Op_Flag>1</Op_Flag></Item></NewDataSet>" 
            Height="264px" Width="321px"></asp:TextBox>
        <br />
        <asp:Button ID="btnsave" runat="server" OnClick="btnsave_Click" Text="Create Opp" />
        <br />
    </div>
    </form>
</body>
</html>
