﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Items.aspx.cs" Inherits="BACRMAPIClient.Items"
    ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        Token<asp:TextBox ID="txtToken" runat="server" Text="F320E217-9131-4C49-9EAF-F907A291FD14"></asp:TextBox>
        <br />
        <fieldset>
            <legend>Get Item</legend>ItemCode:
            <asp:TextBox ID="txtItemCode" runat="server" Width="129px" Text="77"></asp:TextBox>
            <br />
            <asp:Button ID="btnGet" runat="server" Text="Get Item" OnClick="btnGet_Click" />
            <asp:Button ID="btnInventoryStatus" runat="server" OnClick="btnInventoryStatus_Click"
                Text="Get Inventory Status" />
            &nbsp;<asp:Button ID="btnGetItems" runat="server" onclick="btnGetItems_Click" 
                Text="Get Items" />
            from<asp:TextBox ID="txtCurrentpage" runat="server" Width="33px">1</asp:TextBox>
            st page with page Size<asp:TextBox ID="txtPageSize" runat="server" Width="34px">20</asp:TextBox>
&nbsp;Rows<br />
            Units:<asp:TextBox ID="txtUnits" runat="server" Width="129px" Text="1"></asp:TextBox>
            <asp:Button ID="btnPriceRec" runat="server" OnClick="btnPriceRec_Click" Text="Get Recommended Price" />
            <br />
            <asp:TextBox ID="txtXml" TextMode="MultiLine" runat="server" Height="338px" Width="506px"></asp:TextBox>
        </fieldset>
        <fieldset>
            <legend>Add Item</legend>ItemCode:<asp:TextBox ID="txtItemCode1" runat="server"></asp:TextBox>
            <asp:Button
                ID="btnDelete" runat="server" Text="Delete Item" 
                onclick="btnDelete_Click" />
            <br />
            ItemName<asp:TextBox ID="txtItemName" runat="server"></asp:TextBox>
            <br />
            ItemType<asp:DropDownList ID="ddlItemType" runat="server">
                <asp:ListItem Selected="True" Value="0">--Select One--</asp:ListItem>
                <asp:ListItem Value="P">Inventory</asp:ListItem>
                <asp:ListItem Value="N">Non-Inventory</asp:ListItem>
                <asp:ListItem Value="S">Service</asp:ListItem>
            </asp:DropDownList>
            <br />
            Income Account
            <asp:TextBox ID="txtIncomeAccount" runat="server" Text="1798"></asp:TextBox>
            <br />
            Asset Account<asp:TextBox ID="txtAssetAccount" runat="server" Text="1798"></asp:TextBox>
            <br />
            COGS<asp:TextBox ID="txtCOGs" runat="server" Text="1798"></asp:TextBox>
            <asp:Button ID="btnAddItem" runat="server" OnClick="btnAddItem_Click" Text="Add | Update" />
            
            <br />
                        <asp:TextBox Visible="false" ID="txtChildItems" runat=server TextMode=MultiLine Text="<NewDataSet>
  <WareHouse>
    <numWareHouseItemID>116</numWareHouseItemID>
    <vcWarehouse>West WareHouse</vcWarehouse>
    <numWareHouseID>28</numWareHouseID>
    <OnHand>100</OnHand>
    <Reorder>0</Reorder>
    <OnOrder>0</OnOrder>
    <Allocation>0</Allocation>
    <BackOrder>0</BackOrder>
    <Op_Flag>0</Op_Flag>
    <Location />
    <Price>0.0000</Price>
  </WareHouse>
  <WareHouse>
    <numWareHouseItemID>122</numWareHouseItemID>
    <vcWarehouse>East Warehouse</vcWarehouse>
    <numWareHouseID>24</numWareHouseID>
    <OnHand>101</OnHand>
    <Reorder>0</Reorder>
    <OnOrder>0</OnOrder>
    <Allocation>0</Allocation>
    <BackOrder>0</BackOrder>
    <Op_Flag>0</Op_Flag>
    <Location />
    <Price>0.0000</Price>
  </WareHouse>
  <WareHouse>
    <numWareHouseItemID>206</numWareHouseItemID>
    <numWareHouseID>30</numWareHouseID>
    <OnHand>10</OnHand>
    <Reorder>0</Reorder>
    <Op_Flag>2</Op_Flag>
    <Location>0</Location>
  </WareHouse>
  <SerializedItems>
    <numWareHouseItmsDTLID>455</numWareHouseItmsDTLID>
    <numWareHouseItemID>116</numWareHouseItemID>
    <vcSerialNo>1.0</vcSerialNo>
    <Op_Flag>0</Op_Flag>
    <Comments>Test</Comments>
    <Color>3738</Color>
    <Test>3754</Test>
  </SerializedItems>
  <SerializedItems>
    <numWareHouseItmsDTLID>456</numWareHouseItmsDTLID>
    <numWareHouseItemID>116</numWareHouseItemID>
    <vcSerialNo>2</vcSerialNo>
    <Op_Flag>0</Op_Flag>
    <Color>3739</Color>
    <Test>3755</Test>
  </SerializedItems>
  <Table2>
    <Fld_label>Color</Fld_label>
    <fld_id>13</fld_id>
    <fld_type>Drop Down List Box</fld_type>
    <numlistid>80</numlistid>
    <vcURL />
  </Table2>
  <Table2>
    <Fld_label>Test</Fld_label>
    <fld_id>19</fld_id>
    <fld_type>Drop Down List Box</fld_type>
    <numlistid>84</numlistid>
    <vcURL />
  </Table2>
  <Table1>
    <ItemKitID>43</ItemKitID>
    <ChildItemID>45</ChildItemID>
    <QtyItemsReq>120</QtyItemsReq>
    <numWarehouseItmsID>118</numWarehouseItmsID>
  </Table1>
</NewDataSet>" Height="164px" Width="577px"></asp:TextBox>
        </fieldset>
    </div>
    </form>
</body>
</html>
