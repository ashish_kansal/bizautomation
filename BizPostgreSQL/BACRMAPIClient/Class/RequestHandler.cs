using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Net;
using System.Text;
using System.IO;

namespace XMLIntegration
{
	/// <summary>
	/// Request Handler helps to send and receive SOAP requests
	/// </summary>
	public class RequestHandler
	{
		#region Hidden Memebrs
		private string _OperationURLKeyInWebConfig="EuropCarURL";
		#endregion

		#region Memebers & Properties
		private string _Uri;
		/// <summary>
		/// Get/Set URI for request
		/// </summary>
		public string Uri
		{
			set{_Uri = value;}
			get{ return _Uri;}
		}

		private string _SoapEnvelopeStart;
		/// <summary>
		/// Get/Set Soap Envelop starting string
		/// </summary>
		public string SoapEnvelopeStart
		{
			set{_SoapEnvelopeStart = value;}
			get{ return _SoapEnvelopeStart;}
		}

		private string _SoapEnvelopeEnd;
		/// <summary>
		/// Get/Set Soap Envelop ending with string
		/// </summary>
		public string SoapEnvelopeEnd
		{
			set{_SoapEnvelopeEnd = value;}
			get{ return _SoapEnvelopeEnd;}
		}

		private string _Method;
		/// <summary>
		/// Get/Set Request Method GET/POST, default : POST
		/// </summary>
		public string Method
		{
			set{_Method = value;}
			get{ return _Method;}
		}

		private string _UserAgent;
		/// <summary>
		/// Get/Set user agent for request, default : RPT-HTTPClient/0.3-3
		/// </summary>
		public string UserAgent
		{
			set{_UserAgent = value;}
			get{ return _UserAgent;}
		}

		private string _SoapAction;
		/// <summary>
		/// Get/Set SOAP Action
		/// </summary>
		public string SoapAction
		{
			set{_SoapAction = value;}
			get{ return _SoapAction;}
		}

		private string _ContentType;
		/// <summary>
		/// Get/Set type of content, default : text/xml; charset=utf-8
		/// </summary>
		public string ContentType
		{
			set{_ContentType = value;}
			get{ return _ContentType;}
		}

		private bool _UseWebLogicSessionParameterInRequest;
		/// <summary>
		/// Set Use WebLogicSession boolean true or false
		/// </summary>
		public bool UseWebLogicSessionParameterInRequest
		{
			set
			{
				if(value==false)
				{
					_Uri=System.Configuration.ConfigurationSettings.AppSettings[_OperationURLKeyInWebConfig];
				}
				else
				{
					_Uri=System.Configuration.ConfigurationSettings.AppSettings[_OperationURLKeyInWebConfig]+ ";WebLogicSession=" + Utils.NullToString(HttpContext.Current.Session["SESSION_EuropCarSessionID"]);
				}
				_UseWebLogicSessionParameterInRequest=value;
			}
			get{ return _UseWebLogicSessionParameterInRequest;}
		}


		#endregion

		#region Constructor
		public RequestHandler()
		{
			_Uri = System.Configuration.ConfigurationSettings.AppSettings[_OperationURLKeyInWebConfig];
			SetupValues();
		}

		public RequestHandler(bool UseWebLogicSessionParam)
		{
			if(UseWebLogicSessionParam==true)
			{
				_Uri = System.Configuration.ConfigurationSettings.AppSettings[_OperationURLKeyInWebConfig] + ";WebLogicSession=" + Utils.NullToString(HttpContext.Current.Session["SESSION_EuropCarSessionID"]);
			}
			else
			{
				_Uri = System.Configuration.ConfigurationSettings.AppSettings[_OperationURLKeyInWebConfig];
			}
			SetupValues();			
		}

		public RequestHandler(string URI)
		{
			_Uri=URI;
			SetupValues();			
		}
		#endregion

		#region Public Methods

		private void SetupValues()
		{
			_SoapEnvelopeStart = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
				/*<SOAP-ENV:Envelope xmlns:SOAP-ENV='http://schemas.xmlsoap.org/soap/envelope/' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'><SOAP-ENV:Body>";
			_SoapEnvelopeStart += "<ns1:$Method$ xmlns:ns1='OTA' SOAP-ENV:encodingStyle='http://schemas.xmlsoap.org/soap/encoding/'> ";
			_SoapEnvelopeStart += "<brand xsi:type='xsd:string'>$Brand$</brand>";
			_SoapEnvelopeStart += "<schema xsi:type='xsd:string'>$Schema$</schema> ";
			_SoapEnvelopeStart += "<request xsi:type='xsd:string'> ";
			_SoapEnvelopeEnd = " </request></ns1:$Method$></SOAP-ENV:Body></SOAP-ENV:Envelope> ";*/
			_Method = "GET";
			_UserAgent = "RPT-HTTPClient/0.3-3";
			_SoapAction = "";
			_ContentType = "text/xml; charset=utf-8";			
		}

		/// <summary>
		/// Make request with specified URI and xmlMessage
		/// </summary>
		/// <param name="xmlMessage">string</param>
		/// <returns>
		/// Response as XML String
		/// </returns>
		public string makeRequest(string xmlMessage)
		{
			string output = string.Empty;
			 

			string messageBody = _SoapEnvelopeStart;
			messageBody += xmlMessage;
			messageBody = Utils.toXML(messageBody);

			string getVars = "?XML-Request=" + HttpUtility.UrlEncode(messageBody);

			byte[]  buffer = Encoding.ASCII.GetBytes(messageBody);

			//Uri uriOfRequest = new Uri(_Uri);			
			//HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(uriOfRequest);
			HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(string.Format(_Uri+"{0}", getVars));
			
			WebReq.Method = _Method;			

			/*********************** DEBUG XML REQUST/RESPONSE BY LOGGING/MAILING ******************/
            //Debug.PerformAction(messageBody,_Uri,Debug.DebugMessageType.Request);
			/***************************************************************************************/
			
			try
			{						
				HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();
				Stream Answer = WebResp.GetResponseStream();
				StreamReader _Answer = new StreamReader(Answer);
				output  = _Answer.ReadToEnd();
			}
			catch(Exception ex)
			{
				output = ex.Message + "\n" + ex.StackTrace; 
			}				

			/*********************** DEBUG XML REQUST/RESPONSE BY LOGGING/MAILING ******************/
            //Debug.PerformAction(Utils.toXML(output),_Uri,Debug.DebugMessageType.Response);
			/***************************************************************************************/

			return Utils.toXML(output);			
		}

		/// <summary>
		/// Make request with specified URI and xmlMessage
		/// </summary>
		/// <param name="xmlMessage">string</param>
		/// <returns>
		/// Response as XML String
		/// </returns>
		public string makeRequestPOST(string xmlMessage)
		{
			string output = string.Empty;

			string messageBody = _SoapEnvelopeStart;
			messageBody += xmlMessage;
			messageBody = Utils.toXML(messageBody);

			byte[]  buffer = Encoding.ASCII.GetBytes(messageBody);

			Uri uriOfRequest = new Uri(_Uri);			
			HttpWebRequest WebReq = (HttpWebRequest)WebRequest.Create(uriOfRequest);
			
			WebReq.Method = _Method;
			
			WebReq.ContentType =_ContentType;
			
			WebReq.ContentLength = buffer.Length;
			
			Stream PostData = WebReq.GetRequestStream();
			
			PostData.Write(buffer, 0, buffer.Length);
			PostData.Close();

			/*********************** DEBUG XML REQUST/RESPONSE BY LOGGING/MAILING ******************/
            //Debug.PerformAction(messageBody,_Uri,Debug.DebugMessageType.Request);
			/***************************************************************************************/
			
			try
			{						
				HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();
				Stream Answer = WebResp.GetResponseStream();
				StreamReader _Answer = new StreamReader(Answer);
				output  = _Answer.ReadToEnd();
			}
			catch(Exception ex)
			{
				output = ex.Message + "\n" + ex.StackTrace; 
			}				

			/*********************** DEBUG XML REQUST/RESPONSE BY LOGGING/MAILING ******************/
            //Debug.PerformAction(Utils.toXML(output),_Uri,Debug.DebugMessageType.Response);
			/***************************************************************************************/

			return Utils.toXML(output);			
		}

		/// <summary>
		/// Make request with specified URI and xmlMessage
		/// </summary>
		/// <param name="xmlMessage">string</param>
		/// <returns>
		/// Response as XML String
		/// </returns>
		public string makeRequest_old(string xmlMessage)
		{
			string output = string.Empty;
			string messageBody = "";
			byte[] messageBytes;
			//System.Net.ServicePointManager.CertificatePolicy = new XMLIntegration.WebSecurityPolicy();
			Uri uriOfRequest = new Uri(_Uri);
			
			HttpWebRequest webRequest =(HttpWebRequest)WebRequest.Create(uriOfRequest);
			
			messageBody = _SoapEnvelopeStart;
			messageBody += xmlMessage;
			/*messageBody += _SoapEnvelopeEnd;*/

			messageBody = Utils.toXML(messageBody);

			webRequest.Method = _Method;
			webRequest.UserAgent = _UserAgent;
			webRequest.Headers.Add("SOAPAction",_SoapAction);			
			
			webRequest.ProtocolVersion = new System.Version(1,1);
			webRequest.ContentType = _ContentType;
			webRequest.ContentLength = messageBody.Length;
			messageBytes = Encoding.ASCII.GetBytes(messageBody);
			Stream requestStream = webRequest.GetRequestStream();
			requestStream.Write(messageBytes,0,messageBytes.Length);
			HttpWebResponse webResponse =null;
			Stream responseStream = null;
			StreamReader webstream = null;

			/*********************** DEBUG XML REQUST/RESPONSE BY LOGGING/MAILING ******************/
            //Debug.PerformAction(messageBody,Debug.DebugMessageType.Request);
			/***************************************************************************************/

			try
			{
				webResponse = (HttpWebResponse)webRequest.GetResponse();
				webstream = new StreamReader(webResponse.GetResponseStream(),Encoding.ASCII );
				output  = webstream.ReadToEnd();
			}
			catch(Exception ex)
			{
				output = ex.Message + "\n" + ex.StackTrace; 
			}
			finally
			{
				if(webstream != null)
					webstream.Close();
				if(responseStream != null)
					responseStream.Close();
				if(webResponse != null)
					webResponse.Close();
				requestStream.Close();
			}

			/*********************** DEBUG XML REQUST/RESPONSE BY LOGGING/MAILING ******************/
            //Debug.PerformAction(Utils.toXML(output),Debug.DebugMessageType.Response);
			/***************************************************************************************/

			return Utils.toXML(output);
		}
		#endregion

	}
}
