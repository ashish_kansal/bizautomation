using System;
using System.Text;
using System.Data;
using System.IO;
using System.Data.SqlClient;
using System.Web;

namespace XMLIntegration
{
	/// <summary>
	/// Contains misc methods for XML requests
	/// </summary>
	public class MiscRequest
	{
		#region XML Request Common Memebers 
		private string _XMLResponse;
		/// <summary>
		/// Get/Set XML response
		/// </summary>
		public string XMLResponse
		{
			get{return _XMLResponse;}
			set{_XMLResponse=value;}
		}
		private string _XMLResponseSessionID;
		/// <summary>
		/// Get/Set SessionID returned from XML response
		/// </summary>
		public string SessionID
		{
			get{return _XMLResponseSessionID;}
			set{_XMLResponseSessionID=value;}
		}

		private string _XMLResponseReturnCode;
		/// <summary>
		/// Get/Set Return Code returned from XML response
		/// </summary>
		public string ReturnCode
		{
			get{return _XMLResponseReturnCode;}
			set{_XMLResponseReturnCode=value;}
		}
		#endregion

		#region Methods
		/// <summary>
		/// GET XML response for No operation request
		/// </summary>				
		/// 
		public bool makeNoopRequest()
		{
			bool outputResult=false;

			RequestHandler objRH=new RequestHandler(true);
			_XMLResponse=objRH.makeRequest(getXMLRequestNoOperation());
			parseNoopResponseXML();

			if(_XMLResponseReturnCode=="OK")
			{				
				outputResult=true;
			}

			return outputResult;
		}

		/// <summary>
		/// Parse XML returned by server to get message and returnCode
		/// </summary>				
		/// 
		public bool parseNoopResponseXML()
		{			
			bool output = false;
			DataSet dsResponse =new DataSet();

			try
			{
				dsResponse.ReadXml(new StringReader(this._XMLResponse));

				if(dsResponse.Tables.Count>0)
				{
					if(dsResponse.Tables.Contains("message"))
					{
						if(dsResponse.Tables["message"].Rows.Count>0)
						{
							this._XMLResponseSessionID=dsResponse.Tables["message"].Rows[0]["sessionID"].ToString();							
						}
					}
					if(dsResponse.Tables.Contains("serviceResponse"))
					{
						if(dsResponse.Tables["serviceResponse"].Rows.Count>0)
						{
							this._XMLResponseReturnCode=dsResponse.Tables["serviceResponse"].Rows[0]["returnCode"].ToString();	

							if(dsResponse.Tables["serviceResponse"].Columns.Contains("errorCode"))
							{
								_ErrorCode=Utils.NullToString(dsResponse.Tables["serviceResponse"].Rows[0]["errorCode"]);
							}
							output=true;
						}
					}
					else
					{
						SetError("invalidMessage","0","Invalid XML Message");
					}
				}
				else
				{
					SetError("invalidMessage","0","Invalid XML Message");
				}
			}
			catch(Exception ex)
			{
				SetError(ex.Message);
			}
			return output;
		}
		/// <summary>
		/// GET XML for NoOperation request
		/// </summary>		
		/// <returns>Returns XML request as string</returns>
		/// 
		public string getXMLRequestNoOperation()
		{
			/*
			 * Sample XML Request
			 * 
			<message>
				<serviceRequest serviceCode="noOperation" >
					<serviceParameters />
				</serviceRequest >
			<message/>	
			*/
			StringBuilder sbXML=new StringBuilder();
			try
			{				
				sbXML.Append("&lt;message&gt;");
				sbXML.Append("&lt;serviceRequest serviceCode=\"noOperation\"&gt;");				
				sbXML.Append("&lt;serviceParameters /&gt;");				
				sbXML.Append("&lt;/serviceRequest&gt;");
				sbXML.Append("&lt;/message&gt;");
			}
			catch(Exception ex)
			{
				SetError(ex.Message);
			}						
			return sbXML.ToString();
		}

		#endregion	
	
		#region ERROR SECTION

		#region Members & Properties
		private string  _ErrorType;
		/// <summary>
		/// Get/Set Error Type
		/// </summary>
		public string ErrorType
		{
			get{return _ErrorType;}
			set{_ErrorType=value;}
		}

		private string  _ErrorCode;
		/// <summary>
		/// Get/Set Error Code
		/// </summary>
		public string ErrorCode
		{
			get{return _ErrorCode;}
			set{_ErrorCode=value;}
		}

		private string  _ErrorMessage;
		/// <summary>
		/// Get/Set Error Message
		/// </summary>
		public string ErrorMessage
		{
			get{return _ErrorMessage;}
			set{_ErrorMessage=value;}
		}
		#endregion

		#region Methods

		/// <summary>
		/// Private method to set an error
		/// </summary>		
		/// <returns></returns>
		/// 
		private void SetError(string type,string code,string message)
		{
			this._ErrorType=type;
			this._ErrorCode = code;
			this._ErrorMessage = message;
		}
		/// <summary>
		/// Private method to set an error/Especially Exceptions
		/// </summary>		
		/// <returns></returns>
		/// 
		private void SetError(string message)
		{
			this._ErrorType="exception";
			this._ErrorCode = "-2";
			this._ErrorMessage = message;
		}
		#endregion

		#endregion
	}
}
