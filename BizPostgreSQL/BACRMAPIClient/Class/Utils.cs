using System;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace XMLIntegration
{
	/// <summary>
	/// Contains misc static methods
	/// </summary>
	public class Utils
	{
		public static string GetEuropcarSessionId()
		{
			return NullToString(HttpContext.Current.Session["SESSION_EuropCarSessionID"]);
		}

		public static void SetEuropcarSessionId(string SessionCode)
		{
			HttpContext.Current.Session["SESSION_EuropCarSessionID"]=SessionCode;
		}

		public static string GetEuropcarSessionId(string replaceString)
		{
			return NullToString(HttpContext.Current.Session["SESSION_EuropCarSessionID"],replaceString);
		}

		public static string NullToString(Object obj)
		{
			return obj==null?"":obj.ToString();
		}

		public static string NullToStringColumns(DataRow dr,string ColumnName)
		{
			return dr.Table.Columns.Contains(ColumnName)?NullToString(dr[ColumnName]):"";
		}

		public static string NullToString(Object obj,string replaceString)
		{
			return obj==null?replaceString:obj.ToString();
		}

		public static string GetWebConfigKeyValue(string keyNmae)
		{
			return System.Configuration.ConfigurationSettings.AppSettings.Get(keyNmae);
		}

		public static string EncodeURL(string strVal)
		{
			return System.Web.HttpUtility.UrlEncode(strVal);
		}

		public static string DecodeURL(string strVal)
		{
			return System.Web.HttpUtility.UrlDecode(strVal);
		}

		public static string toXML(string response)
		{
			string output = string.Empty;
			if(response != null)
			{
				try
				{
					output = response;
					/*output = response.Substring(response.IndexOf("<return"));
					output = output.Substring((output.IndexOf(">"))+1);
					output = output.Substring(0, output.IndexOf("</return>"));*/
					output = output.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"");
					/*output = "<?xml version='1.0' encoding='UTF-8'?>" + output;*/
				}
				catch(Exception ex)
				{
					output = ex.Message + "\n" + ex.StackTrace;
				}
			}
			return output;
		}

		public static bool IsDecimal(string theValue)
		{
			try
			{
				Convert.ToDouble(theValue);
				return true;
			}
			catch
			{
				return false;
			}
		}

		public static bool IsInteger(string theValue)
		{
			try
			{
				Convert.ToInt32(theValue);
				return true;
			}
			catch
			{
				return false;
			}
		}
	}
}
