﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

using eBay.Service.Call;
using eBay.Service.Core.Sdk;
using eBay.Service.Core.Soap;
using eBay.Service.Util;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Data;
using System.Configuration;
using System.ServiceModel;

namespace EBay_Communicator
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "eBayCommunicator" in both code and config file together.
    public class eBayCommunicator : IeBayCommunicator
    {
        /// <summary>
        /// Gets the OrderTypeCollection and Serialize it to MemoryStream 
        /// </summary>
        /// <param name="Context">ApiContext</param>
        /// <param name="FromDate">FromDate</param>
        /// <param name="ToDate">ToDate</param>
        /// <returns>OrderTypeCollection as MemoryStream</returns>
        public MemoryStream GetOrders(ApiContext Context, DateTime FromDate, DateTime ToDate)
        {
            try
            {
                GetOrdersCall apicall = new GetOrdersCall(Context);
                TimeFilter fltr = new TimeFilter();

                fltr.TimeFrom = FromDate.AddMinutes(-2);
                fltr.TimeTo = ToDate.AddMinutes(-2);

                //OrderTypeCollection orders = apicall.GetOrders(fltr, TradingRoleCodeType.Seller, OrderStatusCodeType.Completed);

                GetOrdersCall apicallModified = new GetOrdersCall(Context);
                double days;
                days = (fltr.TimeTo - fltr.TimeFrom).TotalDays;
                OrderTypeCollection modifieOrders = null;
                apicallModified.ModTimeFrom = fltr.TimeFrom;
                apicallModified.ModTimeTo = days > 30 ? fltr.TimeFrom.AddDays(30) : fltr.TimeTo;
                apicallModified.OrderStatus = OrderStatusCodeType.Completed;
                modifieOrders = apicallModified.GetOrders(null);
                //foreach (OrderType objOrder in modifieOrders)
                //{
                //    if (objOrder.OrderStatus == OrderStatusCodeType.Completed)
                //    {
                //        orders.Add(objOrder);
                //    }
                //}

                MemoryStream ms = SerializeToStream(modifieOrders);
                return ms;
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
        }

        public MemoryStream GetOrderById(ApiContext Context, string OrderId)
        {

            try
            {
                GetOrdersCall apicall = new GetOrdersCall(Context);
                TimeFilter fltr = new TimeFilter();
                StringCollection OrderIdCollection = new StringCollection(new string[] { OrderId });
                OrderTypeCollection orders = apicall.GetOrders(OrderIdCollection);
                MemoryStream ms = SerializeToStream(orders);
                return ms;
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
        }


        public void CancelOrder(ApiContext Context, string OrderId)
        {
            IssueRefundCall objIssueRefundCall = new IssueRefundCall(Context);

            objIssueRefundCall.RefundType = RefundTypeCodeType.Full;

            objIssueRefundCall.RefundReason = RefundReasonCodeType.Other;
            //objIssueRefundCall.TotalRefundToBuyer = 

            objIssueRefundCall.OrderLineItemID = "";

            IssueRefundRequestType objReqType = new IssueRefundRequestType();
            objReqType.RefundType = RefundTypeCodeType.Full;
            //objIssueRefundCall.
        }

        /// <summary>
        /// Adds new ItemType to E-Bay Listing
        /// </summary>
        /// <param name="Context">ApiContext</param>
        /// <param name="msItem">ItemType as MemoryStream</param>
        /// <param name="ItemImages">Item Images List</param>
        /// <returns>E-Bay Item Id</returns>
        public string AddNewItem(ApiContext Context, MemoryStream msItem, List<string> ItemImages)
        {
            try
            {
                ItemType Item = new ItemType();
                XmlSerializer serializer = new XmlSerializer(typeof(ItemType));
                XmlReader Reader = new XmlTextReader(msItem);
                Item = (ItemType)serializer.Deserialize(Reader);
                string ItemId = "";
                AddItemCall apicall = new AddItemCall(Context);
                string ImagePath = "";
                int i = 0;

                foreach (string Imagepath in ItemImages)
                {
                    apicall.PictureFileList.Add(Imagepath);
                }
                apicall.AddItem(Item);
                ItemId = Item.ItemID;
                return ItemId;
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
        }

        /// <summary>
        /// Update Item Detail in E-Bay Listing
        /// </summary>
        /// <param name="Context">ApiContext</param>
        /// <param name="msItem">ItemType as MemoryStream</param>
        /// <param name="ItemImages">Item Images List</param>
        /// <param name="DeletedFields">DeletedFields</param>
        /// <returns></returns>
        public string ReviseItem(ApiContext Context, MemoryStream msItem, List<string> ItemImages, StringCollection DeletedFields)
        {
            try
            {
                ItemType Item = new ItemType();
                XmlSerializer serializer = new XmlSerializer(typeof(ItemType));
                XmlReader Reader = new XmlTextReader(msItem);
                Item = (ItemType)serializer.Deserialize(Reader);
                string ItemId = "";
                ReviseItemCall apicall = new ReviseItemCall(Context);

                string ImagePath = "";
                int i = 0;

                //foreach (XmlNode Image in ItemImages)
                //{
                //    ImagePath = ProductImagePath + DomainId + "\\" + (Image.Attributes["vcPathForImage"].Value).ToString();
                //    apicall.PictureFileList.Add(ImagePath);
                //}

                foreach (string Imagepath in ItemImages)
                {
                    apicall.PictureFileList.Add(Imagepath);
                }
                apicall.ReviseItem(Item, DeletedFields, false);
                ItemId = Item.ItemID;
                //UpdateInventory(Context, ItemId, Item.SKU, Item.Quantity);
                return ItemId;
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Context"></param>
        /// <param name="ItemId"></param>
        /// <param name="SKU"></param>
        /// <param name="Quantity"></param>
        public void UpdateInventory(ApiContext Context, string ItemId, string SKU, int Quantity)
        {
            try
            {
                ReviseInventoryStatusCall objUpdateInventory = new ReviseInventoryStatusCall(Context);
                InventoryStatusTypeCollection objInvStatusColl = new InventoryStatusTypeCollection();
                InventoryStatusType objInvStatus = new InventoryStatusType();
                objInvStatus.ItemID = ItemId;
                objInvStatus.Quantity = Quantity;
                objInvStatus.QuantitySpecified = true;

                objInvStatus.SKU = SKU;
                objInvStatusColl.Add(objInvStatus);
                objUpdateInventory.ReviseInventoryStatus(objInvStatusColl);
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Context">ApiContext</param>
        /// <param name="ItemAPIId">E-Bay Item Id</param>
        /// <param name="APIOrderId"></param>
        /// <param name="APIOrderLineItemId"></param>
        /// <param name="TrackingNo"></param>
        /// <param name="ListingType"></param>
        /// <param name="objShipmentTyp"></param>
        public void UpdateTrackingNo(ApiContext Context, string ItemAPIId, string APIOrderId, string APIOrderLineItemId,
            string TrackingNo, ListingTypeCodeType ListingType, MemoryStream msShipmentTyp)
        {
            try
            {
                ShipmentType ShipmentTypObject = new ShipmentType();
                XmlSerializer serializer = new XmlSerializer(typeof(ShipmentType));
                XmlReader Reader = new XmlTextReader(msShipmentTyp);
                ShipmentTypObject = (ShipmentType)serializer.Deserialize(Reader);

                CompleteSaleCall objCompleteSale = new CompleteSaleCall(Context);
                objCompleteSale.CompleteSale(ItemAPIId, TrackingNo, null, true, true, ListingType,
                    ShipmentTypObject, APIOrderId, APIOrderLineItemId);

            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }

        }

        /// <summary>
        /// Xml Serializer Serialize the Object to MemormStream by XMLTextWriter
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="obj">Object</param>
        /// <returns>Memory Stream</returns>
        public static MemoryStream SerializeToStream<T>(T obj)
        {
            try
            {
                MemoryStream memorystream = new MemoryStream();
                XmlWriter writer = new XmlTextWriter(memorystream, Encoding.Unicode);
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                // Serialize using the XmlTextWriter.
                serializer.Serialize(writer, obj);
                memorystream.Position = 0;
                return memorystream;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Context"></param>
        /// <param name="SellerSKU"></param>
        /// <returns></returns>
        /// ///While Passing Seller SKU it throws Exception "There is no active item matching the specified SKU" 
        /// So, Using E-Bay Item ID to Get Product Details
        public EBayProduct GetItemDetails(ApiContext Context, string ItemID)
        {
            ItemType objItem = new ItemType();
            EBayProduct objEbayProduct = new EBayProduct();
            try
            {
                DetailLevelCodeTypeCollection DetailLevel = new DetailLevelCodeTypeCollection(new DetailLevelCodeType[] { DetailLevelCodeType.ReturnAll });
                GetItemCall objGetItemCall = new GetItemCall(Context);
                objGetItemCall.IncludeItemCompatibilityList = true;
                objGetItemCall.IncludeItemSpecifics = true;
                objGetItemCall.DetailLevelList = DetailLevel;
                objGetItemCall.ItemID = ItemID;
                objGetItemCall.Execute();

                if (objGetItemCall.Item != null)
                {
                    objItem = objGetItemCall.Item;
                    objEbayProduct.EbayItemId = objItem.ItemID;
                    objEbayProduct.ListingType = objItem.ListingType.ToString();
                    objEbayProduct.ListingDuration = objItem.ListingDuration;
                    objEbayProduct.Description = objItem.Description;
                    objEbayProduct.ListingDetailsStartTime = objItem.ListingDetails.StartTime.ToLongDateString();
                    objEbayProduct.ListingDetailsEndTime = objItem.ListingDetails.StartTime.ToLongDateString();
                    objEbayProduct.CategoryID = objItem.PrimaryCategory.CategoryID;
                    objEbayProduct.Title = objItem.Title;
                    objEbayProduct.SellerSKU = objItem.SKU;
                    NameValueListTypeCollection coll = new NameValueListTypeCollection(objItem.ItemSpecifics);
                    foreach (NameValueListType lstNameValue in coll)
                    {
                        if (lstNameValue.Name == "Brand")
                            objEbayProduct.Brand = lstNameValue.Value[0].ToString();
                        if (lstNameValue.Name == "Model")
                            objEbayProduct.ModelNumber = lstNameValue.Value[0].ToString();
                        if (lstNameValue.Name == "Color")
                            objEbayProduct.Color = lstNameValue.Value[0].ToString();
                    }

                    if (objItem.StartPrice != null)
                    {
                        objEbayProduct.StartPriceValue = objItem.StartPrice.Value.ToString();
                    }
                    if (objItem.BuyItNowPrice != null)
                    {
                        objEbayProduct.BuyItNowPriceValue = objItem.BuyItNowPrice.Value.ToString();
                    }
                    if (objItem.SellingStatus.CurrentPrice != null)
                    {
                        objEbayProduct.CurrentPriceValue = objItem.SellingStatus.CurrentPrice.Value.ToString();
                    }

                    if (objItem.QuantitySpecified == true)
                    {
                        objEbayProduct.Quantity = objItem.Quantity.ToString();
                    }
                    if (objItem.QuantityAvailableSpecified == true)
                    {
                        objEbayProduct.QuantityAvailable = objItem.QuantityAvailable.ToString();
                    }
                    if (objItem.ShippingDetails != null)
                    {
                        if (objItem.ShippingDetails.ShippingServiceOptions != null)
                        {
                            foreach (ShippingServiceOptionsType Detail in objItem.ShippingDetails.ShippingServiceOptions)
                            {
                                objEbayProduct.ShippingType = Detail.ShippingService;
                                if (Detail.ShippingServiceCost != null)
                                {
                                    objEbayProduct.StandardShippingCharge = Detail.ShippingServiceCost.Value.ToString();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
            return objEbayProduct;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Context"></param>
        /// <param name="ItemID"></param>
        /// <returns></returns>
        public ItemType GetItemDetailsForItemID(ApiContext Context, string ItemID)
        {
            ItemType objItem = new ItemType();
            EBayProduct objEbayProduct = new EBayProduct();
            try
            {
                DetailLevelCodeTypeCollection DetailLevel = new DetailLevelCodeTypeCollection(new DetailLevelCodeType[] { DetailLevelCodeType.ReturnAll });
                GetItemCall objGetItemCall = new GetItemCall(Context);
                objGetItemCall.IncludeItemCompatibilityList = true;
                objGetItemCall.IncludeItemSpecifics = true;
                objGetItemCall.DetailLevelList = DetailLevel;
                objGetItemCall.ItemID = ItemID;
                objGetItemCall.Execute();

                if (objGetItemCall.Item != null)
                {
                    objItem = objGetItemCall.Item;
                }
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
            return objItem;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Context"></param>
        /// <returns></returns>
        public EBayProductList GetMerchantProductListings(ApiContext Context, int ListingType)
        {
            EBayProductList objEBayProductList = new EBayProductList();
            List<EBayProduct> lstEbayProducts = new List<EBayProduct>();

            string strKeys = "",strValues = "";

            try
            {
                GetMyeBaySellingCall objEbaySellingCall = new GetMyeBaySellingCall(Context);
                ItemListCustomizationType obj1 = new ItemListCustomizationType();
                ItemListCustomizationType obj2 = new ItemListCustomizationType();
                obj2.Include = false;
                obj2.IncludeSpecified = true;
                ItemListCustomizationType objUnsold = new ItemListCustomizationType();
                objUnsold.Include = true;
                objUnsold.IncludeSpecified = true;
                objUnsold.IncludeNotes = true;
                objUnsold.IncludeNotesSpecified = true;
                objUnsold.Sort = ItemSortTypeCodeType.EndTime;
                objUnsold.SortSpecified = true;

                obj1.IncludeNotes = true;
                obj1.Include = true;
                obj1.IncludeNotesSpecified = true;
                obj1.IncludeSpecified = true;
                objEbaySellingCall.ActiveList = obj1;
                objEbaySellingCall.UnsoldList = objUnsold;
                objEbaySellingCall.SoldList = objUnsold;
                objEbaySellingCall.BidList = objUnsold;
                objEbaySellingCall.Execute();
                if (objEbaySellingCall.ActiveListReturn != null)
                {
                    foreach (ItemType oItem in objEbaySellingCall.ActiveListReturn.ItemArray)
                    {
                        EBayProduct objEBayProduct = new EBayProduct();
                        objEBayProduct = GetItemDetails(Context, oItem.ItemID);
                        objEBayProduct.IsArchieve = false;

                        if (oItem.Variations != null)
                        {
                            VariationCollection vCollection = new VariationCollection();
                            vCollection.VariationDetail = new List<VariationDetails>();

                            VariationDetails vd;

                            foreach (VariationType vt1 in oItem.Variations.Variation)
                            {
                                vd = new VariationDetails();
                                vd.VariationTitle = Convert.ToString(vt1.VariationTitle);
                                vd.ListPrice = Convert.ToDouble(vt1.StartPrice.Value);
                                vd.Quantity = Convert.ToInt32(vt1.Quantity);
                                vd.ReOrder = 0;
                                vd.SKU = Convert.ToString(vt1.SKU);
                                vd.VariationName = Convert.ToString(vt1.VariationSpecifics[0].Name);

                                string strItemValue = "";
                                foreach (NameValueListType nvltItem in vt1.VariationSpecifics)
                                {
                                    strItemValue = strItemValue + "," + (Convert.ToString(nvltItem.Name) + "~" + Convert.ToString(nvltItem.Value[0]));
                                }

                                vd.VariationValue = strItemValue.Trim(',').Trim();
                                vCollection.VariationDetail.Add(vd);
                            }


                            vCollection.VariationList = new List<Variations>();
                            Variations varie;
                            foreach (VariationType vt1 in oItem.Variations.Variation)
                            {
                                foreach (NameValueListType vt2 in vt1.VariationSpecifics)
                                {
                                    varie = new Variations();
                                    varie.Name = vt2.Name.ToString();
                                    strKeys = strKeys + "," + varie.Name;

                                    string strItemValue = "";
                                    // Use Iterator to iterate through the values of varie.value
                                    foreach (string strItem in vt2.Value)
                                    {

                                        if (varie.Value == null)
                                        {
                                            strItemValue = strItem;
                                        }
                                        else
                                        {
                                            strItemValue = strItemValue + "," + strItem;
                                        }

                                        if (strValues.Contains(strItemValue) == false)
                                        {
                                            strValues = strValues + "," + strItemValue;

                                            varie.Value = strItemValue.Trim(',');
                                            vCollection.VariationList.Add(varie);
                                        }
                                        else
                                        {
                                            strItemValue = "";
                                        }
                                    }
                                }
                            }
                            objEBayProduct.ItemVariations = vCollection;
                        }

                        lstEbayProducts.Add(objEBayProduct);
                    }
                }

                if (ListingType == 2)
                {
                    if (objEbaySellingCall.UnsoldListReturn != null)
                    {
                        foreach (ItemType oItem in objEbaySellingCall.UnsoldListReturn.ItemArray)
                        {
                            EBayProduct objEBayProduct = new EBayProduct();
                            objEBayProduct = GetItemDetails(Context, oItem.ItemID);
                            objEBayProduct.IsArchieve = true;

                            if (oItem.Variations != null)
                            {
                                VariationCollection vCollection = new VariationCollection();
                                vCollection.VariationList = new List<Variations>();

                                Variations varie;
                                foreach (VariationType vt1 in oItem.Variations.Variation)
                                {
                                    foreach (NameValueListType vt2 in vt1.VariationSpecifics)
                                    {
                                        varie = new Variations();
                                        varie.Name = vt2.Name.ToString();
                                        strKeys = strKeys + "," + varie.Name;

                                        string strItemValue = "";
                                        // Use Iterator to iterate through the values of varie.value
                                        foreach (string strItem in vt2.Value)
                                        {

                                            if (varie.Value == null)
                                            {
                                                strItemValue = strItem;
                                            }
                                            else
                                            {
                                                strItemValue = strItemValue + "," + strItem;
                                            }

                                            if (strValues.Contains(strItemValue) == false)
                                            {
                                                strValues = strValues + "," + strItemValue;

                                                varie.Value = strItemValue.Trim(',');
                                                vCollection.VariationList.Add(varie);
                                            }
                                            else
                                            {
                                                strItemValue = "";
                                            }
                                        }
                                    }
                                }
                                objEBayProduct.ItemVariations = vCollection;
                            }

                            lstEbayProducts.Add(objEBayProduct);
                        }
                    }

                }

                objEBayProductList.LstEBayProduct = lstEbayProducts;
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
            return objEBayProductList;
        }

    }
}
