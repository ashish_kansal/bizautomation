﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml;

using eBay.Service.Call;
using eBay.Service.Core.Sdk;
using eBay.Service.Core.Soap;
using eBay.Service.Util;
using System.IO;


namespace EBay_Communicator
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IeBayCommunicator" in both code and config file together.
    [ServiceContract]
    public interface IeBayCommunicator
    {
        [FaultContract(typeof(error))]
        [OperationContract]
        MemoryStream GetOrders(ApiContext Context, DateTime FromDate, DateTime ToDate);

        [FaultContract(typeof(error))]
        [OperationContract]
        string AddNewItem(ApiContext Context, MemoryStream msItem, List<string> ItemImages);

        [FaultContract(typeof(error))]
        [OperationContract]
        string ReviseItem(ApiContext Context, MemoryStream msItem, List<string> ItemImages, StringCollection DeletedFields);

        [FaultContract(typeof(error))]
        [OperationContract]
        void UpdateInventory(ApiContext Context, string ItemId, string SKU, int Quantity);

        [FaultContract(typeof(error))]
        [OperationContract]
        void UpdateTrackingNo(ApiContext Context, string ItemAPIId, string APIOrderId, string APIOrderLineItemId,
            string TrackingNo, ListingTypeCodeType ListingType, MemoryStream objShipmentTyp);

        [FaultContract(typeof(error))]
        [OperationContract]
        EBayProductList GetMerchantProductListings(ApiContext Context, int ListingType);

        [FaultContract(typeof(error))]
        [OperationContract]
        EBayProduct GetItemDetails(ApiContext Context, string SellerSKU);

        [FaultContract(typeof(error))]
        [OperationContract]
        ItemType GetItemDetailsForItemID(ApiContext Context, string ItemID);

        [FaultContract(typeof(error))]
        [OperationContract]
        MemoryStream GetOrderById(ApiContext Context, string OrderId);

    }

    [DataContract]
    public class error
    {
        [DataMember]
        public string msg;
        [DataMember]
        public string request;

    }

    [DataContract]
    public class EBayProductList
    {
        List<EBayProduct> lstEBayProduct;

        [DataMember]
        public List<EBayProduct> LstEBayProduct
        {
            get { return lstEBayProduct; }
            set { lstEBayProduct = value; }
        }
    }

    [DataContract]
    public class VariationCollection
    {
        List<Variations> _VariationList;
        List<VariationDetails> _VariationDetail;

        [DataMember]
        public List<Variations> VariationList
        {
            get { return _VariationList; }
            set { _VariationList = value; }
        }

        [DataMember]
        public List<VariationDetails> VariationDetail
        {
            get { return _VariationDetail; }
            set { _VariationDetail = value; }
        }
    }

    [DataContract]
    public class Variations
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Value { get; set; }
    }

    [DataContract]
    public class VariationDetails
    {
        [DataMember]
        public string VariationTitle { get; set; }

        [DataMember]
        public string VariationName { get; set; }

        [DataMember]
        public string VariationValue { get; set; }

        [DataMember]
        public int Quantity { get; set; }

        [DataMember]
        public int ReOrder { get; set; }

        [DataMember]
        public string SKU { get; set; }

        [DataMember]
        public Double ListPrice { get; set; }

    }

    [DataContract]
    public class EBayProduct
    {
        string _ListingType, _Title, _Description, _Manufacturer, _BuyerGuaranteePrice, _Quantity, _QuantityAvailable, _CategoryID,
        _BuyItNowPrice, _BuyItNowPricecurrencyID, _BuyItNowPriceValue, _StartPricecurrencyID, _StartPriceValue,
        _CurrentPricecurrencyID, _CurrentPriceValue, _ShippingType, _ListingDetailsStartTime, _ListingDetailsEndTime, _ListingDetailsTimeLeft,
        _Height, _Weight, _Length, _Width, _ShippingHeight, _ShippingWeight, _ShippingLength, _ShippingWidth,
        _StandardShippingCharge, _ListingDuration, _StandardTaxCharge, _WeightUOM, _LengthUOM, _ModelNumber,
        _Brand, _Color, _Warranty, _EbayItemId, _SellerSKU;

        VariationCollection _ItemVariations;

        [DataMember]
        public VariationCollection ItemVariations
        {
            get { return _ItemVariations; }
            set { _ItemVariations = value; }
        }

        bool _IsArchieve;

        [DataMember]
        public bool IsArchieve
        {
            get { return _IsArchieve; }
            set { _IsArchieve = value; }
        }

        [DataMember]
        public string SellerSKU
        {
            get { return _SellerSKU; }
            set { _SellerSKU = value; }
        }

        [DataMember]
        public string CategoryID
        {
            get { return _CategoryID; }
            set { _CategoryID = value; }
        }

        [DataMember]
        public string ListingDetailsEndTime
        {
            get { return _ListingDetailsEndTime; }
            set { _ListingDetailsEndTime = value; }
        }

        [DataMember]
        public string ListingDuration
        {
            get { return _ListingDuration; }
            set { _ListingDuration = value; }
        }

        [DataMember]
        public string ListingType
        {
            get { return _ListingType; }
            set { _ListingType = value; }
        }

        [DataMember]
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        [DataMember]
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        [DataMember]
        public string Manufacturer
        {
            get { return _Manufacturer; }
            set { _Manufacturer = value; }
        }

        [DataMember]
        public string BuyerGuaranteePrice
        {
            get { return _BuyerGuaranteePrice; }
            set { _BuyerGuaranteePrice = value; }
        }

        [DataMember]
        public string Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }

        [DataMember]
        public string QuantityAvailable
        {
            get { return _QuantityAvailable; }
            set { _QuantityAvailable = value; }
        }

        [DataMember]
        public string BuyItNowPrice
        {
            get { return _BuyItNowPrice; }
            set { _BuyItNowPrice = value; }
        }

        [DataMember]
        public string BuyItNowPricecurrencyID
        {
            get { return _BuyItNowPricecurrencyID; }
            set { _BuyItNowPricecurrencyID = value; }
        }

        [DataMember]
        public string BuyItNowPriceValue
        {
            get { return _BuyItNowPriceValue; }
            set { _BuyItNowPriceValue = value; }
        }

        [DataMember]
        public string StartPricecurrencyID
        {
            get { return _StartPricecurrencyID; }
            set { _StartPricecurrencyID = value; }
        }

        [DataMember]
        public string StartPriceValue
        {
            get { return _StartPriceValue; }
            set { _StartPriceValue = value; }
        }

        [DataMember]
        public string CurrentPricecurrencyID
        {
            get { return _CurrentPricecurrencyID; }
            set { _CurrentPricecurrencyID = value; }
        }

        [DataMember]
        public string CurrentPriceValue
        {
            get { return _CurrentPriceValue; }
            set { _CurrentPriceValue = value; }
        }

        [DataMember]
        public string ShippingType
        {
            get { return _ShippingType; }
            set { _ShippingType = value; }
        }

        [DataMember]
        public string ListingDetailsStartTime
        {
            get { return _ListingDetailsStartTime; }
            set { _ListingDetailsStartTime = value; }
        }

        [DataMember]
        public string ListingDetailsTimeLeft
        {
            get { return _ListingDetailsTimeLeft; }
            set { _ListingDetailsTimeLeft = value; }
        }

        [DataMember]
        public string Height
        {
            get { return _Height; }
            set { _Height = value; }
        }

        [DataMember]
        public string Weight
        {
            get { return _Weight; }
            set { _Weight = value; }
        }

        [DataMember]
        public string Length
        {
            get { return _Length; }
            set { _Length = value; }
        }

        [DataMember]
        public string Width
        {
            get { return _Width; }
            set { _Width = value; }
        }

        [DataMember]
        public string ShippingHeight
        {
            get { return _ShippingHeight; }
            set { _ShippingHeight = value; }
        }

        [DataMember]
        public string ShippingWeight
        {
            get { return _ShippingWeight; }
            set { _ShippingWeight = value; }
        }

        [DataMember]
        public string ShippingLength
        {
            get { return _ShippingLength; }
            set { _ShippingLength = value; }
        }

        [DataMember]
        public string ShippingWidth
        {
            get { return _ShippingWidth; }
            set { _ShippingWidth = value; }
        }

        [DataMember]
        public string StandardShippingCharge
        {
            get { return _StandardShippingCharge; }
            set { _StandardShippingCharge = value; }
        }

        [DataMember]
        public string StandardTaxCharge
        {
            get { return _StandardTaxCharge; }
            set { _StandardTaxCharge = value; }
        }

        [DataMember]
        public string WeightUOM
        {
            get { return _WeightUOM; }
            set { _WeightUOM = value; }
        }

        [DataMember]
        public string LengthUOM
        {
            get { return _LengthUOM; }
            set { _LengthUOM = value; }
        }

        [DataMember]
        public string ModelNumber
        {
            get { return _ModelNumber; }
            set { _ModelNumber = value; }
        }

        [DataMember]
        public string Brand
        {
            get { return _Brand; }
            set { _Brand = value; }
        }

        [DataMember]
        public string Color
        {
            get { return _Color; }
            set { _Color = value; }
        }

        [DataMember]
        public string Warranty
        {
            get { return _Warranty; }
            set { _Warranty = value; }
        }

        [DataMember]
        public string EbayItemId
        {
            get { return _EbayItemId; }
            set { _EbayItemId = value; }
        }

        //VariationsType _ItemVariations;
        //[DataMember]
        //public VariationsType ItemVariations
        //{
        //    get { return _ItemVariations; }
        //    set { _ItemVariations = value; }
        //}

        //NameValueListTypeCollection _ItemSpecifics;
        //[DataMember]
        //public NameValueListTypeCollection ItemSpecifics
        //{
        //    get { return _ItemSpecifics; }
        //    set { _ItemSpecifics = value; }
        //}

    }
}
