﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.IO;

using Ez.Newsletter.MagentoApi;

namespace Magento_Communicator
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMagentoCommunicator" in both code and config file together.
    [ServiceContract]
    public interface IMagentoCommunicator
    {
        [FaultContract(typeof(error))]
        [OperationContract]
        string AddNewProduct(string API_URL, string API_User, string API_Key, MemoryStream msProduct, string Quantity, string StoreId, string ProductType, List<byte[]> lstProductImages);

        [FaultContract(typeof(error))]
        [OperationContract]
        bool UpdateProduct(string API_URL, string API_User, string API_Key, string ApiItemId, MemoryStream msProduct, string Quantity, List<byte[]> lstProductImages);

        [FaultContract(typeof(error))]
        [OperationContract]
        bool UpdateInventory(string API_URL, string API_User, string API_Key, string ProductSKU, string Quantity);

        [FaultContract(typeof(error))]
        [OperationContract]
        List<string> GetOrderIds(string API_URL, string API_User, string API_Key, DateTime CreatedAt);

        [FaultContract(typeof(error))]
        [OperationContract]
        MemoryStream GetOrderReportFromID(string API_URL, string API_User, string API_Key, string OrderId);


        [FaultContract(typeof(error))]
        [OperationContract]
        bool UpdateTrackingNumber(string API_URL, string API_User, string API_Key, string OrderId, string TrackingNo, string ShippingCarrier);


        [FaultContract(typeof(error))]
        [OperationContract]
        Dictionary<string, string> GetProductAttributeSet(string API_URL, string API_User, string API_Key);

        [FaultContract(typeof(error))]
        [OperationContract]
        List<Product> GetProductDetailForItemID(string API_URL, string API_User, string API_Key, string ItemID);

        [FaultContract(typeof(error))]
        [OperationContract]
        List<Product> GetProductListings(string API_URL, string API_User, string API_Key, string StoreId);


        [FaultContract(typeof(error))]
        [OperationContract]
        List<string> GetOrderIdBetweenDates(string API_URL, string API_User, string API_Key, DateTime StartDate, DateTime EndDate);
    }

    [DataContract]
    public class error
    {
        [DataMember]
        public string msg;
        [DataMember]
        public string request;

    }
}
