﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.IO;

using System.Xml;
using System.Xml.Serialization;

using CookComputing.XmlRpc;
using Ez.Newsletter.MagentoApi;

namespace Magento_Communicator
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MagentoCommunicator" in both code and config file together.
    public class MagentoCommunicator : IMagentoCommunicator
    {
      
        /// <summary>
        /// 
        /// </summary>
        /// <param name="API_URL"></param>
        /// <param name="API_User"></param>
        /// <param name="API_Key"></param>
        /// <param name="msProduct"></param>
        /// <param name="StoreId"></param>
        /// <param name="ProductType"></param>
        /// <returns></returns>
        public string AddNewProduct(string API_URL, string API_User, string API_Key, MemoryStream msProduct,string Quantity, string StoreId, string ProductType,List<byte[]> lstProductImages)
        {
            Product objProduct;
            string SessionID = "";
            string ItemId = "";
            string WebStoreAPI_URL = "http://" + API_URL + "/index.php/api/xmlrpc";
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Product));
                XmlReader Reader = new XmlTextReader(msProduct);
                objProduct = (Product)serializer.Deserialize(Reader);

                SessionID = Connection.Login(WebStoreAPI_URL, API_User, API_Key);

                ItemId = Product.Create(WebStoreAPI_URL, SessionID, new object[] { ProductType, objProduct.set, objProduct.sku, objProduct, StoreId });
                int i = 0;

                Inventory InventoryUpdate = new Inventory();
                if (Convert.ToInt32(Quantity) > 0)
                    InventoryUpdate.is_in_stock = "1";
                else
                    InventoryUpdate.is_in_stock = "0";
                InventoryUpdate.qty = Quantity;
                bool wasInventoryUpdated = Inventory.Update(WebStoreAPI_URL, SessionID, new object[] { ItemId, InventoryUpdate });

                foreach (byte[] bytarrProductImage in lstProductImages)
                {
                    i = i + 1;
                    ProductImageFile objProductImageFile = new ProductImageFile();
                    ProductImageFile.Data ProductImageFileData = new ProductImageFile.Data();
                    ProductImageFileData.content = System.Convert.ToBase64String(bytarrProductImage);
                    ProductImageFileData.mime = "image/gif";
                    objProductImageFile.file = ProductImageFileData;
                    objProductImageFile.label = objProduct.sku + i.ToString("00");
                    objProductImageFile.types = new string[] { "image", "small_image", "thumbnail" };
                    objProductImageFile.exclude = "0";
                    string imageName = ProductImage.Create(WebStoreAPI_URL, SessionID, new object[] { ItemId, objProductImageFile });
                }
                Connection.EndSession(WebStoreAPI_URL, SessionID);
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
            return ItemId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="API_URL"></param>
        /// <param name="API_User"></param>
        /// <param name="API_Key"></param>
        /// <param name="msProduct"></param>
        /// <returns></returns>
        public bool UpdateProduct(string API_URL, string API_User, string API_Key, string ApiItemId, MemoryStream msProduct, string Quantity, List<byte[]> lstProductImages)
        {
            Product objProduct;
            string SessionID = "";
            bool wasProductUpdated = false;
            string WebStoreAPI_URL = "http://" + API_URL + "/index.php/api/xmlrpc";
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Product));
                XmlReader Reader = new XmlTextReader(msProduct);
                objProduct = (Product)serializer.Deserialize(Reader);
                SessionID = Connection.Login(WebStoreAPI_URL, API_User, API_Key);

                wasProductUpdated = Product.Update(WebStoreAPI_URL, SessionID, new object[] { ApiItemId, objProduct });
                int i = 0;
                bool wasRemoved = false;

                Inventory InventoryUpdate = new Inventory();
                if (Convert.ToInt32(Quantity) > 0)
                    InventoryUpdate.is_in_stock = "1";
                else
                    InventoryUpdate.is_in_stock = "0";
                InventoryUpdate.qty = Quantity;
                bool wasInventoryUpdated = Inventory.Update(WebStoreAPI_URL, SessionID, new object[] { ApiItemId, InventoryUpdate });

                if (lstProductImages.Count > 0)
                {
                    //// gets all the images for the product 
                    ProductImage[] myProductImages = ProductImage.List(WebStoreAPI_URL, SessionID, new object[] { ApiItemId });
                    //remove existing Product images from Magento
                    foreach (ProductImage myProductImage in myProductImages)
                    {
                        wasRemoved = ProductImage.Remove(WebStoreAPI_URL, SessionID, new object[] { ApiItemId, myProductImage.file });
                    }
                    foreach (byte[] bytarrProductImage in lstProductImages)
                    {
                        i = i + 1;
                        ProductImageFile objProductImageFile = new ProductImageFile();
                        ProductImageFile.Data objProductImageFileData = new ProductImageFile.Data();
                        objProductImageFileData.content = System.Convert.ToBase64String(bytarrProductImage);
                        objProductImageFileData.mime = "image/gif";
                        objProductImageFile.file = objProductImageFileData;
                        objProductImageFile.label = objProduct.sku + i.ToString("00");
                        objProductImageFile.types = new string[] { "image", "small_image", "thumbnail" };
                        objProductImageFile.exclude = "0";
                        string imageName = ProductImage.Create(WebStoreAPI_URL, SessionID, new object[] { ApiItemId, objProductImageFile });
                        ProductImage UpdatedProductImageFile = new ProductImage();
                        UpdatedProductImageFile.label = objProduct.sku + i.ToString("00");
                        bool wasUpdated = ProductImage.Update(WebStoreAPI_URL, SessionID, new object[] { ApiItemId, imageName, UpdatedProductImageFile });
                    }
                }
                
                Connection.EndSession(WebStoreAPI_URL, SessionID);
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
            return wasProductUpdated;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="API_URL"></param>
        /// <param name="API_User"></param>
        /// <param name="API_Key"></param>
        /// <param name="ProductSKU"></param>
        /// <param name="Quantity"></param>
        /// <returns></returns>
        public bool UpdateInventory(string API_URL, string API_User, string API_Key, string ProductSKU, string Quantity)
        {
            string SessionID = "";
            bool wasInventoryUpdated = false;
            string WebStoreAPI_URL = "http://" + API_URL + "/index.php/api/xmlrpc";
            try
            {
                SessionID = Connection.Login(WebStoreAPI_URL, API_User, API_Key);
                Inventory InventoryUpdate = new Inventory();
                if(Convert.ToInt32(Quantity)>0)
                InventoryUpdate.is_in_stock = "1";
                else
                    InventoryUpdate.is_in_stock = "0";
                InventoryUpdate.qty = Quantity;
                wasInventoryUpdated = Inventory.Update(WebStoreAPI_URL, SessionID, new object[] { ProductSKU, InventoryUpdate });
                Connection.EndSession(WebStoreAPI_URL, SessionID);
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
            return wasInventoryUpdated;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="API_URL"></param>
        /// <param name="API_User"></param>
        /// <param name="API_Key"></param>
        /// <param name="CreatedAt"></param>
        /// <returns></returns>
        public List<string> GetOrderIds(string API_URL, string API_User, string API_Key, DateTime CreatedAt)
        {
            List<string> OrderIds = new List<string>();
            XmlRpcStruct filterOn = new XmlRpcStruct();
            XmlRpcStruct filterParams = new XmlRpcStruct();
            XmlRpcStruct filterParams1 = new XmlRpcStruct();

            string WebStoreAPI_URL = "http://" + API_URL + "/index.php/api/xmlrpc";
            string SessionID = "";
            try
            {
                SessionID = Connection.Login(WebStoreAPI_URL, API_User, API_Key);

                filterParams.Add("gt", CreatedAt.ToString("s"));

                filterParams1.Add("neq", "Canceled");
                //filterParams1.Add("eq", "complete");
                //filterParams1.Add("eq", "pending");
                //filterParams1.Add("eq", "processing");

                filterOn.Add("created_at", filterParams);
                filterOn.Add("status", filterParams1);
               

                Order[] myOrders = Order.List(WebStoreAPI_URL, SessionID, new object[] { filterOn });

                for (int i = 0; i < myOrders.Length; i++)
                {
                    OrderIds.Add(myOrders[i].increment_id);
                }

            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }

            return OrderIds;
        }

        public List<string> GetOrderIdBetweenDates(string API_URL, string API_User, string API_Key, DateTime StartDate, DateTime EndDate)
        {
            List<string> OrderIds = new List<string>();
            XmlRpcStruct filterOn = new XmlRpcStruct();
            XmlRpcStruct filterParams = new XmlRpcStruct();
            XmlRpcStruct filterParams1 = new XmlRpcStruct();
            XmlRpcStruct filterParams2 = new XmlRpcStruct();

            string WebStoreAPI_URL = "http://" + API_URL + "/index.php/api/xmlrpc";
            string SessionID = "";
            try
            {
                SessionID = Connection.Login(WebStoreAPI_URL, API_User, API_Key);

                filterParams.Add("from", StartDate.ToString("s"));
                filterParams.Add("to", EndDate.ToString("s"));

                filterParams2.Add("neq", "Canceled");
                //filterParams1.Add("eq", "complete");
                //filterParams1.Add("eq", "pending");
                //filterParams1.Add("eq", "processing");

                filterOn.Add("created_at", filterParams);
                //filterOn.Add("end_date", filterParams1);
                filterOn.Add("status", filterParams2);
               

                Order[] myOrders = Order.List(WebStoreAPI_URL, SessionID, new object[] { filterOn });

                for (int i = 0; i < myOrders.Length; i++)
                {
                    OrderIds.Add(myOrders[i].increment_id);
                }

            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }

            return OrderIds;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="API_URL"></param>
        /// <param name="API_User"></param>
        /// <param name="API_Key"></param>
        /// <param name="OrderId"></param>
        /// <returns></returns>
        public MemoryStream GetOrderReportFromID(string API_URL, string API_User, string API_Key, string OrderId)
        {
            MemoryStream ms = new MemoryStream();
            string WebStoreAPI_URL = "http://" + API_URL + "/index.php/api/xmlrpc";
            string SessionID = "";

            try
            {
                SessionID = Connection.Login(WebStoreAPI_URL, API_User, API_Key);
                OrderInfo myOrderInfo = Order.Info(WebStoreAPI_URL, SessionID, new object[] { OrderId });
                ms = SerializeToStream(myOrderInfo);
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }

            return ms;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="API_URL"></param>
        /// <param name="API_User"></param>
        /// <param name="API_Key"></param>
        /// <param name="OrderId"></param>
        /// <param name="TrackingNo"></param>
        /// <param name="ShippingCarrier"></param>
        public bool UpdateTrackingNumber(string API_URL, string API_User, string API_Key, string OrderId, string TrackingNo, string ShippingCarrier)
        {
            string SessionID = "";
            bool IsTrackingNoAdded = false;

            string WebStoreAPI_URL = "http://" + API_URL + "/index.php/api/xmlrpc";
            try
            {
                SessionID = Connection.Login(WebStoreAPI_URL, API_User, API_Key);
                string[] OrderIds = OrderId.Split('-');
                XmlRpcStruct filterOn1 = new XmlRpcStruct();
                XmlRpcStruct filterParams1 = new XmlRpcStruct();
                filterParams1.Add("eq", OrderIds[0]);
                filterOn1.Add("order_id", filterParams1);

                Shipment[] myShipment = Shipment.List(WebStoreAPI_URL, SessionID, new object[] { filterOn1 });
                
                if (myShipment.Length > 0)
                {
                    Shipment.AddTrack(WebStoreAPI_URL, SessionID, new object[] { myShipment[0].increment_id, ShippingCarrier, "added tracking number", TrackingNo });
                    IsTrackingNoAdded = true;
                }
                else
                {
                    string ShipmentId = Shipment.Create(WebStoreAPI_URL, SessionID, new object[] { OrderIds[1], new object[] { }, "Shipment Created", true, true });
                    string myShipmentTrackingNumber = Shipment.AddTrack(WebStoreAPI_URL, SessionID, new object[] { ShipmentId, ShippingCarrier, "added tracking number", TrackingNo });
                    IsTrackingNoAdded = true;
                }
                
                Connection.EndSession(WebStoreAPI_URL, SessionID);
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
            return IsTrackingNoAdded;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="API_URL"></param>
        /// <param name="API_User"></param>
        /// <param name="API_Key"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetProductAttributeSet(string API_URL, string API_User, string API_Key)
        {
            string SessionID = "";
            string WebStoreAPI_URL = "http://" + API_URL + "/index.php/api/xmlrpc";
            Dictionary<string, string> AttributeSetColl = new Dictionary<string, string>();
            string AttributeSetName = "";
            string AttributeSetID = "";
            try
            {

                SessionID = Connection.Login(WebStoreAPI_URL, API_User, API_Key);
                ProductAttributeSet[] ProductAttributeSets = ProductAttributeSet.List(WebStoreAPI_URL, SessionID);
                foreach (ProductAttributeSet Attribute in ProductAttributeSets)
                {
                    AttributeSetName = Attribute.name;
                    AttributeSetID = Attribute.set_id;
                    if (!AttributeSetColl.ContainsKey(AttributeSetID))
                    {
                        AttributeSetColl.Add(AttributeSetID, AttributeSetName);
                    }
                }
                Connection.EndSession(WebStoreAPI_URL, SessionID);
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
            return AttributeSetColl;
        }

        /// <summary>
        /// Xml Serializer Serialize the Object to MemormStream by XMLTextWriter
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="obj">Object</param>
        /// <returns>Memory Stream</returns>
        public static MemoryStream SerializeToStream<T>(T obj)
        {
            try
            {
                MemoryStream memorystream = new MemoryStream();
                XmlWriter writer = new XmlTextWriter(memorystream, Encoding.Unicode);
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                // Serialize using the XmlTextWriter.
                serializer.Serialize(writer, obj);
                memorystream.Position = 0;
                return memorystream;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="API_URL"></param>
        /// <param name="API_User"></param>
        /// <param name="API_Key"></param>
        /// <param name="StoreId"></param>
        /// <returns></returns>
        public List<Product> GetProductListings(string API_URL, string API_User, string API_Key, string StoreId)
        {
            List<Product> lstProducts = new List<Product>();
            ProductAttribute[] arrAttributes;
            Product objProduct1 = new Product();
            string SessionID = "";
            string WebStoreAPI_URL = "http://" + API_URL + "/index.php/api/xmlrpc";
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Product));
                SessionID = Connection.Login(WebStoreAPI_URL, API_User, API_Key);
                //XmlRpcStruct filterOn = new XmlRpcStruct();
                //XmlRpcStruct filterParams = new XmlRpcStruct();
                //filterParams.Add("gt", 0);
                //filterOn.Add("inventory", filterParams);
                Product[] myProducts = Product.List(WebStoreAPI_URL, SessionID, new object[] {  });
                foreach (Product objProduct in myProducts)
                {
                    try
                    {
                        objProduct1 = Product.Info(WebStoreAPI_URL, SessionID, new object[] { objProduct.product_id });
                        //arrAttributes = ProductAttribute.List(WebStoreAPI_URL, SessionID, new object[] { objProduct.product_id });

                        //foreach (ProductAttribute myProductAttribute in arrAttributes)
                        //{
                        //    string code1 = myProductAttribute.code;
                        //    string attribute_id = myProductAttribute.attribute_id;
                        //    string required = myProductAttribute.required;
                        //    string scope = myProductAttribute.scope;
                        //    string type = myProductAttribute.type;

                        //}
                        //ProductAttributeOption[] myProductAttributeOptions = ProductAttribute.Options(WebStoreAPI_URL, SessionID, new object[] { objProduct.product_id });
                        //foreach (ProductAttributeOption myProductAttributeOption in myProductAttributeOptions)
                        //{
                        //    Console.WriteLine(myProductAttributeOption.label);
                        //}
                        try
                        {
                            Inventory[] myInventoryList = Inventory.List(WebStoreAPI_URL, SessionID, new object[] { objProduct.product_id });
                            foreach (Inventory myInventoryItem in myInventoryList)
                            {
                                objProduct1.Quantity = myInventoryItem.qty;
                            }
                        }
                        catch (Exception ex)
                        {
                            objProduct1.Quantity = "0";
                        }
                        lstProducts.Add(objProduct1);
                    }
                    catch (Exception)
                    {
                    }
                }
                Connection.EndSession(WebStoreAPI_URL, SessionID);
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
            return lstProducts;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="API_URL"></param>
        /// <param name="SessionID"></param>
        /// <param name="SellerSKU"></param>
        /// <returns></returns>
        public Product GetProductDetail(string API_URL, string SessionID, string ItemID)
        {
            List<Product> lstProducts = new List<Product>();
            Product objProduct = new Product();

            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Product));
                XmlRpcStruct filterOn = new XmlRpcStruct();
                XmlRpcStruct filterParams = new XmlRpcStruct();
                filterParams.Add("like", ItemID);
                filterOn.Add("product_id", filterParams);
                objProduct = Product.Info(API_URL, SessionID, new object[] { ItemID });
                
                //lstProducts.Add(objProduct);
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
            return objProduct;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="API_URL"></param>
        /// <param name="API_User"></param>
        /// <param name="API_Key"></param>
        /// <param name="ItemID"></param>
        /// <returns></returns>
        public List<Product> GetProductDetailForItemID(string API_URL, string API_User, string API_Key, string ItemID)
        {
            List<Product> lstProducts = new List<Product>();
           
            string SessionID = "";
            string WebStoreAPI_URL = "http://" + API_URL + "/index.php/api/xmlrpc";
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Product));
                SessionID = Connection.Login(WebStoreAPI_URL, API_User, API_Key);
                XmlRpcStruct filterOn = new XmlRpcStruct();
                XmlRpcStruct filterParams = new XmlRpcStruct();
                filterParams.Add("like", ItemID);
                filterOn.Add("product_id", filterParams);

                // Product objProduct = Product.Info(WebStoreAPI_URL, SessionID, new object[] {ItemID });

                Product[] myProducts = Product.List(WebStoreAPI_URL, SessionID, new object[] { filterOn });
                foreach (Product objProduct in myProducts)
                {
                    Product objProduct1 = new Product();
                    try
                    {
                        objProduct1 = Product.Info(WebStoreAPI_URL, SessionID, new object[] { objProduct.product_id });
                        Inventory[] myInventoryList = Inventory.List(WebStoreAPI_URL, SessionID, new object[] { objProduct.product_id });
                        foreach (Inventory myInventoryItem in myInventoryList)
                        {
                            objProduct1.Quantity = myInventoryItem.qty;
                        }
                    }
                    catch (Exception ex)
                    {
                        objProduct1.Quantity = "0";
                    }
                    lstProducts.Add(objProduct1);
                }

                Connection.EndSession(WebStoreAPI_URL, SessionID);
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
            return lstProducts;
        }
    }
}
