﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Data;


namespace AMWS_Communicator
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IAmazonCommunicator" in both code and config file together.
    [ServiceContract]
    public interface IAmazonCommunicator
    {
        [FaultContract(typeof(error))]
        [OperationContract]
        int GetFeedSubmissionCount(ServiceConfigInfo objSvcConfigInfo);

        [FaultContract(typeof(error))]
        [OperationContract]
        string SubmitFeed(ServiceConfigInfo objSvcConfigInfo, string FeedType, MemoryStream FeedContent);

        [FaultContract(typeof(error))]
        [OperationContract]
        MemoryStream FeedSubmissionResult(ServiceConfigInfo objSvcConfigInfo, string FeedSubmissionId);


        [FaultContract(typeof(error))]
        [OperationContract]
        // COrdersList GetOrdersList(ServiceConfigInfo objSvcConfigInfo);
        COrdersList GetOrdersList(ServiceConfigInfo objSvcConfigInfo, DateTime CreatedAfter, DateTime CreatedBefore);

        [FaultContract(typeof(error))]
        [OperationContract]
        CItemsList GetOrderItemsList(ServiceConfigInfo objSvcConfigInfo, string AmazonOrderId);

        [FaultContract(typeof(error))]
        [OperationContract]
        COrdersList GetOrderDetails(ServiceConfigInfo objSvcConfigInfo, string[] AmazonOrderId);


        [FaultContract(typeof(error))]
        [OperationContract]
        Dictionary<string, string> GetASIN_From_SKU(ServiceConfigInfo objSvcConfigInfo, List<string> lstSKU);

        [FaultContract(typeof(error))]
        [OperationContract]
        string RequestReport(ServiceConfigInfo objSvcConfigInfo, string ReportType, string ReportOptions = "");

        [FaultContract(typeof(error))]
        [OperationContract]
        MemoryStream GetReportFromReportID(ServiceConfigInfo objSvcConfigInfo, string ReportID);

        [FaultContract(typeof(error))]
        [OperationContract]
        void ManagedReportSchedule(ServiceConfigInfo objSvcConfigInfo, string ReportType, string Schedule);

        [FaultContract(typeof(error))]
        [OperationContract]
        void UpdateReportAcknowledgement(ServiceConfigInfo objSvcConfigInfo, string[] ReportIds);

        [FaultContract(typeof(error))]
        [OperationContract]
        string[] GetReportList(ServiceConfigInfo objSvcConfigInfo, DateTime FromDate, DateTime ToDate, string ReportType = "");

        [FaultContract(typeof(error))]
        [OperationContract]
        string[] GetAcknowledgedReportList(ServiceConfigInfo objSvcConfigInfo, DateTime FromDate, DateTime ToDate, string ReportType = "");

        [FaultContract(typeof(error))]
        [OperationContract]
        AmazonProductList GetProductDetailsForSellerSKU(ServiceConfigInfo objSvcConfigInfo, string SellerSKU, ref DataTable dtProductListings, DataTable dtCFields = null, DataTable dtCFValues = null);

        [FaultContract(typeof(error))]
        [OperationContract]
        AmazonProduct GetProductDetailsForASIN(ServiceConfigInfo objSvcConfigInfo, List<string> ASINList, ref VariationCollection vCollection, ref DataTable dtProductListings);

        [FaultContract(typeof(error))]
        [OperationContract]
        AmazonProductList GetOrderProductDetailsForSellerSKU(ServiceConfigInfo objSvcConfigInfo, string SellerSKU, ref DataTable dtProductListings, DataTable dtCFields = null, DataTable dtCFValues = null);

    }

    [DataContract]
    public class ServiceConfigInfo
    {
        #region Fields

        string _ServiceCountryCode, _MerchantID, _MarketPlaceID, _AccessKeyID, _SecretKeyValue, _FeedsServiceURL, _OrdersServiceURL, _ProductServiceURL;




        int _ServiceURL_Type;

        #endregion

        #region Fields

        [DataMember]
        public string ServiceCountryCode
        {
            get { return _ServiceCountryCode; }
            set { _ServiceCountryCode = value; }
        }

        [DataMember]
        public string MerchantID
        {
            get { return _MerchantID; }
            set { _MerchantID = value; }
        }

        [DataMember]
        public string MarketPlaceID
        {
            get { return _MarketPlaceID; }
            set { _MarketPlaceID = value; }
        }

        [DataMember]
        public string AccessKeyID
        {
            get { return _AccessKeyID; }
            set { _AccessKeyID = value; }
        }

        [DataMember]
        public string SecretKeyValue
        {
            get { return _SecretKeyValue; }
            set { _SecretKeyValue = value; }
        }

        [DataMember]
        public string FeedsServiceURL
        {
            get { return _FeedsServiceURL; }
            set { _FeedsServiceURL = value; }
        }

        [DataMember]
        public string OrdersServiceURL
        {
            get { return _OrdersServiceURL; }
            set { _OrdersServiceURL = value; }
        }

        [DataMember]
        public string ProductServiceURL
        {
            get { return _ProductServiceURL; }
            set { _ProductServiceURL = value; }
        }

        [DataMember]
        public int ServiceURL_Type
        {
            get { return _ServiceURL_Type; }
            set { _ServiceURL_Type = value; }
        }


        #endregion
    }

    public enum ServiceURL_Type { OrdersService, FeedsService }

    [DataContract(Name = "ServiceURL")]
    public enum ServiceURL_Type1
    {
        [EnumMember]
        OrdersService,
        [EnumMember]
        FeedsService,
    }

    [DataContract]
    [Serializable]
    public class CItemsList
    {

        List<CItems> _ItemsList;
        [DataMember]
        public List<CItems> ItemsList
        {
            get { return _ItemsList; }
            set { _ItemsList = value; }
        }
    }

    [DataContract]
    [Serializable]
    public class COrdersList
    {
        string _CreatedBefore, _LastUpdatedBefore;

        [DataMember]
        public string CreatedBefore
        {
            get { return _CreatedBefore; }
            set { _CreatedBefore = value; }
        }

        [DataMember]
        public string LastUpdatedBefore
        {
            get { return _LastUpdatedBefore; }
            set { _LastUpdatedBefore = value; }
        }

        //
        List<COrders> _Orders;
        [DataMember]
        public List<COrders> Orders
        {
            get { return _Orders; }
            set { _Orders = value; }
        }
    }

    [Serializable]
    [DataContract]
    public class COrders
    {
        #region Fields

        decimal _NumberOfItemsShipped, _NumberOfItemsUnshipped;

        CAddress _ShippingAddress;
        List<CPaymentExecutionDetailItem> _PaymentExecItems;

        string _AmazonOrderId, _SellerOrderId, _PurchaseDate, _LastUpdateDate, _OrderStatus, _FulfillmentChannel, _SalesChannel, _OrderChannel,
            _ShipmentServiceLevelCategory, _ShipServiceLevel, _MarketplaceId, _BuyerName, _BuyerEmail, _PaymentMethod, _subPaymentMethodField;

        List<CItems> _ItemsList;
        CMoney _OrderTotal;

        #endregion Fields

        #region Properties

        [DataMember]
        public List<CItems> ItemsList
        {
            get { return _ItemsList; }
            set { _ItemsList = value; }
        }

        [DataMember]
        public string AmazonOrderId
        {
            get { return _AmazonOrderId; }
            set { _AmazonOrderId = value; }
        }
        [DataMember]
        public string SellerOrderId
        {
            get { return _SellerOrderId; }
            set { _SellerOrderId = value; }
        }
        [DataMember]
        public string PurchaseDate
        {
            get { return _PurchaseDate; }
            set { _PurchaseDate = value; }
        }
        [DataMember]
        public string LastUpdateDate
        {
            get { return _LastUpdateDate; }
            set { _LastUpdateDate = value; }
        }
        [DataMember]
        public string OrderStatus
        {
            get { return _OrderStatus; }
            set { _OrderStatus = value; }
        }
        [DataMember]
        public string FulfillmentChannel
        {
            get { return _FulfillmentChannel; }
            set { _FulfillmentChannel = value; }
        }
        [DataMember]
        public string SalesChannel
        {
            get { return _SalesChannel; }
            set { _SalesChannel = value; }
        }
        [DataMember]
        public string OrderChannel
        {
            get { return _OrderChannel; }
            set { _OrderChannel = value; }
        }
        [DataMember]
        public string ShipServiceLevel
        {
            get { return _ShipServiceLevel; }
            set { _ShipServiceLevel = value; }
        }
        [DataMember]
        public string MarketplaceId
        {
            get { return _MarketplaceId; }
            set { _MarketplaceId = value; }
        }
        [DataMember]
        public string BuyerName
        {
            get { return _BuyerName; }
            set { _BuyerName = value; }
        }
        [DataMember]
        public string BuyerEmail
        {
            get { return _BuyerEmail; }
            set { _BuyerEmail = value; }
        }
        [DataMember]
        public string PaymentMethod
        {
            get { return _PaymentMethod; }
            set { _PaymentMethod = value; }
        }
        [DataMember]
        public string SubPaymentMethodField
        {
            get { return _subPaymentMethodField; }
            set { _subPaymentMethodField = value; }
        }

        [DataMember]
        public decimal NumberOfItemsShipped
        {
            get { return _NumberOfItemsShipped; }
            set { _NumberOfItemsShipped = value; }
        }
        [DataMember]
        public decimal NumberOfItemsUnshipped
        {
            get { return _NumberOfItemsUnshipped; }
            set { _NumberOfItemsUnshipped = value; }
        }
        [DataMember]
        public CAddress ShippingAddress
        {
            get { return _ShippingAddress; }
            set { _ShippingAddress = value; }
        }

        [DataMember]
        public List<CPaymentExecutionDetailItem> PaymentExecItems
        {
            get { return _PaymentExecItems; }
            set { _PaymentExecItems = value; }
        }
        [DataMember]
        public CMoney OrderTotal
        {
            get { return _OrderTotal; }
            set { _OrderTotal = value; }
        }

        [DataMember]
        public string ShipmentServiceLevelCategory
        {
            get { return _ShipmentServiceLevelCategory; }
            set { _ShipmentServiceLevelCategory = value; }
        }
        #endregion Properties
    }

    [Serializable]
    [DataContract]
    public class CItems
    {
        #region Fields

        string _AmazonOrderId, _ASIN, _SellerSKU, _OrderItemId, _Title, _GiftMessageText, _GiftWrapLevel;
        decimal _QuantityOrdered, _QuantityShipped;
        CMoney _ItemPrice, _shippingPrice, _giftWrapPrice, _itemTax, _shippingTax, _giftWrapTax,
        _shippingDiscount, _promotionDiscount, _CODFee, _CODFeeDiscount;

        #endregion Fields

        #region Properties
        [DataMember]
        public string AmazonOrderId
        {
            get { return _AmazonOrderId; }
            set { _AmazonOrderId = value; }
        }
        [DataMember]
        public string ASIN
        {
            get { return _ASIN; }
            set { _ASIN = value; }
        }
        [DataMember]
        public string SellerSKU
        {
            get { return _SellerSKU; }
            set { _SellerSKU = value; }
        }
        [DataMember]
        public string OrderItemId
        {
            get { return _OrderItemId; }
            set { _OrderItemId = value; }
        }
        [DataMember]
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }
        [DataMember]
        public string GiftMessageText
        {
            get { return _GiftMessageText; }
            set { _GiftMessageText = value; }
        }
        [DataMember]
        public string GiftWrapLevel
        {
            get { return _GiftWrapLevel; }
            set { _GiftWrapLevel = value; }
        }

        [DataMember]
        public decimal QuantityOrdered
        {
            get { return _QuantityOrdered; }
            set { _QuantityOrdered = value; }
        }
        [DataMember]
        public decimal QuantityShipped
        {
            get { return _QuantityShipped; }
            set { _QuantityShipped = value; }
        }
        [DataMember]
        public CMoney ItemPrice
        {
            get { return _ItemPrice; }
            set { _ItemPrice = value; }
        }
        [DataMember]
        public CMoney ShippingPrice
        {
            get { return _shippingPrice; }
            set { _shippingPrice = value; }
        }
        [DataMember]
        public CMoney GiftWrapPrice
        {
            get { return _giftWrapPrice; }
            set { _giftWrapPrice = value; }
        }
        [DataMember]
        public CMoney ItemTax
        {
            get { return _itemTax; }
            set { _itemTax = value; }
        }
        [DataMember]
        public CMoney ShippingTax
        {
            get { return _shippingTax; }
            set { _shippingTax = value; }
        }
        [DataMember]
        public CMoney GiftWrapTax
        {
            get { return _giftWrapTax; }
            set { _giftWrapTax = value; }
        }
        [DataMember]
        public CMoney ShippingDiscount
        {
            get { return _shippingDiscount; }
            set { _shippingDiscount = value; }
        }
        [DataMember]
        public CMoney PromotionDiscount
        {
            get { return _promotionDiscount; }
            set { _promotionDiscount = value; }
        }
        [DataMember]
        public CMoney CODFee
        {
            get { return _CODFee; }
            set { _CODFee = value; }
        }
        [DataMember]
        public CMoney CODFeeDiscount
        {
            get { return _CODFeeDiscount; }
            set { _CODFeeDiscount = value; }
        }

        #endregion Properties
    }

    [Serializable]
    [DataContract]
    public class CAddress
    {
        #region Fields

        string _name, _addressLine1, _addressLine2, _addressLine3, _city, _county,
                _district, _stateOrRegion, _postalCode, _countryCode, _phone;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        [DataMember]
        public string AddressLine1
        {
            get { return _addressLine1; }
            set { _addressLine1 = value; }
        }
        [DataMember]
        public string AddressLine2
        {
            get { return _addressLine2; }
            set { _addressLine2 = value; }
        }
        [DataMember]
        public string AddressLine3
        {
            get { return _addressLine3; }
            set { _addressLine3 = value; }
        }
        [DataMember]
        public string City
        {
            get { return _city; }
            set { _city = value; }
        }
        [DataMember]
        public string County
        {
            get { return _county; }
            set { _county = value; }
        }
        [DataMember]
        public string District
        {
            get { return _district; }
            set { _district = value; }
        }
        [DataMember]
        public string StateOrRegion
        {
            get { return _stateOrRegion; }
            set { _stateOrRegion = value; }
        }
        [DataMember]
        public string PostalCode
        {
            get { return _postalCode; }
            set { _postalCode = value; }
        }
        [DataMember]
        public string CountryCode
        {
            get { return _countryCode; }
            set { _countryCode = value; }
        }
        [DataMember]
        public string Phone
        {
            get { return _phone; }
            set { _phone = value; }
        }

        #endregion Fields

    }

    [Serializable]
    [DataContract]
    public class CMoney
    {
        #region Fields

        string _currencyCode, _amount;

        #endregion Fields

        #region Properties
        [DataMember]
        public string CurrencyCode
        {
            get { return _currencyCode; }
            set { _currencyCode = value; }
        }

        [DataMember]
        public string Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }


        #endregion Properties
    }

    [Serializable]
    [DataContract]
    public class CPaymentExecutionDetailItem
    {
        #region Fields

        CMoney _paymentField;
        string _subPaymentMethodField;

        #endregion Fields

        #region Properties

        [DataMember]
        public CMoney PaymentField
        {
            get { return _paymentField; }
            set { _paymentField = value; }
        }

        [DataMember]
        public string SubPaymentMethodField
        {
            get { return _subPaymentMethodField; }
            set { _subPaymentMethodField = value; }
        }

        #endregion Properties
    }

    [DataContract]
    public class error
    {

        //[DataMember]
        //public FaultCode errorcode;
        [DataMember]
        public string msg;
        [DataMember]
        public string request;

    }

    [DataContract]
    public class VariationCollection
    {
        List<Variations> _VariationList;
        List<VariationDetails> _VariationDetail;

        [DataMember]
        public List<Variations> VariationList
        {
            get { return _VariationList; }
            set { _VariationList = value; }
        }

        [DataMember]
        public List<VariationDetails> VariationDetail
        {
            get { return _VariationDetail; }
            set { _VariationDetail = value; }
        }
    }

    [DataContract]
    public class Variations
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Value { get; set; }
    }

    [DataContract]
    public class VariationDetails
    {
        [DataMember]
        public string VariationTitle { get; set; }

        [DataMember]
        public string VariationName { get; set; }

        [DataMember]
        public string VariationValue { get; set; }

        [DataMember]
        public int Quantity { get; set; }

        [DataMember]
        public int ReOrder { get; set; }

        [DataMember]
        public string SKU { get; set; }

        [DataMember]
        public Double ListPrice { get; set; }

    }

    [DataContract]
    public class AmazonProductList
    {
        List<AmazonProduct> lstAmazonProduct;

        [DataMember]
        public List<AmazonProduct> LstAmazonProduct
        {
            get { return lstAmazonProduct; }
            set { lstAmazonProduct = value; }
        }
    }

    [DataContract]
    public class AmazonProduct
    {
        string _ASIN, _ProductId, _Title, _Description, _Manufacturer, _UPC, _Price, _Quantity, _Availability, _TargetCountry, _ProductType, _ProductImageURL,
            _Height, _Weight, _Length, _Width, _ShippingHeight, _ShippingWeight, _ShippingLength, _ShippingWidth,
        _StandardShippingCharge, _StandardTaxCharge, _WeightUOM, _LengthUOM, _ModelNumber, _Brand, _Color, _Warranty;


        string _SellerId, _SellerSKU;

        VariationCollection _ItemVariations;

        [DataMember]
        public VariationCollection ItemVariations
        {
            get { return _ItemVariations; }
            set { _ItemVariations = value; }
        }

        [DataMember]
        public string Color
        {
            get { return _Color; }
            set { _Color = value; }
        }
        [DataMember]
        public string Warranty
        {
            get { return _Warranty; }
            set { _Warranty = value; }
        }
        [DataMember]
        public string Brand
        {
            get { return _Brand; }
            set { _Brand = value; }
        }


        [DataMember]
        public string SellerSKU
        {
            get { return _SellerSKU; }
            set { _SellerSKU = value; }
        }

        [DataMember]
        public string SellerId
        {
            get { return _SellerId; }
            set { _SellerId = value; }
        }

        [DataMember]
        public string ASIN
        {
            get { return _ASIN; }
            set { _ASIN = value; }
        }

        [DataMember]
        public string ModelNumber
        {
            get { return _ModelNumber; }
            set { _ModelNumber = value; }
        }

        [DataMember]
        public string ProductType
        {
            get { return _ProductType; }
            set { _ProductType = value; }
        }

        [DataMember]
        public string WeightUOM
        {
            get { return _WeightUOM; }
            set { _WeightUOM = value; }
        }

        [DataMember]
        public string LengthUOM
        {
            get { return _LengthUOM; }
            set { _LengthUOM = value; }
        }

        [DataMember]
        public string ProductId
        {
            get { return _ProductId; }
            set { _ProductId = value; }
        }

        [DataMember]
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        [DataMember]
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        [DataMember]
        public string Manufacturer
        {
            get { return _Manufacturer; }
            set { _Manufacturer = value; }
        }

        [DataMember]
        public string UPC
        {
            get { return _UPC; }
            set { _UPC = value; }
        }

        [DataMember]
        public string Price
        {
            get { return _Price; }
            set { _Price = value; }
        }

        [DataMember]
        public string Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }

        [DataMember]
        public string Availability
        {
            get { return _Availability; }
            set { _Availability = value; }
        }

        [DataMember]
        public string TargetCountry
        {
            get { return _TargetCountry; }
            set { _TargetCountry = value; }
        }


        [DataMember]
        public string ProductImageURL
        {
            get { return _ProductImageURL; }
            set { _ProductImageURL = value; }
        }

        [DataMember]
        public string Height
        {
            get { return _Height; }
            set { _Height = value; }
        }

        [DataMember]
        public string Weight
        {
            get { return _Weight; }
            set { _Weight = value; }
        }

        [DataMember]
        public string Length
        {
            get { return _Length; }
            set { _Length = value; }
        }

        [DataMember]
        public string Width
        {
            get { return _Width; }
            set { _Width = value; }
        }

        [DataMember]
        public string ShippingHeight
        {
            get { return _ShippingHeight; }
            set { _ShippingHeight = value; }
        }

        [DataMember]
        public string ShippingWeight
        {
            get { return _ShippingWeight; }
            set { _ShippingWeight = value; }
        }

        [DataMember]
        public string ShippingLength
        {
            get { return _ShippingLength; }
            set { _ShippingLength = value; }
        }

        [DataMember]
        public string ShippingWidth
        {
            get { return _ShippingWidth; }
            set { _ShippingWidth = value; }
        }

        [DataMember]
        public string StandardShippingCharge
        {
            get { return _StandardShippingCharge; }
            set { _StandardShippingCharge = value; }
        }

        [DataMember]
        public string StandardTaxCharge
        {
            get { return _StandardTaxCharge; }
            set { _StandardTaxCharge = value; }
        }
    }

}
