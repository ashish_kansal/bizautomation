﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.IO;

using MarketplaceWebServiceOrders;
using MarketplaceWebServiceOrders.Model;

using MarketplaceWebService;
using MarketplaceWebService.Model;

using MarketplaceWebServiceProducts;
using MarketplaceWebServiceProducts.Model;

using System.Xml;
using System.Xml.Linq;

using System.Runtime.Serialization.Formatters.Binary;
using System.Data;


namespace AMWS_Communicator
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AmazonCommunicator" in code, svc and config file together.
    public class AmazonCommunicator : IAmazonCommunicator
    {
        #region Members

        Dictionary<string, string> SKU_ASIN = new Dictionary<string, string>();
        MarketplaceWebServiceOrdersClient OrdersService = null;
        MarketplaceWebServiceOrdersConfig OrdersConfig = null;

        MarketplaceWebServiceProductsClient ProductsService = null;
        MarketplaceWebServiceProductsConfig ProductsConfig = null;//new MarketplaceWebServiceProductsConfig();

        MarketplaceWebServiceConfig FeedsConfig = null;
        MarketplaceWebServiceClient FeedsService = null;

        const string ApplicationName = "BizAutomation";
        const string ApplicationVersion = "V 1.0";



        #endregion Members

        #region Amazon Service Configuration Functions

        /// <summary>
        /// Configs the Service URL to Process the WebRequest
        /// </summary>
        /// <param name="objConfigInfo">DataContract that brings in the Service Cofiguration Information</param>
        private void LoginConfigurations(ServiceConfigInfo objConfigInfo)
        {
            try
            {
                if (objConfigInfo.ServiceURL_Type == 1)//Order Service Library
                {
                    OrdersConfig = new MarketplaceWebServiceOrdersConfig();
                    OrdersConfig.ServiceURL = objConfigInfo.OrdersServiceURL;
                    OrdersService = new MarketplaceWebServiceOrdersClient(
                                       ApplicationName, ApplicationVersion, objConfigInfo.AccessKeyID, objConfigInfo.SecretKeyValue, OrdersConfig);
                    OrdersService =
                          new MarketplaceWebServiceOrdersClient(
                             ApplicationName,
                              ApplicationVersion,
                              objConfigInfo.AccessKeyID,
                              objConfigInfo.SecretKeyValue,
                              OrdersConfig);
                }
                else
                    if (objConfigInfo.ServiceURL_Type == 2)//Feeds Service Library
                    {
                        FeedsConfig = new MarketplaceWebServiceConfig();
                        FeedsConfig.ServiceURL = objConfigInfo.FeedsServiceURL;
                        FeedsService =
                          new MarketplaceWebServiceClient(
                              objConfigInfo.AccessKeyID,
                              objConfigInfo.SecretKeyValue,
                              ApplicationName,
                              ApplicationVersion,
                              FeedsConfig);
                    }
                    else
                        if (objConfigInfo.ServiceURL_Type == 3)//Products Service Library
                        {
                            ProductsConfig = new MarketplaceWebServiceProductsConfig();
                            ProductsConfig.ServiceURL = objConfigInfo.ProductServiceURL;
                            ProductsService =
                              new MarketplaceWebServiceProductsClient(ApplicationName,
                                  ApplicationVersion,
                                  objConfigInfo.AccessKeyID,
                                  objConfigInfo.SecretKeyValue,
                                  ProductsConfig);
                        }

            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Amazon Service Configuration Functions

        #region Operation Contracts

        #region Feed Operations

        // OperationContract Exposed to Client Application
        /// <summary>
        /// Get Feed Submission Count
        /// </summary>
        /// <param name="objSvcConfigInfo">DataContract that brings in the Service Cofiguration Informations</param>
        /// <returns>FeedSubmissionCount</returns>
        public int GetFeedSubmissionCount(ServiceConfigInfo objSvcConfigInfo)
        {
            int FeedSubmissionCount = 0;
            try
            {
                LoginConfigurations(objSvcConfigInfo);
                GetFeedSubmissionCountRequest request = new GetFeedSubmissionCountRequest();
                request.Merchant = objSvcConfigInfo.MerchantID;
                GetFeedSubmissionCountResponse response = FeedsService.GetFeedSubmissionCount(request);

                if (response.IsSetGetFeedSubmissionCountResult())
                {
                    GetFeedSubmissionCountResult getFeedSubmissionCountResult = response.GetFeedSubmissionCountResult;
                    if (getFeedSubmissionCountResult.IsSetCount())
                    {
                        FeedSubmissionCount = (int)getFeedSubmissionCountResult.Count;
                    }
                }
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }

            return FeedSubmissionCount;
        }

        // OperationContract Exposed to Client Application
        /// <summary>
        /// Summits the Feed Content to Amazon MWS
        /// </summary>
        /// <param name="objSvcConfigInfo">DataContract that brings in the Service Cofiguration Informations</param>
        /// <param name="FeedType">FeedType</param>
        /// <param name="FeedContent">FeedContent</param>
        /// <returns>FeedSubmissionId</returns>
        public string SubmitFeed(ServiceConfigInfo objSvcConfigInfo, string FeedType, MemoryStream FeedContent)
        {
            string FeedSubmissionId = "";
            try
            {
                LoginConfigurations(objSvcConfigInfo);
                SubmitFeedRequest request = new SubmitFeedRequest();
                request.Merchant = objSvcConfigInfo.MerchantID;
                request.MarketplaceIdList = new IdList();
                request.MarketplaceIdList.Id = new List<string>(new string[] { objSvcConfigInfo.MarketPlaceID });
                request.FeedContent = FeedContent;
                //string path = System.Web.Hosting.HostingEnvironment.MapPath("~/NewFeed1.xml");
                //request.FeedContent = File.Open(path, FileMode.Open, FileAccess.Read);
                //request.FeedContent = StringToStream(FeedContent);
                request.ContentMD5 = MarketplaceWebServiceClient.CalculateContentMD5(request.FeedContent);
                request.FeedContent.Position = 0;
                request.FeedType = FeedType;

                SubmitFeedResponse response = FeedsService.SubmitFeed(request);
                if (response.IsSetSubmitFeedResult())
                {
                    SubmitFeedResult submitFeedResult = response.SubmitFeedResult;
                    if (submitFeedResult.IsSetFeedSubmissionInfo())
                    {

                        FeedSubmissionInfo feedSubmissionInfo = submitFeedResult.FeedSubmissionInfo;
                        if (feedSubmissionInfo.IsSetFeedSubmissionId())
                        {
                            FeedSubmissionId = feedSubmissionInfo.FeedSubmissionId;
                        }
                        if (feedSubmissionInfo.IsSetFeedType())
                        {
                            string FeedType1 = feedSubmissionInfo.FeedType;
                        }
                        if (feedSubmissionInfo.IsSetSubmittedDate())
                        {
                            string SubmittedDate = feedSubmissionInfo.SubmittedDate;
                        }
                        if (feedSubmissionInfo.IsSetFeedProcessingStatus())
                        {
                            string FeedProcessingStatus = feedSubmissionInfo.FeedProcessingStatus;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
            return FeedSubmissionId;
            // StringEnum.GetStringValue(CarType.Estate)
        }


        // OperationContract Exposed to Client Application
        /// <summary>
        /// Gets the Feed Submission Result
        /// </summary>
        /// <param name="objSvcConfigInfo">DataContract that brings in the Service Cofiguration Informations</param>
        /// <param name="FeedSubmissionId">FeedSubmissionId</param>
        /// <returns>Feed Submission Result as MemoryStream </returns>
        public MemoryStream FeedSubmissionResult(ServiceConfigInfo objSvcConfigInfo, string FeedSubmissionId)
        {
            MemoryStream ms = new MemoryStream();
            try
            {

                LoginConfigurations(objSvcConfigInfo);

                GetFeedSubmissionResultRequest request = new GetFeedSubmissionResultRequest();
                request.Merchant = objSvcConfigInfo.MerchantID;

                // Note that depending on the size of the feed sent in, and the number of errors and warnings,
                // the result can reach sizes greater than 1GB. For this reason we recommend that you _always_ 
                // program to MWS in a streaming fashion. Otherwise, as your business grows you may silently reach
                // the in-memory size limit and have to re-work your solution.
                // NOTE: Due to Content-MD5 validation, the stream must be read/write.

                request.FeedSubmissionId = FeedSubmissionId;
                request.FeedSubmissionResult = ms;
                GetFeedSubmissionResultResponse response = FeedsService.GetFeedSubmissionResult(request);
                if (response.IsSetGetFeedSubmissionResultResult())
                {
                    GetFeedSubmissionResultResult getFeedSubmissionResultResult = response.GetFeedSubmissionResultResult;

                    if (getFeedSubmissionResultResult.IsSetContentMD5())
                    {
                        string ContentMD5 = getFeedSubmissionResultResult.ContentMD5;
                    }

                }
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
            return ms;
        }

        #endregion

        #region Orders Operations

        // OperationContract Exposed to Client Application
        /// <summary>
        ///  Gets All Order's Details List
        /// </summary>
        /// <param name="objSvcConfigInfo">DataContract that brings in the Service Cofiguration Informations</param>
        /// <returns>COrdersList object with list of Orders</returns>
        public COrdersList GetOrdersList(ServiceConfigInfo objSvcConfigInfo, DateTime CreatedAfter, DateTime CreatedBefore)
        {
            COrdersList objOrdersDetailsList = new COrdersList();
            COrders objOrders;
            CItemsList objItemList;

            try
            {
                LoginConfigurations(objSvcConfigInfo);
                ListOrdersRequest request = new ListOrdersRequest();
                request.MarketplaceId = new List<string>(new string[] { objSvcConfigInfo.MarketPlaceID });
                request.SellerId = objSvcConfigInfo.MerchantID;
                request.LastUpdatedAfter = CreatedAfter;
                request.LastUpdatedBefore = CreatedBefore;

                List<string> orderStatus = new List<string>();
                orderStatus.Add(OrderStatusEnum.PartiallyShipped.ToString());
                orderStatus.Add(OrderStatusEnum.Shipped.ToString());
                orderStatus.Add(OrderStatusEnum.Unshipped.ToString());

                request.OrderStatus = orderStatus;

                List<string> lstFulfillmentChannel = new List<string>();
                lstFulfillmentChannel.Add(FulfillmentChannelEnum.AFN.ToString());
                request.FulfillmentChannel = lstFulfillmentChannel;
                int a = -1;
                string AddressStreet = "";//To concatenate Address Lines
                ListOrdersResponse response = OrdersService.ListOrders(request);//Service Request
                //If gets valid List Order Response
                if (response.IsSetListOrdersResult())
                {
                    ListOrdersResult listOrdersResult = response.ListOrdersResult;
                    if (listOrdersResult.IsSetCreatedBefore())
                    {
                        //Console.WriteLine("                CreatedBefore");
                        //Console.WriteLine("                    {0}", listOrdersResult.CreatedBefore);
                    }
                    if (listOrdersResult.IsSetLastUpdatedBefore())
                    {
                        //Console.WriteLine("                LastUpdatedBefore");
                        //Console.WriteLine("                    {0}", listOrdersResult.LastUpdatedBefore);
                    }
                    if (listOrdersResult.IsSetOrders())
                    {
                        List<Order> orderList = listOrdersResult.Orders;
                        List<COrders> OrdersList = new List<COrders>();
                        foreach (Order order in orderList)
                        {
                            objOrders = new COrders();
                            objItemList = new CItemsList();
                            if (order.IsSetOrderStatus())
                            {
                                //Reads only valid Orders Order Status = 1.Shipped, 2.PartiallyShipped, 3. Unshipped
                                if ((order.OrderStatus == OrderStatusEnum.Shipped.ToString()) || (order.OrderStatus == OrderStatusEnum.PartiallyShipped.ToString()) || (order.OrderStatus == OrderStatusEnum.Unshipped.ToString()))
                                {
                                    if (order.IsSetAmazonOrderId())
                                    {
                                        objOrders.AmazonOrderId = order.AmazonOrderId;
                                        //objItemList = GetOrderItemsList(objSvcConfigInfo, order.AmazonOrderId);
                                        //objOrders.ItemsList = objItemList.ItemsList;
                                    }
                                    if (order.IsSetSellerOrderId())
                                    {
                                        objOrders.SellerOrderId = order.SellerOrderId;
                                    }
                                    if (order.IsSetPurchaseDate())
                                    {
                                        objOrders.PurchaseDate = order.PurchaseDate.ToString();
                                    }
                                    if (order.IsSetLastUpdateDate())
                                    {
                                        objOrders.LastUpdateDate = order.LastUpdateDate.ToString();
                                    }
                                    if (order.IsSetPaymentMethod())
                                    {
                                        objOrders.PaymentMethod = order.PaymentMethod.ToString();//PaymentMethod Enum
                                    }

                                    if (order.IsSetBuyerName())
                                    {
                                        objOrders.BuyerName = order.BuyerName;
                                    }
                                    if (order.IsSetBuyerEmail())//If contains Email Address
                                    {
                                        objOrders.BuyerEmail = order.BuyerEmail;
                                    }
                                    if (order.IsSetFulfillmentChannel())
                                    {
                                        objOrders.FulfillmentChannel = order.FulfillmentChannel.ToString();//FulfillmentChannel Enum
                                    }
                                    if (order.IsSetSalesChannel())
                                    {
                                        objOrders.SalesChannel = order.SalesChannel;
                                    }
                                    if (order.IsSetOrderChannel())
                                    {
                                        objOrders.OrderChannel = order.OrderChannel;
                                    }
                                    if (order.IsSetShipmentServiceLevelCategory())
                                    {
                                        objOrders.ShipmentServiceLevelCategory = order.ShipmentServiceLevelCategory;
                                    }
                                    if (order.IsSetShipServiceLevel())
                                    {
                                        objOrders.ShipServiceLevel = order.ShipServiceLevel;
                                    }
                                    if (order.IsSetShippingAddress())
                                    {
                                        objOrders.ShippingAddress = new CAddress();
                                        Address shippingAddress = order.ShippingAddress;
                                        if (shippingAddress.IsSetName())
                                        {
                                            objOrders.ShippingAddress.Name = shippingAddress.Name;
                                        }
                                        if (shippingAddress.IsSetAddressLine1())
                                        {
                                            objOrders.ShippingAddress.AddressLine1 = shippingAddress.AddressLine1;

                                        }
                                        if (shippingAddress.IsSetAddressLine2())
                                        {
                                            objOrders.ShippingAddress.AddressLine2 = shippingAddress.AddressLine2;
                                        }
                                        if (shippingAddress.IsSetAddressLine3())
                                        {
                                            objOrders.ShippingAddress.AddressLine3 = shippingAddress.AddressLine3;
                                        }
                                        if (shippingAddress.IsSetCity())
                                        {
                                            objOrders.ShippingAddress.City = shippingAddress.City;
                                        }
                                        if (shippingAddress.IsSetCounty())
                                        {
                                            objOrders.ShippingAddress.County = shippingAddress.County;
                                        }
                                        if (shippingAddress.IsSetDistrict())
                                        {
                                            objOrders.ShippingAddress.District = shippingAddress.District;
                                        }
                                        if (shippingAddress.IsSetStateOrRegion())
                                        {
                                            objOrders.ShippingAddress.StateOrRegion = shippingAddress.StateOrRegion;
                                        }
                                        if (shippingAddress.IsSetPostalCode())
                                        {
                                            objOrders.ShippingAddress.PostalCode = shippingAddress.PostalCode;
                                        }
                                        if (shippingAddress.IsSetCountryCode())
                                        {
                                            objOrders.ShippingAddress.CountryCode = shippingAddress.CountryCode;
                                        }
                                        if (shippingAddress.IsSetPhone())
                                        {
                                            objOrders.ShippingAddress.Phone = shippingAddress.Phone;
                                        }
                                    }
                                    if (order.IsSetOrderTotal())
                                    {
                                        objOrders.OrderTotal = new CMoney();
                                        Money orderTotal = order.OrderTotal;
                                        if (orderTotal.IsSetCurrencyCode())
                                        {
                                            objOrders.OrderTotal.CurrencyCode = orderTotal.CurrencyCode;

                                        }
                                        if (orderTotal.IsSetAmount())
                                        {
                                            objOrders.OrderTotal.Amount = orderTotal.Amount;
                                        }
                                    }
                                    if (order.IsSetNumberOfItemsShipped())
                                    {
                                        objOrders.NumberOfItemsShipped = order.NumberOfItemsShipped;
                                    }
                                    if (order.IsSetNumberOfItemsUnshipped())
                                    {
                                        objOrders.NumberOfItemsUnshipped = order.NumberOfItemsUnshipped;
                                    }
                                    if (order.IsSetMarketplaceId())
                                    {
                                        objOrders.MarketplaceId = order.MarketplaceId;
                                    }
                                    if (order.IsSetPaymentExecutionDetail())
                                    {
                                        List<PaymentExecutionDetailItem> paymentExecutionDetailItemList = order.PaymentExecutionDetail;
                                        List<CPaymentExecutionDetailItem> PaymentExecItems = new List<CPaymentExecutionDetailItem>();
                                        foreach (PaymentExecutionDetailItem paymentExecutionDetailItem in paymentExecutionDetailItemList)
                                        {
                                            CPaymentExecutionDetailItem objPaymentExecDetailItem = new CPaymentExecutionDetailItem();

                                            if (paymentExecutionDetailItem.IsSetPayment())
                                            {
                                                Money payment = paymentExecutionDetailItem.Payment;
                                                objPaymentExecDetailItem.PaymentField = new CMoney();

                                                if (payment.IsSetCurrencyCode())
                                                {
                                                    objPaymentExecDetailItem.PaymentField.CurrencyCode = payment.CurrencyCode;
                                                }
                                                if (payment.IsSetAmount())
                                                {
                                                    objPaymentExecDetailItem.PaymentField.Amount = payment.Amount;
                                                }
                                            }
                                            if (paymentExecutionDetailItem.IsSetPaymentMethod())
                                            {
                                                objPaymentExecDetailItem.SubPaymentMethodField = paymentExecutionDetailItem.PaymentMethod;
                                            }
                                            PaymentExecItems.Add(objPaymentExecDetailItem);
                                        }
                                        objOrders.PaymentExecItems = PaymentExecItems;
                                    }
                                    OrdersList.Add(objOrders);
                                }
                            }
                        }
                        objOrdersDetailsList.Orders = OrdersList;
                    }
                }
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
            //return DataContractSerializeToStream<COrdersList>(objOrdersDetailsList);
            return objOrdersDetailsList;
        }

        // OperationContract Exposed to Client Application
        /// <summary>
        /// Gets th eList of Items Details for the given Amazon Order ID
        /// </summary>
        /// <param name="objSvcConfigInfo">DataContract that brings in the Service Cofiguration Informations</param>
        /// <param name="AmazonOrderId">Amazon Order Identification Number</param>
        /// <returns>CItemsList object with list of Items</returns>
        public CItemsList GetOrderItemsList(ServiceConfigInfo objSvcConfigInfo, string AmazonOrderId)
        {
            CItemsList objItemsList = new CItemsList();
            DataRow drItem;
            int ItemCount = 0;
            string NextToken = "";
            // string AmazonOrderId = "";
            try
            {
                LoginConfigurations(objSvcConfigInfo);
                ListOrderItemsRequest request = new ListOrderItemsRequest();
                objItemsList.ItemsList = new List<CItems>();
                request.SellerId = objSvcConfigInfo.MerchantID;
                request.AmazonOrderId = AmazonOrderId;
                ListOrderItemsResponse response = OrdersService.ListOrderItems(request);

                if (response.IsSetListOrderItemsResult())
                {
                    ListOrderItemsResult listOrderItemsResult = response.ListOrderItemsResult;
                    if (listOrderItemsResult.IsSetNextToken())
                    {
                        NextToken = listOrderItemsResult.NextToken;
                    }

                    if (listOrderItemsResult.IsSetOrderItems())
                    {
                        List<OrderItem> orderItemList = listOrderItemsResult.OrderItems;

                        foreach (OrderItem orderItem in orderItemList)
                        {
                            CItems objItem = new CItems();

                            if (orderItem.IsSetASIN())
                            {
                                objItem.ASIN = orderItem.ASIN;
                            }
                            if (orderItem.IsSetSellerSKU())
                            {
                                objItem.SellerSKU = orderItem.SellerSKU;
                            }
                            if (orderItem.IsSetOrderItemId())
                            {
                                objItem.OrderItemId = orderItem.OrderItemId;
                            }
                            if (orderItem.IsSetTitle())
                            {
                                objItem.Title = orderItem.Title;
                            }
                            if (orderItem.IsSetQuantityOrdered())
                            {
                                objItem.QuantityOrdered = orderItem.QuantityOrdered;
                            }
                            if (orderItem.IsSetQuantityShipped())
                            {
                                objItem.QuantityShipped = orderItem.QuantityShipped;
                            }

                            if (orderItem.IsSetItemPrice())
                            {
                                Money itemPrice = orderItem.ItemPrice;
                                objItem.ItemPrice = new CMoney();

                                if (itemPrice.IsSetCurrencyCode())
                                {
                                    objItem.ItemPrice.CurrencyCode = itemPrice.CurrencyCode;
                                }
                                if (itemPrice.IsSetAmount())
                                {
                                    objItem.ItemPrice.Amount = itemPrice.Amount;
                                }
                            }


                            if (orderItem.IsSetShippingPrice())
                            {
                                Money shippingPrice = orderItem.ShippingPrice;
                                objItem.ShippingPrice = new CMoney();

                                if (shippingPrice.IsSetCurrencyCode())
                                {
                                    objItem.ShippingPrice.CurrencyCode = shippingPrice.CurrencyCode;
                                    //Console.WriteLine("                                {0}", shippingPrice.CurrencyCode);
                                }
                                if (shippingPrice.IsSetAmount())
                                {
                                    objItem.ShippingPrice.Amount = shippingPrice.Amount;
                                }
                            }
                            if (orderItem.IsSetGiftWrapPrice())
                            {
                                Money giftWrapPrice = orderItem.GiftWrapPrice;
                                objItem.GiftWrapPrice = new CMoney();

                                if (giftWrapPrice.IsSetCurrencyCode())
                                {
                                    objItem.GiftWrapPrice.CurrencyCode = giftWrapPrice.CurrencyCode;
                                }
                                if (giftWrapPrice.IsSetAmount())
                                {
                                    objItem.GiftWrapPrice.Amount = giftWrapPrice.Amount;
                                }
                            }

                            if (orderItem.IsSetItemTax())
                            {
                                Money itemTax = orderItem.ItemTax;
                                objItem.ItemTax = new CMoney();

                                if (itemTax.IsSetCurrencyCode())
                                {
                                    objItem.ItemTax.CurrencyCode = itemTax.CurrencyCode;
                                }
                                if (itemTax.IsSetAmount())
                                {
                                    objItem.ItemTax.Amount = itemTax.Amount;
                                }
                            }
                            if (orderItem.IsSetShippingTax())
                            {
                                Money shippingTax = orderItem.ShippingTax;
                                objItem.ShippingTax = new CMoney();

                                if (shippingTax.IsSetCurrencyCode())
                                {
                                    objItem.ShippingTax.CurrencyCode = shippingTax.CurrencyCode;
                                }
                                if (shippingTax.IsSetAmount())
                                {
                                    objItem.ShippingTax.Amount = shippingTax.Amount;
                                }
                            }
                            if (orderItem.IsSetGiftWrapTax())
                            {
                                Money giftWrapTax = orderItem.GiftWrapTax;
                                objItem.GiftWrapTax = new CMoney();


                                if (giftWrapTax.IsSetCurrencyCode())
                                {
                                    objItem.GiftWrapTax.CurrencyCode = giftWrapTax.CurrencyCode;
                                }
                                if (giftWrapTax.IsSetAmount())
                                {
                                    objItem.GiftWrapTax.Amount = giftWrapTax.Amount;
                                }
                            }
                            if (orderItem.IsSetShippingDiscount())
                            {
                                Money shippingDiscount = orderItem.ShippingDiscount;
                                objItem.ShippingDiscount = new CMoney();

                                if (shippingDiscount.IsSetCurrencyCode())
                                {
                                    objItem.ShippingDiscount.CurrencyCode = shippingDiscount.CurrencyCode;
                                }
                                if (shippingDiscount.IsSetAmount())
                                {
                                    objItem.ShippingDiscount.Amount = shippingDiscount.Amount;
                                }
                            }
                            if (orderItem.IsSetPromotionDiscount())
                            {
                                Money promotionDiscount = orderItem.PromotionDiscount;
                                objItem.PromotionDiscount = new CMoney();

                                if (promotionDiscount.IsSetCurrencyCode())
                                {
                                    objItem.PromotionDiscount.CurrencyCode = promotionDiscount.CurrencyCode;
                                }
                                if (promotionDiscount.IsSetAmount())
                                {
                                    objItem.PromotionDiscount.Amount = promotionDiscount.Amount;
                                }
                            }


                            if (orderItem.IsSetCODFee())
                            {
                                objItem.CODFee = new CMoney();

                                Money CODFee = orderItem.CODFee;
                                if (CODFee.IsSetCurrencyCode())
                                {
                                    objItem.CODFee.CurrencyCode = CODFee.CurrencyCode;
                                }
                                if (CODFee.IsSetAmount())
                                {
                                    objItem.CODFee.Amount = CODFee.Amount;
                                }
                            }
                            if (orderItem.IsSetCODFeeDiscount())
                            {
                                Money CODFeeDiscount = orderItem.CODFeeDiscount;
                                objItem.CODFeeDiscount = new CMoney();

                                if (CODFeeDiscount.IsSetCurrencyCode())
                                {
                                    objItem.CODFeeDiscount.CurrencyCode = CODFeeDiscount.CurrencyCode;
                                }
                                if (CODFeeDiscount.IsSetAmount())
                                {
                                    objItem.CODFeeDiscount.Amount = CODFeeDiscount.Amount;
                                }
                            }
                            if (orderItem.IsSetGiftMessageText())
                            {
                                objItem.GiftMessageText = orderItem.GiftMessageText;
                            }
                            if (orderItem.IsSetGiftWrapLevel())
                            {
                                objItem.GiftMessageText = orderItem.GiftWrapLevel;
                            }
                            objItemsList.ItemsList.Add(objItem);
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
            return objItemsList;
        }

        // OperationContract Exposed to Client Application
        /// <summary>
        /// Gets the Order Details for the given Amazon Order ID
        /// </summary>
        /// <param name="objSvcConfigInfo">DataContract that brings in the Service Cofiguration Informations</param>
        /// <param name="AmazonOrderId">Amazon Order Identification Number</param>
        /// <returns>COrdersList object with Order Details with Items List</returns>
        public COrdersList GetOrderDetails(ServiceConfigInfo objSvcConfigInfo, string[] AmazonOrderId)
        {
            COrdersList objOrdersDetailsList = new COrdersList();
            COrders objOrder;
            CItemsList objItemList;
            // COrders objOrders;
            try
            {

                LoginConfigurations(objSvcConfigInfo);

                GetOrderRequest request = new GetOrderRequest();
                // @TODO: set request parameters here
                request.SellerId = objSvcConfigInfo.MerchantID;
                request.AmazonOrderId = new List<string>(AmazonOrderId);
                // request.AmazonOrderId.Id = new List<string>(new string[] { AmazonOrderId });
                GetOrderResponse response = OrdersService.GetOrder(request);
                if (response.IsSetGetOrderResult())
                {
                    GetOrderResult getOrderResult = response.GetOrderResult;
                    if (getOrderResult.IsSetOrders())
                    {
                        List<Order> orderList = getOrderResult.Orders;
                        List<COrders> OrdersList = new List<COrders>();

                        foreach (Order order in orderList)
                        {
                            objOrder = new COrders();
                            objItemList = new CItemsList();

                            if (order.IsSetOrderStatus())
                            {
                                //Reads only valid Orders Order Status = 1.Shipped, 2.PartiallyShipped, 3. Unshipped
                                if ((order.OrderStatus == OrderStatusEnum.Shipped.ToString()) || (order.OrderStatus == OrderStatusEnum.PartiallyShipped.ToString()) || (order.OrderStatus == OrderStatusEnum.Unshipped.ToString()))
                                {
                                    if (order.IsSetAmazonOrderId())
                                    {
                                        objOrder.AmazonOrderId = order.AmazonOrderId;
                                        objItemList = GetOrderItemsList(objSvcConfigInfo, order.AmazonOrderId);
                                        objOrder.ItemsList = objItemList.ItemsList;
                                    }

                                    if (order.IsSetSellerOrderId())
                                    {
                                        objOrder.SellerOrderId = order.SellerOrderId;
                                    }
                                    if (order.IsSetPurchaseDate())
                                    {
                                        objOrder.PurchaseDate = order.PurchaseDate.ToString();
                                    }
                                    if (order.IsSetLastUpdateDate())
                                    {
                                        objOrder.LastUpdateDate = order.LastUpdateDate.ToString();
                                    }
                                    if (order.IsSetPaymentMethod())
                                    {
                                        objOrder.PaymentMethod = order.PaymentMethod.ToString();//PaymentMethod Enum
                                    }

                                    if (order.IsSetBuyerName())
                                    {
                                        objOrder.BuyerName = order.BuyerName;
                                    }
                                    if (order.IsSetBuyerEmail())//If contains Email Address
                                    {
                                        objOrder.BuyerEmail = order.BuyerEmail;
                                    }
                                    if (order.IsSetFulfillmentChannel())
                                    {
                                        objOrder.FulfillmentChannel = order.FulfillmentChannel.ToString();//FulfillmentChannel Enum
                                    }
                                    if (order.IsSetSalesChannel())
                                    {
                                        objOrder.SalesChannel = order.SalesChannel;
                                    }
                                    if (order.IsSetOrderChannel())
                                    {
                                        objOrder.OrderChannel = order.OrderChannel;
                                    }
                                    if (order.IsSetShipmentServiceLevelCategory())
                                    {
                                        objOrder.ShipmentServiceLevelCategory = order.ShipmentServiceLevelCategory;
                                    }
                                    if (order.IsSetShipServiceLevel())
                                    {
                                        objOrder.ShipServiceLevel = order.ShipServiceLevel;
                                    }
                                    if (order.IsSetShippingAddress())
                                    {
                                        objOrder.ShippingAddress = new CAddress();
                                        Address shippingAddress = order.ShippingAddress;
                                        if (shippingAddress.IsSetName())
                                        {
                                            objOrder.ShippingAddress.Name = shippingAddress.Name;
                                        }
                                        if (shippingAddress.IsSetAddressLine1())
                                        {
                                            objOrder.ShippingAddress.AddressLine1 = shippingAddress.AddressLine1;

                                        }
                                        if (shippingAddress.IsSetAddressLine2())
                                        {
                                            objOrder.ShippingAddress.AddressLine2 = shippingAddress.AddressLine2;
                                        }
                                        if (shippingAddress.IsSetAddressLine3())
                                        {
                                            objOrder.ShippingAddress.AddressLine3 = shippingAddress.AddressLine3;
                                        }
                                        if (shippingAddress.IsSetCity())
                                        {
                                            objOrder.ShippingAddress.City = shippingAddress.City;
                                        }
                                        if (shippingAddress.IsSetCounty())
                                        {
                                            objOrder.ShippingAddress.County = shippingAddress.County;
                                        }
                                        if (shippingAddress.IsSetDistrict())
                                        {
                                            objOrder.ShippingAddress.District = shippingAddress.District;
                                        }
                                        if (shippingAddress.IsSetStateOrRegion())
                                        {
                                            objOrder.ShippingAddress.StateOrRegion = shippingAddress.StateOrRegion;
                                        }
                                        if (shippingAddress.IsSetPostalCode())
                                        {
                                            objOrder.ShippingAddress.PostalCode = shippingAddress.PostalCode;
                                        }
                                        if (shippingAddress.IsSetCountryCode())
                                        {
                                            objOrder.ShippingAddress.CountryCode = shippingAddress.CountryCode;
                                        }
                                        if (shippingAddress.IsSetPhone())
                                        {
                                            objOrder.ShippingAddress.Phone = shippingAddress.Phone;
                                        }
                                    }
                                    if (order.IsSetOrderTotal())
                                    {
                                        objOrder.OrderTotal = new CMoney();
                                        Money orderTotal = order.OrderTotal;
                                        if (orderTotal.IsSetCurrencyCode())
                                        {
                                            objOrder.OrderTotal.CurrencyCode = orderTotal.CurrencyCode;
                                        }
                                        if (orderTotal.IsSetAmount())
                                        {
                                            objOrder.OrderTotal.Amount = orderTotal.Amount;
                                        }
                                    }
                                    if (order.IsSetNumberOfItemsShipped())
                                    {
                                        objOrder.NumberOfItemsShipped = order.NumberOfItemsShipped;
                                    }
                                    if (order.IsSetNumberOfItemsUnshipped())
                                    {
                                        objOrder.NumberOfItemsUnshipped = order.NumberOfItemsUnshipped;
                                    }
                                    if (order.IsSetMarketplaceId())
                                    {
                                        objOrder.MarketplaceId = order.MarketplaceId;
                                    }
                                    if (order.IsSetPaymentExecutionDetail())
                                    {
                                        List<PaymentExecutionDetailItem> paymentExecutionDetailItemList = order.PaymentExecutionDetail;
                                        List<CPaymentExecutionDetailItem> PaymentExecItems = new List<CPaymentExecutionDetailItem>();
                                        foreach (PaymentExecutionDetailItem paymentExecutionDetailItem in paymentExecutionDetailItemList)
                                        {
                                            CPaymentExecutionDetailItem objPaymentExecDetailItem = new CPaymentExecutionDetailItem();

                                            if (paymentExecutionDetailItem.IsSetPayment())
                                            {
                                                Money payment = paymentExecutionDetailItem.Payment;
                                                objPaymentExecDetailItem.PaymentField = new CMoney();

                                                if (payment.IsSetCurrencyCode())
                                                {
                                                    objPaymentExecDetailItem.PaymentField.CurrencyCode = payment.CurrencyCode;
                                                }
                                                if (payment.IsSetAmount())
                                                {
                                                    objPaymentExecDetailItem.PaymentField.Amount = payment.Amount;
                                                }
                                            }
                                            if (paymentExecutionDetailItem.IsSetPaymentMethod())
                                            {
                                                objPaymentExecDetailItem.SubPaymentMethodField = paymentExecutionDetailItem.PaymentMethod;
                                            }
                                            PaymentExecItems.Add(objPaymentExecDetailItem);
                                        }
                                        objOrder.PaymentExecItems = PaymentExecItems;
                                    }
                                    OrdersList.Add(objOrder);
                                }
                            }
                        }
                        objOrdersDetailsList.Orders = OrdersList;
                    }
                }
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }

            return objOrdersDetailsList;
        }


        #endregion

        #region Reports Operations

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objSvcConfigInfo"></param>
        /// <param name="ReportIds"></param>
        public void UpdateReportAcknowledgement(ServiceConfigInfo objSvcConfigInfo, string[] ReportIds)
        {
            try
            {
                LoginConfigurations(objSvcConfigInfo);
                UpdateReportAcknowledgementsRequest request = new UpdateReportAcknowledgementsRequest();
                request.Merchant = objSvcConfigInfo.MerchantID;
                request.Acknowledged = true;
                request.WithReportIdList(new IdList().WithId(ReportIds));
                UpdateReportAcknowledgementsResponse response = FeedsService.UpdateReportAcknowledgements(request);

                if (response.IsSetUpdateReportAcknowledgementsResult())
                {
                    UpdateReportAcknowledgementsResult updateReportAcknowledgementsResult = response.UpdateReportAcknowledgementsResult;
                    List<ReportInfo> reportInfoList = updateReportAcknowledgementsResult.ReportInfo;
                }
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objSvcConfigInfo"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="ReportType"></param>
        /// <returns></returns>
        public string[] GetReportList(ServiceConfigInfo objSvcConfigInfo, DateTime FromDate, DateTime ToDate, string ReportType = "")
        {
            List<string> lstReportId = new List<string>();

            try
            {
                LoginConfigurations(objSvcConfigInfo);
                GetReportListRequest request = new GetReportListRequest();

                request.Merchant = objSvcConfigInfo.MerchantID;
                request.Acknowledged = false;
                request.ReportTypeList = new TypeList().WithType(new string[] { ReportType });
                request.AvailableFromDate = FromDate;
                request.AvailableToDate = ToDate;

                GetReportListResponse response = FeedsService.GetReportList(request);
                if (response.IsSetGetReportListResult())
                {
                    GetReportListResult getReportListResult = response.GetReportListResult;
                    List<ReportInfo> reportInfoList = getReportListResult.ReportInfo;
                    foreach (ReportInfo reportInfo in reportInfoList)
                    {
                        if (reportInfo.IsSetReportId())
                        {
                            lstReportId.Add(reportInfo.ReportId);
                        }
                    }

                    if (getReportListResult.HasNext)
                    {
                        string Token = getReportListResult.NextToken;
                        do
                        {
                            GetReportListByNextTokenRequest request1 = new GetReportListByNextTokenRequest();
                            request1.Merchant = objSvcConfigInfo.MerchantID;
                            //Token = getReportListResult.NextToken;
                            request1.NextToken = Token;
                            GetReportListByNextTokenResponse res = FeedsService.GetReportListByNextToken(request1);
                            if (res.IsSetGetReportListByNextTokenResult())
                            {
                                GetReportListByNextTokenResult result1 = res.GetReportListByNextTokenResult;
                                List<ReportInfo> reportInfoList1 = result1.ReportInfo;
                                foreach (ReportInfo reportInfo in reportInfoList1)
                                {
                                    if (reportInfo.IsSetReportId())
                                    {
                                        lstReportId.Add(reportInfo.ReportId);
                                    }
                                }
                                if (result1.HasNext)
                                {
                                    Token = result1.NextToken;
                                }
                                else
                                    Token = "";
                            }
                        } while (Token != "");
                    }
                }
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
            return lstReportId.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objSvcConfigInfo"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="ReportType"></param>
        /// <returns></returns>
        public string[] GetAcknowledgedReportList(ServiceConfigInfo objSvcConfigInfo, DateTime FromDate, DateTime ToDate, string ReportType = "")
        {
            List<string> lstReportId = new List<string>();

            try
            {
                LoginConfigurations(objSvcConfigInfo);
                GetReportListRequest request = new GetReportListRequest();
                request.Merchant = objSvcConfigInfo.MerchantID;
                request.Acknowledged = true;
                request.ReportTypeList = new TypeList().WithType(new string[] { ReportType });
                request.AvailableFromDate = FromDate;
                request.AvailableToDate = ToDate;

                GetReportListResponse response = FeedsService.GetReportList(request);
                if (response.IsSetGetReportListResult())
                {
                    GetReportListResult getReportListResult = response.GetReportListResult;
                    List<ReportInfo> reportInfoList = getReportListResult.ReportInfo;
                    foreach (ReportInfo reportInfo in reportInfoList)
                    {
                        if (reportInfo.IsSetReportId())
                        {
                            lstReportId.Add(reportInfo.ReportId);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
            return lstReportId.ToArray();
        }

        /// <summary>
        /// Forward a Request for Report to Amazon MWS
        /// </summary>
        /// <param name="objSvcConfigInfo">DataContract that brings in the Service Cofiguration Informations</param>
        /// <param name="ReportType">ReportType</param>
        /// <param name="ReportOptions">ReportOptions</param>
        public string RequestReport(ServiceConfigInfo objSvcConfigInfo, string ReportType, string ReportOptions = "")
        {
            string ReportRequestId = "";
            try
            {
                LoginConfigurations(objSvcConfigInfo);
                RequestReportRequest request = new RequestReportRequest();
                request.Merchant = objSvcConfigInfo.MerchantID;
                request.MarketplaceIdList = new IdList();
                request.MarketplaceIdList.Id = new List<string>(new string[] { objSvcConfigInfo.MarketPlaceID });
                request.ReportType = ReportType;
                //request.ReportOptions = ReportOptions;
                RequestReportResponse response = FeedsService.RequestReport(request);

                if (response.IsSetRequestReportResult())
                {
                    RequestReportResult requestReportResult = response.RequestReportResult;
                    ReportRequestInfo reportRequestInfo = requestReportResult.ReportRequestInfo;
                    // reportRequestInfo Details
                    if (reportRequestInfo.IsSetReportRequestId())
                    {
                        ReportRequestId = reportRequestInfo.ReportRequestId;
                    }

                }
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
            return ReportRequestId;
        }

        /// <summary>
        /// Gets the Report as Stream for the given Report ID
        /// </summary>
        /// <param name="objSvcConfigInfo">DataContract that brings in the Service Cofiguration Informations</param>
        /// <param name="ReportID">Amazon Report Identification Number</param>
        /// <returns></returns>
        public MemoryStream GetReportFromReportID(ServiceConfigInfo objSvcConfigInfo, string ReportID)
        {
            MemoryStream ms = new MemoryStream();
            try
            {

                LoginConfigurations(objSvcConfigInfo);
                GetReportRequest request = new GetReportRequest();
                request.Merchant = objSvcConfigInfo.MerchantID;
                request.ReportId = ReportID;
                request.Report = ms;
                //request.Report = File.Open("report.xml", FileMode.OpenOrCreate, FileAccess.ReadWrite);
                GetReportResponse response = FeedsService.GetReport(request);

            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
            return ms;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objSvcConfigInfo"></param>
        /// <param name="ReportType"></param>
        /// <param name="Schedule"></param>
        public void ManagedReportSchedule(ServiceConfigInfo objSvcConfigInfo, string ReportType, string Schedule)
        {
            MemoryStream ms = new MemoryStream();

            LoginConfigurations(objSvcConfigInfo);
            ManageReportScheduleRequest request = new ManageReportScheduleRequest();
            request.Merchant = objSvcConfigInfo.MerchantID;
            request.ReportType = ReportType;
            request.Schedule = Schedule;
            ManageReportScheduleResponse response = FeedsService.ManageReportSchedule(request);

            if (response.IsSetManageReportScheduleResult())
            {
                ManageReportScheduleResult manageReportScheduleResult = response.ManageReportScheduleResult;
                if (manageReportScheduleResult.IsSetCount())
                {
                    //Console.WriteLine("                    {0}", manageReportScheduleResult.Count);
                }
                List<ReportSchedule> reportScheduleList = manageReportScheduleResult.ReportSchedule;
                foreach (ReportSchedule reportSchedule in reportScheduleList)
                {
                    if (reportSchedule.IsSetReportType())
                    {
                        // Console.WriteLine("                        {0}", reportSchedule.ReportType);
                    }
                    if (reportSchedule.IsSetSchedule())
                    {
                        // Console.WriteLine("                        {0}", reportSchedule.Schedule);
                    }
                    if (reportSchedule.IsSetScheduledDate())
                    {
                        // Console.WriteLine("                        {0}", reportSchedule.ScheduledDate);
                    }
                }
            }

            //return ms;
        }

        #endregion Reports Operations

        #region Products Operation

        /// <summary>
        /// Gets the ASIN for the List of SKU
        /// </summary>
        /// <param name="objSvcConfigInfo"></param>
        /// <param name="lstSKU">Seller's Stock Keeping Unique Identifier</param>
        /// <returns>ASIN for SKU as Dictionary Collection</returns>
        public Dictionary<string, string> GetASIN_From_SKU(ServiceConfigInfo objSvcConfigInfo, List<string> lstSKU)
        {
            string SKU = "";
            string ASIN = "";
            try
            {
                LoginConfigurations(objSvcConfigInfo);
                GetMyPriceForSKURequest request = new GetMyPriceForSKURequest();
                request.SellerId = objSvcConfigInfo.MerchantID;
                request.MarketplaceId = objSvcConfigInfo.MarketPlaceID;
                SellerSKUListType obj = new SellerSKUListType();
                obj.SellerSKU = lstSKU;
                request.SellerSKUList = obj;
                GetMyPriceForSKUResponse response = ProductsService.GetMyPriceForSKU(request);
                List<GetMyPriceForSKUResult> getMyPriceForSKUResultList = response.GetMyPriceForSKUResult;
                foreach (GetMyPriceForSKUResult getMyPriceForSKUResult in getMyPriceForSKUResultList)
                {
                    if (getMyPriceForSKUResult.IsSetProduct())
                    {
                        Product product = getMyPriceForSKUResult.Product;
                        if (product.IsSetIdentifiers())
                        {
                            IdentifierType identifiers = product.Identifiers;
                            if (identifiers.IsSetMarketplaceASIN())
                            {
                                ASINIdentifier marketplaceASIN = identifiers.MarketplaceASIN;
                                if (marketplaceASIN.IsSetASIN())
                                {
                                    ASIN = marketplaceASIN.ASIN;
                                }
                            }
                            if (identifiers.IsSetSKUIdentifier())
                            {
                                SellerSKUIdentifier SKUIdentifier = identifiers.SKUIdentifier;
                                if (SKUIdentifier.IsSetSellerSKU())
                                {
                                    SKU = SKUIdentifier.SellerSKU;
                                }
                            }
                        }
                    }
                    if (!SKU_ASIN.ContainsKey(SKU))
                    {
                        SKU_ASIN.Add(SKU, ASIN);
                    }
                }
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
            return SKU_ASIN;
        }

        VariationDetails vd;
        Variations varie;

        public AmazonProductList GetProductDetailsForSellerSKU(ServiceConfigInfo objSvcConfigInfo, string SellerSKU, ref DataTable dtProdListings, DataTable dtCFields = null, DataTable dtCFValues = null)
        {
            DataTable dtProductDetails = new DataTable();
            AmazonProduct objAmazonProduct = new AmazonProduct();

            AmazonProductList objAmazonProductList = new AmazonProductList();
            List<AmazonProduct> lstAmazonProducts = new List<AmazonProduct>();
            Product objProduct = new Product();
            string xml = "";
            GetMatchingProductForIdRequest request = new GetMatchingProductForIdRequest();
            VariationCollection vCollection = new VariationCollection();
            vCollection.VariationDetail = new List<VariationDetails>();
            vCollection.VariationList = new List<Variations>();

            string strKeyValuePairs = "";
            string strKeys = "", strValues = "";
            List<string> ASINList = new List<string>();
            double dblHeight = 0, dblWidth = 0, dblLength = 0, dblWeight = 0;
            double dblShippingHeight = 0, dblShippingWidth = 0, dblShippingLength = 0, dblShippingWeight = 0;

            try
            {
                LoginConfigurations(objSvcConfigInfo);
                request.SellerId = objSvcConfigInfo.MerchantID;
                request.MarketplaceId = objSvcConfigInfo.MarketPlaceID;
                request.IdType = "SellerSKU";
                List<string> SellerSKUList = new List<string>();
                SellerSKUList.Add(SellerSKU);
                IdListType objIdListType = new IdListType();
                objIdListType.Id = SellerSKUList;
                request.IdList = objIdListType;

                GetMatchingProductForIdResponse response = ProductsService.GetMatchingProductForId(request);
                List<GetMatchingProductForIdResult> getMatchingProductForIdResultList = response.GetMatchingProductForIdResult;
                foreach (GetMatchingProductForIdResult getMatchingProductForIdResult in getMatchingProductForIdResultList)
                {

                    if (getMatchingProductForIdResult.IsSetProducts())
                    {
                        ProductList products = getMatchingProductForIdResult.Products;
                        List<Product> productList = products.Product;
                        foreach (Product product in productList)
                        {

                            if (product.Relationships.Any.Count == 0)
                            {
                                if (product.IsSetAttributeSets())
                                {
                                    foreach (var attribute in product.AttributeSets.Any)
                                    {
                                        xml = ProductsUtil.FormatXml((System.Xml.XmlElement)attribute);
                                        objAmazonProduct = GetProductDetailsFromAttributeSet(xml);

                                        dblLength = Convert.ToDouble(objAmazonProduct.Length);
                                        dblHeight = Convert.ToDouble(objAmazonProduct.Height);
                                        dblWidth = Convert.ToDouble(objAmazonProduct.Width);
                                        dblWeight = Convert.ToDouble(objAmazonProduct.Weight);

                                        dblShippingLength = Convert.ToDouble(objAmazonProduct.ShippingLength);
                                        dblShippingHeight = Convert.ToDouble(objAmazonProduct.ShippingHeight);
                                        dblShippingWidth = Convert.ToDouble(objAmazonProduct.ShippingWidth);
                                        dblShippingWeight = Convert.ToDouble(objAmazonProduct.ShippingWeight);
                                    }
                                }

                                if (product.IsSetIdentifiers())
                                {
                                    if (product.Identifiers.IsSetMarketplaceASIN())
                                        objAmazonProduct.ASIN = product.Identifiers.MarketplaceASIN.ASIN;

                                    if (product.Identifiers.IsSetSKUIdentifier())
                                    {
                                        objAmazonProduct.SellerSKU = product.Identifiers.SKUIdentifier.SellerSKU;
                                    }
                                    else
                                    {
                                        objAmazonProduct.SellerSKU = SellerSKU;
                                    }
                                }
                            }


                            if (product.IsSetRelationships())
                            {
                                if (product.Relationships.Any.Count == 0)
                                {
                                    if (product.IsSetAttributeSets())
                                    {
                                        foreach (var attribute in product.AttributeSets.Any)
                                        {
                                            xml = ProductsUtil.FormatXml((System.Xml.XmlElement)attribute);
                                            objAmazonProduct = GetProductDetailsFromAttributeSet(xml);
                                        }
                                    }

                                    if (product.IsSetIdentifiers())
                                    {
                                        if (product.Identifiers.IsSetMarketplaceASIN())
                                            objAmazonProduct.ASIN = product.Identifiers.MarketplaceASIN.ASIN;

                                        if (product.Identifiers.IsSetSKUIdentifier())
                                        {
                                            objAmazonProduct.SellerSKU = product.Identifiers.SKUIdentifier.SellerSKU;
                                        }
                                        else
                                        {
                                            objAmazonProduct.SellerSKU = SellerSKU;
                                        }
                                    }
                                    lstAmazonProducts.Add(objAmazonProduct);
                                }
                                else
                                {
                                    string strVariationParentASIN = "";
                                    foreach (var relationship in product.Relationships.Any)
                                    {
                                        //Check whether Amazon Item properties have been any variation attributes are exists or not ?
                                        //If any attributes are there than we are adding / updating their ListDetails as per need.
                                        if (dtCFields != null && dtCFields.Rows.Count > 0)
                                        {

                                            foreach (XmlNode objXMLMainNode in ((System.Xml.XmlElement)relationship).ChildNodes)
                                            {
                                                if (Convert.ToString(objXMLMainNode.ParentNode.LocalName) == "VariationParent")
                                                {
                                                    if (objXMLMainNode.ChildNodes.Count > 0)
                                                    {
                                                        strVariationParentASIN = Convert.ToString(objXMLMainNode.LastChild.ChildNodes[1].ChildNodes[0].Value);
                                                        if (strVariationParentASIN != "")
                                                        {
                                                            //ASINList.Add(strVariationParentASIN);
                                                            LoginConfigurations(objSvcConfigInfo);
                                                            request.SellerId = objSvcConfigInfo.MerchantID;
                                                            request.MarketplaceId = objSvcConfigInfo.MarketPlaceID;
                                                            request.IdType = "ASIN";
                                                            List<string> ASINParentList = new List<string>();
                                                            ASINParentList.Add(strVariationParentASIN);
                                                            IdListType objIdParentListType = new IdListType();
                                                            objIdParentListType.Id = ASINParentList;
                                                            request.IdList = objIdParentListType;

                                                            GetMatchingProductForIdResponse responseParent = ProductsService.GetMatchingProductForId(request);
                                                            List<GetMatchingProductForIdResult> getMatchingParentProductForIdResultList = responseParent.GetMatchingProductForIdResult;
                                                            foreach (GetMatchingProductForIdResult getMatchingParentProductForIdResult in getMatchingParentProductForIdResultList)
                                                            {

                                                                if (getMatchingParentProductForIdResult.IsSetProducts())
                                                                {
                                                                    ProductList ParentProducts = getMatchingParentProductForIdResult.Products;
                                                                    List<Product> ParentProductList = ParentProducts.Product;

                                                                    foreach (Product MainProduct in ParentProductList)
                                                                    {
                                                                        if (MainProduct.IsSetAttributeSets())
                                                                        {

                                                                            foreach (var attribute in MainProduct.AttributeSets.Any)
                                                                            {
                                                                                xml = ProductsUtil.FormatXml((System.Xml.XmlElement)attribute);
                                                                                objAmazonProduct = GetProductDetailsFromAttributeSet(xml);
                                                                            }

                                                                            if (MainProduct.Identifiers.IsSetMarketplaceASIN())
                                                                                objAmazonProduct.ASIN = MainProduct.Identifiers.MarketplaceASIN.ASIN;

                                                                            if (MainProduct.Identifiers.IsSetSKUIdentifier())
                                                                                objAmazonProduct.SellerSKU = MainProduct.Identifiers.SKUIdentifier.SellerSKU;

                                                                            objAmazonProduct.Length = Convert.ToString(dblLength);
                                                                            objAmazonProduct.Height = Convert.ToString(dblHeight);
                                                                            objAmazonProduct.Width = Convert.ToString(dblWidth);
                                                                            objAmazonProduct.Weight = Convert.ToString(dblWeight);

                                                                            objAmazonProduct.ShippingLength = Convert.ToString(dblShippingLength);
                                                                            objAmazonProduct.ShippingHeight = Convert.ToString(dblShippingHeight);
                                                                            objAmazonProduct.ShippingWidth = Convert.ToString(dblShippingWidth);
                                                                            objAmazonProduct.ShippingWeight = Convert.ToString(dblShippingWeight);
                                                                        }

                                                                        if (MainProduct.IsSetRelationships())
                                                                        {
                                                                            int inCnt = 0;
                                                                            //int oldCnt = Convert.ToInt32(MainProduct.Relationships.Any.Count);
                                                                            foreach (var relationshipChild in MainProduct.Relationships.Any)
                                                                            {
                                                                                xml = ProductsUtil.FormatXml((System.Xml.XmlElement)relationshipChild);
                                                                                var xDoc = XDocument.Parse(xml);
                                                                                XNamespace ns2 = "http://mws.amazonservices.com/schema/Products/2011-10-01/default.xsd";

                                                                                strKeyValuePairs = "";
                                                                                varie = new Variations();
                                                                                foreach (XElement element in xDoc.Descendants(ns2 + "VariationChild").Descendants())
                                                                                {
                                                                                    if (Convert.ToString(element.Name.LocalName) == "Identifiers" || Convert.ToString(element.Name.LocalName) == "MarketplaceId" || Convert.ToString(element.Name.LocalName) == "MarketplaceASIN")
                                                                                    {
                                                                                        //Skipped using values of Identifiers,MarketplaceIds, MarketplaceASINs
                                                                                    }
                                                                                    else if (Convert.ToString(element.Name.LocalName) == "ASIN")
                                                                                    {
                                                                                        //Creating ASIN list to pass for adding variation details.
                                                                                        ASINList.Add(Convert.ToString(element.Value));
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        strKeyValuePairs = strKeyValuePairs + "," + Convert.ToString(element.Name.LocalName) + "~" + Convert.ToString(element.Value);
                                                                                    }
                                                                                }

                                                                                if (strKeyValuePairs != "" && strKeyValuePairs.Length > 0)
                                                                                {
                                                                                    varie.Name = Convert.ToString(ASINList[inCnt]);
                                                                                    varie.Value = strKeyValuePairs.Trim(',');
                                                                                    vCollection.VariationList.Add(varie);
                                                                                }
                                                                                inCnt = inCnt + 1;
                                                                            }
                                                                        }
                                                                    }

                                                                    GetProductDetailsForASIN(objSvcConfigInfo, ASINList, ref vCollection, ref dtProdListings);
                                                                    objAmazonProduct.ItemVariations = vCollection;
                                                                    lstAmazonProducts.Add(objAmazonProduct);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (product.IsSetAttributeSets())
                                                        {
                                                            foreach (var attribute in product.AttributeSets.Any)
                                                            {
                                                                xml = ProductsUtil.FormatXml((System.Xml.XmlElement)attribute);
                                                                objAmazonProduct = GetProductDetailsFromAttributeSet(xml);
                                                                lstAmazonProducts.Add(objAmazonProduct);
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (product.IsSetAttributeSets())
                                                    {
                                                        foreach (var attribute in product.AttributeSets.Any)
                                                        {
                                                            xml = ProductsUtil.FormatXml((System.Xml.XmlElement)attribute);
                                                            objAmazonProduct = GetProductDetailsFromAttributeSet(xml);
                                                            lstAmazonProducts.Add(objAmazonProduct);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    objAmazonProductList.LstAmazonProduct = lstAmazonProducts;
                }
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
            return objAmazonProductList;
        }

        public AmazonProduct GetProductDetailsForASIN(ServiceConfigInfo objSvcConfigInfo, List<string> ASINList, ref VariationCollection vCollection, ref DataTable dtProductListings)
        {
            DataTable dtProductDetails = new DataTable();
            AmazonProduct objAmazonProduct = new AmazonProduct();
            Product objProduct = new Product();
            string xml = "";
            GetMatchingProductForIdRequest request = new GetMatchingProductForIdRequest();

            try
            {
                LoginConfigurations(objSvcConfigInfo);
                request.SellerId = objSvcConfigInfo.MerchantID;
                request.MarketplaceId = objSvcConfigInfo.MarketPlaceID;
                request.IdType = "ASIN";
                IdListType objIdListType = new IdListType();
                List<string> tmpASINList = new List<string>();

                for (int intCnt = 0; intCnt < ASINList.Count; intCnt++)
                {
                    tmpASINList.Add(ASINList[intCnt]);
                    objIdListType.Id = tmpASINList;
                    request.IdList = objIdListType;

                    GetMatchingProductForIdResponse response = ProductsService.GetMatchingProductForId(request);
                    List<GetMatchingProductForIdResult> getMatchingProductForIdResultList = response.GetMatchingProductForIdResult;
                    foreach (GetMatchingProductForIdResult getMatchingProductForIdResult in getMatchingProductForIdResultList)
                    {
                        if (getMatchingProductForIdResult.IsSetProducts())
                        {
                            ProductList products = getMatchingProductForIdResult.Products;
                            List<Product> productList = products.Product;

                            foreach (Product product in productList)
                            {

                                if (product.IsSetAttributeSets())
                                {
                                    foreach (var attribute in product.AttributeSets.Any)
                                    {
                                        xml = ProductsUtil.FormatXml((System.Xml.XmlElement)attribute);
                                        objAmazonProduct = GetProductDetailsFromAttributeSet(xml);

                                        if (dtProductListings.Rows.Count > 0)
                                        {
                                            DataRow[] drProd;
                                            drProd = dtProductListings.Select("asin1='" + Convert.ToString(ASINList[intCnt]) + "'");
                                            if (drProd != null && drProd.Length > 0)
                                            {
                                                objAmazonProduct.SellerSKU = Convert.ToString(drProd[0]["SellerSKU"]);
                                            }

                                            foreach (DataRow drItem in drProd)
                                            {
                                                drItem["IsChild"] = 1;
                                                dtProductListings.AcceptChanges();
                                            }
                                        }

                                        if (product.Identifiers.IsSetMarketplaceASIN())
                                            objAmazonProduct.ASIN = product.Identifiers.MarketplaceASIN.ASIN;

                                        vd = new VariationDetails();
                                        vd.VariationTitle = Convert.ToString(objAmazonProduct.Title);
                                        vd.ListPrice = Convert.ToDouble(objAmazonProduct.Price);
                                        vd.Quantity = Convert.ToInt32(objAmazonProduct.Quantity);
                                        vd.ReOrder = 0;
                                        vd.SKU = Convert.ToString(objAmazonProduct.SellerSKU);
                                        vd.VariationName = Convert.ToString(objAmazonProduct.ASIN);

                                        IEnumerable<Variations> query = vCollection.VariationList.Where(key => key.Name.Contains(objAmazonProduct.ASIN));
                                        foreach (Variations key in query)
                                        {
                                            vd.VariationValue = Convert.ToString(key.Value);
                                        }
                                        vCollection.VariationDetail.Add(vd);
                                    }
                                }
                            }
                            objAmazonProduct.ItemVariations = vCollection;
                        }
                    }
                    tmpASINList.Remove(ASINList[intCnt]);
                }
            }

            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
            return objAmazonProduct;
        }

        public AmazonProductList GetOrderProductDetailsForSellerSKU(ServiceConfigInfo objSvcConfigInfo, string SellerSKU, ref DataTable dtProdListings, DataTable dtCFields = null, DataTable dtCFValues = null)
        {
            DataTable dtProductDetails = new DataTable();
            AmazonProduct objAmazonProduct = new AmazonProduct();

            AmazonProductList objAmazonProductList = new AmazonProductList();
            List<AmazonProduct> lstAmazonProducts = new List<AmazonProduct>();
            Product objProduct = new Product();
            string xml = "";
            GetMatchingProductForIdRequest request = new GetMatchingProductForIdRequest();
            VariationCollection vCollection = new VariationCollection();
            vCollection.VariationDetail = new List<VariationDetails>();
            vCollection.VariationList = new List<Variations>();

            string strKeyValuePairs = "";
            string strKeys = "", strValues = "";
            string strChildASIN = "";
            List<string> ASINList = new List<string>();
            double dblHeight = 0, dblWidth = 0, dblLength = 0, dblWeight = 0;
            double dblShippingHeight = 0, dblShippingWidth = 0, dblShippingLength = 0, dblShippingWeight = 0;

            try
            {
                LoginConfigurations(objSvcConfigInfo);
                request.SellerId = objSvcConfigInfo.MerchantID;
                request.MarketplaceId = objSvcConfigInfo.MarketPlaceID;
                request.IdType = "SellerSKU";
                List<string> SellerSKUList = new List<string>();
                SellerSKUList.Add(SellerSKU);
                IdListType objIdListType = new IdListType();
                objIdListType.Id = SellerSKUList;
                request.IdList = objIdListType;

                GetMatchingProductForIdResponse response = ProductsService.GetMatchingProductForId(request);
                List<GetMatchingProductForIdResult> getMatchingProductForIdResultList = response.GetMatchingProductForIdResult;
                foreach (GetMatchingProductForIdResult getMatchingProductForIdResult in getMatchingProductForIdResultList)
                {

                    if (getMatchingProductForIdResult.IsSetProducts())
                    {
                        ProductList products = getMatchingProductForIdResult.Products;
                        List<Product> productList = products.Product;
                        foreach (Product product in productList)
                        {

                            if (product.Relationships.Any.Count == 0)
                            {
                                if (product.IsSetAttributeSets())
                                {
                                    foreach (var attribute in product.AttributeSets.Any)
                                    {
                                        xml = ProductsUtil.FormatXml((System.Xml.XmlElement)attribute);
                                        objAmazonProduct = GetProductDetailsFromAttributeSet(xml);

                                        dblLength = Convert.ToDouble(objAmazonProduct.Length);
                                        dblHeight = Convert.ToDouble(objAmazonProduct.Height);
                                        dblWidth = Convert.ToDouble(objAmazonProduct.Width);
                                        dblWeight = Convert.ToDouble(objAmazonProduct.Weight);

                                        dblShippingLength = Convert.ToDouble(objAmazonProduct.ShippingLength);
                                        dblShippingHeight = Convert.ToDouble(objAmazonProduct.ShippingHeight);
                                        dblShippingWidth = Convert.ToDouble(objAmazonProduct.ShippingWidth);
                                        dblShippingWeight = Convert.ToDouble(objAmazonProduct.ShippingWeight);
                                    }
                                }

                                if (product.IsSetIdentifiers())
                                {
                                    if (product.Identifiers.IsSetMarketplaceASIN())
                                        objAmazonProduct.ASIN = product.Identifiers.MarketplaceASIN.ASIN;

                                    strChildASIN = Convert.ToString(objAmazonProduct.ASIN);

                                    if (product.Identifiers.IsSetSKUIdentifier())
                                    {
                                        objAmazonProduct.SellerSKU = product.Identifiers.SKUIdentifier.SellerSKU;
                                    }
                                    else
                                    {
                                        objAmazonProduct.SellerSKU = SellerSKU;
                                    }
                                }
                            }


                            if (product.IsSetRelationships())
                            {
                                if (product.Relationships.Any.Count == 0)
                                {
                                    if (product.IsSetAttributeSets())
                                    {
                                        foreach (var attribute in product.AttributeSets.Any)
                                        {
                                            xml = ProductsUtil.FormatXml((System.Xml.XmlElement)attribute);
                                            objAmazonProduct = GetProductDetailsFromAttributeSet(xml);
                                        }
                                    }

                                    if (product.IsSetIdentifiers())
                                    {
                                        if (product.Identifiers.IsSetMarketplaceASIN())
                                            objAmazonProduct.ASIN = product.Identifiers.MarketplaceASIN.ASIN;

                                        strChildASIN = Convert.ToString(objAmazonProduct.ASIN);

                                        if (product.Identifiers.IsSetSKUIdentifier())
                                        {
                                            objAmazonProduct.SellerSKU = product.Identifiers.SKUIdentifier.SellerSKU;
                                        }
                                        else
                                        {
                                            objAmazonProduct.SellerSKU = SellerSKU;
                                        }
                                    }
                                    lstAmazonProducts.Add(objAmazonProduct);
                                }
                                else
                                {
                                    if (product.Identifiers.IsSetMarketplaceASIN())
                                        objAmazonProduct.ASIN = product.Identifiers.MarketplaceASIN.ASIN;
                                    strChildASIN = Convert.ToString(objAmazonProduct.ASIN);

                                    string strVariationParentASIN = "";
                                    foreach (var relationship in product.Relationships.Any)
                                    {
                                        
                                        //Check whether Amazon Item properties have been any variation attributes are exists or not ?
                                        //If any attributes are there than we are adding / updating their ListDetails as per need.
                                        if (dtCFields != null && dtCFields.Rows.Count > 0)
                                        {

                                            foreach (XmlNode objXMLMainNode in ((System.Xml.XmlElement)relationship).ChildNodes)
                                            {
                                                if (Convert.ToString(objXMLMainNode.ParentNode.LocalName) == "VariationParent")
                                                {
                                                    if (objXMLMainNode.ChildNodes.Count > 0)
                                                    {
                                                        strVariationParentASIN = Convert.ToString(objXMLMainNode.LastChild.ChildNodes[1].ChildNodes[0].Value);
                                                        if (strVariationParentASIN != "")
                                                        {
                                                            //ASINList.Add(strVariationParentASIN);
                                                            LoginConfigurations(objSvcConfigInfo);
                                                            request.SellerId = objSvcConfigInfo.MerchantID;
                                                            request.MarketplaceId = objSvcConfigInfo.MarketPlaceID;
                                                            request.IdType = "ASIN";
                                                            List<string> ASINParentList = new List<string>();
                                                            ASINParentList.Add(strVariationParentASIN);
                                                            IdListType objIdParentListType = new IdListType();
                                                            objIdParentListType.Id = ASINParentList;
                                                            request.IdList = objIdParentListType;

                                                            GetMatchingProductForIdResponse responseParent = ProductsService.GetMatchingProductForId(request);
                                                            List<GetMatchingProductForIdResult> getMatchingParentProductForIdResultList = responseParent.GetMatchingProductForIdResult;
                                                            foreach (GetMatchingProductForIdResult getMatchingParentProductForIdResult in getMatchingParentProductForIdResultList)
                                                            {

                                                                if (getMatchingParentProductForIdResult.IsSetProducts())
                                                                {
                                                                    ProductList ParentProducts = getMatchingParentProductForIdResult.Products;
                                                                    List<Product> ParentProductList = ParentProducts.Product;

                                                                    foreach (Product MainProduct in ParentProductList)
                                                                    {
                                                                        if (MainProduct.IsSetAttributeSets())
                                                                        {

                                                                            foreach (var attribute in MainProduct.AttributeSets.Any)
                                                                            {
                                                                                xml = ProductsUtil.FormatXml((System.Xml.XmlElement)attribute);
                                                                                objAmazonProduct = GetProductDetailsFromAttributeSet(xml);
                                                                            }

                                                                            if (MainProduct.Identifiers.IsSetMarketplaceASIN())
                                                                                objAmazonProduct.ASIN = MainProduct.Identifiers.MarketplaceASIN.ASIN;

                                                                            if (MainProduct.Identifiers.IsSetSKUIdentifier())
                                                                                objAmazonProduct.SellerSKU = MainProduct.Identifiers.SKUIdentifier.SellerSKU;

                                                                            objAmazonProduct.Length = Convert.ToString(dblLength);
                                                                            objAmazonProduct.Height = Convert.ToString(dblHeight);
                                                                            objAmazonProduct.Width = Convert.ToString(dblWidth);
                                                                            objAmazonProduct.Weight = Convert.ToString(dblWeight);

                                                                            objAmazonProduct.ShippingLength = Convert.ToString(dblShippingLength);
                                                                            objAmazonProduct.ShippingHeight = Convert.ToString(dblShippingHeight);
                                                                            objAmazonProduct.ShippingWidth = Convert.ToString(dblShippingWidth);
                                                                            objAmazonProduct.ShippingWeight = Convert.ToString(dblShippingWeight);
                                                                        }

                                                                        if (MainProduct.IsSetRelationships())
                                                                        {
                                                                            int inCnt = 0;
                                                                            //int oldCnt = Convert.ToInt32(MainProduct.Relationships.Any.Count);
                                                                            foreach (var relationshipChild in MainProduct.Relationships.Any)
                                                                            {
                                                                                xml = ProductsUtil.FormatXml((System.Xml.XmlElement)relationshipChild);
                                                                                var xDoc = XDocument.Parse(xml);
                                                                                XNamespace ns2 = "http://mws.amazonservices.com/schema/Products/2011-10-01/default.xsd";

                                                                                strKeyValuePairs = "";
                                                                                varie = new Variations();
                                                                                foreach (XElement element in xDoc.Descendants(ns2 + "VariationChild").Descendants())
                                                                                {
                                                                                    if (Convert.ToString(element.Name.LocalName) == "Identifiers" || Convert.ToString(element.Name.LocalName) == "MarketplaceId" || Convert.ToString(element.Name.LocalName) == "MarketplaceASIN")
                                                                                    {
                                                                                        //Skipped using values of Identifiers,MarketplaceIds, MarketplaceASINs
                                                                                    }
                                                                                    else if (Convert.ToString(element.Name.LocalName) == "ASIN" )
                                                                                    {
                                                                                        if (strChildASIN == Convert.ToString(element.Value))
                                                                                        {
                                                                                            //Creating ASIN list to pass for adding variation details.
                                                                                            ASINList.Add(Convert.ToString(element.Value));
                                                                                        }
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        strKeyValuePairs = strKeyValuePairs + "," + Convert.ToString(element.Name.LocalName) + "~" + Convert.ToString(element.Value);
                                                                                    }
                                                                                }

                                                                                if (strKeyValuePairs != "" && strKeyValuePairs.Length > 0 && ASINList.Count > 0 && strChildASIN == Convert.ToString(ASINList[0]))
                                                                                {
                                                                                    varie.Name = Convert.ToString(ASINList[0]);
                                                                                    varie.Value = strKeyValuePairs.Trim(',');
                                                                                    vCollection.VariationList.Add(varie);
                                                                                    break;
                                                                                }
                                                                                inCnt = inCnt + 1;
                                                                            }
                                                                        }
                                                                    }

                                                                    GetProductDetailsForASIN(objSvcConfigInfo, ASINList, ref vCollection, ref dtProdListings);
                                                                    objAmazonProduct.ItemVariations = vCollection;
                                                                    lstAmazonProducts.Add(objAmazonProduct);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (product.IsSetAttributeSets())
                                                        {
                                                            foreach (var attribute in product.AttributeSets.Any)
                                                            {
                                                                xml = ProductsUtil.FormatXml((System.Xml.XmlElement)attribute);
                                                                objAmazonProduct = GetProductDetailsFromAttributeSet(xml);
                                                                lstAmazonProducts.Add(objAmazonProduct);
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (product.IsSetAttributeSets())
                                                    {
                                                        foreach (var attribute in product.AttributeSets.Any)
                                                        {
                                                            xml = ProductsUtil.FormatXml((System.Xml.XmlElement)attribute);
                                                            objAmazonProduct = GetProductDetailsFromAttributeSet(xml);
                                                            lstAmazonProducts.Add(objAmazonProduct);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    objAmazonProductList.LstAmazonProduct = lstAmazonProducts;
                }
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
            return objAmazonProductList;
        }

        public AmazonProduct GetProductDetailsFromAttributeSet(string xml)
        {
            AmazonProduct objAmazonProduct = new AmazonProduct();
            DataTable dt = new DataTable();
            try
            {
                var xDoc = XDocument.Parse(xml); //or XDocument.Load(filename);
                XNamespace ns = "http://mws.amazonservices.com/schema/Products/2011-10-01/default.xsd";
                foreach (XElement element in xDoc.Descendants(ns + "ItemAttributes").Descendants())
                {
                    if (element.Name == (ns + "Title"))
                    {
                        objAmazonProduct.Title = element.Value;
                    }
                    if (element.Name == (ns + "Model"))
                    {
                        objAmazonProduct.ModelNumber = element.Value;
                    }
                    if (element.Name == (ns + "Manufacturer"))
                    {
                        objAmazonProduct.Manufacturer = element.Value;
                    }
                    if (element.Name == (ns + "Warranty"))
                    {
                        objAmazonProduct.Warranty = element.Value;
                    }
                    if (element.Name == (ns + "PackageQuantity"))
                    {
                        objAmazonProduct.Quantity = element.Value;
                    }
                    if (element.Name == (ns + "Feature"))
                    {
                        objAmazonProduct.Description = element.Value + objAmazonProduct.Description;
                    }
                    if (element.Name == (ns + "Brand"))
                    {
                        objAmazonProduct.Brand = element.Value;
                    }

                    if (element.Name == (ns + "ListPrice"))
                    {

                        var ListPrice = element.Descendants()
                                                 .Select(dim => new
                                                 {
                                                     Type = dim.Name.LocalName,
                                                     Value = dim.Value
                                                 })
                                               .ToList();
                        objAmazonProduct.Price = ListPrice[0].Value ?? "0";
                    }
                    if (element.Name == (ns + "ItemDimensions"))
                    {
                        var itemDimens = element.Descendants().Select(dim => new
                                                 {
                                                     Type = dim.Name.LocalName,
                                                     Unit = dim.Attribute("Units").Value,
                                                     Value = dim.Value
                                                 })
                                                .ToList();
                        foreach (var dimensions in itemDimens)
                        {
                            if (dimensions.Type == "Height")
                                objAmazonProduct.Height = dimensions.Value;
                            else if (dimensions.Type == "Length")
                            {
                                objAmazonProduct.Length = dimensions.Value;
                                objAmazonProduct.LengthUOM = dimensions.Unit;
                            }
                            else if (dimensions.Type == "Width")
                                objAmazonProduct.Width = dimensions.Value;
                            else if (dimensions.Type == "Weight")
                            {
                                objAmazonProduct.Weight = dimensions.Value;
                                objAmazonProduct.WeightUOM = dimensions.Unit;
                            }
                        }
                    }
                    if (element.Name == (ns + "PackageDimensions"))
                    {
                        var PackageDimens = element.Descendants().Select(dim => new
                        {
                            Type = dim.Name.LocalName,
                            Unit = dim.Attribute("Units").Value,
                            Value = dim.Value
                        })
                        .ToList();
                        foreach (var dimensions in PackageDimens)
                        {
                            if (dimensions.Type == "Height")
                                objAmazonProduct.ShippingHeight = dimensions.Value;
                            else if (dimensions.Type == "Length")
                            {
                                objAmazonProduct.ShippingLength = dimensions.Value;
                                objAmazonProduct.LengthUOM = dimensions.Unit;
                            }
                            else if (dimensions.Type == "Width")
                                objAmazonProduct.ShippingWidth = dimensions.Value;
                            else if (dimensions.Type == "Weight")
                            {
                                objAmazonProduct.ShippingWeight = dimensions.Value;
                                objAmazonProduct.WeightUOM = dimensions.Unit;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return objAmazonProduct;
        }

        #endregion Products Operation

        #endregion Operation Contracts

        #region Helper Functions

        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private MemoryStream StringToStream(string str)
        {
            try
            {
                byte[] byteArray = Encoding.UTF8.GetBytes(str);
                //FileStream fs = new FileStream(newFile, FileMode.CreateNew);
                MemoryStream memoryStream = new MemoryStream();
                TextWriter tw = new StreamWriter(memoryStream);

                tw.WriteLine("str");
                return memoryStream;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// DataContract Serializer Serialize the Object to MemormStream by XMLTextWriter
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public MemoryStream DataContractSerializeToStream<T>(T obj)
        {
            MemoryStream memorystream = new MemoryStream();
            MemoryStream ms = new MemoryStream();
            MemoryStream ms2 = new MemoryStream();
            MemoryStream ms3 = new MemoryStream();

            try
            {
                XmlWriter writer = new XmlTextWriter(memorystream, Encoding.Unicode);
                DataContractSerializer serializer = new DataContractSerializer(typeof(T));
                // Serialize using the XmlTextWriter.
                serializer.WriteObject(writer, obj);
                memorystream.Position = 0;

                DataContractSerializer binarydc = new DataContractSerializer(typeof(T));
                XmlDictionaryWriter writer1 = XmlDictionaryWriter.CreateBinaryWriter(ms);
                binarydc.WriteObject(writer1, obj);

                BinaryFormatter f = new BinaryFormatter();
                f.Serialize(ms2, obj);

            }
            catch (Exception ex)
            {

                throw;
            }
            return ms;
            //return ms2;
            //return memorystream;
        }

        /// <summary>
        /// DataContract Serializer Serialize the Object to MemormStream by XMLTextWriter
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public MemoryStream BinaryFormatterToStream<T>(T obj)
        {
            MemoryStream memorystream = new MemoryStream();
            BinaryFormatter binFormat = new BinaryFormatter();
            try
            {
                XmlWriter writer = new XmlTextWriter(memorystream, Encoding.Unicode);
                DataContractSerializer serializer = new DataContractSerializer(typeof(T));
                // Serialize using the XmlTextWriter.
                serializer.WriteObject(writer, obj);
                memorystream.Position = 0;
            }
            catch (Exception)
            {

                throw;
            }
            return memorystream;
        }

        #endregion Helper Functions

        public COrdersList Get123(ServiceConfigInfo objSvcConfigInfo)
        {
            COrdersList objOrdersDetailsList = new COrdersList();
            COrders objOrders;
            LoginConfigurations(objSvcConfigInfo);
            ListOrdersRequest request = new ListOrdersRequest();
            request.MarketplaceId = new List<string>(new string[] { objSvcConfigInfo.MarketPlaceID });
            request.SellerId = objSvcConfigInfo.MerchantID;
            request.CreatedAfter = new DateTime(2012, 4, 25);//Sample Input is Given

            int a = -1;
            string AddressStreet = "";//To concatenate Address Lines
            ListOrdersResponse response = OrdersService.ListOrders(request);//Service Request
            //If gets valid List Order Response
            if (response.IsSetListOrdersResult())
            {
                ListOrdersResult listOrdersResult = response.ListOrdersResult;
                if (listOrdersResult.IsSetCreatedBefore())
                {
                    //Console.WriteLine("                CreatedBefore");
                    //Console.WriteLine("                    {0}", listOrdersResult.CreatedBefore);
                }
                if (listOrdersResult.IsSetLastUpdatedBefore())
                {
                    //Console.WriteLine("                LastUpdatedBefore");
                    //Console.WriteLine("                    {0}", listOrdersResult.LastUpdatedBefore);
                }
                if (listOrdersResult.IsSetOrders())
                {
                    List<Order> orderList = listOrdersResult.Orders;
                    List<COrders> COrdersList = new List<COrders>();
                    foreach (Order order in orderList)
                    {
                        objOrders = new COrders();
                        if (order.IsSetOrderStatus())
                        {
                            //Reads only valid Orders Order Status = 1.Shipped, 2.PartiallyShipped, 3. Unshipped
                            if ((order.OrderStatus == OrderStatusEnum.Shipped.ToString()) || (order.OrderStatus == OrderStatusEnum.PartiallyShipped.ToString()) || (order.OrderStatus == OrderStatusEnum.Unshipped.ToString()))
                            {
                                if (order.IsSetAmazonOrderId())
                                {
                                    objOrders.AmazonOrderId = order.AmazonOrderId;
                                }
                                if (order.IsSetSellerOrderId())
                                {
                                    objOrders.SellerOrderId = order.SellerOrderId;
                                }
                                if (order.IsSetPurchaseDate())
                                {
                                    objOrders.PurchaseDate = order.PurchaseDate.ToString();
                                }
                                if (order.IsSetLastUpdateDate())
                                {
                                    objOrders.LastUpdateDate = order.LastUpdateDate.ToString();
                                }
                                if (order.IsSetPaymentMethod())
                                {
                                    objOrders.PaymentMethod = order.PaymentMethod.ToString();//PaymentMethod Enum
                                }

                                if (order.IsSetBuyerName())
                                {
                                    objOrders.BuyerName = order.BuyerName;
                                }
                                if (order.IsSetBuyerEmail())//If contains Email Address
                                {
                                    objOrders.BuyerEmail = order.BuyerEmail;
                                }
                                if (order.IsSetFulfillmentChannel())
                                {
                                    objOrders.FulfillmentChannel = order.FulfillmentChannel.ToString();//FulfillmentChannel Enum

                                }
                                if (order.IsSetSalesChannel())
                                {
                                    objOrders.SalesChannel = order.SalesChannel;
                                }
                                if (order.IsSetOrderChannel())
                                {
                                    objOrders.OrderChannel = order.OrderChannel;
                                }
                                if (order.IsSetShipmentServiceLevelCategory())
                                {
                                    objOrders.ShipmentServiceLevelCategory = order.ShipmentServiceLevelCategory;
                                }
                                if (order.IsSetShipServiceLevel())
                                {
                                    objOrders.ShipServiceLevel = order.ShipServiceLevel;
                                }
                                if (order.IsSetShippingAddress())
                                {
                                    objOrders.ShippingAddress = new CAddress();
                                    Address shippingAddress = order.ShippingAddress;
                                    if (shippingAddress.IsSetName())
                                    {
                                        objOrders.ShippingAddress.Name = shippingAddress.Name;
                                    }
                                    if (shippingAddress.IsSetAddressLine1())
                                    {
                                        objOrders.ShippingAddress.AddressLine1 = shippingAddress.AddressLine1;

                                    }
                                    if (shippingAddress.IsSetAddressLine2())
                                    {
                                        objOrders.ShippingAddress.AddressLine2 = shippingAddress.AddressLine2;
                                    }
                                    if (shippingAddress.IsSetAddressLine3())
                                    {
                                        objOrders.ShippingAddress.AddressLine3 = shippingAddress.AddressLine3;
                                    }
                                    if (shippingAddress.IsSetCity())
                                    {
                                        objOrders.ShippingAddress.City = shippingAddress.City;
                                    }
                                    if (shippingAddress.IsSetCounty())
                                    {
                                        objOrders.ShippingAddress.County = shippingAddress.County;
                                    }
                                    if (shippingAddress.IsSetDistrict())
                                    {
                                        objOrders.ShippingAddress.District = shippingAddress.District;
                                    }
                                    if (shippingAddress.IsSetStateOrRegion())
                                    {
                                        objOrders.ShippingAddress.StateOrRegion = shippingAddress.StateOrRegion;
                                    }
                                    if (shippingAddress.IsSetPostalCode())
                                    {
                                        objOrders.ShippingAddress.PostalCode = shippingAddress.PostalCode;
                                    }
                                    if (shippingAddress.IsSetCountryCode())
                                    {
                                        objOrders.ShippingAddress.CountryCode = shippingAddress.CountryCode;
                                    }
                                    if (shippingAddress.IsSetPhone())
                                    {
                                        objOrders.ShippingAddress.Phone = shippingAddress.Phone;
                                    }
                                }
                                if (order.IsSetOrderTotal())
                                {
                                    objOrders.OrderTotal = new CMoney();
                                    Money orderTotal = order.OrderTotal;
                                    if (orderTotal.IsSetCurrencyCode())
                                    {
                                        objOrders.OrderTotal.CurrencyCode = orderTotal.CurrencyCode;

                                    }
                                    if (orderTotal.IsSetAmount())
                                    {
                                        objOrders.OrderTotal.Amount = orderTotal.Amount;
                                    }
                                }
                                if (order.IsSetNumberOfItemsShipped())
                                {
                                    objOrders.NumberOfItemsShipped = order.NumberOfItemsShipped;
                                }
                                if (order.IsSetNumberOfItemsUnshipped())
                                {
                                    objOrders.NumberOfItemsUnshipped = order.NumberOfItemsUnshipped;
                                }
                                if (order.IsSetMarketplaceId())
                                {
                                    objOrders.MarketplaceId = order.MarketplaceId;
                                }
                                if (order.IsSetPaymentExecutionDetail())
                                {
                                    List<PaymentExecutionDetailItem> paymentExecutionDetailItemList = order.PaymentExecutionDetail;
                                    List<CPaymentExecutionDetailItem> PaymentExecItems = new List<CPaymentExecutionDetailItem>();
                                    foreach (PaymentExecutionDetailItem paymentExecutionDetailItem in paymentExecutionDetailItemList)
                                    {
                                        CPaymentExecutionDetailItem objPaymentExecDetailItem = new CPaymentExecutionDetailItem();

                                        if (paymentExecutionDetailItem.IsSetPayment())
                                        {
                                            Money payment = paymentExecutionDetailItem.Payment;
                                            objPaymentExecDetailItem.PaymentField = new CMoney();

                                            if (payment.IsSetCurrencyCode())
                                            {
                                                objPaymentExecDetailItem.PaymentField.CurrencyCode = payment.CurrencyCode;
                                            }
                                            if (payment.IsSetAmount())
                                            {
                                                objPaymentExecDetailItem.PaymentField.Amount = payment.Amount;
                                            }
                                        }
                                        if (paymentExecutionDetailItem.IsSetPaymentMethod())
                                        {
                                            objPaymentExecDetailItem.SubPaymentMethodField = paymentExecutionDetailItem.PaymentMethod;
                                        }
                                        PaymentExecItems.Add(objPaymentExecDetailItem);
                                    }
                                    objOrders.PaymentExecItems = PaymentExecItems;
                                }
                                COrdersList.Add(objOrders);
                            }
                        }
                    }
                    objOrdersDetailsList.Orders = COrdersList;
                }
            }
            // return DataContractSerializeToStream<COrdersList>(objOrdersDetailsList);
            return objOrdersDetailsList;
        }

    }
}
