﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceProcess;
using AMWS_Communicator;
using EBay_Communicator;
using Google_Communicator;
using Magento_Communicator;
using System.ServiceModel;

namespace Host_Communicator
{
    class Program : ServiceBase
    {
        #region Service Base Implementation
       
        ServiceHost serviceHost;
        ServiceHost AmazonSvcHost = null;
        ServiceHost EBaySvcHost = null;
        ServiceHost GoogleSvcHost = null;
        ServiceHost MagentoSvcHost = null;


        public static void Main()
        {
            ServiceBase.Run(new Program());
        }

        protected override void OnStart(string[] args)
        {
            HostService();
        }

        protected override void OnStop()
        {
            StopService();
        }

        #endregion  Service Base Implementation

        //static void Main()
        //{
        //    try
        //    {
        //        ServiceHost AmazonSvcHost = null;
        //        ServiceHost EBaySvcHost = null;
        //        ServiceHost GoogleSvcHost = null;
        //        ServiceHost MagentoSvcHost = null;

        //        if (AmazonSvcHost != null)
        //        {
        //            AmazonSvcHost.Close();
        //        }
        //        if (EBaySvcHost != null)
        //        {
        //            EBaySvcHost.Close();
        //        }
        //        if (GoogleSvcHost != null)
        //        {
        //            GoogleSvcHost.Close();
        //        }
        //        if (MagentoSvcHost != null)
        //        {
        //            MagentoSvcHost.Close();
        //        }
        //        AmazonSvcHost = new ServiceHost(typeof(AmazonCommunicator));
        //        AmazonSvcHost.Open();// Starts the Amazon Service

        //        EBaySvcHost = new ServiceHost(typeof(eBayCommunicator));
        //        EBaySvcHost.Open();// Starts the EBay Service

        //        GoogleSvcHost = new ServiceHost(typeof(GoogleCommunicator));
        //        GoogleSvcHost.Open();// Starts the EBay Service

        //        MagentoSvcHost = new ServiceHost(typeof(MagentoCommunicator));
        //        MagentoSvcHost.Open();// Starts the EBay Service
        //        Console.WriteLine("Press Enter to Stop the Online Marketplace Communicator Service");
        //        Console.ReadLine();
        //        if (AmazonSvcHost != null && AmazonSvcHost.State == CommunicationState.Opened)
        //        {
        //            AmazonSvcHost.Close();
        //            AmazonSvcHost = null;
        //        }
        //        if (EBaySvcHost != null && EBaySvcHost.State == CommunicationState.Opened)
        //        {
        //            EBaySvcHost.Close();
        //            EBaySvcHost = null;
        //        }
        //        if (GoogleSvcHost != null && GoogleSvcHost.State == CommunicationState.Opened)
        //        {
        //            GoogleSvcHost.Close();
        //            GoogleSvcHost = null;
        //        }
        //        if (MagentoSvcHost != null && MagentoSvcHost.State == CommunicationState.Opened)
        //        {
        //            MagentoSvcHost.Close();
        //            MagentoSvcHost = null;
        //        }
        //    }
        //    catch (Exception Ex)
        //    {
        //        Console.WriteLine("Error " + Ex.Message);
        //        Console.ReadLine();
        //    }
        //}


        #region Service Handlers

        /// <summary>
        ///Host Service and opens the Service
        /// </summary>
        public void HostService()
        {
            try
            {
                if (AmazonSvcHost != null)
                {
                    AmazonSvcHost.Close();
                }
                if (EBaySvcHost != null)
                {
                    EBaySvcHost.Close();
                }
                if (GoogleSvcHost != null)
                {
                    GoogleSvcHost.Close();
                }
                if (MagentoSvcHost != null)
                {
                    MagentoSvcHost.Close();
                }
                AmazonSvcHost = new ServiceHost(typeof(AmazonCommunicator));
                AmazonSvcHost.Open();// Starts the Amazon Service

                EBaySvcHost = new ServiceHost(typeof(eBayCommunicator));
                EBaySvcHost.Open();// Starts the EBay Service

                GoogleSvcHost = new ServiceHost(typeof(GoogleCommunicator));
                GoogleSvcHost.Open();// Starts the EBay Service

                MagentoSvcHost = new ServiceHost(typeof(MagentoCommunicator));
                MagentoSvcHost.Open();// Starts the EBay Service
            }
            catch (Exception ex)
            {
                AmazonSvcHost.Abort();
                EBaySvcHost.Abort();
                GoogleSvcHost.Abort();
                MagentoSvcHost.Abort();
            }


        }

        /// <summary>
        /// Stop the Service
        /// </summary>
        public void StopService()
        {
            try
            {
                if (AmazonSvcHost != null && AmazonSvcHost.State == CommunicationState.Opened)
                {
                    AmazonSvcHost.Close();
                    AmazonSvcHost = null;
                }
                if (EBaySvcHost != null && EBaySvcHost.State == CommunicationState.Opened)
                {
                    EBaySvcHost.Close();
                    EBaySvcHost = null;
                }
                if (GoogleSvcHost != null && GoogleSvcHost.State == CommunicationState.Opened)
                {
                    GoogleSvcHost.Close();
                    GoogleSvcHost = null;
                }
                if (MagentoSvcHost != null && MagentoSvcHost.State == CommunicationState.Opened)
                {
                    MagentoSvcHost.Close();
                    MagentoSvcHost = null;
                }
            }
            catch (Exception ex)
            {
                AmazonSvcHost.Abort();
                EBaySvcHost.Abort();
                GoogleSvcHost.Abort();
                MagentoSvcHost.Abort();
            }
        }

        #endregion Service Handlers
     

    }
}
