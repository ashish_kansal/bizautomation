﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;

namespace Host_Communicator
{
    [RunInstaller(true)]
    public partial class Installer1 : System.Configuration.Install.Installer
    {
        private ServiceProcessInstaller process;
        private System.ServiceProcess.ServiceInstaller service;
        public Installer1()
        {
            InitializeComponent();
            process = new ServiceProcessInstaller();
            process.Account = ServiceAccount.LocalSystem;
            service = new System.ServiceProcess.ServiceInstaller();
            service.ServiceName = "OnlineMarketPlaceCommunicator";
            Installers.Add(process);
            Installers.Add(service);
        }
    }
}
