﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.IO;

using Google.GData.ContentForShopping;
using Google.GData.ContentForShopping.Elements;
using Google.GData.Client;

namespace Google_Communicator
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IGoogleCommunicator" in both code and config file together.
    [ServiceContract]
    public interface IGoogleCommunicator
    {
        [FaultContract(typeof(error))]
        [OperationContract]
        void AddNewProduct(string AccountId, string Email, string Password, Product objProduct);

        [FaultContract(typeof(error))]
        [OperationContract]
        void NewProductBatchProcessing(string AccountId, string Email, string Password, List<Product> lstProducts);

        [FaultContract(typeof(error))]
        [OperationContract]
        void UpdateInventoryBatchProcessing(string AccountId, string Email, string Password, List<Product> lstProducts);

        [FaultContract(typeof(error))]
        [OperationContract]
        void UpdateInventory(string AccountId, string Email, string Password, Product objProduct);


        [FaultContract(typeof(error))]
        [OperationContract]
        void UpdateTrackingNo(string MerchantId, string MerchantKey, string Environment, string GoogleOrderNo, string Carrier, string TrackingNo);


        [FaultContract(typeof(error))]
        [OperationContract]
        string RequestMerchantToken(string MerchantId, string MerchantKey, string Environment);


        [FaultContract(typeof(error))]
        [OperationContract]
        MemoryStream GetOrderNotifications(string MerchantId, string MerchantKey, string Environment, string MerchantToken);

        [FaultContract(typeof(error))]
        [OperationContract]
        void AddTrackingNo(string MerchantId, string MerchantKey, string Environment, string GoogleOrderNo, string Carrier, string TrackingNo);

        [FaultContract(typeof(error))]
        [OperationContract]
        List<Product> GetProductListings(string AccountId, string Email, string Password);

        [FaultContract(typeof(error))]
        [OperationContract]
        Product GetProductById(string AccountId, string Email, string Password,string TargetCountry, string ProductId);
    }

    [DataContract]
    public class error
    {
        [DataMember]
        public string msg;

        [DataMember]
        public string request;
    }

    [DataContract]
    public class Product
    {
        string _ProductId, _Title, _Description, _Manufacturer, _UPC, _Price, _Quantity, _Availability, _TargetCountry, _ProductType, _ProductURL, _ProductImageURL,
            _Height, _Weight, _Length, _Width, _ShippingHeight, _ShippingWeight, _ShippingLength, _ShippingWidth,
        _StandardShippingCharge, _StandardTaxCharge, _GoogleProductCategory, _WeightUOM, _LengthUOM, _ModelNumber, _BizItemCode;

         [DataMember]
        public string BizItemCode
        {
            get { return _BizItemCode; }
            set { _BizItemCode = value; }
        }

         [DataMember]
        public string ModelNumber
        {
            get { return _ModelNumber; }
            set { _ModelNumber = value; }
        }

        [DataMember]
        public string ProductType
        {
            get { return _ProductType; }
            set { _ProductType = value; }
        }

        [DataMember]
        public string GoogleProductCategory
        {
            get { return _GoogleProductCategory; }
            set { _GoogleProductCategory = value; }
        }

        [DataMember]
        public string WeightUOM
        {
            get { return _WeightUOM; }
            set { _WeightUOM = value; }
        }

        [DataMember]
        public string LengthUOM
        {
            get { return _LengthUOM; }
            set { _LengthUOM = value; }
        }

        [DataMember]
        public string ProductId
        {
            get { return _ProductId; }
            set { _ProductId = value; }
        }

        [DataMember]
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        [DataMember]
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        [DataMember]
        public string Manufacturer
        {
            get { return _Manufacturer; }
            set { _Manufacturer = value; }
        }

        [DataMember]
        public string UPC
        {
            get { return _UPC; }
            set { _UPC = value; }
        }

        [DataMember]
        public string Price
        {
            get { return _Price; }
            set { _Price = value; }
        }

        [DataMember]
        public string Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }

        [DataMember]
        public string Availability
        {
            get { return _Availability; }
            set { _Availability = value; }
        }

        [DataMember]
        public string TargetCountry
        {
            get { return _TargetCountry; }
            set { _TargetCountry = value; }
        }

        [DataMember]
        public string ProductURL
        {
            get { return _ProductURL; }
            set { _ProductURL = value; }
        }

        [DataMember]
        public string ProductImageURL
        {
            get { return _ProductImageURL; }
            set { _ProductImageURL = value; }
        }

        [DataMember]
        public string Height
        {
            get { return _Height; }
            set { _Height = value; }
        }

        [DataMember]
        public string Weight
        {
            get { return _Weight; }
            set { _Weight = value; }
        }

        [DataMember]
        public string Length
        {
            get { return _Length; }
            set { _Length = value; }
        }

        [DataMember]
        public string Width
        {
            get { return _Width; }
            set { _Width = value; }
        }

        [DataMember]
        public string ShippingHeight
        {
            get { return _ShippingHeight; }
            set { _ShippingHeight = value; }
        }

        [DataMember]
        public string ShippingWeight
        {
            get { return _ShippingWeight; }
            set { _ShippingWeight = value; }
        }

        [DataMember]
        public string ShippingLength
        {
            get { return _ShippingLength; }
            set { _ShippingLength = value; }
        }

        [DataMember]
        public string ShippingWidth
        {
            get { return _ShippingWidth; }
            set { _ShippingWidth = value; }
        }

        [DataMember]
        public string StandardShippingCharge
        {
            get { return _StandardShippingCharge; }
            set { _StandardShippingCharge = value; }
        }

        [DataMember]
        public string StandardTaxCharge
        {
            get { return _StandardTaxCharge; }
            set { _StandardTaxCharge = value; }
        }
    }
}
