﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.IO;

using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

using Google.GData.ContentForShopping;
using Google.GData.ContentForShopping.Elements;
using Google.GData.Client;


using GCheckout;
using GCheckout.OrderProcessing;
using GCheckout.AutoGen;

namespace Google_Communicator
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "GoogleCommunicator" in both code and config file together.
    public class GoogleCommunicator : IGoogleCommunicator
    {

        public void AddNewProduct(string AccountId, string Email, string Password, Product objProduct)
        {
            try
            {
                string ApplicationName = "BizAutomation";
                ContentForShoppingService GoogleService = new ContentForShoppingService(ApplicationName, AccountId);
                ProductEntry Item = new ProductEntry();

                GoogleService.setUserCredentials(Email, Password);

                //ProductEntry entry = new ProductEntry();
                Item.Title.Text = objProduct.Title;
                Item.Content.Content = objProduct.Description;
                Item.ProductId = objProduct.ProductId;
                Item.ProductType = objProduct.ProductType;
                Item.GoogleProductCategory = objProduct.GoogleProductCategory;
                AtomLink link = new AtomLink();
                link.Rel = "alternate";
                link.Type = "text/html";
                link.HRef = objProduct.ProductURL;
                Item.Links.Add(link);

                Item.ImageLink = new ImageLink(objProduct.ProductImageURL);
                Item.TargetCountry = objProduct.TargetCountry;
                // Item.Language = "en";
                Item.ContentLanguage = "en";
                // Item.Brand = "Epson";
                Item.Condition = "new";
                Item.Gtin = objProduct.UPC;
                Item.Availability = objProduct.Availability;
                Item.Quantity = Convert.ToInt32(objProduct.Quantity);
                Item.Manufacturer = objProduct.Manufacturer;
                Item.Price = new Price("usd", objProduct.Price);
                Item.ShippingWeight = new ShippingWeight(objProduct.WeightUOM, objProduct.ShippingWeight);
                //Item.ModelNumber = objProduct.ModelNumber;
                Shipping shipping = new Shipping();
                shipping.Country = "US";
                shipping.Region = "CA";
                shipping.Service = "Standard Shipping";
                shipping.Price = new ShippingPrice("usd", objProduct.StandardShippingCharge);
                Item.ShippingRules.Add(shipping);

                Tax tax = new Tax();
                tax.Country = "US";
                tax.Region = "CA";
                tax.Rate = "8.25";
                tax.Ship = true;
                Item.TaxRules.Add(tax);

                ProductControl productControl = new ProductControl();
                productControl.RequiredDestinations.Add(new RequiredDestination("ProductSearch"));
                productControl.RequiredDestinations.Add(new RequiredDestination("ProductAds"));
                Item.AppControl = productControl;
                GoogleService.Insert(Item);

            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
        }

        public void NewProductBatchProcessing(string AccountId, string Email, string Password, List<Product> lstProducts)
        {
            List<ProductEntry> Products = new List<ProductEntry>();
            string ApplicationName = "BizAutomation";
            ContentForShoppingService GoogleService = new ContentForShoppingService(ApplicationName, AccountId);
            GoogleService.DryRun = false;
            GoogleService.AccountId = AccountId;

            try
            {
                GoogleService.setUserCredentials(Email, Password);
                //string token = GoogleService.QueryClientLoginToken();

                foreach (Product objProduct in lstProducts)
                {

                    ProductEntry Item = new ProductEntry();
                    Item.Title.Text = objProduct.Title;
                    Item.Content.Content = objProduct.Description;
                    Item.ProductId = objProduct.ProductId;
                    Item.ProductType = objProduct.ProductType;
                    Item.GoogleProductCategory = objProduct.GoogleProductCategory;
                    AtomLink link = new AtomLink();
                    link.Rel = "alternate";
                    link.Type = "text/html";
                    link.HRef = objProduct.ProductURL;
                    Item.Links.Add(link);

                    Item.ImageLink = new ImageLink(objProduct.ProductImageURL);
                    Item.TargetCountry = objProduct.TargetCountry;
                    // Item.Language = "en";
                    Item.ContentLanguage = "en";
                    // Item.Brand = "Epson";
                    Item.Condition = "new";
                    Item.Gtin = objProduct.UPC;
                    Item.Availability = objProduct.Availability;
                    Item.Quantity = Convert.ToInt32(objProduct.Quantity);
                    Item.Manufacturer = objProduct.Manufacturer;
                    Item.Price = new Price("usd", objProduct.Price);
                    Item.ShippingWeight = new ShippingWeight(objProduct.WeightUOM, objProduct.ShippingWeight);

                    Shipping shipping = new Shipping();
                    shipping.Country = "US";
                    shipping.Region = "CA";
                    shipping.Service = "Standard Shipping";
                    shipping.Price = new ShippingPrice("usd", objProduct.StandardShippingCharge);
                    Item.ShippingRules.Add(shipping);

                    Tax tax = new Tax();
                    tax.Country = "US";
                    tax.Region = "CA";
                    tax.Rate = "8.25";
                    tax.Ship = true;
                    Item.TaxRules.Add(tax);
                   // Item.ModelNumber = objProduct.ModelNumber;

                    ProductControl productControl = new ProductControl();
                    productControl.RequiredDestinations.Add(new RequiredDestination("ProductSearch"));
                    productControl.RequiredDestinations.Add(new RequiredDestination("ProductAds"));
                    Item.AppControl = productControl;
                    Item.BatchData = new GDataBatchEntryData(GDataBatchOperationType.insert);
                    // Item.Id = new AtomId(Item.EditUri.ToString());
                    Products.Add(Item);
                }
                ProductFeed insertedFeed = GoogleService.InsertProducts(Products);
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
        }

        public void UpdateInventoryBatchProcessing(string AccountId, string Email, string Password, List<Product> lstProducts)
        {
            List<ProductEntry> Products = new List<ProductEntry>();
            string ApplicationName = "BizAutomation";
            ContentForShoppingService GoogleService = new ContentForShoppingService(ApplicationName, AccountId);
            try
            {
                GoogleService.setUserCredentials(Email, Password);

                foreach (Product objProduct in lstProducts)
                {
                    ProductEntry Item = new ProductEntry();
                    Item.ProductId = objProduct.ProductId;
                    Item.Quantity = Convert.ToInt32(objProduct.Quantity);
                    Item.Availability = "In Stock";
                    Item.Price = new Price("usd", objProduct.Price);
                    Item.Title.Text = objProduct.Title;
                    Item.Content.Content = objProduct.Description;
                    Item.Condition = "new";
                    AtomLink link = new AtomLink();
                    link.Rel = "alternate";
                    link.Type = "text/html";
                    link.HRef = objProduct.ProductURL;
                    Item.Links.Add(link);

                    //ProductControl productControl = new ProductControl();
                    //productControl.RequiredDestinations.Add(new RequiredDestination("ProductSearch"));
                    //productControl.RequiredDestinations.Add(new RequiredDestination("ProductAds"));
                    //Item.AppControl = productControl;
                    Item.BatchData = new GDataBatchEntryData(GDataBatchOperationType.insert);
                    // Item.Id = new AtomId(Item.EditUri.ToString());
                    Products.Add(Item);
                }
                ProductFeed insertedFeed = GoogleService.UpdateProducts(Products);
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
        }

        public void UpdateInventory(string AccountId, string Email, string Password, Product objProduct)
        {
            
            string ApplicationName = "BizAutomation";
            ContentForShoppingService GoogleService = new ContentForShoppingService(ApplicationName, AccountId);
            try
            {
                    GoogleService.setUserCredentials(Email, Password);
                    ProductEntry Item = new ProductEntry();
                    Item.ProductId = objProduct.ProductId;
                    Item.Quantity = Convert.ToInt32(objProduct.Quantity);
                   // Item.Price = new Price("usd", objProduct.Price);

                    GoogleService.Update(Item);
               // ProductFeed insertedFeed = GoogleService.UpdateProducts(Products);
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
        }

        public void UpdateTrackingNo(string MerchantId, string MerchantKey, string Environment, string GoogleOrderNo, string Carrier, string TrackingNo)
        {

            try
            {
                GCheckout.OrderProcessing.DeliverOrderRequest objDeliverOrderRequest = new 
                    GCheckout.OrderProcessing.DeliverOrderRequest(MerchantId, MerchantKey, Environment, GoogleOrderNo, Carrier, TrackingNo);
                RequestReceivedResponse response = new RequestReceivedResponse();
                response = (RequestReceivedResponse)objDeliverOrderRequest.Send().Response;
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
        }

        public void AddTrackingNo(string MerchantId, string MerchantKey, string Environment, string GoogleOrderNo, string Carrier, string TrackingNo)
        {

            try
            {
                GCheckout.OrderProcessing.AddTrackingDataRequest objTrackingDataReq = new
               GCheckout.OrderProcessing.AddTrackingDataRequest(MerchantId, MerchantKey, Environment, GoogleOrderNo, Carrier, TrackingNo);
                GCheckout.AutoGen.RequestReceivedResponse response = new RequestReceivedResponse();
                response = (RequestReceivedResponse)objTrackingDataReq.Send().Response;
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
        }

        public string RequestMerchantToken(string MerchantId, string MerchantKey, string Environment)
        {
            string MerchantToken = "";
            try
            {
                GCheckout.OrderProcessing.NotificationDataTokenRequest token = new GCheckout.OrderProcessing.NotificationDataTokenRequest(MerchantId, MerchantKey, Environment, DateTime.Now.AddDays(-180));
                GCheckout.OrderProcessing.NotificationDataTokenResponse tokenReponse = new GCheckout.OrderProcessing.NotificationDataTokenResponse(token.Send().ResponseXml);

                GCheckout.AutoGen.NotificationDataTokenResponse TokenResp = new GCheckout.AutoGen.NotificationDataTokenResponse();
                TokenResp = (GCheckout.AutoGen.NotificationDataTokenResponse)tokenReponse.Response;
                MerchantToken = TokenResp.continuetoken;

            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
            return MerchantToken;
        }

        public MemoryStream GetOrderNotifications(string MerchantId, string MerchantKey, string Environment, string MerchantToken)
        {
            MemoryStream ms = new MemoryStream();
            try
            {
                GCheckout.OrderProcessing.NotificationDataRequest DataRequest = new GCheckout.OrderProcessing.NotificationDataRequest(MerchantId, MerchantKey, Environment, MerchantToken);
                GCheckout.OrderProcessing.NotificationDataResponse DataResponse = new GCheckout.OrderProcessing.NotificationDataResponse(DataRequest.Send().ResponseXml);
                GCheckout.AutoGen.NotificationDataResponse NotificationResponse = new GCheckout.AutoGen.NotificationDataResponse();
                NotificationResponse = (GCheckout.AutoGen.NotificationDataResponse)DataResponse.Response;
                ms = SerializeToStream<GCheckout.AutoGen.NotificationDataResponse>(NotificationResponse);
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
            return ms;
        }

        /// <summary>
        /// Xml Serializer Serialize the Object to MemormStream by XMLTextWriter
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="obj">Object</param>
        /// <returns>Memory Stream</returns>
        public static MemoryStream SerializeToStream<T>(T obj)
        {
            try
            {
                MemoryStream memorystream = new MemoryStream();
                XmlWriter writer = new XmlTextWriter(memorystream, Encoding.Unicode);
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                // Serialize using the XmlTextWriter.
                serializer.Serialize(writer, obj);
                memorystream.Position = 0;
                return memorystream;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<Product> GetProductListings(string AccountId, string Email, string Password)
        {
            List<Product> lstProducts = new List<Product>();
            try
            {
                string ApplicationName = "BizAutomation";
                ContentForShoppingService GoogleService = new ContentForShoppingService(ApplicationName, AccountId);
                GoogleService.AccountId = AccountId;
                GoogleService.setUserCredentials(Email, Password);
                string token = GoogleService.QueryClientLoginToken();

                ProductFeed objProductFeeds = GoogleService.Query();
                foreach (ProductEntry objProductEntry in objProductFeeds.Entries)
                {
                    Product objProduct = new Product();
                    objProduct.Title = objProductEntry.Title.Text;
                    objProduct.Description = objProductEntry.Content.Content;
                    objProduct.ProductId = objProductEntry.ProductId;
                    objProduct.ProductType = objProductEntry.ProductType;
                    objProduct.GoogleProductCategory = objProductEntry.GoogleProductCategory;

                    objProduct.ProductImageURL = objProductEntry.ImageLink.Value.ToString();
                    if (objProductEntry.Links != null & objProductEntry.Links.Count > 0)
                    {
                        foreach(AtomLink link in objProductEntry.Links)
                        {
                            objProduct.ProductURL = link.AbsoluteUri;
                        }
                    }
                    objProduct.TargetCountry = objProductEntry.TargetCountry;
                    objProduct.UPC = objProductEntry.Gtin;
                    objProduct.Availability = objProductEntry.Availability;
                    objProduct.Quantity = objProductEntry.Quantity.ToString();
                    objProduct.Manufacturer = objProductEntry.Manufacturer;
                    objProduct.Price = objProductEntry.Price.FloatValue.ToString();
                    if (objProductEntry.ShippingWeight != null)
                    {
                        objProduct.ShippingWeight =  objProductEntry.ShippingWeight.Value.ToString();
                    }
                    
                    objProduct.ShippingWeight = (objProductEntry.ShippingWeight != null) ? objProductEntry.ShippingWeight.Value.ToString() : "0";
                    objProduct.WeightUOM = (objProductEntry.ShippingWeight != null) ? objProductEntry.ShippingWeight.Unit: "";
                    
                    if (objProductEntry.ShippingRules != null & objProductEntry.ShippingRules.Count > 0)
                    {
                        foreach (Shipping shipping in objProductEntry.ShippingRules)
                        {
                            if (shipping.Service == "Standard Shipping")
                            {
                                objProduct.StandardShippingCharge = shipping.Price.Value.ToString();
                            }
                        }
                    }
                    lstProducts.Add(objProduct);
                }
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
            return lstProducts;
        }


        public Product GetProductById(string AccountId, string Email, string Password, string TargetCountry, string ProductId)
        {
            Product objProduct = new Product();
            try
            {
                string ApplicationName = "BizAutomation";
                ContentForShoppingService GoogleService = new ContentForShoppingService(ApplicationName, AccountId);
                GoogleService.AccountId = AccountId;
                GoogleService.setUserCredentials(Email, Password);
                string token = GoogleService.QueryClientLoginToken();

                ProductEntry objProductEntry = GoogleService.Get("en", TargetCountry, ProductId);
                objProduct.Title = objProductEntry.Title.Text;
                objProduct.Description = objProductEntry.Content.Content;
                objProduct.ProductId = objProductEntry.ProductId;
                objProduct.ProductType = objProductEntry.ProductType;
                objProduct.GoogleProductCategory = objProductEntry.GoogleProductCategory;
                objProduct.ProductImageURL = objProductEntry.ImageLink.Value.ToString();
                if (objProductEntry.Links != null & objProductEntry.Links.Count > 0)
                {
                    foreach (AtomLink link in objProductEntry.Links)
                    {
                        objProduct.ProductURL = link.AbsoluteUri;
                    }
                }
                objProduct.TargetCountry = objProductEntry.TargetCountry;
                objProduct.UPC = objProductEntry.Gtin;
                objProduct.Availability = objProductEntry.Availability;
                objProduct.Quantity = objProductEntry.Quantity.ToString();
                objProduct.Manufacturer = objProductEntry.Manufacturer;
                objProduct.Price = objProductEntry.Price.FloatValue.ToString();
                if (objProductEntry.ShippingWeight != null)
                {
                    objProduct.ShippingWeight = objProductEntry.ShippingWeight.Value.ToString();
                }

                objProduct.ShippingWeight = (objProductEntry.ShippingWeight != null) ? objProductEntry.ShippingWeight.Value.ToString() : "0";
                objProduct.WeightUOM = (objProductEntry.ShippingWeight != null) ? objProductEntry.ShippingWeight.Unit : "";

                if (objProductEntry.ShippingRules != null & objProductEntry.ShippingRules.Count > 0)
                {
                    foreach (Shipping shipping in objProductEntry.ShippingRules)
                    {
                        if (shipping.Service == "Standard Shipping")
                        {
                            objProduct.StandardShippingCharge = shipping.Price.Value.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
            return objProduct;
        }
    }
}
