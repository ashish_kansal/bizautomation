using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using PushNotificationClient.WebReference1;
using System.Data.SqlClient;
using System.Net;
using System.Collections.Generic;
using BACRM.BusinessLogic.Common;
using System.Security.Cryptography.X509Certificates;
/// <summary>
/// Summary description for clsExchService
/// </summary>
public class clsExchService
{

    public clsExchService()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public void GetNewMails(ItemIdType ItemId, string subscriptionId, string eventName)
    {
        string connectionString = ConfigurationManager.AppSettings["ConnectionString"];
        SqlConnection selectConnection = new SqlConnection(connectionString);

        try
        {
            decimal numDomainId = 0;
            decimal numUserCntId = 0;

            string text3 = "";
            //SqlDataAdapter adapter = new SqlDataAdapter("select vcEmailId,numDomainId from usermaster where SubscriptionId = '" + subscriptionId + "'", selectConnection);
            DataTable dataTable = new DataTable();
            //adapter.Fill(dataTable);

            SqlCommand command = new SqlCommand("USP_GetDetailsFromSubscriptionId", selectConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add("@SubscriptionId", SqlDbType.VarChar, 200).Value = subscriptionId;
            SqlDataAdapter adapter1 = new SqlDataAdapter(command);
            adapter1.Fill(dataTable);
            selectConnection.Close();
            ExchangeServiceBinding esb = new ExchangeServiceBinding();
            if (dataTable.Rows.Count == 1)
            {
                foreach (DataRow row in dataTable.Rows)
                {
                    text3 = (string)row["vcEmailid"];
                    numDomainId = (decimal)row["numDomainId"];
                    numUserCntId = (decimal)row["numUserDetailId"];


                    CCommon objCommon = new CCommon();
                    
                    //DataSet dsUserDetails;
                    string plainText;
                    string Password;
                    string UserName;
                    //  string DomainName;

                    string passPhrase;
                    string saltValue;
                    string hashAlgorithm;
                    int passwordIterations;
                    string initVector;
                    int keySize;

                    plainText = (string)row["vcExchPassword"];                   // can be any string
                    passPhrase = "AnoopSivaGangaNithyaCarl768@@@@#";             // can be any string
                    saltValue = "s@AnoopCarl234358SivaGanga1231943";             // can be "MD5"
                    hashAlgorithm = "SHA1";                                      // can be any number
                    passwordIterations = 2;                                     // must be 16 bytes
                    initVector = "FNitsD4Ab@Anup@@";                            // can be 192 or 128
                    keySize = 256;

                    Password = (string)objCommon.DecryptExch(plainText, passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize);

                    dataTable = objCommon.GetBizDocs();

                    if ((Boolean)row["bitExchangeIntegration"] == true)
                    {
                        ServicePointManager.CertificatePolicy = new MyCertificateValidation();
                        if ((Boolean)row["bitAccessExchange"] == false)
                        {
                            esb.Url = (string)row["vcExchPath"] + "/EWS/exchange.asmx";
                            UserName = (string)row["vcEmailID"];
                            esb.Credentials = new NetworkCredential(UserName.Split('@')[0], Password, (string)row["vcExchDomain"]);
                        }
                        else
                        {

                            esb.Url = (string)row["vcExchPath"] + "/EWS/exchange.asmx";
                            UserName = (string)row["vcExchUserName"];
                           
                            esb.Credentials = new NetworkCredential(UserName.Split('@')[0], Password, (string)row["vcExchDomain"]);
                            ExchangeImpersonationType type = new ExchangeImpersonationType();
                            ConnectingSIDType type2 = new ConnectingSIDType();
                            if (text3 != "")
                            {
                                type2.PrimarySmtpAddress = text3;
                            }
                            type.ConnectingSID = type2;
                            esb.ExchangeImpersonation = type;

                        }


                        GetItemType type3 = new GetItemType();
                        type3.ItemIds = new ItemIdType[] { ItemId };
                        type3.ItemShape = new ItemResponseShapeType();
                        type3.ItemShape.BaseShape = DefaultShapeNamesType.AllProperties;


                        GetItemResponseType item = esb.GetItem(type3);

                        foreach (ItemInfoResponseMessageType type5 in item.ResponseMessages.Items)
                        {
                            ItemType type6 = type5.Items.Items[0];
                            if (type6.ItemClass == "IPM.Appointment")
                            {
                                this.GetCalItem(type6, eventName, esb,(Int32)row["ResourceId"]);
                            }
                            else if (eventName != "Created" && type6.ItemClass == "IPM.Note")
                            {
                                this.GetEmailMessage(type6, esb, numDomainId, numUserCntId);
                            }
                        }

                    }


                }
            }

            //ExchangeServiceBinding esb = new ExchangeServiceBinding();
            //esb.Url = ConfigurationManager.AppSettings["ExchUrl"] + "/EWS/exchange.asmx";
            //esb.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["ExcUserName"], ConfigurationManager.AppSettings["ExchPass"], ConfigurationManager.AppSettings["DomainServerName"]);
            ////esb.Credentials = new NetworkCredential("administrator", "Biz123456", "BizDemo");
            //ExchangeImpersonationType type = new ExchangeImpersonationType();
            //ConnectingSIDType type2 = new ConnectingSIDType();
            //if (text3 != "")
            //{
            //    type2.PrimarySmtpAddress = text3;
            //}
            //type.ConnectingSID = type2;
            //esb.ExchangeImpersonation = type;



        }
        catch (Exception exception)
        {
            throw exception;
        }
        finally
        {
            selectConnection.Close();
        }
    }





    public void GetEvent(FolderIdType ItemId, string subscriptionId)
    {
        try
        {
            string connectionString = ConfigurationManager.AppSettings["ConnectionString"];
            SqlConnection selectConnection = new SqlConnection(connectionString);
            string text3 = "";
            SqlDataAdapter adapter = new SqlDataAdapter("select vcEmailId,numDomainId from usermaster where SubscriptionId = '" + subscriptionId + "'", selectConnection);
            DataTable dataTable = new DataTable();
            adapter.Fill(dataTable);
            if (dataTable.Rows.Count == 1)
            {
                foreach (DataRow row in dataTable.Rows)
                {
                    text3 = (string)row["vcEmailid"];

                }
            }
            ExchangeServiceBinding binding = new ExchangeServiceBinding();
            //binding.Url = "http://192.168.1.120/EWS/exchange.asmx";
            binding.Url = ConfigurationManager.AppSettings["ExchUrl"] + "/EWS/exchange.asmx";
            binding.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["ExcUserName"], ConfigurationManager.AppSettings["ExchPass"], ConfigurationManager.AppSettings["DomainServerName"]);
            //binding.Credentials = new NetworkCredential("administrator", "Biz123456", "BizDemo");
            ExchangeImpersonationType type = new ExchangeImpersonationType();
            ConnectingSIDType type2 = new ConnectingSIDType();
            if (text3 != "")
            {
                type2.PrimarySmtpAddress = text3;
            }
            type.ConnectingSID = type2;
            binding.ExchangeImpersonation = type;
            GetFolderType type3 = new GetFolderType();
            type3.FolderIds = new FolderIdType[] { ItemId };
            type3.FolderShape = new FolderResponseShapeType();
            type3.FolderShape.BaseShape = DefaultShapeNamesType.AllProperties;
            GetFolderResponseType folder = binding.GetFolder(type3);
            foreach (FolderInfoResponseMessageType type5 in folder.ResponseMessages.Items)
            {
            }
        }
        catch (Exception exception)
        {
            throw exception;
        }
    }





    private void GetEmailMessage(ItemType item2, ExchangeServiceBinding esb, decimal numDomainId, decimal numUserCntId)
    {
        string connectionString = ConfigurationManager.AppSettings["ConnectionString"];
        SqlConnection connection = new SqlConnection(connectionString);
        try
        {
            int num;

            MessageType type = (MessageType)item2;
            string text2 = "";
            string text3 = "";
            string name = "";
            string emailAddress = "";
            string text6 = "";
            string text7 = "";
            for (num = 0; num <= (type.ToRecipients.Length - 1); num++)
            {
                if (num != (type.ToRecipients.Length - 1))
                {
                    text3 = text3 + type.ToRecipients[num].EmailAddress + ",";
                }
                else
                {
                    text3 = text3 + type.ToRecipients[num].EmailAddress;
                }
                if (num != (type.ToRecipients.Length - 1))
                {
                    text2 = text2 + type.ToRecipients[num].Name + ",";
                }
                else
                {
                    text2 = text2 + type.ToRecipients[num].Name;
                }
            }
            if (type.CcRecipients != null)
            {
                for (num = 0; num <= (type.CcRecipients.Length - 1); num++)
                {
                    if (num != (type.CcRecipients.Length - 1))
                    {
                        text7 = text7 + type.CcRecipients[num].EmailAddress + ",";
                    }
                    else
                    {
                        text7 = text7 + type.CcRecipients[num].EmailAddress;
                    }
                    if (num != (type.CcRecipients.Length - 1))
                    {
                        text6 = text6 + type.CcRecipients[num].Name + ",";
                    }
                    else
                    {
                        text6 = text6 + type.CcRecipients[num].Name;
                    }
                }
            }
            emailAddress = type.From.Item.EmailAddress;
            name = type.From.Item.Name;
            string text8 = "";
            string text9 = "";
            string text10 = "";
            string text11 = "";
            if ((type.HasAttachments && (type.Attachments != null)) && (type.Attachments.Length > 0))
            {
                List<RequestAttachmentIdType> list = new List<RequestAttachmentIdType>();
                for (int i = 0; i < type.Attachments.Length; i++)
                {
                    FileAttachmentType type2 = type.Attachments[i] as FileAttachmentType;
                    if (type2 != null)
                    {
                        RequestAttachmentIdType item = new RequestAttachmentIdType();
                        item.Id = type2.AttachmentId.Id;
                        text9 = text9 + type2.ContentType + ",";
                        text11 = text11 + item.Id + ",";
                        list.Add(item);
                    }
                }
                GetAttachmentType type4 = new GetAttachmentType();
                type4.AttachmentShape = new AttachmentResponseShapeType();
                type4.AttachmentIds = list.ToArray();
                GetAttachmentResponseType attachment = esb.GetAttachment(type4);
                foreach (AttachmentInfoResponseMessageType type6 in attachment.ResponseMessages.Items)
                {
                    if (type6.ResponseCode == ResponseCodeType.NoError)
                    {
                        text10 = text10 + type6.Attachments[0].Name + ",";
                    }
                }
            }
            CCommon objCommon = new CCommon();
            SqlCommand command = new SqlCommand("USP_InsertIntoEmailHistory", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add("@vcMessageTo", SqlDbType.VarChar).Value = text3;
            command.Parameters.Add("@vcMessageFrom", SqlDbType.VarChar).Value = emailAddress;
            command.Parameters.Add("@vcMessageFromName", SqlDbType.VarChar).Value = name;
            command.Parameters.Add("@vcSubject", SqlDbType.VarChar).Value = type.Subject;
            command.Parameters.Add("@vcBody", SqlDbType.Text).Value = type.Body.Value;
            command.Parameters.Add("@vcBodyText", SqlDbType.Text).Value = objCommon.StripTags(type.Body.Value, true);
            command.Parameters.Add("@vcCC", SqlDbType.VarChar).Value = text7;
            command.Parameters.Add("@vcBCC", SqlDbType.VarChar).Value = "";
            command.Parameters.Add("@bitHasAttachment", SqlDbType.Bit).Value = type.HasAttachments;
            command.Parameters.Add("@vcItemId", SqlDbType.VarChar).Value = type.ItemId.Id;
            command.Parameters.Add("@vcChangeKey", SqlDbType.VarChar).Value = type.ItemId.ChangeKey;
            command.Parameters.Add("@bitIsRead", SqlDbType.Bit).Value = type.IsRead;
            command.Parameters.Add("@vcSize", SqlDbType.Int).Value = type.Size;
            command.Parameters.Add("@chrSource", SqlDbType.Char).Value = "O";
            command.Parameters.Add("@tinttype", SqlDbType.Int).Value = 1;
            command.Parameters.Add("@vcAttachmentType", SqlDbType.VarChar).Value = text9;
            command.Parameters.Add("@vcFileName", SqlDbType.VarChar).Value = text10;
            command.Parameters.Add("@vcAttachmentItemId", SqlDbType.VarChar).Value = text11;
            command.Parameters.Add("@dtReceived", SqlDbType.DateTime).Value = type.DateTimeReceived;
            command.Parameters.Add("@numDomainId", SqlDbType.BigInt).Value = numDomainId;
            command.Parameters.Add("@numUserCntId", SqlDbType.BigInt).Value = numUserCntId;
            if (type.Categories != null)
            {
                for (int j = 0; j <= (type.Categories.Length - 1); j++)
                {
                    text8 = text8 + type.Categories[j] + ";";
                }
                command.Parameters.Add("@vcCategory", SqlDbType.VarChar).Value = text8;
            }
            else
            {
                command.Parameters.Add("@vcCategory", SqlDbType.VarChar).Value = "";
            }
            connection.Open();
            int num4 = command.ExecuteNonQuery();
            connection.Close();
        }
        catch (Exception exception)
        {
            throw exception;
        }
        finally
        {
            connection.Close();
        }
    }





    private void GetCalItem(ItemType item2, string eventName, ExchangeServiceBinding esb, Int32 ResourceId)
    {
        if (item2.ItemClass == "IPM.Appointment")
        {
            string connectionString = ConfigurationManager.AppSettings["ConnectionString"];
            SqlConnection connection = new SqlConnection(connectionString);
            Guid empty = Guid.Empty;
            try
            {
                Exception exception;
                try
                {
                    DateTime time;
                    int interval;
                    int num4;
                    string text4;
                    string text5;
                    short num6;
                    string text6;
                    int num8;
                    SqlCommand command3;
                    int num = -999;
                    CalendarItemType type = (CalendarItemType)item2;
                    SqlCommand selectCommand = new SqlCommand("GetVarianceKeyByItemId", connection);
                    selectCommand.CommandType = CommandType.StoredProcedure;
                    selectCommand.Parameters.Add("@ItemId", SqlDbType.VarChar, 250).Value = Convert.ToString(type.ItemId.Id);
                    SqlDataAdapter adapter = new SqlDataAdapter(selectCommand);
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);
                    if (dataTable.Rows.Count == 1)
                    {
                        foreach (DataRow row in dataTable.Rows)
                        {
                            if (row["varianceId"] != DBNull.Value)
                            {
                                empty = new Guid(Convert.ToString(row["VarianceID"]));
                            }
                            else
                            {
                                empty = Guid.NewGuid();
                            }
                            num = Convert.ToInt32(row["RecurrenceId"]);
                        }
                    }
                    if ((eventName != "CreatedEvent") && (eventName != "ModifiedEvent"))
                    {
                        goto Label_1404;
                    }
                    SqlCommand command2 = new SqlCommand("Recurrence_Add", connection);
                    command2.CommandType = CommandType.StoredProcedure;
                    if (type.Recurrence == null)
                    {
                        goto Label_110A;
                    }
                    RecurrencePatternBaseType item = type.Recurrence.Item;
                    RecurrenceRangeBaseType type3 = type.Recurrence.Item1;
                    string text2 = item.ToString();
                    switch (type3.ToString())
                    {
                        case "PushNotificationClient.WebReference1.NoEndRecurrenceRangeType":
                            time = new DateTime(0x270f, 12, 0x1f, 12, 0x3b, 0x3b, 0);
                            command2.Parameters.Add("@EndDateUtc", SqlDbType.DateTime).Value = time;
                            break;

                        case "PushNotificationClient.WebReference1.NumberedRecurrenceRangeType":
                            {
                                int numberOfOccurrences = ((NumberedRecurrenceRangeType)type3).NumberOfOccurrences;
                                if (type.LastOccurrence != null)
                                {
                                    command2.Parameters.Add("@EndDateUtc", SqlDbType.DateTime).Value = type.LastOccurrence.End.ToUniversalTime();
                                }
                                else
                                {
                                    command2.Parameters.Add("@EndDateUtc", SqlDbType.DateTime).Value = new DateTime(0x270f, 12, 0x1f, 12, 0x3b, 0x3b, 0);
                                }
                                break;
                            }
                        case "PushNotificationClient.WebReference1.EndDateRecurrenceRangeType":
                            time = ((EndDateRecurrenceRangeType)type3).EndDate.ToUniversalTime();
                            command2.Parameters.Add("@EndDateUtc", SqlDbType.DateTime).Value = time;
                            break;
                    }
                    switch (text2)
                    {
                        case "PushNotificationClient.WebReference1.DailyRecurrencePatternType":
                            interval = ((DailyRecurrencePatternType)item).Interval;
                            command2.Parameters.Add("@Period", SqlDbType.VarChar).Value = "D";
                            command2.Parameters.Add("@DayOfWeekMaskUtc", SqlDbType.Int).Value = 0;
                            command2.Parameters.Add("@PeriodMultiple", SqlDbType.Int).Value = interval;
                            command2.Parameters.Add("@DayofMonth", SqlDbType.Int).Value = 0;
                            command2.Parameters.Add("@MonthOfYear", SqlDbType.Int).Value = 0;
                            command2.Parameters.Add("@UtcOffset", SqlDbType.Int).Value = 0;
                            goto Label_10C5;

                        case "PushNotificationClient.WebReference1.WeeklyRecurrencePatternType":
                            {
                                string[] textArray = ((WeeklyRecurrencePatternType)item).DaysOfWeek.Split(new char[0]);
                                interval = ((WeeklyRecurrencePatternType)item).Interval;
                                num4 = 0;
                                for (int i = 0; i <= (textArray.Length - 1); i++)
                                {
                                    if (textArray[i] == "Sunday")
                                    {
                                        num4 += 1;
                                    }
                                    else if (textArray[i] == "Monday")
                                    {
                                        num4 += 2;
                                    }
                                    else if (textArray[i] == "Tuesday")
                                    {
                                        num4 += 4;
                                    }
                                    else if (textArray[i] == "Wednesday")
                                    {
                                        num4 += 8;
                                    }
                                    else if (textArray[i] == "Thursday")
                                    {
                                        num4 += 16;
                                    }
                                    else if (textArray[i] == "Friday")
                                    {
                                        num4 += 32;
                                    }
                                    else if (textArray[i] == "Saturday")
                                    {
                                        num4 += 64;
                                    }
                                }
                                command2.Parameters.Add("@Period", SqlDbType.VarChar).Value = "W";
                                command2.Parameters.Add("@DayOfWeekMaskUtc", SqlDbType.Int).Value = num4;
                                command2.Parameters.Add("@PeriodMultiple", SqlDbType.Int).Value = interval;
                                command2.Parameters.Add("@DayofMonth", SqlDbType.Int).Value = 0;
                                command2.Parameters.Add("@MonthOfYear", SqlDbType.Int).Value = 0;
                                command2.Parameters.Add("@UtcOffset", SqlDbType.Int).Value = 0;
                                goto Label_10C5;
                            }
                        case "PushNotificationClient.WebReference1.RelativeMonthlyRecurrencePatternType":
                            text4 = Convert.ToString(((RelativeMonthlyRecurrencePatternType)item).DayOfWeekIndex);
                            text5 = Convert.ToString(((RelativeMonthlyRecurrencePatternType)item).DaysOfWeek);
                            interval = ((RelativeMonthlyRecurrencePatternType)item).Interval;
                            num6 = 0;
                            num4 = 0;
                            switch (text4)
                            {
                                case "First":
                                    num6 = -1;
                                    break;

                                case "Second":
                                    num6 = -2;
                                    break;

                                case "Third":
                                    num6 = -3;
                                    break;

                                case "Fourth":
                                    num6 = -4;
                                    break;

                                case "Fifth":
                                    num6 = -5;
                                    break;
                            }
                            if (text5 == "Day")
                            {
                                num4 += 127;
                            }
                            else if (text5 == "Weekday")
                            {
                                num4 += 62;
                            }
                            else if (text5 == "WeekendDay")
                            {
                                num4 += 65;
                            }
                            else if (text5 == "Sunday")
                            {
                                num4 += 1;
                            }
                            else if (text5 == "Monday")
                            {
                                num4 += 2;
                            }
                            else if (text5 == "Tuesday")
                            {
                                num4 += 4;
                            }
                            else if (text5 == "Wednesday")
                            {
                                num4 += 8;
                            }
                            else if (text5 == "Thursday")
                            {
                                num4 += 16;
                            }
                            else if (text5 == "Friday")
                            {
                                num4 += 32;
                            }
                            else if (text5 == "Saturday")
                            {
                                num4 += 64;
                            }
                            command2.Parameters.Add("@Period", SqlDbType.VarChar).Value = "M";
                            command2.Parameters.Add("@DayOfWeekMaskUtc", SqlDbType.Int).Value = num4;
                            command2.Parameters.Add("@PeriodMultiple", SqlDbType.Int).Value = interval;
                            command2.Parameters.Add("@DayofMonth", SqlDbType.Int).Value = num6;
                            command2.Parameters.Add("@MonthOfYear", SqlDbType.Int).Value = 0;
                            command2.Parameters.Add("@UtcOffset", SqlDbType.Int).Value = 0;
                            goto Label_10C5;

                        case "PushNotificationClient.WebReference1.AbsoluteMonthlyRecurrencePatternType":
                            {
                                int dayOfMonth = ((AbsoluteMonthlyRecurrencePatternType)item).DayOfMonth;
                                interval = ((AbsoluteMonthlyRecurrencePatternType)item).Interval;
                                command2.Parameters.Add("@Period", SqlDbType.VarChar).Value = "M";
                                command2.Parameters.Add("@DayOfWeekMaskUtc", SqlDbType.Int).Value = 0;
                                command2.Parameters.Add("@PeriodMultiple", SqlDbType.Int).Value = interval;
                                command2.Parameters.Add("@DayofMonth", SqlDbType.Int).Value = dayOfMonth;
                                command2.Parameters.Add("@MonthOfYear", SqlDbType.Int).Value = 0;
                                command2.Parameters.Add("@UtcOffset", SqlDbType.Int).Value = 0x4d58;
                                goto Label_10C5;
                            }
                        case "PushNotificationClient.WebReference1.RelativeYearlyRecurrencePatternType":
                            text4 = Convert.ToString(((RelativeYearlyRecurrencePatternType)item).DayOfWeekIndex);
                            text5 = Convert.ToString(((RelativeYearlyRecurrencePatternType)item).DaysOfWeek);
                            text6 = Convert.ToString(((RelativeYearlyRecurrencePatternType)item).Month);
                            num6 = 0;
                            num4 = 0;
                            num8 = 0;
                            switch (text6)
                            {
                                case "January":
                                    num8 = 1;
                                    break;

                                case "Feburary":
                                    num8 = 2;
                                    break;

                                case "March":
                                    num8 = 3;
                                    break;

                                case "April":
                                    num8 = 4;
                                    break;

                                case "May":
                                    num8 = 5;
                                    break;

                                case "June":
                                    num8 = 6;
                                    break;

                                case "July":
                                    num8 = 7;
                                    break;

                                case "August":
                                    num8 = 8;
                                    break;

                                case "September":
                                    num8 = 9;
                                    break;

                                case "October":
                                    num8 = 10;
                                    break;

                                case "November":
                                    num8 = 11;
                                    break;

                                case "December":
                                    num8 = 12;
                                    break;
                            }
                            if (text4 == "First")
                            {
                                num6 = -1;
                            }
                            else if (text4 == "Second")
                            {
                                num6 = -2;
                            }
                            else if (text4 == "Third")
                            {
                                num6 = -3;
                            }
                            else if (text4 == "Fourth")
                            {
                                num6 = -4;
                            }
                            else if (text4 == "Fifth")
                            {
                                num6 = -5;
                            }
                            if (text5 == "Day")
                            {
                                num4 += 0x7f;
                            }
                            else if (text5 == "Weekday")
                            {
                                num4 += 62;
                            }
                            else if (text5 == "WeekendDay")
                            {
                                num4 += 65;
                            }
                            else if (text5 == "Sunday")
                            {
                                num4++;
                            }
                            else if (text5 == "Monday")
                            {
                                num4 += 2;
                            }
                            else if (text5 == "Tuesday")
                            {
                                num4 += 4;
                            }
                            else if (text5 == "Wednesday")
                            {
                                num4 += 8;
                            }
                            else if (text5 == "Thursday")
                            {
                                num4 += 16;
                            }
                            else if (text5 == "Friday")
                            {
                                num4 += 32;
                            }
                            else if (text5 == "Saturday")
                            {
                                num4 += 64;
                            }
                            command2.Parameters.Add("@Period", SqlDbType.VarChar).Value = "Y";
                            command2.Parameters.Add("@DayOfWeekMaskUtc", SqlDbType.Int).Value = num4;
                            command2.Parameters.Add("@PeriodMultiple", SqlDbType.Int).Value = 1;
                            command2.Parameters.Add("@DayofMonth", SqlDbType.Int).Value = num6;
                            command2.Parameters.Add("@MonthOfYear", SqlDbType.Int).Value = num8;
                            command2.Parameters.Add("@UtcOffset", SqlDbType.Int).Value = 0;
                            goto Label_10C5;
                    }
                    if (text2 == "PushNotificationClient.WebReference1.AbsoluteYearlyRecurrencePatternType")
                    {
                        int num9 = ((AbsoluteYearlyRecurrencePatternType)item).DayOfMonth;
                        text6 = Convert.ToString(((AbsoluteYearlyRecurrencePatternType)item).Month);
                        num8 = 0;
                        switch (text6)
                        {
                            case "January":
                                num8 = 1;
                                break;

                            case "Feburary":
                                num8 = 2;
                                break;

                            case "March":
                                num8 = 3;
                                break;

                            case "April":
                                num8 = 4;
                                break;

                            case "May":
                                num8 = 5;
                                break;

                            case "June":
                                num8 = 6;
                                break;

                            case "July":
                                num8 = 7;
                                break;

                            case "August":
                                num8 = 8;
                                break;

                            case "September":
                                num8 = 9;
                                break;

                            case "October":
                                num8 = 10;
                                break;

                            case "November":
                                num8 = 11;
                                break;

                            case "December":
                                num8 = 12;
                                break;
                        }
                        command2.Parameters.Add("@Period", SqlDbType.VarChar).Value = "Y";
                        command2.Parameters.Add("@DayOfWeekMaskUtc", SqlDbType.Int).Value = 0;
                        command2.Parameters.Add("@PeriodMultiple", SqlDbType.Int).Value = 1;
                        command2.Parameters.Add("@DayofMonth", SqlDbType.Int).Value = num9;
                        command2.Parameters.Add("@MonthOfYear", SqlDbType.Int).Value = num8;
                        command2.Parameters.Add("@UtcOffset", SqlDbType.Int).Value = 0x4d58;
                    }
                Label_10C5:
                    command2.Parameters.Add("@EditType", SqlDbType.Int).Value = 0;
                    try
                    {
                        connection.Open();
                        num = Convert.ToInt32(command2.ExecuteScalar());
                        connection.Close();
                    }
                    catch (Exception exception1)
                    {
                        exception = exception1;
                        throw exception;
                    }
                Label_110A:
                    command3 = new SqlCommand("[Activity_AddByItemId]", connection);
                    command3.CommandType = CommandType.StoredProcedure;
                    command3.Parameters.Add("@AllDayEvent", SqlDbType.Bit).Value = type.IsAllDayEvent;
                    command3.Parameters.Add("@ActivityDescription", SqlDbType.Text).Value = type.Body.Value;
                    command3.Parameters.Add("@Location", SqlDbType.VarChar, 200).Value = type.Location;
                    command3.Parameters.Add("@StartDateTimeUtc", SqlDbType.DateTime).Value = type.Start;
                    command3.Parameters.Add("@Subject", SqlDbType.VarChar, 200).Value = type.Subject;
                    command3.Parameters.Add("@EnableReminder", SqlDbType.Bit).Value = type.ReminderIsSet;
                    command3.Parameters.Add("@ReminderInterval", SqlDbType.Int).Value = Convert.ToInt32(type.ReminderMinutesBeforeStart) * 60;
                    TimeSpan span = new TimeSpan(type.End.Ticks - type.Start.Ticks);
                    command3.Parameters.Add("@Duration", SqlDbType.Int).Value = span.TotalSeconds;
                    switch (type.LegacyFreeBusyStatus)
                    {
                        case LegacyFreeBusyType.Free:
                            command3.Parameters.Add("@ShowTimeAs", SqlDbType.Int).Value = 0;
                            break;

                        case LegacyFreeBusyType.Tentative:
                            command3.Parameters.Add("@ShowTimeAs", SqlDbType.Int).Value = 1;
                            break;

                        case LegacyFreeBusyType.Busy:
                            command3.Parameters.Add("@ShowTimeAs", SqlDbType.Int).Value = 3;
                            break;

                        case LegacyFreeBusyType.OOF:
                            command3.Parameters.Add("@ShowTimeAs", SqlDbType.Int).Value = 2;
                            break;
                    }
                    command3.Parameters.Add("@Importance", SqlDbType.Int).Value = type.Importance;
                    command3.Parameters.Add("@Status", SqlDbType.Int).Value = 0;
                    command3.Parameters.Add("@RecurrenceKey", SqlDbType.Int).Value = num;
                    command3.Parameters.Add("@ResourceName", SqlDbType.Int).Value = ResourceId;
                    command3.Parameters.Add("@ItemId", SqlDbType.VarChar).Value = type.ItemId.Id;
                    command3.Parameters.Add("@ChangeKey", SqlDbType.VarChar).Value = type.ItemId.ChangeKey;
                    connection.Open();
                    int num10 = command3.ExecuteNonQuery();
                    connection.Close();
                Label_1404:
                    if ((eventName == "ModifiedEvent") && (type.ModifiedOccurrences != null))
                    {
                        for (int j = 0; j < type.ModifiedOccurrences.Length; j++)
                        {
                            OccurrenceInfoType type4 = type.ModifiedOccurrences[j];
                            DateTime time2 = type4.OriginalStart.ToUniversalTime();
                            DateTime time3 = type4.Start.ToUniversalTime();
                            DateTime time4 = type4.End.ToUniversalTime();
                            ItemIdType itemId = new ItemIdType();
                            itemId = type4.ItemId;
                            GetItemType type6 = new GetItemType();
                            type6.ItemIds = new ItemIdType[] { itemId };
                            type6.ItemShape = new ItemResponseShapeType();
                            type6.ItemShape.BaseShape = DefaultShapeNamesType.AllProperties;
                            GetItemResponseType type7 = esb.GetItem(type6);
                            foreach (ItemInfoResponseMessageType type8 in type7.ResponseMessages.Items)
                            {
                                ItemType type9 = type8.Items.Items[0];
                                CalendarItemType type10 = (CalendarItemType)type9;
                                SqlCommand command4 = new SqlCommand("[Variance_AddExch]", connection);
                                command4.CommandType = CommandType.StoredProcedure;
                                command4.Parameters.Add("@AllDayEvent", SqlDbType.Bit).Value = type10.IsAllDayEvent;
                                command4.Parameters.Add("@ActivityDescription", SqlDbType.Text).Value = type10.Body.Value;
                                command4.Parameters.Add("@Location", SqlDbType.VarChar, 200).Value = type10.Location;
                                command4.Parameters.Add("@StartDateTimeUtc", SqlDbType.DateTime).Value = type10.Start.ToUniversalTime();
                                command4.Parameters.Add("@Subject", SqlDbType.VarChar, 200).Value = type10.Subject;
                                command4.Parameters.Add("@EnableReminder", SqlDbType.Bit).Value = type10.ReminderIsSet;
                                command4.Parameters.Add("@ReminderInterval", SqlDbType.Int).Value = Convert.ToInt32(type10.ReminderMinutesBeforeStart) * 60;
                                span = new TimeSpan(type10.End.Ticks - type10.Start.Ticks);
                                command4.Parameters.Add("@Duration", SqlDbType.Int).Value = span.TotalSeconds;
                                switch (type10.LegacyFreeBusyStatus)
                                {
                                    case LegacyFreeBusyType.Free:
                                        command4.Parameters.Add("@ShowTimeAs", SqlDbType.Int).Value = 0;
                                        break;

                                    case LegacyFreeBusyType.Tentative:
                                        command4.Parameters.Add("@ShowTimeAs", SqlDbType.Int).Value = 1;
                                        break;

                                    case LegacyFreeBusyType.Busy:
                                        command4.Parameters.Add("@ShowTimeAs", SqlDbType.Int).Value = 3;
                                        break;

                                    case LegacyFreeBusyType.OOF:
                                        command4.Parameters.Add("@ShowTimeAs", SqlDbType.Int).Value = 2;
                                        break;
                                }
                                command4.Parameters.Add("@Importance", SqlDbType.Int).Value = type9.Importance;
                                command4.Parameters.Add("@Status", SqlDbType.Int).Value = 0;
                                command4.Parameters.Add("@RecurrenceKey", SqlDbType.Int).Value = num;
                                command4.Parameters.Add("@ResourceName", SqlDbType.Int).Value = ResourceId;
                                command4.Parameters.Add("@ItemId", SqlDbType.VarChar).Value = type.ItemId.Id;
                                command4.Parameters.Add("@ChangeKey", SqlDbType.VarChar).Value = type10.ItemId.ChangeKey;
                                command4.Parameters.Add("@OriginalStartDateTimeUtc", SqlDbType.DateTime).Value = type10.OriginalStart.ToUniversalTime();
                                command4.Parameters.Add("@ItemIdOccur", SqlDbType.VarChar).Value = type10.ItemId.Id;
                                if (empty == Guid.Empty)
                                {
                                    empty = Guid.NewGuid();
                                }
                                command4.Parameters.Add("@VarienceKey", SqlDbType.VarChar).Value = Convert.ToString(empty);
                                connection.Open();
                                num10 = command4.ExecuteNonQuery();
                                connection.Close();
                            }
                        }
                    }
                    if ((eventName == "ModifiedEvent") && (type.DeletedOccurrences != null))
                    {
                        for (int k = 0; k < type.DeletedOccurrences.Length; k++)
                        {
                            SqlCommand command5 = new SqlCommand("[Variance_DelOccExch]", connection);
                            command5.CommandType = CommandType.StoredProcedure;
                            DeletedOccurrenceInfoType type11 = type.DeletedOccurrences[k];
                            command5.Parameters.Add("@AllDayEvent", SqlDbType.Bit).Value = 0;
                            command5.Parameters.Add("@ActivityDescription", SqlDbType.Text).Value = " ";
                            command5.Parameters.Add("@Location", SqlDbType.VarChar, 200).Value = " ";
                            command5.Parameters.Add("@StartDateTimeUtc", SqlDbType.DateTime).Value = type11.Start;
                            command5.Parameters.Add("@Subject", SqlDbType.VarChar, 200).Value = " ";
                            command5.Parameters.Add("@EnableReminder", SqlDbType.Bit).Value = 0;
                            command5.Parameters.Add("@ReminderInterval", SqlDbType.Int).Value = 60;
                            span = new TimeSpan((long)5);
                            command5.Parameters.Add("@Duration", SqlDbType.Int).Value = span.TotalSeconds;
                            command5.Parameters.Add("@ShowTimeAs", SqlDbType.Int).Value = 0;
                            command5.Parameters.Add("@Importance", SqlDbType.Int).Value = 0;
                            command5.Parameters.Add("@Status", SqlDbType.Int).Value = -1;
                            command5.Parameters.Add("@RecurrenceKey", SqlDbType.Int).Value = num;
                            command5.Parameters.Add("@ResourceName", SqlDbType.Int).Value = ResourceId;
                            command5.Parameters.Add("@ItemId", SqlDbType.VarChar).Value = type.ItemId.Id;
                            command5.Parameters.Add("@ChangeKey", SqlDbType.VarChar).Value = type.ItemId.ChangeKey;
                            command5.Parameters.Add("@OriginalStartDateTimeUtc", SqlDbType.DateTime).Value = type11.Start;
                            if (empty == Guid.Empty)
                            {
                                empty = Guid.NewGuid();
                            }
                            command5.Parameters.Add("@VarienceKey", SqlDbType.VarChar).Value = Convert.ToString(empty);
                            connection.Open();
                            num10 = command5.ExecuteNonQuery();
                            connection.Close();
                        }
                    }
                }
                catch (Exception exception2)
                {
                    exception = exception2;
                    throw exception;
                }
            }
            finally
            {
                connection.Close();
            }
        }
    }


    public void DeleteMails(ItemIdType ItemId, string subscriptionId)
    {
        string connectionString = ConfigurationManager.AppSettings["ConnectionString"];
        SqlConnection connection = new SqlConnection(connectionString);
        Exception exception;
        try
        {

            SqlCommand command = new SqlCommand("USP_DeleteEmailHistory", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add("@vcItemId", SqlDbType.VarChar).Value = ItemId.Id;
            SqlCommand command2 = new SqlCommand("ActivityDelByItemId", connection);
            command2.CommandType = CommandType.StoredProcedure;
            command2.Parameters.Add("@ItemId", SqlDbType.VarChar).Value = ItemId.Id;
            connection.Open();
            int num = command.ExecuteNonQuery();
            command2.ExecuteNonQuery();
            connection.Close();
        }
        catch (Exception exception2)
        {
            exception = exception2;
            throw exception;
        }
        finally
        {
            connection.Close();
        }

    }
   
    public class MyCertificateValidation : ICertificatePolicy
    {
        public static bool DefaultValidate = false;

         bool ICertificatePolicy.CheckValidationResult(ServicePoint srvPoint, System.Security.Cryptography.X509Certificates.X509Certificate cert, WebRequest request, int problem)
        {

            return true;

        }
    }
}
