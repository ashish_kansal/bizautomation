//
// Copyright (C) 2007 Microsoft Corporation. All rights reserved.\
// Push Notification Client Web Service
//

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;
using PushNotificationClient.WebReference1;
namespace PushNotificationClient
{
    [WebService(Namespace = "http://tempuri.org/")]
    public class PushNotification : System.Web.Services.WebService, INotificationServiceBinding
    {
        public PushNotification()
        { }

        #region INotificationServiceBinding Members

        public SendNotificationResultType SendNotification(SendNotificationResponseType SendNotification1)
        { 
            // Create the response to the push notification.
            SendNotificationResultType result = new SendNotificationResultType();

            // Get the push notifications.
            ResponseMessageType[] rmta = SendNotification1.ResponseMessages.Items;

            foreach (ResponseMessageType rmt in rmta)
            {
                if (rmt.ResponseCode != ResponseCodeType.NoError)
                {
                    // Discontinue the subscription if there is an error.
                    result.SubscriptionStatus = SubscriptionStatusType.Unsubscribe;
                    return result;
                }

                // Cast to the correct response message type.
                SendNotificationResponseMessageType snrmt = rmt as SendNotificationResponseMessageType;

                // Get the notification.
                NotificationType notification = snrmt.Notification;

                // Get the subscription identifier.
                string subscriptionId = notification.SubscriptionId;

                // Get the previous watermark for the subscription.
                string previousWatermark = notification.PreviousWatermark;

                int i = 0;
                bool continueSubscription = true;

                // Cast the notification event to the proper type
                foreach (BaseNotificationEventType bnet in notification.Items)
                {
                    // Get the event name
                    string eventName = notification.ItemsElementName[i].ToString();

                    // Access information about copy or move events.
                    if (bnet is MovedCopiedEventType)
                    {
                        AccessMovedCopiedEvent(bnet as MovedCopiedEventType, eventName as String, subscriptionId as String);
                    }

                    // Access information about create, delete, or new mail events.
                    else if (bnet is BaseObjectChangedEventType)
                    {
                        AccessCreateDeleteNewMailEvent(bnet as BaseObjectChangedEventType, eventName as String, subscriptionId as String);
                    }

                    // Access information about modified events.
                    else if (bnet is ModifiedEventType)
                    {
                        AccessModifiedEvent(bnet as ModifiedEventType);
                    }

                    // No events have occurred. 
                    else
                    {
                        string statusEventWatermark = bnet.Watermark;

                        // Cancel the subscription.
                        continueSubscription = false;
                    }

                    i++;
                }
                // If there are more events ready to be sent, then continue the subscription.
                if (notification.MoreEvents)
                {
                    result.SubscriptionStatus = SubscriptionStatusType.OK;
                    return result;
                }

                // Continue the subscription if an event occurred.
                if (continueSubscription)
                {
                    result.SubscriptionStatus = SubscriptionStatusType.OK;
                    return result;
                }
            }

            result.SubscriptionStatus = SubscriptionStatusType.OK;
            return result;
            // Discontinue the subscription if no events have occurred.
            //result.SubscriptionStatus = SubscriptionStatusType.Unsubscribe;
            //return result;
        }

        #endregion

        // Access event information for moved or copied events.
        public static void AccessMovedCopiedEvent(MovedCopiedEventType mcet, String eventName, String subscriptionId)
        {
            // Get the watermark for the event.
           
            // Get the timestamp for the event.
            DateTime timestamp = mcet.TimeStamp;

            // Get the old parent folder identifier.
            FolderIdType oldParentFolder = mcet.OldParentFolderId;

            // Get the new parent folder identifier.
            FolderIdType newParentFolder = mcet.ParentFolderId;

            // Get either the old folder identifier for a moved/copied folder
            // or the old item identifier for a moved/copid item.
            if (mcet.Item1 is ItemIdType)
            {
                // Get the old item identifier of the item.
                ItemIdType oldItemId = mcet.Item1 as ItemIdType;
                if (eventName == "MovedEvent" )
                {
                    ItemIdType itemId = mcet.Item1 as ItemIdType;
                    PushNotificationClient.WebReference1.ItemIdType itemId1 = new PushNotificationClient.WebReference1.ItemIdType();
                    itemId1.Id = itemId.Id;
                    clsExchService exch = new clsExchService();
                    exch.DeleteMails(itemId1 as PushNotificationClient.WebReference1.ItemIdType, subscriptionId as String);
                }
            }
            else
            {
                // Get the old folder identifier of the folder.
                FolderIdType oldFolderId = mcet.Item1 as FolderIdType;
            }

            // Get either the new folder identifier for a moved/copied folder
            // or the new item identifier fir a moved/copied item.
            if (mcet.Item is ItemIdType)
            {
                // Get the new item identifier of the item.
                ItemIdType newItemId = mcet.Item as ItemIdType;
            }
            else
            {
                // Get the new folder identifier of the folder.
                FolderIdType newFolderId = mcet.Item as FolderIdType;
            }
        }

        // Access event information for create, delete, or new mail events.
        public static void AccessCreateDeleteNewMailEvent(BaseObjectChangedEventType bocet, String eventName, String subscriptionId)
        {
            // Get the watermark for the event.
            string watermark = bocet.Watermark;

            // Get the timestamp for the event.
            DateTime timestamp = bocet.TimeStamp;

            // Get the parent folder identifier.
            FolderIdType parentFolderId = bocet.ParentFolderId;

            // Get either the folder or item identifier for a 
            // create/delete/new mail event.
            if (bocet.Item is ItemIdType)
            {
                // Get the item identifier.
                
               
                    ItemIdType itemId = bocet.Item as ItemIdType;
                    PushNotificationClient.WebReference1.ItemIdType itemId1 = new PushNotificationClient.WebReference1.ItemIdType();
                    itemId1.Id = itemId.Id;
                    clsExchService exch = new clsExchService();
                    exch.GetNewMails(itemId1 as PushNotificationClient.WebReference1.ItemIdType, subscriptionId as String, eventName as String);
                
            }
            else
            {
                // Get the folder identifier.
                FolderIdType folderId = bocet.Item as FolderIdType;
               
            }
        }

        // Access event information for modified events.
        public static void AccessModifiedEvent(ModifiedEventType met)
        {
            // Get the watermark for the event.
            string watermark = met.Watermark;

            // Get the timestamp for the event.
            DateTime timestamp = met.TimeStamp;

            // Get the parent folder identifier.
            FolderIdType parentFolderId = met.ParentFolderId;

            // Get either the folder or item identifier for the
            // modified folder or item.
            if (met.Item is FolderIdType)
            {
                // Check if the folder has an unread count.
                if (met.UnreadCountSpecified)
                {
                    int unreadCount = met.UnreadCount;
                }

                // Get the folder identifier.
                FolderIdType folderId = met.Item as FolderIdType;
            }
            else
            {
                // Get the item identifier.
                ItemIdType itemId = met.Item as ItemIdType;
            }
        }
    }
}