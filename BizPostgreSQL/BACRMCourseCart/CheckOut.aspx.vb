Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports System
Partial Public Class CheckOut
    Inherits System.Web.UI.Page
    Dim lngDivID, lngContId As Long
    Dim decShippingCharge As Decimal


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

       
            If Not IsPostBack Then
                If Session("UserContactID") Is Nothing Then
                    Session("redirect") = "True"
                    Response.Redirect("Login.aspx")
                Else
                    Session("redirect") = "False"
                End If
                lngDivID = Session("DivId")
                lngContId = Session("UserContactID")
                If Not IsPostBack Then
                    txtCardNumber.Attributes.Add("onkeypress", "return CheckNumber()")
                    Dim objItems As New CItems
                    If Session("OppID") Is Nothing Then
                        objItems.OppId = 0
                        objItems.DivisionID = lngDivID
                        objItems.ContactID = lngContId
                    Else
                        objItems.OppId = Session("OppID")
                    End If
                    Dim dtTable As DataTable
                    If Not Session("CourseData") Is Nothing Then
                        dtTable = Session("CourseData")
                        dgAddedCoourse.DataSource = dtTable
                        dgAddedCoourse.DataBind()
                        lblTotal.Text = String.Format("{0:#,##0.00}", dtTable.Compute("SUM(monWListPrice)", ""))
                        Session("TotalAmount") = lblTotal.Text
                    Else
                        Session("TotalAmount") = 0
                    End If
                    'CheckCreditStatus()

                    Dim Years As New ArrayList()
                    Dim i As Integer
                    For i = 1 To 20
                        Years.Add(Now.AddYears(i - 1).Date.ToString("yy"))
                    Next i
                    ddlCardExpYear.DataSource = Years
                    ddlCardExpYear.DataBind()
                    ddlCardExpYear.Items.FindByValue(Now.Year.ToString("yy"))

                    ddlCardExpMonth.SelectedIndex = (Now.Month - 1)


                    Dim objCommon As New CCommon
                    objCommon.sb_FillComboFromDBwithSel(ddlBillCountry, 40, Session("DomainID"))
                    objCommon.sb_FillComboFromDBwithSel(ddlShipCountry, 40, Session("DomainID"))


                    Dim objContacts As New CContacts
                    Dim dtTable1 As DataTable
                    objContacts.ContactID = Session("UserContactID")
                    objContacts.DomainID = Session("DomainID")
                    dtTable1 = objContacts.GetBillOrgorContAdd
                    txtCustomerFirstName.Text = IIf(IsDBNull(dtTable1.Rows(0).Item("vcFirstname")), "", dtTable1.Rows(0).Item("vcFirstname"))
                    txtCustomerLastName.Text = IIf(IsDBNull(dtTable1.Rows(0).Item("vcLastname")), "", dtTable1.Rows(0).Item("vcLastname"))
                    txtCustomerEmail.Text = IIf(IsDBNull(dtTable1.Rows(0).Item("vcEmail")), "", dtTable1.Rows(0).Item("vcEmail"))
                    txtCustomerPhone.Text = dtTable1.Rows(0).Item("vcPhone")
                    txtBillStreet.Text = IIf(IsDBNull(dtTable1.Rows(0).Item("vcBillstreet")), "", dtTable1.Rows(0).Item("vcBillstreet"))
                    txtShipStreet.Text = IIf(IsDBNull(dtTable1.Rows(0).Item("vcShipStreet")), "", dtTable1.Rows(0).Item("vcShipStreet"))
                    txtBillCity.Text = IIf(IsDBNull(dtTable1.Rows(0).Item("vcBillCity")), "", dtTable1.Rows(0).Item("vcBillCity"))
                    txtShipCity.Text = IIf(IsDBNull(dtTable1.Rows(0).Item("vcShipCity")), "", dtTable1.Rows(0).Item("vcShipCity"))
                    If Not IsDBNull(dtTable1.Rows(0).Item("vcBillCountry")) Then
                        If Not ddlBillCountry.Items.FindByValue(dtTable1.Rows(0).Item("vcBillCountry")) Is Nothing Then
                            ddlBillCountry.Items.FindByValue(dtTable1.Rows(0).Item("vcBillCountry")).Selected = True
                        End If
                    End If
                    If Not IsDBNull(dtTable1.Rows(0).Item("vcShipCountry")) Then
                        If Not ddlShipCountry.Items.FindByValue(dtTable1.Rows(0).Item("vcShipCountry")) Is Nothing Then
                            ddlShipCountry.Items.FindByValue(dtTable1.Rows(0).Item("vcShipCountry")).Selected = True
                        End If
                    End If
                    If ddlBillCountry.SelectedIndex > 0 Then
                        FillState(ddlBillState, ddlBillCountry.SelectedItem.Value, Session("DomainID"))
                        If Not IsDBNull(dtTable1.Rows(0).Item("vcBilState")) Then
                            If Not ddlBillState.Items.FindByValue(dtTable1.Rows(0).Item("vcBilState")) Is Nothing Then
                                ddlBillState.Items.FindByValue(dtTable1.Rows(0).Item("vcBilState")).Selected = True
                            End If
                        End If
                    End If
                    If ddlShipCountry.SelectedIndex > 0 Then
                        FillState(ddlShipState, ddlShipCountry.SelectedItem.Value, Session("DomainID"))
                        If Not IsDBNull(dtTable1.Rows(0).Item("vcShipState")) Then
                            If Not ddlShipState.Items.FindByValue(dtTable1.Rows(0).Item("vcShipState")) Is Nothing Then
                                ddlShipState.Items.FindByValue(dtTable1.Rows(0).Item("vcShipState")).Selected = True
                            End If
                        End If
                    End If
                    txtBillCode.Text = IIf(IsDBNull(dtTable1.Rows(0).Item("vcBillPostCode")), "", dtTable1.Rows(0).Item("vcBillPostCode"))
                    txtShipCode.Text = IIf(IsDBNull(dtTable1.Rows(0).Item("vcShipPostCode")), "", dtTable1.Rows(0).Item("vcShipPostCode"))
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub


    Sub CheckCreditStatus()
        Try
            Dim objItems As New CItems
            Dim lngCredit, lngRemainingCredit As Long
            objItems.DivisionID = lngDivID
            lngCredit = objItems.GetCreditStatusofCompany
            lngRemainingCredit = objItems.GetAmountDue.Rows(0).Item("RemainingCredit")
            If lngRemainingCredit <= 0 Then
                radBillMe.Visible = False
                radPay.Checked = True
                radBillMe.Visible = False

            Else
                txtCredit.Text = lngCredit
                txtDueAmount.Text = objItems.GetAmountDue.Rows(0).Item("DueAmount")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Public Function KilogramsToPounds(ByVal Kilograms As Single) As Single

        'divide number of kilograms by .4536 to obtain number of pounds

        KilogramsToPounds = Kilograms / 0.4536


    End Function

    Public Function PoundsToKilograms(ByVal Pounds As Single) As Single

        'multiply the number of pounds times .4536 to obtain 
        'number of Kilograms.
        PoundsToKilograms = Pounds * 0.4536

    End Function

    Private Sub bCharge_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bCharge.Click
        Try

            If dgAddedCoourse.Items.Count > 0 Then
                Dim lngOppBizDocID As Long
                If radPay.Checked = True Then
                    Dim ResponseMessage As String = ""
                    Dim objPaymentGateway As New PaymentGateway
                    objPaymentGateway.DomainID = Session("DomainID")
                    If objPaymentGateway.GatewayTransaction(Session("TotalAmount"), txtCustomerFirstName.Text & " " & txtCustomerLastName.Text, txtCardNumber.Text, txtCardCVV2.Text, ddlCardExpMonth.SelectedValue, ddlCardExpYear.SelectedValue, lngContId, ResponseMessage) Then
                        lngOppBizDocID = CreateOpportunity()
                        btnClick.Attributes.Add("onclick", "return OpenBizInvoice('" & Session("OppID") & "','" & lngOppBizDocID & "')")
                        btnClick.Visible = True
                        Session("Data") = Nothing
                        Session("OppID") = Nothing
                    End If
                    liMessage.Text = ResponseMessage
                ElseIf radBillMe.Checked = True Then
                 
                    bCharge.Enabled = False
                    lngOppBizDocID = CreateOpportunity()
                    liMessage.Text = "Your purchase was processed successfully ! "
                    btnClick.Attributes.Add("onclick", "return OpenBizInvoice('" & Session("OppID") & "','" & lngOppBizDocID & "')")
                    btnClick.Visible = True
                    Session("Data") = Nothing
                    Session("OppID") = Nothing
                End If
            Else
                liMessage.Text = "<font color=red>You'll need to select a course before this order can be processed</font>"
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub




    Sub FillState(ByVal ddl As DropDownList, ByVal intCountry As Integer, ByVal intDomainID As Integer)
        Try

      
            Dim dttable As DataTable
            Dim objUserAccess As New UserAccess
            objUserAccess.DomainID = intDomainID
            objUserAccess.Country = intCountry
            dttable = objUserAccess.SelState
            ddl.DataSource = dttable
            ddl.DataTextField = "vcState"
            ddl.DataValueField = "numStateID"
            ddl.DataBind()
            ddl.Items.Insert(0, "--Select One--")
            ddl.Items.FindByText("--Select One--").Value = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Function CreateOpportunity() As Long
        Try

      
            If Not Session("CourseData") Is Nothing Then
                Dim objOpportunity As New MOpportunity
                objOpportunity.OpportunityId = 0
                objOpportunity.PublicFlag = 0
                objOpportunity.ContactID = Session("UserContactID")
                objOpportunity.DivisionID = Session("DivId")
                objOpportunity.OpportunityName = Session("CompName") & "-" & Format(Now(), "MMMM")
                objOpportunity.UserCntID = Session("UserContactID")
                objOpportunity.DomainID = Session("DomainId")
                objOpportunity.OppType = 1
                objOpportunity.Active = 1
                objOpportunity.SalesorPurType = 0
                objOpportunity.Amount = Session("TotalAmount")
                objOpportunity.EstimatedCloseDate = Now
                objOpportunity.CurrencyID = Session("BaseCurrencyID")

                Dim dtTableItem As New DataTable
                Dim dr As DataRow
                dtTableItem.Columns.Add("numItemCode")
                dtTableItem.Columns.Add("numUnitHour")
                dtTableItem.Columns.Add("monPrice")
                dtTableItem.Columns.Add("monTotAmount")
                dtTableItem.Columns.Add("vcItemDesc")
                dtTableItem.Columns.Add("numWarehouseItmsID")
                dtTableItem.Columns.Add("DropShip")
                dtTableItem.Columns.Add("ItemType")
                dtTableItem.Columns.Add("Op_Flag")
                dtTableItem.Columns.Add("bitDiscountType", GetType(Boolean))
                dtTableItem.Columns.Add("fltDiscount", GetType(Decimal))
                dtTableItem.Columns.Add("monTotAmtBefDiscount", GetType(Decimal))

                Dim dtCourseData As DataTable
                dtCourseData = Session("CourseData")
                For Each dr1 As DataRow In dtCourseData.Rows
                    dr = dtTableItem.NewRow
                    dr("numItemCode") = dr1("numItemCode")
                    dr("numUnitHour") = 1
                    dr("monPrice") = dr1("monWListPrice")
                    dr("monTotAmount") = dr1("monWListPrice")
                    dr("vcItemDesc") = "ItemName:" & dr1("Title") & "<br>ClassNum:" & dr1("vcClassNum") & "<br>Course Code:" & dr1("CourseCode") & "<br>Location:" & dr1("vcWarehouse") & "<br>CourseDate:" & dr1("CourseDate") & "<br>CourseDate:" & dr1("CourseDate") & "<br>Desc:" & dr1("txtItemDesc")
                    dr("numWarehouseItmsID") = dr1("numWareHouseItemID")
                    dr("DropShip") = True
                    dr("ItemType") = "P"
                    dr("Op_Flag") = 1
                    dr("bitDiscountType") = 0
                    dr("fltDiscount") = 0
                    dr("monTotAmtBefDiscount") = dr1("monWListPrice")
                    dr("monTotAmount") = dr("monTotAmtBefDiscount")
                    dtTableItem.Rows.Add(dr)
                Next


                Dim dsNew As New DataSet


                If dtTableItem.Rows.Count > 0 Then
                    dtTableItem.TableName = "Item"
                    dsNew.Tables.Add(dtTableItem.Copy)
                    objOpportunity.strItems = "<?xml version=""1.0"" encoding=""iso-8859-1"" ?>" & dsNew.GetXml
                    dsNew.Tables.Remove(dsNew.Tables(0))
                End If


                Dim arrOutPut() As String = New String(2) {}
                arrOutPut = objOpportunity.Save()
                Session("OppID") = arrOutPut(0)
                Dim objItems As New CItems
                objItems.OppId = Session("OppID")
                objItems.byteMode = 1
                objItems.DomainID = Session("DomainID")
                objItems.UserCntId = Session("UserContactID")
                If radBillMe.Checked = True Then
                    objItems.Amount = 0
                Else
                    objItems.Amount = Session("TotalAmount")
                End If
                objItems.ShippingCost = 0
                Session("CourseData") = Nothing
                Return objItems.UpdateDealStatus1()
            Else
                Return 0
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Function

    Private Sub dgAddedCoourse_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgAddedCoourse.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                Dim dtTable As DataTable
                dtTable = Session("CourseData")
                dtTable.Rows.RemoveAt(e.Item.ItemIndex)
                dgAddedCoourse.DataSource = dtTable
                Response.Redirect("CheckOut.aspx")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class