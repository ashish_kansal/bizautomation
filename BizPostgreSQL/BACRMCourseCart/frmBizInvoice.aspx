<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmBizInvoice.aspx.vb"
    Inherits="BACRMCourseCart.frmBizInvoice" %>

<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>BizDocs</title>

    <script language="javascript" type="text/javascript">
        function PrintIt() {
            tblButtons.style.display = 'none';
            window.print();
            return false;
        }
        function Submit() {
            if (document.Form1.txtCCNo.value == '') {
                alert("Enter Credit Card No")
                document.Form1.txtCCNo.focus();
                return false;
            }
            if (document.Form1.txtCHName.value == '') {
                alert("Enter Card Holder Name")
                document.Form1.txtCHName.focus();
                return false;
            }
            if (document.Form1.txtCVV2.value == '') {
                alert("Enter CVV2 or CVD")
                document.Form1.txtCVV2.focus();
                return false;
            }
        }
        function MakeFinal() {
            if (lblBalance.innerText <= 0) {
                return true
            }
            else {
                alert("BizDoc can not be made final, until the balance due reads 0.00")
                return false
            }
        }
        function Close1(lngOppID) {
            //opener.location='dreamweaver.htm';self.close()">
            // window.open('../opportunity/frmQBStatusReport.aspx?BizDocID='+lngOppID,'','toolbar=no,titlebar=no,top=300,width=800,height=500,scrollbars=yes,resizable=yes')
            window.opener.reDirect('../pagelayout/frmOppurtunitydtl.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=opportunitylist&OpID=' + lngOppID);
            self.close();
            return false;
        }
        function openStatusReport(a) {
            window.open('../opportunity/frmQBStatusReport.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&BizDocID=' + a, '', 'toolbar=no,titlebar=no,top=300,width=800,height=500,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenPaymentDetails(a, b) {
            var BalanceAmt;
            BalanceAmt = 0;
            BalanceAmt = window.document.getElementById('lblBalance').innerText;
            BalanceAmt = BalanceAmt.replace(/,/g, "");
            window.open('../opportunity/frmPaymentDetails.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&a=' + a + '&b=' + b + '&c=' + BalanceAmt, '', 'toolbar=no,titlebar=no,top=300,width=800,height=380,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenAmtPaid(a, b) {

            //alert(window.document.getElementById('lblBalance').innerText);
            var BalanceAmt;
            BalanceAmt = 0;
            BalanceAmt = window.document.getElementById('lblBalance').innerText;
            BalanceAmt = BalanceAmt.replace(/,/g, "");
            // alert(BalanceAmt);
            //OppBizDocID===a
            //OppID==B
            //BalanceAmt==c
            window.open('../opportunity/frmAmtPaid.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&a=' + a + '&b=' + b + '&c=' + BalanceAmt, '', 'toolbar=no,titlebar=no,top=300,width=600,height=270,scrollbars=no,resizable=no');
            return false;
        }

        function OpenRecievedDate(OppID) {
            window.open('../opportunity/frmRecievedDate.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppID=' + OppID, '', 'toolbar=no,titlebar=no,top=300,width=400,height=100,scrollbars=yes,resizable=yes');
            return false;
        }
        function ShowWindow(Page, q, att) {

            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                return false;

            }
        }
        function OpenTerms(a, b) {
            window.open('frmTerms.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppBizDocID=' + a + '&Ter=' + b, '', 'toolbar=no,titlebar=no,top=300,width=400,height=100,scrollbars=yes,resizable=yes');
            return false;
        }

        function OpenLogoPage() {
            window.open('frmLogoUpload.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=BizDocs', '', 'toolbar=no,titlebar=no,top=300,width=400,height=100,scrollbars=yes,resizable=yes');
            return false;
        }
        function openClone() {
            window.open('frmCloneDetails.aspx', '', 'toolbar=no,titlebar=no,top=300,width=400,height=100,scrollbars=yes,resizable=yes');
            return false;
        }
        function Save() {
            if (document.Form1.txtDisc.value != '') {
                if (isNaN(parseFloat(document.Form1.txtDisc.value))) {
                    alert("Enter a Valid Discount Percentage!")
                    document.Form1.txtDisc.focus();
                    return false;
                }
            }

            if (parseFloat(document.Form1.txtDisc.value) > 100) {
                alert("Enter a Valid Discount Percentage!")
                document.Form1.txtDisc.focus();
                return false;
            }
        }

-
	function CheckDisc(eventTarget, eventArgument) {

	    if (document.Form1.txtDisc.value != '') {
	        if (isNaN(parseFloat(document.Form1.txtDisc.value))) {
	            alert("Enter a Valid Discount Percentage!")
	            document.Form1.txtDisc.focus();
	            return false;
	        }
	    }


	    if (parseFloat(document.Form1.txtDisc.value) > 100) {
	        alert("Enter a Valid Discount Percentage!")
	        document.Form1.txtDisc.focus();
	        return false;
	    }
	}
        function Close() {
            window.close();
        }

        function CheckNumber(cint) {
            if (cint == 1) {
                if (!(window.event.keyCode > 47 && window.event.keyCode < 58 || window.event.keyCode == 44 || window.event.keyCode == 46)) {
                    window.event.keyCode = 0;
                }
            }
            if (cint == 2) {
                if (!(window.event.keyCode > 47 && window.event.keyCode < 58)) {
                    window.event.keyCode = 0;
                }
            }

        }
        function openApp(a, b, c) {
            window.open('../Documents/frmDocApprovers.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&DocID=' + a + '&DocType=' + b + '&OpID=' + c + "&DocName=" + document.getElementById('hpID').innerText, '', 'toolbar=no,titlebar=no,left=300,top=450,width=900,height=600,scrollbars=yes,resizable=yes')
            return false;
        }
        function SendEmail(a, b) {
            window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail=' + a + '&pqwRT=' + b, '', 'toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenAtch(a, b, c, d) {
            window.open("../opportunity/frmBizDocAttachments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&BizDocID=" + document.Form1.txtBizDoc.value + "&E=2&OpID=" + a + "&OppBizId=" + b + "&DomainID=" + c + "&ConID=" + c, "", "width=800,height=400,status=no,scrollbars=yes,left=155,top=160");
            return false;
        }
        function openeditAddress(a, b) {
            window.open('../opportunity/frmEditOppAddress.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&AddType=' + a + '&OppID=' + b, '', 'toolbar=no,titlebar=no,left=100,top=350,width=300,height=250,scrollbars=yes,resizable=yes')
            return false;
        }
        function Openfooter() {
            window.open('../opportunity/frmFooterUpload.aspx', '', 'toolbar=no,titlebar=no,top=300,width=400,height=100,scrollbars=yes,resizable=yes');
            return false;
        }
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <br>
    <asp:Table ID="Table1" BorderWidth="1" runat="server" Width="100%" BorderColor="black"
        GridLines="None">
        <asp:TableRow>
            <asp:TableCell>
                <table width="100%" border="0" cellpadding="0">
                    <tr>
                        <td valign="top" align="left">
                            <asp:Image ID="imgLogo" runat="server"></asp:Image>
                        </td>
                        <td align="right" colspan="4" nowrap>
                            <asp:Label ID="lblBizDoc" ForeColor="#c0c0c0" Font-Name="Arial black" runat="server"
                                Font-Size="26" Font-Bold="True"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="0">
                    <tr>
                        <td align="right">
                            <table width="100%">
                                <tr bgcolor="ThreeDShadow">
                                    <td class="normal1">
                                        <font color="white">Discount</font>
                                    </td>
                                    <td class="normal1">
                                        <font color="white">Billing Terms</font>
                                    </td>
                                    <td class="normal1">
                                        <font color="white">Due Date</font>
                                    </td>
                                    <td class="normal1">
                                        <font color="white">Date Created</font>
                                    </td>
                                    <td class="normal1">
                                        <font color="white">ID#</font>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1">
                                        <asp:Label ID="lblDiscount" runat="server"></asp:Label>
                                    </td>
                                    <td class="normal1">
                                        <asp:Label ID="lblBillingTerms" runat="server"></asp:Label>
                                    </td>
                                    <td class="normal1">
                                        <asp:Label ID="lblDuedate" runat="server"></asp:Label>
                                    </td>
                                    <td class="normal1">
                                        <asp:Label ID="lblDate" runat="server"></asp:Label>
                                    </td>
                                    <td class="normal1">
                                        <%--	<asp:label id="lblID" Runat="server"></asp:label>--%>
                                        <asp:HyperLink ID="hpID" runat="server" CssClass="hyperlink"> </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="normal1">
                        <td colspan="5">
                            <table height="100%" width="100%">
                                <tr bgcolor="ThreeDShadow">
                                    <td class="normal1">
                                        <a id="hplBillto" runat="server" class="hyperlink"><font color="white">Bill To</font></a>
                                    </td>
                                    <td class="normal1">
                                        <a id="hplShipTo" runat="server" class="hyperlink"><font color="white">Ship To</font></a>
                                    </td>
                                    <td class="normal1">
                                        <font color="white">Status</font>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1">
                                        <asp:Label ID="lblBillTo" runat="server"></asp:Label>
                                    </td>
                                    <td class="normal1">
                                        <asp:Label ID="lblShipTo" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <table height="100%" width="100%">
                                            <tr>
                                                <td colspan="2">
                                                    <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="normal1">
                                                    <font color="#333399">
                                                        <asp:HyperLink class="normal1" ID="hplAmountPaid" runat="server" CssClass="hyperlink">Amount Paid:</asp:HyperLink></font>
                                                </td>
                                                <td class="normal1">
                                                    <asp:Label ID="lblAmountPaid" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="normal1">
                                                    Balance Due:
                                                </td>
                                                <td class="normal1">
                                                    <asp:Label ID="lblBalance" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="normal1">
                        <td colspan="5">
                            <table height="100%" width="100%">
                                <tr bgcolor="ThreeDShadow">
                                    <td class="normal1">
                                        <font color="white">
                                            <asp:Label ID="lblPONo" runat="server"></asp:Label>&nbsp;#</font>
                                    </td>
                                    <td class="normal1">
                                        <font color="white">Opportunity or Deal ID</font>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblNo" runat="server"></asp:Label>
                                    </td>
                                    <td class="normal1">
                                        <asp:Label ID="lblOppID" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="normal1" bgcolor="ThreeDShadow">
                        <td class="normal1" colspan="5">
                            <font color="white">Comments</font>
                        </td>
                    </tr>
                    <tr class="normal1">
                        <td colspan="5">
                            <asp:Label ID="lblComments" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr class="normal1">
                        <td colspan="5">
                            <asp:DataGrid ID="dgBizDocs" runat="server" ForeColor="Transparent" Font-Size="10px"
                                Width="100%" BorderStyle="None" BorderColor="White" BackColor="White" GridLines="Vertical"
                                HorizontalAlign="Center" AutoGenerateColumns="False">
                                <AlternatingItemStyle Font-Size="8pt" Font-Names="tahoma,verdana,arial,helvetica"
                                    BackColor="White"></AlternatingItemStyle>
                                <ItemStyle Font-Size="8pt" Font-Names="tahoma,verdana,arial,helvetica" HorizontalAlign="Center"
                                    BorderStyle="None" BorderColor="White" VerticalAlign="Middle" BackColor="#e0dfe3">
                                </ItemStyle>
                                <HeaderStyle Font-Size="8pt" Font-Names="tahoma,verdana,arial,helvetica" Wrap="False"
                                    HorizontalAlign="Center" Height="20px" ForeColor="White" BorderColor="Black"
                                    VerticalAlign="Middle" BackColor="#9d9da1"></HeaderStyle>
                                <Columns>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td colspan="4">
                            <table width="100%">
                                <tr class="normal1">
                                    <td>
                                        <font color="#999999"><i>Created By &nbsp;</i></font>
                                    </td>
                                    <td>
                                        <font color="#999999"><i>
                                            <asp:Label ID="lblcreated" runat="server"></asp:Label></i></font>
                                    </td>
                                </tr>
                                <tr class="normal1">
                                    <td>
                                        <font color="#999999"><i>Last Modified By &nbsp;</i></font>
                                    </td>
                                    <td>
                                        <font color="#999999"><i>
                                            <asp:Label ID="lblModifiedby" runat="server"></asp:Label></i></font>
                                    </td>
                                </tr>
                                <tr class="normal1">
                                    <td>
                                        <font color="#999999"><i>Last Viewed By &nbsp;</i></font>
                                    </td>
                                    <td>
                                        <font color="#999999"><i>
                                            <asp:Label ID="lblviewwedby" runat="server"></asp:Label></i></font>
                                    </td>
                                </tr>
                                <tr class="normal1" id="trApprove" runat="server">
                                    <td>
                                        <font color="#999999"><i>Approved By &nbsp;</i></font>
                                    </td>
                                    <td>
                                        <font color="#999999"><i>
                                            <asp:Label ID="lblApprovedBy" runat="server"></asp:Label></i></font>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="right">
                            <table id="tblBizDocSumm" runat="server" border="0">
                            </table>
                        </td>
                    </tr>
                </table>
                <asp:Image ID="imgFooter" runat="server" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:Panel ID="pnlShow" runat="server" Visible="false">
        <table id="tblButtons" width="100%">
            <tr>
                <td align="right" class="normal1" colspan="2">
                    <asp:Button ID="btnPrint" runat="server" Text="Print" Width="50" CssClass="button">
                    </asp:Button>
                    <asp:Button ID="btnClose" runat="server" Text="Close" Width="50" CssClass="button">
                    </asp:Button>
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <table>
                        <tr>
                            <td class="text_bold" colspan="3" style="text-decoration: underline">
                                Approval Status
                            </td>
                        </tr>
                        <tr class="normal1">
                            <td>
                                Pending :
                                <asp:Label ID="lblPending" runat="server"></asp:Label>
                            </td>
                            <td>
                                Approved :
                                <asp:Label ID="lblApproved" runat="server"></asp:Label>
                            </td>
                            <td>
                                Declined :
                                <asp:Label ID="lblDeclined" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
                <td align="right" class="normal1" valign="top">
                    <%-- <asp:hyperlink id="hplStatusRepoting" Runat="server" CssClass="hyperlink">Status Report From Accounting  </asp:hyperlink>--%>
                    <asp:HyperLink ID="hplPaymentDetails" Visible="false" runat="server" CssClass="hyperlink">Payment History</asp:HyperLink>
                    <br />
                    <br />
                    <asp:HyperLink ID="hplBizDocAtch" CssClass="hyperlink" runat="server">Attachments</asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlApprove" runat="server">
                        <table width="100%">
                            <tr>
                                <td class="normal1" align="right">
                                    Comments&nbsp;&nbsp;
                                </td>
                                <td class="normal1" align="left">
                                    <asp:TextBox runat="server" ID="txtComment" CssClass="signup" Width="340" TextMode="MultiLine"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="normal4" align="right">
                                    Document is Ready for Approval&nbsp;&nbsp;
                                </td>
                                <td class="normal4" align="left">
                                    <asp:Button ID="btnApprove" runat="server" CssClass="button" Text="Click Here to Approve" />
                                    <asp:Button ID="btnDecline" runat="server" CssClass="button" Text="Click Here to Decline" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <table id="tblCreditForm" runat="server" visible="false">
                        <tr>
                            <td class="text_bold" colspan="4">
                                Pay By Credit Card
                            </td>
                        </tr>
                        <tr>
                            <td class="normal1" align="right">
                                Credit Card No
                            </td>
                            <td>
                                <asp:TextBox Width="200" ID="txtCCNo" CssClass="signup" runat="server"></asp:TextBox>
                            </td>
                            <td class="normal1" align="right">
                                Card Holder Name
                            </td>
                            <td>
                                <asp:TextBox ID="txtCHName" Width="200" runat="server" CssClass="signup"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="normal1" align="right">
                                CVV2 or CVD
                            </td>
                            <td>
                                <asp:TextBox ID="txtCVV2" CssClass="signup" runat="server"></asp:TextBox>
                            </td>
                            <td class="normal1" align="right">
                                Valid Upto
                            </td>
                            <td>
                                <asp:DropDownList CssClass="signup" ID="ddlMonth" runat="server">
                                    <asp:ListItem Value="01">01</asp:ListItem>
                                    <asp:ListItem Value="02">02</asp:ListItem>
                                    <asp:ListItem Value="03">03</asp:ListItem>
                                    <asp:ListItem Value="04">04</asp:ListItem>
                                    <asp:ListItem Value="05">05</asp:ListItem>
                                    <asp:ListItem Value="06">06</asp:ListItem>
                                    <asp:ListItem Value="07">07</asp:ListItem>
                                    <asp:ListItem Value="08">08</asp:ListItem>
                                    <asp:ListItem Value="09">09</asp:ListItem>
                                    <asp:ListItem Value="10">10</asp:ListItem>
                                    <asp:ListItem Value="11">11</asp:ListItem>
                                    <asp:ListItem Value="12">12</asp:ListItem>
                                </asp:DropDownList>
                                <asp:DropDownList CssClass="signup" ID="ddlYear" runat="server">
                                    <asp:ListItem Value="08">08</asp:ListItem>
                                    <asp:ListItem Value="09">09</asp:ListItem>
                                    <asp:ListItem Value="10">10</asp:ListItem>
                                    <asp:ListItem Value="11">11</asp:ListItem>
                                    <asp:ListItem Value="12">12</asp:ListItem>
                                    <asp:ListItem Value="13">13</asp:ListItem>
                                    <asp:ListItem Value="14">14</asp:ListItem>
                                    <asp:ListItem Value="15">15</asp:ListItem>
                                    <asp:ListItem Value="16">16</asp:ListItem>
                                    <asp:ListItem Value="17">17</asp:ListItem>
                                    <asp:ListItem Value="18">18</asp:ListItem>
                                    <asp:ListItem Value="19">19</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="normal1" align="right">
                                Card Type:
                            </td>
                            <td>
                                <asp:DropDownList runat="server" ID="ddlCardType" CssClass="signup">
                                    <asp:ListItem Value="1">Visa </asp:ListItem>
                                    <asp:ListItem Value="2">MasterCard</asp:ListItem>
                                    <asp:ListItem Value="3">AMEX</asp:ListItem>
                                    <asp:ListItem Value="4">Discover</asp:ListItem>
                                    <asp:ListItem Value="5">Diners </asp:ListItem>
                                    <asp:ListItem Value="6">JCB</asp:ListItem>
                                    <asp:ListItem Value="7">BankCard</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="normal1" align="right">
                                Amount
                            </td>
                            <td>
                                <asp:TextBox ID="txtAmount" runat="server" CssClass="signup"></asp:TextBox>
                            </td>
                        </tr>
                        <tr id="trCred" runat="server" visible="false">
                            <td class="normal1" align="right">
                                Source Key
                            </td>
                            <td>
                                <asp:TextBox ID="txtSourceKey" runat="server" CssClass="signup"></asp:TextBox>
                            </td>
                            <td class="normal1" align="right">
                                Pin
                            </td>
                            <td>
                                <asp:TextBox ID="txtSourcePin" runat="server" CssClass="signup"></asp:TextBox>
                                <asp:TextBox ID="txtTest" runat="server" CssClass="signup"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center">
                                <asp:Button ID="btnCharge" runat="server" Text="Submit" CssClass="button" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br />
    </asp:Panel>
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
    <asp:TextBox ID="txtHidden" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtBizDoc" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtConEmail" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtOppOwner" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtCompName" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtConID" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtBizDocRecOwner" Style="display: none" runat="server"></asp:TextBox>
    <input id="hdSubTotal" runat="server" type="hidden" />
    <input id="hdShipAmt" runat="server" type="hidden" />
    <input id="hdTaxAmt" runat="server" type="hidden" />
    <input id="hdDisc" runat="server" type="hidden" />
    <input id="hdLateCharge" runat="server" type="hidden" />
    <input id="hdGrandTotal" runat="server" type="hidden" />
    </form>
</body>
</html>
