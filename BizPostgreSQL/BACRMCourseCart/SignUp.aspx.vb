Imports BACRM.BusinessLogic.Admin
Imports System.IO
Imports System.Reflection
Imports System.Math
Imports System
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Leads
Imports MailBee.SmtpMail
Imports MailBee
Imports MailBee.Mime
Imports MailBee.Security
Partial Public Class SignUp
    Inherits System.Web.UI.Page
    Dim numDomainId As Long
    Dim dtGenericFormConfig As DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            btnCancel.Attributes.Add("onclick", "return Close()")
            numDomainId = Session("DomainID")                                                 'Get the Domain Id from the web.config
            'Put user code to initialize the page here
            If Not IsPostBack Then                                                                  'The form loads for the first time
                'ViewState("referrer") = Request.UrlReferrer.AbsoluteUri
                Dim dtGenericFormHeaderConfig As DataTable                                          'declare a datatable
                Dim objConfigWizard As New FormGenericFormContents                                  'Create an object of class which encaptulates the functionaltiy
                objConfigWizard.FormID = 4                                                          'Set the property of formId
                objConfigWizard.DomainID = numDomainId                                              'Set the Domain Id
                dtGenericFormHeaderConfig = objConfigWizard.getFormHeaderDetails()                  'call to get the form header details
                If dtGenericFormHeaderConfig.Rows.Count = 0 Then                                    'if the form is not registered in the database
                    btnSubmit.Enabled = False                                                       'Disable any attempt to save/ submit
                    Exit Sub                                                                        'Exit the flow
                End If
                Dim objLeadBoxData As New ImplementLeadsAutoRoutingRules                            'Create an object of class which encaptulates the functionaltiy
                objLeadBoxData.DomainID = numDomainId                                               'set the domain id
                Dim numRoutId As Integer = objLeadBoxData.checkForDefaultAutoRule()                                     'Get the Default Rule Id
                If numRoutId = 0 Then                                                               'if the xml for form fields has not been configured
                    btnSubmit.Enabled = False                                                       'Disable any attempt to save/ submit
                    plhFormControls.Controls.Add(New LiteralControl("Auto Rules are not created, please contact the Administrator.")) 'calls to create the form controls and add it to the form
                    Exit Sub                                                                        'and exit the flow
                End If
                hdGroupId.Value = dtGenericFormHeaderConfig.Rows(0).Item("numGrpId")                'Store the Group Id
                hdCompanyType.Value = dtGenericFormHeaderConfig.Rows(0).Item("vcAdditionalParam")   'Store the Company type/ relationship

                'Calls function for form generation and display for non AOI fields
            End If
            callFuncForFormGenerationNonAOI()
            Dim dvConfig As DataView
            dvConfig = New DataView(dtGenericFormConfig)
            dvConfig.RowFilter = " vcDbColumnName like '%Country'"
            Dim dl As DropDownList
            If dvConfig.Count > 0 Then
                Dim i As Integer
                For i = 0 To dvConfig.Count - 1
                    If dvConfig(i).Item("vcDbColumnName") = "vcBillCountry" Then
                        If Not CType(plhFormControls.FindControl("vcBilState"), DropDownList) Is Nothing Then
                            dl = CType(plhFormControls.FindControl(dvConfig(i).Item("vcDbColumnName")), DropDownList)
                            dl.AutoPostBack = True
                            AddHandler dl.SelectedIndexChanged, AddressOf FillStateDyn
                        End If
                    Else
                        If Not CType(plhFormControls.FindControl(Replace(dvConfig(i).Item("vcDbColumnName"), "Country", "State")), DropDownList) Is Nothing Then
                            dl = CType(plhFormControls.FindControl(dvConfig(i).Item("vcDbColumnName")), DropDownList)
                            dl.AutoPostBack = True
                            AddHandler dl.SelectedIndexChanged, AddressOf FillStateDyn
                        End If
                    End If
                    If Not IsPostBack Then
                        Dim dtTable As DataTable
                        Dim objUserAccess As New UserAccess
                        objUserAccess.DomainID = numDomainId
                        dtTable = objUserAccess.GetDomainDetails()
                        If Not dl.Items.FindByValue(dtTable.Rows(0).Item("numDefCountry")) Is Nothing Then
                            dl.Items.FindByValue(dtTable.Rows(0).Item("numDefCountry")).Selected = True
                            If dl.ID = "vcBillCountry" Then
                                FillState(CType(plhFormControls.FindControl("vcBilState"), DropDownList), dl.SelectedItem.Value, numDomainId)
                            Else
                                FillState(CType(plhFormControls.FindControl(Replace(dl.ID, "Country", "State")), DropDownList), dl.SelectedItem.Value, numDomainId)
                            End If
                        End If
                    End If
                Next
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub


    Sub FillStateDyn(ByVal sender As Object, ByVal e As EventArgs)
        Try
            If sender.ID = "vcBillCountry" Then
                FillState(CType(plhFormControls.FindControl("vcBilState"), DropDownList), sender.SelectedItem.Value, numDomainId)
            Else
                FillState(CType(plhFormControls.FindControl(Replace(sender.ID, "Country", "State")), DropDownList), sender.SelectedItem.Value, numDomainId)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This function is called to initiate the generation of the dynamic form.
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	08/20/2005	Created
    ''' </history>
    '''-----------------------------------------------------------------------------
    Sub callFuncForFormGenerationNonAOI()
        Try
            GenericFormControlsGeneration.FormID = 4                                            'set the form id to Order Form
            GenericFormControlsGeneration.DomainID = numDomainId                                'Set the domain id
            GenericFormControlsGeneration.sXMLFilePath = ConfigurationManager.AppSettings("Path")
            GenericFormControlsGeneration.boolAOIField = 0                                      'Set the AOI flag to non AOI
            dtGenericFormConfig = GenericFormControlsGeneration.getFormControlConfig() 'Declare and pupulate a Datatable to store the form config
            If dtGenericFormConfig.Rows.Count = 0 Then                                          'if the xml for form fields has not been configured
                btnSubmit.Enabled = False                                                       'Disable any attempt to save/ submit
                plhFormControls.Controls.Add(New LiteralControl("The screen is not created. Please use BizForm Wizard to create this screen.")) 'calls to create the form controls and add it to the form
                Exit Sub                                                                   'and exit the flow
            End If

            plhFormControls.Controls.Add(GenericFormControlsGeneration.createFormControls(dtGenericFormConfig)) 'calls to create the form controls and add it to the form
            litClientScript.Text = GenericFormControlsGeneration.getJavascriptArray(dtGenericFormConfig) 'Create teh javascript array and store

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This method is used to handle saving of the configuration after the 
    '''     "Submit" button is clicked
    ''' </summary>
    ''' <param name="sender">Represents the sender object.</param>
    ''' <param name="e">Represents the EventArgs.</param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	09/20/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Dim objLeads As New CLeads
        Dim lnDivID, lngCMPID, lnCntID As Long
        Dim lstItem As ListItem
        Dim dRow As DataRow
        Dim ds As New DataSet
        Try
            dtGenericFormConfig.Columns.Add("vcDbColumnText")
            objLeads.GroupID = 1
            objLeads.ContactType = 70
            objLeads.DivisionName = "-"
            Dim vcAssociatedControlType As String
            Dim vcFieldType As String
            For Each dr As DataRow In dtGenericFormConfig.Rows
                vcFieldType = dr("vcFieldType")
                If vcFieldType = "R" Then
                    vcAssociatedControlType = dr("vcAssociatedControlType")
                    Select Case vcAssociatedControlType
                        Case "EditBox"
                            dr("vcDbColumnText") = CType(plhFormControls.FindControl(dr("vcDbColumnName")), TextBox).Text
                            If dr("vcDbColumnText") <> "" Then
                                AssignValuesEditBox(objLeads, dr("vcDbColumnText"), dr("vcDbColumnName"))
                            End If
                        Case "SelectBox"
                            dr("vcDbColumnText") = CType(plhFormControls.FindControl(dr("vcDbColumnName")), DropDownList).SelectedItem.Value
                            If dr("vcDbColumnText") <> "" Then
                                AssignValuesSelectBox(objLeads, dr("vcDbColumnText"), dr("vcDbColumnName"))
                            Else
                                dr("vcDbColumnText") = 0
                            End If
                        Case "TextBox"
                            dr("vcDbColumnText") = CType(plhFormControls.FindControl(dr("vcDbColumnName")), TextBox).Text
                            If dr("vcDbColumnText") <> "" Then
                                AssignValuesTextBox(objLeads, dr("vcDbColumnText"), dr("vcDbColumnName"))

                            End If
                    End Select
                End If
            Next
            objLeads.DomainID = numDomainId

            Dim objAutoRoutRles As New AutoRoutingRules
            objAutoRoutRles.DomainID = numDomainId
            dtGenericFormConfig.TableName = "Table"
            ds.Tables.Add(dtGenericFormConfig.Copy)
            objAutoRoutRles.strValues = ds.GetXml
            ds.Tables.Remove(ds.Tables(0))



            Dim ds1 As DataSet = objLeads.CheckEmailWeb()
            If ds1.Tables(0).Rows.Count > 0 Then
                litMessage.Text = "A contact with this email already exists "
                Exit Sub
            End If

            If ds1.Tables(1).Rows.Count > 0 Then
                litMessage.Text = "An organization with this domain already exists"
                Exit Sub
            End If





            objLeads.UserCntID = objAutoRoutRles.GetRecordOwner
            objLeads.CRMType = 1
            lngCMPID = objLeads.CreateRecordCompanyInfo
            objLeads.CompanyID = lngCMPID
            lnDivID = objLeads.CreateRecordDivisionsInfo
            objLeads.DivisionID = lnDivID

            lnCntID = objLeads.CreateRecordAddContactInfo()
            Session("CompID") = lngCMPID                                       'Set the Company Id in a session
            Session("DivID") = lnDivID
            Session("UserContactID") = lnCntID


            ''Saving CustomFields

            Dim dsViews As New DataView(dtGenericFormConfig)
            dsViews.RowFilter = "vcFieldType='C'"
            Dim i As Integer

            Dim dtCusTable As New DataTable
            dtCusTable.Columns.Add("fld_id")
            dtCusTable.Columns.Add("Value")
            Dim strdetails As String

            If dsViews.Count > 0 Then
                For i = 0 To dsViews.Count - 1
                    dRow = dtCusTable.NewRow
                    dRow("fld_id") = Replace(dsViews(i).Item("numFormFieldId"), "C", "")
                    dRow("Value") = GetCustFldValue(dsViews(i).Item("vcAssociatedControlType"), dsViews(i).Item("vcDBColumnName"))
                    dtCusTable.Rows.Add(dRow)
                Next



                dtCusTable.TableName = "Table"
                ds.Tables.Add(dtCusTable.Copy)
                strdetails = ds.GetXml
                ds.Tables.Remove(ds.Tables(0))

                Dim ObjCusfld As New CustomFields
                ObjCusfld.strDetails = strdetails
                ObjCusfld.RecordId = lnCntID
                ObjCusfld.locId = 4
                ObjCusfld.SaveCustomFldsByRecId()
            End If
            dsViews.RowFilter = "vcFieldType='D'"
            dtCusTable.Rows.Clear()
            If dsViews.Count > 0 Then
                For i = 0 To dsViews.Count - 1
                    dRow = dtCusTable.NewRow
                    dRow("fld_id") = Replace(dsViews(i).Item("numFormFieldId"), "C", "")
                    dRow("Value") = GetCustFldValue(dsViews(i).Item("vcAssociatedControlType"), dsViews(i).Item("vcDBColumnName"))
                    dtCusTable.Rows.Add(dRow)
                Next



                dtCusTable.TableName = "Table"
                ds.Tables.Add(dtCusTable.Copy)
                strdetails = ds.GetXml
                ds.Tables.Remove(ds.Tables(0))

                Dim ObjCusfld As New CustomFields
                ObjCusfld.strDetails = strdetails
                ObjCusfld.RecordId = lnDivID
                ObjCusfld.locId = 1
                ObjCusfld.SaveCustomFldsByRecId()
            End If




            Dim CookieEnabled As Boolean
            CookieEnabled = Context.Request.Browser.Cookies
            If CookieEnabled = True Then
                Dim objcommon As New CCommon
                Dim cookie As HttpCookie
                cookie = New HttpCookie("CompanyID", lnDivID)
                cookie.Expires = DateAdd(DateInterval.Year, 1, Now)
                Context.Response.Cookies.Set(cookie)

                cookie = New HttpCookie("UserContactID", lnCntID)
                cookie.Expires = DateAdd(DateInterval.Year, 1, Now)
                Context.Response.Cookies.Set(cookie)
            End If
            Session("CompID") = lngCMPID                                       'Set the Company Id in a session
            Session("DivID") = lnDivID
            Session("UserContactID") = lnCntID                                          'Set the Contact Id in a session
            Session("DomainID") = numDomainId
            Session("AddressConfirm") = "True"
            'If Session("redirect") = "True" Then
            '    Response.Redirect("../ShoppingCart/Checkout.aspx")
            'Else
            '    Response.Redirect(ViewState("referrer"), True)
            'End If


            ''Creating Extranet account and sending the password to customer

            Dim strRanNO As String = 0
            strRanNO = System.Web.Security.Membership.GeneratePassword(6, 0)

            If objLeads.Email <> "" Then
                Dim objUserGroups As New UserGroups
                objUserGroups.ContactID = lnCntID
                objUserGroups.DomainId = numDomainId
                objUserGroups.Password = strRanNO
                objUserGroups.CreateExtranetAccount()
                Try

                    Dim strBody As String

                    strBody = ConfigurationManager.AppSettings("Body")

                    strBody = strBody.Replace("@@@@Password", strRanNO)

                    strBody = strBody.Replace("@@@@User", objLeads.Email)


                    Dim objEmail As New Email
                    objEmail.SendSimpleEmail(ConfigurationManager.AppSettings("Subject"), strBody, "", ConfigurationManager.AppSettings("MailFrom"), objLeads.Email)

                Catch ex As Exception

                End Try
            End If





            Dim strScript As String = "<script language=JavaScript>"
            strScript += "window.opener.location.href('CheckOut.aspx'); self.close();"
            strScript += "</script>"
            If (Not Page.IsStartupScriptRegistered("clientScript")) Then
                Page.RegisterStartupScript("clientScript", strScript)
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Sub


    Function GetCustFldValue(ByVal fldType As String, ByVal fld_Id As String) As String
        Try


            If fldType = "EditBox" Then
                Dim txt As TextBox
                txt = Page.FindControl(fld_Id)
                Return txt.Text
            ElseIf fldType = "SelectBox" Then
                Dim ddl As DropDownList
                ddl = Page.FindControl(fld_Id)
                Return CStr(IIf(ddl.SelectedItem.Value = "", 0, ddl.SelectedItem.Value))
            ElseIf fldType = "TextBox" Then
                Dim txt As TextBox
                txt = Page.FindControl(fld_Id)
                Return txt.Text
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Function IsBoolean(ByVal sTrg As String) As Boolean
        Try
            Dim intValue As Boolean = Convert.ToBoolean(sTrg)                                   'Try Casting to a boolean
        Catch Ex As Exception
            Return False                                                                        'if it throws an error then return false to indicate that it is non boolean
        End Try
        Return True                                                                             'Test passed, its booelan
    End Function
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This call returns True if the value passed is Integer else False
    ''' </summary>
    ''' <param name="sTrg">Represents the value that is being evaluated for being an Integer.</param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	08/08/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Function IsNumeric(ByVal sTrg As String) As Boolean
        Try
            Dim intValue As Decimal = Convert.ToInt64(sTrg)                                     'Try Casting to a Integer
        Catch Ex As Exception
            Return False
        End Try
        Return True
    End Function


    Sub FillState(ByVal ddl As DropDownList, ByVal lngCountry As Long, ByVal lngDomainID As Long)
        Try
            Dim dtTable As DataTable
            Dim objUserAccess As New UserAccess
            objUserAccess.DomainID = lngDomainID
            objUserAccess.Country = lngCountry
            dtTable = objUserAccess.SelState
            ddl.DataSource = dtTable
            ddl.DataTextField = "vcState"
            ddl.DataValueField = "numStateID"
            ddl.DataBind()
            ddl.Items.Insert(0, "--Select One--")
            ddl.Items.FindByText("--Select One--").Value = 0

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
   
End Class
