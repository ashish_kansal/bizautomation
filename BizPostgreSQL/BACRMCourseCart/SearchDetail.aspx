<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SearchDetail.aspx.vb"
    MasterPageFile="Course.Master" Inherits="BACRMCourseCart.SearchDetail" %>
<%@ Register Assembly="RadComboBox.Net2" Namespace="Telerik.WebControls" TagPrefix="telerik" %>
<%@ Register Assembly="RadCalendar.Net2" Namespace="Telerik.WebControls" TagPrefix="telerik" %>
<%@ Register Assembly="RadInput.Net2" Namespace="Telerik.WebControls" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CphPage" runat="Server">
    <asp:ScriptManager ID="scrpMgr" runat="server">
    </asp:ScriptManager>
    <igmisc:WebPanel EnableAppStyling="true" StyleSetName="Default" StyleSetPath="~/styles"
        ID="WebPanel2" runat="server" ToolTip="Click arrow to the right to expand and close this description.">
        <PanelStyle CssClass="descriptionPanelContent">
        </PanelStyle>
        <Header Text="<font face=arial >Course Search & Filtering</font>" TextAlignment="Left"
            Font-Size="Medium">
            <ExpandedAppearance>
                <Styles CssClass="descriptionPanelHeader">
                </Styles>
            </ExpandedAppearance>
        </Header>
        <Template>
            <table width="100%">
                <tr>
                    <td>
                        <table width="700" border="0">
                            <tr>
                                <td class="text" align="right">
                                    Search
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSearch" CssClass="signup" Width="200px" runat="server">
                                    </asp:TextBox>
                                </td>
                                <td class="text" align="right" style="white-space: nowrap">
                                    From Date
                                </td>
                                <td colspan="2">
                                    <table>
                                        <tr>
                                            <td width="100">
                                                <telerik:RadDatePicker ID="radFrom" Skin="Web20" EnableViewState="true" Calendar-TitleFormat="MM/dd/yyyy"
                                                    runat="server">
                                                </telerik:RadDatePicker>
                                            </td>
                                            <td align="right" class="text">To Date
                                            </td>
                                            <td>
                                            <table>
                                        <tr>
                                            <td width="100">
                                                <telerik:RadDatePicker ID="radTo" runat="server" EnableViewState="true" Calendar-TitleFormat="MM/dd/yyyy"
                                                    Skin="Web20">
                                                </telerik:RadDatePicker>
                                            </td>
                                        </tr>
                                    </table>
                                            </td>
                                            
                                        </tr>
                                    </table>
                                </td>
                               
                                <td>
                                   
                                </td>
                            </tr>
                            <tr>
                                <td class="text" align="right">
                                    Country
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlCountry" CssClass="signup" Width="200px" AutoPostBack="true"
                                        runat="Server">
                                    </asp:DropDownList>
                                </td>
                                <td class="text" align="right">
                                    State
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlState" CssClass="signup" Width="200px" runat="Server">
                                        <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                     <asp:Button ID="btnSearch" CssClass="button" runat="server" Text="Search" Width="100" />
                                </td>
                                <td style="white-space: nowrap;display:none">
                                    <asp:DropDownList ID="ddlMiles" CssClass="signup" runat="server">
                                        <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                        <asp:ListItem Text="25" Value="25"></asp:ListItem>
                                        <asp:ListItem Text="50" Value="50"></asp:ListItem>
                                        <asp:ListItem Text="75" Value="75"></asp:ListItem>
                                        <asp:ListItem Text="100" Value="100"></asp:ListItem>
                                        <asp:ListItem Text="500" Value="500"></asp:ListItem>
                                        <asp:ListItem Text="All" Value="0"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td colspan="2" class="text" align="right" style="white-space: nowrap;display:none">
                                    Miles from Zipcode
                                    <asp:TextBox ID="txtZipCode" CssClass="signup" runat="Server"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DataGrid ID="dgSearch" Width="100%" AutoGenerateColumns="false" runat="server">
                            <AlternatingItemStyle CssClass="ais" />
                            <ItemStyle CssClass="is" />
                            <HeaderStyle CssClass="hs" />
                            <Columns>
                                <asp:BoundColumn DataField="CourseCode" Visible="false"></asp:BoundColumn>
                                <asp:ButtonColumn DataTextField="CourseCode" CommandName="Detail" HeaderText="Course Code">
                                </asp:ButtonColumn>
                                <asp:ButtonColumn DataTextField="Title" CommandName="Detail" HeaderText="Course">
                                </asp:ButtonColumn>
                                <asp:BoundColumn DataField="monWListPrice" HeaderText="Price" DataFormatString="{0:#,##0.00}">
                                </asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblRecords" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
            <table width="100%" id="tblCart" runat="Server">
                <tr>
                    <td>
                        <br />
                        You have chosen the following course(s)
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DataGrid ID="dgAddedCoourse" Width="100%" AutoGenerateColumns="false" runat="server">
                            <AlternatingItemStyle CssClass="ais" />
                            <ItemStyle CssClass="is" />
                            <HeaderStyle CssClass="hs" />
                            <Columns>
                                <asp:BoundColumn DataField="numItemCode" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numWareHouseItemID" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="vcClassNum" Visible="false"></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="Map">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgMap" runat="server" ImageUrl="Images/Map24.gif" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:ButtonColumn DataTextField="CourseCode" CommandName="Detail" HeaderText="Course Code">
                                </asp:ButtonColumn>
                                <asp:ButtonColumn DataTextField="Title" CommandName="Detail" HeaderText="Course">
                                </asp:ButtonColumn>
                                <asp:BoundColumn DataField="CourseDate" HeaderText="Course Date"></asp:BoundColumn>
                                <asp:BoundColumn DataField="City" HeaderText="City"></asp:BoundColumn>
                                <asp:BoundColumn DataField="State" HeaderText="State"></asp:BoundColumn>
                                <asp:BoundColumn DataField="monWListPrice" HeaderText="Price" DataFormatString="{0:#,##0.00}">
                                </asp:BoundColumn>
                                <asp:TemplateColumn>
                                    <ItemTemplate>
                                        <asp:Button ID="btnDelete" runat="server" CommandName="Delete" Text="X" CssClass="Delete" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <br />
                        <asp:Button ID="btnCheckout" runat="server" Text="Checkout" CssClass="button" />
                    </td>
                </tr>
            </table>
        </Template>
    </igmisc:WebPanel>
</asp:Content>
