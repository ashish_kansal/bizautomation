<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CourseIndPage.aspx.vb"
    MasterPageFile="Course.Master" Inherits="BACRMCourseCart.CourseIndPage" %>

<%@ Register Assembly="RadComboBox.Net2" Namespace="Telerik.WebControls" TagPrefix="telerik" %>
<%@ Register Assembly="RadCalendar.Net2" Namespace="Telerik.WebControls" TagPrefix="telerik" %>
<%@ Register Assembly="RadInput.Net2" Namespace="Telerik.WebControls" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CphPage" runat="Server">
    <asp:ScriptManager ID="scrpMgr" runat="server">
    </asp:ScriptManager>
    <igmisc:WebPanel EnableAppStyling="true" StyleSetName="Default" StyleSetPath="~/styles"
        ID="WebPanel2" runat="server" ToolTip="Click arrow to the right to expand and close this description.">
        <PanelStyle CssClass="descriptionPanelContent">
        </PanelStyle>
        <Header Text="<font face=arial >Course Search & Filtering</font>" TextAlignment="Left"
            Font-Size="Medium">
            <ExpandedAppearance>
                <Styles CssClass="descriptionPanelHeader">
                </Styles>
            </ExpandedAppearance>
        </Header>
        <Template>
            <table align="right">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnCancel" CssClass="button" runat="server" Text="Cancel" Width="50" />
                    </td>
                    <td>
                        <asp:UpdateProgress ID="up1" runat="server" DynamicLayout="False">
                            <ProgressTemplate>
                                <asp:Image ID="Image1" ImageUrl="images/updating.gif" runat="server" ImageAlign="Top" />
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
            </table>
            <asp:UpdatePanel ID="updatepanel1" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
                EnableViewState="true">
                <ContentTemplate>
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:DataGrid ID="dgSearch" Width="100%" AutoGenerateColumns="false" runat="server">
                                    <AlternatingItemStyle CssClass="ais" />
                                    <ItemStyle CssClass="is" />
                                    <HeaderStyle CssClass="hs" />
                                    <Columns>
                                        <asp:BoundColumn DataField="numItemCode" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="numWareHouseItemID" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="vcClassNum" Visible="false"></asp:BoundColumn>
                                        <asp:TemplateColumn  Visible="false"  HeaderText="Map">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgMap" runat="server" ImageUrl="Images/Map24.gif" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:ButtonColumn DataTextField="CourseCode" CommandName="Detail" HeaderText="Course Code">
                                        </asp:ButtonColumn>
                                        <asp:ButtonColumn DataTextField="Title" CommandName="Detail" HeaderText="Course">
                                        </asp:ButtonColumn>
                                        <asp:BoundColumn DataField="CourseDate" HeaderText="Course Date"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="City" HeaderText="City"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="State" HeaderText="State"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="monWListPrice" HeaderText="Price" DataFormatString="{0:#,##0.00}">
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Label ID="lblRecords" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </Template>
    </igmisc:WebPanel>
</asp:Content>
