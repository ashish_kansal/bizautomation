<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SignUp.aspx.vb" Inherits="BACRMCourseCart.SignUp" %>

<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="CSS/MASTER.CSS" runat="server" id="CSSlink" type="text/css" rel="stylesheet" />
    <title>Sign up here</title>

    <script language="javascript" src="../include/GenericOrder.js"></script>

    <script language="javascript" type="text/javascript">
        function Close() {
            window.close();
        }
    </script>

</head>
<body>
    <form id="frmGenericFormOrder" method="post" runat="server">
    <br />
    <table>
        <tr>
            <td>
                <asp:PlaceHolder ID="plhFormControls" runat="server"></asp:PlaceHolder>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Button ID="btnSubmit" Text="Submit" runat="server" CssClass="button"></asp:Button>
                <asp:Button ID="btnCancel" CausesValidation="false" Text="Back" runat="server" CssClass="button">
                </asp:Button>
            </td>
        </tr>
    </table>
    <asp:ValidationSummary ID="ValidationSummary" runat="server" HeaderText="Please check the following value(s)"
        ShowSummary="False" ShowMessageBox="True" DisplayMode="List"></asp:ValidationSummary>
    <input id="hdXMLString" type="hidden" name="hdXMLString" runat="server">
    <input id="hdGroupId" type="hidden" name="hdGroupId" runat="server"><!---Store the Group ID--->
    <input id="hdEmail" type="hidden" name="hdEmail" runat="server"><!---Store the Email Id--->
    <input id="hdCompanyType" type="hidden" name="hdCompanyType" runat="server"><!---Store the Company Type --->
    <asp:Literal ID="litClientScript" runat="server"></asp:Literal>
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
