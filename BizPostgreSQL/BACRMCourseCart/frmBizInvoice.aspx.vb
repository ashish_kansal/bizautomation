'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Alerts
Imports System
Partial Public Class frmBizInvoice
    Inherits System.Web.UI.Page

    Dim dtOppBiDocItems As New DataTable
   Dim lintJournalId As Integer
    Dim lngOppBizDocID, lngOppId As Long
    Dim lngDomainID, lngCntID As Long


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            btnClose.Attributes.Add("onclick", "return Close()")
            btnPrint.Attributes.Add("onclick", "return PrintIt()")
            lngOppBizDocID = Request.QueryString("OppBizId")
            lngOppId = Request.QueryString("OpID")
            lngDomainID = IIf(Request.QueryString("DomainID") Is Nothing, Session("DomainID"), Request.QueryString("DomainID"))
            lngCntID = IIf(Request.QueryString("ConID") Is Nothing, Session("UserContactID"), Request.QueryString("ConID"))
            If Not IsPostBack Then
                Try
                    If Request.QueryString("Show") = "True" Then
                        pnlShow.Visible = True
                        tblCreditForm.Visible = True
                    End If
                Catch ex As Exception

                End Try
                btnCharge.Attributes.Add("onclick", "return Submit()")
                getAddDetails()
                getBizDocDetails()
            End If
           hplPaymentDetails.Attributes.Add("onclick", "return OpenPaymentDetails(" & lngOppBizDocID & "," & lngOppId & ")")
            hplBizDocAtch.Attributes.Add("onclick", "OpenAtch(" & Request.QueryString("OpID") & "," & Request.QueryString("OppBizId") & "," & lngDomainID & "," & lngCntID & ")")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub getBizDocDetails()
        Try
            Dim dtOppBiDocDtl As DataTable
            Dim objOppBizDocs As New OppBizDocs
            objOppBizDocs.OppBizDocId = lngOppBizDocID
            objOppBizDocs.OppId = lngOppId
            objOppBizDocs.DomainID = lngDomainID
            objOppBizDocs.UserCntID = lngCntID
            dtOppBiDocDtl = objOppBizDocs.GetOppBizDocDtl
            If dtOppBiDocDtl.Rows.Count <> 0 Then
                If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("monAmountPaid")) Then
                    lblAmountPaid.Text = String.Format("{0:#,##0.00}", dtOppBiDocDtl.Rows(0).Item("monAmountPaid"))
                Else
                    lblAmountPaid.Text = "0.00"
                End If
                If dtOppBiDocDtl.Rows(0).Item("AppReq") = 1 Then
                    pnlApprove.Visible = True
                Else
                    pnlApprove.Visible = False
                End If
                lblPending.Text = dtOppBiDocDtl.Rows(0).Item("Pending")
                lblApproved.Text = dtOppBiDocDtl.Rows(0).Item("Approved")
                lblDeclined.Text = dtOppBiDocDtl.Rows(0).Item("Declined")
                hplBillto.Attributes.Add("onclick", "return openeditAddress('Bill','" & lngOppId & "')")
                hplShipTo.Attributes.Add("onclick", "return openeditAddress('Ship','" & lngOppId & "')")
                If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("BizdocFooter")) Then
                    imgFooter.Visible = True
                    imgFooter.ImageUrl = "../../BizPortal/documents/docs/" & dtOppBiDocDtl.Rows(0).Item("BizdocFooter")
                Else
                    imgFooter.Visible = False
                End If
                If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("vcBizDocImagePath")) Then
                    imgLogo.Visible = True
                    imgLogo.ImageUrl = "../../BizPortal/documents/docs/" & dtOppBiDocDtl.Rows(0).Item("vcBizDocImagePath")
                Else
                    imgLogo.Visible = False
                End If
                hpID.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("vcBizDocID")), "", dtOppBiDocDtl.Rows(0).Item("vcBizDocID"))
                lblcreated.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("numCreatedby")), "", dtOppBiDocDtl.Rows(0).Item("numCreatedby")) & "  &nbsp;" & IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("dtCreatedDate")), "", dtOppBiDocDtl.Rows(0).Item("dtCreatedDate"))
                lblModifiedby.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("numModifiedBy")), "", dtOppBiDocDtl.Rows(0).Item("numModifiedBy")) & "  &nbsp;" & IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("dtModifiedDate")), "", dtOppBiDocDtl.Rows(0).Item("dtModifiedDate"))
                lblviewwedby.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("numViewedBy")), "", dtOppBiDocDtl.Rows(0).Item("numViewedBy")) & "  &nbsp;" & IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("dtViewedDate")), "", dtOppBiDocDtl.Rows(0).Item("dtViewedDate"))
                lblDate.Text = dtOppBiDocDtl.Rows(0).Item("dtCreatedDate")
                If dtOppBiDocDtl.Rows(0).Item("tintBillingTerms") = True Then
                    If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("dtCreatedDate")) Then
                        Dim strDate As Date
                        strDate = DateAdd(DateInterval.Day, dtOppBiDocDtl.Rows(0).Item("numBillingDays"), dtOppBiDocDtl.Rows(0).Item("dtCreatedDate"))
                        lblDuedate.Text = strDate
                    End If
                    lblBillingTerms.Text = "Net " & dtOppBiDocDtl.Rows(0).Item("numBillingDays") & " , " & IIf(dtOppBiDocDtl.Rows(0).Item("tintInterestType") = False, "-", "+") & dtOppBiDocDtl.Rows(0).Item("fltInterest") & " %"
                Else
                    If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("dtCreatedDate")) Then
                        lblDuedate.Text = dtOppBiDocDtl.Rows(0).Item("dtCreatedDate")
                    End If
                    lblBillingTerms.Text = "-"
                End If
                If dtOppBiDocDtl.Rows(0).Item("fltDiscount") > 0 Then
                    If dtOppBiDocDtl.Rows(0).Item("bitDiscountType") = False Then

                        lblDiscount.Text = dtOppBiDocDtl.Rows(0).Item("fltDiscount") & " %"
                    Else
                        lblDiscount.Text = dtOppBiDocDtl.Rows(0).Item("fltDiscount")
                    End If
                End If
                
                lblStatus.Text = dtOppBiDocDtl.Rows(0).Item("BizDocStatus")
                lblComments.Text = dtOppBiDocDtl.Rows(0).Item("vcComments")
                lblNo.Text = dtOppBiDocDtl.Rows(0).Item("vcPurchaseOdrNo")
                hdShipAmt.Value = String.Format("{0:#,##0.00}", dtOppBiDocDtl.Rows(0).Item("monShipCost"))
            End If
            Dim ds As New DataSet
            Dim dtOppBiDocItems As DataTable
            objOppBizDocs.DomainID = Session("DomainID")
            objOppBizDocs.OppBizDocId = lngOppBizDocID
            objOppBizDocs.OppId = lngOppId
            ds = objOppBizDocs.GetOppInItems
            dtOppBiDocItems = ds.Tables(0)


            Dim objCalculateDealAmount As New CalculateDealAmount
            objCalculateDealAmount.CalculateDealAmount(lngOppId, lngOppBizDocID, CShort(dtOppBiDocDtl.Rows(0).Item("tintOppType")), Session("DomainID"), dtOppBiDocItems)

            hdLateCharge.Value = String.Format("{0:#,##0.00}", objCalculateDealAmount.TotalLateCharges)

            hdDisc.Value = String.Format("{0:#,##0.00}", objCalculateDealAmount.TotalDiscount + IIf(dtOppBiDocItems.Rows.Count > 0, dtOppBiDocItems.Compute("SUM(DiscAmt)", ""), 0))

            hdSubTotal.Value = String.Format("{0:#,##0.00}", objCalculateDealAmount.TotalAmount)

            hdGrandTotal.Value = String.Format("{0:#,##0.00}", objCalculateDealAmount.GrandTotal)
            Session("Itemslist") = dtOppBiDocItems

       


            ''Add columns to datagrid
            Dim i As Integer
            Dim dtdgColumns As DataTable
            dtdgColumns = ds.Tables(2)
            Dim bColumn As BoundColumn
            For i = 0 To dtdgColumns.Rows.Count - 1
                bColumn = New BoundColumn
                bColumn.HeaderText = dtdgColumns.Rows(i).Item("vcFormFieldName")
                bColumn.DataField = dtdgColumns.Rows(i).Item("vcDbColumnName")
                If dtdgColumns.Rows(i).Item("vcFieldDataType") = "M" Then bColumn.DataFormatString = "{0:#,##0.00}"
                dgBizDocs.Columns.Add(bColumn)
            Next
            BindItems()
            Dim objConfigWizard As New FormConfigWizard
            If dtOppBiDocDtl.Rows(0).Item("tintOppType") = 1 Then
               objConfigWizard.FormID = 7
            Else
                objConfigWizard.FormID = 8
            End If
            objConfigWizard.DomainID = Session("DomainID")
            objConfigWizard.BizDocID = dtOppBiDocDtl.Rows(0).Item("numBizDocId")
            Dim dsNew As DataSet
            Dim dtTable As DataTable
            dsNew = objConfigWizard.GetFieldFormListForBizDocsSumm
            dtTable = dsNew.Tables(1)
            If dtTable.Rows.Count = 0 Then dtTable = dsNew.Tables(0)
            Dim tblrow As HtmlTableRow
            Dim tblCell As HtmlTableCell

            Dim strLabelCellID As String


            For Each dr As DataRow In dtTable.Rows
                tblrow = New HtmlTableRow
                tblCell = New HtmlTableCell
                tblCell.Attributes.Add("class", "normal1")
                tblCell.InnerText = dr("vcFormFieldName") & ": "
                tblrow.Cells.Add(tblCell)

                tblCell = New HtmlTableCell
                tblCell.Attributes.Add("class", "normal1")
                If dr("numFormFieldId") = 595 Then
                    tblCell.InnerText = hdSubTotal.Value
                ElseIf dr("numFormFieldId") = 596 Then
                    tblCell.InnerText = hdShipAmt.Value
                ElseIf dr("numFormFieldId") = 597 Then
                    tblCell.InnerText = hdTaxAmt.Value
                ElseIf dr("numFormFieldId") = 598 Then
                    tblCell.InnerText = hdLateCharge.Value
                ElseIf dr("numFormFieldId") = 599 Then
                    tblCell.InnerText = hdDisc.Value
                ElseIf dr("numFormFieldId") = 600 Then
                    tblCell.ID = "tdGradTotal"
                    strLabelCellID = tblCell.ID
                    'tblCell.InnerText = hdGrandTotal.Value
                Else
                    If dtOppBiDocDtl.Rows(0).Item("tintOppType") = 1 Then
                        Dim taxAmt As Decimal
                        taxAmt = IIf(IsDBNull(dtOppBiDocItems.Compute("SUM([" & dr("vcFormFieldName") & "])", "[" & dr("vcFormFieldName") & "]>0")), 0, dtOppBiDocItems.Compute("SUM([" & dr("vcFormFieldName") & "])", "[" & dr("vcFormFieldName") & "]>0"))
                        tblCell.InnerText = String.Format("{0:###0.00}", taxAmt)
                    End If
                End If
                tblrow.Cells.Add(tblCell)
                tblBizDocSumm.Rows.Add(tblrow)
            Next
            If strLabelCellID <> "" Then
                CType(tblBizDocSumm.FindControl(strLabelCellID), HtmlTableCell).InnerText = hdGrandTotal.Value
            End If


        Catch ex As Exception
            Throw ex
        End Try

    End Sub


    Sub getAddDetails()
        Try
            Dim dtOppBizAddDtl As DataTable
            Dim objOppBizDocs As New OppBizDocs
            objOppBizDocs.OppBizDocId = lngOppBizDocID
            objOppBizDocs.OppId = lngOppId
            objOppBizDocs.DomainID = lngDomainID
            dtOppBizAddDtl = objOppBizDocs.GetOppInAddDtl
            If dtOppBizAddDtl.Rows.Count <> 0 Then
                'lblAddress.Text = IIf(IsDBNull(dtOppBizAddDtl.Rows(0).Item("CompName")), "", dtOppBizAddDtl.Rows(0).Item("CompName"))
                lblBillTo.Text = IIf(IsDBNull(dtOppBizAddDtl.Rows(0).Item("BillAdd")), "", dtOppBizAddDtl.Rows(0).Item("BillAdd"))
                lblShipTo.Text = IIf(IsDBNull(dtOppBizAddDtl.Rows(0).Item("ShipAdd")), "", dtOppBizAddDtl.Rows(0).Item("ShipAdd"))
                lblOppID.Text = IIf(IsDBNull(dtOppBizAddDtl.Rows(0).Item("OppName")), "", dtOppBizAddDtl.Rows(0).Item("OppName"))
                lblBizDoc.Text = IIf(IsDBNull(dtOppBizAddDtl.Rows(0).Item("BizDcocName")), "", dtOppBizAddDtl.Rows(0).Item("BizDcocName"))
                txtBizDoc.Text = dtOppBizAddDtl.Rows(0).Item("BizDoc")
                txtConEmail.Text = dtOppBizAddDtl.Rows(0).Item("vcEmail")
                txtOppOwner.Text = dtOppBizAddDtl.Rows(0).Item("Owner")
                txtCompName.Text = dtOppBizAddDtl.Rows(0).Item("CompName")
                txtConID.Text = dtOppBizAddDtl.Rows(0).Item("numContactID")

                txtBizDocRecOwner.Text = dtOppBizAddDtl.Rows(0).Item("BizDocOwner")
                objOppBizDocs.BizDocId = dtOppBizAddDtl.Rows(0).Item("BizDoc")
                objOppBizDocs.DomainID = lngDomainID
                Dim i As Integer
                i = objOppBizDocs.GetBizDocAttachments.Rows.Count
                hplBizDocAtch.Text = "Attachments(" & IIf(i = 0, 1, i) & ")"
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub



    Sub BindItems()
        Try
            Dim dtItems As New DataTable
            dtItems = Session("Itemslist")
            dgBizDocs.DataSource = dtItems
            dgBizDocs.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    

  


    Private Sub btnCharge_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCharge.Click
        Try

            Dim ResponseMessage As String = ""
            Dim objPaymentGateway As New PaymentGateway
            objPaymentGateway.DomainID = Session("DomainID")
            If objPaymentGateway.GatewayTransaction(IIf(IsNumeric(txtAmount.Text), txtAmount.Text, 0), txtCHName.Text, txtCCNo.Text, txtCVV2.Text, ddlMonth.SelectedValue, ddlYear.SelectedValue, txtConID.Text, ResponseMessage) Then
                Dim objOppInvoice As New OppInvoice
                objOppInvoice.AmtPaid = txtAmount.Text
                objOppInvoice.UserCntID = lngCntID
                objOppInvoice.OppBizDocId = Request.QueryString("OppBizId")
                objOppInvoice.OppId = Request.QueryString("OpID")
                objOppInvoice.PaymentMethod = 1
                objOppInvoice.BizDocsPaymentDetId = 0
                ''objOppInvoice.DepositTo = ddlAccounts.SelectedItem.Value
                objOppInvoice.Reference = ResponseMessage
                objOppInvoice.IntegratedToAcnt = False
                objOppInvoice.Memo = ""
                objOppInvoice.DomainId = lngDomainID
                objOppInvoice.DeferredIncomeStartDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                objOppInvoice.bitDisable = True
                objOppInvoice.UpdateAmountPaid()
            End If
            litMessage.Text = ResponseMessage
            getBizDocDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

   

   

End Class