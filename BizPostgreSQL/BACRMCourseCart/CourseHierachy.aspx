<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CourseHierachy.aspx.vb"
    Inherits="BACRMCourseCart.CourseHierachy" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>


</head>
<body>
    <form id="form1" runat="server">
    <br />
    <table align="Left">
        <tr>
            <td>
                <igmisc:WebPanel EnableAppStyling="true" StyleSetName="Default" StyleSetPath="~/styles"
                    ID="WebPanel1" runat="server" ToolTip="Click arrow to the right to expand and close this description.">
                    <PanelStyle CssClass="descriptionPanelContent">
                    </PanelStyle>
                    <Header Text="<font face=arial >Online Course Catalog</font>" TextAlignment="Left">
                        <ExpandedAppearance>
                            <Styles CssClass="descriptionPanelHeader" Font-Size="Medium">
                            </Styles>
                        </ExpandedAppearance>
                    </Header>
                    <Template>
                        <ignav:UltraWebTree ID="UltraWebTree1" runat="server" Font-Names="Arial" TargetFrame="rightframe"
                            Font-Size="12px" BorderStyle="None" BorderWidth="0px" WebTreeTarget="ClassicTree">
                        </ignav:UltraWebTree>
                    </Template>
                </igmisc:WebPanel>
            </td>
        </tr>
    </table>
    <br />
    </form>
</body>
</html>
