﻿Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports System
Partial Public Class CourseIndPage
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                BindGrid()
            End If
        Catch ex As System.Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgSearch_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgSearch.ItemCommand
        If e.CommandName = "Detail" Then
            Response.Redirect("ItemDetail.aspx?ItemID=" & e.Item.Cells(0).Text & "&WItemID=" & e.Item.Cells(1).Text & "&ClassNum=" & e.Item.Cells(2).Text.Trim)
        End If
    End Sub


    Sub BindGrid()
        Try
            Dim objCourses As New CourseCart
            objCourses.DomainID = Session("DomainID")
            objCourses.CountryID = Request.QueryString("Country")
            objCourses.StateID = Request.QueryString("State")
            If Request.QueryString("To") <> "" Then
                objCourses.ToDate = Left(CType(Request.QueryString("To"), String), 10)
            End If
            If Request.QueryString("From") <> "" Then
                objCourses.FromDate = Left(CType(Request.QueryString("From"), String), 10)
            End If
            objCourses.Miles = Request.QueryString("Miles")
            objCourses.ZipCode = Request.QueryString("Zipcode")
            objCourses.ModelID = Request.QueryString("CourseCode")
            objCourses.byteMode = 1
            Dim dtTable As DataTable
            dtTable = objCourses.GetCourses
            If dtTable.Rows.Count = 1 Then
                Response.Redirect("ItemDetail.aspx?ItemID=" & dtTable.Rows(0).Item("numItemCode") & "&WItemID=" & dtTable.Rows(0).Item("numWareHouseItemID") & "&ClassNum=" & dtTable.Rows(0).Item("vcClassNum").ToString.Trim)
            End If
            dgSearch.DataSource = dtTable
            dgSearch.DataBind()
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub btnCancel_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles btnCancel.Command
        Response.Redirect("SearchDetail.aspx")
    End Sub
End Class