Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports System
Partial Public Class ItemDetail
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

       
            If Not IsPostBack Then
                Dim objItems As New CourseCart
                Dim dtItemDetails As DataTable
                objItems.WareHouseItemID = Request.QueryString("WItemID")
                objItems.ClassNum = Request.QueryString("ClassNum")
                dtItemDetails = objItems.GetCourseDetail
                lblModelName.Text = IIf(IsDBNull(dtItemDetails.Rows(0).Item("Title")), "", dtItemDetails.Rows(0).Item("Title"))
                lblDescription.Text = IIf(IsDBNull(dtItemDetails.Rows(0).Item("txtItemDesc")), "", dtItemDetails.Rows(0).Item("txtItemDesc"))
                lblUnitCost.Text = String.Format("{0:#,##0.00}", dtItemDetails.Rows(0).Item("monWListPrice"))
                lblAddress.Text = IIf(IsDBNull(dtItemDetails.Rows(0).Item("Address")), "", dtItemDetails.Rows(0).Item("Address"))
                lblCourseDate.Text = IIf(IsDBNull(dtItemDetails.Rows(0).Item("CourseDate")), "", dtItemDetails.Rows(0).Item("CourseDate"))
                lblCourseCode.Text = IIf(IsDBNull(dtItemDetails.Rows(0).Item("CourseCode")), "", dtItemDetails.Rows(0).Item("CourseCode"))
                lblCourseLength.Text = IIf(IsDBNull(dtItemDetails.Rows(0).Item("LENGTH")), "", dtItemDetails.Rows(0).Item("LENGTH")) & " Days"
                lblPrerequisites.Text = IIf(IsDBNull(dtItemDetails.Rows(0).Item("PREREQUISITS")), "", dtItemDetails.Rows(0).Item("PREREQUISITS"))
                lblAudience.Text = IIf(IsDBNull(dtItemDetails.Rows(0).Item("AUDIENCE_TEXT")), "", dtItemDetails.Rows(0).Item("AUDIENCE_TEXT"))
                lblCourseObjectives.Text = IIf(IsDBNull(dtItemDetails.Rows(0).Item("OBJECTIVE")), "", dtItemDetails.Rows(0).Item("OBJECTIVE"))
                lblCourseOutline.Text = IIf(IsDBNull(dtItemDetails.Rows(0).Item("Topic")), "", dtItemDetails.Rows(0).Item("Topic"))
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    

    Private Sub btnRegister_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegister.Click
        Try

            Dim objItems As New CourseCart
            Dim dtTable As DataTable
            objItems.WareHouseItemID = Request.QueryString("WItemID")
            objItems.ClassNum = Request.QueryString("ClassNum")
            dtTable = objItems.GetCourseDetail
            If Not Session("CourseData") Is Nothing Then
                dtTable.Merge(CType(Session("CourseData"), DataTable))
            End If
            Session("CourseData") = dtTable
            Response.Redirect("SearchDetail.aspx")

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("SearchDetail.aspx")
    End Sub
End Class