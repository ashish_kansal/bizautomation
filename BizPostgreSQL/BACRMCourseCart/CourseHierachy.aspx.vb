﻿Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports System
Partial Public Class CourseHierachy
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                PopulateMenu()
         End If
          
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
       
    End Sub


    Private Sub PopulateMenu()
        Try
            Dim objItems As New CItems
            Dim dt As DataTable
            objItems.byteMode = 10
            objItems.DomainID = Session("DomainID")
            dt = objItems.SeleDelCategory
            PopulateMenuitem(dt, UltraWebTree1.Nodes)
        Catch ex As Exception
            Throw ex
        End Try
        
    End Sub

    Private Sub PopulateMenuitem(ByVal dt As DataTable, ByVal TreeViewnode As Infragistics.WebUI.UltraWebNavigator.Nodes)
        Try
            For Each dr As DataRow In dt.Rows
                Dim tn As New Infragistics.WebUI.UltraWebNavigator.Node
                tn.Text = dr("vcCategoryName").ToString()
                tn.Tag = dr("numCategoryID").ToString() & IIf(dr("Type") <> 2, "C", "")

                If dr("Type") = 0 Then
                    tn.TargetUrl = "SearchDetail.aspx?CatID=" & dr("numCategoryID")
                ElseIf dr("Type") = 1 Then
                    tn.TargetUrl = dr("vcLink")
                ElseIf dr("Type") = 2 Then
                    tn.TargetUrl = "ItemDetail.aspx?ItemID=" & dr("numCategoryID")
                End If
                If dr("Type") = 2 Then
                    If Not UltraWebTree1.Find(dr("Category").ToString() & "C") Is Nothing Then
                        UltraWebTree1.Find(dr("Category").ToString() & "C").Nodes.Add(tn)
                    End If
                Else
                    If dr("Category") = 0 Then
                        UltraWebTree1.Nodes.Add(tn)
                        tn.Expanded = True
                    Else
                        If Not UltraWebTree1.Find(dr("Category").ToString() & "C") Is Nothing Then
                            UltraWebTree1.Find(dr("Category").ToString() & "C").Nodes.Add(tn)
                        End If
                    End If
                End If
                'tn.Expanded = boolExpand
            Next
        Catch ex As Exception
            Throw ex
        End Try
        
    End Sub

End Class