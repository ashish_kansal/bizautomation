<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="Course.Master" CodeBehind="Login.aspx.vb"
    Inherits="BACRMCourseCart.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CphPage" runat="Server">
    <igmisc:WebPanel EnableAppStyling="true" StyleSetName="Default" StyleSetPath="~/styles"
        ID="WebPanel2" runat="server" ToolTip="Click arrow to the right to expand and close this description.">
        <PanelStyle CssClass="descriptionPanelContent">
        </PanelStyle>
        <Header Text="<font face=arial >Login</font>" TextAlignment="Left" Font-Size="Medium">
            <ExpandedAppearance>
                <Styles CssClass="descriptionPanelHeader">
                </Styles>
            </ExpandedAppearance>
        </Header>
        <Template>
            <br />
            <br />
            <table id="tblLogin" align="center" runat="server" border="0">
                <tr>
                    <td class="normal4" colspan="2">
                        <asp:Literal ID="litMessage" runat="server"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td class="normal1" align="right">
                        Email Address
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtEmaillAdd" CssClass="signup" runat="server" Width="250"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="normal1" align="right">
                        Password
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtPassword" CssClass="signup" TextMode="Password" Width="250" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Button ID="btnLogin" CssClass="button" runat="server" Text="Login" Width="80">
                        </asp:Button>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <a href="#" onclick="OpenSignUp()" runat="server" id="lnkNewUsers">New Users - Sign
                            up here</a>
                    </td>
                </tr>
            </table>
            <br />
            <br />
        </Template>
    </igmisc:WebPanel>

    <script language="javascript" type="text/javascript">
    function OpenSignUp()
		{
			window.open('SignUp.aspx','','toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')
			return false;
		}
    </script>

</asp:Content>
