Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Partial Public Class Course
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Session("UserContactID") Is Nothing Then
                lnkLogin.Visible = True
                lnkLogOut.Visible = False
            Else
                lnkLogin.Visible = False
                lnkLogOut.Visible = True
            End If
        End If
    End Sub

    Private Sub lnkLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLogin.Click
        Response.Redirect("Login.aspx")
    End Sub

    Private Sub lnkLogOut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLogOut.Click
        Session.Abandon()
        Response.Redirect("Login.aspx")
    End Sub
End Class