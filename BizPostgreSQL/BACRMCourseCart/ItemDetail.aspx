<%@ Page Language="vb" MasterPageFile="Course.Master" AutoEventWireup="false" CodeBehind="ItemDetail.aspx.vb"
    Inherits="BACRMCourseCart.ItemDetail" %>
<%@ Register Assembly="RadComboBox.Net2" Namespace="Telerik.WebControls" TagPrefix="telerik" %>
<%@ Register Assembly="RadCalendar.Net2" Namespace="Telerik.WebControls" TagPrefix="telerik" %>
<%@ Register Assembly="RadInput.Net2" Namespace="Telerik.WebControls" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CphPage" runat="Server">
    <igmisc:WebPanel EnableAppStyling="true" StyleSetName="Default" StyleSetPath="~/styles"
        ID="WebPanel2" runat="server" ToolTip="Click arrow to the right to expand and close this description.">
        <PanelStyle CssClass="descriptionPanelContent">
        </PanelStyle>
        <Header Text="<font face=arial >Course Detail</font>" TextAlignment="Left" Font-Size="Medium">
            <ExpandedAppearance>
                <Styles CssClass="descriptionPanelHeader">
                </Styles>
            </ExpandedAppearance>
        </Header>
        <Template>
            <table>
                <tr>
                    <td colspan="2">
                        <table width="100%">
                            <tr>
                                <td>
                                    <asp:Label ID="lblModelName" CssClass="text_bold" runat="server"></asp:Label>
                                </td>
                                <td align="right">
                                    <asp:Button ID="btnRegister" CssClass="button" runat="server" Text="Register for Course"
                                        Width="150" />
                                    <asp:Button ID="btnCancel" CssClass="button" runat="server" Text="Cancel" Width="50" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    Price: $<asp:Label ID="lblUnitCost" runat="server" Font-Bold="True"></asp:Label>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Course Date:
                                    <asp:Label ID="lblCourseDate" runat="server" Font-Bold="True"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Course Code:
                                    <asp:Label ID="lblCourseCode" runat="server" Font-Bold="True"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Course Length:
                                    <asp:Label ID="lblCourseLength" runat="server" Font-Bold="True"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Address:
                                    <asp:Label ID="lblAddress" runat="server" Font-Bold="True"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Literal ID="litMessage" runat="server"></asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="lblDescription" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <b>Prerequisites</b>
                        <asp:Label ID="lblPrerequisites" runat="server"></asp:Label>
                        </br>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <b>Audience</b>
                        <asp:Label ID="lblAudience" runat="server"></asp:Label>
                        </br>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <b>Course Objectives</b>
                        <asp:Label ID="lblCourseObjectives" runat="server"></asp:Label>
                        </br>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <b>Course Outlines</b>
                        <asp:Label ID="lblCourseOutline" runat="server"></asp:Label>
                        </br>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="lblText" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </Template>
    </igmisc:WebPanel>
</asp:Content>
