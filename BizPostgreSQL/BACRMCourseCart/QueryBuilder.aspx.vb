﻿Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Partial Public Class QueryBuilder
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
           
        End If

    End Sub


    Sub BindGrid()
        Dim objCourses As New CourseCart
        objCourses.DomainID = Session("DomainID")

        If Request.QueryString("Warehouse") <> "" Then
            objCourses.Warehouse = Request.QueryString("Warehouse")
        End If

        If Request.QueryString("State") <> "" Then
            objCourses.State = Request.QueryString("State")
        End If

        If Request.QueryString("Chapter") <> "" Then
            objCourses.Chapter = Request.QueryString("Chapter")
        End If

        If Request.QueryString("SubChapter") <> "" Then
            objCourses.SubChapter = Request.QueryString("SubChapter")
        End If

        If Request.QueryString("ToDate") <> "" Then
            objCourses.ToDate = Request.QueryString("ToDate")
        End If
        If Request.QueryString("FromDate") <> "" Then
            objCourses.FromDate = Request.QueryString("FromDate")
        End If
        If Request.QueryString("CatID") = 61 Then
            objCourses.CategoryID = 0
        ElseIf Request.QueryString("CatID") <> "" Then
            objCourses.CategoryID = Request.QueryString("CatID")
        End If
        If Request.QueryString("CourseCode") <> "" Then
            objCourses.ModelID = Request.QueryString("CourseCode")
        End If

        If Request.QueryString("CatID") = 61 Then
            objCourses.CategoryID = 0
        Else
            objCourses.CategoryID = Request.QueryString("CatID")
        End If

        objCourses.byteMode = 2
        Dim dtTable As DataTable
        dtTable = objCourses.GetCourses
        dgSearch.DataSource = dtTable
        dgSearch.DataBind()
        lblRecords.Text = dtTable.Rows.Count & " result(s) found."

    End Sub

    Private Sub dgSearch_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgSearch.ItemCommand
        If e.CommandName = "Detail" Then
            Response.Redirect("ItemDetail.aspx?ItemID=" & e.Item.Cells(0).Text & "&WItemID=" & e.Item.Cells(1).Text & "&ClassNum=" & e.Item.Cells(2).Text.Trim)
        End If
    End Sub
End Class