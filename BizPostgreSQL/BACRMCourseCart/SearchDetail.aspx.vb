﻿Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Partial Public Class SearchDetail
    Inherits System.Web.UI.Page
    Dim lngCategory As Long = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dim objCommon As New CCommon
            objCommon.sb_FillComboFromDBwithSel(ddlCountry, 40, Session("DomainID"))
            Dim dtTable As DataTable
            Dim objUserAccess As New UserAccess
            objUserAccess.DomainID = Session("DomainID")
            dtTable = objUserAccess.GetDomainDetails()
            If Not ddlCountry.Items.FindByValue(dtTable.Rows(0).Item("numDefCountry")) Is Nothing Then
                ddlCountry.Items.FindByValue(dtTable.Rows(0).Item("numDefCountry")).Selected = True
                FillState(ddlState, ddlCountry.SelectedItem.Value, Session("DomainID"))
            End If
            If Request.QueryString("CatID") <> "" Then
                BindGrid()
            End If
            If Not Session("CourseData") Is Nothing Then
                dgAddedCoourse.DataSource = Session("CourseData")
                dgAddedCoourse.DataBind()
                tblCart.Visible = True
            Else
                tblCart.Visible = False
            End If
        End If

    End Sub

    Sub BindGrid()
        Dim objCourses As New CourseCart
        objCourses.DomainID = Session("DomainID")
        objCourses.CountryID = ddlCountry.SelectedValue
        objCourses.StateID = ddlState.SelectedValue
        objCourses.SearchText = txtSearch.Text
        If radTo.DateInput.Text <> "" Then
            objCourses.ToDate = radTo.SelectedDate
        End If
        If radFrom.DateInput.Text <> "" Then
            objCourses.FromDate = radFrom.SelectedDate
        End If
        If Request.QueryString("CatID") = 61 Then
            objCourses.CategoryID = 0
        Else
            objCourses.CategoryID = Request.QueryString("CatID")
        End If
        objCourses.Miles = ddlMiles.SelectedValue
        objCourses.ZipCode = txtZipCode.Text
        objCourses.byteMode = 0
        Dim dtTable As DataTable
        dtTable = objCourses.GetCourses
        dgSearch.DataSource = dtTable
        dgSearch.DataBind()
        lblRecords.Text = dtTable.Rows.Count & " result(s) found."

    End Sub





    Protected Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        BindGrid()
    End Sub

    Sub FillState(ByVal ddl As DropDownList, ByVal lngCountry As Long, ByVal lngDomainID As Long)
        Dim dtTable As DataTable
        Dim objUserAccess As New UserAccess
        objUserAccess.DomainID = lngDomainID
        objUserAccess.Country = lngCountry
        dtTable = objUserAccess.SelState
        ddl.DataSource = dtTable
        ddl.DataTextField = "vcState"
        ddl.DataValueField = "numStateID"
        ddl.DataBind()
        ddl.Items.Insert(0, "--Select One--")
        ddl.Items.FindByText("--Select One--").Value = 0
    End Sub


    Private Sub ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCountry.SelectedIndexChanged
        FillState(ddlState, ddlCountry.SelectedValue, Session("DomainID"))
    End Sub

    Private Sub dgSearch_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgSearch.ItemCommand
        If e.CommandName = "Detail" Then
            Response.Redirect("CourseIndPage.aspx?CourseCode=" & e.Item.Cells(0).Text & "&Country=" & ddlCountry.SelectedItem.Value & "&From=" & radFrom.DateInput.Text & "&To=" & radTo.DateInput.Text & "&State=" & ddlState.SelectedItem.Value & "&Miles=" & ddlMiles.SelectedItem.Value & "&Zipcode=" & txtZipCode.Text)
        End If
    End Sub





    Private Sub dgAddedCoourse_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgAddedCoourse.ItemCommand
        If e.CommandName = "Delete" Then
            Dim dtTable As DataTable
            dtTable = Session("CourseData")
            dtTable.Rows.RemoveAt(e.Item.ItemIndex)
            dgAddedCoourse.DataSource = dtTable
            dgAddedCoourse.DataBind()
        ElseIf e.CommandName = "Detail" Then
            Response.Redirect("ItemDetail.aspx?ItemID=" & e.Item.Cells(0).Text & "&WItemID=" & e.Item.Cells(1).Text & "&ClassNum=" & e.Item.Cells(2).Text.Trim)
        End If
    End Sub

    Private Sub btnCheckout_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheckout.Click
        If Session("UserContactID") Is Nothing Then
            Response.Redirect("Login.aspx")
        Else
            Response.Redirect("checkout.aspx")
        End If
    End Sub

End Class