Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common

Partial Public Class Login : Inherits BACRMPage
    Dim strFrom As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Set cookie value if Querystring contains "id_site"
            'Dim objCommon As New CCommon
            'Dim lngDomainID As Long
            'If Not GetQueryStringVal( "id_site") Is Nothing Then
            '    lngDomainID = CCommon.ToLong(objCommon.Decrypt(GetQueryStringVal( "id_site")))
            '    If lngDomainID > 0 Then
            '        If Not Request.Cookies("id_site") Is Nothing Then
            '            Request.Cookies("id_site").Value = lngDomainID
            '            Request.Cookies("id_site").Expires = DateAdd(DateInterval.Year, 1, Now)
            '        Else
            '            Dim cookie As HttpCookie
            '            cookie = New HttpCookie("id_site", lngDomainID)
            '            cookie.Expires = DateAdd(DateInterval.Year, 1, Now)
            '            Context.Response.Cookies.Set(cookie)
            '        End If
            '    End If
            'End If
            If Not IsPostBack Then
                strFrom = CCommon.ToString(GetQueryStringVal( "From"))
                If strFrom.ToLower = "bizcart" Or strFrom.ToLower = "email" Then
                    hdnWebStoreURL.Value = CCommon.ToString(Request.UrlReferrer)
                    Dim strUsername, strPassword As String
                    strUsername = GetQueryStringVal( "u")
                    Dim objCommon As New CCommon
                    strPassword = objCommon.Decrypt(GetQueryStringVal( "p"))

                    If Not (strUsername Is Nothing And strPassword Is Nothing) Then
                        Login(strUsername, strPassword)
                    End If
                    'Response.Write(Session("WebStoreURL"))
                    'Else
                    '    Session.Abandon()
                End If

                btnLogin.Attributes.Add("onclick", "return Login()")

                'Show Logo of company on Login page if Cookie is available
                'If Not Request.Cookies("id_site") Is Nothing Then
                'If CCommon.ToLong(Session("DomainID")) > 0 Then
                '    Dim dtTable As DataTable
                '    Dim objUserAccess As New UserAccess
                '    objUserAccess.DomainID = CCommon.ToLong(Session("DomainID")) 'CCommon.ToLong(Request.Cookies.Get("id_site").Value)
                '    dtTable = objUserAccess.GetDomainDetails()
                '    If dtTable.Rows.Count > 0 Then
                '        If Not IsDBNull(dtTable.Rows(0).Item("vcPortalLogo")) Then imgLogo.Src = "Documents/Docs/" & objUserAccess.DomainID & "/" & dtTable.Rows(0).Item("vcPortalLogo")
                '    End If
                'End If

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub


    Private Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Try
            Login(txtEmaillAdd.Text.Trim, txtPassword.Text.Trim)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkForgot_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkForgot.Click
        Try
            trLogin.Visible = False
            trDomain.Visible = False
            trForgotPwd.Visible = True
            trPassword.Visible = False
            trForgotPwdLink.Visible = False
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub Login(ByVal email As String, ByVal password As String)
        Try
            'Dim offset, remainder As Integer
            'Dim mulFact As Short
            'offset = 0
            'Try
            '    offset = DateDiff(DateInterval.Minute, CDate(txtOffset.Text), Date.UtcNow)

            '    'remainder = (offset / 30)
            '    If offset < 0 Then
            '        offset = offset * -1
            '        mulFact = -1
            '    Else : mulFact = 1
            '    End If
            '    Math.DivRem(offset, 30, remainder)
            '    If remainder > 15 Then
            '        remainder = 30 - remainder
            '        offset = offset + remainder
            '    Else : offset = offset - remainder
            '    End If
            '    offset = offset * mulFact
            'Catch ex As Exception

            'End Try
            Session("ClientMachineUTCTimeOffset") = txtOffset.Text.Trim
            Dim objUserAccess As New UserAccess
            Dim dttable As DataTable
            objUserAccess.Email = email
            objUserAccess.Password = password
            If ddlDomain.Items.Count = 0 Then
                FillDomain()
                litMessage.Text = ""
                If ddlDomain.Items.Count > 1 Then
                    Exit Sub
                End If
            End If

            objUserAccess.DomainID = CCommon.ToLong(ddlDomain.SelectedValue) 'CCommon.ToLong(Session("DomainID")) 'Request.Cookies.Get("id_site").Value
            If CCommon.ToLong(ddlDomain.SelectedValue) = 0 Then
                litMessage.Text = "Wrong Username/Password.!"
                Exit Sub
            Else
                litMessage.Text = ""
            End If

            dttable = objUserAccess.ExtranetLogin
            If dttable.Rows(0).Item(0) = 1 Then
                Session("CompID") = dttable.Rows(0).Item(3)
                Session("UserContactID") = dttable.Rows(0).Item(1)
                Session("ContactId") = dttable.Rows(0).Item(1)
                Session("DivId") = dttable.Rows(0).Item(2)
                Session("GroupId") = dttable.Rows(0).Item(4)
                Session("CRMType") = dttable.Rows(0).Item(5)
                Session("DomainID") = dttable.Rows(0).Item(6)
                Session("CompName") = dttable.Rows(0).Item(7)
                Session("RecOwner") = dttable.Rows(0).Item(8)
                Session("RecOwnerName") = dttable.Rows(0).Item(9)
                Session("PartnerGroup") = dttable.Rows(0).Item(10)
                Session("RelationShip") = dttable.Rows(0).Item(11)
                Session("Profile") = dttable.Rows(0).Item("numProfileID")
                Session("HomePage") = dttable.Rows(0).Item(12)
                Session("UserEmail") = dttable.Rows(0).Item(13)
                Session("DateFormat") = dttable.Rows(0).Item("vcDateFormat")
                Session("ContactName") = dttable.Rows(0).Item("ContactName")
                Session("PartnerAccess") = dttable.Rows(0).Item(15)
                Session("PagingRows") = dttable.Rows(0).Item("tintCustomPagingRows")
                Session("DefCountry") = dttable.Rows(0).Item("numDefCountry")
                Session("PopulateUserCriteria") = dttable.Rows(0).Item("tintAssignToCriteria")
                Session("EnableIntMedPage") = IIf(dttable.Rows(0).Item("bitIntmedPage") = True, 1, 0)
                Session("Internal") = 1
                Session("StartDate") = dttable.Rows(0).Item("StartDate") '' Start date
                Session("EndDate") = dttable.Rows(0).Item("EndDate") '' End date
                Session("DomainName") = dttable.Rows(0).Item("vcDomainName") '' End date
                Session("BaseCurrencyID") = dttable.Rows(0).Item("numCurrencyID")

                Session("SMTPServer") = dttable.Rows(0).Item("vcSMTPServer")
                Session("SMTPPort") = dttable.Rows(0).Item("numSMTPPort")
                Session("SMTPAuth") = dttable.Rows(0).Item("bitSMTPAuth")
                Session("SMTPPassword") = dttable.Rows(0).Item("vcSmtpPassword")
                Session("bitSMTPSSL") = dttable.Rows(0).Item("bitSMTPSSL")
                Session("SMTPServerIntegration") = dttable.Rows(0).Item("bitSMTPServer")

                Session("CompWindow") = 1
                Session("Logo") = dttable.Rows(0).Item("vcPortalLogo")
                Session("WebStoreURL") = hdnWebStoreURL.Value
                Session("EnableCustomizePortal") = dttable.Rows(0).Item("bitCustomizePortal")
                Session("SiteType") = Request.Url.Scheme + ":"
                If CCommon.ToBool(Session("EnableCustomizePortal")) = True Then
                    Dim objSite As New Global.BACRM.BusinessLogic.ShioppingCart.Sites
                    Dim dtStyle As DataTable
                    objSite.SiteID = 0
                    objSite.DomainID = Session("DomainID")
                    objSite.StyleType = 2
                    dtStyle = objSite.GetStyles()
                    'Add style which are physically available 

                    For i As Integer = 0 To dtStyle.Rows.Count - 1
                        Dim row As DataRow = dtStyle.Rows(i)
                        If Not System.IO.File.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & "PortalCss\" & row("StyleFileName")) Then
                            dtStyle.Rows(i).Delete()
                        End If
                    Next
                    dtStyle.AcceptChanges()
                    Session("PortalStyle") = dtStyle
                End If

                Dim objAdmin As New CAdmin
                Dim dtTab As DataTable
                objAdmin.UserCntID = Session("ContactId")
                objAdmin.DivisionID = Session("DivId")
                objAdmin.DomainID = Session("DomainId")
                dtTab = objAdmin.GetDefaultTabName()
                If dtTab.Rows.Count > 0 Then Session("DefaultTab") = dtTab
                Response.Redirect("CommonPages/frmDashboard.aspx", False)
            ElseIf dttable.Rows(0).Item(0) > 1 Then
                Session("CompID") = Nothing
                Session("UserContactID") = Nothing
                Session("DivId") = Nothing
                Session("GroupId") = Nothing
                litMessage.Text = "Found Multiple Email Address in the system. Please Contact Administrator !"
            Else
                Session("CompID") = Nothing
                Session("UserContactID") = Nothing
                Session("DivId") = Nothing
                Session("GroupId") = Nothing
                litMessage.Text = "You have failed to enter registered email address. Try again, or contact your representative for assistance !"
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub FillDomain()
        Try
            Dim objUserAccess As New UserAccess
            Dim dtDomain As DataTable
            objUserAccess.Email = txtEmaillAdd.Text.Trim()
            objUserAccess.Password = txtPassword.Text.Trim()
            dtDomain = objUserAccess.ExtranetLoginDomains
            If dtDomain IsNot Nothing AndAlso dtDomain.Rows.Count > 0 Then
                ddlDomain.DataValueField = "numDomainID"
                ddlDomain.DataTextField = "vcDomainName"
                ddlDomain.DataSource = dtDomain
                ddlDomain.DataBind()
                If dtDomain.Rows.Count = 1 Then
                    trDomain.Visible = False
                Else
                    trDomain.Visible = True
                End If

            Else
                trDomain.Visible = False
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSendPwd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSendPwd.Click
        Try
            Dim objUserAccess As New UserAccess
            objUserAccess.Email = txtEmaillAdd.Text.Trim
            objUserAccess.boolPortal = True
            objUserAccess.DomainID = CCommon.ToLong(Session("DomainID"))
            objUserAccess.SendPassoword()
            If objUserAccess.NoOfRows = 1 Then
                Dim objSendMail As New Email
                objSendMail.SendSystemEmail("Your Password !", "Hi <br><br><br>   your Password is " & objUserAccess.Password, "", ConfigurationManager.AppSettings("FromAddress"), txtEmaillAdd.Text.Trim)
                litMessage.Text = "Your Password has been sent to your registered email address !"
            ElseIf objUserAccess.NoOfRows = 0 Then
                litMessage.Text = "Check Your Email Id and try Again !"
            ElseIf objUserAccess.NoOfRows > 1 Then
                litMessage.Text = "Found Multiple Records.Contact Administrator !"
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Try
            trLogin.Visible = True
            trForgotPwd.Visible = False
            trPassword.Visible = True
            trForgotPwdLink.Visible = True
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class