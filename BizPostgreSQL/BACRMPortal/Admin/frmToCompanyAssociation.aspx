<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmToCompanyAssociation.aspx.vb" Inherits="BACRMPortal.frmToCompanyAssociation"%>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
		<title>Company Association</title>
		<script language="javascript">
		function Close()
		{
			window.close()
			return false;
		}
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<br>
			<table width="100%">
				<tr>
					<td align="right">
						<asp:Button ID="btnClose" Runat="server" Text="Close" Width="50" CssClass="button"></asp:Button>
					</td>
				</tr>
				<tr>
					<td>
					<table width="100%" class="aspTable" style=" height:150px"><tr><td valign="top">
						<asp:datagrid id="dgAssociation" runat="server" Width="100%" CssClass="dg" BorderColor="white"
							AutoGenerateColumns="False" AllowSorting="True">
							<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							<ItemStyle CssClass="is"></ItemStyle>
							<HeaderStyle CssClass="hs"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="numAssociationID" Visible="false" HeaderText="Was Associated From"></asp:BoundColumn>
								<asp:BoundColumn DataField="Company" HeaderText="Was Associated From"></asp:BoundColumn>
								<asp:BoundColumn DataField="bitParentOrg" HeaderText="Parent/ Child Org. ?"></asp:BoundColumn>
								<asp:BoundColumn DataField="vcData" HeaderText="As Its"></asp:BoundColumn>
							</Columns>
						</asp:datagrid>
						</td></tr></table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
