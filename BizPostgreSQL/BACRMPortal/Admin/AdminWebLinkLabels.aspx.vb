Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Common
Partial Public Class AdminWebLinkLabels : Inherits BACRMPage

    Dim lngDivID As Long

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngDivID = GetQueryStringVal( "rtyWR")
            If lngDivID = 0 Then lngDivID = Session("DivID")
            If IsPostBack = False Then sb_GetLabels()
            btnClose.Attributes.Add("onclick", "return Close()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub sb_GetLabels()
        Try
            Dim objLeads As New CLeads
            Dim dtWebLinks As DataTable
            objLeads.DivisionID = lngDivID
            dtWebLinks = objLeads.WebLinks
            If dtWebLinks.Rows.Count > 0 Then
                If IsDBNull(dtWebLinks.Rows(0).Item("vcWebLink1Label")) = False Then txtLink1Label.Text = dtWebLinks.Rows(0).Item("vcWebLink1Label")
                If IsDBNull(dtWebLinks.Rows(0).Item("vcWebLink2Label")) = False Then txtLink2Label.Text = dtWebLinks.Rows(0).Item("vcWebLink2Label")
                If IsDBNull(dtWebLinks.Rows(0).Item("vcWebLink3Label")) = False Then txtLink3Label.Text = dtWebLinks.Rows(0).Item("vcWebLink3Label")
                If IsDBNull(dtWebLinks.Rows(0).Item("vcWebLink4Label")) = False Then txtLink4Label.Text = dtWebLinks.Rows(0).Item("vcWebLink4Label")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            Dim objLeads As New CLeads
            objLeads.DivisionID = lngDivID
            objLeads.WebLabel1 = txtLink1Label.Text
            objLeads.WebLabel2 = txtLink2Label.Text
            objLeads.WebLabel3 = txtLink3Label.Text
            objLeads.WebLabel4 = txtLink4Label.Text
            objLeads.UpdateWebLinks()
            Response.Write("<script>opener.location.reload(true); self.close();</script>")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class