Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Common
Partial Class frmFromCompanyAssociation
    Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Dim intDivId As Integer
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            intDivId = Session("DivId")
            If IsPostBack = False Then bindGrid()
            btnClose.Attributes.Add("onclick", "return Close()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub bindGrid()
        Try
            Dim objProspects As New CProspects
            Dim dtAssociationFrom As DataTable
            objProspects.DivisionID = intDivId
            'objProspects.DivisionIdList = CStr(intDivId)
            objProspects.bitAssociatedTo = 0
            dtAssociationFrom = objProspects.GetAssociationTo().Tables(0)
            dgAssociation.DataSource = dtAssociationFrom
            dgAssociation.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class
