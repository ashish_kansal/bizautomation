<%@ Register TagPrefix="menu1" TagName="PartnerPoint" src="../Common/PartnerPoint.ascx" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" src="../include/calandar.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="newaction.aspx.vb" Inherits="BACRMPortal.newaction" validateRequest="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head  runat="server">		
        <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
          <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
		<title>Action Items </title>
		<script language="javascript"> 
		function openTime(a,b,c,d)
		{
		
		window.open("../cases/frmCasetime.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&caseId="+a+"&CntId="+b+"&DivId="+c+"&CaseTimeId="+document.Form1.txtCaseTimeID.value,'','toolbar=no,titlebar=no,left=500, top=300,width=650,height=300,scrollbars=yes,resizable=yes');
		return false;
		}
		function Close()
		{
		    window.close()
		    return false;
		}
		function openExpense(a,b,c,d)
		{
		
		window.open("../cases/frmCaseExpense.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&caseId="+a+"&CntId="+b+"&DivId="+c+"&CaseExpId="+document.Form1.txtCaseExpenseID.value,'','toolbar=no,titlebar=no,left=500, top=300,width=650,height=300,scrollbars=yes,resizable=yes');
		return false;
		}
		
		function Save()
		{
			if (document.Form1.ddlCompanyName.selectedIndex==-1) 
			{
				alert("Select Company")
				document.Form1.ddlCompanyName.focus();
				return false;
			}
			if (document.Form1.ddlCompanyName.value==0)
			{
				alert("Select Company")
				document.Form1.ddlCompanyName.focus();
				return false;
			}
				if (document.Form1.ddlTaskContact.selectedIndex==-1)
			{
				alert("Select Contact")
				document.Form1.ddlTaskContact.focus();
				return false;
			}
			if (document.Form1.ddlTaskContact.value==0)
			{
				alert("Select Contact")
				document.Form1.ddlTaskContact.focus();
				return false;
			}
			if (document.Form1.ddlTaskType.value==0)
			{
				alert("Select Action Item Type")
				document.Form1.ddlTaskType.focus();
				return false;
			}
		}
		function openEmpAvailability()
		{
			var strTime,strStart,strEnd;
			if (document.Form1.chkAM.checked==true)
			{
				strStart='AM'
			}
			else
			{
				strStart='PM'
			}
			if (document.Form1.chkEndAM.checked==true)
			{
				strEnd='AM'
			}
			else
			{
				strEnd='PM'
			}
			strTime=document.Form1.ddltime.options[document.Form1.ddltime.selectedIndex].text+' ' + strStart+'-'+document.Form1.ddlEndTime.options[document.Form1.ddlEndTime.selectedIndex].text +' ' +strEnd;
			window.open("../ActionItems/frmEmpAvailability.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Date="+document.Form1.cal_txtDate.value+'&Time='+strTime,'','toolbar=no,titlebar=no,top=0,left=100,width=850,height=500,scrollbars=yes,resizable=yes');
			return false;
		}
		function AssignTo(a,b)
		{
			document.Form1.ddlAssignTo.value=a;
			document.Form1.cal_txtDate.value=b;
			return false;
		}
		function AssignCaseTimeId(a)
		{
			document.Form1.txtCaseTimeID.value=a;
//			alert("break")
//			alert(document.Form1.txtCaseTimeID.value)
			//document.Form1.cal_txtDate.value=b;
			return false;
		}
	    function AssignCaseExpenseId(a)
		{
			document.Form1.txtCaseExpenseID.value=a;
			return false;
		}
		
		function OpenActionItemWindow()
		{
		 var objEditActionItem = document.getElementById('editActionItem')
		 var urlQString = objEditActionItem.href
		 var start = urlQString.lastIndexOf("?") + 1
		 urlQString =  urlQString.substring( start )
		 window.open('../ActionItems/frmActionItemTemplate.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&' + urlQString  ,'','toolbar=no,titlebar=no,top=100,left=250,width=700,height=350,scrollbars=yes,resizable=yes')
		}
		function Loaddata()
		{
		  document.Form1.btnLoadData.click()
		  return false;
		}
		</script>
	</HEAD>
	<BODY>
		
		<form id="Form1" name="Form1" method="post" runat="server">
				<menu1:PartnerPoint id="PartnerPoint1" runat="server"></menu1:PartnerPoint>
		<asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
   
			<table id="table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td vAlign="bottom" width="215">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;New Action Item &nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td align="center"><asp:label id="lblError" runat="server" Font-Names="Verdana" Font-Size="9px" Height="2px" ForeColor="Red"
							Visible="False"></asp:label></td>
				    <td align="right"><asp:button id="btnSave" Text="Save" CssClass="button" Runat="server"></asp:button>
					<asp:button id="btnSaveClose" Text="Save &amp; Close" CssClass="button" Runat="server"></asp:button>
						<asp:button id="btnCancel" Text="Cancel" CssClass="button" Runat="server"></asp:button></td>
				</tr>
			</table>
			<asp:table id="table5" Runat="server" GridLines="None" BorderColor="black" Width="100%" BorderWidth="1" CssClass="aspTable">
				<asp:tableRow>
					<asp:tableCell VerticalAlign="Top">
						<br>
						<table id="table1"  width="100%">
					
							<tr>
							      <td rowspan="30" valign="top" >
									   <img src="../images/ActionItem-32.gif" />
								</td>
								<td class="normal1" align="right">Customer<FONT color="red">*</FONT>
								</td>
								<td>
									<asp:textbox id="txtCompName" Runat="server" cssclass="signup" width="100"></asp:textbox>&nbsp;
									<asp:button id="btnGo" Text="Go" CssClass="button" Runat="server"></asp:button>&nbsp;
									<asp:dropdownlist id="ddlCompanyName" CssClass="signup" Runat="server" Width="180" AutoPostBack="true">
										<asp:ListItem Value="0">--Select One--</asp:ListItem>
									</asp:dropdownlist></td>
								  <td class="normal1" align="right">Action Item Templates</td>
							    <td>
							       
							                <asp:DropDownList ID="listActionItemTemplate" CssClass="signup" Runat="server" Width="180" AutoPostBack="true">
							                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
							                </asp:DropDownList>&nbsp;
							    
							                <asp:HyperLink ID='editActionItem' runat="server" Font-Size=8  NavigateUrl="../Admin/newaction.aspx?Mode=Edit" Text="Edit" ForeColor="#180073" ></asp:HyperLink>
							     
							    </td>
							</tr>
							<tr>
								<td class="normal1" align="right">Contact<FONT color="red">*</FONT></td>
								<td>
									<asp:dropdownlist id="ddlTaskContact" CssClass="signup" Runat="server" Width="180" AutoPostBack="true">
										<asp:ListItem Value="0">--Select One--</asp:ListItem>
									</asp:dropdownlist></td>
								<td class="normal1" align="right">Priority
								</td>
								<td>
									<asp:dropdownlist id="ddlStatus" CssClass="signup" Runat="server" Width="180"></asp:dropdownlist></td>
							</tr>
							<tr>
								<td class="text" align="right" width="125" height="2">Due date</td>
								<td class="text">
									<BizCalendar:Calendar ID="cal" runat="server" />
								</td>
								<td class="normal1" align="right">Type</td> 
								<td>
									<asp:dropdownlist id="ddlTaskType" CssClass="signup" Runat="server" Width="180" AutoPostBack="true">
									
									</asp:dropdownlist></td>
							</tr>
							<tr>
								<td class="normal1" align="right">
								Activity
								<td>
									<asp:dropdownlist id="ddlActivity" CssClass="signup" Runat="server" Width="180"></asp:dropdownlist></td>
								<td class="text" align="right">Assign To</td>
								<td class="normal1">
									<asp:dropdownlist id="ddlAssignTo" CssClass="signup" Runat="server" Width="180"></asp:dropdownlist>
									&nbsp;&nbsp;&nbsp;
									<asp:HyperLink ID="hplEmpAvaliability" Runat="server" NavigateUrl="#">
										<font color="#180073">Availability</font></asp:HyperLink>
								</td>
							</tr>
							<tr>
								<td class="normal1" align="right">Reminder<img src="../images/Bell-16.gif" />
								</td>
								<td>
									<asp:dropdownlist id="ddlRemainder" CssClass="signup" Runat="server" Width="180">
										<asp:ListItem Value="0">--Select One--</asp:ListItem>
										<asp:ListItem Value="5">5 minutes</asp:ListItem>
										<asp:ListItem Value="15">15 minutes</asp:ListItem>
										<asp:ListItem Value="30">30 Minutes</asp:ListItem>
										<asp:ListItem Value="60">1 Hour</asp:ListItem>
										<asp:ListItem Value="240">4 Hour</asp:ListItem>
										<asp:ListItem Value="480">8 Hour</asp:ListItem>
										<asp:ListItem Value="1440">24 Hour</asp:ListItem>
									</asp:dropdownlist>
									<asp:label id="lblre" CssClass="text" Runat="server">&nbsp;(Set Before Event)
									</asp:label></td>
								<td class="normal1" align="right">Snooze
								</td>
								<td>
									<asp:dropdownlist id="ddlSnooze" CssClass="signup" Runat="server" Width="180">
									    <asp:ListItem Value="0">--Select One--</asp:ListItem>
										<asp:ListItem Value="5">5 minutes</asp:ListItem>
										<asp:ListItem Value="15">15 minutes</asp:ListItem>
										<asp:ListItem Value="30">30 Minutes</asp:ListItem>
										<asp:ListItem Value="60">1 Hour</asp:ListItem>
										<asp:ListItem Value="240">4 Hour</asp:ListItem>
										<asp:ListItem Value="480">8 Hour</asp:ListItem>
										<asp:ListItem Value="1440">24 Hour</asp:ListItem>
									</asp:dropdownlist>
									<asp:label id="Label1" CssClass="text" Runat="server">&nbsp;(Set After Event)
									</asp:label></td>
							</tr>
							<tr>
								<td class="text" align="right">Start Time</td>
								<td class="text"><img src="../images/Clock-16.gif" />
									<asp:dropdownlist id="ddltime" CssClass="signup" Runat="server" Width="55">
										<asp:ListItem Selected="False" Value="23">12:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="24">12:30</asp:ListItem>
										<asp:ListItem Selected="False" Value="1">1:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="2">1:30</asp:ListItem>
										<asp:ListItem Selected="False" Value="3">2:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="4">2:30</asp:ListItem>
										<asp:ListItem Selected="False" Value="5">3:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="6">3:30</asp:ListItem>
										<asp:ListItem Selected="False" Value="7">4:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="8">4:30</asp:ListItem>
										<asp:ListItem Selected="False" Value="9">5:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="10">5:30</asp:ListItem>
										<asp:ListItem Selected="False" Value="11">6:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="12">6:30</asp:ListItem>
										<asp:ListItem Selected="False" Value="13">7:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="14">7:30</asp:ListItem>
										<asp:ListItem Selected="true" Value="15">8:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="16">8:30</asp:ListItem>
										<asp:ListItem Selected="False" Value="17">9:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="18">9:30</asp:ListItem>
										<asp:ListItem Selected="False" Value="19">10:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="20">10:30</asp:ListItem>
										<asp:ListItem Selected="False" Value="21">11:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="22">11:30</asp:ListItem>
									</asp:dropdownlist>&nbsp;AM <input id="chkAM" type="radio" CHECKED value="0" name="AM" runat="server">
									PM <input id="chkPM" type="radio" value="1" name="AM" runat="server">
								</td>
											<td class="text" align="right">
								        Email Alert
								</td>
								<td><asp:CheckBox ID="chkAlert" runat="server"  /><asp:dropdownlist id="ddlEmailTemplate" CssClass="signup" Runat="server" Width="180" ></asp:dropdownlist></td>
								</tr>
								<tr>
								<td class="text" align="right">End Time</td>
								<td class="text"><img src="../images/Clock-16.gif" />
									<asp:dropdownlist id="ddlEndTime" CssClass="signup" Runat="server" Width="55">
										<asp:ListItem Selected="False" Value="23">12:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="24">12:30</asp:ListItem>
										<asp:ListItem Selected="False" Value="1">1:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="2">1:30</asp:ListItem>
										<asp:ListItem Selected="False" Value="3">2:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="4">2:30</asp:ListItem>
										<asp:ListItem Selected="False" Value="5">3:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="6">3:30</asp:ListItem>
										<asp:ListItem Selected="False" Value="7">4:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="8">4:30</asp:ListItem>
										<asp:ListItem Selected="False" Value="9">5:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="10">5:30</asp:ListItem>
										<asp:ListItem Selected="False" Value="11">6:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="12">6:30</asp:ListItem>
										<asp:ListItem Selected="False" Value="13">7:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="14">7:30</asp:ListItem>
										<asp:ListItem Selected="true" Value="15">8:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="16">8:30</asp:ListItem>
										<asp:ListItem Selected="False" Value="17">9:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="18">9:30</asp:ListItem>
										<asp:ListItem Selected="False" Value="19">10:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="20">10:30</asp:ListItem>
										<asp:ListItem Selected="False" Value="21">11:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="22">11:30</asp:ListItem>
									</asp:dropdownlist>&nbsp;AM <input id="chkEndAM" type="radio" CHECKED value="0" name="EndAM" runat="server">
									PM <input id="chkEndPM" type="radio" value="1" name="EndAM" runat="server">
								</td>
								<td  align="right">
								
								
								</td>
								<td nowrap>
								<table>
								   <tr>
								   <td>
								       
								   </td>
								    <td class="normal1">
								        <asp:textbox id="txtHours" Runat="server" cssclass="signup" width="30"></asp:textbox>&nbsp;hours&nbsp;
								    </td>
								    <td class="normal1">
								        <asp:radiobuttonlist id="rdlEmailTemplate"  Runat="server" Repeatdirection="Horizontal"><asp:ListItem Value="1" Selected="true" Text="after due date"></asp:ListItem><asp:ListItem Value="0" Text="before due date"></asp:ListItem></asp:radiobuttonlist> 
								    </td>
								   </tr>
								</table>
								       
								</td>
								</tr>												
							
							<tr>
								<td class="normal1" vAlign="top" align="right">Comments</td>
								<td vAlign="top" colSpan="3">
									<asp:textbox id="txtComments" runat="server" CssClass="signup" Width="660" Height="180"  TextMode="MultiLine"
										MaxLength="100"></asp:textbox></td>
										
							</tr>
							<tr>
								<td></td>
								<td>
									<asp:checkbox id="chkOutlook" runat="server" Text="Add to Calendar" CssClass="text"></asp:checkbox></td>
								<td></td>
								<td>
									<asp:checkbox id="chkPopupRemainder" runat="server" Text="Have Popup Remind Assignee Prior to Event"
										CssClass="text"></asp:checkbox></td>
							</tr>
							<tr>
								<td class="text" id="tdEndTime" align="right" colSpan="3" runat="server"></td>
							</tr>
						</table>
						<br>
					</asp:tableCell>
				</asp:tableRow>
			</asp:table>
			<asp:Button ID="btnLoadData" runat="server"  style="display:none" />
			<asp:TextBox runat="server" ID="txtCaseTimeID" style="display:none" Text="0"></asp:TextBox>  
			<asp:TextBox runat="server" ID="txtCaseExpenseID" style="display:none" Text="0"></asp:TextBox>  
		</form>
		
		
	</BODY>
</HTML>
