<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AdminWebLinkLabels.aspx.vb" Inherits="BACRMPortal.AdminWebLinkLabels" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1"  runat="server">
        <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
		<title>Web Link Labels</title>
		<script language="javascript" type="text/javascript" >
		function Close()
		{
			window.close()
			return false;
		}
		</script>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
		<br>
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td valign="bottom">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;Web Link 
									Labels&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td align="right">
						<asp:Button ID="btnSaveClose" Runat="server" CssClass="button" Text="Save &amp; Close"></asp:Button>
						<asp:Button ID="btnClose" Runat="server" CssClass="button" Text="Close"></asp:Button>
					</td>
				</tr>
			</table>
			<asp:table id="tblProducts" BorderWidth="1" CellPadding="0" CellSpacing="0" Runat="server" CssClass="aspTable"
				Width="100%" BorderColor="black" GridLines="None">
				<asp:TableRow>
					<asp:TableCell>
						<table border="0" cellpadding="1" cellspacing="1" width="100%">
							<tr>
								<td class="normal1" align="right">
									Link 1 Label
								</td>
								<td>
									<asp:TextBox id="txtLink1Label" MaxLength="100" Runat="server" CssClass="signup"></asp:TextBox>
								</td>
								<td class="normal1" align="right">
									Link 2 Label
								</td>
								<td>
									<asp:TextBox id="txtLink2Label" MaxLength="100" Runat="server" CssClass="signup"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td class="normal1" align="right">
									Link 3 Label
								</td>
								<td>
									<asp:TextBox id="txtLink3Label" MaxLength="100" Runat="server" CssClass="signup"></asp:TextBox>
								</td>
								<td class="normal1" align="right">
									Link 4 Label
								</td>
								<td>
									<asp:TextBox id="txtLink4Label" MaxLength="100" Runat="server" CssClass="signup"></asp:TextBox>
								</td>
							</tr>
						</table>
					</asp:TableCell>
				</asp:TableRow>
			</asp:table>
		</form>
	</body>
</HTML>

