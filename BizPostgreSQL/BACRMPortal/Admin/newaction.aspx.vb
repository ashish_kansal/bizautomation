Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Outlook

Public Class newaction : Inherits BACRMPage

    Dim objCommon As CCommon
    Dim objCampaign As Campaign
    Dim objContacts As CContacts
    Dim objActionItem As ActionItem
    Dim objOutLook As COutlook
    Dim objAlerts As CAlerts
    Dim numcommId As Long = 0
    Dim m_aryRightsForActItem() As Integer

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            objCommon = New CCommon
            m_aryRightsForActItem = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmActItemList.aspx", Session("UserContactID"), 26, 1)

            If m_aryRightsForActItem(RIGHTSTYPE.ADD) = 0 Then btnSave.Visible = False

            ' bcz user may delete some action items using Pop window
            ' so we need to load all the items once again
            If Not Request.QueryString.Get("doPostback") Is Nothing Then
                Dim url As String
                url = Request.Url.ToString
                url = url.Replace("&doPostback=yes", "").TrimEnd()
                'If url.IndexOf("&selectedAction=") >= 0 Then ' if added in previous request then removE IT
                '    Dim currentValue As String = Request.QueryString.Get("selectedAction")
                '    url = url.Replace("&selectedAction=" & currentValue, "").TrimEnd()
                'End If
                'url = url + "&selectedAction=" & listActionItemTemplate.SelectedIndex.ToString
                Response.Redirect(url)
            End If
            If Not IsPostBack Then
                If GetQueryStringVal( "Popup") = "True" Then
                    btnCancel.Attributes.Add("onclick", "return Close()")
                    PartnerPoint1.Visible = False
                End If
                'objCommon = New CCommon
                objCommon.sb_FillComboFromDBwithSel(ddlTaskType, 73, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlStatus, 447, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlActivity, 32, Session("DomainID"))
                sb_FillEmpList()
                If objCampaign Is Nothing Then objCampaign = New Campaign

                objCampaign.DomainID = Session("DomainID")
                ddlEmailTemplate.DataSource = objCampaign.GetEmailTemplates
                ddlEmailTemplate.DataTextField = "VcDocName"
                ddlEmailTemplate.DataValueField = "numGenericDocID"
                ddlEmailTemplate.DataBind()
                ddlEmailTemplate.Items.Insert(0, "--Select One--")
                ddlEmailTemplate.Items.FindByText("--Select One--").Value = 0
            End If
            Dim strDate As Date = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
            If ddlTaskType.SelectedItem.Value = "973" Then
                ddlAssignTo.Enabled = False
                ddltime.Enabled = False
                chkAM.Disabled = True
                chkPM.Disabled = True
                ddlEndTime.Enabled = False
                chkEndAM.Disabled = True
                chkEndPM.Disabled = True
            ElseIf ddlTaskType.SelectedItem.Value = "974" Then
                ddltime.Enabled = False
                chkAM.Disabled = True
                chkPM.Disabled = True
                ddlEndTime.Enabled = False
                chkEndAM.Disabled = True
                chkEndPM.Disabled = True
                ddlAssignTo.Enabled = True
            Else
                ddlAssignTo.Enabled = True
                ddltime.Enabled = True
                chkAM.Disabled = False
                chkPM.Disabled = False
                ddlEndTime.Enabled = True
                chkEndAM.Disabled = False
                chkEndPM.Disabled = False

            End If
            If GetQueryStringVal( "frm") = "Cases" Then
                btnSave.Visible = True
            Else : btnSave.Visible = False
            End If

            If Not IsPostBack Then

                ' attach java script evene
                editActionItem.Attributes.Add("onClick", "OpenActionItemWindow();return false;")
                editActionItem.NavigateUrl = "../Admin/newaction.aspx?Mode=Edit" & "&ID=-1"
                If GetQueryStringVal( "frm") = "Cases" Then
                    Dim lst As New ListItem
                    lst.Text = "Task"
                    lst.Value = "972"
                    ddlTaskType.Items.Clear()
                    ddlTaskType.Items.Add(lst)
                    'ddlTaskType.Items.FindByValue("971").Selected = True
                    ddlTaskType.Enabled = False
                End If

                cal.SelectedDate = strDate
                If Len(Trim(GetQueryStringVal( "CntID"))) > 0 Then
                    If objCommon Is Nothing Then objCommon = New CCommon

                    objCommon.ContactID = GetQueryStringVal( "CntID")
                    objCommon.charModule = "C"
                    objCommon.GetCompanySpecificValues1()
                    sb_LoadSpecificCompany(objCommon.DivisionID)
                    If Not ddlCompanyName.Items.FindByValue(objCommon.DivisionID) Is Nothing Then
                        ddlCompanyName.Items.FindByValue(objCommon.DivisionID).Selected = True
                        '  btnTime.Attributes.Add("onclick", "openTime('" & GetQueryStringVal( "CaseId") & "','" & GetQueryStringVal( "CntId") & "','" & ddlCompanyName.SelectedValue & "','" & txtCaseTimeID.Text & "')")
                        ' btnExpense.Attributes.Add("onclick", "openExpense('" & GetQueryStringVal( "CaseId") & "','" & GetQueryStringVal( "CntId") & "','" & ddlCompanyName.SelectedValue & "','" & txtCaseExpenseID.Text & "')")
                    End If

                    sb_LoadContacts()
                    If Not ddlTaskContact.Items.FindByValue(objCommon.ContactID) Is Nothing Then
                        ddlTaskContact.Items.FindByValue(objCommon.ContactID).Selected = True
                    End If
                End If
                If Request.QueryString.Get("Mode") Is Nothing Then LoadActionItemTemplateData()
            End If

            If GetQueryStringVal( "frm") = "outlook" Then
                Dim objOutlook As New COutlook
                Dim dttable As DataTable
                objOutlook.numEmailHstrID = GetQueryStringVal( "EmailHstrId")
                objOutlook.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dttable = objOutlook.getMail()
                If dttable.Rows.Count > 0 Then txtComments.Text = dttable.Rows(0).Item("vcBodyText")
                PartnerPoint1.Visible = False
            End If
            btnSaveClose.Attributes.Add("onclick", "return Save()")
            hplEmpAvaliability.Attributes.Add("onclick", "return openEmpAvailability();")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub Page_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.PreRender

    End Sub

    '//Adding EmployList in dropdown list

    Private Sub sb_FillEmpList()
        Try
            If objContacts Is Nothing Then objContacts = New CContacts

            Dim dtEmployeeList As New DataTable
            objContacts.DomainID = Session("DomainID")
            dtEmployeeList = objContacts.EmployeeList
            ddlAssignTo.DataSource = dtEmployeeList
            ddlAssignTo.DataTextField = "vcUserName"
            ddlAssignTo.DataValueField = "numContactID"
            ddlAssignTo.DataBind()
            If Not ddlAssignTo.Items.FindByValue(Session("UserContactID")) Is Nothing Then
                ddlAssignTo.ClearSelection()
                ddlAssignTo.Items.FindByValue(Session("UserContactID")).Text = "My Self"
                ddlAssignTo.Items.FindByValue(Session("UserContactID")).Selected = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub sb_LoadCompany()
        Try
            ddlCompanyName.Items.Clear()
            If objCommon Is Nothing Then objCommon = New CCommon
            objCommon.DomainID = Session("DomainID")
            objCommon.Filter = Trim(txtCompName.Text) & "%"
            ddlCompanyName.DataSource = objCommon.PopulateOrganization.Tables(0).DefaultView
            ddlCompanyName.DataTextField = "vcCompanyname"
            ddlCompanyName.DataValueField = "numDivisionID"
            ddlCompanyName.DataBind()
            ddlCompanyName.Items.Insert(0, New ListItem("--Select One--", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub sb_LoadContacts()
        Try
            ddlTaskContact.Items.Clear()
            Dim objOpportunities As New COpportunities
            objOpportunities.DivisionID = ddlCompanyName.SelectedItem.Value
            ddlTaskContact.DataSource = objOpportunities.ListContact().Tables(0).DefaultView()
            ddlTaskContact.DataTextField = "Name"
            ddlTaskContact.DataValueField = "numcontactId"
            ddlTaskContact.DataBind()
            ddlTaskContact.Items.Insert(0, New ListItem("--Select One--", "0"))
            If ddlTaskContact.Items.Count = 2 Then
                ddlTaskContact.Items(1).Selected = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function sb_SaveActionItem() As Boolean
        Dim intTaskFlg As Integer   'Task flag for identifying whether task or Communication
        Dim lngCommId As Long
        Dim lngActivityId As Long = 0
        Try
            sb_SaveActionItem = True
            Dim strStartTime, strEndTime, strDate As String

            strDate = cal.SelectedDate
            strStartTime = strDate.Trim & " " & ddltime.SelectedItem.Text.Trim & ":00" & IIf(chkAM.Checked = True, " AM", " PM")
            strEndTime = strDate.Trim & " " & ddlEndTime.SelectedItem.Text.Trim & ":00" & IIf(chkEndAM.Checked = True, " AM", " PM")

            'Checking whether the Type is Task or Communication or Notes
            'intTaskFlg = IIf(ddlTaskType.SelectedItem.Text = "Task", 0, 1)
            'If ddlTaskType.SelectedItem.Text = "Communication" Then
            '    intTaskFlg = 1
            'ElseIf ddlTaskType.SelectedItem.Text = "Task" Then
            '    intTaskFlg = 3
            'ElseIf ddlTaskType.SelectedItem.Text = "Notes" Then
            '    intTaskFlg = 4
            'ElseIf ddlTaskType.SelectedItem.Text = "Follow-up Anytime" Then
            '    intTaskFlg = 5
            'ElseIf ddlTaskType.SelectedItem.Text = "" Then
            '    intTaskFlg = 4
            'End If

            Dim strCalendar As String
            If ddlTaskType.SelectedValue <> 973 And ddlTaskType.SelectedValue <> 974 Then
                Dim strSplitStartTimeDate As Date                                               'To store the Date and Time for the start of the Action Item
                Dim strSplitEndTimeDate As Date         'To store the Date and Time for the end of the Action Item
                strSplitStartTimeDate = CType(strStartTime, DateTime).AddMinutes(Session("ClientMachineUTCTimeOffset"))        'Convert the Start Date and Time to UT
                strSplitEndTimeDate = CType(strEndTime, DateTime).AddMinutes(Session("ClientMachineUTCTimeOffset"))           'Convert the End Date and Time to UTC
                Dim strDesc As String = "Company : " & ddlCompanyName.SelectedItem.Text & "<BR>" & "Contact : " & ddlTaskContact.SelectedItem.Text & "<BR>" & "AssignedTo : " & ddlAssignTo.SelectedItem.Text & "<BR>" & txtComments.Text

                If ddlAssignTo.SelectedItem.Text = "My Self" Then
                    If chkOutlook.Checked = True Then
                        If objOutLook Is Nothing Then objOutLook = New COutlook
                        objOutLook.UserCntID = Session("UserContactId")
                        objOutLook.DomainId = Session("DomainId")
                        Dim dtTable As DataTable
                        dtTable = objOutLook.GetResourceId
                        If dtTable.Rows.Count > 0 Then
                            objOutLook.AllDayEvent = False
                            objOutLook.ActivityDescription = strDesc
                            objOutLook.Location = ""
                            objOutLook.Subject = ddlTaskType.SelectedItem.Text
                            objOutLook.Duration = DateDiff(DateInterval.Second, strSplitStartTimeDate, strSplitEndTimeDate)
                            objOutLook.StartDateTimeUtc = strSplitStartTimeDate
                            objOutLook.EnableReminder = True
                            objOutLook.ReminderInterval = 900
                            objOutLook.ShowTimeAs = 3
                            objOutLook.Importance = 0
                            objOutLook.RecurrenceKey = -999
                            objOutLook.ResourceName = dtTable.Rows(0).Item("ResourceId")
                            lngActivityId = objOutLook.AddActivity()
                        End If
                        'strCalendar = clsProspects.fn_InsertOWAActionItem(Session("SiteType"), strSplitStartTimeDate, strSplitEndTimeDate, Session("ServerName"), Session("UserEmail"), txtComments.Text)
                    End If
                Else
                    If chkOutlook.Checked = True Then
                        If objOutLook Is Nothing Then objOutLook = New COutlook
                        objOutLook.UserCntID = ddlAssignTo.SelectedItem.Value
                        objOutLook.DomainId = Session("DomainId")
                        Dim dtTable As DataTable
                        dtTable = objOutLook.GetResourceId
                        If dtTable.Rows.Count > 0 Then
                            objOutLook.AllDayEvent = False
                            objOutLook.ActivityDescription = strDesc
                            objOutLook.Location = ""
                            objOutLook.Subject = ddlTaskType.SelectedItem.Text
                            objOutLook.Duration = DateDiff(DateInterval.Second, strSplitStartTimeDate, strSplitEndTimeDate)
                            objOutLook.StartDateTimeUtc = strSplitStartTimeDate
                            objOutLook.EnableReminder = True
                            objOutLook.ReminderInterval = 900
                            objOutLook.ShowTimeAs = 3
                            objOutLook.Importance = 2
                            objOutLook.RecurrenceKey = -999
                            objOutLook.ResourceName = dtTable.Rows(0).Item("ResourceId")
                            lngActivityId = objOutLook.AddActivity()
                        End If
                        '  Dim objContacts As New CContacts
                        ' strCalendar = clsProspects.fn_InsertOWAActionItem(Session("SiteType"), strSplitStartTimeDate, strSplitEndTimeDate, Session("ServerName"), IIf(ddlAssignTo.SelectedItem.Text = "My Self", Session("UserEmail"), objContacts.EmployeeEmail(ddlAssignTo.SelectedItem.Value)), txtComments.Text)
                    End If
                End If
            End If
            Dim intAssignTo As Integer

            If ddlAssignTo.Enabled = True Then intAssignTo = ddlAssignTo.SelectedItem.Value
            Dim chkSnoozeStatus, chkRemainderStatus As Integer
            If chkPopupRemainder.Checked = True Then
                chkRemainderStatus = 1
            Else : chkRemainderStatus = 0
            End If
            If ddlSnooze.SelectedItem.Value > 0 Then
                chkSnoozeStatus = 1
            Else : chkSnoozeStatus = 0
            End If
            If objActionItem Is Nothing Then objActionItem = New ActionItem

            With objActionItem
                .CommID = 0
                .Task = ddlTaskType.SelectedValue

                .ContactID = ddlTaskContact.SelectedItem.Value
                .DivisionID = ddlCompanyName.SelectedItem.Value
                .Details = txtComments.Text
                .AssignedTo = intAssignTo
                .UserCntID = Session("UserContactID")
                .DomainID = Session("DomainID")
                If ddlTaskType.SelectedValue = 973 Then
                    .BitClosed = 1
                Else : .BitClosed = 0
                End If
                .CalendarName = strCalendar
                If ddlTaskType.SelectedValue = 973 Or ddlTaskType.SelectedValue = 974 Then
                    .StartTime = cal.SelectedDate
                    .EndTime = DateAdd(DateInterval.Day, 1, .StartTime)
                Else
                    .StartTime = strStartTime
                    .EndTime = strEndTime
                End If
                .Activity = ddlActivity.SelectedItem.Value
                .Status = ddlStatus.SelectedItem.Value
                .Snooze = ddlSnooze.SelectedItem.Value
                .SnoozeStatus = chkSnoozeStatus
                .Remainder = ddlRemainder.SelectedItem.Value
                .RemainderStatus = chkRemainderStatus
                .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                .bitOutlook = IIf(chkOutlook.Checked = True, 1, 0)
                .SendEmailTemplate = rdlEmailTemplate.SelectedValue
                .EmailTemplate = ddlEmailTemplate.SelectedValue
                .Hours = IIf(txtHours.Text = "", 0, txtHours.Text)
                .Alert = IIf(chkAlert.Checked = True, 1, 0)
                .ActivityId = lngActivityId
                If GetQueryStringVal( "frm") = "Cases" Then
                    .CaseID = GetQueryStringVal( "CaseID")
                    '.CaseTimeId = CInt(txtCaseTimeID.Text)
                    '.CaseExpId = CInt(txtCaseExpenseID.Text)
                End If
            End With

            numcommId = objActionItem.SaveCommunicationinfo()
            If objContacts Is Nothing Then objContacts = New CContacts
            objContacts.RecID = numcommId
            objContacts.Type = "A"
            objContacts.UserCntID = Session("UserContactID")
            objContacts.AddVisiteddetails()
            ''Sending mail to Assigneee
            If Session("UserContactID") <> ddlAssignTo.SelectedItem.Value Then
                objAlerts = New CAlerts
                Dim dtDetails As DataTable
                objAlerts.AlertDTLID = 5 'Alert DTL ID for sending alerts in opportunities
                objAlerts.DomainID = Session("DomainID")
                dtDetails = objAlerts.GetIndAlertDTL
                If dtDetails.Rows.Count > 0 Then
                    If dtDetails.Rows(0).Item("tintAlertOn") = 1 Then
                        Dim dtEmailTemplate As DataTable
                        Dim objDocuments As New DocumentList
                        objDocuments.GenDocID = dtDetails.Rows(0).Item("numEmailTemplate")
                        objDocuments.DomainID = Session("DomainId")
                        dtEmailTemplate = objDocuments.GetDocByGenDocID
                        If dtEmailTemplate.Rows.Count > 0 Then
                            Dim objSendMail As New Email
                            Dim dtMergeFields As New DataTable
                            Dim drNew As DataRow
                            dtMergeFields.Columns.Add("Assignee")
                            dtMergeFields.Columns.Add("CommName")
                            drNew = dtMergeFields.NewRow
                            drNew("Assignee") = ddlAssignTo.SelectedItem.Text
                            drNew("CommName") = txtComments.Text
                            dtMergeFields.Rows.Add(drNew)
                            If objCommon Is Nothing Then objCommon = New CCommon
                            objCommon.byteMode = 0
                            objCommon.ContactID = ddlAssignTo.SelectedItem.Value
                            objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), IIf(dtDetails.Rows(0).Item("tintCCManager") = 1, objCommon.GetManagerEmail, ""), Session("UserEmail"), objCommon.GetContactsEmail(), dtMergeFields)
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub ddlCompanyName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlCompanyName.SelectedIndexChanged
        Try
            ' btnTime.Attributes.Add("onclick", "openTime('" & GetQueryStringVal( "CaseId") & "','" & GetQueryStringVal( "CntId") & "','" & ddlCompanyName.SelectedValue & "','" & txtCaseTimeID.Text & "')")
            'btnExpense.Attributes.Add("onclick", "openExpense('" & GetQueryStringVal( "CaseId") & "','" & GetQueryStringVal( "CntId") & "','" & ddlCompanyName.SelectedValue & "','" & txtCaseExpenseID.Text & "')")
            sb_LoadContacts()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub sb_LoadSpecificCompany(ByVal lngDivisionID As Long)
        Try
            If objContacts Is Nothing Then objContacts = New CContacts
            Dim strCompanyName As String
            objContacts.DivisionID = lngDivisionID
            strCompanyName = objContacts.GetCompanyName

            ddlCompanyName.Items.Clear()
            ddlCompanyName.Items.Add(New ListItem("--Select One--", 0))
            ddlCompanyName.Items.Add(New ListItem(strCompanyName, lngDivisionID))
        Catch Ex As Exception
            Throw Ex
        End Try
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            sb_SaveActionItem()
            If GetQueryStringVal( "Popup") = "True" Then
                Dim strScript As String = "<script language=JavaScript>"
                strScript += "self.close();"
                strScript += "</script>"
                If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
            Else : PageDirect()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub PageDirect()
        Try
            If GetQueryStringVal( "frm") = "contactdetails" Then
                If Session("EnableIntMedPage") = 1 Then
                    Response.Redirect("../pagelayout/frmContact.aspx?frm=" & GetQueryStringVal( "frm1") & "&CntId=" & GetQueryStringVal( "CntID"))
                Else : Response.Redirect("../contact/frmContacts.aspx?frm=" & GetQueryStringVal( "frm1") & "&CntId=" & GetQueryStringVal( "CntID"))
                End If
            ElseIf GetQueryStringVal( "frm") = "Accounts" Then
                If objCommon Is Nothing Then objCommon = New CCommon
                objCommon.ContactID = GetQueryStringVal( "CntID")
                objCommon.charModule = "C"
                objCommon.GetCompanySpecificValues1()
                If Session("EnableIntMedPage") = 1 Then
                    Response.Redirect("../pagelayout/frmAccountdtl.aspx?frm=" & GetQueryStringVal( "frm1") & "&klds+7kldf=fjk-las&DivId=" & objCommon.DivisionID)
                Else : Response.Redirect("../account/frmAccounts.aspx?frm=" & GetQueryStringVal( "frm1") & "&klds+7kldf=fjk-las&DivId=" & objCommon.DivisionID)
                End If
            ElseIf GetQueryStringVal( "frm") = "Prospects" Then
                If objCommon Is Nothing Then objCommon = New CCommon
                objCommon.ContactID = GetQueryStringVal( "CntID")
                objCommon.charModule = "C"
                objCommon.GetCompanySpecificValues1()
                If Session("EnableIntMedPage") = 1 Then
                    Response.Redirect("../pagelayout/frmProspectdtl.aspx?frm=" & GetQueryStringVal( "frm1") & "&DivID=" & objCommon.DivisionID)
                Else : Response.Redirect("../prospects/frmProspects.aspx?frm=" & GetQueryStringVal( "frm1") & "&DivID=" & objCommon.DivisionID)
                End If
            ElseIf GetQueryStringVal( "frm") = "Leadedetails" Then
                If objCommon Is Nothing Then objCommon = New CCommon
                objCommon.ContactID = GetQueryStringVal( "CntID")
                objCommon.charModule = "C"
                objCommon.GetCompanySpecificValues1()
                If Session("EnableIntMedPage") = 1 Then
                    Response.Redirect("../pagelayout/frmLeaddtl.aspx?frm=" & GetQueryStringVal( "frm1") & "&DivID=" & objCommon.DivisionID)
                Else : Response.Redirect("../Leads/frmLeads.aspx?frm=" & GetQueryStringVal( "frm1") & "&DivID=" & objCommon.DivisionID)
                End If
            ElseIf GetQueryStringVal( "frm") = "Cases" Then
                If Session("EnableIntMedPage") = 1 Then
                    Response.Redirect("../pagelayout/frmCasesdtl.aspx?frm=caselist&CaseID=" & GetQueryStringVal( "CaseId"))
                Else : Response.Redirect("../Cases/frmCases.aspx?frm=caselist&CaseID=" & GetQueryStringVal( "CaseId"))
                End If
            ElseIf GetQueryStringVal( "frm") = "tickler" Then
                Response.Redirect("../common/frmTicklerDisplay.aspx")
            ElseIf GetQueryStringVal( "frm") = "outlook" Then
                Response.Write("<script>window.close();</script>")
            Else : Response.Redirect("../common/frmTicklerDisplay.aspx")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            PageDirect()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            sb_LoadCompany()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub LoadActionItemTemplateData()
        Try
            If objActionItem Is Nothing Then objActionItem = New ActionItem

            Dim dtActionItems As DataTable
            objActionItem.DomainID = Session("DomainId")
            objActionItem.UserCntID = Session("UserContactId")
            dtActionItems = objActionItem.LoadActionItemTemplateData()

            If Not dtActionItems Is Nothing Then
                listActionItemTemplate.DataSource = dtActionItems
                listActionItemTemplate.DataTextField = "TemplateName"
                listActionItemTemplate.DataValueField = "RowID"
                listActionItemTemplate.DataBind()
                listActionItemTemplate.Items.Insert(0, New ListItem("--Select One--", 0))
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub listActionItemTemplate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles listActionItemTemplate.SelectedIndexChanged
        Try
            If Not listActionItemTemplate.SelectedItem Is Nothing And listActionItemTemplate.SelectedIndex > 0 Then
                editActionItem.NavigateUrl = ""
                editActionItem.NavigateUrl = "../Admin/newaction.aspx?Mode=Edit" & "&ID=" & listActionItemTemplate.SelectedItem.Value
            Else : editActionItem.NavigateUrl = "../Admin/newaction.aspx?Mode=Edit" & "&ID=-1"
            End If

            ' get this template data and bind it to current page
            Dim actionItemID As Int32
            Dim thisActionItemTable As DataTable
            If listActionItemTemplate.SelectedIndex > 0 Then
                actionItemID = Convert.ToInt32(listActionItemTemplate.SelectedValue)
                thisActionItemTable = LoadThisActionItemData(actionItemID) ' this will data from data base
                BindTemplateData(thisActionItemTable) ' pass data table contains values
            Else
                Dim strDate As Date = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                txtComments.Text = ""
                cal.SelectedDate = strDate
                txtComments.Text = ""
                ddlStatus.SelectedIndex = 0
                ddlActivity.SelectedIndex = 0
                ddlTaskType.SelectedIndex = 0
            End If
            ' if selected value is 5 then we have to set date as 15/06/1981, Anytime ' eblow code will do same
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Function LoadThisActionItemData(ByVal actionItemID As Int32) As DataTable
        Try
            If objActionItem Is Nothing Then objActionItem = New ActionItem
            With objActionItem
                .RowID = actionItemID
            End With
            Return objActionItem.LoadThisActionItemTemplateData()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub BindTemplateData(ByVal thisActionItemTable As DataTable)
        Try
            'templateName.Text = thisActionItemTable.Rows(0)("TemplateName")
            Dim strDate As String = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow).AddDays(Convert.ToDouble(thisActionItemTable.Rows(0)("DueDays")))
            cal.SelectedDate = strDate
            txtComments.Text = thisActionItemTable.Rows(0)("Comments")
            ddlStatus.SelectedItem.Selected = False
            ddlActivity.SelectedItem.Selected = False
            ddlTaskType.SelectedItem.Selected = False
            ddlEmailTemplate.SelectedItem.Selected = False
            If Not ddlStatus.Items.FindByValue(thisActionItemTable.Rows(0)("Priority")) Is Nothing Then
                ddlStatus.Items.FindByValue(thisActionItemTable.Rows(0)("Priority")).Selected = True
            End If
            If Not ddlActivity.Items.FindByValue(thisActionItemTable.Rows(0)("Activity")) Is Nothing Then
                ddlActivity.Items.FindByValue(thisActionItemTable.Rows(0)("Activity")).Selected = True
            End If
            Dim _type As String = thisActionItemTable.Rows(0)("Type")
            ddlTaskType.SelectedIndex = -1
            If Not ddlTaskType.Items.FindByText(_type.Trim()) Is Nothing Then
                ddlTaskType.Items.FindByText(_type.Trim()).Selected = True
            End If

            If Not ddlEmailTemplate.Items.FindByValue(thisActionItemTable.Rows(0).Item("numEmailTemplate")) Is Nothing Then
                ddlEmailTemplate.Items.FindByValue(thisActionItemTable.Rows(0).Item("numEmailTemplate")).Selected = True
            End If
            If thisActionItemTable.Rows(0).Item("bitAlert") = False Then
                chkAlert.Checked = False
            Else : chkAlert.Checked = True
            End If
            rdlEmailTemplate.SelectedValue = IIf(thisActionItemTable.Rows(0).Item("bitSendEmailTemp") = True, 1, 0)
            txtHours.Text = thisActionItemTable.Rows(0).Item("tintHours")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function StripTags(ByVal HTML As String) As String
        Try
            ' Removes tags from passed HTML
            Dim objRegEx As  _
                System.Text.RegularExpressions.Regex
            Dim str As String = objRegEx.Replace(HTML, "<[^>]*>", "").Replace("P {", "")
            str = str.Replace("MARGIN-TOP: 0px; MARGIN-BOTTOM: 0px", "")
            Return str.Trim
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnLoadData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLoadData.Click
        Try
            BindTemplateData(LoadThisActionItemData(listActionItemTemplate.SelectedValue))
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            sb_SaveActionItem()
            Response.Redirect("../admin/actionitemdetails.aspx?frm=Cases&CommId=" & numcommId & "&CaseId=" & GetQueryStringVal( "CaseId"))
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class
