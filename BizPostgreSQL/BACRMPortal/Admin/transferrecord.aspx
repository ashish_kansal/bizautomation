<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="transferrecord.aspx.vb" Inherits="BACRMPortal.transferrecord" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head  runat="server">
        <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
		<title>Transfer OwnerShip</title>
		<script language="javascript">
		function check()
		{
			if (document.Form1.ddlEmployee.value==0)
			{
				alert("Please Select Employee")
				document.Form1.ddlEmployee.focus()
				return false;
			}
		}
		function Close()
		{
			window.close();
		}
		</script>
	</HEAD>
	<body>
		<form id="Form1" name="Form1" method="post" runat="server">
			<br>
			<TABLE id="Table3" cellpadding="0" cellspacing="0" width="100%">
				<TR>
					<td vAlign="bottom" nowrap colspan="2" align="left">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;Transfer Record 
									Ownership&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
				</TR>
			</TABLE>
			<asp:table id="Table2" Runat="server" GridLines="None" BorderColor="black" Width="100%" BorderWidth="1" CssClass="aspTable"
				CellSpacing="0" CellPadding="0">
				<asp:TableRow>
					<asp:TableCell VerticalAlign="Top">
						<br>
						<table width="100%">
							<TR>
								<TD class="normal1" align="right" valign="top">Select User</TD>
								<TD>
									<asp:dropdownlist id="ddlEmployee" cssclass="signup" runat="server"></asp:dropdownlist>
								</TD>
							</TR>
							<tr>
								<td colspan="2" align="center">
									<asp:button id="btnTransfer" Runat="server" Text="Transfer Ownership" CssClass="button"></asp:button>
									&nbsp;
									<asp:button id="btnCancel" Runat="server" Text="Close" CssClass="button" Width="55"></asp:button>
								</td>
							</tr>
						</table>
						<br>
					</asp:TableCell>
				</asp:TableRow>
			</asp:table>
			<table width="100%">
				<tr>
					<td class="normal4" align="center">
						<asp:literal id="litMessage" Runat="server" EnableViewState="False"></asp:literal></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
