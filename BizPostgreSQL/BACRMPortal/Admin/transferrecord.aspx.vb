Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common

Public Class transferrecord : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not IsPostBack Then sb_FillEmpList()
            btnCancel.Attributes.Add("onclick", "return Close()")
            btnTransfer.Attributes.Add("onclick", "return check()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub sb_FillEmpList()
        Try
            If GetQueryStringVal( "frm") = "Opp" Then
                Dim objCommon As New CCommon
                If Session("PopulateUserCriteria") = 1 Then
                    objCommon.OppID = GetQueryStringVal( "pluYR")
                    objCommon.charModule = "O"
                    objCommon.GetCompanySpecificValues1()
                    objCommon.sb_FillConEmpFromTerritories(ddlEmployee, Session("DomainID"), 0, 0, objCommon.TerittoryID)
                ElseIf Session("PopulateUserCriteria") = 2 Then
                    objCommon.sb_FillConEmpFromDBUTeam(ddlEmployee, Session("DomainID"), Session("UserContactID"))
                Else : objCommon.sb_FillConEmpFromDBSel(ddlEmployee, Session("DomainID"), 0, 0)
                End If
            ElseIf GetQueryStringVal( "frm") = "Projects" Then
                Dim objCommon As New CCommon
                If Session("PopulateUserCriteria") = 1 Then
                    objCommon.ProID = GetQueryStringVal( "tyrCV")
                    objCommon.charModule = "P"
                    objCommon.GetCompanySpecificValues1()
                    objCommon.sb_FillConEmpFromTerritories(ddlEmployee, Session("DomainID"), 0, 0, objCommon.TerittoryID)
                ElseIf Session("PopulateUserCriteria") = 2 Then
                    objCommon.sb_FillConEmpFromDBUTeam(ddlEmployee, Session("DomainID"), Session("UserContactID"))
                Else : objCommon.sb_FillConEmpFromDBSel(ddlEmployee, Session("DomainID"), 0, 0)
                End If
            Else
                Dim objCommon As New CCommon
                If Session("PopulateUserCriteria") = 1 Then
                    objCommon.DivisionID = GetQueryStringVal( "rtyWR")
                    objCommon.charModule = "D"
                    objCommon.GetCompanySpecificValues1()
                    objCommon.sb_FillConEmpFromTerritories(ddlEmployee, Session("DomainID"), 0, 0, objCommon.TerittoryID)
                ElseIf Session("PopulateUserCriteria") = 2 Then
                    objCommon.sb_FillConEmpFromDBUTeam(ddlEmployee, Session("DomainID"), Session("UserContactID"))
                Else : objCommon.sb_FillConEmpFromDBSel(ddlEmployee, Session("DomainID"), 0, 0)
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnTransfer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTransfer.Click
        Try
            Dim objUserAccess As New UserAccess
            objUserAccess.UserCntID = ddlEmployee.SelectedItem.Value
            If GetQueryStringVal( "frm") = "Opp" Then
                objUserAccess.RecID = GetQueryStringVal( "pluYR")
                objUserAccess.byteMode = 1
            ElseIf GetQueryStringVal( "frm") = "Projects" Then
                objUserAccess.RecID = GetQueryStringVal( "tyrCV")
                objUserAccess.byteMode = 2
            Else
                objUserAccess.RecID = GetQueryStringVal( "rtyWR")
                objUserAccess.byteMode = 0
            End If
            objUserAccess.DomainID = Session("DomainID")
            objUserAccess.TransferOwnership()
            litMessage.Text = "Sucessfully transfered"
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class
