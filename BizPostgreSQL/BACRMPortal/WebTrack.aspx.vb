Imports BACRM.BusinessLogic.Common
Partial Public Class WebTrack
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Clear()
        If ConfigurationManager.AppSettings("IsDebugMode") = "true" Then
            Dim sb As New System.Text.StringBuilder
            sb.AppendLine("/* PortalName Used to set domain id=" & IIf(Request.Url.Host.IndexOf(".") > -1, Request.Url.Host.Split(".")(0).ToString, Request.Url.Host))
            sb.AppendLine("****************Session Values****************************")
            Dim Keys As IEnumerator = Session.Keys.GetEnumerator()
            While Keys.MoveNext
                sb.AppendLine(Keys.Current.ToString & "=" & CCommon.ToString(Session(Keys.Current.ToString())))
            End While
            sb.AppendLine("****************Cookie Values****************************")
            Dim cookie As IEnumerator = Request.Cookies.Keys.GetEnumerator()
            While cookie.MoveNext
                sb.AppendLine(cookie.Current.ToString & "=" & Request.Cookies.Get(cookie.Current.ToString()).Value)
            End While
            sb.AppendLine("*/")
            Response.Write(sb.ToString)
        End If
        Response.ContentType = "text/javascript"
        Response.End()
    End Sub

End Class