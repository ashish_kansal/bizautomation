﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class frmLandingPage

    '''<summary>
    '''MasterCSS control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents MasterCSS As Global.System.Web.UI.HtmlControls.HtmlLink

    '''<summary>
    '''form1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents form1 As Global.System.Web.UI.HtmlControls.HtmlForm

    '''<summary>
    '''hdSurId control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdSurId As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdRespondentID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdRespondentID As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdRecOwner control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdRecOwner As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdGroupId control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdGroupId As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdCRMType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdCRMType As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdRelationShip control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdRelationShip As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdProfile control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdProfile As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''hdCreateDBEntry control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdCreateDBEntry As Global.System.Web.UI.HtmlControls.HtmlInputHidden

    '''<summary>
    '''lblHeader control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblHeader As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''rdbLandingPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rdbLandingPage As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''btnContinue control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnContinue As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''lblFooter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFooter As Global.System.Web.UI.WebControls.Label
End Class
