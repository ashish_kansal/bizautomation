<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PartnerPoint.ascx.vb"
    Inherits="BACRMPortal.Common_PartnerPoint" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<script language="javascript" type="text/javascript">
    functiongoto(target, frame, div, a) {

        target = target + '?DivID=' + document.getElementById('PartnerPoint1_txtDivision').value + '&CntID=' + document.getElementById('PartnerPoint1_txtContact').value + '&Type=' + a + '&ProID=' + document.getElementById('PartnerPoint1_txtPro').value + '&OpID=' + document.getElementById('PartnerPoint1_txtOpp').value + '&CaseID=' + document.getElementById('PartnerPoint1_txtCase').value
        window.open(target, '', 'toolbar=no,titlebar=no,left=150, top=150,width=750,height=450,scrollbars=yes,resizable=yes')

    }
    function goto1(target, frame, div, a) {

        target = target + '?DivID=' + document.getElementById('PartnerPoint1_txtDivision').value + '&CntID=' + document.getElementById('PartnerPoint1_txtContact').value + '&Type=' + a + '&ProID=' + document.getElementById('PartnerPoint1_txtPro').value + '&OpID=' + document.getElementById('PartnerPoint1_txtOpp').value + '&CaseID=' + document.getElementById('PartnerPoint1_txtCase').value
        window.open(target, '', 'toolbar=no,titlebar=no,left=150, top=150,width=850,height=550,scrollbars=yes,resizable=yes')

    }
    function reDirect(a) {
        document.location.href=a;
    }
    function reDirectPage(a) {
        document.location.href=a;
    }
        
</script>

<table class="ShortCutBar" id="table2" cellspacing="0" cellpadding="0" width="100%"
    border="0">
    <tr>
        <td width="2" height="22">
        </td>
        <td class="Text_WB" align="right">
            New:&nbsp;&nbsp;
        </td>
        <td class="Text_W">
            <a onclick="goto('../Common/frmSelRelationship.aspx?I=1','cntOpenItem','divNew');"
                href="#"><font color="#ffffff">Relationship</font></a>, <a onclick="goto('../Contact/newContact.aspx?I=1','cntOpenItem','divNew',0);"
                    href="#"><font color="#ffffff">Contact</font></a>, <a onclick="goto1('../Opportunity/frmAddOpportunity.aspx?I=1','cntOpenItem','divNew',0);"
                        href="#"><font color="#ffffff">Opportunity</font></a>, <a onclick="goto('../cases/frmAddCases.aspx?I=1','cntOpenItem','divNew',0);"
                            href="#"><font color="#ffffff">Case</font></a>, <a onclick="goto('../Projects/frmProjectAdd.aspx?I=1','cntOpenItem','divNew',0);"
                                href="#"><font color="#ffffff">Project</font></a>
        </td>
    </tr>
</table>
<br />
<table width="100%">
    <tr>
        <td align="right">
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" DynamicLayout="False">
                <ProgressTemplate>
                    <asp:Image ID="Image1" ImageUrl="~/images/updating.gif" runat="server" ImageAlign="Top" />
                </ProgressTemplate>
            </asp:UpdateProgress>
        </td>
    </tr>
</table>
<input id="txtDivision" style="display: none" type="text" name="txtDivision" runat="server" />
<input id="txtContact" style="display: none" type="text" name="txtContact" runat="server" />
<input id="txtOpp" style="display: none" type="text" name="txtOpp" runat="server" />
<input id="txtPro" style="display: none" type="text" name="txtPro" runat="server" />
<input id="txtCase" style="display: none" type="text" name="txtCase" runat="server" />
