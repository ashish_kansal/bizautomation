<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmPortal.aspx.vb" Inherits="BACRMPortal.frmPortal" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <link id="Link1" href="~/images/BizSkin/TabStrip.BizSkin.css" rel="stylesheet" type="text/css"
        runat="server" />
    <script language="javascript" type="text/javascript">
        function OpeninFrame(a) {
            if (a == '#') {
                return false;
            }
            else if (a == 'ActionItems/frmActItemList.aspx') {
                parent.frames['mainframe'].location.href = '../' + a + '?ClientMachineUTCTimeOffset=' + new Date().getTimezoneOffset();
                parent.frames['topframe'].location.href = '../Common/frmPartnerPortal.aspx'
            }
            else {
                parent.frames['mainframe'].location.href = '../' + a;
            }
        }
        function Login() {
            if (parent.parent.frames.length > 0) {
                parent.parent.document.location.href = "frmLogout.aspx?Redirect=1"
            }
            else if (parent.frames.length > 0) {
                parent.document.location.href = "frmLogout.aspx?Redirect=1"
            }
            else {
                document.location.href = "frmLogout.aspx?Redirect=1"
            }

            //parent.location.href='common/frmticklerdisplay.aspx?ClientMachineUTCTimeOffset='+new Date().getTimezoneOffset();
            return false;
        }
        function RediretWebStore(a) {
            top.location = a;
            return false;
        }
        function goto(target, frame, div) {

            window.open(target, '', 'toolbar=no,titlebar=no,left=150, top=150,width=750,height=450,scrollbars=yes,resizable=yes')
        }
        function reDirect(a) {
            top.document.getElementById('mainframe').src = a;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="ScriptManager" runat="server" />
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td>
                <img runat="server" id="imgLogo" height="43">
            </td>
            <td align="right" rowspan="2" valign="top" class="normal14">
                <b>Logged in as:&nbsp;</b><asp:Label ID="lblUserName" runat="server" Text=""></asp:Label>&nbsp;&nbsp;<asp:HyperLink
                    ID="hplLogout" NavigateUrl="#" runat="server" CssClass="text_bold" onclick="return Login();">Logout</asp:HyperLink>&nbsp;
                <br />
                <table>
                    <tr>
                        <td align="left">
                            &nbsp;<i style="font-size: smaller;">Powered by</i>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="http://www.bizautomation.com" target="_blank">
                                <img style="border: 0;" src="../images/poweredBiz.gif"></a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="bottom">
                <telerik:RadTabStrip ID="RadTabStrip1" runat="server" EnableEmbeddedSkins="false"
                    Skin="BizSkin" SelectedIndex="0">
                    <Tabs>
                    </Tabs>
                </telerik:RadTabStrip>
            </td>
        </tr>
    </table>
    <table class="ShortCutBar" id="table2" cellspacing="0" cellpadding="0" width="100%"
        border="0">
        <tr>
            <td width="2" height="22">
            </td>
            <td class="Text_W">
                <asp:HyperLink ID="hplBackToWebStore" runat="server" Text="<font color=#ffffff>Go to the web store</font>"
                    Visible="false"></asp:HyperLink>
            </td>
            <td class="Text_WB" align="right">
                New:&nbsp;&nbsp;
            </td>
            <td class="Text_W">
                <a onclick="goto('../Contact/frmCusAddContact.aspx?I=1','cntOpenItem','divNew',0);"
                    href="#"><font color="#ffffff">Contact</font></a>, <a onclick="goto('../Cases/frmCusAddCase.aspx?I=1','cntOpenItem','divNew',0);"
                        href="#"><font color="#ffffff">Case</font></a>
                <%--<A  href="../Order/frmOrder.aspx?I=1"><font color="#ffffff">Order</font></A>--%>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
