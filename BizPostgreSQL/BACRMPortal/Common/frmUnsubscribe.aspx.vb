﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Admin

Partial Public Class frmUnsubscribe
    Inherits BACRMPage

    Private lngDomainId As Long
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngDomainId = GetQueryStringVal( "D")

            If Not IsPostBack Then
                txtUnsubscribe.Text = GetQueryStringVal( "e")

                If lngDomainId > 0 Then
                    Dim dtTable As DataTable
                    Dim objUserAccess As New UserAccess
                    objUserAccess.DomainID = lngDomainId
                    dtTable = objUserAccess.GetDomainDetails()
                    If dtTable.Rows.Count > 0 Then
                        If Not IsDBNull(dtTable.Rows(0).Item("vcPortalLogo")) Then imgLogo.Src = "../Documents/Docs/" & objUserAccess.DomainID & "/" & dtTable.Rows(0).Item("vcPortalLogo")
                    Else : imgLogo.Visible = False
                    End If
                End If

                Dim objOppBizDoc As New OppBizDocs
                objOppBizDoc.DomainID = lngDomainId
                objOppBizDoc.BizDocId = 0
                objOppBizDoc.OppType = 0
                objOppBizDoc.TemplateType = 1

                Dim dt As DataTable = objOppBizDoc.GetBizDocTemplate()
                If dt.Rows.Count > 0 Then
                    'RadEditor1.Content = HttpUtility.HtmlDecode(CCommon.ToString(dt.Rows(0)("txtBizDocTemplate")))

                    Dim strBizDocUI As String = HttpUtility.HtmlDecode(dt.Rows(0)("txtBizDocTemplate"))
                    If strBizDocUI.Length > 0 Then
                        strBizDocUI = strBizDocUI.Replace("#Logo#", CCommon.RenderControl(imgLogo))
                        strBizDocUI = strBizDocUI.Replace("#EmailTextBox#", CCommon.RenderControl(txtUnsubscribe))
                        strBizDocUI = strBizDocUI.Replace("#SubmitButton#", CCommon.RenderControl(btnUnsubscribe))
                    End If

                    litTemplate.Text = strBizDocUI
                    tblOriginal.Visible = False
                    tblFormatted.Visible = True

                    Dim styles As New HtmlGenericControl("style")
                    styles.Attributes.Add("type", "text/css")
                    styles.InnerText = HttpUtility.HtmlDecode(CCommon.ToString(dt.Rows(0)("txtCSS")))
                    Me.Header.Controls.Add(styles)
                End If

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnUnsubscribe_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUnsubscribe.Click
        Try
            Dim objContact As New CContacts
            objContact.ContactID = CCommon.ToLong(GetQueryStringVal("c"))
            objContact.BroadcastID = CCommon.ToLong(GetQueryStringVal("BroacastId"))
            objContact.Email = txtUnsubscribe.Text.Trim
            objContact.OptOut = True
            objContact.UpdateOptOut()
            'tblUnsub.Visible = False
            lblMessage.Visible = True
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub
End Class