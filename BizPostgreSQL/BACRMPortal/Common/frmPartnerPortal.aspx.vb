Imports ASPNETExpert.WebControls
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Partial Public Class frmPartnerPortal : Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then BuildTotalMenu()
            hplLogout.Attributes.Add("onclick", "return Login()")
            If Session("EnableIntMedPage") = 1 Then
                hplPortal.Attributes.Add("onclick", "return RedirectToPortal(1)")
            Else : hplPortal.Attributes.Add("onclick", "return RedirectToPortal(2)")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub BuildTotalMenu()
        Try
            Dim dtTabData As DataTable
            Dim topItem As MenuItem
            Dim menuItem As MenuItem
            Dim objTab As New Tabs
            objTab.GroupID = Session("PartnerGroup")
            objTab.RelationshipID = 0
            objTab.DomainID = Session("DomainID")
            dtTabData = objTab.GetTabData()
            Dim i As Integer
            Dim dtTab As DataTable
            dtTab = Session("DefaultTab")
            For i = 0 To dtTabData.Rows.Count - 1
                dtTabData.Rows(i).Item("vcURL") = Replace(dtTabData.Rows(i).Item("vcURL"), "RecordID", Session("UserContactID"))
                If dtTabData.Rows(i).Item("bitFixed") = True Then
                    Select Case dtTabData.Rows(i).Item("numTabName").ToString
                        Case "Leads" : topItem = CreateParentItem(dtTab.Rows(0).Item("vcLead"), dtTabData.Rows(i).Item("vcURL"), Nothing)
                        Case "Prospects" : topItem = CreateParentItem(dtTab.Rows(0).Item("vcProspect"), dtTabData.Rows(i).Item("vcURL"), Nothing)
                        Case "Accounts" : topItem = CreateParentItem(dtTab.Rows(0).Item("vcAccount"), dtTabData.Rows(i).Item("vcURL"), Nothing)
                        Case "Contacts" : topItem = CreateParentItem(dtTab.Rows(0).Item("vcContact"), dtTabData.Rows(i).Item("vcURL"), Nothing)
                        Case Else
                            topItem = CreateParentItem(dtTabData.Rows(i).Item("numTabName"), dtTabData.Rows(i).Item("vcURL"), Nothing)
                    End Select
                Else : topItem = CreateParentItem(dtTabData.Rows(i).Item("numTabName"), "include/SimpleMenu.aspx?Page=" & dtTabData.Rows(i).Item("vcURL"), Nothing)
                End If
                TotallyBuiltMenu.TopGroup.Items.Add(topItem)
                If dtTabData.Rows(i).Item("numTabId") = 20 Then
                    Dim dtTabDataMenu As DataTable
                    If Session("TabDataMenu") Is Nothing Then
                        objTab.ListId = 5
                        objTab.mode = False
                        dtTabDataMenu = objTab.GetTabMenuItem()
                        Session("TabDataMenu") = dtTabDataMenu
                    Else : dtTabDataMenu = Session("TabDataMenu")
                    End If

                    'If Not dtTab Is Nothing Then
                    '    menuItem = CreateParentItem(dtTab.Rows(0).Item("vcLead").ToString & "s", "#", "icon_open_over.gif")
                    '    topItem.SubMenu.Items.Add(menuItem)
                    '    menuItem.SubMenu.Items.Add(CreateItem("My" & dtTab.Rows(0).Item("vcLead").ToString & "s", "Leads/frmLeadList.aspx?GrpID=5", "icon_project_over.gif"))
                    '    menuItem.SubMenu.Items.Add(CreateItem("Web " & dtTab.Rows(0).Item("vcLead").ToString & "s", "Leads/frmLeadList.aspx?GrpID=1", "icon_project_over.gif"))
                    '    menuItem.SubMenu.Items.Add(CreateItem("Public " & dtTab.Rows(0).Item("vcLead").ToString & "s", "Leads/frmLeadList.aspx?GrpID=2", "icon_project_over.gif"))
                    '    topItem.SubMenu.Items.Add(CreateItem(dtTab.Rows(0).Item("vcProspect").ToString & "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "prospects/frmCompanyList.aspx?CRMType=1", "icon_new_over.gif"))
                    '    topItem.SubMenu.Items.Add(CreateItem(dtTab.Rows(0).Item("vcAccount").ToString, "prospects/frmCompanyList.aspx?CRMType=2", "icon_new_over.gif"))
                    'Else
                    '    menuItem = CreateParentItem("Leads", "#", "icon_open_over.gif")
                    '    topItem.SubMenu.Items.Add(menuItem)
                    '    menuItem.SubMenu.Items.Add(CreateItem("MyLeads", "Leads/frmLeadList.aspx?GrpID=5", "icon_project_over.gif"))
                    '    menuItem.SubMenu.Items.Add(CreateItem("WebLeads", "Leads/frmLeadList.aspx?GrpID=1", "icon_project_over.gif"))
                    '    menuItem.SubMenu.Items.Add(CreateItem("Public Leads", "Leads/frmLeadList.aspx?GrpID=2", "icon_project_over.gif"))
                    '    topItem.SubMenu.Items.Add(CreateItem("Prospects&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", "prospects/frmCompanyList.aspx?CRMType=1", "icon_new_over.gif"))
                    '    topItem.SubMenu.Items.Add(CreateItem("Accounts", "prospects/frmCompanyList.aspx?CRMType=2", "icon_new_over.gif"))
                    'End If

                    'Dim k As Integer
                    'For k = 0 To dtTabDataMenu.Rows.Count - 1
                    '    topItem.SubMenu.Items.Add(CreateItem(IIf(Len(dtTabDataMenu.Rows(k).Item("vcData")) > 20, Left(dtTabDataMenu.Rows(k).Item("vcData"), 18) & "..", dtTabDataMenu.Rows(k).Item("vcData")), "prospects/frmCompanyList.aspx?RelId=" & dtTabDataMenu.Rows(k).Item("numListItemID"), "icon_new_over.gif"))
                    'Next
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function CreateParentItem(ByVal text As String, ByVal leftIcon As String, ByVal leftIconOver As String) As MenuItem
        Try
            Dim result As MenuItem = CreateItem(text, leftIcon, leftIconOver)
            result.SubMenu = New MenuGroup
            Return result
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function CreateItem(ByVal text As String, ByVal leftIcon As String, ByVal leftIconOver As String) As MenuItem
        Try
            Dim result As MenuItem = New MenuItem(text)
            result.OnClientAfter.Click = ("OpeninFrame('" & leftIcon & "')")
            Return result
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class