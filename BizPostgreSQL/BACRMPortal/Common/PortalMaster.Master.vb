﻿Imports BACRM.BusinessLogic.Common
Imports System.Text.RegularExpressions
Imports System.Globalization
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Accounting
Imports System.Xml.Serialization
Imports System.Text
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports System.Net.Mail
Imports System.Net
Imports BACRM.BusinessLogic.Admin

Public Class PortalMaster
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If CCommon.ToLong(Session("DomainID")) = 0 Then
                Response.Write("<script>top.location= '../login.aspx';</script>")
                Exit Sub
                'Response.Redirect("../login.aspx", False)
            End If
            If Not IsPostBack Then
                If Session("Logo") = "" Then
                    imgLogo.Src = "../images/New LogoBiz1.JPG"
                Else : imgLogo.Src = "../Documents/Docs/" & Session("DomainID") & "/" & Session("Logo")
                End If

                lblUserName.Text = Session("ContactName") & ", " & Session("CompName")
            End If

            Dim currentTab As Telerik.Web.UI.RadTab
            Dim strQueryString As String = Request.Url.PathAndQuery
            If strQueryString.Contains("?") Then
                Dim objQSV As New QueryStringValues
                strQueryString = Request.Url.PathAndQuery
                strQueryString = strQueryString.Substring(0, strQueryString.IndexOf("?"))
                strQueryString = strQueryString + "?type=" + objQSV.GetQueryStringVal(Request.QueryString("enc"), "type", False)
                currentTab = RadTabStrip1.FindTabByUrl(strQueryString)
            Else
                currentTab = RadTabStrip1.FindTabByUrl(Request.Url.PathAndQuery)
            End If

            If currentTab IsNot Nothing Then
                currentTab.Selected = True
            End If

            Dim dtTable As New DataTable
            Dim IsTabVisible As Boolean = False
            Dim objUserAccess As New UserAccess
            objUserAccess.DomainID = Session("DomainID")
            dtTable = objUserAccess.GetDomainDetails()
            For Each strItem As String In CCommon.ToString(dtTable.Rows(0).Item("vcHideTabs")).Split(",")
                If RadTabStrip1.Tabs.FindTabByValue(CCommon.ToShort(strItem)) IsNot Nothing Then
                    RadTabStrip1.Tabs.FindTabByValue(CCommon.ToShort(strItem)).Visible = False
                    IsTabVisible = True
                End If
            Next

            'If no tab is selected to show then by default show all tabs
            If IsTabVisible = False Then
                For intC As Integer = 0 To 6
                    RadTabStrip1.Tabs.FindTabByValue(intC + 1).Visible = True
                Next
            End If

        Catch ex As Exception

        End Try
    End Sub

End Class