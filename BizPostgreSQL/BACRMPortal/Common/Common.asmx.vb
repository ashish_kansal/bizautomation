﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports BACRM.BusinessLogic.ShioppingCart
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports System.Web.Script.Serialization
Imports System.Web.Script.Services

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class Common
    Inherits System.Web.Services.WebService

    Dim objEncryption As New QueryStringValues
#Region "Web Methods"
    <WebMethod()> _
    Public Function ManageRatings(ByVal TypeId As String, ByVal ReferenceId As String, ByVal RatingCount As String, ByVal ContactId As String, ByVal IpAddress As String, ByVal SiteId As String, ByVal DomainId As String) As Boolean   ', int TypeId ,string  ReferenceId ,int RatingCount ,long ContactId ,string  IpAddress ,long SiteId ,long DomainId)() As SiteId,long
        Try

            Dim objReview As New Review

            objReview.RatingId = 0 'objEncryption.Decrypt(RatingId)
            objReview.TypeId = objEncryption.Decrypt(TypeId)
            objReview.ReferenceId = objEncryption.Decrypt(ReferenceId)
            objReview.RatingCount = RatingCount
            objReview.ContactId = objEncryption.Decrypt(ContactId)
            objReview.IpAddress = objEncryption.Decrypt(IpAddress)
            objReview.DomainID = objEncryption.Decrypt(DomainId)
            objReview.SiteId = objEncryption.Decrypt(SiteId)
            objReview.CreatedDate = CCommon.ToString(System.DateTime.Now)

            objReview.ManageRatings()
            Return True

        Catch ex As Exception
            Throw ex
        End Try
    End Function
    <WebMethod()> _
    Public Function ManageReviewDetail(ByVal ReviewId As String, ByVal ContactId As String, ByVal Mode As Integer, ByVal ReportValue As Boolean) As String   ', int TypeId ,string  ReferenceId ,int RatingCount ,long ContactId ,string  IpAddress ,long SiteId ,long DomainId)() As SiteId,long
        Try

            Dim objReview As New Review

            objReview.ReviewId = CCommon.ToLong(objEncryption.Decrypt(ReviewId))
            objReview.ContactId = CCommon.ToLong(objEncryption.Decrypt(ContactId))
            objReview.Mode = Mode
            If Mode = 0 Then
                objReview.Abuse = ReportValue
            ElseIf Mode = 1 Then
                objReview.Helpful = ReportValue
            End If

            If objReview.ManageReviewDetail() Then
                Return GetHelpfulReviewCount(CCommon.ToLong(objEncryption.Decrypt(ReviewId)))
            End If

            Return ""
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    <WebMethod()> _
    Public Function UpdateAverageRating(ByVal ReferenceId As String, ByVal SiteId As String, ByVal DomainId As String) As String    ', int TypeId ,string  ReferenceId ,int RatingCount ,long ContactId ,string  IpAddress ,long SiteId ,long DomainId)() As SiteId,long
        Try

            Dim objReview As New Review
            Dim AvgRating As Double = 0
            AvgRating = GetAvgRatings(CCommon.ToLong(objEncryption.Decrypt(ReferenceId)), CCommon.ToLong(objEncryption.Decrypt(SiteId)), CCommon.ToLong(objEncryption.Decrypt(DomainId)))
            Dim title() As String = {"Very Poor", "Poor", "Ok", "Good", "very Good"}
            Dim strAvgRatingControl As String = GetRatingContol("ItemAverageRating", True, True, AvgRating, True, 2, "star", False, title)

            Return strAvgRatingControl
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    <WebMethod()> _
    Public Function UpdateRatingCount(ByVal ReferenceId As String, ByVal TypeId As String, ByVal SiteId As String, ByVal DomainId As String) As Long ', int TypeId ,string  ReferenceId ,int RatingCount ,long ContactId ,string  IpAddress ,long SiteId ,long DomainId)() As SiteId,long
        Try
            Dim dtRating As DataTable = New DataTable
            Dim objReview As Review = New Review
            objReview.ReferenceId = objEncryption.Decrypt(ReferenceId)
            objReview.DomainID = objEncryption.Decrypt(DomainId)
            objReview.SiteId = objEncryption.Decrypt(SiteId)
            objReview.TypeId = 2
            objReview.ReviewId = objEncryption.Decrypt(ReferenceId)
            objReview.Mode = 3
            dtRating = objReview.GetRatings
            If (dtRating.Rows.Count > 0) Then
                Return CCommon.ToDouble(dtRating.Rows(0)("RatingCount"))
            End If

            Return 0
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    <WebMethod()> _
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json)> _
    Public Function CheckIfDuplicateEmail(ByVal email As String, ByVal domain As String) As String
        Try
            Dim objEnc As New QueryStringValues
            Dim numDomainId As Long = objEnc.Decrypt(domain)

            Dim objContact As New CContacts
            objContact.DomainID = CCommon.ToLong(numDomainId)
            objContact.ContactID = 0
            objContact.Email = email

            If objContact.IsDuplicateEmail() Then
                Return New JavaScriptSerializer().Serialize(1)
            Else
                Return New JavaScriptSerializer().Serialize(0)
            End If
        Catch ex As Exception
            Throw
        End Try
    End Function
#End Region

#Region "Local Methods"
    Private Function GetRatingContol(ByVal Name As String, ByVal IsDisabled As Boolean, ByVal IsAverage As Boolean, ByVal CheckCount As Double, ByVal IsSplit As Boolean, ByVal SplitCount As Integer, ByVal CssClass As String, ByVal IsRequired As Boolean, ByVal title() As String) As String
        Try
            Dim strRating As String = ""
            Dim strCssClass As String = CssClass
            Dim strChecked As String = ""
            Dim strDisabled As String = ""
            Dim titleCounter As Integer = 0
            Dim RatingLimit As Integer = 5
            Dim FinalCheckCount As Integer = 0
            Dim Remainder As Double = 0
            If IsAverage Then
                Remainder = (CheckCount Mod 1)

                FinalCheckCount = (Math.Floor(CheckCount) * SplitCount)
                If (Remainder > 0) Then
                    If (Remainder > 0.5) Then
                        FinalCheckCount = (FinalCheckCount + 2)
                    Else
                        FinalCheckCount = (FinalCheckCount + 1)
                    End If
                End If
            Else
                FinalCheckCount = CType(CheckCount, Integer)
            End If
            If IsSplit Then
                strCssClass = (strCssClass + (" {split:" + CCommon.ToString(SplitCount) + "} "))
                RatingLimit = (RatingLimit * SplitCount)
            End If
            If IsDisabled Then
                strDisabled = "disabled= ""disabled"""
            Else
                strDisabled = ""
            End If
            Dim strTempCssClass As String = ""
            Dim i As Integer = 1
            Do While (i <= RatingLimit)
                If (i = FinalCheckCount) Then
                    strChecked = "checked = ""checked"""
                Else
                    strChecked = ""
                End If
                If IsRequired Then
                    If (i = 1) Then
                        strTempCssClass = strCssClass
                        strCssClass = (strCssClass + " required")
                    Else
                        strCssClass = strTempCssClass
                    End If
                End If

                strRating = (strRating + "<input class=""" + strCssClass + """ type=""radio""  name= """ + Name + """ value= """ + CCommon.ToString(i) + """ title = """ + (CCommon.ToString(title(titleCounter)) + """" + strDisabled + " " + strChecked + " />"))
                If (IsSplit <> True) Then
                    titleCounter = (titleCounter + 1)
                ElseIf ((i Mod SplitCount) _
                            = 0) Then
                    titleCounter = (titleCounter + 1)
                End If
                i = (i + 1)
            Loop
            Return strRating
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Function GetAvgRatings(ByVal ReferenceId As String, ByVal SiteId As Long, ByVal DomainId As Long) As Double
        Try
            Dim dtRating As DataTable = New DataTable
            Dim objReview As Review = New Review
            objReview.ReferenceId = ReferenceId
            objReview.DomainID = DomainId
            objReview.SiteId = SiteId
            objReview.TypeId = 2
            objReview.Mode = 2
            dtRating = objReview.GetRatings
            If (dtRating.Rows.Count > 0) Then
                Return CCommon.ToDouble(dtRating.Rows(0)("AvgRatings"))
            End If
            Return 0
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Function GetHelpfulReviewCount(ByVal ReviewId As Long) As String
        Try
            Dim dtReview As DataTable = New DataTable
            Dim objReview As Review = New Review
            objReview.ReviewId = ReviewId
            objReview.Mode = 6
            dtReview = objReview.GetReviews()

            If (dtReview.Rows.Count > 0) Then
                Return CCommon.ToString(dtReview.Rows(0)("vcHelpfulVote")) + " of " + CCommon.ToString(dtReview.Rows(0)("vcTotalVote"))
            End If

            Return ""
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    <WebMethod(EnableSession:=True)> _
    Public Function SaveGridColumnWidth(ByVal str As String)
        Try
            If str.Length > 0 Then
                Dim ds As New DataSet
                Dim dtTable As New DataTable
                Dim dr As DataRow

                dtTable.TableName = "GridColumnWidth"

                dtTable.Columns.Add("numFormId")
                dtTable.Columns.Add("numFieldId")
                dtTable.Columns.Add("bitCustom")
                dtTable.Columns.Add("intColumnWidth")

                Dim strValues() As String = str.Trim(";").Split(";")
                Dim strIDWidth(), strID() As String

                For i As Integer = 0 To strValues.Length - 1
                    dr = dtTable.NewRow
                    strIDWidth = strValues(i).Split(":")

                    strID = strIDWidth(0).Split("~")
                    If CCommon.ToLong(strID(1)) > 0 Then
                        dr("numFormId") = strID(0)
                        dr("numFieldId") = strID(1)
                        dr("bitCustom") = strID(2)

                        dr("intColumnWidth") = strIDWidth(1)

                        dtTable.Rows.Add(dr)
                    End If
                Next

                ds.Tables.Add(dtTable)

                Dim objCommon As New CCommon

                objCommon.Str = ds.GetXml
                objCommon.UserCntID = 0 'Context.Session("UserContactID")
                objCommon.DomainID = Context.Session("DomainID")
                objCommon.SaveGridColumnWidth()
            End If
            Return ""
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    <WebMethod(EnableSession:=True)> _
    Public Function GetItemPriceAndDiscount(ByVal ItemCode As Long, ByVal WarehouseItemID As Long) As String
        Dim strResult As String = ""
        Try
            If ItemCode > 0 AndAlso WarehouseItemID > 0 Then
                Dim ds As New DataSet
                Dim dtTable As New DataTable

                Dim objCommon As New CCommon
                objCommon.DomainID = Context.Session("DomainID")
                dtTable = objCommon.GetItemPriceAndDiscount(ItemCode, WarehouseItemID)

                If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                    strResult = CCommon.ToString(dtTable.Rows(0)("numItemCode")) & "~" & _
                                CCommon.ToString(dtTable.Rows(0)("numWareHouseItemID")) & "~" & _
                                CCommon.ToString(dtTable.Rows(0)("monHighestDiscount")) & "~" & _
                                CCommon.ToString(dtTable.Rows(0)("monHighestPrice"))
                End If
            End If
        Catch ex As Exception
            strResult = ""
            Throw ex
        End Try

        Return strResult
    End Function
#End Region
End Class