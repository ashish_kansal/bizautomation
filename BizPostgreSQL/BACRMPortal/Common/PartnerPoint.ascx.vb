Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Partial Public Class Common_PartnerPoint : Inherits BACRMUserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                If GetQueryStringVal("DivID") <> "" Then txtDivision.Value = GetQueryStringVal("DivID")
                If GetQueryStringVal("CntID") <> "" Then txtContact.Value = GetQueryStringVal("CntID")
                If GetQueryStringVal("ProID") <> "" Then txtPro.Value = GetQueryStringVal("ProID")
                If GetQueryStringVal("CaseID") <> "" Then txtCase.Value = GetQueryStringVal("CaseID")
                If GetQueryStringVal("OpID") <> "" Then txtOpp.Value = GetQueryStringVal("OpID")
                Response.AddHeader("Refresh", CStr(CInt(Session.Timeout) * 60))
                If Session("PartnerGroup") = 0 Then Response.Redirect("../Login.aspx?i=1")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class