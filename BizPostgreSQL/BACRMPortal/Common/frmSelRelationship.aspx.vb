Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common

Partial Public Class frmSelRelationship : Inherits BACRMPage

    '
    'Dim objCommon As New CCommon

    'Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '    Try
    '        btnclose.Attributes.Add("onclick", "return Close();")
    '        m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmRelationship.aspx", Session("UserContactID"), 25, 5)
    '        If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AS")

    '        If Not IsPostBack Then
    '            Dim dtTabDataMenu As DataTable
    '            If Session("TabDataMenu") Is Nothing Then
    '                Dim objTab As New Tabs
    '                objTab.ListId = 5
    '                objTab.DomainID = Session("DomainID")
    '                objTab.mode = False
    '                dtTabDataMenu = objTab.GetTabMenuItem()
    '                Session("TabDataMenu") = dtTabDataMenu
    '            Else : dtTabDataMenu = Session("TabDataMenu")
    '            End If
    '            Dim dtTab As DataTable
    '            dtTab = Session("DefaultTab")
    '            radRelationship.DataTextField = "vcData"
    '            radRelationship.DataValueField = "numListItemID"
    '            radRelationship.DataSource = dtTabDataMenu
    '            radRelationship.DataBind()
    '            radRelationship.Items.Insert(0, dtTab.Rows(0).Item("vcLead").ToString)
    '            radRelationship.Items.FindByText(dtTab.Rows(0).Item("vcLead").ToString).Value = 0
    '            radRelationship.Items.Insert(1, dtTab.Rows(0).Item("vcProspect").ToString)
    '            radRelationship.Items.FindByText(dtTab.Rows(0).Item("vcProspect").ToString).Value = 1
    '            radRelationship.Items.Insert(2, dtTab.Rows(0).Item("vcAccount").ToString)
    '            radRelationship.Items.FindByText(dtTab.Rows(0).Item("vcAccount").ToString).Value = 2
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
    '    Try
    '        If radRelationship.SelectedIndex = -1 Then
    '            litMessage.Text = "Please select a Relationship"
    '        Else
    '            If radRelationship.SelectedValue > 2 Then
    '                Response.Redirect("../Common/frmNewOrganization.aspx?RelID=" & radRelationship.SelectedValue)
    '            Else : Response.Redirect("../Common/frmNewOrganization.aspx?CRMType=" & radRelationship.SelectedValue)
    '            End If
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

End Class