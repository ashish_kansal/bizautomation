
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Contacts
Imports System.IO
Imports System.Reflection
Imports System.Math
Imports BACRM.BusinessLogic.alerts
Imports BACRM.BusinessLogic.documents
Imports BACRM.BusinessLogic.common
Imports BACRM.BusinessLogic.prospects
Imports BACRM.BusinessLogic.Leads
Namespace BACRM.UserInterface.Admin
    Public Class frmGenericFormLeadboxTest
        Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.ID = "frmGenericFormLeadbox"

        End Sub
        'Protected WithEvents plhFormControls As System.Web.UI.WebControls.PlaceHolder
        ' Protected WithEvents plhFormControlsAOI As System.Web.UI.WebControls.PlaceHolder
        'Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
        ' Protected WithEvents hdRedirectURL As System.Web.UI.HtmlControls.HtmlInputHidden
        'Protected WithEvents hdDomainId As System.Web.UI.HtmlControls.HtmlInputHidden
        'Protected WithEvents hdXMLString As System.Web.UI.HtmlControls.HtmlInputHidden
        'Protected WithEvents tblLeadBox As System.Web.UI.WebControls.table
        'Protected WithEvents tblLeadBoxAOI As System.Web.UI.WebControls.table
        'Protected WithEvents ValidationSummary As System.Web.UI.WebControls.ValidationSummary
        'Protected WithEvents litClientScript As System.Web.UI.WebControls.Literal
        'Protected WithEvents hdGroupId As System.Web.UI.HtmlControls.HtmlInputHidden

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object
        Protected WithEvents tblAoi As System.Web.UI.HtmlControls.Htmltable
        Private numDomainId As Integer
        Dim dtGenericFormConfig, dtGenericFormHeaderConfig As DataTable
        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This event is fired each time thepage is called. In this event we will 
        '''     get the data from the DB create the form.
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/07/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            If Trim(GetQueryStringVal(Request.QueryString("enc"), "D")) <> "" Then
                numDomainId = GetQueryStringVal(Request.QueryString("enc"), "D")                                              'Get the Domain Id from the web.config
                Dim objConfigWizard As New FormGenericFormContents                                  'Create an object of class which encaptulates the functionaltiy

                objConfigWizard.FormID = 3                                                          'Set the property of formId
                objConfigWizard.DomainID = numDomainId
                dtGenericFormHeaderConfig = objConfigWizard.getFormHeaderDetails()                  'call to get the form header details
                If dtGenericFormHeaderConfig.Rows.Count = 0 Then                                    'if the form is not registered in the database
                    btnSubmit.Enabled = False                                                       'Disable any attempt to save/ submit
                    Exit Sub                                                                        'Exit the flow
                End If
                hdRedirectURL.Value = dtGenericFormHeaderConfig.Rows(0).Item("vcAdditionalParam")   'Store the redirect URL here
                hdDomainId.Value = numDomainId                                                      'Store the Domain ID
                hdGroupId.Value = dtGenericFormHeaderConfig.Rows(0).Item("numGrpId")                'Store the Group Id
                lblHeader.Text = dtGenericFormHeaderConfig.Rows(0).Item("ntxtHeader")
                lblFooter.Text = dtGenericFormHeaderConfig.Rows(0).Item("ntxtFooter")
                lblLeft.Text = dtGenericFormHeaderConfig.Rows(0).Item("ntxtLeft")
                lblRight.Text = dtGenericFormHeaderConfig.Rows(0).Item("ntxtRight")
                styleLead.InnerText = dtGenericFormHeaderConfig.Rows(0).Item("ntxtStyle")
                GenericFormControlsGeneration.FormID = 3                                            'set the form id for Lead Box
                GenericFormControlsGeneration.DomainID = numDomainId                                'Set the domain id
                GenericFormControlsGeneration.sXMLFilePath = Server.MapPath("") & "\..\Documents\Docs\" 'set the file path
                dtGenericFormConfig = GenericFormControlsGeneration.getFormControlConfig()          'get the datatable to contain the form config
                If dtGenericFormConfig.Rows.Count = 0 Then                                          'if the xml for form fields has not been configured
                    btnSubmit.Enabled = False                                                       'Disable any attempt to save/ submit
                    plhFormControls.Controls.Add(New LiteralControl("The screen is not created. Please use BizForm Wizard to create this screen.")) 'calls to create the form controls and add it to the form
                    Exit Sub                                                                        'and exit the flow
                End If
                Dim objLeadBoxData As New ImplementLeadsAutoRoutingRules                            'Create an object of class which encaptulates the functionaltiy
                objLeadBoxData.DomainID = hdDomainId.Value                                          'set the domain id
                Dim numRoutId As Integer = objLeadBoxData.checkForDefaultAutoRule()                                     'Get the Default Rule Id
                If numRoutId = 0 Then                                                               'if the xml for form fields has not been configured
                    btnSubmit.Enabled = False                                                       'Disable any attempt to save/ submit
                    plhFormControls.Controls.Add(New LiteralControl("Auto Rules are not created, please contact the Administrator.")) 'calls to create the form controls and add it to the form
                    Exit Sub                                                                        'and exit the flow
                End If

                'btnSubmit.Attributes.Add("onclick", "javascript: PreSaveProcess(document.frmGenericFormLeadbox);")
                litClientScript.Text = GenericFormControlsGeneration.getJavascriptArray(dtGenericFormConfig) 'Create teh javascript array and store
                callFuncForFormGenerationNonAOI(dtGenericFormConfig)                                'Calls function for form generation and display for non AOI fields
                Dim dvConfig As DataView
                dvConfig = New DataView(dtGenericFormConfig)
                dvConfig.RowFilter = " vcDbColumnName like '%Country'"
                If dvConfig.Count > 0 Then
                    Dim i As Integer
                    For i = 0 To dvConfig.Count - 1
                        If dvConfig(i).Item("vcDbColumnName") = "vcBillCountry" Then
                            If Not CType(plhFormControls.FindControl("vcBilState"), DropDownList) Is Nothing Then
                                Dim dl As DropDownList
                                dl = CType(plhFormControls.FindControl(dvConfig(i).Item("vcDbColumnName")), DropDownList)
                                dl.AutoPostBack = True
                                AddHandler dl.SelectedIndexChanged, AddressOf FillStateDyn
                            End If
                        Else
                            If Not CType(plhFormControls.FindControl(Replace(dvConfig(i).Item("vcDbColumnName"), "Country", "State")), DropDownList) Is Nothing Then
                                Dim dl As DropDownList
                                dl = CType(plhFormControls.FindControl(dvConfig(i).Item("vcDbColumnName")), DropDownList)
                                dl.AutoPostBack = True
                                AddHandler dl.SelectedIndexChanged, AddressOf FillStateDyn
                            End If
                        End If

                    Next
                End If
                callFuncForFormGenerationAOI()                                   'Calls function for form generation and display for AOI fields
            Else
                plhFormControls.Controls.Add(New LiteralControl("The screen is not created. Please use BizForm Wizard to create this screen.")) 'calls to create the form controls and add it to the form
                btnSubmit.Enabled = False                                                       'Disable any attempt to save/ submit
                'tblLeadBoxAOI.Visible = False
            End If
        End Sub



        Sub FillStateDyn(ByVal sender As Object, ByVal e As EventArgs)
            If sender.ID = "vcBillCountry" Then
                FillState(CType(plhFormControls.FindControl("vcBilState"), DropDownList), sender.SelectedItem.Value, numDomainId)
            Else
                FillState(CType(plhFormControls.FindControl(Replace(sender.ID, "Country", "State")), DropDownList), sender.SelectedItem.Value, numDomainId)
            End If
        End Sub


        Sub callFuncForFormGenerationNonAOI(ByVal dtFormConfig As Datatable)
            GenericFormControlsGeneration.boolAOIField = 0                                               'Set the AOI flag to non AOI
            plhFormControls.Controls.Add(GenericFormControlsGeneration.createFormControls(dtFormConfig)) 'calls to create the form controls and add it to the form
        End Sub


        Function callFuncForFormGenerationAOI() As DataTable
            Dim dsFormConfig As DataSet
            Dim objGenericAdvSearch As New FormGenericAdvSearch 'Declare a DataTable
            objGenericAdvSearch.AuthenticationGroupID = 0
            objGenericAdvSearch.FormID = 3
            objGenericAdvSearch.DomainID = numDomainId
            dsFormConfig = objGenericAdvSearch.getAOIList()                                                     'Get the AOIList
            GenericFormControlsGeneration.createFormControlsAOI(dsFormConfig, plhFormControlsAOI)  'calls to create the form controls and add it to the form
            'Return dtFormConfig                                                                                 'Return the datatable which contains the AOI List
        End Function





        Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
            Dim objLeads As New CLeads
            Dim lnDivID, lngCMPID, lnCntID As Long
            Dim lstItem As ListItem
            Dim dRow As DataRow
            Dim ds As New DataSet
            Try
                dtGenericFormConfig.Columns.Add("vcDbColumnText")
                objLeads.GroupID = 1
                objLeads.ContactType = 70
                objLeads.DivisionName = "-"
                Dim vcAssociatedControlType As String
                Dim vcFieldType As String
                For Each dr As DataRow In dtGenericFormConfig.Rows
                    vcFieldType = dr("vcFieldType")
                    If vcFieldType = "R" Then
                        vcAssociatedControlType = dr("vcAssociatedControlType")
                        Select Case vcAssociatedControlType
                            Case "EditBox"
                                dr("vcDbColumnText") = CType(plhFormControls.FindControl(dr("vcDbColumnName")), TextBox).Text
                                If dr("vcDbColumnText") <> "" Then
                                    AssignValuesEditBox(objLeads, dr("vcDbColumnText"), dr("vcDbColumnName"))
                                End If
                            Case "SelectBox"
                                dr("vcDbColumnText") = CType(plhFormControls.FindControl(dr("vcDbColumnName")), DropDownList).SelectedItem.Value
                                If dr("vcDbColumnText") <> "" Then
                                    AssignValuesSelectBox(objLeads, dr("vcDbColumnText"), dr("vcDbColumnName"))
                                Else
                                    dr("vcDbColumnText") = 0
                                End If
                            Case "TextBox"
                                dr("vcDbColumnText") = CType(plhFormControls.FindControl(dr("vcDbColumnName")), TextBox).Text
                                If dr("vcDbColumnText") <> "" Then
                                    AssignValuesTextBox(objLeads, dr("vcDbColumnText"), dr("vcDbColumnName"))

                                End If
                        End Select
                    End If
                Next
                objLeads.DomainID = numDomainId
                Dim objAutoRoutRles As New AutoRoutingRules
                objAutoRoutRles.DomainID = numDomainId

                'Adding AOI also to the datatable for finding the record owner with column name hardcoding to 'AOI'
                If Not plhFormControlsAOI.FindControl("cbListAOI") Is Nothing Then
                    Dim chkList As CheckBoxList
                    chkList = plhFormControlsAOI.FindControl("cbListAOI")
                    For Each lstItem In chkList.Items
                        If lstItem.Selected = True Then
                            dRow = dtGenericFormConfig.NewRow
                            dRow("vcDbColumnName") = "AOI"
                            dRow("vcDbColumnText") = lstItem.Value
                            dtGenericFormConfig.Rows.Add(dRow)
                        End If
                    Next
                End If
                dtGenericFormConfig.TableName = "Table"
                ds.Tables.Add(dtGenericFormConfig.Copy)
                objAutoRoutRles.strValues = ds.GetXml
                ds.Tables.Remove(ds.Tables(0))

                objLeads.UserCntID = objAutoRoutRles.GetRecordOwner
                objLeads.CRMType = 0
                lngCMPID = objLeads.CreateRecordCompanyInfo
                objLeads.CompanyID = lngCMPID
                lnDivID = objLeads.CreateRecordDivisionsInfo
                objLeads.DivisionID = lnDivID
                lnCntID = objLeads.CreateRecordAddContactInfo()
                Session("CompID") = lngCMPID                                       'Set the Company Id in a session
                Session("DivID") = lnDivID
                Session("UserContactID") = lnCntID

                ''Saving CustomFields

                Dim dsViews As New DataView(dtGenericFormConfig)
                dsViews.RowFilter = "vcFieldType='C'"
                Dim i As Integer

                Dim dtCusTable As New DataTable
                dtCusTable.Columns.Add("fld_id")
                dtCusTable.Columns.Add("Value")
                Dim strdetails As String

                If dsViews.Count > 0 Then
                    For i = 0 To dsViews.Count - 1
                        dRow = dtCusTable.NewRow
                        dRow("fld_id") = Replace(dsViews(i).Item("numFormFieldId"), "C", "")
                        dRow("Value") = GetCustFldValue(dsViews(i).Item("vcAssociatedControlType"), dsViews(i).Item("vcDBColumnName"))
                        dtCusTable.Rows.Add(dRow)
                    Next



                    dtCusTable.TableName = "Table"
                    ds.Tables.Add(dtCusTable.Copy)
                    strdetails = ds.GetXml
                    ds.Tables.Remove(ds.Tables(0))

                    Dim ObjCusfld As New CustomFields
                    ObjCusfld.strDetails = strdetails
                    ObjCusfld.RecordId = lnCntID
                    ObjCusfld.locId = 4
                    ObjCusfld.SaveCustomFldsByRecId()
                End If
                dsViews.RowFilter = "vcFieldType='D'"
                dtCusTable.Rows.Clear()
                If dsViews.Count > 0 Then
                    For i = 0 To dsViews.Count - 1
                        dRow = dtCusTable.NewRow
                        dRow("fld_id") = Replace(dsViews(i).Item("numFormFieldId"), "C", "")
                        dRow("Value") = GetCustFldValue(dsViews(i).Item("vcAssociatedControlType"), dsViews(i).Item("vcDBColumnName"))
                        dtCusTable.Rows.Add(dRow)
                    Next



                    dtCusTable.TableName = "Table"
                    ds.Tables.Add(dtCusTable.Copy)
                    strdetails = ds.GetXml
                    ds.Tables.Remove(ds.Tables(0))

                    Dim ObjCusfld As New CustomFields
                    ObjCusfld.strDetails = strdetails
                    ObjCusfld.RecordId = lnDivID
                    ObjCusfld.locId = 1
                    ObjCusfld.SaveCustomFldsByRecId()
                End If


                Dim dtTable As New DataTable
                dtTable.Columns.Add("numAOIId")
                dtTable.Columns.Add("Status")
                If Not plhFormControlsAOI.FindControl("cbListAOI") Is Nothing Then
                    Dim chkList As CheckBoxList
                    chkList = plhFormControlsAOI.FindControl("cbListAOI")

                    For Each lstItem In chkList.Items
                        If lstItem.Selected = True Then
                            dRow = dtTable.NewRow
                            dRow("numAOIId") = lstItem.Value
                            dRow("Status") = 1
                            dtTable.Rows.Add(dRow)
                        End If
                    Next

                    dtTable.TableName = "Table"
                    ds.Tables.Add(dtTable.Copy)
                    strdetails = ds.GetXml
                    ds.Tables.Remove(ds.Tables(0))

                    Dim ObjContacts As New CContacts
                    ObjContacts.strAOI = strdetails
                    ObjContacts.ContactID = lnCntID
                    ObjContacts.SaveAOI()
                End If
                Try
                    Dim objAlerts As New CAlerts
                    Dim dtDetails As DataTable
                    objAlerts.AlertDTLID = 4 'Alert DTL ID for sending alerts in opportunities
                    objAlerts.DomainID = numDomainId
                    dtDetails = objAlerts.GetIndAlertDTL
                    If dtDetails.Rows.Count > 0 Then
                        If dtDetails.Rows(0).Item("tintAlertOn") = 1 Then
                            Dim dtEmailTemplate As DataTable
                            Dim objDocuments As New DocumentList
                            objDocuments.GenDocID = dtDetails.Rows(0).Item("numEmailTemplate")
                            objDocuments.DomainID = numDomainId
                            dtEmailTemplate = objDocuments.GetDocByGenDocID
                            If dtEmailTemplate.Rows.Count > 0 Then
                                Dim objProspects As New CProspects
                                Dim dtComInfo As DataTable
                                Dim dtConInfo As DataTable
                                objProspects.DivisionID = lnDivID
                                objProspects.DomainID = numDomainId
                                objProspects.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                                dtComInfo = objProspects.GetCompanyInfoForEdit

                                Dim objContacts As New CContacts
                                objContacts.ContactID = lnCntID
                                objContacts.DomainID = numDomainId
                                objContacts.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                                dtConInfo = objContacts.GetCntInfoForEdit1

                                Dim objSendMail As New clsSendEmail
                                Dim dtMergeFields As New DataTable
                                Dim drNew As DataRow
                                dtMergeFields.Columns.Add("Assignee")
                                dtMergeFields.Columns.Add("Organization")
                                dtMergeFields.Columns.Add("vcFirstName")
                                dtMergeFields.Columns.Add("vcLastName")
                                dtMergeFields.Columns.Add("Phone")
                                dtMergeFields.Columns.Add("Email")

                                drNew = dtMergeFields.NewRow
                                drNew("Organization") = dtComInfo.Rows(0).Item("vcCompanyName")
                                drNew("Assignee") = dtComInfo.Rows(0).Item("vcCreatedBy")
                                drNew("vcFirstName") = dtConInfo.Rows(0).Item("vcFirstName")
                                drNew("vcLastName") = dtConInfo.Rows(0).Item("vcLastName")
                                drNew("Phone") = dtConInfo.Rows(0).Item("numPhone")
                                drNew("Email") = dtConInfo.Rows(0).Item("vcEmail")
                                dtMergeFields.Rows.Add(drNew)
                                Dim objCommon As New CCommon
                                objCommon.byteMode = 0
                                objCommon.ContactID = objLeads.UserCntID
                                objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), IIf(dtDetails.Rows(0).Item("tintCCManager") = 1, objCommon.GetManagerEmail, ""), ConfigurationManager.AppSettings("FromAddress"), objCommon.GetContactsEmail, dtMergeFields)
                            End If
                        End If
                    End If
                Catch ex As Exception

                End Try
                If GetQueryStringVal(Request.QueryString("enc"), "Page") <> "" Then
                    hdRedirectURL.Value = GetQueryStringVal(Request.QueryString("enc"), "Page")
                End If

                Response.Redirect(hdRedirectURL.Value & "&DivID=" & Session("DivID"), True)
            Catch ex As Exception
            End Try
        End Sub

        Function GetCustFldValue(ByVal fldType As String, ByVal fld_Id As String) As String
            If fldType = "EditBox" Then
                Dim txt As TextBox
                txt = Page.FindControl(fld_Id)
                Return txt.Text
            ElseIf fldType = "SelectBox" Then
                Dim ddl As DropDownList
                ddl = Page.FindControl(fld_Id)
                Return CStr(IIf(ddl.SelectedItem.Value = "", 0, ddl.SelectedItem.Value))
            ElseIf fldType = "TextBox" Then
                Dim txt As TextBox
                txt = Page.FindControl(fld_Id)
                Return txt.Text
            End If
        End Function


        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This call sets the values for Lead Data in the Class variable
        ''' </summary>
        ''' <param name="objLeadBoxData">Represents the object.</param>
        ''' <param name="sPropertyName">Represents the name of the property in the class.</param>
        ''' <param name="sPropertyValue">Represents the value of the property to be set in the class.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/08/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Sub SetProperties(ByRef objLeadBoxData As FormGenericLeadBox, ByVal sPropertyName As String, ByVal sPropertyValue As String)
            Dim theType As Type = objLeadBoxData.GetType
            Dim myProperties() As PropertyInfo = theType.GetProperties((BindingFlags.Public Or BindingFlags.Instance))
            Dim fillObject As Object = objLeadBoxData
            Dim PropertyItem As PropertyInfo
            For Each PropertyItem In myProperties                                                       'Loop thru Writeonly public properties
                With PropertyItem
                    If PropertyItem.Name = sPropertyName Then                                           'Compare the class property name with that of the db column name
                        Select Case PropertyItem.PropertyType.ToString()                                'Selecting the datataype of teh property
                            Case "System.String"
                                PropertyItem.SetValue(fillObject, sPropertyValue, Nothing)
                            Case "System.Int64", "System.Int32", "System.Int16"                         'Integer type properties
                                If IsNumeric(sPropertyValue) Then
                                    PropertyItem.SetValue(fillObject, CInt(sPropertyValue), Nothing)    'Typecasting to integer before setting the value
                                End If
                            Case "System.Boolean"                                                       'Boolean properties
                                If IsBoolean(sPropertyValue) Then
                                    PropertyItem.SetValue(fillObject, CBool(sPropertyValue), Nothing)   'Typecasting to boolean before setting the value
                                End If
                        End Select
                        Exit Sub
                    End If
                End With
            Next
        End Sub
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This call returns True if the value passed is Boolean else False
        ''' </summary>
        ''' <param name="sTrg">Represents the value that is being evaluated for being Boolean.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/08/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function IsBoolean(ByVal sTrg As String) As Boolean
            Try
                Dim intValue As Boolean = Convert.ToBoolean(sTrg)                                   'Try Casting to a boolean
            Catch Ex As Exception
                Return False                                                                        'if it throws an error then return false to indicate that it is non boolean
            End Try
            Return True                                                                             'Test passed, its booelan
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This call returns True if the value passed is Integer else False
        ''' </summary>
        ''' <param name="sTrg">Represents the value that is being evaluated for being an Integer.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/08/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function IsNumeric(ByVal sTrg As String) As Boolean
            Try
                Dim intValue As Decimal = Convert.ToInt64(sTrg)                                     'Try Casting to a Integer
            Catch Ex As Exception
                Return False
            End Try
            Return True
        End Function

    End Class
End Namespace
