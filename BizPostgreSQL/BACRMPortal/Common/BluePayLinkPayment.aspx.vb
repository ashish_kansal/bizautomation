﻿Imports BACRM.BusinessLogic.Common
Imports System.Collections.Specialized
Imports BACRM.BusinessLogic.Opportunities
Imports System.Security.Cryptography

Namespace BACRM.UserInterface.Admin

    Public Class BluePayLinkPayment
        Inherits BACRMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Dim BluePaySuccessURL As String = ""
            Dim BluePayDeclineURL As String = ""

            Try
                Dim objCommon As New CCommon
                objCommon.DomainID = CCommon.ToLong(GetQueryStringVal("domainID"))

                Dim dt As DataTable = objCommon.GetDomainSettingValue("vcBluePaySuccessURL")
                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    BluePaySuccessURL = CCommon.ToString(dt.Rows(0)("vcBluePaySuccessURL"))
                End If

                dt = objCommon.GetDomainSettingValue("vcBluePayDeclineURL")
                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    BluePayDeclineURL = CCommon.ToString(dt.Rows(0)("vcBluePayDeclineURL"))
                End If

                If CCommon.ToString(GetQueryStringVal("Result")).ToLower() = "approved" Then
                    Dim objOpportunityAutomation As New OpportunityAutomation
                    Try
                        objOpportunityAutomation.BluepayCustomPaymentForm(CCommon.ToLong(GetQueryStringVal("ORDER_ID")), CCommon.ToLong(GetQueryStringVal("INVOICE_ID")), CCommon.ToString(GetQueryStringVal("PAYMENT_TYPE")), CCommon.ToString(GetQueryStringVal("RRNO")), CCommon.ToDecimal(GetQueryStringVal("AMOUNT")), CCommon.ToString(GetQueryStringVal("MESSAGE")), CCommon.ToString(GetQueryStringVal("CARD_TYPE")))
                    Catch ex As Exception
                        'DO NOT RAISE ERROR
                    End Try
                    If Not String.IsNullOrEmpty(BluePaySuccessURL) Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "PaymentSuccessful", "window.location.href=""" & BluePaySuccessURL & """;", True)
                    Else
                        ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "PaymentSuccessful", "alert('Your payment has been processed successfully'); window.location.href=""http://www.bizautomation.com"";", True)
                    End If
                Else
                    If Not String.IsNullOrEmpty(BluePayDeclineURL) Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "PaymentDeclined", "window.location.href=""" & BluePayDeclineURL & """;", True)
                    Else
                        ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "PaymentDeclined", "alert('Sorry, your payment failed'); window.location.href=""http://www.bizautomation.com"";", True)
                    End If

                    Dim objQueryStringValues As New QueryStringValues()
                    Throw New Exception(objQueryStringValues.Decrypt(Request.QueryString("enc")))
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)

                If Not String.IsNullOrEmpty(BluePayDeclineURL) Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "PaymentDeclined", "window.location.href=""" & BluePayDeclineURL & """;", True)
                Else
                    ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "PaymentDeclined", "alert('Sorry, your payment failed'); window.location.href=""http://www.bizautomation.com"";", True)
                End If
            End Try
        End Sub
    End Class
End Namespace