<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmAccounts.aspx.vb" Inherits="BACRMPortal.frmAccounts"%>
<%@ Register TagPrefix="menu1" TagName="Menu" src="../common/topbar.ascx" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebTab.v7.3, Version=7.3.20073.38, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebTab" TagPrefix="igtab" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
       <META http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
       <META http-equiv="Page-Exit" content="blendTrans(Duration=0.01)"> 
		<title>Accounts</title>
		<script language="javascript" type="text/javascript" >
		function FillAddress(a)
		{
		 document.Form1.all['lblAddress'].innerText=a;
		 return false;
		}
		function DeleteRecord()
		{
			if(confirm('Are you sure, you want to delete the selected record?'))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		function DeleteMessage()
		{
			alert("You Are not Authorized to Delete the Selected Record !");
			return false;
		}
		function fn_GoToURL(varURL)
		{
			
			if ((varURL!='') && (varURL.substr(0,7)=='http://') && (varURL.length > 7))
			{
				var LoWindow=window.open(varURL,"","");
				LoWindow.focus();
			}
			return false;
		}
		function fn_EditLabels(URL)
		{
			window.open(URL,'WebLinkLabel','width=500,height=70,status=no,scrollbar=yes,top=110,left=150');
			return false;
		}
		function Opentransfer(url)
		{
			window.open(url,'',"width=340,height=200,status=no,top=100,left=150");
			return false;	
		}
		function OpenLst(url)
		{
			window.open(url,'','toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
			return false;	
		}
		function OpenAdd(a)
		{
			window.open("../common/frmAddress.aspx?DivId="+a,'','toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=no,resizable=no')
			return false;
		}
		function OpenDocuments()
		{
			window.open("../admin/companydocuments.aspx",'','toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
			return false;
		}
		function OpenTo()
		{
			window.open("../Contacts/frmToCompanyAssociation.aspx",'','toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
			return false;
		}
		function OpenFrom()
		{
			window.open("../Contacts/frmFromCompanyAssociation.aspx",'','toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
			return false;
		}
		function OpenGroup(a,b,c)
		{
			window.open('../admin/addNewGroup.aspx?frm=prospectsdisp&CmpID='+a+'&DivID='+b+'&CRMType=1&CntID='+c,'','toolbar=no,titlebar=no,top=300,width=300,height=150,scrollbars=yes,resizable=yes')
			return false;
		}
	
	
		</script>
	</head>
	<body>
		
		<form id="Form1" method="post" runat="server">
		<asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
		<menu1:menu id="webmenu1" runat="server"></menu1:menu>
		
        <asp:updatepanel ID="updatepanel1" runat="server" ChildrenAstriggers="true" UpdateMode="Conditional" EnableViewState="true" >
		<ContentTemplate>
			<table width="100%" >
				<tr>
                    
					<td>
						<table id="tblMenu" borderColor="black" cellspacing="0" cellpadding="0" width="100%" border="0"
							runat="server">
							<tr>
								<td class="tr1" align="center"><b>Record Owner: </b>
									<asp:label id="lblRecordOwner" runat="server" ForeColor="Black"></asp:label></td>
								<td class="td1" width="1" height="18"></td>
								<td class="tr1" align="center"><b>Created By: </b>
									<asp:label id="lblCreatedBy" runat="server" ForeColor="Black"></asp:label></td>
								<td class="td1" width="1" height="18"></td>
								<td class="tr1" align="center"><b>Last Modified By: </b>
									<asp:label id="lblLastModifiedBy" runat="server" ForeColor="Black"></asp:label></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br>
			<table cellspacing="0" cellpadding="0" width="100%">
				<tr>
					<td>
						<table cellspacing="0" cellpadding="0" width="100%">
							<tr>
								
								<td class="normal1" align="center">Organization ID :
									<asp:label id="lblCustomerId" Runat="server"></asp:label></td>
								<td class="normal1" align="right">&nbsp;Universal Support Key:<asp:label id="lblSupportKey" Runat="server"></asp:label></td>
								<td><asp:button id="btnSupportKey" Runat="server" Text="Generate" CssClass="button"></asp:button></td>
								
								<td align="right" >
								<asp:button id="btnSave" Runat="server" Text="Save" CssClass="button" Width=60></asp:button>
								<asp:button id="btnSaveClose" Runat="server" Text="Save & close" CssClass="button" ></asp:button>
								<asp:button id="btnCancel" Runat="server" CssClass="button" Text="Cancel"></asp:button>
								</td>
								
							</tr>
						</table>
					</td>
				</tr>
				</table>
				<table cellSpacing="0" cellPadding="0" width="100%" align="center">
				<tr>
					<td></td>
				</tr>
				<tr>
					<td>
				<igtab:ultrawebtab  ImageDirectory="" style="POSITION:absolute;z-index:101;" id="uwOppTab" runat="server" ThreeDEffect="True" BorderStyle="Solid" Width="100%" BarHeight="0"  BorderWidth="0">
                     <DefaultTabStyle Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Arial" >
                        </DefaultTabStyle>
                        <RoundedImage LeftSideWidth="7" RightSideWidth="8" ShiftOfImages="0" SelectedImage="../images/ig_tab_winXPs3.gif" NormalImage="../images/ig_tab_winXP3.gif" HoverImage="../images/ig_tab_winXPs3.gif" FillStyle="LeftMergedWithCenter" ></RoundedImage>
                     <SelectedTabStyle Height="23px"  ForeColor="white">
                        </SelectedTabStyle>
                        <HoverTabStyle Height="23px" ForeColor="white"></HoverTabStyle>
                         <Tabs>
                             <igtab:Tab Text="&nbsp;&nbsp;&nbsp;Organization Detail&nbsp;&nbsp;&nbsp;" >
                                <ContentTemplate>
							                        <asp:table id="tblProspects" Runat="server" GridLines="None" BorderColor="black" Width="100%"
							                        Height="300" BorderWidth="1" cellspacing="0" cellpadding="0" CssClass="aspTable">
							                        <asp:tableRow>
								                        <asp:tableCell VerticalAlign="Top" >
									                        <br>
									                        <table id="tblDetails" runat="server" cellspacing="0" cellpadding="1" width="100%" border="0">
				                        <tr>
				                         <td rowspan="30" valign="top" >
									                                <img src="../images/Building-48.gif" />
											                    </td>
					                        <td class="normal1" align="right">
						                        <asp:label id="lblCustomer" Runat="server">Name</asp:label></td>
					                        <td>
						                        <asp:textbox id="txtrelationShipName" TabIndex=1 Runat="server" CssClass="signup" Width="180"></asp:textbox></td>
					                        <td class="normal1" align="right">Phone/Fax</td>
					                        <td>
						                        <asp:textbox id="txtPhone"  Runat="server" CssClass="signup" Width="90" TabIndex="7"></asp:textbox>
						                        <asp:textbox id="txtFax"  Runat="server" CssClass="signup" Width="90" TabIndex="8"></asp:textbox></td>
						                        <td class="normal1" align="right">Assigned To</td>
					                        <td class="normal1">
						                        <asp:dropdownlist id="ddlAssignedTo" CssClass="signup" Runat="server" Width="180px"></asp:dropdownlist>
					                        </td>
                        					
				                        </tr>
				                        <tr>
					                        <td class="normal1" align="right">
						                        Division</td>
					                        <td>
						                        <asp:textbox id="txtdivision" Runat="server" TabIndex=2 CssClass="signup" Width="180"></asp:textbox></td>
					                        <td class="normal1" align="right">
						                        Rating</td>
					                        <td>
						                        <asp:dropdownlist id="ddlRating" TabIndex=9 Runat="server" CssClass="signup" Width="180"></asp:dropdownlist></td>
					                        <td class="normal1" align="right">
						                        Employees</td>
					                        <td>
						                        <asp:dropdownlist id="ddlNoOfEmp" TabIndex=14 Runat="server" CssClass="signup" Width="180"></asp:dropdownlist></td>
                        				
				                        </tr>
				                        <tr>
					                        <td class="normal1" align="right">
						                        Industry</td>
					                        <td>
						                        <asp:dropdownlist id="ddlIndustry" TabIndex=3 Runat="server" CssClass="signup" Width="180"></asp:dropdownlist></td>
					                        <td class="normal1" align="right">
						                        Annual Revenue</td>
					                        <td>
						                        <asp:dropdownlist id="ddlAnnualRevenue" TabIndex=10 Runat="server" CssClass="signup" Width="180"></asp:dropdownlist></td>
						                        <td class="normal1" align="right">
						                        Org. Status</td>
					                        <td>
						                        <asp:dropdownlist id="ddlStatus" TabIndex=15 Runat="server" CssClass="signup" Width="180"></asp:dropdownlist></td>
                        					
				                        </tr>
				                        <tr>
					                        <td class="normal1" align="right">
						                        Relationship</td>
					                        <td>
						                        <asp:dropdownlist id="ddlRelationhip" TabIndex=4 AutoPostBack="true" Runat="server" CssClass="signup" Width="180"></asp:dropdownlist></td>
					                        <td class="normal1" align="right">
						                        Web</td>
                        						
					                        <td>
						                        <asp:textbox id="txtWeb" Runat="server" TabIndex=11 CssClass="signup" Width="145"></asp:textbox>&nbsp;
						                        <asp:button id="btnGo" Runat="server" TabIndex=12 CssClass="button" Text="Go" Width="25"></asp:button></td>
					                        <td class="normal1" align="right">
						                        Campaign</td>
					                        <td>
						                        <asp:DropDownList id="ddlCampaign" TabIndex=16 Runat="server" CssClass="signup" Width="180"></asp:DropDownList>
					                        </td>				
                        					
				                        </tr>
				                        <tr>
					                        <td class="normal1" align="right">
						                        Org. Profile</td>
					                        <td>
						                        <asp:dropdownlist id="ddlProfile" TabIndex=5 Runat="server" CssClass="signup" Width="180"></asp:dropdownlist></td>
					                        <td class="normal1" align="right">
						                        Territory</td>
					                        <td>
						                        <asp:dropdownlist id="ddlTerriory" TabIndex=13 Runat="server" CssClass="signup" Width="180"></asp:dropdownlist></td>
                        				
				                            <td class="normal1" align="right">Follow-up Status</td>
													                        <td class="normal1">
														                        <asp:dropdownlist id="ddlFollow" Runat="server" CssClass="signup" Width="180" TabIndex="19"></asp:dropdownlist>
				                            </td>
                        					
				                        </tr>
				                        <tr>
					                        <td class="normal1" align="right">
						                        Info. Source</td>
					                        <td>
						                        <asp:dropdownlist id="ddlInfoSource" TabIndex=6 Runat="server" CssClass="signup" Width="180"></asp:dropdownlist>
					                        </td>
					                        <td class="normal1" align="right">Private</td>
					                        <td ><asp:checkbox id="chkPrivate" TabIndex=17 Runat="server"></asp:checkbox></td>
					                        <td></td>
					                        <td class="normal1">
						                        Associations:&nbsp;&nbsp;&nbsp;&nbsp;
						                        <asp:hyperlink id="hplTo" TabIndex=18 Runat="server" CssClass="hyperlink">
							                        <font color="#180073">To</font></asp:hyperlink>(<asp:Label runat="server" Text="" ID="lblAssoCountT"></asp:Label>)&nbsp;/&nbsp;
						                        <asp:hyperlink id="hplFrom" TabIndex=19 Runat="server" CssClass="hyperlink">
							                        <font color="#180073">From</font></asp:hyperlink>(<asp:Label runat="server" Text="" ID="lblAssoCountF"></asp:Label>)
					                        </td>
				                        </tr>
				                        <tr>
					                        <td class="normal1" align="right">
						                        <asp:HyperLink id="hplAddress" Runat="server" CssClass="hyperlink">
							                        <font color="#180073">Address</font></asp:HyperLink></td>
					                        <td class="normal1" colSpan="5">
						                        <asp:label id="lblAddress" runat="server" Width="100%" text=""></asp:label></td>
				                        </tr>
			                        </table>
			                        <table cellspacing="1" cellpadding="1" width="100%" border="0">
				                        <tr>
					                        <td class="normal1" align="right">Comments</td>
					                        <td class="normal1" colSpan="3">
						                        <asp:textbox id="txtComments" runat="server" CssClass="signup" Width="750" TextMode="MultiLine"
							                        Rows="3"></asp:textbox></td>
				                        </tr>
				                        <tr>
					                        <td class="normal7" align="right">Web Links
					                        </td>
					                        <td>
						                        <asp:Button id="btnEditWebLnk" Runat="server" Text="Edit Web Links" CssClass="button"></asp:Button>
					                        </td>
					                        <td>
					                        </td>
					                        <td>
					                        </td>
				                        </tr>
				                        <tr>
					                        <td class="TEXT" align="right">
						                        <asp:label id="lblWebLink1" Runat="server"></asp:label></td>
					                        <td>
						                        <asp:textbox id="txtWebLink1" runat="server" cssclass="signup" width="250px" value="http://"></asp:textbox>&nbsp;
						                        <asp:Button id="btnWebGo1" Runat="server" Text="Go" Width="25" CssClass="button"></asp:Button>
					                        </td>
					                        <td class="TEXT" align="right">
						                        <asp:label id="lblWebLink2" Runat="server"></asp:label></td>
					                        <td>
						                        <asp:textbox id="txtWebLink2" runat="server" cssclass="signup" width="250px" value="http://"></asp:textbox>&nbsp;
						                        <asp:Button id="btnWebGo2" Runat="server" Text="Go" Width="25" CssClass="button"></asp:Button>
					                        </td>
				                        </tr>
				                        <tr>
					                        <td class="TEXT" align="right">
						                        <asp:label id="lblWebLink3" Runat="server"></asp:label></td>
					                        <td>
						                        <asp:textbox id="txtWebLink3" runat="server" cssclass="signup" width="250px" value="http://"></asp:textbox>&nbsp;
						                        <asp:Button id="btnWebGo3" Runat="server" Text="Go" Width="25" CssClass="button"></asp:Button>
					                        </td>
					                        <td class="TEXT" align="right">
						                        <asp:label id="lblWebLink4" Runat="server"></asp:label></td>
					                        <td>
						                        <asp:textbox id="txtWebLink4" runat="server" cssclass="signup" width="250px" value="http://"></asp:textbox>&nbsp;
						                        <asp:Button id="btnWebGo4" Runat="server" Text="Go" Width="25" CssClass="button"></asp:Button>
					                        </td>
				                        </tr>
			                        </table>
									                        <br>
								                        </asp:tableCell>
							                        </asp:tableRow>
						                        </asp:table>
						                        </ContentTemplate>
						   </igtab:Tab>
                         </Tabs>
                         </igtab:ultrawebtab>
						<</td>
				</tr>
			</table>
			<table width="100%">
				<tr>
					<td class="normal4" align="center"><asp:literal id="litMessage" Runat="server"></asp:literal></td>
				</tr>
			</table>
			<asp:textbox id="txtHidden" style="DISPLAY: none" Runat="server"></asp:textbox><asp:textbox id="txtrOwner" style="DISPLAY: none" Runat="server"></asp:textbox>
			</ContentTemplate>
			</asp:UpdatePanel>
			                            <table width="100%" runat="server"  visible="false" >
												<tr>
													    <td class="normal1" align="right">Billing Terms</td>
													    <td class="normal1">
													        <asp:dropdownlist id="ddlCredit" Runat="server" CssClass="signup" Width="130" Visible="False"></asp:dropdownlist>
														    <asp:CheckBox ID="chkBillinTerms" Runat="server"></asp:CheckBox>&nbsp; Msg.
														    <asp:TextBox ID="txtSummary"  Runat="server" CssClass="signup"></asp:TextBox></td>
													    <td class="normal1" align="right">
														    Net</td>
													    <td class="normal1">
														    <asp:TextBox ID="txtNetdays" Runat="server" Width="40" CssClass="signup"></asp:TextBox>
														    days
														    <asp:RadioButton ID="radPlus" GroupName="rad" Checked="true" Runat="server" Text="Plus"></asp:RadioButton>
														    <asp:RadioButton ID="radMinus" Runat="server" GroupName="rad" Text="Minus"></asp:RadioButton>&nbsp;
														    <asp:TextBox ID="txtInterest" Runat="server" Width="40" CssClass="signup"></asp:TextBox>
														    %
													    </td>
													    <td class="normal1" align="right">Tax
													    </td>
													    <td class="normal1">
														    <asp:TextBox ID="txtTaxPercentage" Runat="server" Width="40" CssClass="signup"></asp:TextBox>%</td>
												</tr>
										</table> 
		</form>
	</body>
</html>
