﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Survey
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports System.Reflection
Imports BACRM.BusinessLogic.Tracking
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Workflow
Public Class frmLandingPage
    Inherits BACRMPage
    Private numDomainId As Integer
    Dim strRedirect As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            numDomainId = GetQueryStringVal( "D")                                          'Get the Domain Id from the url
            Dim numSurId As Integer                                                         'Declare the integer for the Survey Id
            numSurId = GetQueryStringVal( "numSurId")                                      'Pick up the Survey Id from the Query String
            hdRespondentID.Value = GetQueryStringVal( "numRespondantID")                   'Pick up the Respondent Id from the Query String
            hdSurId.Value = numSurId                                                        'Set the Survey Id in local variable

            If Not IsPostBack Then                                                          'The form loads for the first time
                Dim objSurvey As New SurveyAdministration                               'Create an object of Survey Administration Class
                objSurvey.SurveyId = hdSurId.Value                                      'Set the Survey ID
                objSurvey.DomainId = numDomainId                                        'Set the Domain Id

                Dim dtSurveyMasterInfo As DataTable                                 'Declare a Datatable
                dtSurveyMasterInfo = objSurvey.getSurveyMasterData()                'Get the Survey Master information in a datatable

                rdbLandingPage.Items.Add(New ListItem(ReSetFromXML(dtSurveyMasterInfo.Rows(0).Item("vcLandingPageYes")), 0))
                rdbLandingPage.Items.Add(New ListItem(ReSetFromXML(dtSurveyMasterInfo.Rows(0).Item("vcLandingPageNo")), 1))
                rdbLandingPage.SelectedIndex = 0

                objSurvey.DomainId = numDomainId
                objSurvey.SurveyId = hdSurId.Value
                objSurvey.PageType = 2
                Dim dsPage As DataSet
                dsPage = objSurvey.GetSurveyTemplate()
                Dim dt As DataTable = dsPage.Tables(0)

                If dsPage.Tables(0).Rows.Count > 0 Then

                    Dim length1 As Integer = dt.Rows(0)("txtTemplate").ToString().IndexOf("#Form#")

                    lblHeader.Text = HttpUtility.HtmlDecode(CCommon.ToString(dt.Rows(0)("txtTemplate").ToString().Substring(0, length1)))
                    lblFooter.Text = HttpUtility.HtmlDecode(CCommon.ToString(dt.Rows(0)("txtTemplate").ToString().Substring(length1 + 6)))

                    If dsPage.Tables(1).Rows.Count > 0 Then
                        MasterCSS.Visible = False
                        For Each dr As DataRow In dsPage.Tables(1).Rows
                            CCommon.AddStyleSheetLink("../Documents/Docs/" & numDomainId & "/Survey/" & dr("StyleFileName").ToString, Me.Page)
                        Next
                    Else
                        MasterCSS.Visible = True
                    End If

                    If dsPage.Tables(2).Rows.Count > 0 Then
                        For Each dr As DataRow In dsPage.Tables(2).Rows
                            CCommon.AddJavascriptLink("../Documents/Docs/" & numDomainId & "/Survey/" & dr("StyleFileName").ToString, Me.Page)
                        Next
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Function ReSetFromXML(ByVal sValue As String) As String
        Try
            Return Replace(Replace(Replace(Replace(sValue, "&amp;", "&"), "&#39;", "'"), "&lt;", "<"), "&gt;", ">")
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Function MakeSafeForXML(ByVal sValue As String) As String
        Try
            Return Replace(Replace(Replace(Replace(Replace(sValue, "'", "&#39;"), """", "&#39;&#39;"), "&", "&amp;"), "<", "&lt;"), ">", "&gt;")
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Protected Sub btnContinue_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnContinue.Click
        Try
            If rdbLandingPage.SelectedValue = 0 Then
                Response.Redirect("frmSurveyExecutionSurvey.aspx?numSurId=" & hdSurId.Value & "&numRespondantID=" & hdRespondentID.Value & "&D=" & numDomainId, True) 'Redirect to the Survey
            Else
                Dim objSurvey As New SurveyAdministration                               'Create an object of Survey Administration Class
                objSurvey.SurveyId = hdSurId.Value                                      'Set the Survey ID
                objSurvey.DomainId = numDomainId                                        'Set the Domain Id

                Dim bitEmailTemplateContact, bitEmailTemplateRecordOwner As Boolean
                Dim numEmailTemplate1Id, numEmailTemplate2Id As Int32
                Dim numSurveyRespondentContactId As Integer                             'The contact id Is stored after creation of a lead
                Dim numSSurveyRespondentCompanyId As Integer
                Dim numSurveyRespondentDivisionId As Integer

                If hdSurId.Value > 0 Then                                               'Form is opened for a particular survey only
                    Dim dtSurveyMasterInfo As DataTable                                 'Declare a Datatable
                    dtSurveyMasterInfo = objSurvey.getSurveyMasterData()                'Get the Survey Master information in a datatable
                    bitEmailTemplateContact = dtSurveyMasterInfo.Rows(0).Item("bitEmailTemplateContact")
                    bitEmailTemplateRecordOwner = dtSurveyMasterInfo.Rows(0).Item("bitEmailTemplateRecordOwner")

                    numEmailTemplate1Id = dtSurveyMasterInfo.Rows(0).Item("numEmailTemplate1Id")
                    numEmailTemplate2Id = dtSurveyMasterInfo.Rows(0).Item("numEmailTemplate2Id")

                    strRedirect = dtSurveyMasterInfo.Rows(0).Item("vcRedirectURL")

                    If (dtSurveyMasterInfo.Rows(0).Item("bitRelationshipProfile") = True) Then
                        hdRelationShip.Value = IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("numRelationShipId")), 0, dtSurveyMasterInfo.Rows(0).Item("numRelationShipId"))
                        hdCRMType.Value = IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("tIntCRMType")), 0, dtSurveyMasterInfo.Rows(0).Item("tIntCRMType"))
                        hdGroupId.Value = IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("numGrpId")), 1, dtSurveyMasterInfo.Rows(0).Item("numGrpId"))
                        hdRecOwner.Value = IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("numRecOwner")), 0, dtSurveyMasterInfo.Rows(0).Item("numRecOwner"))
                        hdProfile.Value = IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("numProfileId")), 0, dtSurveyMasterInfo.Rows(0).Item("numProfileId"))

                        hdCreateDBEntry.Value = 1
                    ElseIf (dtSurveyMasterInfo.Rows(0).Item("bitCreateRecord") = True) Then
                        Dim dtSurveyDefault As DataTable
                        dtSurveyDefault = objSurvey.getDefaultSurveyRating()

                        If dtSurveyDefault.Rows.Count > 0 Then
                            hdRelationShip.Value = IIf(IsDBNull(dtSurveyDefault.Rows(0).Item("numRelationShipId")), 0, dtSurveyDefault.Rows(0).Item("numRelationShipId"))
                            hdCRMType.Value = IIf(IsDBNull(dtSurveyDefault.Rows(0).Item("tIntCRMType")), 0, dtSurveyDefault.Rows(0).Item("tIntCRMType"))
                            hdGroupId.Value = IIf(IsDBNull(dtSurveyDefault.Rows(0).Item("numGrpId")), 1, dtSurveyDefault.Rows(0).Item("numGrpId"))
                            hdRecOwner.Value = IIf(IsDBNull(dtSurveyDefault.Rows(0).Item("numRecOwner")), 0, dtSurveyDefault.Rows(0).Item("numRecOwner"))
                            hdProfile.Value = IIf(IsDBNull(dtSurveyDefault.Rows(0).Item("numProfileId")), 0, dtSurveyDefault.Rows(0).Item("numProfileId"))

                            hdCreateDBEntry.Value = 1
                        End If
                    End If

                End If


                If hdCreateDBEntry.Value = 1 Then
                    Dim objLeads As New CLeads

                    '' Added By Anoop 
                    Dim ds As New DataSet
                    Dim dRow As DataRow
                    'Dim objLeads As New CLeads
                    Dim dtGenericFormConfig As DataTable
                    dtGenericFormConfig = Session("dtSurveyRegDTL")
                    objLeads.GroupID = hdGroupId.Value
                    objLeads.ContactType = 70
                    objLeads.CRMType = hdCRMType.Value
                    objLeads.CompanyType = hdRelationShip.Value
                    objLeads.DivisionName = "-"

                    Dim objBusinessClass As New BusinessClass
                    Dim importWiz As New ImportWizard

                    Dim vcAssociatedControlType As String
                    Dim vcFieldType As String
                    For Each dr As DataRow In dtGenericFormConfig.Rows
                        vcFieldType = dr("vcFieldType")
                        If vcFieldType = "R" Then
                            vcAssociatedControlType = dr("vcAssociatedControlType")
                            Select Case vcAssociatedControlType
                                Case "TextBox"
                                    If dr("vcDbColumnText") <> "" Then AssignValuesEditBox(objLeads, dr("vcDbColumnText"), dr("vcDbColumnName"))
                                Case "SelectBox"
                                    If dr("vcDbColumnText") <> "" Then
                                        AssignValuesSelectBox(objLeads, dr("vcDbColumnText"), dr("vcDbColumnName"))
                                    Else : dr("vcDbColumnText") = 0
                                    End If
                                Case "TextArea"
                                    If dr("vcDbColumnText") <> "" Then AssignValuesTextBox(objLeads, dr("vcDbColumnText"), dr("vcDbColumnName"))
                            End Select
                        End If
                    Next
                    objLeads.DomainID = numDomainId

                    objBusinessClass.IPNo = objBusinessClass.GetIPNumberFromIPAddress(Request.UserHostAddress)
                    Dim lngCountryID As Long = importWiz.GetStateAndCountry(0, objBusinessClass.GetCountryFromIPaddress(ConfigurationManager.AppSettings("ConnectionString")), numDomainId)
                    If lngCountryID > 0 Then
                        objLeads.Country = lngCountryID
                    End If

                    If hdRecOwner.Value = "" Then
                        hdRecOwner.Value = 0
                    End If
                    If hdRecOwner.Value = 0 Then
                        Dim objAutoRoutRles As New AutoRoutingRules
                        objAutoRoutRles.DomainID = numDomainId
                        dtGenericFormConfig.TableName = "Table"
                        ds.Tables.Add(dtGenericFormConfig.Copy)
                        objAutoRoutRles.strValues = ds.GetXml
                        ds.Tables.Remove(ds.Tables(0))
                        hdRecOwner.Value = objAutoRoutRles.GetRecordOwner
                    End If

                    objLeads.AssignedTo = hdRecOwner.Value
                    objLeads.UserCntID = hdRecOwner.Value
                    objLeads.CompanyID = objLeads.CreateRecordCompanyInfo()
                    numSSurveyRespondentCompanyId = objLeads.CompanyID
                    objLeads.Profile = hdProfile.Value
                    objLeads.DivisionID = objLeads.CreateRecordDivisionsInfo
                    numSurveyRespondentDivisionId = objLeads.DivisionID
                    numSurveyRespondentContactId = objLeads.CreateRecordAddContactInfo()
                    Session("DivID") = objLeads.DivisionID 'Addded by chintan reason: will be used in tracking leads in marketing section
                    'add cookie to browser of Division id which can be used to track future visit of same user
                    CCommon.AddDivisionCookieToBrowser(objLeads.DivisionID)

                    Dim dsViews As New DataView(dtGenericFormConfig)
                    dsViews.RowFilter = "vcFieldType='C'"
                    Dim i As Integer

                    Dim dtCusTable As New DataTable
                    dtCusTable.Columns.Add("FldDTLID")
                    dtCusTable.Columns.Add("fld_id")
                    dtCusTable.Columns.Add("Value")
                    Dim strdetails As String

                    If dsViews.Count > 0 Then
                        For i = 0 To dsViews.Count - 1
                            dRow = dtCusTable.NewRow
                            dRow("FldDTLID") = 0
                            dRow("fld_id") = Replace(dsViews(i).Item("numFormFieldId"), "C", "")
                            dRow("Value") = dsViews(i).Item("vcDbColumnText")
                            dtCusTable.Rows.Add(dRow)
                        Next

                        dtCusTable.TableName = "Table"
                        ds.Tables.Add(dtCusTable.Copy)
                        strdetails = ds.GetXml
                        ds.Tables.Remove(ds.Tables(0))

                        Dim ObjCusfld As New CustomFields
                        ObjCusfld.strDetails = strdetails
                        ObjCusfld.RecordId = objLeads.ContactID
                        ObjCusfld.locId = 4
                        ObjCusfld.SaveCustomFldsByRecId()
                    End If
                    dsViews.RowFilter = "vcFieldType='D'"
                    dtCusTable.Rows.Clear()
                    If dsViews.Count > 0 Then
                        For i = 0 To dsViews.Count - 1
                            dRow = dtCusTable.NewRow
                            dRow("FldDTLID") = 0
                            dRow("fld_id") = Replace(dsViews(i).Item("numFormFieldId"), "C", "")
                            dRow("Value") = dsViews(i).Item("vcDbColumnText")
                            dtCusTable.Rows.Add(dRow)
                        Next

                        dtCusTable.TableName = "Table"
                        ds.Tables.Add(dtCusTable.Copy)
                        strdetails = ds.GetXml
                        ds.Tables.Remove(ds.Tables(0))

                        Dim ObjCusfld As New CustomFields
                        ObjCusfld.strDetails = strdetails
                        ObjCusfld.RecordId = objLeads.DivisionID
                        ObjCusfld.locId = 1
                        ObjCusfld.SaveCustomFldsByRecId()
                    End If

                    'Added By Sachin Sadhu||Date:25thAug2014
                    'Purpose :To Added Organization data in work Flow queue based on created Rules
                    '          Using Change tracking
                    Dim objWfA As New Workflow()
                    objWfA.DomainID = numDomainId
                    objWfA.UserCntID = Session("UserContactID")
                    objWfA.RecordID = CCommon.ToLong(Session("DivID"))
                    objWfA.SaveWFOrganizationQueue()
                    'ss//end of code

                    'Purpose :To Added Contact data in work Flow queue based on created Rules
                    '         Using Change tracking
                    Dim objWF As New Workflow()
                    objWF.DomainID = numDomainId
                    objWF.UserCntID = Session("UserContactID")
                    objWF.RecordID = objLeads.ContactID
                    objWF.SaveWFContactQueue()
                    ' ss//end of code
                    Session("SMTPServerIntegration") = True

                    If bitEmailTemplateContact Then
                        If objLeads.Email IsNot Nothing Then
                            If objLeads.Email.Length > 0 Then
                                'Commented by :Sachin Sadhu||Date:25thSept2014
                                'Purpose : To Replace with WFA(part of Email Alerts)
                                'CAlerts.SendWebLeadAlerts(numEmailTemplate1Id, numSurveyRespondentDivisionId, numSurveyRespondentContactId, numSurveyRespondentContactId, hdRecOwner.Value, numDomainId)
                                'end of code
                            End If
                        End If
                    End If

                    If bitEmailTemplateRecordOwner Then
                        'Commented by :Sachin Sadhu||Date:25thSept2014
                        'Purpose : To Replace with WFA(part of Email Alerts)
                        'CAlerts.SendWebLeadAlerts(numEmailTemplate2Id, numSurveyRespondentDivisionId, numSurveyRespondentContactId, hdRecOwner.Value, hdRecOwner.Value, numDomainId)
                        'end of code
                    End If
                End If
                Session("dtSurveyRegDTL") = Nothing

                Response.Redirect(strRedirect, True) 'Redirect to the Other Page
                End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    'Function SendAlerts(ByVal lngAlertDtlID As Long, ByVal lnDivID As Long, ByVal lnCntID As Long, ByVal lngUserContactID As Long, Optional ByVal lngRecordOwner As Long = 0)
    '    Try
    '        Dim dtEmailTemplate As DataTable
    '        Dim objDocuments As New DocumentList
    '        objDocuments.GenDocID = lngAlertDtlID
    '        objDocuments.DomainID = numDomainId
    '        dtEmailTemplate = objDocuments.GetDocByGenDocID
    '        If dtEmailTemplate.Rows.Count > 0 Then
    '            Dim objProspects As New CProspects
    '            Dim dtComInfo As DataTable
    '            Dim dtConInfo As DataTable
    '            objProspects.DivisionID = lnDivID
    '            objProspects.DomainID = numDomainId
    '            objProspects.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
    '            dtComInfo = objProspects.GetCompanyInfoForEdit

    '            Dim objContacts As New CContacts
    '            objContacts.ContactID = lnCntID
    '            objContacts.DomainID = numDomainId
    '            objContacts.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
    '            dtConInfo = objContacts.GetCntInfoForEdit1

    '            Dim objSendMail As New Email
    '            Dim dtMergeFields As New DataTable
    '            Dim drNew As DataRow
    '            dtMergeFields.Columns.Add("Assignee")
    '            dtMergeFields.Columns.Add("Organization")
    '            dtMergeFields.Columns.Add("vcFirstName")
    '            dtMergeFields.Columns.Add("vcLastName")
    '            dtMergeFields.Columns.Add("Phone")
    '            dtMergeFields.Columns.Add("Email")
    '            If dtConInfo.Rows.Count > 0 Then    ' Added this  code which prevents send alert in case the contact is repeated and to avoid error
    '                drNew = dtMergeFields.NewRow
    '                drNew("Organization") = dtComInfo.Rows(0).Item("vcCompanyName")
    '                drNew("Assignee") = dtComInfo.Rows(0).Item("vcCreatedBy")
    '                drNew("vcFirstName") = dtConInfo.Rows(0).Item("vcFirstName")
    '                drNew("vcLastName") = dtConInfo.Rows(0).Item("vcLastName")
    '                drNew("Phone") = dtConInfo.Rows(0).Item("numPhone")
    '                drNew("Email") = dtConInfo.Rows(0).Item("vcEmail")
    '                dtMergeFields.Rows.Add(drNew)

    '                Dim objCommon As New CCommon
    '                objCommon.ContactID = lngUserContactID
    '                objCommon.byteMode = 0
    '                If lngRecordOwner > 0 Then
    '                    Dim objCommon2 As New CCommon
    '                    objCommon2.ContactID = lngRecordOwner
    '                    Dim strFrmEmail As String = objCommon2.GetContactsEmail
    '                    objSendMail.AddEmailToJob(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), "", strFrmEmail, objCommon.GetContactsEmail, dtMergeFields, "", "", numDomainId)
    '                Else
    '                    objSendMail.AddEmailToJob(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), "", ConfigurationManager.AppSettings("FromAddress"), objCommon.GetContactsEmail, dtMergeFields, "", "", numDomainId)
    '                End If

    '            End If
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try

    'End Function

End Class