<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmNewOrganization.aspx.vb" Inherits="BACRMPortal.frmNewOrganization" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Relationship</title>
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <script>
    	function Save()
					{
						if (document.form1.txtFirstName.value=="")
						{
							alert("Enter First Name")
							document.form1.txtFirstName.focus()
							return false;
						}
						if (document.form1.txtLastName.value=="")
						{
							alert("Enter Last Name")
							document.form1.txtLastName.focus()
							return false;
						}
						if (document.form1.ddlGroup!=null)
						{
						    if (document.form1.ddlGroup.value==0)
						    {
							    alert("Select Group ")
							    document.form1.ddlGroup.focus()
							    return false;
						    }
						}
					}
					function Focus()
					{
						document.form1.txtCompany.focus();
					}
						function Close()
		                {
		                    window.close()
		                    return false;
		                }
		                function Back()
		                {
		                    window.location.href="../Common/frmSelRelationship.aspx"
		                    return false;
		                }
		</script>
</head>
<body onload="Focus()">
    <form id="form1" runat="server">
    <br />
    <TABLE id="Table1" height="2" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td valign="bottom">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;New 
									<asp:Label ID="lblRelationship" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td align="right">&nbsp;
						<asp:button id="btnSave" Runat="server" CssClass="button" Text="Save &amp; Close"></asp:button>
						<asp:button id="btnBack" Runat="server" CssClass="button" Text="Back" Width="50"></asp:button>
						<asp:button id="btnClose" Runat="server" CssClass="button" Text="Close" Width="50"></asp:button>
					</td>
				</tr>
			</TABLE>
			<asp:table id="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" Runat="server" Width="100%" CssClass="aspTable"
				BorderColor="black" GridLines="None">
				<asp:TableRow>
					<asp:TableCell>
						<br>
					
						<table cellSpacing="2" cellPadding="0" width="650" border="0">
								<tr>
								<td rowspan="10" valign="top" style="background-image:url(../images/Building-48.gif);background-repeat:no-repeat;background-position-x:20px;background-position-y:10px">
                                     <br />
                                     &nbsp;<img src="../images/Greenman-32.gif" style="position:relative; left: 16px; top: 14px;" /><br />
                                   
											</td>
								<td class="normal1" align="right"><asp:Label ID="lblCustomer" runat="server"></asp:Label>&nbsp;
								</td>
								<td colSpan="3">
									<asp:textbox id="txtCompany" runat="server" Width="200" MaxLength="50" cssclass="signup"></asp:textbox></td>
							</tr>
							<tr>
								<td class="normal1" align="right">First Name<FONT color="red">*</FONT></td>
								<td colSpan="3">
									<asp:textbox id="txtFirstName" runat="server" Width="200" MaxLength="50" CssClass="signup"></asp:textbox></td>
							</tr>
							<tr>
								<td class="normal1" align="right">Last Name<FONT color="red">*</FONT></td>
								<td colSpan="3"><asp:textbox id="txtLastName" runat="server" Width="200" MaxLength="50" CssClass="signup"></asp:textbox>
								</td>
							</tr>
						
							<tr>
								<td class="normal1" align="right">Info. Source</td>
								<td colSpan="3"><asp:dropdownlist cssclass="signup" id="ddlInfoSource" runat="server" Width="200">
							
									</asp:dropdownlist></td>
							</tr>
							<tr>
								<td class="normal1" align="right">Phone</td>
								<td colSpan="3" class="normal1">
									<asp:textbox id="txtPhone" runat="server" Width="150px" MaxLength="50" cssclass="signup"></asp:textbox>&nbsp;
									&nbsp;Ext&nbsp;
									<asp:textbox id="txtextn" runat="server" Width="70" MaxLength="6" cssclass="signup"></asp:textbox></td>
							</tr>
							<tr>
								<td class="normal1" style="WIDTH: 124px" align="right" colSpan="1" rowSpan="1">Email</td>
								<td colSpan="3">
									<asp:textbox id="txtEmail" runat="server" Width="200" MaxLength="50" CssClass="signup"></asp:textbox></td>
							</tr>
							
							<tr id="trRelationship" runat="server" >
								<td class="normal1" align="right">Relationship</td>
								<td colSpan="3">
									<asp:dropdownlist id="ddlRelationShip" runat="server" Width="200" cssclass="signup"></asp:dropdownlist></td>
							</tr>
							<tr>
								<td class="normal1" align="right">Territory</td>
								<td colSpan="3">
									<asp:dropdownlist id="ddlTerritory" runat="server" Width="200" cssclass="signup">
								
									</asp:dropdownlist></td>
							</tr>
							<tr id="trGroups" runat="server" visible=false >
								<td class="normal1" align="right">Group<FONT color="red">*</FONT>
								</td>
								<td colSpan="3">
									<asp:dropdownlist id="ddlGroup" runat="server" Width="200" cssclass="signup"></asp:dropdownlist>&nbsp;
								</td>
							</tr>
						</table>
						<br />
					</asp:TableCell>
				    </asp:TableRow>
				</asp:table>
    </form>
</body>
</html>
