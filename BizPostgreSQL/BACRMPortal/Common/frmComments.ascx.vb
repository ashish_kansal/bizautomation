﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Case

Public Class frmComments
    Inherits BACRMUserControl

    Dim objCommon As New CCommon

    Private _CaseID As Long
    Public Property CaseID() As Long
        Get
            Return _CaseID
        End Get
        Set(ByVal value As Long)
            _CaseID = value
        End Set
    End Property
    Private _StageID As Long
    Public Property StageID() As Long
        Get
            Return _StageID
        End Get
        Set(ByVal value As Long)
            _StageID = value
        End Set
    End Property


    Private _HasEditRights As Boolean
    Public Property HasEditRights() As Boolean
        Get
            Return _HasEditRights
        End Get
        Set(ByVal value As Boolean)
            _HasEditRights = value
        End Set
    End Property




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If StageID <> 0 Or CaseID <> 0 Then
                If Session("show") = 1 Then
                    tblComments.Visible = True
                    ShowComments()
                End If
                If HasEditRights = False Then
                    lnlbtnPost.Visible = False
                End If

                btnSubmit.Attributes.Add("onclick", "return SaveComment()")
            End If


        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Sub
    Private Sub lnkbtnNoofCom_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles lnkbtnNoofCom.Command
        Try
            If e.CommandName = "Show" Then
                tblComments.Visible = True
                pnlPostComment.Visible = False
                ShowComments()
                Session("show") = 1
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnlbtnPost_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles lnlbtnPost.Command
        Try
            If e.CommandName = "Post" Then
                tblComments.Visible = False
                pnlPostComment.Visible = True
                txtComments.Text = ""
                txtHeading.Text = ""
                txtLstUptCom.Text = ""
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub ShowComments()
        Try
            tblComments.Rows.Clear()
            Dim dtCaseComments As DataTable
            objCommon.CaseID = _CaseID
            objCommon.StageID = _StageID
            objCommon.byteMode = 0
            objCommon.DomainID = Session("DomainId")
            dtCaseComments = objCommon.ManageComments
            hdnCommentCount.Value = dtCaseComments.Rows.Count
            lnkbtnNoofCom.Text = "  Comments (" & hdnCommentCount.Value.ToString & " )"
            Dim i As Integer
            If dtCaseComments.Rows.Count > 0 Then
                Dim tblRow As TableRow
                Dim tblCell As TableCell
                Dim lbl As Label
                Dim btnDelete As Button
                Dim btnUpdate As Button
                Dim k As Integer = 0
                For i = 0 To dtCaseComments.Rows.Count - 1
                    tblRow = New TableRow
                    If k <> 1 Then
                        tblRow.CssClass = "tr1"
                    Else : tblRow.CssClass = "tr2"
                    End If
                    tblCell = New TableCell
                    tblCell.Text = dtCaseComments.Rows(i).Item("vcHeading")
                    tblCell.CssClass = "text_bold"
                    tblRow.Cells.Add(tblCell)
                    tblCell = New TableCell
                    If dtCaseComments.Rows(i).Item("numCommentID") = IIf(txtLstUptCom.Text = "", 0, txtLstUptCom.Text) Then
                        btnUpdate = New Button
                        btnUpdate.Text = "Edit"
                        AddHandler btnUpdate.Click, AddressOf Me.btnUpdate_Click
                        btnUpdate.Width = Unit.Pixel(50)
                        btnUpdate.ID = "btnUpdate~" & dtCaseComments.Rows(i).Item("numCommentID")
                        btnUpdate.CssClass = "button"
                        btnUpdate.Visible = HasEditRights
                        tblCell.Controls.Add(btnUpdate)
                    End If
                    lbl = New Label
                    lbl.Text = "&nbsp;&nbsp;"
                    tblCell.Controls.Add(lbl)

                    btnDelete = New Button
                    btnDelete.Text = "Delete"
                    AddHandler btnDelete.Click, AddressOf Me.btnDeleteComm_Click
                    btnDelete.Width = Unit.Pixel(50)
                    btnDelete.ID = dtCaseComments.Rows(i).Item("numCommentID") * 100
                    btnDelete.CssClass = "button"
                    btnDelete.Visible = HasEditRights
                    tblCell.Controls.Add(btnDelete)
                    lbl = New Label
                    lbl.Text = "&nbsp;&nbsp;"
                    tblCell.Controls.Add(lbl)

                    tblCell.HorizontalAlign = HorizontalAlign.Right
                    tblRow.Cells.Add(tblCell)
                    tblComments.Rows.Add(tblRow)

                    tblRow = New TableRow
                    If k <> 1 Then
                        tblRow.CssClass = "tr1"
                    Else : tblRow.CssClass = "tr2"
                    End If
                    tblCell = New TableCell
                    tblCell.ColumnSpan = 2
                    tblCell.Text = dtCaseComments.Rows(i).Item("txtComments")
                    tblCell.CssClass = "normal1"
                    tblRow.Cells.Add(tblCell)
                    tblComments.Rows.Add(tblRow)
                    If k <> 0 Then
                        k = 0
                    Else : k = 1
                    End If
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub btnDeleteComm_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            objCommon.CaseID = _CaseID
            objCommon.StageID = _StageID
            objCommon.byteMode = 2
            objCommon.CommentID = (sender.id) / 100
            objCommon.DomainID = Session("DomainId")
            objCommon.ManageComments()
            ShowComments()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            pnlPostComment.Visible = False
            tblComments.Visible = True
            objCommon.CaseID = _CaseID
            objCommon.StageID = _StageID
            objCommon.Comments = txtComments.Text
            objCommon.Heading = txtHeading.Text
            objCommon.ContactID = Session("UserContactID")
            objCommon.CommentID = 0
            objCommon.byteMode = 1
            objCommon.DomainID = Session("DomainId")
            If txtLstUptCom.Text <> "" Then objCommon.CommentID = txtLstUptCom.Text
            txtLstUptCom.Text = objCommon.ManageComments().Rows(0).Item(0)
            ShowComments()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            pnlPostComment.Visible = True
            tblComments.Visible = False
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class