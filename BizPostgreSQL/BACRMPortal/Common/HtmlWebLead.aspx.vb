﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Contacts
Imports System.IO
Imports System.Reflection
Imports System.Math
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Tracking
Imports System.Collections.Specialized
Imports BACRM.BusinessLogic.Workflow

Namespace BACRM.UserInterface.Admin
    Public Class HtmlWebLead
        Inherits BACRMPage

        Private numDomainId As Long
        Private numGroupId As Long
        Private numFormId As Long
        Private numSubFormId, openPopup As Long
        Private strLinkdeinId As String
        Private strLinkedinUrl As String
        Private vcSuccessURL, vcFailURL As String
        Private isIframe As Boolean

        Private vcPartnerSource As String
        Private vcPartnerContact As String

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                Dim uri As Uri
                Dim nvc As NameValueCollection = Request.Form

                If Request.UrlReferrer Is Nothing Then
                    If nvc("hfUrlReferrer") Is Nothing Then
                        Throw New Exception("Url Referrer not available.")
                    End If

                    uri = New Uri(nvc("hfUrlReferrer").ToString())
                Else
                    uri = Request.UrlReferrer
                End If

                If nvc("hfForm") Is Nothing Or nvc("hfDomain") Is Nothing Or nvc("hfGroup") Is Nothing Then
                    Response.Redirect(uri.AbsoluteUri & "?Error=Domain Form and group are not found", False)
                    Exit Sub
                End If

                If String.IsNullOrEmpty(nvc("hfForm")) Or String.IsNullOrEmpty(nvc("hfDomain")) Or String.IsNullOrEmpty(nvc("hfGroup")) Then
                    Response.Redirect(uri.AbsoluteUri & "?Error=Domain Form and group are not found", False)
                    Exit Sub
                End If
                If nvc("hfPartenrSource") Is Nothing Then
                    vcPartnerSource = ""
                Else
                    vcPartnerSource = Convert.ToString(nvc("hfPartenrSource"))
                End If
                If nvc("hfPartenerContact") Is Nothing Then
                    vcPartnerContact = ""
                Else
                    vcPartnerContact = Convert.ToString(nvc("hfPartenerContact"))
                End If

                Dim objEnc As New QueryStringValues

                numDomainId = objEnc.Decrypt(nvc("hfDomain"))
                numSubFormId = objEnc.Decrypt(nvc("hfSubForm"))
                numGroupId = objEnc.Decrypt(nvc("hfGroup"))
                numFormId = objEnc.Decrypt(nvc("hfForm"))
                openPopup = nvc("hfPopupOpen")
                strLinkdeinId = nvc("hfLinkedinId")
                strLinkedinUrl = nvc("hfLinkedinUrl")
                'Validate URL
                Dim objFormWizard As FormConfigWizard
                objFormWizard = New FormConfigWizard
                objFormWizard.DomainID = numDomainId
                objFormWizard.FormID = numFormId
                objFormWizard.AuthenticationGroupID = numGroupId
                objFormWizard.numSubFormId = numSubFormId
                objFormWizard.tintType = 2
                Dim dtList As DataTable
                dtList = objFormWizard.ManageHTMLFormURL()
                Dim drBizDocItems() As DataRow = dtList.Select("vcURL like '%" & uri.Host.ToString & "%'")

                If drBizDocItems.Length = 0 Then
                    Response.Redirect(uri.AbsoluteUri & "?Error=From URL is not specified", False)
                    Exit Sub
                Else
                    vcSuccessURL = drBizDocItems(0).Item("vcSuccessURL")
                    vcFailURL = drBizDocItems(0).Item("vcFailURL")
                    'isIframe = CCommon.ToLong(drBizDocItems(0).Item("bitIframe"))
                End If

                Dim objLeads As New CLeads
                Dim lnDivID, lngCMPID, lnCntID, lngRecOwner As Long
                Dim dRow As DataRow
                Dim ds As New DataSet

                objLeads.GroupID = numGroupId
                objLeads.ContactType = 70
                objLeads.PrimaryContact = True

                objLeads.DivisionName = "-"

                Dim dtGenericFormConfig As New DataTable
                dtGenericFormConfig.Columns.Add("vcDbColumnName")
                dtGenericFormConfig.Columns.Add("vcDbColumnText")

                Dim dtCusTable_C As New DataTable
                dtCusTable_C.Columns.Add("FldDTLID")
                dtCusTable_C.Columns.Add("fld_id")
                dtCusTable_C.Columns.Add("Value")

                Dim dtCusTable_D As New DataTable
                dtCusTable_D = dtCusTable_C.Clone()
                Dim strdetails As String
                Dim nvPairs As String()

                For Each myKey As String In nvc.AllKeys
                    'Response.Write("<br />" & myKey & "-" & nvc(myKey))
                    nvPairs = myKey.Split("_")

                    If nvPairs.Length > 1 Then
                        If nvPairs(0).Contains("cbListAOI") Then
                            dRow = dtGenericFormConfig.NewRow
                            dRow("vcDbColumnName") = "AOI"
                            dRow("vcDbColumnText") = nvc(myKey)
                            dtGenericFormConfig.Rows.Add(dRow)
                        ElseIf nvPairs(1).Contains("C") Then
                            dRow = dtCusTable_C.NewRow
                            dRow("FldDTLID") = 0
                            dRow("fld_id") = nvPairs(1).Replace("C", "")
                            dRow("Value") = nvc(myKey)
                            dtCusTable_C.Rows.Add(dRow)
                        ElseIf nvPairs(1).Contains("D") Then
                            dRow = dtCusTable_D.NewRow
                            dRow("FldDTLID") = 0
                            dRow("fld_id") = nvPairs(1).Replace("D", "")
                            dRow("Value") = nvc(myKey)
                            dtCusTable_D.Rows.Add(dRow)
                        Else
                            AssignValuesEditBox(objLeads, nvc(myKey), nvPairs(0))
                            AssignValuesSelectBox(objLeads, nvc(myKey), nvPairs(0))
                            AssignValuesTextBox(objLeads, nvc(myKey), nvPairs(0))

                            dRow = dtGenericFormConfig.NewRow
                            dRow("vcDbColumnName") = nvPairs(0)
                            dRow("vcDbColumnText") = nvc(myKey)
                            dtGenericFormConfig.Rows.Add(dRow)
                        End If
                    End If
                Next

                objLeads.DomainID = numDomainId

                Try
                    If numDomainId = 1 Then
                        Dim objBusinessClass As New BusinessClass
                        Dim ipStack As New IPStack
                        ipStack.DomainID = numDomainId
                        ipStack.GetUserLocationDetailFromIp(Request.UserHostAddress)

                        objLeads.Country = CCommon.ToLong(ipStack.CountryID)
                        objLeads.State = CCommon.ToLong(ipStack.StateID)
                        objLeads.City = CCommon.ToString(ipStack.city)
                        objLeads.PostalCode = CCommon.ToString(ipStack.zip)
                    Else
                        'Ip to country , attach Billing country by default from ip address
                        Dim objBusinessClass As New BusinessClass
                        objBusinessClass.IPNo = objBusinessClass.GetIPNumberFromIPAddress(Request.UserHostAddress)
                        'Response.Write("your country is " + objBusinessClass.GetCountryFromIPaddress(ConfigurationManager.AppSettings("ConnectionString")))
                        Dim importWiz As New ImportWizard
                        Dim lngCountryID As Long = importWiz.GetStateAndCountry(0, objBusinessClass.GetCountryFromIPaddress(ConfigurationManager.AppSettings("ConnectionString")), numDomainId)
                        If lngCountryID > 0 Then
                            objLeads.Country = lngCountryID
                        End If
                    End If
                Catch ex As Exception
                    'Do NOT THROW ERROR
                End Try

                dtGenericFormConfig.TableName = "Table"

                Dim lngAssignto As Long = lngRecOwner
                Dim lngDripCampaign As Long = 0
                Dim objAutoRoutRles As New AutoRoutingRules
                objAutoRoutRles.DomainID = numDomainId

                ds.Tables.Add(dtGenericFormConfig.Copy)
                objAutoRoutRles.strValues = ds.GetXml
                ds.Tables.Remove(ds.Tables(0))
                If numSubFormId > 0 Then
                    Dim dtConfig As New DataTable
                    Dim objFormConfig As New FormConfigWizard()
                    objFormConfig.DomainID = numDomainId
                    objFormConfig.numSubFormId = numSubFormId
                    dtConfig = objFormConfig.GetLeadSubFormListDetails()
                    If dtConfig.Rows.Count > 0 Then
                        If Convert.ToBoolean(dtConfig.Rows(0)("bitByPassRoutingRules")) = True Then
                            lngAssignto = Convert.ToInt32(dtConfig.Rows(0)("numAssignTo"))
                        End If
                        If Convert.ToBoolean(dtConfig.Rows(0)("bitDripCampaign")) = True Then
                            lngDripCampaign = Convert.ToInt32(dtConfig.Rows(0)("numDripCampaign"))
                        End If
                    End If
                End If
                objLeads.DripCampaign = lngDripCampaign
                objLeads.AssignedTo = lngAssignto
                lngRecOwner = objAutoRoutRles.GetRecordOwner
                objLeads.LinkedinId = strLinkdeinId
                objLeads.LinkedinURL = strLinkedinUrl
                objLeads.UserCntID = lngRecOwner

                objLeads.PartnerContact = vcPartnerContact
                objLeads.PartnerSource = vcPartnerSource

                If numDomainId = 1 Then
                    Try
                        If Not String.IsNullOrEmpty(objLeads.WebSite) Then
                            Dim website As String = objLeads.WebSite.Trim().Replace("http://", "").Replace("https://", "").Replace("www.", "")
                            If (website.Contains("/")) Then
                                website = website.Substring(0, website.IndexOf("/"))
                            End If

                            Dim objBuiltWidth As New BuiltWith
                            objBuiltWidth = objBuiltWidth.GetDomainDetails(website)

                            If Not objBuiltWidth Is Nothing AndAlso objBuiltWidth.Results.Length > 0 Then
                                objLeads.BuiltWithJson = objBuiltWidth.JsonResult
                                If Not objBuiltWidth.Results(0).Meta Is Nothing Then
                                    If objBuiltWidth.Results(0).Meta.CompanyName <> "" AndAlso (String.IsNullOrEmpty(objLeads.CompanyName) Or Convert.ToString(objLeads.CompanyName) = "-") Then
                                        objLeads.CompanyName = objBuiltWidth.Results(0).Meta.CompanyName
                                    End If

                                    If Not objBuiltWidth.Results(0).Meta Is Nothing AndAlso objBuiltWidth.Results(0).Meta.Telephones.Length > 0 Then
                                        If String.IsNullOrEmpty(objLeads.ComPhone) Then
                                            objLeads.ComPhone = objBuiltWidth.Results(0).Meta.Telephones(0)
                                        End If

                                        If String.IsNullOrEmpty(objLeads.ContactPhone) Then
                                            objLeads.ContactPhone = objBuiltWidth.Results(0).Meta.Telephones(0)
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    Catch ex As Exception
                        ExceptionModule.ExceptionPublish(ex, numDomainId, 0, Request)
                        'DO NOT THROW ERROR
                    End Try
                End If

                objLeads.CRMType = 0
                lngCMPID = objLeads.CreateRecordCompanyInfo
                objLeads.CompanyID = lngCMPID
                lnDivID = objLeads.CreateRecordDivisionsInfo
                objLeads.DivisionID = lnDivID

                Dim objWfA As New Workflow
                objWfA.DomainID = objLeads.DomainID
                objWfA.UserCntID = objLeads.UserCntID
                objWfA.RecordID = lnDivID
                objWfA.SaveWFOrganizationQueue()

                lnCntID = objLeads.CreateRecordAddContactInfo()
                Session("CompID") = lngCMPID        'Set the Company Id in a session
                Session("DivID") = lnDivID
                Session("UserContactID") = lnCntID

                objWfA.RecordID = lnCntID
                objWfA.SaveWFContactQueue()

                'add cookie to browser of Division id which can be used to track future visit of same user
                CCommon.AddDivisionCookieToBrowser(lnDivID)

                ''Saving CustomFields

                If dtCusTable_C.Rows.Count > 0 Then
                    dtCusTable_C.TableName = "Table"
                    ds.Tables.Add(dtCusTable_C.Copy)
                    strdetails = ds.GetXml
                    ds.Tables.Remove(ds.Tables(0))

                    Dim ObjCusfld As New CustomFields
                    ObjCusfld.strDetails = strdetails
                    ObjCusfld.RecordId = lnCntID
                    ObjCusfld.locId = 4
                    ObjCusfld.SaveCustomFldsByRecId()
                End If

                If dtCusTable_D.Rows.Count > 0 Then
                    dtCusTable_D.TableName = "Table"
                    ds.Tables.Add(dtCusTable_D.Copy)
                    strdetails = ds.GetXml
                    ds.Tables.Remove(ds.Tables(0))

                    Dim ObjCusfld As New CustomFields
                    ObjCusfld.strDetails = strdetails
                    ObjCusfld.RecordId = lnDivID
                    ObjCusfld.locId = 1
                    ObjCusfld.SaveCustomFldsByRecId()
                End If

                Dim dtTableAOI As New DataTable
                dtTableAOI.Columns.Add("numAOIId")
                dtTableAOI.Columns.Add("Status")
                Dim drAOI() As DataRow = dtGenericFormConfig.Select("vcDbColumnName='AOI'")

                If drAOI.Length > 0 Then
                    For Each dr As DataRow In drAOI
                        dRow = dtTableAOI.NewRow
                        dRow("numAOIId") = dr("vcDbColumnText")
                        dRow("Status") = 1
                        dtTableAOI.Rows.Add(dRow)
                    Next

                    dtTableAOI.TableName = "Table"
                    ds.Tables.Add(dtTableAOI.Copy)
                    strdetails = ds.GetXml
                    ds.Tables.Remove(ds.Tables(0))

                    Dim ObjContacts As New CContacts
                    ObjContacts.strAOI = strdetails
                    ObjContacts.ContactID = lnCntID
                    ObjContacts.SaveAOI()
                End If

                Session("SMTPServerIntegration") = True
                'send email to record owner of lead and his manager.
                '  SendAlerts(4, lnDivID, objLeads.UserCntID, objLeads.UserCntID, lngRecOwner)

                Dim objAlerts As New CAlerts
                Dim dtIds As DataTable
                objAlerts.AlertID = 2   ' 2 is for new web lead arrival
                dtIds = objAlerts.GetAlerDetailIds()
                If dtIds.Rows.Count = 2 Then
                    If dtIds.Rows.Count = 2 Then
                        'Send welcome Email to Lead Contact
                        '  SendAlerts(dtIds.Rows(0).Item("numAlertDtlId"), lnDivID, lnCntID, lnCntID, lngRecOwner) ' here last parameter is lngUsrcntID so as to send mail to 
                        'Added by : Pratik : Add the contact in e campaign if it is selected

                        Dim dtDetails As DataTable
                        objAlerts.AlertDTLID = dtIds.Rows(1).Item("numAlertDtlId")
                        objAlerts.DomainID = numDomainId
                        dtDetails = objAlerts.GetIndAlertDTL
                        If dtDetails.Rows.Count > 0 Then
                            If dtDetails.Rows(0).Item("tintAlertOn") = 1 Then
                                Dim objCampaign As New Campaign
                                objCampaign.ConEmailCampID = 0
                                objCampaign.ContactId = lnCntID
                                objCampaign.ECampaignID = dtDetails.Rows(0).Item("numEmailCampaignId")
                                objCampaign.StartDate = DateTime.Today
                                objCampaign.Engaged = 1
                                objCampaign.UserCntID = lngRecOwner
                                objCampaign.ManageConECamp()
                            End If
                        End If

                    End If
                End If

                If Not Request.Files Is Nothing AndAlso Request.Files.Count > 0 AndAlso Request.Files(0).ContentLength > 0 Then
                    Dim fileName As String = Request.Files(0).FileName.Split(Convert.ToChar("."))(0) & DateTime.Now.ToString("yyyyMMhhHHss")
                    Dim fileType As String = Request.Files(0).FileName.Split(Convert.ToChar("."))(1)
                    Dim strFileName As String = fileName & "." & fileType

                    If Not Directory.Exists(CCommon.GetDocumentPhysicalPath(numDomainId)) Then
                        Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(numDomainId))
                    End If
                    Dim strFilePath As String = CCommon.GetDocumentPhysicalPath(numDomainId) + strFileName
                    Request.Files(0).SaveAs(strFilePath)
                    Try
                        Dim objDocuments As New DocumentList()

                        objDocuments.DomainID = numDomainId
                        objDocuments.UserCntID = lnCntID
                        objDocuments.UrlType = "L"
                        objDocuments.DocumentStatus = 7983
                        objDocuments.DocCategory = 370
                        objDocuments.FileType = fileType
                        objDocuments.DocName = fileName
                        objDocuments.DocDesc = fileName
                        objDocuments.FileName = fileName
                        objDocuments.DocumentSection = "A"
                        objDocuments.RecID = lnDivID
                        objDocuments.DocumentType = 2
                        objDocuments.SaveDocuments()
                    Catch ex As Exception

                    End Try
                End If

                'Response.Redirect(Request.UrlReferrer.AbsoluteUri)
                If openPopup = 1 Then
                    Response.Write("<script>top.location='" + vcSuccessURL + "';parent.location='" + vcSuccessURL + "';</script>")
                Else
                    Response.Redirect(vcSuccessURL, False)
                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                If openPopup = 1 Then
                    Response.Write("<script>top.location='" + vcFailURL + "';parent.location='" + vcFailURL + "';</script>")
                Else
                    Response.Redirect(vcFailURL, False)
                End If
            End Try
        End Sub

        ''' <summary>
        ''' Added By : Pratik Vasani
        ''' Added this function  to Send a alert
        ''' </summary>
        ''' <param name="lngAlertDtlID"></param>
        ''' <param name="lnDivID"></param>
        ''' <param name="lnCntID"></param>
        ''' <param name="lngUserContactID"></param>
        ''' <remarks>Sends E mail alerts</remarks>
        Sub SendAlerts(ByVal lngAlertDtlID As Long, ByVal lnDivID As Long, ByVal lnCntID As Long, ByVal lngUserContactID As Long, ByVal lngRecordOwner As Long)
            Try
                Dim objAlerts As New CAlerts
                Dim dtDetails As DataTable
                objAlerts.AlertDTLID = lngAlertDtlID
                objAlerts.DomainID = numDomainId
                dtDetails = objAlerts.GetIndAlertDTL
                If dtDetails.Rows.Count > 0 Then
                    If dtDetails.Rows(0).Item("tintAlertOn") = 1 Then
                        Dim objCommon As New CCommon
                        Dim strFrmEmail As String = ""
                        If lngRecordOwner > 0 Then
                            objCommon.ContactID = lngRecordOwner
                            strFrmEmail = objCommon.GetContactsEmail
                        End If

                        objCommon.byteMode = 1
                        objCommon.ContactID = lngUserContactID

                        Dim objSendEmail As New Email

                        objSendEmail.DomainID = numDomainId
                        objSendEmail.DocID = dtDetails.Rows(0).Item("numEmailTemplate")
                        objSendEmail.TemplateCode = "" '  "#SYS#EMAIL_ALERT:NEW_WEBLEAD_ARRIVAL"
                        objSendEmail.ModuleID = 1
                        objSendEmail.RecordIds = lnCntID.ToString
                        objSendEmail.FromEmail = strFrmEmail
                        objSendEmail.ToEmail = "##ContactFirstName## <##ContactEmail##>"
                        objSendEmail.CCEmail = IIf(dtDetails.Rows(0).Item("tintCCManager") = 1, objCommon.GetManagerEmail, "")
                        objSendEmail.SendEmailTemplate()

                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try

        End Sub
    End Class
End Namespace