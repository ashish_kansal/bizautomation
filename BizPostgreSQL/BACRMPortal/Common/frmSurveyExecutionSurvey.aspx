<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSurveyExecutionSurvey.aspx.vb"
    Inherits="BACRMPortal.BACRM.UserInterface.Survey.frmSurveyExecutionSurvey" ValidateRequest="false"
    EnableViewState="False" %>

<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <title>Survey</title>
    <script language="javascript" src="../include/Survey.js"></script>
</head>
<body onload="initArrayOnPageLoad()">
    <form id="frmSurveyExecutionSurvey" method="post" runat="server">
    <input id="hdSurId" type="hidden" name="hdSurId" runat="server"><!---Store the Survey ID--->
    <input id="hdRuleFourActive" runat="server" type="hidden" name="hdRuleFourActive">
    <input id="hdRuleFiveActive" runat="server" type="hidden" name="hdRuleFiveActive">
    <input id="tIntRuleFourRadio" runat="server" type="hidden" name="tIntRuleFourRadio">
    <input id="tIntCreateNewID" runat="server" type="hidden" name="tIntCreateNewID">
    <input id="boolPopulateSalesOpportunity" runat="server" type="hidden" name="boolPopulateSalesOpportunity">
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td>
                <asp:Label ID="lblHeader" runat="server"></asp:Label><br />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Literal ID="litTRIDList" runat="server"></asp:Literal>
                <table cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td valign="bottom">
                            <table class="TabStyle">
                                <tr>
                                    <td>
                                        &nbsp;&nbsp;&nbsp;<asp:Literal ID="litSurveyName" runat="Server"></asp:Literal><a
                                            id="FocusTopReference">&nbsp;</a>&nbsp;&nbsp;&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="normal1" align="left" height="23">
                            &nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
                <asp:PlaceHolder ID="plhSurveyFormControls" runat="server"></asp:PlaceHolder>
                <table id="tblNavgationControls" cellspacing="0" cellpadding="0" width="100%" runat="Server">
                    <tr>
                        <td valign="bottom" align="right">
                            <asp:Button ID="btnConcludeSurvey" runat="server" CssClass="button" Text="Conclude Survey">
                            </asp:Button>&nbsp;
                        </td>
                    </tr>
                </table>
                <a id="FocusBottomReference">&nbsp;</a>
                <input id="hdRespondentID" type="hidden" name="hdRespondentID" runat="server">
                <input id="hdSinglePage" type="hidden" name="hdSinglePage" runat="server">
                <input id="hdRandomAnswers" type="hidden" name="hdRandomAnswers" runat="server">
                <input id="hdRedirectURL" type="hidden" name="hdRedirectURL" runat="server">
                <input id="hdSurveyRating" type="hidden" name="hdSurveyRating" runat="server" value="0">
                <input id="hdRecOwner" type="hidden" name="hdRecOwner" runat="server" value="0">
                <input id="hdGroupId" type="hidden" name="hdGroupId" runat="server" value="0">
                <input id="hdCRMType" type="hidden" name="hdCRMType" runat="server" value="0">
                <input id="hdRelationShip" type="hidden" name="hdRelationShip" runat="server">
                <input id="hdProfile" type="hidden" name="hdProfile" runat="server">
                <input id="hdCreateDBEntry" type="hidden" name="hdCreateDBEntry" runat="server" value="0">
                <input id="hdFollowUpStatus" type="hidden" name="hdFollowUpStatus" runat="server">

                <input id="hdTrackableSurveyControlEntries" type="hidden" name="hdTrackableSurveyControlEntries"
                    runat="server">
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblFooter" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
