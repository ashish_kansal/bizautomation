<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmPartnerPortal.aspx.vb" Inherits="BACRMPortal.frmPartnerPortal" %>
<%@ Register TagPrefix="ec" Namespace="ASPNETExpert.WebControls" Assembly="ASPNETExpert.WebControls" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
     <script language="javascript" >
    function OpeninFrame(a)
    {
        if (a=='#')
        {
            return false;
        }
        else if (a=='common/frmTicklerdisplay.aspx')
        {
            parent.frames['mainframe'].location.href='../'+a+'?ClientMachineUTCTimeOffset='+new Date().getTimezoneOffset();
        }
        else
        {
            parent.frames['mainframe'].location.href='../'+a;
        }
    }
      function Login()
		{
			if (parent.parent.frames.length>0)
			{
			    parent.parent.document.location.href="../Login.aspx"
			}
			else if (parent.frames.length>0)
			{
			    parent.document.location.href="../Login.aspx"
			}
			else
			{
			    document.location.href="../Login.aspx"
			}
			
			
			//parent.location.href='common/frmticklerdisplay.aspx?ClientMachineUTCTimeOffset='+new Date().getTimezoneOffset();
			return false;
		}
     function RedirectToPortal(a)
        {
                parent.document.location.href='../Common/frmMainPage.aspx'
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <table cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td>
			<IMG   src="../images/New LogoBiz1.JPG" >
		</td>
		 <td>
                <asp:HyperLink ID="hplPortal" runat="server" NavigateUrl="#" CssClass="text_bold">Back To Customer Portal</asp:HyperLink>&nbsp;&nbsp;&nbsp;
                <asp:HyperLink ID="hplLogout" runat="server" NavigateUrl="#" CssClass="text_bold">Logout</asp:HyperLink>
            </td>
    </tr>
        <tr>
            <td colspan="2">
               <ec:ExpertMenu runat="server" id="TotallyBuiltMenu" NavigateOnClick="true">
                     <Looks>
                        <ec:MenuItemDualIconLook Id="TopItemLook">
                            <InitLook  Height="25px" CssClass="TopItemClass" Cursor="Pointer">
                                <TextSection CssClass="TopItemTextClass" />
                            </InitLook>
                            <HoveredLook Height="25px">
                                <TextSection CssClass="TopItemTextHoveredClass" />
                            </HoveredLook>
                            <BindCriteria>
                                <ec:PropertyValueCriteria Property="HasSubMenu" Value="false" />
                            </BindCriteria>
                        </ec:MenuItemDualIconLook>
                        <ec:MenuItemDualIconLook Id="TopParentItemLook" Base="TopItemLook">
                            <BindCriteria>
                                <ec:PropertyValueCriteria Property="Depth"  Value="0" />
                            </BindCriteria>
                        </ec:MenuItemDualIconLook>
                        <ec:MenuItemDualIconLook Id="ItemLook">
                            <InitLook Height="25px" CssClass="ItemClass" Cursor="Pointer">
                                <TextSection CssClass="ItemTextClass" />
                            </InitLook>
                            <HoveredLook Height="25px" CssClass="ItemClass">
                                <TextSection CssClass="ItemTextHoveredClass" />
                            </HoveredLook>
                        </ec:MenuItemDualIconLook>
                        <ec:MenuItemDualIconLook  Id="ParentItemLook" Base="ItemLook">
                            <BindCriteria>
                                <ec:PropertyValueCriteria Property="Depth" Operator="Greater" Value="0" />
                                <ec:PropertyValueCriteria Property="HasSubMenu"  Value="true" />
                            </BindCriteria>
                        </ec:MenuItemDualIconLook>
                    </Looks>
                
                    </ec:ExpertMenu>
            </td>
           
        </tr>
    </table>
     
         
    </form>
</body>
</html>
