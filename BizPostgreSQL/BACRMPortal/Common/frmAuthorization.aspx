<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmAuthorization.aspx.vb" Inherits="BACRMPortal.frmAuthorization"%>
<%@ Register TagPrefix="menu1" TagName="Menu" src="../common/topbar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
		<title>frmAuthorization</title>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
		<menu1:menu id="webmenu1" runat="server"></menu1:menu>
		<asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
		<br />
		<br />
		<br />
			<table align=center >
				<tr>
					<td style="color:Red">
						<b>You don�t have permission to use this resource. Contact your administrator</b>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
