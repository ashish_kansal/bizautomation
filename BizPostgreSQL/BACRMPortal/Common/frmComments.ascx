﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmComments.ascx.vb" Inherits="BACRMPortal.frmComments" %>
<table width="100%" cellpadding="0" cellspacing="0">
    <script language="javascript">
        function SaveComment() {

            if (document.getElementById('txtHeading').value == 0) {
                alert("Enter Comment Heading")
                document.getElementById('txtHeading').focus();
                return false;
            }

        }
    </script>
    <tr>
        <td class="normal1">
            <asp:LinkButton ID="lnkbtnNoofCom" runat="server" CommandName="Show"></asp:LinkButton>&nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnlbtnPost" runat="server" CommandName="Post">Post Comment</asp:LinkButton>&nbsp;&nbsp;&nbsp;
        </td>
    </tr>
    <tr>
        <td>
            <asp:Table ID="tblComments" CellPadding="0" CellSpacing="0" runat="server" Width="100%"
                Visible="False">
            </asp:Table>
        </td>
    </tr>
    <td>
        <asp:Panel ID="pnlPostComment" runat="server" Visible="False">
            <table width="100%">
                <tr>
                    <td class="normal1" align="right">
                        Heading&nbsp;
                    </td>
                    <td>
                        <asp:TextBox ID="txtHeading" runat="server" Width="500" CssClass="signup" ClientIDMode="Static"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="normal1" align="right">
                        Comment&nbsp;
                    </td>
                    <td>
                        <asp:TextBox ID="txtComments" runat="server" Width="500" CssClass="signup" Height="90"
                            TextMode="MultiLine"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td align="left">
                        <asp:Button ID="btnSubmit" runat="server" Width="50" CssClass="button" Text="Submit">
                        </asp:Button>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </td>
</table>
<asp:TextBox ID="txtLstUptCom" Style="display: none" runat="server"></asp:TextBox>
<asp:HiddenField ID="hdnCommentCount" runat="server" />
