<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmGenericFormLeadbox.aspx.vb" Inherits="BACRMPortal.BACRM.UserInterface.Admin.frmGenericFormLeadbox" ValidateRequest="false" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">

		<title>LeadBox</title>
		<style id="styleLead" runat="server" >
		
		</style>
		<script language="javascript" src="../include/GenericLeadBox.js"></script>
		<script language="javascript" type="text/javascript" >
	        function CheckNumber()
					{
							if (!(window.event.keyCode > 47 && window.event.keyCode < 58))
							{
								window.event.keyCode=0;
							}
					}
		</script>
	</head>
	<body>
		<form id="frmGenericFormLeadbox" method="post" runat="server">
			<input id="hdRedirectURL" type="hidden" name="hdRedirectURL" runat="server"><!---The Redirection URL--->
			<input id="hdDomainId" type="hidden" name="hdDomainId" runat="server"><!---Store the Doamin ID--->
			<input id="hdGroupId" type="hidden" name="hdGroupId" runat="server"><!---Store the Group ID--->
			
		    <table align="center" >
		        <tr>
		            <td colspan="3">
		                <asp:Label ID="lblHeader" runat="server" ></asp:Label>
		            </td>
		        </tr>
		        <tr>
		            <td valign="top">
		            <br />
		                <asp:Label ID="lblLeft" runat="server" ></asp:Label>
		            </td>
		            <td>
		            <table>
				<tr>
				    
					<td>
						<asp:placeholder id="plhFormControls" runat="server" EnableViewState="True"></asp:placeholder>
					</td>
				</tr>
				<tr>
					<td>
					    <asp:PlaceHolder id="plhFormControlsAOI" runat="server"></asp:PlaceHolder>	
					</td>
				</tr>
				<tr>
				<td align="center" >
				<asp:button id="btnSubmit" Text="Submit" Runat="server" cssclass="button" Width="70"></asp:button>
				
				</td>
				</tr>
			    </table>
			    <asp:validationsummary id="ValidationSummary" runat="server" HeaderText="Please check the following value(s)"
				ShowSummary="False" ShowMessageBox="True" DisplayMode="List"></asp:validationsummary><asp:literal id="litClientScript" Runat="server"></asp:literal><input id="hdXMLString" type="hidden" name="hdXMLString" runat="server">
				<br />
				<asp:Label ID="lblFooter" runat="server" ></asp:Label>
				
				
		            </td>
		            <td valign="top" >
		            <br />
		                    <asp:Label ID="lblRight" runat="server" ></asp:Label>
		            </td>
		        </tr>
		    </table>			
			
		</form>
	</body>
</html>
