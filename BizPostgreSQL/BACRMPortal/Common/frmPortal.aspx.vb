'Imports ASPNETExpert.WebControls
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports Telerik.Web.UI
Partial Public Class frmPortal : Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If CCommon.ToLong(Session("DomainID")) = 0 Then
                Response.Write("<script>top.location= '../login.aspx';</script>")
                Exit Sub
                'Response.Redirect("../login.aspx", False)
            End If
            If Not IsPostBack Then
                If Session("Logo") = "" Then
                    imgLogo.Src = "../images/New LogoBiz1.JPG"
                Else : imgLogo.Src = "../Documents/Docs/" & Session("DomainID") & "/" & Session("Logo")
                End If
                BuildTotalMenu()
                lblUserName.Text = Session("ContactName") & ", " & Session("CompName")
                'In shortcut bar
                If Not Session("WebStoreURL") Is Nothing Then
                    If CCommon.ToString(Session("WebStoreURL")).Length > 2 Then
                        hplBackToWebStore.Visible = True
                        hplBackToWebStore.NavigateUrl = "javascript:RediretWebStore('" & Session("WebStoreURL") & "');"
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub BuildTotalMenu()
        If CCommon.ToLong(Session("GroupId")) = 0 Then
            Response.Clear()
            Response.Write("Can't log into the portal until a value has been selected in the ""Customer & Vendor Portal Access Group"" drop-down within the External user's profile")
            Exit Sub
        End If
        Try
            Dim dtTabData As DataTable
            Dim objTab As New Tabs
            objTab.GroupID = Session("GroupId")
            objTab.RelationshipID = Session("RelationShip")
            objTab.ProfileID = Session("Profile")
            objTab.DomainID = Session("DomainID")
            dtTabData = objTab.GetTabData()
            Dim dtTab As DataTable
            dtTab = Session("DefaultTab")
            Dim i As Integer
            For i = 0 To dtTabData.Rows.Count - 1
                dtTabData.Rows(i).Item("vcURL") = Replace(dtTabData.Rows(i).Item("vcURL"), "RecordID", Session("UserContactID"))
                If dtTabData.Rows(i).Item("bitFixed") = True Then
                    If dtTabData.Rows(i).Item("numTabId") = 9 Then
                        If Session("EnableIntMedPage") = 1 Then
                            CreateItem(dtTabData.Rows(i).Item("numTabName"), "pagelayout/frmCustAccountdtl.aspx", dtTabData.Rows(i).Item("numTabId"))
                        Else : CreateItem(dtTabData.Rows(i).Item("numTabName"), dtTabData.Rows(i).Item("vcURL"), dtTabData.Rows(i).Item("numTabId"))
                        End If

                    ElseIf dtTabData.Rows(i).Item("numTabId") = 26 Then
                        If Session("PartnerAccess") = True Then
                            CreateItem(dtTabData.Rows(i).Item("numTabName"), dtTabData.Rows(i).Item("vcURL"), dtTabData.Rows(i).Item("numTabId"))
                        End If
                    Else
                        Select Case dtTabData.Rows(i).Item("numTabName").ToString
                            Case "Leads" : CreateItem(dtTab.Rows(0).Item("vcLead"), dtTabData.Rows(i).Item("vcURL"), dtTabData.Rows(i).Item("numTabId"))
                            Case "Prospects" : CreateItem(dtTab.Rows(0).Item("vcProspect"), dtTabData.Rows(i).Item("vcURL"), dtTabData.Rows(i).Item("numTabId"))
                            Case "Accounts" : CreateItem(dtTab.Rows(0).Item("vcAccount"), dtTabData.Rows(i).Item("vcURL"), dtTabData.Rows(i).Item("numTabId"))
                            Case "Contacts" : CreateItem(dtTab.Rows(0).Item("vcContact"), dtTabData.Rows(i).Item("vcURL"), dtTabData.Rows(i).Item("numTabId"))
                            Case Else
                                CreateItem(dtTabData.Rows(i).Item("numTabName"), dtTabData.Rows(i).Item("vcURL"), dtTabData.Rows(i).Item("numTabId"))
                        End Select
                    End If
                Else
                    CreateItem(dtTabData.Rows(i).Item("numTabName"), "Common/SimpleMenu.aspx?Page=" & dtTabData.Rows(i).Item("vcURL"), dtTabData.Rows(i).Item("numTabId"))
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub CreateItem(ByVal text As String, ByVal URL As String, ByVal Value As Long)
        Try
            Dim radtab As New RadTab(text, Value)
            radtab.Target = "mainframe"
            radtab.NavigateUrl = "../" & URL
            RadTabStrip1.Tabs.Add(radtab)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
  

End Class