﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmLandingPage.aspx.vb"
    Inherits="BACRMPortal.frmLandingPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Survey</title>
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
</head>
<body>
    <form id="form1" runat="server">
    <input id="hdSurId" type="hidden" name="hdSurId" runat="server">
    <input id="hdRespondentID" type="hidden" name="hdRespondentID" runat="server">
    <input id="hdRecOwner" type="hidden" name="hdRecOwner" runat="server" value="0">
    <input id="hdGroupId" type="hidden" name="hdGroupId" runat="server" value="0">
    <input id="hdCRMType" type="hidden" name="hdCRMType" runat="server" value="0">
    <input id="hdRelationShip" type="hidden" name="hdRelationShip" runat="server" value="0">
    <input id="hdProfile" type="hidden" name="hdProfile" runat="server">
    <input id="hdCreateDBEntry" type="hidden" name="hdCreateDBEntry" runat="server" value="0">
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td>
                <asp:Label ID="lblHeader" runat="server"></asp:Label><br />
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:RadioButtonList ID="rdbLandingPage" runat="server">
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td valign="bottom" align="right">
                <asp:Button ID="btnContinue" runat="server" CssClass="button" Text="Continue"></asp:Button>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblFooter" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
