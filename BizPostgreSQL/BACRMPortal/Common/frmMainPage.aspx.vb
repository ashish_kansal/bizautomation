Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Partial Public Class frmMainPage
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim objTab As New Tabs
            objTab.GroupID = Session("GroupId")
            objTab.RelationshipID = Session("RelationShip")
            objTab.ProfileID = Session("Profile")
            objTab.DomainID = Session("DomainID")
            Dim str As String
            str = objTab.GetFirstPage
            If Not str Is Nothing Then
                If Session("EnableIntMedPage") = 1 Then
                    If str = "account/frmCusAccounts.aspx" Then str = "pagelayout/frmCustAccountdtl.aspx"
                End If
                mainframe.Attributes.Add("src", "../" & str)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class