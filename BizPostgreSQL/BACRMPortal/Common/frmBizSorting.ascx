﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmBizSorting.ascx.vb"
    Inherits="BACRMPortal.frmBizSorting" %>
<%@ OutputCache Duration="88400" VaryByParam="none" %>
<script type="text/javascript" language="javascript">
    function fnSortByChar(varSortChar) {
        var SortChar = $('#txtSortChar');
        SortChar.val(varSortChar);
        //        console.log(SortChar.val());
        if (typeof ($('#txtCurrrentPage')) != 'undefined') {
            $('#txtCurrrentPage').val(1);
        }
        $('#btnGo1').click();
    }

    $(document).ready(function () {
        //set onclick event
        $('#tblSort tr td a').on("click", function (event) {
            fnSortByChar(this.id);
        });
        SetAttributes();
    });

    function SetAttributes() {
        //Reset active Element
        $('#tblSort tr td').each(function () {
            $(this).removeClass("active");

        });

        if ($('#txtSortChar').val() == "")
            $('#txtSortChar').val("0")
        //Apply Active class to clicked td
        $('#tblSort tr td a:#' + $('#txtSortChar').val().toLowerCase() + "'").parent().attr("class", "active");
    }
</script>
<asp:Panel ID="pnlSort" runat="server">
</asp:Panel>
<table id="tblSort" cellpadding="1" cellspacing="1">
    <tr>
        <td class="" width="30px" height="21" align="center" valign="middle">
            <a id="a">
                <div>
                    A</div>
            </a>
        </td>
        <td class="" width="30px" height="21" align="center" valign="middle">
            <a id="b">
                <div>
                    B</div>
            </a>
        </td>
        <td class="" width="30px" height="21" align="center" valign="middle">
            <a id="c">
                <div>
                    C</div>
            </a>
        </td>
        <td class="" width="30px" height="21" align="center" valign="middle">
            <a id="d">
                <div>
                    D</div>
            </a>
        </td>
        <td class="" width="30px" height="21" align="center" valign="middle">
            <a id="e">
                <div>
                    E</div>
            </a>
        </td>
        <td class="" width="30px" height="21" align="center" valign="middle">
            <a id="f">
                <div>
                    F</div>
            </a>
        </td>
        <td class="" width="30px" height="21" align="center" valign="middle">
            <a id="g">
                <div>
                    G</div>
            </a>
        </td>
        <td class="" width="30px" height="21" align="center" valign="middle">
            <a id="h">
                <div>
                    H</div>
            </a>
        </td>
        <td class="" width="30px" height="21" align="center" valign="middle">
            <a id="I">
                <div>
                    I</div>
            </a>
        </td>
        <td class="" width="30px" height="21" align="center" valign="middle">
            <a id="j">
                <div>
                    J</div>
            </a>
        </td>
        <td class="" width="30px" height="21" align="center" valign="middle">
            <a id="k">
                <div>
                    K</div>
            </a>
        </td>
        <td class="" width="30px" height="21" align="center" valign="middle">
            <a id="l">
                <div>
                    L</div>
            </a>
        </td>
        <td class="" width="30px" height="21" align="center" valign="middle">
            <a id="m">
                <div>
                    M</div>
            </a>
        </td>
        <td class="" width="30px" height="21" align="center" valign="middle">
            <a id="n">
                <div>
                    N</div>
            </a>
        </td>
        <td class="" width="30px" height="21" align="center" valign="middle">
            <a id="o">
                <div>
                    O</div>
            </a>
        </td>
        <td class="" width="30px" height="21" align="center" valign="middle">
            <a id="p">
                <div>
                    P</div>
            </a>
        </td>
        <td class="" width="30px" height="21" align="center" valign="middle">
            <a id="q">
                <div>
                    Q</div>
            </a>
        </td>
        <td class="" width="30px" height="21" align="center" valign="middle">
            <a id="r">
                <div>
                    R</div>
            </a>
        </td>
        <td class="" width="30px" height="21" align="center" valign="middle">
            <a id="s">
                <div>
                    S</div>
            </a>
        </td>
        <td class="" width="30px" height="21" align="center" valign="middle">
            <a id="t">
                <div>
                    T</div>
            </a>
        </td>
        <td class="" width="30px" height="21" align="center" valign="middle">
            <a id="u">
                <div>
                    U</div>
            </a>
        </td>
        <td class="" width="30px" height="21" align="center" valign="middle">
            <a id="v">
                <div>
                    V</div>
            </a>
        </td>
        <td class="" width="30px" height="21" align="center" valign="middle">
            <a id="w">
                <div>
                    W</div>
            </a>
        </td>
        <td class="" width="30px" height="21" align="center" valign="middle">
            <a id="x">
                <div>
                    X</div>
            </a>
        </td>
        <td class="" width="30px" height="21" align="center" valign="middle">
            <a id="y">
                <div>
                    Y</div>
            </a>
        </td>
        <td class="" width="30px" height="21" align="center" valign="middle">
            <a id="z">
                <div>
                    Z</div>
            </a>
        </td>
        <td class="all" width="25px" height="21" align="center" valign="middle">
            <a id="0">
                <div>
                    All</div>
            </a>
        </td>
    </tr>
</table>
