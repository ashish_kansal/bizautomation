<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmChangePassword.aspx.vb"
    Inherits="BACRMPortal.frmChangePassword" %>

<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <title>Change Password</title>
    <script language="javascript">
        function Close() {
            //document.location.href = "../Login.aspx";
            window.close();
            return false;
        }
        function Submit() {
            if (document.Form1.txtOldPassword.value == '') {
                alert("Enter Old Password")
                document.Form1.txtOldPassword.focus()
                return false;
            }
            if (document.Form1.txtNewPassword.value == '') {
                alert("Enter New Password")
                document.Form1.txtNewPassword.focus()
                return false;
            }
            if (document.Form1.txtConPassword.value == '') {
                alert("Enter Confirm New Password")
                document.Form1.txtConPassword.focus()
                return false;
            }
            if (document.Form1.txtNewPassword.value != document.Form1.txtConPassword.value) {
                alert("Password Doesn't Match")
                document.Form1.txtConPassword.focus()
                return false;
            }
        }
    </script>
</head>
<body style="margin: 0 10px 0 10px;">
    <form id="Form1" method="post" runat="server">
    <br />
    <table cellspacing="0" cellpadding="0" width="100%" border="0" align="center">
        <tr>
            <td valign="bottom">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="TabLeft">
                            Change Password
                        </td>
                        <td class="TabRight">
                        </td>
                    </tr>
                </table>
            </td>
            <td align="right">
            </td>
        </tr>
    </table>
    <asp:Table ID="tblOppr" CellPadding="0" CellSpacing="0" BorderWidth="1" Height="150"
        runat="server" CssClass="aspTable" Width="450" BorderColor="black" GridLines="None">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br>
                <table class="aspTable">
                    <tr>
                        <td class="normal4" colspan="2" align="center">
                            <asp:Literal EnableViewState="False" ID="litMessage" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Old Password
                        </td>
                        <td>
                            <asp:TextBox ID="txtOldPassword" TextMode="Password" CssClass="signup" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            New Password
                        </td>
                        <td>
                            <asp:TextBox ID="txtNewPassword" CssClass="signup" TextMode="Password" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Confirm New Password
                        </td>
                        <td>
                            <asp:TextBox ID="txtConPassword" CssClass="signup" TextMode="Password" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td align="left">
                            <asp:Button ID="btnSubmit" runat="server" CssClass="button" Text="Submit"></asp:Button>&nbsp;
                             <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" OnClientClick="return Close();">
                </asp:Button>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <img runat="server" id="imgLogo">
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    </form>
</body>
</html>
