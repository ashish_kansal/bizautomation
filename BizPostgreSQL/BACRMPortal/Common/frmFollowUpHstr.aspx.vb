Imports BACRM.BusinessLogic.Leads
Partial Public Class frmFollowUpHstr
    Inherits System.Web.UI.Page


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            BindGrid()
        End If
    End Sub

    Sub BindGrid()
        Dim objLeads As New CLeads
        Dim dtFollowHstr As DataTable
        objLeads.DivisionID = GetQueryStringVal(Request.QueryString("enc"), "Div")
        dtFollowHstr = objLeads.GetFollowUpHstr
        dgFollow.DataSource = dtFollowHstr
        dgFollow.DataBind()
    End Sub


    Function ReturnName(ByVal bintCreatedDate As Date) As String
        Dim strCreateDate As String
        strCreateDate = FormattedDateFromDate(bintCreatedDate, Session("DateFormat"))
        Return strCreateDate
    End Function

    Private Sub dgFollow_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgFollow.ItemCommand
        If e.CommandName = "Delete" Then
            Dim objLeads As New CLeads
            objLeads.FollowUpHstrID = e.Item.Cells(0).Text
            objLeads.DeleteFollowUpHstr()
            BindGrid()
        End If
    End Sub

End Class