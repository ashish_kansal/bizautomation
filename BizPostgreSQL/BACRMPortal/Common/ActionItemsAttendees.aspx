﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ActionItemsAttendees.aspx.vb"
    Inherits="BACRMPortal.ActionItemsAttendees" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Action Items Attendees</title>
    <style>
        BODY
        {
            font-family: Arial;
            font-size: 10pt;
            font-style: normal;
            font-weight: normal;
            line-height: normal;
            margin: 0px 0px 0px 0px;
            text-decoration: none;
            background-color: white;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Table ID="tblOriginal" BorderWidth="0" runat="server" Width="100%" BorderColor="black"
        GridLines="None">
        <asp:TableRow>
            <asp:TableCell>
                <table>
                    <tr>
                        <td valign="top" align="left">
                            <img runat="server" id="imgLogo">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <fieldset id="FActionItems" runat="server">
                                <legend><b>Action Items Attendees</b></legend>
                                <br />
                                Status :
                                <asp:DropDownList ID="ddlActionItemsAttendees" runat="server">
                                </asp:DropDownList>
                                <br />
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" />
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:Table ID="tblFormatted" Visible="false" BorderWidth="0" runat="server" Width="100%"
        BorderColor="black" GridLines="None">
        <asp:TableRow>
            <asp:TableCell>
                <asp:Literal ID="litTemplate" runat="server"></asp:Literal>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <br />
    <asp:Label runat="server" Visible="false" ID="lblMessage"></asp:Label>
    </form>
</body>
</html>
