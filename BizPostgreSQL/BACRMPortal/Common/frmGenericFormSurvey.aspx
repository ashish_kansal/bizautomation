<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmGenericFormSurvey.aspx.vb"
    Inherits="BACRMPortal.BACRM.UserInterface.Survey.frmGenericFormSurvey" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Small Business Software CRM, Free Trial of ERP Accounting Software for 30 Days
    </title>
    <meta name="description" content="Bizautomation offers one month free trial to experience business management software tools." />
    <meta name="keywords" content="business software trial, crm software trial,one month trial erp, business management software trial" />
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <script type="text/javascript" language="javascript" src="../include/Survey.js"></script>
    <script language="javascript" type="text/javascript">
        function CheckNumber() {
            if (!(window.event.keyCode > 47 && window.event.keyCode < 58)) {
                window.event.keyCode = 0;
            }
        }
    </script>
    <style>
        .tableBorder
        {
            border-style: solid;
            border-width: 1px;
        }
    </style>
</head>
<body>
    <form id="frmGenericFormSurvey" method="post" runat="server">
    <input id="hdSurId" type="hidden" name="hdSurId" runat="server"><!---Store the Survey ID--->
    <input id="hdOnlyRegistration" type="hidden" name="hdOnlyRegistration" runat="server"><!---Store the flag which indicates if the entire survey is to be displayed ir not --->
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td>
                <asp:Label ID="lblHeader" runat="server"></asp:Label><br />
            </td>
        </tr>
        <tr>
            <td>
                <table cellspacing="0" cellpadding="0" width="100%" class="tableBorder">
                    <tr>
                        <td valign="bottom">
                            <table class="TabStyle">
                                <tr>
                                    <td>
                                        &nbsp;&nbsp;&nbsp;<asp:Literal ID="litSurveyName" runat="Server"></asp:Literal>&nbsp;&nbsp;&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="normal1" align="left" height="23">
                            &nbsp;&nbsp;(<span class="normal4">*</span>) Required
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Table ID="tblProspects" runat="server" GridLines="None" BorderColor="black"
                                Width="100%" BorderWidth="0" CellSpacing="0" CellPadding="0" CssClass="aspTable">
                                <asp:TableRow>
                                    <asp:TableCell VerticalAlign="Top">
                                        <br />
                                        <asp:PlaceHolder ID="plhFormControls" runat="server"></asp:PlaceHolder>
                                        <br />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Table ID="tblAlreadyRegisteredUser" Style="display: none" runat="server" Width="100%"
                                BorderWidth="0" BorderColor="black" GridLines="None">
                                <asp:TableRow>
                                    <asp:TableCell CssClass="normal1" Width="20%" HorizontalAlign="Right">
						Enter Email <span class="normal4">*</span>&nbsp;
                                    </asp:TableCell>
                                    <asp:TableCell CssClass="normal1" Width="80%">
                                        <asp:TextBox ID="txtExistingContactEmail" runat="server" CssClass="signup" MaxLength="50"></asp:TextBox>&nbsp;
                                        <i>If you�re already in our system, type your email address and proceed to the survey.</i>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table id="tblNavgationControls" cellspacing="0" cellpadding="0" width="100%" runat="Server">
                                <tr>
                                    <td valign="bottom" align="left">
                                        &nbsp;&nbsp;<asp:Button ID="btnExistingUsers" runat="server" CssClass="button" Text="Existing Users click here">
                                        </asp:Button>&nbsp;
                                    </td>
                                    <td valign="bottom" align="right">
                                        <asp:Button ID="btnContinue" runat="server" CssClass="button" Text="Submit" Style="display: inline">
                                        </asp:Button>&nbsp;
                                        <asp:Button ID="btnSignUp" runat="server" CssClass="button" Text="Submit" Style="display: none"
                                            CausesValidation="False"></asp:Button>&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblFooter" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:ValidationSummary ID="ValidationSummary" runat="server" DisplayMode="List" ShowMessageBox="True"
        ShowSummary="False" HeaderText="Please check the following value(s)"></asp:ValidationSummary>
    <asp:Literal ID="litClientScript" runat="server"></asp:Literal>
    <input id="hdRecOwner" type="hidden" name="hdRecOwner" runat="server" value="0">
    <input id="hdGroupId" type="hidden" name="hdGroupId" runat="server" value="0">
    <input id="hdCRMType" type="hidden" name="hdCRMType" runat="server" value="0">
    <input id="hdRelationShip" type="hidden" name="hdRelationShip" runat="server" value="0">
    <input id="hdProfile" type="hidden" name="hdProfile" runat="server">
    <input id="hdCreateDBEntry" type="hidden" name="hdCreateDBEntry" runat="server" value="0">
    </form>
</body>
</html>
