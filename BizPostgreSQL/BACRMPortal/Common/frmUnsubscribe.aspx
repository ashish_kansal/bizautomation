﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmUnsubscribe.aspx.vb"
    Inherits="BACRMPortal.frmUnsubscribe" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        BODY
        {
            font-family: Arial;
            font-size: 10pt;
            font-style: normal;
            font-weight: normal;
            line-height: normal;
            margin: 0px 0px 0px 0px;
            text-decoration: none;
            background-color: white;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Table ID="tblOriginal" BorderWidth="0" runat="server" Width="100%" BorderColor="black"
        GridLines="None">
        <asp:TableRow>
            <asp:TableCell>
                <table runat="server" id="tblUnsub" width="100%">
                 <tr>
                        <td valign="top" align="left">
                            <img runat="server" id="imgLogo" >
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <fieldset>
                                <legend><b>Unsubscribe Mailing List</b></legend>
                                <br />
                                Please confirm that you want to unsubscribe. Keep in mind that if you unsubscribe,
                                you will not receive information about holiday / celebration discounts and other
                                event-related great deals.
                                <br />
                                To unsubscribe, please enter your e-mail address below and press "Unsubscribe".
                                <br />
                                <br />
                                <asp:TextBox ID="txtUnsubscribe" runat="server"></asp:TextBox>
                                <asp:Button ID="btnUnsubscribe" runat="server" Text="Unsubscribe" />
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:Table ID="tblFormatted" Visible="false" BorderWidth="0" runat="server" Width="100%"
        BorderColor="black" GridLines="None">
        <asp:TableRow>
            <asp:TableCell>
                <asp:Literal ID="litTemplate" runat="server"></asp:Literal>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:Label runat="server" Visible="false" ID="lblMessage" Text="Your subscription preferences have been saved."></asp:Label>
    </form>
</body>
</html>
