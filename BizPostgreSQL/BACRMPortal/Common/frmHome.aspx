<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmHome.aspx.vb" Inherits="BACRMPortal.Common_frmHome" %>
<%@ Register TagPrefix="menu1" TagName="Menu" src="../common/topbar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <title>Home</title>
</head>
<body>

    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
    <menu1:menu id="webmenu1" runat="server"></menu1:menu>
     <asp:updatepanel ID="updatepanel1" runat="server" ChildrenAstriggers="true" UpdateMode="Conditional" EnableViewState="true">
		<ContentTemplate>
                <iframe width="100%" scrolling=auto   height="1000px" frameborder="0" src="../go.aspx" runat="server" id="ifHome"></iframe>
   </ContentTemplate>
   </asp:updatepanel>
    </form>
</body>
</html>
