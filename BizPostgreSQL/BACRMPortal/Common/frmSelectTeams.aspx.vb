Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Forecasting
Imports BACRM.BusinessLogic.Common
Partial Public Class frmSelectTeams
    Inherits BACRMPage

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not IsPostBack Then
                Dim objUserAccess As New UserAccess
                Dim dtTeamUsers As DataTable
                objUserAccess.UserCntID = Session("UserContactID")
                objUserAccess.DomainID = Session("DomainID")
                dtTeamUsers = objUserAccess.GetTeamForUsers
                lstTeamAvail.DataSource = dtTeamUsers
                lstTeamAvail.DataTextField = "vcData"
                lstTeamAvail.DataValueField = "numlistitemid"
                lstTeamAvail.DataBind()

                Dim objForeCast As New Forecast
                Dim dtTeamForForecast As DataTable
                objForeCast.UserCntID = Session("UserContactID")
                objForeCast.DomainID = Session("DomainID")
                objForeCast.ForecastType = GetQueryStringVal( "Type")
                dtTeamForForecast = objForeCast.GetTeamsForForRep
                lstTeamAdd.DataSource = dtTeamForForecast
                lstTeamAdd.DataTextField = "vcData"
                lstTeamAdd.DataValueField = "numlistitemid"
                lstTeamAdd.DataBind()
            End If
            btnAdd.Attributes.Add("OnClick", "return move(document.Form1.lstTeamAvail,document.Form1.lstTeamAdd)")
            btnRemove.Attributes.Add("OnClick", "return remove(document.Form1.lstTeamAdd,document.Form1.lstTeamAvail)")
            btnSave.Attributes.Add("onclick", "Save()")
            btnClose.Attributes.Add("onclick", "return Close()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim ObjForeCast As New Forecast
            ObjForeCast.strTeam = hdnValue.Text
            ObjForeCast.UserCntID = Session("UserContactID")
            ObjForeCast.DomainID = Session("DomainID")
            ObjForeCast.ForecastType = GetQueryStringVal( "Type")
            ObjForeCast.ManageTeamsForForRept()
            Response.Write("<script>opener.PopupCheck(); self.close();</script>")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class