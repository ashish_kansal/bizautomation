Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Leads

Partial Public Class frmNewOrganization : Inherits BACRMPage

    Dim lngDivisionId As Long

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            btnSave.Attributes.Add("onclick", "return Save()")
            btnClose.Attributes.Add("onclick", "return Close()")
            btnBack.Attributes.Add("onclick", "return Back()")
            If Not IsPostBack Then
                Dim dtTab As DataTable
                dtTab = Session("DefaultTab")
                Session("Help") = "Organization"
                Dim objCommon As New CCommon
                objCommon.sb_FillComboFromDBwithSel(ddlRelationShip, 5, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlTerritory, 78, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlInfoSource, 18, Session("DomainID"))
                objCommon.sb_FillGroupsFromDBSel(ddlGroup)
                For Each Item As ListItem In ddlGroup.Items
                    If Item.Text = "My Leads" Then
                        Item.Text = "My " & dtTab.Rows(0).Item("vcLead") & "s"
                    ElseIf Item.Text = "PublicLeads" Then
                        Item.Text = "Public " & dtTab.Rows(0).Item("vcLead") & "s"
                    ElseIf Item.Text = "WebLeads" Then
                        Item.Text = "Web " & dtTab.Rows(0).Item("vcLead") & "s"
                    End If
                Next
                If GetQueryStringVal( "RelID") <> "" Then
                    trRelationship.Visible = False
                    If Not ddlRelationShip.Items.FindByValue(GetQueryStringVal( "RelID")) Is Nothing Then
                        ddlRelationShip.Items.FindByValue(GetQueryStringVal( "RelID")).Selected = True
                    End If
                    lblCustomer.Text = ddlRelationShip.SelectedItem.Text
                    lblRelationship.Text = ddlRelationShip.SelectedItem.Text
                Else
                    If GetQueryStringVal( "CRMType") = 0 Then
                        trGroups.Visible = True
                        lblCustomer.Text = dtTab.Rows(0).Item("vcLead")
                        lblRelationship.Text = dtTab.Rows(0).Item("vcLead")
                    ElseIf GetQueryStringVal( "CRMType") = 1 Then
                        lblCustomer.Text = dtTab.Rows(0).Item("vcProspect")
                        lblRelationship.Text = dtTab.Rows(0).Item("vcProspect")
                    ElseIf GetQueryStringVal( "CRMType") = 2 Then
                        lblCustomer.Text = dtTab.Rows(0).Item("vcAccount")
                        lblRelationship.Text = dtTab.Rows(0).Item("vcAccount")
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Function InsertProspect() As Boolean
        Dim objLeads As New CLeads
        Try
            With objLeads
                If Len(txtCompany.Text) < 1 Then txtCompany.Text = txtLastName.Text & "," & txtFirstName.Text
                .CompanyName = txtCompany.Text.Trim
                .InfoSource = ddlInfoSource.SelectedItem.Value
                .TerritoryID = ddlTerritory.SelectedItem.Value
                .CustName = txtCompany.Text.Trim
                If GetQueryStringVal( "RelID") <> "" Then
                    .CompanyType = ddlRelationShip.SelectedValue
                    .CRMType = 1
                Else
                    .CompanyType = ddlRelationShip.SelectedValue
                    .CRMType = GetQueryStringVal( "CRMType")
                End If
                .DivisionName = "-"
                .LeadBoxFlg = 1
                .UserCntID = Session("UserContactID")
                .Country = Session("DefCountry")
                .SCountry = Session("DefCountry")
                .FirstName = txtFirstName.Text
                .LastName = txtLastName.Text
                .ContactPhone = txtPhone.Text
                .PhoneExt = txtextn.Text
                .Email = txtEmail.Text
                .DomainID = Session("DomainID")
                .ContactType = 70
                .GroupID = ddlGroup.SelectedValue
            End With
            objLeads.CompanyID = objLeads.CreateRecordCompanyInfo
            lngDivisionId = objLeads.CreateRecordDivisionsInfo
            objLeads.DivisionID = lngDivisionId
            objLeads.CreateRecordAddContactInfo()
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If InsertProspect() = True Then
                Dim strScript As String = "<script language=JavaScript>"
                If GetQueryStringVal( "RelID") <> "" Then
                    strScript += "window.opener.reDirect('../Organization/frmPartnerProsDtl.aspx?DivID=" & lngDivisionId & "'); self.close();"
                Else
                    If GetQueryStringVal( "CRMType") = 0 Then
                        strScript += "window.opener.reDirect('../Leads/frmLeads.aspx?DivID=" & lngDivisionId & "'); self.close();"
                    ElseIf GetQueryStringVal( "CRMType") = 1 Then
                        strScript += "window.opener.reDirect('../Organization/frmPartnerProsDtl.aspx?DivID=" & lngDivisionId & "'); self.close();"
                    ElseIf GetQueryStringVal( "CRMType") = 2 Then
                        strScript += "window.opener.reDirect('../Organization/frmPartnerActDtl.aspxDivID=" & lngDivisionId & "'); self.close();"
                    End If
                End If
                strScript += "</script>"
                If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class