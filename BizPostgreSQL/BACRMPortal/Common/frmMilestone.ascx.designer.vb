﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Milestone

    '''<summary>
    '''tblMileStoneDetail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblMileStoneDetail As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''tblProgress control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblProgress As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''lblProjectName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblProjectName As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTotalProgress control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTotalProgress As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddlProcessList control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlProcessList As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''btnAddProcess control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAddProcess As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnRemoveProcess control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnRemoveProcess As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''tblRowStatge control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblRowStatge As Global.System.Web.UI.WebControls.TableRow

    '''<summary>
    '''btnColExp control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnColExp As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''tvProjectStage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tvProjectStage As Global.System.Web.UI.WebControls.TreeView

    '''<summary>
    '''mvStage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents mvStage As Global.System.Web.UI.WebControls.MultiView

    '''<summary>
    '''vDetail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents vDetail As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''lblDAssignTo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDAssignTo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblDLastModify control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDLastModify As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''btnEdit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnEdit As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''lblDStageName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDStageName As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblDCreatedBy control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDCreatedBy As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblDStageProgress control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDStageProgress As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblDStartDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDStartDate As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''hplDDocuments control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hplDDocuments As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''lblDDueDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDDueDate As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblDDescription control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDDescription As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''trDTimeDetail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trDTimeDetail As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''hplDStageTime control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hplDStageTime As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''lblDStageTime control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDStageTime As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblDTime control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDTime As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblTimeBudget control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTimeBudget As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''trDExpnseDetail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trDExpnseDetail As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''hplDStageExpense control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hplDStageExpense As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''lblDStageExpense control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDStageExpense As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblDExpense control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblDExpense As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblExpenseBudget control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblExpenseBudget As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''trDDependencies control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trDDependencies As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''tvDependencies control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tvDependencies As Global.System.Web.UI.WebControls.TreeView

    '''<summary>
    '''frmComments1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents frmComments1 As Global.BACRMPortal.frmComments

    '''<summary>
    '''vEdit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents vEdit As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''btnAdd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAdd As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnDelete control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnDelete As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSave As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''hfStagePerID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfStagePerID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfConfiguration control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfConfiguration As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfSalesProsID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfSalesProsID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfMileStoneName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfMileStoneName As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfParentStageID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfParentStageID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfStageID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfStageID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfDueDays control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfDueDays As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfAssignTo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfAssignTo As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfProjectID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfProjectID As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hplEAssignTo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hplEAssignTo As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''lblEAssignToText control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEAssignToText As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblEAssignTo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEAssignTo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblELastModify control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblELastModify As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtStageName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtStageName As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ddStageProgress control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddStageProgress As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''hplEDocuments control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hplEDocuments As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''trDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trDate As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''calStartDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents calStartDate As Global.BACRMPortal.calandar

    '''<summary>
    '''calDueDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents calDueDate As Global.BACRMPortal.calandar

    '''<summary>
    '''txtDescription control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtDescription As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''trETimeDetail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trETimeDetail As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''hplEStageTime control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hplEStageTime As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''lblEStageTime control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEStageTime As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblETime control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblETime As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''spTimeBudget control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents spTimeBudget As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''chkTimeBudget control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkTimeBudget As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''txtTimeBudget control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTimeBudget As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''trEExpenseDetail control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trEExpenseDetail As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''hplEStageExpense control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hplEStageExpense As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''lblEStageExpense control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEStageExpense As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblEExpense control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblEExpense As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''spExpenseBudget control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents spExpenseBudget As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''chkExpenseBudget control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkExpenseBudget As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''txtExpenseBudget control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtExpenseBudget As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''trEDependencies control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trEDependencies As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''ddlProjectStage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlProjectStage As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''btnDependencies control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnDependencies As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''gvDependencies control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents gvDependencies As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''vAdd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents vAdd As Global.System.Web.UI.WebControls.View

    '''<summary>
    '''btnAddSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAddSave As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''rdbStageType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents rdbStageType As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''txtAStageName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtAStageName As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''hfParentStageID1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfParentStageID1 As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''hfStageID1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfStageID1 As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''lblAStagePercentage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAStagePercentage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''hfSetPercentage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hfSetPercentage As Global.System.Web.UI.WebControls.HyperLink

    '''<summary>
    '''calAStartDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents calAStartDate As Global.BACRMPortal.calandar

    '''<summary>
    '''calADueDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents calADueDate As Global.BACRMPortal.calandar

    '''<summary>
    '''txtADescription control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtADescription As Global.System.Web.UI.WebControls.TextBox
End Class
