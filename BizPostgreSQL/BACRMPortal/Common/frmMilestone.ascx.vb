﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Opportunities
Imports System.Globalization

Partial Public Class Milestone
    Inherits BACRMUserControl

    Dim dtStages, _dtStageDetail, dtParentStage, dtProjectProcess, dtProjectProgress As New DataTable
    Dim intNodeFound As Boolean = False
    Dim Pnode As New TreeNode
    Dim objCommon As New CCommon
    Dim objAdmin As New CAdmin
    Dim objOpportunity As MOpportunity
    Dim objProject As New Project
    Public m_aryRightsForPage(), m_aryRightsForBudget(), m_aryRightsForStage(), m_aryRightsForDate(), m_aryRightsForAssign(), m_aryRightsForTime(), m_aryRightsForExpense() As Integer
    Public _ModuleType As ModuleType
    ' lblD = lbl for detail view
    ' lblE = lbl for Edit view 

    Enum ModuleType As Integer
        Opportunity
        Projects
    End Enum
    Public Property RightsForPage() As Integer()
        Get
            Return m_aryRightsForPage
        End Get
        Set(ByVal value As Integer())
            m_aryRightsForPage = value
        End Set
    End Property

    Private _OppID As Long
    Public Property OppID() As Long
        Get
            Return _OppID
        End Get
        Set(ByVal value As Long)
            _OppID = value
        End Set
    End Property
    Public _ProjectId As Long
    Public Property ProjectId As Long
        Get
            Return _ProjectId
        End Get
        Set(ByVal value As Long)
            _ProjectId = value
        End Set
    End Property

    Private _DivisionID As Long
    Public Property DivisionID() As Long
        Get
            Return _DivisionID
        End Get
        Set(ByVal value As Long)
            _DivisionID = value
        End Set
    End Property

    Private _ProjectRecOwner As Long
    Public Property ProjectRecOwner() As Long
        Get
            Return _ProjectRecOwner
        End Get
        Set(ByVal value As Long)
            _ProjectRecOwner = value
        End Set
    End Property

    Private _ProType As Short
    Public Property ProType() As Short
        Get
            Return _ProType
        End Get
        Set(ByVal value As Short)
            _ProType = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.IsPostBack Then
            Dim eventTarget As String = If((Me.Request("__EVENTTARGET") Is Nothing), String.Empty, Me.Request("__EVENTTARGET"))

            If eventTarget = "btnAssignChange" Then
                BindProcessControl()

                objAdmin.OppID = _OppID
                objAdmin.ProjectID = _ProjectId
                objAdmin.UserCntID = Session("UserContactID")
                objAdmin.Mode = 1
                Dim dtlAccess As DataTable = objAdmin.GetStageAccessDetail()

                If dtlAccess.Rows.Count > 0 Then
                    intNodeFound = False
                    Pnode = Nothing
                    FindNodeByValue(tvProjectStage.Nodes, dtlAccess.Rows(0).Item("numStageDetailsId").ToString())

                    If Pnode IsNot Nothing Then
                        tvProjectStage.FindNode(Pnode.ValuePath).Selected = True
                    End If
                End If
            ElseIf eventTarget = "btnDeleteConfirm" Then
                Try
                    objAdmin.DomainID = Session("DomainId")
                    objAdmin.ModeType = _ModuleType
                    objAdmin.ProjectID = IIf(_ModuleType = ModuleType.Projects, _ProjectId, _OppID)
                    objAdmin.numStageDetailsId = tvProjectStage.SelectedNode.Value

                    objAdmin.DeleteMilestoneStage()

                    Dim dtStagePercentage As New DataTable
                    dtStagePercentage = Session("StagePercentage")

                    Dim ds As New DataSet
                    ds.Tables.Add(dtStagePercentage)
                    objAdmin.StageDetail = ds.GetXml()
                    objAdmin.Update_StagePercentageDetails()

                    Session("StagePercentage") = Nothing

                    BindRepeaterControl()
                    'BindTotalProgress()
                    tvProjectStage.ExpandAll()
                    mvStage.ActiveViewIndex = -1
                Catch ex As Exception
                    If ex.Message = "DEPENDANT" Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "alert", "alert('Can not remove record, Your option is to remove Time and Expense associated with this stages and try again.')", True)
                    Else
                        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                        Response.Write(ex)
                    End If
                End Try
            End If

            If eventTarget = "btnAssignChange" Or eventTarget = "btnChangeTimeExpense" Then
                If tvProjectStage.SelectedNode IsNot Nothing Then
                    objAdmin = New CAdmin
                    objAdmin.DomainID = Session("DomainId")
                    objAdmin.SalesProsID = tvProjectStage.SelectedNode.Value
                    objAdmin.Mode = 2
                    _dtStageDetail = objAdmin.StageItemDetails()
                    If _dtStageDetail.Rows.Count = 1 Then
                        lblEAssignTo.Text = _dtStageDetail.Rows(0).Item("vcAssignTo").ToString()

                        lblDTime.Text = String.Format("{0}", _dtStageDetail.Rows(0).Item("numTime"))
                        lblDExpense.Text = String.Format("Expense Amount:({0})", _dtStageDetail.Rows(0).Item("numExpense"))

                        lblETime.Text = String.Format("{0}", _dtStageDetail.Rows(0).Item("numTime"))
                        lblEExpense.Text = String.Format("Expense Amount:({0})", _dtStageDetail.Rows(0).Item("numExpense"))
                    End If
                End If
            End If


        End If

        m_aryRightsForBudget = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmProjects.aspx", Session("userID"), 12, 7)
        m_aryRightsForStage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmProjects.aspx", Session("userID"), 12, 8)
        m_aryRightsForDate = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmProjects.aspx", Session("userID"), 12, 9)
        m_aryRightsForAssign = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmProjects.aspx", Session("userID"), 12, 10)
        m_aryRightsForTime = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmProjects.aspx", Session("userID"), 12, 11)
        m_aryRightsForExpense = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmProjects.aspx", Session("userID"), 12, 12)

        If m_aryRightsForTime(RIGHTSTYPE.VIEW) = 0 Then
            hplDStageTime.Visible = False
            lblDStageTime.Visible = True

            hplEStageTime.Visible = False
            lblEStageTime.Visible = True
        Else
            hplDStageTime.Visible = True
            lblDStageTime.Visible = False

            hplEStageTime.Visible = True
            lblEStageTime.Visible = False
        End If

        If m_aryRightsForExpense(RIGHTSTYPE.VIEW) = 0 Then
            hplDStageExpense.Visible = False
            lblDStageExpense.Visible = True

            hplEStageExpense.Visible = False
            lblEStageExpense.Visible = True
        Else
            hplDStageExpense.Visible = True
            lblDStageExpense.Visible = False

            hplEStageExpense.Visible = True
            lblEStageExpense.Visible = False
        End If

        If Not Page.IsPostBack Then
            Try
                Session("StagePercentage") = Nothing

                If RightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AC")
                End If

                If _ProjectRecOwner <> Session("UserContactID") Then
                    btnRemoveProcess.Visible = False
                End If

                BindProcessControl()

                If Not GetQueryStringVal( "StageId") Is Nothing Then
                    Dim StageId As Integer
                    StageId = GetQueryStringVal( "StageId")
                    intNodeFound = False
                    Pnode = Nothing
                    FindNodeByValue(tvProjectStage.Nodes, StageId)

                    If Pnode IsNot Nothing Then
                        tvProjectStage.FindNode(Pnode.ValuePath).Selected = True
                        selectNode()
                    End If
                Else
                    objAdmin.OppID = _OppID
                    objAdmin.ProjectID = _ProjectId
                    objAdmin.UserCntID = Session("UserContactID")
                    objAdmin.Mode = 1
                    Dim dtlAccess As DataTable = objAdmin.GetStageAccessDetail()

                    If dtlAccess.Rows.Count > 0 Then
                        intNodeFound = False
                        Pnode = Nothing
                        FindNodeByValue(tvProjectStage.Nodes, dtlAccess.Rows(0).Item("numStageDetailsId").ToString())

                        If Pnode IsNot Nothing Then
                            tvProjectStage.FindNode(Pnode.ValuePath).Selected = True
                            selectNode()
                        End If
                    End If
                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End If

        Page.RegisterStartupScript("TotalProgressBar", "<script language=javascript>progress(" & CCommon.ToInteger(lblTotalProgress.Text.Replace("%", "").Trim) & ",'TotalProgressContainer','TotalProgress',120);</script>")

        If mvStage.ActiveViewIndex = 0 Then
            Page.RegisterStartupScript("MyProgressBar", "<script language=javascript>progress(" & lblDStageProgress.Text.Replace("%", "").Trim() & ",'ProgressContainer','progress',60);</script>")
        End If

        If (tvProjectStage.SelectedNode IsNot Nothing) Then
            frmComments1.StageID = tvProjectStage.SelectedNode.Value
            Session("show") = 1
        End If

        btnSave.Attributes.Add("onclick", "return EditSvae()")
        btnAddSave.Attributes.Add("onclick", "return EditSvae()")
    End Sub
    'Public Sub BindTotalProgress()
    '    Try
    '        objProject = New Project
    '        objProject.DomainID = Session("DomainId")
    '        objProject.ContactID = Session("UserContactID")
    '        objProject.ProjectID = IIf(_ModuleType = ModuleType.Projects, _ProjectId, _OppID)
    '        objProject.Mode = _ModuleType
    '        dtProjectProgress = objProject.USP_GetProjectTotalProgress()



    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Public Sub BindProcessControl()
        Try
            objAdmin = New CAdmin
            objAdmin.DomainID = Session("DomainId")
            objAdmin.SalesProsID = IIf(_ModuleType = ModuleType.Projects, _ProjectId, _OppID)
            objAdmin.Mode = IIf(_ModuleType = ModuleType.Projects, 3, 4)
            _dtStageDetail = objAdmin.StageItemDetails()

            If _dtStageDetail.Rows.Count = 1 Then
                LoadBusinessProcess()

                If _dtStageDetail.Rows(0).Item("Total") > 0 Then
                    'tblMile.Visible = False
                    ddlProcessList.Enabled = False
                    btnAddProcess.Visible = False

                    tblProgress.Visible = True
                    tblRowStatge.Visible = True

                    BindRepeaterControl()
                    'BindTotalProgress()
                    tvProjectStage.ExpandAll()
                Else
                    'tblMile.Visible = True
                    ddlProcessList.Enabled = True
                    btnAddProcess.Visible = True
                    btnRemoveProcess.Visible = False

                    tblProgress.Visible = True
                    tblRowStatge.Visible = False
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub BindRepeaterControl()
        Try
            objProject = New Project
            objProject.DomainID = Session("DomainId")
            objProject.ContactID = Session("UserContactID")
            objProject.ProjectID = IIf(_ModuleType = ModuleType.Projects, _ProjectId, _OppID)
            objProject.Mode = 2 ' _ModuleType
            Dim dsStages As DataSet = objProject.GetProjectStageHierarchy()
            dtStages = dsStages.Tables(0)

            tvProjectStage.Nodes.Clear()

            If dtStages.Rows.Count > 0 Then
                Dim node As New TreeNode

                Dim color As System.Drawing.Color
                Dim DueDateColor As System.Drawing.Color = Drawing.Color.Black

                Dim startDate As DateTime
                Dim endDate As DateTime
                Dim dueDateSpan As TimeSpan
                Dim text As String
                tvProjectStage.Nodes.Clear()

                For Each r As DataRow In dtStages.Rows

                    'If cblUser.Items.FindByValue(r.Item("numContactId").ToString()) IsNot Nothing Then
                    '    cblUser.Items.FindByValue(r.Item("numContactId").ToString()).Selected = True
                    'End If
                    If ddlProcessList.Items.FindByValue(r.Item("slp_id")) IsNot Nothing Then
                        ddlProcessList.Items.FindByValue(r.Item("slp_id")).Selected = True
                    End If

                    Select Case Convert.ToInt32(r.Item("numStage"))
                        Case 0
                            color = Drawing.Color.Black
                        Case 1
                            color = System.Drawing.ColorTranslator.FromHtml("#0000FF")
                        Case 2
                            color = System.Drawing.ColorTranslator.FromHtml("#CCAC74")
                        Case 3
                            color = System.Drawing.Color.Green
                        Case Else
                            color = System.Drawing.Color.Black
                    End Select

                    node = New TreeNode

                    text = String.Empty
                    DueDateColor = System.Drawing.Color.Black

                    If Session("DateFormat").ToString() = "DD/MM/YYYY" Then
                        endDate = Date.Parse(r.Item("dtEndDate"), New CultureInfo("fr-FR", False))
                    Else
                        endDate = DateTime.Parse(r.Item("dtEndDate"))
                    End If

                    If Convert.ToBoolean(r.Item("bitClose")) = True Then
                        'endDate = r.Item("dtEndDate")
                        text = "Completed " + r.Item("dtEndDate")
                    Else
                        'startDate = r.Item("dtStartDate")
                        dueDateSpan = endDate.Date.Subtract(Date.Now)

                        If tvProjectStage.Nodes.Count = 0 Then
                            text = "Start " + r.Item("dtStartDate") + ", "
                        End If

                        'text = text + "Due " + r.Item("dtEndDate") + ", " + String.Format("<strong>{0:# days to go;# days late ;0 days to go}</strong>", dueDateSpan.Days)

                        If dueDateSpan.Days < 0 Then
                            DueDateColor = System.Drawing.Color.Red
                        End If
                        text = String.Format("{0} Due {1}, <font style='color: {3};font-weight: bold;'>{2:# days to go;# days late ;0 days to go}</font>", text, r.Item("dtEndDate"), dueDateSpan.Days, System.Drawing.ColorTranslator.ToHtml(DueDateColor))
                    End If


                    node.Text = String.Format("<font style='color: {0};font-weight: bold;'>{1}</font> : {2} <font color=green>({3} %)</font> {4} <span title=""Percentages this stage contributes in milestone""><font style='color:gray;font-size:smaller'>[{5} %]</font></span>",
                                             System.Drawing.ColorTranslator.ToHtml(color), r.Item("vcStageName").ToString(), text,
                                             r.Item("tinProgressPercentage").ToString(), r.Item("vcAssignTo").ToString(), r.Item("tintPercentage").ToString())

                    node.Value = r.Item("numStageDetailsId").ToString()

                    If Convert.ToInt32(r.Item("numAssignTo")) = Convert.ToInt32(Session("UserContactID")) Then
                        node.Text = String.Format("<img src='../images/CheckMark.jpg' width='12' border='0'/>&nbsp;&nbsp;{0}", node.Text)
                    End If

                    intNodeFound = False
                    Pnode = Nothing
                    FindNodeByValue(tvProjectStage.Nodes, r.Item("numParentStageID").ToString())

                    If Pnode Is Nothing Then
                        Pnode = tvProjectStage.FindNode(r.Item("vcMileStoneName").ToString())
                        If Pnode Is Nothing Then
                            Dim MainNode As New TreeNode
                            MainNode.Text = String.Format("<font style='color: black;font-weight: bold;font-size: 9pt;'>{0}</font> <span title=""i.e. Holds {1}% of total completion""><font style='color:gray;font-size:smaller'>[{1} %]</font></span>", r.Item("vcMileStoneName").ToString().Trim(), r.Item("numStagePercentage").ToString())
                            MainNode.Value = r.Item("vcMileStoneName").ToString()
                            MainNode.SelectAction = TreeNodeSelectAction.Expand
                            tvProjectStage.Nodes.Add(MainNode)

                            MainNode.ChildNodes.Add(node)
                        Else
                            Pnode.ChildNodes.Add(node)
                        End If

                    Else
                        Pnode.ChildNodes.Add(node)
                    End If
                Next
            Else
                mvStage.ActiveViewIndex = -1
            End If

            If dsStages.Tables.Count = 2 Then
                If dsStages.Tables(1).Rows.Count = 1 Then
                    lblProjectName.Text = dsStages.Tables(1).Rows(0).Item("ProjectName")
                    lblTotalProgress.Text = dsStages.Tables(1).Rows(0).Item("TotalProgress").ToString() + " %"
                    Page.RegisterStartupScript("TotalProgressBar1", "<script language=javascript>progress(" & CCommon.ToInteger(dsStages.Tables(1).Rows(0).Item("TotalProgress")) & ",'TotalProgressContainer','TotalProgress',120);</script>")
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub FindNodeByValue(ByVal n As TreeNodeCollection, ByVal val As String)
        Try
            For i As Integer = 0 To n.Count - 1
                If n(i).Value = val Then
                    'n(i).[Select]()
                    intNodeFound = True
                    Pnode = n(i)
                    Return
                    Exit Sub
                End If
                'n(i).Expand()
                If n(i).ChildNodes.Count > 0 Then
                    FindNodeByValue(n(i).ChildNodes, val)
                End If
                If intNodeFound Then
                    Return
                End If
                'n(i).Collapse()
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Public Sub BindRepeaterControl()
    '    Try
    '        Dim objProject As New Project
    '        dtStages = objProject.GetProjectStageHierarchy(Session("DomainId"), ProjectId, 1)

    '        If dtStages.Rows.Count > 0 Then
    '            Dim node As New TreeNode
    '            Dim Pnode As New TreeNode
    '            Dim color As System.Drawing.Color
    '            Dim DueDateColor As System.Drawing.Color = Drawing.Color.Black

    '            Dim startDate As DateTime
    '            Dim dueDate As DateTime
    '            Dim compDate As DateTime
    '            Dim dueDateSpan As TimeSpan
    '            Dim text As String

    '            For Each r As DataRow In dtStages.Rows

    '                'If cblUser.Items.FindByValue(r.Item("numContactId").ToString()) IsNot Nothing Then
    '                '    cblUser.Items.FindByValue(r.Item("numContactId").ToString()).Selected = True
    '                'End If
    '                Select Case Convert.ToInt32(r.Item("numStage"))
    '                    Case 0
    '                        color = Drawing.Color.Black
    '                    Case 1
    '                        color = System.Drawing.Color.DarkBlue
    '                    Case 2
    '                        color = System.Drawing.Color.DarkMagenta
    '                    Case 3
    '                        color = System.Drawing.Color.Green
    '                    Case Else
    '                        color = System.Drawing.Color.Black
    '                End Select

    '                node = New TreeNode

    '                text = String.Empty
    '                DueDateColor = System.Drawing.Color.Black

    '                If Convert.ToBoolean(r.Item("bitStageCompleted")) = True Then
    '                    compDate = DateTime.Parse(r.Item("bintStageComDate"))
    '                    text = "Completed " + compDate.Date
    '                Else
    '                    startDate = DateTime.Parse(r.Item("bintCreatedDate"))
    '                    dueDate = DateTime.Parse(r.Item("bintDueDate"))
    '                    dueDateSpan = dueDate.Date.Subtract(Date.Now)

    '                    If tvProjectStage.Nodes.Count = 0 Then
    '                        text = "Start " + startDate.Date + ", "
    '                    End If

    '                    text = text + "Due " + dueDate.Date + ", " + String.Format("<strong>{0:# days to go;# days late ;0 days to go}</strong>", dueDateSpan.Days)

    '                    If dueDateSpan.Days < 0 Then
    '                        DueDateColor = System.Drawing.Color.Red
    '                    End If
    '                End If


    '                node.Text = String.Format("<font color={5}><font color={0}>{1}</font> : {2} <font color=green>({3:p})</font> {4}</font>",
    '                                         System.Drawing.ColorTranslator.ToHtml(color), r.Item("vcStageDetail").ToString(), text,
    '                                         r.Item("tinProgressPercentage").ToString(), r.Item("vcAssignTo").ToString(), System.Drawing.ColorTranslator.ToHtml(DueDateColor))

    '                node.Value = r.Item("numProStageId").ToString()
    '                node.NavigateUrl = ""

    '                If tvProjectStage.FindNode(r.Item("numProParentStageId").ToString()) Is Nothing Then
    '                    tvProjectStage.Nodes.Add(node)
    '                Else
    '                    Pnode = tvProjectStage.FindNode(r.Item("numProParentStageId").ToString())

    '                    Pnode.ChildNodes.Add(node)
    '                End If
    '            Next
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Private Sub tvProjectStage_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvProjectStage.SelectedNodeChanged
        Try
            selectNode()
            'BindTotalProgress()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub selectNode()
        Try
            If tvProjectStage.SelectedNode.Value IsNot Nothing Then
                If (CCommon.ToInteger(tvProjectStage.SelectedNode.Value)) Then

                    If m_aryRightsForStage(RIGHTSTYPE.UPDATE) = 0 Then
                        btnEdit.Visible = False
                    End If
                    objAdmin.DomainID = Session("DomainId")
                    objAdmin.SalesProsID = tvProjectStage.SelectedNode.Value
                    objAdmin.Mode = 2
                    _dtStageDetail = objAdmin.StageItemDetails()
                    mvStage.SetActiveView(vDetail)
                    If _dtStageDetail.Rows.Count = 1 Then
                        lblDAssignTo.Text = _dtStageDetail.Rows(0).Item("vcAssignTo").ToString()
                        lblDCreatedBy.Text = _dtStageDetail.Rows(0).Item("vcCreatedBy")
                        lblDLastModify.Text = _dtStageDetail.Rows(0).Item("vcModifiedBy") + ", " + _dtStageDetail.Rows(0).Item("bintModifiedDate")
                        lblDStageName.Text = _dtStageDetail.Rows(0).Item("vcStageName").ToString()
                        lblDStageProgress.Text = String.Format("{0} %", _dtStageDetail.Rows(0).Item("tinProgressPercentage").ToString())
                        lblDStartDate.Text = _dtStageDetail.Rows(0).Item("dtStartDate")

                        hplDDocuments.Text = String.Format("({0})", _dtStageDetail.Rows(0).Item("numDocuments"))
                        'hplDDocuments.Attributes.Add("onclick", "return OpenDocuments(" & _dtStageDetail.Rows(0).Item("numStageDetailsId") & ");")

                        lblDDueDate.Text = _dtStageDetail.Rows(0).Item("dtEndDate")
                        lblDDescription.Text = _dtStageDetail.Rows(0).Item("vcDescription").ToString()
                        lblDTime.Text = String.Format("{0}", _dtStageDetail.Rows(0).Item("numTime"))
                        lblDExpense.Text = String.Format("Expense Amount:({0})", _dtStageDetail.Rows(0).Item("numExpense"))

                        hplDStageTime.Attributes.Add("onclick", "return OpenTime(" & tvProjectStage.SelectedNode.Value & "," & IIf(_ModuleType = ModuleType.Projects, _ProjectId, _OppID) & "," & _DivisionID & ",'" & lblDStageName.Text.Trim.Replace("'", "") & "');")
                        hplDStageExpense.Attributes.Add("onclick", "return OpenExpense(" & tvProjectStage.SelectedNode.Value & "," & IIf(_ModuleType = ModuleType.Projects, _ProjectId, _OppID) & "," & _DivisionID & ",'" & lblDStageName.Text.Trim.Replace("'", "") & "');")

                        If m_aryRightsForBudget(RIGHTSTYPE.VIEW) = 0 Then
                            lblTimeBudget.Visible = False
                            lblExpenseBudget.Visible = False
                        Else
                            lblTimeBudget.Text = String.Format("<strong>Billable Time Budget (in Hrs):</strong>{0:N2}", _dtStageDetail.Rows(0).Item("monTimeBudget"))
                            lblExpenseBudget.Text = String.Format("<strong>Expense Budget (Amt):</strong>{0:N2}", _dtStageDetail.Rows(0).Item("monExpenseBudget"))
                        End If


                        Page.RegisterStartupScript("MyProgressBar1", "<script language=javascript>progress(" & _dtStageDetail.Rows(0).Item("tinProgressPercentage") & ",'ProgressContainer','progress',60);</script>")

                        If _ModuleType = ModuleType.Opportunity Then
                            trDTimeDetail.Visible = False
                            trDExpnseDetail.Visible = False

                            trDDependencies.Visible = False

                            trEDependencies.Visible = False
                            trETimeDetail.Visible = False
                            trEExpenseDetail.Visible = False

                        Else
                            BindTreeDependency()
                        End If

                        BindStageAccessDetail()
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub BindStageAccessDetail()
        Try
            objAdmin.OppID = _OppID
            objAdmin.ProjectID = _ProjectId
            objAdmin.numStageDetailsId = tvProjectStage.SelectedNode.Value
            objAdmin.UserCntID = Session("UserContactID")
            objAdmin.Mode = 0
            objAdmin.ManageStageAccessDetail()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub BindTreeDependency()
        Try
            objAdmin.Mode = 2
            objAdmin.ModeType = _ModuleType
            objAdmin.DomainID = Session("DomainId")
            objAdmin.ProjectID = IIf(_ModuleType = ModuleType.Projects, _ProjectId, _OppID)
            objAdmin.numStageDetailsId = tvProjectStage.SelectedNode.Value

            Dim dtDependency As New DataTable
            dtDependency = objAdmin.GetStageItemDependency()
            tvDependencies.Nodes.Clear()

            Dim MainNode As New TreeNode

            MainNode.Text = String.Format("<strong>{0} ({1})</strong>", "Dependencies", dtDependency.Rows.Count)
            tvDependencies.Nodes.Add(MainNode)
            MainNode.SelectAction = TreeNodeSelectAction.Expand

            If dtDependency.Rows.Count > 0 Then
                Dim node, UpNode, DownNode As New TreeNode

                UpNode.Text = String.Format("<strong>{0}</strong>", "Up Stream")
                MainNode.ChildNodes.Add(UpNode)
                UpNode.SelectAction = TreeNodeSelectAction.Expand

                DownNode.Text = String.Format("<strong>{0}</strong>", "Down Stream")
                MainNode.ChildNodes.Add(DownNode)
                DownNode.SelectAction = TreeNodeSelectAction.Expand

                For Each r As DataRow In dtDependency.Rows

                    node = New TreeNode
                    node.SelectAction = TreeNodeSelectAction.None
                    node.Text = String.Format("{0}", r.Item("vcStageName").ToString())
                    node.Value = r.Item("numStageDetailID").ToString()

                    If r.Item("DependType").ToString() = "UP" Then
                        UpNode.ChildNodes.Add(node)
                    ElseIf r.Item("DependType").ToString() = "DOWN" Then
                        DownNode.ChildNodes.Add(node)
                    End If
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEdit.Click
        Try

            If (CCommon.ToInteger(tvProjectStage.SelectedNode.Value)) Then

                If m_aryRightsForStage(RIGHTSTYPE.ADD) = 0 Then btnAdd.Visible = False

                If m_aryRightsForStage(RIGHTSTYPE.DELETE) = 0 Then btnDelete.Visible = False

                mvStage.SetActiveView(vEdit)

                objAdmin = New CAdmin
                objAdmin.DomainID = Session("DomainId")
                objAdmin.SalesProsID = tvProjectStage.SelectedNode.Value
                objAdmin.Mode = 2
                _dtStageDetail = objAdmin.StageItemDetails()
                If _dtStageDetail.Rows.Count = 1 Then
                    lblEAssignTo.Text = _dtStageDetail.Rows(0).Item("vcAssignTo").ToString()

                    If m_aryRightsForAssign(RIGHTSTYPE.VIEW) = 0 Then
                        lblEAssignToText.Visible = True
                        hplEAssignTo.Visible = False
                    Else
                        hplEAssignTo.Attributes.Add("onclick", "return OpenTransfer('../admin/transferrecord.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=Projects&tyrCV=" + ProjectId.ToString() + "&StageId=" + tvProjectStage.SelectedNode.Value + "')")
                        lblEAssignToText.Visible = False
                        hplEAssignTo.Visible = True
                    End If

                    lblELastModify.Text = _dtStageDetail.Rows(0).Item("vcModifiedBy") + ", " + _dtStageDetail.Rows(0).Item("bintModifiedDate")
                    txtStageName.Text = _dtStageDetail.Rows(0).Item("vcStageName").ToString()

                    'ddStageProgress.SelectedIndex = -1
                    If ddStageProgress.Items.FindByValue(_dtStageDetail.Rows(0).Item("tinProgressPercentage").ToString()) IsNot Nothing Then
                        ddStageProgress.SelectedValue = ddStageProgress.Items.FindByValue(_dtStageDetail.Rows(0).Item("tinProgressPercentage").ToString()).Value
                    End If

                    hplEDocuments.Text = String.Format("({0})", _dtStageDetail.Rows(0).Item("numDocuments"))
                    'hplEDocuments.Attributes.Add("onclick", "return OpenDocuments(" & _dtStageDetail.Rows(0).Item("numStageDetailsId") & ");")



                    If Session("DateFormat").ToString() = "DD/MM/YYYY" Then
                        calStartDate.SelectedDate = Date.Parse(_dtStageDetail.Rows(0).Item("dtStartDate"), New CultureInfo("fr-FR", False))
                        calDueDate.SelectedDate = Date.Parse(_dtStageDetail.Rows(0).Item("dtEndDate"), New CultureInfo("fr-FR", False))
                    Else
                        calStartDate.SelectedDate = _dtStageDetail.Rows(0).Item("dtStartDate").ToString()
                        calDueDate.SelectedDate = _dtStageDetail.Rows(0).Item("dtEndDate").ToString()
                    End If

                    If m_aryRightsForDate(RIGHTSTYPE.ADD) = 0 Then
                        trDate.Visible = False
                    Else
                        trDate.Visible = True
                    End If

                    txtDescription.Text = _dtStageDetail.Rows(0).Item("vcDescription").ToString()
                    lblETime.Text = String.Format("{0}", _dtStageDetail.Rows(0).Item("numTime"))
                    lblEExpense.Text = String.Format("Expense Amount:({0})", _dtStageDetail.Rows(0).Item("numExpense"))

                    hfStagePerID.Value = _dtStageDetail.Rows(0).Item("numStagePercentageId").ToString()
                    hfConfiguration.Value = _dtStageDetail.Rows(0).Item("tintConfiguration").ToString()
                    hfSalesProsID.Value = _dtStageDetail.Rows(0).Item("slp_id").ToString()
                    hfMileStoneName.Value = _dtStageDetail.Rows(0).Item("vcMileStoneName").ToString()
                    hfDueDays.Value = _dtStageDetail.Rows(0).Item("intDueDays").ToString()
                    hfAssignTo.Value = _dtStageDetail.Rows(0).Item("numAssignTo").ToString()
                    hfProjectID.Value = _dtStageDetail.Rows(0).Item("numProjectID").ToString()

                    If m_aryRightsForBudget(RIGHTSTYPE.ADD) = 0 Then
                        spTimeBudget.Visible = False
                        spExpenseBudget.Visible = False
                    End If

                    chkTimeBudget.Checked = _dtStageDetail.Rows(0).Item("bitTimeBudget").ToString()
                    chkExpenseBudget.Checked = _dtStageDetail.Rows(0).Item("bitExpenseBudget").ToString()
                    txtTimeBudget.Text = String.Format("{0:N2}", _dtStageDetail.Rows(0).Item("monTimeBudget"))
                    txtExpenseBudget.Text = String.Format("{0:N2}", _dtStageDetail.Rows(0).Item("monExpenseBudget"))


                    hfParentStageID.Value = _dtStageDetail.Rows(0).Item("numParentStageID").ToString()
                    hfStageID.Value = tvProjectStage.SelectedNode.Value

                    hfParentStageID1.Value = _dtStageDetail.Rows(0).Item("numParentStageID").ToString()
                    hfStageID1.Value = tvProjectStage.SelectedNode.Value

                    hplEStageTime.Attributes.Add("onclick", "return OpenTime(" & tvProjectStage.SelectedNode.Value & "," & IIf(_ModuleType = ModuleType.Projects, _ProjectId, _OppID) & "," & _DivisionID & ",'" & lblDStageName.Text.Trim.Replace("'", "") & "');")
                    hplEStageExpense.Attributes.Add("onclick", "return OpenExpense(" & tvProjectStage.SelectedNode.Value & "," & IIf(_ModuleType = ModuleType.Projects, _ProjectId, _OppID) & "," & _DivisionID & ",'" & lblDStageName.Text.Trim.Replace("'", "") & "');")

                    btnDelete.Attributes.Add("onclick", "var answer = confirm('Are you sure, you want to remove the selected Process?');if (answer){window.open(""../projects/frmDeleteStage.aspx?Type=Parent&StageID=" + tvProjectStage.SelectedNode.Value + """, '', 'toolbar=no,titlebar=no,top=200,width=700,height=450,scrollbars=yes,resizable=yes')}else{return false;}")

                    If (_dtStageDetail.Rows(0).Item("numChildCount") > 0) Then
                        btnDelete.Visible = False
                    Else
                        If m_aryRightsForStage(RIGHTSTYPE.DELETE) = 0 Then
                            btnDelete.Visible = False
                        Else
                            btnDelete.Visible = True
                        End If
                    End If

                    hfSetPercentage.NavigateUrl = ""

                    BindDependency()
                    'BindTotalProgress()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub BindDependency()
        Try
            objAdmin.Mode = 1
            objAdmin.ModeType = _ModuleType
            objAdmin.DomainID = Session("DomainId")
            objAdmin.ProjectID = IIf(_ModuleType = ModuleType.Projects, _ProjectId, _OppID)
            objAdmin.numStageDetailsId = tvProjectStage.SelectedNode.Value

            dtParentStage = objAdmin.GetStageItemDependency()

            ddlProjectStage.DataSource = dtParentStage
            ddlProjectStage.DataTextField = "vcStageName"
            ddlProjectStage.DataValueField = "numStageDetailsId"
            ddlProjectStage.DataBind()
            ddlProjectStage.Items.Insert(0, "--Select One--")
            ddlProjectStage.Items.FindByText("--Select One--").Value = 0

            Dim dtStageItemDependency As DataTable
            objAdmin.Mode = 0
            dtStageItemDependency = objAdmin.GetStageItemDependency()
            gvDependencies.DataSource = dtStageItemDependency
            gvDependencies.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click

        Try
            objAdmin.DomainID = Session("DomainId")
            objAdmin.UserCntID = Session("UserContactID")
            objAdmin.StageDetail = txtStageName.Text
            objAdmin.Description = txtDescription.Text
            objAdmin.StartDate = calStartDate.SelectedDate
            objAdmin.EndDate = calDueDate.SelectedDate
            objAdmin.Assignto = -1
            objAdmin.Percentage = ddStageProgress.SelectedValue
            objAdmin.numStageDetailsId = tvProjectStage.SelectedNode.Value

            objAdmin.StagePerID = hfStagePerID.Value
            objAdmin.Configuration = hfConfiguration.Value
            objAdmin.SalesProsID = hfSalesProsID.Value
            objAdmin.MileStoneName = hfMileStoneName.Value
            objAdmin.ParentStageID = hfParentStageID.Value
            objAdmin.DueDays = hfDueDays.Value

            If ddStageProgress.SelectedValue = 100 Then
                objAdmin.bitClose = True
            Else
                objAdmin.bitClose = False
            End If

            objAdmin.bitTimeBudget = chkTimeBudget.Checked
            objAdmin.bitExpenseBudget = chkExpenseBudget.Checked

            If (chkTimeBudget.Checked) Then
                objAdmin.monTimeBudget = txtTimeBudget.Text
            Else
                objAdmin.monTimeBudget = 0
            End If

            If (chkExpenseBudget.Checked) Then
                objAdmin.monExpenseBudget = txtExpenseBudget.Text
            Else
                objAdmin.monExpenseBudget = 0
            End If


            objAdmin.UpdateStageDetails()
            Dim id As Int32 = tvProjectStage.SelectedNode.Value
            'Dim objProject As New Project
            'objProject.UpdateStagePercentageDetails_User(Session("DomainId"), ProjectId, tvProjectStage.SelectedNode.Value, txtStageName.Text,
            '                                             ddStageProgress.SelectedValue, calStartDate.SelectedDate, calDueDate.SelectedDate, txtDescription.Text, Session("UserContactID"))
            BindRepeaterControl()
            'BindTotalProgress()

            intNodeFound = False
            Pnode = Nothing
            FindNodeByValue(tvProjectStage.Nodes, id)

            If Pnode IsNot Nothing Then
                tvProjectStage.FindNode(Pnode.ValuePath).Selected = True
                selectNode()

            End If

            tvProjectStage.ExpandAll()
            'mvStage.ActiveViewIndex = -1

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click
        Try
            txtAStageName.Text = ""
            txtADescription.Text = ""
            'ddAStageProgress.SelectedValue = 0
            rdbStageType.SelectedValue = 0
            calAStartDate.SelectedDate = ""
            calADueDate.SelectedDate = ""
            mvStage.SetActiveView(vAdd)

            hfSetPercentage.Attributes.Add("onclick", "javascript:CheckStagePercentage()")

            'BindTotalProgress()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub btnAddSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddSave.Click
        Try
            If Session("StagePercentage") Is Nothing Then
                Response.Write("<script language='javascript'>alert('Set % of completion')</script>")
            Else

                Dim dtStagePercentage As New DataTable
                dtStagePercentage = Session("StagePercentage")


                objAdmin.StagePerID = hfStagePerID.Value
                objAdmin.Configuration = hfConfiguration.Value
                objAdmin.StageDetail = txtAStageName.Text
                objAdmin.SalesProsID = hfSalesProsID.Value
                objAdmin.DomainID = Session("DomainId")
                objAdmin.UserCntID = Session("UserContactID")
                objAdmin.Assignto = hfAssignTo.Value
                objAdmin.MileStoneName = hfMileStoneName.Value
                'objAdmin.Percentage = ddAStageProgress.SelectedValue
                objAdmin.Percentage = dtStagePercentage.Rows(dtStagePercentage.Rows.Count - 1)("tintPercentage")
                objAdmin.Description = txtADescription.Text
                objAdmin.StartDate = calAStartDate.SelectedDate
                objAdmin.EndDate = calADueDate.SelectedDate
                objAdmin.ProjectID = _ProjectId
                objAdmin.OppID = _OppID

                If rdbStageType.SelectedValue = 0 Then
                    objAdmin.ParentStageID = hfParentStageID.Value
                Else
                    objAdmin.ParentStageID = tvProjectStage.SelectedNode.Value
                End If
                objAdmin.InsertStageDetails()

                'Dim objProject As New Project
                'objProject.UpdateStagePercentageDetails_User(Session("DomainId"), ProjectId, tvProjectStage.SelectedNode.Value, txtStageName.Text,
                '                                             ddStageProgress.SelectedValue, calStartDate.SelectedDate, calDueDate.SelectedDate, txtDescription.Text, Session("UserContactID"))


                Dim ds As New DataSet
                ds.Tables.Add(dtStagePercentage)
                objAdmin.StageDetail = ds.GetXml()
                objAdmin.Update_StagePercentageDetails()

                Session("StagePercentage") = Nothing

                BindRepeaterControl()
                'BindTotalProgress()
                tvProjectStage.ExpandAll()
                mvStage.ActiveViewIndex = -1

                objAdmin.OppID = _OppID
                objAdmin.ProjectID = _ProjectId
                objAdmin.UserCntID = Session("UserContactID")
                objAdmin.Mode = 1
                Dim dtlAccess As DataTable = objAdmin.GetStageAccessDetail()

                If dtlAccess.Rows.Count > 0 Then
                    intNodeFound = False
                    Pnode = Nothing
                    FindNodeByValue(tvProjectStage.Nodes, dtlAccess.Rows(0).Item("numStageDetailsId").ToString())

                    If Pnode IsNot Nothing Then
                        tvProjectStage.FindNode(Pnode.ValuePath).Selected = True
                        selectNode()
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnDependencies_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDependencies.Click
        Try
            objAdmin = New CAdmin
            objAdmin.numStageDetailsId = tvProjectStage.SelectedNode.Value
            objAdmin.numDependantOnID = ddlProjectStage.SelectedValue
            objAdmin.Mode = 0
            objAdmin.ManageStageItemDependency()

            BindDependency()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub gvDependencies_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDependencies.RowCommand
        If e.CommandName = "DeleteDependency" Then
            Try
                objAdmin = New CAdmin

                objAdmin.numStageDetailsId = gvDependencies.DataKeys(e.CommandArgument).Values("numStageDetailId").ToString()
                objAdmin.numDependantOnID = gvDependencies.DataKeys(e.CommandArgument).Values("numDependantOnId").ToString()
                objAdmin.Mode = 1
                objAdmin.ManageStageItemDependency()

                BindDependency()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End If
    End Sub

    'Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
    '    Try
    '        objAdmin.DomainID = Session("DomainId")
    '        objAdmin.ModeType = _ModuleType
    '        objAdmin.ProjectID = IIf(_ModuleType = ModuleType.Projects, _ProjectId, _OppID)
    '        objAdmin.numStageDetailsId = tvProjectStage.SelectedNode.Value

    '        objAdmin.DeleteMilestoneStage()

    '        BindRepeaterControl()
    '        'BindTotalProgress()
    '        tvProjectStage.ExpandAll()
    '        mvStage.ActiveViewIndex = -1
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub ddlProcessList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProcessList.SelectedIndexChanged
    '    Try
    '        If ddlProcessList.SelectedIndex > 0 Then
    '            'objProject.ProjectProcessId = CInt(ddlProcessList.SelectedItem.Value)
    '            'objProject.ContactID = Session("UserContactID")
    '            'objProject.DomainID = Session("DomainId")
    '            'dtProjectProcess = objProject.SalesProcessDtlByProcessId
    '            objAdmin.DomainID = Session("DomainId")
    '            objAdmin.ProjectID = ProjectId
    '            objAdmin.SalesProsID = CInt(ddlProcessList.SelectedItem.Value)

    '            objAdmin.CopyStagePercentageDetails()
    '            BindRepeaterControl()
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub
    Sub LoadBusinessProcess()
        Try
            If objOpportunity Is Nothing Then objOpportunity = New MOpportunity
            objOpportunity.ProType = _ProType
            objOpportunity.DomainID = Session("DomainID")
            ddlProcessList.DataSource = objOpportunity.BusinessProcess
            ddlProcessList.DataTextField = "slp_name"
            ddlProcessList.DataValueField = "slp_id"
            ddlProcessList.DataBind()
            ddlProcessList.Items.Insert(0, "--Select One--")
            ddlProcessList.Items.FindByText("--Select One--").Value = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnAddProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddProcess.Click
        Try
            If ddlProcessList.SelectedIndex > 0 Then
                'objProject.ProjectProcessId = CInt(ddlProcessList.SelectedItem.Value)
                'objProject.ContactID = Session("UserContactID")
                'objProject.DomainID = Session("DomainId")
                'dtProjectProcess = objProject.SalesProcessDtlByProcessId
                objAdmin.DomainID = Session("DomainId")
                objAdmin.ProjectID = _ProjectId
                objAdmin.OppID = _OppID
                objAdmin.SalesProsID = CInt(ddlProcessList.SelectedItem.Value)
                objAdmin.UserCntID = Session("UserContactID")

                objAdmin.CopyStagePercentageDetails()
                BindProcessControl()
                'BindTotalProgress()

                If _ProjectRecOwner <> Session("UserContactID") Then
                    btnRemoveProcess.Visible = False
                Else
                    btnRemoveProcess.Visible = True
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub


    Private Sub btnColExp_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles btnColExp.Command
        If e.CommandName = "Collapse" Then
            tvProjectStage.CollapseAll()
            btnColExp.Text = "Expand Tree"
            btnColExp.CommandName = "Expand"
            btnColExp.ToolTip = "Expand Project Stage"
        Else
            tvProjectStage.ExpandAll()
            btnColExp.Text = "Collapse Tree"
            btnColExp.CommandName = "Collapse"
            btnColExp.ToolTip = "Collapse Project Stage"
        End If


    End Sub

    Private Sub btnRemoveProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveProcess.Click
        Try

            objAdmin.DomainID = Session("DomainId")
            objAdmin.ModeType = _ModuleType
            objAdmin.ProjectID = IIf(_ModuleType = ModuleType.Projects, _ProjectId, _OppID)
            Try
                objAdmin.RemoveStagePercentageDetails()
            Catch ex As Exception
                If ex.Message = "DEPENDANT" Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "alert", "alert('Can not remove process, Your option is to remove Time and Expense associated with all stages and try again.')", True)
                End If
            End Try

            mvStage.ActiveViewIndex = -1
            BindProcessControl()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class