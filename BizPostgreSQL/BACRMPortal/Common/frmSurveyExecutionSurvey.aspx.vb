Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Survey
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports System.Reflection
Imports BACRM.BusinessLogic.Tracking
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Workflow

Namespace BACRM.UserInterface.Survey
    Public Class frmSurveyExecutionSurvey : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.ID = "frmSurveyExecutionSurvey1"

        End Sub
        Protected WithEvents plhSurveyFormControls As System.Web.UI.WebControls.PlaceHolder
        Protected WithEvents tblNavgationControls As System.Web.UI.HtmlControls.HtmlTable
        Protected WithEvents btnConcludeSurvey As System.Web.UI.WebControls.Button
        Protected WithEvents litTRIDList As System.Web.UI.WebControls.Literal
        Protected WithEvents litSurveyName As System.Web.UI.WebControls.Literal
        Protected WithEvents hdSurId As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdSinglePage As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdRandomAnswers As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdRedirectURL As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdSurveyRating As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdRecOwner As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdGroupId As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdCRMType As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdRelationShip As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdCreateDBEntry As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdTrackableSurveyControlEntries As System.Web.UI.HtmlControls.HtmlInputHidden
        Private numDomainId As Integer
        Dim numThisSurveyQuestionNumber As Integer                              'Declare an Integer Question Number for tracking this Survey (Only)
        Protected WithEvents hdRespondentID As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents btnBack As System.Web.UI.WebControls.Button
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This event is fired each time thepage is called. In this event we will 
        '''     get the data from the DB create the form.
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/07/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        ''' 
        Dim numRating As Integer
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not Session("RecOwner") Is Nothing Then
                    hdRecOwner.Value = Session("RecOwner")
                End If

                numDomainId = GetQueryStringVal( "D")                                          'Get the Domain Id from the url
                Dim numSurId As Integer                                                         'Declare the integer for the Survey Id
                numSurId = GetQueryStringVal( "numSurId")                                      'Pick up the Survey Id from the Query String
                hdRespondentID.Value = GetQueryStringVal( "numRespondantID")                   'Pick up the Respondent Id from the Query String
                hdSurId.Value = numSurId                                                        'Set the Survey Id in local variable
                Dim boolPostRegistration As Boolean
                If Trim(GetQueryStringVal( "bitPostRegistration")) = "" Then
                    boolPostRegistration = False

                Else : boolPostRegistration = True
                End If
                'Put user code to initialize the page here
                If Not IsPostBack Then                                                          'The form loads for the first time
                    callForSurveyGeneration()                                                   'Calls for Survey Generation
                End If
                PostInitializeControlsClientEvents(boolPostRegistration)                        'Call to attache the client events

                Dim objSurvey As New SurveyAdministration
                objSurvey.DomainId = numDomainId
                objSurvey.SurveyId = hdSurId.Value
                objSurvey.PageType = 3
                Dim dsPage As DataSet
                dsPage = objSurvey.GetSurveyTemplate()
                Dim dt As DataTable = dsPage.Tables(0)

                If dsPage.Tables(0).Rows.Count > 0 Then

                    Dim length1 As Integer = dt.Rows(0)("txtTemplate").ToString().IndexOf("#Form#")

                    lblHeader.Text = HttpUtility.HtmlDecode(CCommon.ToString(dt.Rows(0)("txtTemplate").ToString().Substring(0, length1)))
                    lblFooter.Text = HttpUtility.HtmlDecode(CCommon.ToString(dt.Rows(0)("txtTemplate").ToString().Substring(length1 + 6)))

                    If dsPage.Tables(1).Rows.Count > 0 Then
                        MasterCSS.Visible = False
                        For Each dr As DataRow In dsPage.Tables(1).Rows
                            CCommon.AddStyleSheetLink("../Documents/Docs/" & numDomainId & "/Survey/" & dr("StyleFileName").ToString, Me.Page)
                        Next
                    Else
                        MasterCSS.Visible = True
                    End If

                    If dsPage.Tables(2).Rows.Count > 0 Then
                        For Each dr As DataRow In dsPage.Tables(2).Rows
                            CCommon.AddJavascriptLink("../Documents/Docs/" & numDomainId & "/Survey/" & dr("StyleFileName").ToString, Me.Page)
                        Next
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is called to create the interface for Survey Questionnaire.
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        '''-----------------------------------------------------------------------------
        Sub callForSurveyGeneration()
            Try
                Dim objSurvey As New SurveyAdministration                           'Create an object of Survey Administration Class
                objSurvey.SurveyId = hdSurId.Value                                  'Set the Survey ID
                objSurvey.SurveyInfo = objSurvey.getSurveyInformation4Execution
                objSurvey.SurveyInfo.Tables(0).TableName = "SurveyMaster"           'Set the name of the table of Survey Master Infor
                objSurvey.SurveyInfo.Tables(1).TableName = "SurveyQuestionMaster"   'Set the name of the table of Questions
                objSurvey.SurveyInfo.Tables(2).TableName = "SurveyAnsMaster"        'Set the name of the table of Answers
                objSurvey.SurveyInfo.Tables(3).TableName = "SurveyWorkflowRules"    'Set the name of the table of Work Flow Rules
                objSurvey.SurveyInfo.Tables(4).TableName = "SurveyMatrixMaster"    'Set the name of the table of Work Flow Rules

                hdRandomAnswers.Value = objSurvey.SurveyInfo.Tables(0).Rows(0).Item("bitRandomAnswers") 'Random Answers Flag
                hdSinglePage.Value = objSurvey.SurveyInfo.Tables(0).Rows(0).Item("bitSinglePageSurvey") 'Single Page Survey Flag
                litSurveyName.Text = Server.HtmlDecode(objSurvey.SurveyInfo.Tables(0).Rows(0).Item("vcSurName")) 'Display the Survey name
                hdRedirectURL.Value = CorrectProtocol(objSurvey.SurveyInfo.Tables(0).Rows(0).Item("vcRedirectURL"))  'Store teh Redirect URL
                CallForQuestionnaireGeneration(objSurvey)                           'Call to generate the Questions
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is called to create the interface for Survey Questions.
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        '''-----------------------------------------------------------------------------
        Function CallForQuestionnaireGeneration(ByRef objSurvey As SurveyAdministration)
            Try
                Dim tblSurveyQuestionnaire As New Table                                 'Declare a Datatable Object
                tblSurveyQuestionnaire.Width.Percentage(100)                            'Set the Width of the Survey table
                tblSurveyQuestionnaire.CellPadding = 1                                  'Set the cell padding
                tblSurveyQuestionnaire.CellSpacing = 0                                  'Set the cell spacing
                tblSurveyQuestionnaire.BorderColor = Color.Black                        'Border Color is Black
                tblSurveyQuestionnaire.BackColor = Color.Black                          'Border Color is Black
                tblSurveyQuestionnaire.BorderWidth = Unit.Pixel(1)                      'Border Width is 1 pixel
                tblSurveyQuestionnaire.Width = Unit.Percentage(100)                     'Width of the table is 100%
                tblSurveyQuestionnaire.ID = "tblFormLeadBoxNonAOItable"                 'give a name to the table
                tblSurveyQuestionnaire.GridLines = GridLines.None                       'There are no GridLines
                tblSurveyQuestionnaire.EnableViewState = True                           'Enable view state for the table

                Dim drtableRow As DataRow                                               'Declare a Data Row Obejct
                Dim tblRowQ, tblRowA As TableRow                                        'Declare a tableRow Object
                Dim tblCell As TableCell                                                'Declare a tableCell Object
                Dim numThisSurveyQuestionRowIndex As Integer                            'Declare an Integer Row Index for tracking this Survey (Only)
                Dim sTBListScript As New System.Text.StringBuilder                      'Declare a StringBuilder to contain the table Rows ids

                sTBListScript.Append(vbCrLf & "<script language='javascript'>")
                sTBListScript.Append(vbCrLf & "var SurveyMaintableRowIds = new Array();" & vbCrLf)    'Create a array to hold the field information in javascript
                sTBListScript.Append(vbCrLf & "var SurveyMaintableRowIdsReplica = new Array();" & vbCrLf)    'Create a replica array to hold the field information in javascript
                sTBListScript.Append(vbCrLf & "var SurveyMaintableAllPossibleRowIds = new Array();" & vbCrLf)    'Create a array to hold the field information in javascript
                objSurvey.SurveyExecution.SurveyExecution().CurrentQuestionDataRow = 0   'Reset the Current Question Row Index
                objSurvey.SurveyExecution.SurveyExecution().NumberOfQuestions = objSurvey.SurveyInfo.Tables("SurveyQuestionMaster").Rows.Count 'Init Number of Questions
                While objSurvey.SurveyExecution.SurveyExecution().CurrentQuestionDataRow < objSurvey.SurveyExecution.SurveyExecution().NumberOfQuestions
                    drtableRow = objSurvey.SurveyInfo.Tables("SurveyQuestionMaster").Rows(objSurvey.SurveyExecution.SurveyExecution().CurrentQuestionDataRow)
                    objSurvey.SurveyId = drtableRow.Item("numSurID")                     'Set the Survey ID for each row

                    If objSurvey.SurveyId = hdSurId.Value Then
                        numThisSurveyQuestionNumber = drtableRow.Item("numQID")          'Get the latest Quesiton Id as a base reference
                    End If

                    tblRowQ = New TableRow                                               'Instantiate a new tableRow Object
                    tblRowQ.Height = Unit.Pixel(24)                                      'Set the height of the row
                    tblRowQ.ID = "trQ_" & drtableRow.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & drtableRow.Item("numQID") 'Give an ID to the table Row
                    tblRowQ.CssClass = "ais"                                             'Display the question in a light blue color background
                    If hdSinglePage.Value Then                                           'Since Single Page Surveys is requested
                        tblRowQ.Attributes.Add("Style", "display:'inline';")             'All quesitons are displayed
                    Else                                                                 'Dynamic Survey is requested
                        If drtableRow.Item("numSurID") = hdSurId.Value And drtableRow.Item("numQID") = 1 Then
                            tblRowQ.Attributes.Add("Style", "display:'inline';")         'Display only the first row
                        Else : tblRowQ.Attributes.Add("Style", "display:'none';")           'Hide successive rows
                        End If
                    End If

                    tblCell = New TableCell                                             'Instantiate a New tableCell
                    tblCell.CssClass = "Question"                                        'Set the Css Class
                    tblCell.Width = Unit.Percentage(87)                                 'Set the width of the cell
                    tblCell.HorizontalAlign = HorizontalAlign.Left                      'Left Align the questions
                    tblCell.VerticalAlign = VerticalAlign.Top                           'Top align the question
                    tblCell.Style.Add("PADDING-LEFT", "40px;")                          'Left Pad teh data
                    tblCell.Text = "<span class='num' id='spNum_" & drtableRow.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & drtableRow.Item("numQID") & "'>" & objSurvey.SurveyExecution.SurveyExecution().CurrentQuestionDataRow + 1 & "</span><span id='" & "spQ_" & drtableRow.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & drtableRow.Item("numQID") & "' class='LinkArrow' style='cursor: pointer;' onclick=""javascript: ToggleAnswerDisplay('" & drtableRow.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & drtableRow.Item("numQID") & "');"">&uarr;</span> " & "<span style='cursor: pointer;' onclick=""javascript: ToggleAnswerDisplay('" & drtableRow.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & drtableRow.Item("numQID") & "');"">" & Server.HtmlDecode(drtableRow.Item("vcQuestion")) & "</span>"                     'Display the Question
                    tblRowQ.Cells.Add(tblCell)                                          'Add the table Cell to the Row

                    tblCell = New TableCell                                             'Instantiate a New tableCell
                    tblCell.CssClass = "normal1"                                        'Set the Css Class
                    tblCell.Width = Unit.Percentage(13)                                 'Set the width of the cell
                    If drtableRow.Item("numSurID") = hdSurId.Value And drtableRow.Item("numQID") = 1 Then
                        tblCell.Text = "<a href=""javascript: ClearAllSelection();""><span class='LinkArrow'>&oslash;</span> Clear Selection</a>" 'Display the link
                    Else : tblCell.Text = "<a href=""javascript: GoTop();"">Top</a>&nbsp;&nbsp;<a href=""javascript: DisplayFocusToBottomOftable();"">Bottom</a>" 'Display the link
                    End If
                    tblRowQ.Cells.Add(tblCell)                                           'Add the table Cell to the Row

                    tblRowA = New TableRow                                               'Instantiate another new tableRow Object
                    tblRowA.Height = Unit.Pixel(24)                                      'Set the height of the row
                    tblRowA.ID = "trA_" & drtableRow.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & drtableRow.Item("numQID") 'Give an ID to the table Row

                    If hdSinglePage.Value Then                                           'Since Single Page Surveys is requested
                        tblRowA.Attributes.Add("Style", "display:'inline';")             'All quesitons are displayed
                    Else                                                                 'Dynamic Survey is requested
                        If drtableRow.Item("numSurID") = hdSurId.Value And drtableRow.Item("numQID") = 1 Then
                            tblRowA.Attributes.Add("Style", "display:'inline';")         'Display only the first row
                        Else : tblRowA.Attributes.Add("Style", "display:'none';")           'Hide successive rows
                        End If
                    End If
                    objSurvey.QuestionId = drtableRow.Item("numQID")                    'Associate the Questio Id
                    CallForAnswerGeneration(objSurvey, tblRowA, drtableRow.Item("numQID"), drtableRow.Item("tintAnsType"), drtableRow.Item("boolMatrixColumn")) 'Call to display teh answers

                    tblSurveyQuestionnaire.Rows.Add(tblRowQ)                            'Add the row to teh table
                    tblSurveyQuestionnaire.Rows.Add(tblRowA)                            'Add the row to teh table
                    If objSurvey.SurveyId = hdSurId.Value Then
                        sTBListScript.Append("SurveyMaintableRowIds[" & numThisSurveyQuestionRowIndex & "] = '" & drtableRow.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & drtableRow.Item("numQID") & "';" & vbCrLf) 'start creating the array elements, each holding field information
                        sTBListScript.Append("SurveyMaintableRowIdsReplica[" & numThisSurveyQuestionRowIndex & "] = '" & drtableRow.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & drtableRow.Item("numQID") & "';" & vbCrLf) 'start creating the replica array elements, each holding field information
                        numThisSurveyQuestionRowIndex += 1                              'Increment the Question Counter for this Survey
                    End If
                    sTBListScript.Append("SurveyMaintableAllPossibleRowIds[" & objSurvey.SurveyExecution.SurveyExecution().CurrentQuestionDataRow & "] = '" & drtableRow.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & drtableRow.Item("numQID") & "';" & vbCrLf) 'start creating the array elements, each holding field information
                    objSurvey.SurveyExecution.SurveyExecution().CurrentQuestionDataRow += 1 'Increment the Row index in Question table
                End While
                sTBListScript.Append("</script>")                                       'end the client side javascript block
                litTRIDList.Text &= vbCrLf & sTBListScript.ToString()                   'Store the Array client side
                plhSurveyFormControls.Controls.Add(tblSurveyQuestionnaire)              'Add the table to the placeholder control
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is called to create the interface for Survey Answers inside a question.
        ''' </summary>
        ''' <param name="objSurvey">Class Object containing the Dataset of Survey Information</param>
        ''' <param name="tblRow">The tableRow to which Answers will be added</param>
        ''' <param name="numQID">The Question Id</param>
        ''' <param name="tIntAnsType">The Answer Type</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        '''-----------------------------------------------------------------------------
        Function CallForAnswerGeneration(ByRef objSurvey As SurveyAdministration, ByRef tblRow As TableRow, ByVal numQID As Integer, ByVal tIntAnsType As Integer, ByVal boolMatrixColumn As Boolean) As TableRow
            Try
                Dim dtAnswers As DataTable                                              'Declare a Datatable
                dtAnswers = objSurvey.GetAnswersForSurveyQuestion()                     'Request for Answers related to teh Sruvey
                Dim tblCell As New TableCell                                            'Instantiate a New tableCell
                tblCell.CssClass = "Answer1"                                            'Set the Css Class
                tblCell.HorizontalAlign = HorizontalAlign.Left                          'Left Align the questions
                tblCell.ColumnSpan = 2                                                  '2 Columns
                'tblCell.BorderWidth = Unit.Pixel(8)                                     'Thick borders
                'tblCell.BorderColor = Color.White                                       'White color border
                'tblCell.BackColor = Color.White                                         'White color Background
                tblCell.Attributes.Add("width", "100%")
                'tblCell.Style.Add("PADDING-LEFT", "50px;")                              'Left Pad teh data
                Dim oControlBox As Object                                               'declare a generalized object
                Dim drAnswer As DataRow                                                 'Declare a DataRow
                Dim intAnswerRowIndex As Int16                                          'Delcare an Integer Row Index variable
                Dim sTBListScript As New System.Text.StringBuilder                      'Declare a StringBuilder to contain the table Rows ids

                Dim sTBRating As New System.Text.StringBuilder                      'Declare a StringBuilder to contain the Rating

                sTBRating.Append(vbCrLf & "<script language='javascript'>")
                sTBRating.Append(vbCrLf & "var SurveyRating" & objSurvey.SurveyId & "_" & numThisSurveyQuestionNumber & "_" & numQID & " = new Array();" & vbCrLf)   'Create a array to hold the answer field information in javascript

                sTBListScript.Append(vbCrLf & "<script language='javascript'>")
                sTBListScript.Append(vbCrLf & "var SurveyRowIdsAnsControls" & objSurvey.SurveyId & "_" & numThisSurveyQuestionNumber & "_" & numQID & " = new Array();" & vbCrLf)   'Create a array to hold the answer field information in javascript
                Select Case tIntAnsType
                    Case 1                                                              'Radio Button
                        If boolMatrixColumn = False Then
                            For Each drAnswer In dtAnswers.Rows
                                oControlBox = New RadioButton                               'instantiate a Radio Button
                                oControlBox.GroupName = "Gro" & drAnswer.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID
                                oControlBox.ID = "ans_" & drAnswer.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "_" & drAnswer.Item("numAnsID") 'set the name of the Radio Button
                                oControlBox.Text = Server.HtmlDecode(drAnswer.Item("vcAnsLabel")) 'Set the label
                                oControlBox.CssClass = "Answer"                               'Set the class attribute for the Radio Button control
                                numRating = 0
                                oControlBox.Attributes.Add("onclick", CallForAnsAttributeGeneration(objSurvey, drAnswer)) 'Attach an attribute to teh control
                                tblCell.Controls.Add(oControlBox)                           'Add the Textbox to teh cell
                                tblCell.Controls.Add(New LiteralControl("<br>"))            'Add the line carraige teh cell
                                tblRow.Cells.Add(tblCell)                                   'Add the cell to teh row
                                sTBListScript.Append("SurveyRowIdsAnsControls" & drAnswer.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "[" & intAnswerRowIndex & "] = new SurveyAnswerElements('Radio','ans_" & drAnswer.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "_" & drAnswer.Item("numAnsID") & "');" & vbCrLf) 'start creating the array elements, each holding field information

                                sTBRating.Append("SurveyRating" & drAnswer.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "[" & intAnswerRowIndex & "] = new SurveyRatingElements('Radio','ans_" & dtAnswers.Rows(0).Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "_" & drAnswer.Item("numAnsID") & "'," & numRating & ");" & vbCrLf) 'start creating the array elements, each holding field information

                                intAnswerRowIndex += 1                                      'Increment the ans row counter
                            Next
                        Else
                            Dim tblMatrix As New Table
                            tblMatrix.Width.Percentage(100)                            'Set the Width of the Survey table
                            tblMatrix.Width = Unit.Percentage(100)                     'Width of the table is 100%
                            tblMatrix.GridLines = GridLines.None                        'There are no GridLines
                            Dim tblMRow As TableRow                                        'Declare a tableRow Object
                            Dim tblMCell As TableCell                                                'Declare a tableCell Object

                            Dim dtMatrixColumn As DataTable                                              'Declare a Datatable
                            dtMatrixColumn = objSurvey.GetMatrixForSurveyQuestion()                     'Request for Answers related to teh Sruvey
                            Dim drMatrixColumn As DataRow

                            tblMRow = New TableRow
                            tblMCell = New TableCell
                            tblMCell.Controls.Add(New LiteralControl("&nbsp"))
                            tblMRow.Cells.Add(tblMCell)

                            For Each drMatrixColumn In dtMatrixColumn.Rows
                                tblMCell = New TableCell
                                tblMCell.HorizontalAlign = HorizontalAlign.Center

                                oControlBox = New Label                                  'instantiate a CheckBox
                                oControlBox.Text = Server.HtmlDecode(drMatrixColumn.Item("vcAnsLabel")) 'Set the label
                                oControlBox.CssClass = "Answer"                               'Set the class attribute for the CheckBox control
                                tblMCell.Controls.Add(oControlBox)                           'Add the Textbox to teh cell

                                tblMRow.Cells.Add(tblMCell)
                            Next

                            tblMatrix.Rows.Add(tblMRow)
                            Dim i As Integer = 0
                            For Each drAnswer In dtAnswers.Rows
                                tblMRow = New TableRow

                                oControlBox = New Label
                                oControlBox.ID = "ans_" & drAnswer.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "_" & drAnswer.Item("numAnsID") 'set the name of the textbox
                                oControlBox.Text = Server.HtmlDecode(drAnswer.Item("vcAnsLabel")) 'Set the label
                                oControlBox.CssClass = "Answer"                             'Set the class attribute for the textbox control

                                tblMCell = New TableCell
                                tblMCell.Width = Unit.Percentage(25)
                                tblMCell.Controls.Add(oControlBox)                           'Add the Textbox to teh cell
                                tblMatrix.Rows.Add(tblMRow)

                                For Each drMatrixColumn In dtMatrixColumn.Rows
                                    tblMCell = New TableCell

                                    oControlBox = New RadioButton                               'instantiate a Radio Button
                                    oControlBox.GroupName = "Gro" & drMatrixColumn.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "_" & drAnswer.Item("numAnsID")
                                    oControlBox.ID = "Matrix_" & drMatrixColumn.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "_" & drAnswer.Item("numAnsID") & "_" & drMatrixColumn.Item("numMatrixID")  'set the name of the CheckBox
                                    'oControlBox.Text = Server.HtmlDecode(drMatrixColumn.Item("vcAnsLabel")) 'Set the label
                                    oControlBox.CssClass = "Answer"                               'Set the class attribute for the Radio Button control
                                    'tblCell.HorizontalAlign = HorizontalAlign.Center
                                    numRating = 0
                                    oControlBox.Attributes.Add("onclick", CallForAnsAttributeGeneration(objSurvey, drMatrixColumn, i, dtAnswers.Rows.Count)) 'Attach an attribute to teh control

                                    tblMCell.HorizontalAlign = HorizontalAlign.Center
                                    tblCell.Controls.Add(oControlBox)                           'Add the Textbox to teh cell

                                    sTBListScript.Append("SurveyRowIdsAnsControls" & drAnswer.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "[" & intAnswerRowIndex & "] = new SurveyAnswerElements('Radio','Matrix_" & drAnswer.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "_" & drAnswer.Item("numAnsID") & "_" & drMatrixColumn.Item("numMatrixID") & "');" & vbCrLf) 'start creating the array elements, each holding field information

                                    sTBRating.Append("SurveyRating" & drAnswer.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "[" & intAnswerRowIndex & "] = new SurveyRatingElements('Radio','Matrix_" & dtAnswers.Rows(0).Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "_" & drAnswer.Item("numAnsID") & "_" & drMatrixColumn.Item("numMatrixID") & "'," & numRating & ");" & vbCrLf) 'start creating the array elements, each holding field information

                                    intAnswerRowIndex += 1                                      'Increment the ans row counter
                                    tblMRow.Cells.Add(tblMCell)
                                Next
                                i += 1
                                tblMatrix.Rows.Add(tblMRow)
                            Next

                            tblCell.Controls.Add(tblMatrix)                           'Add the Textbox to teh cell
                            tblRow.Cells.Add(tblCell)
                        End If
                    Case 2
                        For Each drAnswer In dtAnswers.Rows
                            tblCell.Controls.Add(New LiteralControl(Server.HtmlDecode(drAnswer.Item("vcAnsLabel")))) 'Add the Textbox to teh cell
                            tblCell.Controls.Add(New LiteralControl("<br>"))            'Add the line carraige teh cell

                            oControlBox = New TextBox                                   'Instantiate a TextBox
                            oControlBox.ID = "ans_" & drAnswer.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "_" & drAnswer.Item("numAnsID") 'set the name of the textbox
                            oControlBox.Width = Unit.Pixel(430)                         'set the width of the textbox
                            oControlBox.Height = Unit.Pixel(40)                         'set the height of the textbox
                            oControlBox.TextMode = TextBoxMode.MultiLine                'This is a multiline textarea
                            oControlBox.CssClass = "signup"                             'Set the class attribute for the textbox control
                            tblCell.Controls.Add(oControlBox)                           'Add the Textbox to teh cell
                            tblCell.Controls.Add(New LiteralControl("<br>"))            'Add the line carraige teh cell
                            tblRow.Cells.Add(tblCell)                                   'Add the cell to teh row
                            sTBListScript.Append("SurveyRowIdsAnsControls" & drAnswer.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "[" & intAnswerRowIndex & "] = new SurveyAnswerElements('TextBox','ans_" & drAnswer.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "_" & drAnswer.Item("numAnsID") & "');" & vbCrLf) 'start creating the array elements, each holding field information

                            sTBRating.Append("SurveyRating" & drAnswer.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "[" & intAnswerRowIndex & "] = new SurveyRatingElements('TextBox','ans_" & dtAnswers.Rows(0).Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "_" & drAnswer.Item("numAnsID") & "'," & numRating & ");" & vbCrLf) 'start creating the array elements, each holding field information

                            intAnswerRowIndex += 1                                      'Increment the ans row counter
                        Next
                        tblRow.Cells(0).Controls.Add(New LiteralControl("<span class='normal3' style='cursor: hand;' onclick=""javascript: ShowNextRow('" & objSurvey.SurveyId & "_" & numThisSurveyQuestionNumber & "_" & numQID & "','ThisRow');"">Click here to continue</span>"))
                    Case 4
                        oControlBox = New DropDownList                                  'instantiate a drop down
                        oControlBox.ID = "ans_" & dtAnswers.Rows(0).Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID 'set the name of the textbox
                        oControlBox.Width = Unit.Pixel(230)                             'set the width of the drop down
                        oControlBox.CssClass = "signup"                                 'Set the class attribute for the selectbox control
                        oControlBox.Attributes.Add("onchange", "eval(this.options[selectedIndex].value)") 'Attach an attribute to teh control
                        Dim newListItem As ListItem                                     'Declare a ListIntem

                        Dim i As Integer = 1
                        For Each drAnswer In dtAnswers.Rows
                            newListItem = New ListItem                                  'Create a new listitem
                            newListItem.Text = Server.HtmlDecode(drAnswer.Item("vcAnsLabel")) 'Set the Answer as label
                            numRating = 0
                            newListItem.Value = CallForAnsAttributeGeneration(objSurvey, drAnswer) 'Set the value for the answer

                            sTBRating.Append("SurveyRating" & drAnswer.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "[" & i & "] = new SurveyRatingElements('SelectBox','ans_" & dtAnswers.Rows(0).Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "_" & i & "'," & numRating & ");" & vbCrLf) 'start creating the array elements, each holding field information

                            oControlBox.Items.Add(newListItem)
                            i = i + 1
                        Next
                        newListItem = New ListItem                                      'Create a new listitem
                        newListItem.Text = "---Select One---"                           'Set the display text
                        newListItem.Value = "javascript: HideNextRow('" & dtAnswers.Rows(0).Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "','ThisRow');" 'Set the Value to hide this tr
                        oControlBox.Items.Insert(0, newListItem)                        'Make this for the first element

                        tblCell.Controls.Add(oControlBox)                               'Add the Textbox to teh cell
                        tblRow.Cells.Add(tblCell)                                       'Add the cell to the row
                        sTBListScript.Append("SurveyRowIdsAnsControls" & drAnswer.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "[" & intAnswerRowIndex & "] = new SurveyAnswerElements('SelectBox','ans_" & dtAnswers.Rows(0).Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "');" & vbCrLf) 'start creating the array elements, each holding field information
                        intAnswerRowIndex += 1                                      'Increment the ans row counter
                    Case Else
                        If boolMatrixColumn = False Then
                            For Each drAnswer In dtAnswers.Rows
                                oControlBox = New CheckBox                                  'instantiate a CheckBox
                                oControlBox.ID = "ans_" & drAnswer.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "_" & drAnswer.Item("numAnsID") 'set the name of the CheckBox
                                oControlBox.Text = Server.HtmlDecode(drAnswer.Item("vcAnsLabel")) 'Set the label
                                oControlBox.CssClass = "Answer"                               'Set the class attribute for the CheckBox control
                                numRating = 0
                                oControlBox.Attributes.Add("onclick", CallForAnsAttributeGeneration(objSurvey, drAnswer)) 'Attach an attribute to teh control
                                tblCell.Controls.Add(oControlBox)                           'Add the Textbox to teh cell
                                tblCell.Controls.Add(New LiteralControl("<br>"))            'Add the line carraige teh cell
                                tblRow.Cells.Add(tblCell)                                   'Add the cell to teh row
                                sTBListScript.Append("SurveyRowIdsAnsControls" & drAnswer.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "[" & intAnswerRowIndex & "] = new SurveyAnswerElements('CheckBox','ans_" & drAnswer.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "_" & drAnswer.Item("numAnsID") & "');" & vbCrLf) 'start creating the array elements, each holding field information

                                sTBRating.Append("SurveyRating" & drAnswer.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "[" & intAnswerRowIndex & "] = new SurveyRatingElements('CheckBox','ans_" & dtAnswers.Rows(0).Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "_" & drAnswer.Item("numAnsID") & "'," & numRating & ");" & vbCrLf) 'start creating the array elements, each holding field information

                                intAnswerRowIndex += 1                                      'Increment the ans row counter
                            Next
                        Else

                            Dim tblMatrix As New Table
                            tblMatrix.Width.Percentage(100)                            'Set the Width of the Survey table
                            tblMatrix.Width = Unit.Percentage(100)                     'Width of the table is 100%
                            tblMatrix.GridLines = GridLines.None                        'There are no GridLines
                            Dim tblMRow As TableRow                                        'Declare a tableRow Object
                            Dim tblMCell As TableCell                                                'Declare a tableCell Object

                            Dim dtMatrixColumn As DataTable                                              'Declare a Datatable
                            dtMatrixColumn = objSurvey.GetMatrixForSurveyQuestion()                     'Request for Answers related to teh Sruvey
                            Dim drMatrixColumn As DataRow

                            tblMRow = New TableRow
                            tblMCell = New TableCell
                            tblMCell.Controls.Add(New LiteralControl("&nbsp"))
                            tblMRow.Cells.Add(tblMCell)

                            For Each drMatrixColumn In dtMatrixColumn.Rows
                                tblMCell = New TableCell
                                tblMCell.HorizontalAlign = HorizontalAlign.Center

                                oControlBox = New Label                                  'instantiate a CheckBox
                                oControlBox.Text = Server.HtmlDecode(drMatrixColumn.Item("vcAnsLabel")) 'Set the label
                                oControlBox.CssClass = "Answer"                               'Set the class attribute for the CheckBox control
                                tblMCell.Controls.Add(oControlBox)                           'Add the Textbox to teh cell

                                tblMRow.Cells.Add(tblMCell)                                   'Add the cell to teh row
                            Next

                            tblMatrix.Rows.Add(tblMRow)
                            Dim i As Integer = 0

                            For Each drAnswer In dtAnswers.Rows
                                tblMRow = New TableRow
                                oControlBox = New Label
                                oControlBox.ID = "ans_" & drAnswer.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "_" & drAnswer.Item("numAnsID") 'set the name of the textbox
                                oControlBox.Text = Server.HtmlDecode(drAnswer.Item("vcAnsLabel")) 'Set the label
                                oControlBox.CssClass = "Answer"                             'Set the class attribute for the textbox control
                                tblMCell = New TableCell

                                tblMCell.Width = Unit.Percentage(25)
                                tblMCell.Controls.Add(oControlBox)                           'Add the Textbox to teh cell
                                tblMRow.Cells.Add(tblMCell)


                                For Each drMatrixColumn In dtMatrixColumn.Rows
                                    tblMCell = New TableCell

                                    oControlBox = New CheckBox                                  'instantiate a CheckBox
                                    oControlBox.ID = "Matrix_" & drMatrixColumn.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "_" & drAnswer.Item("numAnsID") & "_" & drMatrixColumn.Item("numMatrixID")  'set the name of the CheckBox
                                    oControlBox.CssClass = "Answer"                               'Set the class attribute for the CheckBox control

                                    numRating = 0
                                    oControlBox.Attributes.Add("onclick", CallForAnsAttributeGeneration(objSurvey, drMatrixColumn, i, dtAnswers.Rows.Count)) 'Attach an attribute to teh control

                                    tblMCell.HorizontalAlign = HorizontalAlign.Center
                                    tblMCell.Controls.Add(oControlBox)                           'Add the Textbox to teh cell
                                    'tblMCell.Controls.Add(New LiteralControl("&nbsp;&nbsp;"))            'Add the line carraige teh cell

                                    sTBListScript.Append("SurveyRowIdsAnsControls" & drAnswer.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "[" & intAnswerRowIndex & "] = new SurveyAnswerElements('CheckBox','Matrix_" & drAnswer.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "_" & drAnswer.Item("numAnsID") & "_" & drMatrixColumn.Item("numMatrixID") & "');" & vbCrLf) 'start creating the array elements, each holding field information

                                    sTBRating.Append("SurveyRating" & drAnswer.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "[" & intAnswerRowIndex & "] = new SurveyRatingElements('CheckBox','Matrix_" & dtAnswers.Rows(0).Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & numQID & "_" & drAnswer.Item("numAnsID") & "_" & drMatrixColumn.Item("numMatrixID") & "'," & numRating & ");" & vbCrLf) 'start creating the array elements, each holding field information

                                    intAnswerRowIndex += 1                                      'Increment the ans row counter

                                    tblMRow.Cells.Add(tblMCell)
                                Next
                                i += 1
                                tblMatrix.Rows.Add(tblMRow)
                            Next

                            tblCell.Controls.Add(tblMatrix)                           'Add the Textbox to teh cell
                            tblRow.Cells.Add(tblCell)
                        End If
                End Select
                sTBListScript.Append("</script>")                                       'end the client side javascript block
                sTBRating.Append("</script>")                                       'end the client side javascript block

                litTRIDList.Text &= vbCrLf & sTBListScript.ToString()                   'Store the Array client side

                litTRIDList.Text &= vbCrLf & sTBRating.ToString()                   'Store the Array client side

                tblRow.Cells.Add(tblCell)                                               'Add the table Cell to the Row
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is called to attach an attribute to the control.
        ''' </summary>
        ''' <param name="objSurvey">Class Object containing the Dataset of Survey Information</param>
        ''' <param name="tblDataRow">The DataRow containing the Answer </param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        '''-----------------------------------------------------------------------------
        Function CallForAnsAttributeGeneration(ByRef objSurvey As SurveyAdministration, ByVal tblAnsDataRow As DataRow, Optional ByVal RowIndex As Integer = 0, Optional ByVal total As Integer = 0) As String
            Try
                Dim stBScript As New System.Text.StringBuilder                                  'Declare a StringBuilder
                stBScript.Append("javascript: ")                                                'put the initial string
                Dim numSurID As Integer                                                         'A local variable for Survey ID
                numSurID = CInt(tblAnsDataRow.Item("numSurID"))                                 'Default Value for Survey Id
                Dim numQID As Integer                                                           'A local variable for Question ID
                numQID = CInt(tblAnsDataRow.Item("numQID"))                                     'Default value for Question id
                Dim sOpenURL As String = ""                                                     'Flag to check if Open URL is encountered or not
                If tblAnsDataRow.Item("boolSurveyRuleAttached") Then                            'If a Rule is attached
                    Dim dtWorkFlowRules As DataTable                                            'Declare a Datatable of Rules

                    If tblAnsDataRow.Table.Columns.Contains("numMatrixID") Then
                        objSurvey.MatrixID = tblAnsDataRow.Item("numMatrixID")                         'Set the Answer ID
                        objSurvey.AnswerId = 0
                    Else
                        objSurvey.AnswerId = tblAnsDataRow.Item("numAnsID")                         'Set the Answer ID
                        objSurvey.MatrixID = 0
                    End If

                    dtWorkFlowRules = objSurvey.GetSurveyWorkflowRulesDetailsForSurveyAnswer    'Get the Rules for the Survey Answer

                    Dim drWorkFlowRule As DataRow                                               'Declare a DataRow
                    For Each drWorkFlowRule In dtWorkFlowRules.Rows                             'Loop through the rules
                        Select Case CInt(drWorkFlowRule.Item("numRuleID"))
                            Case 1
                                stBScript.Append("IncludeQuestions('" & drWorkFlowRule.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & drWorkFlowRule.Item("numQID") & "','" & numThisSurveyQuestionNumber & "','" & drWorkFlowRule.Item("vcQuestionList") & "'," & drWorkFlowRule.Item("numLinkedSurID") & ");") 'Attach javascript to fork to another set of question
                                numSurID = drWorkFlowRule.Item("numSurID")                      'Set the Survey ID
                                numQID = drWorkFlowRule.Item("numQID")                          'Set the Question ID
                                Dim dsForkedSurveyQuestionAnswers As DataSet = objSurvey.GetForkedSurveyQuestionAnswer(drWorkFlowRule.Item("numLinkedSurID"), drWorkFlowRule.Item("vcQuestionList")) 'Fork to get the other survey's questions and answers
                                Dim dtForkedSurveyQuestions As DataTable                    'Declare Question Datatables
                                Dim dtForkedSurveyAnswers As DataTable                      'Declare Answer Datatables
                                dtForkedSurveyQuestions = dsForkedSurveyQuestionAnswers.Tables(0)  'Get the Questions table
                                dtForkedSurveyAnswers = dsForkedSurveyQuestionAnswers.Tables(1) 'Get the Answers table
                                Dim drForkedSurveyQuestions, drForkedSurveyAnswers As DataRow   'Declare a DataRow object
                                Dim drForkedSurveyQuestionCopy As DataRow
                                Dim intNewQuestionIndex As Integer                              'Declare an integer
                                For intNewQuestionIndex = dtForkedSurveyQuestions.Rows.Count - 1 To 0 Step -1
                                    drForkedSurveyQuestions = dtForkedSurveyQuestions.Rows(intNewQuestionIndex)
                                    drForkedSurveyQuestionCopy = objSurvey.SurveyInfo.Tables("SurveyQuestionMaster").NewRow 'Create a New Row object
                                    drForkedSurveyQuestionCopy.ItemArray = drForkedSurveyQuestions.ItemArray
                                    objSurvey.SurveyInfo.Tables("SurveyQuestionMaster").Rows.InsertAt(drForkedSurveyQuestionCopy, objSurvey.SurveyExecution.SurveyExecution().CurrentQuestionDataRow + 1) 'Insert teh Question Row at 1st postion as the 1st question does nto change to another survey
                                Next

                                objSurvey.SurveyExecution.SurveyExecution().NumberOfQuestions += dtForkedSurveyQuestions.Rows.Count 'Increment teh nos of Questions
                                For Each drForkedSurveyAnswers In dtForkedSurveyAnswers.Rows
                                    objSurvey.SurveyInfo.Tables("SurveyAnsMaster").ImportRow(drForkedSurveyAnswers) 'Import the answers rows
                                Next
                            Case 2
                                stBScript.Append("SkipQuestion('" & drWorkFlowRule.Item("numSurID") & "_" & numThisSurveyQuestionNumber & "_" & drWorkFlowRule.Item("numQID") & "','" & drWorkFlowRule.Item("vcQuestionList") & "'," & drWorkFlowRule.Item("numSurID") & ");") 'Attach Javascript to skip question
                                numSurID = drWorkFlowRule.Item("numSurID")                      'Set the Survey ID
                                numQID = drWorkFlowRule.Item("numQID")                          'Set the Question ID
                                'Case 3
                                '    stBScript.Append("CreateDBEntry(" & drWorkFlowRule.Item("tIntCRMType") & "," & drWorkFlowRule.Item("numGrpID") & "," & drWorkFlowRule.Item("numCompanyType") & "," & drWorkFlowRule.Item("numRecOwner") & ");") 'Attach Javasript to create db entry data
                                '    numSurID = drWorkFlowRule.Item("numSurID")                      'Set the Survey ID
                                '    numQID = drWorkFlowRule.Item("numQID")                          'Set the Question ID
                            Case 4
                                numSurID = drWorkFlowRule.Item("numSurID")                      'Set the Survey ID
                                numQID = drWorkFlowRule.Item("numQID")                          'Set the Question ID
                            Case 5

                                numSurID = drWorkFlowRule.Item("numSurID")                      'Set the Survey ID
                                numQID = drWorkFlowRule.Item("numQID")                          'Set the Question ID

                            Case 6
                                numSurID = drWorkFlowRule.Item("numSurID")                      'Set the Survey ID
                                numQID = drWorkFlowRule.Item("numQID")                          'Set the Question ID
                                sOpenURL = drWorkFlowRule.Item("vcPopUpURL")                    'Open URL is encountered
                            Case 7
                                'stBScript.Append("AddRating(" & drWorkFlowRule.Item("numResponseRating") & ",this);") 'Attach Javascript to add Response Rating for the Question that is selected

                                numRating = drWorkFlowRule.Item("numResponseRating")
                                numSurID = drWorkFlowRule.Item("numSurID")                      'Set the Survey ID
                                numQID = drWorkFlowRule.Item("numQID")                          'Set the Question ID
                        End Select

                    Next
                End If

                If total = 0 Then
                    stBScript.Append("ShowNextRow('" & numSurID & "_" & numThisSurveyQuestionNumber & "_" & numQID & "','ThisRow');")
                ElseIf total - 1 = RowIndex Then
                    stBScript.Append("ShowNextRow('" & numSurID & "_" & numThisSurveyQuestionNumber & "_" & numQID & "','ThisRow');")
                End If
                If sOpenURL <> "" Then stBScript.Append("OpenURL('" & CorrectProtocol(sOpenURL) & "');") 'Attach Javascript to Open the pop url. This is used last or else the popup receeds into the background
                Return stBScript.ToString()
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This event is fired when the Survey is submitted.
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/23/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub btnConcludeSurvey_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConcludeSurvey.Click
            Try
                Dim sSurveyExectionControlsInformation As String                        'String to store the controls for the survey
                sSurveyExectionControlsInformation = hdTrackableSurveyControlEntries.Value 'Pick up the list of controls associated with the answers

                Dim dsSurveyExecutionInfo As New DataSet                                'Declare and  create a new DataSet
                Dim dtSurveyResponseInfo As New DataTable                               'Declare and create a new datatable
                dtSurveyResponseInfo.TableName = "SurveyResponse"                       'Set the name for the table
                dtSurveyResponseInfo.Columns.Add(New DataColumn("numRespondantID", System.Type.GetType("System.String"))) 'Add a column to store the Respondent ID
                dtSurveyResponseInfo.Columns.Add(New DataColumn("numParentSurID", System.Type.GetType("System.String"))) 'Add a column to store the Parent Survey ID
                dtSurveyResponseInfo.Columns.Add(New DataColumn("numSurID", System.Type.GetType("System.String"))) 'Add a column to store the Survey ID
                dtSurveyResponseInfo.Columns.Add(New DataColumn("numQuestionID", System.Type.GetType("System.String"))) 'Add a column to store the Question ID
                dtSurveyResponseInfo.Columns.Add(New DataColumn("numAnsID", System.Type.GetType("System.String"))) 'Add a column to store the ans value selected
                dtSurveyResponseInfo.Columns.Add(New DataColumn("vcAnsText", System.Type.GetType("System.String"))) 'Add a column to store the text data entered
                dtSurveyResponseInfo.Columns.Add(New DataColumn("numMatrixID", System.Type.GetType("System.String"))) 'Add a column to store the Matrix value selected

                Dim drSurveyResponse As DataRow                                         'Declare a DataRow
                Dim sControlsArray(), sControlQAID() As String                          'Declare an array to store the controls and their Question and Answer IDs

                sControlsArray = sSurveyExectionControlsInformation.Split(",")          'Split the string containing the Controls

                'Dim iLastQuestion As Integer = 0, iLastAnswer As Integer = 0
                Dim iNumArrayItem As Integer                                            'Declare an integer counter
                For iNumArrayItem = 0 To sControlsArray.Length - 1
                    sControlQAID = CStr(sControlsArray.GetValue(iNumArrayItem)).Split("_") 'Split the name of the control

                    'If sControlQAID.Length = 6 Then                                     'CheckBox /TextBox/ RadioButtons

                    '    If iLastQuestion = 0 AndAlso iLastAnswer = 0 Then
                    '        drSurveyResponse = dtSurveyResponseInfo.NewRow()                    'Create a New row object
                    '        drSurveyResponse.Item("numRespondantID") = hdRespondentID.Value     'Set the Respondent ID
                    '        drSurveyResponse.Item("numParentSurID") = sControlQAID.GetValue(1)   'Set the Parent Survey ID
                    '        drSurveyResponse.Item("numSurID") = hdSurId.Value                   'Set theSurvey ID
                    '    End If

                    '    drSurveyResponse.Item("numQuestionID") = sControlQAID.GetValue(3)  'Set the Question ID
                    '    drSurveyResponse.Item("numAnsID") = sControlQAID.GetValue(4)    'Set the Answer ID

                    '    If iLastQuestion = sControlQAID.GetValue(3) AndAlso iLastAnswer = sControlQAID.GetValue(4) Then
                    '        drSurveyResponse.Item("numMatrixID") += "," & sControlQAID.GetValue(5)    'Set the Matrix ID
                    '    Else
                    '        dtSurveyResponseInfo.Rows.Add(drSurveyResponse)                     'Add the row to the table
                    '    End If
                    '    iLastQuestion = sControlQAID.GetValue(3)
                    '    iLastAnswer = sControlQAID.GetValue(4)
                    'Else
                    drSurveyResponse = dtSurveyResponseInfo.NewRow()                    'Create a New row object
                    drSurveyResponse.Item("numRespondantID") = hdRespondentID.Value     'Set the Respondent ID
                    drSurveyResponse.Item("numParentSurID") = sControlQAID.GetValue(1)   'Set the Parent Survey ID
                    drSurveyResponse.Item("numSurID") = hdSurId.Value                   'Set theSurvey ID

                    If sControlQAID.Length = 5 Then                                     'CheckBox /TextBox/ RadioButtons
                        drSurveyResponse.Item("numQuestionID") = sControlQAID.GetValue(3) 'Set the Question ID
                        drSurveyResponse.Item("numAnsID") = sControlQAID.GetValue(4)    'Set the Answer ID
                        If Trim(Request.Form(CStr(sControlsArray.GetValue(iNumArrayItem)))) <> "on" Then
                            drSurveyResponse.Item("vcAnsText") = Request.Form(CStr(sControlsArray.GetValue(iNumArrayItem))) 'Set the Answers
                        End If
                        drSurveyResponse.Item("numMatrixID") = "0"
                    ElseIf sControlQAID.Length = 6 Then
                        drSurveyResponse.Item("numQuestionID") = sControlQAID.GetValue(3) 'Set the Question ID
                        drSurveyResponse.Item("numAnsID") = sControlQAID.GetValue(4)    'Set the Answer ID
                        drSurveyResponse.Item("numMatrixID") = sControlQAID.GetValue(5)
                    Else                                                                'Any control other than a select box
                        drSurveyResponse.Item("numQuestionID") = sControlQAID.GetValue(3) 'Set the Question ID
                        drSurveyResponse.Item("numAnsID") = Request.Form(CStr(sControlsArray.GetValue(iNumArrayItem))) 'Set the Answer ID
                        drSurveyResponse.Item("numMatrixID") = "0"
                    End If

                    dtSurveyResponseInfo.Rows.Add(drSurveyResponse)                     'Add the row to the table
                    'End If
                Next
                dsSurveyExecutionInfo.Tables.Add(dtSurveyResponseInfo)                  'Add the table to the dataset

                Dim objSurvey As New SurveyAdministration                               'Create an object of Survey Administration Class
                objSurvey.SurveyId = hdSurId.Value                                      'Set teh Survey ID
                objSurvey.SurveyExecution.SurveyExecution().RespondentID = hdRespondentID.Value 'Set the Respondent ID
                objSurvey.SurveyExecution.SurveyExecution().SurveyRating = hdSurveyRating.Value 'Set the Survey Rating
                Dim numSurveyRespondentContactId As Integer                             'The contact id Is stored after creation of a lead
                Dim numSSurveyRespondentCompanyId As Integer
                Dim numSurveyRespondentDivisionId As Integer

                'If Not Session("DivID") Is Nothing Then
                '    numSurveyRespondentDivisionId = Session("DivID")
                'End If
                'If Not Session("UserContactID") Is Nothing Then
                '    numSurveyRespondentContactId = Session("UserContactID")
                'End If
                'If Not Session("CompID") Is Nothing Then
                '    numSSurveyRespondentCompanyId = Session("CompID")
                'End If

                If hdSurId.Value > 0 Then                                               'Form is opened for a particular survey only
                    Dim dtSurveyMasterInfo As DataTable                                 'Declare a Datatable
                    objSurvey.SurveyId = hdSurId.Value                                      'Set the Survey ID
                    objSurvey.DomainId = numDomainId                                        'Set the Domain Id
                    dtSurveyMasterInfo = objSurvey.getSurveyMasterData()                'Get the Survey Master information in a datatable

                    If (dtSurveyMasterInfo.Rows(0).Item("bitRelationshipProfile") = True) Then
                        hdRelationShip.Value = IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("numRelationShipId")), 0, dtSurveyMasterInfo.Rows(0).Item("numRelationShipId"))
                        hdCRMType.Value = IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("tIntCRMType")), 0, dtSurveyMasterInfo.Rows(0).Item("tIntCRMType"))
                        hdGroupId.Value = IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("numGrpId")), 1, dtSurveyMasterInfo.Rows(0).Item("numGrpId"))
                        hdRecOwner.Value = IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("numRecOwner")), 0, dtSurveyMasterInfo.Rows(0).Item("numRecOwner"))
                        hdProfile.Value = IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("numProfileId")), 0, dtSurveyMasterInfo.Rows(0).Item("numProfileId"))

                        hdCreateDBEntry.Value = 1
                    ElseIf (dtSurveyMasterInfo.Rows(0).Item("bitCreateRecord") = True) Then
                        dtSurveyMasterInfo = New DataTable
                        dtSurveyMasterInfo = objSurvey.getSurveyRating()

                        If dtSurveyMasterInfo.Rows.Count > 0 Then
                            hdRelationShip.Value = IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("numRelationShipId")), 0, dtSurveyMasterInfo.Rows(0).Item("numRelationShipId"))
                            hdCRMType.Value = IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("tIntCRMType")), 0, dtSurveyMasterInfo.Rows(0).Item("tIntCRMType"))
                            hdGroupId.Value = IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("numGrpId")), 1, dtSurveyMasterInfo.Rows(0).Item("numGrpId"))
                            hdRecOwner.Value = IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("numRecOwner")), 0, dtSurveyMasterInfo.Rows(0).Item("numRecOwner"))
                            hdProfile.Value = IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("numProfileId")), 0, dtSurveyMasterInfo.Rows(0).Item("numProfileId"))
                            hdFollowUpStatus.Value = IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("numFollowUpStatus")), 0, dtSurveyMasterInfo.Rows(0).Item("numFollowUpStatus"))

                            hdCreateDBEntry.Value = 1
                        End If
                    End If
                End If

                Dim objLeads As New CLeads
                If hdCreateDBEntry.Value = 1 And hdRespondentID.Value <> 0 Then         'Request to make entries for Lead/ Prospect/ Account into Database
                    'If hdRespondentID.Value <> 0 Then         'Request to make entries for Lead/ Prospect/ Account into Database
                    objSurvey.SurveyExecution.SurveyExecution().RelationShipID = hdRelationShip.Value 'Pick up the Relatiioship ID
                    objSurvey.SurveyExecution.SurveyExecution().GroupID = hdGroupId.Value 'Pick up the Grup ID
                    objSurvey.SurveyExecution.SurveyExecution().CRMType = hdCRMType.Value 'Pick up the CRM Type
                    objSurvey.SurveyExecution.SurveyExecution().RecOwnerID = hdRecOwner.Value 'Pick up the Record OWner

                    'Dim objLeadBoxData As New FormGenericLeadBox                        'Create an instance of FormGenericLeadBox Class
                    'objLeadBoxData.objLeads.GroupID = hdGroupId.Value                   'Set the GroupID
                    'If objLeadBoxData.objLeads.ContactType = 0 Then
                    '    objLeadBoxData.objLeads.CompanyType = hdRelationShip.Value      'Set teh Relationship
                    'End If
                    'objLeadBoxData.objLeads.ContactType = 70                            'Set the Contact type to Primary contact
                    'objLeadBoxData.objLeads.CRMType = hdCRMType.Value                   'Set the CRM Type
                    'objLeadBoxData.objLeads.DomainID = numDomainId                      'Set the Domain ID
                    'CreateLeadAccordingToAutoRules(objLeadBoxData, objSurvey)           'Go for Creation of Leads/ Accounts/ Prospects

                    If Not Session("dtSurveyRegDTL") Is Nothing Then
                        '' Added By Anoop 
                        Dim ds As New DataSet
                        Dim dRow As DataRow
                        'Dim objLeads As New CLeads
                        Dim dtGenericFormConfig As DataTable
                        dtGenericFormConfig = Session("dtSurveyRegDTL")
                        objLeads.GroupID = hdGroupId.Value
                        objLeads.ContactType = 70
                        objLeads.CRMType = hdCRMType.Value
                        objLeads.CompanyType = hdRelationShip.Value
                        objLeads.DivisionName = "-"

                        Dim objBusinessClass As New BusinessClass
                        Dim importWiz As New ImportWizard

                        Dim vcAssociatedControlType As String
                        Dim vcFieldType As String
                        For Each dr As DataRow In dtGenericFormConfig.Rows
                            vcFieldType = dr("vcFieldType")
                            If vcFieldType = "R" Then
                                vcAssociatedControlType = dr("vcAssociatedControlType")
                                Select Case vcAssociatedControlType
                                    Case "TextBox"
                                        If dr("vcDbColumnText") <> "" Then AssignValuesEditBox(objLeads, dr("vcDbColumnText"), dr("vcDbColumnName"))
                                    Case "SelectBox"
                                        If dr("vcDbColumnText") <> "" Then
                                            AssignValuesSelectBox(objLeads, dr("vcDbColumnText"), dr("vcDbColumnName"))
                                        Else : dr("vcDbColumnText") = 0
                                        End If
                                    Case "TextArea"
                                        If dr("vcDbColumnText") <> "" Then AssignValuesTextBox(objLeads, dr("vcDbColumnText"), dr("vcDbColumnName"))
                                End Select
                            End If
                        Next
                        objLeads.DomainID = numDomainId

                        objBusinessClass.IPNo = objBusinessClass.GetIPNumberFromIPAddress(Request.UserHostAddress)
                        Dim lngCountryID As Long = importWiz.GetStateAndCountry(0, objBusinessClass.GetCountryFromIPaddress(ConfigurationManager.AppSettings("ConnectionString")), numDomainId)
                        If lngCountryID > 0 Then
                            objLeads.Country = lngCountryID
                        End If

                        If hdRecOwner.Value = "" Then
                            hdRecOwner.Value = 0
                        End If
                        If hdRecOwner.Value = 0 Then
                            Dim objAutoRoutRles As New AutoRoutingRules
                            objAutoRoutRles.DomainID = numDomainId
                            dtGenericFormConfig.TableName = "Table"
                            ds.Tables.Add(dtGenericFormConfig.Copy)
                            objAutoRoutRles.strValues = ds.GetXml
                            ds.Tables.Remove(ds.Tables(0))
                            hdRecOwner.Value = objAutoRoutRles.GetRecordOwner
                        End If

                        objLeads.AssignedTo = hdRecOwner.Value
                        objLeads.UserCntID = hdRecOwner.Value
                        objLeads.Profile = hdProfile.Value
                        objLeads.CompanyID = objLeads.CreateRecordCompanyInfo()
                        numSSurveyRespondentCompanyId = objLeads.CompanyID

                        If hdFollowUpStatus.Value.Length > 0 Then
                            objLeads.FollowUpStatus = hdFollowUpStatus.Value
                        End If
                        objLeads.DivisionID = objLeads.CreateRecordDivisionsInfo
                        numSurveyRespondentDivisionId = objLeads.DivisionID
                        numSurveyRespondentContactId = objLeads.CreateRecordAddContactInfo()
                        Session("DivID") = objLeads.DivisionID 'Addded by chintan reason: will be used in tracking leads in marketing section
                        'add cookie to browser of Division id which can be used to track future visit of same user
                        CCommon.AddDivisionCookieToBrowser(objLeads.DivisionID)

                        Dim dsViews As New DataView(dtGenericFormConfig)
                        dsViews.RowFilter = "vcFieldType='C'"
                        Dim i As Integer

                        Dim dtCusTable As New DataTable
                        dtCusTable.Columns.Add("FldDTLID")
                        dtCusTable.Columns.Add("fld_id")
                        dtCusTable.Columns.Add("Value")
                        Dim strdetails As String

                        If dsViews.Count > 0 Then
                            For i = 0 To dsViews.Count - 1
                                dRow = dtCusTable.NewRow
                                dRow("FldDTLID") = 0
                                dRow("fld_id") = Replace(dsViews(i).Item("numFormFieldId"), "C", "")
                                dRow("Value") = dsViews(i).Item("vcDbColumnText")
                                dtCusTable.Rows.Add(dRow)
                            Next

                            dtCusTable.TableName = "Table"
                            ds.Tables.Add(dtCusTable.Copy)
                            strdetails = ds.GetXml
                            ds.Tables.Remove(ds.Tables(0))

                            Dim ObjCusfld As New CustomFields
                            ObjCusfld.strDetails = strdetails
                            ObjCusfld.RecordId = objLeads.ContactID
                            ObjCusfld.locId = 4
                            ObjCusfld.SaveCustomFldsByRecId()
                        End If
                        dsViews.RowFilter = "vcFieldType='D'"
                        dtCusTable.Rows.Clear()
                        If dsViews.Count > 0 Then
                            For i = 0 To dsViews.Count - 1
                                dRow = dtCusTable.NewRow
                                dRow("FldDTLID") = 0
                                dRow("fld_id") = Replace(dsViews(i).Item("numFormFieldId"), "C", "")
                                dRow("Value") = dsViews(i).Item("vcDbColumnText")
                                dtCusTable.Rows.Add(dRow)
                            Next

                            dtCusTable.TableName = "Table"
                            ds.Tables.Add(dtCusTable.Copy)
                            strdetails = ds.GetXml
                            ds.Tables.Remove(ds.Tables(0))

                            Dim ObjCusfld As New CustomFields
                            ObjCusfld.strDetails = strdetails
                            ObjCusfld.RecordId = objLeads.DivisionID
                            ObjCusfld.locId = 1
                            ObjCusfld.SaveCustomFldsByRecId()
                        End If
                    End If

                    Dim bitEmailTemplateContact, bitEmailTemplateRecordOwner As Boolean
                    Dim numEmailTemplate1Id, numEmailTemplate2Id As Int32

                    If hdSurId.Value > 0 Then                                               'Form is opened for a particular survey only
                        Dim dtSurveyMasterInfo As DataTable                                 'Declare a Datatable
                        objSurvey.SurveyId = hdSurId.Value                                      'Set the Survey ID
                        objSurvey.DomainId = numDomainId                                        'Set the Domain Id
                        dtSurveyMasterInfo = objSurvey.getSurveyMasterData()                'Get the Survey Master information in a datatable
                        bitEmailTemplateContact = dtSurveyMasterInfo.Rows(0).Item("bitEmailTemplateContact")
                        bitEmailTemplateRecordOwner = dtSurveyMasterInfo.Rows(0).Item("bitEmailTemplateRecordOwner")

                        numEmailTemplate1Id = dtSurveyMasterInfo.Rows(0).Item("numEmailTemplate1Id")
                        numEmailTemplate2Id = dtSurveyMasterInfo.Rows(0).Item("numEmailTemplate2Id")
                    End If

                    'Added By Sachin Sadhu||Date:25thAug2014
                    'Purpose :To Added Organization data in work Flow queue based on created Rules
                    '          Using Change tracking
                    Dim objWfA As New Workflow()
                    objWfA.DomainID = numDomainId
                    objWfA.UserCntID = Session("UserContactID")
                    objWfA.RecordID = CCommon.ToLong(objLeads.DivisionID)
                    objWfA.SaveWFOrganizationQueue()
                    'ss//end of code

                    'Purpose :To Added Contact data in work Flow queue based on created Rules
                    '         Using Change tracking
                    Dim objWF As New Workflow()
                    objWF.DomainID = numDomainId
                    objWF.UserCntID = Session("UserContactID")
                    objWF.RecordID = objLeads.ContactID
                    objWF.SaveWFContactQueue()
                    ' ss//end of code

                    Session("SMTPServerIntegration") = True

                    If bitEmailTemplateContact Then
                        If objLeads.Email IsNot Nothing Then
                            If objLeads.Email.Length > 0 Then
                                'Commented by :Sachin Sadhu||Date:25thSept2014
                                'Purpose : To Replace with WFA(part of Email Alerts)
                                'CAlerts.SendWebLeadAlerts(numEmailTemplate1Id, numSurveyRespondentDivisionId, numSurveyRespondentContactId, numSurveyRespondentContactId, hdRecOwner.Value, numDomainId)
                                'end of code
                            End If
                        End If
                    End If

                    If bitEmailTemplateRecordOwner Then
                        'Commented by :Sachin Sadhu||Date:25thSept2014
                        'Purpose : To Replace with WFA(part of Email Alerts)
                        ' CAlerts.SendWebLeadAlerts(numEmailTemplate2Id, numSurveyRespondentDivisionId, numSurveyRespondentContactId, hdRecOwner.Value, hdRecOwner.Value, numDomainId)
                        'end of code
                    End If

                    Session("dtSurveyRegDTL") = Nothing
                End If

                ' ToDo : Add details for rule 4 and 5 here
                Dim dsItemTemp As New DataSet
                If (numSurveyRespondentContactId > 0 And numSSurveyRespondentCompanyId > 0) Then
                    'This means that the contact has been created or it exists
                    Dim objContact As CContacts = New CContacts()
                    objLeads.ContactID = numSurveyRespondentContactId
                    Dim objOpportunity

                    If (dtSurveyResponseInfo.Rows.Count > 0) Then

                        objSurvey.SurveyId = hdSurId.Value                                  'Set the Survey ID
                        objSurvey.SurveyInfo = objSurvey.getSurveyInformation4Execution
                        objSurvey.SurveyInfo.Tables(0).TableName = "SurveyMaster"           'Set the name of the table of Survey Master Infor
                        objSurvey.SurveyInfo.Tables(1).TableName = "SurveyQuestionMaster"   'Set the name of the table of Questions
                        objSurvey.SurveyInfo.Tables(2).TableName = "SurveyAnsMaster"        'Set the name of the table of Answers
                        objSurvey.SurveyInfo.Tables(3).TableName = "SurveyWorkflowRules"    'Set the name of the table of Work Flow Rules
                        objSurvey.SurveyInfo.Tables(4).TableName = "SurveyMatrixMaster"    'Set the name of the table of Work Flow Rules

                        Dim dtAnswerRules As DataTable
                        Dim drAnswerRulesRow As DataRow
                        Dim dtSurveyWorkflowRules As DataTable
                        dtSurveyWorkflowRules = objSurvey.SurveyInfo.Tables(3)

                        For Each drSurveyResponse In dtSurveyResponseInfo.Rows
                            objSurvey.QuestionId = drSurveyResponse("numQuestionId")

                            If drSurveyResponse("numMatrixID") > 0 Then
                                objSurvey.MatrixID = drSurveyResponse("numMatrixID")
                                objSurvey.AnswerId = 0
                            Else
                                objSurvey.MatrixID = 0
                                objSurvey.AnswerId = drSurveyResponse("numAnsId")
                            End If

                            dtAnswerRules = objSurvey.GetSurveyWorkflowRulesDetailsForSurveyAnswer()

                            For Each drAnswerRulesRow In dtAnswerRules.Rows
                                Dim dtSelectField As DataTable = objSurvey.GetSelectFields(0)
                                Dim dc(0) As DataColumn
                                dc(0) = dtSelectField.Columns("SurveySelectFieldId")
                                dtSelectField.PrimaryKey = dc
                                If (drAnswerRulesRow.Item("numRuleId") = 4) And objSurvey.QuestionId = drAnswerRulesRow("numQId") And objSurvey.AnswerId = drAnswerRulesRow("numAnsId") And objSurvey.MatrixID = drAnswerRulesRow("numMatrixID") Then
                                    Dim strSelectFields As String
                                    Select Case drAnswerRulesRow("tIntRuleFourRadio")
                                        Case 1, 2
                                            strSelectFields = drAnswerRulesRow("vcRuleFourSelectFields")
                                            Dim strTemp As String
                                            Dim drTemp As DataRow
                                            For Each strTemp In strSelectFields.Split(",")
                                                drTemp = dtSelectField.Rows.Find(strTemp)
                                                If Not drTemp Is Nothing Then   ' If there are no matching rows skip
                                                    Select Case drTemp("vcAssociatedControlType").ToString.Trim()
                                                        Case "TextBox"
                                                            If Not IsDBNull(drSurveyResponse.Item("vcAnsText")) Then
                                                                If drSurveyResponse.Item("vcAnsText") <> "" Then
                                                                    AssignValuesEditBox(objLeads, drSurveyResponse.Item("vcAnsText"), drTemp.Item("vcOrigDbColumnName"))
                                                                End If
                                                            End If
                                                        Case "TextArea"
                                                            If Not IsDBNull(drSurveyResponse.Item("vcAnsText")) Then
                                                                If drSurveyResponse.Item("vcAnsText") <> "" Then
                                                                    AssignValuesTextBox(objLeads, drSurveyResponse.Item("vcAnsText"), drTemp.Item("vcOrigDbColumnName"))
                                                                End If
                                                            End If

                                                        Case "SelectBox"
                                                            If Not IsDBNull(drSurveyResponse.Item("vcAnsText")) Then
                                                                If IsNumeric(drSurveyResponse.Item("vcAnsText")) Then
                                                                    If drSurveyResponse.Item("vcAnsText") <> "" Then
                                                                        AssignValuesSelectBox(objLeads, drSurveyResponse.Item("vcAnsText"), drTemp.Item("vcOrigDbColumnName"))
                                                                    End If
                                                                End If
                                                            End If
                                                    End Select
                                                End If
                                            Next
                                        Case 3
                                            objOpportunity = New MOpportunity()
                                            objOpportunity.OpportunityId = 0
                                            objOpportunity.ContactID = numSurveyRespondentContactId
                                            objOpportunity.DivisionID = numSurveyRespondentDivisionId
                                            objOpportunity.UserCntID = hdRecOwner.Value
                                            objOpportunity.PublicFlag = 0
                                            objOpportunity.EstimatedCloseDate = Date.Now
                                            'objOpportunity.ShipDate = Date.Now
                                            objOpportunity.DomainID = numDomainId
                                            objOpportunity.OpportunityName = "Opportunity " + numSurveyRespondentContactId.ToString()
                                            objOpportunity.OppType = 1      ' 1 for sales opportunity , 2 for purchase opportunity
                                        Case 4
                                            Dim strUrl As String = "../../BacrmUI/Opportunity/frmNewSalesOrder.aspx?&uihTR=" + numSurveyRespondentContactId.ToString() + "&rtyWR=" + numSurveyRespondentDivisionId.ToString()
                                            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "SaleOrderPOPUp", "<script>OpenSaleOrder('" + strUrl + "'); </script>")
                                        Case Else
                                    End Select
                                End If
                            Next
                            ' For Rule 5
                            For Each drAnswerRulesRow In dtAnswerRules.Rows
                                Dim dtSelectField As DataTable = objSurvey.GetSelectFields(1)
                                Dim dc(0) As DataColumn
                                dc(0) = dtSelectField.Columns("SurveySelectFieldId")
                                dtSelectField.PrimaryKey = dc
                                If (drAnswerRulesRow.Item("numRuleId") = 5) And objSurvey.QuestionId = drAnswerRulesRow("numQId") And objSurvey.AnswerId = drAnswerRulesRow("numAnsId") And objSurvey.MatrixID = drAnswerRulesRow("numMatrixID") Then
                                    If Not objOpportunity Is Nothing Then
                                        Dim strSelectFields As String
                                        strSelectFields = drAnswerRulesRow("vcRuleFiveSelectFields")
                                        Dim strTemp As String
                                        Dim drTemp As DataRow
                                        Dim boolIsItemCreated As Boolean = False
                                        'dsItemTemp = objOpportunity.ItemsByOppId()
                                        Dim dtItem As DataTable
                                        Dim dr As DataRow
                                        'dsItemTemp = createSet()
                                        'dtItem = dsItemTemp.Tables(0)

                                        For Each strTemp In strSelectFields.Split(",")
                                            drTemp = dtSelectField.Rows.Find(strTemp)
                                            If Not drTemp Is Nothing Then



                                                Select Case drTemp("vcAssociatedControlType").ToString.Trim()
                                                    Case "TextBox"
                                                        If Not IsDBNull(drSurveyResponse.Item("vcAnsText")) Then
                                                            If drSurveyResponse.Item("vcAnsText") <> "" Then
                                                                AssignValuesOpportunityEditBox(objOpportunity, dr, drTemp.Item("vcOrigDbColumnName"), drSurveyResponse.Item("vcAnsText"))
                                                            End If
                                                        End If
                                                    Case "SelectBox"
                                                        If Not IsDBNull(drSurveyResponse.Item("vcAnsText")) Then
                                                            If IsNumeric(drSurveyResponse.Item("vcAnsText")) Then
                                                                If drSurveyResponse.Item("vcAnsText") <> "" Then
                                                                    AssignValuesOpportunityEditBox(objOpportunity, dr, drTemp.Item("vcOrigDbColumnName"), drSurveyResponse.Item("vcAnsText"))
                                                                End If
                                                            End If
                                                        End If
                                                End Select
                                            End If
                                        Next
                                        strSelectFields = ""
                                        strTemp = ""

                                        If (drAnswerRulesRow.Item("boolPopulateSalesOpportunity") = True) Then
                                            strSelectFields = drAnswerRulesRow("vcItem")
                                            dsItemTemp = createSet()
                                            dtItem = dsItemTemp.Tables(0)
                                            For Each strTemp In strSelectFields.Split(",")
                                                drTemp = dtSelectField.Rows.Find(strTemp)

                                                dr = dtItem.NewRow()
                                                dr.Item("numItemCode") = CLng(strTemp)
                                                Dim intOppCode As Integer
                                                Dim dtItemTable As DataTable
                                                Dim objItems As CItems = New CItems
                                                objItems.ItemCode = CLng(strTemp)
                                                dtItemTable = objItems.ItemDetails
                                                objOpportunity.ItemCode = CLng(strTemp)
                                                objOpportunity.ItemType = "Survey Created"
                                                dr.Item("numoppitemtCode") = objOpportunity.CreateOppItem()
                                                dr.Item("numUnitHour") = 1
                                                dr.Item("monPrice") = dtItemTable.Rows(0).Item("monListPrice")
                                                dr.Item("Op_Flag") = 1
                                                dr.Item("ItemType") = "Survey Created"
                                                dr.Item("monTotAmount") = dr.Item("monPrice") * dr.Item("numUnitHour")
                                                dtItem.Rows.Add(dr)
                                            Next
                                            dsItemTemp.AcceptChanges()
                                        End If

                                    End If
                                End If
                            Next
                        Next
                    End If
                    ' Save all the objects
                    If Not objOpportunity Is Nothing Then
                        Try
                            objOpportunity.strItems = dsItemTemp.GetXml()
                            objOpportunity.Save()
                        Catch
                            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Error", "<script> alert('Error Occured Sales Opportunity was not saved'); </script>")
                        End Try

                    End If
                    Try
                        'objLeads.CreateRecordCompanyInfo()
                        'objLeads.CreateRecordDivisionsInfo()
                        'objLeads.CreateRecordAddContactInfo()
                    Catch
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Error", "<script> alert('Error Occured Lead not saved'); </script>")
                    End Try

                End If
                objSurvey.DomainId = numDomainId                                        'Set the Domain Id
                objSurvey.SaveSurveyResponseInformationInDatabase(dsSurveyExecutionInfo, numSurveyRespondentContactId) 'Call to Save the Survey Respondents Information in teh database and retrieves the ID of the Respondent
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
            Session("dtSurveyRegDTL") = Nothing
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Redirect", "<script> window.location = '" + hdRedirectURL.Value + "'; </script>")
            Response.Redirect(hdRedirectURL.Value, True)                                'Redirect after the survey is submitted
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This call for implementing the Auto Routing Rules
        ''' </summary>
        ''' <param name="objLeadBoxData">Represents the FormGenericLeadBox object.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/08/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function CreateLeadAccordingToAutoRules(ByRef objLeadBoxData As FormGenericLeadBox, ByRef objSurvey As SurveyAdministration) As String
            Try
                Dim dtSurveyData As DataTable                                                                       'declare a datatable
                dtSurveyData = objSurvey.getSurveyRespondentInformation()                                           'Call to get the Respondent information and the value

                Dim drDataRows As DataRow                                                                           'declare a datarow
                Dim objRuleColumnAndValue As ImplementLeadsAutoRoutingRules.RuleColumnAndValue                      'Create a object of ImplementLeadsAutoRoutingRules.RuleColumnAndValue
                Dim arrRuleColumnAndValueArrayList As New ArrayList
                Dim strCustom As String
                strCustom = ""
                For Each drDataRows In dtSurveyData.Rows()                                                          'Loop through the rows
                    objRuleColumnAndValue = New ImplementLeadsAutoRoutingRules.RuleColumnAndValue(drDataRows.Item("vcDbColumnName"), drDataRows.Item("vcDbColumnValueText"))
                    arrRuleColumnAndValueArrayList.Add(objRuleColumnAndValue)                                       'add name and value pairs
                    'Set a sync between the class variables and the database columns to transfer the values
                    SetProperties(objLeadBoxData, drDataRows.Item("vcDbColumnName"), drDataRows.Item("vcDbColumnValue"))
                    If Left(drDataRows.Item("vcDbColumnName"), 9) = "Fld_Value" Then
                        Dim strColumName As String
                        strColumName = drDataRows.Item("vcDbColumnName").replace("Fld_Value", "")
                        strCustom = strCustom & strColumName & "~" & drDataRows.Item("vcDbColumnValue") & ","
                    End If
                Next

                Dim objLeadAutoRules As New ImplementLeadsAutoRoutingRules
                objLeadAutoRules.arrRuleColumnAndValueArrayList = arrRuleColumnAndValueArrayList
                Dim numRecOwner As Integer                                                                          'Declare the record owner
                objLeadAutoRules.DomainID = numDomainId                                                             'Set the Domain Id
                If hdRecOwner.Value = 0 Then
                    numRecOwner = objLeadAutoRules.ImplementRoutingRule()                                           'retrieve the record owner
                Else : numRecOwner = hdRecOwner.Value                                                                  'Pick up the owner as specified
                End If
                objLeadBoxData.recordOwner = numRecOwner                                                            'Set the Record Owner
                objLeadBoxData.UserID = numRecOwner                                                                 'set the user id
                If objLeadBoxData.numContactType = 0 Then                                                           'If Contact Type is not specified
                    objLeadBoxData.numContactType = 70                                        'Make it the primary contact
                    objLeadBoxData.numCompanyType = 46
                End If
                Dim str As String
                str = objLeadBoxData.CreateLead(objLeadBoxData)
                Dim i As Integer
                Dim strRows As String()
                strRows = strCustom.Split(",")
                If strRows.Length > 0 Then
                    Dim objCusFields As New CustomFields
                    objCusFields.DivisionID = objLeadBoxData.numDivisionID
                    objCusFields.ContactID = objLeadBoxData.numContactID
                    For i = 0 To strRows.Length - 2
                        objCusFields.CusFldId = strRows(i).Split("~")(0)
                        objCusFields.CusValue = strRows(i).Split("~")(1)
                        objCusFields.SaveCustomFldsForLeadBox()
                    Next
                End If
                Return str                                                    'Call to create a lead
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This call sets the values for Lead Data in the Class variable
        ''' </summary>
        ''' <param name="objLeadBoxData">Represents the object.</param>
        ''' <param name="sPropertyName">Represents the name of the property in the class.</param>
        ''' <param name="sPropertyValue">Represents the value of the property to be set in the class.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/08/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Sub SetProperties(ByRef objLeadBoxData As FormGenericLeadBox, ByVal sPropertyName As String, ByVal sPropertyValue As String)
            Try
                Dim theType As Type = objLeadBoxData.GetType
                Dim myProperties() As PropertyInfo = theType.GetProperties((BindingFlags.Public Or BindingFlags.Instance))
                Dim fillObject As Object = objLeadBoxData
                Dim PropertyItem As PropertyInfo
                For Each PropertyItem In myProperties                                                       'Loop thru Writeonly public properties
                    With PropertyItem
                        If PropertyItem.Name = sPropertyName Then                                           'Compare the class property name with that of the db column name
                            Select Case PropertyItem.PropertyType.ToString()                                'Selecting the datataype of teh property
                                Case "System.String"
                                    PropertyItem.SetValue(fillObject, sPropertyValue, Nothing)
                                Case "System.Int64", "System.Int32", "System.Int16"                         'Integer type properties
                                    If IsNumeric(sPropertyValue) Then
                                        PropertyItem.SetValue(fillObject, CInt(sPropertyValue), Nothing)    'Typecasting to integer before setting the value
                                    End If
                                Case "System.Boolean"                                                       'Boolean properties
                                    If IsBoolean(sPropertyValue) Then
                                        PropertyItem.SetValue(fillObject, CBool(sPropertyValue), Nothing)   'Typecasting to boolean before setting the value
                                    End If
                            End Select
                            Exit Sub
                        End If
                    End With
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This call returns True if the value passed is Boolean else False
        ''' </summary>
        ''' <param name="sTrg">Represents the value that is being evaluated for being Boolean.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/08/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function IsBoolean(ByVal sTrg As String) As Boolean
            Try
                Dim intValue As Boolean = Convert.ToBoolean(sTrg)                                   'Try Casting to a boolean
            Catch Ex As Exception
                Return False                                                                        'if it throws an error then return false to indicate that it is non boolean
            End Try
            Return True                                                                             'Test passed, its booelan
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This call returns True if the value passed is Integer else False
        ''' </summary>
        ''' <param name="sTrg">Represents the value that is being evaluated for being an Integer.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/08/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function IsNumeric(ByVal sTrg As String) As Boolean
            Try
                Dim intValue As Decimal = Convert.ToInt64(sTrg)                                     'Try Casting to a Integer
            Catch Ex As Exception
                Return False
            End Try
            Return True
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This call returns the URL after appending a http:// (if it is already not there)
        ''' </summary>
        ''' <param name="sTrg">Represents the value that is being evaluated for correctness of URL.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/29/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function CorrectProtocol(ByVal sTrg As String) As String
            Try
                If sTrg.Substring(0, 4) <> "http" Then          'Check the URL
                    sTrg = "http://" & sTrg                     'else correct the URL
                End If
            Catch Ex As Exception
                Return sTrg
            End Try
            Return sTrg
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to attach client side events to the controls
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/22/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Sub PostInitializeControlsClientEvents(ByVal boolPostRegistration As Boolean)
            Try
                If (boolPostRegistration) Then
                    btnConcludeSurvey.Attributes.Add("onclick", "javascript: return DoPostRegistration(" & hdSurId.Value & "," & numDomainId & ");")                 'calls for closing the Rules window
                Else : btnConcludeSurvey.Attributes.Add("onclick", "javascript: return ConfirmSurveySubmission(true);")                 'calls for closing the Rules window
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Sub AssignValuesOpportunityEditBox(ByRef objOpp As MOpportunity, ByRef drItem As DataRow, ByVal strDBColName As String, ByVal strValue As String)
            Select Case strDBColName


                Case "vcPOppName"
                    objOpp.OpportunityName = strValue
                Case "intPEstimatedCloseDate"
                    objOpp.EstimatedCloseDate = CDate(strValue)
                Case "numAssignedTo"
                    objOpp.AssignedTo = CLng(strValue)
                Case "numCampainID"
                    objOpp.CampaignID = CLng(strValue)
                Case "lngPConclAnalysis"
                    objOpp.ConAnalysis = CLng(strValue)
                Case "tintSource"
                    objOpp.Source = CLng(strValue)
                Case "tintActive"
                    objOpp.Active = CLng(strValue)
                Case "numSalesType"
                    objOpp.SalesorPurType = CLng(strValue)
                Case "numPurType"
                    objOpp.SalesorPurType = CLng(strValue)
                Case "monPAmount"
                    objOpp.Amount = CDec(strValue)

                    'Case "vcItemName"
                    '    drItem.Item("vcItemName") = strValue
                    'Case "txtItemDesc"
                    '    drItem.Item("vcItemDesc") = strValue

                    'Case "vcModelID"
                    '    drItem.Item("vcModelID") = strValue
                    'Case "charItemType"
                    '    drItem.Item("ItemType") = strValue
                    'Case "bitSerialized"
                    '    drItem.Item("bitSerialized") = strValue

                    'Case "monListPrice"
                    '    drItem.Item("monPrice") = CDec(strValue)

                    'Case "numItemCode"
                    '    drItem.Item("numItemCode") = CLng(strValue)
                    '    objOpp.ItemCode = CLng(strValue)

                    'Case "monPrice"
                    '    drItem.Item("numItemCode") = CDec(strValue)
                    'Case "bitKitParent"
                    '    If (CBool(strValue) = True) Then
                    '        drItem.Item("Op_Flag") = 2
                    '    Else
                    '        drItem.Item("Op_Flag") = 1
                    '    End If
                    'Case "numUnitHour"
                    '    drItem.Item("numUnitHour") = CDec(strValue)

            End Select

        End Sub
        Function createSet() As DataSet
            Try

                Dim dsTemp As DataSet = New DataSet
                Dim dtItem As New DataTable
                Dim dtSerItem As New DataTable
                Dim dtChildItems As New DataTable
                dtItem.Columns.Add("numoppitemtCode")
                dtItem.Columns.Add("numItemCode")
                dtItem.Columns.Add("numUnitHour")
                dtItem.Columns.Add("monPrice")
                dtItem.Columns.Add("monTotAmount", GetType(Decimal))
                dtItem.Columns.Add("numSourceID")
                dtItem.Columns.Add("vcItemDesc")
                dtItem.Columns.Add("numWarehouseID")
                dtItem.Columns.Add("vcItemName")
                dtItem.Columns.Add("Warehouse")
                dtItem.Columns.Add("numWarehouseItmsID")
                dtItem.Columns.Add("ItemType")
                dtItem.Columns.Add("Attributes")
                dtItem.Columns.Add("Op_Flag")
                dtItem.Columns.Add("DropShip", GetType(Boolean))

                dtSerItem.Columns.Add("numWarehouseItmsDTLID")
                dtSerItem.Columns.Add("numWItmsID")
                dtSerItem.Columns.Add("numoppitemtCode")
                dtSerItem.Columns.Add("vcSerialNo")
                dtSerItem.Columns.Add("Comments")
                dtSerItem.Columns.Add("Attributes")

                dtChildItems.Columns.Add("numOppChildItemID")
                dtChildItems.Columns.Add("numoppitemtCode")
                dtChildItems.Columns.Add("numItemCode")
                dtChildItems.Columns.Add("numQtyItemsReq")
                dtChildItems.Columns.Add("vcItemName")
                dtChildItems.Columns.Add("monListPrice")
                dtChildItems.Columns.Add("UnitPrice")
                dtChildItems.Columns.Add("charItemType")
                dtChildItems.Columns.Add("txtItemDesc")
                dtChildItems.Columns.Add("Op_Flag")

                dtItem.TableName = "Item"
                dtSerItem.TableName = "SerialNo"
                dtChildItems.TableName = "ChildItems"

                dsTemp.Tables.Add(dtItem)
                dsTemp.Tables.Add(dtSerItem)
                dsTemp.Tables.Add(dtChildItems)
                dtItem.PrimaryKey = New DataColumn() {dsTemp.Tables(0).Columns("numoppitemtCode")}
                dtChildItems.PrimaryKey = New DataColumn() {dsTemp.Tables(2).Columns("numOppChildItemID")}
                'dsTemp.Relations.Add("KitRel", ds.Tables(0).Columns("numoppitemtCode"), ds.Tables(2).Columns("numoppitemtCode"))
                'dsTemp.Relations.Add("Item", ds.Tables(0).Columns("numWarehouseID"), ds.Tables(1).Columns("numWarehouseID"))
                'Session("Data") = dsTemp
                Return dsTemp
            Catch ex As Exception
                Throw ex
            End Try
        End Function



    End Class
End Namespace
