'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace BACRM.UserInterface.Survey

    Partial Public Class frmSurveyExecutionSurvey

        '''<summary>
        '''Head1 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Head1 As Global.System.Web.UI.HtmlControls.HtmlHead

        '''<summary>
        '''MasterCSS control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents MasterCSS As Global.System.Web.UI.HtmlControls.HtmlLink

        '''<summary>
        '''frmSurveyExecutionSurvey control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents frmSurveyExecutionSurvey As Global.System.Web.UI.HtmlControls.HtmlForm

        '''<summary>
        '''hdRuleFourActive control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents hdRuleFourActive As Global.System.Web.UI.HtmlControls.HtmlInputHidden

        '''<summary>
        '''hdRuleFiveActive control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents hdRuleFiveActive As Global.System.Web.UI.HtmlControls.HtmlInputHidden

        '''<summary>
        '''tIntRuleFourRadio control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents tIntRuleFourRadio As Global.System.Web.UI.HtmlControls.HtmlInputHidden

        '''<summary>
        '''tIntCreateNewID control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents tIntCreateNewID As Global.System.Web.UI.HtmlControls.HtmlInputHidden

        '''<summary>
        '''boolPopulateSalesOpportunity control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents boolPopulateSalesOpportunity As Global.System.Web.UI.HtmlControls.HtmlInputHidden

        '''<summary>
        '''lblHeader control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents lblHeader As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''hdProfile control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents hdProfile As Global.System.Web.UI.HtmlControls.HtmlInputHidden

        '''<summary>
        '''hdFollowUpStatus control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents hdFollowUpStatus As Global.System.Web.UI.HtmlControls.HtmlInputHidden

        '''<summary>
        '''lblFooter control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents lblFooter As Global.System.Web.UI.WebControls.Label
    End Class
End Namespace
