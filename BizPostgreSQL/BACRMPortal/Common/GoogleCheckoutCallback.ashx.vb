﻿Imports System.Web
Imports System.Web.Services
Imports System.IO
Imports GCheckout
Imports GCheckout.AutoGen
Imports GCheckout.Util
Imports GCheckout.MerchantCalculation
Imports BACRM.BusinessLogic.Common

Public Class GoogleCheckoutCallback
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Try
            'Serial Number Send back for acknowledgement ...
            Dim SerialNumber As String = ""

            'Get XML Send by Google Checkout
            Dim RequestStream As Stream = context.Request.InputStream
            Dim RequestStreamReader As StreamReader = New StreamReader(RequestStream)
            Dim RequestXml As String = RequestStreamReader.ReadToEnd()   'doc.InnerXml  'RequestStreamReader.ReadToEnd()  'Convert.ToString(doc.InnerXml);  
            RequestStream.Close()

            'Manage Various Notifications...
            Select Case (EncodeHelper.GetTopElement(RequestXml))
                Case "new-order-notification"
                    Dim N1 As NewOrderNotification = CType(EncodeHelper.Deserialize(RequestXml, GetType(NewOrderNotification)), NewOrderNotification)
                    SerialNumber = N1.serialnumber
                    Dim xmlNodes() As System.Xml.XmlNode = N1.shoppingcart.merchantprivatedata.Any
                    Dim Common As New CCommon
                    Common.Mode = 26
                    Common.UpdateRecordID = CCommon.ToLong(xmlNodes(0).ChildNodes(0).ChildNodes(0).Value)
                    Common.DomainID = CCommon.ToLong(xmlNodes(0).ChildNodes(1).ChildNodes(0).Value)
                    Common.UpdateSingleFieldValue()

            End Select

            'Send acknowledgement back with serial Number .Otherwise google checkout countinuously send Request .
            Dim response As New GCheckout.AutoGen.NotificationAcknowledgment With
                {.serialnumber = SerialNumber}
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.BinaryWrite(EncodeHelper.Serialize(response))
       
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class