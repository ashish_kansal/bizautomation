﻿Imports System.Web
Imports System.Web.Services
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Common

Public Class EmailTracking1
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        context.Response.ContentType = "text/plain"
        Try
            If CCommon.ToLong(context.Request.QueryString("EmailHstrID")) > 0 Then
                Dim objCampaign As New Campaign
                objCampaign.EmailHstrId = CCommon.ToLong(context.Request.QueryString("EmailHstrID"))
                objCampaign.UpdateEmailHstrTracking()
            End If
            If CCommon.ToLong(context.Request.QueryString("BroadCastID")) > 0 And CCommon.ToLong(context.Request.QueryString("ContID")) > 0 Then
                Dim objCampaign As New Campaign
                objCampaign.broadcastID = CCommon.ToLong(context.Request.QueryString("BroadCastID"))
                objCampaign.ContactId = CCommon.ToLong(context.Request.QueryString("ContID"))
                objCampaign.UpdateBroadHstrTracking()
            End If
            If CCommon.ToLong(context.Request.QueryString("BroadCastID")) > 0 And CCommon.ToLong(context.Request.QueryString("LinkId")) > 0 And CCommon.ToLong(context.Request.QueryString("ContactId")) > 0 Then
                Dim objCampaign As New Campaign
                objCampaign.broadcastID = CCommon.ToLong(context.Request.QueryString("BroadCastID"))
                objCampaign.numLinkId = CCommon.ToLong(context.Request.QueryString("LinkId"))
                objCampaign.ContactId = CCommon.ToLong(context.Request.QueryString("ContactID"))

                objCampaign.ManageBroadcastingLink()

                Dim dtTable As DataTable
                
                dtTable = objCampaign.GetBroadcastingLink()
                If dtTable.Rows.Count > 0 Then
                    Dim strLink As String = CCommon.ToString(dtTable.Rows(0)("vcOriginalLink"))
                    If strLink.ToLower().StartsWith("http") Then
                        context.Response.Redirect(strLink, False)
                    Else
                        context.Response.Redirect("http://" + strLink, False)
                    End If
                End If
            End If
            If CCommon.ToLong(context.Request.QueryString("numConECampDTLID")) > 0 Then
                Dim objCampaign As New Campaign
                objCampaign.UpdateCampaignTracking(CCommon.ToLong(context.Request.QueryString("numConECampDTLID")))
            End If
            If CCommon.ToLong(context.Request.QueryString("ConECampDTLID")) > 0 And CCommon.ToLong(context.Request.QueryString("LinkID")) > 0 Then
                Dim objCampaign As New Campaign
                objCampaign.numLinkId = CCommon.ToLong(context.Request.QueryString("LinkID"))
                Dim strLink As String = objCampaign.UpdateConECampaignDTLLinkClicked()

                If Not String.IsNullOrEmpty(strLink) Then
                    If strLink.ToLower().StartsWith("http") Then
                        context.Response.Redirect(strLink, False)
                    Else
                        context.Response.Redirect("http://" + strLink, False)
                    End If
                End If
            End If

            context.Response.Write("Hello")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class