﻿Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Outlook
Imports BACRM.BusinessLogic.Opportunities

Public Class ActionItemsAttendees
    Inherits BACRMPage

    Private lngCommId, lngContId, lngDomainId As Long
    Dim objCommon As CCommon
    Dim objActionItem As ActionItem
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngCommId = GetQueryStringVal( "EId")
            lngContId = GetQueryStringVal( "CId")
            lngDomainId = GetQueryStringVal( "D")

            If lngCommId = 0 Or lngContId = 0 Or lngDomainId = 0 Then
                Return
            End If

            If Not IsPostBack Then
                objCommon = New CCommon
                objCommon.sb_FillComboFromDB(ddlActionItemsAttendees, 320, lngDomainId)

                objActionItem = New ActionItem

                objActionItem.CommID = lngCommId
                objActionItem.UserCntID = lngContId

                Dim dtAttendeeActivity As DataTable
                dtAttendeeActivity = objActionItem.CommunicationAttendees_Detail

                If dtAttendeeActivity.Rows.Count = 0 Then
                    FActionItems.Visible = False
                    lblMessage.Text = "No Action Items for you!!!"
                    lblMessage.Visible = True
                    Exit Sub
                Else
                    If ddlActionItemsAttendees.Items.FindByValue(dtAttendeeActivity.Rows(0).Item("numStatus")) IsNot Nothing Then
                        ddlActionItemsAttendees.Items.FindByValue(dtAttendeeActivity.Rows(0).Item("numStatus")).Selected = True
                    End If
                End If

                If lngDomainId > 0 Then
                    Dim dtTable As DataTable
                    Dim objUserAccess As New UserAccess
                    objUserAccess.DomainID = lngDomainId
                    dtTable = objUserAccess.GetDomainDetails()
                    If dtTable.Rows.Count > 0 Then
                        If Not IsDBNull(dtTable.Rows(0).Item("vcPortalLogo")) Then imgLogo.Src = "../Documents/Docs/" & objUserAccess.DomainID & "/" & dtTable.Rows(0).Item("vcPortalLogo")
                    Else : imgLogo.Visible = False
                    End If
                End If

                Dim objOppBizDoc As New OppBizDocs
                objOppBizDoc.DomainID = lngDomainId
                objOppBizDoc.BizDocId = 0
                objOppBizDoc.OppType = 0
                objOppBizDoc.TemplateType = 2

                Dim dt As DataTable = objOppBizDoc.GetBizDocTemplate()
                If dt.Rows.Count > 0 Then
                    'RadEditor1.Content = HttpUtility.HtmlDecode(CCommon.ToString(dt.Rows(0)("txtBizDocTemplate")))

                    Dim strBizDocUI As String = HttpUtility.HtmlDecode(dt.Rows(0)("txtBizDocTemplate"))
                    If strBizDocUI.Length > 0 Then
                        strBizDocUI = strBizDocUI.Replace("#Logo#", CCommon.RenderControl(imgLogo))
                        strBizDocUI = strBizDocUI.Replace("#AttendeesStatusDropDown#", CCommon.RenderControl(ddlActionItemsAttendees))
                        strBizDocUI = strBizDocUI.Replace("#SubmitButton#", CCommon.RenderControl(btnSubmit))
                    End If

                    litTemplate.Text = strBizDocUI
                    tblOriginal.Visible = False
                    tblFormatted.Visible = True

                    Dim styles As New HtmlGenericControl("style")
                    styles.Attributes.Add("type", "text/css")
                    styles.InnerText = HttpUtility.HtmlDecode(CCommon.ToString(dt.Rows(0)("txtCSS")))
                    Me.Header.Controls.Add(styles)
                End If

            
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        Try
            objActionItem = New ActionItem

            objActionItem.CommID = lngCommId
            objActionItem.UserCntID = lngContId
            objActionItem.Status = ddlActionItemsAttendees.SelectedValue

            objActionItem.ChangeCommunicationAttendees_Status()

            lblMessage.Text = "Action Items Status updated successfully!!!"
            lblMessage.Visible = True
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub
End Class