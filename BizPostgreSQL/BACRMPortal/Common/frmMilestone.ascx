﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmMilestone.ascx.vb"
    Inherits="BACRMPortal.Milestone" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="~/include/calandar.ascx" %>
<%@ Register Src="~/Common/frmComments.ascx" TagName="frmComments" TagPrefix="uc1" %>
<script language="javascript">
    function $Search(name, tag, elm) {
        var tag = tag || "*";
        var elm = elm || document;
        var elements = (tag == "*" && elm.all) ? elm.all : elm.getElementsByTagName(tag);
        var returnElements = [];
        var current;
        var length = elements.length;

        for (var i = 0; i < length; i++) {
            current = elements[i];
            if (current.id.indexOf(name) > 0) {
                returnElements.push(current);
            }
        }
        return returnElements[0];
    };
    function OpenDocuments(a) {
        window.open("../Documents/frmSpecDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=PS&yunWE=" + a, '', 'toolbar=no,titlebar=no,top=200,width=700,height=450,scrollbars=yes,resizable=yes')
        return false;
    }

    function OpenTransfer(url) {
        window.open(url, '', "width=340,height=150,status=no,top=100,left=150");
        return false;
    }

    //var prg_width = 60;
    function progress(ProgInPercent, Container, progress, prg_width) {
        //Set Total Width
        var OuterDiv = document.getElementById(Container);
        OuterDiv.style.width = prg_width + 'px';
        OuterDiv.style.height = '5px';

        if (ProgInPercent > 100) {
            ProgInPercent = 100;
        }
        //Set Progress Percentage
        var node = document.getElementById(progress);
        node.style.width = parseInt(ProgInPercent) * prg_width / 100 + 'px';
    }

    function EditSvae() {


        if (document.getElementById('txtStageName').value == 0) {
            alert("Enter Stage Name")
            document.getElementById('txtStageName').focus();
            return false;
        }
        if ($Search('calStartDate_txtDate').value == 0) {
            alert("Enter Start Date")
            $Search('calStartDate_txtDate').focus();
            return false;
        }
        if ($Search('calDueDate_txtDate').value == 0) {
            alert("Enter Due Date")
            $Search('calDueDate_txtDate').focus();
            return false;
        }

        var SDate = parseDate($Search('calStartDate_txtDate').value);
        var EDate = parseDate($Search('calDueDate_txtDate').value);

        var startDate = formatDate(SDate, 'dd-MM-yyyy')
        var endDate = formatDate(EDate, 'dd-MM-yyyy')


        if (compareDates(startDate, 'dd-MM-yyyy', endDate, 'dd-MM-yyyy') == 1) {
            alert("Due Date must be greater than or equal to  Start Date");
            return false;
        }

    }

    function AddSave() {

        //        var sessionValue = '<%= Session("StagePercentage") %>';
        //        if (sessionValue == "") {
        //            alert("Set % of completion");
        //            return false;
        //        }

        if (document.getElementById('txtAStageName').value == 0) {
            alert("Enter Stage Name")
            document.getElementById('txtAStageName').focus();
            return false;
        }
        if ($Search('calAStartDate_txtDate').value == 0) {
            alert("Enter Start Date")
            $Search('calAStartDate_txtDate').focus();
            return false;
        }
        if ($Search('calADueDate_txtDate').value == 0) {
            alert("Enter Due Date")
            $Search('calADueDate_txtDate').focus();
            return false;
        }

        var SDate = parseDate($Search('calAStartDate_txtDate').value);
        var EDate = parseDate($Search('calADueDate_txtDate').value);

        var startDate = formatDate(SDate, 'dd-MM-yyyy')
        var endDate = formatDate(EDate, 'dd-MM-yyyy')


        if (compareDates(startDate, 'dd-MM-yyyy', endDate, 'dd-MM-yyyy') == 1) {
            alert("Due Date must be greater than or equal to  Start Date");
            return false;
        }
    }

    function CheckDependencies() {
        if (document.getElementById("ddlProjectStage").value == 0) {
            alert('Select stage');
            document.getElementById("ddlProjectStage").focus();
            return false;
        }
        return true;
    }

    function CheckStagePercentage() {
        if (document.getElementById("rdbStageType_0").checked) {
            //window.open("frmStagePercentage.aspx?StageID=" + document.getElementById("hfParentStageID1").value, '', 'toolbar=no,titlebar=no,top=200,width=700,height=450,scrollbars=yes,resizable=yes')
            window.open("../projects/frmStagePercentage.aspx?Type=Parent&StageID=" + document.getElementById("hfStageID1").value, '', 'toolbar=no,titlebar=no,top=200,width=700,height=450,scrollbars=yes,resizable=yes')
        }
        else {
            window.open("../projects/frmStagePercentage.aspx?Type=Child&StageID=" + document.getElementById("hfStageID1").value, '', 'toolbar=no,titlebar=no,top=200,width=700,height=450,scrollbars=yes,resizable=yes')
        }
        return false;
    }

    function CheckNumber(cint, e) {
        var k;
        document.all ? k = e.keyCode : k = e.which;
        if (cint == 1) {
            if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                if (e.preventDefault) {
                    e.preventDefault();
                }
                else
                    e.returnValue = false;
                return false;
            }
        }
        if (cint == 2) {
            if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
                if (e.preventDefault) {
                    e.preventDefault();
                }
                else
                    e.returnValue = false;
                return false;
            }
        }
    }
</script>
<style type="text/css">
    a:link
    {
        text-decoration: none;
    }
    .MilestoneHeader
    {
        font-family: Arial;
        font-size: 8pt;
        font-style: normal;
        font-variant: normal;
        font-weight: normal;
        color: White;
        background-color: #667193;
    }
    .ProjectName
    {
        font-family: "Segoe UI" ,Arial,sans-serif;
        font-size: 12px;
        font-variant: normal;
        font-weight: bold;
    }
    .SelectedNodeStyle
    {
        border: 2px solid highlight;
        padding: 3px 5px;
    }
    .NodeStyle
    {
        font-size: 8pt;
        color: Black;
        padding: 3px 5px;
    }
</style>
<asp:Table ID="tblMileStoneDetail" CellPadding="0" CellSpacing="0" BorderWidth="0"
    runat="server" Width="100%" CssClass="asptable" BorderColor="black" GridLines="None">
    <asp:TableRow ID="tblProgress" runat="server">
        <asp:TableCell VerticalAlign="Top" ColumnSpan="2">
            <table width="100%" class="MilestoneHeader">
                <tr>
                    <td width="45%">
                        <asp:Label ID="lblProjectName" runat="server" CssClass="ProjectName"></asp:Label>
                    </td>
                    <td>
                        Total Progress :
                        <asp:Label ID="lblTotalProgress" runat="server"></asp:Label>&nbsp;
                        <div id="TotalProgressContainer" style="border: 1px solid white; height: 5px; font-size: 1px;
                            background-color: White; line-height: 0;">
                            &nbsp;<div id="TotalProgress" style="height: 5px; width: 0px; background-color: Green;">
                            </div>
                        </div>
                    </td>
                    <td align="right" width="45%">
                        Business Process&nbsp;
                        <asp:DropDownList ID="ddlProcessList" runat="server" CssClass="signup" Width="180">
                        </asp:DropDownList>
                        &nbsp;
                        <asp:Button ID="btnAddProcess" runat="server" Text="Add Process" CssClass="button" />&nbsp;
                        <asp:Button ID="btnRemoveProcess" runat="server" Text="Remove Process" CssClass="button"
                            OnClientClick="return confirm('Are you sure, you want to remove the selected Process?');" />
                    </td>
                </tr>
            </table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow ID="tblRowStatge" runat="server">
        <asp:TableCell HorizontalAlign="Left" VerticalAlign="Top" Width="55%" Style="border-right-style: dotted;
            border-right-width: 1.5px;border-right-color:Gray;">
            &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="btnColExp" runat="server" Text="Collapse Tree"
                CommandName="Collapse" ToolTip="Collapse Project Stage" Font-Bold="true" ForeColor="GrayText"
                Font-Size="XX-Small" />
            <asp:TreeView ID="tvProjectStage" runat="server" CssClass="treeview" ExpandDepth="0"
                Font-Size="XX-Small" ImageSet="Arrows" PathSeparator="|">
                <HoverNodeStyle Font-Underline="false" ForeColor="#5555DD" />
                <SelectedNodeStyle CssClass="SelectedNodeStyle" />
                <NodeStyle CssClass="NodeStyle" />
            </asp:TreeView>
        </asp:TableCell><asp:TableCell HorizontalAlign="Left" VerticalAlign="Top" Width="45%">
            <asp:MultiView ID="mvStage" runat="server">
                <asp:View ID="vDetail" runat="server">
                    <table class="normal1" width="100%">
                        <tr class="tr1">
                            <td align="right">
                                <strong>Assign To: </strong>
                            </td>
                            <td align="left">
                                <asp:Label ID="lblDAssignTo" runat="server"></asp:Label>
                            </td>
                            <td align="right">
                                <strong>Last Modified By: </strong>
                            </td>
                            <td align="left">
                                <asp:Label ID="lblDLastModify" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="right" valign="bottom">
                                <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="button" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <strong>Stage Name:</strong>
                            </td>
                            <td align="left">
                                <asp:Label ID="lblDStageName" runat="server"></asp:Label>
                            </td>
                            <td style="color: GrayText" align="right">
                                <strong>Created By: </strong>
                            </td>
                            <td style="color: GrayText" align="right">
                                <asp:Label ID="lblDCreatedBy" runat="server" Style="float: left; color: Gray;"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <strong>Stage Progress:</strong>
                            </td>
                            <td align="left">
                                <asp:Label ID="lblDStageProgress" runat="server"></asp:Label>&nbsp;<div id="ProgressContainer"
                                    style="border: 1px solid black; height: 5px; font-size: 1px; line-height: 0;
                                    width: 60px">
                                    &nbsp;<div id="progress" style="height: 5px; width: 0px; background-color: green;">
                                    </div>
                                </div>
                            </td>
                            <td align="right">
                                <strong>Start Date:</strong>
                            </td>
                            <td align="left">
                                <asp:Label ID="lblDStartDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <img src="../images/folder_add.png" />
                                <strong>Documents:</strong>
                            </td>
                            <td align="left">
                                <asp:HyperLink ID="hplDDocuments" runat="server" CssClass="hyperlink">
                                </asp:HyperLink>
                            </td>
                            <td align="right">
                                <strong>Due Date:</strong>
                            </td>
                            <td align="left">
                                <asp:Label ID="lblDDueDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <strong>Description:</strong>
                            </td>
                            <td colspan="3" align="left">
                                <asp:Label ID="lblDDescription" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr id="trDTimeDetail" runat="server">
                            <td align="right">
                                <img src="../images/time_add.png" /><strong>
                                <asp:HyperLink ID="hplDStageTime" CssClass="hyperlink" runat="server">Time:</asp:HyperLink>
                                <asp:Label ID="lblDStageTime" runat="server" Text="Time:" Visible="false"></asp:Label></strong>
                            </td>
                            <td align="left" colspan="3">
                                <asp:Label ID="lblDTime" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;
                                <asp:Label ID="lblTimeBudget" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr id="trDExpnseDetail" runat="server">
                            <td align="right">
                                <img src="../images/money_add.png" /><strong>
                                <asp:HyperLink ID="hplDStageExpense" CssClass="hyperlink" runat="server">Expense:</asp:HyperLink>
                                <asp:Label ID="lblDStageExpense" runat="server" Text="Expense:" Visible="false" ></asp:Label></strong>
                            </td>
                            <td align="left" colspan="3">
                                <asp:Label ID="lblDExpense" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;
                                <asp:Label ID="lblExpenseBudget" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr id="trDDependencies" runat="server">
                            <td colspan="4">
                                <fieldset>
                                    <legend><strong>Dependencies</strong></legend>
                                    <asp:TreeView ID="tvDependencies" runat="server">
                                    </asp:TreeView>
                                </fieldset>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <fieldset>
                                    <legend><strong>Comments</strong></legend>
                                    <uc1:frmComments ID="frmComments1" runat="server" />
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </asp:View>
                <asp:View ID="vEdit" runat="server">
                    <table class="normal1" 
                        width="100%">
                        <tr>
                            <td>
                                <asp:Button ID="btnAdd" runat="server" Text="Add New Stage" CssClass="button" />
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <asp:Button ID="btnDelete" runat="server" Text="Delete This Stage" CssClass="button" />
                            </td>
                            <td>
                                <asp:Button ID="btnSave" runat="server" Text="Save & Close" CssClass="button" />
                                <asp:HiddenField ID="hfStagePerID" runat="server" />
                                <asp:HiddenField ID="hfConfiguration" runat="server" />
                                <asp:HiddenField ID="hfSalesProsID" runat="server" />
                                <asp:HiddenField ID="hfMileStoneName" runat="server" />
                                <asp:HiddenField ID="hfParentStageID" runat="server" ClientIDMode="Static" />
                                <asp:HiddenField ID="hfStageID" runat="server" ClientIDMode="Static" />
                                <asp:HiddenField ID="hfDueDays" runat="server" />
                                <asp:HiddenField ID="hfAssignTo" runat="server" />
                                <asp:HiddenField ID="hfProjectID" runat="server" />
                            </td>
                        </tr>
                        <tr class="tr1">
                            <td align="right">
                                <strong>
                                    <asp:HyperLink ID="hplEAssignTo" runat="server" Visible="true" CssClass="BizLink"
                                        ToolTip="Transfer Ownership" Text="Assign To:"></asp:HyperLink>
                                    <asp:Label ID="lblEAssignToText" runat="server" Text="Assign To:" Visible="false"></asp:Label>
                                </strong>
                            </td>
                            <td align="left">
                                <asp:Label ID="lblEAssignTo" runat="server"></asp:Label>
                            </td>
                            <td align="right">
                                <strong>Last Modified By:</strong>
                            </td>
                            <td align="left">
                                <asp:Label ID="lblELastModify" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <strong>Stage Name:</strong>
                            </td>
                            <td colspan="3" align="left">
                                <asp:TextBox ID="txtStageName" runat="server" Width="250" MaxLength="50" ClientIDMode="Static"
                                    CssClass="signup"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <strong>Stage Progress:</strong>
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddStageProgress" runat="server" CssClass="signup">
                                    <asp:ListItem Value="0" Selected="True">0</asp:ListItem>
                                    <asp:ListItem Value="25" Selected="True">25%</asp:ListItem>
                                    <asp:ListItem Value="50">50%</asp:ListItem>
                                    <asp:ListItem Value="75">75%</asp:ListItem>
                                    <asp:ListItem Value="100">100%</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="right">
                                <strong>Documents:</strong>
                            </td>
                            <td align="left">
                                <asp:HyperLink ID="hplEDocuments" runat="server" CssClass="hyperlink">
                                </asp:HyperLink>
                            </td>
                        </tr>
                        <tr id="trDate" runat="server">
                            <td align="right">
                                <strong>Start Date:</strong>
                            </td>
                            <td align="left">
                                <BizCalendar:Calendar ID="calStartDate" runat="server" />
                            </td>
                            <td align="right">
                                <strong>Due Date:</strong>
                            </td>
                            <td align="left">
                                <BizCalendar:Calendar ID="calDueDate" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <strong>Description:</strong>
                            </td>
                            <td colspan="3" align="left">
                                <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Columns="30"
                                    CssClass="signup"></asp:TextBox>
                            </td>
                        </tr>
                        <tr id="trETimeDetail" runat="server">
                            <td align="right"><strong>
                                <asp:HyperLink ID="hplEStageTime" CssClass="hyperlink" runat="server">Time:</asp:HyperLink>
                                <asp:Label ID="lblEStageTime" runat="server" Text="Time:" Visible="false"></asp:Label></strong>
                            </td>
                            <td align="left" colspan="3">
                                <asp:Label ID="lblETime" runat="server"></asp:Label>
                                <span id="spTimeBudget" runat="server">&nbsp;&nbsp;&nbsp;
                                    <asp:CheckBox ID="chkTimeBudget" runat="server" ClientIDMode="Static" />&nbsp;<label
                                        for="chkTimeBudget"><strong>Billable Time Budget (in Hrs):</strong></label>&nbsp;<asp:TextBox ID="txtTimeBudget" runat="server"
                                            Width="40px" onkeypress="CheckNumber(1,event)" MaxLength="10" CssClass="signup"></asp:TextBox></span>
                            </td>
                        </tr>
                        <tr id="trEExpenseDetail" runat="server">
                            <td align="right"><strong>
                                <asp:HyperLink ID="hplEStageExpense" CssClass="hyperlink" runat="server">Expense:</asp:HyperLink>
                                <asp:Label ID="lblEStageExpense" runat="server" Text="Expense:" Visible="false"></asp:Label></strong>
                            </td>
                            <td align="left" colspan="3">
                                <asp:Label ID="lblEExpense" runat="server"></asp:Label>
                                <span id="spExpenseBudget" runat="server">&nbsp;&nbsp;&nbsp;
                                    <asp:CheckBox ID="chkExpenseBudget" runat="server" ClientIDMode="Static" />&nbsp;<label
                                        for="chkExpenseBudget"><strong>Expense Budget (Amt):</strong></label>&nbsp;<asp:TextBox ID="txtExpenseBudget"
                                            runat="server" Width="40px" onkeypress="CheckNumber(1,event)" MaxLength="10"
                                            CssClass="signup"></asp:TextBox></span>
                            </td>
                        </tr>
                        <tr id="trEDependencies" runat="server">
                            <td colspan="4">
                                <fieldset>
                                    <legend><strong>Upstream Dependencies </strong></legend>
                                    <table>
                                        <tr>
                                            <td colspan="4">
                                                Stages that need to be completed before this one begins.
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <asp:DropDownList ID="ddlProjectStage" runat="server" CssClass="signup" ClientIDMode="Static">
                                                </asp:DropDownList>
                                                &nbsp;
                                                <asp:Button ID="btnDependencies" runat="server" Text="Add" CssClass="button" OnClientClick="return CheckDependencies()" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <br />
                                                <asp:GridView ID="gvDependencies" runat="server" Width="100%" CssClass="dg" AllowSorting="true"
                                                    AutoGenerateColumns="False" BorderColor="white" DataKeyNames="numStageDetailId,numDependantOnId"
                                                    ShowHeader="false" BorderStyle="Solid" BorderWidth="1">
                                                    <AlternatingRowStyle CssClass="is" />
                                                    <RowStyle CssClass="ais"></RowStyle>
                                                    <HeaderStyle CssClass="hs"></HeaderStyle>
                                                    <Columns>
                                                        <asp:BoundField DataField="vcStageName" ItemStyle-HorizontalAlign="Center" />
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="DeleteDependency"
                                                                    CommandArgument="<%# Container.DataItemIndex %>" Text="Delete" OnClientClick="return confirm('Are you sure, you want to delete the selected Dependency?');"> </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </asp:View>
                <asp:View ID="vAdd" runat="server">
                    <table class="normal1" style="border-left-style: solid; border-left-width: thin">
                        <tr>
                            <td colspan="4" align="right">
                                <asp:Button ID="btnAddSave" runat="server" Text="Save & Close" CssClass="button"
                                    OnClientClick="return AddSave()" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                            </td>
                            <td colspan="3" align="left">
                                <asp:RadioButtonList ID="rdbStageType" runat="server" CssClass="signup" ClientIDMode="Static">
                                    <asp:ListItem Value="0" Selected="True">New Stage on Same Level</asp:ListItem>
                                    <asp:ListItem Value="1">Sub Stage on this One</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <strong>Stage Name:</strong>
                            </td>
                            <td colspan="3" align="left">
                                <asp:TextBox ID="txtAStageName" runat="server" Width="250" MaxLength="50" ClientIDMode="Static"
                                    CssClass="signup"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <strong>This stage comprises % of completion:</strong>
                            </td>
                            <td colspan="3" align="left">
                                <asp:HiddenField ID="hfParentStageID1" runat="server" ClientIDMode="Static" />
                                <asp:HiddenField ID="hfStageID1" runat="server" ClientIDMode="Static" />
                                <%-- <asp:DropDownList ID="ddAStageProgress" runat="server" CssClass="signup">
                                    <asp:ListItem Value="0">0</asp:ListItem>
                                    <asp:ListItem Value="25">25%</asp:ListItem>
                                    <asp:ListItem Value="50">50%</asp:ListItem>
                                    <asp:ListItem Value="75">75%</asp:ListItem>
                                    <asp:ListItem Value="100">100%</asp:ListItem>
                                </asp:DropDownList>--%><asp:Label ID="lblAStagePercentage" runat="server" ClientIDMode="Static"
                                    Font-Bold="true"></asp:Label><asp:HyperLink ID="hfSetPercentage" runat="server" ClientIDMode="Static">Set % of completion</asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <strong>Start Date:</strong>
                            </td>
                            <td align="left">
                                <BizCalendar:Calendar ID="calAStartDate" runat="server" />
                            </td>
                            <td align="right">
                                <strong>Due Date:</strong>
                            </td>
                            <td align="left">
                                <BizCalendar:Calendar ID="calADueDate" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <strong>Description:</strong>
                            </td>
                            <td colspan="3" align="left">
                                <asp:TextBox ID="txtADescription" runat="server" TextMode="MultiLine" Columns="30"
                                    CssClass="signup"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </asp:View>
            </asp:MultiView>
        </asp:TableCell></asp:TableRow>
</asp:Table>
<p>
</p>
