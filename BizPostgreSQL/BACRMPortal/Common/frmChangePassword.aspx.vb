Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Partial Class frmChangePassword
    Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnCancel As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Session("UserContactID") = Nothing Then Response.Redirect("../Common/frmLogout.aspx")
            If Not IsPostBack Then
                If Session("Logo") = "" Then
                    imgLogo.Src = "../Documents/Docs/" & Session("DomainID") & "/" & Session("Logo")
                Else : imgLogo.Visible = False
                End If
            End If
            btnSubmit.Attributes.Add("onclick", "return Submit()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            Dim objUserAccess As New UserAccess
            objUserAccess.ContactID = Session("UserContactID")
            objUserAccess.DivisionID = Session("DivId")
            objUserAccess.Password = txtOldPassword.Text.Trim
            objUserAccess.NewPassword = txtNewPassword.Text.Trim
            objUserAccess.DomainID = Session("DomainID")

            If objUserAccess.ChangePassword = 1 Then
                litMessage.Text = "Password has been successfully changed !"
            Else : litMessage.Text = "You're not authorized to change the password !"
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class
