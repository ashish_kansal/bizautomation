Imports BACRM.BusinessLogic.Survey
Imports BACRM.BusinessLogic.Admin
Imports System.IO
Imports System.Math
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Tracking
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Workflow

Namespace BACRM.UserInterface.Survey
    Public Class frmGenericFormSurvey
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.ID = "frmGenericFormSurvey1" 'Renamed by chintan ,Multiple controls with the same ID 'frmGenericFormSurvey' were found. Trace requires that controls have unique IDs.

        End Sub
        Protected WithEvents plhFormControls As System.Web.UI.WebControls.PlaceHolder
        Protected WithEvents litSurveyName As System.Web.UI.WebControls.Literal
        Protected WithEvents hdSurId As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdOnlyRegistration As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents btnContinue As System.Web.UI.WebControls.Button
        Protected WithEvents tblNavgationControls As System.Web.UI.HtmlControls.HtmlTable
        Protected WithEvents litClientScript As System.Web.UI.WebControls.Literal
        Protected WithEvents ValidationSummary As System.Web.UI.WebControls.ValidationSummary
        Protected WithEvents btnExistingUsers As System.Web.UI.WebControls.Button
        Protected WithEvents tblAlreadyRegisteredUser As System.Web.UI.WebControls.Table
        Protected WithEvents txtExistingContactEmail As System.Web.UI.WebControls.TextBox
        Private designerPlaceholderDeclaration As System.Object
        Protected WithEvents btnSignUp As System.Web.UI.WebControls.Button
        Private numDomainId As Integer
        Dim boolPostRegistration As Boolean
        Dim dtGenericFormConfig As DataTable
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region



        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Trim(GetQueryStringVal("D")) <> "" Then
                    numDomainId = CCommon.ToInteger(GetQueryStringVal("D"))                                          'Get the Domain Id from the url
                    Dim numSurId As Integer                                                         'Declare the integer for the Survey Id
                    numSurId = GetQueryStringVal("numSurId")                                      'Pick up the Survey Id from the Query String
                    hdSurId.Value = numSurId                                                        'Set the Sruvey Id in local variable


                    Dim cOnlyRegistration As Char                                                   'Declare the integer for the Survey Id
                    If GetQueryStringVal("boolOnlyRegistration") = "" Then
                        cOnlyRegistration = "N"                                                     'Display the entire survey
                    Else : cOnlyRegistration = GetQueryStringVal("boolOnlyRegistration")             'Pick up the flag which denotes if the survey is to be diosplayed completely
                    End If
                    If Trim(GetQueryStringVal("bitPostRegistration")) = "" Then
                        boolPostRegistration = False
                    Else : boolPostRegistration = True
                    End If
                    If cOnlyRegistration = "Y" Then
                        Response.Redirect("frmSurveyExecutionSurvey.aspx?numSurId=" & numSurId & "&numRespondantID=0&D=" & numDomainId, True) 'Redirect to the Survey
                    End If
                    hdOnlyRegistration.Value = cOnlyRegistration                                    'Set the flag in local variable
                    'Put user code to initialize the page here
                    callFuncForFormGenerationNonAOI()

                    Dim objSurvey As New SurveyAdministration                               'Create an object of Survey Administration Class
                    objSurvey.DomainID = numDomainId
                    objSurvey.SurveyId = hdSurId.Value
                    objSurvey.PageType = 1
                    Dim dsPage As DataSet
                    dsPage = objSurvey.GetSurveyTemplate()
                    Dim dt As DataTable = dsPage.Tables(0)

                    If dsPage.Tables(0).Rows.Count > 0 Then

                        Dim length1 As Integer = dt.Rows(0)("txtTemplate").ToString().IndexOf("#Form#")

                        lblHeader.Text = HttpUtility.HtmlDecode(CCommon.ToString(dt.Rows(0)("txtTemplate").ToString().Substring(0, length1)))
                        lblFooter.Text = HttpUtility.HtmlDecode(CCommon.ToString(dt.Rows(0)("txtTemplate").ToString().Substring(length1 + 6)))

                        If dsPage.Tables(1).Rows.Count > 0 Then
                            MasterCSS.Visible = False
                            For Each dr As DataRow In dsPage.Tables(1).Rows
                                CCommon.AddStyleSheetLink("../Documents/Docs/" & numDomainId & "/Survey/" & dr("StyleFileName").ToString, Me.Page)
                            Next
                        Else
                            MasterCSS.Visible = True
                        End If

                        If dsPage.Tables(2).Rows.Count > 0 Then
                            For Each dr As DataRow In dsPage.Tables(2).Rows
                                CCommon.AddJavascriptLink("../Documents/Docs/" & numDomainId & "/Survey/" & dr("StyleFileName").ToString, Me.Page)
                            Next
                        End If
                    End If

                    If Not IsPostBack Then callFunctionForExtraControlBinding()
                    'set focus to first empty textbox
                    If ClientScript.IsStartupScriptRegistered("setfocus") = False Then ClientScript.RegisterStartupScript(Me.GetType, "setfocus", "SetFocusOnFirstEmptyTextBoxControl();", True)
                Else
                    plhFormControls.Controls.Add(New LiteralControl("The screen is not created. Please use BizForm Wizard to create this screen.")) 'calls to create the form controls and add it to the form
                    btnSignUp.Enabled = False                                                       'Disable any attempt to save/ submit
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is called to initiate the generation of the dynamic form.
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        '''-----------------------------------------------------------------------------
        Sub callFuncForFormGenerationNonAOI()
            Try
                plhFormControls.Controls.Clear()
                GenerateGenericFormControls.FormID = 5                                            'set the form id to Survey Form
                GenerateGenericFormControls.DomainID = numDomainId                                'Set the domain id
                GenerateGenericFormControls.AuthGroupId = hdSurId.Value

                GenerateGenericFormControls.sXMLFilePath = Server.MapPath("") & "\..\Documents\Docs\" 'set the file path
                GenerateGenericFormControls.boolAOIField = 0                                      'Set the AOI flag to non AOI
                dtGenericFormConfig = GenerateGenericFormControls.getFormControlConfig() 'get the datatable to contain the form config
                If dtGenericFormConfig.Rows.Count = 0 Then                                          'if the xml for form fields has not been configured
                    btnContinue.Enabled = False                                                     'Disable any attempt to save/ submit
                    plhFormControls.Controls.Add(New LiteralControl("The screen is not created. Please use BizForm Wizard to create this screen.")) 'calls to create the form controls and add it to the form
                    Exit Sub                                                                   'and exit the flow
                End If
                Dim objLeadBoxData As New ImplementLeadsAutoRoutingRules                            'Create an object of class which encaptulates the functionaltiy
                objLeadBoxData.DomainID = numDomainId                                               'set the domain id
                Dim numRoutId As Integer = objLeadBoxData.FetchDefaultRuleId()                                     'Get the Default Rule Id
                If numRoutId = 0 Then                                                               'if the xml for form fields has not been configured
                    btnContinue.Enabled = False                                                     'Disable any attempt to save/ submit
                    plhFormControls.Controls.Add(New LiteralControl("Auto Rules are not created, please contact the Administrator.")) 'calls to create the form controls and add it to the form
                    Exit Sub                                                                        'and exit the flow
                End If
                plhFormControls.Controls.Add(GenerateGenericFormControls.createFormControls(dtGenericFormConfig)) 'calls to create the form controls and add it to the form
                litClientScript.Text &= vbCrLf & GenerateGenericFormControls.getJavascriptArray(dtGenericFormConfig) 'Create teh javascript array and store
                Dim dvConfig As DataView
                dvConfig = New DataView(dtGenericFormConfig)
                dvConfig.RowFilter = " vcDbColumnName like '%Country'"
                If dvConfig.Count > 0 Then
                    Dim i As Integer
                    For i = 0 To dvConfig.Count - 1
                        Dim dl As DropDownList
                        If dvConfig(i).Item("vcDbColumnName") = "vcBillCountry" Then
                            If Not CType(plhFormControls.FindControl("vcBilState"), DropDownList) Is Nothing Then

                                dl = CType(plhFormControls.FindControl(dvConfig(i).Item("vcDbColumnName")), DropDownList)
                                dl.AutoPostBack = True
                                AddHandler dl.SelectedIndexChanged, AddressOf FillStateDyn
                            End If
                        Else
                            If Not CType(plhFormControls.FindControl(Replace(dvConfig(i).Item("vcDbColumnName"), "Country", "State")), DropDownList) Is Nothing Then

                                dl = CType(plhFormControls.FindControl(dvConfig(i).Item("vcDbColumnName")), DropDownList)
                                dl.AutoPostBack = True
                                AddHandler dl.SelectedIndexChanged, AddressOf FillStateDyn
                            End If
                        End If
                        If Not IsPostBack Then
                            Dim dtTable As DataTable
                            Dim objUserAccess As New UserAccess
                            objUserAccess.DomainID = numDomainId
                            dtTable = objUserAccess.GetDomainDetails()
                            If Not dl.Items.FindByValue(dtTable.Rows(0).Item("numDefCountry")) Is Nothing Then
                                dl.Items.FindByValue(dtTable.Rows(0).Item("numDefCountry")).Selected = True
                                If dl.ID = "vcBillCountry" Then
                                    FillState(CType(plhFormControls.FindControl("vcBilState"), DropDownList), dl.SelectedItem.Value, numDomainId)
                                Else
                                    FillState(CType(plhFormControls.FindControl(Replace(dl.ID, "Country", "State")), DropDownList), dl.SelectedItem.Value, numDomainId)
                                End If
                            End If
                        End If
                    Next
                End If
                'Set control's default value from query string ,so one can submit data through query and pre populate form
                For index As Integer = 0 To dtGenericFormConfig.Rows.Count - 1
                    Dim control As Control = plhFormControls.FindControl(dtGenericFormConfig.Rows(index)("vcDbColumnName"))
                    If Not control Is Nothing Then
                        Select Case dtGenericFormConfig.Rows(index)("vcAssociatedControlType").ToString
                            Case "CheckBox", "Check box"
                                CType(control, CheckBox).Checked = CCommon.ToBool(Request(dtGenericFormConfig.Rows(index)("vcDbColumnName")))
                            Case "TextArea", "TextBox"
                                CType(control, TextBox).Text = CCommon.ToString(Request(dtGenericFormConfig.Rows(index)("vcDbColumnName")))
                            Case "RadioBox"
                                CType(control, RadioButtonList).SelectedValue = CCommon.ToString(Request(dtGenericFormConfig.Rows(index)("vcDbColumnName")))
                            Case "SelectBox"
                                CType(control, DropDownList).SelectedValue = CCommon.ToString(Request(dtGenericFormConfig.Rows(index)("vcDbColumnName")))
                        End Select
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub FillStateDyn(ByVal sender As Object, ByVal e As EventArgs)
            Try
                If sender.ID = "vcBillCountry" Then
                    FillState(CType(plhFormControls.FindControl("vcBilState"), DropDownList), sender.SelectedItem.Value, numDomainId)
                Else : FillState(CType(plhFormControls.FindControl(Replace(sender.ID, "Country", "State")), DropDownList), sender.SelectedItem.Value, numDomainId)
                End If
                'Response.Write(sender.ID)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is called to building of not dynamic controls.
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        '''-----------------------------------------------------------------------------
        Sub callFunctionForExtraControlBinding()
            Try
                If hdSurId.Value > 0 Then                                               'Form is opened for a particular survey only
                    Dim objSurvey As New SurveyAdministration                           'Create an object of Survey Administration Class
                    objSurvey.SurveyId = hdSurId.Value                                  'Set the Survey ID
                    objSurvey.DomainID = numDomainId                                    'Set the Domain ID
                    Dim dtSurveyMasterInfo As DataTable                                 'Declare a Datatable
                    dtSurveyMasterInfo = objSurvey.getSurveyMasterData()                'Get the Survey Master information in a datatable
                    If dtSurveyMasterInfo.Rows(0).Item("bitSkipRegistration") = True And hdOnlyRegistration.Value = "N" Then
                        Response.Redirect("frmSurveyExecutionSurvey.aspx?numSurId=" & hdSurId.Value & "&numRespondantID=0&D=" & numDomainId, True)
                    End If
                    litSurveyName.Text = dtSurveyMasterInfo.Rows(0).Item("vcSurName")   'Display the name of the survey
                    If hdOnlyRegistration.Value = "Y" Then                              'If only Registration Screen is to be displayed
                        tblNavgationControls.Visible = False                            'Hide the Continue and the Back button
                    Else
                        Dim objRegtable As Object                                       'The generic object
                        objRegtable = Page.FindControl("tblProspects")     'Get a holder to the table object
                        tblProspects.Attributes.Add("display", "inline")
                        'CType(objRegtable, HtmlTable).Attributes.Add("style", "display:inline;") 'Add attribute
                        If dtSurveyMasterInfo.Rows(0).Item("bitPostRegistration") Then
                            btnContinue.Text = "Register and Conclude Survey"
                        End If
                        btnExistingUsers.Attributes.Add("onclick", "javascript: return EnableAlreadyRegisteredUsers(this);") 'Allow pre-registered users to proceed to survey
                        btnSignUp.Attributes.Add("onclick", "javascript: return CheckIfEmailIsEntered();") 'Check for Email
                    End If
                Else
                    litSurveyName.Text = "Survey Registration"                          'Not specific to any particular survey to no name
                    tblNavgationControls.Visible = False                                'Hide the Continue and the Back button
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Function GetCustFldValue(ByVal fldType As String, ByVal fld_Id As String) As String
            Try
                If fldType = "TextArea" Or fldType = "TextBox" Then
                    Dim txt As TextBox
                    txt = Page.FindControl(fld_Id)
                    Return txt.Text
                ElseIf fldType = "SelectBox" Then
                    Dim ddl As DropDownList
                    ddl = Page.FindControl(fld_Id)
                    Return CStr(IIf(ddl.SelectedItem.Value = "", 0, ddl.SelectedItem.Value))
                Else : Return ""
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This event is fired when the user clicks to proceed to the actual survey
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/23/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub btnContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnContinue.Click
            Try
                Dim numSurveyRespondentsID As Integer                                   'Declare a variable for the Survey Respondents Id
                Dim objSurvey As New SurveyAdministration                               'Create an object of Survey Administration Class
                objSurvey.SurveyId = hdSurId.Value                                      'Set the Survey ID
                objSurvey.DomainID = numDomainId                                        'Set the Domain Id

                Dim boolSurveyRedirect, bitLandingPage, bitEmailTemplateContact, bitEmailTemplateRecordOwner As Boolean
                Dim numEmailTemplate1Id, numEmailTemplate2Id As Int32
                Dim strRedirect As String = ""

                If hdSurId.Value > 0 Then                                               'Form is opened for a particular survey only
                    Dim dtSurveyMasterInfo As DataTable                                 'Declare a Datatable
                    dtSurveyMasterInfo = objSurvey.getSurveyMasterData()                'Get the Survey Master information in a datatable
                    boolSurveyRedirect = dtSurveyMasterInfo.Rows(0).Item("bitSurveyRedirect")
                    strRedirect = CCommon.ToString(dtSurveyMasterInfo.Rows(0).Item("vcSurveyRedirect"))
                    bitLandingPage = dtSurveyMasterInfo.Rows(0).Item("bitLandingPage")
                    bitEmailTemplateContact = dtSurveyMasterInfo.Rows(0).Item("bitEmailTemplateContact")
                    bitEmailTemplateRecordOwner = dtSurveyMasterInfo.Rows(0).Item("bitEmailTemplateRecordOwner")

                    numEmailTemplate1Id = dtSurveyMasterInfo.Rows(0).Item("numEmailTemplate1Id")
                    numEmailTemplate2Id = dtSurveyMasterInfo.Rows(0).Item("numEmailTemplate2Id")

                    If (dtSurveyMasterInfo.Rows(0).Item("bitRelationshipProfile") = True) Then
                        hdRelationShip.Value = IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("numRelationShipId")), 0, dtSurveyMasterInfo.Rows(0).Item("numRelationShipId"))
                        hdCRMType.Value = IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("tIntCRMType")), 0, dtSurveyMasterInfo.Rows(0).Item("tIntCRMType"))
                        hdGroupId.Value = IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("numGrpId")), 1, dtSurveyMasterInfo.Rows(0).Item("numGrpId"))
                        hdRecOwner.Value = IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("numRecOwner")), 0, dtSurveyMasterInfo.Rows(0).Item("numRecOwner"))
                        hdProfile.Value = IIf(IsDBNull(dtSurveyMasterInfo.Rows(0).Item("numProfileId")), 0, dtSurveyMasterInfo.Rows(0).Item("numProfileId"))

                        hdCreateDBEntry.Value = 1
                    End If
                End If

                If boolSurveyRedirect = False Then
                    dtGenericFormConfig.Columns.Add("vcDbColumnText")
                    Dim vcAssociatedControlType As String
                    Dim vcFieldType As String
                    For Each dr As DataRow In dtGenericFormConfig.Rows
                        vcFieldType = dr("vcFieldType")
                        If vcFieldType = "R" Then
                            vcAssociatedControlType = dr("vcAssociatedControlType")
                            Select Case vcAssociatedControlType
                                Case "TextBox", "TextArea" : dr("vcDbColumnText") = CType(plhFormControls.FindControl(dr("vcDbColumnName")), TextBox).Text
                                Case "SelectBox"
                                    If dr("vcDbColumnName").ToString.ToLower.Contains("country") Then
                                        Dim objBusinessClass As New BusinessClass
                                        objBusinessClass.IPNo = objBusinessClass.GetIPNumberFromIPAddress(Request.UserHostAddress)
                                        Dim importWiz As New ImportWizard
                                        Dim lngCountryID As Long = importWiz.GetStateAndCountry(0, objBusinessClass.GetCountryFromIPaddress(ConfigurationManager.AppSettings("ConnectionString")), numDomainId)
                                        If lngCountryID > 0 Then
                                            dr("vcDbColumnText") = lngCountryID
                                        Else
                                            dr("vcDbColumnText") = CType(plhFormControls.FindControl(dr("vcDbColumnName")), DropDownList).SelectedItem.Value
                                        End If
                                    Else
                                        dr("vcDbColumnText") = CType(plhFormControls.FindControl(dr("vcDbColumnName")), DropDownList).SelectedItem.Value
                                    End If
                            End Select
                        ElseIf vcFieldType = "C" Then
                            dr("vcDbColumnText") = GetCustFldValue(dr("vcAssociatedControlType"), dr("vcDBColumnName"))
                        ElseIf vcFieldType = "D" Then
                            dr("vcDbColumnText") = GetCustFldValue(dr("vcAssociatedControlType"), dr("vcDBColumnName"))
                        End If
                    Next
                    If Not plhFormControls.FindControl("vcEmail") Is Nothing Then
                        Dim objLeads As New CLeads
                        objLeads.Email = CType(plhFormControls.FindControl("vcEmail"), TextBox).Text
                        objLeads.DomainID = numDomainId
                        If ValidateIfEmailExists(objLeads) = False Then Exit Sub
                    End If
                    Session("dtSurveyRegDTL") = dtGenericFormConfig
                Else

                    If hdCreateDBEntry.Value = 1 Then
                        Dim objLeads As New CLeads

                        '' Added By Anoop 
                        Dim ds As New DataSet
                        Dim dRow As DataRow
                        dtGenericFormConfig.Columns.Add("vcDbColumnText")
                        objLeads.GroupID = hdGroupId.Value
                        objLeads.ContactType = 70
                        objLeads.PrimaryContact = True

                        objLeads.CRMType = hdCRMType.Value
                        objLeads.CompanyType = hdRelationShip.Value
                        objLeads.DivisionName = "-"
                        Dim vcAssociatedControlType As String
                        Dim objBusinessClass As New BusinessClass
                        Dim importWiz As New ImportWizard

                        Dim vcFieldType As String
                        For Each dr As DataRow In dtGenericFormConfig.Rows
                            vcFieldType = dr("vcFieldType")
                            If vcFieldType = "R" Then
                                vcAssociatedControlType = dr("vcAssociatedControlType")
                                Select Case vcAssociatedControlType
                                    Case "TextBox"
                                        dr("vcDbColumnText") = CType(plhFormControls.FindControl(dr("vcDbColumnName")), TextBox).Text

                                        If dr("vcDbColumnText") <> "" Then
                                            AssignValuesEditBox(objLeads, dr("vcDbColumnText"), dr("vcDbColumnName"))
                                        End If
                                    Case "SelectBox"
                                        If dr("vcDbColumnName").ToString.ToLower.Contains("country") Then
                                            objBusinessClass.IPNo = objBusinessClass.GetIPNumberFromIPAddress(Request.UserHostAddress)
                                            Dim lngCountryID1 As Long = importWiz.GetStateAndCountry(0, objBusinessClass.GetCountryFromIPaddress(ConfigurationManager.AppSettings("ConnectionString")), numDomainId)
                                            If lngCountryID1 > 0 Then
                                                dr("vcDbColumnText") = lngCountryID1
                                            Else
                                                dr("vcDbColumnText") = CType(plhFormControls.FindControl(dr("vcDbColumnName")), DropDownList).SelectedItem.Value
                                            End If
                                        Else
                                            dr("vcDbColumnText") = CType(plhFormControls.FindControl(dr("vcDbColumnName")), DropDownList).SelectedItem.Value
                                        End If

                                        If dr("vcDbColumnText") <> "" Then
                                            AssignValuesSelectBox(objLeads, dr("vcDbColumnText"), dr("vcDbColumnName"))
                                        Else
                                            dr("vcDbColumnText") = 0
                                        End If
                                    Case "TextArea"
                                        dr("vcDbColumnText") = CType(plhFormControls.FindControl(dr("vcDbColumnName")), TextBox).Text

                                        If dr("vcDbColumnText") <> "" Then
                                            AssignValuesTextBox(objLeads, dr("vcDbColumnText"), dr("vcDbColumnName"))
                                        End If
                                End Select
                                'ElseIf vcFieldType = "C" Then
                                '    dr("vcDbColumnText") = GetCustFldValue(dr("vcAssociatedControlType"), dr("vcDBColumnName"))
                                'ElseIf vcFieldType = "D" Then
                                '    dr("vcDbColumnText") = GetCustFldValue(dr("vcAssociatedControlType"), dr("vcDBColumnName"))
                            End If
                        Next
                        objLeads.DomainID = numDomainId

                        objBusinessClass.IPNo = objBusinessClass.GetIPNumberFromIPAddress(Request.UserHostAddress)
                        Dim lngCountryID As Long = importWiz.GetStateAndCountry(0, objBusinessClass.GetCountryFromIPaddress(ConfigurationManager.AppSettings("ConnectionString")), numDomainId)
                        If lngCountryID > 0 Then
                            objLeads.Country = lngCountryID
                        End If

                        If hdRecOwner.Value = "" Then
                            hdRecOwner.Value = 0
                        End If
                        If hdRecOwner.Value = 0 Then
                            Dim objAutoRoutRles As New AutoRoutingRules
                            objAutoRoutRles.DomainID = numDomainId
                            dtGenericFormConfig.TableName = "Table"
                            ds.Tables.Add(dtGenericFormConfig.Copy)
                            objAutoRoutRles.strValues = ds.GetXml
                            ds.Tables.Remove(ds.Tables(0))
                            hdRecOwner.Value = objAutoRoutRles.GetRecordOwner
                        End If

                        Dim lnDivID, lngCMPID, lnCntID, lngRecOwner As Long

                        objLeads.UserCntID = hdRecOwner.Value
                        objLeads.AssignedTo = hdRecOwner.Value
                        objLeads.Profile = hdProfile.Value

                        'Validate Email exist
                        If ValidateIfEmailExists(objLeads) = False Then Exit Sub

                        'Create Lead Record
                        objLeads.CompanyID = objLeads.CreateRecordCompanyInfo()
                        objLeads.DivisionID = objLeads.CreateRecordDivisionsInfo

                        lnDivID = objLeads.DivisionID
                        lngRecOwner = objLeads.UserCntID
                        lnCntID = objLeads.CreateRecordAddContactInfo()
                        lngCMPID = objLeads.CompanyID

                        Session("DivID") = lnDivID 'Addded by chintan reason: will be used in tracking leads in marketing section
                        'add cookie to browser of Division id which can be used to track future visit of same user
                        CCommon.AddDivisionCookieToBrowser(lnDivID)
                        'Session("RecOwner") = lngRecOwner
                        'Session("UserContactID") = lnCntID
                        'Session("CompID") = lngCMPID

                        Dim dsViews As New DataView(dtGenericFormConfig)
                        dsViews.RowFilter = "vcFieldType='C'"
                        Dim i As Integer

                        Dim dtCusTable As New DataTable
                        dtCusTable.Columns.Add("FldDTLID")
                        dtCusTable.Columns.Add("fld_id")
                        dtCusTable.Columns.Add("Value")
                        Dim strdetails As String

                        If dsViews.Count > 0 Then
                            For i = 0 To dsViews.Count - 1
                                dRow = dtCusTable.NewRow
                                dRow("FldDTLID") = 0
                                dRow("fld_id") = Replace(dsViews(i).Item("numFormFieldId"), "C", "")
                                dRow("Value") = dsViews(i).Item("vcDbColumnText")
                                dtCusTable.Rows.Add(dRow)
                            Next

                            dtCusTable.TableName = "Table"
                            ds.Tables.Add(dtCusTable.Copy)
                            strdetails = ds.GetXml
                            ds.Tables.Remove(ds.Tables(0))

                            Dim ObjCusfld As New CustomFields
                            ObjCusfld.strDetails = strdetails
                            ObjCusfld.RecordId = objLeads.ContactID
                            ObjCusfld.locId = 4
                            ObjCusfld.SaveCustomFldsByRecId()
                        End If
                        dsViews.RowFilter = "vcFieldType='D'"
                        dtCusTable.Rows.Clear()
                        If dsViews.Count > 0 Then
                            For i = 0 To dsViews.Count - 1
                                dRow = dtCusTable.NewRow
                                dRow("FldDTLID") = 0
                                dRow("fld_id") = Replace(dsViews(i).Item("numFormFieldId"), "C", "")
                                dRow("Value") = dsViews(i).Item("vcDbColumnText")
                                dtCusTable.Rows.Add(dRow)
                            Next

                            dtCusTable.TableName = "Table"
                            ds.Tables.Add(dtCusTable.Copy)
                            strdetails = ds.GetXml
                            ds.Tables.Remove(ds.Tables(0))

                            Dim ObjCusfld As New CustomFields
                            ObjCusfld.strDetails = strdetails
                            ObjCusfld.RecordId = objLeads.DivisionID
                            ObjCusfld.locId = 1
                            ObjCusfld.SaveCustomFldsByRecId()
                        End If

                        'Added By Sachin Sadhu||Date:25thAug2014
                        'Purpose :To Added Organization data in work Flow queue based on created Rules
                        '          Using Change tracking
                        Dim objWfA As New Workflow()
                        objWfA.DomainID = numDomainId
                        objWfA.UserCntID = Session("UserContactID")
                        objWfA.RecordID = lnDivID
                        objWfA.SaveWFOrganizationQueue()
                        'ss//end of code

                        'Purpose :To Added Contact data in work Flow queue based on created Rules
                        '         Using Change tracking
                        Dim objWF As New Workflow()
                        objWF.DomainID = numDomainId
                        objWF.UserCntID = Session("UserContactID")
                        objWF.RecordID = lnCntID
                        objWF.SaveWFContactQueue()
                        ' ss//end of code
                        Session("SMTPServerIntegration") = True

                        If bitEmailTemplateContact Then
                            If objLeads.Email IsNot Nothing Then
                                If objLeads.Email.Length > 0 Then
                                    'Commented by :Sachin Sadhu||Date:25thSept2014
                                    'Purpose : To Replace with WFA(part of Email Alerts)
                                    ' CAlerts.SendWebLeadAlerts(numEmailTemplate1Id, lnDivID, lnCntID, lnCntID, lngRecOwner, numDomainId) 'Alert DTL ID for sending alerts in opportunities
                                    'end of code
                                End If

                            End If
                        End If

                        If bitEmailTemplateRecordOwner Then
                            'Commented by :Sachin Sadhu||Date:25thSept2014
                            'Purpose : To Replace with WFA(part of Email Alerts)
                            ' CAlerts.SendWebLeadAlerts(numEmailTemplate2Id, lnDivID, lnCntID, lngRecOwner, lngRecOwner, numDomainId)
                            'end of code
                        End If
                    End If
                End If
                'Ip to country , attach Billing country by default from ip address
                'Dim objBusinessClass As New BusinessClass
                'objBusinessClass.IPNo = objBusinessClass.GetIPNumberFromIPAddress(Request.UserHostAddress)
                'Dim importWiz As New ImportWizard
                'objSurvey.co = importWiz.GetStateAndCountry(0, objBusinessClass.GetCountryFromIPaddress(ConfigurationManager.AppSettings("ConnectionString")), numDomainId)

                numSurveyRespondentsID = objSurvey.SaveSurveyRespondentInformationInDatabase("") 'Call to Save the Survey Respondents Information in teh database and retrieves the ID of the Respondent
                If numSurveyRespondentsID <> 0 Then

                    If boolPostRegistration Then
                        litClientScript.Text &= "<script language='javascript'>RegistrationCompleted(" & numSurveyRespondentsID & ");</script>"
                    ElseIf boolSurveyRedirect = True And strRedirect.Length > 0 Then
                        Response.Redirect(strRedirect)
                    ElseIf bitLandingPage Then
                        Response.Redirect("frmLandingPage.aspx?numSurId=" & objSurvey.SurveyId & "&numRespondantID=" & numSurveyRespondentsID & "&D=" & numDomainId, True)
                    Else
                        Response.Redirect("frmSurveyExecutionSurvey.aspx?numSurId=" & objSurvey.SurveyId & "&numRespondantID=" & numSurveyRespondentsID & "&D=" & numDomainId, True) 'Redirect to the Survey
                    End If
                Else : litClientScript.Text &= "<script language='javascript'>alert('An error occurrred while saving your information. Please try again or else contact the Administrator.')</script>"
                End If
            Catch ex As Exception
                litClientScript.Text &= "<script language='javascript'>alert('An error occurrred while saving your information. Please try again or else contact the Administrator.')</script>"
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This event is fired when the user clicks to SignUp as a pre-registered user
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/29/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub btnSignUp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSignUp.Click
            Try
                Dim numSurveyRespondentsID As Integer                                   'Declare a variable for the Survey Respondents Id
                Dim objSurvey As New SurveyAdministration                               'Create an object of Survey Administration Class
                objSurvey.SurveyId = hdSurId.Value                                      'Set the Survey ID
                objSurvey.DomainID = numDomainId                                        'Set the Domain ID
                numSurveyRespondentsID = CInt(objSurvey.CheckRegisteredUserAndSaveInformationInDatabase(txtExistingContactEmail.Text)) 'Call to Save the Survey Respondents Information in teh database and retrieves the ID of the Respondent
                If numSurveyRespondentsID <> 0 Then
                    Dim boolSurveyRedirect As Boolean
                    Dim strRedirect As String = ""

                    If hdSurId.Value > 0 Then                                               'Form is opened for a particular survey only
                        Dim dtSurveyMasterInfo As DataTable                                 'Declare a Datatable
                        dtSurveyMasterInfo = objSurvey.getSurveyMasterData()                'Get the Survey Master information in a datatable
                        boolSurveyRedirect = dtSurveyMasterInfo.Rows(0).Item("bitSurveyRedirect")
                        strRedirect = dtSurveyMasterInfo.Rows(0).Item("vcSurveyRedirect")
                    End If

                    If boolPostRegistration Then
                        litClientScript.Text &= "<script language='javascript'>RegistrationCompleted(" & numSurveyRespondentsID & ");</script>"
                    ElseIf boolSurveyRedirect = True Then
                        Response.Redirect(strRedirect)
                    Else : Response.Redirect("frmSurveyExecutionSurvey.aspx?numSurId=" & hdSurId.Value & "&numRespondantID=" & numSurveyRespondentsID & "&D=" & numDomainId, True) 'Redirect to the Survey
                    End If
                Else
                    litClientScript.Text = "<script language='javascript'>alert('We are sorry but we could not find your email in our database, please register with us before proceeding to the survey.')</script>"
                    callFuncForFormGenerationNonAOI()                                           'Calls function for form generation and display for non AOI fields
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        'Sub SendAlerts(ByVal lngAlertDtlID As Long, ByVal lnDivID As Long, ByVal lnCntID As Long, ByVal lngUserContactID As Long, Optional ByVal lngRecordOwner As Long = 0)
        '    Try
        '        Dim dtEmailTemplate As DataTable
        '        Dim objDocuments As New DocumentList
        '        objDocuments.GenDocID = lngAlertDtlID
        '        objDocuments.DomainID = numDomainId
        '        dtEmailTemplate = objDocuments.GetDocByGenDocID
        '        If dtEmailTemplate.Rows.Count > 0 Then
        '            Dim objProspects As New CProspects
        '            Dim dtComInfo As DataTable
        '            Dim dtConInfo As DataTable
        '            objProspects.DivisionID = lnDivID
        '            objProspects.DomainID = numDomainId
        '            objProspects.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
        '            dtComInfo = objProspects.GetCompanyInfoForEdit

        '            Dim objContacts As New CContacts
        '            objContacts.ContactID = lnCntID
        '            objContacts.DomainID = numDomainId
        '            objContacts.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
        '            dtConInfo = objContacts.GetCntInfoForEdit1

        '            Dim objSendMail As New Email
        '            Dim dtMergeFields As New DataTable
        '            Dim drNew As DataRow
        '            dtMergeFields.Columns.Add("Assignee")
        '            dtMergeFields.Columns.Add("Organization")
        '            dtMergeFields.Columns.Add("vcFirstName")
        '            dtMergeFields.Columns.Add("vcLastName")
        '            dtMergeFields.Columns.Add("Phone")
        '            dtMergeFields.Columns.Add("Email")
        '            If dtConInfo.Rows.Count > 0 Then    ' Added this  code which prevents send alert in case the contact is repeated and to avoid error
        '                drNew = dtMergeFields.NewRow
        '                drNew("Organization") = dtComInfo.Rows(0).Item("vcCompanyName")
        '                drNew("Assignee") = dtComInfo.Rows(0).Item("vcCreatedBy")
        '                drNew("vcFirstName") = dtConInfo.Rows(0).Item("vcFirstName")
        '                drNew("vcLastName") = dtConInfo.Rows(0).Item("vcLastName")
        '                drNew("Phone") = dtConInfo.Rows(0).Item("numPhone")
        '                drNew("Email") = dtConInfo.Rows(0).Item("vcEmail")
        '                dtMergeFields.Rows.Add(drNew)

        '                Dim objCommon As New CCommon
        '                objCommon.ContactID = lngUserContactID
        '                objCommon.byteMode = 0
        '                If lngRecordOwner > 0 Then
        '                    Dim objCommon2 As New CCommon
        '                    objCommon2.ContactID = lngRecordOwner
        '                    Dim strFrmEmail As String = objCommon2.GetContactsEmail
        '                    objSendMail.AddEmailToJob(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), "", strFrmEmail, objCommon.GetContactsEmail, dtMergeFields, "", "", numDomainId)
        '                Else
        '                    objSendMail.AddEmailToJob(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), "", ConfigurationManager.AppSettings("FromAddress"), objCommon.GetContactsEmail, dtMergeFields, "", "", numDomainId)
        '                End If

        '            End If
        '        End If
        '    Catch ex As Exception
        '        Throw ex
        '    End Try

        'End Sub
        Function ValidateIfEmailExists(ByRef objLeads As CLeads) As Boolean
            Try
                If CCommon.ToString(objLeads.Email).Length > 0 Then
                    objLeads.GetConIDCompIDDivIDFromEmail()
                    If objLeads.DivisionID > 0 Then
                        litClientScript.Text = "<script>alert('An account with that email address already exists. Please contact our sales department at (888)-407-4781 ext 1');</script>"
                        Return False
                    End If
                End If
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace
