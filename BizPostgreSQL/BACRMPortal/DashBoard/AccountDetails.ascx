﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AccountDetails.ascx.vb"
    Inherits="BACRMPortal.AccountDetails" %>
<script type="text/javascript">
    function OpenCompanyDetails() {
        window.open('../Contact/frmEditCompanyDetails.aspx', '', 'toolbar=0,titlebar=0,menubar=0,location=1,left=100,top=50,width=750,height=500,scrollbars=yes,resizable=yes');
        return false;
    }

    function OpenChangePassword() {
        window.open('../Common/frmChangePassword.aspx', '', 'toolbar=0,titlebar=0,menubar=0,location=1,left=100,top=50,width=750,height=300,scrollbars=yes,resizable=yes');
        return false;
    }
</script>
<div class="portlet" id="10">
    <div class="portlet-header">
        Update my account details
    </div>
    <%--<table cellpadding="0" cellspacing="0">
    <tr>
        <td class="TabLeft">
            Update my account details
        </td>
        <td class="TabRight">
        </td>
    </tr>
</table>--%>
    <div class="portlet-content">
        <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="0" Height="100"
            runat="server" Width="450" BorderColor="black" GridLines="None">
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Top">
                    </br>
                <table>
                    <tr>
                        <td class="normal1" align="left">
                            <asp:HyperLink ID="hplEditCompany" runat="server" Text="Edit my company’s details"
                                CssClass="hyperlink" onclick="javascript:OpenCompanyDetails();" />
                        </td>
                        <td>
                            <asp:HyperLink ID="hplChangePassword" runat="server" Text="Change password"
                                CssClass="hyperlink" onclick="javascript:OpenChangePassword();" />
                        </td>
                    </tr>
                    <%--<tr>
                        <td class="normal1" align="left">
                            <asp:HyperLink ID="hplEditContact" runat="server" Text="Edit my company’s contact details"
                                CssClass="hyperlink" />
                        </td>
                        <td>
                        </td>
                    </tr>--%>
                </table>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>
</div>
