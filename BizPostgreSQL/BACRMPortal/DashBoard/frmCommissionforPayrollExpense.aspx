﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCommissionforPayrollExpense.aspx.vb"
    Inherits="BACRMPortal.frmCommissionforPayrollExpense1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Commission List</title>
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <script language="javascript" type="text/javascript">
        function Close() {
            window.close();
            return false;
        }
        function OpenBizInvoice(a, b) {
            window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            return false;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server" method="post">
    <br />
    <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td valign="bottom">
                <table class="TabStyle">
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp; Commission List&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td align="right">
                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close" Width="50">
                </asp:Button>
            </td>
        </tr>
    </table>
    <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Height="384px">
        <asp:TableRow ID="TableRow1" VerticalAlign="Top" runat="server">
            <asp:TableCell ID="TableCell1" runat="server">
                <asp:DataGrid ID="dgCommission" AllowSorting="true" runat="server" Width="100%" CssClass="dg"
                    AutoGenerateColumns="False" BorderColor="white">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn DataField="Name" HeaderText="<font color=white>Deal Won ID</font>">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="OppStatus" HeaderText="<font color=white>Deal Status</font>">
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="BizDoc">
                            <ItemTemplate>
                                <%--<span class="hyperlink" onclick="return OpenBizInvoice('<%# DataBinder.Eval(Container.DataItem, "numOppId") %>','<%# DataBinder.Eval(Container.DataItem, "numOppBizDocsId") %>');">--%>
                                    <%# DataBinder.Eval(Container.DataItem, "vcBizDocID")%>
                                    <%--</span>--%>
                                <asp:Label ID="lblBizDocID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "numBizDocId") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn HeaderText="Item" DataField="vcItemName"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Units" DataField="numUnitHour"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Amount" DataField="monTotAmount" DataFormatString="{0:##,#00.00}">
                        </asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Vendor Cost" DataField="VendorCost" DataFormatString="{0:##,#00.00}">
                        </asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Commission Amount" DataField="CommissionAmt" DataFormatString="{0:##,#00.00}">
                        </asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Commission" DataField="decCommission" DataFormatString="{0:##,#00.00}">
                        </asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Based On" DataField="BasedOn"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Commission Type" DataField="CommissionType"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    </form>
</body>
</html>
