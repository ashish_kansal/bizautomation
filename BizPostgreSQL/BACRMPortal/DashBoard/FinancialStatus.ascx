﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="FinancialStatus.ascx.vb"
    Inherits="BACRMPortal.FinancialStatus" %>
<script type="text/javascript">
    function OpenGL(lngDivisionID) {
        window.open("../Accounting/frmJournalEntry.aspx?DivisionID=" + lngDivisionID + "&ModeID=1", '', 'toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')
        return false;
    }
</script>
<div class="portlet" id="4">
    <div class="portlet-header">
        Financial status
    </div>
    <%--<table cellpadding="0" cellspacing="0">
    <tr>
        <td class="TabLeft">
            Financial Status
        </td>
        <td class="TabRight">
        </td>
    </tr>
</table>
    --%>
    <div class="portlet-content">
        <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="0" Height="100"
            runat="server" Width="450" BorderColor="black" GridLines="None">
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Top">
                    <table align="left" border="0" style="margin-left: 20px;">
                        <tr>
                            <td align="right" class="normal1">Total Balance Due :
                            </td>
                            <td class="normal1">
                                <asp:Label ID="lblBalDue" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="normal1">Total Remaining Credit :
                            </td>
                            <td class="normal1">
                                <asp:Label ID="lblRemCredit" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="normal1">Total Amount Past Due :
                            </td>
                            <td class="normal1">
                                <asp:Label ID="lblAmtPastDue" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <%--<tr>
                        <td align="right" class="normal1">
                            <asp:HyperLink NavigateUrl="#" ID="hplGL" runat="server" Text="General Ledger (G/L) Transactions " />
                        </td>
                    </tr>--%>
                    </table>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>
</div>
