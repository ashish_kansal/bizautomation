﻿Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Projects
Public Class OpenProjects
    Inherits BACRMUserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            BindOpenProjects()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub BindOpenProjects()
        Try
            Dim dtProjects As DataTable
            Dim objProjects As New Project
            Dim SortChar As Char
            If ViewState("SortChar") <> "" Then
                SortChar = ViewState("SortChar")
            Else : SortChar = "0"
            End If
            With objProjects
                .DomainID = Session("DomainID")
                .SortCharacter = SortChar
                .DivisionID = Session("DivId")
                .UserRightType = 0 'm_aryRightsForPage(RIGHTSTYPE.VIEW)
                .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                .CurrentPage = 1
                .PageSize = Session("PagingRows")
                .TotalRecords = 0
                .columnName = "pro.bintcreateddate"
                .columnSortOrder = "Asc"
                .sMode = 1
            End With
            Dim dsProject As DataSet = objProjects.GetProjectListPortal(True)
            If dsProject IsNot Nothing AndAlso dsProject.Tables.Count > 0 Then
                dtProjects = dsProject.Tables(0)
            Else
                dtProjects = Nothing
            End If
            dgProjcts.DataSource = dtProjects
            dgProjcts.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgProjcts_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgProjcts.ItemCommand
        Try
            'If e.CommandName = "project" Then
            '    Session("ProID") = e.Item.Cells(0).Text
            '    Response.Redirect("../pagelayout/frmCustProjectdtl.aspx?frm=dashboard", False)
            'End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class