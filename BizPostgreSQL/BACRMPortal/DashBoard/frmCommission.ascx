<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmCommission.ascx.vb"
    Inherits="BACRMPortal.frmCommission" %>
<%@ Register Assembly="Infragistics35.WebUI.UltraWebChart.v9.1" Namespace="Infragistics.WebUI.UltraWebChart"
    TagPrefix="igchart" %>
<script type="text/javascript" language="javascript">
    function OpenCommission(a, b) {
        window.open('../dashboard/frmCommissionforPayrollExpense.aspx?ComRuleID=' + a + '&ItemCode=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=780,height=700,scrollbars=yes,resizable=yes');
        return false;
    }
</script>
<div class="portlet">
    <div class="portlet-header">
        <asp:Label ID="lblHeader" runat="server"></asp:Label>
    </div>
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server">
    </asp:ScriptManager>
    <div class="portlet-content">
        <asp:Repeater ID="repCommission" runat="server">
            <ItemTemplate>
                <table id="tbl" cellpadding="0" cellspacing="0" borderwidth="0" height="100" runat="server"
                    width="100%" bordercolor="black" gridlines="None">
                    <tr>
                        <td align="center">
                            <asp:Label ID="lblHeader" runat="server" CssClass="text_bold" Text='<%#Eval("vcHeader") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <igchart:UltraChart ID="UltraChart1" runat="server" BackColor="#E9EDF4" BorderColor="#868686"
                                BorderWidth="1px" EmptyChartText="" Version="7.3" BackgroundImageFileName=""
                                ChartType="BarChart" Width="550">
                                <axis>
                                <Y Extent="40" LineColor="135, 161, 210" LineThickness="1" TickmarkInterval="1" TickmarkStyle="Smart"
                                    Visible="True">
                                    <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                        Visible="False" />
                                    <MajorGridLines AlphaLevel="255" Color="135, 161, 210" DrawStyle="Dot" Thickness="1"
                                        Visible="False" />
                                    <Labels Font="Arial, 8.25pt" HorizontalAlign="Far" ItemFormatString="&lt;ITEM_LABEL&gt;"
                                        Orientation="Horizontal" VerticalAlign="Center">
                                        <SeriesLabels Font="Verdana, 7pt" FontColor="DimGray" HorizontalAlign="Center" Orientation="Horizontal"
                                            VerticalAlign="Near" FormatString="">
                                            <Layout Behavior="Auto">
                                            </Layout>
                                        </SeriesLabels>
                                    </Labels>
                                </Y>
                                <Y2 LineThickness="1" TickmarkInterval="0" TickmarkStyle="Smart" Visible="False">
                                    <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                        Visible="False" />
                                    <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                        Visible="True" />
                                    <Labels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Near" ItemFormatString="&lt;ITEM_LABEL&gt;"
                                        Orientation="Horizontal" VerticalAlign="Center" Visible="False">
                                        <Layout Behavior="Auto">
                                        </Layout>
                                        <SeriesLabels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Center" Orientation="VerticalLeftFacing"
                                            VerticalAlign="Center">
                                            <Layout Behavior="Auto">
                                            </Layout>
                                        </SeriesLabels>
                                    </Labels>
                                </Y2>
                                <Z LineThickness="1" TickmarkInterval="0" TickmarkStyle="Smart" Visible="False">
                                    <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                        Visible="False" />
                                    <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                        Visible="True" />
                                    <Labels Font="Verdana, 7pt" FontColor="DimGray" HorizontalAlign="Near" ItemFormatString="&lt;ITEM_LABEL&gt;"
                                        Orientation="Horizontal" VerticalAlign="Center" Visible="False">
                                        <Layout Behavior="Auto">
                                        </Layout>
                                        <SeriesLabels Font="Verdana, 7pt" FontColor="DimGray" HorizontalAlign="Near" Orientation="Horizontal"
                                            VerticalAlign="Center">
                                            <Layout Behavior="Auto">
                                            </Layout>
                                        </SeriesLabels>
                                    </Labels>
                                </Z>
                                <X Extent="33" LineColor="135, 161, 210" LineThickness="1" TickmarkInterval="40"
                                    TickmarkStyle="Smart" Visible="True">
                                    <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                        Visible="False" />
                                    <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                        Visible="False" />
                                    <Labels Font="Arial, 8.25pt" HorizontalAlign="Far" Orientation="Horizontal" VerticalAlign="Center"
                                        ItemFormatString="&lt;DATA_VALUE:0.##&gt;">
                                        <SeriesLabels Font="Arial, 8.25pt" HorizontalAlign="Far" Orientation="VerticalLeftFacing"
                                            VerticalAlign="Center" Visible="False" FormatString="">
                                        </SeriesLabels>
                                    </Labels>
                                    <Margin>
                                        <Far Value="2.6415094339622645" />
                                    </Margin>
                                </X>
                                <X2 LineThickness="1" TickmarkInterval="40" TickmarkStyle="Smart" Visible="False">
                                    <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                        Visible="False" />
                                    <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                        Visible="True" />
                                    <Labels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Far" ItemFormatString="&lt;DATA_VALUE:00.##&gt;"
                                        Orientation="VerticalLeftFacing" VerticalAlign="Center" Visible="False">
                                        <Layout Behavior="Auto">
                                        </Layout>
                                        <SeriesLabels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Far" Orientation="VerticalLeftFacing"
                                            VerticalAlign="Center" FormatString="">
                                            <Layout Behavior="Auto">
                                            </Layout>
                                        </SeriesLabels>
                                    </Labels>
                                </X2>
                                <PE Fill="Cornsilk" ElementType="None" />
                                <Z2 LineThickness="1" TickmarkInterval="0" TickmarkStyle="Smart" Visible="False">
                                    <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                        Visible="False" />
                                    <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                        Visible="True" />
                                    <Labels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Near" ItemFormatString=""
                                        Orientation="Horizontal" VerticalAlign="Center" Visible="False">
                                        <Layout Behavior="Auto">
                                        </Layout>
                                        <SeriesLabels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Near" Orientation="VerticalLeftFacing"
                                            VerticalAlign="Center">
                                            <Layout Behavior="Auto">
                                            </Layout>
                                        </SeriesLabels>
                                    </Labels>
                                </Z2>
                            </axis>
                                <%--<BarChart>
                                <ChartText>
                                    <igchartprop:ChartTextAppearance ItemFormatString="&lt;ITEM_LABEL&gt;" Visible="true"
                                        HorizontalAlign="Near" VerticalAlign="Center" Column="-2" Row="-2" />
                                </ChartText>
                            </BarChart>--%>
                                <border color="134, 134, 134" cornerradius="10" />
                                <effects>
                                <Effects>
                                    <igchartprop:gradienteffect style="backwarddiagonal">
                                    </igchartprop:gradienteffect>
                                    <igchartprop:strokeeffect strokeopacity="255" strokewidth="0">
                                    </igchartprop:strokeeffect>
                                </Effects>
                            </effects>
                                <colormodel alphalevel="150" colorbegin="79, 129, 189" modelstyle="CustomLinear">
                                <Skin ApplyRowWise="False">
                                    <PEs>
                                        <igchartprop:PaintElement ElementType="Gradient" Fill="46, 120, 208" FillGradientStyle="Vertical"
                                            FillStopColor="23, 65, 115" Stroke="29, 82, 145" StrokeWidth="0"></igchartprop:PaintElement>
                                    </PEs>
                                </Skin>
                            </colormodel>
                                <legend backgroundcolor="Transparent" borderthickness="0" font="Microsoft Sans Serif, 9.75pt, style=Bold"
                                    location="Left"></legend>
                                <tooltips font-bold="False" font-italic="False" font-overline="False" font-strikeout="False"
                                    font-underline="False" />
                                <data zeroaligned="True">
                            </data>
                                <titleleft font="Arial, 8.25pt" extent="33" visible="True" flip="True" horizontalalign="Center">
                                <Margins Bottom="0" Left="0" Right="0" Top="0" />
                            </titleleft>
                            </igchart:UltraChart>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Label ID="lblFooter" runat="server" CssClass="text_bold"></asp:Label>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
            <SeparatorTemplate>
                <hr />
            </SeparatorTemplate>
        </asp:Repeater>
        <asp:Label ID="lblError" runat="server" CssClass="text_bold"></asp:Label>
    </div>
</div>
