﻿Imports BACRM.BusinessLogic.Contract
Imports BACRM.BusinessLogic.Common
Public Class Contracts
    Inherits BACRMUserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                LoadContractsInfo()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub LoadContractsInfo()
        Try
            If Not IsPostBack Then
                Dim objContracts As New CContracts
                Dim dtTable As DataTable
                objContracts.DivisionId = Session("DivID")
                objContracts.UserCntId = Session("UserContactId")
                objContracts.DomainId = Session("DomainId")
                dtTable = objContracts.GetContractDdlList()
                ddlContract.DataSource = dtTable
                ddlContract.DataTextField = "vcContractName"
                ddlContract.DataValueField = "numcontractId"
                ddlContract.DataBind()
                ddlContract.Items.Insert(0, "--Select One--")
                ddlContract.Items.FindByText("--Select One--").Value = "0"
                lblRemAmount.Text = "0"
                lblRemDays.Text = "0"
                lblRemInci.Text = "0"
                lblRemHours.Text = "0"
                lblExpirationDate.Text = ""
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlContract_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlContract.SelectedIndexChanged
        Try
            BindContractinfo()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub BindContractinfo()
        Try
            Dim objContracts As New CContracts
            Dim dtTable As DataTable
            objContracts.ContractID = ddlContract.SelectedValue
            objContracts.UserCntId = Session("UserContactId")
            objContracts.DomainId = Session("DomainId")
            dtTable = objContracts.GetContractDtl()
            If dtTable.Rows.Count > 0 Then
                lblRemAmount.Text = IIf(IsDBNull(dtTable.Rows(0).Item("RemAmount")), 0, String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("RemAmount")))
                lblRemHours.Text = IIf(IsDBNull(dtTable.Rows(0).Item("RemHours")), 0, String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("RemHours")))
                lblRemDays.Text = IIf(IsDBNull(dtTable.Rows(0).Item("days")), 0, String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("days")))
                If dtTable.Rows(0).Item("bitincidents") = True Then
                    lblRemInci.Text = IIf(IsDBNull(dtTable.Rows(0).Item("Incidents")), 0, dtTable.Rows(0).Item("Incidents"))
                Else : lblRemInci.Text = 0
                End If
                lblExpirationDate.Text = CCommon.ToString(dtTable.Rows(0).Item("ExpDate"))
            Else
                lblRemAmount.Text = 0
                lblRemHours.Text = 0
                lblRemDays.Text = 0
                lblRemInci.Text = 0
                lblExpirationDate.Text = ""
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class