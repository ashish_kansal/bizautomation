﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="OpenCases.ascx.vb"
    Inherits="BACRMPortal.OpenCases" %>
<div class="portlet" id="7">
    <div class="portlet-header">
        Open Cases
    </div>
    <%--    <table cellpadding="0" cellspacing="0">
            <tr>
                <td class="TabLeft">
                    Open Cases
                </td>
                <td class="TabRight">
                </td>
            </tr>
        </table>--%>
    <div class="portlet-content">
        <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="0" runat="server"
            Width="100%" BorderColor="black" GridLines="None">
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Top">
                    </br>
                    <div style="height: 150px; overflow: scroll;">
                        <table width="100%">
                            <tr>
                                <td class="normal1" align="center">
                                    <asp:DataGrid ID="dgReptCases" CssClass="dg" Width="100%" runat="server" BorderColor="white"
                                        AllowSorting="True" AutoGenerateColumns="False">
                                        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                        <ItemStyle CssClass="is"></ItemStyle>
                                        <HeaderStyle CssClass="hs"></HeaderStyle>
                                        <Columns>
                                            <%--<asp:BoundColumn DataField="numCompanyID" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="numDivisionID" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="ContactID" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="tintCRMType" Visible="false"></asp:BoundColumn>--%>
                                            <asp:BoundColumn DataField="numCaseid" Visible="false"></asp:BoundColumn>
                                            <asp:TemplateColumn HeaderText="Case Number">
                                                <ItemTemplate>
                                                    <%--<asp:LinkButton runat="server" ID="lbCases" Text='<%# DataBinder.Eval(Container.DataItem, "vcCaseNumber") %>'
                                                        CommandName="case">
                                                    </asp:LinkButton>--%>
                                                    <%# DataBinder.Eval(Container.DataItem, "vcCaseNumber") %>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="textSubject" HeaderText="Subject"></asp:BoundColumn>
                                            <asp:TemplateColumn HeaderText="Resolve Date">
                                                <ItemTemplate>
                                                    <%# ReturnName(DataBinder.Eval(Container.DataItem, "intTargetResolveDate")) %>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>
</div>
