﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="OpenProjects.ascx.vb"
    Inherits="BACRMPortal.OpenProjects" %>
<script type="text/javascript">
    function progress(ProgInPercent, Container, progress, prg_width) {
        //Set Total Width
        var OuterDiv = document.getElementById(Container);
        OuterDiv.style.width = prg_width + 'px';
        OuterDiv.style.height = '5px';

        if (ProgInPercent > 100) {
            ProgInPercent = 100;
        }
        //Set Progress Percentage
        var node = document.getElementById(progress);
        node.style.width = parseInt(ProgInPercent) * prg_width / 100 + 'px';
    }
</script>
<div class="portlet" id="8">
    <div class="portlet-header">
        Open Projects
    </div>
    <%--<table cellpadding="0" cellspacing="0">
    <tr>
        <td class="TabLeft">
            Open Projects
        </td>
        <td class="TabRight">
        </td>
    </tr>
</table>--%>
    <div class="portlet-content">
        <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="0" Height="150"
            runat="server" Width="100%" BorderColor="black" GridLines="None">
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Top">
                    <div style="height: 150px; overflow: scroll;">
                        <asp:DataGrid ID="dgProjcts" CssClass="dg" Width="100%" runat="server" BorderColor="white"
                            AllowSorting="True" AutoGenerateColumns="False">
                            <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                            <ItemStyle CssClass="is"></ItemStyle>
                            <HeaderStyle CssClass="hs"></HeaderStyle>
                            <Columns>
                                <asp:BoundColumn DataField="numProID" Visible="false"></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="Project Name">
                                    <ItemTemplate>
                                        <%--<asp:LinkButton runat="server" ID="lbProjects" Text='<%# DataBinder.Eval(Container.DataItem, "Name") %>'
                                        CommandName="project">
                                    </asp:LinkButton>--%>
                                        <%# DataBinder.Eval(Container.DataItem, "Name") %>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="DueDate" HeaderText="Due Date"></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="Total Progress">
                                    <ItemTemplate>
                                        &nbsp;<%# DataBinder.Eval(Container.DataItem, "intTotalProgress") %> %
                                    <div id="ProgressContainer_<%# Eval("numProID") %>" style="border: 1px solid black; height: 5px; font-size: 1px; line-height: 0; width: 60px">
                                        &nbsp;<div id="progress_<%# Eval("numProID") %>" style="height: 5px; width: 0px; background-color: green;">
                                        </div>

                                        <script type="text/javascript">
                                            progress(<%# DataBinder.Eval(Container.DataItem, "intTotalProgress") %>, 'ProgressContainer_<%# Eval("numProID") %>', 'progress_<%# Eval("numProID") %>', 100)
                                        </script>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>
</div>
