Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Common
'Imports Infragistics.UltraChart.Resources.Appearance
'Imports Infragistics.WebUI.UltraWebChart
Partial Public Class frmCommission
    Inherits BACRMUserControl

    'Dim objCustomReport As New CustomReports
    'Dim objDashboard As New DashBoard

    'Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '    Try
    '        CreateReport()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Public Sub CreateReport()
    '    Try
    '        lblHeader.Text = "Commission Report"

    '        objDashboard = New DashBoard
    '        objDashboard.UserCntID = Session("DivId")
    '        objDashboard.DomainID = Session("DomainID")

    '        Dim ds As DataSet
    '        ds = objDashboard.GetCommissionContactsReport

    '        If ds.Tables.Count > 0 Then
    '            repCommission.DataSource = ds.Tables(0)
    '            repCommission.DataBind()
    '        Else
    '            lblError.Text = "Please Set ""Commission structure & items affected by the rule"" from Domain Details->Order Management."
    '        End If


    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub repCommission_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles repCommission.ItemDataBound
    '    If e.Item.ItemType = ListItemType.Item Then
    '        Try
    '            Dim dttable2 As New DataTable
    '            objCustomReport.DynamicQuery = DataBinder.Eval(e.Item.DataItem, "textquerygrp")
    '            objCustomReport.UserCntID = Session("divid")
    '            objCustomReport.DomainID = Session("domainid")
    '            Dim ds As DataSet
    '            ds = objCustomReport.ExecuteDynamicSql()
    '            Dim dttable3 As DataTable
    '            dttable3 = ds.Tables(0)

    '            If dttable3.Columns.Count <= 1 Then Exit Sub

    '            If dttable3.Rows.Count <= 0 Then Exit Sub

    '            For Each dtrow As DataRow In dttable3.Rows
    '                dttable2.Columns.Add(IIf(dtrow.Item(0) = "", "-", dtrow.Item(0)), dttable3.Columns(1).DataType)
    '            Next
    '            Dim dr As DataRow
    '            dr = dttable2.NewRow
    '            For Each dtrow As DataRow In dttable3.Rows
    '                dr(IIf(dtrow.Item(0) = "", "-", dtrow.Item(0))) = dtrow.Item(1)
    '            Next
    '            dttable2.Rows.Add(dr)
    '            dttable2.AcceptChanges()

    '            CType(e.Item.FindControl("lblfooter"), Label).Text = "<a class=""hyperlink"" href=""#"" onclick=""return OpenCommission(" & ds.Tables(1).Rows(0)("numcomruleid").ToString() & "," & ds.Tables(1).Rows(0)("numitemcode").ToString() & ")""><b>Total commission made:</b></a>" & ds.Tables(1).Rows(0)("totalcommissionamt").ToString() & "(" & ds.Tables(1).Rows(0)("vccommissionduration").ToString() & ")"

    '            CType(e.Item.FindControl("UltraChart1"), UltraChart).TitleBottom.Text = ds.Tables(1).Rows(0)("vccombasedon").ToString()
    '            CType(e.Item.FindControl("UltraChart1"), UltraChart).TitleBottom.HorizontalAlign = StringAlignment.Center

    '            CType(e.Item.FindControl("UltraChart1"), UltraChart).Axis.Y.Extent = 20

    '            Dim i As Integer
    '            For i = 0 To dttable3.Rows.Count - 1

    '                If dttable3.Rows.Count - 1 = i Then
    '                    CType(e.Item.FindControl("UltraChart1"), UltraChart).BarChart.ChartText.Add(New ChartTextAppearance(CType(e.Item.FindControl("UltraChart1"), UltraChart), -2, i, True, New Font("verdana", 7.0), _
    '                                                   Color.Black, dttable3.Rows(i)("vctext"), StringAlignment.Center, StringAlignment.Near, 0))
    '                Else
    '                    CType(e.Item.FindControl("UltraChart1"), UltraChart).BarChart.ChartText.Add(New ChartTextAppearance(CType(e.Item.FindControl("UltraChart1"), UltraChart), -2, i, True, New Font("verdana", 7.0), _
    '                       Color.Black, dttable3.Rows(i)("vctext"), StringAlignment.Center, StringAlignment.Far, 0))
    '                End If

    '            Next i

    '            CType(e.Item.FindControl("UltraChart1"), UltraChart).Data.DataSource = dttable2
    '            CType(e.Item.FindControl("UltraChart1"), UltraChart).Data.DataBind()
    '        Catch ex As Exception

    '        End Try
    '    End If
    'End Sub
End Class