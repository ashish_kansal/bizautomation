﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmPortalDashBoard.aspx.vb"
    Inherits="BACRMPortal.frmPortalDashBoard" %>

<%@ Register Src="OpenCases.ascx" TagName="OpenCases" TagPrefix="uc1" %>
<%@ Register Src="Contracts.ascx" TagName="Contracts" TagPrefix="uc2" %>
<%@ Register Src="Documents.ascx" TagName="Documents" TagPrefix="uc3" %>
<%@ Register Src="AccountDetails.ascx" TagName="AccountDetails" TagPrefix="uc4" %>
<%@ Register Src="Assets.ascx" TagName="Assets" TagPrefix="uc5" %>
<%@ Register Src="FinancialStatus.ascx" TagName="FinancialStatus" TagPrefix="uc6" %>
<%@ Register Src="ItemsToReturn.ascx" TagName="ItemsToReturn" TagPrefix="uc7" %>
<%@ Register Src="OpenProjects.ascx" TagName="OpenProjects" TagPrefix="uc8" %>
<%@ Register Src="BizdocAmtDue.ascx" TagName="BizdocAmtDue" TagPrefix="uc9" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../JavaScript/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="../JavaScript/jquery-ui-1.8.5.custom.min.js" type="text/javascript"></script>
    <title>Dashboard</title>
    <script type="text/javascript">
        $(function () {
            $(".column").sortable({
                disable: true,
                connectWith: '.column',
                stop: function (event, ui) {
                },
                update: function (event, ui) {
                    var Column1;
                    var Column2;
                    Column1 = $('#Column1').sortable('toArray');
                    //                    console.log(Column1);
                    Column2 = $('#Column2').sortable('toArray');
                    //                    console.log(Column2);
                    $.ajax({

                        type: "POST",
                        url: "frmPortalDashboard.aspx/UpdateOrder",
                        data: '{Column1Values:"' + Column1.toString() + '",Column2Values:"' + Column2.toString() + '",RelID:"' + document.getElementById('hdnRelationship').value + '",ProfID:"' + document.getElementById('hdnProfile').value + '",DID:"' + document.getElementById('hdnDomain').value + '",UID:"' + document.getElementById('hdnU').value + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        cache: false,
                        success: function (msg) {
                            if (msg.d == 'True') {
                                $('#dvSucess').text('Widget order saved sucessfully');
                                FadeOut(2000);
                            }
                        }
                    })
                }

            });

            $(".portlet").addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
			.find(".portlet-header")
				.addClass("ui-widget-header ui-corner-all")
				.prepend('<span class="ui-icon ui-icon-minusthick"></span>')
				.end()
			.find(".portlet-content");

            $(".portlet-header .ui-icon").click(function () {
                $(this).toggleClass("ui-icon-minusthick").toggleClass("ui-icon-plusthick");
                $(this).parents(".portlet:first").find(".portlet-content").toggle();
            });


            // $(".column").disableSelection();



        });

        function FadeOut(NoMSeconds) {
            $('#dvSucess').show();
            setTimeout(function () {
                $("#dvSucess").fadeOut("slow", function () {
                    $("#dvSucess").hide();
                });

            }, NoMSeconds);
        }
    </script>
    <style type="text/css">
        .column
        {
            width: 600px;
            float: left;
            padding-bottom: 100px;
        }
        .portlet
        {
            margin: 0 1em 1em 0;
            background-color: #F3F4F5;
        }
        .portlet-header
        {
            margin: 0.3em;
            padding-bottom: 4px;
            padding-left: 0.2em;
            background-color: #697496;
            color: White;
            font-family: Sans-Serif, Arial , Tahoma;
            font-size: small;
            cursor:move;
        }
        .portlet-header .ui-icon
        {
            float: right;
        }
        .portlet-contentfrmAmtPaid
        {
            padding: 0.4em;
        }
        .ui-sortable-placeholder
        {
            border: 1px dotted black;
            visibility: visible !important;
            height: 50px !important;
        }
        .ui-sortable-placeholder *
        {
            visibility: hidden;
        }
        
        .success
        {
            border: 1px solid;
            margin: 10px 0px;
            padding: 15px 10px 15px 50px;
            background-repeat: no-repeat;
            background-position: 10px center;
            color: #4F8A10;
            background-color: #DFF2BF;
            background-image: url('../images/success.png');
        }
    </style>
</head>
<body style="height: 100%">
    <form id="form1" runat="server">
    <%-- <asp:ScriptManager ID="ScriptManager" runat="server" EnablePageMethods="true" />--%>
    <table width="100%" height="100%">
        <tr>
            <td>
                <div class="success" id="dvSucess" style="display: none">
                </div>
            </td>
            <td align="right">
                <asp:LinkButton Text="Reset" runat="server" ID="lnkReset" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="demo" style="margin: 10px 10px 10px 10px;">
                    <div class="column" id="Column1">
                        <asp:PlaceHolder ID="phColumn1" runat="server"></asp:PlaceHolder>
                    </div>
                    <div class="column" id="Column2">
                        <asp:PlaceHolder ID="phColumn2" runat="server"></asp:PlaceHolder>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnRelationship" runat="server" />
    <asp:HiddenField ID="hdnProfile" runat="server" />
    <asp:HiddenField ID="hdnDomain" runat="server" />
    <asp:HiddenField ID="hdnU" runat="server" />
    </form>
</body>
</html>
