﻿Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common

Public Class FinancialStatus
    Inherits BACRMUserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim objItems As New CItems
            Dim lngCredit As Long
            objItems.DivisionID = Session("DivId")
            lngCredit = objItems.GetCreditStatusofCompany
            Dim dtTable As DataTable
            dtTable = objItems.GetAmountDue
            lblBalDue.Text = String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("AmountDueSO"))
            lblRemCredit.Text = String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("RemainingCredit"))
            lblAmtPastDue.Text = String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("AmountPastDueSO"))
            'hplGL.Attributes.Add("onclick", "return OpenGL('" & Session("DivId") & "')")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class