﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Contracts.ascx.vb"
    Inherits="BACRMPortal.Contracts" %>
<div class="portlet" id="2">
    <div class="portlet-header">
        Contracts
    </div>
    <%--<table cellpadding="0" cellspacing="0">
    <tr>
        <td class="TabLeft">
            Contracts
        </td>
        <td class="TabRight">
        </td>
    </tr>
</table>--%>
    <div class="portlet-content">
        <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="0" Height="150"
            runat="server" Width="100%" BorderColor="black" GridLines="None">
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Top">
                    </br>
                <table width="100%" class="normal1">
                    <tr>
                        <td align="right">Contract :
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlContract" AutoPostBack="true" Width="180" runat="server"
                                CssClass="signup">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">Amount :
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblRemAmount" CssClass="normal1"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">Contract Expiration Date :
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblExpirationDate" CssClass="normal1"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">Days Remaining :
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblRemDays" CssClass="normal1"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">Incidents Remaining :
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblRemInci" CssClass="normal1"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">Hours Remaining :
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblRemHours" CssClass="normal1"></asp:Label>
                        </td>
                    </tr>
                </table>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>
</div>
