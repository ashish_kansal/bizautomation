﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Assets.ascx.vb" Inherits="BACRMPortal.Assets" %>
<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<div class="portlet" id="1">
    <div class="portlet-header">
        Assets <%--<span style="float: right">
            <asp:HyperLink runat="server" ID="hplViewAll" NavigateUrl="~/Item/frmAssetList.aspx"
                Text="View all assets >>" CssClass="hyperlink" style="color:White"></asp:HyperLink></span>--%>
    </div>
    <%--<table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="TabLeft">
                            Assets
                        </td>
                        <td class="TabRight">
                        </td>
                    </tr>
                </table>
            </td>
            <td align="right">
            </td>
        </tr>
    </table>--%>
    <div class="portlet-content">
        <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="0" Height="150"
            runat="server" Width="100%" BorderColor="black" GridLines="None">
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Top">
                    </br>
                <table width="100%">
                    <tr>
                        <td colspan="2" align="center">
                            <asp:DataGrid ID="dgAssets" runat="server" CssClass="dg" Width="100%" BorderColor="white"
                                AutoGenerateColumns="false">
                                <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                <ItemStyle CssClass="is"></ItemStyle>
                                <HeaderStyle CssClass="hs"></HeaderStyle>
                                <Columns>
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <%# CCommon.GetImageHTML(Eval("vcPathForTImage"),1) %>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="vcItemName" HeaderText="Item"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="vcModelId" HeaderText="Model"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="txtItemDesc" HeaderText="Description"></asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>
</div>
