﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item
Public Class Assets
    Inherits BACRMUserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            LoadAssets()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Public Sub LoadAssets()
        Try
            Dim ds As DataSet
            Dim objItems As New CItems
            Dim SortChar As Char



            If ViewState("SortCharITems") <> "" Then
                SortChar = ViewState("SortCharITems")
            Else : SortChar = "0"
            End If

            With objItems
                .DivisionID = Session("DivId")
                .ItemClassification = 0
                .SortCharacter = SortChar
                .CurrentPage = 1
                .KeyWord = ""
                .DomainID = Session("DomainID")
                .PageSize = Session("PagingRows")
                .TotalRecords = 0
                .columnName = "vcItemName"
                .columnSortOrder = "Asc"
                ds = .getCompanyAssetsSerial
            End With

            Dim dt As DataTable
            dt = ds.Tables(0).Clone()
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows.Count > 5 Then
                    For i As Integer = 0 To 4
                        dt.ImportRow(ds.Tables(0).Rows(i))
                    Next
                    'hplViewAll.Visible = True
                Else
                    For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                        dt.ImportRow(ds.Tables(0).Rows(i))
                    Next
                    'hplViewAll.Visible = False
                End If
            End If

            dgAssets.DataSource = dt
            dgAssets.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class