﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities

Public Class BizdocAmtDue
    Inherits BACRMUserControl


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblTitle.Text = "Invoice amounts due"
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub BindDatagrid()
        Try
            Dim dtOpportunity As DataTable
            Dim objOpportunity As New COpportunities
            With objOpportunity
                .UserCntID = Session("UserContactID")
                .DomainID = Session("DomainID")
                .DivisionID = Session("DivID")
                .OppType = 1
            End With

            dtOpportunity = objOpportunity.GetInvoiceList

            dgOrder.DataSource = dtOpportunity
            dgOrder.DataBind()
            If dtOpportunity.Rows.Count > 0 Then lblBalance.Text = CCommon.ToDecimal(dtOpportunity.Compute(" sum(BalanceDue) ", "")).ToString("#,##0.00")

        Catch ex As Exception
            Throw ex
        End Try
    End Sub


End Class