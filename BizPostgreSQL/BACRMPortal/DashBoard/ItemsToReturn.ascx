﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ItemsToReturn.ascx.vb"
    Inherits="BACRMPortal.ItemsToReturn" %>
<script type="text/javascript">
    function OpenReturn() {
        window.open('../item/frmAddReturn.aspx', '', 'toolbar=no,titlebar=no,top=200,width=800,height=450,scrollbars=yes,resizable=yes');
        return false;
    }
</script>
<div class="portlet" id="6">
    <div class="portlet-header">
        Items awaiting return <span style="float: right">
            <asp:HyperLink runat="server" ID="hplRequestReturn" NavigateUrl="javascript:" Text="Request Return >>"
                onclick="return OpenReturn();" CssClass="hyperlink" Style="color: White"></asp:HyperLink></span>
    </div>
    <div class="portlet-content">
        <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="0" Height="150"
            runat="server" Width="100%" BorderColor="black" GridLines="None">
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Top">
                    </br>
                <div style="height: 150px; overflow: scroll;">
                    <asp:DataGrid ID="dgSalesReturns" runat="server" AutoGenerateColumns="False" CssClass="dg"
                        Width="100%" AllowSorting="True">
                        <Columns>
                            <asp:BoundColumn HeaderText="Item Name" DataField="vcItemName" ReadOnly="true" />
                            <asp:BoundColumn HeaderText="Qty" DataField="quantitytoreturn" ReadOnly="true" />
                            <asp:BoundColumn HeaderText="Reason for return" DataField="reasonforreturn" ReadOnly="true"
                                ItemStyle-HorizontalAlign="Center" />
                            <asp:BoundColumn HeaderText="Status" DataField="ReturnStatus" ReadOnly="true" ItemStyle-HorizontalAlign="Center" />
                        </Columns>
                        <AlternatingItemStyle CssClass="ais" />
                        <ItemStyle CssClass="is" />
                        <HeaderStyle CssClass="hs" />
                    </asp:DataGrid>
                </div>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>
</div>
