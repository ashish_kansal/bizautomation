﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="BizdocAmtDue.ascx.vb"
    Inherits="BACRMPortal.BizdocAmtDue" %>
<script type="text/javascript">
    function OpenBizInvoice(a, b) {
        if (a > 0 && b > 0) {
            window.open('../Opportunity/frmBizInvoice.aspx?Show=True&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=0,titlebar=0,menubar=0,location=1,left=100,top=50,width=750,height=800,scrollbars=yes,resizable=yes');
            return false;
        }
        return false;
    }
    function OpenAmountPaid() {
        window.open('../opportunity/frmNewAmtPaid.aspx', '', 'toolbar=0,titlebar=0,menubar=0,location=1,left=50,top=50,width=1050,height=500,scrollbars=yes,resizable=yes');
        return false;
    }

</script>
<div class="portlet" id="5">
    <div class="portlet-header">
        <asp:Label Text="" runat="server" ID="lblTitle" />
    </div>
    <div class="portlet-content">
        <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="0" Height="150"
            runat="server" Width="100%" BorderColor="black" GridLines="None">
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Top">
                    <table width="100%">
                        <tr>
                            <td class="normal1" colspan="2" align="right">Balance :&nbsp;<asp:Label Text="" runat="server" ID="lblBalance" />&nbsp;
                            <asp:Button Text="Pay Now" runat="server" ID="btnPayNow" CssClass="ybutton" OnClientClick="return OpenAmountPaid();" />&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="normal1" align="right">
                                <div style="height: 150px; overflow: scroll;">
                                    <asp:DataGrid ID="dgOrder" runat="server" Width="100%" CssClass="dg" AllowSorting="True"
                                        AutoGenerateColumns="False" BorderColor="white">
                                        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                        <ItemStyle CssClass="is"></ItemStyle>
                                        <HeaderStyle CssClass="hs"></HeaderStyle>
                                        <Columns>
                                            <asp:TemplateColumn>
                                                <HeaderTemplate>
                                                    Invoice
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <a href="#" onclick="return OpenBizInvoice('<%# Eval("numOppId") %>','<%# Eval("numOppBizDocsId") %>');">
                                                        <%# Eval("vcBizDocID")%></a>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Billing Date / Due Date">
                                                <ItemTemplate>
                                                    <%# DataBinder.Eval(Container.DataItem, "BillingDate")%>
                                /
                                <%# DataBinder.Eval(Container.DataItem, "DueDate")%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Total Amt / Paid Amt / Due Amt" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <%# String.Format("{0:#,##0.00}", Eval("monDealAmount"))%>
                                                /
                                                <%# String.Format("{0:#,##0.00}", Eval("monAmountPaid"))%>
                                                /
                                                <%# String.Format("{0:#,##0.00}", Eval("BalanceDue"))%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </div>
                            </td>
                        </tr>
                    </table>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>
</div>
