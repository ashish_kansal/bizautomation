﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Documents
Public Class Documents
    Inherits BACRMUserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindGrid()
        Try
            Dim dtDocuments As DataTable
            Dim objDocuments As New DocumentList
            objDocuments.UserCntID = Session("UserContactID")
            objDocuments.byteMode = 1
            dtDocuments = objDocuments.ExtGetDocuments()
            dgDocs.DataSource = dtDocuments
            dgDocs.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgDocs_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgDocs.ItemCommand
        Try
            If e.CommandName = "Name" Then
                Session("DocID") = e.Item.Cells(0).Text()
                Response.Redirect("../Documents/frmCusAddDocs.aspx?frm=dashboard&Type=" & e.Item.Cells(1).Text & "&RecID=" & e.Item.Cells(3).Text & "&DocID=" & e.Item.Cells(0).Text, False)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgDocs_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgDocs.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim btn As LinkButton = e.Item.FindControl("lbDocName")
                Dim lbl As Label = e.Item.FindControl("lblFileType")
                If lbl.Text.ToUpper = "B" Then
                    btn.Attributes.Add("onclick", "return OpenBizInvoice(" & e.Item.Cells(3).Text() & ", " & e.Item.Cells(0).Text() & ");")
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim objDocuments As New DocumentList
            objDocuments.ContactID = Session("UserContactID")
            objDocuments.UserCntID = Session("UserContactID")
            Dim bitSelected As Boolean = True
            Dim sendEmail As New ArrayList
            Dim dt As New DataTable
            dt.Columns.Add("numRecOwner")
            dt.Columns.Add("DocumentName")
            dt.Columns.Add("Category")
            dt.Columns.Add("txtComment")
            dt.Columns.Add("bitApproved")
            dt.Columns.Add("bitSent")
            Dim dr As DataRow
            For Each dgrow As DataGridItem In dgDocs.Items
                bitSelected = False
                objDocuments.GenDocID = dgrow.Cells(0).Text.Trim
                objDocuments.CDocType = dgrow.Cells(1).Text.Trim.ToUpper()
                objDocuments.Comments = CType(dgrow.FindControl("txtComment"), TextBox).Text.Trim
                If CType(dgrow.FindControl("rbApprove"), RadioButton).Checked Then
                    objDocuments.byteMode = 3
                    objDocuments.ManageApprovers()
                    bitSelected = True
                ElseIf CType(dgrow.FindControl("rbDeclined"), RadioButton).Checked Then
                    objDocuments.byteMode = 4
                    objDocuments.ManageApprovers()
                    bitSelected = True
                End If

                If bitSelected Then
                    dr = dt.NewRow
                    dr("numRecOwner") = dgrow.Cells(2).Text.Trim
                    dr("DocumentName") = CType(dgrow.FindControl("lbDocName"), LinkButton).Text
                    dr("Category") = CType(dgrow.FindControl("lblDocumentCategory"), Label).Text.Trim
                    dr("txtComment") = CType(dgrow.FindControl("txtComment"), TextBox).Text.Trim
                    dr("bitApproved") = IIf(CType(dgrow.FindControl("rbApprove"), RadioButton).Checked, 1, 0)
                    dr("bitSent") = 0
                    dt.Rows.Add(dr)

                    'sendEmail.Add(dgrow)

                End If

            Next

            Dim sb As New System.Text.StringBuilder
            Dim dtMailMerger As New DataTable
            For Each dr1 As DataRow In dt.Rows

                If dr1("bitSent") = 0 Then
                    sb.Append("<table>")
                    sb.Append("<tr>")
                    sb.Append("<td><b>Document Name</b></td>")
                    sb.Append("<td><b>Category</b></td>")
                    sb.Append("<td><b>Comment</b></td>")
                    sb.Append("<td><b>Status</b></td>")
                    sb.Append("</tr>")
                    For i As Int16 = 0 To dt.Rows.Count - 1
                        If dr1("numRecOwner") = dt.Rows(i)("numRecOwner") And dt.Rows(i)("bitSent") = 0 Then
                            sb.Append("<tr>")
                            sb.Append("<td>" & dt.Rows(i)("DocumentName") & "</td>")
                            sb.Append("<td>" & dt.Rows(i)("Category") & "</td>")
                            sb.Append("<td>" & dt.Rows(i)("txtComment") & "</td>")
                            sb.Append("<td>" & IIf(dt.Rows(i)("bitApproved") = 1, "Approved", "Declined") & "</td>")
                            sb.Append("</tr>")
                            dt.Rows(i)("bitSent") = 1

                        End If
                    Next
                    sb.Replace("<td>", "<td style=""border-bottom: 1px solid rgb(204, 204, 204); font: 12px arial; color: rgb(90, 90, 90); padding-left: 3px;"">")
                    sb.Append("</table>")




                    Dim objSendEmail As New Email

                    objSendEmail.DomainID = Session("DomainID")
                    objSendEmail.TemplateCode = "#SYS#PORTAL:DOCUMENTS_TO_APPROVE"
                    objSendEmail.ModuleID = 1
                    objSendEmail.RecordIds = dr1("numRecOwner") 'numRecOwner
                    objSendEmail.AdditionalMergeFields.Add("ListOfApprovedOrDeclinedDocuments", sb.ToString)
                    objSendEmail.AdditionalMergeFields.Add("LoggedInUser", Session("ContactName"))
                    objSendEmail.FromEmail = Session("UserEmail")
                    objSendEmail.ToEmail = "##ContactFirstName## <##ContactEmail##>"
                    objSendEmail.SendEmailTemplate()

                    'objSendMail.SendSimpleEmail(Session("ContactName") & " responded to document your approval request", sb.ToString, "", Session("UserEmail"), objCommon.GetContactsEmail, DomainID:=Session("DomainID")) ')
                    'objSendMail.AddEmailToJob(Session("ContactName") & " responded to document your approval request", sb.ToString, "", Session("UserEmail"), "chintan@bizautomation.com", dtMailMerger, DomainID:=Session("DomainID")) 'objCommon.GetContactsEmail)
                    sb.Clear()
                End If

            Next

            
           
            
            BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class