﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Public Class ItemsToReturn
    Inherits BACRMUserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub BindGrid()
        Try
            Dim objOpportunity As New MOpportunity
            With objOpportunity
                .DomainID = Session("DomainID")
                .CurrentPage = 1
                .PageSize = Session("PagingRows")
                .TotalRecords = 0
                .Mode = 1
                .DivisionID = Session("DivId")
            End With

            dgSalesReturns.DataSource = objOpportunity.GetReturnsReport
            dgSalesReturns.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class