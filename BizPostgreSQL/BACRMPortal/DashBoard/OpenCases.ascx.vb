﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.Case

Public Class OpenCases
    Inherits BACRMUserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            BindOpenCases()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub BindOpenCases()
        Try
            Dim dsCases As DataSet
            Dim dtCases As DataTable
            Dim objCases As New CCases

            With objCases

                .Status = 1008 ' open cases 
                .sMode = 1
                .UserCntID = 0 'Session("UserContactID")
                .DomainID = Session("DomainID")
                .DivisionID = Session("DivId")
                .UserRightType = 3
                .FirstName = ""
                .LastName = ""
                .SortCharacter = "0"
                .CustName = ""
                .CurrentPage = 1
                .columnName = "cs.bintcreateddate"
                .PageSize = Session("PagingRows")
                .TotalRecords = 0
                .columnSortOrder = "Desc"
                .RegularSearchCriteria = ""
                .CustomSearchCriteria = ""
                .TotalRecords = 0

            End With

            dsCases = objCases.GetCaseListForPortal
            If dsCases IsNot Nothing AndAlso dsCases.Tables.Count > 0 Then
                dtCases = dsCases.Tables(0)
            Else
                dtCases = Nothing
            End If
            dgReptCases.DataSource = dtCases
            dgReptCases.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Function ReturnName(ByVal SDate) As String
        Try
            Dim strDate As String = ""
            If Not IsDBNull(SDate) Then
                strDate = FormattedDateFromDate(SDate, Session("DateFormat"))
                If Format(SDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                    strDate = "<font color=red>" & strDate & "</font>"
                ElseIf Format(SDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, 1, Now()), "yyyyMMdd") Then
                    strDate = "<font color=orange>" & strDate & "</font>"
                End If
            End If
            Return strDate
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub dgReptCases_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgReptCases.ItemCommand
        Try

            If e.CommandName = "case" Then
                Session("CaseID") = e.Item.Cells(0).Text
                Response.Redirect("../pagelayout/frmCustCasedtl.aspx?frm=Dashboard")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    
End Class