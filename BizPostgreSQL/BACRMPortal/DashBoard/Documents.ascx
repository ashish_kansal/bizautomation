﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Documents.ascx.vb"
    Inherits="BACRMPortal.Documents" %>
<div class="portlet" id="3">
    <div class="portlet-header">
        Documents to approve
    </div>
    <%--<table cellpadding="0" cellspacing="0">
    <tr>
        <td class="TabLeft">
            Documents to Approve
        </td>
        <td class="TabRight">
        </td>
    </tr>
</table>
    --%>
    <div class="portlet-content">
        <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="0" Height="185"
            runat="server" Width="100%" BorderColor="black" GridLines="None">
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Top">
                    <table width="100%">
                        <tr>
                            <td class="normal1" colspan="2" align="right">
                                <asp:Button Text="Save Selection" runat="server" ID="btnSave" CssClass="ybutton" />&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="normal1" align="right">
                                <div style="height: 185px; overflow: scroll;">
                                    <asp:DataGrid ID="dgDocs" runat="server" Width="100%" CssClass="dg" AllowSorting="false"
                                        AutoGenerateColumns="False" BorderColor="white">
                                        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                        <ItemStyle CssClass="is"></ItemStyle>
                                        <HeaderStyle CssClass="hs"></HeaderStyle>
                                        <Columns>
                                            <asp:BoundColumn Visible="false" DataField="numGenericDocID"></asp:BoundColumn>
                                            <asp:BoundColumn Visible="false" DataField="cDocType"></asp:BoundColumn>
                                            <asp:BoundColumn Visible="False" DataField="numRecOwner"></asp:BoundColumn>
                                            <asp:BoundColumn Visible="False" DataField="numRecID"></asp:BoundColumn>
                                            <asp:TemplateColumn HeaderText="Document Name">
                                                <ItemTemplate>
                                                    <asp:LinkButton Text='<%# Eval("vcDocName") %>' runat="server" ID="lbDocName" CommandName="Name" />
                                                    <asp:Label Text='<%# Eval("vcDocumentSection") %>' runat="server" ID="lblFileType"
                                                        Style="display: none" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Document Category">
                                                <ItemTemplate>
                                                    <asp:Label Text='<%# Eval("Category") %>' runat="server" ID="lblDocumentCategory" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Approve">
                                                <ItemTemplate>
                                                    <asp:RadioButton ID="rbApprove" Text="" runat="server" GroupName='<%# Eval("numGenericDocID") %>'
                                                        CssClass="signup" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Declined">
                                                <ItemTemplate>
                                                    <asp:RadioButton ID="rbDeclined" Text="" runat="server" GroupName='<%# Eval("numGenericDocID") %>'
                                                        CssClass="signup" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Comments">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="txtComment" CssClass="signup" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </div>
                            </td>
                        </tr>
                    </table>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>
</div>
