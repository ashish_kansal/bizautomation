Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Common
Partial Public Class frmcomAssociationTo
    Inherits System.Web.UI.Page

#Region "Variables"
    Dim vcDivIDList, vcContactIDList As String
    Dim boolAssociateDivision, boolAssociateContacts As Boolean
    Dim boolAllowSingleAssociationsDisplay As Boolean = True
#End Region
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This event is fired each time thepage is called. In this event we will 
    '''     get the data from the DB create the form.
    ''' </summary>
    ''' <param name="sender">Represents the sender object.</param>
    ''' <param name="e">Represents the EventArgs.</param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	12/29/2005	Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            boolAssociateDivision = CBool(IIf(Trim(GetQueryStringVal(Request.QueryString("enc"), "boolAssociateDiv")) <> "", GetQueryStringVal(Request.QueryString("enc"), "boolAssociateDiv"), False)) 'Associating a division
            boolAssociateContacts = CBool(IIf(Trim(GetQueryStringVal(Request.QueryString("enc"), "boolAssociateCont")) <> "", GetQueryStringVal(Request.QueryString("enc"), "boolAssociateCont"), False)) 'Copy/ Merge contacts
            If boolAssociateDivision Then                                       'Check if we are associatiating divisions or copying/ merging contacts
                vcDivIDList = GetQueryStringVal(Request.QueryString("enc"), "numDivisionId")              'Getting the Division Id from the Query String
                boolAssociateContacts = False                                   'Not Merging/ Copying Contacts
                btnMergeContacts.Visible = False                                'Make the Merge Contacts button invisible as here we are associating divisions
                btnCopyContacts.Visible = False                                 'Make the Copy Contacts button invisible as here we are associating divisions
                lblScreenHeader.Text = "Organization Association To"            'Set the screen header display
                PostInitializeControls()                                        'Call to attach click event to the checkboxes and other controls
            ElseIf boolAssociateContacts Then
                vcContactIDList = GetQueryStringVal(Request.QueryString("enc"), "numContactId")           'Get the contact Ids from the query string
                boolAssociateDivision = False                                   'Not associating a Division with another parent
                boolAssociateContacts = True                                    'Merging/ Copying Contacts
                btnAdd.Visible = False                                          'Add button is associated with Managing Associatio
                lblScreenHeader.Text = "Merge/ Copy Contacts"                   'Set the screen header display
                rqValAssociation.Enabled = False                                'Validator is not required here
            Else
                litMessage.Text = "<script language=javascript>alert('Unable to display the screen. Please contact the Administrator.');</script>" 'Display message that the screen could nto be loaded
                Exit Sub
            End If
            If Not IsPostBack Then
                If boolAssociateDivision Then
                    Dim objCommon As New CCommon 'Check if we are associatiating divisions or copying/ merging contacts
                    objCommon.sb_FillComboFromDBwithSel(ddlType, 43, Session("DomainID"))  'Fill drop down of Associations
                    BindGridOfAssociationTo()                                   'Bind the DataGrid with existing associations
                End If
                ddlCompany.Enabled = False                                      'Disable selection of the Company from the drop down
            End If
            btnClose.Attributes.Add("onclick", "return Close();")               'Attach client side event to the button to close the window using Javascript
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This function is called to bind existing associations to the Data Grid
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	12/29/2005	Modified
    ''' </history>
    '''-----------------------------------------------------------------------------
    Public Sub BindGridOfAssociationTo()
        Try
            Dim objProspects As New CProspects                          'Create a object of Prospects
            objProspects.DivisionIdList = vcDivIDList                   'Set the DivisionId Property
            objProspects.bitAssociatedTo = 1                            'Get Organizations Associated to the current Organization
            Dim dtAssocations As DataTable = objProspects.GetAssociationTo() 'Declare a DataTable
            If dtAssocations.Rows.Count > 0 Then
                If vcDivIDList.IndexOf(",") > 0 Or CInt(dtAssocations.Compute("COUNT(vcData)", "vcData = 'Subsidary'")) > 0 Then 'Get the count of subsidaries
                    ddlType.Items.Remove(ddlType.Items.FindByText("Subsidary")) 'Remove Subsidary
                End If
                If vcDivIDList.IndexOf(",") < 0 And CInt(dtAssocations.Compute("COUNT(bitParentOrg)", "bitParentOrg = 'Parent'")) > 0 Then 'Get the count parent org
                    cbParentOrgIndicator.Enabled = False                    'Disable the checkbox of Parent
                    cbParentOrgIndicator.Checked = False                'Unchecked
                Else
                    cbParentOrgIndicator.Enabled = True                     'Enable the checkbox of Parent
                End If
            End If
            Dim dvAssociation As DataView = dtAssocations.DefaultView   'Get the default view of the datatable
            dvAssociation.Sort = "Company, vcDivisionName"              'Sort by the Association From company Names and the division Names
            dgAssociation.DataSource = dvAssociation                    'Set the DataSource of the DataGrid
            dgAssociation.DataBind()                                    'Bind the DataGrid to the DataSet
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Trigerred when the Go button is clicked. This event searches for Org Names with the specified keywords
    ''' </summary>
    ''' <param name="sender">Represents the sender object.</param>
    ''' <param name="e">Represents the EventArgs.</param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	12/29/2005	Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub btnSearchOrgByKeywords_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchOrgByKeywords.Click
        Try
            Dim objOpportunities As New COpportunities                          'Create an object of Opportunities
            With objOpportunities
                .DomainID = Session("DomainID")                                 'Set the Domain ID
                .UserCntID = Session("UserContactID")
                .CompFilter = Trim(txtCompany.Text) & "%"                       'The Keyword for Search
            End With
            ddlCompany.DataSource = objOpportunities.ListCustomer().Tables(0).DefaultView 'Get the list of organizations names which match the given keystrokes
            ddlCompany.DataTextField = "vcCompanyname"                          'Set the text field for display
            ddlCompany.DataValueField = "numDivisionID"                          'Set the value field of the listbox
            ddlCompany.DataBind()                                               'Bind the listbox
            ddlCompany.Items.Insert(0, "--Select One--")                        'Add a default 1st entry which reads "Select One"
            ddlCompany.Items.FindByText("--Select One--").Value = 0
            ddlCompany.Enabled = True                                           'Enable selection of the Company from the drop down
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Trigerred when the Add button is clicked. This function adds a new assocaiton to the list of existing assocation
    ''' </summary>
    ''' <param name="sender">Represents the sender object.</param>
    ''' <param name="e">Represents the EventArgs.</param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	12/30/2005	Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            Dim objProspects As New CProspects                          'Create a new object of prospects
            Dim vcDisplayMessage As String = ""                         'Declare a String Variable which will store the message if any
            Dim vcReturnFlag As String                                  'Declare an String variable
            Dim lngDivID As Long                                        'Create a new numeric variable
            With objProspects
                .AssociationID = 0                                      'New Assocation Id
                .CompanyID = ddlCompany.SelectedItem.Value              'Set the Org Id into the class property
                .DivisionID = ddlCompany.SelectedItem.Value            'Specity the Division Id
                .AssociationType = ddlType.SelectedItem.Value           'The Association Type
                .bitDeleted = 0                                         'Deleted flag is 0 since we are adding an assocoation
                .UserCntID = Session("UserContactID")                             'Set the User Id into the class property
                .DomainID = Session("DomainID")                         'Set the Domain ID
                .BoolParentOrg = cbParentOrgIndicator.Checked           'Indicator if the current org is the parent org or not
                .BoolChildOrg = cbChildOrgIndicator.Checked             'Indicator if the current org is the child org or not
                .bitSharePortal = chkShare.Checked
                For Each lngDivID In vcDivIDList.Split(",")             'split and pick up each Association From Div Ids
                    .AssociationFrom = lngDivID                         'Set the Division Id
                    vcReturnFlag = .ManageAssociation()                 'Add the new assocition to the list of existing assocations
                    If vcReturnFlag <> "0" Then                         '0 indicates that the Association has been successfully accepted
                        If vcDisplayMessage.IndexOf(Trim(vcReturnFlag)) < 0 Then 'To stop duplicate names from being alerted
                            vcDisplayMessage &= "\'" & Trim(vcReturnFlag) & "\' already has a Parent. For association to a Parent from this screen, you will need to remove the existing association first.\n" 'Set the message
                        End If
                    End If
                Next
            End With
            BindGridOfAssociationTo()                                   'Refresh hte list of associations
            If Trim(vcDisplayMessage) <> "" Then
                litMessage.Text = "<script language=javascript>alert('" & vcDisplayMessage & "');</script>" 'Display message that the assiction to parent already exists
            End If
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Triggered as a result of an event in the Assocation DataGrid. Mainly called when Delete button is clicked here
    ''' </summary>
    ''' <param name="sender">Represents the sender object.</param>
    ''' <param name="e">Represents the EventArgs.</param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	12/30/2005	Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub dgAssociation_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgAssociation.ItemCommand
        Try
            If e.CommandName = "Delete" Then                                        'The following section is for deleting a association
                Dim objProspects As New CProspects                                  'Create a new object of Prospects
                objProspects.AssociationID = e.Item.Cells(0).Text                   'Set the id of the assocaiton from the datagrid
                objProspects.UserCntID = Session("UserContactID")                             'Set the user Id
                If objProspects.DeleteAssociation = False Then                      'Error occurred during setting the assocation active status as deleted
                    litMessage.Text = "<script language=javascript>alert('Dependent Records Exists. Cannot Be deleted.');</script>" 'Display message that the association cannot be deleted
                Else
                    BindGridOfAssociationTo()                                       'Delete successful so refresh the Associations in the DataGrid
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Triggered as a result of an binding each Assocation to the DataGrid. 
    '''     This function is used to attach the Delete button on the last cell of the datagrid
    ''' </summary>
    ''' <param name="sender">Represents the sender object.</param>
    ''' <param name="e">Represents the EventArgs.</param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	12/30/2005	Modified
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub dgAssociation_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgAssociation.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then 'The delete button appears for each row (except Header and Footer)
                Dim btnDelete As Button                                                 'Declare a button
                btnDelete = e.Item.FindControl("btnDelete")                             'Get the handle/ reference to the button 
                btnDelete.Attributes.Add("onclick", "return DeleteRecord();")           'Attach client trigger to the button click event
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Trigerred when the "Merge Contact(s) with another Parent" button is clicked. This function merges the selected contacts in another parent/ division
    ''' </summary>
    ''' <param name="sender">Represents the sender object.</param>
    ''' <param name="e">Represents the EventArgs.</param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	01/16/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub btnMergeContacts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMergeContacts.Click, btnCopyContacts.Click
        Try
            Dim objProspects As New CProspects                          'Create a new object of prospects
            With objProspects
                .CompanyID = ddlCompany.SelectedItem.Value              'Set the Org Id into the class property
                .DivisionID = ddlCompany.SelectedItem.Value            'Specify the Division Id
                .bitDeleted = 0                                         'Deleted flag is 0
                .UserCntID = Session("UserContactID")                             'Set the User Id into the class property
                .DomainID = Session("DomainID")                         'Set the Domain ID
                .ContactIdList = vcContactIDList
                Dim btnSrcButton As Button = CType(sender, Button)      'typecast to button
                If btnSrcButton.ID.ToString = "btnMergeContacts" Then   'Check for the source of the event
                    .CopyOrMergeContacts("M")                           'Merge the contacts with another parent org
                    litMessage.Text = "<script language=javascript>alert('Selected Contacts has been merged with the selected Parent Organization.\nPlease wait while the screen refreshes.');opener.document.getElementById('btnDupSearch').click();this.close();</script>" 'Display message that the operation is successful
                ElseIf btnSrcButton.ID.ToString = "btnCopyContacts" Then 'Check if the source of the event is the Copy button
                    .CopyOrMergeContacts("C")                           'Copy the contacts to another parent org
                    litMessage.Text = "<script language=javascript>alert('Selected Contacts has been copied to the selected Parent Organization.\nPlease wait while the screen refreshes.');opener.document.getElementById('btnDupSearch').click();this.close();</script>" 'Display message that the operation is successful
                End If
            End With
        Catch ex As Exception
            litMessage.Text = "<script language=javascript>alert('Unable to perform the requested operation. Please contact the Administrator.');</script>" 'Display message that the operation is unsuccessful
        End Try
    End Sub
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This method is used to databind the listboxes to the dataset
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	09/07/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Sub PostInitializeControls()
        '================================================================================
        ' Purpose: Does post initialization settings of the controls
        '
        ' History
        ' Ver   Date        Ref     Author              Reason Created
        ' 1     09/07/2005          Debasish Nag        Initialization extra settings 
        '                                               of the controls
        '================================================================================
        cbParentOrgIndicator.Attributes.Add("OnClick", "javascript: EnsureParentChildUniqueness(this);")     'calls for validation
        cbChildOrgIndicator.Attributes.Add("OnClick", "javascript: EnsureParentChildUniqueness(this);")     'calls for validation
    End Sub
End Class