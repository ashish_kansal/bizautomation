Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Common

Partial Public Class frmContactAddress : Inherits BACRMPage

    Dim lngContID As Long
    Dim strAdd As String
    ''OBSOLETE CODE.. Copy latest from BACRMUI
    'Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '    Try
    '        lngContID = GetQueryStringVal( "pqwRT")
    '        If Not IsPostBack Then
    '            Session("Help") = "Contacts"
    '            Dim objCommon As New CCommon
    '            objCommon.sb_FillComboFromDBwithSel(ddlContactLocation, 20, Session("DomainID")) 'Fill the Contact Location Combo
    '            objCommon.sb_FillComboFromDBwithSel(ddlContactLocation1, 20, Session("DomainID")) 'Fill the Contact Location Combo
    '            objCommon.sb_FillComboFromDBwithSel(ddlContactLocation2, 20, Session("DomainID")) 'Fill the Contact Location Combo
    '            objCommon.sb_FillComboFromDBwithSel(ddlContactLocation3, 20, Session("DomainID")) 'Fill the Contact Location Combo
    '            objCommon.sb_FillComboFromDBwithSel(ddlContactLocation4, 20, Session("DomainID")) 'Fill the Contact Location Combo
    '            objCommon.sb_FillComboFromDBwithSel(ddlPAddCountry, 40, Session("DomainID"))
    '            objCommon.sb_FillComboFromDBwithSel(ddlAddCountry, 40, Session("DomainID"))
    '            objCommon.sb_FillComboFromDBwithSel(ddlAddCountry1, 40, Session("DomainID"))
    '            objCommon.sb_FillComboFromDBwithSel(ddlAddCountry2, 40, Session("DomainID"))
    '            objCommon.sb_FillComboFromDBwithSel(ddlAddCountry3, 40, Session("DomainID"))
    '            objCommon.sb_FillComboFromDBwithSel(ddlAddcountry4, 40, Session("DomainID"))
    '            objCommon.sb_FillComboFromDBwithSel(ddlAddcountry4, 40, Session("DomainID"))
    '            Loadaddress()
    '        End If
    '        btnClose.Attributes.Add("onclick", "return Close()")
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Sub Loadaddress()
    '    Try
    '        Dim dtContactInfo As DataTable
    '        Dim objContacts As New CContacts
    '        objContacts.ContactID = lngContID
    '        dtContactInfo = objContacts.GetEditContactAdd
    '        TxtPAddCity.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcpCity")), "", dtContactInfo.Rows(0).Item("vcpCity"))
    '        TxtPAddStreet.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcpStreet")), "", dtContactInfo.Rows(0).Item("vcpStreet"))

    '        If Not IsDBNull(dtContactInfo.Rows(0).Item("vcpCountry")) Then
    '            If Not ddlPAddCountry.Items.FindByValue(dtContactInfo.Rows(0).Item("vcpCountry")) Is Nothing Then
    '                ddlPAddCountry.Items.FindByValue(dtContactInfo.Rows(0).Item("vcpCountry")).Selected = True
    '            End If
    '        End If
    '        If ddlPAddCountry.SelectedIndex > 0 Then
    '            FillState(ddlPAddState, ddlPAddCountry.SelectedItem.Value, Session("DomainID"))
    '            If Not IsDBNull(dtContactInfo.Rows(0).Item("vcpState")) Then
    '                If Not ddlPAddState.Items.FindByValue(dtContactInfo.Rows(0).Item("vcpState")) Is Nothing Then
    '                    ddlPAddState.Items.FindByValue(dtContactInfo.Rows(0).Item("vcpState")).Selected = True
    '                End If
    '            End If
    '        End If
    '        TxtPAddPostal.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcPPostalCode")), "", dtContactInfo.Rows(0).Item("vcPPostalCode"))

    '        TxtAddCity.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcCity")), "", dtContactInfo.Rows(0).Item("vcCity"))
    '        TxtAddStreet.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcStreet")), "", dtContactInfo.Rows(0).Item("vcStreet"))

    '        If Not IsDBNull(dtContactInfo.Rows(0).Item("vcCountry")) Then
    '            If Not ddlAddCountry.Items.FindByValue(dtContactInfo.Rows(0).Item("vcCountry")) Is Nothing Then
    '                ddlAddCountry.Items.FindByValue(dtContactInfo.Rows(0).Item("vcCountry")).Selected = True
    '            End If
    '        End If
    '        If ddlAddCountry.SelectedIndex > 0 Then
    '            FillState(ddlAddState, ddlAddCountry.SelectedItem.Value, Session("DomainID"))
    '            If Not IsDBNull(dtContactInfo.Rows(0).Item("vcState")) Then
    '                If Not ddlAddState.Items.FindByValue(dtContactInfo.Rows(0).Item("vcState")) Is Nothing Then
    '                    ddlAddState.Items.FindByValue(dtContactInfo.Rows(0).Item("vcState")).Selected = True
    '                End If
    '            End If
    '        End If
    '        TxtAddPostal.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("intPostalCode")), "", dtContactInfo.Rows(0).Item("intPostalCode"))

    '        'added 4 more address
    '        TxtAddCity1.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcCity1")), "", dtContactInfo.Rows(0).Item("vcCity1"))
    '        TxtAddStreet1.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcStreet1")), "", dtContactInfo.Rows(0).Item("vcStreet1"))

    '        If Not IsDBNull(dtContactInfo.Rows(0).Item("vcCountry1")) Then
    '            If Not ddlAddCountry1.Items.FindByValue(dtContactInfo.Rows(0).Item("vcCountry1")) Is Nothing Then
    '                ddlAddCountry1.Items.FindByValue(dtContactInfo.Rows(0).Item("vcCountry1")).Selected = True
    '            End If
    '        End If
    '        If ddlAddCountry1.SelectedIndex > 0 Then
    '            FillState(ddlAddState1, ddlAddCountry1.SelectedItem.Value, Session("DomainID"))
    '            If Not IsDBNull(dtContactInfo.Rows(0).Item("vcState1")) Then
    '                If Not ddlAddState1.Items.FindByValue(dtContactInfo.Rows(0).Item("vcState1")) Is Nothing Then
    '                    ddlAddState1.Items.FindByValue(dtContactInfo.Rows(0).Item("vcState1")).Selected = True
    '                End If
    '            End If
    '        End If
    '        TxtAddPostal1.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcPostalCode1")), "", dtContactInfo.Rows(0).Item("vcPostalCode1"))

    '        TxtAddCity2.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcCity2")), "", dtContactInfo.Rows(0).Item("vcCity2"))
    '        TxtAddStreet2.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcStreet2")), "", dtContactInfo.Rows(0).Item("vcStreet2"))

    '        If Not IsDBNull(dtContactInfo.Rows(0).Item("vcCountry1")) Then
    '            If Not ddlAddCountry2.Items.FindByValue(dtContactInfo.Rows(0).Item("vcCountry2")) Is Nothing Then
    '                ddlAddCountry2.Items.FindByValue(dtContactInfo.Rows(0).Item("vcCountry2")).Selected = True
    '            End If
    '        End If
    '        If ddlAddCountry2.SelectedIndex > 0 Then
    '            FillState(ddlAddState2, ddlAddCountry2.SelectedItem.Value, Session("DomainID"))
    '            If Not IsDBNull(dtContactInfo.Rows(0).Item("vcState2")) Then
    '                If Not ddlAddState2.Items.FindByValue(dtContactInfo.Rows(0).Item("vcState2")) Is Nothing Then
    '                    ddlAddState2.Items.FindByValue(dtContactInfo.Rows(0).Item("vcState2")).Selected = True
    '                End If
    '            End If
    '        End If
    '        TxtAddPostal2.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcPostalCode2")), "", dtContactInfo.Rows(0).Item("vcPostalCode2"))

    '        TxtAddCity3.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcCity3")), "", dtContactInfo.Rows(0).Item("vcCity3"))
    '        TxtAddStreet3.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcStreet3")), "", dtContactInfo.Rows(0).Item("vcStreet3"))

    '        If Not IsDBNull(dtContactInfo.Rows(0).Item("vcCountry3")) Then
    '            If Not ddlAddCountry3.Items.FindByValue(dtContactInfo.Rows(0).Item("vcCountry3")) Is Nothing Then
    '                ddlAddCountry3.Items.FindByValue(dtContactInfo.Rows(0).Item("vcCountry3")).Selected = True
    '            End If
    '        End If
    '        If ddlAddCountry3.SelectedIndex > 0 Then
    '            FillState(ddlAddState3, ddlAddCountry3.SelectedItem.Value, Session("DomainID"))
    '            If Not IsDBNull(dtContactInfo.Rows(0).Item("vcState3")) Then
    '                If Not ddlAddState3.Items.FindByValue(dtContactInfo.Rows(0).Item("vcState3")) Is Nothing Then
    '                    ddlAddState3.Items.FindByValue(dtContactInfo.Rows(0).Item("vcState3")).Selected = True
    '                End If
    '            End If
    '        End If
    '        TxtAddPostal3.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcPostalCode3")), "", dtContactInfo.Rows(0).Item("vcPostalCode3"))

    '        TxtAddCity4.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcCity4")), "", dtContactInfo.Rows(0).Item("vcCity4"))
    '        TxtAddStreet4.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcStreet4")), "", dtContactInfo.Rows(0).Item("vcStreet4"))

    '        If Not IsDBNull(dtContactInfo.Rows(0).Item("vcCountry4")) Then
    '            If Not ddlAddcountry4.Items.FindByValue(dtContactInfo.Rows(0).Item("vcCountry4")) Is Nothing Then
    '                ddlAddcountry4.Items.FindByValue(dtContactInfo.Rows(0).Item("vcCountry4")).Selected = True
    '            End If
    '        End If
    '        If ddlAddcountry4.SelectedIndex > 0 Then
    '            FillState(ddlAddState4, ddlAddcountry4.SelectedItem.Value, Session("DomainID"))
    '            If Not IsDBNull(dtContactInfo.Rows(0).Item("vcState4")) Then
    '                If Not ddlAddState4.Items.FindByValue(dtContactInfo.Rows(0).Item("vcState4")) Is Nothing Then
    '                    ddlAddState4.Items.FindByValue(dtContactInfo.Rows(0).Item("vcState4")).Selected = True
    '                End If
    '            End If
    '        End If
    '        TxtAddPostal4.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcPostalCode4")), "", dtContactInfo.Rows(0).Item("vcPostalCode4"))

    '        If Not IsDBNull(dtContactInfo.Rows(0).Item("vcContactLocation")) Then
    '            If Not ddlContactLocation.Items.FindByValue(dtContactInfo.Rows(0).Item("vcContactLocation")) Is Nothing Then
    '                ddlContactLocation.Items.FindByValue(dtContactInfo.Rows(0).Item("vcContactLocation")).Selected = True
    '            End If
    '        End If
    '        If Not IsDBNull(dtContactInfo.Rows(0).Item("vcContactLocation1")) Then
    '            If Not ddlContactLocation.Items.FindByValue(dtContactInfo.Rows(0).Item("vcContactLocation1")) Is Nothing Then
    '                ddlContactLocation1.Items.FindByValue(dtContactInfo.Rows(0).Item("vcContactLocation1")).Selected = True
    '            End If
    '        End If

    '        If Not IsDBNull(dtContactInfo.Rows(0).Item("vcContactLocation2")) Then
    '            If Not ddlContactLocation.Items.FindByValue(dtContactInfo.Rows(0).Item("vcContactLocation2")) Is Nothing Then
    '                ddlContactLocation2.Items.FindByValue(dtContactInfo.Rows(0).Item("vcContactLocation2")).Selected = True
    '            End If
    '        End If
    '        If Not IsDBNull(dtContactInfo.Rows(0).Item("vcContactLocation3")) Then
    '            If Not ddlContactLocation.Items.FindByValue(dtContactInfo.Rows(0).Item("vcContactLocation3")) Is Nothing Then
    '                ddlContactLocation3.Items.FindByValue(dtContactInfo.Rows(0).Item("vcContactLocation3")).Selected = True
    '            End If
    '        End If
    '        If Not IsDBNull(dtContactInfo.Rows(0).Item("vcContactLocation4")) Then
    '            If Not ddlContactLocation.Items.FindByValue(dtContactInfo.Rows(0).Item("vcContactLocation4")) Is Nothing Then
    '                ddlContactLocation4.Items.FindByValue(dtContactInfo.Rows(0).Item("vcContactLocation4")).Selected = True
    '            End If
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
    '    Try
    '        Save()
    '        Dim strScript As String = "<script language=JavaScript>window.opener.FillAddress('" & strAdd & "');</script>"
    '        If (Not Page.IsStartupScriptRegistered("clientScript")) Then
    '            Page.RegisterStartupScript("clientScript", strScript)
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub ddlAddCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAddCountry.SelectedIndexChanged
    '    Try
    '        FillState(ddlAddState, ddlAddCountry.SelectedItem.Value, Session("DomainID"))
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub ddlAddCountry1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAddCountry1.SelectedIndexChanged
    '    Try
    '        FillState(ddlAddState1, ddlAddCountry1.SelectedItem.Value, Session("DomainID"))
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub ddlAddCountry2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAddCountry2.SelectedIndexChanged
    '    Try
    '        FillState(ddlAddState2, ddlAddCountry2.SelectedItem.Value, Session("DomainID"))
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub ddlAddCountry3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAddCountry3.SelectedIndexChanged
    '    Try
    '        FillState(ddlAddState3, ddlAddCountry3.SelectedItem.Value, Session("DomainID"))
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub ddlAddcountry4_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAddcountry4.SelectedIndexChanged
    '    Try
    '        FillState(ddlAddState4, ddlAddcountry4.SelectedItem.Value, Session("DomainID"))
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub ddlPAddCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPAddCountry.SelectedIndexChanged
    '    Try
    '        FillState(ddlPAddState, ddlPAddCountry.SelectedItem.Value, Session("DomainID"))
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Sub Save()
    '    Try
    '        Dim objLeads As New CLeads
    '        With objLeads
    '            .ContactID = lngContID
    '            .City = TxtAddCity.Text
    '            .City1 = TxtAddCity1.Text
    '            .City2 = TxtAddCity2.Text
    '            .City3 = TxtAddCity3.Text
    '            .City4 = TxtAddCity4.Text
    '            .PCity = TxtPAddCity.Text
    '            .Street = TxtAddStreet.Text
    '            .Street1 = TxtAddStreet1.Text
    '            .Street2 = TxtAddStreet2.Text
    '            .Street3 = TxtAddStreet3.Text
    '            .Street4 = TxtAddStreet4.Text
    '            .PStreet = TxtPAddStreet.Text
    '            .State = ddlAddState.SelectedItem.Value
    '            .State1 = ddlAddState1.SelectedItem.Value
    '            .State2 = ddlAddState2.SelectedItem.Value
    '            .State3 = ddlAddState3.SelectedItem.Value
    '            .State4 = ddlAddState4.SelectedItem.Value
    '            .PState = ddlPAddState.SelectedItem.Value
    '            .PostalCode = TxtAddPostal.Text
    '            .PostalCode1 = TxtAddPostal1.Text
    '            .PostalCode2 = TxtAddPostal2.Text
    '            .PostalCode3 = TxtAddPostal3.Text
    '            .PostalCode4 = TxtAddPostal4.Text
    '            .PPostalCode = TxtPAddPostal.Text
    '            .Country = ddlAddCountry.SelectedItem.Value
    '            .Country1 = ddlAddCountry1.SelectedItem.Value
    '            .Country2 = ddlAddCountry2.SelectedItem.Value
    '            .Country3 = ddlAddCountry3.SelectedItem.Value
    '            .Country4 = ddlAddcountry4.SelectedItem.Value
    '            .PCountry = ddlPAddCountry.SelectedItem.Value
    '        End With

    '        If TxtPAddStreet.Text <> "" Then strAdd = TxtPAddStreet.Text
    '        If TxtPAddCity.Text <> "" Then
    '            If strAdd = "" Then
    '                strAdd = TxtPAddCity.Text
    '            Else : strAdd = strAdd & "," & TxtPAddCity.Text
    '            End If
    '        End If
    '        If ddlPAddState.SelectedValue <> 0 Then
    '            If strAdd = "" Then
    '                strAdd = ddlPAddState.SelectedItem.Text
    '            Else : strAdd = strAdd & "," & ddlPAddState.SelectedItem.Text
    '            End If
    '        End If
    '        If TxtPAddPostal.Text <> "" Then
    '            If strAdd = "" Then
    '                strAdd = TxtPAddPostal.Text
    '            Else : strAdd = strAdd & "," & TxtPAddPostal.Text
    '            End If
    '        End If
    '        If ddlPAddCountry.SelectedValue <> 0 Then
    '            If strAdd = "" Then
    '                strAdd = ddlPAddCountry.SelectedItem.Text
    '            Else : strAdd = strAdd & "," & ddlPAddCountry.SelectedItem.Text
    '            End If
    '        End If
    '        objLeads.ManageContactAddress()
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
    '    Try
    '        Save()
    '        Dim strScript As String = "<script language=JavaScript>window.opener.FillAddress('" & strAdd & "');self.close()</script>"
    '        If (Not Page.IsStartupScriptRegistered("clientScript")) Then
    '            Page.RegisterStartupScript("clientScript", strScript)
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

End Class