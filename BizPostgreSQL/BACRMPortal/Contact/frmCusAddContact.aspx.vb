Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Common

Partial Public Class frmCusAddContact : Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                
                Dim objCommon As New CCommon
                m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmContactList.aspx", Session("UserContactID"), 15, 2)
                If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then btnSave.Visible = False
                btnSave.Attributes.Add("onclick", "return Save()")
                btnClose.Attributes.Add("onclick", "return Close()")
                Dim dtTab As DataTable
                dtTab = Session("DefaultTab")
                If dtTab.Rows.Count > 0 Then
                    lbContacts.Text = "New " & IIf(IsDBNull(dtTab.Rows(0).Item("vcContact")), "Contacts", dtTab.Rows(0).Item("vcContact").ToString)
                Else : lbContacts.Text = "New Contact"
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim ContactId As Long
            'Create object of leads class
            Dim objLeads As New CLeads
            'Passing paramter to object
            'Save Additional Contact information
            With objLeads
                .DivisionID = Session("DivId")
                .FirstName = txtFirstName.Text
                .LastName = txtLastName.Text
                .ContactPhone = txtPhone.Text
                .PhoneExt = txtExt.Text
                .Email = txtEmail.Text
                .DomainID = Session("DomainID")
                .UserCntID = Session("UserContactID")
                ContactId = .CreateRecordAddContactInfo
            End With
            Session("ContactId") = ContactId
            Dim strScript As String = "<script language=JavaScript>"
            strScript += "window.opener.reDirect('../Contact/frmCstContacts.aspx'); self.close();"
            strScript += "</script>"
            If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class
