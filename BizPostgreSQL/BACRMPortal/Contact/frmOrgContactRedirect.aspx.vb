Imports BACRM.BusinessLogic.Survey
Imports BACRM.BusinessLogic.Common
Partial Public Class frmOrgContactRedirect
    Inherits BACRMPage

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            'Put user code to initialize the page here
            If IsPostBack Then                                              'Postback is necessary
                Dim dtContactInfoForURLRedirection As DataTable             'Declare a datatable
                Dim objSurveyAdministration As New SurveyAdministration     'Instantiate an object of survey administration
                Dim sFromScreen As String = IIf(hdRedirectionNewWindow.Value = 1, "SurveyRespondents", "AdvSearch") 'Indicate where the flow of screen is coming from
                If hdContactId.Value <> "" Then                             'Contact Id exists
                    Dim numContactId As Integer = hdContactId.Value         'Store the Contact Id
                    dtContactInfoForURLRedirection = objSurveyAdministration.GetContactInfoForURLRedirection(numContactId, "Cont") 'Call to get the URL parameters
                ElseIf hdDivisionId.Value <> "" Then                        'Division Id is passed
                    Dim numDivisionId As Integer = hdDivisionId.Value       'Store the Division Id
                    dtContactInfoForURLRedirection = objSurveyAdministration.GetContactInfoForURLRedirection(numDivisionId, "Div") 'Call to get the URL parameters
                End If
                Dim sBufURL As New System.Text.StringBuilder                'Instantiate a stringbuilder object
                If dtContactInfoForURLRedirection.Rows.Count > 0 Then       'There must be a contact existing
                    If hdRedirectionEntity.Value = "Contact" Then           'Redirecting to Contacts Page
                        sBufURL.Append("../Contact/frmContacts.aspx?frm=" & sFromScreen & "&CmpID=" & dtContactInfoForURLRedirection.Rows(0).Item("numCompanyId") & "&DivID=" & dtContactInfoForURLRedirection.Rows(0).Item("numDivisionId") & "&CRMTYPE=" & dtContactInfoForURLRedirection.Rows(0).Item("tintCRMType") & "&CntID=" & dtContactInfoForURLRedirection.Rows(0).Item("numContactId"))  'Create the URL string for redirection to contact details
                    Else
                        'Redirecting to Org Details Page
                        ' ''If dtContactInfoForURLRedirection.Rows(0).Item("tintCRMType") = 0 Then              'Leads
                        ' ''    sBufURL.Append("../Leads/frmLeads.aspx?frm=" & sFromScreen & "&CmpID=" & dtContactInfoForURLRedirection.Rows(0).Item("numCompanyId") & "&DivID=" & dtContactInfoForURLRedirection.Rows(0).Item("numDivisionId") & "&CntID=" & dtContactInfoForURLRedirection.Rows(0).Item("numContactId") & "&GrpID=" & dtContactInfoForURLRedirection.Rows(0).Item("numGrpId")) 'Create the URL string for redirection to Org Leads
                        ' ''ElseIf dtContactInfoForURLRedirection.Rows(0).Item("tintCRMType") = 1 Then          'Prospects
                        ' ''    sBufURL.Append("../prospects/frmProspects.aspx?frm=" & sFromScreen & "&CmpID=" & dtContactInfoForURLRedirection.Rows(0).Item("numCompanyId") & "&DivID=" & dtContactInfoForURLRedirection.Rows(0).Item("numDivisionId") & "&CRMTYPE=" & dtContactInfoForURLRedirection.Rows(0).Item("tintCRMType") & "&GrpID=" & dtContactInfoForURLRedirection.Rows(0).Item("numGrpId")) 'Create the URL string for redirection to Org Prospects
                        ' ''ElseIf dtContactInfoForURLRedirection.Rows(0).Item("tintCRMType") = 2 Then          'Accounts
                        ' ''    sBufURL.Append("../Account/frmAccounts.aspx?frm=" & sFromScreen & "&CmpID=" & dtContactInfoForURLRedirection.Rows(0).Item("numCompanyId") & "&DivID=" & dtContactInfoForURLRedirection.Rows(0).Item("numDivisionId") & "&CRMTYPE=" & dtContactInfoForURLRedirection.Rows(0).Item("tintCRMType") & "&CntID=" & dtContactInfoForURLRedirection.Rows(0).Item("numContactId")) 'Create the URL string for redirection to Org Accounts
                        ' ''End If

                        If dtContactInfoForURLRedirection.Rows(0).Item("tintCRMType") = 0 Then 'Leads
                            sBufURL.Append("../Leads/frmLeads.aspx?frm=" & sFromScreen & "&DivID=" & dtContactInfoForURLRedirection.Rows(0).Item("numDivisionId") & "&CntID=" & dtContactInfoForURLRedirection.Rows(0).Item("numContactId") & "&CommID=" & dtContactInfoForURLRedirection.Rows(0).Item("numCompanyId") & "&CRMTYPE=" & dtContactInfoForURLRedirection.Rows(0).Item("tintCRMType") & "&GrpID=" & dtContactInfoForURLRedirection.Rows(0).Item("numGrpId"))
                        ElseIf dtContactInfoForURLRedirection.Rows(0).Item("tintCRMType") = 1 Then 'Prospect
                            sBufURL.Append("../Organization/frmPartnerProsDtl.aspx?frm=" & sFromScreen & "&DivID=" & dtContactInfoForURLRedirection.Rows(0).Item("numDivisionId") & "&CntID=" & dtContactInfoForURLRedirection.Rows(0).Item("numContactId") & "&CommID=" & dtContactInfoForURLRedirection.Rows(0).Item("numCompanyId") & "&CRMTYPE=" & dtContactInfoForURLRedirection.Rows(0).Item("tintCRMType") & "&GrpID=" & dtContactInfoForURLRedirection.Rows(0).Item("numGrpId"))
                        ElseIf dtContactInfoForURLRedirection.Rows(0).Item("tintCRMType") = 2 Then ' Accounts
                            ''Response.Redirect("../Organization/frmPartnerActDtl.aspx?frm=contactdetail&DivID=" & objCommon.DivisionID & "&CntID=" & lngCntID & "&CommID=" & GetQueryStringVal("CommID") & "&frm1=" & GetQueryStringVal("frm"))
                            sBufURL.Append("../Organization/frmPartnerActDtl.aspx?frm=" & sFromScreen & "&DivID=" & dtContactInfoForURLRedirection.Rows(0).Item("numDivisionId") & "&CntID=" & dtContactInfoForURLRedirection.Rows(0).Item("numContactId") & "&CommID=" & dtContactInfoForURLRedirection.Rows(0).Item("numCompanyId") & "&CRMTYPE=" & dtContactInfoForURLRedirection.Rows(0).Item("tintCRMType") & "&GrpID=" & dtContactInfoForURLRedirection.Rows(0).Item("numGrpId"))
                        End If
                    End If
                    If hdRedirectionNewWindow.Value = 1 Then
                        litClientMessage.Text = "<script language=javascript>window.open('" & sBufURL.ToString & "','','toolbar=no,titlebar=no,left=10, top=30,width=850,height=550,scrollbars=no,resizable=yes');</script>" 'Open the URL in new window
                    Else : litClientMessage.Text = "<script language=javascript>parent.location.href='" & sBufURL.ToString & "';</script>" 'Open the URL in the same window
                    End If
                    hdContactId.Value = ""                                  'Reset the contact id which is used
                    hdDivisionId.Value = ""                                 'Reset the division id which is used
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class