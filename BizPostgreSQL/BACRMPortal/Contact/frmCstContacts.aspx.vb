Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports System.Reflection
Imports Telerik.Web.UI

Partial Class frmCstContacts : Inherits BACRMPage

    Dim lngCntID, lngDivID As Long
    Dim FromDate, ToDate As String
    Dim m_aryRightsForCusFlds(), m_aryRightsForEmail(), m_aryRightsForOpp(), m_aryRightsForPage(), m_aryRightsForAOI() As Integer

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents btnGo As System.Web.UI.WebControls.Button
    Protected WithEvents btnAssGo As System.Web.UI.WebControls.Button
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim SI As Integer = 0
    Dim SI1 As Integer = 0
    Dim SI2 As Integer = 0
    Dim frm As String = ""
    Dim frm1 As String = ""
    Dim frm2 As String = ""
    Dim objCommon As CCommon

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Session("UserContactID") = Nothing Then
                Response.Redirect("../Common/frmLogout.aspx")
            End If
            If Not GetQueryStringVal( "SI") Is Nothing Then
                SI = GetQueryStringVal( "SI")
            End If
            If Not GetQueryStringVal( "SI1") Is Nothing Then
                SI1 = GetQueryStringVal( "SI1")
            Else : SI1 = 0
            End If
            If Not GetQueryStringVal( "SI2") Is Nothing Then
                SI2 = GetQueryStringVal( "SI2")
            Else : SI2 = 0
            End If
            If Not GetQueryStringVal( "frm") Is Nothing Then
                frm = ""
                frm = GetQueryStringVal( "frm")
            Else : frm = ""
            End If
            If Not GetQueryStringVal( "frm1") Is Nothing Then
                frm1 = ""
                frm1 = GetQueryStringVal( "frm1")
            Else : frm1 = ""
            End If
            If Not GetQueryStringVal( "SI2") Is Nothing Then
                frm2 = ""
                frm2 = GetQueryStringVal( "frm2")
            Else : frm2 = ""
            End If

            lngDivID = Session("DivId")
            lngCntID = Session("ContactId")

            If Not IsPostBack Then
                objCommon = New CCommon
                m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmContacts.aspx", Session("UserContactID"), 15, 3)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("../Common/frmAuthorization.aspx")
                End If
                If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                    btnSave.Visible = False
                    btnSaveClose.Visible = False
                End If
                Dim dtTab As DataTable
                dtTab = Session("DefaultTab")
                If dtTab.Rows.Count > 0 Then
                    radOppTab.Tabs(0).Text = dtTab.Rows(0).Item("vcContact") & " Details"
                Else : radOppTab.Tabs(0).Text = "Contact Details"
                End If
                LoadDropDowns()
                LoadInformation()
            End If
            DisplayDynamicFlds()
            If Not IsPostBack Then
                If radOppTab.Tabs.Count > SI Then radOppTab.SelectedIndex = SI
            End If
            hplTo.Attributes.Add("onclick", "return OpenTo();")
            hplFrom.Attributes.Add("onclick", "return OpenFrom();")
            hplAddress.Attributes.Add("onclick", "return openAddress(" & lngCntID & ")")
            btnSave.Attributes.Add("onclick", "return Save();")
            btnSaveClose.Attributes.Add("onclick", "return Save();")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadInformation()
        Try
            Dim dtContactInfo As DataTable
            Dim objContacts As New CContacts
            objContacts.ContactID = lngCntID
            objContacts.DomainID = Session("DomainID")
            objContacts.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            dtContactInfo = objContacts.GetCntInfoForEdit1

            lblAssoCountF.Text = dtContactInfo.Rows(0).Item("AssociateCountFrom")
            lblAssoCountT.Text = dtContactInfo.Rows(0).Item("AssociateCountTo")
            hplCustomer.Text = dtContactInfo.Rows(0).Item("vcCompanyName")

            If Session("EnableIntMedPage") = 1 Then
                hplCustomer.NavigateUrl = "../pagelayout/frmCustAccountdtl.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=contactdetail&DivID=" & dtContactInfo.Rows(0).Item("numDivisionID") & "&CntID=" & lngCntID & "&frm1=" & GetQueryStringVal( "frm")
            Else : hplCustomer.NavigateUrl = "../account/frmCusAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=contactedit&DivID=" & dtContactInfo.Rows(0).Item("numDivisionID") & "&CntID=" & lngCntID & "&frm1=" & GetQueryStringVal( "frm")
            End If

            lblRecordOwner.Text = dtContactInfo.Rows(0).Item("RecordOwner")
            lblCreatedBy.Text = dtContactInfo.Rows(0).Item("CreatedBy")
            lblModifiedBy.Text = dtContactInfo.Rows(0).Item("ModifiedBy")

            txtFirstname.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcFirstName")), "", dtContactInfo.Rows(0).Item("vcFirstName"))  'display First Name
            txtLastName.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcLastName")), "", dtContactInfo.Rows(0).Item("vcLastName")) ' Display Last Name
            txtPhone.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("numPhone")), "", dtContactInfo.Rows(0).Item("numPhone"))  'display Phone
            txtEmail.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcEmail")), "", dtContactInfo.Rows(0).Item("vcEmail"))   'Display Email
            If Not IsDBNull(dtContactInfo.Rows(0).Item("vcCategory")) Then
                If Not ddlcategory.Items.FindByValue(dtContactInfo.Rows(0).Item("vcCategory")) Is Nothing Then
                    ddlcategory.Items.FindByValue(dtContactInfo.Rows(0).Item("vcCategory")).Selected = True
                End If
            End If
            If Not IsDBNull(dtContactInfo.Rows(0).Item("vcPosition")) Then
                If Not ddlPosition.Items.FindByValue(dtContactInfo.Rows(0).Item("vcPosition")) Is Nothing Then
                    ddlPosition.Items.FindByValue(dtContactInfo.Rows(0).Item("vcPosition")).Selected = True
                End If
            End If
            If Not IsDBNull(dtContactInfo.Rows(0).Item("bintDOB")) Then
                txtAg.Text = Date.Today.Year - Year(dtContactInfo.Rows(0).Item("bintDOB"))
                cal.SelectedDate = dtContactInfo.Rows(0).Item("bintDOB")
            End If

            If IsDBNull(dtContactInfo.Rows(0).Item("txtNotes")) Then
                txtComments.Text = ""   'Dispalay Comments
            Else : txtComments.Text = Server.HtmlDecode(dtContactInfo.Rows(0).Item("txtNotes"))   'Dispalay Comments
            End If

            txtExtension.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("numPhoneExtension")), "", dtContactInfo.Rows(0).Item("numPhoneExtension")) 'Display Extension
            txtMobile.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("numCell")), "", dtContactInfo.Rows(0).Item("numCell"))
            txtHome.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("NumHomePhone")), "", dtContactInfo.Rows(0).Item("NumHomePhone"))
            txtFax.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcFax")), "", dtContactInfo.Rows(0).Item("vcFax"))
            If IsDBNull(dtContactInfo.Rows(0).Item("bitOptOut")) Then
                optout.Checked = False
            Else
                If dtContactInfo.Rows(0).Item("bitOptOut") = 0 Then
                    optout.Checked = False
                Else : optout.Checked = True
                End If
            End If
            hidCompName.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcCompanyName")), "", dtContactInfo.Rows(0).Item("vcCompanyName"))
            hidEml.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcGivenName")), "", dtContactInfo.Rows(0).Item("vcGivenName"))
            txtAsstFName.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcAsstFirstName")), "", dtContactInfo.Rows(0).Item("vcAsstFirstName"))
            txtAsstLName.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcAsstLastName")), "", dtContactInfo.Rows(0).Item("vcAsstLastName"))
            txtAsstPhone.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("numAsstPhone")), "", dtContactInfo.Rows(0).Item("numAsstPhone"))
            txtAsstExt.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("numAsstExtn")), "", dtContactInfo.Rows(0).Item("numAsstExtn"))
            txtAsstEmail.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcAsstEmail")), "", dtContactInfo.Rows(0).Item("vcAsstEmail"))
            lblStreet.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcpStreet")), "", dtContactInfo.Rows(0).Item("vcpStreet") & ", ")
            lblCity.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcPCity")), "", dtContactInfo.Rows(0).Item("vcpCity") & ", ")
            lblPostal.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcPPostalCode")), "", dtContactInfo.Rows(0).Item("vcPPostalCode") & ", ")
            lblState.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("State")), "", dtContactInfo.Rows(0).Item("State") & ", ")
            lblCountry.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("Country")), "", dtContactInfo.Rows(0).Item("Country"))
            txtTitle.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcTitle")) = False, dtContactInfo.Rows(0).Item("vcTitle"), "")
            txtAltEmail.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcAltEmail")) = False, dtContactInfo.Rows(0).Item("vcAltEmail"), "")

            If Not IsDBNull(dtContactInfo.Rows(0).Item("charSex")) Then
                If Not ddlSex.Items.FindByValue(dtContactInfo.Rows(0).Item("charSex")) Is Nothing Then
                    ddlSex.Items.FindByValue(dtContactInfo.Rows(0).Item("charSex")).Selected = True 'Show the contact type as selected
                End If
            End If

            If Not ddlType.Items.FindByValue(dtContactInfo.Rows(0).Item("numContactType")) Is Nothing Then
                ddlType.Items.FindByValue(dtContactInfo.Rows(0).Item("numContactType")).Selected = True 'Show the contact type as selected
                txtContactType.Text = dtContactInfo.Rows(0).Item("numContactType")
            Else : txtContactType.Text = 0
            End If
            If Not IsDBNull(dtContactInfo.Rows(0).Item("numTeam")) Then
                If Not ddlTeam.Items.FindByValue(dtContactInfo.Rows(0).Item("numTeam")) Is Nothing Then
                    ddlTeam.Items.FindByValue(dtContactInfo.Rows(0).Item("numTeam")).Selected = True 'Show the contact type as selected
                End If
            End If

            If Not IsDBNull(dtContactInfo.Rows(0).Item("vcDepartment")) Then
                If Not ddlDepartment.Items.FindByValue(dtContactInfo.Rows(0).Item("vcDepartment")) Is Nothing Then
                    ddlDepartment.Items.FindByValue(dtContactInfo.Rows(0).Item("vcDepartment")).Selected = True 'Show the contact type as selected
                End If
            End If

            If Not IsDBNull(dtContactInfo.Rows(0).Item("numManagerID")) Then
                If Not ddlManagers.Items.FindByValue(dtContactInfo.Rows(0).Item("numManagerID")) Is Nothing Then
                    ddlManagers.Items.FindByValue(dtContactInfo.Rows(0).Item("numManagerID")).Selected = True
                End If
            End If
            'Added by Debasish Nag on 23rd Jan 2006 to incorporate Emp Status of the Contact
            If Not IsDBNull(dtContactInfo.Rows(0).Item("numEmpStatus")) Then
                If Not ddlEmpStatus.Items.FindByValue(dtContactInfo.Rows(0).Item("numEmpStatus")) Is Nothing Then
                    ddlEmpStatus.Items.FindByValue(dtContactInfo.Rows(0).Item("numEmpStatus")).Selected = True 'Show the contact type as selected
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            txtContactType.Text = ddlType.SelectedItem.Value
            SaveContactInfo()
            SaveCusField()
            LoadInformation()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub SaveContactInfo()
        Try
            Dim objContacts As New CContacts
            With objContacts
                .ContactID = lngCntID
                .Department = ddlDepartment.SelectedItem.Value
                .FirstName = txtFirstname.Text
                .LastName = txtLastName.Text
                .Email = txtEmail.Text
                .Sex = ddlSex.SelectedItem.Value
                .Position = ddlPosition.SelectedItem.Value
                .ContactPhone = txtPhone.Text
                .ContactPhoneExt = txtExtension.Text
                If (cal.SelectedDate <> "") Then .DOB = cal.SelectedDate
                .Comments = txtComments.Text
                .Category = ddlcategory.SelectedItem.Value
                .CellPhone = txtMobile.Text
                .HomePhone = txtHome.Text
                .Fax = txtFax.Text
                .AssFirstName = txtAsstFName.Text
                .AssLastName = txtAsstLName.Text
                .AssPhone = txtAsstPhone.Text
                .AssPhoneExt = txtAsstExt.Text
                .AssEmail = txtAsstEmail.Text
                .ContactType = ddlType.SelectedItem.Value
                .OptOut = optout.Checked
                .Manager = ddlManagers.SelectedItem.Value
                .Team = ddlTeam.SelectedItem.Value
                .UserCntID = Session("UserContactID")
                .EmpStatus = ddlEmpStatus.SelectedItem.Value
                .AltEmail = txtAltEmail.Text.Trim
                .Title = txtTitle.Text.Trim
            End With
            objContacts.ManageContactInfo()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            SaveContactInfo()
            SaveCusField()
            PageRedirect()
            'Response.Redirect("../Contacts/frmContactList.aspx")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadDropDowns()
        Try
            objCommon.sb_FillManagerFromDBSel(ddlManagers, Session("DomainID"), lngCntID, lngDivID)
            objCommon.sb_FillComboFromDBwithSel(ddlPosition, 41, Session("DomainID"))
            objCommon.sb_FillComboFromDBwithSel(ddlType, 8, Session("DomainID"))
            objCommon.sb_FillComboFromDBwithSel(ddlTeam, 35, Session("DomainID"))
            objCommon.sb_FillComboFromDBwithSel(ddlDepartment, 19, Session("DomainID"))
            objCommon.sb_FillComboFromDBwithSel(ddlcategory, 25, Session("DomainID"))
            objCommon.sb_FillComboFromDBwithSel(ddlEmpStatus, 44, Session("DomainID"))  'Fill the Contacts Status drop down
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub DisplayDynamicFlds()
        Try
            'm_aryRightsForCusFlds = clsAuthorization.fn_GetPageListUserRights("frmContacts.aspx", Session("UserContactID"), 11, 9)
            'If m_aryRightsForCusFlds(RIGHTSTYPE.VIEW) = 0 Then
            'Exit Sub
            'End If
            Dim strDate As String
            Dim bizCalendar As UserControl
            Dim _myUC_DueDate As PropertyInfo
            Dim PreviousRowID As Integer = 0
            Dim objRow As HtmlTableRow
            Dim objCell As HtmlTableCell
            Dim i, k As Integer
            Dim dttable As DataTable
            'Tabstrip4.Items.Clear()
            Dim ObjCus As New CustomFields
            ObjCus.locId = 4
            ObjCus.RelId = ddlType.SelectedItem.Value
            ObjCus.RecordId = lngCntID
            ObjCus.DomainID = Session("DomainID")
            dttable = ObjCus.GetCustFlds.Tables(0)
            Session("CusFields") = dttable

            If radOppTab.Tabs.Count > 1 Then
                Dim iItemcount As Integer
                iItemcount = radOppTab.Tabs.Count
                While radOppTab.Tabs.Count > 1
                    radOppTab.Tabs.RemoveAt(iItemcount - 1)
                    iItemcount = iItemcount - 1
                End While
            End If

            If dttable.Rows.Count > 0 Then
                'Main Detail Section
                k = 0
                objRow = New HtmlTableRow
                For i = 0 To dttable.Rows.Count - 1
                    If dttable.Rows(i).Item("TabId") = 0 Then
                        If k = 3 Then
                            k = 0
                            tblMain.Rows.Add(objRow)
                            objRow = New HtmlTableRow
                        End If

                        objCell = New HtmlTableCell
                        objCell.Align = "Right"
                        objCell.Attributes.Add("class", "normal1")
                        If dttable.Rows(i).Item("fld_type") <> "Link" Then
                            objCell.InnerText = dttable.Rows(i).Item("fld_label")
                        End If
                        objRow.Cells.Add(objCell)
                        If dttable.Rows(i).Item("fld_type") = "Text Box" Then
                            objCell = New HtmlTableCell
                            CreateTexBox(objRow, objCell, dttable.Rows(i).Item("fld_id"), dttable.Rows(i).Item("Value"))
                        ElseIf dttable.Rows(i).Item("fld_type") = "Drop Down List Box" Then
                            objCell = New HtmlTableCell
                            CreateDropdown(objRow, objCell, dttable.Rows(i).Item("fld_id"), CInt(dttable.Rows(i).Item("Value")), dttable.Rows(i).Item("numlistid"))
                        ElseIf dttable.Rows(i).Item("fld_type") = "Check box" Then
                            objCell = New HtmlTableCell
                            CreateChkBox(objRow, objCell, dttable.Rows(i).Item("fld_id"), CInt(dttable.Rows(i).Item("Value")))
                        ElseIf dttable.Rows(i).Item("fld_type") = "Text Area" Then
                            objCell = New HtmlTableCell
                            CreateTextArea(objRow, objCell, dttable.Rows(i).Item("fld_id"), dttable.Rows(i).Item("Value"))
                        ElseIf dttable.Rows(i).Item("fld_type") = "Date Field" Then
                            PreviousRowID = i
                            objCell = New HtmlTableCell
                            bizCalendar = LoadControl("../include/calandar.ascx")
                            bizCalendar.ID = "cal" & dttable.Rows(i).Item("fld_id")
                            objCell.Controls.Add(bizCalendar)
                            objRow.Cells.Add(objCell)
                        ElseIf dttable.Rows(i).Item("fld_type") = "Link" Then
                            objCell = New HtmlTableCell
                            CreateLink(objRow, objCell, dttable.Rows(i).Item("fld_id"), dttable.Rows(i).Item("vcURL"), lngCntID, dttable.Rows(i).Item("fld_label"))
                        End If
                        k = k + 1
                    End If
                Next
                tblMain.Rows.Add(objRow)

                'CustomField Section
                Dim Tab As RadTab
                'Dim pageView As PageView
                Dim aspTable As HtmlTable
                Dim Table As Table
                Dim tblcell As TableCell
                Dim tblRow As TableRow
                k = 0
                ViewState("TabId") = dttable.Rows(0).Item("TabId")
                ViewState("Check") = 0
                ViewState("FirstTabCreated") = 0
                For i = 0 To dttable.Rows.Count - 1
                    If dttable.Rows(i).Item("TabId") <> 0 Then
                        If ViewState("TabId") <> dttable.Rows(i).Item("TabId") Or ViewState("FirstTabCreated") = 0 Then
                            If ViewState("Check") <> 0 Then
                                aspTable.Rows.Add(objRow)
                                tblcell.Controls.Add(aspTable)
                                tblRow.Cells.Add(tblcell)
                                Table.Rows.Add(tblRow)
                                Tab.PageView.Controls.Add(Table)
                            End If
                            k = 0
                            ViewState("Check") = 1
                            '  If Not IsPostBack Then
                            ViewState("FirstTabCreated") = 1
                            ViewState("TabId") = dttable.Rows(i).Item("TabId")
                            Tab = New RadTab
                            Tab.Text = "&nbsp;&nbsp;" & dttable.Rows(i).Item("tabname") & "&nbsp;&nbsp;"
                            radOppTab.Tabs.Add(Tab)
                            'End If
                            'pageView = New PageView
                            aspTable = New HtmlTable
                            Table = New Table
                            Table.Width = Unit.Percentage(100)
                            Table.BorderColor = System.Drawing.Color.FromName("black")
                            Table.GridLines = GridLines.None
                            Table.BorderWidth = Unit.Pixel(1)
                            Table.Height = Unit.Pixel(300)
                            Table.CssClass = "aspTable"
                            tblcell = New TableCell
                            tblRow = New TableRow
                            aspTable.Width = "100%"
                            tblcell.VerticalAlign = VerticalAlign.Top
                            aspTable.Width = "100%"
                            objRow = New HtmlTableRow
                            objCell = New HtmlTableCell
                            objCell.InnerHtml = "<br>"
                            objRow.Cells.Add(objCell)
                            aspTable.Rows.Add(objRow)
                            objRow = New HtmlTableRow
                        End If
                        If k = 3 Then
                            k = 0
                            aspTable.Rows.Add(objRow)
                            objRow = New HtmlTableRow
                        End If
                        objCell = New HtmlTableCell
                        objCell.Align = "right"
                        objCell.Attributes.Add("class", "normal1")
                        If dttable.Rows(i).Item("fld_type") <> "Link" Then
                            objCell.InnerText = dttable.Rows(i).Item("fld_label")
                        End If
                        objRow.Cells.Add(objCell)
                        If dttable.Rows(i).Item("fld_type") = "Text Box" Then
                            objCell = New HtmlTableCell
                            CreateTexBox(objRow, objCell, dttable.Rows(i).Item("fld_id"), dttable.Rows(i).Item("Value"))
                        ElseIf dttable.Rows(i).Item("fld_type") = "Drop Down List Box" Then
                            objCell = New HtmlTableCell
                            CreateDropdown(objRow, objCell, dttable.Rows(i).Item("fld_id"), CInt(dttable.Rows(i).Item("Value")), dttable.Rows(i).Item("numlistid"))
                        ElseIf dttable.Rows(i).Item("fld_type") = "Check box" Then
                            objCell = New HtmlTableCell
                            CreateChkBox(objRow, objCell, dttable.Rows(i).Item("fld_id"), CInt(dttable.Rows(i).Item("Value")))
                        ElseIf dttable.Rows(i).Item("fld_type") = "Text Area" Then
                            objCell = New HtmlTableCell
                            CreateTextArea(objRow, objCell, dttable.Rows(i).Item("fld_id"), dttable.Rows(i).Item("Value"))
                        ElseIf dttable.Rows(i).Item("fld_type") = "Date Field" Then
                            objCell = New HtmlTableCell
                            bizCalendar = LoadControl("../include/calandar.ascx")
                            bizCalendar.ID = "cal" & dttable.Rows(i).Item("fld_id")
                            objCell.Controls.Add(bizCalendar)
                            objRow.Cells.Add(objCell)
                        ElseIf dttable.Rows(i).Item("fld_type") = "Frame" Then
                            objCell = New HtmlTableCell
                            Dim strFrame As String
                            Dim URL As String
                            URL = dttable.Rows(i).Item("vcURL")
                            URL = URL.Replace("RecordID", lngCntID)
                            strFrame = "<iframe src ='" & URL & "' width='100%' frameborder='0' height= '800px'></iframe>"
                            objCell.Controls.Add(New LiteralControl(strFrame))
                            objRow.Cells.Add(objCell)
                        ElseIf dttable.Rows(i).Item("fld_type") = "Link" Then
                            objCell = New HtmlTableCell
                            CreateLink(objRow, objCell, dttable.Rows(i).Item("fld_id"), dttable.Rows(i).Item("vcURL"), lngCntID, dttable.Rows(i).Item("fld_label"))
                        End If
                        k = k + 1
                    End If
                Next
                If ViewState("Check") = 1 Then
                    aspTable.Rows.Add(objRow)
                    tblcell.Controls.Add(aspTable)
                    tblRow.Cells.Add(tblcell)
                    Table.Rows.Add(tblRow)
                    Tab.PageView.Controls.Add(Table)
                End If
            End If
            Dim dvCusFields As DataView
            dvCusFields = dttable.DefaultView
            dvCusFields.RowFilter = "fld_type='Date Field'"
            Dim iViewCount As Integer
            For iViewCount = 0 To dvCusFields.Count - 1
                If Not IsDBNull(dvCusFields(iViewCount).Item("Value")) Then
                    bizCalendar = radOppTab.FindControl("cal" & dvCusFields(iViewCount).Item("fld_id"))
                    Dim _myControlType As Type = bizCalendar.GetType()
                    _myUC_DueDate = _myControlType.GetProperty("SelectedDate")
                    strDate = dvCusFields(iViewCount).Item("Value")
                    If strDate = "0" Then strDate = ""
                    If strDate <> "" Then
                        'strDate = DateFromFormattedDate(strDate, Session("DateFormat"))
                        _myUC_DueDate.SetValue(bizCalendar, strDate, Nothing)
                    End If
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub SaveCusField()
        Try
            Dim dttable As DataTable
            Dim i As Integer
            If Not Session("CusFields") Is Nothing Then

                dttable = Session("CusFields")
                For i = 0 To dttable.Rows.Count - 1
                    If dttable.Rows(i).Item("fld_type") = "Text Box" Then
                        Dim txt As TextBox
                        txt = radOppTab.FindControl(dttable.Rows(i).Item("fld_id"))
                        dttable.Rows(i).Item("Value") = txt.Text
                    ElseIf dttable.Rows(i).Item("fld_type") = "Drop Down List Box" Then
                        Dim ddl As DropDownList
                        ddl = radOppTab.FindControl(dttable.Rows(i).Item("fld_id"))
                        dttable.Rows(i).Item("Value") = CStr(ddl.SelectedItem.Value)
                    ElseIf dttable.Rows(i).Item("fld_type") = "Check box" Then
                        Dim chk As CheckBox
                        chk = radOppTab.FindControl(dttable.Rows(i).Item("fld_id"))
                        If chk.Checked = True Then
                            dttable.Rows(i).Item("Value") = "1"
                        Else : dttable.Rows(i).Item("Value") = "0"
                        End If
                    ElseIf dttable.Rows(i).Item("fld_type") = "Text Area" Then
                        Dim txt As TextBox
                        txt = radOppTab.FindControl(dttable.Rows(i).Item("fld_id"))
                        dttable.Rows(i).Item("Value") = txt.Text
                    ElseIf dttable.Rows(i).Item("fld_type") = "Date Field" Then
                        Dim BizCalendar As UserControl
                        BizCalendar = radOppTab.FindControl("cal" & dttable.Rows(i).Item("fld_id"))

                        Dim strDueDate As String
                        Dim _myControlType As Type = BizCalendar.GetType()
                        Dim _myUC_DueDate As PropertyInfo = _myControlType.GetProperty("SelectedDate")
                        strDueDate = _myUC_DueDate.GetValue(BizCalendar, Nothing)
                        If strDueDate <> "" Then
                            dttable.Rows(i).Item("Value") = strDueDate
                        Else : dttable.Rows(i).Item("Value") = ""
                        End If
                    End If
                Next

                Dim ds As New DataSet
                Dim strdetails As String
                dttable.TableName = "Table"
                ds.Tables.Add(dttable.Copy)
                strdetails = ds.GetXml
                ds.Tables.Remove(ds.Tables(0))

                Dim ObjCusfld As New CustomFields
                ObjCusfld.strDetails = strdetails
                ObjCusfld.locId = 4
                ObjCusfld.RecordId = lngCntID
                ObjCusfld.SaveCustomFldsByRecId()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub SaveAOI()
        Try
            Dim dttable As DataTable
            Dim i As Integer
            If Not Session("AOIForContact") Is Nothing Then

                dttable = Session("AOIForContact")
                For i = 0 To dttable.Rows.Count - 1
                    Dim chk As CheckBox
                    chk = radOppTab.FindControl("chkAOI" & dttable.Rows(i).Item("numAOIId"))
                    If chk.Checked = True Then
                        dttable.Rows(i).Item("Status") = 1
                    Else : dttable.Rows(i).Item("Status") = 0
                    End If
                Next
                Dim ds As New DataSet
                Dim strdetails As String
                dttable.TableName = "table"
                ds.Tables.Add(dttable.Copy)
                strdetails = ds.GetXml
                ds.Tables.Remove(ds.Tables(0))

                Dim ObjContacts As New CContacts
                ObjContacts.strAOI = strdetails
                ObjContacts.ContactID = lngCntID
                ObjContacts.SaveAOI()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            ''Response.Redirect("../Contacts/frmContactList.aspx")
            PageRedirect()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub PageRedirect()
        Try
            If GetQueryStringVal( "frm") = "OppList" Then
                Response.Redirect("../opportunity/frmCusOppList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
            ElseIf GetQueryStringVal( "frm") = "CaseList" Then
                Response.Redirect("../Cases/frmCusCaseList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
            ElseIf GetQueryStringVal( "frm") = "ProjectList" Then
                Response.Redirect("../Projects/frmCusProList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
            ElseIf GetQueryStringVal( "frm") = "Contactdtl" Then
                Response.Redirect("../pagelayout/frmCustContactdtl.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
            Else : Response.Redirect("../Contact/frmCustContactList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class