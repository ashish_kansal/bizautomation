<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="newContact.aspx.vb" Inherits="BACRMPortal.newContact" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
        <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
		<title>Contact</title>
		<script language="javascript" type="text/javascript" >
			function confirm_delete()
			{
					if (confirm('Delete selected Contact - Are you sure ?'))
						{
						    return true
						}
			       return false			
									
			} 
			function Save()
			{
		
			if (document.Form1.ddlCompany.value==0)
						{
							alert("Select Company")
							document.Form1.ddlCompany.focus()
							return false;
						}
			if (document.Form1.ddlCompany.selectedIndex==-1)
						{
							alert("Select Company")
							document.Form1.ddlCompany.focus()
							return false;
						}
		
				if (document.Form1.txtFirstName.value=="")
						{
							alert("Enter First Name")
							document.Form1.txtFirstName.focus()
							return false;
						}
				if (document.Form1.txtLastName.value=="")
						{
							alert("Enter Last Name")
							document.Form1.txtLastName.focus()
							return false;
						}
			}
			function Focus()
					{
						 if (document.Form1.ddlCompany.selectedIndex>=0)
						 {
							document.Form1.txtFirstName.focus();
						 }
						 else
						 {
							document.Form1.txtCompName.focus();
						 }
					}
		function Close()
		{
		    window.close()
		    return false;
		}
		</script>
	</HEAD>
	<body onload="Focus()" >
		<form id="Form1" method="post" runat="server">
		<asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
     <br />
	    <table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td valign="bottom">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;<asp:Label ID="lbContacts" runat="server" ></asp:Label>&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td align="right">
						<asp:Button ID="btnSaveNew" Runat="server" Text="Save &amp; New" CssClass="button"></asp:Button>
						<asp:Button ID="btnSave" Runat="server" Text="Save &amp; Close" CssClass="button"></asp:Button>
					    <asp:button id="btnClose" Runat="server" CssClass="button" Text="Close" Width="50"></asp:button>
					</td>
				</tr>
		
			</table>
			<asp:updatepanel ID="updatepanel" runat="server" ChildrenAstriggers="true" UpdateMode="Conditional" EnableViewState="true" >
        <ContentTemplate>
			<asp:table id="tblOppr" CellPadding="0" CellSpacing="0" BorderWidth="1" Height="300" Runat="server" CssClass="aspTable"
				Width="100%" BorderColor="black" GridLines="None">
				<asp:TableRow>
					<asp:TableCell VerticalAlign="Top">
						<br>
						<table width="100%" border="0">
				<tr>
				 <td rowspan="30" valign="top" >
						<img src="../images/Greenman-32.gif" />
					</td>
					<td class="text" align="right" width="180">Customer</td>
					<td>
						<asp:textbox id="txtCompName" Runat="server" width="165" cssclass="signup"></asp:textbox>
						&nbsp;
						<asp:Button ID="btnGo" Runat="server" Text="Go" CssClass="button"></asp:Button>&nbsp;
						<asp:dropdownlist id="ddlCompany" CssClass="signup" Runat="server" Width="200"></asp:dropdownlist></td>
				</tr>
			
				<tr>
					<td class="text" align="right">First Name<font color="#ff0000">*</font></td>
					<td>
						<asp:textbox id="txtFirstName" runat="server" CssClass="signup" Width="200"></asp:textbox></td>
				</tr>
				<tr>
					<td class="text" align="right">Last Name<font color="#ff0000">*</font></td>
					<td>
						<asp:textbox id="txtLastName" runat="server" CssClass="signup" Width="200"></asp:textbox></td>
				</tr>
				<tr>
					<td class="text" align="right">Phone</td>
					<td>
						<asp:textbox id="txtPhone" runat="server" CssClass="signup" Width="140" MaxLength="15"></asp:textbox>&nbsp;
						<asp:label Runat="server" id="lblExt" CssClass="text">Ext</asp:label>&nbsp;
						<asp:textbox id="txtExt" runat="server" CssClass="signup" Width="65px" MaxLength="15"></asp:textbox></td>
				</tr>
				<tr>
					<td class="text" align="right">Email</td>
					<td>
						<asp:textbox id="txtEmail" runat="server" CssClass="signup" Width="200" MaxLength="50"></asp:textbox></td>
				</tr>
				<tr>
					<td class="text" align="right">Position</td>
					<td>
						<asp:dropdownlist id="ddlPosition" CssClass="signup" Runat="server" Width="200"></asp:dropdownlist></td>
				</tr>
			</table>
					</asp:TableCell>
				</asp:TableRow>
			</asp:table>
			</ContentTemplate>
			</asp:updatepanel>
		</form>
	</body>
</HTML>
