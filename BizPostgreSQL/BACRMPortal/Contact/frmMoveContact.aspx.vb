Imports BACRM.BusinessLogic.Common 
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Prospects

Public Class frmMoveContact : Inherits BACRMPage

    Dim numDivID, numContactID As Integer
    Dim vcFrmScreen As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.ID = "frmMoveContact"

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            numContactID = GetQueryStringVal( "numContactId")                  'Get the contact Ids from the query string
            vcFrmScreen = GetQueryStringVal( "frm")                            'The referrer screen
            If Not IsPostBack Then ddlCompany.Enabled = False 'Disable selection of the Company from the drop down
            btnClose.Attributes.Add("onclick", "return Close();")               'Attach client side event to the button to close the window using Javascript
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Trigerred when the Go button is clicked. This event searches for Org Names with the specified keywords
    ''' </summary>
    ''' <param name="sender">Represents the sender object.</param>
    ''' <param name="e">Represents the EventArgs.</param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	04/26/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub btnSearchOrgByKeywords_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearchOrgByKeywords.Click
        Try

            Dim objCommon As New CCommon
            With objCommon
                .DomainID = Session("DomainID")                                                                        'Since we are searching for a company
                .UserCntID = Session("UserContactID")
                .Filter = Trim(txtCompany.Text) & "%"                       'The Keyword for Search
            End With
            ddlCompany.DataSource = objCommon.PopulateOrganization.Tables(0).DefaultView 'Get the list of organizations names which match the given keystrokes
            ddlCompany.DataTextField = "vcCompanyname"                          'Set the text field for display
            ddlCompany.DataValueField = "numDivisionID"                          'Set the value field of the listbox
            ddlCompany.DataBind()                                               'Bind the listbox
            ddlCompany.Items.Insert(0, "--Select One--")                        'Add a default 1st entry which reads "Select One"
            ddlCompany.Items.FindByText("--Select One--").Value = 0
            ddlCompany.Enabled = True                                           'Enable selection of the Company from the drop down
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This method is used to compose a message for the button
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	04/26/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Sub ComposeMoveButtonMessage()
        Try
            Dim objProspects As New CProspects                                  'Create a new object of prospects
            objProspects.DivisionID = numDivID                                  'Specify the Division Id
            Dim dtContactList As DataTable                                      'Declare a datatable to hold the contact list
            dtContactList = objProspects.GetContactsForTheDivision()            'Call to get the list of contacts
            Dim sButtonMessage As String                                        'String that will store the message
            If dtContactList.Rows.Count = 1 Then                                'The only exisitng contact in the division
                sButtonMessage = "Because this is the only contact in the Organization, the Organization will be deleted after the contact is moved. Are you sure you want to do this?." 'Message for a single contact
            Else                                                                'More than one cotnact in teh division
                sButtonMessage = "You are about to move this contact from its existing parent Organization / Division and add it to the Organization / Division you have selected. Are you sure you want to do this ?" 'Message for moving the contact
            End If
            litButtonMesg.Text = "<script language=javascript>var sButtomMessage = '" & sButtonMessage & "';</script>" 'Set the buttom meessage into a var
            btnMergeContacts.Attributes.Add("onclick", "return validateAndConfirmMove()") 'Set the click event for validation
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Trigerred when the "Merge Contact(s) with another Parent" button is clicked. This function merges the selected contacts in another parent/ division
    ''' </summary>
    ''' <param name="sender">Represents the sender object.</param>
    ''' <param name="e">Represents the EventArgs.</param>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	04/26/2006	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private Sub btnMergeContacts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMergeContacts.Click
        Try
            Dim objProspects As New CProspects                          'Create a new object of prospects
            With objProspects
                .DivisionID = ddlCompany.SelectedItem.Value            'Specify the Division Id
                .bitDeleted = 0                                         'Deleted flag is 0
                .UserCntID = Session("UserContactID")                             'Set the User Id into the class property
                .DomainID = Session("DomainID")                         'Set the Domain ID
                .ContactIdList = numContactID
                Dim btnSrcButton As Button = CType(sender, Button)      'typecast to button
                .CopyOrMergeContacts("M")                               'Merge the contacts with another parent org
                .DivisionID = numDivID                                  'Specify the Division Id
                .SyncDivAndCompOfMergedContacts()
                Dim dtContactInfoForURLRedirection
                dtContactInfoForURLRedirection = .GetContactInfoForURLRedirection(numContactID) 'Call to get the URL parameters
                Dim sBufURL As New System.Text.StringBuilder            'Instantiate a stringbuilder object
                sBufURL.Append("frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=" & vcFrmScreen & "&CntId=" & dtContactInfoForURLRedirection.Rows(0).Item("numContactId"))  'Create the URL string for redirection to contact details
                litMessage.Text = "<script language=javascript>alert('Selected Contacts has been merged with the selected Parent Organization.');opener.location.href='" & sBufURL.ToString & "';this.close();</script>" 'Display message that the operation is successful
            End With
        Catch ex As Exception
            litMessage.Text = "<script language=javascript>alert('Unable to perform the requested operation. Please contact the Administrator.');</script>" 'Display message that the operation is unsuccessful
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class
