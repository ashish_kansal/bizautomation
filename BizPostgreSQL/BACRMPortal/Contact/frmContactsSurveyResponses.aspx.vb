

Imports BACRM.BusinessLogic.Survey
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Survey
    Public Class frmContactsSurveyResponses : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        'Protected WithEvents dgContactsSurveyResponses As System.Web.UI.WebControls.DataGrid
        Dim numRespondentId As Long 'The Respondent id for the contact
        Dim numSurId As Long 'The Survey ID ID
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                'Put user code to initialize the page here
                numSurId = GetQueryStringVal( "numSurId")              'Capture the Survey ID
                numRespondentId = GetQueryStringVal( "numRespondentId") 'Capture the Respondents ID
                If Not IsPostBack Then BindContactsQuestionAndAnswers() 'Call to display the Question and the Answers which were selected by the contact
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to display the contact's Questions and his answers
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	10/02/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function BindContactsQuestionAndAnswers()
            Try
                Dim objSurvey As New SurveyAdministration                   'Declare and create a object of SurveyAdministration
                objSurvey.DomainId = Session("DomainID")                    'Set the Doamin Id
                objSurvey.SurveyExecution.SurveyExecution().RespondentID = numRespondentId 'Set the Respondent ID
                objSurvey.SurveyId = numSurId                               'Set the Survey Id
                Dim dtSurveyContactResponses As DataTable                   'Declare a datatable
                dtSurveyContactResponses = objSurvey.getSurveyContactsResponses() 'call function to get the list of available surveys

                dgContactsSurveyResponses.DataSource = dtSurveyContactResponses 'set the datasource
                dgContactsSurveyResponses.DataBind()                        'databind the datgrid
            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class
End Namespace
