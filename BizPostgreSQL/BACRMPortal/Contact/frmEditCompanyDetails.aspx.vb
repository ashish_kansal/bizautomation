﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Leads

Public Class frmEditCompanyDetails
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                LoadContactDetails()
                BindOrgDetails()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub LoadContactDetails()
        Try
            Dim dtContactInfo As DataTable
            Dim objContacts As New CContacts
            objContacts.ContactID = Session("ContactId")
            objContacts.DomainID = Session("DomainID")
            objContacts.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            dtContactInfo = objContacts.GetCntInfoForEdit1
            'hplCustomer.Text = dtContactInfo.Rows(0).Item("vcCompanyName")

            txtFirstName.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcFirstName")), "", dtContactInfo.Rows(0).Item("vcFirstName"))  'display First Name
            txtLastName.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcLastName")), "", dtContactInfo.Rows(0).Item("vcLastName")) ' Display Last Name
            txtPhone.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("numPhone")), "", dtContactInfo.Rows(0).Item("numPhone"))  'display Phone
            txtEmail.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcEmail")), "", dtContactInfo.Rows(0).Item("vcEmail"))   'Display Email
            txtExt.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("numPhoneExtension")), "", dtContactInfo.Rows(0).Item("numPhoneExtension")) 'Display Extension
            If CCommon.ToInteger(dtContactInfo.Rows(0).Item("bitOptOut")) = 0 Then
                chkOptOut.Checked = False
            Else : chkOptOut.Checked = True
            End If
            txtCellNumber.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("numCell")), "", dtContactInfo.Rows(0).Item("numCell"))
            'txtHome.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("NumHomePhone")), "", dtContactInfo.Rows(0).Item("NumHomePhone"))
            txtFax.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcFax")), "", dtContactInfo.Rows(0).Item("vcFax"))

            'lblStreet.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcpStreet")), "", dtContactInfo.Rows(0).Item("vcpStreet") & ", ")
            'lblCity.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcPCity")), "", dtContactInfo.Rows(0).Item("vcpCity") & ", ")
            'lblPostal.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcPPostalCode")), "", dtContactInfo.Rows(0).Item("vcPPostalCode") & ", ")
            'lblState.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("State")), "", dtContactInfo.Rows(0).Item("State") & ", ")
            'lblCountry.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("Country")), "", dtContactInfo.Rows(0).Item("Country"))
            'txtTitle.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcTitle")) = False, dtContactInfo.Rows(0).Item("vcTitle"), "")
            'txtAltEmail.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcAltEmail")) = False, dtContactInfo.Rows(0).Item("vcAltEmail"), "")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub BindOrgDetails()
        Try
            Dim dtComInfo As DataTable
            Dim objProspects As New CProspects
            objProspects.DivisionID = Session("DivId")
            objProspects.DomainID = Session("DomainID")
            objProspects.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            dtComInfo = objProspects.GetCompanyInfoForEdit
            txtOrgName.Text = dtComInfo.Rows(0).Item("vcCompanyName")
            If Not IsDBNull(dtComInfo.Rows(0).Item("vcWebSite")) Then
                txtOrgWebsite.Text = IIf(dtComInfo.Rows(0).Item("vcWebSite") = "", "http://", dtComInfo.Rows(0).Item("vcWebSite"))
            Else : txtOrgWebsite.Text = "http://"
            End If
            txtAddress.Text = IIf(dtComInfo.Rows(0).Item("vcBillStreet") = "", "", dtComInfo.Rows(0).Item("vcBillStreet") & ", ") & IIf(dtComInfo.Rows(0).Item("vcBillCity") = "", "", dtComInfo.Rows(0).Item("vcBillCity") & ", ") & IIf(dtComInfo.Rows(0).Item("vcBilState") = "", "", dtComInfo.Rows(0).Item("vcBilState") & ", ") & IIf(IsDBNull(dtComInfo.Rows(0).Item("vcBillPostCode")), "", dtComInfo.Rows(0).Item("vcBillPostCode") & ", ") & dtComInfo.Rows(0).Item("vcBillCountry")
            txtOrgPhone.Text = IIf(IsDBNull(dtComInfo.Rows(0).Item("vcComPhone")) = False, dtComInfo.Rows(0).Item("vcComPhone"), "")
            txtOrgFax.Text = IIf(IsDBNull(dtComInfo.Rows(0).Item("vcComFax")) = False, dtComInfo.Rows(0).Item("vcComFax"), "")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim objContacts As New CContacts
            With objContacts
                .ContactID = Session("ContactId")
                .FirstName = CCommon.PreventSqlInjection(txtFirstName.Text)
                .LastName = CCommon.PreventSqlInjection(txtLastName.Text)
                .Email = CCommon.PreventSqlInjection(txtEmail.Text)
                .ContactPhone = CCommon.PreventSqlInjection(txtPhone.Text)
                .ContactPhoneExt = CCommon.PreventSqlInjection(txtExt.Text)
                .CellPhone = CCommon.PreventSqlInjection(txtCellNumber.Text)
                .Fax = CCommon.PreventSqlInjection(txtFax.Text)
                .OptOut = chkOptOut.Checked
                .UserCntID = Session("UserContactID")
                .IsSelectiveUpdate = True
            End With
            objContacts.ManageContactInfo()

            Dim objLeads As New CLeads
            With objLeads
                .CompanyID = Session("CompID")
                .FirstName = CCommon.PreventSqlInjection(txtFirstName.Text)
                .LastName = CCommon.PreventSqlInjection(txtLastName.Text)
                .CompanyName = CCommon.PreventSqlInjection(txtOrgName.Text)
                .DivisionID = Session("DivId")
                .UserCntID = Session("UserContactID")
                .WebSite = CCommon.PreventSqlInjection(txtOrgWebsite.Text)
                .ComPhone = CCommon.PreventSqlInjection(txtOrgPhone.Text)
                .ComFax = CCommon.PreventSqlInjection(txtOrgFax.Text)
                .IsSelectiveUpdate = True
            End With
            objLeads.CreateRecordCompanyInfo()
            objLeads.ManageCompanyDivisionsInfo1()

            LoadContactDetails()
            BindOrgDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class