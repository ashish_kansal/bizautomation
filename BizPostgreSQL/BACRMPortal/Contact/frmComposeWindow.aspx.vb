'created by anoop jayaraj
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Outlook
Imports MailBee
Imports MailBee.Security
Imports MailBee.Mime
Imports MailBee.SmtpMail
Imports System.IO
Imports Infragistics.WebUI.WebHtmlEditor

Public Class frmComposeWindow : Inherits System.Web.UI.Page

    Dim objCommon As New CCommon
    Private m As Smtp = Nothing

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddlEmailtemplate As System.Web.UI.WebControls.DropDownList
    Protected WithEvents chkVCard As System.Web.UI.WebControls.CheckBox
    Protected WithEvents hplAttachments As System.Web.UI.WebControls.HyperLink
    Protected WithEvents lblAttachents As System.Web.UI.WebControls.Label
    Protected WithEvents btnTo As System.Web.UI.WebControls.Button
    Protected WithEvents btnCc As System.Web.UI.WebControls.Button
    Protected WithEvents btnBcc As System.Web.UI.WebControls.Button


    Protected WithEvents txtSubject As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnSend As System.Web.UI.WebControls.Button
    Protected WithEvents chkAddtoContactsDoc As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkAddtoOrgDoc As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkReceipt As System.Web.UI.WebControls.CheckBox
    Protected WithEvents chkTrack As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnSignature As System.Web.UI.WebControls.Button



    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not IsPostBack Then
                If GetQueryStringVal(Request.QueryString("enc"), "PickAtch") <> 1 Then Session("Attachements") = Nothing
                oEditHtml.UploadedFilesDirectory = ConfigurationManager.AppSettings("StateAndCountryList")
                oEditHtml.Text = Session("Signature")

                If Not Session("Content") Is Nothing Then
                    oEditHtml.Text = Session("Content")
                    Session("Content") = Nothing
                End If
                If GetQueryStringVal(Request.QueryString("enc"), "frm") = "outlook" Then
                    'If Not Session("EmailDesc") Is Nothing Then
                    '    oEditHtml.Text = Session("EmailDesc")
                    'End If
                    Dim objOutlook As New COutlook
                    Dim dttable As DataTable
                    objOutlook.numEmailHstrID = GetQueryStringVal(Request.QueryString("enc"), "EmailHstrId")
                    dttable = objOutlook.getMail()
                    If dttable.Rows.Count > 0 Then
                        Dim str As String
                        str = "<br>" & oEditHtml.Text
                        oEditHtml.Text = "<hr />" & dttable.Rows(0).Item("Body") & str
                    End If
                End If
                If GetQueryStringVal(Request.QueryString("enc"), "frm") = "message" Then

                    Dim str As String = ""
                    str = "<br>" & oEditHtml.Text
                    oEditHtml.Text = "<hr />" & Session("MailBody") & str
                    Session("MailBody") = Nothing
                End If

                If GetQueryStringVal(Request.QueryString("enc"), "pqwRT") <> "" Then   '''ContactID
                    ViewState("ContID") = GetQueryStringVal(Request.QueryString("enc"), "pqwRT")
                    objCommon.ContactID = GetQueryStringVal(Request.QueryString("enc"), "pqwRT")
                    objCommon.charModule = "C"
                    objCommon.GetCompanySpecificValues1()
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "CaseID") <> "" Then
                    objCommon.CaseID = GetQueryStringVal(Request.QueryString("enc"), "CaseID")
                    objCommon.charModule = "S"
                    objCommon.GetCompanySpecificValues1()
                    ViewState("ContID") = objCommon.ContactID
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "CommID") <> "" Then
                    objCommon.CommID = GetQueryStringVal(Request.QueryString("enc"), "CommID")
                    objCommon.charModule = "A"
                    objCommon.GetCompanySpecificValues1()
                    ViewState("ContID") = objCommon.ContactID
                ElseIf GetQueryStringVal(Request.QueryString("enc"), "rtyWR") <> "" Then   ''''DivisionID
                    objCommon.DivisionID = GetQueryStringVal(Request.QueryString("enc"), "rtyWR")
                    objCommon.charModule = "D"
                    objCommon.GetCompanySpecificValues1()
                    ViewState("ContID") = objCommon.ContactID
                End If

                Dim contactId As Long = 0
                contactId = objCommon.ContactID
                Dim compId As Long = 0
                compId = objCommon.CompID
                Dim DivId As Long = 0
                DivId = objCommon.DivisionID
                hplAttachments.Attributes.Add("onclick", "return OpenAttachments('" & DivId & "','" & contactId & "','" & compId & "')")
                LoadTemplates()
                autoTo.Text = GetQueryStringVal(Request.QueryString("enc"), "LsEmail")
                If GetQueryStringVal(Request.QueryString("enc"), "Subj") <> "" Then
                    txtSubject.Text = GetQueryStringVal(Request.QueryString("enc"), "Subj")
                End If

                If ViewState("ContID") Is Nothing Then
                    chkAddtoContactsDoc.Visible = False
                Else : chkAddtoOrgDoc.Visible = False
                End If
            End If
            If Not ViewState("ContID") Is Nothing Then
                '***************************************************
                '	USING CUSTOM TAG INSERTION FEATURE
                '***************************************************

                'oEdit1.CustomTagList = New String(,) { _
                '{"First Name", "##vcFirstName##"}, _
                '{"Last Name", "##vcLastName##"}, _
                '{"Company Name", "##vcCompanyName##"}, _
                '{"Shipping Address", "##Ship##"}}
                Dim ToolBarDropDown As New ToolbarDropDown
                ToolBarDropDown = oEditHtml.FindByKeyOrAction("Insert")
                ToolBarDropDown.Items.Add(New ToolbarDropDownItem("First Name", "##vcFirstName##"))
                ToolBarDropDown.Items.Add(New ToolbarDropDownItem("Last Name", "##vcLastName##"))
                ToolBarDropDown.Items.Add(New ToolbarDropDownItem("Company Name", "##vcCompanyName##"))
                ToolBarDropDown.Items.Add(New ToolbarDropDownItem("Shipping Address", "##Ship##"))
            End If

            btnTo.Attributes.Add("onclick", "return OpenContactList()")
            btnCc.Attributes.Add("onclick", "return OpenContactList()")
            btnBcc.Attributes.Add("onclick", "return OpenContactList()")
            '                hplAttachments.Attributes.Add("onclick", "return OpenAttachments('" & autoTo.Text & "')")
            btnSignature.Attributes.Add("onclick", "return OpenSignature()")
            If GetQueryStringVal(Request.QueryString("enc"), "PickAtch") = 1 Then
                If Not Session("Attachements") Is Nothing Then
                    Dim dtTable As DataTable
                    dtTable = Session("Attachements")
                    Dim k As Integer
                    lblAttachents.Text = ""
                    For k = 0 To dtTable.Rows.Count - 1
                        lblAttachents.Text = lblAttachents.Text & dtTable.Rows(k).Item("Filename") & ","
                    Next
                    lblAttachents.Text.TrimEnd(",")
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    ' modified by tarun
    ' reason : load email templates for this user only
    Sub LoadTemplates()
        Try
            Dim objCampaign As New Campaign
            Dim dtTable As DataTable
            Dim currentUser As Int32 = -1
            Dim currentDomainID As Int32 = -1
            If Not Session("UserContactID") Is Nothing And Not Session("DomainID") Is Nothing Then
                currentUser = Convert.ToInt32(Session("UserContactID").ToString())
                currentDomainID = Convert.ToInt32(Session("DomainID").ToString())
            End If
            objCampaign.UserCntID = Session("UserContactID")
            objCampaign.DomainID = Session("DomainID")
            dtTable = objCampaign.GetUserEmailTemplates
            ddlEmailtemplate.DataSource = dtTable
            ddlEmailtemplate.DataTextField = "VcDocName"
            ddlEmailtemplate.DataValueField = "numGenericDocID"
            ddlEmailtemplate.DataBind()
            ddlEmailtemplate.Items.Insert(0, "--Select One--")
            ddlEmailtemplate.Items.FindByText("--Select One--").Value = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlEmailtemplate_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlEmailtemplate.SelectedIndexChanged
        Try
            If ddlEmailtemplate.SelectedIndex > 0 Then
                Dim dtDocDetails As DataTable
                Dim objDocuments As New DocumentList
                With objDocuments
                    .GenDocID = ddlEmailtemplate.SelectedItem.Value
                    .DomainID = Session("DomainID")
                    dtDocDetails = .GetDocByGenDocID
                End With
                Dim strDesc As String
                strDesc = dtDocDetails.Rows(0).Item("vcDocdesc")

                txtSubject.Text = IIf(IsDBNull(dtDocDetails.Rows(0).Item("vcSubject")), "", dtDocDetails.Rows(0).Item("vcSubject"))
                oEditHtml.Text = strDesc
                '  oEdit1.Content = strDesc
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSend.Click
        Smtp.LicenseKey = ConfigurationManager.AppSettings("SMTPLicense")
        Try
            If HttpContext.Current.Session("SMTPServerIntegration") = False Then Exit Sub
            Dim i As Integer
            Dim objContacts As New CContacts
            Dim dtEmailBroadCast As DataTable
            Dim objFormGenericAdvSearch As New FormGenericAdvSearch
            If Not ViewState("ContID") Is Nothing Then
                objContacts.strContactIDs = ViewState("ContID")
                dtEmailBroadCast = objContacts.GetDtlsForEmailComp
            End If

            Dim lngEmailHstrID As Long

            objContacts.MessageTo = autoTo.Text
            objContacts.MessageFrom = Session("UserEmail")
            objContacts.MessageFromName = Session("ContactName")
            objContacts.Cc = autoCC.Text
            objContacts.Bcc = autoBcc.Text
            objContacts.Subject = txtSubject.Text
            objContacts.Body = oEditHtml.Text
            objContacts.ItemId = ""
            objContacts.ChangeKey = ""
            objContacts.bitRead = False
            objContacts.Type = "B"
            objContacts.tinttype = 2
            If Session("Attachements") Is Nothing Then
                objContacts.HasAttachment = False
            Else : objContacts.HasAttachment = True
            End If
            objContacts.CreatedOn = Now
            objContacts.MCategory = "B"
            objContacts.MessageFromName = ""
            objContacts.MessageToName = ""
            objContacts.CCName = ""
            objContacts.Size = 0
            objContacts.AttachmentItemId = ""
            objContacts.AttachmentType = ""
            objContacts.FileName = ""
            objContacts.DomainID = Session("DomainId")
            objContacts.UserCntID = Session("UserContactId")
            objContacts.BodyText = objCommon.StripTags(oEditHtml.Text, False)
            '  objContacts.Body = oEdit1.Content
            lngEmailHstrID = objContacts.InsertIntoEmailHstr()

            Dim intUnsussesfull As Integer = 0
            If HttpContext.Current.Session("SMTPServerIntegration") = False Then
                Exit Sub
            End If

            Dim svr As SmtpServer = New SmtpServer
            m = New Smtp
            m.Log.Enabled = True
            m.Log.Filename = "C:\aspNetEmail.log"
            m.Log.DisableOnException = False

            If HttpContext.Current.Session("SMTPAuth") = True Then
                svr.AuthMethods = AuthenticationMethods.Auto
                svr.AuthOptions = AuthenticationOptions.PreferSimpleMethods
                svr.AccountName = HttpContext.Current.Session("UserEmail")
                Dim strPassword As String
                strPassword = objCommon.Decrypt(HttpContext.Current.Session("SMTPPassword"))
                svr.Password = strPassword
            End If
            If HttpContext.Current.Session("SMTPPort") <> 0 Then svr.Port = HttpContext.Current.Session("SMTPPort")
            If HttpContext.Current.Session("bitSMTPSSL") = True Then svr.SslMode = SslStartupMode.UseStartTls
            svr.Name = HttpContext.Current.Session("SMTPServer")
            m.SmtpServers.Add(svr)

            m.From.AsString = Session("UserEmail")
            m.From.DisplayName = Session("ContactName")

            m.To.AsString = autoTo.Text
            m.Bcc.AsString = autoBcc.Text
            m.Cc.AsString = autoCC.Text
            'If chkVCard.Checked = True Then
            '    Dim vc As vCardWriter.vCard = LoadvCard()
            '    m.AddvCard(vc)
            'End If

            'matches a column name in the tabled named 'fldFirstName'
            m.Subject = txtSubject.Text

            If Not Session("Attachements") Is Nothing Then
                Dim objDocuments As New DocumentList
                Dim dtTable As DataTable
                Dim k As Integer
                dtTable = Session("Attachements")
                For k = 0 To dtTable.Rows.Count - 1
                    If chkAddtoContactsDoc.Checked = True Then
                        With objDocuments
                            .SpecDocID = 0
                            .DomainID = Session("DomainId")
                            .ContactID = Session("UserContactID")
                            .UrlType = "L"
                            .BussClass = 0
                            .DocCategory = 0
                            .FileType = ".doc"
                            .DocName = Split(dtTable.Rows(k).Item("FileLocation"), "/")(3)
                            .DocDesc = ""
                            .FileName = dtTable.Rows(k).Item("FileLocation")
                            .RecID = ViewState("ContID")
                            .strType = "C"
                            .byteMode = 0
                            .SaveSpecDocuments()
                        End With
                    End If
                    If chkAddtoOrgDoc.Checked = True Then
                        With objDocuments
                            .SpecDocID = 0
                            .DomainID = Session("DomainId")
                            .ContactID = Session("UserContactID")
                            .UrlType = "L"
                            .BussClass = 0
                            .DocCategory = 0
                            .FileType = ".doc"
                            .DocName = Split(dtTable.Rows(k).Item("FileLocation"), "/")(3)
                            .DocDesc = ""
                            .FileName = dtTable.Rows(k).Item("FileLocation")
                            .RecID = 1
                            .strType = "A"
                            .byteMode = 0
                            .SaveSpecDocuments()
                        End With
                    End If
                    objDocuments.EmailHstrID = lngEmailHstrID
                    objDocuments.FileName = objDocuments.DocName
                    objDocuments.FileLocation = dtTable.Rows(k).Item("FileLocation")
                    objDocuments.InsertAttachemnt()

                    dtTable.Rows(k).Item("FileLocation") = Replace(dtTable.Rows(k).Item("FileLocation"), "../documents/docs/", Server.MapPath("../documents/docs/"))
                    If File.Exists(dtTable.Rows(k).Item("FileLocation")) = True Then ' If Folder Does not exists create New Folder.
                        m.AddAttachment(dtTable.Rows(k).Item("FileLocation"))
                    End If
                Next
            End If

            If chkTrack.Checked = True Then
                m.BodyHtmlText = oEditHtml.Text & "<img style='display:none'  src=" & ConfigurationManager.AppSettings("EmailTracking") & "?EmailHstrID=" & lngEmailHstrID & ">"
                'm.Body = oEdit1.Content & "<img style='display:none'  src=" & ConfigurationManager.AppSettings("EmailTracking") & "?EmailHstrID=" & lngEmailHstrID & ">"
            Else
                m.BodyHtmlText = oEditHtml.Text
                'm.Body = oEdit1.Content
            End If
            'If chkReceipt.Checked = True Then
            '    m.ReturnReceipt = True
            '    m.ReturnReceiptAddress = Session("UserEmail")
            'End If
            'm.Username = "superuser"
            'm.Password = ConfigurationManager.AppSettings("superuser")
            If Not ViewState("ContID") Is Nothing Then
                m.SendMailMerge(Session("UserEmail"), Nothing, dtEmailBroadCast)
            Else : m.Send()
            End If

            lblAttachents.Text = ""
            Session("Attachements") = Nothing
            Response.Write("<script>self.close();</script>")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    ''Function LoadvCard() As vCardWriter.vCard
    ''    Try


    ''        Dim dtContactInfo As DataTable
    ''        Dim objContacts As New CContacts
    ''        objContacts.ContactID = Session("UserContactID")
    ''        objContacts.DomainID = Session("DomainID")
    ''        objContacts.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
    ''        dtContactInfo = objContacts.GetCntInfoForEdit1
    ''        If dtContactInfo.Rows.Count > 0 Then

    ''            Dim vc As New vCardWriter.vCard

    ''            'set the name information
    ''            vc.Name.FirstName = dtContactInfo.Rows(0).Item("vcFirstName")
    ''            vc.Name.LastName = dtContactInfo.Rows(0).Item("vcLastName")

    ''            'set the formatted name
    ''            vc.FormattedName.Value = dtContactInfo.Rows(0).Item("vcFirstName") & " " & dtContactInfo.Rows(0).Item("vcLastName")

    ''            'add a url for work
    ''            vc.Url.UrlAddress = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcWebSite")), "", dtContactInfo.Rows(0).Item("vcWebSite"))

    ''            'add organization name
    ''            vc.Organization.Company = dtContactInfo.Rows(0).Item("vcCompanyName")

    ''            'set the primary address -- which will be a work address in this example
    ''            Dim workAddress As New vCardWriter.DeliveryAddress(dtContactInfo.Rows(0).Item("vcStreet"), dtContactInfo.Rows(0).Item("vcCity"), dtContactInfo.Rows(0).Item("vcState"), dtContactInfo.Rows(0).Item("intPostalCode"), dtContactInfo.Rows(0).Item("vcCountry"))
    ''            workAddress.DeliveryAddressType = vCardWriter.DeliveryAddressType.WORK
    ''            vc.DeliveryAddress = workAddress

    ''            'add an additional home address
    ''            'Dim homeAddress As New vCardWriter.DeliveryAddress("100 Peachtree Ln", "Stevens Point", "WI", "54481", "USA")
    ''            'homeAddress.DeliveryAddressType = vCardWriter.DeliveryAddressType.HOME
    ''            'vc.AddProperty(homeAddress)


    ''            'set the primary phone -- which will be a work phone
    ''            vc.Telephone.AddTelephoneType(vCardWriter.TelephoneType.WORK)
    ''            vc.Telephone.AddTelephoneType(vCardWriter.TelephoneType.VOICE)
    ''            vc.Telephone.Number = dtContactInfo.Rows(0).Item("numPhone")

    ''            'add a work fax phone
    ''            Dim faxNumber As New vCardWriter.Telephone(dtContactInfo.Rows(0).Item("vcFax"))
    ''            faxNumber.AddTelephoneType(vCardWriter.TelephoneType.WORK)
    ''            faxNumber.AddTelephoneType(vCardWriter.TelephoneType.FAX)
    ''            vc.AddProperty(faxNumber)

    ''            'add a cell phone
    ''            Dim cellNumber As New vCardWriter.Telephone(dtContactInfo.Rows(0).Item("numCell"))
    ''            cellNumber.AddTelephoneType(vCardWriter.TelephoneType.CELL)
    ''            vc.AddProperty(cellNumber)

    ''            'set the email address
    ''            vc.Email.EmailAddress = dtContactInfo.Rows(0).Item("vcEmail")
    ''            vc.Email.Preferred = True

    ''            Return vc
    ''        End If
    ''    Catch ex As Exception
    ''        Throw ex
    ''    End Try
    ''End Function 'LoadvCard

    ''This event is raised before each row is merged into the EmailMessage object
    'Public Shared Sub OnBeforeRowMerge(ByVal sender As Object, ByVal e As BeforeRowMergeEventArgs)
    '    'Dim dr As DataRow = e.DataRow
    '    ''in this example, the emailAddress happens to be in the first column
    '    'Dim emailAddresses As String = dr("vcEmail").ToString()

    '    ''manually add in the address
    '    ''CType(sender, EmailMessage).AddTo("customto@custom.com")

    '    'Console.WriteLine(emailAddresses)
    'End Sub 'OnBeforeRowMerge


    ''This event is raised after each row is sent
    'Public Shared Sub OnRowSent(ByVal sender As Object, ByVal e As MergedRowSentEventArgs)
    '    'Dim m As EmailMessage = CType(sender, EmailMessage)
    '    'Dim dr As DataRow = e.Row
    '    'Dim emailAddress As String = dr("vcEmail").ToString()
    '    'If e.Success = True Then
    '    '    dr("Status") = 1
    '    'Else
    '    '    dr("Status") = 0
    '    'End If

    '    'Console.WriteLine(emailAddress)
    '    'Console.WriteLine(e.Success)
    '    'Console.WriteLine(m.SmtpData.Length)
    '    'Console.WriteLine(m.MessageId)
    'End Sub 'OnRowSent

    Private Sub autoTo_ProvideChoiceListOnCallback(ByVal sender As Object, ByVal e As Syncfusion.Web.UI.WebControls.Tools.ACUserChoiceListEventArgs) Handles autoTo.ProvideChoiceListOnCallback
        Try
            e.ChoiceList = getEmailCompany(e.Key)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Public Function getEmailCompany(ByVal key As String) As ArrayList
        Try
            Dim dtContacts As DataTable
            Dim m_aryRightsForPage() As Integer
            m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmContactList.aspx", Session("UserContactID"), 11, 2)
            Dim objContacts As New CContacts
            With objContacts
                .KeyWord = key
                .UserCntID = Session("UserContactID")
                .UserRightType = m_aryRightsForPage(RIGHTSTYPE.VIEW)
                .DomainID = Session("DomainID")
                .CurrentPage = 1
                .PageSize = 100
                .TotalRecords = 0
                .columnName = "ADC.bintcreateddate"
                If Session("Asc") = 1 Then
                    .columnSortOrder = "Desc"
                Else : .columnSortOrder = "Asc"
                End If
            End With

            dtContacts = objContacts.GetContactEmailList
            Dim customChoiceList As ArrayList = New ArrayList()
            Dim userChoiceList As ArrayList = New ArrayList()
            ' Dim key As String =            

            For Each row As DataRow In dtContacts.Rows
                Dim name As String = row("Company").ToString()
                Dim mailId As String = row("vcEmail").ToString()
                Dim items As ChoiceList = New ChoiceList(name, mailId)
                customChoiceList.Add(items)
            Next row
            Return customChoiceList
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Class ChoiceList

        Private name_Renamed As String
        Private email_Renamed As String

        Public Sub New(ByVal col1 As String, ByVal col2 As String)
            Try
                Me.name_Renamed = col1
                Me.email_Renamed = col2
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public ReadOnly Property Name() As String
            Get
                Try
                    Return Me.name_Renamed
                Catch ex As Exception
                    Throw ex
                End Try
            End Get
        End Property

        Public ReadOnly Property Email() As String
            Get
                Try
                    Return Me.email_Renamed
                Catch ex As Exception
                    Throw ex
                End Try
            End Get
        End Property

    End Class

    Private Sub autoCC_ProvideChoiceListOnCallback(ByVal sender As Object, ByVal e As Syncfusion.Web.UI.WebControls.Tools.ACUserChoiceListEventArgs) Handles autoCC.ProvideChoiceListOnCallback
        Try
            e.ChoiceList = getEmailCompany(e.Key)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub autoBcc_ProvideChoiceListOnCallback(ByVal sender As Object, ByVal e As Syncfusion.Web.UI.WebControls.Tools.ACUserChoiceListEventArgs) Handles autoBcc.ProvideChoiceListOnCallback
        Try
            e.ChoiceList = getEmailCompany(e.Key)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class

