<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmGetEmailAdd.aspx.vb"
    Inherits="frmGetEmailAdd" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <title>Email Address</title>

    <script language="javascript">
        function Close() {
            window.close()
            return false;
        }
        function FocusBox() {
            document.Form1.txtFind.focus();
        }
        function GetEmailaddress(a) {
            var str;
            str = ''
            var strI = '';
            for (var i = 2; i <= document.getElementById('dgContacts').rows.length; i++) {
                if (i < 10) {
                    strI = '0' + i
                }
                else {
                    strI = i
                }
                if (document.getElementById('dgContacts_ctl' + strI + '_chkEmail').checked == true) {
                    if (document.all) {
                        str = str + document.getElementById('dgContacts_ctl' + strI + '_lblEmail').innerText + ";"
                    } else {
                        str = str + document.getElementById('dgContacts_ctl' + strI + '_lblEmail').textContent + ";"
                    }

                }
            }
            window.opener.InsertEmail(str.substring(0, str.length - 1), a)
            window.close();

        }
        function Selectall() {

            var strI = '';
            for (var i = 2; i <= document.getElementById('dgContacts').rows.length; i++) {
                if (i < 10) {
                    strI = '0' + i
                }
                else {
                    strI = i
                }
                document.getElementById('dgContacts_ctl' + strI + '_chkEmail').checked = true

            }
            return false;
        }
    </script>

</head>
<body onload="FocusBox()">
    <form id="Form1" method="post" runat="server">
    <table width="100%">
        <tr>
            <td align="right">
                <asp:TextBox ID="txtFind" runat="server" CssClass="signup"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="btnFind" runat="server" Text="Find" Width="70" CssClass="Credit">
                </asp:Button>
            </td>
            <td>
                <asp:Button ID="btnTo" CssClass="Credit" runat="server" Text="To >>" Width="100">
                </asp:Button>
            </td>
            <td>
                <asp:Button ID="btnCc" CssClass="Credit" runat="server" Text="Cc >>" Width="100">
                </asp:Button>
            </td>
            <td>
                <asp:Button ID="btnBcc" CssClass="Credit" runat="server" Text="Bcc >>" Width="100">
                </asp:Button>
            </td>
            <td>
                <asp:Button ID="btnClose" CssClass="Credit" runat="server" Text="Close" Width="100">
                </asp:Button>
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td class="normal1" align="center">
                No of Records:
                <asp:Label ID="lblRecordCount" runat="server"></asp:Label>
            </td>
            <td align="right">
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="lblNext" runat="server" CssClass="Text_bold">Next:</asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:LinkButton ID="lnk2" runat="server">2</asp:LinkButton>
                        </td>
                        <td class="normal1">
                            <asp:LinkButton ID="lnk3" runat="server">3</asp:LinkButton>
                        </td>
                        <td class="normal1">
                            <asp:LinkButton ID="lnk4" runat="server">4</asp:LinkButton>
                        </td>
                        <td class="normal1">
                            <asp:LinkButton ID="lnk5" runat="server">5</asp:LinkButton>
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkFirst" runat="server"><div class="LinkArrow"><<</div>
                            </asp:LinkButton>
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkPrevious" runat="server"><div class="LinkArrow"><</div>
                            </asp:LinkButton>
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblPage" runat="server">Page</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtCurrrentPage" runat="server" Width="28px" CssClass="signup" AutoPostBack="True"
                                MaxLength="5"></asp:TextBox>
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblOf" runat="server">of</asp:Label>
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblTotal" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkNext" runat="server" CssClass="LinkArrow"><div class="LinkArrow">></div>
                            </asp:LinkButton>
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkLast" runat="server"><div class="LinkArrow">>></div>
                            </asp:LinkButton>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:DataGrid ID="dgContacts" AllowSorting="True" runat="server" Width="100%" CssClass="dg"
                    AutoGenerateColumns="False" BorderColor="white">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn DataField="vcFirstName" SortExpression="vcFirstName" HeaderText="<font color=white>First Name</font>">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="vcLastName" SortExpression="vcLastName" HeaderText="<font color=white>Last name</font>">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="vcEmail" SortExpression="vcEmail" HeaderText="<font color=white>Email Address</font>">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Company" SortExpression="Company" HeaderText="<font color=white>Company</font>">
                        </asp:BoundColumn>
                        <asp:TemplateColumn>
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkHEmail" runat="server"></asp:CheckBox>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblEmail" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "vcEmail") %>'>
                                </asp:Label>
                                <asp:CheckBox ID="chkEmail" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
    <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" runat="server" Style="display: none"></asp:TextBox>
    </form>
</body>
</html>
