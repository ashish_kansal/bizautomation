<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmConfContactList.aspx.vb" Inherits="BACRMPortal.frmConfContactList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
        <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
		<title>Contacts Column Customization</title>
		<script language="javascript" src="../javascript/AdvSearchScripts.js"></script>
		<script language="javascript" type="text/javascript" >
		function Save()
		{
		    	var str='';
	            for(var i=0; i<document.frmcontact.lstSelectedfld.options.length; i++)
			            {
				            var SelectedValue;
				            SelectedValue = document.frmcontact.lstSelectedfld.options[i].value;
				            str=str+ SelectedValue +','
			            }
			            document.frmcontact.hdnCol.value=str;
			          //  alert( document.frmcontact.hdnCol.value)
		}
		
		</script>
	</HEAD>
	<body >
		<form id="frmcontact" method="post" runat="server" style="LEFT: 10px; TOP: 10px; POSITION: relative;">
			<table cellSpacing="0" cellPadding="0" width="500">
				<tr>
					<td style="WIDTH: 211px" vAlign="bottom">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;Contacts 
									Layout&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td class="normal1">Contact Type</td>
					<td > <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="true" CssClass="signup" ></asp:DropDownList></td>
					<td align="right" height="23">
					<asp:Button runat="server" ID="btnSave" Text="Save" CssClass="button" Width="50" />
					<input class="button" id="btnClose" style="width:50" onclick="javascript:window.close()" type="button" value="Close">&nbsp;&nbsp;&nbsp;
					</td>
				</tr>
			</table>
			<asp:table id="tblAdvSearchFieldCustomization" Runat="server" Width="500" GridLines="None" CssClass="aspTable"
				BorderColor="black" BorderWidth="1">
				<asp:TableRow>
					<asp:TableCell>
						<table cellpadding="0" cellspacing="0" width="100%">
							
							<tr>
								<td align="center" class="normal1" valign="top">
									Available Fields<br>
									&nbsp;&nbsp;
									<asp:ListBox ID="lstAvailablefld" Runat="server" Width="150" Height="200" CssClass="signup" EnableViewState="False"></asp:ListBox>
								</td>
								<td align="center" class="normal1" valign="middle">
									<input type="button" id="btnAdd" Class="button" Value="Add >" onclick="javascript:move(document.frmcontact.lstAvailablefld,document.frmcontact.lstSelectedfld)">
									<br>
									<br>
									<input type="button" id="btnRemove" Class="button" Value="< Remove" onclick="javascript:remove(document.frmcontact.lstSelectedfld,document.frmcontact.lstAvailablefld);"></td>
								<td align="center" class="normal1">
									Selected Fields/ Choose Order<br>
									<asp:ListBox ID="lstSelectedfld" Runat="server" Width="150" Height="200" CssClass="signup" EnableViewState="False"></asp:ListBox>
								</td>
								<td align="center" class="normal1" valign="middle">
									<br>
									<br>
									<br>
									<br>
									<br>
									<input type="button" id="btnMoveupOne" Class="button" style="FONT-FAMILY: Webdings" value="5"
										onclick="javascript:MoveUp(document.frmcontact.lstSelectedfld);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<br>
									<br>
									<input type="button" id="btnMoveDownOne" Class="button" style="FONT-FAMILY: Webdings" value="6"
										onclick="javascript:MoveDown(document.frmcontact.lstSelectedfld);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<br>
									<br>
									<br>
									<br>
									<br>
									(Max 10)
								</td>
							</tr>
						</table>
					</asp:TableCell>
				
				</asp:TableRow>
				<asp:TableRow>
				    <asp:TableCell>
				               <table width="100%" >
					                    <tr>
					                            <td colspan="2" class="normal7" visible="false">
					                                     
					                            </td>
					                    </tr>
					                    <tr>
					                            <td class="normal1" visible="false">
                    					           
					                            </td>
					                           <td>
                    					                <asp:dropdownlist Visible="false" id="ddlSort" runat="server"  cssclass="signup">
							                                <asp:ListItem Value="2" Selected="true">My Contacts</asp:ListItem>
							                                <asp:ListItem Value="1">All Contacts</asp:ListItem>
							                                <asp:ListItem Value="3">Employees</asp:ListItem>
							                                <asp:ListItem Value="4">Added in Last 7 days</asp:ListItem>
							                                <asp:ListItem Value="5">Last 20 Added by me</asp:ListItem>
							                                <asp:ListItem Value="6">Last 20 Modified by me</asp:ListItem>
							                                <asp:ListItem Value="7">Favorites</asp:ListItem>
						                                </asp:dropdownlist>
					                            </td> 
					                    </tr>
					                   <tr >
					                             <td colspan="2" class="normal7">
					                                    When you click on the Relationships tab, is this the list you want to see ? 
					                                    <asp:RadioButton ID="radY" GroupName="Group" runat="server" Text="Yes" />
					                                    <asp:RadioButton ID="radN" GroupName="Group" runat="server" Checked="true" Text="No" />
					                            </td>
					                   </tr> 
					        </table>
					    
				    
				    </asp:TableCell>
				</asp:TableRow>
			</asp:table><input id="hdnCol" type="hidden" name="hdXMLString" runat="server" value="" />
			<input id="hdSave" type="hidden" name="hdSave" runat="server" value="False" />
		</form>
		</body>
</html>
