''modified by anoop jayaraj
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common

Public Class newContact : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim ContactId As Long

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not IsPostBack Then
                Dim dtTab As DataTable
                dtTab = Session("DefaultTab")
                If dtTab.Rows.Count > 0 Then
                    lbContacts.Text = "New " & IIf(IsDBNull(dtTab.Rows(0).Item("vcContact")), "Contacts", dtTab.Rows(0).Item("vcContact").ToString & "s")
                Else : lbContacts.Text = "New Contacts"
                End If
                Session("Help") = "Contacts"
                
                Dim objCommon As New CCommon
                m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmContacts.aspx", Session("UserContactID"), 28, 1)
                If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                    btnSave.Visible = False
                    Exit Sub
                Else : btnSave.Visible = True
                End If
                objCommon.sb_FillComboFromDBwithSel(ddlPosition, 41, Session("DomainID"))
                If GetQueryStringVal( "uihTR") <> "" Or GetQueryStringVal( "fghTY") <> "" Or GetQueryStringVal( "rtyWR") <> "" Or GetQueryStringVal( "tyrCV") <> "" Or GetQueryStringVal( "pluYR") <> "" Then
                    If GetQueryStringVal( "uihTR") <> "" Then
                        objCommon.ContactID = GetQueryStringVal( "uihTR")
                        objCommon.charModule = "C"
                    ElseIf GetQueryStringVal( "rtyWR") <> "" Then
                        objCommon.DivisionID = GetQueryStringVal( "rtyWR")
                        objCommon.charModule = "D"
                    ElseIf GetQueryStringVal( "tyrCV") <> "" Then
                        objCommon.ProID = GetQueryStringVal( "tyrCV")
                        objCommon.charModule = "P"
                    ElseIf GetQueryStringVal( "pluYR") <> "" Then
                        objCommon.OppID = GetQueryStringVal( "pluYR")
                        objCommon.charModule = "O"
                    ElseIf GetQueryStringVal( "fghTY") <> "" Then
                        objCommon.CaseID = GetQueryStringVal( "fghTY")
                        objCommon.charModule = "S"
                    End If
                    objCommon.GetCompanySpecificValues1()
                   Dim strCompany, strDivision As String
                    strCompany = objCommon.GetCompanyName
                    ddlCompany.Items.Add(strCompany)
                    ddlCompany.Items.FindByText(strCompany).Value = objCommon.DivisionID
                End If
            End If
            btnSave.Attributes.Add("onclick", "return Save()")
            btnSaveNew.Attributes.Add("onclick", "return Save()")
            btnClose.Attributes.Add("onclick", "return Close()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Public Function FillCustomer()
        Try

            Dim objCommon As New CCommon
            With objCommon
                .DomainID = Session("DomainID")
                .Filter = Trim(txtCompName.Text) & "%"
                .UserCntID = Session("UserContactID")
                ddlCompany.DataSource = objCommon.PopulateOrganization.Tables(0).DefaultView
                ddlCompany.DataTextField = "vcCompanyname"
                ddlCompany.DataValueField = "numDivisionID"
                ddlCompany.DataBind()
            End With
            ddlCompany.Items.Insert(0, New ListItem("---Select One---", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            CreateLead()
            Dim strScript As String = "<script language=JavaScript>"
            strScript += "window.opener.reDirectPage('../contact/frmContacts.aspx?frm=contactlist&uio78=09kji&CntId=" & ContactId & "'); self.close();"
            strScript += "</script>"
            If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            FillCustomer()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSaveNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveNew.Click
        Try
            CreateLead()
            Response.Redirect("../contact/newContact.aspx?DivID=" & ddlCompany.SelectedItem.Value)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub CreateLead()
        Try
            Dim objLeads As New CLeads
            With objLeads
                .DivisionID = ddlCompany.SelectedItem.Value
                .FirstName = txtFirstName.Text
                .LastName = txtLastName.Text
                .ContactPhone = txtPhone.Text
                .PhoneExt = txtExt.Text
                .Email = txtEmail.Text
                .Position = ddlPosition.SelectedItem.Value
                .DomainID = Session("DomainID")
                .UserCntID = Session("UserContactID")
                ContactId = .CreateRecordAddContactInfo
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class
