<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAddAttachments.aspx.vb" Inherits="BACRMPortal.frmAddAttachments" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
		<title>Attachments</title>
		<script language=javascript >
		function Upload()
		{
		 if (document.Form1.fileupload.value==''&& document.Form1.ddlDocuments.value==0)
		 {
		  alert("Browse a file to upload or select a document")
		  return false;
		  }
		}
		function Close()
		{
			var attch=document.Form1.txtAttachements.value;
			window.opener.AttachmentDetails(attch)
			window.close()
			return false;
		}
		</script>
		
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server" encType="multipart/form-data">
			<br>
			<table width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;Add 
									Attachments&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td align="right">
						<asp:Button ID="btnUpload" Runat="server" CssClass="button" Text="Upload"></asp:Button>
						<asp:Button ID="btnClose" Runat="server" CssClass="button" Width="50" Text="Close"></asp:Button>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<asp:table id="Table2" Runat="server" BorderWidth="1" Width="100%" Height="300" BorderColor="black" CssClass="aspTable"
							GridLines="None">
							<asp:TableRow>
								<asp:TableCell VerticalAlign="Top">
									<table width=100%>
										<tr>
											<td align="right" class="normal1">Upload a File
											</td>
											<td><input class="signup" id="fileupload" type="file" name="fileupload" runat="server">
											</td>
										</tr>
										<tr>
											<td align="right" class="normal1">
												Or
											</td>
										</tr>
										<tr>
											<td align="right" class="normal1">Select a file from Documents
											</td>
											<td>
												<asp:DropDownList ID="ddlDocuments" Width="200" CssClass=signup Runat="server"></asp:DropDownList>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<asp:datagrid id="dgAttachments" runat="server" CssClass="dg" Width="100%" BorderColor="white"
													AutoGenerateColumns="False">
													<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
													<ItemStyle CssClass="is"></ItemStyle>
													<HeaderStyle CssClass="hs"></HeaderStyle>
													<Columns>
													<asp:HyperLinkColumn DataNavigateUrlField=FileLocation DataTextField= Filename HeaderText="File Name" Target=_blank ></asp:HyperLinkColumn>
														<asp:TemplateColumn>
															<HeaderTemplate>
																<asp:Button ID="btnHdelete" Runat="server" CssClass="Delete" Text="X"></asp:Button>
															</HeaderTemplate>
															<ItemTemplate>
																<asp:Button ID="btnDelete" Runat="server" CssClass="Delete" Text="X" CommandName="Delete"></asp:Button>
																<asp:LinkButton ID="lnkdelete" Runat="server" Visible="false">
																	<font color="#730000">*</font></asp:LinkButton>
															</ItemTemplate>
														</asp:TemplateColumn>
													</Columns>
												</asp:datagrid>
											</td>
										</tr>
									</table>
								</asp:TableCell>
							</asp:TableRow>
						</asp:table>
					</td>
				</tr>
			</table>
			<asp:TextBox ID="txtAttachements" Runat="server" style="DISPLAY:none"></asp:TextBox>
		</form>
	</body>
</HTML>
