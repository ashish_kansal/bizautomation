<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCusAddContact.aspx.vb" Inherits="BACRMPortal.frmCusAddContact" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
		<title>Contact</title>
		<script language="javascript" type="text/javascript" >
			function confirm_delete()
			{
					if (confirm('Delete selected Contact - Are you sure ?'))
						{
						    return true
						}
			       return false			
									
			} 
			function Save()
			{
				if (document.Form1.txtFirstName.value=="")
						{
							alert("Enter First Name")
							document.Form1.txtFirstName.focus()
							return false;
						}
				if (document.Form1.txtLastName.value=="")
						{
							alert("Enter Last Name")
							document.Form1.txtLastName.focus()
							return false;
						}
			}
			function Close()
		{
		    window.close()
		    return false;
		}
		</script>
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
		
		<asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
			<br />
			<table cellspacing="0" cellpadding="0" width="100%" border="0">
				<tr>
					<td valign="bottom">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;<asp:Label ID="lbContacts" runat="server" ></asp:Label>&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<TD class="text_red"><asp:label id="lblMsg" runat="server" CssClass="text" ForeColor="#FF8080"></asp:label></TD>
					<td align="right">
					    <asp:button id="btnSave" CssClass="button" Text="Save &amp; Close" Runat="server"></asp:button>
					    <asp:button id="btnClose" Runat="server" CssClass="button" Text="Close" Width="50"></asp:button>
					 </td>
				</tr>
			</table>
   <asp:updatepanel ID="updatepanel" runat="server" ChildrenAstriggers="true" UpdateMode="Conditional" EnableViewState="true" >
		    <ContentTemplate>
		        <asp:table id="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" Runat="server" Width="100%" CssClass="aspTable"
				BorderColor="black" GridLines="None">
				<asp:TableRow>
					<asp:TableCell>
				            <br>
						            <table id="table1" cellspacing="1" cellpadding="0" width="100%" border="0">
							            <tr>
							                 <td rowspan="30" valign="top" >
						                        <img src="../images/Greenman-32.gif" />
					                        </td>
								            <TD class="text" align="right">First Name<font color="#ff0000">*</font></TD>
								            <TD><asp:textbox id="txtFirstName" runat="server" CssClass="signup" Width="150px" MaxLength="50"></asp:textbox></TD>
							            </tr>
							            <tr>
								            <TD class="text" align="right">Last Name<font color="#ff0000">*</font></TD>
								            <TD><asp:textbox id="txtLastName" runat="server" CssClass="signup" Width="150px" MaxLength="50"></asp:textbox></TD>
							            </tr>
							            <tr>
								            <TD class="text" align="right">Phone</TD>
								            <TD><asp:textbox id="txtPhone" runat="server" CssClass="signup" Width="150px" MaxLength="15"></asp:textbox>&nbsp;
									            <asp:label id="lblExt" CssClass="text" Runat="server">Ext</asp:label>&nbsp;<asp:textbox id="txtExt" runat="server" CssClass="signup" Width="65px" MaxLength="15"></asp:textbox></TD>
							            </tr>
							            <tr>
								            <TD class="text" align="right">Email</TD>
								            <TD><asp:textbox id="txtEmail" runat="server" CssClass="signup" Width="150px" MaxLength="50"></asp:textbox></TD>
							            </tr>
						            </table>
					     
			            <br />
					</asp:TableCell>
					</asp:TableRow>
				</asp:table>
			
			</ContentTemplate>
			</asp:updatepanel>
		</form>
	</body>
</html>
