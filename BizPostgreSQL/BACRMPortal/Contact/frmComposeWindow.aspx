<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" ValidateRequest="false"
    CodeBehind="frmComposeWindow.aspx.vb" Inherits="BACRMPortal.frmComposeWindow" %>

<%@ Register Assembly="Syncfusion.Tools.Web, Version=4.402.0.51, Culture=neutral, PublicKeyToken=3d67ed1f87d44c89"
    Namespace="Syncfusion.Web.UI.WebControls.Tools" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Compose</title>

    <script language="javascript">
        function OpenSignature() {
            window.open('../Marketing/frmAddSignature.aspx', '', 'toolbar=no,titlebar=no,left=300, top=100,width=305,height=280,scrollbars=no,resizable=yes');
            return false;
        }
        function OpenContactList() {
            window.open('../contact/frmGetEmailAdd.aspx', '', 'width=650,height=350,status=no,titlebar=no,scrollbars=1,resizable=true,top=110,left=250')
            return false;
        }
        function InsertEmail(str, a) {
            if (a == 1) {
                if (document.Form1.autoTo.value == '') {
                    document.Form1.autoTo.value = str;
                }
                else {
                    document.Form1.autoTo.value = document.Form1.autoTo.value + ";" + str;
                }
            }
            if (a == 2) {
                if (document.Form1.autoCC.value == '') {
                    document.Form1.autoCC.value = str;
                }
                else {
                    document.Form1.autoCC.value = document.Form1.autoCC.value + ";" + str;
                }
            }
            if (a == 3) {
                if (document.Form1.autoBcc.value == '') {
                    document.Form1.autoBcc.value = str;
                }
                else {
                    document.Form1.autoBcc.value = document.Form1.autoBcc.value + ";" + str;
                }
            }
        }
        function OpenAttachments(c, a, b) {
            window.open('../Marketing/frmAddAttachments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ContId=' + a + '&compId=' + b + '&DivId=' + c, '', 'toolbar=no,titlebar=no,left=300, top=100,width=600,height=500,scrollbars=yes,resizable=yes');
            return false;
        }
        function AttachmentDetails(a) {
            if (document.all) {
                document.getElementById('lblAttachents').innerText = '';
                document.getElementById('lblAttachents').innerText = a;

            } else {
                document.getElementById('lblAttachents').textContent = '';
                document.getElementById('lblAttachents').textContent = a;
            }
            return false;
        }
        		
       

    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <asp:ScriptManager runat="server" ID="scriptManager">
    </asp:ScriptManager>
    <table align="center">
        <tr>
            <td>
                <asp:Button ID="btnSend" Width="100" runat="server" AccessKey="S" Text="Send" CssClass="Credit"
                    Height="23"></asp:Button>
            </td>
            <td class="normal1">
                Email Template&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:DropDownList ID="ddlEmailtemplate" AutoPostBack="True" runat="server" CssClass="signup">
                </asp:DropDownList>
                &nbsp;&nbsp;&nbsp;<asp:CheckBox ID="chkTrack" Text="Track This Message" runat="server">
                </asp:CheckBox>
                &nbsp;&nbsp;<asp:Button ID="btnSignature" runat="server" Text="Signature" CssClass="Credit">
                </asp:Button>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td class="normal1">
                &nbsp;&nbsp;
                <asp:CheckBox ID="chkReceipt" Text="Delivery Receipt" runat="server"></asp:CheckBox>
                <asp:CheckBox ID="chkVCard" Text="Add vCard" runat="server"></asp:CheckBox>
                <asp:CheckBox ID="chkAddtoContactsDoc" runat="server" Text="Add Attachments to Contact Documents">
                </asp:CheckBox>
                <asp:CheckBox ID="chkAddtoOrgDoc" runat="server" Text="Add Attachments to Parent Org. Library">
                </asp:CheckBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnTo" CssClass="Credit" runat="server" Text="To" Width="100"></asp:Button>
            </td>
            <td colspan="3">
                <cc1:AutoCompleteTextBox ID="autoTo" runat="server" DDBackColor="white" CssClass="signup"
                    Width="600" DataColumnIndex="1" DelimiterChars=";">
					        <HeaderTemplate>
				                <table id="ACtable" cellspacing="0">
			                </HeaderTemplate>   
			                <AlternatingItemTemplate>
				                <tr>
					                <td align =left class="signup" ><%#DataBinder.Eval(Container.DataItem,"Name")%></td>
					                <td align=right  class ="signup" ><%#DataBinder.Eval(Container.DataItem, "Email")%></td>
				                 </tr>
			                </AlternatingItemTemplate>
			                <ItemTemplate>
				                  <tr>
					            <td align =left class ="signup"><%#DataBinder.Eval(Container.DataItem, "Name")%></td>
					            <td align=right  class ="signup" >  <%#DataBinder.Eval(Container.DataItem, "Email")%> </td>
				                 </tr>
			                  </ItemTemplate>
			                <FooterTemplate>
				                  </table>
			              </FooterTemplate>
                </cc1:AutoCompleteTextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnCc" CssClass="Credit" runat="server" Text="Cc" Width="100"></asp:Button>
            </td>
            <td colspan="3">
                <cc1:AutoCompleteTextBox ID="autoCC" runat="server" CssClass="signup" Width="600"
                    DataColumnIndex="1" DDBackColor="white" DelimiterChars=";">
					        <HeaderTemplate>
				                <table id="ACtable" cellspacing="0">
			                </HeaderTemplate>   
			                <AlternatingItemTemplate>
				                <tr>
					                <td align =left class="signup" ><%#DataBinder.Eval(Container.DataItem,"Name")%></td>
					                <td align=right  class ="signup" ><%#DataBinder.Eval(Container.DataItem, "Email")%></td>
				                 </tr>
			                </AlternatingItemTemplate>
			                <ItemTemplate>
				                  <tr>
					            <td align =left class ="signup"><%#DataBinder.Eval(Container.DataItem, "Name")%></td>
					            <td align=right  class ="signup" >  <%#DataBinder.Eval(Container.DataItem, "Email")%> </td>
				                 </tr>
			                  </ItemTemplate>
			                <FooterTemplate>
				                  </table>
			              </FooterTemplate>
                </cc1:AutoCompleteTextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnBcc" CssClass="Credit" runat="server" Text="Bcc" Width="100">
                </asp:Button>
            </td>
            <td colspan="3">
                <cc1:AutoCompleteTextBox ID="autoBcc" runat="server" CssClass="signup" Width="600"
                    DataColumnIndex="1" DDBackColor="white" DelimiterChars=";">
					        <HeaderTemplate>
				                <table id="ACtable" cellspacing="0">
			                </HeaderTemplate>   
			                <AlternatingItemTemplate>
				                <tr>
					                <td align =left class="signup" ><%#DataBinder.Eval(Container.DataItem,"Name")%></td>
					                <td align=right  class ="signup" ><%#DataBinder.Eval(Container.DataItem, "Email")%></td>
				                 </tr>
			                </AlternatingItemTemplate>
			                <ItemTemplate>
				                  <tr>
					            <td align =left class ="signup"><%#DataBinder.Eval(Container.DataItem, "Name")%></td>
					            <td align=right  class ="signup" >  <%#DataBinder.Eval(Container.DataItem, "Email")%> </td>
				                 </tr>
			                  </ItemTemplate>
			                <FooterTemplate>
				                  </table>
			              </FooterTemplate>
                </cc1:AutoCompleteTextBox>
            </td>
        </tr>
        <tr>
            <td align="right" class="normal1">
                Subject
            </td>
            <td colspan="3">
                <asp:TextBox ID="txtSubject" CssClass="signup" runat="server" Width="600"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right" class="normal1">
                <img src="../images/attachemnt.gif">
                <asp:HyperLink ID="hplAttachments" NavigateUrl="#" runat="server">Attachments</asp:HyperLink>
            </td>
            <td class="normal1" colspan="3">
                &nbsp;<asp:Label ID="lblAttachents" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td>
                <ig_spell:WebSpellChecker ID="oSpellChecker" runat="server">
                </ig_spell:WebSpellChecker>
                <ighedit:WebHtmlEditor ImageDirectory="../images/htmleditor/" Width="100%" Height="255"
                    ID="oEditHtml" SpellCheckerID="oSpellChecker" runat="server" FontFormattingList="Heading 1=<h1>&amp;Heading 2=<h2>&amp;Heading 3=<h3>&amp;Heading 4=<h4>&amp;Heading 5=<h5>&amp;Normal=<p>"
                    SpecialCharacterList="&amp;#937;,&amp;#931;,&amp;#916;,&amp;#934;,&amp;#915;,&amp;#936;,&amp;#928;,&amp;#920;,&amp;#926;,&amp;#923;,&amp;#958;,&amp;#956;,&amp;#951;,&amp;#966;,&amp;#969;,&amp;#949;,&amp;#952;,&amp;#948;,&amp;#950;,&amp;#968;,&amp;#946;,&amp;#960;,&amp;#963;,&amp;szlig;,&amp;thorn;,&amp;THORN;,&amp;#402,&amp;#1041;,&amp;#1046;,&amp;#1044;,&amp;#1062;,&amp;#1064;,&amp;#1070;,&amp;#1071;,&amp;#1073;,&amp;#1078;,&amp;#1092;,&amp;#1096;,&amp;#1102;,&amp;#1103;,&amp;#12362;,&amp;#12354;,&amp;#32117;,&amp;#25152;,&amp;AElig;,&amp;Aring;,&amp;Ccedil;,&amp;ETH;,&amp;Ntilde;,&amp;Ouml;,&amp;aelig;,&amp;aring;,&amp;atilde;,&amp;auml;,&amp;ccedil;,&amp;ecirc;,&amp;eth;,&amp;euml;,&amp;ntilde;,&amp;cent;,&amp;pound;,&amp;curren;,&amp;yen;,&amp;#8470;,&amp;#153;,&amp;copy;,&amp;reg;,&amp;#151;,@,&amp;#149;,&amp;iexcl;,&amp;#14;,&amp;#18;,&amp;#24;,&amp;#26;,&amp;#27;,&amp;brvbar;,&amp;sect;,&amp;uml;,&amp;ordf;,&amp;not;,&amp;macr;,&amp;para;,&amp;deg;,&amp;plusmn;,&amp;laquo;,&amp;raquo;,&amp;middot;,&amp;cedil;,&amp;ordm;,&amp;sup1;,&amp;sup2;,&amp;sup3;,&amp;frac14;,&amp;frac12;,&amp;frac34;,&amp;iquest;,&amp;times;,&amp;divide;"
                    FontNameList="Arial,Verdana,Tahoma,Courier New,Georgia" FontStyleList="Blue Underline=color:blue;text-decoration:underline;&amp;Red Bold=color:red;font-weight:bold;&amp;ALL CAPS=text-transform:uppercase;&amp;all lowercase=text-transform:lowercase;&amp;Reset="
                    FontSizeList="1,2,3,4,5,6,7">
                    <DialogStyle Font-Size="8pt" Font-Names="sans-serif" BorderWidth="1px" ForeColor="Black"
                        BorderStyle="Solid" BorderColor="Black" BackColor="#ECE9D8"></DialogStyle>
                    <Toolbar>
                        <ighedit:ToolbarImage Type="DoubleSeparator"></ighedit:ToolbarImage>
                        <ighedit:ToolbarButton Type="Bold"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton Type="Italic"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton Type="Underline"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton Type="Strikethrough"></ighedit:ToolbarButton>
                        <ighedit:ToolbarImage Type="Separator"></ighedit:ToolbarImage>
                        <ighedit:ToolbarButton Type="Subscript"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton Type="Superscript"></ighedit:ToolbarButton>
                        <ighedit:ToolbarImage Type="Separator"></ighedit:ToolbarImage>
                        <ighedit:ToolbarButton Type="Cut"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton Type="Copy"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton Type="Paste"></ighedit:ToolbarButton>
                        <ighedit:ToolbarImage Type="Separator"></ighedit:ToolbarImage>
                        <ighedit:ToolbarButton Type="Undo"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton Type="Redo"></ighedit:ToolbarButton>
                        <ighedit:ToolbarImage Type="Separator"></ighedit:ToolbarImage>
                        <ighedit:ToolbarButton Type="JustifyLeft"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton Type="JustifyCenter"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton Type="JustifyRight"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton Type="JustifyFull"></ighedit:ToolbarButton>
                        <ighedit:ToolbarImage Type="Separator"></ighedit:ToolbarImage>
                        <ighedit:ToolbarButton Type="Indent"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton Type="Outdent"></ighedit:ToolbarButton>
                        <ighedit:ToolbarImage Type="Separator"></ighedit:ToolbarImage>
                        <ighedit:ToolbarButton Type="UnorderedList"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton Type="OrderedList"></ighedit:ToolbarButton>
                        <ighedit:ToolbarImage Type="Separator"></ighedit:ToolbarImage>
                        <ighedit:ToolbarDialogButton Type="InsertRule">
                            <Dialog Strings="" InternalDialogType="InsertRule"></Dialog>
                        </ighedit:ToolbarDialogButton>
                        <ighedit:ToolbarImage Type="RowSeparator"></ighedit:ToolbarImage>
                        <ighedit:ToolbarImage Type="DoubleSeparator"></ighedit:ToolbarImage>
                        <ighedit:ToolbarDialogButton Type="FontColor">
                            <Dialog Strings=""></Dialog>
                        </ighedit:ToolbarDialogButton>
                        <ighedit:ToolbarDialogButton Type="FontHighlight">
                            <Dialog Strings=""></Dialog>
                        </ighedit:ToolbarDialogButton>
                        <ighedit:ToolbarDialogButton Type="SpecialCharacter">
                            <Dialog Strings="" InternalDialogType="SpecialCharacterPicker" Type="InternalWindow">
                            </Dialog>
                        </ighedit:ToolbarDialogButton>
                        <ighedit:ToolbarMenuButton Type="InsertTable">
                            <Menu Width="80px">
                                <ighedit:HtmlBoxMenuItem Act="TableProperties">
                                    <Dialog Strings="" InternalDialogType="InsertTable"></Dialog>
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem Act="InsertColumnRight">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem Act="InsertColumnLeft">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem Act="InsertRowAbove">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem Act="InsertRowBelow">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem Act="DeleteRow">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem Act="DeleteColumn">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem Act="IncreaseColspan">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem Act="DecreaseColspan">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem Act="IncreaseRowspan">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem Act="DecreaseRowspan">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem Act="CellProperties">
                                    <Dialog Strings="" InternalDialogType="CellProperties"></Dialog>
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem Act="TableProperties">
                                    <Dialog Strings="" InternalDialogType="ModifyTable"></Dialog>
                                </ighedit:HtmlBoxMenuItem>
                            </Menu>
                        </ighedit:ToolbarMenuButton>
                        <ighedit:ToolbarButton Type="ToggleBorders"></ighedit:ToolbarButton>
                        <ighedit:ToolbarImage Type="Separator"></ighedit:ToolbarImage>
                        <ighedit:ToolbarButton Type="InsertLink"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton Type="RemoveLink"></ighedit:ToolbarButton>
                        <ighedit:ToolbarImage Type="Separator"></ighedit:ToolbarImage>
                        <ighedit:ToolbarButton Type="Save" RaisePostback="True"></ighedit:ToolbarButton>
                        <ighedit:ToolbarUploadButton Type="Open">
                            <Upload Filter="*.htm,*.html,*.asp,*.aspx" Height="350px" Mode="File" Width="500px"
                                Strings=""></Upload>
                        </ighedit:ToolbarUploadButton>
                        <ighedit:ToolbarButton Type="Preview"></ighedit:ToolbarButton>
                        <ighedit:ToolbarImage Type="Separator"></ighedit:ToolbarImage>
                        <ighedit:ToolbarDialogButton Type="FindReplace">
                            <Dialog Strings="" InternalDialogType="FindReplace"></Dialog>
                        </ighedit:ToolbarDialogButton>
                        <ighedit:ToolbarDialogButton Type="InsertBookmark">
                            <Dialog Strings="" InternalDialogType="InsertBookmark"></Dialog>
                        </ighedit:ToolbarDialogButton>
                        <ighedit:ToolbarUploadButton Type="InsertImage">
                            <Upload Height="420px" Width="500px" Strings=""></Upload>
                        </ighedit:ToolbarUploadButton>
                        <ighedit:ToolbarUploadButton Type="InsertFlash">
                            <Upload Filter="*.swf" Height="440px" Mode="Flash" Width="500px" Strings=""></Upload>
                        </ighedit:ToolbarUploadButton>
                        <ighedit:ToolbarUploadButton Type="InsertWindowsMedia">
                            <Upload Filter="*.asf,*.wma,*.wmv,*.wm,*.avi,*.mpg,*.mpeg,*.m1v,*.mp2,*.mp3,*.mpa,*.mpe,*.mpv2,*.m3u,*.mid,*.midi,*.rmi,*.aif,*.aifc,*.aiff,*.au,*.snd,*.wav,*.cda,*.ivf"
                                Height="400px" Mode="WindowsMedia" Width="500px" Strings=""></Upload>
                        </ighedit:ToolbarUploadButton>
                        <ighedit:ToolbarDialogButton Type="Help">
                            <Dialog Strings="" InternalDialogType="Text"></Dialog>
                        </ighedit:ToolbarDialogButton>
                        <ighedit:ToolbarButton Type="CleanWord"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton Type="WordCount"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton Type="PasteHtml"></ighedit:ToolbarButton>
                        <ighedit:ToolbarMenuButton Type="Zoom">
                            <Menu Width="180px">
                                <ighedit:HtmlBoxMenuItem Act="Zoom25">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem Act="Zoom50">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem Act="Zoom75">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem Act="Zoom100">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem Act="Zoom200">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem Act="Zoom300">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem Act="Zoom400">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem Act="Zoom500">
                                </ighedit:HtmlBoxMenuItem>
                                <ighedit:HtmlBoxMenuItem Act="Zoom600">
                                </ighedit:HtmlBoxMenuItem>
                            </Menu>
                        </ighedit:ToolbarMenuButton>
                        <ighedit:ToolbarButton Type="TogglePositioning"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton Type="BringForward"></ighedit:ToolbarButton>
                        <ighedit:ToolbarButton Type="SendBackward"></ighedit:ToolbarButton>
                        <ighedit:ToolbarImage Type="RowSeparator"></ighedit:ToolbarImage>
                        <ighedit:ToolbarImage Type="DoubleSeparator"></ighedit:ToolbarImage>
                        <ighedit:ToolbarDropDown Type="FontName">
                        </ighedit:ToolbarDropDown>
                        <ighedit:ToolbarDropDown Type="FontSize">
                        </ighedit:ToolbarDropDown>
                        <ighedit:ToolbarDropDown Type="FontFormatting">
                        </ighedit:ToolbarDropDown>
                        <ighedit:ToolbarDropDown Type="FontStyle">
                        </ighedit:ToolbarDropDown>
                        <ighedit:ToolbarDropDown Type="Insert">
                            <Items>
                                <ighedit:ToolbarDropDownItem></ighedit:ToolbarDropDownItem>
                            </Items>
                        </ighedit:ToolbarDropDown>
                        <ighedit:ToolbarButton Type="SpellCheck"></ighedit:ToolbarButton>
                    </Toolbar>
                    <RightClickMenu>
                        <ighedit:HtmlBoxMenuItem Act="Cut">
                        </ighedit:HtmlBoxMenuItem>
                        <ighedit:HtmlBoxMenuItem Act="Copy">
                        </ighedit:HtmlBoxMenuItem>
                        <ighedit:HtmlBoxMenuItem Act="Paste">
                        </ighedit:HtmlBoxMenuItem>
                        <ighedit:HtmlBoxMenuItem Act="PasteHtml">
                        </ighedit:HtmlBoxMenuItem>
                        <ighedit:HtmlBoxMenuItem Act="CellProperties">
                            <Dialog Strings="" InternalDialogType="CellProperties"></Dialog>
                        </ighedit:HtmlBoxMenuItem>
                        <ighedit:HtmlBoxMenuItem Act="TableProperties">
                            <Dialog Strings="" InternalDialogType="ModifyTable"></Dialog>
                        </ighedit:HtmlBoxMenuItem>
                        <ighedit:HtmlBoxMenuItem Act="InsertImage">
                        </ighedit:HtmlBoxMenuItem>
                    </RightClickMenu>
                </ighedit:WebHtmlEditor>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
