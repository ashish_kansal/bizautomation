﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmEditCompanyDetails.aspx.vb"
    Inherits="BACRMPortal.frmEditCompanyDetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <title>Update Company Details</title>
    <script language="javascript" type="text/javascript">
        function Save() {
            if (trim(document.form1.txtFirstName.value) == "") {
                alert("Enter First Name")
                document.form1.txtFirstName.focus()
                return false;
            }
            if (trim(document.form1.txtLastName.value) == "") {
                alert("Enter Last Name")
                document.form1.txtLastName.focus()
                return false;
            }

            if (trim(document.form1.txtOrgName.value) == "") {
                alert("Enter Organization Name")
                document.form1.txtOrgName.focus()
                return false;
            }
        }
        function Close() {
            //window.location.href = '../Dashboard/frmPortalDashBoard.aspx';
            window.close();
            return false;
        }
        function trim(str, chars) {
            return ltrim(rtrim(str, chars), chars);
        }

        function ltrim(str, chars) {
            chars = chars || "\\s";
            return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
        }

        function rtrim(str, chars) {
            chars = chars || "\\s";
            return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <br />
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td valign="bottom">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="TabLeft">
                            Update Company Details
                        </td>
                        <td class="TabRight">
                        </td>
                    </tr>
                </table>
            </td>
            <td class="text_red">
                <asp:Label ID="lblMsg" runat="server" CssClass="text" ForeColor="#FF8080"></asp:Label>
            </td>
            <td align="right">
                <asp:Button ID="btnSave" CssClass="button" Text="Save" runat="server" OnClientClick="return Save();">
                </asp:Button>
                <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" OnClientClick="return Close();">
                </asp:Button>&nbsp;
            </td>
        </tr>
    </table>
    <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None">
        <asp:TableRow>
            <asp:TableCell>
                <br>
                <fieldset>
                    <legend>Contact Details</legend>
                    <table id="table1" cellspacing="1" cellpadding="0" width="500px" border="0">
                        <tr>
                            <td rowspan="30" valign="top" width="75px">
                                <img src="../images/Greenman-32.gif" />
                            </td>
                            <td class="text" align="right" width="140px">
                                First Name<font color="#ff0000">*</font>
                            </td>
                            <td>
                                <asp:TextBox ID="txtFirstName" runat="server" CssClass="signup" Width="150px" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="text" align="right">
                                Last Name<font color="#ff0000">*</font>
                            </td>
                            <td>
                                <asp:TextBox ID="txtLastName" runat="server" CssClass="signup" Width="150px" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="text" align="right">
                                Phone
                            </td>
                            <td>
                                <asp:TextBox ID="txtPhone" runat="server" CssClass="signup" Width="150px" MaxLength="15"></asp:TextBox>&nbsp;
                                <asp:Label ID="lblExt" CssClass="text" runat="server">Ext</asp:Label>&nbsp;<asp:TextBox
                                    ID="txtExt" runat="server" CssClass="signup" Width="50px" MaxLength="6"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="text" align="right">
                                Cell
                            </td>
                            <td>
                                <asp:TextBox ID="txtCellNumber" runat="server" CssClass="signup" Width="150px" MaxLength="15"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="text" align="right">
                                Fax
                            </td>
                            <td>
                                <asp:TextBox ID="txtFax" runat="server" CssClass="signup" Width="150px" MaxLength="15"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="text" align="right">
                                Email
                            </td>
                            <td>
                                <asp:TextBox ID="txtEmail" runat="server" CssClass="signup" Width="150px" MaxLength="50" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="text" align="right">
                                <label for="chkOptOut">Unsubscribe to Newsletter</label>
                            </td>
                            <td>
                                <asp:CheckBox runat="server" ID="chkOptOut" CssClass="signup" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <br />
                <fieldset>
                    <legend>Company Details</legend>
                    <table id="table2" cellspacing="1" cellpadding="0" width="500px" border="0">
                        <tr>
                            <td rowspan="30" valign="top" width="75px">
                                <img src="../images/Building-48.gif" />
                            </td>
                            <td class="text" align="right" width="140px">
                                Organization Name<font color="#ff0000">*</font>
                            </td>
                            <td>
                                <asp:TextBox ID="txtOrgName" runat="server" CssClass="signup" Width="150px" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="text" align="right">
                                Phone<font color="#ff0000">*</font>
                            </td>
                            <td>
                                <asp:TextBox ID="txtOrgPhone" runat="server" CssClass="signup" Width="150px" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="text" align="right">
                                Fax<font color="#ff0000">*</font>
                            </td>
                            <td>
                                <asp:TextBox ID="txtOrgFax" runat="server" CssClass="signup" Width="150px" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="text" align="right">
                                Website
                            </td>
                            <td>
                                <asp:TextBox ID="txtOrgWebsite" runat="server" CssClass="signup" Width="150px" MaxLength="50"></asp:TextBox>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="text" align="right">
                                Address
                            </td>
                            <td>
                                <asp:TextBox ID="txtAddress" runat="server" CssClass="signup" Width="150px" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <br />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    </form>
</body>
</html>
