<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmCstContacts.aspx.vb"
    Inherits="BACRMPortal.frmCstContacts" %>

<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../common/topbar.ascx" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <title>Contacts</title>
    <script language="javascript" type="text/javascript">
        function openActionItem(a, b, c, d) {
            window.location.href = "../Admin/actionitemdetails.aspx?frm=contactdetails&CommId=" + a + "&CaseId=" + b + "&CaseTimeId=" + c + "&CaseExpId=" + d;
            return false;
        }
        function OpenEmailMessage(a, b) {
            window.open("../contact/frmEmailMessage.aspx?Email=" + a + "&Date=" + b, '', 'width=750,height=525,status=no,titlebar=no,scrollbars=yes,top=110,left=250')
            return false;
        }
        function FillAddress(a) {
            if (document.all) {
                document.getElementById('uwOppTab__ctl0_lblStreet').innerText = a;
                document.getElementById('uwOppTab__ctl0_lblCity').innerText = "";
                document.getElementById('uwOppTab__ctl0_lblPostal').innerText = "";
                document.getElementById('uwOppTab__ctl0_lblState').innerText = "";
                document.getElementById('uwOppTab__ctl0_lblCountry').innerText = "";
            } else {
                document.getElementById('uwOppTab__ctl0_lblStreet').textContent = a;
                document.getElementById('uwOppTab__ctl0_lblCity').textContent = "";
                document.getElementById('uwOppTab__ctl0_lblPostal').textContent = "";
                document.getElementById('uwOppTab__ctl0_lblState').textContent = "";
                document.getElementById('uwOppTab__ctl0_lblCountry').textContent = "";
            }
            return false;
        }
        function openAddress(CntID) {
            window.open("../contact/frmContactAddress.aspx?pqwRT=" + CntID, '', 'toolbar=no,titlebar=no,top=300,width=850,height=350,scrollbars=no,resizable=no')
            return false;
        }
        function ShowWindow(Page, q, att) {
            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                //window.location.reload(true);
                return false;

            }

        }
        function fn_SendMail(txtMailAddr) {
            if (txtMailAddr != '') {
                window.open('../common/callemail.aspx?LsEmail=' + txtMailAddr, 'mail');
            }
            return false;
        }
        function OpenTo() {
            window.open("../Admin/frmToCompanyAssociation.aspx", '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenFrom() {
            window.open("../Admin/frmFromCompanyAssociation.aspx", '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenLast10OpenAct(a) {
            window.open("../admin/DisplayActionItem.aspx?cntid=" + a + "&type=1", '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenLst10ClosedAct(a) {
            window.open("../admin/DisplayActionItem.aspx?cntid=" + a + "&type=2", '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function Disableddl() {
            if (Tabstrip2.selectedIndex == 0) {
                document.Form1.ddlOppStatus.style.display = "none";
            }
            else {
                document.Form1.ddlOppStatus.style.display = "";
            }
        }
        function Save() {
            if (document.Form1.uwOppTab$_ctl0$txtFirstname.value == "") {
                alert("Enter First Name")
                document.Form1.uwOppTab$_ctl0$txtFirstname.focus();
                return false;
            }
            if (document.Form1.uwOppTab$_ctl0$txtLastName.value == "") {
                alert("Enter Last Name")
                document.Form1.uwOppTab$_ctl0$txtLastName.focus();
                return false;
            }
            if (document.Form1.txtContactType.value == 70) {
                if (document.Form1.uwOppTab$_ctl0$ddlType.value != 70) {
                    alert("You can't change the contact type 'Primary Contact' from this record, because there must be  one Primary Contact for every Organization record. Go to another contact record within the same Organization, and change its 'type' value to Primary Contact.")
                    document.Form1.uwOppTab$_ctl0$ddlType.focus();
                    return false;
                }
            }
        }
    </script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <menu1:webmenu ID="webmenu1" runat="server"></menu1:webmenu>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
        EnableViewState="true">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td>
                        <table bordercolor="black" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td class="tr1" align="center">
                                    <b>Record Owner : </b>
                                    <asp:Label ID="lblRecordOwner" runat="server" ForeColor="Black"></asp:Label>
                                </td>
                                <td class="td1" width="1" height="18">
                                </td>
                                <td class="tr1" align="center">
                                    <b>Created By : </b>
                                    <asp:Label ID="lblCreatedBy" runat="server" ForeColor="Black"></asp:Label>
                                </td>
                                <td class="td1" width="1" height="18">
                                </td>
                                <td class="tr1" align="center">
                                    <b>Last Modified By : </b>
                                    <asp:Label ID="lblModifiedBy" runat="server" ForeColor="Black"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td>
                    </td>
                    <td class="normal1" align="center">
                        Organization : <u>
                            <asp:HyperLink ID="hplCustomer" runat="server" CssClass="hyperlink"></asp:HyperLink>
                    </td>
                    <td align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"></asp:Button>
                        <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Save &amp; Close">
                        </asp:Button>
                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close"></asp:Button>
                    </td>
                </tr>
            </table>
            </td> </tr>
            <tr>
                <td>
                    <telerik:RadTabStrip ID="radOppTab" runat="server" UnSelectChildren="True" Skin="Vista"
                        ClickSelectedTab="True" AutoPostBack="True" SelectedIndex="0" MultiPageID="radMultiPage_OppTab">
                        <Tabs>
                            <telerik:RadTab Text="&nbsp;&nbsp;Contact Details&nbsp;&nbsp;" Value="Details" PageViewID="radPageView_Details">
                            </telerik:RadTab>
                        </Tabs>
                    </telerik:RadTabStrip>
                    <telerik:RadMultiPage ID="radMultiPage_OppTab" runat="server" SelectedIndex="0" CssClass="pageView"
                        Width="100%">
                        <telerik:RadPageView ID="radPageView_Details" runat="server">
                            <asp:Table ID="tblContacts" runat="server" BorderWidth="1" Width="100%" GridLines="None"
                                BorderColor="black" Height="300" CssClass="aspTable">
                                <asp:TableRow>
                                    <asp:TableCell VerticalAlign="Top">
                                        <br>
                                        <table width="100%" id="tblMain" runat="server">
                                            <tr>
                                                <td rowspan="30" valign="top">
                                                    <img src="../images/Greenman-32.gif" />
                                                </td>
                                                <td class="normal1" valign="middle" align="right">
                                                    First/Last Name<font color="red">*</font>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtFirstname" TabIndex="1" CssClass="signup" Width="90" runat="server"></asp:TextBox>
                                                    <asp:TextBox ID="txtLastName" TabIndex="2" CssClass="signup" Width="90" runat="server"></asp:TextBox>
                                                </td>
                                                <td class="normal1" align="right">
                                                    Phone/Ext
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtPhone" TabIndex="10" CssClass="signup" Width="135" runat="server"></asp:TextBox>
                                                    <asp:TextBox ID="txtExtension" Width="45" TabIndex="11" CssClass="signup" runat="server"></asp:TextBox>
                                                </td>
                                                <td class="normal1" valign="middle" align="right">
                                                    Position
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlPosition" TabIndex="17" CssClass="signup" runat="server"
                                                        Width="180">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="normal1" align="right">
                                                    Email
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtEmail" CssClass="signup" TabIndex="3" Width="180" runat="server"></asp:TextBox>&nbsp;
                                                </td>
                                                <td class="normal1" align="right">
                                                    Cell
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtMobile" runat="server" TabIndex="12" Width="180" CssClass="signup"></asp:TextBox>
                                                </td>
                                                <td class="normal1" align="right">
                                                    Title
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtTitle" runat="server" TabIndex="18" Width="180" CssClass="signup"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="normal1" align="right">
                                                    Alternate Email
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAltEmail" CssClass="signup" TabIndex="4" Width="180" runat="server"></asp:TextBox>&nbsp;
                                                </td>
                                                <td class="normal1" align="right">
                                                    Home
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtHome" runat="server" Width="180" TabIndex="13" CssClass="signup"></asp:TextBox>
                                                </td>
                                                <td class="normal1" align="right">
                                                    Department
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlDepartment" Width="180" TabIndex="19" runat="server" CssClass="signup">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="normal1" align="right">
                                                    DOB
                                                </td>
                                                <td class="normal1">
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <BizCalendar:Calendar ID="cal" runat="server" />
                                                            </td>
                                                            <td>
                                                                &nbsp;Age
                                                                <asp:TextBox ID="txtAg" runat="server" CssClass="signup" Width="30" TabIndex="9"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td class="normal1" align="right">
                                                    Fax
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtFax" runat="server" Width="180" TabIndex="14" CssClass="signup"></asp:TextBox>
                                                </td>
                                                <td class="normal1" align="right">
                                                    Gender
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlSex" CssClass="signup" runat="server" TabIndex="20" Width="180">
                                                        <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                        <asp:ListItem Value="M">Male</asp:ListItem>
                                                        <asp:ListItem Value="F">Female</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="normal1" align="right">
                                                    Contact Type
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlType" TabIndex="8" CssClass="signup" AutoPostBack="True"
                                                        runat="server" Width="180">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="normal1" align="right">
                                                    Category
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlcategory" Width="180" TabIndex="15" runat="server" CssClass="signup">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="normal1" align="right">
                                                    Opt-out
                                                </td>
                                                <td class="normal1">
                                                    <asp:CheckBox ID="optout" runat="server"></asp:CheckBox>&nbsp;&nbsp;&nbsp; Status:
                                                    <asp:DropDownList ID="ddlEmpStatus" CssClass="signup" TabIndex="21" runat="server"
                                                        Width="110">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="normal1" align="right" valign="top">
                                                    <asp:Label ID="lblManager" runat="server">Manager</asp:Label>
                                                </td>
                                                <td valign="top">
                                                    <asp:DropDownList ID="ddlManagers" TabIndex="9" runat="server" Width="180" CssClass="signup">
                                                        <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="normal1" align="right" valign="top">
                                                    Team
                                                </td>
                                                <td valign="top">
                                                    <asp:DropDownList ID="ddlTeam" TabIndex="16" runat="server" Width="180" CssClass="signup">
                                                    </asp:DropDownList>
                                                </td>
                                                <td colspan="2" class="normal1">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="normal1" align="right">
                                                    <asp:HyperLink ID="hplAddress" runat="server" onClick="ShowWindow('Layer1','','show')"
                                                        CssClass="hyperlink">Address</asp:HyperLink>
                                                </td>
                                                <td colspan="2" class="normal1">
                                                    <asp:Label ID="lblStreet" runat="server"></asp:Label>
                                                    <asp:Label ID="lblCity" runat="server"></asp:Label>
                                                    <asp:Label ID="lblPostal" runat="server"></asp:Label>
                                                    <asp:Label ID="lblState" runat="server"></asp:Label>
                                                    <asp:Label ID="lblCountry" runat="server"></asp:Label>
                                                </td>
                                                <td class="normal1" colspan="2">
                                                </td>
                                                <td class="normal1">
                                                    Associations:&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <asp:HyperLink ID="hplTo" TabIndex="18" runat="server" CssClass="hyperlink">
							                <font color="#180073">To</font></asp:HyperLink>(<asp:Label runat="server" Text=""
                                                ID="lblAssoCountT"></asp:Label>)&nbsp;/&nbsp;
                                                    <asp:HyperLink ID="hplFrom" TabIndex="19" runat="server" CssClass="hyperlink">
							                <font color="#180073">From</font></asp:HyperLink>(<asp:Label runat="server" Text=""
                                                ID="lblAssoCountF"></asp:Label>)
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="100%">
                                            <tr>
                                                <td class="normal1" align="right">
                                                    Comments
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="txtComments" runat="server" Width="500" CssClass="signup" Height="50"
                                                        MaxLength="250" TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="normal1" colspan="6">
                                                    <b>Assistant's Information</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="normal1" align="right">
                                                    First/Last Name
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAsstFName" CssClass="signup" Width="90" runat="server"></asp:TextBox>&nbsp;
                                                    <asp:TextBox ID="txtAsstLName" runat="server" Width="90" CssClass="signup"></asp:TextBox>
                                                </td>
                                                <td class="normal1" align="right">
                                                    Phone/Ext
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAsstPhone" CssClass="signup" Width="135" runat="server"></asp:TextBox>&nbsp;
                                                    <asp:TextBox ID="txtAsstExt" CssClass="signup" runat="server" Width="45"></asp:TextBox>
                                                </td>
                                                <td class="normal1" align="right">
                                                    Email
                                                </td>
                                                <td class="normal1">
                                                    <asp:TextBox ID="txtAsstEmail" runat="server" Width="180" CssClass="signup"></asp:TextBox>&nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                        <br>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </telerik:RadPageView>
                    </telerik:RadMultiPage>
                <%--    <igtab:UltraWebTab ImageDirectory="" ID="uwOppTab" runat="server" ThreeDEffect="True"
                        BorderStyle="Solid" Width="100%" BarHeight="0" BorderWidth="0">
                        <defaulttabstyle height="23px" font-bold="true" font-size="11px" font-names="Arial">
                        </defaulttabstyle>
                        <roundedimage leftsidewidth="7" rightsidewidth="8" shiftofimages="0" selectedimage="../images/ig_tab_winXPs3.gif"
                            normalimage="../images/ig_tab_winXP3.gif" hoverimage="../images/ig_tab_winXPs3.gif"
                            fillstyle="LeftMergedWithCenter"></roundedimage>
                        <selectedtabstyle height="23px" forecolor="white">
                        </selectedtabstyle>
                        <hovertabstyle height="23px" forecolor="white">
                        </hovertabstyle>
                        <tabs>
                            <igtab:Tab Text="&nbsp;&nbsp;Contact Details&nbsp;&nbsp;">
                                <ContentTemplate>
                                    
                                </ContentTemplate>
                            </igtab:Tab>
                        </tabs>
                    </igtab:UltraWebTab>--%>
                </td>
            </tr>
            </table>
            <asp:TextBox ID="hidEml" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="hidCompName" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtContactType" Style="display: none" runat="server"></asp:TextBox>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
