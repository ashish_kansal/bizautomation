'created by anoop jayaraj
Imports BACRM.BusinessLogic.Contacts
'Imports Infragistics.WebUI.WebHtmlEditor
Imports BACRM.BusinessLogic.Common
Public Class frmEmailMessage
    Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    'Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    '    Try
    '        If Not IsPostBack Then
    '            Dim objContacts As New CContacts
    '            Dim dtEmail As DataTable
    '            objContacts.EmailHstrID = GetQueryStringVal( "Email")
    '            objContacts.CreatedOn = Replace(GetQueryStringVal( "Date"), "%20", " ")
    '            dtEmail = objContacts.EmailDetails
    '            If dtEmail.Rows.Count = 1 Then
    '                lblTo.Text = dtEmail.Rows(0).Item("vcMessageTo")
    '                lblFrom.Text = dtEmail.Rows(0).Item("vcMessageFrom")
    '                lblCc.Text = dtEmail.Rows(0).Item("vcCC")
    '                lblBcc.Text = dtEmail.Rows(0).Item("vcBCC")
    '                lblSubject.Text = dtEmail.Rows(0).Item("vcSubject")
    '                ' oEdit1.Content = dtEmail.Rows(0).Item("vcBody")
    '                oEditHtml.Text = dtEmail.Rows(0).Item("vcBody")
    '                Dim dtAttachments As DataTable
    '                dtAttachments = objContacts.EmailAttachmentDtls
    '                If dtAttachments.Rows.Count > 0 Then
    '                    Dim i As Integer
    '                    Dim hpl As HyperLink
    '                    Dim lbl As Label
    '                    For i = 0 To dtAttachments.Rows.Count - 1
    '                        If i <> 0 Then
    '                            lbl = New Label
    '                            lbl.Text = "&nbsp; ,"
    '                            tdAttachments.Controls.Add(lbl)
    '                        End If
    '                        hpl = New HyperLink
    '                        hpl.Text = dtAttachments.Rows(i).Item("vcFileName")
    '                        hpl.NavigateUrl = dtAttachments.Rows(i).Item("vcLocation")
    '                        hpl.CssClass = "normal1"
    '                        hpl.Target = "_blank"
    '                        tdAttachments.Controls.Add(hpl)
    '                    Next
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

End Class
