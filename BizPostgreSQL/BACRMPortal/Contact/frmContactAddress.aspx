<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmContactAddress.aspx.vb" Inherits="BACRMPortal.frmContactAddress" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
        <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
		<title>Address</title>
		<script language="javascript">
			function CheckNumber()
					{
							if (!(window.event.keyCode > 47 && window.event.keyCode < 58))
							{
								window.event.keyCode=0;
							}
					}
		function Close()
		{
		     window.close();
		}
		</script>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
			<table width="100%" border="0" class="aspTable">
				<tr>
					<td colspan="6" align="right" valign="top">
						<asp:button id="btnSave" Runat="server" CssClass="button" Text="Save" Width="50"></asp:button>
						<asp:button id="btnSaveClose" Runat="server" CssClass="button" Text="Save &amp; Close"></asp:button>
						<asp:button id="btnClose" Runat="server" CssClass="button" Text="Close" Width="50"></asp:button>
					</td>
				</tr>
				<tr>
					<td class="normal7" align="left" colspan="2">Primary Address
						<asp:dropdownlist id="Dropdownlist1"  Runat="server" Width="130px" cssclass="signup"
							Visible="False">
							<asp:ListItem Value="0">Select One</asp:ListItem>
						</asp:dropdownlist></td>
					<td class="normal7" align="right">Address</td>
					<td>
						<asp:dropdownlist id="ddlContactLocation" TabIndex=15  Runat="server" Width="130px" cssclass="signup"></asp:dropdownlist></td>
					<td class="normal7" align="right">Address</td>
					<td>
						<asp:dropdownlist id="ddlContactLocation1" TabIndex=21  Runat="server" Width="130px" cssclass="signup"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td class="normal1" align="right">Street</td>
					<td>
						<asp:textbox id="TxtPAddStreet" TabIndex=10  TextMode=MultiLine  Width="200" runat="server" cssclass="signup" MaxLength="50"></asp:textbox></td>
					<td class="normal1" align="right">Street</td>
					<td>
						<asp:textbox id="TxtAddStreet" TabIndex=16 TextMode=MultiLine Width="200" runat="server" cssclass="signup" MaxLength="50"></asp:textbox></td>
					<td class="normal1" align="right">Street</td>
					<td>
						<asp:textbox id="TxtAddStreet1" TabIndex=22 TextMode=MultiLine  Width="200" runat="server" cssclass="signup" MaxLength="50"></asp:textbox></td>
				</tr>
				<tr>
					<td class="normal1" align="right">City</td>
					<td>
						<asp:textbox id="TxtPAddCity" Width="200" TabIndex=11 runat="server" cssclass="signup" MaxLength="50"></asp:textbox></td>
					<td class="normal1" align="right">City</td>
					<td>
						<asp:textbox id="TxtAddCity" TabIndex=17  Width="200" runat="server" cssclass="signup" MaxLength="50"></asp:textbox></td>
					<td class="normal1" align="right">City</td>
					<td>
						<asp:textbox id="TxtAddCity1" TabIndex=23  Width="200" runat="server" cssclass="signup" MaxLength="50"></asp:textbox></td>
				</tr>
				<tr>
					<td class="normal1" align="right">State</td>
					<td>
						<asp:DropDownList ID="ddlPAddState" TabIndex=12 Runat="server" Width="200" CssClass="signup">
							<asp:ListItem Value="0">--Select One--</asp:ListItem>
						</asp:DropDownList>
					</td>
					<td class="normal1" align="right">State</td>
					<td>
						<asp:DropDownList ID="ddlAddState" TabIndex=18 Runat="server" Width="200" CssClass="signup">
							<asp:ListItem Value="0">--Select One--</asp:ListItem>
						</asp:DropDownList></td>
					<td class="normal1" align="right">State</td>
					<td>
						<asp:DropDownList ID="ddlAddState1" TabIndex=24 Runat="server" Width="200" CssClass="signup">
							<asp:ListItem Value="0">--Select One--</asp:ListItem>
						</asp:DropDownList></td>
				</tr>
				<tr>
					<td class="normal1" align="right">Postal</td>
					<td>
						<asp:textbox id="TxtPAddPostal" TabIndex=13  runat="server" Width="200" cssclass="signup"></asp:textbox></td>
					<td class="normal1" align="right">Postal</td>
					<td>
						<asp:textbox id="TxtAddPostal" TabIndex=19 runat="server" Width="200" cssclass="signup"></asp:textbox></td>
					<td class="normal1" align="right">Postal</td>
					<td>
						<asp:textbox id="TxtAddPostal1" TabIndex=25 runat="server" Width="200" cssclass="signup"></asp:textbox></td>
				</tr>
				<tr>
					<td class="normal1" align="right">Country</td>
					<td>
						<asp:DropDownList ID="ddlPAddCountry" TabIndex=14 AutoPostBack="True" Runat="server" Width="200" CssClass="signup">
							<asp:ListItem Value="0">--Select One--</asp:ListItem>
						</asp:DropDownList>
					</td>
					<td class="normal1" align="right">Country</td>
					<td>
						<asp:DropDownList ID="ddlAddCountry" TabIndex=20 AutoPostBack="True" Runat="server" Width="200" CssClass="signup">
							<asp:ListItem Value="0">--Select One--</asp:ListItem>
						</asp:DropDownList>
					</td>
					<td class="normal1" align="right">Country</td>
					<td>
						<asp:DropDownList ID="ddlAddCountry1" TabIndex=26 AutoPostBack="True" Runat="server" Width="200" CssClass="signup">
							<asp:ListItem Value="0">--Select One--</asp:ListItem>
						</asp:DropDownList></td>
				</tr>
				<tr>
					<td class="normal7" align="right">Address</td>
					<td>
						<asp:dropdownlist id="ddlContactLocation2" TabIndex=27  Runat="server" Width="130px" cssclass="signup"></asp:dropdownlist></td>
					<td class="normal7" align="right">Address</td>
					<td>
						<asp:dropdownlist id="ddlContactLocation3" TabIndex=33  Runat="server" Width="130px" cssclass="signup"></asp:dropdownlist></td>
					<td class="normal7" align="right">Address</td>
					<td>
						<asp:dropdownlist id="ddlContactLocation4" TabIndex=39  Runat="server" Width="130px" cssclass="signup"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td class="normal1" align="right">Street</td>
					<td>
						<asp:textbox id="TxtAddStreet2" TextMode=MultiLine TabIndex=28  runat="server" cssclass="signup" MaxLength="50"
							Width="200"></asp:textbox></td>
					<td class="normal1" align="right">Street</td>
					<td>
						<asp:textbox id="TxtAddStreet3" TextMode=MultiLine TabIndex=34  runat="server" cssclass="signup" MaxLength="50"
							Width="200"></asp:textbox></td>
					<td class="normal1" align="right">Street</td>
					<td>
						<asp:textbox id="TxtAddStreet4" TextMode=MultiLine  TabIndex=40 runat="server" cssclass="signup" MaxLength="50"
							Width="200"></asp:textbox></td>
				</tr>
				<tr>
					<td class="normal1" align="right">City</td>
					<td>
						<asp:textbox id="TxtAddCity2" TabIndex=29  Width="200" runat="server" cssclass="signup" MaxLength="50"></asp:textbox></td>
					<td class="normal1" align="right">City</td>
					<td>
						<asp:textbox id="TxtAddCity3" TabIndex=35  Width="200" runat="server" cssclass="signup" MaxLength="50"></asp:textbox></td>
					<td class="normal1" align="right">City</td>
					<td>
						<asp:textbox id="TxtAddCity4" TabIndex=41  Width="200" runat="server" cssclass="signup" MaxLength="50"></asp:textbox></td>
				</tr>
				<tr>
					<td class="normal1" align="right">State</td>
					<td>
						<asp:DropDownList ID="ddlAddState2" TabIndex=30 Runat="server" Width="200" CssClass="signup">
							<asp:ListItem Value="0">--Select One--</asp:ListItem>
						</asp:DropDownList>
					</td>
					<td class="normal1" align="right">State</td>
					<td>
						<asp:DropDownList ID="ddlAddState3" TabIndex=36 Runat="server" Width="200" CssClass="signup">
							<asp:ListItem Value="0">--Select One--</asp:ListItem>
						</asp:DropDownList>
					</td>
					<td class="normal1" align="right">State</td>
					<td>
						<asp:DropDownList ID="ddlAddState4" TabIndex=42 Runat="server" Width="200" CssClass="signup">
							<asp:ListItem Value="0">--Select One--</asp:ListItem>
						</asp:DropDownList></td>
				</tr>
				<tr>
					<td class="normal1" align="right">Postal</td>
					<td>
						<asp:textbox id="TxtAddPostal2" TabIndex=31  runat="server" Width="200" cssclass="signup"></asp:textbox></td>
					<td class="normal1" align="right">Postal</td>
					<td>
						<asp:textbox id="TxtAddPostal3" TabIndex=37  runat="server" Width="200" cssclass="signup"></asp:textbox></td>
					<td class="normal1" align="right">Postal</td>
					<td>
						<asp:textbox id="TxtAddPostal4" TabIndex=43  runat="server" Width="200" cssclass="signup"></asp:textbox></td>
				</tr>
				<tr>
					<td class="normal1" align="right">Country</td>
					<td>
						<asp:DropDownList ID="ddlAddCountry2" TabIndex=32 AutoPostBack="True" Runat="server" Width="200" CssClass="signup">
							<asp:ListItem Value="0">--Select One--</asp:ListItem>
						</asp:DropDownList></td>
					<td class="normal1" align="right">Country</td>
					<td>
						<asp:DropDownList ID="ddlAddCountry3" TabIndex=38 AutoPostBack="True" Runat="server" Width="200" CssClass="signup">
							<asp:ListItem Value="0">--Select One--</asp:ListItem>
						</asp:DropDownList></td>
					<td class="normal1" align="right">Country</td>
					<td>
						<asp:DropDownList ID="ddlAddcountry4" TabIndex=44 AutoPostBack="True" Runat="server" Width="200" CssClass="signup">
							<asp:ListItem Value="0">--Select One--</asp:ListItem>
						</asp:DropDownList></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>

