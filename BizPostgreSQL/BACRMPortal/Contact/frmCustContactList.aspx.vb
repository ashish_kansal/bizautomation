'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Account
Imports BACRM.BusinessLogic.Common

Public Class frmCustContactList : Inherits BACRMPage

    Dim SI As Integer = 0
    Dim SI1 As Integer = 0
    Dim SI2 As Integer = 0
    Dim frm As String = ""
    Dim frm1 As String = ""
    Dim frm2 As String = ""

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    ' Protected WithEvents lblRecordCount As System.Web.UI.WebControls.Label
    Protected WithEvents txtCustomer As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtFirstName As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtLastName As System.Web.UI.WebControls.TextBox
    ' Protected WithEvents btnGo As System.Web.UI.WebControls.Button
    Protected WithEvents ddlSort As System.Web.UI.WebControls.DropDownList
    '  Protected WithEvents lblNext As System.Web.UI.WebControls.Label
    Protected WithEvents lnk1 As System.Web.UI.WebControls.LinkButton
    'Protected WithEvents lnk2 As System.Web.UI.WebControls.LinkButton
    'Protected WithEvents lnk3 As System.Web.UI.WebControls.LinkButton
    'Protected WithEvents lnk4 As System.Web.UI.WebControls.LinkButton
    'Protected WithEvents lnk5 As System.Web.UI.WebControls.LinkButton
    'Protected WithEvents lnkFirst As System.Web.UI.WebControls.LinkButton
    'Protected WithEvents lnkPrevious As System.Web.UI.WebControls.LinkButton
    'Protected WithEvents lblPage As System.Web.UI.WebControls.Label
    'Protected WithEvents txtCurrrentPage As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblOf As System.Web.UI.WebControls.Label
    'Protected WithEvents lblTotal As System.Web.UI.WebControls.Label
    'Protected WithEvents lnkNext As System.Web.UI.WebControls.LinkButton
    'Protected WithEvents lnkLast As System.Web.UI.WebControls.LinkButton
    'Protected WithEvents litMessage As System.Web.UI.WebControls.Literal
    'Protected WithEvents txtTotalPage As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtTotalRecords As System.Web.UI.WebControls.TextBox
    'Protected WithEvents txtSortChar As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hidenav As System.Web.UI.HtmlControls.HtmltableCell
    'Protected WithEvents dgContacts As System.Web.UI.WebControls.DataGrid
    Dim strColumn As String
    
    '  Protected WithEvents table2 As System.Web.UI.WebControls.table
    Protected WithEvents btnNew As System.Web.UI.WebControls.Button
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Dim objCommon As New CCommon
            m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmContactList.aspx", Session("UserContactID"), 15, 2)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../Common/frmAuthorization.aspx")
            End If
            If Not GetQueryStringVal( "SI") Is Nothing Then
                SI = GetQueryStringVal( "SI")
            End If
            If Not GetQueryStringVal( "SI1") Is Nothing Then
                SI1 = GetQueryStringVal( "SI1")
            Else : SI1 = 0
            End If
            If Not GetQueryStringVal( "SI2") Is Nothing Then
                SI2 = GetQueryStringVal( "SI2")
            Else : SI2 = 0
            End If
            If Not GetQueryStringVal( "frm") Is Nothing Then
                frm = ""
                frm = GetQueryStringVal( "frm")
            Else : frm = ""
            End If
            If Not GetQueryStringVal( "frm1") Is Nothing Then
                frm1 = ""
                frm1 = GetQueryStringVal( "frm1")
            Else : frm1 = ""
            End If
            If Not GetQueryStringVal( "SI2") Is Nothing Then
                frm2 = ""
                frm2 = GetQueryStringVal( "frm2")
            Else : frm2 = ""
            End If
            'If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
            '    btnNew.Visible = False
            'End If
            Dim dtTab As DataTable
            dtTab = Session("DefaultTab")
            If dtTab.Rows.Count > 0 Then
                lbContacts.Text = dtTab.Rows(0).Item("vcContact") & "s"
            Else : lbContacts.Text = "Contacts"
            End If
            If Not IsPostBack Then
                Session("Asc") = 1
                txtCurrrentPage.Text = 1
                BindDatagrid()
            End If
            If txtSortChar.Text <> "" Then
                ViewState("SortChar") = txtSortChar.Text
                ViewState("Column") = "vcFirstName"
                Session("Asc") = 0
                BindDatagrid()
                txtSortChar.Text = ""
            End If
            'btnNew.Attributes.Add("onclick", "return goto('../Contacts/frmNewContact.aspx','cntOpenItem','divNew');")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindDatagrid()
        Try
            Dim dtContacts As DataTable
            Dim objContacts As New CContacts
            Dim SortChar As Char
            If ViewState("SortChar") <> "" Then
                SortChar = ViewState("SortChar")
            Else : SortChar = "0"
            End If
            With objContacts
                .UserCntID = Session("UserContactID")
                .DomainID = Session("DomainID")
                .UserRightType = m_aryRightsForPage(RIGHTSTYPE.VIEW)
                .SortCharacter = SortChar
                If txtCurrrentPage.Text.Trim <> "" Then
                    .CurrentPage = txtCurrrentPage.Text
                Else : .CurrentPage = 1
                End If
                .PageSize = Session("PagingRows")
                .TotalRecords = 0
                If ViewState("Column") <> "" Then
                    .columnName = ViewState("Column")
                Else : .columnName = "DM.bintcreateddate"
                End If
                If Session("Asc") = 1 Then
                    .columnSortOrder = "Desc"
                Else : .columnSortOrder = "Asc"
                End If
                .DivisionID = Session("DivID")
            End With
            dtContacts = objContacts.GetContactListPortal
            If objContacts.TotalRecords = 0 Then
                hidenav.Visible = False
                lblRecordCount.Text = 0
            Else
                hidenav.Visible = True
                lblRecordCount.Text = String.Format("{0:#,###}", objContacts.TotalRecords)
                Dim strTotalPage As String()
                Dim decTotalPage As Decimal
                decTotalPage = lblRecordCount.Text / Session("PagingRows")
                decTotalPage = Math.Round(decTotalPage, 2)
                strTotalPage = CStr(decTotalPage).Split(".")
                If (lblRecordCount.Text Mod Session("PagingRows")) = 0 Then
                    lblTotal.Text = strTotalPage(0)
                    txtTotalPage.Text = strTotalPage(0)
                Else
                    lblTotal.Text = strTotalPage(0) + 1
                    txtTotalPage.Text = strTotalPage(0) + 1
                End If
                txtTotalRecords.Text = lblRecordCount.Text
            End If
            dgContacts.DataSource = dtContacts
            dgContacts.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub lnkLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkLast.Click
        Try
            txtCurrrentPage.Text = txtTotalPage.Text
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkPrevious.Click
        Try
            If txtCurrrentPage.Text = 1 Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text - 1
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkFirst.Click
        Try
            txtCurrrentPage.Text = 1
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkPrevious.Click, lnkNext.Click
        Try
            If txtCurrrentPage.Text = txtTotalPage.Text Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 1
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk2.Click
        Try
            If txtCurrrentPage.Text + 1 = txtTotalPage.Text Or txtCurrrentPage.Text + 1 > txtTotalPage.Text Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 2
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk3.Click
        Try
            If txtCurrrentPage.Text + 2 = txtTotalPage.Text Or txtCurrrentPage.Text + 2 > txtTotalPage.Text Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 3
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk4.Click
        Try
            If txtCurrrentPage.Text + 3 = txtTotalPage.Text Or txtCurrrentPage.Text + 3 > txtTotalPage.Text Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 4
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk5.Click
        Try
            If txtCurrrentPage.Text + 4 = txtTotalPage.Text Or txtCurrrentPage.Text + 4 > txtTotalPage.Text Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 5
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub txtCurrrentPage_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCurrrentPage.TextChanged
        Try
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgContacts_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgContacts.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As Button
                Dim lnkDelete As LinkButton
                lnkDelete = e.Item.FindControl("lnkDelete")
                btnDelete = e.Item.FindControl("btnDelete")
                If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                    btnDelete.Visible = False
                    lnkDelete.Visible = True
                    lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                ElseIf m_aryRightsForPage(RIGHTSTYPE.DELETE) = 1 Then
                    Try
                        If e.Item.Cells(1).Text = Session("UserContactID") Then
                            btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                        Else
                            btnDelete.Visible = False
                            lnkDelete.Visible = True
                            lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                        End If
                    Catch ex As Exception

                    End Try
                ElseIf m_aryRightsForPage(RIGHTSTYPE.DELETE) = 3 Then
                    btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgContacts_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgContacts.ItemCommand
        Try
            Dim lngContID As Long
            Dim StrEmail As String
            If Not e.CommandName = "Sort" Then lngContID = e.Item.Cells(0).Text()
            If e.CommandName = "Company" Then
                If Session("EnableIntMedPage") = 1 Then
                    Response.Redirect("../pagelayout/frmCustAccountdtl.aspx?frm=ContactList" & "&SI1=0&SI2=" & SI1 & "&frm1=" & frm1 & "&frm2=" & frm2)
                Else : Response.Redirect("../account/frmCusAccounts.aspx?frm=ContactList" & "&SI1=0&SI2=" & SI1 & "&frm1=" & frm1 & "&frm2=" & frm2)
                End If
            ElseIf e.CommandName = "Contact" Then
                Session("ContactId") = lngContID
                If Session("EnableIntMedPage") = 1 Then
                    Response.Redirect("../pagelayout/frmCustContactdtl.aspx?frm=ContactList" & "&SI1=0&SI2=" & SI1 & "&frm1=" & frm1 & "&frm2=" & frm2)
                Else : Response.Redirect("../Contact/frmCstContacts.aspx?frm=ContactList" & "&SI1=0&SI2=" & SI1 & "&frm1=" & frm1 & "&frm2=" & frm2)
                End If
            ElseIf e.CommandName = "Delete" Then
                Dim objContact As New CContacts
                objContact.ContactID = lngContID
                objContact.DomainID = Session("DomainID")
                Dim strError As String = objContact.DelContact()
                If strError = "" Then
                    BindDatagrid()
                    litMessage.Text = ""
                Else
                    litMessage.Text = strError
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgContacts_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgContacts.SortCommand
        Try
            strColumn = e.SortExpression.ToString()
            If ViewState("Column") <> strColumn Then
                ViewState("Column") = strColumn
                Session("Asc") = 0
            Else
                If Session("Asc") = 0 Then
                    Session("Asc") = 1
                Else : Session("Asc") = 0
                End If
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class