Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts

Partial Public Class frmConfContactList : Inherits BACRMPage

    Dim objContact As CContacts
    Dim objCommon As New CCommon

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            btnSave.Attributes.Add("onclick", "Save()")
            If Not IsPostBack Then
                objCommon.sb_FillComboFromDBwithSel(ddlType, 8, Session("DomainID"))
                BindLists()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindLists()
        Try
            Dim ds As DataSet
            objContact = New CContacts
            objContact.DomainID = Session("DomainId")
            objContact.FormId = 10
            objContact.UserCntID = Session("UserContactId")
            objContact.ContactType = ddlType.SelectedValue
            ds = objContact.GetColumnConfiguration()
            lstAvailablefld.DataSource = ds.Tables(0)
            lstAvailablefld.DataTextField = "vcFormFieldName"
            lstAvailablefld.DataValueField = "numFormFieldID"
            lstAvailablefld.DataBind()
            lstSelectedfld.DataSource = ds.Tables(1)
            lstSelectedfld.DataValueField = "numFormFieldID"
            lstSelectedfld.DataTextField = "vcFormFieldName"
            lstSelectedfld.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        Try
            BindLists()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If objContact Is Nothing Then objContact = New CContacts

            Dim dsNew As New DataSet
            Dim dtTable As New DataTable
            dtTable.Columns.Add("numFormFieldID")
            dtTable.Columns.Add("bitCustom")
            dtTable.Columns.Add("tintOrder")
            Dim i As Integer

            Dim dr As DataRow
            Dim str As String()
            str = hdnCol.Value.Split(",")
            For i = 0 To str.Length - 2
                dr = dtTable.NewRow
                dr("numFormFieldID") = str(i).Split("~")(0)
                dr("bitCustom") = str(i).Split("~")(1)
                dr("tintOrder") = i
                dtTable.Rows.Add(dr)
            Next

            dtTable.TableName = "Table"
            dsNew.Tables.Add(dtTable.Copy)

            objContact.ContactType = ddlType.SelectedValue
            objContact.DomainID = Session("DomainId")
            objContact.UserCntID = Session("UserContactId")
            objContact.FormId = 10
            objContact.strXml = dsNew.GetXml
            objContact.SaveContactColumnConfiguration()
            dsNew.Tables.Remove(dsNew.Tables(0))
            If radY.Checked = True Then
                objContact.bitDefault = True
            Else : objContact.bitDefault = True
            End If
            objContact.FilterID = ddlSort.SelectedValue
            objContact.SaveDefaultFilter()
            BindLists()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class

