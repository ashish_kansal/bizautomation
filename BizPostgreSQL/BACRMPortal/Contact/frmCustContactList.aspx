<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmCustContactList.aspx.vb"
    Inherits="BACRMPortal.frmCustContactList" %>

<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../common/topbar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">

    <script language="javascript" type="text/javascript">
        functiongoto(target, frame, div) {
            //target = target + '?CmpID='+ document.frmHeader.webmenu1_txtCompany.value +'&DivID=' + document.frmHeader.webmenu1_txtDivision.value
            //frames[frame].location.href=target;
            //document.getElementById(div).style.visibility= 'visible';
            window.open(target, '', 'toolbar=no,titlebar=no,left=150, top=150,width=750,height=450,scrollbars=no,resizable=yes')

        }
        function fnSortByChar(varSortChar) {
            document.Form1.txtSortChar.value = varSortChar;
            if (document.Form1.txtCurrrentPage != null) {
                document.Form1.txtCurrrentPage.value = 1;
            }
            document.Form1.btnGo.click();
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <menu1:webmenu ID="webmenu1" runat="server"></menu1:webmenu>
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="updatepanel1" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
        EnableViewState="true">
        <ContentTemplate>
            <br>
            <asp:Button runat="server" ID="btnGo" Text="" Style="display: none"></asp:Button>
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td valign="bottom">
                        <table class="TabStyle">
                            <tr>
                                <td>
                                    &nbsp;&nbsp;&nbsp;<asp:Label ID="lbContacts" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="normal1" width="150" align="center">
                        No of Records:
                        <asp:Label ID="lblRecordCount" runat="server"></asp:Label>
                    </td>
                    <td id="hidenav" nowrap align="right" runat="server">
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="lblNext" runat="server" CssClass="Text_bold">Next:</asp:Label>
                                </td>
                                <td class="normal1">
                                    <asp:LinkButton ID="lnk2" runat="server">2</asp:LinkButton>
                                </td>
                                <td class="normal1">
                                    <asp:LinkButton ID="lnk3" runat="server">3</asp:LinkButton>
                                </td>
                                <td class="normal1">
                                    <asp:LinkButton ID="lnk4" runat="server">4</asp:LinkButton>
                                </td>
                                <td class="normal1">
                                    <asp:LinkButton ID="lnk5" runat="server">5</asp:LinkButton>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkFirst" runat="server"><div class="LinkArrow"><<</div>
                                    </asp:LinkButton>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkPrevious" runat="server"><div class="LinkArrow"><</div>
                                    </asp:LinkButton>
                                </td>
                                <td class="normal1">
                                    <asp:Label ID="lblPage" runat="server">Page</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCurrrentPage" runat="server" Width="28px" CssClass="signup" AutoPostBack="True"
                                        MaxLength="5"></asp:TextBox>
                                </td>
                                <td class="normal1">
                                    <asp:Label ID="lblOf" runat="server">of</asp:Label>
                                </td>
                                <td class="normal1">
                                    <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkNext" runat="server" CssClass="LinkArrow"><div class="LinkArrow">></div>
                                    </asp:LinkButton>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkLast" runat="server"><div class="LinkArrow">>></div>
                                    </asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table cellspacing="1" cellpadding="1" width="100%" border="0">
                <tr>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="a" href="javascript:fnSortByChar('a')">
                            <div class="A2Z">
                                A</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="b" href="javascript:fnSortByChar('b')">
                            <div class="A2Z">
                                B</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="c" href="javascript:fnSortByChar('c')">
                            <div class="A2Z">
                                C</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="d" href="javascript:fnSortByChar('d')">
                            <div class="A2Z">
                                D</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="e" href="javascript:fnSortByChar('e')">
                            <div class="A2Z">
                                E</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="f" href="javascript:fnSortByChar('f')">
                            <div class="A2Z">
                                F</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="g" href="javascript:fnSortByChar('g')">
                            <div class="A2Z">
                                G</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="h" href="javascript:fnSortByChar('h')">
                            <div class="A2Z">
                                H</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="I" href="javascript:fnSortByChar('i')">
                            <div class="A2Z">
                                I</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="j" href="javascript:fnSortByChar('j')">
                            <div class="A2Z">
                                J</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="k" href="javascript:fnSortByChar('k')">
                            <div class="A2Z">
                                K</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="l" href="javascript:fnSortByChar('l')">
                            <div class="A2Z">
                                L</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="m" href="javascript:fnSortByChar('m')">
                            <div class="A2Z">
                                M</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="n" href="javascript:fnSortByChar('n')">
                            <div class="A2Z">
                                N</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="o" href="javascript:fnSortByChar('o')">
                            <div class="A2Z">
                                O</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="p" href="javascript:fnSortByChar('p')">
                            <div class="A2Z">
                                P</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="q" href="javascript:fnSortByChar('q')">
                            <div class="A2Z">
                                Q</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="r" href="javascript:fnSortByChar('r')">
                            <div class="A2Z">
                                R</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="s" href="javascript:fnSortByChar('s')">
                            <div class="A2Z">
                                S</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="t" href="javascript:fnSortByChar('t')">
                            <div class="A2Z">
                                T</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="u" href="javascript:fnSortByChar('u')">
                            <div class="A2Z">
                                U</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="v" href="javascript:fnSortByChar('v')">
                            <div class="A2Z">
                                V</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="w" href="javascript:fnSortByChar('w')">
                            <div class="A2Z">
                                W</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="x" href="javascript:fnSortByChar('x')">
                            <div class="A2Z">
                                X</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="y" href="javascript:fnSortByChar('y')">
                            <div class="A2Z">
                                Y</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="z" href="javascript:fnSortByChar('z')">
                            <div class="A2Z">
                                Z</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="all" href="javascript:fnSortByChar('0')">
                            <div class="A2Z">
                                All</div>
                        </a>
                    </td>
                </tr>
            </table>
            <asp:Table ID="table2" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
                Width="100%" BorderColor="black" GridLines="None" Height="350" CssClass="aspTable">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Top">
                        <asp:DataGrid ID="dgContacts" AllowSorting="True" runat="server" Width="100%" CssClass="dg"
                            AutoGenerateColumns="False" BorderColor="white">
                            <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                            <ItemStyle CssClass="is"></ItemStyle>
                            <HeaderStyle CssClass="hs"></HeaderStyle>
                            <Columns>
                                <asp:BoundColumn Visible="False" DataField="numContactID"></asp:BoundColumn>
                                <asp:ButtonColumn DataTextField="Contact" SortExpression="vcFirstName" HeaderText="<font color=white>Contact</font>"
                                    CommandName="Contact"></asp:ButtonColumn>
                                <asp:ButtonColumn DataTextField="Company" SortExpression="vcCompanyName" HeaderText="<font color=white>Company</font>"
                                    CommandName="Company"></asp:ButtonColumn>
                                <asp:BoundColumn DataField="Telephone" SortExpression="numPhone" HeaderText="<font color=white>Phone - Ext</font>">
                                </asp:BoundColumn>
                                <asp:HyperLinkColumn DataTextField="vcEmail" SortExpression="vcEmail" DataNavigateUrlField="vcEmail"
                                    HeaderText="<font color=white>Email Address</font>" Target="_blank" DataNavigateUrlFormatString="mailto:{0}">
                                </asp:HyperLinkColumn>
                                <asp:BoundColumn DataField="Rating" SortExpression="LD.vcData" HeaderText="<font color=white>Rating</font>">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="Status" SortExpression="LD1.vcData" HeaderText="<font color=white>Status</font>">
                                </asp:BoundColumn>
                                <asp:TemplateColumn>
                                    <ItemTemplate>
                                        <asp:Button ID="btnDelete" runat="server" CssClass="Delete" Text="X" CommandName="Delete">
                                        </asp:Button>
                                        <asp:LinkButton ID="lnkDelete" runat="server" Visible="false">
																	<font color="#730000">*</font></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
            <table width="100%">
                <tr>
                    <td class="normal4" align="center">
                        <asp:Literal ID="litMessage" runat="server"></asp:Literal>
                    </td>
                </tr>
            </table>
            <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtSortChar" runat="server" Style="display: none"></asp:TextBox>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
