Imports BACRM.BusinessLogic.Prospects
Partial Class frmFromCompanyAssociation
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Dim intDivId As Integer
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        intDivId = Session("DivId")
        If IsPostBack = False Then
            bindGrid()
        End If
        btnClose.Attributes.Add("onclick", "return Close()")
    End Sub

    Sub bindGrid()
        Dim objProspects As New CProspects
        Dim dtAssociationFrom As New Datatable
        objProspects.DivisionID = intDivId
        objProspects.DivisionIdList = CStr(intDivId)
        objProspects.bitAssociatedTo = 0
        dtAssociationFrom = objProspects.GetAssociationTo()
        dgAssociation.DataSource = dtAssociationFrom
        dgAssociation.DataBind()
    End Sub
End Class
