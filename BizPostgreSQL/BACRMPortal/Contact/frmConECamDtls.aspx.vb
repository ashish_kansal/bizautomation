Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Common
Partial Class frmConECamDtls
    Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not IsPostBack Then
                Dim objCamapigns As New Campaign
                Dim dtTable As DataTable
                objCamapigns.ContactId = GetQueryStringVal( "ContID")
                dtTable = objCamapigns.ConECampaignDtls

                Dim objCampaign As New Campaign
                Dim dtECamp As DataTable
                objCamapigns.DomainID = Session("DomainID")
                dtECamp = objCampaign.ECampaignTemplates

                ddlEmailCampaign.DataSource = dtECamp
                ddlEmailCampaign.DataTextField = "vcECampName"
                ddlEmailCampaign.DataValueField = "numECampaignID"
                ddlEmailCampaign.DataBind()
                ddlEmailCampaign.Items.Insert(0, "--Select One--")
                ddlEmailCampaign.Items.FindByText("--Select One--").Value = 0
                If dtTable.Rows.Count = 1 Then
                    txtConCampaignID.Text = dtTable.Rows(0).Item("numConEmailCampID")
                    If dtTable.Rows(0).Item("bitEngaged") = True Then
                        radEnagaged.Checked = True
                        radDisengage.Checked = False
                    Else
                        radEnagaged.Checked = False
                        radDisengage.Checked = True
                    End If
                    If Not IsDBNull(dtTable.Rows(0).Item("intStartDate")) Then cal.SelectedDate = dtTable.Rows(0).Item("intStartDate")
                    If Not IsDBNull(dtTable.Rows(0).Item("numECampaignID")) Then
                        If Not ddlEmailCampaign.Items.FindByValue(dtTable.Rows(0).Item("numECampaignID")) Is Nothing Then
                            ddlEmailCampaign.Items.FindByValue(dtTable.Rows(0).Item("numECampaignID")).Selected = True
                        End If
                    End If

                    If Not IsDBNull(dtTable.Rows(0).Item("txtDesc")) Then lblDescription.Text = dtTable.Rows(0).Item("txtDesc")
                End If
            End If
            btnClose.Attributes.Add("onclick", "return Close()")
            btnSave.Attributes.Add("onclick", "return Save()")
            btnSaveClose.Attributes.Add("onclick", "return Save()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnAssign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAssign.Click
        Try
            txtConCampaignID.Text = 0
            ddlEmailCampaign.SelectedIndex = 0
            cal.SelectedDate = ""
            lblDescription.Text = ""
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            save()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            save()
            Response.Write("<script>self.close();</script>")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub save()
        Try
            Dim objCampaign As New Campaign
            objCampaign.ConEmailCampID = IIf(txtConCampaignID.Text.Trim = "", 0, txtConCampaignID.Text.Trim)
            objCampaign.ContactId = GetQueryStringVal( "ContID")
            objCampaign.ECampaignID = ddlEmailCampaign.SelectedItem.Value
            objCampaign.StartDate = cal.SelectedDate
            objCampaign.Engaged = IIf(radEnagaged.Checked = True, 1, 0)
            objCampaign.UserCntID = Session("UserContactID")
            txtConCampaignID.Text = objCampaign.ManageConECamp()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlEmailCampaign_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEmailCampaign.SelectedIndexChanged
        Try
            Dim objCampaign As New Campaign
            Dim dtTable As DataTable
            objCampaign.ECampaignID = ddlEmailCampaign.SelectedItem.Value
            dtTable = objCampaign.ECampaignDtls.Tables(0)
            If dtTable.Rows.Count = 1 Then lblDescription.Text = dtTable.Rows(0).Item("txtDesc")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class
