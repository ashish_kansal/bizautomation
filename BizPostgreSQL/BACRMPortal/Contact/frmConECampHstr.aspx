<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmConECampHstr.aspx.vb" Inherits="BACRMPortal.frmConECampHstr" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
		<title>E-Campaign History</title>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
			<br>
			<asp:table id="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" Width="100%" BorderColor="black" CssClass="aspTable"
				GridLines="None" Runat="server" Height=300>
				<asp:TableRow>
					<asp:TableCell VerticalAlign=Top >
						<table width="100%">
							<tr>
								<td class="text_bold">
									Email Campaign History :
								</td>
							</tr>
							<tr>
								<td>
									<asp:datagrid id="dgECampHstr" runat="server" AllowSorting="True" CssClass="dg" Width="100%" BorderColor="white"
										AutoGenerateColumns="False">
										<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
										<ItemStyle CssClass="is"></ItemStyle>
										<HeaderStyle CssClass="hs"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="ECamp" HeaderText="Campaign Name, Description"></asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Start Date">
															<ItemTemplate>
																<%# ReturnName(DataBinder.Eval(Container.DataItem, "intStartDate")) %>
															</ItemTemplate>
														</asp:TemplateColumn>
											<asp:BoundColumn DataField="Engaged" HeaderText="Status"></asp:BoundColumn>
											<asp:BoundColumn DataField="Activity" HeaderText="Last Activity,Sent On"></asp:BoundColumn>
										</Columns>
									</asp:datagrid>
								</td>
							</tr>
						</table>
					</asp:TableCell>
				</asp:TableRow>
			</asp:table></form>
	</body>
</HTML>
