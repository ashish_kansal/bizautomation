Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Common

Partial Public Class frmCusCaseList
    Inherits BACRMPage

    Dim SI As Integer = 0
    Dim SI1 As Integer = 0
    Dim SI2 As Integer = 0
    Dim frm As String = ""
    Dim frm1 As String = ""
    Dim frm2 As String = ""
    Dim strColumn As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("UserContactID") = Nothing Then
                Response.Redirect("../Common/frmLogout.aspx")
            End If
            Dim objCommon As New CCommon
            GetUserRightsForPage(15, 7)

            If GetQueryStringVal("SI") <> "" Then SI = GetQueryStringVal("SI")
            If GetQueryStringVal("SI1") <> "" Then
                SI1 = GetQueryStringVal("SI1")
            Else : SI1 = 0
            End If
            If GetQueryStringVal("SI2") <> "" Then
                SI2 = GetQueryStringVal("SI2")
            Else : SI2 = 0
            End If
            If GetQueryStringVal("frm") <> "" Then
                frm = ""
                frm = GetQueryStringVal("frm")
            Else : frm = ""
            End If
            If GetQueryStringVal("frm1") <> "" Then
                frm1 = ""
                frm1 = GetQueryStringVal("frm1")
            Else : frm1 = ""
            End If
            If GetQueryStringVal("SI2") <> "" Then
                frm2 = ""
                frm2 = GetQueryStringVal("frm2")
            Else : frm2 = ""
            End If
            'If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
            '    btnNew.Visible = False
            'End If
            If Not IsPostBack Then
                Session("Asc") = 1
                txtCurrrentPage.Text = 1
                BindDatagrid()
            End If
            If txtSortChar.Text <> "" Then
                ViewState("SortChar") = txtSortChar.Text
                ViewState("Column") = "vcCaseNumber"
                Session("Asc") = 0
                BindDatagrid()
                txtSortChar.Text = ""
            End If
            'btnNew.Attributes.Add("onclick", "return goto('../Cases/frmNewCase.aspx','cntOpenItem','divNew');")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindDatagrid()
        Try
            Dim SortChar As Char
            If ViewState("SortChar") <> "" Then
                SortChar = ViewState("SortChar")
            Else : SortChar = "0"
            End If
            Dim dtCases As DataTable
            Dim objCases As New CCases

            With objCases
                .DomainID = Session("DomainID")
                .SortCharacter = SortChar
                .DivisionID = Session("DivId")
                .UserRightType = m_aryRightsForPage(RIGHTSTYPE.VIEW)
                If txtCurrrentPage.Text.Trim <> "" Then
                    .CurrentPage = txtCurrrentPage.Text
                Else : .CurrentPage = 1
                End If
                .PageSize = Session("PagingRows")
                .TotalRecords = 0
                If ViewState("Column") <> "" Then
                    .columnName = ViewState("Column")
                Else : .columnName = "cs.bintcreateddate"
                End If
                If Session("Asc") = 1 Then
                    .columnSortOrder = "Desc"
                Else : .columnSortOrder = "Asc"
                End If
            End With
            dtCases = objCases.GetCaseListPortal
            If objCases.TotalRecords = 0 Then
                hidenav.Visible = False
                lblRecordCountCases.Text = 0
            Else
                hidenav.Visible = True
                lblRecordCountCases.Text = String.Format("{0:#,###}", objCases.TotalRecords)
                Dim strTotalPage As String()
                Dim decTotalPage As Decimal
                decTotalPage = lblRecordCountCases.Text / Session("PagingRows")
                decTotalPage = Math.Round(decTotalPage, 2)
                strTotalPage = CStr(decTotalPage).Split(".")
                If (lblRecordCountCases.Text Mod Session("PagingRows")) = 0 Then
                    lblTotal.Text = strTotalPage(0)
                    txtTotalPage.Text = strTotalPage(0)
                Else
                    lblTotal.Text = strTotalPage(0) + 1
                    txtTotalPage.Text = strTotalPage(0) + 1
                End If
                txtTotalRecords.Text = lblRecordCountCases.Text
            End If
            dgCases.DataSource = dtCases
            dgCases.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub lnkLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkLast.Click
        Try
            txtCurrrentPage.Text = txtTotalPage.Text
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkPrevious.Click
        Try
            If txtCurrrentPage.Text = 1 Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text - 1
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkFirst.Click
        Try
            txtCurrrentPage.Text = 1
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkPrevious.Click, lnkNext.Click
        Try
            If txtCurrrentPage.Text = txtTotalPage.Text Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 1
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk2.Click
        Try
            If txtCurrrentPage.Text + 1 = txtTotalPage.Text Or txtCurrrentPage.Text + 1 > txtTotalPage.Text Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 2
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk3.Click
        Try
            If txtCurrrentPage.Text + 2 = txtTotalPage.Text Or txtCurrrentPage.Text + 2 > txtTotalPage.Text Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 3
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk4.Click
        Try
            If txtCurrrentPage.Text + 3 = txtTotalPage.Text Or txtCurrrentPage.Text + 3 > txtTotalPage.Text Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 4
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk5.Click
        Try
            If txtCurrrentPage.Text + 4 = txtTotalPage.Text Or txtCurrrentPage.Text + 4 > txtTotalPage.Text Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 5
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub txtCurrrentPage_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCurrrentPage.TextChanged
        Try
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgCases_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgCases.SortCommand
        Try
            strColumn = e.SortExpression.ToString()
            If ViewState("Column") <> strColumn Then
                ViewState("Column") = strColumn
                Session("Asc") = 0
            Else
                If Session("Asc") = 0 Then
                    Session("Asc") = 1
                Else : Session("Asc") = 0
                End If
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgCases_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgCases.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As Button
                Dim lnkCaseDelete As LinkButton
                btnDelete = e.Item.FindControl("btnDeleteCases")
                lnkCaseDelete = e.Item.FindControl("lnkCaseDelete")
                If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                    btnDelete.Visible = False
                    lnkCaseDelete.Visible = True
                    lnkCaseDelete.Attributes.Add("onclick", "return DeleteMessage()")
                Else : btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgCases_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgCases.ItemCommand
        Try
            Dim lngCaseID As Long
            If Not e.CommandName = "Sort" Then lngCaseID = e.Item.Cells(0).Text
            If e.CommandName = "Case" Then
                Session("CaseID") = lngCaseID
                If Session("EnableIntMedPage") = 1 Then
                    Response.Redirect("../pagelayout/frmCustCasedtl.aspx?frm=CaseList" & "&SI1=0&SI2=" & SI1 & "&frm1=" & frm1 & "&frm2=" & frm2, False)
                Else : Response.Redirect("../cases/frmCusCaseDTL.aspx?frm=CaseList" & "&SI1=0&SI2=" & SI1 & "&frm1=" & frm1 & "&frm2=" & frm2, False)
                End If
            ElseIf e.CommandName = "Company" Then
                If Session("EnableIntMedPage") = 1 Then
                    Response.Redirect("../pagelayout/frmCustAccountdtl.aspx?frm=CaseList" & "&SI1=0&SI2=" & SI1 & "&frm1=" & frm1 & "&frm2=" & frm2, False)
                Else : Response.Redirect("../Account/frmCusAccounts.aspx?frm=CaseList" & "&SI1=0&SI2=" & SI1 & "&frm1=" & frm1 & "&frm2=" & frm2, False)
                End If
            ElseIf e.CommandName = "Contact" Then
                Dim objCommon As New CCommon
                objCommon.CaseID = lngCaseID
                objCommon.charModule = "S"
                objCommon.GetCompanySpecificValues1()
                Session("ContactId") = objCommon.ContactID
                If Session("EnableIntMedPage") = 1 Then
                    Response.Redirect("../pagelayout/frmCustContactdtl.aspx?frm=CaseList" & "&SI1=0&SI2=" & SI1 & "&frm1=" & frm1 & "&frm2=" & frm2, False)
                Else : Response.Redirect("../contacts/frmCstContacts.aspx?frm=CaseList" & "&SI1=0&SI2=" & SI1 & "&frm1=" & frm1 & "&frm2=" & frm2, False)
                End If
            ElseIf e.CommandName = "Delete" Then
                Dim objCases As New CCases
                objCases.CaseID = lngCaseID
                objCases.DomainID = Session("DomainID")
                If objCases.DeleteCase() = False Then
                    litMessage.Text = "Dependent Records Exists.Cannot be deleted."
                Else
                    BindDatagrid()
                    litMessage.Text = ""
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Function ReturnName(ByVal CreatedDate) As String
        Try
            Dim strCreateDate As String
            If IsDBNull(CreatedDate) = False Then
                strCreateDate = FormattedDateFromDate(CreatedDate, Session("DateFormat"))
                If Format(CreatedDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                    strCreateDate = "<font color=red>Today</font>"
                ElseIf Format(CreatedDate, "yyyyMMdd") < Format(Now(), "yyyyMMddhhmmss") Then
                    strCreateDate = "<font color=red>" & strCreateDate & "</font>"
                End If
            Else : strCreateDate = "-"
            End If
            Return strCreateDate
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function ReturnDateTime(ByVal CloseDate) As String
        Try
            Dim strTargetResolveDate As String = ""
            Dim temp As String = ""
            If Not IsDBNull(CloseDate) Then
                strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))

                Dim timePart As String = CloseDate.ToShortTimeString.Substring(0, CloseDate.ToShortTimeString.Length - 1)
                ' remove gaps
                If timePart.Split(" ").Length >= 2 Then timePart = timePart.Split(" ").GetValue(0) + timePart.Split(" ").GetValue(1)

                ' check Today date components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                ' if both are same it is today
                If (CloseDate.Date = Now.Date And CloseDate.Month = Now.Month And CloseDate.Year = Now.Year) Then
                    strTargetResolveDate = "<font color=red><b>Today</b></font>"
                    Return strTargetResolveDate

                    ' check [ TodayDate + 1 ] Date.... components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                    ' if both are same it was Yesterday
                ElseIf (CloseDate.Date.AddDays(1).Date = Now.Date And CloseDate.AddDays(1).Month = Now.Month And CloseDate.AddDays(1).Year = Now.Year) Then
                    strTargetResolveDate = "<font color=purple><b>Yesterday</b></font>"
                    Return strTargetResolveDate

                    ' check TodayDate .... components [ Date , Month , Year ] with Parameter [ CloseDate + 1 ] Date....  components [ Date , Month , Year ] 
                    ' if both are same it will Tomorrow
                ElseIf (CloseDate.Date = Now.AddDays(1).Date And CloseDate.Month = Now.AddDays(1).Month And CloseDate.Year = Now.AddDays(1).Year) Then
                    temp = CloseDate.Hour.ToString + ":" + CloseDate.Minute.ToString
                    strTargetResolveDate = "<font color=orange><b>Tomorrow</b></font>"
                    Return strTargetResolveDate

                    ' display day name for next 4 days from now
                ElseIf Format(CloseDate, "yyyyMMdd") = Format(DateTime.Now.AddDays(2), "yyyyMMdd") Or Format(CloseDate, "yyyyMMdd") = Format(DateTime.Now.AddDays(3), "yyyyMMdd") Or Format(CloseDate, "yyyyMMdd") = Format(DateTime.Now.AddDays(4), "yyyyMMdd") Or Format(CloseDate, "yyyyMMdd") = Format(DateTime.Now.AddDays(5), "yyyyMMdd") Then
                    strTargetResolveDate = "<b>" & CloseDate.DayOfWeek.ToString & "</b>"
                    Return strTargetResolveDate
                Else
                    strTargetResolveDate = strTargetResolveDate
                    Return strTargetResolveDate
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class

