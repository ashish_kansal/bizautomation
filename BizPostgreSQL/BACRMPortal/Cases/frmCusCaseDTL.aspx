<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCusCaseDTL.aspx.vb"
    Inherits="BACRMPortal.frmCusCaseDTL" %>

<<%--%@ Register TagPrefix="menu1" TagName="webmenu" Src="../common/topbar.ascx" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Src="~/Common/frmComments.ascx" TagName="frmComments" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
      <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <meta http-equiv="Page-Enter" content="blendtrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendtrans(Duration=0.01)">
    <script language="javascript" type="text/javascript">
        function openActionItem(a, b, c, d, e, f) {
            if (e == 'Email') {
                window.open("../contact/frmEmailMessage.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Email=" + a + "&Date=" + f, '', 'width=750,height=525,status=no,titlebar=no,scrollbars=yes,top=110,left=250')
                return false;
            }
            else {
                window.location.href = "../admin/actionitemdetails.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=Cases&CommId=" + a + "&CaseId=" + b + "&CaseTimeId=" + c + "&CaseExpId=" + d;
                return false;
            }

        }
        function chkAll() {
            for (i = 1; i <= 20; i++) {
                var str;
                if (i < 10) {
                    str = '0' + i
                }
                else {
                    str = i
                }
                if (document.getElementById('uwOppTab__ctl3_rptCorr_ctl00_chkDelete').checked == true) {
                    if (document.getElementById('uwOppTab__ctl3_rptCorr_ctl' + str + '_chkADelete') != null) {
                        document.getElementById('uwOppTab__ctl3_rptCorr_ctl' + str + '_chkADelete').checked = true;
                    }
                }
                else {
                    if (document.getElementById('uwOppTab__ctl3_rptCorr_ctl' + str + '_chkADelete') != null) {
                        document.getElementById('uwOppTab__ctl3_rptCorr_ctl' + str + '_chkADelete').checked = false;
                    }
                }
            }
        }

        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function NewTask(a, b) {
            //				alert("cntid : "+a);
            //				alert("caseId : "+b);
            window.location = "../Admin/newaction.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=Cases&CntID=" + a + "&CaseID=" + b;


        }
        function OpenEmailMessage(a, b) {
            window.open("../contact/frmEmailMessage.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Email=" + a + "&Date=" + b, '', 'width=650,height=350,status=no,titlebar=no,scrollbar=yes,top=110,left=150')
            return false;
        }
        function ShowWindow(Page, q, att) {
            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                document.Form1.ddlStatus.style.display = 'none';
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                document.Form1.ddlStatus.style.display = '';
                return false;

            }
        }
        function openSol(a) {
            window.open("../cases/frmCaseLinkSol.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&CaseID=" + a, '', 'width=800,height=500,status=no,titlebar=no,scrollbar=yes,top=110,left=150,resizable=yes')
            //			document.getElementById('cntdoc'].src="../cases/frmCaseLinkSol.aspx?CaseID="+a 
            //			document.getElementById('divSol'].style.visibility = "visible";
            return false;

        }
        function AddContact() {
            if (document.Form1.uwOppTab__ctl1_ddlAssocContactId.value == 0) {
                alert("Select Contact");
                document.Form1.uwOppTab.tabIndex = 1;
                document.Form1.uwOppTab__ctl1_ddlAssocContactId.focus();
                return false;
            }
            var str;
            for (i = 0; i < document.Form1.elements.length; i++) {
                if (i <= 9) {
                    str = '0' + (i + 1)
                }
                else {
                    str = i + 1
                }
                if (document.getElementById('uwOppTab__ctl1_dgContact_ctl' + str + '_txtContactID') != null) {
                    if (document.getElementById('uwOppTab__ctl1_dgContact_ctl' + str + '_txtContactID').value == document.Form1.uwOppTab__ctl1_ddlAssocContactId.value) {
                        alert("Associated contact is already added");
                        return false;
                    }
                }
            }

        }
    </script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <menu1:webmenu ID="webmenu1" runat="server"></menu1:webmenu>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
        EnableViewState="true">
        <ContentTemplate>
            <table id="tblMenu" bordercolor="black" cellspacing="0" cellpadding="0" width="100%"
                border="0" runat="server">
                <tr>
                    <td class="tr1" align="center">
                        <b>Record Owner: </b>
                        <asp:Label ID="lblRecordOwner" runat="server" ForeColor="Black"></asp:Label>
                    </td>
                    <td class="td1" width="1" height="18">
                    </td>
                    <td class="tr1" align="center">
                        <b>Created By: </b>
                        <asp:Label ID="lblCreatedBy" runat="server" ForeColor="Black"></asp:Label>
                    </td>
                    <td class="td1" width="1" height="18">
                    </td>
                    <td class="tr1" align="center">
                        <b>Last Modified By: </b>
                        <asp:Label ID="lblLastModifiedBy" runat="server" ForeColor="Black"></asp:Label>
                    </td>
                </tr>
            </table>
            <table width="100%" align="center">
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td class="normal1" align="center">
                                    Organization : <u>
                                        <asp:HyperLink ID="hplCustomer" runat="server" CssClass="hyperlink"></asp:HyperLink>
                                </td>
                                <td class="normal1" align="right">
                                    Case Number :
                                </td>
                                <td class="normal1">
                                    <asp:Label ID="lblcasenumber" ForeColor="Red" runat="server"></asp:Label>
                                </td>
                                <td class="normal1" align="right">
                                    Status
                                </td>
                                <td class="normal1">
                                    <asp:DropDownList ID="ddlStatus" runat="server" Width="200" CssClass="signup">
                                    </asp:DropDownList>
                                </td>
                                <td align="right">
                                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"></asp:Button>
                                    <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Save &amp; Close">
                                    </asp:Button>
                                    <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close"></asp:Button>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <igtab:UltraWebTab ImageDirectory="" ID="uwOppTab" runat="server" ThreeDEffect="true"
                BorderStyle="Solid" Width="100%" BarHeight="0" BorderWidth="0">
                <DefaultTabStyle Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Arial">
                </DefaultTabStyle>
                <RoundedImage LeftSideWidth="7" RightSideWidth="8" ShiftOfImages="0" SelectedImage="../images/ig_tab_winXPs3.gif"
                    NormalImage="../images/ig_tab_winXP3.gif" HoverImage="../images/ig_tab_winXPs3.gif"
                    FillStyle="LeftMergedWithCenter"></RoundedImage>
                <SelectedTabStyle Height="23px" ForeColor="white">
                </SelectedTabStyle>
                <HoverTabStyle Height="23px" ForeColor="white">
                </HoverTabStyle>
                <Tabs>
                    <igtab:Tab Text="&nbsp;&nbsp;Case Details&nbsp;&nbsp;">
                        <ContentTemplate>
                            <asp:Table ID="tblCase" CellPadding="0" CellSpacing="0" BorderWidth="1" Height="300"
                                runat="server" CssClass="aspTable" Width="100%" BorderColor="black" GridLines="None">
                                <asp:TableRow>
                                    <asp:TableCell VerticalAlign="Top">
                                        <br>
                                        <table width="100%" border="0" id="tblDetails" runat="server">
                                            <tr>
                                                <td valign="top" rowspan="30">
                                                    <img src="../images/SuitCase-32.gif" />
                                                </td>
                                                <td class="normal1" align="right">
                                                    Subject
                                                </td>
                                                <td class="normal1">
                                                    <asp:TextBox ID="txtSubject" runat="server" CssClass="signup" Width="180"></asp:TextBox>
                                                </td>
                                                <td class="normal1" align="right">
                                                    Resolve Date
                                                </td>
                                                <td class="normal1">
                                                    <BizCalendar:Calendar ID="calResolve" runat="server" />
                                                </td>
                                                <td class="normal1" align="right">
                                                    Assigned to
                                                </td>
                                                <td class="normal1">
                                                    <asp:DropDownList ID="ddlAssignedTo" runat="server" CssClass="signup" Width="180">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="normal1" align="right">
                                                    <asp:Label ID="lblContact" runat="server">Contact : </asp:Label>
                                                </td>
                                                <td class="normal1">
                                                    <asp:HyperLink ID="hLinkContact" runat="server"></asp:HyperLink>
                                                </td>
                                                <td class="normal1" align="right">
                                                    <asp:Label ID="lblPhone" runat="server">Phone, Ext : </asp:Label>
                                                </td>
                                                <td class="normal1">
                                                    <asp:Label ID="Label8" runat="server"></asp:Label>
                                                </td>
                                                <td class="normal1" align="right">
                                                    <asp:Label ID="lblEmail" runat="server">Email : </asp:Label>
                                                </td>
                                                <td class="normal1">
                                                    <asp:HyperLink ID="hLinkEmailID" runat="server"></asp:HyperLink>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="normal1" align="right">
                                                    Priority
                                                </td>
                                                <td class="normal1">
                                                    <asp:DropDownList ID="ddlPriority" runat="server" CssClass="signup" Width="180">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="normal1" align="right">
                                                    Type
                                                </td>
                                                <td class="normal1">
                                                    <asp:DropDownList ID="ddlType" runat="server" CssClass="signup" Width="180">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="normal1" align="right">
                                                    Reason
                                                </td>
                                                <td class="normal1">
                                                    <asp:DropDownList ID="ddlCommonReasons" runat="server" CssClass="signup" Width="180">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="normal1" align="right">
                                                    Origin
                                                </td>
                                                <td class="normal1">
                                                    <asp:DropDownList ID="ddlOrgin" runat="server" CssClass="signup" Width="180">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="normal1" align="right">
                                                    Opportunity Or Deal
                                                </td>
                                                <td class="normal1" colspan="5">
                                                    <asp:ListBox ID="lstOpportunity" runat="server" CssClass="signup" Width="300" SelectionMode="Multiple">
                                                    </asp:ListBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="100%">
                                            <tr>
                                                <td class="normal1" align="right">
                                                    Description
                                                </td>
                                                <td class="normal1" colspan="5">
                                                    <asp:TextBox ID="txtdescription" runat="server" CssClass="signup" TextMode="MultiLine"
                                                        Width="650" Height="80"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr id="trInt" runat="server" visible="false">
                                                <td class="normal1" align="right">
                                                    Internal Comments
                                                </td>
                                                <td class="normal1" colspan="5">
                                                    <asp:TextBox ID="txtIntComments" runat="server" CssClass="signup" TextMode="MultiLine"
                                                        Width="650" Height="80"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <asp:Panel ID="pnlKnowledgeBase" runat="server">
                                                <tr>
                                                    <td class="normal1" align="right">
                                                        Solutions added from<br>
                                                        the Knowledge Base
                                                    </td>
                                                    <td colspan="5">
                                                        <asp:DataGrid ID="dgSolution" runat="server" CssClass="dg" Width="100%" BorderColor="white"
                                                            ShowFooter="False">
                                                            <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                                            <ItemStyle CssClass="is"></ItemStyle>
                                                            <HeaderStyle CssClass="hs"></HeaderStyle>
                                                        </asp:DataGrid>
                                                        <br>
                                                    </td>
                                                </tr>
                                            </asp:Panel>
                                            <tr>
                                                <td colspan="6" class="normal1">
                                                    <uc1:frmComments ID="frmComments1" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                        <br>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </igtab:Tab>
                    <igtab:Tab Text="&nbsp;&nbsp;Associated Contacts&nbsp;&nbsp;">
                        <ContentTemplate>
                            <asp:Table ID="Table1" BorderWidth="1" Height="300" runat="server" Width="100%" BorderColor="black"
                                CssClass="aspTable" GridLines="None">
                                <asp:TableRow>
                                    <asp:TableCell VerticalAlign="Top">
                                        <br />
                                        <table width="100%">
                                            <tr>
                                                <td class="normal1" align="right">
                                                    Contact
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlAssocContactId" runat="server" CssClass="signup" Width="180">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="normal1" align="right">
                                                    Contact Role
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlContactrole" runat="server" CssClass="signup" Width="180">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="normal1" align="right">
                                                    Share Opportunity via Partner Point ?
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkShare" runat="server" />&nbsp;&nbsp;&nbsp;
                                                    <asp:Button ID="btnAddContact" runat="server" CssClass="button" Text="Add Contact">
                                                    </asp:Button>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                        <asp:DataGrid ID="dgContact" runat="server" CssClass="dg" Width="100%" BorderColor="white"
                                            AutoGenerateColumns="False">
                                            <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                            <ItemStyle CssClass="is"></ItemStyle>
                                            <HeaderStyle CssClass="hs"></HeaderStyle>
                                            <Columns>
                                                <asp:BoundColumn DataField="numContactId" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="bitPartner" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderText="Organization,Relationship" DataField="Company"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderText="First & Last Name" DataField="Name"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderText="Phone - Ext" DataField="Phone"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderText="Email" DataField="Email"></asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="Contact Role">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label4" runat="server" CssClass="normal1" Text='<%# DataBinder.Eval(Container.DataItem, "Contactrole") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Share Opportunity via Partner Point ?">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblShare" runat="server" CssClass="cell" Font-Size="Large"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn>
                                                    <HeaderTemplate>
                                                        <asp:Button ID="btnHdeleteCnt" runat="server" CssClass="Delete" Text="X"></asp:Button>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtContactID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "numContactId") %>'>
                                                        </asp:TextBox>
                                                        <asp:Button ID="btnDeleteCnt" runat="server" CssClass="Delete" Text="X" CommandName="Delete">
                                                        </asp:Button>
                                                        <asp:LinkButton ID="lnkDeleteCnt" runat="server" Visible="false">
																<font color="#730000">*</font></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                        <br>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </igtab:Tab>
                </Tabs>
            </igtab:UltraWebTab>
            <table width="100%">
                <tr>
                    <td class="normal4" align="center">
                        <asp:Literal ID="litMessage" runat="server"></asp:Literal>
                    </td>
                </tr>
            </table>
            <asp:TextBox ID="txtTotalPage" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtTotalRecords" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtdivId" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtCntId" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtLstUptCom" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtEmailTotalPage" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtEmailTotalRecords" Style="display: none" runat="server"></asp:TextBox>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
--%>