<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCaseLinkSol.aspx.vb" Inherits="BACRMPortal.frmCaseLinkSol" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
		<title>Solution</title>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<br>
			<table align="right">
				<tr>
					<td class="normal1">
						Find Solution using Name or Keyword
					</td>
					<td>
						<asp:TextBox id="txtKeyWord" Runat="server" CssClass="signup" Width="200"></asp:TextBox>
					</td>
					<td>
						<asp:Button id="btnGoBases" Runat="server" CssClass="button" Text="Go" width="25"></asp:Button>
					</td>
				</tr>
			</table>
			<br>
			<br>
			<asp:datagrid id="dgBases" runat="server" Width="100%" CssClass="dg" AllowSorting="True" AutoGenerateColumns="False"
				BorderColor="white">
				<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
				<ItemStyle CssClass="is"></ItemStyle>
				<HeaderStyle CssClass="hs"></HeaderStyle>
				<Columns>
					<asp:BoundColumn Visible="False" DataField="numSolnID"></asp:BoundColumn>
					<asp:buttonColumn DataTextField="vcSolnTitle" SortExpression="vcSolnTitle" HeaderText="<font color=white>Solution Name</font>"
						CommandName="Solution"></asp:buttonColumn>
					<asp:BoundColumn DataField="txtSolution" SortExpression="txtSolution" HeaderText="<font color=white>Solution Description</font>"></asp:BoundColumn>
					<asp:TemplateColumn>
						<ItemStyle Width="10"></ItemStyle>
						<HeaderTemplate>
							<asp:Button ID="btnHSoldelete" Runat="server" CssClass="button" Text="Link"></asp:Button>
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Button ID="btnSolDelete" Runat="server" CssClass="button" Text="Link" CommandName="Link"></asp:Button>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
			</asp:datagrid>
		</form>
	</body>
</HTML>