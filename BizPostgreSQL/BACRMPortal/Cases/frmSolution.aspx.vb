'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common

Public Class frmSolution : Inherits BACRMPage

    Dim SI As Integer = 0
    Dim SI1 As Integer = 0
    Dim SI2 As Integer = 0
    Dim frm As String = ""
    Dim frm1 As String = ""
    Dim frm2 As String = ""
    Dim objCommon As CCommon

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Dim lngSolId As Long

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            lngSolId = GetQueryStringVal( "SolId")
            If Not GetQueryStringVal( "SI") Is Nothing Then
                SI = GetQueryStringVal( "SI")
            End If
            If Not GetQueryStringVal( "SI1") Is Nothing Then
                SI1 = GetQueryStringVal( "SI1")
            Else : SI1 = 0
            End If
            If Not GetQueryStringVal( "SI2") Is Nothing Then
                SI2 = GetQueryStringVal( "SI2")
            Else : SI2 = 0
            End If
            If Not GetQueryStringVal( "frm") Is Nothing Then
                frm = ""
                frm = GetQueryStringVal( "frm")
            Else : frm = ""
            End If
            If Not GetQueryStringVal( "frm1") Is Nothing Then
                frm1 = ""
                frm1 = GetQueryStringVal( "frm1")
            Else : frm1 = ""
            End If
            If Not GetQueryStringVal( "SI2") Is Nothing Then
                frm2 = ""
                frm2 = GetQueryStringVal( "frm2")
            Else : frm2 = ""
            End If
            If Not IsPostBack Then
                Session("Help") = "Support"
                objCommon = New CCommon
                Dim m_aryRightsForPage(), m_aryRightsForLinkToCases() As Integer
                m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmSolution.aspx", Session("UserContactID"), 31, 8)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AC")
                Else
                    If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                        btnSave.Visible = False
                        btnSaveClose.Visible = False
                    End If
                End If
                m_aryRightsForLinkToCases = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmSolution.aspx", Session("UserContactID"), 31, 9)
                'If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then btnlnkCase.Visible = False
                LoadInformation()
            End If
            btnLinkGo.Attributes.Add("onclick", "return fn_GoToURL(txtLink.value);")
            hplDocuments.Attributes.Add("onclick", "return OpenDocuments(" & lngSolId & ");")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadInformation()
        Try
            objCommon.sb_FillComboFromDBwithSel(ddlCategory, 34, Session("DomainID"))
            Dim objSolution As New Solution
            Dim dtSolDetails As DataTable
            objSolution.SolID = lngSolId
            dtSolDetails = objSolution.GetSolutionForEdit
            txtSolution.Text = dtSolDetails.Rows(0).Item("vcSolnTitle")
            txtLink.Text = dtSolDetails.Rows(0).Item("vcLink")
            If Not ddlCategory.Items.FindByValue(dtSolDetails.Rows(0).Item("numCategory")) Is Nothing Then
                ddlCategory.Items.FindByValue(dtSolDetails.Rows(0).Item("numCategory")).Selected = True
            End If
            txtSolDesc.Text = dtSolDetails.Rows(0).Item("txtSolution")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Sub LoadCases()
    '    Try
    '        ddlCaseNo.Items.Clear()
    '        Dim i As Integer
    '        Dim ObjCases As New CCases
    '        Dim dtCaseNo As DataTable
    '        ObjCases.DivisionID = ddlCompany.SelectedItem.Value
    '        dtCaseNo = ObjCases.GetOpenCases
    '        For i = 0 To dtCaseNo.Rows.Count - 1
    '            ddlCaseNo.Items.Add(New ListItem(dtCaseNo.Rows(i).Item("vcCaseNumber"), dtCaseNo.Rows(i).Item("numCaseId")))
    '        Next
    '        ddlCaseNo.Items.Insert(0, "--Select One--")
    '        ddlCaseNo.Items.FindByText("--Select One--").Value = 0
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub btnlnkCase_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnlnkCase.Click
    '    Try
    '        Dim str As String()
    '        str = CStr(ddlCaseNo.SelectedItem.Value).Split("~")
    '        Dim objSolution As New Solution
    '        objSolution.CaseID = str(0)
    '        objSolution.SolID = lngSolId
    '        objSolution.LinkSolToCases()
    '        Response.Redirect("../cases/frmCases.aspx?frm=Solution&frm=cases&CaseID=" & str(0) & "&DivID=" & str(1) & "&CntID=" & str(2) & "&SolId=" & lngSolId & "&Index=1")
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    Sub Save()
        Try
            Dim objSolution As New Solution
            objSolution.Category = ddlCategory.SelectedItem.Value
            objSolution.SolName = txtSolution.Text
            objSolution.SolDesc = txtSolDesc.Text
            objSolution.SolID = lngSolId
            objSolution.Link = txtLink.Text
            objSolution.UserCntID = Session("UserContactID")
            objSolution.SaveSolution()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Save()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            Save()
            Response.Redirect("../cases/frmCaseList.aspx?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            If frm = "frmCases" Then
                Response.Redirect("../pagelayout/frmCustCasedtl.aspx?frm=CaseList" & "&SI1=0&SI2=" & SI1 & "&frm1=" & frm1 & "&frm2=" & frm2, False)
            Else
                Response.Redirect("../cases/frmCaseList.aspx?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    'Private Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
    '    Try
    '        LoadCases()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Public Sub FillDDL()
    '    Try
    '        Dim objCommon As New CCommon
    '        With objCommon
    '            .DomainID = Session("DomainID")
    '            .Filter = Trim(txtCompName.Text) & "%"
    '            .UserCntID = Session("UserContactID")
    '            ddlCompany.DataSource = objCommon.PopulateOrganization.Tables(0).DefaultView
    '            ddlCompany.DataTextField = "vcCompanyname"
    '            ddlCompany.DataValueField = "numDivisionID"
    '            ddlCompany.DataBind()
    '            ddlCompany.Items.Insert(0, "--Select One--")
    '            ddlCompany.Items.FindByText("--Select One--").Value = 0
    '        End With
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
    '    Try
    '        FillDDL()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

End Class

