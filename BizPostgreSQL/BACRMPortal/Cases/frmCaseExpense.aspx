<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCaseExpense.aspx.vb" Inherits="BACRMPortal.frmCaseExpense" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" src="../include/calandar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
        <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
          <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
		<title>Expense</title>
	</head>
	<body>
		<script type="text/javascript"  language="javascript">
			function Close(a)
			{
				window.close();
			}
			function Save()
			{
				if (document.Form1.txtAmount.value=="")
				{
					alert("Enter Amount")
					document.Form1.txtAmount.focus();
					return false;
				}
				if (document.Form1.radBill.checked==true)
				{
				    if (document.Form1.ddlOpp.value=="0")
				    {
				       alert("Select Opportunity")
					    document.Form1.ddlOpp.focus();
					    return false; 
				    }
				}
			}
		</script>
		<form id="Form1"   method="post" runat="server">
			<table width="100%" >
				<tr>
				   
					<td align="right" colSpan="3">
						<asp:Button ID="btndelete" Runat="server" CssClass="button" Text="Delete" Width=50></asp:Button>
						<asp:Button ID="btnSave" Runat="server" CssClass="button" Text="Save &amp; Close"></asp:Button>
						<asp:Button ID="btnCancel" Runat="server" CssClass="button" Text="Close"></asp:Button>
						<br>
						<br>
					</td>
				</tr>
				<tr>
				    <td></td>
				     <td align="left" valign="middle">
				        <asp:RadioButton ID="radBill" runat="server" CssClass="normal1" GroupName="Bill" Text="Billable"  Checked="true" />
				        <asp:RadioButton ID="radNonBill" runat="server" CssClass="normal1" GroupName="Bill" Text="Non-Billable" />
				    </td>
				</tr>
				<tr>
					<td class="normal1" align="right">Amount
					</td>
					<td class="normal1"><asp:textbox CssClass=signup id="txtAmount" Runat="server"></asp:textbox>
					 <asp:CheckBox runat="server" Text="Reimbursable" ID="chkReimb" CssClass="signup" />
					</td>
					<td class="normal1" noWrap align="right" style="DISPLAY:none">Select BizDoc
					</td>
					<td style="DISPLAY:none"><asp:dropdownlist id="ddlBizDoc" Runat="server" CssClass="signup" Width="130px"></asp:dropdownlist></td>
				</tr>
				<tr>
		             <td class="normal1" align="right">
                               Created Date 
                     </td>
		            <td>
		                <table>
		                    <tr>
                            
                           <td>
                              <BizCalendar:Calendar ID="CalCreated" runat="server" />
                           </td>
                           <td></td>
								            <td class="text" colspan="2" id="td1" runat="server" ><IMG src="../images/Clock-16.gif"  align="absMiddle">
									            <asp:dropdownlist id="ddlCreatedTime" Runat="server" Width="55" CssClass="signup">
										            <asp:ListItem Selected="False" Value="23">12:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="24">12:30</asp:ListItem>
										            <asp:ListItem Selected="False" Value="1">1:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="2">1:30</asp:ListItem>
										            <asp:ListItem Selected="False" Value="3">2:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="4">2:30</asp:ListItem>
										            <asp:ListItem Selected="False" Value="5">3:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="6">3:30</asp:ListItem>
										            <asp:ListItem Selected="False" Value="7">4:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="8">4:30</asp:ListItem>
										            <asp:ListItem Selected="False" Value="9">5:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="10">5:30</asp:ListItem>
										            <asp:ListItem Selected="False" Value="11">6:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="12">6:30</asp:ListItem>
										            <asp:ListItem Selected="False" Value="13">7:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="14">7:30</asp:ListItem>
										            <asp:ListItem Selected="True" Value="15">8:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="16">8:30</asp:ListItem>
										            <asp:ListItem Selected="False" Value="17">9:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="18">9:30</asp:ListItem>
										            <asp:ListItem Selected="False" Value="19">10:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="20">10:30</asp:ListItem>
										            <asp:ListItem Selected="False" Value="21">11:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="22">11:30</asp:ListItem>
									            </asp:dropdownlist>&nbsp;AM<input id="chkCreatedAm" type="radio" CHECKED value="0" name="AM" runat="server"/>PM<input id="chkCreatedPm" type="radio" value="1" name="AM" runat="server"/>
								            </td>								            
                        </tr>
		                </table>
		            </td>
		        </tr>
				</table>
				 <table width="100%" class="normal1">
				    <tr>
					    <td class="normal1" align="right">Contract
					    </td>
					    <td ><asp:DropDownList ID="ddlContract" AutoPostBack="true" Width="180" runat="server"  CssClass="signup"></asp:DropDownList>&nbsp; (Apply  Expenses to the Contract )</td>
				    </tr>
				    <tr>
				       <td align="right">
				            Contract Amount Balance
				        </td>
				        <td align="left">
				          <asp:Label ID="lblRemAmount" runat="server" CssClass="normal1"></asp:Label>
				       </td>
				    </tr>
				 <tr class="normal1" align="right">
				        <td align="right" class="normal1">
				            Opportunity 
				        </td>
				        <td  colSpan="2" align="left" class="normal1">
				            <asp:DropDownList ID="ddlOpp" runat="server" CssClass="signup" Width="300" ></asp:DropDownList>
				        </td>
				    </tr>
				<tr>
				
					<td class="normal1" align="right">Description
					</td>
					<td colSpan="2"><asp:TextBox TextMode="MultiLine"  Runat="server" Width="400" Height="50" id="txtDesc"></asp:TextBox></td>
				</tr>
				 <tr>
				        <td align="center" colspan="3" class="normal4">
				          <asp:Literal runat="server" id="litMessage" EnableViewState="False"  ></asp:Literal> 				            
				        </td>
				    </tr>
			    </table>
		<asp:textbox id="txtCategoryHDRID" Runat="server" Text="0" style="display:none" ></asp:textbox>
		<asp:textbox id="txtCaseId" Runat="server" Text="0" style="display:none" ></asp:textbox>
		<asp:textbox id="txtCommId" Runat="server" Text="0" style="display:none" ></asp:textbox>
		<asp:textbox id="txtDivId" Runat="server" Text="0" style="display:none" ></asp:textbox>
		</form>
	</body>
</html>
