'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Outlook

Public Class frmAddCases : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ' pnlDeal.Visible = False   '' Support key has been removed
            If Not IsPostBack Then
                Session("Help") = "Support"
                Dim objCommon As New CCommon
                ' Checking the rights for View
                
                m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmAddCases.aspx", Session("UserContactID"), 31, 1)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AS")
                If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                    btnSaveClose.Visible = False
                    Exit Sub
                Else : btnSaveClose.Visible = True
                End If
                If GetQueryStringVal( "uihTR") <> "" Or GetQueryStringVal( "fghTY") <> "" Or GetQueryStringVal( "rtyWR") <> "" Or GetQueryStringVal( "tyrCV") <> "" Or GetQueryStringVal( "pluYR") <> "" Then
                    If GetQueryStringVal( "uihTR") <> "" Then
                        objCommon.ContactID = GetQueryStringVal( "uihTR")
                        objCommon.charModule = "C"
                    ElseIf GetQueryStringVal( "rtyWR") <> "" Then
                        objCommon.DivisionID = GetQueryStringVal( "rtyWR")
                        objCommon.charModule = "D"
                    ElseIf GetQueryStringVal( "tyrCV") <> "" Then
                        objCommon.ProID = GetQueryStringVal( "tyrCV")
                        objCommon.charModule = "P"
                    ElseIf GetQueryStringVal( "pluYR") <> "" Then
                        objCommon.OppID = GetQueryStringVal( "pluYR")
                        objCommon.charModule = "O"
                    ElseIf GetQueryStringVal( "fghTY") <> "" Then
                        objCommon.CaseID = GetQueryStringVal( "fghTY")
                        objCommon.charModule = "S"
                    End If
                    objCommon.GetCompanySpecificValues1()

                    Dim strCompany, strDivision As String
                    objCommon.DivisionID = objCommon.DivisionID
                    strCompany = objCommon.GetCompanyName
                    ddlCompany.Items.Add(strCompany)
                    ddlCompany.Items.FindByText(strCompany).Value = objCommon.DivisionID
                    FillContact()
                    If Not ddlContacts.Items.FindByValue(objCommon.ContactID) Is Nothing Then ddlContacts.Items.FindByValue(objCommon.ContactID).Selected = True
                End If
                If GetQueryStringVal( "frm") = "outlook" Then
                    Dim objOutlook As New COutlook
                    Dim dttable As DataTable
                    objOutlook.numEmailHstrID = GetQueryStringVal( "EmailHstrId")
                    objOutlook.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    dttable = objOutlook.getMail()
                    If dttable.Rows.Count > 0 Then txtDescription.Text = dttable.Rows(0).Item("vcBodyText")
                End If
            End If
            btnSaveClose.Attributes.Add("onclick", "return Save()")
            btnClose.Attributes.Add("onclick", "return Close()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Public Function FillCustomer()
        Try
            Dim objCommon As New CCommon
            With objCommon
                .DomainID = Session("DomainID")
                .Filter = Trim(txtCompName.Text) & "%"
                .UserCntID = Session("UserContactID")
                ddlCompany.DataSource = objCommon.PopulateOrganization.Tables(0).DefaultView
                ddlCompany.DataTextField = "vcCompanyname"
                ddlCompany.DataValueField = "numDivisionID"
                ddlCompany.DataBind()
            End With
            ddlCompany.Items.Insert(0, New ListItem("---Select One---", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            FillCustomer()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            FillContact()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Public Function FillContact()
        Try
            Dim fillCombo As New COpportunities
            With fillCombo
                .DivisionID = ddlCompany.SelectedItem.Value
                ddlContacts.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
                ddlContacts.DataTextField = "Name"
                ddlContacts.DataValueField = "numcontactId"
                ddlContacts.DataBind()
            End With
            ddlContacts.Items.Insert(0, New ListItem("---Select One---", "0"))
            If ddlContacts.Items.Count = 2 Then
                ddlContacts.Items(1).Selected = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            Dim objCases As New CCases
            Dim CaseNo As Long
            CaseNo = objCases.GetMaxCaseID
            Dim CaseId As Integer
            With objCases

                ''Modified by Shaziya on 28th March 07
                ''Support key Type has been removed

                'If radDealSupportKey.Checked = True Then
                '    .OppID = ddlDealSupport.SelectedItem.Value
                'End If

                .CaseNo = Format(CaseNo, "00000000000")
                .DivisionID = ddlCompany.SelectedItem.Value
                .ContactID = ddlContacts.SelectedItem.Value
                .Subject = txtSubject.Text
                .Desc = txtDescription.Text
                .DomainID = Session("DomainID")
                .UserCntId = Session("UserContactID")
                .TargetResolveDate = calResolve.SelectedDate
                ''Modified By Shaziya On 27th March 07
                ''SupportKeyType is by default Universal 

                .SupportKeyType = 0

                'If radUniversalSupportKey.Checked = True Then
                '    .SupportKeyType = 0
                'Else
                '    .SupportKeyType = 1
                'End If

            End With
            CaseId = objCases.SaveCases
            ''Sending mail to Account holder and Manager
            Try

                Dim objAlerts As New CAlerts
                Dim dtDetails As DataTable
                objAlerts.AlertDTLID = 11 'Alert DTL ID for sending alerts in opportunities
                objAlerts.DomainID = Session("DomainID")
                dtDetails = objAlerts.GetIndAlertDTL
                If dtDetails.Rows.Count > 0 Then
                    If dtDetails.Rows(0).Item("tintAlertOn") = 1 Then
                        Dim dtEmailTemplate As DataTable
                        Dim objDocuments As New DocumentList
                        objDocuments.GenDocID = dtDetails.Rows(0).Item("numEmailTemplate")
                        objDocuments.DomainID = Session("DomainId")
                        dtEmailTemplate = objDocuments.GetDocByGenDocID
                        If dtEmailTemplate.Rows.Count > 0 Then
                            Dim objSendMail As New Email
                            Dim dtMergeFields As New DataTable
                            Dim drNew As DataRow
                            dtMergeFields.Columns.Add("CaseID")
                            dtMergeFields.Columns.Add("Organization")
                            dtMergeFields.Columns.Add("Employee")
                            drNew = dtMergeFields.NewRow
                            drNew("CaseID") = CaseId
                            drNew("Organization") = ddlCompany.SelectedItem.Text
                            drNew("Employee") = Session("ContactName")
                            dtMergeFields.Rows.Add(drNew)
                            Dim objCommon As New CCommon
                            objCommon.byteMode = 0
                            objCommon.ContactID = Session("UserContactID")
                            objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), IIf(dtDetails.Rows(0).Item("tintCCManager") = 1, objCommon.GetManagerEmail, ""), Session("UserEmail"), Session("UserEmail"), dtMergeFields)
                        End If
                    End If
                End If
            Catch ex As Exception

            End Try
            Dim strScript As String = "<script language=JavaScript>"
            strScript += "window.opener.reDirectPage('../cases/frmCases.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&CaseID=" & CaseId & "'); self.close();"
            strScript += "</script>"
            If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    ''Modified By Shaziya On 27th March 07
    ''SupportKeyType is by default Universal 
    'Private Sub radDealSupportKey_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radDealSupportKey.CheckedChanged
    '    If radDealSupportKey.Checked = True Then
    '        pnlDeal.Visible = True
    '    End If
    '    ddlDealSupport.Items.Clear()
    '    Dim objCases As New CCases
    '    Dim dtDealSupportKey As New DataTable
    '    Dim i As Integer
    '    If ddlCompany.SelectedIndex > 0 Then
    '        objCases.DivisionID = ddlCompany.SelectedItem.Value
    '        dtDealSupportKey = objCases.GetDealSupportKey
    '    End If
    '    For i = 0 To dtDealSupportKey.Rows.Count - 1
    '        ddlDealSupport.Items.Add(New ListItem("D" & Format(dtDealSupportKey.Rows(i).Item("numOppID"), "00000000000"), dtDealSupportKey.Rows(i).Item("numOppID")))
    '    Next
    '    ddlDealSupport.Items.Insert(0, "--Select One--")
    '    ddlDealSupport.Items.FindByText("--Select One--").Value = 0
    'End Sub

    'Private Sub radUniversalSupportKey_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radUniversalSupportKey.CheckedChanged
    '    If radUniversalSupportKey.Checked = True Then
    '        pnlDeal.Visible = False
    '    End If
    'End Sub

    Public Function StripTags(ByVal HTML As String) As String
        Try
            ' Removes tags from passed HTML
            Dim objRegEx As  _
                System.Text.RegularExpressions.Regex
            Dim str As String = objRegEx.Replace(HTML, "<[^>]*>", "").Replace("P {", "")
            str = str.Replace("MARGIN-TOP: 0px; MARGIN-BOTTOM: 0px", "")
            Return str.Trim
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class

