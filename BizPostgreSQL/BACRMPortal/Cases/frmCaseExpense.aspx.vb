Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Contract
Imports BACRM.BusinessLogic.TimeAndExpense
Imports BACRM.BusinessLogic.Common
Partial Public Class frmCaseExpense
    Inherits BACRMPage

    Dim CatHdrId As Long = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            CatHdrId = GetQueryStringVal( "CatHdrId")
            If Not IsPostBack Then
                CalCreated.SelectedDate = Now()
                If CatHdrId <> 0 Then
                    getdetails()
                Else
                    txtCaseId.Text = GetQueryStringVal( "CaseId")
                    txtCommId.Text = GetQueryStringVal( "CommId")
                    txtDivId.Text = GetQueryStringVal( "DivId")
                    LoadContractsInfo(txtDivId.Text)
                    loadOpportunities(txtCaseId.Text)
                    getdetails()
                End If
            End If

            If GetQueryStringVal( "frm") <> "TE" Then btnCancel.Attributes.Add("onclick", "return Close();")
            btnSave.Attributes.Add("onclick", "return Save();")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub loadOpportunities(ByVal lngcaseid As Long)
        Try
            Dim objTimeExp As New TimeExpenseLeave
            objTimeExp.CaseID = lngcaseid
            ddlOpp.DataSource = objTimeExp.GetCaseOpp()
            ddlOpp.DataTextField = "vcPOppName"
            ddlOpp.DataValueField = "numOppId"
            ddlOpp.DataBind()
            ddlOpp.Items.Insert(0, "--Select One--")
            ddlOpp.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub getdetails()
        Try
            Dim objTimeExp As New TimeExpenseLeave
            Dim dtExpenseDetails As DataTable
            objTimeExp.UserCntID = Session("UserContactId")
            objTimeExp.DomainID = Session("DomainID")
            objTimeExp.CaseID = txtCaseId.Text
            objTimeExp.StageId = txtCommId.Text
            objTimeExp.TEType = 3
            objTimeExp.CategoryID = 2
            objTimeExp.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            objTimeExp.CategoryHDRID = CatHdrId
            dtExpenseDetails = objTimeExp.GetTimeAndExpDetails

            If dtExpenseDetails.Rows.Count <> 0 Then
                txtDivId.Text = dtExpenseDetails.Rows(0).Item("numDivisionID")
                txtCaseId.Text = dtExpenseDetails.Rows(0).Item("numCaseId")
                txtCommId.Text = dtExpenseDetails.Rows(0).Item("numStageId")
                If CatHdrId <> 0 Then
                    LoadContractsInfo(txtDivId.Text)
                    loadOpportunities(txtCaseId.Text)
                End If
                txtCategoryHDRID.Text = dtExpenseDetails.Rows(0).Item("numCategoryHDRID")
                If Not ddlOpp.Items.FindByValue(dtExpenseDetails.Rows(0).Item("numOppId")) Is Nothing Then
                    ddlOpp.SelectedItem.Selected = False
                    ddlOpp.Items.FindByValue(dtExpenseDetails.Rows(0).Item("numOppId")).Selected = True
                End If
                txtAmount.Text = String.Format("{0:#,##0.00}", dtExpenseDetails.Rows(0).Item("monAmount"))
                txtDesc.Text = IIf(IsDBNull(dtExpenseDetails.Rows(0).Item("txtDesc")), "", dtExpenseDetails.Rows(0).Item("txtDesc"))
                radBill.Checked = IIf(dtExpenseDetails.Rows(0).Item("numtype") = 1, True, False)
                If radBill.Checked = False Then
                    radNonBill.Checked = True
                End If
                chkReimb.Checked = dtExpenseDetails.Rows(0).Item("bitReimburse")
                If Not dtExpenseDetails.Rows(0).Item("numContractId") = 0 Then
                    ddlContract.Items.FindByValue(dtExpenseDetails.Rows(0).Item("numContractId")).Selected = True
                    BindContractinfo()
                End If
                CalCreated.SelectedDate = dtExpenseDetails.Rows(0).Item("dtFromDate")
                ddlCreatedTime.SelectedItem.Selected = False
                If Not ddlCreatedTime.Items.FindByText(Format(dtExpenseDetails.Rows(0).Item("dtFromDate"), "h:mm")) Is Nothing Then
                    ddlCreatedTime.Items.FindByText(Format(dtExpenseDetails.Rows(0).Item("dtFromDate"), "h:mm")).Selected = True
                End If
                If Format(dtExpenseDetails.Rows(0).Item("dtFromDate"), "tt") = "AM" Then
                    chkCreatedAm.Checked = True
                Else : chkCreatedAm.Checked = True
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub saveDetails()
        Try
            Dim strStartDate, strStartTime As String
            Dim strSplitStartTimeDate As Date
            strStartDate = CalCreated.SelectedDate
            strStartTime = strStartDate.Trim & " " & ddlCreatedTime.SelectedItem.Text.Trim & ":00" & IIf(chkCreatedAm.Checked = True, " AM", " PM")
            strSplitStartTimeDate = CType(strStartTime, DateTime).AddMinutes(Session("ClientMachineUTCTimeOffset"))

            Dim objTimeExp As New TimeExpenseLeave
            objTimeExp.CategoryHDRID = txtCategoryHDRID.Text
            objTimeExp.DivisionID = txtDivId.Text
            objTimeExp.CategoryID = 2
            objTimeExp.CategoryType = IIf(radBill.Checked, 1, 2)
            If radBill.Checked Then
                objTimeExp.ContractID = ddlContract.SelectedItem.Value
            Else : objTimeExp.ContractID = 0
            End If
            objTimeExp.Amount = CInt(txtAmount.Text)
            objTimeExp.CaseID = txtCaseId.Text
            objTimeExp.StageId = txtCommId.Text
            objTimeExp.OppID = ddlOpp.SelectedValue
            objTimeExp.Desc = txtDesc.Text.Trim
            objTimeExp.UserCntID = Session("UserContactId")
            objTimeExp.DomainID = Session("DomainID")
            objTimeExp.TEType = 3
            objTimeExp.FromDate = strSplitStartTimeDate
            objTimeExp.ToDate = strSplitStartTimeDate
            objTimeExp.bitReimburse = chkReimb.Checked
            objTimeExp.ManageTimeAndExpense()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadContractsInfo(ByVal intDivId As Integer)
        Try
            Dim objContract As New CContracts
            Dim dtTable As DataTable
            objContract.DivisionId = intDivId
            objContract.UserCntId = Session("UserContactId")
            objContract.DomainId = Session("DomainId")
            dtTable = objContract.GetContractDdlList()
            ddlContract.DataSource = dtTable

            ddlContract.DataTextField = "vcContractName"
            ddlContract.DataValueField = "numcontractId"
            ddlContract.DataBind()
            ddlContract.Items.Insert(0, "--Select One--")
            ddlContract.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindContractinfo()
        Try
            Dim objContract As New CContracts
            Dim dtTable As DataTable
            objContract.CategoryHDRID = txtCategoryHDRID.Text
            objContract.ContractID = ddlContract.SelectedValue
            objContract.DomainId = Session("DomainId")
            dtTable = objContract.GetContractHrsAmount()
            If dtTable.Rows.Count > 0 Then
                lblRemAmount.Text = IIf(dtTable.Rows(0).Item("bitamount"), String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("RemAmount")), "-")
                If txtAmount.Text = "" Then txtAmount.Text = dtTable.Rows(0).Item("decRate")
            Else : lblRemAmount.Text = ""
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If radBill.Checked = True And ddlContract.SelectedValue <> 0 Then
                Dim amount As Decimal
                amount = txtAmount.Text
                If lblRemAmount.Text <> "-" Then
                    If amount > lblRemAmount.Text Then
                        litMessage.Text = "Cannot Save as Contract Amount Not Sufficient"
                        Exit Sub
                    End If
                Else
                    litMessage.Text = "Cannot Save as Contract Amount Not Sufficient"
                    Exit Sub
                End If
            End If
            saveDetails()
            If GetQueryStringVal( "frm") = "TE" Then
                Response.Redirect("../TimeAndExpense/frmDaydetails.aspx?Date=" & GetQueryStringVal( "Date") & "&CntID=" & GetQueryStringVal( "CntID"))
            ElseIf GetQueryStringVal( "frm") = "TimeAndExpDets" Then
                Response.Redirect("../Accounting/frmTimeAndExpensesDetails.aspx?NameU=" & GetQueryStringVal( "CntID") & "&StartDate=" & GetQueryStringVal( "Date") & "&EndDate=" & GetQueryStringVal( "EndDate"))
            Else : Response.Write("<script>window.close();</script>")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btndelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelete.Click
        Try
            Dim objTimeAndExp As New TimeExpenseLeave
            objTimeAndExp.CategoryHDRID = txtCategoryHDRID.Text
            objTimeAndExp.DomainID = Session("DomainId")
            objTimeAndExp.DeleteTimeExpLeave()
            If GetQueryStringVal( "frm") = "TE" Then
                Response.Redirect("../TimeAndExpense/frmDaydetails.aspx?Date=" & GetQueryStringVal( "Date") & "&CntID=" & GetQueryStringVal( "CntID"))
            ElseIf GetQueryStringVal( "frm") = "TimeAndExpDets" Then
                Response.Redirect("../Accounting/frmTimeAndExpensesDetails.aspx?NameU=" & GetQueryStringVal( "CntID") & "&StartDate=" & GetQueryStringVal( "Date") & "&EndDate=" & GetQueryStringVal( "EndDate"))
            Else : Response.Write("<script>window.close();</script>")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlContract_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlContract.SelectedIndexChanged
        Try
            BindContractinfo()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            If GetQueryStringVal( "frm") = "TE" Then
                Response.Redirect("../TimeAndExpense/frmDaydetails.aspx?Date=" & GetQueryStringVal( "Date") & "&CntID=" & GetQueryStringVal( "CntID"))
            ElseIf GetQueryStringVal( "frm") = "TimeAndExpDets" Then
                Response.Redirect("../Accounting/frmTimeAndExpensesDetails.aspx?NameU=" & GetQueryStringVal( "CntID") & "&StartDate=" & GetQueryStringVal( "Date") & "&EndDate=" & GetQueryStringVal( "EndDate"))
            Else : Response.Write("<script>window.close();</script>")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class
