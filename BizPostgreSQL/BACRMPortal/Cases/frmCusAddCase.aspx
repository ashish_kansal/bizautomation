<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCusAddCase.aspx.vb"
    Inherits="BACRMPortal.frmCusAddCase" %>

<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <title>Case</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script language="javascript">
        function Save() {
            if ((document.Form1.ddlContacts.selectedIndex == -1) || (document.Form1.ddlContacts.value == 0)) {
                alert("Select Contact")
                document.Form1.ddlContacts.focus();
                return false;
            }
            if (document.Form1.txtSubject.value == "") {
                alert("Enter Subject")
                document.Form1.txtSubject.focus();
                return false;
            }
            if (document.Form1.calResolve_txtDate.value == "") {
                alert("Enter Resolve Date")
                document.Form1.calResolve_txtDate.focus();
                return false;

            }
            if (document.Form1.txtDescription.value == "") {
                alert("Enter Description")
                document.Form1.txtDescription.focus();
                return false;
            }
        }
        function Close() {
            window.close()
            return false;
        }
        //This is used to activate the Grid Row whenever we click on a radio button
        function activateGridRow(theIndex) {
            igtbl_getGridById('uwOrders').Rows.getRow(theIndex).activate();
        }
    </script>
</head>
<body>
    <form id="Form1" name="Form1" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server">
    </asp:ScriptManager>
    <br />
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td valign="bottom">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="TabLeft">
                            New Case
                        </td>
                        <td class="TabRight">
                        </td>
                    </tr>
                </table>
            </td>
            <td align="right">
                <asp:Button ID="btnSaveClose" runat="server" Text="Save &amp; Close" CssClass="button">
                </asp:Button>
                <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" Width="50">
                </asp:Button>
            </td>
        </tr>
    </table>
    <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="100%" BorderColor="black" GridLines="None" CssClass="aspTable">
        <asp:TableRow>
            <asp:TableCell>
                <br>
                <table border="0" width="100%">
                    <tr>
                        <td valign="top" rowspan="30">
                            <img src="../images/SuitCase-32.gif" />
                        </td>
                        <td class="normal1" align="right" width="100px">
                            Contact<font color="red">*</font>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlContacts" runat="server" Width="130" CssClass="signup" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Subject<font color="red">*</font>
                        </td>
                        <td>
                            <asp:TextBox ID="txtSubject" CssClass="signup" runat="server" Width="200"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Resolve Date<font color="red">*</font>
                        </td>
                        <td>
                            <BizCalendar:Calendar ID="calResolve" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Description<font color="red">*</font>
                        </td>
                        <td>
                            <asp:TextBox ID="txtDescription" CssClass="signup" runat="server" Width="500" TextMode="MultiLine"
                                Height="50"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Most recent orders&nbsp;
                        </td>
                        <td>
                            <telerik:radgrid id="gvOrders" runat="server" width="100%" autogeneratecolumns="False"
                                gridlines="None" showfooter="false" skin="windows" enableembeddedskins="false"
                                cssclass="dg" alternatingitemstyle-cssclass="ais" itemstyle-cssclass="is" headerstyle-cssclass="hs">
                                <MasterTableView DataKeyNames="numOppId" HierarchyLoadMode="Client" DataMember="Orders">
                                    <DetailTables>
                                        <telerik:GridTableView Width="100%" runat="server" ItemStyle-HorizontalAlign="Center"
                                            AlternatingItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            ShowFooter="true" DataKeyNames="numOppId,numoppitemtCode" DataMember="OrderItems"
                                            CssClass="dg" AlternatingItemStyle-CssClass="ais" ItemStyle-CssClass="is" HeaderStyle-CssClass="hs">
                                            <ParentTableRelation>
                                                <telerik:GridRelationFields DetailKeyField="numOppId" MasterKeyField="numOppId" />
                                            </ParentTableRelation>
                                            <Columns>
                                                <telerik:GridTemplateColumn>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridBoundColumn HeaderText="numoppitemtCode" Visible="false" DataField="numoppitemtCode">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="numOppId" Visible="false" DataField="numOppId">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="numItemCode" Visible="false" DataField="numItemCode">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridTemplateColumn HeaderText="Image">
                                                    <ItemTemplate>
                                                        <%#CCommon.GetImageHTML(Eval("vcPathForTImage"), 1)%>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridBoundColumn HeaderText="Item Name" ItemStyle-Width="10%" DataField="vcItemName">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="Model #" ItemStyle-Width="10%" DataField="vcModelId">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="Description" ItemStyle-Width="10%" DataField="vcItemDesc">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="Units" ItemStyle-Width="10%" DataField="numUnitHour">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="Total Amount" ItemStyle-Width="10%" DataField="monTotAmount"
                                                    DataFormatString="{0:0.00}">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="Serial No" ItemStyle-Width="10%" DataField="vcSerialNo">
                                                </telerik:GridBoundColumn>
                                            </Columns>
                                        </telerik:GridTableView>
                                    </DetailTables>
                                    <Columns>
                                        <telerik:GridBoundColumn HeaderText="Date Created" DataField="bintCreatedDate">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Order ID" DataField="vcPOppName">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Order Amount" DataField="monDealAmount" DataFormatString="{0:0.00}">
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings AllowExpandCollapse="true" />
                            </telerik:radgrid>
                            <%-- <igtbl:UltraWebGrid ID="uwOrders" Width="100%" DisplayLayout-AllowRowNumberingDefault="ByDataIsland"
                                runat="server" Browser="Xml" Height="100%">
                                <DisplayLayout AutoGenerateColumns="false" RowHeightDefault="20px" AllowAddNewDefault="Yes"
                                    Version="3.00" SelectTypeRowDefault="Single" ViewType="Hierarchical" TableLayout="Auto"
                                    SelectTypeCellDefault="Extended" BorderCollapseDefault="Separate" AllowColSizingDefault="Free"
                                    Name="uwItem" EnableClientSideRenumbering="true" SelectTypeColDefault="Extended"
                                    AllowUpdateDefault="Yes">
                                    <HeaderStyleDefault CssClass="hs" VerticalAlign="Middle" Font-Size="8pt" Font-Bold="true"
                                        Font-Names="Arial" BorderStyle="Solid" HorizontalAlign="Left" ForeColor="white"
                                        BackColor="#52658C" Height="20px">
                                        <Padding Left="2px" Right="2px"></Padding>
                                        <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft=""></BorderDetails>
                                    </HeaderStyleDefault>
                                    <RowSelectorStyleDefault Height="20px" CssClass="is">
                                    </RowSelectorStyleDefault>
                                    <RowAlternateStyleDefault Height="20px" BackColor="#C6D3E7">
                                    </RowAlternateStyleDefault>
                                    <FrameStyle Width="100%" Cursor="Default" BorderWidth="0px" Font-Size="8pt" Font-Names="Arial"
                                        BorderStyle="Double">
                                    </FrameStyle>
                                    <FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
                                        <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
                                        </BorderDetails>
                                    </FooterStyleDefault>
                                    <EditCellStyleDefault BorderWidth="0px" BorderStyle="None">
                                    </EditCellStyleDefault>
                                    <SelectedRowStyleDefault ForeColor="White" BackColor="#666666">
                                    </SelectedRowStyleDefault>
                                    <RowStyleDefault BorderWidth="1px" Font-Size="8pt" Font-Names="Arial" BorderColor="Gray"
                                        BorderStyle="Solid" BackColor="White">
                                        <Padding Left="5px" Right="5px"></Padding>
                                        <BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
                                    </RowStyleDefault>
                                    <RowExpAreaStyleDefault BackColor="LightSteelBlue">
                                    </RowExpAreaStyleDefault>
                                </DisplayLayout>
                                <Bands>
                                    <igtbl:UltraGridBand AllowDelete="No" AllowAdd="Yes">
                                        <Columns>
                                      
                                            <igtbl:UltraGridColumn HeaderText="numOppId" BaseColumnName="numOppId" AllowUpdate="No"
                                                IsBound="false" Hidden="true" Key="numOppId">
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn HeaderText="Date Created" AllowUpdate="No" IsBound="false"
                                                BaseColumnName="bintCreatedDate" Key="bintCreatedDate">
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn HeaderText="Order ID" AllowUpdate="No" IsBound="false" BaseColumnName="vcPOppName"
                                                Key="vcPOppName">
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn HeaderText="Order Amount" AllowUpdate="No" IsBound="false"
                                                BaseColumnName="monDealAmount" Key="monDealAmount" Format="0.00">
                                            </igtbl:UltraGridColumn>
                                        </Columns>
                                    </igtbl:UltraGridBand>
                                    <igtbl:UltraGridBand AllowDelete="No" AllowAdd="Yes">
                                        <Columns>
                                            <igtbl:UltraGridColumn HeaderText="" BaseColumnName="" AllowUpdate="Yes" Type="CheckBox"
                                                IsBound="false" Hidden="false" Key="" Width="20">
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Hidden="true" IsBound="false" BaseColumnName="numoppitemtCode"
                                                Key="numoppitemtCode">
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn HeaderText="numOppId" BaseColumnName="numOppId" AllowUpdate="No"
                                                IsBound="false" Hidden="true" Key="numOppId">
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Hidden="true" IsBound="false" BaseColumnName="numItemCode"
                                                Key="numItemCode">
                                            </igtbl:UltraGridColumn>
                                            <igtbl:TemplatedColumn HeaderText="Image">
                                                <CellTemplate>
                                                    <%#CCommon.GetImageHTML(DataBinder.Eval(Container.DataItem,"vcPathForTImage"), 1)%>
                                                </CellTemplate>
                                            </igtbl:TemplatedColumn>
                                            <igtbl:UltraGridColumn HeaderText="Item Name" AllowUpdate="No" IsBound="false" BaseColumnName="vcItemName"
                                                Key="vcItemName">
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn HeaderText="Model #" AllowUpdate="No" IsBound="false" BaseColumnName="vcModelId"
                                                Key="vcModelId">
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn HeaderText="Description" AllowUpdate="No" IsBound="false"
                                                BaseColumnName="vcItemDesc" Key="vcItemDesc">
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn HeaderText="Units" AllowUpdate="No" IsBound="false" BaseColumnName="numUnitHour"
                                                Key="numUnitHour">
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn HeaderText="Total Amount" AllowUpdate="No" IsBound="false"
                                                BaseColumnName="monTotAmount" Key="monTotAmount" Format="0.00">
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn HeaderText="Serial No" AllowUpdate="No" IsBound="false" BaseColumnName="vcSerialNo"
                                                Key="vcSerialNo">
                                            </igtbl:UltraGridColumn>
                                        </Columns>
                                    </igtbl:UltraGridBand>
                                </Bands>
                            </igtbl:UltraWebGrid>--%>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    </form>
</body>
</html>
