Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Opportunities
Imports Infragistics.WebUI.UltraWebTab
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Contract
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents
Imports System.Reflection

Partial Public Class frmCusCaseDTL : Inherits BACRMPage

    '    Dim objCommon As New CCommon
    '    Dim objContacts As CContacts
    '    Dim objCases As CCases
    '    Dim objSolution As Solution
    '    Dim objCus As CustomFields
    '    Dim objContracts As CContracts
    '    Dim SI As Integer = 0
    '    Dim SI1 As Integer = 0
    '    Dim SI2 As Integer = 0
    '    Dim frm As String = ""
    '    Dim frm1 As String = ""
    '    Dim frm2 As String = ""
    '    Dim lngCaseId As Long
    '    Dim lngCntId As Long
    '    Dim strColumn As String

    '#Region " Web Form Designer Generated Code "

    '    'This call is required by the Web Form Designer.
    '    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    '    End Sub
    '    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    '    'Do not delete or move it.
    '    Private designerPlaceholderDeclaration As System.Object

    '    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
    '        'CODEGEN: This method call is required by the Web Form Designer
    '        'Do not modify it using the code editor.
    '        InitializeComponent()
    '    End Sub

    '#End Region

    '    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
    '        Try
    '            If Not GetQueryStringVal( "SI") Is Nothing Then
    '                SI = GetQueryStringVal( "SI")
    '            End If
    '            If Not GetQueryStringVal( "SI1") Is Nothing Then
    '                SI1 = GetQueryStringVal( "SI1")
    '            Else : SI1 = 0
    '            End If
    '            If Not GetQueryStringVal( "SI2") Is Nothing Then
    '                SI2 = GetQueryStringVal( "SI2")
    '            Else : SI2 = 0
    '            End If
    '            If Not GetQueryStringVal( "frm") Is Nothing Then
    '                frm = ""
    '                frm = GetQueryStringVal( "frm")
    '            Else : frm = ""
    '            End If
    '            If Not GetQueryStringVal( "frm1") Is Nothing Then
    '                frm1 = ""
    '                frm1 = GetQueryStringVal( "frm1")
    '            Else : frm1 = ""
    '            End If
    '            If Not GetQueryStringVal( "SI2") Is Nothing Then
    '                frm2 = ""
    '                frm2 = GetQueryStringVal( "frm2")
    '            Else : frm2 = ""
    '            End If
    '            lngCaseId = Session("CaseID")
    '            If Not IsPostBack Then
    '                If uwOppTab.Tabs.Count > SI Then uwOppTab.SelectedTabIndex = SI
    '                If objContacts Is Nothing Then objContacts = New CContacts

    '                objContacts.RecID = lngCaseId
    '                objContacts.Type = "S"
    '                objContacts.UserCntID = Session("UserContactID")
    '                objContacts.AddVisiteddetails()
    '                Session("show") = 1
    '                Session("Help") = "Support"
    '                
    '                m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmCases.aspx", Session("UserContactID"), 15, 8)
    '                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
    '                    Response.Redirect("../admin/authentication.aspx?mesg=AC")
    '                Else
    '                    If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
    '                        btnSave.Visible = False
    '                        btnSaveClose.Visible = False
    '                    End If
    '                End If
    '                FillContact()
    '                LoadDropdowns()
    '                LoadInformation()
    '            End If
    '            DisplayDynamicFlds()
    '            frmComments1.CaseID = lngCaseId

    '            btnAddContact.Attributes.Add("onclick", "return AddContact()")
    '        Catch ex As Exception
    '            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '            Response.Write(ex)
    '        End Try
    '    End Sub

    '    Sub LoadInformation()
    '        Try
    '            If objCases Is Nothing Then objCases = New CCases
    '            Dim dtCaseDetails As DataTable
    '            objCases.CaseID = lngCaseId
    '            objCases.DomainID = Session("DomainID")
    '            objCases.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
    '            dtCaseDetails = objCases.GetCaseDTL
    '            If dtCaseDetails.Rows.Count > 0 Then
    '                hplCustomer.Text = dtCaseDetails.Rows(0).Item("vcCompanyName")
    '                txtCntId.Text = dtCaseDetails.Rows(0).Item("numContactID")
    '                lngCntId = dtCaseDetails.Rows(0).Item("numContactID")
    '                txtdivId.Text = dtCaseDetails.Rows(0).Item("numDivisionID")

    '                If Session("EnableIntMedPage") = 1 Then
    '                    hplCustomer.NavigateUrl = "../pagelayout/frmCustAccountdtl.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=CaseDetails&DivID=" & Session("DivID") & "&frm1=" & GetQueryStringVal( "frm")
    '                Else : hplCustomer.NavigateUrl = "../account/frmCusAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=CaseDetails&DivID=" & Session("DivID") & "&frm1=" & GetQueryStringVal( "frm")
    '                End If
    '                ViewState("ContractID") = dtCaseDetails.Rows(0).Item("numcontractId")
    '                'If dtCaseDetails.Rows(0).Item("tintSupportKeyType") = 0 Then
    '                '    If Not IsDBNull(dtCaseDetails.Rows(0).Item("numUniversalSupportKey")) Then
    '                '        lblsupportkey.Text = "U" & Format(dtCaseDetails.Rows(0).Item("numUniversalSupportKey"), "00000000000")
    '                '    End If
    '                'End If
    '                lblcasenumber.Text = dtCaseDetails.Rows(0).Item("vcCaseNumber")
    '                ViewState("AssignedTo") = IIf(IsDBNull(dtCaseDetails.Rows(0).Item("numAssignedTo")), 0, dtCaseDetails.Rows(0).Item("numAssignedTo"))
    '                If Not IsDBNull(dtCaseDetails.Rows(0).Item("numAssignedTo")) Then
    '                    If Not ddlAssignedTo.Items.FindByValue(dtCaseDetails.Rows(0).Item("numAssignedTo")) Is Nothing Then
    '                        ddlAssignedTo.Items.FindByValue(dtCaseDetails.Rows(0).Item("numAssignedTo")).Selected = True
    '                    End If
    '                End If
    '                hLinkContact.Text = dtCaseDetails.Rows(0).Item("vcName")
    '                hLinkEmailID.Text = dtCaseDetails.Rows(0).Item("vcEmail")
    '                Label8.Text = dtCaseDetails.Rows(0).Item("Phone")
    '                txtSubject.Text = IIf(IsDBNull(dtCaseDetails.Rows(0).Item("textSubject")), "", dtCaseDetails.Rows(0).Item("textSubject"))
    '                If Not IsDBNull(dtCaseDetails.Rows(0).Item("intTargetResolveDate")) Then
    '                    calResolve.SelectedDate = dtCaseDetails.Rows(0).Item("intTargetResolveDate")
    '                End If
    '                If Not IsDBNull(dtCaseDetails.Rows(0).Item("numStatus")) Then
    '                    If Not ddlStatus.Items.FindByValue(dtCaseDetails.Rows(0).Item("numStatus")) Is Nothing Then
    '                        ddlStatus.ClearSelection()
    '                        ddlStatus.Items.FindByValue(dtCaseDetails.Rows(0).Item("numStatus")).Selected = True
    '                    End If
    '                End If
    '                If Not IsDBNull(dtCaseDetails.Rows(0).Item("numPriority")) Then
    '                    If Not ddlPriority.Items.FindByValue(dtCaseDetails.Rows(0).Item("numPriority")) Is Nothing Then
    '                        ddlPriority.ClearSelection()
    '                        ddlPriority.Items.FindByValue(dtCaseDetails.Rows(0).Item("numPriority")).Selected = True
    '                    End If
    '                End If
    '                If Not IsDBNull(dtCaseDetails.Rows(0).Item("numType")) Then
    '                    If Not ddlType.Items.FindByValue(dtCaseDetails.Rows(0).Item("numType")) Is Nothing Then
    '                        ddlType.ClearSelection()
    '                        ddlType.Items.FindByValue(dtCaseDetails.Rows(0).Item("numType")).Selected = True
    '                    End If
    '                End If
    '                If Not IsDBNull(dtCaseDetails.Rows(0).Item("numOrigin")) Then
    '                    If Not ddlOrgin.Items.FindByValue(dtCaseDetails.Rows(0).Item("numOrigin")) Is Nothing Then
    '                        ddlOrgin.ClearSelection()
    '                        ddlOrgin.Items.FindByValue(dtCaseDetails.Rows(0).Item("numOrigin")).Selected = True
    '                    End If
    '                End If
    '                If Not IsDBNull(dtCaseDetails.Rows(0).Item("numOrigin")) Then
    '                    If Not ddlCommonReasons.Items.FindByValue(dtCaseDetails.Rows(0).Item("numReason")) Is Nothing Then
    '                        ddlCommonReasons.ClearSelection()
    '                        ddlCommonReasons.Items.FindByValue(dtCaseDetails.Rows(0).Item("numReason")).Selected = True
    '                    End If
    '                End If
    '                txtdescription.Text = IIf(IsDBNull(dtCaseDetails.Rows(0).Item("textDesc")), "", dtCaseDetails.Rows(0).Item("textDesc"))
    '                txtIntComments.Text = IIf(IsDBNull(dtCaseDetails.Rows(0).Item("textInternalComments")), "", dtCaseDetails.Rows(0).Item("textInternalComments"))

    '                lblCreatedBy.Text = dtCaseDetails.Rows(0).Item("CreatedBy")
    '                lblRecordOwner.Text = dtCaseDetails.Rows(0).Item("RecOwner")
    '                lblLastModifiedBy.Text = dtCaseDetails.Rows(0).Item("ModifiedBy")

    '                Dim dtOpportunity As DataTable
    '                Dim dtSelOpportunity As DataTable

    '                objCases.DomainID = Session("DomainId")
    '                objCases.DivisionID = dtCaseDetails.Rows(0).Item("numDivisionID")
    '                objCases.byteMode = 1
    '                objCases.CaseID = lngCaseId
    '                dtOpportunity = objCases.GetOpportunities

    '                lstOpportunity.DataSource = dtOpportunity
    '                lstOpportunity.DataTextField = "vcPOppName"
    '                lstOpportunity.DataValueField = "numOppId"
    '                lstOpportunity.DataBind()
    '                dtSelOpportunity = objCases.GetCaseOpportunities

    '                Dim i As Integer = 0
    '                For i = 0 To dtSelOpportunity.Rows.Count - 1
    '                    If Not lstOpportunity.Items.FindByValue(dtSelOpportunity.Rows(i).Item("numOppid")) Is Nothing Then
    '                        lstOpportunity.Items.FindByValue(dtSelOpportunity.Rows(i).Item("numOppid")).Selected = True
    '                    End If
    '                Next
    '            End If

    '            dgContact.DataSource = objCases.AssCntsByCaseID
    '            dgContact.DataBind()

    '            If objSolution Is Nothing Then objSolution = New Solution
    '            Dim dtSolution As DataTable
    '            objSolution.CaseID = lngCaseId
    '            dtSolution = objSolution.GetSolutionForCases
    '            If dtSolution.Rows.Count > 0 Then
    '                pnlKnowledgeBase.Visible = True
    '                dgSolution.DataSource = dtSolution
    '                dgSolution.DataBind()
    '            Else : pnlKnowledgeBase.Visible = False
    '            End If
    '        Catch ex As Exception
    '            Throw ex
    '        End Try
    '    End Sub

    '    Sub LoadDropdowns()
    '        Try
    '            objCommon.sb_FillComboFromDBwithSel(ddlContactrole, 26, Session("DomainID"))
    '            objCommon.sb_FillComboFromDBwithSel(ddlStatus, 14, Session("DomainID"))
    '            objCommon.sb_FillComboFromDBwithSel(ddlOrgin, 15, Session("DomainID"))
    '            objCommon.sb_FillComboFromDBwithSel(ddlPriority, 13, Session("DomainID"))
    '            objCommon.sb_FillComboFromDBwithSel(ddlType, 16, Session("DomainID"))
    '            objCommon.sb_FillComboFromDBwithSel(ddlCommonReasons, 17, Session("DomainID"))
    '            If Session("PopulateUserCriteria") = 1 Then
    '                objCommon.CaseID = lngCaseId
    '                objCommon.charModule = "S"
    '                objCommon.GetCompanySpecificValues1()
    '                objCommon.sb_FillConEmpFromTerritories(ddlAssignedTo, Session("DomainID"), 0, 0, objCommon.TerittoryID)
    '            ElseIf Session("PopulateUserCriteria") = 2 Then
    '                objCommon.sb_FillConEmpFromDBUTeam(ddlAssignedTo, Session("DomainID"), Session("UserContactID"))
    '            Else : objCommon.sb_FillConEmpFromDBSel(ddlAssignedTo, Session("DomainID"), 0, 0)
    '            End If
    '        Catch ex As Exception
    '            Throw ex
    '        End Try
    '    End Sub

    '    Function save() As Integer
    '        Try
    '            If objCases Is Nothing Then objCases = New CCases
    '            With objCases
    '                .Subject = txtSubject.Text
    '                .Desc = txtdescription.Text
    '                If calResolve.SelectedDate <> "" Then .TargetResolveDate = calResolve.SelectedDate
    '                .DivisionID = txtdivId.Text
    '                .ContactID = txtCntId.Text
    '                .Status = ddlStatus.SelectedItem.Value
    '                .Orgin = ddlOrgin.SelectedItem.Value
    '                .Priority = ddlPriority.SelectedItem.Value
    '                .CaseType = ddlType.SelectedItem.Value
    '                .Reason = ddlCommonReasons.SelectedItem.Value
    '                .InternalComments = txtIntComments.Text
    '                .UserCntId = Session("UserContactID")
    '                .CaseID = lngCaseId
    '                If ddlAssignedTo.SelectedItem.Value = 0 Then
    '                    .AssignTo = Session("RecOwner")
    '                Else : .AssignTo = ddlAssignedTo.SelectedItem.Value
    '                End If
    '                .ContractID = ViewState("ContractID")
    '            End With
    '            If objCases.SaveCases() = 0 Then
    '                Return 1
    '                Exit Function
    '            End If
    '            Dim i As Integer = 0
    '            Dim strOppSelected As String = ""
    '            For i = 0 To lstOpportunity.Items.Count - 1
    '                If lstOpportunity.Items(i).Selected = True Then
    '                    strOppSelected = strOppSelected & lstOpportunity.Items(i).Value.ToString & ","
    '                End If
    '            Next
    '            objCases.CaseID = lngCaseId
    '            objCases.DomainID = Session("DomainID")
    '            objCases.strOppSel = strOppSelected
    '            objCases.SaveCaseOpportunities()
    '            If ddlAssignedTo.SelectedItem.Value > 0 And ViewState("AssignedTo") <> ddlAssignedTo.SelectedItem.Value Then
    '                Try
    '                    Dim objAlerts As New CAlerts
    '                    Dim dtCaseAlert As DataTable
    '                    objAlerts.AlertDTLID = 11 'Alert DTL ID for sending alerts in opportunities
    '                    objAlerts.DomainID = Session("DomainID")
    '                    dtCaseAlert = objAlerts.GetIndAlertDTL
    '                    If dtCaseAlert.Rows.Count > 0 Then
    '                        If dtCaseAlert.Rows(0).Item("tintAlertOn") = 1 Then
    '                            Dim dtEmailTemplate As DataTable
    '                            Dim objDocuments As New DocumentList
    '                            objDocuments.GenDocID = dtCaseAlert.Rows(0).Item("numEmailTemplate")
    '                            objDocuments.DomainID = Session("DomainID")
    '                            dtEmailTemplate = objDocuments.GetDocByGenDocID
    '                            If dtEmailTemplate.Rows.Count > 0 Then
    '                                Dim objSendMail As New Email
    '                                Dim dtMergeFields As New DataTable
    '                                Dim drNew As DataRow
    '                                dtMergeFields.Columns.Add("Assignee")
    '                                dtMergeFields.Columns.Add("CaseID")
    '                                dtMergeFields.Columns.Add("DueDate")
    '                                drNew = dtMergeFields.NewRow
    '                                drNew("Assignee") = ddlAssignedTo.SelectedItem.Text
    '                                drNew("CaseID") = lblcasenumber.Text
    '                                drNew("DueDate") = calResolve.SelectedDate
    '                                dtMergeFields.Rows.Add(drNew)
    '                                objCommon.ContactID = ddlAssignedTo.SelectedItem.Value
    '                                objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), "", Session("UserEmail"), objCommon.GetContactsEmail(), dtMergeFields)
    '                            End If
    '                        End If
    '                    End If
    '                Catch ex As Exception

    '                End Try
    '            End If
    '            Return 0
    '        Catch ex As Exception
    '            Throw ex
    '        End Try
    '    End Function

    '    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
    '        Try
    '            If save() = 1 Then
    '                litMessage.Text = " No of Incidents Not Sufficient"
    '                Exit Sub
    '            End If
    '            SaveCusField()
    '        Catch ex As Exception
    '            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '            Response.Write(ex)
    '        End Try
    '    End Sub

    '    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
    '        Try
    '            If save() = 1 Then
    '                litMessage.Text = " No of Incidents Not Sufficient"
    '                Exit Sub
    '            End If
    '            SaveCusField()
    '            PageRedirect()
    '        Catch ex As Exception
    '            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '            Response.Write(ex)
    '        End Try
    '    End Sub

    '    Sub DisplayDynamicFlds()
    '        Try
    '            Dim strDate As String
    '            Dim bizCalendar As UserControl
    '            Dim _myUC_DueDate As PropertyInfo
    '            Dim PreviousRowID As Integer = 0
    '            Dim objRow As HtmlTableRow
    '            Dim objCell As HtmlTableCell
    '            Dim i, k As Integer
    '            Dim dtTable As DataTable
    '            ' Tabstrip3.Items.Clear()
    '            objCus = New CustomFields
    '            objCus.locId = 3
    '            objCus.RecordId = lngCaseId
    '            objCus.DomainID = Session("DomainID")
    '            dtTable = objCus.GetCustFlds.Tables(0)
    '            Session("CusFields") = dtTable

    '            If uwOppTab.Tabs.Count > 4 Then
    '                Dim iItemcount As Integer
    '                iItemcount = uwOppTab.Tabs.Count
    '                While uwOppTab.Tabs.Count > 4
    '                    uwOppTab.Tabs.RemoveAt(iItemcount - 1)
    '                    iItemcount = iItemcount - 1
    '                End While
    '            End If

    '            If dtTable.Rows.Count > 0 Then

    '                'Main Detail Section
    '                k = 0
    '                objRow = New HtmlTableRow
    '                For i = 0 To dtTable.Rows.Count - 1
    '                    If dtTable.Rows(i).Item("TabId") = 0 Then
    '                        If k = 3 Then
    '                            k = 0
    '                            tblDetails.Rows.Add(objRow)
    '                            objRow = New HtmlTableRow
    '                        End If

    '                        objCell = New HtmlTableCell
    '                        objCell.Align = "Right"
    '                        objCell.Attributes.Add("class", "normal1")
    '                        If dtTable.Rows(i).Item("fld_type") <> "Link" Then
    '                            objCell.InnerText = dtTable.Rows(i).Item("fld_label")
    '                        Else : objCell.InnerText = ""
    '                        End If
    '                        objRow.Cells.Add(objCell)
    '                        If dtTable.Rows(i).Item("fld_type") = "Text Box" Then
    '                            objCell = New HtmlTableCell
    '                            CreateTexBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))
    '                        ElseIf dtTable.Rows(i).Item("fld_type") = "Drop Down List Box" Then
    '                            objCell = New HtmlTableCell
    '                            CreateDropdown(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CInt(dtTable.Rows(i).Item("Value")), dtTable.Rows(i).Item("numlistid"))
    '                        ElseIf dtTable.Rows(i).Item("fld_type") = "Check box" Then
    '                            objCell = New HtmlTableCell
    '                            CreateChkBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CInt(dtTable.Rows(i).Item("Value")))
    '                        ElseIf dtTable.Rows(i).Item("fld_type") = "Text Area" Then
    '                            objCell = New HtmlTableCell
    '                            CreateTextArea(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))
    '                        ElseIf dtTable.Rows(i).Item("fld_type") = "Date Field" Then
    '                            PreviousRowID = i
    '                            objCell = New HtmlTableCell
    '                            bizCalendar = LoadControl("../include/calandar.ascx")
    '                            bizCalendar.ID = "cal" & dtTable.Rows(i).Item("fld_id")
    '                            objCell.Controls.Add(bizCalendar)
    '                            objRow.Cells.Add(objCell)
    '                        ElseIf dtTable.Rows(i).Item("fld_type") = "Link" Then
    '                            objCell = New HtmlTableCell
    '                            CreateLink(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("vcURL"), lngCaseId, dtTable.Rows(i).Item("fld_label"))
    '                        End If
    '                        k = k + 1
    '                    End If
    '                Next
    '                tblDetails.Rows.Add(objRow)

    '                'CustomField Section
    '                Dim Tab As Tab
    '                'Dim pageView As PageView
    '                Dim aspTable As HtmlTable
    '                Dim Table As Table
    '                Dim tblcell As TableCell
    '                Dim tblRow As TableRow
    '                k = 0
    '                ViewState("TabId") = dtTable.Rows(0).Item("TabId")
    '                ViewState("Check") = 0
    '                ViewState("FirstTabCreated") = 0
    '                ' Tabstrip3.Items.Clear()
    '                For i = 0 To dtTable.Rows.Count - 1
    '                    If dtTable.Rows(i).Item("TabId") <> 0 Then
    '                        If ViewState("TabId") <> dtTable.Rows(i).Item("TabId") Or ViewState("FirstTabCreated") = 0 Then
    '                            If ViewState("Check") <> 0 Then
    '                                aspTable.Rows.Add(objRow)
    '                                tblcell.Controls.Add(aspTable)
    '                                tblRow.Cells.Add(tblcell)
    '                                Table.Rows.Add(tblRow)
    '                                Tab.ContentPane.Controls.Add(Table)
    '                            End If
    '                            k = 0
    '                            ViewState("FirstTabCreated") = 1
    '                            ViewState("Check") = 1
    '                            '   If Not IsPostBack Then
    '                            ViewState("TabId") = dtTable.Rows(i).Item("TabId")
    '                            Tab = New Tab
    '                            Tab.Text = "&nbsp;&nbsp;" & dtTable.Rows(i).Item("tabname") & "&nbsp;&nbsp;"
    '                            uwOppTab.Tabs.Add(Tab)
    '                            'End If
    '                            'pageView = New PageView
    '                            aspTable = New HtmlTable
    '                            Table = New Table
    '                            Table.Width = Unit.Percentage(100)
    '                            Table.BorderColor = System.Drawing.Color.FromName("black")
    '                            Table.GridLines = GridLines.None
    '                            Table.BorderWidth = Unit.Pixel(1)
    '                            Table.Height = Unit.Pixel(300)
    '                            Table.CssClass = "aspTable"
    '                            tblcell = New TableCell
    '                            tblRow = New TableRow
    '                            tblcell.VerticalAlign = VerticalAlign.Top
    '                            aspTable.Width = "100%"
    '                            objRow = New HtmlTableRow
    '                            objCell = New HtmlTableCell
    '                            objCell.InnerHtml = "<br>"
    '                            objRow.Cells.Add(objCell)
    '                            aspTable.Rows.Add(objRow)
    '                            objRow = New HtmlTableRow
    '                        End If


    '                        'pageView.Controls.Add("")
    '                        If k = 3 Then
    '                            k = 0
    '                            aspTable.Rows.Add(objRow)
    '                            objRow = New HtmlTableRow
    '                        End If
    '                        objCell = New HtmlTableCell
    '                        objCell.Align = "right"
    '                        objCell.Attributes.Add("class", "normal1")
    '                        If dtTable.Rows(i).Item("fld_type") <> "Link" Then
    '                            objCell.InnerText = dtTable.Rows(i).Item("fld_label")
    '                        End If
    '                        objRow.Cells.Add(objCell)
    '                        If dtTable.Rows(i).Item("fld_type") = "Text Box" Then
    '                            objCell = New HtmlTableCell
    '                            CreateTexBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))
    '                        ElseIf dtTable.Rows(i).Item("fld_type") = "Drop Down List Box" Then
    '                            objCell = New HtmlTableCell
    '                            CreateDropdown(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CInt(dtTable.Rows(i).Item("Value")), dtTable.Rows(i).Item("numlistid"))
    '                        ElseIf dtTable.Rows(i).Item("fld_type") = "Check box" Then
    '                            objCell = New HtmlTableCell
    '                            CreateChkBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CInt(dtTable.Rows(i).Item("Value")))
    '                        ElseIf dtTable.Rows(i).Item("fld_type") = "Text Area" Then
    '                            objCell = New HtmlTableCell
    '                            CreateTextArea(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))
    '                        ElseIf dtTable.Rows(i).Item("fld_type") = "Date Field" Then
    '                            objCell = New HtmlTableCell
    '                            bizCalendar = LoadControl("../include/calandar.ascx")
    '                            bizCalendar.ID = "cal" & dtTable.Rows(i).Item("fld_id")
    '                            objCell.Controls.Add(bizCalendar)
    '                            objRow.Cells.Add(objCell)
    '                        ElseIf dtTable.Rows(i).Item("fld_type") = "Link" Then
    '                            objCell = New HtmlTableCell
    '                            CreateLink(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("vcURL"), lngCaseId, dtTable.Rows(i).Item("fld_label"))
    '                        ElseIf dtTable.Rows(i).Item("fld_type") = "Frame" Then
    '                            objCell = New HtmlTableCell
    '                            Dim strFrame As String
    '                            Dim URL As String
    '                            URL = dtTable.Rows(i).Item("vcURL")
    '                            URL = URL.Replace("RecordID", lngCaseId)
    '                            strFrame = "<iframe src ='" & URL & "' width='100%' frameborder='0' height= '800px'></iframe>"
    '                            objCell.Controls.Add(New LiteralControl(strFrame))
    '                            objRow.Cells.Add(objCell)
    '                        End If
    '                        k = k + 1
    '                    End If

    '                Next
    '                If ViewState("Check") = 1 Then
    '                    aspTable.Rows.Add(objRow)
    '                    tblcell.Controls.Add(aspTable)
    '                    tblRow.Cells.Add(tblcell)
    '                    Table.Rows.Add(tblRow)
    '                    Tab.ContentPane.Controls.Add(Table)
    '                End If
    '            End If

    '            Dim dvCusFields As DataView
    '            dvCusFields = dtTable.DefaultView
    '            dvCusFields.RowFilter = "fld_type='Date Field'"
    '            Dim iViewCount As Integer
    '            For iViewCount = 0 To dvCusFields.Count - 1
    '                If Not IsDBNull(dvCusFields(iViewCount).Item("Value")) Then
    '                    bizCalendar = uwOppTab.FindControl("cal" & dvCusFields(iViewCount).Item("fld_id"))
    '                    Dim _myControlType As Type = bizCalendar.GetType()
    '                    _myUC_DueDate = _myControlType.GetProperty("SelectedDate")
    '                    strDate = dvCusFields(iViewCount).Item("Value")
    '                    If strDate = "0" Then strDate = ""
    '                    If strDate <> "" Then
    '                        'strDate = DateFromFormattedDate(strDate, Session("DateFormat"))
    '                        _myUC_DueDate.SetValue(bizCalendar, strDate, Nothing)
    '                    End If
    '                End If
    '            Next
    '        Catch ex As Exception
    '            Throw ex
    '        End Try
    '    End Sub

    '    Sub SaveCusField()
    '        Try
    '            If Not Session("CusFields") Is Nothing Then
    '                Dim dtTable As DataTable
    '                Dim i As Integer
    '                dtTable = Session("CusFields")
    '                For i = 0 To dtTable.Rows.Count - 1
    '                    If dtTable.Rows(i).Item("fld_type") = "Text Box" Then
    '                        Dim txt As TextBox
    '                        txt = uwOppTab.FindControl(dtTable.Rows(i).Item("fld_id"))
    '                        dtTable.Rows(i).Item("Value") = txt.Text
    '                    ElseIf dtTable.Rows(i).Item("fld_type") = "Drop Down List Box" Then
    '                        Dim ddl As DropDownList
    '                        ddl = uwOppTab.FindControl(dtTable.Rows(i).Item("fld_id"))
    '                        dtTable.Rows(i).Item("Value") = CStr(ddl.SelectedItem.Value)
    '                    ElseIf dtTable.Rows(i).Item("fld_type") = "Check box" Then
    '                        Dim chk As CheckBox
    '                        chk = uwOppTab.FindControl(dtTable.Rows(i).Item("fld_id"))
    '                        If chk.Checked = True Then
    '                            dtTable.Rows(i).Item("Value") = "1"
    '                        Else : dtTable.Rows(i).Item("Value") = "0"
    '                        End If
    '                    ElseIf dtTable.Rows(i).Item("fld_type") = "Text Area" Then
    '                        Dim txt As TextBox
    '                        txt = uwOppTab.FindControl(dtTable.Rows(i).Item("fld_id"))
    '                        dtTable.Rows(i).Item("Value") = txt.Text
    '                    ElseIf dtTable.Rows(i).Item("fld_type") = "Date Field" Then
    '                        Dim BizCalendar As UserControl
    '                        BizCalendar = uwOppTab.FindControl("cal" & dtTable.Rows(i).Item("fld_id"))

    '                        Dim strDueDate As String
    '                        Dim _myControlType As Type = BizCalendar.GetType()
    '                        Dim _myUC_DueDate As PropertyInfo = _myControlType.GetProperty("SelectedDate")
    '                        strDueDate = _myUC_DueDate.GetValue(BizCalendar, Nothing)
    '                        If strDueDate <> "" Then
    '                            dtTable.Rows(i).Item("Value") = strDueDate
    '                        Else : dtTable.Rows(i).Item("Value") = ""
    '                        End If
    '                    End If
    '                Next

    '                Dim ds As New DataSet
    '                Dim strdetails As String
    '                dtTable.TableName = "Table"
    '                ds.Tables.Add(dtTable.Copy)
    '                strdetails = ds.GetXml
    '                ds.Tables.Remove(ds.Tables(0))
    '                ds.Dispose()

    '                objCus.strDetails = strdetails
    '                objCus.locId = 3
    '                objCus.RecordId = lngCaseId
    '                objCus.SaveCustomFldsByRecId()
    '            End If
    '        Catch ex As Exception
    '            Throw ex
    '        End Try
    '    End Sub

    '    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    '        Try
    '            PageRedirect()
    '        Catch ex As Exception
    '            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '            Response.Write(ex)
    '        End Try
    '    End Sub

    '    Sub PageRedirect()
    '        Try
    '            If GetQueryStringVal( "frm") = "CaseList" Then
    '                Response.Redirect("../Cases/frmCusCaseList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2, False)
    '            ElseIf GetQueryStringVal( "frm") = "casedtl" Then
    '                Response.Redirect("../pagelayout/frmCustCasedtl.aspx?frm=CaseList" & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2, False)
    '            End If
    '        Catch ex As Exception
    '            Throw ex
    '        End Try
    '    End Sub




    '    Sub FillCustomer(ByVal ddlCombo As DropDownList, ByVal strName As String)
    '        Try
    '            Dim objCommon As New CCommon
    '            With objCommon
    '                .DomainID = Session("DomainID")
    '                .UserCntID = Session("UserContactID")
    '                .Filter = Trim(strName) & "%"
    '                ddlCombo.DataSource = objCommon.PopulateOrganization.Tables(0).DefaultView
    '                ddlCombo.DataTextField = "vcCompanyname"
    '                ddlCombo.DataValueField = "numDivisionID"
    '                ddlCombo.DataBind()
    '            End With
    '            ddlCombo.Items.Insert(0, New ListItem("--Select One--", "0"))
    '        Catch ex As Exception
    '            Throw ex
    '        End Try
    '    End Sub

    '    Private Sub btnAddContact_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddContact.Click
    '        Try
    '            If objCases Is Nothing Then objCases = New CCases
    '            objCases.byteMode = 0
    '            objCases.CaseID = lngCaseId
    '            objCases.DomainID = Session("DomainID")
    '            objCases.ContactID = CInt(ddlAssocContactId.SelectedItem.Value)
    '            objCases.ContactRole = ddlContactrole.SelectedValue
    '            objCases.bitPartner = chkShare.Checked
    '            dgContact.DataSource = objCases.AddCaseContacts
    '            dgContact.DataBind()
    '            ddlContactrole.SelectedIndex = 0
    '            chkShare.Checked = False
    '        Catch ex As Exception
    '            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '            Response.Write(ex)
    '        End Try
    '    End Sub

    '    Public Sub FillContact()
    '        Try
    '            Dim fillCombo As New COpportunities
    '            With fillCombo
    '                .DivisionID = Session("DivID")
    '                ddlAssocContactId.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
    '                ddlAssocContactId.DataTextField = "Name"
    '                ddlAssocContactId.DataValueField = "numcontactId"
    '                ddlAssocContactId.DataBind()
    '            End With
    '            ddlAssocContactId.Items.Insert(0, New ListItem("---Select One---", "0"))
    '        Catch ex As Exception
    '            Throw ex
    '        End Try
    '    End Sub

    '    Private Sub dgContact_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgContact.ItemCommand
    '        Try
    '            If e.CommandName = "Delete" Then
    '                If objCases Is Nothing Then objCases = New CCases
    '                objCases.CaseID = lngCaseId
    '                objCases.ContactID = e.Item.Cells(0).Text
    '                objCases.byteMode = 1
    '                objCases.DomainID = Session("DomainID")
    '                dgContact.DataSource = objCases.AddCaseContacts()
    '                dgContact.DataBind()
    '            End If
    '        Catch ex As Exception
    '            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '            Response.Write(ex)
    '        End Try
    '    End Sub

    '    Private Sub dgContact_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgContact.ItemDataBound
    '        Try
    '            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
    '                Dim btnDelete As Button
    '                Dim lnkDelete As LinkButton
    '                lnkDelete = e.Item.FindControl("lnkDeleteCnt")
    '                btnDelete = e.Item.FindControl("btnDeleteCnt")
    '                If e.Item.Cells(1).Text = 1 Then CType(e.Item.FindControl("lblShare"), Label).Text = "a"
    '            End If
    '        Catch ex As Exception
    '            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '            Response.Write(ex)
    '        End Try
    '    End Sub

End Class