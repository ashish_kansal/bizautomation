<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCusCaseList.aspx.vb"
    Inherits="BACRMPortal.frmCusCaseList" %>

<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../common/topbar.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <title></title>
    <script language="javascript" type="text/javascript">
        function goto(target, frame, div) {
            window.open(target, '', 'toolbar=no,titlebar=no,left=150, top=150,width=750,height=450,scrollbars=no,resizable=yes')

        }
        function fnSortByChar(varSortChar) {
            document.Form1.txtSortChar.value = varSortChar;
            if (document.Form1.txtCurrrentPage != null) {
                document.Form1.txtCurrrentPage.value = 1;
            }
            document.Form1.btnGo.click();
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
    </script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <menu1:webmenu ID="webmenu1" runat="server"></menu1:webmenu>
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
        EnableViewState="true">
        <ContentTemplate>
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td valign="bottom">
                        <asp:Button runat="server" ID="btnGo" Text="" Style="display: none"></asp:Button>
                        <table class="TabStyle">
                            <tr>
                                <td>
                                    &nbsp;&nbsp;&nbsp;Cases&nbsp;&nbsp;&nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="normal1" width="150">
                        No of Records:
                        <asp:Label ID="lblRecordCountCases" runat="server"></asp:Label>
                    </td>
                    <td id="hidenav" nowrap align="right" runat="server">
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="lblNext" runat="server" CssClass="Text_bold">Next:</asp:Label>
                                </td>
                                <td class="normal1">
                                    <asp:LinkButton ID="lnk2" runat="server">2</asp:LinkButton>
                                </td>
                                <td class="normal1">
                                    <asp:LinkButton ID="lnk3" runat="server">3</asp:LinkButton>
                                </td>
                                <td class="normal1">
                                    <asp:LinkButton ID="lnk4" runat="server">4</asp:LinkButton>
                                </td>
                                <td class="normal1">
                                    <asp:LinkButton ID="lnk5" runat="server">5</asp:LinkButton>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkFirst" runat="server">
										<div class="LinkArrow"><<</div>
                                    </asp:LinkButton>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkPrevious" runat="server">
										<div class="LinkArrow"><</div>
                                    </asp:LinkButton>
                                </td>
                                <td class="normal1">
                                    <asp:Label ID="lblPage" runat="server">Page</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCurrrentPage" runat="server" MaxLength="5" AutoPostBack="true"
                                        Width="28px" CssClass="signup"></asp:TextBox>
                                </td>
                                <td class="normal1">
                                    <asp:Label ID="lblOf" runat="server">of</asp:Label>
                                </td>
                                <td class="normal1">
                                    <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkNext" runat="server" CssClass="LinkArrow">
										<div class="LinkArrow">></div>
                                    </asp:LinkButton>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkLast" runat="server">
										<div class="LinkArrow">>></div>
                                    </asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table cellspacing="1" cellpadding="1" width="100%" border="0">
                <tr valign="top">
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="a" href="javascript:fnSortByChar('a')">
                            <div class="A2Z">
                                A</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="b" href="javascript:fnSortByChar('b')">
                            <div class="A2Z">
                                B</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="c" href="javascript:fnSortByChar('c')">
                            <div class="A2Z">
                                C</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="d" href="javascript:fnSortByChar('d')">
                            <div class="A2Z">
                                D</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="e" href="javascript:fnSortByChar('e')">
                            <div class="A2Z">
                                E</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="f" href="javascript:fnSortByChar('f')">
                            <div class="A2Z">
                                F</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="g" href="javascript:fnSortByChar('g')">
                            <div class="A2Z">
                                G</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="h" href="javascript:fnSortByChar('h')">
                            <div class="A2Z">
                                H</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="I" href="javascript:fnSortByChar('i')">
                            <div class="A2Z">
                                I</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="j" href="javascript:fnSortByChar('j')">
                            <div class="A2Z">
                                J</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="k" href="javascript:fnSortByChar('k')">
                            <div class="A2Z">
                                K</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="l" href="javascript:fnSortByChar('l')">
                            <div class="A2Z">
                                L</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="m" href="javascript:fnSortByChar('m')">
                            <div class="A2Z">
                                M</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="n" href="javascript:fnSortByChar('n')">
                            <div class="A2Z">
                                N</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="o" href="javascript:fnSortByChar('o')">
                            <div class="A2Z">
                                O</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="p" href="javascript:fnSortByChar('p')">
                            <div class="A2Z">
                                P</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="q" href="javascript:fnSortByChar('q')">
                            <div class="A2Z">
                                Q</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="r" href="javascript:fnSortByChar('r')">
                            <div class="A2Z">
                                R</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="s" href="javascript:fnSortByChar('s')">
                            <div class="A2Z">
                                S</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="t" href="javascript:fnSortByChar('t')">
                            <div class="A2Z">
                                T</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="u" href="javascript:fnSortByChar('u')">
                            <div class="A2Z">
                                U</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="v" href="javascript:fnSortByChar('v')">
                            <div class="A2Z">
                                V</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="w" href="javascript:fnSortByChar('w')">
                            <div class="A2Z">
                                W</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="x" href="javascript:fnSortByChar('x')">
                            <div class="A2Z">
                                X</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="y" href="javascript:fnSortByChar('y')">
                            <div class="A2Z">
                                Y</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="z" href="javascript:fnSortByChar('z')">
                            <div class="A2Z">
                                Z</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="all" href="javascript:fnSortByChar('0')">
                            <div class="A2Z">
                                All</div>
                        </a>
                    </td>
                </tr>
            </table>
            <asp:Table ID="table2" Width="100%" Height="350" GridLines="None" BorderColor="black"
                runat="server" BorderWidth="1" CellSpacing="0" CellPadding="0" CssClass="aspTable">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Top">
                        <asp:DataGrid ID="dgCases" runat="server" Width="100%" CssClass="dg" BorderColor="white"
                            AutoGenerateColumns="False" AllowSorting="true">
                            <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                            <ItemStyle CssClass="is"></ItemStyle>
                            <HeaderStyle CssClass="hs"></HeaderStyle>
                            <Columns>
                                <asp:BoundColumn Visible="False" DataField="numCaseID"></asp:BoundColumn>
                                <asp:ButtonColumn DataTextField="vcCaseNumber" SortExpression="vcCaseNumber" HeaderText="<font color=white>Case Number</font>"
                                    CommandName="Case"></asp:ButtonColumn>
                                <asp:TemplateColumn HeaderText="<font color=white>Resolve Date</font>" SortExpression="inttargetresolvedate">
                                    <ItemTemplate>
                                        <%#ReturnDateTime(DataBinder.Eval(Container.DataItem, "inttargetresolvedate"))%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:ButtonColumn DataTextField="vccompanyname" SortExpression="vccompanyname" HeaderText="<font color=white>Organization Name</font>"
                                    CommandName="Company"></asp:ButtonColumn>
                                <%--<asp:ButtonColumn DataTextField="Name" SortExpression="ADC.vcFirstName" HeaderText="<font color=white>Contact</font>"
											CommandName="Contact"></asp:ButtonColumn>--%>
                                <asp:BoundColumn DataField="Name" HeaderText="<font color=white>Contact</font>">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="textsubject" SortExpression="textsubject" HeaderText="<font color=white>Subject</font>">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="Status" SortExpression="lst1.vcData" HeaderText="<font color=white>Status</font>">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="priority" SortExpression="lst.vcData" HeaderText="<font color=white>Priority</font>">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="AssignedToBy" SortExpression="ADC1.vcFirstName" HeaderText="<font color=white>Assigned To/ By</font>">
                                </asp:BoundColumn>
                                <asp:TemplateColumn>
                                    <ItemTemplate>
                                        <asp:Button ID="btnDeleteCases" runat="server" CssClass="Delete" Text="X" CommandName="Delete">
                                        </asp:Button>
                                        <asp:LinkButton ID="lnkCaseDelete" runat="server" Visible="false">
								<font color="#730000">*</font></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
            <table width="100%">
                <tr>
                    <td class="normal4" align="center">
                        <asp:Literal ID="litMessage" runat="server"></asp:Literal>
                    </td>
                </tr>
            </table>
            <asp:TextBox ID="txtTotalPage" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtTotalRecords" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtSortChar" Style="display: none" runat="server"></asp:TextBox>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
