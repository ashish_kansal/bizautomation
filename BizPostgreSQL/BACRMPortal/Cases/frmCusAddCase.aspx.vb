'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Imports Telerik.Web.UI

Partial Public Class frmCusAddCase
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then FillContacts()
            btnSaveClose.Attributes.Add("onclick", "return Save()")
            btnClose.Attributes.Add("onclick", "return Close()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub FillContacts()
        Try
            FillContact(ddlContacts, Session("DivId"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function FillContact(ByVal ddlCombo As DropDownList, ByVal lngDivision As Long)
        Try
            Dim fillCombo As New COpportunities
            With fillCombo
                .DivisionID = lngDivision
                ddlCombo.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
                ddlCombo.DataTextField = "vcFirstName"
                ddlCombo.DataValueField = "numcontactId"
                ddlCombo.DataBind()
            End With
            ddlCombo.Items.Insert(0, New ListItem("---Select One---", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            Dim objCases As New CCases
            Dim CaseNo As Long
            CaseNo = objCases.GetMaxCaseID
            Dim CaseId As Integer
            With objCases
                ''Modified by shaziya on 28th march 07
                ''Universal key made default

                'If radDealSupportKey.Checked = True Then
                '    .OppID = ddlDealSupport.SelectedItem.Value
                'End If
                .CaseNo = Format(CaseNo, "00000000000")
                .DivisionID = Session("DivId")
                .ContactID = ddlContacts.SelectedItem.Value
                .Subject = txtSubject.Text
                .Desc = txtDescription.Text
                .DomainID = Session("DomainID")
                .UserCntId = Session("UserContactID")
                'If radUniversalSupportKey.Checked = True Then
                .SupportKeyType = 0
                .AssignTo = Session("RecOwner")
                .TargetResolveDate = calResolve.SelectedDate
                .Status = 1008 'Create Open case by default
                'Else
                '    .SupportKeyType = 1
                'End If
            End With
            CaseId = objCases.SaveCases
            Session("CaseID") = CaseId

            objCases.CaseID = CaseId
            objCases.DomainID = Session("DomainID")
            objCases.strOppSel = GetOppItemsAsString()
            objCases.SaveCaseOpportunities()

            ''Sending mail to Account holder and Manager
            Try
                Dim objAlerts As New CAlerts
                Dim dtDetails As DataTable
                objAlerts.AlertDTLID = 10 'Alert DTL ID for sending alerts in opportunities
                objAlerts.DomainID = Session("DomainID")
                dtDetails = objAlerts.GetIndAlertDTL
                If dtDetails.Rows.Count > 0 Then
                    If dtDetails.Rows(0).Item("tintAlertOn") = 1 Then
                        Dim dtEmailTemplate As DataTable
                        Dim objDocuments As New DocumentList
                        objDocuments.GenDocID = dtDetails.Rows(0).Item("numEmailTemplate")
                        objDocuments.DomainID = Session("DomainID")
                        dtEmailTemplate = objDocuments.GetDocByGenDocID
                        If dtEmailTemplate.Rows.Count > 0 Then
                            Dim objSendMail As New Email
                            Dim dtMergeFields As New DataTable
                            Dim drNew As DataRow
                            dtMergeFields.Columns.Add("CaseID")
                            dtMergeFields.Columns.Add("Organization")
                            dtMergeFields.Columns.Add("Employee")
                            drNew = dtMergeFields.NewRow
                            drNew("CaseID") = CaseId
                            drNew("Organization") = Session("CompName")
                            drNew("Employee") = Session("RecOwnerName")
                            dtMergeFields.Rows.Add(drNew)
                            Dim objCommon As New CCommon
                            objCommon.byteMode = 0
                            objCommon.ContactID = Session("RecOwner")
                            objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), IIf(dtDetails.Rows(0).Item("tintCCManager") = 1, objCommon.GetManagerEmail, ""), Session("UserEmail"), objCommon.GetContactsEmail, dtMergeFields)
                        End If
                    End If
                End If
            Catch ex As Exception

            End Try
            Dim strScript As String = "<script language=JavaScript>"
            strScript += "window.opener.reDirect('../pagelayout/frmCustCasedtl.aspx?frm=CaseList'); self.close();"
            strScript += "</script>"
            If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    'Get top 5 orders placed by selected customer
    Private Sub BindRecentOrders()
        Try
            Dim objOpp As New COpportunities
            Dim ds As DataSet
            objOpp.DomainID = Session("DomainID")
            objOpp.ContactID = CCommon.ToLong(ddlContacts.SelectedValue)
            objOpp.DivisionID = Session("DivId")
            objOpp.OppType = 1 'sales orders
            objOpp.PageSize = 5
            ds = objOpp.GetTopOrders()
            If ds.Relations.Count = 0 And ds.Tables.Count = 2 Then
                ds.Tables(0).TableName = "Orders"
                ds.Tables(1).TableName = "OrderItems"
                ds.Relations.Add("Orders", ds.Tables(0).Columns("numOppId"), ds.Tables(1).Columns("numOppId"), False)
            End If
            gvOrders.DataSource = ds
            gvOrders.DataBind()
          

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub ddlContacts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlContacts.SelectedIndexChanged
        Try
            If ddlContacts.SelectedValue <> "" Then
                BindRecentOrders()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Function GetOppItemsAsString() As String
        Try
            Dim ds As New DataSet
            Dim dt As New DataTable
            dt.Columns.Add("numOppId")
            dt.Columns.Add("numOppItemID")
            Dim dr As DataRow
            'Dim en As IEnumerator
            'Dim row As UltraGridRow
            'For Each item As UltraGridRow In uwOrders.Rows
            '    'If CCommon.ToBool(item.Cells(0).Value) = True Then
            '    en = item.Rows.GetEnumerator()
            '    While en.MoveNext()
            '        row = en.Current
            '        If CCommon.ToBool(row.Cells(0).Value) = True Then
            '            dr = dt.NewRow
            '            dr("numOppId") = item.Cells(0).Value.ToString
            '            dr("numOppItemID") = row.Cells(1).Value.ToString
            '            dt.Rows.Add(dr)
            '        End If
            '    End While
            '    'End If
            'Next

            For Each drOrder As GridDataItem In gvOrders.Items
                If drOrder.HasChildItems Then
                    Dim item As GridNestedViewItem = drOrder.ChildItem()

                    For Each drItem As GridDataItem In item.NestedTableViews(0).Items
                        If CType(drItem.FindControl("chkSelect"), CheckBox).Checked = True Then
                            dr = dt.NewRow
                            dr("numOppId") = drItem.GetDataKeyValue("numOppId")
                            dr("numOppItemID") = drItem.GetDataKeyValue("numoppitemtCode")
                            dt.Rows.Add(dr)
                        End If
                    Next
                End If
            Next

            ds.Tables.Add(dt)
            Return ds.GetXml()
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
