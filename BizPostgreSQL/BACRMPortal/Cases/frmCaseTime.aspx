<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCaseTime.aspx.vb" Inherits="BACRMPortal.frmCaseTime" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" src="../include/calandar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
	<link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
		<title>Time</title>
          <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
		<script language="javascript">
		function Close(a)
			{ 
//			    alert(a)
				
				window.close();
			}
		function Save()
			{
				if (document.Form1.radBill.checked==true)
				{
				    if (document.Form1.txtRate.value=="")
				    {
					    alert("Enter Rate")
					    //document.Form1.txtRate.focus();
					    return false;
				    }
				}
				if (document.Form1.calFrom_txtDate.value=="")
				{
					alert("Enter Start Date")
					document.Form1.calFrom_txtDate.focus();
					return false;
				}
				if (document.Form1.calto_txtDate.value=="")
				{
					alert("Enter End Date")
					document.Form1.calto_txtDate.focus();
					return false;
				}
				
				if (document.Form1.radBill.checked==true)
				{
				    if (document.Form1.ddlOpp.value=="0")
				    {
				       alert("Select Opportunity")
					    document.Form1.ddlOpp.focus();
					    return false; 
				    }
				}
			}
			function OpenRec(a)
		{
			window.open('../opportunity/frmRecomBillTime.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Hour='+document.Form1.txtHour.value+'&DivID='+a,'','toolbar=no,titlebar=no,left=300,top=300,width=300,height=200,scrollbars=yes,resizable=yes')
			return false;
		}
		function FillBox(a,b)
		{
		 document.Form1.txtRate.value=a
		 document.Form1.txtItemID.value=b;

		}
		</script>
	</HEAD>
	<body  >
		<form id="Form1" method="post" runat="server">

		        
			<table width="100%" >
				<tr>
				  
					<td align="right" colSpan="6">
						<asp:Button ID="btndelete" Runat="server" CssClass="button" Text="Delete" Width=50></asp:Button>
						<asp:Button ID="btnSave" Runat="server" CssClass="button" Text="Save &amp; Close"></asp:Button>
						<asp:Button ID="btnCancel" Runat="server" CssClass="button" Text="Close"></asp:Button>
						<br>
						<br>
					</td>
				</tr>
			      <tr>
			        <td></td>
			          <td>
				        <asp:RadioButton ID="radBill" AutoPostBack="true" runat="server" CssClass="normal1" GroupName="Bill" Text="Billable"  Checked="true" />
				        <asp:RadioButton ID="radNonBill" runat="server" AutoPostBack="true" CssClass="normal1" GroupName="Bill" Text="Non-Billable" />
				    </td>
			      </tr>
			      <asp:Panel runat="server" id="pnlRate">
				<tr>
					<td class="normal1" align="right"><asp:HyperLink ID=hplRec runat=server CssClass=hyperlink>Rate/Hour</asp:HyperLink>
					</td>
					<td><asp:textbox id="txtRate" CssClass=signup Runat="server" Width="50"></asp:textbox></td>
				</tr></asp:Panel>
				<tr>
				    	<td class="normal1" align="right">From Date</td>
					<td><BizCalendar:Calendar ID="calFrom" runat="server" /></td>
					<td class="text" id="tdStartTime" runat="server" ><IMG  src="../images/Clock-16.gif"  align="absMiddle">
									            <asp:dropdownlist id="ddltime" Runat="server" Width="55" CssClass="signup">
										            <asp:ListItem Selected="False" Value="23">12:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="24">12:30</asp:ListItem>
										            <asp:ListItem Selected="False" Value="1">1:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="2">1:30</asp:ListItem>
										            <asp:ListItem Selected="False" Value="3">2:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="4">2:30</asp:ListItem>
										            <asp:ListItem Selected="False" Value="5">3:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="6">3:30</asp:ListItem>
										            <asp:ListItem Selected="False" Value="7">4:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="8">4:30</asp:ListItem>
										            <asp:ListItem Selected="False" Value="9">5:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="10">5:30</asp:ListItem>
										            <asp:ListItem Selected="False" Value="11">6:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="12">6:30</asp:ListItem>
										            <asp:ListItem Selected="False" Value="13">7:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="14">7:30</asp:ListItem>
										            <asp:ListItem Selected="True" Value="15">8:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="16">8:30</asp:ListItem>
										            <asp:ListItem Selected="False" Value="17">9:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="18">9:30</asp:ListItem>
										            <asp:ListItem Selected="False" Value="19">10:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="20">10:30</asp:ListItem>
										            <asp:ListItem Selected="False" Value="21">11:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="22">11:30</asp:ListItem>
									            </asp:dropdownlist>&nbsp;AM<input id="chkAM" type="radio" CHECKED value="0" name="AM" runat="server"/>PM<input id="chkPM" type="radio" value="1" name="AM" runat="server"/>
								            </td>
				</tr>
				<tr>
				<td class="normal1" align="right">End Date</td>
				         <td>
                             <BizCalendar:Calendar ID="calto" runat="server" />
                           </td>
                                        <td class="text"  id="tdEndTime" runat="server" ><IMG  src="../images/Clock-16.gif"  align="absMiddle"/>
									            <asp:dropdownlist id="ddlEndTime" Runat="server" Width="55" CssClass="signup">
										            <asp:ListItem Selected="False" Value="23">12:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="24">12:30</asp:ListItem>
										            <asp:ListItem Selected="False" Value="1">1:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="2">1:30</asp:ListItem>
										            <asp:ListItem Selected="False" Value="3">2:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="4">2:30</asp:ListItem>
										            <asp:ListItem Selected="False" Value="5">3:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="6">3:30</asp:ListItem>
										            <asp:ListItem Selected="False" Value="7">4:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="8">4:30</asp:ListItem>
										            <asp:ListItem Selected="False" Value="9">5:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="10">5:30</asp:ListItem>
										            <asp:ListItem Selected="False" Value="11">6:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="12">6:30</asp:ListItem>
										            <asp:ListItem Selected="False" Value="13">7:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="14">7:30</asp:ListItem>
										            <asp:ListItem Selected="False" Value="15">8:00</asp:ListItem>
										            <asp:ListItem Selected="True" Value="16">8:30</asp:ListItem>
										            <asp:ListItem Selected="False" Value="17">9:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="18">9:30</asp:ListItem>
										            <asp:ListItem Selected="False" Value="19">10:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="20">10:30</asp:ListItem>
										            <asp:ListItem Selected="False" Value="21">11:00</asp:ListItem>
										            <asp:ListItem Selected="False" Value="22">11:30</asp:ListItem>
									            </asp:dropdownlist>&nbsp;AM<input id="chkEndAM" type="radio" CHECKED value="0" name="EndAM" runat="server"/>PM<input id="chkEndPM" type="radio" value="1" name="EndAM" runat="server"/>
								            </td>
								           
				</tr>
			
				    <tr>
					    <td class="normal1" align="right">Contract
					    </td>
					    <td colspan="2"><asp:DropDownList ID="ddlContract" AutoPostBack="true" Width="180" runat="server"  CssClass="signup"></asp:DropDownList>&nbsp;(Apply  Hours and / or Expenses)</td>
				    </tr>
				        <tr>
				            <td class="normal1" align="right">
				                 Contract Hours Remaining :
				                </td>
				                <td>
				                <asp:Label ID="lblRemHours" runat="server" CssClass="normal1"></asp:Label>
				                </td>
				            </tr>
		
				  <tr class="normal1" align="right">
				        <td align="right" class="normal1">
				            Opportunity 
				        </td>
				        <td  colspan="2"  align="left" class="normal1">
				            <asp:DropDownList ID="ddlOpp" runat="server" Width="300" CssClass="signup" ></asp:DropDownList>
				        </td>
				    </tr>
				    <tr>
					    <td class="normal1" align="right">Description
					    </td>
					    <td colSpan="2"><asp:textbox id="txtDesc" CssClass=signup Runat="server" Width="405" Height="50" TextMode="MultiLine"></asp:textbox></td>
				    </tr>
				    <tr>
				        <td align="center" colspan="6" class="normal4">
				          <asp:Literal runat="server" id="litMessage" EnableViewState="False"  ></asp:Literal> 				            
				        </td>
				    </tr>
			    </table>
		<asp:textbox id="txtCategoryHDRID" Runat="server" Text="0" style="display:none" ></asp:textbox>
		</form>
	</body>
</html>
