Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Common
Partial Public Class frmCaseLinkSol
    Inherits BACRMPage

#Region "Variables"
    Dim strColumn As String
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not IsPostBack Then
                Session("Help") = "Support"
                bindGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub bindGrid()
        Try
            Dim dtSolution As DataTable
            Dim objSolution As New Solution
            objSolution.Category = 0
            objSolution.KeyWord = txtKeyWord.Text
            objSolution.DomainID = Session("DomainID")
            objSolution.SortCharacter = "0"
            If strColumn <> "" Then objSolution.columnName = strColumn
            If Session("Asc") = 1 Then
                objSolution.columnSortOrder = "Asc"
            Else : objSolution.columnSortOrder = "Desc"
            End If
            dtSolution = objSolution.GetKnowledgeBases
            dgBases.DataSource = dtSolution
            dgBases.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgBases_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgBases.SortCommand
        Try
            strColumn = e.SortExpression.ToString()
            If Session("Column") <> strColumn Then
                Session("Column") = strColumn
                Session("Asc") = 0
            Else
                If Session("Asc") = 0 Then
                    Session("Asc") = 1
                Else : Session("Asc") = 0
                End If
            End If
            bindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnGoBases_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGoBases.Click
        Try
            bindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgBases_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgBases.ItemCommand
        Try
            If e.CommandName = "Link" Then
                Dim objSolution As New Solution
                objSolution.CaseID = GetQueryStringVal( "CaseID")
                objSolution.SolID = e.Item.Cells(0).Text.Trim
                objSolution.LinkSolToCases()
                Dim strScript As String = "<script language=JavaScript>"
                strScript += "window.parent.location.reload(true);"
                strScript += "</script>"
                If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class