<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmAddCases.aspx.vb" Inherits="BACRMPortal.frmAddCases"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" src="../include/calandar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
        <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
		<title>Case</title>
          <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
	<script language="javascript" type="text/javascript" >
	function Save()
	{
	  if ((document.Form1.ddlCompany.selectedIndex==-1 )||(document.Form1.ddlCompany.value==0))
			{
				alert("Select Company")
				document.Form1.txtCompName.focus();
				return false;
			}
			if ((document.Form1.ddlContacts.selectedIndex==-1 )||(document.Form1.ddlContacts.value==0 ))
			{
				alert("Select Contact")
				document.Form1.ddlContacts.focus();
				return false;
			}
			if (document.Form1.txtSubject.value=="")
			{
				alert("Enter Subject")
				document.Form1.txtSubject.focus();
				return false;
			}
			if (document.Form1.calResolve_txtDate.value=="")
			{
			    alert("Enter Resolve Date")
				document.Form1.calResolve_txtDate.focus();
				return false;
			    
			}
			if (document.Form1.txtDescription.value=="")
			{
				alert("Enter Description")
				document.Form1.txtDescription.focus();
				return false;
			}
	}
	function Focus()
					{
						document.Form1.txtCompName.focus();
					}
     function Close()
		{
		    window.close()
		    return false;
		}
		</script>
	</HEAD>
	<body onload="Focus()" >
		<form id="Form1" method="post" runat="server">
		<asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
       <br />
        <table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td valign="bottom">
					<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;New 
									Case&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td align="right" >
						<asp:Button ID="btnSaveClose" Runat="server" Text="Save &amp; Close" CssClass="button"></asp:Button>
						<asp:button id="btnClose" Runat="server" CssClass="button" Text="Close" Width="50"></asp:button>
					</td>
				</tr>
			</table>
         <asp:updatepanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" >
            <ContentTemplate>
			<asp:table id="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" Runat="server" Width="100%" CssClass="aspTable"
				BorderColor="black" GridLines="None">
				<asp:TableRow>
					<asp:TableCell>
						<br/>
						<table border="0" width="100%">
							<tr>
							    <td valign="top"  rowspan="30">
                                     <img src="../images/SuitCase-32.gif" />
								</td>
								<td class="normal1" align="right">Account<FONT color="red">*</FONT>
								<td>
									<asp:textbox id="txtCompName" Runat="server" Width="145"  cssclass="signup"></asp:textbox>&nbsp;
									<asp:Button ID="btnGo" Runat="server" Text="Go" CssClass="button"></asp:Button>&nbsp;
									<asp:DropDownList ID="ddlCompany" Runat="server" Width="200" AutoPostBack="True" CssClass="signup"></asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="normal1" align="right">Contact<FONT color="red">*</FONT>
								</td>
								<td>
									<asp:DropDownList ID="ddlContacts" Runat="server" Width="200" CssClass="signup"></asp:DropDownList></td>
							</tr>
							<tr>
								<td class="normal1" align="right">Subject<FONT color="red">*</FONT></td>
								<td>
									<asp:TextBox ID="txtSubject" cssclass="signup" Runat="server" Width="200"></asp:TextBox></td>
							</tr>
							<tr>
								<td class="normal1" align="right">Resolve Date<FONT color="red">*</FONT></td>
								<td>
									<BizCalendar:Calendar ID="calResolve" runat="server" />
								</td>
							</tr>
							<tr>
								<td class="normal1" align="right">Description<FONT color="red">*</FONT></td>
								<td>
									<asp:TextBox ID="txtDescription" cssclass="signup" Runat="server" Width="500" TextMode=MultiLine  Height="50"></asp:TextBox></td>
							</tr>
							
						</table>
						<br/>
					</asp:TableCell>
				</asp:TableRow>
			</asp:table>
			</ContentTemplate>
			</asp:updatepanel>
		</form>
	</body>
</HTML>
