<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmSolution.aspx.vb" Inherits="BACRMPortal.frmSolution"%>
<%@ Register TagPrefix="menu1" TagName="webmenu" src="../common/topbar.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1"    runat="server">		
        <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
		<title>Solution</title>
		<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
         <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)"> 
		<script language="javascript">
		function fn_GoToURL(varURL)
		{
			
			if ((varURL!='') && (varURL.substr(0,7)=='http://') && (varURL.length > 7))
			{
				var LoWindow=window.open(varURL,"","");
				LoWindow.focus();
			}
			return false;
		}
		function OpenDocuments(a)
		{
			window.open("../Documents/frmSpecDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=S&yunWE="+a,'','toolbar=no,titlebar=no,top=200,width=700,height=450,scrollbars=yes,resizable=yes')
			return false;
		}
		</script>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
		<menu1:webmenu id="webmenu1" runat="server"></menu1:webmenu>
            <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>
		
		<asp:updatepanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" ><ContentTemplate>
			<br>
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td valign="bottom">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp; Solution 
									Details&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td align="right">
						<asp:button id="btnSave" Runat="server" Text="Save" CssClass="button"></asp:button>
						<asp:button id="btnSaveClose" Runat="server" CssClass="button" text="Save &amp; Close"></asp:button>
						<asp:button id="btnCancel" Runat="server" Text="Cancel" CssClass="button"></asp:button></td>
				</tr>
			</table>
			<asp:table id="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" Runat="server" Width="100%" CssClass="aspTable"
				BorderColor="black" GridLines="None">
				<asp:TableRow>
					<asp:TableCell>
						<br>
						<table width="100%" border="0">
							<%--<tr>
								<td class="normal1" align="right">Customer</td>
								<td class="normal1">
									<asp:textbox id="txtCompName" Runat="server" width="125px" cssclass="signup"></asp:textbox>
									&nbsp;
									<asp:Button ID="btnSearch" Runat="server" Width="25" CssClass="button" Text="Go"></asp:Button>&nbsp;
									<asp:dropdownlist id="ddlCompany" CssClass="signup" Runat="server" AutoPostBack="True" Width="200"></asp:dropdownlist>
									</td>
							</tr>
							<tr>
								<td class="normal1" align="right">
								Case(s)
								<td>
									<asp:DropDownList ID="ddlCaseNo" Runat="server" Width="480" CssClass="signup"></asp:DropDownList>
									&nbsp;
									<asp:Button id="btnlnkCase" CssClass="button" Runat="server" Width="190" Text="Link Solution to an Open Case"></asp:Button>
								</td>
							</tr>--%>
							<tr>
								<td class="normal1" align="right">Soluton Name<FONT color="red">*</FONT>
								</td>
								<td>
									<asp:TextBox ID="txtSolution" cssclass="signup" Runat="server" Width="400"></asp:TextBox></td>
							</tr>
							<tr>
								<td class="normal1" align="right">Solution Category<FONT color="red">*</FONT>
								</td>
								<td>
									<asp:DropDownList ID="ddlCategory" Runat="server" Width="130" CssClass="signup"></asp:DropDownList></td>
							</tr>
							<tr>
								<td class="normal1" align="right">Solution Description<FONT color="red">*</FONT></td>
								<td colspan="4">
									<asp:TextBox ID="txtSolDesc" cssclass="signup" Runat="server" TextMode="MultiLine" Width="700" Height="250"></asp:TextBox></td>
							</tr>
							<tr>
								<td class="normal1" align="right">URL
								</td>
								<td>
									<asp:textbox id="txtLink" runat="server" value="http://" width="250px" cssclass="signup"></asp:textbox>&nbsp;
									<asp:button id="btnLinkGo" Runat="server" CssClass="button" Text="Go" Width="25"></asp:button>
								</td>
							</tr>
							<tr>
								<td>
								</td>
								<td class="normal1">
									<asp:hyperlink id="hplDocuments" Runat="server" CssClass="hyperlink">
										<font color="#180073">Documents</font></asp:hyperlink>
								</td>
							</tr>
						</table>
						<br>
					</asp:TableCell>
				</asp:TableRow>
			</asp:table>
			</ContentTemplate>
			</asp:updatepanel>
		</form>
	</body>
</HTML>
