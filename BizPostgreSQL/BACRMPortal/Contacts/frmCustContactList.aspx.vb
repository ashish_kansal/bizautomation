
'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Account
Namespace BACRM.UserInterface.Contacts

    Public Class frmCustContactList
        Inherits System.Web.UI.Page
        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        ' Protected WithEvents lblRecordCount As System.Web.UI.WebControls.Label
        Protected WithEvents txtCustomer As System.Web.UI.WebControls.TextBox
        Protected WithEvents txtFirstName As System.Web.UI.WebControls.TextBox
        Protected WithEvents txtLastName As System.Web.UI.WebControls.TextBox
        ' Protected WithEvents btnGo As System.Web.UI.WebControls.Button
        Protected WithEvents ddlSort As System.Web.UI.WebControls.DropDownList
        '  Protected WithEvents lblNext As System.Web.UI.WebControls.Label
        Protected WithEvents lnk1 As System.Web.UI.WebControls.LinkButton
        'Protected WithEvents lnk2 As System.Web.UI.WebControls.LinkButton
        'Protected WithEvents lnk3 As System.Web.UI.WebControls.LinkButton
        'Protected WithEvents lnk4 As System.Web.UI.WebControls.LinkButton
        'Protected WithEvents lnk5 As System.Web.UI.WebControls.LinkButton
        'Protected WithEvents lnkFirst As System.Web.UI.WebControls.LinkButton
        'Protected WithEvents lnkPrevious As System.Web.UI.WebControls.LinkButton
        'Protected WithEvents lblPage As System.Web.UI.WebControls.Label
        'Protected WithEvents txtCurrrentPage As System.Web.UI.WebControls.TextBox
        'Protected WithEvents lblOf As System.Web.UI.WebControls.Label
        'Protected WithEvents lblTotal As System.Web.UI.WebControls.Label
        'Protected WithEvents lnkNext As System.Web.UI.WebControls.LinkButton
        'Protected WithEvents lnkLast As System.Web.UI.WebControls.LinkButton
        'Protected WithEvents litMessage As System.Web.UI.WebControls.Literal
        'Protected WithEvents txtTotalPage As System.Web.UI.WebControls.TextBox
        'Protected WithEvents txtTotalRecords As System.Web.UI.WebControls.TextBox
        'Protected WithEvents txtSortChar As System.Web.UI.WebControls.TextBox
        'Protected WithEvents hidenav As System.Web.UI.HtmlControls.HtmltableCell
        'Protected WithEvents dgContacts As System.Web.UI.WebControls.DataGrid
        Dim strColumn As String
        Dim m_aryRightsForPage() As Integer
        '  Protected WithEvents table2 As System.Web.UI.WebControls.table
        Protected WithEvents btnNew As System.Web.UI.WebControls.Button
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If Session("UserContactID") = Nothing Then
                Response.Redirect("../Login.aspx")
            End If
            m_aryRightsForPage = clsAuthorization.fn_ExternalPageLevelRights("frmContactList.aspx", Session("UserContactID"), 15, 2)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../Common/frmAuthorization.aspx")
            End If
            If Not GetQueryStringVal(Request.QueryString("enc"), "SI") Is Nothing Then
                SI = GetQueryStringVal(Request.QueryString("enc"), "SI")
            End If
            If Not GetQueryStringVal(Request.QueryString("enc"), "SI1") Is Nothing Then
                SI1 = GetQueryStringVal(Request.QueryString("enc"), "SI1")
            Else
                SI1 = 0
            End If
            If Not GetQueryStringVal(Request.QueryString("enc"), "SI2") Is Nothing Then
                SI2 = GetQueryStringVal(Request.QueryString("enc"), "SI2")
            Else
                SI2 = 0
            End If
            If Not GetQueryStringVal(Request.QueryString("enc"), "frm") Is Nothing Then
                frm = ""
                frm = GetQueryStringVal(Request.QueryString("enc"), "frm")
            Else
                frm = ""
            End If
            If Not GetQueryStringVal(Request.QueryString("enc"), "frm1") Is Nothing Then
                frm1 = ""
                frm1 = GetQueryStringVal(Request.QueryString("enc"), "frm1")
            Else
                frm1 = ""
            End If
            If Not GetQueryStringVal(Request.QueryString("enc"), "SI2") Is Nothing Then
                frm2 = ""
                frm2 = GetQueryStringVal(Request.QueryString("enc"), "frm2")
            Else
                frm2 = ""
            End If
            'If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
            '    btnNew.Visible = False
            'End If
            Dim dtTab As New DataTable
            dtTab = Session("DefaultTab")
            If dtTab.Rows.Count > 0 Then
                lbContacts.Text = dtTab.Rows(0).Item("vcContact") & "s"
            Else
                lbContacts.Text = "Contacts"
            End If
            If Not IsPostBack Then
                Session("Asc") = 1
                txtCurrrentPage.Text = 1
                BindDatagrid()
            End If
            If txtSortChar.Text <> "" Then
                ViewState("SortChar") = txtSortChar.Text
                ViewState("Column") = "vcFirstName"
                Session("Asc") = 0
                BindDatagrid()
                txtSortChar.Text = ""
            End If
            'btnNew.Attributes.Add("onclick", "return goto('../Contacts/frmNewContact.aspx','cntOpenItem','divNew');")
        End Sub

        Sub BindDatagrid()
            Dim dtContacts As New DataTable
            Dim objContacts As New CContacts
            Dim SortChar As Char
            If ViewState("SortChar") <> "" Then
                SortChar = ViewState("SortChar")
            Else
                SortChar = "0"
            End If
            With objContacts
                .UserCntID = Session("UserContactID")
                .DomainID = Session("DomainID")
                .SortCharacter = SortChar
                If txtCurrrentPage.Text.Trim <> "" Then
                    .CurrentPage = txtCurrrentPage.Text
                Else
                    .CurrentPage = 1
                End If
                .PageSize = Session("PagingRows")
                .TotalRecords = 0
                If ViewState("Column") <> "" Then
                    .columnName = ViewState("Column")
                Else
                    .columnName = "DM.bintcreateddate"
                End If
                If Session("Asc") = 1 Then
                    .columnSortOrder = "Desc"
                Else
                    .columnSortOrder = "Asc"
                End If
                .CompanyID = Session("CompID")
            End With
            dtContacts = objContacts.GetContactList
            If objContacts.TotalRecords = 0 Then
                hidenav.Visible = False
                lblRecordCount.Text = 0
            Else
                hidenav.Visible = True
                lblRecordCount.Text = String.Format("{0:#,###}", objContacts.TotalRecords)
                Dim strTotalPage As String()
                Dim decTotalPage As Decimal
                decTotalPage = lblRecordCount.Text / Session("PagingRows")
                decTotalPage = Math.Round(decTotalPage, 2)
                strTotalPage = CStr(decTotalPage).Split(".")
                If (lblRecordCount.Text Mod Session("PagingRows")) = 0 Then
                    lblTotal.Text = strTotalPage(0)
                    txtTotalPage.Text = strTotalPage(0)
                Else
                    lblTotal.Text = strTotalPage(0) + 1
                    txtTotalPage.Text = strTotalPage(0) + 1
                End If
                txtTotalRecords.Text = lblRecordCount.Text
            End If
            dgContacts.DataSource = dtContacts
            dgContacts.DataBind()
        End Sub

        Private Sub lnkLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkLast.Click
            txtCurrrentPage.Text = txtTotalPage.Text
            BindDatagrid()
        End Sub

        Private Sub lnkPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkPrevious.Click
            If txtCurrrentPage.Text = 1 Then
                Exit Sub
            Else
                txtCurrrentPage.Text = txtCurrrentPage.Text - 1
            End If
            BindDatagrid()
        End Sub

        Private Sub lnkFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkFirst.Click
            txtCurrrentPage.Text = 1
            BindDatagrid()
        End Sub

        Private Sub lnkNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkPrevious.Click, lnkNext.Click
            If txtCurrrentPage.Text = txtTotalPage.Text Then
                Exit Sub
            Else
                txtCurrrentPage.Text = txtCurrrentPage.Text + 1
            End If
            BindDatagrid()
        End Sub

        Private Sub lnk2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk2.Click
            If txtCurrrentPage.Text + 1 = txtTotalPage.Text Or txtCurrrentPage.Text + 1 > txtTotalPage.Text Then
                Exit Sub
            Else
                txtCurrrentPage.Text = txtCurrrentPage.Text + 2
            End If
            BindDatagrid()
        End Sub

        Private Sub lnk3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk3.Click
            If txtCurrrentPage.Text + 2 = txtTotalPage.Text Or txtCurrrentPage.Text + 2 > txtTotalPage.Text Then
                Exit Sub
            Else
                txtCurrrentPage.Text = txtCurrrentPage.Text + 3
            End If
            BindDatagrid()
        End Sub

        Private Sub lnk4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk4.Click
            If txtCurrrentPage.Text + 3 = txtTotalPage.Text Or txtCurrrentPage.Text + 3 > txtTotalPage.Text Then
                Exit Sub
            Else
                txtCurrrentPage.Text = txtCurrrentPage.Text + 4
            End If
            BindDatagrid()
        End Sub

        Private Sub lnk5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk5.Click
            If txtCurrrentPage.Text + 4 = txtTotalPage.Text Or txtCurrrentPage.Text + 4 > txtTotalPage.Text Then
                Exit Sub
            Else
                txtCurrrentPage.Text = txtCurrrentPage.Text + 5
            End If
            BindDatagrid()
        End Sub

        Private Sub txtCurrrentPage_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCurrrentPage.TextChanged
            BindDatagrid()
        End Sub

        Private Sub dgContacts_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgContacts.ItemDataBound
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As Button
                Dim lnkDelete As LinkButton
                lnkDelete = e.Item.FindControl("lnkDelete")
                btnDelete = e.Item.FindControl("btnDelete")
                If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                    btnDelete.Visible = False
                    lnkDelete.Visible = True
                    lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                Else
                    btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                End If
            End If
        End Sub

        Private Sub dgContacts_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgContacts.ItemCommand
            Dim lngContID As Long
            Dim StrEmail As String
            If Not e.CommandName = "Sort" Then
                lngContID = e.Item.Cells(0).Text()
            End If
            If e.CommandName = "Company" Then
                If Session("EnableIntMedPage") = 1 Then
                    Response.Redirect("../pagelayout/frmAccountdtl.aspx?frm=ContactList" & "&SI1=0&SI2=" & SI1 & "&frm1=" & frm1 & "&frm2=" & frm2)
                Else
                    Response.Redirect("../common/frmAccounts.aspx?frm=ContactList" & "&SI1=0&SI2=" & SI1 & "&frm1=" & frm1 & "&frm2=" & frm2)
                End If

            ElseIf e.CommandName = "Contact" Then
                Session("ContactId") = lngContID
                If Session("EnableIntMedPage") = 1 Then
                    Response.Redirect("../pagelayout/frmContactdtl.aspx?frm=ContactList" & "&SI1=0&SI2=" & SI1 & "&frm1=" & frm1 & "&frm2=" & frm2)
                Else
                    Response.Redirect("../Contacts/frmContacts.aspx?frm=ContactList" & "&SI1=0&SI2=" & SI1 & "&frm1=" & frm1 & "&frm2=" & frm2)
                End If

            ElseIf e.CommandName = "Delete" Then
                Dim intRetVal As Integer
                Dim objContact As New CContacts
                objContact.ContactID = lngContID
                objContact.DomainID = Session("DomainID")
                intRetVal = objContact.DelContact
                If intRetVal > 1 Then
                    litMessage.Text = "Dependent Records Exists.Cannot be deleted."
                Else
                    BindDatagrid()
                    litMessage.Text = ""
                End If
            End If
        End Sub

        Private Sub dgContacts_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgContacts.SortCommand
            strColumn = e.SortExpression.ToString()
            If ViewState("Column") <> strColumn Then
                ViewState("Column") = strColumn
                Session("Asc") = 0
            Else
                If Session("Asc") = 0 Then
                    Session("Asc") = 1
                Else
                    Session("Asc") = 0
                End If
            End If
            BindDatagrid()
        End Sub

        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            BindDatagrid()
        End Sub
    End Class
End Namespace