Imports System.Data.SqlClient
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Admin
Partial Class frmNewContact
    Inherits System.Web.UI.Page
    Protected WithEvents divFrmNew3 As System.Web.UI.HtmlControls.HtmlGenericControl
    Protected WithEvents cntOpenItem3 As System.Web.UI.HtmlControls.HtmlGenericControl



#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Dim ContactId As Long
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        btnSave.Attributes.Add("onclick", "return Save()")
        btnClose.Attributes.Add("onclick", "return Close()")
        Dim dtTab As New DataTable
        dtTab = Session("DefaultTab")
        If dtTab.Rows.Count > 0 Then
            lbContacts.Text = "New " & IIf(IsDBNull(dtTab.Rows(0).Item("vcContact")), "Contacts", dtTab.Rows(0).Item("vcContact").ToString & "s")
        Else
            lbContacts.Text = "New Contacts"
        End If
    End Sub


    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim ContactId As Long
            'Create object of leads class
            Dim objLeads As New CLeads
            'Passing paramter to object
            'Save Additional Contact information
            With objLeads
                .DivisionID = Session("DivId")
                .FirstName = txtFirstName.Text
                .LastName = txtLastName.Text
                .Phone = txtPhone.Text
                .PhoneExt = txtExt.Text
                .Email = txtEmail.Text
                .DomainID = Session("DomainID")
                .UserCntID = Session("ConID")
                ContactId = .CreateRecordAddContactInfo
            End With
            Session("ContactId") = ContactId
            Dim strScript As String = "<script language=JavaScript>"
            strScript += "window.opener.reDirect('../Contacts/frmContacts.aspx'); self.close();"
            strScript += "</script>"
            If (Not Page.IsStartupScriptRegistered("clientScript")) Then
                Page.RegisterStartupScript("clientScript", strScript)
            End If
        Catch ex As Exception

        End Try
    End Sub

End Class
