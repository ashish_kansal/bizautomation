<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmMoveContact.aspx.vb" Inherits="BACRMPortal.frmMoveContact" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Move Contact</title>
		<script language="javascript" type="text/javascript">
		function Close()
        {
	        window.close()
	        return false;
        }
		</script>
	</head>
	<body>
		<form id="frmMoveContact" action="" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0" width="100%" style="PADDING-TOP: 2px">
				<tr>
					<td style="WIDTH: 258px" vAlign="bottom" width="258">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;Move Contact&nbsp;</td>
							</tr>
						</table>
					</td>
					<td class="normal1" align="right">
						<asp:button id="btnMergeContacts" CssClass="button" Width="80" Text="Move Contact"
							Runat="server"></asp:button>&nbsp;
						<asp:button id="btnClose" CausesValidation="false" CssClass="button" Width="50" Text="Close"
							Runat="server"></asp:button>&nbsp;
					</td>
				</tr>
			</table>
			<asp:table id="tblMoveContact" Width="100%" Runat="server" BorderColor="black" GridLines="None" CssClass="aspTable"
				BorderWidth="1" CellPadding="2" CellSpacing="0" Height="170">
				<asp:tablerow>
				<asp:TableCell VerticalAlign="Top" >
				<br />
				        <table>
				                <tr>
				                    <td class="normal1" align="right" >Organization
				                     
				                    </td>
				                    <td>
				                      <asp:TextBox ID="txtCompany" Runat="server" Width="110px" cssclass="signup"></asp:TextBox>&nbsp;
				                     <asp:Button ID="btnSearchOrgByKeywords" Runat="Server" Text="Go" Width="30" CssClass="button" CausesValidation="false"></asp:Button>&nbsp;
				                         <asp:DropDownList ID="ddlCompany" Runat="server" Width="200" AutoPostBack="True" CssClass="signup"></asp:DropDownList>
				                    </td>
				                </tr>
				        </table>
				</asp:TableCell>
			</asp:tablerow>
			</asp:table>
			<asp:requiredfieldvalidator id="rqValCompany" Runat="server" ErrorMessage="Please select a Organization." Display="None"
				EnableClientScript="True" ControlToValidate="ddlCompany"></asp:requiredfieldvalidator>
			<asp:validationsummary id="ValidationSummary" runat="server" DisplayMode="List" ShowMessageBox="True" ShowSummary="False"
				HeaderText="Please check the following value(s)"></asp:validationsummary>
                <asp:literal id="litMessage" Runat="server" EnableViewState="False"></asp:literal>
                <asp:literal id="litButtonMesg" Runat="server" EnableViewState="False"></asp:literal>
		</form>
	</body>
</HTML>
