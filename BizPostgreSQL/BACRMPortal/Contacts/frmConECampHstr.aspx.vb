Imports BACRM.BusinessLogic.Marketing
Partial Class frmConECampHstr
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            Dim objCamapign As New Campaign
            Dim dtECampHstr As New DataTable
            objCamapign.ContactId = GetQueryStringVal(Request.QueryString("enc"), "ContID")
            dtECampHstr = objCamapign.ConECampaignHstr
            dgECampHstr.DataSource = dtECampHstr
            dgECampHstr.DataBind()
        End If
    End Sub

    Function ReturnName(ByVal CloseDate As Date) As String
        Dim strTargetResolveDate As String = ""
        strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
        Return strTargetResolveDate
    End Function

End Class
