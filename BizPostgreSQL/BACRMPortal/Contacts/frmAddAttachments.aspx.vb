Imports BACRM.BusinessLogic.Documents
Imports System.IO
Partial Public Class frmAddAttachments
    Inherits System.Web.UI.Page
#Region "Variables"
    Dim strDocName As String                                        'To Store the information where the File is stored.
    Dim strFileType As String
    Dim strFileName As String
    Dim URLType As String
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            LoadDocuments()
            bindGrid()
        End If
        btnUpload.Attributes.Add("onclick", "return Upload()")
        btnClose.Attributes.Add("onclick", "return Close()")
    End Sub

    Sub LoadDocuments()
        Dim objDocuments As New DocumentList
        Dim dtDocuments As New DataTable
        dtDocuments = objDocuments.GetGenericDocList
        ddlDocuments.DataSource = dtDocuments
        ddlDocuments.DataTextField = "vcdocname"
        ddlDocuments.DataValueField = "numGenericDocid"
        ddlDocuments.DataBind()
        ddlDocuments.Items.Insert(0, "--Select One--")
        ddlDocuments.Items.FindByText("--Select One--").Value = 0
    End Sub


    Sub UploadFile()
        Dim strFName As String()
        Dim strFilePath As String
        Try
            If (fileupload.PostedFile.ContentLength > 0) Then
                strFileName = Path.GetFileName(fileupload.PostedFile.FileName)      'Getting the File Name
                If Directory.Exists(Server.MapPath("../documents/docs")) = False Then ' If Folder Does not exists create New Folder.
                    Directory.CreateDirectory(Server.MapPath("../documents/docs"))
                End If
                strFilePath = Server.MapPath("../documents/docs") & "\" & strFileName
                strFName = Split(strFileName, ".")
                strFileType = strFName(strFName.Length - 1)                     'Getting the Extension of the File
                strDocName = strFileName                  'Getting the Name of the File without Extension
                fileupload.PostedFile.SaveAs(strFilePath)
                strFileName = "../documents/docs/" & strFileName
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub bindGrid()
        Dim dtTable As New DataTable
        Dim i As Integer
        If Not Session("Attachements") Is Nothing Then
            dtTable = Session("Attachements")
            dgAttachments.DataSource = dtTable
            dgAttachments.DataBind()
        End If
        txtAttachements.Text = ""
        For i = 0 To dtTable.Rows.Count - 1
            txtAttachements.Text = txtAttachements.Text + dtTable.Rows(i).Item("Filename") + ","
        Next
        txtAttachements.Text = txtAttachements.Text.TrimEnd(",")
    End Sub

    Private Sub btnUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        Dim dtTable As New DataTable
        Dim dr As DataRow
        If ddlDocuments.SelectedItem.Value > 0 Then
            Dim dtDocDetails As New DataTable
            Dim objDocuments As New DocumentList
            With objDocuments
                .GenDocID = ddlDocuments.SelectedItem.Value
                objDocuments.DomainID = Session("DomainID")
                dtDocDetails = .GetDocByGenDocID
            End With
            If dtDocDetails.Rows(0).Item("numDocCategory") = 370 Then
                strFileName = Replace(dtDocDetails.Rows(0).Item("VcFileName"), "..", Session("SiteType") & "//" & Request.ServerVariables("SERVER_NAME") & "/" & ConfigurationManager.AppSettings("PortalVirtualDirectoryName"))
            Else
                strFileName = dtDocDetails.Rows(0).Item("VcFileName")
            End If
            strDocName = Replace(dtDocDetails.Rows(0).Item("VcFileName"), "../documents/docs/", "")
        Else
            UploadFile()
        End If
        If Session("Attachements") Is Nothing Then
            dtTable.Columns.Add("Filename")
            dtTable.Columns.Add("FileLocation")
        Else
            dtTable = Session("Attachements")
        End If
        dr = dtTable.NewRow
        dr("Filename") = strDocName
        dr("FileLocation") = strFileName
        dtTable.Rows.Add(dr)
        Session("Attachements") = dtTable
        bindGrid()
        ddlDocuments.SelectedIndex = 0
    End Sub


    Private Sub dgAttachments_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgAttachments.ItemCommand
        If e.CommandName = "Delete" Then
            Dim dtTable As New DataTable
            Dim intcnt As Integer
            Dim myRow As DataRow
            dtTable = Session("Attachements")
            For Each myRow In dtTable.Rows
                If intcnt = e.Item.ItemIndex Then
                    dtTable.Rows.Remove(myRow)
                    Exit For
                End If
                intcnt = intcnt + 1
            Next
            Session("Attachements") = dtTable
            bindGrid()
        End If

    End Sub

End Class