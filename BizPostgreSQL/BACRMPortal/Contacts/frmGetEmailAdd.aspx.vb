'Created by Siva
Imports BACRM.BusinessLogic.Contacts
Partial Public Class frmGetEmailAdd
    Inherits System.Web.UI.Page

#Region "Variables"
    Dim m_aryRightsForPage() As Integer
    Dim strColumn As String
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        m_aryRightsForPage = clsAuthorization.fn_ExternalPageLevelRights("frmContactList.aspx", Session("userID"), 11, 2)
        If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
            Response.Redirect("../admin/authentication.aspx?mesg=AC")
        End If
        If Not IsPostBack Then
            Session("Asc") = 1
            txtCurrrentPage.Text = 1
            BindDatagrid()
        End If
        btnTo.Attributes.Add("onclick", "return GetEmailaddress(1)")
        btnCc.Attributes.Add("onclick", "return GetEmailaddress(2)")
        btnBcc.Attributes.Add("onclick", "return GetEmailaddress(3)")
        btnClose.Attributes.Add("onclick", "return Close()")
    End Sub

    Sub BindDatagrid()
        Dim dtContacts As New DataTable
        Dim objContacts As New CContacts
        With objContacts
            .UserCntID = session("ConID")
            .UserRightType = m_aryRightsForPage(RIGHTSTYPE.VIEW)
            .DomainID = Session("DomainID")
            .KeyWord = txtFind.Text
            If txtCurrrentPage.Text.Trim <> "" Then
                .CurrentPage = txtCurrrentPage.Text
            Else
                .CurrentPage = 1
            End If
            .PageSize = Session("PagingRows")
            .TotalRecords = 0
            If ViewState("Column") <> "" Then
                .columnName = ViewState("Column")
            Else
                .columnName = "ADC.bintcreateddate"
            End If
            If Session("Asc") = 1 Then
                .columnSortOrder = "Desc"
            Else
                .columnSortOrder = "Asc"
            End If
        End With
        dtContacts = objContacts.GetContactEmailList
        If objContacts.TotalRecords = 0 Then
            lblRecordCount.Text = 0
        Else
            lblRecordCount.Text = String.Format("{0:#,###}", objContacts.TotalRecords)
            Dim strTotalPage As String()
            Dim decTotalPage As Decimal
            decTotalPage = lblRecordCount.Text / Session("PagingRows")
            decTotalPage = Math.Round(decTotalPage, 2)
            strTotalPage = CStr(decTotalPage).Split(".")
            If (lblRecordCount.Text Mod Session("PagingRows")) = 0 Then
                lblTotal.Text = strTotalPage(0)
                txtTotalPage.Text = strTotalPage(0)
            Else
                lblTotal.Text = strTotalPage(0) + 1
                txtTotalPage.Text = strTotalPage(0) + 1
            End If
            txtTotalRecords.Text = lblRecordCount.Text
        End If
        dgContacts.DataSource = dtContacts
        dgContacts.DataBind()
    End Sub

    Private Sub lnkLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkLast.Click
        txtCurrrentPage.Text = txtTotalPage.Text
        BindDatagrid()
    End Sub

    Private Sub lnkPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkPrevious.Click
        If txtCurrrentPage.Text = 1 Then
            Exit Sub
        Else
            txtCurrrentPage.Text = txtCurrrentPage.Text - 1
        End If
        BindDatagrid()
    End Sub

    Private Sub lnkFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkFirst.Click
        txtCurrrentPage.Text = 1
        BindDatagrid()
    End Sub

    Private Sub lnkNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkNext.Click
        If txtCurrrentPage.Text = txtTotalPage.Text Then
            Exit Sub
        Else
            txtCurrrentPage.Text = txtCurrrentPage.Text + 1
        End If
        BindDatagrid()
    End Sub

    Private Sub lnk2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk2.Click
        If txtCurrrentPage.Text + 1 = txtTotalPage.Text Or txtCurrrentPage.Text + 1 > txtTotalPage.Text Then
            Exit Sub
        Else
            txtCurrrentPage.Text = txtCurrrentPage.Text + 2
        End If
        BindDatagrid()
    End Sub

    Private Sub lnk3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk3.Click
        If txtCurrrentPage.Text + 2 = txtTotalPage.Text Or txtCurrrentPage.Text + 2 > txtTotalPage.Text Then
            Exit Sub
        Else
            txtCurrrentPage.Text = txtCurrrentPage.Text + 3
        End If
        BindDatagrid()
    End Sub

    Private Sub lnk4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk4.Click
        If txtCurrrentPage.Text + 3 = txtTotalPage.Text Or txtCurrrentPage.Text + 3 > txtTotalPage.Text Then
            Exit Sub
        Else
            txtCurrrentPage.Text = txtCurrrentPage.Text + 4
        End If
        BindDatagrid()
    End Sub

    Private Sub lnk5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk5.Click
        If txtCurrrentPage.Text + 4 = txtTotalPage.Text Or txtCurrrentPage.Text + 4 > txtTotalPage.Text Then
            Exit Sub
        Else
            txtCurrrentPage.Text = txtCurrrentPage.Text + 5
        End If
        BindDatagrid()
    End Sub

    Private Sub txtCurrrentPage_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCurrrentPage.TextChanged
        BindDatagrid()
    End Sub

    Private Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFind.Click
        txtCurrrentPage.Text = 1
        BindDatagrid()
    End Sub

    Private Sub dgContacts_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgContacts.SortCommand
        strColumn = e.SortExpression.ToString()
        If ViewState("Column") <> strColumn Then
            ViewState("Column") = strColumn
            Session("Asc") = 0
        Else
            If Session("Asc") = 0 Then
                Session("Asc") = 1
            Else
                Session("Asc") = 0
            End If
        End If
        BindDatagrid()
    End Sub

    Private Sub dgContacts_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgContacts.ItemDataBound
        If e.Item.ItemType = ListItemType.Header Then
            Dim chk As CheckBox
            chk = e.Item.FindControl("chkHEmail")
            chk.Attributes.Add("onclick", "return Selectall()")
        End If
    End Sub

End Class