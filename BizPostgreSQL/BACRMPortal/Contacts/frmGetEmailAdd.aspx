<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmGetEmailAdd.aspx.vb" Inherits="BACRMPortal.frmGetEmailAdd" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Email Address</title>
		<script language="javascript">
		function Close()
		{
			window.close()
			return false;
		}
		function FocusBox()
		{
		 document.Form1.txtFind.focus();
		}
		function GetEmailaddress(a)
		{
		   var str;
		   str=''
		   for(var i=2; i<=dgContacts.rows.length; i++)
			{
				if (document.Form1.all['dgContacts__ctl'+i+'_chkEmail'].checked==true)
				{
				  str=str+document.Form1.all['dgContacts__ctl'+i+'_lblEmail'].innerText+";"
				}
			}
			window.opener.InsertEmail(str.substring(0,str.length-1),a)
			
		}
		function Selectall()
		{
		
		   for(var i=2; i<=dgContacts.rows.length; i++)
			{
				document.Form1.all['dgContacts__ctl'+i+'_chkEmail'].checked=true
				
			}
			return false;
		}
		</script>
	</HEAD>
	<body onload="FocusBox()">
		<form id="Form1" method="post" runat="server">
			<table width="100%">
				<tr>
					<td align="right">
						<asp:TextBox ID="txtFind" Runat="server" CssClass="signup"></asp:TextBox>
					</td>
					<td>
						<asp:Button id="btnFind" Runat="server" Text="Find" Width="70" CssClass="Credit"></asp:Button>
					</td>
					<td>
						<asp:Button ID="btnTo" CssClass="Credit" Runat="server" Text="To >>" Width="100"></asp:Button>
					</td>
					<td>
						<asp:Button ID="btnCc" CssClass="Credit" Runat="server" Text="Cc >>" Width="100"></asp:Button>
					</td>
					<td>
						<asp:Button ID="btnBcc" CssClass="Credit" Runat="server" Text="Bcc >>" Width="100"></asp:Button>
					</td>
					<td>
						<asp:Button ID="btnClose" CssClass="Credit" Runat="server" Text="Close" Width="100"></asp:Button>
					</td>
				</tr>
			</table>
			<table width="100%">
				<tr>
					<TD class="normal1" align="center">No of Records:
						<asp:label id="lblRecordCount" runat="server"></asp:label></TD>
					<td align="right">
						<table>
							<tr>
								<td><asp:label id="lblNext" runat="server" cssclass="Text_bold">Next:</asp:label></td>
								<td class="normal1"><asp:linkbutton id="lnk2" runat="server">2</asp:linkbutton></td>
								<td class="normal1"><asp:linkbutton id="lnk3" runat="server">3</asp:linkbutton></td>
								<td class="normal1"><asp:linkbutton id="lnk4" runat="server">4</asp:linkbutton></td>
								<td class="normal1"><asp:linkbutton id="lnk5" runat="server">5</asp:linkbutton></td>
								<td><asp:linkbutton id="lnkFirst" runat="server"><div class="LinkArrow">9</div>
									</asp:linkbutton></td>
								<td><asp:linkbutton id="lnkPrevious" runat="server"><div class="LinkArrow">3</div>
									</asp:linkbutton></td>
								<td class="normal1"><asp:label id="lblPage" runat="server">Page</asp:label></td>
								<td><asp:textbox id="txtCurrrentPage" runat="server" Width="28px" CssClass="signup" AutoPostBack="True"
										MaxLength="5"></asp:textbox></td>
								<td class="normal1"><asp:label id="lblOf" runat="server">of</asp:label></td>
								<td class="normal1"><asp:label id="lblTotal" runat="server"></asp:label></td>
								<td><asp:linkbutton id="lnkNext" runat="server" CssClass="LinkArrow"><div class="LinkArrow">4</div>
									</asp:linkbutton></td>
								<td><asp:linkbutton id="lnkLast" runat="server"><div class="LinkArrow">:</div>
									</asp:linkbutton></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<asp:datagrid id="dgContacts" AllowSorting="True" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
							BorderColor="white">
							<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							<ItemStyle CssClass="is"></ItemStyle>
							<HeaderStyle CssClass="hs"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="vcFirstName" SortExpression="vcFirstName" HeaderText="<font color=white>First Name</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="vcLastName" SortExpression="vcLastName" HeaderText="<font color=white>Last name</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="vcEmail" SortExpression="vcEmail" HeaderText="<font color=white>Email Address</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="Company" SortExpression="Company" HeaderText="<font color=white>Company</font>"></asp:BoundColumn>
								<asp:TemplateColumn>
									<HeaderTemplate>
										<asp:CheckBox ID="chkHEmail" Runat="server"></asp:CheckBox>
									</HeaderTemplate>
									<ItemTemplate>
										<asp:Label ID=lblEmail Runat=server style="display:none" Text ='<%# DataBinder.Eval(Container.DataItem, "vcEmail") %>'>
										</asp:Label>
										<asp:CheckBox ID="chkEmail" Runat="server"></asp:CheckBox>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid>
					</td>
				</tr>
			</table>
			<asp:TextBox ID="txtTotalPage" Runat="server" style="DISPLAY:none"></asp:TextBox>
			<asp:TextBox ID="txtTotalRecords" Runat="server" style="DISPLAY:none"></asp:TextBox>
			<asp:TextBox ID="txtSortChar" Runat="server" style="DISPLAY:none"></asp:TextBox>
		</form>
	</body>
</HTML>
