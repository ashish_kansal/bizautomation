<%@ Register TagPrefix="menu1" TagName="webmenu" src="../common/topbar.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmContacts.aspx.vb" Inherits="BACRMPortal.frmContacts"%>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" src="../common/calandar.ascx" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebTab.v7.3, Version=7.3.20073.38, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebTab" TagPrefix="igtab" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Contacts</title>   
		<script language="javascript" type="text/javascript">
		 function openActionItem(a,b,c,d)
		{
		window.location.href="../ActionItems/frmActionItemDtl.aspx?frm=contactdetails&CommId=" +a +"&CaseId=" +b+ "&CaseTimeId="+c+ "&CaseExpId=" +d;
		return false;
		}
		function  OpenEmailMessage(a,b)
		{
			window.open("../contact/frmEmailMessage.aspx?Email="+a+"&Date="+b,'','width=750,height=525,status=no,titlebar=no,scrollbars=yes,top=110,left=250')
			return false;
		}
		function FillAddress(a)
		{
		 document.Form1.all['lblStreet'].innerText=a;
		 document.Form1.all['lblCity'].innerText="";
		 document.Form1.all['lblPostal'].innerText="";
		 document.Form1.all['lblState'].innerText="";Parent :
		 document.Form1.all['lblCountry'].innerText="";
		 return false;
		}
		function openAddress(CntID)
		{
			window.open("../contacts/frmContactAddress.aspx?ContID="+CntID,'','toolbar=no,titlebar=no,top=300,width=850,height=350,scrollbars=no,resizable=no')
			return false;
		}
		function ShowWindow(Page,q,att) 
		{
			if (att=='show')
			{
				document.all[Page].style.visibility = "visible";
				return false;
		
			}
			if (att=='hide')
			{
				document.all[Page].style.visibility = "hidden";
				//window.location.reload(true);
				return false;
		
			}
			
		}
		function fn_SendMail(txtMailAddr)
		{ 
			if(txtMailAddr!='')
				{
				window.open('../common/callemail.aspx?LsEmail=' + txtMailAddr,'mail');
				}
				return false;
		}
		function OpenTo()
		{
			window.open("../Contacts/frmToCompanyAssociation.aspx",'','toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
			return false;
		}
		function OpenFrom()
		{
			window.open("../Contacts/frmFromCompanyAssociation.aspx",'','toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
			return false;
		}
		
			function OpenLast10OpenAct(a)
		{
			window.open("../admin/DisplayActionItem.aspx?cntid=" + a +"&type=1",'','toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
			return false;
		}
		function OpenLst10ClosedAct(a)
		{
			window.open("../admin/DisplayActionItem.aspx?cntid=" + a +"&type=2",'','toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
			return false;
		}
		function Disableddl()
		{
			if (Tabstrip2.selectedIndex==0)
			{
				document.Form1.ddlOppStatus.style.display="none";
			}
			else
			{
				document.Form1.ddlOppStatus.style.display="";
			} 
		}
		function Save()
		{
		  if (document.Form1.uwOppTab$_ctl0$txtFirstname.value=="")
		  {
			alert("Enter First Name")
			document.Form1.uwOppTab$_ctl0$txtFirstname.focus();
			return false;
		  }
		   if (document.Form1.uwOppTab$_ctl0$txtLastName.value=="")
		  {
			alert("Enter Last Name")
			document.Form1.uwOppTab$_ctl0$txtLastName.focus();
			return false;
		  }
		  if (document.Form1.txtContactType.value==70)
		  {
			if (document.Form1.uwOppTab$_ctl0$ddlType.value!=70)
			{
			alert("You can't change the contact type 'Primary Contact' from this record, because there must be  one Primary Contact for every Organization record. Go to another contact record within the same Organization, and change its 'type' value to Primary Contact.")
			document.Form1.uwOppTab$_ctl0$ddlType.focus();
			return false;
			}
		  }
		}
		</script>
	</head>
	<body >
		
		<form id="Form1" method="post" runat="server">
		<menu1:webmenu id="webmenu1" runat="server"></menu1:webmenu>
            <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
            </asp:ScriptManager>
            <asp:updatepanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" >
            <ContentTemplate>
			<table width="100%">
				<tr>
					<td>
						<table borderColor="black" cellspacing="0" cellpadding="0" width="100%" border="0">
							<tr>
								<td class="tr1" align="center"><b>Record Owner : </b>
									<asp:label id="lblRecordOwner" runat="server" ForeColor="Black"></asp:label></td>
								<td class="td1" width="1" height="18"></td>
								<td class="tr1" align="center"><b>Created By : </b>
									<asp:label id="lblCreatedBy" runat="server" ForeColor="Black"></asp:label></td>
								<td class="td1" width="1" height="18"></td>
								<td class="tr1" align="center"><b>Last Modified By : </b>
									<asp:label id="lblModifiedBy" runat="server" ForeColor="Black"></asp:label></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br>
			<table cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td>
										
								</td>
								<td class="normal1" align="center">Parent : <u>
										<asp:label id="lblCustomer" Runat="server" CssClass="hyperlink" onClick="ShowWindow('Layer3','','show')"></asp:label></u></td>
								<TD align="right">
									<asp:button id="btnSave" Runat="server" CssClass="button" Text="Save"></asp:button>
									<asp:button id="btnSaveClose" Runat="server" CssClass="button" text="Save &amp; Close"></asp:button>
									<asp:button id="btnCancel" Runat="server" CssClass="button" Text="Cancel"></asp:button>
								</TD>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
				<igtab:ultrawebtab   ImageDirectory="" style="POSITION:absolute;z-index:101;" id="uwOppTab" runat="server" ThreeDEffect="True" BorderStyle="Solid" Width="100%" BarHeight="0"  BorderWidth="0">
                        <DefaultTabStyle Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Arial" >
                        </DefaultTabStyle>
                        <RoundedImage LeftSideWidth="7" RightSideWidth="8" ShiftOfImages="0" SelectedImage="../images/ig_tab_winXPs3.gif" NormalImage="../images/ig_tab_winXP3.gif" HoverImage="../images/ig_tab_winXPs3.gif" FillStyle="LeftMergedWithCenter" ></RoundedImage>
                     <SelectedTabStyle Height="23px"  ForeColor="white">
                        </SelectedTabStyle>
                        <HoverTabStyle Height="23px" ForeColor="white"></HoverTabStyle>
                        <Tabs>
                             <igtab:Tab Text="&nbsp;&nbsp;Contact Details&nbsp;&nbsp;" >
                                <ContentTemplate>
						                <asp:table id="tblContacts" Runat="server" BorderWidth="1" Width="100%" GridLines="None" BorderColor="black"
							                Height="300" CssClass="aspTable">
							                <asp:tableRow>
								                <asp:tableCell VerticalAlign="Top">
									                <br>
									                <table width="100%" id="tblMain" runat="server">
										                <tr>
										                     <td rowspan="30" valign="top" >
						                                        <img src="../images/Greenman-32.gif" />
					                                        </td>
											                <TD class="normal1" valign="middle" align="right">
												                First/Last Name<FONT color="red">*</FONT></TD>
											                <TD>
												                <asp:textbox id="txtFirstname" TabIndex=1 CssClass="signup" width="90" runat="server"></asp:textbox>
												                <asp:textbox id="txtLastName" TabIndex=2 CssClass="signup" width="90" runat="server"></asp:textbox>
											                </TD>
											                <TD class="normal1" align="right">Phone/Ext</TD>
											                <TD>
												                <asp:textbox id="txtPhone" TabIndex=10 CssClass="signup" width="135" runat="server"></asp:textbox>
												                <asp:textbox id="txtExtension" width="45" TabIndex=11 CssClass="signup" runat="server"></asp:textbox>
											                </TD>
											                <TD class="normal1" valign="middle" align="right">
												                Position</TD>
											                <TD>
												                <asp:dropdownlist id="ddlPosition" TabIndex=17 CssClass="signup" Runat="server" Width="180"></asp:dropdownlist>
											                </TD>
										                </tr>
										                <TR>
											                <TD class="normal1" align="right">
												                Email</TD>
											                <TD>
												                <asp:textbox id="txtEmail" CssClass="signup" TabIndex=3 width="180" runat="server"></asp:textbox>&nbsp;
											                </TD>
											                <TD class="normal1" align="right">Cell</TD>
											                <TD>
												                <asp:textbox id="txtMobile" runat="server" TabIndex=12 width="180" CssClass="signup"></asp:textbox></TD>
											                <TD class="normal1" align="right">Title</TD>
											                <TD>
												                <asp:textbox id="txtTitle" runat="server" TabIndex=18 width="180" CssClass="signup"></asp:textbox></TD>
										                </TR>
										                <TR>
											                <TD class="normal1" align="right">
												                Alternate Email</TD>
											                <TD>
												                <asp:textbox id="txtAltEmail" CssClass="signup" TabIndex=4 width="180" runat="server"></asp:textbox>&nbsp;
											                </TD>
											                <TD class="normal1" align="right">Home</TD>
											                <TD>
												                <asp:textbox id="txtHome" runat="server" width="180" TabIndex=13 CssClass="signup"></asp:textbox></TD>
											                <TD class="normal1" align="right">Department</TD>
											                <TD>
												                <asp:dropdownlist id="ddlDepartment" width="180" TabIndex=19 Runat="server" cssclass="signup"></asp:dropdownlist></TD>
										                </TR>
										                <TR>
											                <TD class="normal1" align="right">DOB
											                </TD>
											                <td class="normal1">
											                <table cellpadding="0" cellspacing="0">
													                            <tr>
													                                <td>
													                                    <BizCalendar:Calendar ID="cal" runat="server" />
													                                </td>
													                                <td>
													                                    &nbsp;Age
													                                    <asp:TextBox ID="txtAg" Runat="server" CssClass="signup" Width="30" TabIndex="9"></asp:TextBox>
													                                </td>
													                            </tr>
                        													
													                        </table></td>
											                <TD class="normal1" align="right">Fax</TD>
											                <TD>
												                <asp:textbox id="txtFax" runat="server" width="180" TabIndex=14 CssClass="signup"></asp:textbox></TD>
											                <TD class="normal1" align="right">
												                Gender
											                </TD>
											                <TD>
												                <asp:dropdownlist id="ddlSex" CssClass="signup" runat="server" TabIndex=20 width="180">
													                <asp:ListItem Value="0">--Select One--</asp:ListItem>
													                <asp:ListItem Value="M">Male</asp:ListItem>
													                <asp:ListItem Value="F">Female</asp:ListItem>
												                </asp:dropdownlist>
											                </TD>
										                </TR>
										                <TR>
											                <TD class="normal1" align="right">
												                Contact Type
											                </TD>
											                <TD>
												                <asp:dropdownlist id="ddlType" TabIndex=8 CssClass="signup" AutoPostBack="True" runat="server" width="180"></asp:dropdownlist>
											                </TD>
											                <TD class="normal1" align="right">Category</TD>
											                <TD>
												                <asp:dropdownlist id="ddlcategory" width="180" TabIndex=15 Runat="server" cssclass="signup"></asp:dropdownlist></TD>
											                <TD class="normal1" align="right">Opt-out</TD>
											                <TD class="normal1">
												                <asp:CheckBox ID="optout" Runat="server"></asp:CheckBox>&nbsp;&nbsp;&nbsp; 
												                Status:
												                <asp:dropdownlist id="ddlEmpStatus" CssClass="signup" TabIndex=21 runat="server" Width="110"></asp:dropdownlist></TD>
										                </TR>
										                <tr>
											                <td class="normal1" align="right" valign="top">
												                <asp:Label ID="lblManager" Runat="server">Manager</asp:Label></td>
											                <TD valign="top">
												                <asp:DropDownList ID="ddlManagers" TabIndex=9 Runat="server" width="180" CssClass="signup">
													                <asp:ListItem Value="0">--Select One--</asp:ListItem>
												                </asp:DropDownList>
											                </TD>
											                <td class="normal1" align="right" valign="top">Team
											                </td>
											                <td valign="top">
												                <asp:DropDownList id="ddlTeam" TabIndex=16 Runat="server" width="180" CssClass="signup"></asp:DropDownList>
											                </td>
											                <td colspan="2" class="normal1">
											                </td>
										                </tr>
										                <tr>
											                <td class="normal1" align="right">
												                <asp:hyperlink id="hplAddress" Runat="server" onClick="ShowWindow('Layer1','','show')" CssClass="hyperlink">Address</asp:hyperlink>
											                </td>
											                <td colspan="2" class="normal1">
												                <asp:label id="lblStreet" Runat="server"></asp:label>
												                <asp:label id="lblCity" Runat="server"></asp:label>
												                <asp:label id="lblPostal" Runat="server"></asp:label>
												                <asp:label id="lblState" Runat="server"></asp:label>
												                <asp:label id="lblCountry" Runat="server"></asp:label></td>
											                <td class="normal1" colspan="2">
											                </td>
											                <td class="normal1">
												                Associations:&nbsp;&nbsp;&nbsp;&nbsp;
												                <asp:hyperlink id="hplTo" TabIndex=18 Runat="server" CssClass="hyperlink">
							                <font color="#180073">To</font></asp:hyperlink>(<asp:Label runat="server" Text="" ID="lblAssoCountT"></asp:Label>)&nbsp;/&nbsp;
						                <asp:hyperlink id="hplFrom" TabIndex=19 Runat="server" CssClass="hyperlink">
							                <font color="#180073">From</font></asp:hyperlink>(<asp:Label runat="server" Text="" ID="lblAssoCountF"></asp:Label>)
											                </td>
										                </tr>
									                </table>
									                <table width="100%">
										                <TR>
											                <TD class="normal1" align="right">Comments</TD>
											                <TD colSpan="3">
												                <asp:textbox id="txtComments" runat="server" width="500" cssclass="signup" Height="50" MaxLength="250"
													                TextMode="MultiLine"></asp:textbox></TD>
										                </TR>
										                <TR>
											                <TD class="normal1" colspan="6"><b>Assistant's Information</b></TD>
										                </TR>
										                <tr>
											                <TD class="normal1" align="right">First/Last Name</TD>
											                <TD>
												                <asp:textbox id="txtAsstFName" CssClass="signup" width="90" runat="server"></asp:textbox>&nbsp;
												                <asp:textbox id="txtAsstLName" runat="server" width="90" CssClass="signup"></asp:textbox>
											                </TD>
											                <TD class="normal1" align="right">Phone/Ext</TD>
											                <td>
												                <asp:textbox id="txtAsstPhone" CssClass="signup" width="135" runat="server"></asp:textbox>&nbsp;
												                <asp:textbox id="txtAsstExt" CssClass="signup" runat="server" width="45"></asp:textbox></td>
											                <TD class="normal1" align="right">Email</TD>
											                <TD class="normal1">
												                <asp:textbox id="txtAsstEmail" runat="server" width="180" CssClass="signup"></asp:textbox>&nbsp;
											                </TD>
										                </tr>
									                </table>
									                <br>
								                </asp:tableCell>
							                </asp:tableRow>
						                </asp:table>
							</ContentTemplate>
						</igtab:Tab>
							  </Tabs>
							 </igtab:ultrawebtab>
					</td>
				</tr>
			</table>
			
				<asp:textbox id="hidEml" style="DISPLAY: none" Runat="server"></asp:textbox>
			    <asp:textbox id="hidCompName" style="DISPLAY: none" Runat="server"></asp:textbox>
			    <asp:textbox id="txtContactType" style="DISPLAY: none" Runat="server"></asp:textbox>
			</ContentTemplate>
		</asp:updatepanel>
			
		</form>
	</body>
</HTML>
