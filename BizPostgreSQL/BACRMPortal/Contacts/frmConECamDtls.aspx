<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmConECamDtls.aspx.vb" Inherits="BACRMPortal.frmConECamDtls" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" src="../common/calandar.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>E-Campaign Status</title>
		<script language="JavaScript" src="../include/DATE-PICKER.JS" type="text/javascript"></script>    
		<script language=javascript >
		function Close()
		{
			window.close();
			return false;
		}
		function Save()
		{
		    if (document.Form1.txtStartDate.value=="")
		    {
		        alert("Enter Start Date")
		        document.Form1.txtStartDate.focus()
		        return false;
		    }
		    if (document.Form1.ddlEmailCampaign.value==0)
		    {
		        alert("Select Email Campaign")
		        document.Form1.ddlEmailCampaign.focus()
		        return false;
		    }
		}
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
		<asp:ScriptManager ID="sc" runat="server" EnablePartialRendering="true"></asp:ScriptManager>
			<br>
			<table width="100%">
				<tr>
					<td align="right">
					<asp:button id="btnAssign" Width="150" Runat="server" CssClass="button" Text="Assign a New E-Camapign"></asp:button>
					<asp:button id="btnSave" Runat="server" CssClass="button" Text="Save"></asp:button>
					<asp:button id="btnSaveClose" Runat="server" CssClass="button" Text="Save &amp; Close"></asp:button>
					<asp:button id="btnClose" Runat="server" CssClass="button" Text="Close"></asp:button>
				</td>
				</tr>
			</table>
			<asp:table id="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" Width="100%" BorderColor="black" CssClass="aspTable"
				GridLines="None" Runat="server" Height=200>
				<asp:TableRow>
					<asp:TableCell VerticalAlign=Top >
						<table width="100%">
							<tr>
								<td class="text_bold" align="right">
									Email Campaign Status :
								</td>
								<td class="normal1">
									Engage
									<asp:RadioButton  ID="radEnagaged" GroupName="rad" Runat="server"></asp:RadioButton>
									Disengage
									<asp:RadioButton ID="radDisengage" GroupName="rad" Runat="server" Checked="True"></asp:RadioButton>
								</td>
							</tr>
							<tr>
								<td class="normal1" align="right">Start date
								</td>
								<td>
									<BizCalendar:Calendar ID="cal" runat="server" />
								</td>
							</tr>
							<tr>
								<td class="normal1" align="right">Email Campaign
								</td>
								<td>
									<asp:DropDownList ID="ddlEmailCampaign" AutoPostBack=True  Width=300 Runat="server" CssClass="signup"></asp:DropDownList>
								</td>
							</tr>
							<tr>
								<td class="normal1" align="right">Description
								</td>
								<td class="normal1">
									<asp:Label ID="lblDescription" Runat="server"></asp:Label>
								</td>
							</tr>
						</table>
					</asp:TableCell>
				</asp:TableRow>
			</asp:table><asp:textbox id="txtConCampaignID" style="DISPLAY: none" Runat="server"></asp:textbox></form>
	</body>
</HTML>
