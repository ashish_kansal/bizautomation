<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmcomAssociationTo.aspx.vb" Inherits="BACRMPortal.frmcomAssociationTo" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Organization Association To</title>
		<script language="javascript" src="../javascript/CompanyAssociations.js"></script>
	</HEAD>
	<body>
		<form id="frmComAssociationTo" action="" method="post" runat="server">
			<asp:requiredfieldvalidator id="rqValCompany1" style="Z-INDEX: 102; LEFT: 384px; POSITION: absolute; TOP: 96px"
				Runat="server" ControlToValidate="ddlCompany" EnableClientScript="True" Display="None" ErrorMessage="Please select a Organization."
				InitialValue="0"></asp:requiredfieldvalidator>
				<br />
			<table cellSpacing="0" cellPadding="0" width="100%" style="PADDING-TOP: 2px">
				<tr>
					<td style="WIDTH: 258px" vAlign="bottom" width="258">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;<asp:label id="lblScreenHeader" Runat="server"></asp:label>&nbsp;</td>
							</tr>
						</table>
					</td>
					<td class="normal1" align="right">
						<asp:button id="btnMergeContacts" CssClass="button" Width="200" Text="Merge Contact(s) into another Division"
							Runat="server"></asp:button>&nbsp;
						<asp:button id="btnCopyContacts" CssClass="button" Width="195" Text="Copy Contact(s) into another Division"
							Runat="server"></asp:button>&nbsp;
						<asp:button id="btnAdd" CssClass="button" Width="50" Text="Add" Runat="server"></asp:button>&nbsp;
						<asp:button id="btnClose" CausesValidation="false" CssClass="button" Width="50" Text="Close"
							Runat="server"></asp:button>&nbsp;
					</td>
				</tr>
			</table>
			<asp:table id="tblAssocationToHeader" Width="100%" Runat="server" BorderColor="black" GridLines="None" CssClass="aspTable"
				BorderWidth="1" CellPadding="2" CellSpacing="0" Height="400">
				<asp:tablerow>
				<asp:TableCell VerticalAlign="Top" >
				<br />
					<table width="100%">
			        <tr>
			            <td class="normal1" align="right" >
			            Organization
			            </td>
			            <td>
			                    <asp:TextBox ID="txtCompany" Runat="server" Width="130" cssclass="signup"></asp:TextBox>
						        &nbsp;
                                <asp:Button ID="btnSearchOrgByKeywords" Runat="Server" Text="Go" CssClass="button" CausesValidation="false" Width="20"></asp:Button>&nbsp;
						        <asp:DropDownList ID="ddlCompany" Runat="server" Width="200" CssClass="signup"></asp:DropDownList>
			            </td>
			           
			        </tr>
			        <tr>
			        <td></td>
			            <td class="normal1">
			                <asp:CheckBox id="cbParentOrgIndicator" runat="server"></asp:CheckBox>This is the Parent Organization.&nbsp;&nbsp;&nbsp;&nbsp;
                            
			            </td>
			          <td></td>
			           <td class="normal1">
			                <asp:CheckBox id="cbChildOrgIndicator" runat="server"></asp:CheckBox>This is the Child Organization.&nbsp;&nbsp;
			            </td>
			            </tr>
			            <tr>
			            <td>
			            
			            </td>
			            <td class="normal1">
			            <asp:CheckBox id="chkShare" runat="server"></asp:CheckBox>Share Organization Via Partnet Point?&nbsp;&nbsp;
			            </td>
			            <td class="normal1" align="right" >
			                Association Type
			            </td>
			            <td>
			                <asp:DropDownList ID="ddlType" Width="180" Runat="server" CssClass="signup"></asp:DropDownList>
			            </td>
			        </tr>
			       			</table>
			                <asp:datagrid id="dgAssociation" runat="server" CssClass="dg" Width="100%" AllowSorting="True"
				                AutoGenerateColumns="False" BorderColor="white" CellPadding="0" CellSpacing="0">
				                <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
				                <ItemStyle CssClass="is"></ItemStyle>
				                <HeaderStyle CssClass="hs"></HeaderStyle>
				                <Columns>
					                <asp:BoundColumn DataField="numAssociationID" Visible="false" HeaderText="Was Associated From"></asp:BoundColumn>
					                <asp:BoundColumn DataField="Company" HeaderText="Associated From (Source)"></asp:BoundColumn>
					                <asp:BoundColumn DataField="TargetCompany" HeaderText="Associated To (Target)"></asp:BoundColumn>
					                <asp:BoundColumn DataField="bitParentOrg" HeaderText="Parent/ Child Org. ?"></asp:BoundColumn>
					                <asp:BoundColumn DataField="vcData" HeaderText="As Its"></asp:BoundColumn>
					                <asp:BoundColumn DataField="SharePortal" HeaderText="Share Organization <br/>Via Partnet Point"></asp:BoundColumn>
					                <asp:TemplateColumn>
						                <HeaderTemplate>
							                <asp:Button ID="btnHdelete" Runat="server" CssClass="Delete" Text="r"></asp:Button>
						                </HeaderTemplate>
						                <ItemTemplate>
							                <asp:Button ID="btnDelete" Runat="server" CssClass="Delete" Text="r" CommandName="Delete"></asp:Button>
						                </ItemTemplate>
					                </asp:TemplateColumn>
				                </Columns>
			                </asp:datagrid>
			  

			</asp:TableCell>
				</asp:tablerow>
			</asp:table>
			
			<asp:requiredfieldvalidator id="rqValCompany" Runat="server" ErrorMessage="Please select a Organization." Display="None"
				EnableClientScript="True" ControlToValidate="ddlCompany"></asp:requiredfieldvalidator>
			<asp:requiredfieldvalidator id="rqValAssociation" Runat="server" ErrorMessage="Please select an Association."
				Display="None" EnableClientScript="True" ControlToValidate="ddlType" InitialValue="0"></asp:requiredfieldvalidator>
			<asp:validationsummary id="ValidationSummary" runat="server" DisplayMode="List" ShowMessageBox="True" ShowSummary="False"
				HeaderText="Please check the following value(s)"></asp:validationsummary>
			
			
			
			<table width="100%">
				<tr>
					<td class="normal4" align="center"><asp:literal id="litMessage" Runat="server" EnableViewState="False"></asp:literal></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
