<%@ Page Language="vb"  MasterPageFile="site.master"  AutoEventWireup="false" CodeBehind="frmConfirmAddress.aspx.vb" Inherits="BACRMPortal.frmConfirmAddress" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CphPage" Runat="Server">
 <table>
    <tr>
		<th nowrap colspan="2">Customer Information:</th>
							    </tr>
								<tr>
									<td nowrap align="right">First Name:</td>
									<td>
										<asp:textbox id="txtCustomerFirstName" CssClass="signup"  runat="server" width="182px"></asp:textbox>
										</td>
										<td nowrap align="right">Last Name:</td>
									<td>
										<asp:textbox id="txtCustomerLastName" CssClass="signup"  runat="server" width="182px"></asp:textbox></td>
								</tr>
								<tr>
									<td nowrap align="right">Phone:</td>
									<td>
										<asp:textbox id="txtCustomerPhone" CssClass="signup"  runat="server" width="182px"></asp:textbox></td>
								
									<td nowrap align="right">Email:</td>
									<td>
										<asp:textbox id="txtCustomerEmail"  CssClass="signup" runat="server" width="182px"></asp:textbox></td>
								</tr>
								<tr>
								   <th nowrap colspan="2">
										Billing Address:
									</th>
								   <th nowrap colspan="2">
										Shipping Address:
									</th>
								</tr>
								<tr>
									<td nowrap align="right">Street:</td>
									<td>
										<asp:textbox id="txtBillStreet" CssClass="signup"  runat="server" width="181px"></asp:textbox></td>
								
									<td nowrap align="right">Street:</td>
									<td>
										<asp:textbox id="txtShipStreet" CssClass="signup"  runat="server" width="181px"></asp:textbox></td>
								</tr>
								<tr>
									<td nowrap align="right">City:</td>
									<td>
										<asp:textbox id="txtBillCity" CssClass="signup"  runat="server" width="181px"></asp:textbox></td>
								
									<td nowrap align="right">City:</td>
									<td>
										<asp:textbox id="txtShipCity" CssClass="signup"  runat="server" width="181px"></asp:textbox></td>
								</tr>
								<tr>
									<td nowrap align="right">Zipcode:</td>
									<td>
										<asp:textbox id="txtBillCode"  CssClass="signup" runat="server" width="182px"></asp:textbox></td>
							
									<td nowrap align="right">Zipcode:</td>
									<td>
										<asp:textbox id="txtShipCode"  CssClass="signup" runat="server" width="182px"></asp:textbox></td>
								</tr>
								<tr>
									<td nowrap align="right">Country:</td>
									<td>
									      <asp:DropDownList runat="server" ID="ddlBillCountry" AutoPostBack="true" CssClass="signup"></asp:DropDownList>		
									</td>
							
									<td nowrap align="right">Country:</td>
									<td>
									      <asp:DropDownList runat="server" ID="ddlShipCountry" AutoPostBack="true" CssClass="signup"></asp:DropDownList>		
									</td>
								</tr>
								<tr>
									<td nowrap align="right">State:</td>
									<td>
										<asp:DropDownList runat="server" ID="ddlBillState" CssClass="signup"></asp:DropDownList></td>		
							
									<td nowrap align="right">State:</td>
									<td>
										<asp:DropDownList runat="server" ID="ddlShipState" CssClass="signup"></asp:DropDownList></td>		
								</tr>
								<tr>
								    <td colspan="4" align="center"  > 
								        <asp:Button ID="btnSubmit" runat="server" Text="Confirm & Save"  />
								    </td>
								</tr>
 </table>
</asp:Content>
