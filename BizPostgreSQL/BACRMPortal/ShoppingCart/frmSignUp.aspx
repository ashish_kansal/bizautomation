<%@ Page Language="vb" EnableEventValidation="false"   AutoEventWireup="false" CodeBehind="frmSignUp.aspx.vb" Inherits="BACRMPortal.frmSignUp" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <LINK href="../CSS/EComm.css" runat="server" id="CSSlink"   type="text/css" rel="stylesheet"/>
		<title>Sign up here</title>
		<script language="javascript" src="../include/GenericOrder.js"></script>
		<script language="javascript" type="text/javascript" >
	        function Close()
					{
							window.close();
					}
		</script>
	</HEAD>
	<body >
		<form id="frmGenericFormOrder" method="post" runat="server">
		<br />
		<table >
		    <tr>
		        <td>
		            <asp:placeholder id="plhFormControls" runat="server"></asp:placeholder>
		        </td>
		    </tr>
		     <tr>
		        <td align="center" >
		            <asp:button id="btnSubmit" Text="Submit" Runat="server" cssclass="button"></asp:button>
		            <asp:button id="btnCancel" CausesValidation="false"  Text="Back" Runat="server" cssclass="button"></asp:button>
		        </td>
		    </tr>
		</table>
	
	
	
			<asp:validationsummary id="ValidationSummary" v runat="server" HeaderText="Please check the following value(s)"
				ShowSummary="False" ShowMessageBox="True" DisplayMode="List"></asp:validationsummary>
			<input id="hdXMLString" type="hidden" name="hdXMLString" runat="server"> <input id="hdGroupId" type="hidden" name="hdGroupId" runat="server"><!---Store the Group ID--->
			<input id="hdEmail" type="hidden" name="hdEmail" runat="server"><!---Store the Email Id--->
			<input id="hdCompanyType" type="hidden" name="hdCompanyType" runat="server"><!---Store the Company Type --->
			<asp:literal id="litClientScript" Runat="server"></asp:literal>
		</form>
	</body>
</html>
