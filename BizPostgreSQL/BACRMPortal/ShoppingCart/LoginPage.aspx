<%@ Page Language="vb"  EnableEventValidation="false"  MasterPageFile="site.master" AutoEventWireup="false" CodeBehind="LoginPage.aspx.vb" Inherits="BACRMPortal.ShoppingCart_LoginPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CphPage" Runat="Server" >
<br />
<br />
	<table id="tblLogin" runat="server"   border="0">
						
							<tr>
								<td class="normal4" colSpan="2"><asp:literal id="litMessage" Runat="server"></asp:literal></td>
							</tr>
							<tr>
								<td class="normal1" align="right">Email Address
								</td>
								<td align="left"><asp:textbox id="txtEmaillAdd" CssClass="signup" Runat="server" Width="250"></asp:textbox></td>
							</tr>
							<tr>
								<td class="normal1" align="right">Password
								</td>
								<td align="left"><asp:textbox id="txtPassword" CssClass="signup" TextMode="Password" Width="250" Runat="server"></asp:textbox></td>
							</tr>
							<tr>
								<td align="center" colSpan="2"><asp:button id="btnLogin" CssClass="button" Runat="server" Text="Login" Width="80"></asp:button></td>
							</tr>
							<tr>
							    <td align="center" colSpan="2">
							        <a href="#" onclick="OpenSignUp()" runat="server" id="lnkNewUsers"  >New Users - Sign up here</a>
							    </td>
							</tr>
						</table>
		&nbsp;
<script language="javascript" type="text/javascript" >
    function OpenSignUp()
		{
			window.open('frmSignUp.aspx','','toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')
			return false;
		}
</script>
</asp:Content>
