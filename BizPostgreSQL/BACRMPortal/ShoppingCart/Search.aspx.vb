Imports BACRM.BusinessLogic.Item
Imports System.IO
Imports BACRM.BusinessLogic.Common
Partial Public Class ShoppingCart_Search
    Inherits System.Web.UI.Page

    Dim imgThump As System.Web.UI.WebControls.Image
    Dim lblFile As Label

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Dim objItems As New CItems
                Dim ds As DataSet
                Dim dt As DataTable
                Dim tblRow As TableRow
                Dim tblCell As TableCell
                Dim tbl As Table
                Dim tblCell1 As TableCell
                Dim tblRow1 As TableRow
                Dim hpl As HyperLink
                Dim lbl As Label
                Dim img As System.Web.UI.WebControls.Image
                If Session("WareHouseID") Is Nothing Then
                    If Not Request.Cookies.Get("CompanyID") Is Nothing Then
                        objItems.DivisionID = Request.Cookies.Get("CompanyID").Value
                        objItems.DomainID = Request.Cookies.Get("id_site").Value
                        Session("WareHouseID") = objItems.GetWarehouseFromDivID
                    Else
                        objItems.DomainID = Request.Cookies.Get("id_site").Value
                        Session("WareHouseID") = objItems.GetWarehouseFromDivID
                    End If
                End If
                objItems.str = GetQueryStringVal(Request.QueryString("enc"), "str")
                objItems.DomainID = Request.Cookies.Get("id_site").Value
                objItems.WarehouseID = Session("WareHouseID")
                ds = objItems.GetCategoriesAndItems
                dt = ds.Tables(0)
                Dim itemColumns, i, j As Integer
                itemColumns = ds.Tables(1).Rows(0).Item(0)
                i = 0
                For j = 0 To dt.Rows.Count - 1
                    If itemColumns = i Or i = 0 Then
                        i = 0
                        tblRow = New TableRow
                    End If
                    tblCell = New TableCell
                    tbl = New Table
                    tblRow1 = New TableRow
                    tblCell1 = New TableCell
                    hpl = New HyperLink
                    hpl.Text = dt.Rows(j).Item("vcItemName")
                    hpl.NavigateUrl = "../ShoppingCart/frmItemDetail.aspx?ItemID=" & dt.Rows(j).Item("numItemCode")
                    hpl.CssClass = "Header"
                    lbl = New Label
                    lbl.Text = " &nbsp;" & dt.Rows(j).Item("InStock")
                    lbl.CssClass = "Stock"
                    tblCell1.Controls.Add(hpl)
                    tblCell1.Controls.Add(lbl)
                    tblCell1.ColumnSpan = 2
                    tblRow1.Cells.Add(tblCell1)
                    tbl.Rows.Add(tblRow1)

                    tblRow1 = New TableRow
                    tblCell1 = New TableCell
                    If File.Exists(Server.MapPath("../Documents/Docs/" & dt.Rows(j).Item("vcPathForTImage"))) = True Then
                        img = New System.Web.UI.WebControls.Image
                        img.ImageUrl = "../Documents/Docs/" & dt.Rows(j).Item("vcPathForTImage")
                        img.DescriptionUrl = "../ShoppingCart/frmItemDetail.aspx?ItemID=" & dt.Rows(j).Item("numItemCode")
                        tblCell1.Controls.Add(img)
                    End If
                    tblRow1.Cells.Add(tblCell1)

                    tblCell1 = New TableCell
                    tblCell1.Text = dt.Rows(j).Item("txtItemDesc")
                    tblRow1.Cells.Add(tblCell1)
                    tbl.Rows.Add(tblRow1)

                    tblRow1 = New TableRow
                    tblCell1 = New TableCell
                    tblCell1.ColumnSpan = 2
                    tblCell1.Text = "<a class='Header' href=../ShoppingCart/frmItemDetail.aspx?ItemID=" & dt.Rows(j).Item("numItemCode") & ">More<img alt='More Detail' src='../images/morearrow.gif'></a>"
                    tblRow1.Cells.Add(tblCell1)
                    tbl.Rows.Add(tblRow1)
                    tblCell.Controls.Add(tbl)
                    tblRow.Cells.Add(tblCell)
                    i = i + 1
                    If i = itemColumns Or j = dt.Rows.Count - 1 Then
                        tblDetails.Rows.Add(tblRow)
                    End If
                Next
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class