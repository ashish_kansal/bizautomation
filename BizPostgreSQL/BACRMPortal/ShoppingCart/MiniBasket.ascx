<%@ Control Language="vb" AutoEventWireup="true" CodeBehind="MiniBasket.ascx.vb"
    Inherits="BACRMPortal.ShoppingCart_MiniBasket" %>
<table>
    <tr>
        <td align="right">
            <a href="LoginPage.aspx" runat="server" id="lnlLogin"><b>Login</b></a>
            <asp:LinkButton ID="lnkLogout" runat="server" Text="<b>Logout</b>"></asp:LinkButton>
        </td>
    </tr>
</table>
<asp:Panel ID="pnlCart" runat="server">
    <div class="textboxheader" style="width: 260px">
        <img src="../images/shopping_cart.gif" align="absmiddle" />&nbsp;Your Cart
    </div>
    <div class="textbox" style="width: 250px">
        <asp:DataGrid ID="dgBasket" runat="server" ShowHeader="false" AutoGenerateColumns="False"
            Width="250px">
            <Columns>
                <asp:BoundColumn DataField="numoppitemtCode" Visible="False"></asp:BoundColumn>
                <asp:BoundColumn HeaderText="Item" DataField="vcItemName"></asp:BoundColumn>
                <asp:BoundColumn HeaderText="Units" DataField="numUnitHour"></asp:BoundColumn>
                <asp:TemplateColumn>
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkDel" runat="server" Text="X" class="Delete" CommandName="Delete"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
        <div class="subtotal">
            <asp:Label runat="server" ID="lblTotal"></asp:Label>
        </div>
        <a href="../ShoppingCart/checkout.aspx" class="checkout">Checkout >>></a>
    </div>
</asp:Panel>
