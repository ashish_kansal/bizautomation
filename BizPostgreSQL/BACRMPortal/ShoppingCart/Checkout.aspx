<%@ Page Language="vb" MasterPageFile="site.master" AutoEventWireup="false" Codebehind="Checkout.aspx.vb"
    Inherits="BACRMPortal.ShoppingCart_Checkout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CphPage" runat="Server">

    <script language="javascript" type="text/javascript">
function CheckNumber()
					{
						
							if (!(window.event.keyCode > 47 && window.event.keyCode < 58))
							{
								window.event.keyCode=0;
							}
					}
			function OpenBizInvoice(a,b)
			{
				window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID='+a + '&OppBizId='+b+'&Show=True','','toolbar=no,menubar=yes,titlebar=no,left=100,width=680,height=700,scrollbars=yes,resizable=yes');
				return false;
			}
    </script>

    <table cellspacing="1" cellpadding="2" width="100%" bgcolor="white">
        <tr>
            <td class="sectionheader">
                Order Details</td>
        </tr>
        <tr>
            <td bgcolor="white">
                <asp:GridView ID="dgBasket" runat="server" AutoGenerateColumns="False" Width="100%">
                    <EmptyDataTemplate>
                        &nbsp;-- No items in your basket --
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:BoundField DataField="numItemCode" Visible="False"></asp:BoundField>
                        <asp:BoundField HeaderText="Item" DataField="vcItemName"></asp:BoundField>
                        <asp:BoundField HeaderText="Units" DataField="numUnitHour"></asp:BoundField>
                        <asp:BoundField HeaderText="Price/Unit" DataField="monPrice" DataFormatString="{0:C}">
                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Total" DataField="monTotAmount" DataFormatString="{0:C}">
                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
    <table align="right">
        <tr>
            <td class="text_bold" align="right">
                Subtotal: &nbsp;
            </td>
            <td align="right">
                <asp:Label ID="lblSubTotal" runat="server" CssClass="text"></asp:Label></td>
        </tr>
        <tr>
            <td class="text_bold" align="right">
                Tax: &nbsp;
            </td>
            <td align="right">
                <asp:Label ID="lblTax" runat="server" CssClass="text"></asp:Label></td>
        </tr>
        <tr>
            <td class="text_bold" align="right">
                Shipping Charges: &nbsp;
            </td>
            <td align="right">
                <asp:Label ID="lblShippingCost" runat="server" CssClass="text"></asp:Label></td>
        </tr>
        <tr>
            <td class="text_bold" align="right">
                Total: &nbsp;
            </td>
            <td align="right">
                <asp:Label ID="lblTotal" runat="server" CssClass="text"></asp:Label></td>
        </tr>
    </table>
    <br />
    <br />
    <br />
    <br />
    <br />
    <table width="500" align="center" border="0" cellpadding="5" cellspacing="5">
        <tr>
            <td>
                <asp:RadioButton ID="radBillMe" runat="server" GroupName="rad" Text='<b>Bill Me</b> &nbsp;(If you or your company has terms with us.)'>
                </asp:RadioButton>
            </td>
            <td class="normal1">
            </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButton ID="radPay" Checked="true" AutoPostBack="true" runat="server" GroupName="rad"
                    Text='<b>Pay by Credit Card</b>'></asp:RadioButton>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table id="Table2" width="90%" class="normal1">
                    <tr>
                        <td width="50%">
                            <table id="Table3" cellspacing="2" cellpadding="5">
                                <tr>
                                    <th nowrap colspan="2">
                                        Customer Information:
                                    </th>
                                </tr>
                                <tr>
                                    <td nowrap align="right">
                                        First Name:</td>
                                    <td>
                                        <asp:Label ID="txtCustomerFirstName" runat="server"></asp:Label>
                                    </td>
                                    <td nowrap align="right">
                                        Last Name:</td>
                                    <td>
                                        <asp:Label ID="txtCustomerLastName" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap align="right">
                                        Phone:</td>
                                    <td>
                                        <asp:Label ID="txtCustomerPhone" runat="server"></asp:Label>
                                    </td>
                                    <td nowrap align="right">
                                        Email:</td>
                                    <td>
                                        <asp:Label ID="txtCustomerEmail" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <th nowrap colspan="2">
                                        Billing Address:
                                    </th>
                                    <th nowrap colspan="2">
                                        Shipping Address:
                                    </th>
                                </tr>
                                <tr>
                                    <td nowrap align="right">
                                        Street:</td>
                                    <td>
                                        <asp:Label ID="txtBillStreet" runat="server"></asp:Label>
                                    </td>
                                    <td nowrap align="right">
                                        Street:</td>
                                    <td>
                                        <asp:Label ID="txtShipStreet" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap align="right">
                                        City:</td>
                                    <td>
                                        <asp:Label ID="txtBillCity" runat="server"></asp:Label>
                                    </td>
                                    <td nowrap align="right">
                                        Street:</td>
                                    <td>
                                        <asp:Label ID="txtShipCity" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap align="right">
                                        Zipcode:</td>
                                    <td>
                                        <asp:Label ID="txtBillCode" runat="server"></asp:Label>
                                    </td>
                                    <td nowrap align="right">
                                        Zipcode:</td>
                                    <td>
                                        <asp:Label ID="txtShipCode" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap align="right">
                                        Country:</td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="ddlBillCountry" Enabled="false" AutoPostBack="true"
                                            CssClass="signup">
                                        </asp:DropDownList>
                                    </td>
                                    <td nowrap align="right">
                                        Country:</td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="ddlShipCountry" Enabled="false" AutoPostBack="true"
                                            CssClass="signup">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap align="right">
                                        State:</td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="ddlBillState" Enabled="false" AutoPostBack="true"
                                            CssClass="signup">
                                            <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                        </asp:DropDownList></td>
                                    <td nowrap align="right">
                                        State:</td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="ddlShipState" Enabled="false" AutoPostBack="true"
                                            CssClass="signup">
                                            <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <th nowrap colspan="4">
                                        Credit Card Information
                                    </th>
                                </tr>
                                <tr>
                                    <td nowrap align="right">
                                        Card Number:</td>
                                    <td>
                                        <asp:TextBox ID="txtCardNumber" CssClass="signup" runat="server" Width="180px"></asp:TextBox></td>
                                    <td nowrap align="right">
                                        Card Type:</td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="ddlCardType" CssClass="signup">
                                            <asp:ListItem Value="1">Visa </asp:ListItem>
                                            <asp:ListItem Value="2">MasterCard</asp:ListItem>
                                            <asp:ListItem Value="3">AMEX</asp:ListItem>
                                            <asp:ListItem Value="4">Discover</asp:ListItem>
                                            <asp:ListItem Value="5">Diners </asp:ListItem>
                                            <asp:ListItem Value="6">JCB</asp:ListItem>
                                            <asp:ListItem Value="7">BankCard</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap align="right">
                                        Exp Date (MM/YY):
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlCardExpMonth" CssClass="signup" runat="server">
                                            <asp:ListItem Value="01">01</asp:ListItem>
                                            <asp:ListItem Value="02">02</asp:ListItem>
                                            <asp:ListItem Value="03">03</asp:ListItem>
                                            <asp:ListItem Value="04">04</asp:ListItem>
                                            <asp:ListItem Value="05">05</asp:ListItem>
                                            <asp:ListItem Value="06">06</asp:ListItem>
                                            <asp:ListItem Value="07">07</asp:ListItem>
                                            <asp:ListItem Value="08">08</asp:ListItem>
                                            <asp:ListItem Value="09">09</asp:ListItem>
                                            <asp:ListItem Value="10">10</asp:ListItem>
                                            <asp:ListItem Value="11">11</asp:ListItem>
                                            <asp:ListItem Value="12">12</asp:ListItem>
                                        </asp:DropDownList>&nbsp;/&nbsp;
                                        <asp:DropDownList ID="ddlCardExpYear" CssClass="signup" runat="server">
                                        </asp:DropDownList></td>
                                    <td nowrap align="right" height="31">
                                        CVV2 Data:</td>
                                    <td height="31">
                                        <asp:TextBox ID="txtCardCVV2" CssClass="signup" runat="server" Width="117px"></asp:TextBox></td>
                                </tr>
                            </table>
                            <p align="center">
                                <asp:Button ID="bCharge" runat="server" CssClass="signup" Width="70" Text="Submit"></asp:Button>
                                <asp:Button ID="btnClick" runat="server" Visible="false" CssClass="signup" Text="Click Here To See the Details">
                                </asp:Button>
                            </p>
                            <p align="center">
                                <asp:Label ID="lblWarning" runat="server" ForeColor="Red" Visible="False" Font-Size="XX-Small">Supply a Login/Password</asp:Label></p>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4" style="color: Green">
                            <asp:Literal ID="liMessage" runat="server" EnableViewState="False"> </asp:Literal>
                        </td>
                    </tr>
                </table>
                <strong>
                    <asp:Literal ID="litOutput" runat="server" EnableViewState="False"></asp:Literal></strong>
            </td>
        </tr>
        <tr>
            <td>
                <div runat="server">
                    <asp:Table ID="tblShiipingRates" runat="server">
                        <asp:TableRow>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </td>
        </tr>
    </table>
    <asp:TextBox ID="txtCredit" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtDueAmount" runat="server" Style="display: none"></asp:TextBox>
</asp:Content>
