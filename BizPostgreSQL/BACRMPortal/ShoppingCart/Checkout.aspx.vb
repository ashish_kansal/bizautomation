Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Common
Imports nsoftware.IBizUPS
Imports BACRM.BusinessLogic.Admin

Partial Public Class ShoppingCart_Checkout
    Inherits System.Web.UI.Page
    Dim lngDivID, lngContId, lngOppBizDocID As Long
    Dim decShippingCharge As Decimal = 0



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                If Session("UserContactID") Is Nothing Then
                    Session("redirect") = "True"
                    Response.Redirect("../ShoppingCart/LoginPage.aspx")
                Else
                    Session("redirect") = "False"
                End If
                If Session("AddressConfirm") <> "True" Then
                    Response.Redirect("../ShoppingCart/frmConfirmAddress.aspx")
                End If
                lngDivID = Request.Cookies.Get("CompanyID").Value
                lngContId = Session("UserContactID")
                If Not IsPostBack Then
                    txtCardNumber.Attributes.Add("onkeypress", "return CheckNumber()")
                    Dim objItems As New CItems
                    If Session("OppID") Is Nothing Then
                        objItems.OppId = 0
                        objItems.DivisionID = lngDivID
                        objItems.ContactID = lngContId
                    Else
                        objItems.OppId = Session("OppID")
                    End If

                    If Not Session("Data") Is Nothing Then
                        Dim dttable As DataTable
                        Dim ds As DataSet
                        ds = Session("Data")
                        dttable = ds.Tables(0)
                        For Each dr As DataRow In dttable.Rows
                            objItems.ItemCode = dr("numItemCode")
                            objItems.NoofUnits = dr("numUnitHour")
                            dr("monPrice") = String.Format("{0:#,##0.00}", objItems.GetItemPriceafterdiscount())
                            dr("monTotAmount") = String.Format("{0:#,##0.00}", dr("numUnitHour") * dr("monPrice"))
                        Next
                        ds.AcceptChanges()
                        Session("Data") = ds
                        objItems.str = ds.GetXml
                        dgBasket.DataSource = dttable
                        dgBasket.DataBind()
                        'GetShippingRates()
                    End If
                    objItems.DomainID = Request.Cookies.Get("id_site").Value
                    objItems.OppName = Format(Now(), "MMMM")
                    objItems.AddShoppinItems()
                    Session("OppID") = objItems.OppId



                    Dim objOpportunities As New MOpportunity
                    Dim dtItems As DataTable
                    objOpportunities.OpportunityId = Session("OppID")
                    objOpportunities.DomainID = Request.Cookies.Get("id_site").Value
                    dtItems = objOpportunities.ItemsByOppId.Tables(0)

                    Dim Tax, TotalAmount, SubTotal As Decimal
                    Dim i As Integer
                    'objItems.DivisionID = lngDivID
                    'Tax = objItems.GetTaxPercentage

                    If dtItems.Rows.Count > 0 Then
                        SubTotal = CType(dtItems.Compute("SUM(monTotAmount)", ""), Decimal)
                        For i = 0 To dtItems.Rows.Count - 1
                            If dtItems.Rows(i).Item("Tax") > 0 Then
                                Tax = Tax + (dtItems.Rows(i).Item("Amount") * dtItems.Rows(i).Item("Tax") / 100)
                            End If
                        Next
                    Else

                    End If
                    lblSubTotal.Text = String.Format("{0:#,##0.00}", SubTotal)
                    lblTax.Text = String.Format("{0:#,##0.00}", Tax)
                    lblTotal.Text = String.Format("{0:#,##0.00}", SubTotal + Tax)
                    lblShippingCost.Text = String.Format("{0:#,##0.00}", decShippingCharge)
                    Session("TotalAmount") = String.Format("{0:#,##0.00}", SubTotal + Tax + lblShippingCost.Text)
                    CheckCreditStatus()

                    Dim Years As New ArrayList()
                    For i = 1 To 20
                        Years.Add(DateTime.Now.AddYears(i - 1).Date.ToString("yy"))
                    Next i
                    ddlCardExpYear.DataSource = Years
                    ddlCardExpYear.DataBind()
                    ddlCardExpYear.Items.FindByValue(Now.Year.ToString("yy"))

                    ddlCardExpMonth.SelectedIndex = (DateTime.Now.Month - 1)

                    'ddlGateway.DataSource = System.Enum.GetValues(GetType(nsoftware.IBizPay.IchargeGateways))
                    'ddlGateway.DataBind()
                    ' ddlGateway.Items.Insert(0, "--Select One--")
                    ' ddlGateway.Items(0).Value = "0"
                    ' ddlGateway.SelectedIndex = 1 'set Authorze.Net as the default gateway
                    ' ddlCardType.DataSource = System.Enum.GetValues(GetType(nsoftware.IBizPay.IchargeCardTypes))
                    ' ddlCardType.DataBind()
                    ' ddlCardType.Items.Insert(0, "--Select One--")
                    ' ddlCardType.Items(0).Value = "0"

                    Dim objCommon As New CCommon
                    objCommon.sb_FillComboFromDBwithSel(ddlBillCountry, 40, Session("DomainID"))
                    objCommon.sb_FillComboFromDBwithSel(ddlShipCountry, 40, Session("DomainID"))


                    Dim objContacts As New CContacts
                    Dim dtTable1 As DataTable
                    objContacts.ContactID = Session("UserContactID")
                    objContacts.DomainID = Request.Cookies.Get("id_site").Value
                    dtTable1 = objContacts.GetBillOrgorContAdd
                    txtCustomerFirstName.Text = IIf(IsDBNull(dtTable1.Rows(0).Item("vcFirstname")), "", dtTable1.Rows(0).Item("vcFirstname"))
                    txtCustomerLastName.Text = IIf(IsDBNull(dtTable1.Rows(0).Item("vcLastname")), "", dtTable1.Rows(0).Item("vcLastname"))
                    txtCustomerEmail.Text = IIf(IsDBNull(dtTable1.Rows(0).Item("vcEmail")), "", dtTable1.Rows(0).Item("vcEmail"))
                    txtCustomerPhone.Text = dtTable1.Rows(0).Item("vcPhone")
                    txtBillStreet.Text = IIf(IsDBNull(dtTable1.Rows(0).Item("vcBillstreet")), "", dtTable1.Rows(0).Item("vcBillstreet"))
                    txtShipStreet.Text = IIf(IsDBNull(dtTable1.Rows(0).Item("vcShipStreet")), "", dtTable1.Rows(0).Item("vcShipStreet"))
                    txtBillCity.Text = IIf(IsDBNull(dtTable1.Rows(0).Item("vcBillCity")), "", dtTable1.Rows(0).Item("vcBillCity"))
                    txtShipCity.Text = IIf(IsDBNull(dtTable1.Rows(0).Item("vcShipCity")), "", dtTable1.Rows(0).Item("vcShipCity"))
                    If Not IsDBNull(dtTable1.Rows(0).Item("vcBillCountry")) Then
                        If Not ddlBillCountry.Items.FindByValue(dtTable1.Rows(0).Item("vcBillCountry")) Is Nothing Then
                            ddlBillCountry.Items.FindByValue(dtTable1.Rows(0).Item("vcBillCountry")).Selected = True
                        End If
                    End If
                    If Not IsDBNull(dtTable1.Rows(0).Item("vcShipCountry")) Then
                        If Not ddlShipCountry.Items.FindByValue(dtTable1.Rows(0).Item("vcShipCountry")) Is Nothing Then
                            ddlShipCountry.Items.FindByValue(dtTable1.Rows(0).Item("vcShipCountry")).Selected = True
                        End If
                    End If
                    If ddlBillCountry.SelectedIndex > 0 Then
                        FillState(ddlBillState, ddlBillCountry.SelectedItem.Value, Session("DomainID"))
                        If Not IsDBNull(dtTable1.Rows(0).Item("vcBilState")) Then
                            If Not ddlBillState.Items.FindByValue(dtTable1.Rows(0).Item("vcBilState")) Is Nothing Then
                                ddlBillState.Items.FindByValue(dtTable1.Rows(0).Item("vcBilState")).Selected = True
                            End If
                        End If
                    End If
                    If ddlShipCountry.SelectedIndex > 0 Then
                        FillState(ddlShipState, ddlShipCountry.SelectedItem.Value, Session("DomainID"))
                        If Not IsDBNull(dtTable1.Rows(0).Item("vcShipState")) Then
                            If Not ddlShipState.Items.FindByValue(dtTable1.Rows(0).Item("vcShipState")) Is Nothing Then
                                ddlShipState.Items.FindByValue(dtTable1.Rows(0).Item("vcShipState")).Selected = True
                            End If
                        End If
                    End If
                    txtBillCode.Text = IIf(IsDBNull(dtTable1.Rows(0).Item("vcBillPostCode")), "", dtTable1.Rows(0).Item("vcBillPostCode"))
                    txtShipCode.Text = IIf(IsDBNull(dtTable1.Rows(0).Item("vcShipPostCode")), "", dtTable1.Rows(0).Item("vcShipPostCode"))
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub GetShippingRates()
        Try
            Dim objItem As New CItems
            Dim ds As DataSet
            Dim dtShipDtls As DataTable
            objItem.DomainID = Session("DomainID")
            objItem.ShippingCMPID = 0
            ds = objItem.ShippingDtls
            dtShipDtls = ds.Tables(0)

            Dim lngShipCMPID As Long
            lngShipCMPID = ds.Tables(1).Rows(0).Item(0)

            Dim dtWareHouse As DataTable

            objItem.WarehouseID = Session("WarehouseID")
            objItem.DomainID = Session("DomainID")
            dtWareHouse = objItem.GetWareHouses

            Dim FromZipcode As String = dtWareHouse.Rows(0).Item("vcWPinCode")
            decShippingCharge = 0
            If lngShipCMPID = 88 Then
                'Dim objRates As New nsoftware.IBizUPS.Rates
                'objRates.AccessKey = dtShipDtls.Rows(0).Item("vcShipFieldValue")
                'objRates.UserId = dtShipDtls.Rows(1).Item("vcShipFieldValue")
                'objRates.Password = dtShipDtls.Rows(2).Item("vcShipFieldValue")
                'objRates.ShipperNumber = dtShipDtls.Rows(3).Item("vcShipFieldValue")
                'objRates.Server = dtShipDtls.Rows(4).Item("vcShipFieldValue")
                'objRates.RequestOption = RatesRequestOptions.roRate
                'objRates.ServiceType = RatesServiceTypes.stStandard
                'objRates.PackageType = RatesPackageTypes.ptPackage
                'objRates.PickupType = RatesPickupTypes.ptOneTimePickup
                'objRates.CustomerType = RatesCustomerTypes.ccOccasional
                'objRates.DeliveryConfirmation = True
                'objRates.FromZipCode = FromZipcode
                'objRates.FromCountryCode = "US"
                'objRates.ToZipCode = txtShipCode.Text
                'objRates.ToCountryCode = "US"
                'objRates.Weight = "1.0"
                'objRates.WeightUnit = RatesWeightUnits.wuLbs
                'objRates.GetRates()
                'Dim i As Integer
                'decShippingCharge = 0
                'For i = 0 To objRates.ServiceIndex - 1
                '    objRates.ServiceIndex = i
                '    decShippingCharge = objRates.ServiceTotalCharge
                '    Exit For
                'Next
            ElseIf lngShipCMPID = 91 Then
                'domesticrates1.UserId = dtUsps.Rows(0).Item("vcUserName")
                'domesticrates1.Password = dtUsps.Rows(0).Item("vcPassword")

                ''domesticrates1.Server = "http://production.shippingapis.com/ShippingAPI.dll"
                'domesticrates1.Server = "https://secure.shippingapis.com/ShippingAPITest.dll"

                'domesticrates1.FromZipCode = FromZipcode
                'domesticrates1.ToZipCode = ToZipcode
                'domesticrates1.PackagePounds = 10
                'domesticrates1.PackageOunces = 0
                'domesticrates1.PackageSize = DomesticratesPackageSizes.psRegular
                'domesticrates1.Machinable = False
                'domesticrates1.ServiceType = DomesticratesServiceTypes.svcAll
                'domesticrates1.ShippingContainer = DomesticratesShippingContainers.scNone
            ElseIf lngShipCMPID = 92 Then
                'Rates1.ServerURL = "https://gateway.fedex.com:443/GatewayDC"
                'Rates1.AccountNumber = dtFedEx.Rows(0).Item("vcAccountNo")
                'Rates1.MeterNumber = dtFedEx.Rows(0).Item("vcMeterNo")
                'Rates1.FromPostalCode = FromZipcode
                'Rates1.FromCountry = "US"
                ''Rates1.FromState = "NC"
                'Rates1.DestinationPostalCode = ToZipcode
                ''Rates1.DestinationState = "MD"
                'Rates1.PackagingType = 0
                'Rates1.Weight = "1"
                'Rates1.Value = "10.00"
                'Rates1.ServiceType = RatesServiceTypes.stUnspecified
                'Rates1.GetRates()
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Sub CheckCreditStatus()
        Try
            Dim objItems As New CItems
            Dim lngCredit, lngRemainingCredit As Long
            objItems.DivisionID = lngDivID
            lngCredit = objItems.GetCreditStatusofCompany
            lngRemainingCredit = objItems.GetAmountDue.Rows(0).Item("RemainingCredit")
            If lngRemainingCredit <= 0 Then
                radBillMe.Visible = False
                radPay.Checked = True
                radBillMe.Visible = False

            Else
                txtCredit.Text = lngCredit
                txtDueAmount.Text = objItems.GetAmountDue.Rows(0).Item("DueAmount")
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Sub


    Public Function KilogramsToPounds(ByVal Kilograms As Single) As Single

        'divide number of kilograms by .4536 to obtain number of pounds

        KilogramsToPounds = Kilograms / 0.4536


    End Function

    Public Function PoundsToKilograms(ByVal Pounds As Single) As Single

        'multiply the number of pounds times .4536 to obtain 
        'number of Kilograms.
        PoundsToKilograms = Pounds * 0.4536

    End Function

    Private Sub bCharge_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bCharge.Click
        Try
            If radPay.Checked = True Then
              
                Dim ResponseMessage As String = ""
                Dim objPaymentGateway As New PaymentGateway
                objPaymentGateway.DomainID = Session("DomainID")
                If objPaymentGateway.GatewayTransaction(Session("TotalAmount"), txtCustomerFirstName.Text & " " & txtCustomerLastName.Text, txtCardNumber.Text, txtCardCVV2.Text, ddlCardExpMonth.SelectedValue, ddlCardExpYear.SelectedValue, lngContId, ResponseMessage) Then
                    CreateBizDocs()
                End If
                liMessage.Text = ResponseMessage

            ElseIf radBillMe.Checked = True Then
                Dim totalDue As Decimal
                totalDue = CDec(txtDueAmount.Text) + CDec(Session("TotalAmount"))
                If totalDue > txtCredit.Text Then
                    liMessage.Text = "You have exceeded the maximum amount you can place on a bill-to account by 'whatever the amount is' Please reduce your total amount to compensate, or if you prefer, execute the order by paying with a check. "
                Else
                    bCharge.Enabled = False
                    liMessage.Text = "Your purchase was processed successfully ! "
                    Dim objItems As New CItems
                    objItems.OppId = Session("OppID")
                    objItems.ShippingCost = lblShippingCost.Text
                    objItems.byteMode = 1
                    objItems.Amount = 0
                    objItems.DomainID = Session("DomainID")
                    objItems.UserCntId = Session("UserContactID")
                    lngOppBizDocID = objItems.UpdateDealStatus1()
                    btnClick.Attributes.Add("onclick", "return OpenBizInvoice('" & Session("OppID") & "','" & lngOppBizDocID & "')")
                    btnClick.Visible = True
                    Session("Data") = Nothing
                    Session("OppID") = Nothing
                End If
            End If
            If Session("UserContactID") > 0 Then
                Dim objItems As New CItems
                objItems.DivisionID = Session("DivId")
                Dim dt As DataTable
                dt = objItems.GetAmountDue
                CType(Master.FindControl("lblBalDue"), Label).Text = String.Format("{0:#,##0.00}", dt.Rows(0).Item("DueAmount"))
                CType(Master.FindControl("lblRemCredit"), Label).Text = String.Format("{0:#,##0.00}", dt.Rows(0).Item("RemainingCredit"))
                CType(Master.FindControl("lblAmtPastDue"), Label).Text = String.Format("{0:#,##0.00}", dt.Rows(0).Item("PastDueAmount"))
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub



    Function CreateBizDocs() As Boolean
        Try
            Dim objItems As New CItems
            objItems.OppId = Session("OppID")
            objItems.byteMode = 1
            objItems.Amount = Session("TotalAmount")
            objItems.ShippingCost = lblShippingCost.Text
            objItems.DomainID = Session("DomainID")
            objItems.UserCntId = Session("UserContactID")
            lngOppBizDocID = objItems.UpdateDealStatus1()
            btnClick.Attributes.Add("onclick", "return OpenBizInvoice('" & Session("OppID") & "','" & lngOppBizDocID & "')")
            btnClick.Visible = True
            Session("Data") = Nothing
            Session("OppID") = Nothing
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function



End Class