Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common

Partial Public Class ShoppingCart_LoginPage : Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("UserContactID") > 0 Then tblLogin.Visible = False
            If Not IsPostBack Then
                Dim objUserAccess As New UserAccess
                objUserAccess.DomainID = Request.Cookies.Get("id_site").Value
                If objUserAccess.GetECommerceDetails.Rows(0).Item("bitHideNewUsers") = True Then lnkNewUsers.Visible = False
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Try
            Dim objUserAccess As New UserAccess
            Dim dttable As DataTable
            objUserAccess.Email = txtEmaillAdd.Text.Trim
            objUserAccess.Password = txtPassword.Text.Trim
            objUserAccess.DomainID = Request.Cookies.Get("id_site").Value
            dttable = objUserAccess.ExtranetLogin
            If dttable.Rows(0).Item(0) = 1 Then
                Session("CompID") = dttable.Rows(0).Item(3)
                Session("UserContactID") = dttable.Rows(0).Item(1)
                Session("ContactId") = dttable.Rows(0).Item(1)
                Session("DivId") = dttable.Rows(0).Item(2)
                Session("GroupId") = dttable.Rows(0).Item(4)
                Session("CRMType") = dttable.Rows(0).Item(5)
                Session("DomainID") = dttable.Rows(0).Item(6)
                Session("CompName") = dttable.Rows(0).Item(7)
                Session("RecOwner") = dttable.Rows(0).Item(8)
                Session("RecOwnerName") = dttable.Rows(0).Item(9)
                Session("PartnerGroup") = dttable.Rows(0).Item(10)
                Session("RelationShip") = dttable.Rows(0).Item(11)
                Session("HomePage") = dttable.Rows(0).Item(12)
                Session("UserEmail") = dttable.Rows(0).Item(13)
                Session("DateFormat") = dttable.Rows(0).Item("vcDateFormat")
                Session("ContactName") = dttable.Rows(0).Item(14)
                Session("PartnerAccess") = dttable.Rows(0).Item(15)
                Session("PagingRows") = dttable.Rows(0).Item("tintCustomPagingRows")
                Session("DefCountry") = dttable.Rows(0).Item("numDefCountry")
                Session("PopulateUserCriteria") = dttable.Rows(0).Item("tintAssignToCriteria")
                Session("EnableIntMedPage") = IIf(dttable.Rows(0).Item("bitIntmedPage") = True, 1, 0)
                Session("DomainName") = dttable.Rows(0).Item("vcDomainName") '' End date
                Session("Internal") = 1
                tblLogin.Visible = False
                Dim CookieEnabled As Boolean
                CookieEnabled = Context.Request.Browser.Cookies
                If CookieEnabled = True Then
                    Dim objcommon As New CCommon
                    Dim cookie As HttpCookie
                    cookie = New HttpCookie("CompanyID", Session("DivId"))
                    cookie.Expires = DateAdd(DateInterval.Year, 1, Now)
                    Context.Response.Cookies.Set(cookie)

                    cookie = New HttpCookie("UserContactID", Session("UserContactID"))
                    cookie.Expires = DateAdd(DateInterval.Year, 1, Now)
                    Context.Response.Cookies.Set(cookie)
                End If
                If Session("redirect") = "True" Then
                    Response.Redirect("../ShoppingCart/Checkout.aspx")
                End If
            ElseIf dttable.Rows(0).Item(0) > 1 Then
                Session("CompID") = Nothing
                Session("UserContactID") = Nothing
                Session("DivId") = Nothing
                Session("GroupId") = Nothing
                litMessage.Text = "Found Multiple Email Address in the system. Please Contact Administrator !"
            Else
                Session("CompID") = Nothing
                Session("UserContactID") = Nothing
                Session("DivId") = Nothing
                Session("GroupId") = Nothing
                litMessage.Text = "You have failed to enter registered email address. Try again, or contact your representative for assistance !"
            End If
            Response.Redirect(Request.Url.PathAndQuery)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class