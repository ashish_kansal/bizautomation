Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts

Partial Public Class frmConfirmAddress : Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Dim objCommon As New CCommon
                objCommon.sb_FillComboFromDBwithSel(ddlBillCountry, 40, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlShipCountry, 40, Session("DomainID"))

                Dim objContacts As New CContacts
                Dim dtTable1 As DataTable
                objContacts.ContactID = Session("UserContactID")
                objContacts.DomainID = Request.Cookies.Get("id_site").Value
                dtTable1 = objContacts.GetBillOrgorContAdd
                txtCustomerFirstName.Text = IIf(IsDBNull(dtTable1.Rows(0).Item("vcFirstname")), "", dtTable1.Rows(0).Item("vcFirstname"))
                txtCustomerLastName.Text = IIf(IsDBNull(dtTable1.Rows(0).Item("vcLastname")), "", dtTable1.Rows(0).Item("vcLastname"))
                txtCustomerEmail.Text = IIf(IsDBNull(dtTable1.Rows(0).Item("vcEmail")), "", dtTable1.Rows(0).Item("vcEmail"))
                txtCustomerPhone.Text = dtTable1.Rows(0).Item("vcPhone")

                txtBillStreet.Text = IIf(IsDBNull(dtTable1.Rows(0).Item("vcBillstreet")), "", dtTable1.Rows(0).Item("vcBillstreet"))
                txtShipStreet.Text = IIf(IsDBNull(dtTable1.Rows(0).Item("vcShipStreet")), "", dtTable1.Rows(0).Item("vcShipStreet"))
                txtBillCity.Text = IIf(IsDBNull(dtTable1.Rows(0).Item("vcBillCity")), "", dtTable1.Rows(0).Item("vcBillCity"))
                txtShipCity.Text = IIf(IsDBNull(dtTable1.Rows(0).Item("vcShipCity")), "", dtTable1.Rows(0).Item("vcShipCity"))
                If Not IsDBNull(dtTable1.Rows(0).Item("vcBillCountry")) Then
                    If Not ddlBillCountry.Items.FindByValue(dtTable1.Rows(0).Item("vcBillCountry")) Is Nothing Then
                        ddlBillCountry.Items.FindByValue(dtTable1.Rows(0).Item("vcBillCountry")).Selected = True
                    End If
                End If
                If Not IsDBNull(dtTable1.Rows(0).Item("vcShipCountry")) Then
                    If Not ddlShipCountry.Items.FindByValue(dtTable1.Rows(0).Item("vcShipCountry")) Is Nothing Then
                        ddlShipCountry.Items.FindByValue(dtTable1.Rows(0).Item("vcShipCountry")).Selected = True
                    End If
                End If
                If ddlBillCountry.SelectedIndex > 0 Then
                    FillState(ddlBillState, ddlBillCountry.SelectedItem.Value, Session("DomainID"))
                    If Not IsDBNull(dtTable1.Rows(0).Item("vcBilState")) Then
                        If Not ddlBillState.Items.FindByValue(dtTable1.Rows(0).Item("vcBilState")) Is Nothing Then
                            ddlBillState.Items.FindByValue(dtTable1.Rows(0).Item("vcBilState")).Selected = True
                        End If
                    End If
                End If
                If ddlShipCountry.SelectedIndex > 0 Then
                    FillState(ddlShipState, ddlShipCountry.SelectedItem.Value, Session("DomainID"))
                    If Not IsDBNull(dtTable1.Rows(0).Item("vcShipState")) Then
                        If Not ddlShipState.Items.FindByValue(dtTable1.Rows(0).Item("vcShipState")) Is Nothing Then
                            ddlShipState.Items.FindByValue(dtTable1.Rows(0).Item("vcShipState")).Selected = True
                        End If
                    End If
                End If
                txtBillCode.Text = IIf(IsDBNull(dtTable1.Rows(0).Item("vcBillPostCode")), "", dtTable1.Rows(0).Item("vcBillPostCode"))
                txtShipCode.Text = IIf(IsDBNull(dtTable1.Rows(0).Item("vcShipPostCode")), "", dtTable1.Rows(0).Item("vcShipPostCode"))
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            Dim objContacts As New CContacts
            objContacts.ContactID = Session("UserContactID")
            objContacts.FirstName = txtCustomerFirstName.Text
            objContacts.LastName = txtCustomerLastName.Text
            objContacts.Email = txtCustomerEmail.Text
            objContacts.ContactPhone = txtCustomerPhone.Text
            objContacts.BillStreet = txtBillStreet.Text
            objContacts.BillCity = txtBillCity.Text
            objContacts.BillCountry = ddlBillCountry.SelectedItem.Value
            objContacts.BillPostal = txtBillCode.Text
            objContacts.BillState = ddlBillState.SelectedItem.Value
            objContacts.ShipStreet = txtShipStreet.Text
            objContacts.ShipState = ddlShipState.SelectedItem.Value
            objContacts.ShipPostal = txtShipCode.Text
            objContacts.ShipCountry = ddlShipCountry.SelectedItem.Value
            objContacts.ShipCity = txtShipCity.Text
            objContacts.DivisionID = Request.Cookies.Get("CompanyID").Value
            objContacts.UpdateCompanyAddress()
            Session("AddressConfirm") = "True"
            Response.Redirect("../ShoppingCart/Checkout.aspx")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlBillCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBillCountry.SelectedIndexChanged
        Try
            If ddlBillCountry.SelectedIndex > 0 Then
                FillState(ddlBillState, ddlBillCountry.SelectedItem.Value, Session("DomainID"))
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlShipCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShipCountry.SelectedIndexChanged
        Try
            If ddlShipCountry.SelectedIndex > 0 Then
                FillState(ddlShipState, ddlShipCountry.SelectedItem.Value, Session("DomainID"))
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class