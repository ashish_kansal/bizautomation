Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Partial Public Class ShoppingCart_Site
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                PopulateMenu()
                If Session("Header") Is Nothing Then
                    Dim dtTable As DataTable
                    Dim objUserAccess As New UserAccess
                    objUserAccess.DomainID = Request.Cookies.Get("id_site").Value
                    dtTable = objUserAccess.GetECommerceDetails()
                    If dtTable.Rows.Count > 0 Then
                        Session("Header") = dtTable.Rows(0).Item("txtHeader")
                        Session("Footer") = dtTable.Rows(0).Item("txtFooter")
                        Session("Css") = dtTable.Rows(0).Item("vcStyleSheet")
                    Else
                        Session("Header") = ""
                        Session("Footer") = ""
                        Session("Css") = ""
                    End If
                End If
                lblFooter.Text = Session("Footer")
                lblHeader.Text = Session("Header")
                If Session("Css") <> "" Then
                    CSSlink.Attributes.Add("href", "../Documents/Docs/" & Session("Css"))
                Else : CSSlink.Attributes.Add("href", "../CSS/EComm.css")
                End If
                If Session("UserContactID") > 0 Then
                    Dim objItems As New CItems
                    objItems.DivisionID = Session("DivId")
                    Dim dt As DataTable
                    dt = objItems.GetAmountDue
                    lblBalDue.Text = String.Format("{0:#,##0.00}", dt.Rows(0).Item("DueAmount"))
                    lblRemCredit.Text = String.Format("{0:#,##0.00}", dt.Rows(0).Item("RemainingCredit"))
                    lblAmtPastDue.Text = String.Format("{0:#,##0.00}", dt.Rows(0).Item("PastDueAmount"))
                Else : tdCredit.Visible = False
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub PopulateMenu()
        Try
            Dim objItems As New CItems
            Dim dt As DataTable
            objItems.byteMode = 10
            objItems.DomainID = Request.Cookies.Get("id_site").Value
            dt = objItems.SeleDelCategory
            PopulateMenuitem(dt, UltraWebTree1.Nodes)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub PopulateMenuitem(ByVal dt As DataTable, ByVal TreeViewnode As Infragistics.WebUI.UltraWebNavigator.Nodes)
        Try
            Dim tnParent As Infragistics.WebUI.UltraWebNavigator.Node
            For Each dr As DataRow In dt.Rows
                Dim tn As New Infragistics.WebUI.UltraWebNavigator.Node
                tn.Text = dr("vcCategoryName").ToString()
                tn.Tag = dr("numCategoryID").ToString() & IIf(dr("Type") <> 2, "C", "")

                If dr("Type") = 0 Then
                    tn.TargetUrl = "../ShoppingCart/frmItemDescrpition.aspx?CatID=" & dr("numCategoryID")
                ElseIf dr("Type") = 1 Then
                    tn.TargetUrl = dr("vcLink")
                ElseIf dr("Type") = 2 Then
                    tn.TargetUrl = "../ShoppingCart/frmItemDetail.aspx?ItemID=" & dr("numCategoryID")
                End If
                If dr("Type") = 2 Then
                    If Not UltraWebTree1.Find(dr("Category").ToString() & "C") Is Nothing Then
                        UltraWebTree1.Find(dr("Category").ToString() & "C").Nodes.Add(tn)
                    End If
                Else
                    If dr("Category") = 0 Then
                        UltraWebTree1.Nodes.Add(tn)
                    Else
                        If Not UltraWebTree1.Find(dr("Category").ToString() & "C") Is Nothing Then
                            UltraWebTree1.Find(dr("Category").ToString() & "C").Nodes.Add(tn)
                        End If
                    End If
                End If
                tn.Expanded = True
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub PopulateSubMenuLevel(ByVal parentid As Integer, ByVal parentNode As Infragistics.WebUI.UltraWebNavigator.Node)
        Try
            Dim objItems As New CItems
            Dim dt As DataTable
            objItems.byteMode = 7
            objItems.CategoryID = parentid
            objItems.DomainID = Request.Cookies.Get("id_site").Value
            dt = objItems.SeleDelCategory
            PopulateMenuitem(dt, parentNode.Nodes)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Try
            Response.Redirect("../ShoppingCart/Search.aspx?str=" & txtSearch.Text.Trim)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class
