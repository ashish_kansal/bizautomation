Imports BACRM.BusinessLogic.Common
Partial Public Class ShoppingCart_MiniBasket : Inherits System.Web.UI.UserControl

    Dim ds As DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                If Session("UserContactID") > 0 Then
                    lnlLogin.Visible = False
                Else : lnkLogout.Visible = False
                End If
                If Not Session("Data") Is Nothing Then
                    Dim dt As DataTable
                    ds = Session("Data")
                    dt = ds.Tables(0)
                    dgBasket.DataSource = dt
                    dgBasket.DataBind()
                    lblTotal.Text = "$ " & CType(IIf(IsDBNull(dt.Compute("SUM(monTotAmount)", "")), 0, dt.Compute("SUM(monTotAmount)", "")), Decimal)
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkLogout_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLogout.Click
        Try
            Session.Abandon()
            lnlLogin.Visible = True
            Response.Redirect("../ShoppingCart/LoginPage.aspx")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgBasket_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgBasket.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                Dim dt As DataTable
                ds = Session("Data")
                dt = ds.Tables(0)
                Dim dr As DataRow
                For Each dr In dt.Rows
                    If dr("numoppitemtCode") = e.Item.Cells(0).Text Then
                        dt.Rows.Remove(dr)
                        Exit For
                    End If
                Next
                dgBasket.DataSource = dt
                dgBasket.DataBind()
                lblTotal.Text = "$ " & CType(IIf(IsDBNull(dt.Compute("SUM(monTotAmount)", "")), 0, dt.Compute("SUM(monTotAmount)", "")), Decimal)
                Session("Items") = dt
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class