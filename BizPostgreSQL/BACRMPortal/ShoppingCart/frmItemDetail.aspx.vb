Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common

Partial Public Class ShoppingCart_frmItemDetail : Inherits System.Web.UI.Page

    Dim lngItemCode As Long
    Dim objCommon As New CCommon
    Dim strValues As String
    Dim dtOppAtributes As DataTable
    Dim dsTemp As DataSet
    Dim dtOppOptAttributes As DataTable
    Dim objItems As New CItems

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngItemCode = GetQueryStringVal(Request.QueryString("enc"), "ItemID")
            If Not IsPostBack Then
                Dim dtTable As DataTable
                If Session("Data") Is Nothing Then
                    createSet()
                Else
                    dsTemp = Session("Data")
                    dtTable = dsTemp.Tables(0)
                    Dim dr As DataRow
                    txtAddedItems.Text = ""
                    For Each dr In dtTable.Rows
                        txtAddedItems.Text = txtAddedItems.Text & dr("numWarehouseItmsID") & ","
                    Next
                    txtAddedItems.Text = txtAddedItems.Text.Trim(",")
                End If
                If Session("WareHouseID") Is Nothing Then
                    If Not Request.Cookies.Get("CompanyID") Is Nothing Then
                        objItems.DivisionID = Request.Cookies.Get("CompanyID").Value
                        objItems.DomainID = Request.Cookies.Get("id_site").Value
                        Session("WareHouseID") = objItems.GetWarehouseFromDivID
                    Else
                        objItems.DomainID = Request.Cookies.Get("id_site").Value
                        Session("WareHouseID") = objItems.GetWarehouseFromDivID
                    End If
                End If
                Dim dtItemDetails As DataTable
                Dim ds As DataSet

                objItems.ItemCode = lngItemCode
                objItems.WarehouseID = Session("WareHouseID")
                objItems.DomainID = Request.Cookies.Get("id_site").Value
                ds = objItems.ItemDetailsForEcomm
                dtItemDetails = ds.Tables(0)
                hplOptImage.Visible = False
                imgOptItem.Visible = False
                If dtItemDetails.Rows.Count > 0 Then

                    lblModelName.Text = IIf(IsDBNull(dtItemDetails.Rows(0).Item("vcItemName")), "", dtItemDetails.Rows(0).Item("vcItemName"))
                    lblDescription.Text = IIf(IsDBNull(dtItemDetails.Rows(0).Item("txtItemDesc")), "", dtItemDetails.Rows(0).Item("txtItemDesc"))

                    If Not IsDBNull(dtItemDetails.Rows(0).Item("monListPrice")) Then
                        lblUnitCost.Text = String.Format("{0:#,##0.00}", dtItemDetails.Rows(0).Item("monListPrice"))
                    End If

                    If Not IsDBNull(dtItemDetails.Rows(0).Item("vcPathForImage")) Then
                        If dtItemDetails.Rows(0).Item("vcPathForImage") <> "" Then
                            imgProduct.ImageUrl = "../Documents/Docs/" & dtItemDetails.Rows(0).Item("vcPathForImage")
                            imgProduct.Width = Unit.Pixel(400)
                        Else : imgProduct.Visible = False
                        End If
                    Else : imgProduct.Visible = False
                    End If

                    If dtItemDetails.Rows(0).Item("charItemType") = "P" Or dtItemDetails.Rows(0).Item("charItemType") = "A" Then
                        If dtItemDetails.Rows(0).Item("bitSerialized") = True Then
                            txtHidValue.Text = "True"
                            txtWareHouseItemID.Text = dtItemDetails.Rows(0).Item("numWareHouseItemID")
                        Else
                            txtHidValue.Text = "False"
                            If dtItemDetails.Rows.Count = 1 Then
                                txtWareHouseItemID.Text = dtItemDetails.Rows(0).Item("numWareHouseItemID")
                            End If
                        End If
                        txtWeight.Text = dtItemDetails.Rows(0).Item("intWeight")
                        txtFreeShipping.Text = dtItemDetails.Rows(0).Item("bitFreeShipping")
                        txtOnHand.Text = CType(dtItemDetails.Compute("SUM(numOnHand)", ""), Decimal)
                        txtBackOrder.Text = dtItemDetails.Rows(0).Item("bitAllowBackOrder")
                        If Not Session("DivId") Is Nothing Then lblStock.Text = dtItemDetails.Rows(0).Item("InStock")
                    Else : txtHidValue.Text = ""
                    End If
                End If
                objItems.ItemCode = lngItemCode
                dtTable = objItems.GetOptAccWareHouses
                If dtTable.Rows.Count > 0 Then
                    tblOptSelection.Visible = True
                    ddlOptItem.DataSource = dtTable
                    ddlOptItem.DataTextField = "vcItemName"
                    ddlOptItem.DataValueField = "numItemCode"
                    ddlOptItem.DataBind()
                    ddlOptItem.Items.Insert(0, "--Select One--")
                    ddlOptItem.Items.FindByText("--Select One--").Value = "0"
                Else : tblOptSelection.Visible = False
                End If

                ''Header Text
                objItems.byteMode = 0
                objItems.ItemCode = lngItemCode
                lblText.Text = objItems.GetShoppingCartHDRDtls
            End If

            If lngItemCode > 0 And txtHidValue.Text = "False" Then CreateAttributes()
            If ddlOptItem.SelectedIndex > 0 And txtHidOptValue.Text <> "" Then CreateOptAttributes()
            lnkOptItemAdd.Attributes.Add("onclick", "return AddOption('" & txtOptUnits.ClientID & "','" & txtHidOptValue.ClientID & "','" & txtAddedItems.ClientID & "','" & ddlOptItem.ClientID & "','')")
            lnkAddToCart.Attributes.Add("onclick", "return Add('" & txtUnits.ClientID & "','" & txtHidValue.ClientID & "','" & txtAddedItems.ClientID & "','')")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub createSet()
        Try
            dsTemp = New DataSet
            Dim dtItem As New DataTable
            dtItem.Columns.Add("numoppitemtCode")
            dtItem.Columns.Add("numItemCode")
            dtItem.Columns.Add("numUnitHour")
            dtItem.Columns.Add("monPrice")
            dtItem.Columns.Add("monTotAmount", GetType(Decimal))
            dtItem.Columns.Add("numSourceID")
            dtItem.Columns.Add("vcItemDesc")
            dtItem.Columns.Add("numWarehouseID")
            dtItem.Columns.Add("vcItemName")
            dtItem.Columns.Add("Warehouse")
            dtItem.Columns.Add("numWarehouseItmsID")
            dtItem.Columns.Add("ItemType")
            dtItem.Columns.Add("Attributes")
            dtItem.Columns.Add("AttrValues")
            dtItem.Columns.Add("FreeShipping")
            dtItem.Columns.Add("Weight")
            dtItem.Columns.Add("Op_Flag")
            dtItem.Columns.Add("bitDiscountType", GetType(Boolean))
            dtItem.Columns.Add("fltDiscount", GetType(Decimal))
            dtItem.Columns.Add("monTotAmtBefDiscount", GetType(Decimal))
            dtItem.TableName = "Item"
            dsTemp.Tables.Add(dtItem)
            dtItem.PrimaryKey = New DataColumn() {dtItem.Columns("numoppitemtCode")}
            Session("Data") = dsTemp
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub CreateAttributes()
        Try
            Dim strAttributes As String = ""
            strValues = ""
            Dim boolLoadNextdropdown As Boolean
            objItems.ItemCode = lngItemCode
            objItems.WarehouseID = Session("WareHouseID")
            dtOppAtributes = objItems.GetOppItemAttributesForEcomm
            If dtOppAtributes.Rows.Count > 0 Then
                Dim i As Integer
                Dim lbl As Label
                Dim tblrow As TableRow
                Dim tblcell As TableCell
                Dim ddl As DropDownList
                tblrow = New TableRow
                For i = 0 To dtOppAtributes.Rows.Count - 1
                    tblcell = New TableCell
                    tblcell.CssClass = "normal1"
                    tblcell.HorizontalAlign = HorizontalAlign.Right
                    lbl = New Label
                    lbl.Text = dtOppAtributes.Rows(i).Item("Fld_label") & IIf(txtHidValue.Text = "True", "", "<font color='red'>*</font>")
                    tblcell.Controls.Add(lbl)
                    tblrow.Cells.Add(tblcell)
                    If dtOppAtributes.Rows(i).Item("fld_type") = "Drop Down List Box" Then
                        tblcell = New TableCell
                        ddl = New DropDownList
                        ddl.CssClass = "signup"
                        ddl.ID = "Attr" & dtOppAtributes.Rows(i).Item("fld_id")
                        tblcell.Controls.Add(ddl)
                        ddl.AutoPostBack = True
                        AddHandler ddl.SelectedIndexChanged, AddressOf Me.LoadItemsonChangeofAttr
                        tblrow.Cells.Add(tblcell)
                        strAttributes = strAttributes & ddl.ID & "~" & dtOppAtributes.Rows(i).Item("Fld_label") & ","
                        If ddl.SelectedIndex > 0 Then strValues = strValues & ddl.SelectedValue & ","
                        If i = 0 Then
                            objCommon.sb_FillAttibuesForEcommFromDB(ddl, dtOppAtributes.Rows(i).Item("numlistid"), lngItemCode, "", Session("WareHouseID"))
                        Else : objCommon.sb_FillAttibuesForEcommFromDB(ddl, IIf(boolLoadNextdropdown = True, dtOppAtributes.Rows(i).Item("numlistid"), 0), lngItemCode, strValues.TrimEnd(","), Session("WareHouseID"))
                        End If
                        If ddl.SelectedIndex > 0 Then boolLoadNextdropdown = True
                    End If
                Next
                strAttributes = strAttributes.TrimEnd(",")
                lnkAddToCart.Attributes.Add("onclick", "return Add('" & txtUnits.ClientID & "','" & txtHidValue.ClientID & "','" & txtAddedItems.ClientID & "','" & strAttributes & "')")
                tblItemAttr.Rows.Add(tblrow)
            Else : lnkAddToCart.Attributes.Add("onclick", "return Add('" & txtUnits.ClientID & "','" & txtHidValue.ClientID & "','" & txtAddedItems.ClientID & "','')")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadItemsonChangeofAttr(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            strValues = ""
            Dim i As Integer
            Dim ddl As DropDownList
            Dim boolEnable As Boolean = True
            Dim boolLoadnextDropdown As Boolean = False
            Dim senderID, controlID As String
            senderID = sender.ID
            For i = 0 To dtOppAtributes.Rows.Count - 1
                controlID = "Attr" & dtOppAtributes.Rows(i).Item("fld_id")
                ddl = tblItemAttr.FindControl(controlID)
                If ddl.SelectedIndex > 0 And boolLoadnextDropdown = False Then strValues = strValues & ddl.SelectedValue & ","
                If boolLoadnextDropdown = True Then
                    objCommon.sb_FillAttibuesForEcommFromDB(ddl, dtOppAtributes.Rows(i).Item("numlistid"), lngItemCode, strValues.TrimEnd(","), Session("WareHouseID"))
                    boolLoadnextDropdown = False
                End If
                If senderID = controlID Then
                    boolLoadnextDropdown = True
                    If i = dtOppAtributes.Rows.Count - 1 Then
                        Dim dtTable As DataTable
                        dtTable = objCommon.GetWarehouseOnAttrSelEcomm(lngItemCode, strValues, Session("WareHouseID"))
                        txtWareHouseItemID.Text = dtTable.Rows(0).Item("numWareHouseItemId")
                        txtOnHand.Text = dtTable.Rows(0).Item("numOnHand")
                        lblStock.Text = dtTable.Rows(0).Item("InStock")
                    End If
                End If
            Next
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkAddToCart_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddToCart.Click
        Try
            If txtUnits.Text > txtOnHand.Text Then
                If txtBackOrder.Text = "False" Then
                    litMessage.Text = "Sufficient Quantity is not present in the inventory"
                    Exit Sub
                End If
            End If

            dsTemp = Session("Data")
            Dim dtItem As DataTable
            dtItem = dsTemp.Tables(0)
            Dim dr As DataRow
            dr = dtItem.NewRow
            dr("numoppitemtCode") = CType(IIf(IsDBNull(dtItem.Compute("MAX(numoppitemtCode)", "")), 0, dtItem.Compute("MAX(numoppitemtCode)", "")), Integer) + 1
            dr("numItemCode") = lngItemCode
            dr("numUnitHour") = txtUnits.Text
            If txtFreeShipping.Text <> "True" Then dr("Weight") = txtWeight.Text

            Dim objPbook As New PriceBookRule
            objPbook.ItemID = lngItemCode
            objPbook.QntyofItems = dr("numUnitHour")
            objPbook.DomainID = Request.Cookies.Get("id_site").Value
            objPbook.DivisionID = Session("DivId")
            Dim dtCalPrice As DataTable
            dtCalPrice = objPbook.GetPriceBasedonPriceBook
            If dtCalPrice.Rows.Count > 0 Then
                dr("monPrice") = dtCalPrice.Rows(0).Item("DiscItemPrice")
            Else : dr("monPrice") = lblUnitCost.Text
            End If

            dr("bitDiscountType") = 0
            dr("fltDiscount") = 0
            dr("monTotAmtBefDiscount") = dr("numUnitHour") * dr("monPrice")
            dr("monTotAmount") = dr("monTotAmtBefDiscount")

            dr("vcItemDesc") = ""
            dr("numWarehouseID") = ""
            dr("vcItemName") = lblModelName.Text
            If txtHidValue.Text = "False" Then
                'dr("Warehouse") = ddlWarehouse.SelectedItem.Text
                dr("numWarehouseItmsID") = txtWareHouseItemID.Text
                If txtHidValue.Text = "False" Then
                    dr("Attributes") = objCommon.GetAttributesForWarehouseItem(dr("numWarehouseItmsID"), 0)
                End If
                Dim i As Integer
                Dim controlID As String
                Dim ddl As DropDownList
                For i = 0 To dtOppAtributes.Rows.Count - 1
                    controlID = "Attr" & dtOppAtributes.Rows(i).Item("fld_id")
                    ddl = tblItemAttr.FindControl(controlID)
                    If ddl.SelectedIndex > 0 Then strValues = strValues & ddl.SelectedValue & ","
                Next
                dr("AttrValues") = strValues.TrimEnd(",")
            End If
            dr("ItemType") = ""
            dr("Op_Flag") = 1
            dtItem.Rows.Add(dr)
            dsTemp.AcceptChanges()
            Dim dtTable As DataTable
            dtTable = dsTemp.Tables(0)
            txtAddedItems.Text = ""
            For Each dr In dtTable.Rows
                txtAddedItems.Text = txtAddedItems.Text & dr("numWarehouseItmsID") & ","
            Next
            txtAddedItems.Text = txtAddedItems.Text.Trim(",")
            Response.Redirect(Request.Url.PathAndQuery)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlOptItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOptItem.SelectedIndexChanged
        Try
            LoadOptItemDetails()
            If ddlOptItem.SelectedIndex > 0 And txtHidOptValue.Text <> "" Then
                tblOptItemAttr.Controls.Clear()
                CreateOptAttributes()
            Else : lnkOptItemAdd.Attributes.Add("onclick", "return AddOption('" & txtOptUnits.ClientID & "','" & txtHidOptValue.ClientID & "','" & txtAddedItems.ClientID & "','" & ddlOptItem.ClientID & "','')")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadOptItemDetails()
        Try
            If ddlOptItem.SelectedIndex > 0 Then
                Dim dtTable As DataTable
                Dim ds As DataSet
                objItems.ItemCode = lngItemCode
                objItems.WarehouseID = Session("WareHouseID")
                objItems.DomainID = Request.Cookies.Get("id_site").Value
                ds = objItems.ItemDetailsForEcomm
                dtTable = ds.Tables(0)
                If dtTable.Rows.Count = 1 Then
                    lblOptPrice.Text = dtTable.Rows(0).Item("monListPrice")
                    lblOptDesc.Text = dtTable.Rows(0).Item("txtItemDesc")
                    If Not IsDBNull(dtTable.Rows(0).Item("vcPathForImage")) Then
                        If dtTable.Rows(0).Item("vcPathForImage") <> "" Then
                            imgOptItem.Visible = True
                            hplOptImage.Visible = True
                            imgOptItem.ImageUrl = "../Documents/Docs/" & dtTable.Rows(0).Item("vcPathForImage")
                            hplOptImage.Attributes.Add("onclick", "return OpenImage(" & lngItemCode & ")")
                        Else
                            imgOptItem.Visible = False
                            hplOptImage.Visible = False
                        End If
                    Else
                        imgOptItem.Visible = False
                        hplOptImage.Visible = False
                    End If
                    If dtTable.Rows(0).Item("charItemType") = "P" Or dtTable.Rows(0).Item("charItemType") = "A" Then
                        txtOptUnits.Visible = True
                        txtHidOptValue.Text = "False"
                        If dtTable.Rows.Count = 1 Then
                            txtOptWareHouseItemID.Text = dtTable.Rows(0).Item("numWareHouseItemID")
                        End If
                        txtOptWeight.Text = dtTable.Rows(0).Item("intWeight")
                        txtOptFreeShipping.Text = dtTable.Rows(0).Item("bitFreeShipping")
                        txtOptOnHand.Text = CType(dtTable.Compute("SUM(numOnHand)", ""), Decimal)
                        txtOptBackOrder.Text = dtTable.Rows(0).Item("bitAllowBackOrder")
                        lblOptStock.Text = dtTable.Rows(0).Item("InStock")
                    Else : txtHidOptValue.Text = ""
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub CreateOptAttributes()
        Try
            Dim strAttributes As String = ""
            strValues = ""
            Dim boolLoadNextdropdown As Boolean
            objItems.ItemCode = ddlOptItem.SelectedValue
            dtOppOptAttributes = objItems.GetOppItemAttributes()
            If dtOppOptAttributes.Rows.Count > 0 Then
                Dim i As Integer
                Dim lbl As Label
                Dim tblrow As TableRow
                Dim tblcell As TableCell
                Dim ddl As DropDownList
                tblrow = New TableRow
                For i = 0 To dtOppAtributes.Rows.Count - 1
                    tblcell = New TableCell
                    tblcell.CssClass = "normal1"
                    tblcell.HorizontalAlign = HorizontalAlign.Right
                    lbl = New Label
                    lbl.Text = dtOppOptAttributes.Rows(i).Item("Fld_label") & IIf(txtHidOptValue.Text = "True", "", "<font color='red'>*</font>")
                    tblcell.Controls.Add(lbl)
                    tblrow.Cells.Add(tblcell)

                    If dtOppOptAttributes.Rows(i).Item("fld_type") = "Drop Down List Box" Then
                        tblcell = New TableCell
                        ddl = New DropDownList
                        ddl.CssClass = "signup"
                        ddl.ID = "AttrOpp" & dtOppOptAttributes.Rows(i).Item("fld_id")

                        tblcell.Controls.Add(ddl)
                        ddl.AutoPostBack = True
                        AddHandler ddl.SelectedIndexChanged, AddressOf Me.LoadOptItemsonChangeofAttr
                        tblrow.Cells.Add(tblcell)
                        strAttributes = strAttributes & ddl.ID & "~" & dtOppOptAttributes.Rows(i).Item("Fld_label") & ","
                        If ddl.SelectedIndex > 0 Then strValues = strValues & ddl.SelectedValue & ","
                        If i = 0 Then
                            objCommon.sb_FillAttibuesForEcommFromDB(ddl, dtOppOptAttributes.Rows(i).Item("numlistid"), ddlOptItem.SelectedValue, "", Session("WareHouseID"))
                        Else : objCommon.sb_FillAttibuesForEcommFromDB(ddl, IIf(boolLoadNextdropdown = True, dtOppOptAttributes.Rows(i).Item("numlistid"), 0), ddlOptItem.SelectedValue, strValues.TrimEnd(","), Session("WareHouseID"))
                        End If
                        If ddl.SelectedIndex > 0 Then boolLoadNextdropdown = True
                    End If
                Next
                strAttributes = strAttributes.TrimEnd(",")
                lnkOptItemAdd.Attributes.Add("onclick", "return AddOption('" & txtOptUnits.ClientID & "','" & txtHidOptValue.ClientID & "','" & txtAddedItems.ClientID & "','" & ddlOptItem.ClientID & "','" & strAttributes & "')")
                tblOptItemAttr.Rows.Add(tblrow)
            End If
            lnkOptItemAdd.Attributes.Add("onclick", "return AddOption('" & txtOptUnits.ClientID & "','" & txtHidOptValue.ClientID & "','" & txtAddedItems.ClientID & "','" & ddlOptItem.ClientID & "','')")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadOptItemsonChangeofAttr(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            strValues = ""
            Dim i As Integer
            Dim ddl As DropDownList
            Dim boolEnable As Boolean = True
            Dim boolLoadnextDropdown As Boolean = False
            Dim senderID, controlID As String
            senderID = sender.ID
            For i = 0 To dtOppOptAttributes.Rows.Count - 1
                controlID = "AttrOpp" & dtOppOptAttributes.Rows(i).Item("fld_id")
                ddl = tblOptItemAttr.FindControl(controlID)
                If ddl.SelectedIndex > 0 And boolLoadnextDropdown = False Then strValues = strValues & ddl.SelectedValue & ","
                If boolLoadnextDropdown = True Then
                    objCommon.sb_FillAttibuesForEcommFromDB(ddl, dtOppOptAttributes.Rows(i).Item("numlistid"), ddlOptItem.SelectedValue, strValues.TrimEnd(","), Session("WareHouseID"))
                    boolLoadnextDropdown = False
                End If
                If senderID = controlID Then
                    boolLoadnextDropdown = True
                    If i = dtOppOptAttributes.Rows.Count - 1 Then
                        Dim dtTable As DataTable
                        dtTable = objCommon.GetWarehouseOnAttrSelEcomm(lngItemCode, strValues, Session("WareHouseID"))
                        txtOptWareHouseItemID.Text = dtTable.Rows(0).Item("numWareHouseItemId")
                        txtOptOnHand.Text = dtTable.Rows(0).Item("numOnHand")
                        lblOptStock.Text = dtTable.Rows(0).Item("InStock")
                    End If
                End If
            Next
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkOptItemAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkOptItemAdd.Click
        Try
            If txtOptUnits.Text > txtOptOnHand.Text Then
                If txtOptBackOrder.Text = "False" Then
                    litOptMessge.Text = "Sufficient Quantity is not present in the inventory"
                    Exit Sub
                End If
            End If

            dsTemp = Session("Data")
            Dim dtItem As DataTable
            dtItem = dsTemp.Tables(0)
            Dim dr As DataRow
            dr = dtItem.NewRow
            dr("numoppitemtCode") = CType(IIf(IsDBNull(dtItem.Compute("MAX(numoppitemtCode)", "")), 0, dtItem.Compute("MAX(numoppitemtCode)", "")), Integer) + 1
            dr("numItemCode") = ddlOptItem.SelectedValue
            dr("numUnitHour") = txtOptUnits.Text
            If txtFreeShipping.Text <> "True" Then
                dr("Weight") = txtWeight.Text
            End If
            Dim objPbook As New PriceBookRule
            objPbook.ItemID = ddlOptItem.SelectedValue
            objPbook.QntyofItems = dr("numUnitHour")
            objPbook.DomainID = Request.Cookies.Get("id_site").Value
            objPbook.DivisionID = Session("DivId")
            Dim dtCalPrice As DataTable
            dtCalPrice = objPbook.GetPriceBasedonPriceBook
            If dtCalPrice.Rows.Count > 0 Then
                dr("monPrice") = dtCalPrice.Rows(0).Item("DiscItemPrice")
            Else : dr("monPrice") = lblOptPrice.Text
            End If

            dr("bitDiscountType") = 0
            dr("fltDiscount") = 0
            dr("monTotAmtBefDiscount") = dr("numUnitHour") * dr("monPrice")
            dr("monTotAmount") = dr("monTotAmtBefDiscount")
            dr("vcItemDesc") = lblOptDesc.Text
            dr("numWarehouseID") = ""
            dr("vcItemName") = ddlOptItem.SelectedItem.Text
            dr("numWarehouseItmsID") = txtOptWareHouseItemID.Text
            dr("ItemType") = "Options & Accessories "
            dr("Attributes") = objCommon.GetAttributesForWarehouseItem(dr("numWarehouseItmsID"), 0)
            dtItem.Rows.Add(dr)
            dsTemp.AcceptChanges()
            ddlOptItem.SelectedIndex = 0
            tblOptItemAttr.Controls.Clear()
            lblOptDesc.Text = ""
            txtOptUnits.Text = ""
            dsTemp = Session("Data")
            Dim dtTable As DataTable
            dtTable = dsTemp.Tables(0)
            txtAddedItems.Text = ""
            For Each dr In dtTable.Rows
                txtAddedItems.Text = txtAddedItems.Text & dr("numWarehouseItmsID") & ","
            Next
            txtAddedItems.Text = txtAddedItems.Text.Trim(",")
            Response.Redirect(Request.Url.PathAndQuery)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class