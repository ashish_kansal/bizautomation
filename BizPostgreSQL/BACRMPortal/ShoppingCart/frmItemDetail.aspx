<%@ Page Language="vb" MasterPageFile="site.master" EnableEventValidation="false"
    AutoEventWireup="true" CodeBehind="frmItemDetail.aspx.vb" Inherits="BACRMPortal.ShoppingCart_frmItemDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CphPage" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
        EnableViewState="true">
        <ContentTemplate>
            <table>
                <tr>
                    <td colspan="2">
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="lblModelName" CssClass="Header" runat="server"></asp:Label>
                                </td>
                                <td>
                                    &nbsp;<asp:Label ID="lblStock" CssClass="Stock" runat="server"></asp:Label><asp:Label
                                        CssClass="Stock" ID="lblQuantity" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    Price $<asp:Label ID="lblUnitCost" runat="server" Font-Bold="True"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Table ID="tblItemAttr" runat="server">
                                    </asp:Table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    Units&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtUnits" runat="server"></asp:TextBox>
                                    <asp:LinkButton ID="lnkAddToCart" runat="server" CssClass="addtocartlink">Add To Cart</asp:LinkButton>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Literal ID="litMessage" runat="server"></asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Image ID="imgProduct" runat="server" />
                    </td>
                    <td>
                        <asp:Label ID="lblDescription" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="lblText" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
            <br />
            <table id="tblOptSelection" runat="server" visible="false">
                <tr>
                    <td colspan="2">
                        <b>Options And Accessories</b>
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td class="normal1" rowspan="111111115">
                        <asp:Image ID="imgOptItem" runat="server" BorderWidth="1" Width="100" BorderColor="black"
                            Height="100"></asp:Image><br>
                        <asp:HyperLink ID="hplOptImage" runat="server" CssClass="hyperlink" NavigateUrl="">View Full Image</asp:HyperLink>
                    </td>
                    <td>
                        <table id="Table2" runat="server">
                            <tr>
                                <td class="normal1" valign="top">
                                    Item
                                    <asp:DropDownList ID="ddlOptItem" TabIndex="19" runat="server" CssClass="signup"
                                        Width="450" AutoPostBack="true">
                                    </asp:DropDownList>
                                </td>
                                <td style="color: orange">
                                    &nbsp;<asp:Label ID="lblOptStock" runat="server"></asp:Label><asp:Label ID="lblOptQuantity"
                                        runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    Price $<asp:Label ID="lblOptPrice" runat="server" Font-Bold="True"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="white-space: nowrap">
                                    <asp:Table ID="tblOptItemAttr" runat="server">
                                    </asp:Table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    Units
                                    <asp:TextBox ID="txtOptUnits" runat="server" CssClass="signup" Width="90px"></asp:TextBox>
                                    <asp:LinkButton ID="lnkOptItemAdd" runat="server" CssClass="addtocartlink">Add To Cart</asp:LinkButton>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Literal ID="litOptMessge" runat="server"></asp:Literal>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Label ID="lblOptDesc" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <asp:TextBox ID="txtHidValue" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtHidEditOppItem" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtHidOptValue" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtAddedItems" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtWareHouseItemID" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtOptWareHouseItemID" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtFreeShipping" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtBackOrder" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtOnHand" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtWeight" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtOptFreeShipping" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtOptBackOrder" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtOptOnHand" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtOptWeight" runat="server" Style="display: none"></asp:TextBox>
            <br />
            <br />
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>

    <script language="javascript" type="text/javascript">
        function Add(a, c, d, e) {

            if (document.getElementById(ctl00_MiniBasket2_lnlLogin) != null) {
                document.location.href = ctl00_MiniBasket2_lnlLogin;
                return false;
            }
            if (document.getElementById('ctl00_CphPage_txtHidValue').value == "False") {
                for (i = 0; i < document.getElementById('ctl00_CphPage_tblItemAttr').all.length; i++) {
                    if (document.getElementById('ctl00_CphPage_tblItemAttr').all[i].type == 'select-one') {
                        if (document.getElementById('ctl00_CphPage_tblItemAttr').all[i].value == 0) {
                            alert("Select Attributes")
                            document.getElementById('ctl00_CphPage_tblItemAttr').all[i].focus();
                            return false;
                        }
                    }
                }

            }
            if (document.getElementById(c).value != "") {
                if (document.getElementById('ctl00_CphPage_txtWareHouseItemID').value == 0 || document.getElementById('ctl00_CphPage_txtWareHouseItemID').value == '') {
                    alert("InSufficient Quantity")
                    return false;
                }
            }


            if (document.getElementById(c).value != "") {
                if (document.getElementById(d).value != '') {
                    var ddlIDs = document.getElementById(d).value.split(",");
                    for (i = 0; i < ddlIDs.length; i++) {
                        if (ddlIDs[i] == document.getElementById('ctl00_CphPage_txtWareHouseItemID').value) {
                            alert("This Item is already added to opportunity. Please Edit the details")
                            return false;
                        }
                    }
                }

            }

            if (document.getElementById(a).value == "") {
                alert("Enter Units")
                document.getElementById(a).focus();
                return false;
            }

        }
        function AddOption(a, c, d, e, f) {

            if (document.getElementById(e).value == 0) {
                alert("Select Option Item")
                document.getElementById(e).focus();
                return false;
            }
            if (document.getElementById(c).value != "") {
                if (document.getElementById('ctl00_CphPage_txtOptWareHouseItemID').value == 0 || document.getElementById('ctl00_CphPage_txtOptWareHouseItemID').value == '') {
                    alert("Sufficient Quantity is not present in the inventory")
                    return false;
                }
            }
            if (document.getElementById(c).value == "False") {
                if (f != '') {
                    var ddlIDs = f.split(",");
                    for (i = 0; i < ddlIDs.length; i++) {
                        if (document.getElementById('ctl00_CphPage_' + ddlIDs[i].split("~")[0]).value == "0") {
                            alert("Select " + ddlIDs[i].split("~")[1])
                            document.getElementById('ctl00_CphPage_' + ddlIDs[i].split("~")[0]).focus();
                            return false;
                        }
                    }
                }

            }
            if (document.getElementById(c).value != "") {
                if (document.getElementById(b) != null) {
                    if (document.getElementById(b).value == 0) {
                        alert("Item is not present in inventory")
                        document.getElementById(b).focus();
                        return false;
                    }
                }

            }

            if (document.getElementById(c).value != "") {
                if (document.getElementById(d).value != '') {
                    var ddlIDs = document.getElementById(d).value.split(",");
                    for (i = 0; i < ddlIDs.length; i++) {
                        if (ddlIDs[i] == document.getElementById('ctl00_CphPage_txtOptWareHouseItemID').value) {
                            alert("This Item is already added to opportunity. Please Edit the details")
                            return false;
                        }
                    }
                }

            }
            if (document.getElementById(a).value == "") {
                alert("Enter Units")
                document.getElementById(a).focus();
                return false;
            }
        }
    </script>

</asp:Content>
