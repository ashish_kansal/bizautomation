<%@ Page Language="vb" AutoEventWireup="false" AspCompat="true"  CodeBehind="frmBizDocAttachments.aspx.vb" Inherits="BACRMPortal.frmBizDocAttachments" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>BizDoc Attachments</title>
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <script language="javascript" >
    function Close()
    {
        window.close();
        return false;
    }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <br />
    <table width="100%">
    	        <tr>
					<td align="right" colSpan="2">
					    <asp:Button ID="btnSort" Runat="server" CssClass="button" Text="Sort Documents"></asp:Button>
						<asp:Button ID="btnAdd" Runat="server" CssClass="button" Text="Add Documents"></asp:Button>
						<asp:Button ID="btnCancel" Runat="server" CssClass="button" Text="Close"></asp:Button><br>
						<br />
						
					</td>
				</tr>
	</table> 
    <table width="100%">
    	        <tr>
    	            <td class="normal1" align="right">
    	                BizDoc
    	            </td>
    	            <td>
    	                <asp:DropDownList ID="ddlBizDoc" runat="server" AutoPostBack="true"  CssClass="signup"></asp:DropDownList>
    	            </td>
    	        </tr>
			</table>
			<br />
    	    <asp:datagrid id="dgAttachments" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
										BorderColor="white">
										<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
										<ItemStyle CssClass="is"></ItemStyle>
										<HeaderStyle CssClass="hs"></HeaderStyle>
										<Columns>
										<asp:BoundColumn DataField="numAttachmntID"   Visible="false" ></asp:BoundColumn>
										<asp:BoundColumn DataField="vcURL"   Visible="false" ></asp:BoundColumn>
										<asp:BoundColumn DataField="vcDocName"   Visible="false" ></asp:BoundColumn>
										<asp:HyperLinkColumn HeaderText="Document Name"   DataNavigateUrlFormatString="../opportunity/frmBizDocAddAttchmnts.aspx?AtchID={0}"  DataNavigateUrlField="numAttachmntID" DataTextField="vcDocName"></asp:HyperLinkColumn>
										<asp:TemplateColumn>
										    <ItemTemplate>
										        <asp:hyperlink ID="hplOpenLink" runat="server" Target="_blank" CssClass="hyperlink"  Text="Open Link"></asp:hyperlink>
										    </ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn>
												<ItemTemplate>
													<asp:Button ID="btnDelete" Runat="server" CssClass="Delete" Text="r" CommandName="Delete"></asp:Button>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid>
    </form>
</body>
</html>
