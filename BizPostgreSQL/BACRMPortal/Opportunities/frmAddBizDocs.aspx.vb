'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Partial Class frmAddBizDocs
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents imgBtnSaveClose As System.Web.UI.WebControls.ImageButton
    Protected WithEvents imgbtnCancel As System.Web.UI.WebControls.ImageButton

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not IsPostBack Then
                Dim objCommon As New CCommon
                objCommon.sb_FillComboFromDBwithSel(ddlBizDocs, 27, Session("DomainID"))
            End If
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

        Private Sub btnSaveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
            If ddlBizDocs.SelectedItem.Value > 0 Then
                Dim str As String()
                Dim k As Integer
                str = GetQueryStringVal(Request.QueryString("enc"), "BizDocIDs").Split(",")
                For k = 0 To str.Length - 2
                    If ddlBizDocs.SelectedItem.Value = str(k) Then
                        litMessage.Text = "A BizDoc by the same name is already created. Please select another BizDoc !"
                        Exit Sub
                    End If
                Next
                Dim objBizDocs As New OppBizDocs
                objBizDocs.OppId = GetQueryStringVal(Request.QueryString("enc"), "OpID")
                objBizDocs.BizDocId = ddlBizDocs.SelectedItem.Value
                objBizDocs.UserCntID = Session("UserContactID")
                objBizDocs.SaveBizDoc()
                Response.Redirect("frmBizDocs.aspx?OpID=" & GetQueryStringVal(Request.QueryString("enc"), "OpID"))
            End If
        Catch ex As Exception
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("frmBizDocs.aspx?OpID=" & GetQueryStringVal(Request.QueryString("enc"), "OpID"))
        End Sub
    End Class

