<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmBizInvoice.aspx.vb" Inherits="BACRMPortal.frmBizInvoice" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>BizDocs</title>
		<script language="javascript" type="text/javascript" >
		function MakeFinal()
		{
			if (lblBalance.innerText<=0)
			{
				return true
			}
			else
			{
				alert("BizDoc can not be made final, until the balance due reads 0.00")
				return false
			}
		}
			function OpenAmtPaid(a,b)
			{
				window.open('frmAmtPaid.aspx?OppBizDocID='+a +'&Amt='+b,'','toolbar=no,titlebar=no,top=300,width=400,height=100,scrollbars=yes,resizable=yes');
				return false;
			}
			
			function OpenTerms(a,b)
				{
				window.open('frmTerms.aspx?OppBizDocID='+a +'&Ter='+b,'','toolbar=no,titlebar=no,top=300,width=400,height=100,scrollbars=yes,resizable=yes');
				return false;
			}
			function openClone()
				{
				window.open('frmCloneDetails.aspx','','toolbar=no,titlebar=no,top=300,width=400,height=100,scrollbars=yes,resizable=yes');
				return false;
			}
			function Save()
			{
				if (document.Form1.txtDisc.value!='')
				{
					if (isNaN(parseFloat(document.Form1.txtDisc.value)))
					{
						alert("Enter a Valid Discount Percentage!")
						document.Form1.txtDisc.focus();
						return false;
					}
				}
			
				if (parseFloat(document.Form1.txtDisc.value)>100)
				{
					alert("Enter a Valid Discount Percentage!")
					document.Form1.txtDisc.focus();
					return false;
				}
			}
			
			-
	function CheckDisc(eventTarget, eventArgument) {
	
		if (document.Form1.txtDisc.value!='')
				{
					if (isNaN(parseFloat(document.Form1.txtDisc.value)))
					{
						alert("Enter a Valid Discount Percentage!")
						document.Form1.txtDisc.focus();
						return false;
					}
				}
			
			
				if (parseFloat(document.Form1.txtDisc.value)>100)
				{
					alert("Enter a Valid Discount Percentage!")
					document.Form1.txtDisc.focus();
					return false;
				}
	}
	function Close()
	{
		window.close();
	}
	function Print()
	{
		tblButtons.style.display='none';
		window.print();
		return false;
	}
	function openApp(a,b)
		{
			window.open('../Documents/frmDocApprovers.aspx?DocID='+a+'&DocType='+b,'','toolbar=no,titlebar=no,left=300,top=450,width=900,height=500,scrollbars=yes,resizable=yes')
		    return false;
		}
function OpenAtch(a,b,c,d)
		{
		    window.open("../Opportunities/frmBizDocAttachments.aspx?BizDocID="+document.Form1.txtBizDoc.value+"&E=2&OpID="+a + "&OppBizId=" + b + "&DomainID=" + c + "&ConID=" + c ,"","width=800,height=400,status=no,scrollbars=yes,left=155,top=160");
			return false;
		}
		</script>
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
			<br>
			<asp:table id="table1" BorderWidth="1" Runat="server" Width="700" BorderColor="black" GridLines="None">
				<asp:tableRow>
					<asp:tableCell>
						<table width="100%" border="0" cellpadding="0">
							<tr>
								<td valign="top" align="left">
									<asp:Image ID="imgLogo" Runat="server" ImageUrl="../Documents/Docs/Logo.gif"></asp:Image>
								</td>
								<td align="right" colSpan="4" nowrap>
									<asp:label id="lblBizDoc" ForeColor="#c0c0c0" Font-Name="Arial black" Runat="server" Font-Size="26"
										Font-Bold="True"></asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							</tr>
						</table>
						<table width="100%" border="0" cellpadding="0">
							<tr>
								<td class="normal1" align="right">Discount</td>
								<td class="normal1">
									<asp:textbox id="txtDisc" Runat="server" CssClass="signup" Width="30" AutoPostBack="True"></asp:textbox>%
								</td>
								<td class="normal1" nowrap>
									<asp:hyperlink id="hplTermsName" Runat="server"></asp:hyperlink>
									<asp:Label ID="lblTerms" Runat="server" style="DISPLAY:none"></asp:Label>
								</td>
								<td align="right">
									<table width="100%">
										<tr bgColor="buttonshadow">
											<td class="normal1"><font color="white">Billing Terms</font>
											</td>
											<td class="normal1"><font color="white">Due Date</font>
											</td>
											<td class="normal1"><font color="white">Date Created</font>
											</td>
											<td class="normal1"><font color="white">ID#</font>
											</td>
										</tr>
										<tr>
											<td class="normal1">
												<asp:label id="lblBillingTerms" Runat="server"></asp:label>
											</td>
											<td class="normal1">
												<asp:label id="lblDuedate" Runat="server"></asp:label>
											</td>
											<td class="normal1">
												<asp:label id="lblDate" Runat="server"></asp:label>
											</td>
											<td class="normal1">
												<asp:label id="lblID" Runat="server"></asp:label>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr class="normal1">
								<td colSpan="5">
									<table height="100%" width="100%">
										<tr bgColor="buttonshadow">
											<td class="normal1"><font color="white">Bill To</font></td>
											<td class="normal1"><font color="white">Ship To</font></td>
											<td class="normal1"><font color="white">Status</font></td>
										</tr>
										<tr>
											<td class="normal1">
												<asp:label id="lblBillTo" Runat="server"></asp:label></td>
											<td class="normal1">
												<asp:label id="lblShipTo" Runat="server"></asp:label></td>
											<td>
												<table height="100%" width="100%">
													<tr>
														<td class="normal1"><font color="#333399">
																<asp:hyperlink class="normal1" id="hplAmountPaid" Runat="server" CssClass="hyperlink">Amount Paid:</asp:hyperlink></font></td>
														<td class="normal1">
															<asp:label id="lblAmountPaid" Runat="server"></asp:label></td>
													</tr>
													<tr>
														<td class="normal1">Balance Due:
														</td>
														<td class="normal1">
															<asp:label id="lblBalance" Runat="server"></asp:label></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr class="normal1">
								<td colSpan="5">
									<table height="100%" width="100%">
										<tr bgColor="buttonshadow">
											<td class="normal1"><font color="white">P.O.#</font></td>
											<td class="normal1"><font color="white">Opportunity or Deal ID</font></td>
										</tr>
										<tr>
											<td>
												<asp:textbox id="txtPO" Runat="server" CssClass="signup" Width="200"></asp:textbox></td>
											<td class="normal1">
												<asp:label id="lblOppID" Runat="server"></asp:label></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr class="normal1" bgColor="buttonshadow">
								<td class="normal1" colSpan="5"><font color="white">Comments</font>
								</td>
							</tr>
							<tr class="normal1">
								<td colSpan="5">
									<asp:textbox id="txtComments" Runat="server" CssClass="signup" Width="630"></asp:textbox></td>
							</tr>
							<tr class="normal1">
								<td colSpan="5">
								<asp:datagrid id="dgBizDocs" runat="server" ForeColor="Transparent" Font-Size="10px" Width="100%"
										BorderStyle="None" BorderColor="White" BackColor="White" GridLines="Vertical" HorizontalAlign="Center"
										AutoGenerateColumns="False">
										<AlternatingItemStyle Font-Size="8pt" Font-Names="tahoma,verdana,arial,helvetica" BackColor="White"></AlternatingItemStyle>
										<ItemStyle Font-Size="8pt" Font-Names="tahoma,verdana,arial,helvetica" HorizontalAlign="Center"
											BorderStyle="None" BorderColor="White" VerticalAlign="Middle" BackColor="#e0dfe3"></ItemStyle>
										<HeaderStyle Font-Size="8pt" Font-Names="tahoma,verdana,arial,helvetica" Wrap="False" HorizontalAlign="Center"
											Height="20px" ForeColor="White" BorderColor="Black" VerticalAlign="Middle" BackColor="#9d9da1"></HeaderStyle>
										<Columns>
											
										</Columns>
									</asp:datagrid></td>
							</tr>
						</table>
						<table width="100%">
							<tr>
								<td colspan="4">
									<table width="100%">
										<tr class="normal1">
											<td><font color="#999999"><i>Created By &nbsp;</i></font>
											</td>
											<td><font color="#999999"><i>
														<asp:label id="lblcreated" Runat="server"></asp:label></i></font>
											</td>
										</tr>
										<tr class="normal1">
											<td><font color="#999999"><i>Last Modified By &nbsp;</i></font>
											</td>
											<td><font color="#999999"><i>
														<asp:label id="lblModifiedby" Runat="server"></asp:label></i></font>
											</td>
										</tr>
										<tr class="normal1">
											<td><font color="#999999"><i>Last Viewed By &nbsp;</i></font>
											</td>
											<td><font color="#999999"><i>
														<asp:label id="lblviewwedby" Runat="server"></asp:label></i></font>
											</td>
										</tr>
										<tr class="normal1" id="trApprove" runat="server">
											<td><font color="#999999"><i>Approved By &nbsp;</i></font>
											</td>
											<td><font color="#999999"><i>
														<asp:label id="lblApprovedBy" Runat="server"></asp:label></i></font>
											</td>
										</tr>
									</table>
								</td>
								<td align="right">
									<table border="0">
										<tr class="normal1">
											<td align="right" class="normal1"><i>Sub Total :</i>
											</td>
											<td align="right">
												<i>
													<asp:Label ID="lblTotAmt" Runat="server"></asp:Label></i>
											</td>
										</tr>
										<asp:panel id="pnlShipping" Runat="server">
											<TR class="normal1">
												<TD class="normal1" align="right"><I>Shipping :</I></FONT>
												</TD>
												<TD align="right"><I>
														<asp:label id="lblShipping" Runat="server"></asp:label></I></TD>
											</TR>
										</asp:panel>
										<tr class="normal1">
											<td align="right" class="normal1"><i>Tax Amount :</i></FONT>
											</td>
											<td align="right">
												<i>
													<asp:label id="lblTaxAmount" Runat="server"></asp:label></i>
											</td>
										</tr>
										<tr class="normal1">
											<td align="right" class="normal1"><i>Discount :</i></FONT>
											</td>
											<td align="right">
												<i>
													<asp:Label ID="lblDiscount" Runat="server"></asp:Label></i>
											</td>
										</tr>
										<tr class="normal1">
											<td align="right" class="normal1"><i>Late Charge :</i></FONT>
											</td>
											<td align="right">
												<i>
													<asp:Label ID="lblLateCharge" Runat="server"></asp:Label></i>
											</td>
										</tr>
										<tr class="normal1" id="trDisc" runat="server">
											<td align="right" class="text_bold">Grand Total :</FONT>
											</td>
											<td align="right">
												<asp:Label ID="lblTotAmtDisc" Runat="server"></asp:Label>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<asp:Image ID="imgFooter" runat="server"  />
					</asp:tableCell>
				</asp:tableRow>
			</asp:table>
			<asp:textbox id="txtBizDocRecOwner" style="DISPLAY: none" Runat="server"></asp:textbox>
			<asp:textbox id="txtBizDoc" style="DISPLAY: none" Runat="server"></asp:textbox>
			<table id="tblButtons" runat="server"  width="100%">
				<tr class="normal1">
					<td align="right" colSpan="2">
						<br>
						<asp:button id="btnApprovers" Runat="server" Text="List of Approvers"  CssClass="button"></asp:button>
						<asp:button id="btnPrint" Runat="server" Text="Print" CssClass="button"></asp:button>
						<asp:button id="btnClose" Runat="server" Text="Close" CssClass="button"></asp:button>
						<br />
						<br />
						<asp:HyperLink ID="hplBizDocAtch" CssClass="hyperlink" runat="server" >Attachments</asp:HyperLink>
						</td>
				</tr>
			</table>
			<br />
			<asp:Panel ID="pnlApprove" runat="server">
									<table width="100%">
									         <tr>
									            <td class="normal1" align="right">
									            Comments&nbsp;&nbsp; 
									            </td>
									            <td class="normal1" align="left">
									            <asp:TextBox  runat="server" ID="txtComment" CssClass="signup" Width="340"  TextMode="MultiLine"></asp:TextBox>
									            </td>
									         </tr>
				                            <tr>
					                            <td class="normal4" align="right">
					                            Document is Ready for Approval&nbsp;&nbsp;
					                            </td>
						                           <td class="normal4" align="left">
						                           <asp:Button ID="btnApprove" runat="server" CssClass="button"  Text="Click Here to Approve"/>
						                           <asp:Button ID="btnDecline" runat="server" CssClass="button"  Text="Click Here to Decline"/>
						                           </td>
				                            </tr>
			                            </table>
									</asp:Panel>
		</form>
	</body>
</HTML>
