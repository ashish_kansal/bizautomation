<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmNewOpprtunity.aspx.vb" Inherits="BACRMPortal.frmNewOpprtunity"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Opportunity</title>
		<script language="javascript">
		function Save()
		{
			if ((document.frm.ddlContacts.selectedIndex==-1 )||(document.frm.ddlContacts.value==0 ))
			{
				alert("Select Contact")
				document.frm.ddlContacts.focus();
				return false;
			}
			if (document.frm.ddlOppType.value==0 )
			{
				alert("Select Opportunity Type")
				document.frm.ddlOppType.focus();
				return false;
			}
			if (document.frm.ddlSource.value==0 )
			{
				alert("Select Source")
				document.frm.ddlSource.focus();
				return false;
			}
			if (document.frm.ddlSource.value==0 )
			{
				alert("Select Source")
				document.frm.ddlSource.focus();
				return false;
			}
			if (dtgItem.rows.length==1)
				{
					alert("Select an Item");
					document.frm.ddlItem.focus();
					return false;
				}
		}
		
		function Add()
		{
			if (document.frm.ddlItem.value==0 )
			{
				alert("Select Item")
				document.frm.ddlItem.focus();
				return false;
			}
			if (document.frm.txtUnits.value=="" )
			{
				alert("Enter Units")
				document.frm.txtUnits.focus();
				return false;
			}
			if (document.frm.txtPrice.value=="")
			{
				alert("Enter Price")
				document.frm.txtPrice.focus();
				return false;
			}
		}
		function CheckNumber(cint)
					{
						if (cint==1)
						{
							if (!(window.event.keyCode > 47 && window.event.keyCode < 58 || window.event.keyCode==44 || window.event.keyCode==46))
							{
								window.event.keyCode=0;
							}
						}
						if (cint==2)
						{
							if (!(window.event.keyCode > 47 && window.event.keyCode < 58))
							{
								window.event.keyCode=0;
							}
						}
						
					}
			function Close()
		{
		    window.close()
		    return false;
		}
		</script>
	</HEAD>
	<BODY>
		<form id="frm" method="post" runat="server">
            <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"/>
            <br />
            <table id="table1" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server">
				<tr>
					<td valign="bottom">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;New 
									Opportunity&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td class="text_red">&nbsp;
						<asp:label id="lblOpptComments" runat="server" Visible="False" Font-Size="9px"></asp:label></td>
					<TD align="right">
						<asp:Button ID="btnSave" Runat="server" Text="Save &amp; Close" CssClass="button"></asp:Button>
						<asp:button id="btnClose" Runat="server" CssClass="button" Text="Close" Width="50"></asp:button>
					</TD>
				</tr>
			</table>
			
			<asp:table id="tblOppr" cellpadding="0" cellspacing="0" BorderWidth="1" Height="300" Runat="server"
				Width="100%" BorderColor="black" GridLines="None" CssClass="aspTable">
				<asp:tableRow>
					<asp:tableCell VerticalAlign=Top >
					<br />
						<table width="100%">
							<tr>
									 <td rowspan="30" valign="top" >
						                                <img src="../images/Dart-32.gif" />
					                                </td>
					             <td class="text" align="right">Contact<FONT color="#ff3333"> *</FONT></td>
								<td>
									<asp:dropdownlist id="ddlContacts" Runat="server" Width="180" CssClass="signup"></asp:dropdownlist></td>
							</tr>
							<TR>
								<TD class="text" align="right">Opportunity Type<FONT color="#ff3333"> *</FONT></TD>
								<TD>
									<asp:dropdownlist id="ddlOppType" Runat="server" CssClass="signup" Width="180">
										<asp:ListItem Value="0">--Select One--</asp:ListItem>
										<asp:ListItem Value="1">Sales Opportunities</asp:ListItem>
										<asp:ListItem Value="2">Purchase Opportunities</asp:ListItem>
									</asp:dropdownlist></TD>
							</TR>
							<TR>
								<TD class="text" align="right">Oppotunity Source<FONT color="#ff3333"> *</FONT></TD>
								<TD>
									<asp:dropdownlist id="ddlSource" Runat="server" CssClass="signup" Width="180"></asp:dropdownlist></TD>
							</TR>
							<TR>
								<TD class="text" align="right">Campaign</TD>
								<TD>
									<asp:dropdownlist id="ddlCampaign" Runat="server" CssClass="signup" Width="180"></asp:dropdownlist></TD>
							</TR>
							<TR>
								<TD class="text" align="right">Item <font color="#ff3333">*</font></TD>
								<TD>
									<asp:dropdownlist id="ddlItem" Runat="server" CssClass="signup" Width="180"></asp:dropdownlist></TD>
							</TR>
							<TR>
								<TD class="text" align="right">Units / Hours <font color="#ff3333">*</font></TD>
								<TD class="text">
									<asp:textbox id="txtUnits" Runat="server" CssClass="signup"></asp:textbox>&nbsp;&nbsp; 
									Price <FONT color="#ff3333">*</FONT>
									<asp:textbox id="txtPrice" Runat="server" CssClass="signup"></asp:textbox></TD>
							</TR>
							<tr>
								<td align="right" colSpan="2">
									<asp:Button ID="btnAdd" Runat="server" CssClass="button" Width="50" Text="Add"></asp:Button>
								</td>
							</tr>
						</table>
                       
                        <asp:datagrid id="dtgItem" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
							BorderColor="white">
							<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							<ItemStyle CssClass="is"></ItemStyle>
							<HeaderStyle CssClass="hs"></HeaderStyle>
							<Columns>
								<asp:BoundColumn Visible="False" DataField="ItemCode" HeaderText="ItemCode"></asp:BoundColumn>
								<asp:BoundColumn DataField="ItemName" HeaderText="Item"></asp:BoundColumn>
								<asp:BoundColumn DataField="Unit" HeaderText="Unit"></asp:BoundColumn>
								<asp:BoundColumn DataField="Price" DataFormatString="{0:#,###.00}" HeaderText="Price"></asp:BoundColumn>
								<asp:TemplateColumn>
									<HeaderTemplate>
										<asp:Button ID="btnHdelete" Runat="server" CssClass="Delete" Text="r"></asp:Button>
									</HeaderTemplate>
									<ItemTemplate>
										<asp:Button ID="btnDelete" Runat="server" CssClass="Delete" Text="r" CommandName="Delete"></asp:Button>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid>
											
						<br>
					</asp:tableCell>
				</asp:tableRow>
			</asp:table>
				
		</form>
	</BODY>
</HTML>
