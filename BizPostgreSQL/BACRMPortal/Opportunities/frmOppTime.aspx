<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmOppTime.aspx.vb" Inherits="BACRMPortal.frmOppTime" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Billable Time</title>
		<script language="javascript">
			function Close()
			{
				window.close();
			}
				function Save()
			{
				if (document.Form1.txtRate.value=="")
				{
					alert("Enter Rate")
					document.Form1.txtRate.focus();
					return false;
				}
				if (document.Form1.txtHour.value=="")
				{
					alert("Enter Hour")
					document.Form1.txtHour.focus();
					return false;
				}
			}
			function OpenRec(a)
		{
			window.open('../opportunities/frmRecomBillTime.aspx?Hour='+document.Form1.txtHour.value+'&DivID='+a,'','toolbar=no,titlebar=no,left=300,top=300,width=400,height=200,scrollbars=yes,resizable=yes')
			return false;
		}
		function FillBox(a,b)
		{
		 document.Form1.txtRate.value=a;
		 document.Form1.txtItemID.value=b;
		}
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table width="100%">
				<tr>
					<td class="text_bold" colSpan="2">Billable Time</td>
					<td align="right" colSpan="6">
					<asp:Button ID="btndelete" Runat="server" CssClass="button" Text="Delete" Width=50></asp:Button>
						<asp:Button ID="btnSaveClose" Runat="server" CssClass="button" Text="Save &amp; Close"></asp:Button>
						<asp:Button ID="btnCancel" Runat="server" CssClass="button" Text="Close" Width=50></asp:Button><br>
						<br>
					</td>
				</tr>
				<tr>
					<td class="normal1" align="right">
					<asp:HyperLink ID=hplRec runat=server CssClass=hyperlink>Rate/Hour</asp:HyperLink>
					</td>
					<td><asp:textbox id="txtRate" Runat="server" Width="50" CssClass=signup></asp:textbox></td>
					<td class="normal1" align="right">Hour
					</td>
					<td><asp:textbox id="txtHour" Runat="server" Width="50" CssClass=signup></asp:textbox></td>
					<td class="normal1" align="right">Minutes
					</td>
					<td><asp:dropdownlist id="ddlMin" Runat="server" Width="50" CssClass="signup">
							<asp:ListItem Value="00">00</asp:ListItem>
							<asp:ListItem Value="15">15</asp:ListItem>
							<asp:ListItem Value="30">30</asp:ListItem>
							<asp:ListItem Value="45">45</asp:ListItem>
						</asp:dropdownlist></td>
	
				</tr>
				<tr>
					<td>
						<br>
					</td>
				</tr>
				<tr>
					<td class="normal1" align="right">Description
					</td>
					<td colSpan="7"><asp:textbox id="txtDesc" CssClass=signup Runat="server" Width="405" Height="50" TextMode="MultiLine"></asp:textbox></td>
				</tr>
			</table>
			<asp:TextBox ID=txtItemID runat=server style="display:none"></asp:TextBox>
		</form>
	</body>
</HTML>
