Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Partial Public Class frmLinkedProjects
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim objOpportunity As New MOpportunity
        objOpportunity.OpportunityId = GetQueryStringVal(Request.QueryString("enc"), "opId")
        Dim dtTable As New DataTable
        dtTable = objOpportunity.GetLinkedProjects
        dgLinkProjects.DataSource = dtTable
        dgLinkProjects.DataBind()

    End Sub

End Class