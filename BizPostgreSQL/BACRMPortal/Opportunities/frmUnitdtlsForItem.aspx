<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmUnitdtlsForItem.aspx.vb" Inherits="BACRMPortal.frmUnitdtlsForItem" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title></title>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<asp:datagrid id="dgUnits" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
				BorderColor="white">
				<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
				<ItemStyle CssClass="is"></ItemStyle>
				<HeaderStyle CssClass="hs"></HeaderStyle>
				<Columns>
					<asp:BoundColumn DataField="numQtyOnHand" DataFormatString="{0:#,##0}"  HeaderText="On Hand"></asp:BoundColumn>
					<asp:BoundColumn DataField="numQtyOnOrder" DataFormatString="{0:#,##0}" HeaderText="On Order"></asp:BoundColumn>
					<asp:BoundColumn DataField="Percentage" HeaderText="On Allocation"></asp:BoundColumn>
					<asp:BoundColumn DataField="numQtyBackOrder" DataFormatString="{0:#,##0}" HeaderText="On Backorder"></asp:BoundColumn>
				</Columns>
			</asp:datagrid>
		</form>
	</body>
</HTML>
