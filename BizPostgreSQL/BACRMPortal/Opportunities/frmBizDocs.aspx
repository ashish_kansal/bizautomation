<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmBizDocs.aspx.vb" Inherits="BACRMPortal.frmBizDocs" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>BizDocs</title>
		<script language="javascript">
		
			function Close()
			{
				window.close();
				return false;
			}
			function AddBizDocs(a)
			{
				var str;
				str='';
				var stri;
				for (i = 1; i < dgBizDocs.rows.length; i++)
				{
				    if (i<10)
				        {
				           stri='0'+(i+1)
				        }
				         else 
				         {
				            stri=i+1
				         }
				str=str + document.all['dgBizDocs_ctl'+(stri)+'_lblBizDocID'].innerText+',';
				}
	
				window.location.href("frmAddBizDocs.aspx?OpID="+a+"&BizDocIDs="+str)
				return false;
			}
			function OpenBizInvoice(a,b)
			{
				window.open('../Opportunities/frmBizInvoice.aspx?OpID='+a + '&OppBizId='+b,'','toolbar=no,titlebar=no,menubar=yes,left=100,width=680,height=700,scrollbars=yes,resizable=yes');
				return false;
			}
				function DeleteRecord()
		{
			if(confirm('Are you sure, you want to delete the selected record?'))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		function DeleteMessage()
		{
			alert("You Are not Authorized to Delete the Selected Record !");
			return false;
		}
		</script>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
			<table cellspacing="0" cellpadding="0" width="100%" border="0">
				<tr>
					
					<td align="right"><asp:button id="btnAdd" Runat="server" Text="Add BizDoc" CssClass="button"></asp:button>
					</td>
				</tr>
				<tr>
					<td colSpan="3" valign="top">
					<br />
					<asp:table id="tblMile" Runat="server" BorderColor="black" GridLines="None" Width="100%" cellspacing="0"
							cellpadding="0" BorderWidth="1" CssClass="aspTable"  Height="250">
							<asp:tableRow>
								<asp:tableCell VerticalAlign="Top" >
								
									<asp:datagrid id="dgBizDocs" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
										BorderColor="white">
										<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
										<ItemStyle CssClass="is"></ItemStyle>
										<HeaderStyle CssClass="hs"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn HeaderText="BizDoc">
												<ItemTemplate>
													<asp:HyperLink ID=hplBizdcoc Runat=server CssClass=hyperlink Text='<%# DataBinder.Eval(Container.DataItem, "BizDoc") %>'>
													</asp:HyperLink>
													<asp:Label ID=lblBizDocID Runat=server style="display:none" Text='<%# DataBinder.Eval(Container.DataItem, "numBizDocId") %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Date Created">
												<ItemTemplate>
													<%# ReturnName(DataBinder.Eval(Container.DataItem, "CreatedDate")) %>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn HeaderText="Opportunty/Deal Name" DataField="vcPOppName"></asp:BoundColumn>
											<asp:BoundColumn HeaderText="ID.#" DataField="vcBizDocID"></asp:BoundColumn>
											<asp:BoundColumn HeaderText="P.O." DataField="vcPurchaseOdrNo"></asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Amount">
												<ItemTemplate>
													<%# CalTotalAmt(DataBinder.Eval(Container.DataItem, "numOppBizDocsId")) %>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn>
												<HeaderTemplate>
													<asp:Button ID="btnHdelete" Runat="server" CssClass="Delete" Text="r"></asp:Button>
												</HeaderTemplate>
												<ItemTemplate>
													<asp:TextBox ID="txtBizDocID" Runat="server" style="display:none" Text='<%# DataBinder.Eval(Container.DataItem, "numOppBizDocsId") %>'>
													</asp:TextBox>
													<asp:Button ID="btnDelete" Runat="server" CssClass="Delete" Text="r" CommandName="Delete"></asp:Button>
													<asp:LinkButton ID="lnkDelete" Runat="server" Visible="false">
														<font color="#730000">*</font></asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid>
								</asp:tableCell>
							</asp:tableRow>
						</asp:table></td>
				</tr>
			</table>
			<table width="100%">
				<tr>
					<td class="normal4" align="center"><asp:literal id="litMessage" Runat="server"></asp:literal></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
