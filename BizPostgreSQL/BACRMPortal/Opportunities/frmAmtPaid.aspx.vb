'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Opportunities


Partial Class frmAmtPaid
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not IsPostBack Then
                txtAmount.Text = GetQueryStringVal(Request.QueryString("enc"), "Amt")
            End If
            btnCancel.Attributes.Add("onclick", "return WindowClose()")
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub


    Private Sub btnSaveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            Dim objOppInvoice As New OppInvoice(Session("UserContactID"))
            objOppInvoice.AmtPaid = txtAmount.Text
            objOppInvoice.UserId = Session("UserContactID")
            objOppInvoice.OppBizDocId = GetQueryStringVal(Request.QueryString("enc"), "OppBizDocID")
            objOppInvoice.UpdateAmountPaid()
            Response.Write("<script>opener.location.reload(true); self.close();</script>")
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub


End Class


