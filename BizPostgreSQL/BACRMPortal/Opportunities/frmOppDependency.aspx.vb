'***************************************************************************************************************************
'     Author Name				 :  Anoop Jayaraj
'     Date Written				 :  18/2/2005
'***************************************************************************************************************************

Imports BACRM.BusinessLogic.Opportunities


Partial Class frmOppDependency
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            If Not IsPostBack Then
                Dim objDependency As New Dependency(Session("UserContactID"))
                Getdetails()
            End If
            btnCancel.Attributes.Add("onclick", "return Close();")
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Sub Getdetails()
        Try
            Dim objDependency As New Dependency(Session("UserContactID"))
            Dim dtDependncy As New DataTable
            objDependency.OpporunityID = GetQueryStringVal(Request.QueryString("enc"), "Opid")
            objDependency.OppStageId = GetQueryStringVal(Request.QueryString("enc"), "OPPStageID")
            dtDependncy = objDependency.GetDependencyDtls
            Session("Dependency") = dtDependncy
            BindDependency()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Sub BindDependency()
        Try
            Dim dtDependncy As New Datatable
            dtDependncy = Session("Dependency")
            dgDependency.DataSource = dtDependncy
            dgDependency.DataBind()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub
    
End Class
