'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Partial Class frmBizInvoice
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents hplTerms As System.Web.UI.WebControls.HyperLink
    Protected WithEvents lblAddress As System.Web.UI.WebControls.Label
    Protected WithEvents Button1 As System.Web.UI.WebControls.Button
    Protected WithEvents Button2 As System.Web.UI.WebControls.Button
    Dim lngBizDocID, lngOppId As Long

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            btnClose.Attributes.Add("onclick", "return Close()")
            lngBizDocID = GetQueryStringVal(Request.QueryString("enc"), "OppBizId")
            lngOppId = GetQueryStringVal(Request.QueryString("enc"), "OpID")
            If Not IsPostBack Then
                getAddDetails()
                getBizDocDetails()
            End If
            btnPrint.Attributes.Add("onclick", "return Print();")
            txtDisc.Attributes.Add("onkeypress", "return CheckDisc('txtDisc','')")
            btnApprovers.Attributes.Add("onclick", "return openApp(" & lngBizDocID & ",'B')")
            hplBizDocAtch.Attributes.Add("onclick", "OpenAtch(" & lngOppId & "," & lngBizDocID & "," & Session("DomainID") & "," & Session("UserContactID") & ")")
            If GetQueryStringVal(Request.QueryString("enc"), "DomainID") <> "" Then
                tblButtons.Visible = False
            End If
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Sub getBizDocDetails()
        Try
            Dim dtOppBiDocDtl As New Datatable
            Dim objOppBizDocs As New OppBizDocs
            objOppBizDocs.OppBizDocId = lngBizDocID
            objOppBizDocs.OppId = lngOppId
            objOppBizDocs.DomainID = IIf(Session("DomainID") Is Nothing, GetQueryStringVal(Request.QueryString("enc"), "DomainID"), Session("DomainID"))
            objOppBizDocs.UserCntID = IIf(Session("UserContactID") Is Nothing, GetQueryStringVal(Request.QueryString("enc"), "ConID"), Session("UserContactID"))
            dtOppBiDocDtl = objOppBizDocs.GetOppBizDocDtl
            If dtOppBiDocDtl.Rows.Count <> 0 Then
                If Not IsPostBack Then
                    If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("decDiscount")) Then
                        txtDisc.Text = Math.Round(dtOppBiDocDtl.Rows(0).Item("decDiscount"), 2)
                    Else
                        txtDisc.Text = "0.00"
                    End If
                End If
                txtPO.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("vcPurchaseOdrNo")), "", dtOppBiDocDtl.Rows(0).Item("vcPurchaseOdrNo"))
                If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("monAmountPaid")) Then
                    lblAmountPaid.Text = String.Format("{0:#,##0.00}", dtOppBiDocDtl.Rows(0).Item("monAmountPaid"))
                Else
                    lblAmountPaid.Text = "0.00"
                End If
                If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("BizdocFooter")) Then
                    imgFooter.Visible = True
                    imgFooter.ImageUrl = "../documents/docs/Footer.gif"
                Else
                    imgFooter.Visible = False
                End If

                txtComments.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("vcComments")), "", dtOppBiDocDtl.Rows(0).Item("vcComments"))
                lblID.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("vcBizDocID")), "", dtOppBiDocDtl.Rows(0).Item("vcBizDocID"))
                lblcreated.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("numCreatedby")), "", dtOppBiDocDtl.Rows(0).Item("numCreatedby")) & "  &nbsp;" & IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("dtCreatedDate")), "", dtOppBiDocDtl.Rows(0).Item("dtCreatedDate"))
                lblModifiedby.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("numModifiedBy")), "", dtOppBiDocDtl.Rows(0).Item("numModifiedBy")) & "  &nbsp;" & IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("dtModifiedDate")), "", dtOppBiDocDtl.Rows(0).Item("dtModifiedDate"))
                lblviewwedby.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("numViewedBy")), "", dtOppBiDocDtl.Rows(0).Item("numViewedBy")) & "  &nbsp;" & IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("dtViewedDate")), "", dtOppBiDocDtl.Rows(0).Item("dtViewedDate"))
                lblDate.Text = FormattedDateFromDate(dtOppBiDocDtl.Rows(0).Item("dtCreatedDate"), Session("DateFormat"))
                hplTermsName.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("vcData")), "", dtOppBiDocDtl.Rows(0).Item("vcData"))
                lblTerms.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("numTerms")), 0, dtOppBiDocDtl.Rows(0).Item("numTerms"))
                If dtOppBiDocDtl.Rows(0).Item("tintBillingTerms") = 1 Then
                    If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("bintAccountClosingDate")) Then
                        Dim strDate As Date
                        strDate = DateAdd(DateInterval.Day, dtOppBiDocDtl.Rows(0).Item("numBillingDays"), dtOppBiDocDtl.Rows(0).Item("bintAccountClosingDate"))
                        lblDuedate.Text = FormattedDateFromDate(strDate, Session("DateFormat"))
                    End If
                    lblBillingTerms.Text = "Net " & dtOppBiDocDtl.Rows(0).Item("numBillingDays") & " , " & IIf(dtOppBiDocDtl.Rows(0).Item("tintInterestType") = 0, "-", "+") & dtOppBiDocDtl.Rows(0).Item("fltInterest") & " %"
                Else
                    If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("bintAccountClosingDate")) Then
                        lblDuedate.Text = FormattedDateFromDate(dtOppBiDocDtl.Rows(0).Item("bintAccountClosingDate"), Session("DateFormat"))
                    End If
                    lblBillingTerms.Text = "-"
                End If
                If dtOppBiDocDtl.Rows(0).Item("tintFinal") = 1 Then
                    trApprove.Visible = True
                    lblApprovedBy.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("ApprovedBy")), "", dtOppBiDocDtl.Rows(0).Item("ApprovedBy")) & "  &nbsp;" & IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("dtApprovedDate")), "", dtOppBiDocDtl.Rows(0).Item("dtApprovedDate"))
                Else
                    trApprove.Visible = False
                End If
                If dtOppBiDocDtl.Rows(0).Item("AppReq") = 1 Then
                    pnlApprove.Visible = True
                Else
                    pnlApprove.Visible = False
                End If
            End If
            Dim ds As New DataSet
            Dim dtOppBiDocItems As New DataTable
            objOppBizDocs.DomainID = IIf(Session("DomainID") Is Nothing, GetQueryStringVal(Request.QueryString("enc"), "DomainID"), Session("DomainID"))
            objOppBizDocs.OppBizDocId = lngBizDocID
            objOppBizDocs.OppId = lngOppId
            ds = objOppBizDocs.GetOppInItems
            dtOppBiDocItems = ds.Tables(0)
            dtOppBiDocItems.Columns.Add("TotalTax", GetType(Decimal))
            Dim decTotalAmount As Decimal
            Dim decTaxAmount As Decimal
            Dim i As Integer
            For i = 0 To dtOppBiDocItems.Rows.Count - 1
                decTotalAmount = decTotalAmount + dtOppBiDocItems.Rows(i).Item("amount")
                decTaxAmount = decTaxAmount + dtOppBiDocItems.Rows(i).Item("amount") * dtOppBiDocItems.Rows(i).Item("Tax") / 100
            Next
            Dim decDisc, decInterest As Decimal
            If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("bintAccountClosingDate")) Then
                If dtOppBiDocDtl.Rows(0).Item("tintBillingTerms") = 1 Then
                    Dim strDate1 As String

                    strDate1 = DateAdd(DateInterval.Day, dtOppBiDocDtl.Rows(0).Item("numBillingDays"), dtOppBiDocDtl.Rows(0).Item("bintAccountClosingDate"))
                    strDate1 = Format(CDate(strDate1), "yyyyMMdd")

                    If dtOppBiDocDtl.Rows(0).Item("tintInterestType") = 0 Then
                        If strDate1 > Format(Now(), "yyyyMMdd") Then
                            decDisc = 0
                        Else
                            decDisc = dtOppBiDocDtl.Rows(0).Item("fltInterest")
                        End If
                    Else
                        If strDate1 > Format(Now(), "yyyyMMdd") Then
                            decInterest = 0
                        Else
                            decInterest = dtOppBiDocDtl.Rows(0).Item("fltInterest")
                        End If
                    End If
                End If
            End If
            Dim decDiscAmount, decTotalDiscount, decTotalAmtDisc As Decimal

            If txtDisc.Text <> "" Then
                Try

                    decDiscAmount = decTotalAmount * (100 - txtDisc.Text) / 100
                    If lblShipping.Text = 0 Then
                        pnlShipping.Visible = False
                    End If
                    If decDisc > 0 Then
                        decTotalDiscount = decTotalAmount * txtDisc.Text / 100 + decDiscAmount * decDisc / 100
                        decTotalAmtDisc = decTotalAmount - decTotalDiscount + lblShipping.Text + decTaxAmount
                        lblLateCharge.Text = "0.00"
                    Else
                        decTotalDiscount = decTotalAmount * txtDisc.Text / 100
                        If decInterest * decDiscAmount = 0 Then
                            lblLateCharge.Text = "0.00"
                        Else
                            lblLateCharge.Text = decInterest * decDiscAmount / 100
                        End If
                        decTotalAmtDisc = decTotalAmount + decInterest * decDiscAmount / 100 - decTotalDiscount + lblShipping.Text + decTaxAmount
                    End If
                    trDisc.Visible = True
                Catch ex As Exception

                End Try

            Else
                If decDisc > 0 Then
                    decTotalDiscount = decTotalAmount * decDisc / 100
                    decTotalAmtDisc = decTotalAmount - decTotalDiscount + lblShipping.Text + decTaxAmount
                    lblLateCharge.Text = "0.00"
                Else
                    decTotalDiscount = "0.00"
                    decTotalAmtDisc = decTotalAmount + decTotalAmount * decInterest / 100 + lblShipping.Text + decTaxAmount
                End If
                trDisc.Visible = False
            End If
            lblTotAmtDisc.Text = String.Format("{0:#,##0.00}", decTotalAmtDisc)
            If decTotalDiscount = 0 Then
                lblDiscount.Text = "0.00"
            Else
                lblDiscount.Text = String.Format("{0:#,##0.00}", decTotalDiscount)
            End If
            If decTaxAmount = 0 Then
                lblTaxAmount.Text = "0.00"
            Else
                lblTaxAmount.Text = String.Format("{0:#,##0.00}", decTaxAmount)
            End If
            lblBalance.Text = String.Format("{0:#,##0.00}", decTotalAmtDisc - lblAmountPaid.Text)
            lblTotAmt.Text = String.Format("{0:#,##0.00}", decTotalAmount)
            Session("Itemslist") = dtOppBiDocItems

            '''Add columns to datagrid
            Dim dtdgColumns As New DataTable
            dtdgColumns = ds.Tables(2)
            Dim bColumn As BoundColumn
            For i = 0 To dtdgColumns.Rows.Count - 1
                bColumn = New BoundColumn
                bColumn.HeaderText = dtdgColumns.Rows(i).Item("vcFormFieldName")
                bColumn.DataField = dtdgColumns.Rows(i).Item("vcDbColumnName")
                If dtdgColumns.Rows(i).Item("vcFieldDataType") = "M" Then
                    bColumn.DataFormatString = "{0:#,##0.00}"
                End If
                dgBizDocs.Columns.Add(bColumn)
            Next
            BindItems()
            objOppBizDocs.ContactID = IIf(Session("UserContactID") Is Nothing, GetQueryStringVal(Request.QueryString("enc"), "ConID"), Session("UserContactID"))
            objOppBizDocs.UpdateBizDocsViewdDtls()
        Catch ex As Exception
            Response.Write(ex)
        End Try

    End Sub



    Sub BindItems()
        Try
            Dim dtItems As New DataTable
            dtItems = Session("Itemslist")
            dgBizDocs.DataSource = dtItems
            dgBizDocs.DataBind()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Sub getAddDetails()
        Try
            Dim dtOppBizAddDtl As New Datatable
            Dim objOppBizDocs As New OppBizDocs
            objOppBizDocs.OppBizDocId = lngBizDocID
            objOppBizDocs.OppId = lngOppId
            dtOppBizAddDtl = objOppBizDocs.GetOppInAddDtl
            If dtOppBizAddDtl.Rows.Count <> 0 Then
                'lblAddress.Text = IIf(IsDBNull(dtOppBizAddDtl.Rows(0).Item("CompName")), "", dtOppBizAddDtl.Rows(0).Item("CompName"))
                lblBillTo.Text = IIf(IsDBNull(dtOppBizAddDtl.Rows(0).Item("BillAdd")), "", dtOppBizAddDtl.Rows(0).Item("BillAdd"))
                lblShipTo.Text = IIf(IsDBNull(dtOppBizAddDtl.Rows(0).Item("ShipAdd")), "", dtOppBizAddDtl.Rows(0).Item("ShipAdd"))
                lblOppID.Text = IIf(IsDBNull(dtOppBizAddDtl.Rows(0).Item("OppName")), "", dtOppBizAddDtl.Rows(0).Item("OppName"))
                lblBizDoc.Text = IIf(IsDBNull(dtOppBizAddDtl.Rows(0).Item("BizDcocName")), "", dtOppBizAddDtl.Rows(0).Item("BizDcocName"))
                lblShipping.Text = String.Format("{0:#,##0.00}", dtOppBizAddDtl.Rows(0).Item("ShipAmount"))
                txtBizDoc.Text = dtOppBizAddDtl.Rows(0).Item("BizDoc")
                txtBizDocRecOwner.Text = dtOppBizAddDtl.Rows(0).Item("BizDocOwner")
                objOppBizDocs.BizDocId = dtOppBizAddDtl.Rows(0).Item("BizDoc")
                objOppBizDocs.DomainID = Session("DomainID")
                Dim i As Integer
                i = objOppBizDocs.GetBizDocAttachments.Rows.Count
                hplBizDocAtch.Text = "Attachments(" & IIf(i = 0, 1, i) & ")"
            End If
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        If lngBizDocID > 0 Then
            Dim objDocuments As New DocumentList
            objDocuments.GenDocID = lngBizDocID
            objDocuments.ContactID = Session("UserContactID")
            objDocuments.CDocType = "B"
            objDocuments.byteMode = 3
            objDocuments.Comments = txtComment.Text
            objDocuments.ManageApprovers()
            pnlApprove.Visible = False
            Try
                Dim objSendMail As New clsSendEmail
                Dim objCommon As New CCommon
                objCommon.ContactID = txtBizDocRecOwner.Text
                objSendMail.SendSimpleEmail(Session("ContactName") & " just approved the document " & lblID.Text, "", "", Session("UserEmail"), objCommon.GetContactsEmail)
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub btnDecline_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDecline.Click
        If lngBizDocID > 0 Then
            Dim objDocuments As New DocumentList
            objDocuments.GenDocID = lngBizDocID
            objDocuments.ContactID = Session("UserContactID")
            objDocuments.CDocType = "B"
            objDocuments.byteMode = 4
            objDocuments.Comments = txtComment.Text
            objDocuments.ManageApprovers()
            pnlApprove.Visible = False
            Try
                Dim objSendMail As New clsSendEmail
                Dim objCommon As New CCommon
                objCommon.ContactID = txtBizDocRecOwner.Text
                objSendMail.SendSimpleEmail(Session("ContactName") & " just declined the document " & lblID.Text, "", "", Session("UserEmail"), objCommon.GetContactsEmail)
            Catch ex As Exception

            End Try
        End If
    End Sub
End Class

