Imports BACRM.BusinessLogic.Item
    Partial Class frmItemPriceRecommd
        Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If Not IsPostBack Then
                Dim objItems As New CItems
            objItems.ItemCode = GetQueryStringVal(Request.QueryString("enc"), "Itemcode")
            objItems.NoofUnits = IIf(GetQueryStringVal(Request.QueryString("enc"), "Unit") = "", 0, GetQueryStringVal(Request.QueryString("enc"), "Unit"))
            objItems.OppId = GetQueryStringVal(Request.QueryString("enc"), "OppID")
            Dim dtItemPricemgmt As New Datatable
            dtItemPricemgmt = objItems.GetPriceManagementList
            dgItemPricemgmt.DataSource = dtItemPricemgmt
            dgItemPricemgmt.DataBind()
        End If
        btnClose.Attributes.Add("onclick", "return Close();")
    End Sub

    Function ReturnName(ByVal bintCreatedDate) As String
        Dim strCreateDate As String = ""
        If Not IsDBNull(bintCreatedDate) Then
            strCreateDate = FormattedDateFromDate(bintCreatedDate, Session("DateFormat"))
        End If

        Return strCreateDate
    End Function

    Private Sub dgItemPricemgmt_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgItemPricemgmt.ItemDataBound
        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
            Dim lbl As Label
            lbl = e.Item.FindControl("lblLstPrice")
            Dim btnAdd As Button
            btnAdd = e.Item.FindControl("btnAdd")
            If GetQueryStringVal(Request.QueryString("enc"), "Type") = "Edit" Then
                btnAdd.Attributes.Add("onclick", "return AccessParent('" & String.Format("{0:#,###.00}", CDec(lbl.Text)) & "','2','" & GetQueryStringVal(Request.QueryString("enc"), "txtName").Trim & "')")
            Else
                btnAdd.Attributes.Add("onclick", "return AccessParent('" & String.Format("{0:#,###.00}", CDec(lbl.Text)) & "','1','')")
            End If

        End If
    End Sub
    End Class

