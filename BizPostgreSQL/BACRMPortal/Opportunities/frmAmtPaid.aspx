<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmAmtPaid.aspx.vb" Inherits="BACRMPortal.frmAmtPaid" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Amount Paid</title>
		<script>
			function WindowClose()
			{
				window.close();
				return false;
			}
			function Close()
			{
				window.close();
			}
		</script>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
			<table width="100%">
				<tr>
					<td align="right" colSpan="2">
						<asp:Button ID="btnSaveClose" Runat="server" CssClass="button" Text="Save &amp; Close"></asp:Button>
						<asp:Button ID="btnCancel" Runat="server" CssClass="button" Text="Cancel"></asp:Button>
						<br>
						<br>
					</td>
				</tr>
				<tr>
					<td class="normal1" align="right">Amount Paid:</td>
					<td><asp:TextBox ID="txtAmount" Runat="server" class="signup"></asp:TextBox></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
