'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Opportunities



Partial Class frmOppExpense
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    ''Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
    ''    Try
    ''        If Not IsPostBack Then
    ''            Dim objOppTimeExpense As New OppTimeExpense(Session("UserContactID"))
    ''            Dim dtBizDoc As New DataTable
    ''            dtBizDoc = objOppTimeExpense.GetBizDoc(ConfigurationManager.AppSettings("ConnectionString"))
    ''            ddlBizDoc.DataSource = dtBizDoc
    ''            ddlBizDoc.DataValueField = "numListItemID"
    ''            ddlBizDoc.DataTextField = "vcData"
    ''            ddlBizDoc.DataBind()
    ''            ddlBizDoc.Items.Insert(0, "--Select One--")
    ''            ddlBizDoc.Items.FindByText("--Select One--").Value = 0
    ''            getdetails()
    ''        End If
    ''        btnCancel.Attributes.Add("onclick", "return Close();")
    ''    Catch ex As Exception
    ''        Response.Write(ex)
    ''    End Try
    ''End Sub

    ''Sub getdetails()
    ''    Try
    ''        Dim objOppTimeExpense As New OppTimeExpense(Session("UserContactID"))
    ''        Dim dtExpenseDetails As New DataTable
    ''        objOppTimeExpense.OppID = Request("Opid")
    ''        objOppTimeExpense.OppStageID = Request("OPPStageID")
    ''        dtExpenseDetails = objOppTimeExpense.GetExpenseDetails
    ''        If dtExpenseDetails.Rows.Count <> 0 Then
    ''            ddlBizDoc.Items.FindByValue(dtExpenseDetails.Rows(0).Item("numBizDocId")).Selected = True
    ''            txtAmount.Text = String.Format("{0:#,##0.00}", dtExpenseDetails.Rows(0).Item("monAmount"))
    ''            txtDesc.Text = IIf(IsDBNull(dtExpenseDetails.Rows(0).Item("vcDesc")), "", dtExpenseDetails.Rows(0).Item("vcDesc"))
    ''        End If
    ''    Catch ex As Exception
    ''        Response.Write(ex)
    ''    End Try
    ''End Sub


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not IsPostBack Then

                getdetails()
            End If
            btnCancel.Attributes.Add("onclick", "return Close();")
            btnSave.Attributes.Add("onclick", "return Save();")

        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Sub getdetails()
        Try
            Dim objOppTimeExpense As New OppTimeExpense
            Dim dtExpenseDetails As New DataTable
            objOppTimeExpense.OppID = GetQueryStringVal(Request.QueryString("enc"), "Opid")
            objOppTimeExpense.OppStageID = GetQueryStringVal(Request.QueryString("enc"), "OPPStageID")
            dtExpenseDetails = objOppTimeExpense.GetExpenseDetails
            If dtExpenseDetails.Rows.Count <> 0 Then
                txtAmount.Text = String.Format("{0:#,##0.00}", dtExpenseDetails.Rows(0).Item("monAmount"))
                txtDesc.Text = IIf(IsDBNull(dtExpenseDetails.Rows(0).Item("vcDesc")), "", dtExpenseDetails.Rows(0).Item("vcDesc"))
            End If
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Sub saveDetails()
        Try
            Dim objOppTimeExpense As New OppTimeExpense(Session("UserContactID"))
            objOppTimeExpense.OppID = GetQueryStringVal(Request.QueryString("enc"), "Opid")
            objOppTimeExpense.OppStageID = GetQueryStringVal(Request.QueryString("enc"), "OPPStageID")
            objOppTimeExpense.Amount = CInt(txtAmount.Text)
            objOppTimeExpense.Desc = txtDesc.Text
            objOppTimeExpense.SaveExpenseDetails()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub


    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            saveDetails()
            Response.Write("<script>window.close();</script>")
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btndelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelete.Click
        Dim objOppTimeExpense As New OppTimeExpense(Session("UserContactID"))
        objOppTimeExpense.OppID = GetQueryStringVal(Request.QueryString("enc"), "Opid")
        objOppTimeExpense.OppStageID = GetQueryStringVal(Request.QueryString("enc"), "OPPStageID")
        objOppTimeExpense.DeleteExpenseDetails()
        txtAmount.Text = ""
        txtDesc.Text = ""
    End Sub

End Class


