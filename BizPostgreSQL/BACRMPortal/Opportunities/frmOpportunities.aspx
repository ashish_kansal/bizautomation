<%@ Register TagPrefix="menu1" TagName="webmenu" src="../common/topbar.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmOpportunities.aspx.vb" Inherits="BACRMPortal.frmOpportunities"%>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebTab.v7.3, Version=7.3.20073.38, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebTab" TagPrefix="igtab" %>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebGrid.v7.3, Version=7.3.20073.38, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
   Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" src="../common/calandar.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1"    runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Opportunities</title>
		<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
         <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)"> 

		<script language="javascript" type="text/javascript">
		function openTrackAsset(a,b)
		{
		    window.open("../opportunities/frmTrackAsset.aspx?opId="+a+"&DivId="+b,'','toolbar=no,titlebar=no,left=300,top=200,width=800,height=500,scrollbars=yes,resizable=yes')
		    return false;
		}
		function fn_GoToURL(varURL)
		{
			
			if ((varURL!='') && (varURL.substr(0,7)=='http://') && (varURL.length > 7))
			{
				var LoWindow=window.open(varURL,"","");
				LoWindow.focus();
			}
			return false;
		}
		function ShowlinkedProjects(a)
		        {
		        window.open("../opportunities/frmLinkedProjects.aspx?opId="+a,'','toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')
		        return false;
		        }
			function DeleteRecord()
				{
					if(confirm('Are you sure, you want to delete the selected record?'))
					{
						return true;
					}
					else
					{
						return false;
					}
				}
		function DeletMsg()
			{
				var bln=confirm("You�re about to remove the Stage from this Process, all stage data will be deleted")
				if (bln==true)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		function OpenDocuments(a)
		{
			window.open("../Documents/frmSpecDocuments.aspx?Type=O&RecID="+a,'','toolbar=no,titlebar=no,top=200,width=700,height=450,left=200,scrollbars=yes,resizable=yes')
			return false;
		}
		function DealCompleted()
		{
		
			alert("This Deal will now be removed from the 'Open Deals' section, and will reside only in the 'Closed Deals' section within the Organization the deal is for. Except for BizDocs or any Projects that depend on BizDocs - Modifications to Deal Details, Milestones & Stages, Associated Contacts, and Products / Services, will no longer be allowed.")
			return false;
		}
		function CannotShip()
		{
		
			alert("You can't ship at this time because you don't have enough quantity on hand to support your shipment. Your options are to modify your order, or replenish inventory (to check inventory click on the edit link within the line item, then the value in the 'Products/Services' column):")
			return false;
		}
		function AlertMsg()
		{
			if (confirm("Please note that after a 'Received' or 'Shipped' request is executed, except for the BizDocs and any Projects that depend on BizDocs - Additional changes will not be permitted on this Deal (i.e. it will be frozen). Do you wish to continue ?")) 
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		function OpenImage(a)
		{
			window.open('../opportunities/frmFullImage.aspx?ItemCode='+a,'','toolbar=no,titlebar=no,left=300, top=100,width=600,height=500,scrollbars=yes,resizable=yes');
			return false;
		}
		function Update(ddl,txt,txtPrice)
		{
			if (ddl.value==0)
			{
				alert("Select Item")
				ddl.focus()
				return false;
			}
			if (txt.value=='')
			{
				alert("Enter Units")
				txt.focus()
				return false;
			}
			if (txtPrice.value=='')
			{
				alert("Enter Price")
				txtPrice.focus()
				return false;
			}
		}

		function AddPrice(a,b,c)
		{
			if (b==1)
			{
			document.Form1.txtprice.value=a;
			return false;
			}
			else
			{
				document.all('uwOppTab__ctl3_'+c).value=a;
				return false;
			}
		}
		function openCompetition(a)
		{
		window.open("../opportunities/frmCompetition.aspx?ItemCode="+a,'','toolbar=no,titlebar=no,left=300,top=450,width=700,height=150,scrollbars=yes,resizable=yes')
			/*document.all['IframeComp'].src="../opportunity/frmCompetition.aspx?ItemCode="+a ;
			document.all['divCompetition'].style.visibility = "visible";*/
			return false;
		}
	
		function openUnit(a)
		{
			a=document.all[a].value
			window.open('../opportunities/frmUnitdtlsForItem.aspx?ItemCode='+a,'','toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')
		}
        function openItem(a,b,c,d)
		{
		    if (document.all[a].selectedIndex<1)
		    {
		        a=0
		    }
		    else
		    {
		        a=document.all[a].value
		    }
		    if (document.all[d].value==1)
		    {
		        b=0
		    }
		    if ((document.all[d].value==1) && (typeof(document.all['uwItemSel'])!='undefined'))
		    {
		        b=0
		        var grid = igtbl_getGridById('uwItemSel');
	            var i;
	            var j;
		        for(i=0; i < grid.Rows.length;i++)
	            {
		            var row = grid.Rows.getRow(i);
		            var ChildRows = row.getChildRows();
		            if(ChildRows!=null)
		            {
			            for (j=0; j < ChildRows.length; j++)
			            {
		              
		                        var childRow = ChildRows[j];
                                //alert(childRow.getCell(0))
                                //var test =igtbl_getElementById(childRow.id)
                                //alert(test)
                            
                                var Test= igtbl_getRowById(childRow.id)
                                if  (Test.getCellFromKey("Select").getValue()=='true')
                                {
                                    b=b+1
                                }
                                //rows.getRow(0).getCell(0).setValue(check);

			            }
			            
		            }				
	            }

		    }
		    else if (document.all[d].value==0)
		    {
		          if (document.all[b].value=="")
		            {
		                b=0
		            }
		            else
		            {
		                b=document.all[b].value
		            }
		    }
		    if (a!=0 && b!=0)
		    {
		        window.open('../opportunities/frmItemPriceRecommd.aspx?ItemCode='+a+'&Unit='+b+'&DivID='+c+'&Type=Edit','','toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')
		
		    }
		}
		function openOptItem(a,b,c)
		{
		    if (document.all[a].selectedIndex<1)
		    {
		        a=0
		    }
		    else
		    {
		        a=document.all[a].value
		    }
		          if (document.all[b].value=="")
		            {
		                b=0
		            }
		            else
		            {
		                b=document.all[b].value
		            }
		    if (a!=0 && b!=0)
		    {
		        window.open('../opportunities/frmItemPriceRecommd.aspx?ItemCode='+a+'&Unit='+b+'&DivID='+c+'&OptItem='+1+'&Type=Edit','','toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')
		
		    }
		}
		function ShowWindow(Page,q,att) 
		{
		
			if (att=='show')
			{
				document.all[Page].style.visibility = "visible";
				return false;
		
			}
			if (att=='hide')
			{
				document.all[Page].style.visibility = "hidden";
				return false;
		
			}
		}
			function CheckNumber(cint)
					{
						if (cint==1)
						{
							if (!(window.event.keyCode > 47 && window.event.keyCode < 58 || window.event.keyCode==44 || window.event.keyCode==46))
							{
								window.event.keyCode=0;
							}
						}
						if (cint==2)
						{
							if (!(window.event.keyCode > 47 && window.event.keyCode < 58))
							{
								window.event.keyCode=0;
							}
						}
						
					}
		function Save(cint)
		{
		
		if (cint==1)
		{
			if (document.Form1.ddlCompanyName.value==0)
			{
				alert("Select Customer");
				document.Form1.uwOppTab.tabIndex=0;
				document.Form1.ddlCompanyName.focus();
				return false;
			}
			if (document.Form1.ddlTaskContact.selectedIndex==0)
			{
				alert("Select Contact");
				document.Form1.uwOppTab.tabIndex=0;
				document.Form1.ddlTaskContact.focus();
				return false;
			}
		}
	
		if (document.Form1.uwOppTab__ctl0_calDue_txtDate.value=='')
			{
				alert("Enter Due Date");
				document.Form1.uwOppTab.tabIndex=0;
				return false;
			}
		if (typeof(document.all['uwOppTab__ctl1_chkDClosed'])!='undefined')
		{
			if (document.all['uwOppTab__ctl1_chkDClosed'].checked==true)
				{
				    if (document.Form1.all['uwOppTab__ctl1_ddlClReason'].value==0)
						{
							alert("Select Conclusion Analysis")
							document.Form1.uwOppTab.tabIndex=1;
							document.Form1.all['uwOppTab__ctl1_ddlClReason'].focus()
							return false;
						}
				}
		}
		if (typeof(document.all['uwOppTab__ctl1_chkDlost'])!='undefined')
		{
			if (document.all['uwOppTab__ctl1_chkDlost'].checked==true)
				{
					if (document.Form1.all['uwOppTab__ctl1_ddlClReason'].value==0)
						{
							alert("Select Conclusion Analysis")
							document.Form1.uwOppTab.tabIndex=1;
							document.Form1.all['uwOppTab__ctl1_ddlClReason'].focus()
							return false;
						}
					
				}
		}		
	
		}
		function AddContact()
		{
			if (document.Form1.ddlcompany.value==0)
			{
				alert("Select Customer");
				document.Form1.uwOppTab.tabIndex=2;
				document.Form1.ddlcompany.focus();
				return false;
			}
			if (document.Form1.ddlAssocContactId.value==0)
			{
				alert("Select Contact");
				document.Form1.uwOppTab.tabIndex=2;
				document.Form1.ddlAssocContactId.focus();
				return false;
			}
			var str;
			for(i=0;i<document.Form1.elements.length;i++)
				{
				if (i<=9)
				{
				    str='0'+(i+1)
				}
				else
				{
				    str=i+1
				}
				if (typeof(document.Form1.all['dgContact_ctl'+str+'_txtContactID'])!='undefined')
					{
						if (document.Form1.all['dgContact_ctl'+str+'_txtContactID'].value==document.Form1.ddlAssocContactId.value)
							{
								alert("Associated contact is already added");
								return false;
							}
					} 
				}
			
		}
		function Add(a)
		{
		
			    	if (document.Form1.uwOppTab__ctl3_ddlItems.value==0 )
			        {
				        alert("Select Item")
				        document.Form1.uwOppTab__ctl3_ddlItems.focus();
				        return false;
			        }
			        if (document.Form1.txtHidValue.value=="False")
			        {
			            if (a!='')
			            {
			                var ddlIDs=a.split(",");
			                for(i=0;i<ddlIDs.length;i++)
				            {
				                if (document.all['uwOppTab__ctl3_'+ddlIDs[i].split("~")[0]].value=="0" )
			                    {
				                    alert("Select "+ ddlIDs[i].split("~")[1])
				                    document.all['uwOppTab__ctl3_'+ddlIDs[i].split("~")[0]].focus();
				                    return false;
			                    }
			                }
			            }
			            
			          }
			        if (typeof(document.Form1.uwOppTab__ctl3_ddlWarehouse)!='undefined')
			        {
			             if (document.Form1.uwOppTab__ctl3_ddlWarehouse.value==0 )
			                {
				                alert("Select Warehouse")
				                document.Form1.uwOppTab__ctl3_ddlWarehouse.focus();
				                return false;
			                }
			         }
			        if (document.Form1.txtHidValue.value!="")
			        {
			            if (document.Form1.txtAddedItems.value!='')
			            {
			                var ddlIDs=document.Form1.txtAddedItems.value.split(",");
			                for(i=0;i<ddlIDs.length;i++)
				            {
				                if (ddlIDs[i]==document.Form1.uwOppTab__ctl3_ddlWarehouse.value)
			                    {
				                    alert("This Item is already added to opportunity. Please Edit the details")
				                    return false;
			                    }
			                }
			            }
			            
			          }
			       if (document.Form1.txtHidValue.value=="False"||document.Form1.txtHidValue.value=="")
			        {
			             if (document.Form1.uwOppTab__ctl3_txtunits.value=="" )
			                {
				                alert("Enter Units")
				                document.Form1.uwOppTab__ctl3_txtunits.focus();
				                return false;
			                }
			         }
			       
			       
			        if (document.Form1.uwOppTab__ctl3_txtprice.value=="")
			        {
				        alert("Enter Price")
				        document.Form1.uwOppTab__ctl3_txtprice.focus();
				        return false;
			        }
		}
		function AddOption(a)
		{
			    	if (document.Form1.uwOppTab__ctl3_ddlOptItem.value==0 )
			        {
				        alert("Select Option Item")
				        document.Form1.uwOppTab__ctl3_ddlOptItem.focus();
				        return false;
			        }
			        if (document.Form1.txtHidOptValue.value=="False")
			        {
			            if (a!='')
			            {
			                var ddlIDs=a.split(",");
			                for(i=0;i<ddlIDs.length;i++)
				            {
				                if (document.all['uwOppTab__ctl3_'+ddlIDs[i].split("~")[0]].value=="0" )
			                    {
				                    alert("Select "+ ddlIDs[i].split("~")[1])
				                    document.all['uwOppTab__ctl3_'+ddlIDs[i].split("~")[0]].focus();
				                    return false;
			                    }
			                }
			            }
			            
			          }
			        if (typeof(document.Form1.uwOppTab__ctl3_ddlOptWarehouse)!='undefined')
			        {
			             if (document.Form1.uwOppTab__ctl3_ddlOptWarehouse.value==0 )
			                {
				                alert("Select Warehouse")
				                document.Form1.uwOppTab__ctl3_ddlOptWarehouse.focus();
				                return false;
			                }
			         }
			        if (document.Form1.txtHidOptValue.value!="")
			        {
			            if (document.Form1.txtAddedItems.value!='')
			            {
			                var ddlIDs=document.Form1.txtAddedItems.value.split(",");
			                for(i=0;i<ddlIDs.length;i++)
				            {
				                if (ddlIDs[i]==document.Form1.uwOppTab__ctl3_ddlOptWarehouse.value)
			                    {
				                    alert("This Item is already added to opportunity. Please Edit the details")
				                    return false;
			                    }
			                }
			            }
			            
			          }
			       if (document.Form1.txtHidOptValue.value=="False"||document.Form1.txtHidOptValue.value=="")
			        {
			             if (document.Form1.uwOppTab__ctl3_txtOptUnits.value=="" )
			                {
				                alert("Enter Units")
				                document.Form1.uwOppTab__ctl3_txtOptUnits.focus();
				                return false;
			                }
			         }
			       
			       
			        if (document.Form1.uwOppTab__ctl3_txtOptPrice.value=="")
			        {
				        alert("Enter Price")
				        document.Form1.uwOppTab__ctl3_txtOptPrice.focus();
				        return false;
			        }

		}
		
		function deleteItem()
		{
			var bln;
			bln=window.confirm("Delete Seleted Row - Are You Sure ?")
			if(bln==true) 
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		function OpenBiz(a)
		{
			window.open('../opportunities/frmBizDocs.aspx?OpID='+a,'','toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes');
			return false;
		}
		
	
		function OpenDependency(a,b,c,d)
		{
			window.open('../opportunities/frmOppDependency.aspx?OPPStageID='+a+'&Opid='+b+'&PerID='+c+'&StgDtlId='+d,'','toolbar=no,titlebar=no,left=500, top=300,width=500,height=200,scrollbars=yes,resizable=yes');
			return false;
		}
		function OpenExpense(a,b,c,d,e)
		{
			window.open('../opportunities/frmOppExpense.aspx?OPPStageID='+a+'&Opid='+b+'&PerID='+c+'&StgDtlId='+d+'&DivId='+e,'','toolbar=no,titlebar=no,left=500, top=300,width=500,height=200,scrollbars=yes,resizable=yes');
			return false;
		}
		function OpenTime(a,b,c,d,e)
		{
			window.open('../opportunities/frmOppTime.aspx?OPPStageID='+a+'&Opid='+b+'&PerID='+c+'&StgDtlId='+d+'&DivId='+e,'','toolbar=no,titlebar=no,left=500, top=300,width=650,height=300,scrollbars=yes,resizable=yes');
			return false;
		}
		function OpenSubStage(a,b,c,d)
		{
			window.open('../opportunities/frmOPPSubStagesaspx.aspx?OPPStageID='+a+'&Opid='+b+'&PerID='+c+'&StgDtlId='+d,'','toolbar=no,titlebar=no,left=500, top=300,width=500,height=200,scrollbars=yes,resizable=yes');
			return false;
		}
		function CheckBoxCon(a,b,c)
		{
			if (parseInt(c)==1)
			{
				document.all['chkStage~'+a+'~'+b].checked=true
			}
			else
			{
				document.all['chkStage~'+a+'~'+b].checked=false
			}
		}
		function ValidateCheckBox(cint)
		{
			if (cint==1)
			{	
				if (document.all['uwOppTab__ctl1_chkDClosed'].checked==true)
				{
					if (document.all['uwOppTab__ctl1_chkDlost'].checked==true)
					{
					alert("The Deal is already Lost !")
					document.all['uwOppTab__ctl1_chkDClosed'].checked=false
					return false;
					}
				
				}
			}
			if (cint==2)
			{	
				if (document.all['uwOppTab__ctl1_chkDlost'].checked==true)
				{
					if (document.all['uwOppTab__ctl1_chkDClosed'].checked==true)
					{
						alert("The Deal is already Closed !")
						document.all['uwOppTab__ctl1_chkDlost'].checked=false
						return false;
					}
					document.Form1.all['uwOppTab__ctl0_chkActive'].checked=false;
				}
			}
			
		}
		function ShowWindow(Page,q,att) 
		{
			if (att=='show')
			{
				document.all[Page].style.visibility = "visible";
				return false;
		
			}
			if (att=='hide')
			{
				document.all[Page].style.visibility = "hidden";
				return false;
		
			}
			
		}
			function DeleteRecord()
		{
			if(confirm('Are you sure, you want to delete the selected record?'))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		function DeleteMessage()
		{
			alert("You Are not Authorized to Delete the Selected Record !");
			return false;
		}
		function OpenCreateOpp(a,b)
		{
		    window.open('../opportunities/frmCreateSalesPurFromOpp.aspx?OppID='+a+'&OppType='+b,'','toolbar=no,titlebar=no,left=100,top=100,width=1000,height=700,scrollbars=yes,resizable=yes')
		    return false;
		}
		function OpenConfSerItem(a)
		{
		    window.open('../opportunities/frmAddSerializedItem.aspx?OppID='+a,'','toolbar=no,titlebar=no,left=100,top=100,width=1000,height=700,scrollbars=yes,resizable=yes')
		    return false;
		}
		</script>
	</head>
	<body>
		
		<form id="Form1" method="post" runat="server">
<menu1:webmenu id="webmenu1" runat="server"></menu1:webmenu>
            <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>
			<table width="100%" align="center">
				<tr>
					<td>
						<table id="tblMenu" borderColor="black" cellSpacing="0" cellPadding="0" width="100%" border="0"
							runat="server">
							<tr>
								<td class="tr1" align="center"><b>Record Owner: </b>
									<asp:label id="lblRecordOwner" runat="server" ForeColor="Black"></asp:label></td>
								<td class="td1" width="1" height="18"></td>
								<td class="tr1" align="center"><b>Created By: </b>
									<asp:label id="lblCreatedBy" runat="server" ForeColor="Black"></asp:label></td>
								<td class="td1" width="1" height="18"></td>
								<td class="tr1" align="center"><b>Last Modified By: </b>
									<asp:label id="lblLastModifiedBy" runat="server" ForeColor="Black"></asp:label></td>
							</tr>
						</table>
					</td>
				</tr>
			
				<tr>
					<td>
						<table width="100%">
							<tr>
								<td class="normal1"><asp:label id="lblCustomerType" Runat="server"></asp:label><u><asp:label id="lblCustomerName" onclick="ShowWindow('Layer3','','show')" Runat="server" CssClass="hyperlink"></asp:label></u>
								&nbsp;<asp:Label ID="lblDealCompletedDate" Runat="server" CssClass="text"></asp:Label>
								
								</td>
								<td align="right">
								    <asp:Button ID="btnTrackAsset" runat="server" Visible="false" CssClass="button" text="Track As Customer Asset" Width="175"/>
								    <asp:button id="btnCreateOpp" Runat="server" CssClass="button" Visible="false"></asp:button>
								    <asp:button id="btnConfSerItems" Runat="server" CssClass="button" Visible="false" Text="Configure Serialized Item"></asp:button>
									<asp:button id="btnReceivedOrShipped" Runat="server" CssClass="button" Visible="false"></asp:button>
									<asp:button id="btnSave" Runat="server" CssClass="button" Text="Save"></asp:button>
									<asp:button id="btnSaveClose" Runat="server" CssClass="button" text="Save &amp; Close"></asp:button>
									<asp:button id="btnCancel" Runat="server" CssClass="button" Text="Cancel"></asp:button>
									<asp:Button ID="btnActdelete" Runat="server" CssClass="Delete" Text="r" ></asp:Button>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				
			</table>
		<table width="100%">
				<tr>
					<td class="normal4" align="center"><asp:literal id="litMessage" Runat="server"></asp:literal></td>
				</tr>
			</table>
			<asp:textbox id="txtrecOwner" style="DISPLAY: none" Runat="server"></asp:textbox>
		 	
			
           <igtab:ultrawebtab  ImageDirectory="" style="POSITION:absolute;z-index:101;" id="uwOppTab" runat="server" ThreeDEffect="True" BorderStyle="Solid" Width="100%" BarHeight="0"  BorderWidth="0">
                  <DefaultTabStyle Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Arial" >
                        </DefaultTabStyle>
                        <RoundedImage LeftSideWidth="7" RightSideWidth="8" ShiftOfImages="0" SelectedImage="../images/ig_tab_winXPs3.gif" NormalImage="../images/ig_tab_winXP3.gif" HoverImage="../images/ig_tab_winXPs3.gif" FillStyle="LeftMergedWithCenter" ></RoundedImage>
                     <SelectedTabStyle Height="23px"  ForeColor="white">
                        </SelectedTabStyle>
                        <HoverTabStyle Height="23px" ForeColor="white"></HoverTabStyle>
                        <Tabs>
                        <igtab:Tab Text="Opportunity Details" >
                            <ContentTemplate>
                        <asp:table id="tblOppr" CellPadding="0" CellSpacing="0" BorderWidth="1" Runat="server" CssClass="aspTable"
				            Width="100%" BorderColor="black" GridLines="None">
				            <asp:TableRow>
					            <asp:TableCell VerticalAlign=Top >
											<table id="tblDetails" runat="server" width="100%" border="0">
												<tr>
													<td rowspan="30" valign="top" >
						                                <img src="../images/Dart-32.gif" />
					                                </td><td class="normal1" align="right">Name
													</td>
													<td>
														<asp:textbox id="txtName" Runat="server" width="300" cssclass="signup" MaxLength="100"></asp:textbox></td>
												<td class="normal1" align="right">Assigned To</td>
																<td class="normal1">
																	<asp:dropdownlist id="ddlAssignedTo" CssClass="signup" Runat="server" Width="180px"></asp:dropdownlist>
																</td>
												</tr>
												<tr>
													
													
														<td class="normal1" align="right">Due Date<FONT color="red">*</FONT></td>
											        <td><BizCalendar:Calendar runat="server" ID="calDue" />
														</td>
												<td class="normal1" align="right">Contact<FONT color="red">*</FONT>
													</td>
													<td>
														<asp:dropdownlist id="ddlContact" Runat="server" CssClass="signup" Width="180" Enabled="False"></asp:dropdownlist></td>	
												</tr>
												
												<tr>
													<td class="normal1" align="right">Opportunity Source
													</td>
													<td class="normal1">
													<asp:dropdownlist id="ddlSource" Runat="server" Width="180" CssClass="signup"></asp:dropdownlist></td>
													<td class="normal1" align="right">
														<asp:Label ID="lblsalesorPurType" runat="server"> </asp:Label>
													</td>
													<td>
														<asp:DropDownList ID="ddlSalesorPurType" runat="server" CssClass="signup" Width="180"></asp:DropDownList>
													</td>
																
												</tr>
												
												<tr>
													<td class="normal1" align="right">Ship Date
													</td>
													<td>
													<table>
													<tr>
													<td><BizCalendar:Calendar ID="calShip" runat="server" /></td>
													<td>&nbsp;<font class="normal1">Shipping Cost</font> &nbsp;</td>
													<td><asp:textbox id="txtShipCost" tabIndex="9" Runat="server" width="90px" cssclass="signup"></asp:textbox></td>
													</tr>
													</table>
													</td>
													<td class="normal1" align="right">Billing Terms</td>
													<td class="normal1">
														<asp:CheckBox ID="chkBillinTerms" Runat="server"></asp:CheckBox>&nbsp; Msg.
														<asp:TextBox ID="txtSummary"  Runat="server" CssClass="signup"></asp:TextBox></td>
													
												</tr>
												<tr>
												</tr>
												<tr>
													<td class="text" align="right">Ship Via</td>
													<td>
														<asp:textbox id="txtCompName" Runat="server" width="80" cssclass="signup"></asp:textbox>&nbsp;
														<asp:Button ID="btnGo" Runat="server" Text="Go" Width="25" CssClass="button"></asp:Button>&nbsp;
														<asp:dropdownlist id="ddlShipCompany" Runat="server" Width="200" CssClass="signup"></asp:dropdownlist></td>
														<td class="normal1" align="right">Net</td>
													<td class="normal1">
														<asp:TextBox ID="txtNetdays" Runat="server" Width="40" CssClass="signup"></asp:TextBox>
														days
														<asp:RadioButton ID="radPlus" GroupName="rad" Runat="server" Text="Plus"></asp:RadioButton>
														<asp:RadioButton ID="radMinus" Runat="server" GroupName="rad" Text="Minus"></asp:RadioButton>&nbsp;
														<asp:TextBox ID="txtInterest" Runat="server" Width="40" CssClass="signup"></asp:TextBox>
													</td>
													
												</tr>
												<tr>
													<td class="normal1" align="right" rowspan="2">Tracking URL
													</td>
													<td rowspan="2">
														<asp:TextBox ID="txttrackingURL" Runat="server" CssClass="signup" Width="300" TextMode="MultiLine"
															Height="40" Text="http://"></asp:TextBox>&nbsp;
														<asp:Button ID="btntrackGo" Runat="server" CssClass="button" Text="Go" Width="25"></asp:Button>
														<br>
													</td>
													<td class="normal1" align="right">Active
													</td>
													<td class="normal1">
														<asp:CheckBox ID="chkActive" Runat="server"></asp:CheckBox>&nbsp;&nbsp;&nbsp;<asp:HyperLink id="hplDocuments" Runat="server" CssClass="hyperlink">
															</asp:HyperLink>
														/
														<asp:HyperLink id="hplLnkProjects" Runat="server" CssClass="hyperlink"></asp:HyperLink>
														
													</td>
												</tr>
												<tr>
													<td class="normal1" align="right">Amount :
													</td>
													<td class="normal1">
														<asp:Label ID="lblAmount" Runat="server"></asp:Label></td>
												</tr>
												<tr>
													<td class="normal1" align="right">Comments
													</td>
													<td>
														<asp:textbox id="txtOComments" tabIndex="11" Height="44px" Runat="server" width="300" cssclass="signup"
															MaxLength="250" TextMode="MultiLine"></asp:textbox></td>
												
												    <td align="right" class="normal1" id="tdSales1" runat="server" visible="false" >Campaign</td>
													<td id="tdSales2" runat="server" visible="false" >
													     <asp:dropdownlist id="ddlCampaign" Runat="server" CssClass="signup" Width="180"></asp:dropdownlist>
													</td>
											
													
												</tr>
												
											</table>
											<br/>
								 
                        	    </asp:TableCell>
				                    </asp:TableRow>
			                    </asp:table>
			                     </ContentTemplate> 
                        </igtab:Tab>
                        <igtab:Tab Text="Milestones & Stages">
                            <ContentTemplate>
                              <asp:table id="Table1" CellPadding="0" CellSpacing="0" BorderWidth="1" Runat="server" CssClass="aspTable"
				                    Width="100%" BorderColor="black" GridLines="None">
				                <asp:TableRow>
					                <asp:TableCell VerticalAlign=Top >
											<br>
											<table width="100%">
												<tr>
													<td class="normal1" align="right">Conclusion Reason
													</td>
													<td>
														<asp:dropdownlist id="ddlClReason" Runat="server" CssClass="signup" Width="180"></asp:dropdownlist></td>
												
											
												</tr>
												
												<tr id="trSalesProcess" runat="server">
													<td class="normal1" align="right">Opportunity Process
													</td>
													<td>
														<asp:dropdownlist id="ddlProcessList" Runat="server" CssClass="signup" Width="180" AutoPostBack="true"></asp:dropdownlist></td>
												</tr>
											</table>
											<asp:table id="tblMilestone" Runat="server" Width="100%" GridLines="none" BorderWidth="0" CellSpacing="0" ></asp:table>
									</asp:TableCell>
				                    </asp:TableRow>
			                    </asp:table>
                            </ContentTemplate>
                        </igtab:Tab>
                        <igtab:Tab Text="Associated Contacts">
                            <ContentTemplate>
                            <asp:table id="Table2" CellPadding="0" CellSpacing="0" BorderWidth="1" Runat="server" CssClass="aspTable"
				                    Width="100%" BorderColor="black" GridLines="None">
				                <asp:TableRow>
					                <asp:TableCell VerticalAlign=Top >
											<br>
											<table width="100%">
												<tr>
													<td class="normal1" align="right">Customer</td>
													<td>
														<asp:textbox id="txtComp" Runat="server" cssclass="signup" width="125px"></asp:textbox>&nbsp;
														<asp:button id="imgCustGo" Runat="server" CssClass="button" Text="Go" Width="30"></asp:button>&nbsp;
														<asp:dropdownlist id="ddlcompany" tabIndex="15" Runat="server" CssClass="signup" Width="200" AutoPostBack="true"></asp:dropdownlist></td>
												
													<td class="normal1" align="right">Asssociated Contacts</td>
													<td>
														<asp:dropdownlist id="ddlAssocContactId" Runat="server" CssClass="signup" Width="180"></asp:dropdownlist>
														&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														<asp:button id="ImgbtnAddContact" Runat="server" CssClass="button" Text="Add Contact"></asp:button><br>
								
													</td>
												</tr>
											</table>
											<br />
											<asp:datagrid id="dgContact" runat="server" CssClass="dg" Width="100%" BorderColor="white" AutoGenerateColumns="False">
												<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
												<ItemStyle CssClass="is"></ItemStyle>
												<HeaderStyle CssClass="hs"></HeaderStyle>
												<Columns>
													<asp:BoundColumn DataField="numContactId" Visible="false"></asp:BoundColumn>
													<asp:BoundColumn DataField="bitPartner" Visible="false"></asp:BoundColumn>
													<asp:BoundColumn HeaderText="Organization,Relationship" DataField="Company"></asp:BoundColumn>
													<asp:BoundColumn HeaderText="First & Last Name" DataField="Name"></asp:BoundColumn>
													<asp:BoundColumn HeaderText="Phone - Ext" DataField="Phone"></asp:BoundColumn>
													<asp:BoundColumn HeaderText="Email" DataField="Email"></asp:BoundColumn>
													<asp:TemplateColumn HeaderText="Contact Role">
														<ItemTemplate>
															<asp:Label ID="lblContcRoleid" Runat="server" Visible=false Text='<%# DataBinder.Eval(Container.DataItem, "ControleId") %>'>
															</asp:Label>
															<asp:DropDownList ID="ddlContactrole" Runat="server" CssClass="signup"></asp:DropDownList>
														</ItemTemplate>
													</asp:TemplateColumn >
														<asp:TemplateColumn HeaderText="Share Opportunity via Partner Point ?" >
														<ItemTemplate>
														            <asp:CheckBox ID="chkShare" runat="server"  />
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn >
														<HeaderTemplate>
															<asp:Button ID="btnHdeleteCnt" Runat="server" CssClass="Delete" Text="r"></asp:Button>
														</HeaderTemplate>
														<ItemTemplate>
															<asp:TextBox id="txtContactID" Runat="server" style="display:none" Text='<%# DataBinder.Eval(Container.DataItem, "numContactId") %>'>
															</asp:TextBox>
															<asp:Button ID="btnDeleteCnt" Runat="server" CssClass="Delete" Text="r" CommandName="Delete"></asp:Button>
															<asp:LinkButton ID="lnkDeleteCnt" Runat="server" Visible="false">
																<font color="#730000">*</font></asp:LinkButton>
														</ItemTemplate>
													</asp:TemplateColumn>
												</Columns>
											</asp:datagrid>
											
								</asp:TableCell>
				                    </asp:TableRow>
			                    </asp:table>
                            </ContentTemplate>
                        </igtab:Tab>
                         <igtab:Tab Text="Product/Service">
                            <ContentTemplate>
                               <asp:table id="Table3" CellPadding="0" CellSpacing="0" BorderWidth="1" Runat="server" CssClass="aspTable"
				                    Width="100%" BorderColor="black" GridLines="None">
				                <asp:TableRow>
					                <asp:TableCell VerticalAlign=Top >
											<br>
											<table align="center" >
											
												<tr>
													<td class="normal1" rowSpan="111111115">
														<asp:image id="imgItem" Runat="server" BorderWidth="1" Width="100" BorderColor="black" Height="100"></asp:image><br>
														<asp:hyperlink id="hplImage" Runat="server" CssClass="hyperlink" NavigateUrl="">View Full Image</asp:hyperlink></td>
														<td>
														          <table id="tblItems" runat="server" >
														                <tr>
														                   <td class="normal1" vAlign="top" align="right">Item<FONT color="red">*</FONT></td>
													                        <td class="normal1" vAlign="top" colSpan="5">
														                        <asp:dropdownlist id="ddlItems" tabIndex="19" Runat="server" CssClass="signup" Width="450" AutoPostBack="true"></asp:dropdownlist>
													                        </td>
													                        <td class="normal1" align="right">Type</td>
													                        <td class="normal1">
														                        <asp:dropdownlist id="ddltype" Runat="server" CssClass="signup" Width="180" Enabled="False">
															                        <asp:ListItem Value="P">Product</asp:ListItem>
															                        <asp:ListItem Value="S">Service</asp:ListItem>
															                        <asp:ListItem Value="A">Accessory</asp:ListItem>
														                        </asp:dropdownlist>
														                    </td>
														                    <td class="normal1" align="right" >List Price :
														                    </td>
														                    <td>
														                    <asp:label id="lblListPrice" Runat="server"></asp:label>
														                    </td>
														                </tr>
														                <tr >
														                  
														                    <td colspan="6" style="white-space:nowrap">
														                        <asp:PlaceHolder ID="phItems" runat="server"></asp:PlaceHolder>
														                    </td>
														                </tr>
														                <tr id="trWareHouse" runat="server">
														                    <td class="normal1" align="right">
														                        Warehouse<FONT color="red">*</FONT>
														                    </td>
														                    <td>
														                    <asp:DropDownList ID="ddlWarehouse" AutoPostBack="true" runat="server" CssClass="signup" Width="180"></asp:DropDownList>
														                  
														                    </td>
														                </tr>
														                <tr>
														                    <td class="text_bold" align="right">Description </td>
													                        <td class="normal1" colSpan="5">
														                        <asp:TextBox ID="txtdesc" runat="server"  TextMode="MultiLine" Width="500" ></asp:TextBox>
														                     </td>
														                      <td class="normal1" align="right"><asp:Label ID="lblMakeRecurring" runat ="server" Text ="Recurring Template" Visible ="false"></asp:Label> </td>
													                        <td class="normal1">
														                       <asp:DropDownList ID="ddlMakeRecurring" runat="server" CssClass="signup" Width="180" Visible="false"></asp:DropDownList>
														                     </td>
														                </tr>
														                <tr>
														                   
													                        <td class="normal1" align="right">
														                        <asp:hyperlink id="hplUnit" Runat="server" CssClass="hyperlink">Units/Hours</asp:hyperlink><FONT color="red">*</FONT>
														                        </td>
														                        <td class="normal1" nowrap>
														                            <asp:textbox id="txtunits" Runat="server" CssClass="signup" Width="90px" ></asp:textbox>
														                            <asp:hyperlink id="hplPrice" Runat="server" CssClass="hyperlink">Price</asp:hyperlink><font color="red">*</font>
														                            <asp:textbox id="txtprice" Runat="server" CssClass="signup" Width="90px" MaxLength="11"></asp:textbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														                            <asp:button id="btnAdd" Runat="server" width="60px" CssClass="button" Text="Add"></asp:button>&nbsp;&nbsp;&nbsp;<asp:button id="btnEditCancel" Runat="server" Visible="false" CssClass="button" Text="Cancel"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														                     </td>
														                </tr>
														          </table>
														
														</td>
													
												</tr>
											</table>
											<table width="100%"  id="tblItemWareHouse" runat="server"  Visible="false">
										
											    <tr>
											        <td>
											          <igtbl:ultrawebgrid id="uwItemSel" Width="100%" DisplayLayout-AllowRowNumberingDefault="ByDataIsland"  runat="server" Browser="Xml"   Height="100%">
					                                    <DisplayLayout AutoGenerateColumns="true"  RowHeightDefault="18"  AllowAddNewDefault="Yes" Version="3.00" SelectTypeRowDefault="Single"
					                                    ViewType="Hierarchical" TableLayout="Auto" SelectTypeCellDefault="Extended" BorderCollapseDefault="Separate" AllowColSizingDefault="Free" 
					                                    Name="uwItemSel" EnableClientSideRenumbering="true" SelectTypeColDefault="Extended" AllowUpdateDefault="Yes">
					                                     <HeaderStyleDefault VerticalAlign="Middle" Font-Size="8pt" Font-Names="Arial" BorderStyle="Solid" HorizontalAlign="Left" ForeColor="white" BackColor="#52658C">
                                                            <Padding Left="2px" Right="2px"></Padding>
                                                            <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
                                                        </HeaderStyleDefault>
                                                        <RowSelectorStyleDefault BackColor="White"></RowSelectorStyleDefault>
                                                        <FrameStyle Width="100%" Cursor="Default" BorderWidth="3px" Font-Size="8pt" Font-Names="Arial" BorderStyle="Double"></FrameStyle>
                                                        <FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
                                                            <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
                                                        </FooterStyleDefault>
                                                        <EditCellStyleDefault BorderWidth="0px" BorderStyle="None"></EditCellStyleDefault>
                                                        <SelectedRowStyleDefault ForeColor="White" BackColor="#666666"></SelectedRowStyleDefault>
                                                        <RowStyleDefault BorderWidth="1px" Font-Size="8pt" Font-Names="Arial" BorderColor="Gray" BorderStyle="Solid" BackColor="White">
                                                            <Padding Left="5px" Right="5px"></Padding>
                                                            <BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
                                                        </RowStyleDefault>
                                                        <RowExpAreaStyleDefault BackColor="LightSteelBlue"></RowExpAreaStyleDefault>
				                                    </DisplayLayout>
											                    <Bands>
												                    <igtbl:UltraGridBand AllowDelete="No" AllowAdd="Yes"  AddButtonCaption="WareHouse" BaseTableName="WareHouse" Key="WareHouse">
												                    <Columns>
												                        <igtbl:UltraGridColumn  Hidden="true" Width="100%" IsBound="true" BaseColumnName="numWareHouseItemID" Key="numWareHouseItemID" >
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn Hidden="true" IsBound="true" BaseColumnName="numWareHouseID" Key="numWareHouseID" >
												                        </igtbl:UltraGridColumn>
												                         <igtbl:UltraGridColumn Hidden="true" IsBound="true" BaseColumnName="numItemCode" Key="numItemCode" >
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn HeaderText="Warehouse" AllowUpdate="No" IsBound="true" BaseColumnName="vcWarehouse" Key="vcWarehouse" >
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn HeaderText="On Hand" AllowUpdate="No" IsBound="true" BaseColumnName="OnHand" Key="OnHand" >
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn HeaderText="On Order" AllowUpdate="No" IsBound="true" BaseColumnName="OnOrder" Key="OnOrder" >
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn HeaderText="Reorder" AllowUpdate="No" IsBound="true" BaseColumnName="Reorder" Key="Reorder" >
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn HeaderText="Allocation" AllowUpdate="No" IsBound="true" BaseColumnName="Allocation" Key="Allocation" >
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn HeaderText="BackOrder" AllowUpdate="No" IsBound="true" BaseColumnName="BackOrder" Key="BackOrder" >
												                        </igtbl:UltraGridColumn>
												                          <igtbl:UltraGridColumn HeaderText="Units" Hidden="true" AllowUpdate="Yes" Format="###,##0.00"  Key="Units" >
												                        </igtbl:UltraGridColumn>
												                    </Columns>
												                    </igtbl:UltraGridBand>
												                    <igtbl:UltraGridBand  AllowDelete="No" AllowAdd="Yes" AllowUpdate="No"    AddButtonCaption="Serialized Items" BaseTableName="SerializedItems" Key="SerializedItems">
													                    <Columns>
												                            <igtbl:UltraGridColumn AllowUpdate="Yes"  Type="CheckBox" Key="Select">
												                            </igtbl:UltraGridColumn>
													                    </Columns>
												                    </igtbl:UltraGridBand>
											                    </Bands>
										                    </igtbl:ultrawebgrid>
											        </td>
											    </tr>
											</table>
											<br />
											<br />
											<table align="center" id="tblOptSelection" runat="server" visible="false">
											<tr>
											        <td class="text_bold" colspan="2">
											            Choose Options And Accessories
											            <br /><br />
											            
											        </td>
											    </tr>
												<tr>
													<td class="normal1" rowSpan="111111115">
														<asp:image id="imgOptItem" Runat="server" BorderWidth="1" Width="100" BorderColor="black" Height="100"></asp:image><br>
														<asp:hyperlink id="hplOptImage" Runat="server" CssClass="hyperlink" NavigateUrl="">View Full Image</asp:hyperlink></td>
														<td>
														          <table id="Table4" runat="server" >
														                <tr>
														                   <td class="normal1" vAlign="top" align="right">Option Item<FONT color="red">*</FONT></td>
													                        <td class="normal1" vAlign="top" colSpan="5">
														                        <asp:dropdownlist id="ddlOptItem" tabIndex="19" Runat="server" CssClass="signup" Width="450" AutoPostBack="true"></asp:dropdownlist>
													                        </td>
														                    <td class="normal1" align="right" >List Price :
														                    </td>
														                    <td>
														                    <asp:label id="lstOptPrice" Runat="server"></asp:label>
														                    </td>
														                </tr>
														                <tr >
														                  
														                    <td colspan="6" style="white-space:nowrap">
														                        <asp:PlaceHolder ID="phOptItem" runat="server" ></asp:PlaceHolder>
														                    </td>
														                </tr>
														                <tr>
														                    <td id="trOptWareHouse" runat="server" class="normal1" align="right">
														                        Warehouse<FONT color="red">*</FONT>
														                    </td>
														                    <td>
														                    <asp:DropDownList ID="ddlOptWarehouse" AutoPostBack="true"  runat="server" CssClass="signup" Width="180"></asp:DropDownList>
														                   
														                    </td>
														                </tr>
														                <tr>
														                    <td class="text_bold" align="right">Description </td>
													                        <td class="normal1" colSpan="5">
														                        <asp:TextBox ID="txtOptDesc" runat="server"  TextMode="MultiLine" Width="500" ></asp:TextBox>
														                     </td>
														                      <td class="normal1" align="right">Recurring Template</td>
													                        <td class="normal1">
														                       <asp:DropDownList ID="ddlOptMakeRecurring" runat="server" CssClass="signup" Width="180"></asp:DropDownList>
														                     </td>
														                </tr>
														                <tr>
														                   
													                        <td class="normal1" align="right">
														                        <asp:hyperlink id="hplOptUnits" Runat="server" CssClass="hyperlink">Units/Hours</asp:hyperlink><FONT color="red">*</FONT>
														                        </td>
														                        <td class="normal1" nowrap>
														                            <asp:textbox id="txtOptUnits" Runat="server" CssClass="signup" Width="90px" ></asp:textbox>
														                            <asp:hyperlink id="hplOptPrice" Runat="server" CssClass="hyperlink">Price</asp:hyperlink><font color="red">*</font>
														                            <asp:textbox id="txtOptPrice" Runat="server" CssClass="signup" Width="90px" MaxLength="11"></asp:textbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														                            <asp:button id="btnOptAdd" Runat="server" width="60px" CssClass="button" Text="Add"></asp:button>&nbsp;&nbsp;&nbsp;<asp:button id="Button2" Runat="server" Visible="false" CssClass="button" Text="Cancel"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														                     </td>
														                </tr>
														          </table>
														
														</td>
													
												</tr>
											</table> 
												        <igtbl:ultrawebgrid id="uwOptionItem" Visible="false" Width="100%" DisplayLayout-AllowRowNumberingDefault="ByDataIsland"  runat="server" Browser="Xml"   Height="100%">
					                                    <DisplayLayout AutoGenerateColumns="true"  RowHeightDefault="18"  AllowAddNewDefault="Yes" Version="3.00" SelectTypeRowDefault="Single"
					                                    ViewType="Hierarchical" TableLayout="Auto" SelectTypeCellDefault="Extended" BorderCollapseDefault="Separate" AllowColSizingDefault="Free" 
					                                    Name="uwItemSel" EnableClientSideRenumbering="true" SelectTypeColDefault="Extended" AllowUpdateDefault="Yes">
					                                     <HeaderStyleDefault VerticalAlign="Middle" Font-Size="8pt" Font-Names="Arial" BorderStyle="Solid" HorizontalAlign="Left" ForeColor="white" BackColor="#52658C">
                                                            <Padding Left="2px" Right="2px"></Padding>
                                                            <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
                                                        </HeaderStyleDefault>
                                                        <RowSelectorStyleDefault BackColor="White"></RowSelectorStyleDefault>
                                                        <FrameStyle Width="100%" Cursor="Default" BorderWidth="3px" Font-Size="8pt" Font-Names="Arial" BorderStyle="Double"></FrameStyle>
                                                        <FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
                                                            <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
                                                        </FooterStyleDefault>
                                                        <EditCellStyleDefault BorderWidth="0px" BorderStyle="None"></EditCellStyleDefault>
                                                        <SelectedRowStyleDefault ForeColor="White" BackColor="#666666"></SelectedRowStyleDefault>
                                                        <RowStyleDefault BorderWidth="1px" Font-Size="8pt" Font-Names="Arial" BorderColor="Gray" BorderStyle="Solid" BackColor="White">
                                                            <Padding Left="5px" Right="5px"></Padding>
                                                            <BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
                                                        </RowStyleDefault>
                                                        <RowExpAreaStyleDefault BackColor="LightSteelBlue"></RowExpAreaStyleDefault>
				                                    </DisplayLayout>
											                    <Bands>
												                    <igtbl:UltraGridBand AllowDelete="No" AllowAdd="Yes"  AddButtonCaption="WareHouse" BaseTableName="WareHouse" Key="WareHouse">
												                    <Columns>
												                        <igtbl:UltraGridColumn  Hidden="true" Width="100%" IsBound="true" BaseColumnName="numWareHouseItemID" Key="numWareHouseItemID" >
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn Hidden="true" IsBound="true" BaseColumnName="numWareHouseID" Key="numWareHouseID" >
												                        </igtbl:UltraGridColumn>
												                         <igtbl:UltraGridColumn Hidden="true" IsBound="true" BaseColumnName="numItemCode" Key="numItemCode" >
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn HeaderText="Warehouse" AllowUpdate="No" IsBound="true" BaseColumnName="vcWarehouse" Key="vcWarehouse" >
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn HeaderText="On Hand" AllowUpdate="No" IsBound="true" BaseColumnName="OnHand" Key="OnHand" >
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn HeaderText="On Order" AllowUpdate="No" IsBound="true" BaseColumnName="OnOrder" Key="OnOrder" >
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn HeaderText="Reorder" AllowUpdate="No" IsBound="true" BaseColumnName="Reorder" Key="Reorder" >
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn HeaderText="Allocation" AllowUpdate="No" IsBound="true" BaseColumnName="Allocation" Key="Allocation" >
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn HeaderText="BackOrder" AllowUpdate="No" IsBound="true" BaseColumnName="BackOrder" Key="BackOrder" >
												                        </igtbl:UltraGridColumn>
												                          <igtbl:UltraGridColumn HeaderText="Units" Hidden="true" AllowUpdate="Yes" Format="###,##0.00"  Key="Units" >
												                        </igtbl:UltraGridColumn>
												                    </Columns>
												                    </igtbl:UltraGridBand>
												             
											                    </Bands>
										                    </igtbl:ultrawebgrid>
												  
									
										                    
										                    
									<br />       
									<table width="100%"  id="tblProducts"  runat="server" visible="false">
											    <tr>
											        <td class="text_bold">
											            Items Added To Opportunity
											        </td>
											    </tr>
											    <tr>
											        <td>          
									                <igtbl:ultrawebgrid Width="100%"  DisplayLayout-AutoGenerateColumns="false"   id="ucItem" DisplayLayout-AllowRowNumberingDefault="ByDataIsland"  runat="server" Browser="Xml">
					                                    <DisplayLayout AllowAddNewDefault="Yes" RowHeightDefault="18px" Version="3.00" SelectTypeRowDefault="Single"
					                                    ViewType="Hierarchical" SelectTypeCellDefault="Extended" BorderCollapseDefault="Separate" AllowColSizingDefault="Free"
					                                    Name="UltraWebGrid1" EnableClientSideRenumbering="true"  TableLayout="Fixed"  SelectTypeColDefault="Extended" AllowUpdateDefault="Yes">
					                                   <HeaderStyleDefault VerticalAlign="Middle" Font-Size="8pt" Font-Names="Arial" BorderStyle="Solid" HorizontalAlign="Left" ForeColor="white" BackColor="#52658C">
                                                        <Padding Left="5px" Right="5px"></Padding>
                                                        <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
                                                    </HeaderStyleDefault>
                                                    <RowSelectorStyleDefault BackColor="White"></RowSelectorStyleDefault>
                                                    <FrameStyle Width="100%" Cursor="Default" BorderWidth="3px" Font-Size="8pt" Font-Names="Arial" BorderStyle="Double"></FrameStyle>
                                                    <FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
                                                        <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
                                                    </FooterStyleDefault>
                                                    <EditCellStyleDefault BorderWidth="0px" BorderStyle="None"></EditCellStyleDefault>
                                                    <SelectedRowStyleDefault ForeColor="White" BackColor="#666666"></SelectedRowStyleDefault>
                                                    <RowStyleDefault BorderWidth="1px" Font-Size="8pt" Font-Names="Arial" BorderColor="Gray" BorderStyle="Solid" BackColor="White">
                                                        <Padding Left="5px" Right="5px"></Padding>
                                                        <BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
                                                    </RowStyleDefault>
                                                    <RowExpAreaStyleDefault BackColor="LightSteelBlue"></RowExpAreaStyleDefault>
				                                           </DisplayLayout>
											                    <Bands >
												                    <igtbl:UltraGridBand  AllowDelete="Yes" BaseTableName="Item" Key="Item">
												                    <Columns>
												                        <igtbl:UltraGridColumn Hidden="true" Width="100%"  IsBound="true" BaseColumnName="numoppitemtCode" Key="numoppitemtCode" >
												                           
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn Hidden="true"   IsBound="true" BaseColumnName="numWarehouseItmsID" Key="numWareHouseItemID" >
												                           
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn Hidden="true"   IsBound="true" BaseColumnName="numWareHouseID" Key="numWareHouseID" >
												                           
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn Hidden="true"   IsBound="true" BaseColumnName="numItemCode" Key="numItemCode" >
												                           
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn HeaderText="Item" Width="12%" AllowUpdate="No" IsBound="true" BaseColumnName="vcItemName" Key="vcItemName" >
												                           
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn HeaderText="Warehouse" Width="12%" AllowUpdate="No" IsBound="true" BaseColumnName="Warehouse" Key="Warehouse" >
												                           
												                        </igtbl:UltraGridColumn>
												                           <igtbl:UltraGridColumn HeaderText="Type" Width="5%" AllowUpdate="No" IsBound="true" BaseColumnName="ItemType" Key="ItemType" >
												                           
												                        </igtbl:UltraGridColumn>
												                          <igtbl:UltraGridColumn CellMultiline="Yes" Width="10%" HeaderText="Desc" AllowUpdate="No" IsBound="true" BaseColumnName="vcItemDesc" Key="vcItemDesc" >
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn CellMultiline="Yes" Width="10%" HeaderText="Attributes" AllowUpdate="No" IsBound="true" BaseColumnName="Attributes" Key="Attributes" >
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn HeaderText="Unit/Hours" Width="7%" Format="###,##0.00" AllowUpdate="No" IsBound="true" BaseColumnName="numUnitHour" Key="numUnitHour" >
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn HeaderText="Unit Price" Width="7%" Format="###,##0.00" AllowUpdate="No" IsBound="true" BaseColumnName="monPrice" Key="monPrice" >
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn HeaderText="Amount" Width="7%" Format="###,##0.00" AllowUpdate="No" IsBound="true" BaseColumnName="monTotAmount" Key="monTotAmount" >
												                        </igtbl:UltraGridColumn>
												                       <igtbl:UltraGridColumn Hidden="true"   IsBound="true" BaseColumnName="numRecurringId" Key="numRecurringId" >
												                       </igtbl:UltraGridColumn>
												                       <igtbl:UltraGridColumn HeaderText="Recurring" Width="10%" AllowUpdate="No" IsBound="true" BaseColumnName="vcRecurringDesc" Key="vcRecurringDesc" >
												                       </igtbl:UltraGridColumn>
												                       <igtbl:UltraGridColumn Type="Button" Key="Action" ServerOnly="false"  CellButtonDisplay="Always">
												                       </igtbl:UltraGridColumn>
												                    </Columns>
												                    </igtbl:UltraGridBand>
												                    <igtbl:UltraGridBand AllowDelete="No" AllowUpdate="No"  BaseTableName="SerialNo" Key="SerialNo">
													                    <Columns>
													                      <igtbl:UltraGridColumn Hidden="true"   IsBound="true" BaseColumnName="numWarehouseItmsDTLID" Key="numWarehouseItmsDTLID" ></igtbl:UltraGridColumn>
												                           <igtbl:UltraGridColumn Hidden="true"   IsBound="true" BaseColumnName="numoppitemtCode" Key="numoppitemtCode" >
												                        </igtbl:UltraGridColumn>
														                    <igtbl:UltraGridColumn  HeaderText="Serial No" Key="vcSerialNo" Width="200px" BaseColumnName="vcSerialNo">
														                    </igtbl:UltraGridColumn>
                    													       <igtbl:UltraGridColumn  HeaderText="Attributes" Key="Attributes"  Width="200px" BaseColumnName="Attributes">
														                    </igtbl:UltraGridColumn>
													                    </Columns>
												                    </igtbl:UltraGridBand>
											                    </Bands>
										                    </igtbl:ultrawebgrid>
						                                   </td>
											    </tr>
											</table>
								
									</asp:TableCell>
				                    </asp:TableRow>
			                    </asp:table>
                            </ContentTemplate>
                        </igtab:Tab>
                        <igtab:Tab Text="BizDocs" >
                        <ContentTemplate>
                          <asp:table id="table5" Runat="server" BorderWidth="1" Width="100%" BackColor="white" cellspacing="0"
					                cellpadding="0" BorderColor="black" GridLines="None" Height=300 CssClass="aspTable">
					                <asp:tableRow>
						                <asp:tableCell VerticalAlign="Top">
							                <table width="100%">
								                <tr>
									                <td>
										                <div id="divBizDocsDtl" runat="server">
											                <iframe id="IframeBiz" frameborder="0"  width="100%" scrolling="auto" runat="server" height="300"></iframe>
										                </div>
									                </td>
								                </tr>
							                </table>
						                </asp:tableCell>
					                </asp:tableRow>
				                </asp:table>
				                </ContentTemplate>
                        </igtab:Tab>
                    </Tabs>
                </igtab:ultrawebtab>
              
					<asp:TextBox ID="txtHidValue"  runat="server"  style="display:none"></asp:TextBox>	
	                <asp:TextBox ID="txtHidEditOppItem"  runat="server" style="display:none"></asp:TextBox>	
	                <asp:TextBox ID="txtHidOptValue"  runat="server" style="display:none"></asp:TextBox>
	                <asp:TextBox ID="txtAddedItems"  runat="server" style="display:none"></asp:TextBox>
					<asp:TextBox ID="ddlOppType"  runat="server" style="display:none"></asp:TextBox>
                    <asp:TextBox ID="txtSerialize"  runat="server" style="display:none"></asp:TextBox>
                    <asp:TextBox ID="txtTemplateId" Text="0"  runat="server" style="display:none"></asp:TextBox>
                          <asp:TextBox ID="txtProcessId" Text="0"  runat="server" style="display:none"></asp:TextBox>
                  <div id="Layer3" style="Z-INDEX: 2; LEFT: 96px; VISIBILITY: hidden; POSITION: absolute; TOP: 10px"><asp:table id="table11" Runat="server" BorderWidth="1" Height="96px" BackColor="white" CellSpacing="0"
					CellPadding="0" BorderColor="black" GridLines="None">
					<asp:tableRow>
						<asp:tableCell VerticalAlign="Top">
							<table cellSpacing="1" cellPadding="1" width="100%" border="0">
								<tr>
									<td class="text_bold" colSpan="2">&nbsp;Customer Information</td>
									<td vAlign="top" align="right" colSpan="4">
										<asp:button id="btnCusOk" Runat="server" CssClass="button" Text="Ok" Width="25"></asp:button></td>
								</tr>
								<tr>
									<td class="text" align="right">&nbsp;<b>Customer:</b></td>
									<td class="normal1">
										<asp:hyperlink id="hplCustName" Runat="server" CssClass="hyperlink"></asp:hyperlink></td>
									<td class="text" align="right">&nbsp;<b>Division:</b></td>
									<td>
										<asp:label id="lblcustdivision" runat="server" cssclass="normal1" width="100px"></asp:label></td>
									<td class="text" align="right">&nbsp;<b>Territory:</b></td>
									<td>
										<asp:label id="lbltrtry" runat="server" cssclass="normal1" width="100px"></asp:label></td>
								</tr>
								<tr>
									<td class="text" align="right">&nbsp;<b>Rating:</b></td>
									<td>
										<asp:label id="lblrat" runat="server" cssclass="normal1" width="100px"></asp:label></td>
									<td class="text" align="right">&nbsp;<b>Status:</b></td>
									<td>
										<asp:label id="lblstat" runat="server" cssclass="normal1" width="100px"></asp:label></td>
									<td class="text" align="right">&nbsp;<b>Industry:</b></td>
									<td>
										<asp:label id="lblind" runat="server" cssclass="normal1" width="100px"></asp:label></td>
								</tr>
								<tr>
									<td class="text" align="right">&nbsp;<b>Type:</b></td>
									<td>
										<asp:label id="lbltype" runat="server" cssclass="normal1" width="100px"></asp:label></td>
									<td class="text" align="right">&nbsp;<b>Credit:</b></td>
									<td>
										<asp:label id="lblcredit" runat="server" cssclass="normal1" width="100px"></asp:label></td>
									<td class="text" align="right">&nbsp;<b>Web:</b></td>
									<td>
										<asp:label id="lblweb" runat="server" cssclass="normal1" width="100px"></asp:label></td>
								</tr>
								<tr>
									<td class="text" align="right">&nbsp;<b>Profile:</b></td>
									<td>
										<asp:label id="lblprofile" runat="server" cssclass="normal1" width="100px"></asp:label></td>
									<td class="text" align="right">&nbsp;<b>Group:</b></td>
									<td>
										<asp:label id="lblgrp" runat="server" cssclass="normal1" width="100px"></asp:label></td>
								</tr>
							</table>
						</asp:tableCell>
					</asp:tableRow>
				</asp:table>
				
			</div>
		
			</form>
	</body>
</html>