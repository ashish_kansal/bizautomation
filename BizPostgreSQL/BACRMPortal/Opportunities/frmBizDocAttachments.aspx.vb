Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Partial Public Class frmBizDocAttachments
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim objCommon As New CCommon
            objCommon.sb_FillComboFromDBwithSel(ddlBizDoc, 27, Session("DomainID"))
            If Not ddlBizDoc.Items.FindByValue(GetQueryStringVal(Request.QueryString("enc"), "BizDocID")) Is Nothing Then
                ddlBizDoc.Items.FindByValue(GetQueryStringVal(Request.QueryString("enc"), "BizDocID")).Selected = True
            End If
            LoadBizDocAttachMnts()
            If GetQueryStringVal(Request.QueryString("enc"), "E") = 2 Then
                btnSort.Visible = False
                btnAdd.Visible = False
                dgAttachments.Columns(5).Visible = False
            End If
        End If
        btnCancel.Attributes.Add("onclick", "return Close()")
    End Sub

    Sub LoadBizDocAttachMnts()
        Dim objOpportunity As New OppBizDocs
        objOpportunity.BizDocId = ddlBizDoc.SelectedValue
        objOpportunity.DomainID = Session("DomainID")
        dgAttachments.DataSource = objOpportunity.GetBizDocAttachments
        dgAttachments.DataBind()
    End Sub

    Private Sub ddlBizDoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBizDoc.SelectedIndexChanged
        LoadBizDocAttachMnts()
    End Sub

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Response.Redirect("../opportunity/frmBizDocAddAttchmnts.aspx?BizDocID=" & ddlBizDoc.SelectedValue)
    End Sub

    Private Sub btnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSort.Click
        Response.Redirect("../opportunity/frmBizDocAttSort.aspx?BizDocID=" & ddlBizDoc.SelectedValue)
    End Sub

    Private Sub dgAttachments_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgAttachments.ItemCommand
        If e.CommandName = "Delete" Then
            Dim objOpportunity As New OppBizDocs
            objOpportunity.BizDocAtchID = e.Item.Cells(0).Text
            objOpportunity.DelBizDocAtchmnts()
            LoadBizDocAttachMnts()
        End If
    End Sub

    Private Sub dgAttachments_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgAttachments.ItemDataBound
        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
            Dim hplOpenLink As HyperLink
            hplOpenLink = e.Item.FindControl("hplOpenLink")
            hplOpenLink.NavigateUrl = "../documents/docs/" & e.Item.Cells(1).Text
            If e.Item.Cells(2).Text = "BizDoc" Then
                e.Item.Cells(3).Enabled = False
                If GetQueryStringVal(Request.QueryString("enc"), "E") = 2 Then
                    hplOpenLink.Visible = True
                    Dim objPDFCON As New PDFCON
                    hplOpenLink.NavigateUrl = "../Documents/Docs/" & objPDFCON.Convert(Replace(ConfigurationManager.AppSettings("PortalURL"), "/Login.aspx", "") & "/Opportunities/frmBizInvoice.aspx?OpID=" & GetQueryStringVal(Request.QueryString("enc"), "OpID") & "&OppBizId=" & GetQueryStringVal(Request.QueryString("enc"), "OppBizId") & "&DomainID=" & GetQueryStringVal(Request.QueryString("enc"), "DomainID") & "&ConID=" & GetQueryStringVal(Request.QueryString("enc"), "ConID"))
                Else
                    hplOpenLink.Visible = False
                End If
                CType(e.Item.FindControl("btnDelete"), Button).Visible = False
            End If
            If GetQueryStringVal(Request.QueryString("enc"), "E") = 2 Then
                e.Item.Cells(3).Enabled = False
            End If
        End If
    End Sub

End Class