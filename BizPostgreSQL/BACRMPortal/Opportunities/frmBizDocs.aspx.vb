'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Opportunities



Partial Class frmBizDocs
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents hplNew As System.Web.UI.WebControls.HyperLink
    Dim m_aryRightsForPage() As Integer
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            btnAdd.Attributes.Add("onclick", "return AddBizDocs('" & GetQueryStringVal(Request.QueryString("enc"), "OpID") & "');")
            If Not IsPostBack Then
                getDetails()
            End If

        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Sub getDetails()
        Try
            Dim dttable As New Datatable
            Dim objBizDocs As New OppBizDocs(Session("UserContactID"))
            objBizDocs.OppId = GetQueryStringVal(Request.QueryString("enc"), "OpID")
            dttable = objBizDocs.GetExtBizDocsByOOpId
            dgBizDocs.DataSource = dttable
            dgBizDocs.DataBind()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub


    Private Sub dgBizDocs_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgBizDocs.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                Dim txtOppBizId As TextBox
                txtOppBizId = e.Item.FindControl("txtBizDocID")
                Dim objBizDocs As New OppBizDocs(Session("UserContactID"))
                objBizDocs.OppBizDocId = txtOppBizId.Text
                objBizDocs.DeleteBizDoc()
                Response.Redirect("frmBizDocs.aspx?OpID=" & GetQueryStringVal(Request.QueryString("enc"), "OpID"))
            End If
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgBizDocs_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgBizDocs.ItemDataBound
        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
            Dim btnDelete As Button
            Dim lnkDelete As LinkButton
            lnkDelete = e.Item.FindControl("lnkDelete")
            btnDelete = e.Item.FindControl("btnDelete")
            btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            Dim hpl As HyperLink
            hpl = e.Item.FindControl("hplBizdcoc")
            Dim txtOppBizId As TextBox
            txtOppBizId = e.Item.FindControl("txtBizDocID")
            hpl.Attributes.Add("onclick", "return OpenBizInvoice('" & GetQueryStringVal(Request.QueryString("enc"), "OpID") & "','" & txtOppBizId.Text & "')")
        End If
    End Sub

    Function ReturnName(ByVal CloseDate As Date) As String
        Dim strTargetResolveDate As String
        strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
        If Format(CloseDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
            strTargetResolveDate = "<font color=red>Today</font>"
        ElseIf Format(CloseDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, 1, Now()), "yyyyMMdd") Then
            strTargetResolveDate = "<font color=orange>" & strTargetResolveDate & "</font>"
        End If
        Return strTargetResolveDate
    End Function

    Function CalTotalAmt(ByVal BizDocID As Long) As String
        Dim dtOppBiDocDtl As New DataTable
        Dim objOppBizDocs As New OppBizDocs
        objOppBizDocs.OppBizDocId = BizDocID
        objOppBizDocs.OppId = GetQueryStringVal(Request.QueryString("enc"), "OpID")
        dtOppBiDocDtl = objOppBizDocs.GetOppBizDocDtl
        Dim dblDisc, dblShipping As Double
        If dtOppBiDocDtl.Rows.Count <> 0 Then
            If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("decDiscount")) Then
                dblDisc = Math.Round(dtOppBiDocDtl.Rows(0).Item("decDiscount"), 2)
            Else
                dblDisc = "0.00"
            End If
            If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("monShipCost")) Then
                dblShipping = Math.Round(dtOppBiDocDtl.Rows(0).Item("monShipCost"), 2)
            Else
                dblShipping = "0.00"
            End If
        End If
        Dim dtOppBiDocItems As New DataTable
        objOppBizDocs.OppBizDocId = BizDocID
        objOppBizDocs.OppId = GetQueryStringVal(Request.QueryString("enc"), "OpID")
        dtOppBiDocItems = objOppBizDocs.GetOppInItems.Tables(0)
        Dim decTotalAmount As Decimal
        Dim decTaxAmount As Decimal
        Dim i As Integer
        For i = 0 To dtOppBiDocItems.Rows.Count - 1
            decTotalAmount = decTotalAmount + dtOppBiDocItems.Rows(i).Item("amount")
            decTaxAmount = decTaxAmount + dtOppBiDocItems.Rows(i).Item("amount") * dtOppBiDocItems.Rows(i).Item("Tax") / 100
        Next
        Dim decDisc, decInterest As Decimal
        If dtOppBiDocDtl.Rows(0).Item("tintBillingTerms") = 1 Then
            If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("bintAccountClosingDate")) Then
                Dim strDate1 As Date
                strDate1 = DateAdd(DateInterval.Day, dtOppBiDocDtl.Rows(0).Item("numBillingDays"), dtOppBiDocDtl.Rows(0).Item("bintAccountClosingDate"))

                If dtOppBiDocDtl.Rows(0).Item("tintInterestType") = 0 Then
                    If strDate1 > Now() Then
                        decDisc = 0
                    Else
                        decDisc = dtOppBiDocDtl.Rows(0).Item("fltInterest")
                    End If
                Else
                    If strDate1 > Now() Then
                        decInterest = 0
                    Else
                        decInterest = dtOppBiDocDtl.Rows(0).Item("fltInterest")
                    End If
                End If
            End If
        End If
        Dim decDiscAmount, decTotalDiscount, decTotalAmtDisc, decLateCharge As Decimal

        If dblDisc <> 0 Then
            Try

                decDiscAmount = decTotalAmount * (100 - dblDisc) / 100

                If decDisc > 0 Then
                    decTotalDiscount = decTotalAmount * dblDisc / 100 + decDiscAmount * decDisc / 100
                    decTotalAmtDisc = decTotalAmount - decTotalDiscount + dblShipping + decTaxAmount
                    decLateCharge = "0.00"
                Else
                    decTotalDiscount = decTotalAmount * dblDisc / 100
                    If decInterest * decDiscAmount = 0 Then
                        decLateCharge = "0.00"
                    Else
                        decLateCharge = decInterest * decDiscAmount / 100
                    End If
                    decTotalAmtDisc = decTotalAmount + decInterest * decDiscAmount / 100 - decTotalDiscount + dblShipping + decTaxAmount
                End If

            Catch ex As Exception

            End Try

        Else
            If decDisc > 0 Then
                decTotalDiscount = decTotalAmount * decDisc / 100
                decTotalAmtDisc = decTotalAmount - decTotalDiscount + dblShipping + decTaxAmount
                decLateCharge = "0.00"
            Else
                decTotalDiscount = "0.00"
                decTotalAmtDisc = decTotalAmount + decTotalAmount * decInterest / 100 + dblShipping + decTaxAmount
            End If

        End If
        Return String.Format("{0:#,##0.00}", decTotalAmtDisc)
    End Function


End Class


