
'***************************************************************************************************************************
'     Author Name				 :  Anoop Jayaraj
'     Date Written				 :  18/2/2005
'***************************************************************************************************************************
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports system.Reflection
Imports BACRM.BusinessLogic.Item
Imports Infragistics.WebUI.UltraWebGrid
Imports Infragistics.WebUI.UltraWebTab
Imports BACRM.BusinessLogic.Accounting
Partial Class frmOpportunities
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Dim dsTemp As DataSet
    Dim ds As DataSet
    Dim objOpportunity As New MOpportunity
    Dim dtAssignTo As DataTable
    Dim objCommon As New CCommon
    Dim dtOppAtributes As DataTable
    Dim dtOppOptAttributes As DataTable
    Dim strValues As String

    Dim myRow As DataRow
    Dim arrOutPut() As String = New String(2) {}
    Dim lngOppId As Long
    Dim lngDivId As Long
    Dim str As String = ""
    Private designerPlaceholderDeclaration As System.Object
    Dim SI As Integer = 0
    Dim SI1 As Integer = 0
    Dim SI2 As Integer = 0
    Dim frm As String = ""
    Dim frm1 As String = ""
    Dim frm2 As String = ""
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
      
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not GetQueryStringVal(Request.QueryString("enc"), "SI") Is Nothing Then
                SI = GetQueryStringVal(Request.QueryString("enc"), "SI")
            End If
            If Not GetQueryStringVal(Request.QueryString("enc"), "SI1") Is Nothing Then
                SI1 = GetQueryStringVal(Request.QueryString("enc"), "SI1")
            Else
                SI1 = 0
            End If
            If Not GetQueryStringVal(Request.QueryString("enc"), "SI2") Is Nothing Then
                SI2 = GetQueryStringVal(Request.QueryString("enc"), "SI2")
            Else
                SI2 = 0
            End If
            If Not GetQueryStringVal(Request.QueryString("enc"), "frm") Is Nothing Then
                frm = ""
                frm = GetQueryStringVal(Request.QueryString("enc"), "frm")
            Else
                frm = ""
            End If
            If Not GetQueryStringVal(Request.QueryString("enc"), "frm1") Is Nothing Then
                frm1 = ""
                frm1 = GetQueryStringVal(Request.QueryString("enc"), "frm1")
            Else
                frm1 = ""
            End If
            If Not GetQueryStringVal(Request.QueryString("enc"), "SI2") Is Nothing Then
                frm2 = ""
                frm2 = GetQueryStringVal(Request.QueryString("enc"), "frm2")
            Else
                frm2 = ""
            End If
            lngOppId = Session("OppID")
            ImgbtnAddContact.Attributes.Add("onclick", "return AddContact()")
            txtunits.Attributes.Add("onkeypress", "CheckNumber(2)")
            txtprice.Attributes.Add("onkeypress", "CheckNumber(1)")
            txtShipCost.Attributes.Add("onkeypress", "CheckNumber(1)")
            txtNetdays.Attributes.Add("onkeypress", "CheckNumber(2)")
            txtInterest.Attributes.Add("onkeypress", "CheckNumber(1)")
            btnCusOk.Attributes.Add("onclick", "return ShowWindow('Layer3','','hide')")
            hplDocuments.Attributes.Add("onclick", "return OpenDocuments(" & lngOppId & ");")
            btntrackGo.Attributes.Add("onclick", "return fn_GoToURL('" & txttrackingURL.Text & "');")

            'm_aryRightsForAssContacts = clsAuthorization.fn_ExternalPageLevelRights("frmOpportunities.aspx", Session("UserContactID"), 10, 4)
            ' m_aryRightsForItems = clsAuthorization.fn_ExternalPageLevelRights("frmOpportunities.aspx", Session("UserContactID"), 10, 5)

            If Not IsPostBack Then

                Dim objContacts As New CContacts
                objContacts.RecID = lngOppId
                objContacts.Type = "O"
                objContacts.UserCntID = Session("UserContactID")
                objContacts.AddVisiteddetails()

                Session("Help") = "Opportunity"
                'If m_aryRightsForAssContacts(RIGHTSTYPE.VIEW) = 0 Then
                '    ImgbtnAddContact.Visible = False
                'ElseIf m_aryRightsForAssContacts(RIGHTSTYPE.ADD) = 0 Then
                '    ImgbtnAddContact.Visible = False
                'End If
                Dim strDate As String = FormattedDateFromDate(Now, Session("DateFormat"))

                'If m_aryRightsForItems(RIGHTSTYPE.ADD) = 0 Then
                '    btnAdd.Visible = False
                'End If
                imgItem.Visible = False
                hplImage.Visible = False
                'imgOptItem.Visible = False
                '  hplOptImage.Visible = False
                If GetQueryStringVal(Request.QueryString("enc"), "SelectedIndex") <> "" Then
                    uwOppTab.SelectedTabIndex = GetQueryStringVal(Request.QueryString("enc"), "SelectedIndex")
                End If
                If lngOppId <> 0 Then
                    LoadSavedInformation()
                    tblMenu.Visible = True
                Else
                    calDue.SelectedDate = strDate
                    tblMenu.Visible = False
                End If
                'Dim tbBizDocs As Infragistics.WebUI.UltraWebTab.Tab
                'tbBizDocs = uwOppTab.Tabs(4)
                IframeBiz.Attributes.Add("src", "../opportunities/frmBizDocs.aspx?OpID=" & lngOppId)
            End If
            DisplayDynamicFlds()
            If ViewState("MileCheck") <> 1 Then
                trSalesProcess.Visible = False
            End If
            btnSave.Attributes.Add("onclick", "return Save(2)")
            btnSaveClose.Attributes.Add("onclick", "return Save(2)")
            CreatMilestone()
            If Session("OppType") = 1 Then

            ElseIf Session("OppType") = 2 Then
                If ddlItems.SelectedIndex <> -1 Then
                    hplUnit.Attributes.Add("onclick", "return openUnit('" & ddlItems.SelectedItem.Value.Split(",")(0) & "','" & txtunits.Text & "')")
                End If
            End If

            hplLnkProjects.Attributes.Add("onclick", "return ShowlinkedProjects(" & lngOppId & ")")

            btnActdelete.Attributes.Add("onclick", "return DeleteRecord()")
            If ddlItems.Items.Count > 0 Then


                If ddlItems.SelectedItem.Value > 0 Then
                    DisplayDynamicFldsItems()
                End If
                If ddlItems.SelectedIndex > 0 And txtHidValue.Text <> "" Then
                    If Session("OppType") = 2 And txtSerialize.Text = 1 Then
                        If ddlWarehouse.Items.Count = 0 Then
                            ddlWarehouse.DataSource = objCommon.GetWarehouseOnAttrSel(ddlItems.SelectedValue, "")
                            ddlWarehouse.DataTextField = "vcWareHouse"
                            ddlWarehouse.DataValueField = "numWareHouseItemId"
                            ddlWarehouse.DataBind()
                            ddlWarehouse.Items.Insert(0, "--Select One--")
                            ddlWarehouse.Items.FindByText("--Select One--").Value = "0"
                        End If
                    Else
                        CreateAttributes()
                    End If
                Else
                    btnAdd.Attributes.Add("onclick", "return Add('')")
                End If
            End If
            If ddlOptItem.Items.Count > 0 Then
                If ddlOptItem.SelectedIndex > 0 And txtHidOptValue.Text <> "" Then
                    CreateOptAttributes()
                Else
                    btnOptAdd.Attributes.Add("onclick", "return AddOption('')")
                End If
            End If
            If Not IsPostBack Then
                If uwOppTab.Tabs.Count > SI Then
                    uwOppTab.SelectedTabIndex = SI
                End If
            End If
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub


    Sub LoadCustomerInfo(ByVal intContactID As Integer)
        Dim dtCompanyInfo As DataTable
        Dim objContacts As New CContacts
        objContacts.ContactID = intContactID
        dtCompanyInfo = objContacts.GetCompnyDetailsByConID
        If dtCompanyInfo.Rows.Count > 0 Then
            lblCustomerName.Text = dtCompanyInfo.Rows(0).Item("vcCompanyName")
            lblcustdivision.Text = dtCompanyInfo.Rows(0).Item("vcDivisionName")
            lngDivId = dtCompanyInfo.Rows(0).Item("numDivisionID")
            btnTrackAsset.Attributes.Add("onclick", "return openTrackAsset('" & lngOppId & "','" & lngDivId & "')")
            hplCustName.Text = dtCompanyInfo.Rows(0).Item("vcCompanyName")
            If dtCompanyInfo.Rows(0).Item("tintCRMType") = 0 Then
                hplCustName.NavigateUrl = "../Leads/frmLeads.aspx?frm=oppdetail&opId=" & lngOppId & "&DivID=" & dtCompanyInfo.Rows(0).Item("numDivisionID")
            ElseIf dtCompanyInfo.Rows(0).Item("tintCRMType") = 1 Then
                hplCustName.NavigateUrl = "../prospects/frmProspects.aspx?frm=oppdetail&opId=" & lngOppId & "&DivID=" & dtCompanyInfo.Rows(0).Item("numDivisionID")
            ElseIf dtCompanyInfo.Rows(0).Item("tintCRMType") = 2 Then
                hplCustName.NavigateUrl = "../account/frmAccounts.aspx?frm=oppdetail&opId=" & lngOppId & "&DivID=" & dtCompanyInfo.Rows(0).Item("numDivisionID")
            End If
            If IsDBNull(dtCompanyInfo.Rows(0).Item("Territory")) = False Then lbltrtry.Text = dtCompanyInfo.Rows(0).Item("Territory")
            If IsDBNull(dtCompanyInfo.Rows(0).Item("Rating")) = False Then lblrat.Text = dtCompanyInfo.Rows(0).Item("Rating")
            If IsDBNull(dtCompanyInfo.Rows(0).Item("Status")) = False Then lblstat.Text = dtCompanyInfo.Rows(0).Item("Status")
            If IsDBNull(dtCompanyInfo.Rows(0).Item("industry")) = False Then lblind.Text = dtCompanyInfo.Rows(0).Item("industry")
            If IsDBNull(dtCompanyInfo.Rows(0).Item("Type")) = False Then lbltype.Text = dtCompanyInfo.Rows(0).Item("Type")
            If IsDBNull(dtCompanyInfo.Rows(0).Item("Credit")) = False Then lblcredit.Text = dtCompanyInfo.Rows(0).Item("Credit")
            If IsDBNull(dtCompanyInfo.Rows(0).Item("vcwebsite")) = False Then lblweb.Text = dtCompanyInfo.Rows(0).Item("vcwebsite")
            If IsDBNull(dtCompanyInfo.Rows(0).Item("vcprofile")) = False Then lblprofile.Text = dtCompanyInfo.Rows(0).Item("vcprofile")
            If IsDBNull(dtCompanyInfo.Rows(0).Item("grpname")) = False Then lblgrp.Text = dtCompanyInfo.Rows(0).Item("grpname")
        End If
    End Sub

    Sub LoadSavedInformation()
        Try
            objCommon.sb_FillComboFromDBwithSel(ddlCampaign, 24, Session("DomainID"))
            objCommon.sb_FillComboFromDBwithSel(ddlClReason, 12, Session("DomainID"))
            objCommon.sb_FillComboFromDBwithSel(ddlSource, 9, Session("DomainID"))
            'objCommon.sb_FillComboFromDBwithSel(ddlContactRole, 26, Session("DomainID"))
            Dim dtDetails As DataTable
            objOpportunity.OpportunityId = lngOppId
            objOpportunity.DomainID = Session("DomainID")
            objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            dtDetails = objOpportunity.OpportunityDTL.Tables(0)
            LoadCustomerInfo(dtDetails.Rows(0).Item("ContactID"))
            Session("OppType") = dtDetails.Rows(0).Item("tintOppType")
            ddlOppType.Text = dtDetails.Rows(0).Item("tintOppType")
            hplDocuments.Text = "Documents(" & dtDetails.Rows(0).Item("DocumentCount") & ")"
            hplLnkProjects.Text = "Linked Projects(" & dtDetails.Rows(0).Item("NoOfProjects") & ")"
            If dtDetails.Rows(0).Item("tintOppType") = 1 Then
                ' m_aryRightsForPage = clsAuthorization.fn_ExternalPageLevelRights("frmOpportunities.aspx", Session("UserContactID"), 10, 3)
                objCommon.sb_FillComboFromDBwithSel(ddlSalesorPurType, 45, Session("DomainID"))
                lblCustomerType.Text = "Customer : "
                tdSales1.Visible = True
                tdSales2.Visible = True
                lblsalesorPurType.Text = "Sales Type"
                '  If m_aryRightsForItems(RIGHTSTYPE.VIEW) <> 0 And m_aryRightsForItems(RIGHTSTYPE.ADD) <> 0 Then
                LoadItem(ddlItems, 1, dtDetails.Rows(0).Item("numDivisionID"))
                'End If
                hplPrice.CssClass = "hyperlink"
                hplPrice.Text = "Price"
                hplPrice.Attributes.Add("onclick", "return openItem('" & ddlItems.ClientID & "','" & txtunits.ClientID & "','" & dtDetails.Rows(0).Item("numDivisionID") & "','" & txtSerialize.ClientID & "')")
                hplUnit.Attributes.Add("onclick", "return openUnit('" & ddlItems.ClientID & "')")
                hplOptPrice.Attributes.Add("onclick", "return openOptItem('" & ddlOptItem.ClientID & "','" & txtOptUnits.ClientID & "','" & dtDetails.Rows(0).Item("numDivisionID") & "')")
                hplOptUnits.Attributes.Add("onclick", "return openUnit('" & ddlOptItem.ClientID & "')")
            ElseIf dtDetails.Rows(0).Item("tintOppType") = 2 Then
                '  m_aryRightsForPage = clsAuthorization.fn_ExternalPageLevelRights("frmOpportunities.aspx", Session("UserContactID"), 10, 9)
                objCommon.sb_FillComboFromDBwithSel(ddlSalesorPurType, 46, Session("DomainID"))
                lblCustomerType.Text = "Vendor : "
                lblsalesorPurType.Text = "Purchase Type"
                '  If m_aryRightsForItems(RIGHTSTYPE.VIEW) <> 0 And m_aryRightsForItems(RIGHTSTYPE.ADD) <> 0 Then
                LoadItem(ddlItems, 2, dtDetails.Rows(0).Item("numDivisionID"))
                '  End If
                hplPrice.Text = "Purchase Price"
            End If
            LoadSalesProcess()
            'If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
            '    Response.Redirect("../admin/authentication.aspx?mesg=AC")
            'Else
            '    If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
            '        btnSave.Visible = False
            '        btnSaveClose.Visible = False
            '    End If
            '    If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
            '        btnActDelete.Visible = False
            '    End If
            'End If
            txtName.Text = dtDetails.Rows(0).Item("VcPoppName")
            If Not IsDBNull(dtDetails.Rows(0).Item("numCampainID")) Then
                If Not ddlCampaign.Items.FindByValue(dtDetails.Rows(0).Item("numCampainID")) Is Nothing Then
                    ddlCampaign.Items.FindByValue(dtDetails.Rows(0).Item("numCampainID")).Selected = True
                End If
            End If
            If Not IsDBNull(dtDetails.Rows(0).Item("numSalesOrPurType")) Then
                If Not ddlSalesorPurType.Items.FindByValue(dtDetails.Rows(0).Item("numSalesOrPurType")) Is Nothing Then
                    ddlSalesorPurType.Items.FindByValue(dtDetails.Rows(0).Item("numSalesOrPurType")).Selected = True
                End If
            End If
            If Not IsDBNull(dtDetails.Rows(0).Item("tintSource")) Then
                If Not ddlSource.Items.FindByValue(dtDetails.Rows(0).Item("tintSource")) Is Nothing Then
                    ddlSource.Items.FindByValue(dtDetails.Rows(0).Item("tintSource")).Selected = True
                End If
            End If

            If Not IsDBNull(dtDetails.Rows(0).Item("intpEstimatedCloseDate")) Then
                calDue.SelectedDate = dtDetails.Rows(0).Item("intpEstimatedCloseDate")
            End If
            If Not IsDBNull(dtDetails.Rows(0).Item("monPAmount")) Then
                lblAmount.Text = String.Format("{0:#,##0.00}", dtDetails.Rows(0).Item("monPAmount"))
            End If
            If Not IsDBNull(dtDetails.Rows(0).Item("numRecOwner")) Then
                txtRecOwner.Text = dtDetails.Rows(0).Item("numRecOwner")
            End If
            If Not IsDBNull(dtDetails.Rows(0).Item("tintActive")) Then
                If dtDetails.Rows(0).Item("tintActive") = 0 Then
                    chkActive.Checked = False
                Else
                    chkActive.Checked = True
                End If
            End If
            lblLastModifiedBy.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("ModifiedBy")), "", dtDetails.Rows(0).Item("ModifiedBy"))
            lblCreatedBy.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("CreatedBy")), "", dtDetails.Rows(0).Item("CreatedBy"))
            lblRecordOwner.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("RecordOwner")), "", dtDetails.Rows(0).Item("RecordOwner"))
            txtOComments.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("txtComments")), "", dtDetails.Rows(0).Item("txtComments"))
            If Not IsDBNull(dtDetails.Rows(0).Item("lngPConclAnalysis")) Then
                If Not ddlClReason.Items.FindByValue(dtDetails.Rows(0).Item("lngPConclAnalysis")) Is Nothing Then
                    ddlClReason.Items.FindByValue(dtDetails.Rows(0).Item("lngPConclAnalysis")).Selected = True
                End If
            End If
            ddlContact.Items.Insert(0, dtDetails.Rows(0).Item("numContactId"))
            If Not IsDBNull(dtDetails.Rows(0).Item("dtShipDate")) Then
                calShip.SelectedDate = dtDetails.Rows(0).Item("dtShipDate")
            End If
            If Not IsDBNull(dtDetails.Rows(0).Item("monShipCost")) Then
                txtShipCost.Text = String.Format("{0:#,##0.00}", dtDetails.Rows(0).Item("monShipCost"))
            End If
            txtCompName.Text = dtDetails.Rows(0).Item("ShipCompany")
            If txtCompName.Text <> "" Then
                FillCustomer(ddlShipCompany, txtCompName.Text)
                If Not IsDBNull(dtDetails.Rows(0).Item("numShipVia")) Then
                    If Not ddlShipCompany.Items.FindByValue(dtDetails.Rows(0).Item("numShipVia")) Is Nothing Then
                        ddlShipCompany.Items.FindByValue(dtDetails.Rows(0).Item("numShipVia")).Selected = True
                    End If
                End If
            End If
            objCommon.sb_FillConEmpFromDBSel(ddlAssignedTo, Session("DomainID"), 0, 0)
            If Not IsDBNull(dtDetails.Rows(0).Item("numAssignedTo")) Then
                If Not ddlAssignedTo.Items.FindByValue(dtDetails.Rows(0).Item("numAssignedTo")) Is Nothing Then
                    ddlAssignedTo.Items.FindByValue(dtDetails.Rows(0).Item("numAssignedTo")).Selected = True
                End If
            End If
            txtCompName.Text = ""
            txtTrackingURL.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("vcTrackingURL")), "", dtDetails.Rows(0).Item("vcTrackingURL"))
            If Not IsDBNull(dtDetails.Rows(0).Item("fltInterest")) Then
                txtInterest.Text = String.Format("{0:#,##0.00}", dtDetails.Rows(0).Item("fltInterest"))
            End If
            If Not IsDBNull(dtDetails.Rows(0).Item("numBillingDays")) Then
                txtNetDays.Text = String.Format("{0:#,###}", dtDetails.Rows(0).Item("numBillingDays"))
            End If
            If Not IsDBNull(dtDetails.Rows(0).Item("tintBillingTerms")) Then
                If dtDetails.Rows(0).Item("tintBillingTerms") = 0 Then
                    chkBillinTerms.Checked = False
                Else
                    chkBillinTerms.Checked = True
                    txtSummary.Text = "Net " & dtDetails.Rows(0).Item("numBillingDays") & " , " & IIf(dtDetails.Rows(0).Item("tintInterestType") = 0, "-", "+") & dtDetails.Rows(0).Item("fltInterest") & " %"
                End If
            End If
            If Not IsDBNull(dtDetails.Rows(0).Item("tintInterestType")) Then
                If dtDetails.Rows(0).Item("tintInterestType") = 0 Then
                    radMinus.Checked = True
                Else
                    radPlus.Checked = True
                End If
            End If
            If dtDetails.Rows(0).Item("tintOppStatus") = 1 Then
                uwOppTab.Tabs(0).Text = "Deal Details"
                lblDealCompletedDate.Text = "Deal Completed Date : " & FormattedDateFromDate(dtDetails.Rows(0).Item("bintAccountClosingDate"), Session("DateFormat"))
                btnCreateOpp.Visible = True
                btnCreateOpp.Attributes.Add("onClick", "return OpenCreateOpp('" & lngOppId & "','" & dtDetails.Rows(0).Item("tintOppType") & "')")
                If dtDetails.Rows(0).Item("tintOppType") = 1 Then
                    btnCreateOpp.Text = "Create Purchase Deals/Orders"
                Else
                    btnCreateOpp.Text = "Create Sales Deals/Orders"
                End If
            End If
            If dtDetails.Rows(0).Item("tintOppStatus") = 1 And dtDetails.Rows(0).Item("tintshipped") = 0 Then
                btnReceivedOrShipped.Visible = True
                If dtDetails.Rows(0).Item("tintOppType") = 1 Then
                    If objOpportunity.CheckCanbeShipped = 1 Then
                        btnReceivedOrShipped.Attributes.Add("onclick", "return CannotShip()")
                    Else
                        btnReceivedOrShipped.Attributes.Add("onclick", "return AlertMsg()")
                    End If
                    btnReceivedOrShipped.Text = "Shipped"
                Else
                    btnReceivedOrShipped.Attributes.Add("onclick", "return AlertMsg()")
                    btnReceivedOrShipped.Text = "Received"
                End If
            End If
            If dtDetails.Rows(0).Item("tintOppStatus") = 1 And dtDetails.Rows(0).Item("tintshipped") = 1 Then
                btnSave.Enabled = False
                btnSaveClose.Enabled = False
                btnActDelete.Enabled = False
            End If
            If dtDetails.Rows(0).Item("tintOppType") = 2 And dtDetails.Rows(0).Item("tintOppStatus") = 1 And dtDetails.Rows(0).Item("tintshipped") = 1 Then
                btnConfSerItems.Visible = True
                btnConfSerItems.Attributes.Add("onclick", "return OpenConfSerItem(" & lngOppId & ")")
            End If
            Dim dtItems As DataTable
            objOpportunity.DomainID = Session("DomainId")
            dsTemp = objOpportunity.ItemsByOppId
            dtItems = dsTemp.Tables(0)
            Dim k As Integer
            Dim intCheck As Integer = 0
            For k = 0 To dtItems.Rows.Count - 1
                If dtItems.Rows(k).Item("ItemType") <> "Service" Then
                    intCheck = 1
                End If
            Next
            If intCheck = 0 Then
                btnReceivedOrShipped.Text = "Deal Completed"
                btnReceivedOrShipped.Attributes.Add("onclick", "return DealCompleted()")
            End If
            Dim dtItem As DataTable
            Dim dtSerItem As DataTable
            dtItem = dsTemp.Tables(0)
            dtSerItem = dsTemp.Tables(1)
            dtItem.TableName = "Item"
            dtSerItem.TableName = "SerialNo"
            dtItem.PrimaryKey = New DataColumn() {dsTemp.Tables(0).Columns("numoppitemtCode")}
            'Response.Write(dsTemp.GetXml)
            If dtSerItem.ParentRelations.Count = 0 Then
                dsTemp.Relations.Add("Item", dsTemp.Tables(0).Columns("numoppitemtCode"), dsTemp.Tables(1).Columns("numoppitemtCode"))
            End If
            dsTemp.AcceptChanges()

            Dim dtMilestone As DataTable
            objOpportunity.UserCntID = Session("UserContactID")
            dtMilestone = objOpportunity.BusinessProcessByOpID
            Session("SalesProcessDetails") = dtMilestone

            Session("Data") = dsTemp
            '  If m_aryRightsForItems(RIGHTSTYPE.VIEW) <> 0 Then
            BindItems(dsTemp)
            '  End If


            If dtMilestone.Rows.Count <= 2 Then
                ViewState("MileCheck") = 1
            End If
            CreatMilestone()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub



    Private Sub ddlcompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlcompany.SelectedIndexChanged
        Try
            FillContact(ddlAssocContactId, CInt(ddlcompany.SelectedItem.Value))
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub


    Public Sub FillContact(ByVal ddlCombo As DropDownList, ByVal lngDivision As Long)
        Dim fillCombo As New COpportunities
        With fillCombo
            .DivisionID = lngDivision
            ddlCombo.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
            ddlCombo.DataTextField = "Name"
            ddlCombo.DataValueField = "numcontactId"
            ddlCombo.DataBind()
        End With
        ddlCombo.Items.Insert(0, New ListItem("---Select One---", "0"))
    End Sub


    Private Sub populateDataSet(ByVal objItems As CItems, ByVal lngItemCode As Long)
        Try
            ds = Nothing
            objItems.ItemCode = lngItemCode
            ds = objItems.GetItemWareHouses()
            If Session("OppType") = 2 Then
                ds.Tables(1).Rows.Clear()
                ds.AcceptChanges()
            End If
            If ds.Relations.Count < 1 Then
                ds.Tables(0).TableName = "WareHouse"
                ds.Tables(1).TableName = "SerializedItems"
                ds.Tables(0).PrimaryKey = New DataColumn() {ds.Tables(0).Columns("numWareHouseItemID")}
                ds.Tables(1).PrimaryKey = New DataColumn() {ds.Tables(1).Columns("numWareHouseItmsDTLID")}
                ds.Relations.Add("Customers", ds.Tables(0).Columns("numWareHouseItemID"), ds.Tables(1).Columns("numWareHouseItemID"))
            End If
        Catch ex As Exception

        End Try
    End Sub

    ''On selectindex changed of Items in Milestones and stages
    Private Sub ddlProcessList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProcessList.SelectedIndexChanged
        Try
            Dim dtSalesProcess As DataTable
            objOpportunity.SalesProcessId = CInt(ddlProcessList.SelectedItem.Value)
            objOpportunity.UserCntID = Session("UserContactID")
            objOpportunity.DomainID = Session("DomainId")
            objOpportunity.OpportunityId = lngOppId
            dtSalesProcess = objOpportunity.BusinessProcessByOpID
            Session("SalesProcessDetails") = dtSalesProcess
            If dtSalesProcess.Rows.Count <= 2 Then
                ViewState("MileCheck") = 1
            End If
            ViewState("byteMode") = 1
            txtProcessId.Text = ddlProcessList.SelectedItem.Value
            CreatMilestone()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Sub ClearItems()
        Try
            ddlItems.SelectedIndex = 0
            txtdesc.Text = ""
            ddltype.SelectedIndex = 0
            txtunits.Text = ""
            txtprice.Text = ""
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Sub BindItems(ByVal dsTemp As DataSet)
        Try
            tblProducts.Visible = True
            ucItem.DataSource = dsTemp
            ucItem.DataBind()

        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub










    Sub CreatMilestone()
        Try

            Dim dtSalesProcess As DataTable
            dtSalesProcess = Session("SalesProcessDetails")
            tblMilestone.Rows.Clear()
            If dtSalesProcess.Rows.Count <> 0 Then
                LoadAssignTo()
                Dim i As Integer
                Dim tblCell As TableCell
                Dim tblrow As TableRow
                Dim chkDlost As CheckBox
                Dim chkDClosed As CheckBox
                Dim boolDealWon, boolDealLost As Boolean
                Dim strWonComm, strLostComm As String
                strWonComm = ""
                strLostComm = ""
                Dim btnAdd As Button
                Dim btnDelete As Button
                Dim InitialPercentage As Integer = dtSalesProcess.Rows(0).Item(0)
                If dtSalesProcess.Rows(0).Item(0) <> 0 And dtSalesProcess.Rows(0).Item(0) <> 100 Then
                    If dtSalesProcess.Rows.Count <> 2 Then
                        If Not dtSalesProcess.Rows(0).Item("Op_Flag") = 1 Then
                            tblrow = New TableRow
                            tblCell = New TableCell
                            tblCell.Text = "<font color=white>&nbsp;&nbsp;&nbsp;&nbsp;   Milestone - " & InitialPercentage & "%&nbsp;&nbsp;" & dtSalesProcess.Rows(0).Item("vcStagePercentageDtl") & "</font>"
                            tblCell.Height = Unit.Pixel(20)
                            tblCell.CssClass = "text_bold"
                            tblCell.ColumnSpan = 6
                            tblCell.BackColor = System.Drawing.Color.FromName("#52658C")
                            tblrow.Controls.Add(tblCell)
                            tblMilestone.Controls.Add(tblrow)
                            txtTemplateId.Text = dtSalesProcess.Rows(0).Item("numTemplateId")

                            ' If Trim(lngOppId) = "" Or Viewstate("MileCheck") = 1 Then
                            tblrow = New TableRow
                            tblCell = New TableCell
                            tblCell.ColumnSpan = 6
                            btnAdd = New Button
                            btnAdd.Text = "Add"
                            btnAdd.CssClass = "button"
                            btnAdd.Width = Unit.Pixel(40)
                            btnDelete = New Button
                            btnDelete.Text = "Delete"
                            btnDelete.CssClass = "button"
                            btnAdd.ID = "btnAdd~" & InitialPercentage
                            btnDelete.ID = "btnDelete~" & InitialPercentage
                            btnDelete.Attributes.Add("onclick", "return DeletMsg()")
                            AddHandler btnAdd.Click, AddressOf btnAddClick
                            AddHandler btnDelete.Click, AddressOf btnDeleteClick
                            tblCell.HorizontalAlign = HorizontalAlign.Right
                            Dim lblSB As New Label
                            lblSB.ID = "lblSB~" & InitialPercentage
                            lblSB.Text = "&nbsp;"
                            tblCell.Controls.Add(btnAdd)
                            tblCell.Controls.Add(lblSB)
                            tblCell.Controls.Add(btnDelete)
                            tblrow.Controls.Add(tblCell)
                            tblMilestone.Controls.Add(tblrow)
                            ' End If
                        End If
                    End If
                End If
                ViewState("CheckColor") = 0
                For i = 0 To dtSalesProcess.Rows.Count - 1
                    If dtSalesProcess.Rows(i).Item(0) <> 100 And dtSalesProcess.Rows(i).Item(0) <> 0 Then
                        txtTemplateId.Text = dtSalesProcess.Rows(i).Item("numTemplateId")
                        If InitialPercentage = dtSalesProcess.Rows(i).Item(0) Then
                            If dtSalesProcess.Rows(i).Item("Op_Flag") <> 1 Then
                                createStages(dtSalesProcess.Rows(i))
                                InitialPercentage = dtSalesProcess.Rows(i).Item(0)
                            End If
                        Else
                            If dtSalesProcess.Rows(i).Item("Op_Flag") <> 1 Then
                                InitialPercentage = dtSalesProcess.Rows(i).Item(0)
                                tblrow = New TableRow
                                tblCell = New TableCell
                                tblCell.Text = "<font color=white>&nbsp;&nbsp;&nbsp;&nbsp;   Milestone - " & InitialPercentage & "%&nbsp;&nbsp;" & dtSalesProcess.Rows(i).Item("vcStagePercentageDtl") & "</font>"
                                tblCell.Height = Unit.Pixel(20)
                                tblCell.CssClass = "text_bold"
                                tblCell.ColumnSpan = 6
                                tblCell.BackColor = System.Drawing.Color.FromName("#52658C")
                                tblrow.Controls.Add(tblCell)
                                tblMilestone.Controls.Add(tblrow)


                                '  If Trim(lngOppId) = "" Or Viewstate("MileCheck") = 1 Then  
                                tblrow = New TableRow
                                tblCell = New TableCell
                                tblCell.ColumnSpan = 6
                                btnAdd = New Button
                                btnAdd.Text = "Add"
                                btnAdd.Width = Unit.Pixel(40)
                                btnAdd.CssClass = "button"
                                btnDelete = New Button
                                btnDelete.Text = "Delete"
                                btnDelete.CssClass = "button"
                                btnAdd.ID = "btnAdd~" & InitialPercentage
                                btnDelete.ID = "btnDelete~" & InitialPercentage
                                btnDelete.Attributes.Add("onclick", "return DeletMsg()")
                                AddHandler btnAdd.Click, AddressOf btnAddClick
                                AddHandler btnDelete.Click, AddressOf btnDeleteClick
                                tblCell.HorizontalAlign = HorizontalAlign.Right
                                Dim lblSBs As New Label
                                lblSBs.ID = "lblSBs" & InitialPercentage
                                lblSBs.Text = "&nbsp;"
                                tblCell.Controls.Add(btnAdd)
                                tblCell.Controls.Add(lblSBs)
                                tblCell.Controls.Add(btnDelete)
                                tblrow.Controls.Add(tblCell)
                                tblMilestone.Controls.Add(tblrow)
                                'End If
                                ViewState("CheckColor") = 0
                                createStages(dtSalesProcess.Rows(i))
                            End If
                        End If
                    ElseIf dtSalesProcess.Rows(i).Item(0) = 100 Then
                        boolDealWon = dtSalesProcess.Rows(i).Item("bitStageCompleted")
                        strWonComm = dtSalesProcess.Rows(i).Item("vcComments")
                    ElseIf dtSalesProcess.Rows(i).Item(0) = 0 Then
                        boolDealLost = dtSalesProcess.Rows(i).Item("bitStageCompleted")
                        strLostComm = dtSalesProcess.Rows(i).Item("vcComments")
                    End If
                Next

                ''Deal Conclusion
                tblrow = New TableRow
                Dim txtDComments As TextBox
                Dim lblComm As Label
                tblCell = New TableCell
                tblCell.Text = "<font color=white>&nbsp;&nbsp;&nbsp;&nbsp;   Deal Conclusion</font>"
                tblCell.Height = Unit.Pixel(20)
                tblCell.CssClass = "text_bold"
                tblCell.ColumnSpan = 6
                tblCell.BackColor = System.Drawing.Color.FromName("#52658C")
                tblrow.Controls.Add(tblCell)
                tblMilestone.Controls.Add(tblrow)

                '' deal Closed
                tblrow = New TableRow
                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                tblCell.ColumnSpan = 6
                chkDClosed = New CheckBox
                chkDClosed.ID = "chkDClosed"
                chkDClosed.Attributes.Add("onclick", "return ValidateCheckBox(1)")
                chkDClosed.Text = "Deal Won &nbsp; &nbsp; &nbsp;"
                chkDClosed.Checked = boolDealWon
                lblComm = New Label
                lblComm.Text = "Comments &nbsp; "
                txtDComments = New TextBox
                txtDComments.Text = strWonComm
                txtDComments.CssClass = "signup"
                txtDComments.ID = "txtDCComm"
                txtDComments.Width = Unit.Pixel(600)
                tblCell.Controls.Add(chkDClosed)
                tblCell.CssClass = "normal1"
                tblCell.Controls.Add(lblComm)
                tblCell.Controls.Add(txtDComments)
                tblrow.Controls.Add(tblCell)
                tblMilestone.Controls.Add(tblrow)

                '' deal Lost
                tblrow = New TableRow
                tblCell = New TableCell
                tblCell.ColumnSpan = 6
                tblCell.CssClass = "normal1"
                chkDlost = New CheckBox
                chkDlost.ID = "chkDlost"
                chkDlost.Checked = boolDealLost
                chkDlost.Text = "Deal Lost &nbsp; &nbsp; &nbsp; "
                chkDlost.Attributes.Add("onclick", "return ValidateCheckBox(2)")
                lblComm = New Label
                lblComm.Text = "Comments &nbsp; "
                txtDComments = New TextBox
                txtDComments.ID = "txtDLComm"
                txtDComments.Text = strLostComm
                txtDComments.CssClass = "signup"
                txtDComments.Width = Unit.Pixel(600)
                tblCell.Controls.Add(chkDlost)
                tblCell.Controls.Add(lblComm)
                tblCell.Controls.Add(txtDComments)
                tblrow.Controls.Add(tblCell)
                tblMilestone.Controls.Add(tblrow)
            End If
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Sub createStages(ByVal dr As DataRow)
        Try
            Dim tblCell As TableCell
            Dim tblrow As TableRow
            Dim lbl As Label
            Dim chkStage As CheckBox
            Dim txtStage As TextBox
            Dim lblStageCM As Label
            Dim lblInform As Label
            Dim txtComm As TextBox
            Dim ddlStatus As DropDownList
            Dim ddlAgginTo As DropDownList
            Dim chkAlert As CheckBox
            Dim txtChecKStage As TextBox
            Dim hpkLink As HyperLink

            '''First row
            tblrow = New TableRow
            If ViewState("CheckColor") = 1 Then
                tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
            End If
            tblCell = New TableCell
            txtStage = New TextBox
            txtStage.ID = "txtStage~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
            txtStage.Text = dr.Item("vcstageDetail")
            txtStage.CssClass = "signup"
            txtStage.Width = Unit.Pixel(250)
            tblCell.ColumnSpan = 4
            tblCell.CssClass = "normal1"
            tblCell.Controls.Add(txtStage)


            lblStageCM = New Label
            lblStageCM.Text = "&nbsp;&nbsp;Stage Status&nbsp;&nbsp;"
            lblStageCM.ID = "lblStatus" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
            ddlStatus = New DropDownList
            objCommon.sb_FillComboFromDBwithSel(ddlStatus, 42, Session("DomainID"))
            If Not IsDBNull(dr.Item("numStage")) Then
                If Not ddlStatus.Items.FindByValue(dr.Item("numStage")) Is Nothing Then
                    ddlStatus.Items.FindByValue(dr.Item("numStage")).Selected = True
                End If
            End If
            ddlStatus.CssClass = "signup"
            ddlStatus.ID = "ddlStatus~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
            tblCell.CssClass = "normal1"
            tblCell.Controls.Add(lblStageCM)
            tblCell.Controls.Add(ddlStatus)
            tblrow.Controls.Add(tblCell)

            tblCell = New TableCell
            lblStageCM = New Label
            lblStageCM.Text = "Last Modified By:  "
            lblStageCM.ID = "lblStageModifiedBy" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
            lblInform = New Label
            If Not IsDBNull(dr.Item("numModifiedBy")) Then
                lblInform.Text = IIf(dr.Item("numModifiedBy") = 0, "", dr.Item("numModifiedByName"))
            End If
            lblInform.ID = "lblInformBy" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
            tblCell.CssClass = "normal1"
            tblCell.Controls.Add(lblStageCM)
            tblCell.Controls.Add(lblInform)
            tblrow.Controls.Add(tblCell)


            tblCell = New TableCell
            lblStageCM = New Label
            lblStageCM.Text = "Last Modified Date:  "
            lblStageCM.ID = "lblStageModified" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
            lblInform = New Label
            If Not IsDBNull(dr.Item("bintModifiedDate")) Then
                lblInform.Text = FormattedDateFromDate(dr.Item("bintModifiedDate"), Session("DateFormat"))
            End If
            lblInform.ID = "lblInform" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
            tblCell.CssClass = "normal1"
            tblCell.Controls.Add(lblStageCM)
            tblCell.Controls.Add(lblInform)
            tblrow.Controls.Add(tblCell)
            tblMilestone.Controls.Add(tblrow)

            'Second Row
            tblrow = New TableRow
            If ViewState("CheckColor") = 1 Then
                tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
            End If
            tblCell = New TableCell
            tblCell.CssClass = "normal1"
            tblCell.ColumnSpan = 4
            lbl = New Label
            txtComm = New TextBox
            txtComm.ID = "txtComm~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
            txtComm.Width = Unit.Pixel(750)
            txtComm.CssClass = "signup"
            txtComm.Text = dr.Item("vcComments")
            lbl.Text = "Comments &nbsp;"
            tblCell.Controls.Add(lbl)
            tblCell.Controls.Add(txtComm)
            tblCell.CssClass = "normal1"
            tblrow.Controls.Add(tblCell)
            tblCell = New TableCell
            tblCell.CssClass = "normal1"
            lbl = New Label
            lbl.Text = "This stage comprises "
            tblCell.Controls.Add(lbl)
            tblCell.ColumnSpan = 2
            Dim txtStagePer As New TextBox
            txtStagePer.ID = "txtStagePer~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
            txtStagePer.Width = Unit.Pixel(30)
            txtStagePer.CssClass = "signup"
            txtStagePer.Text = dr.Item("tintPercentage")
            tblCell.Controls.Add(txtStagePer)
            lbl = New Label
            lbl.Text = "  % of completion"
            tblCell.Controls.Add(lbl)
            tblrow.Controls.Add(tblCell)
            tblMilestone.Controls.Add(tblrow)

            ''Third Row


            If ViewState("MileCheck") = 0 Then
                tblrow = New TableRow
                If ViewState("CheckColor") = 1 Then
                    tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
                End If
                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                hpkLink = New HyperLink
                hpkLink.ID = "hpkLinkTime" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")

                hpkLink.Attributes.Add("onclick", "return OpenTime(" & dr.Item("OppStageID") & "," & lngOppId & "," & dr.Item("numStagePercentage") & "," & dr.Item("numstagedetailsID") & "," & lngDivId & ");")

                hpkLink.Text = "<font class='hyperlink'>Time</font>"
                hpkLink.CssClass = "hyperlink"
                lbl = New Label
                lbl.ID = "lblTime" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                If Not IsDBNull(dr.Item("Time")) Then
                    lbl.Text = "<font color=red>&nbsp;" & dr.Item("Time") & "</font>"
                End If
                tblCell.Controls.Add(hpkLink)
                tblCell.Controls.Add(lbl)
                tblrow.Controls.Add(tblCell)



                tblCell = New TableCell
                tblCell.ColumnSpan = 3
                hpkLink = New HyperLink
                hpkLink.ID = "hpkLinkExpense" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                'If Not dr.Item("numOppStageID") Is Nothing Then
                hpkLink.Attributes.Add("onclick", "return OpenExpense(" & dr.Item("OppStageID") & "," & lngOppId & "," & dr.Item("numStagePercentage") & "," & dr.Item("numstagedetailsID") & "," & lngDivId & ");")

                hpkLink.Text = "<font class='hyperlink'>Expense</font>"
                hpkLink.CssClass = "hyperlink"
                tblCell.CssClass = "normal1"
                lbl = New Label
                lbl.ID = "lblExp" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                If Not IsDBNull(dr.Item("Expense")) Then
                    lbl.Text = "<font color=red>&nbsp;" & String.Format("{0:#,##0.00}", CDec(dr.Item("Expense"))) & "</font>"
                End If
                tblCell.Controls.Add(hpkLink)
                tblCell.Controls.Add(lbl)
                tblrow.Controls.Add(tblCell)

                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                hpkLink = New HyperLink
                hpkLink.ID = "hpkLinkDependency" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")

                hpkLink.Attributes.Add("onclick", "return OpenDependency(" & dr.Item("OppStageID") & "," & lngOppId & "," & dr.Item("numStagePercentage") & "," & dr.Item("numstagedetailsID") & ");")
                'End If
                hpkLink.Text = "<font class='hyperlink'>Dependency</font>"
                hpkLink.CssClass = "hyperlink"
                lbl = New Label
                lbl.ID = "lblDep" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                If Not IsDBNull(dr.Item("Depend")) Then
                    If dr.Item("Depend") = "1" Then
                        lbl.Text = "<font color='red' face='Wingdings 2'>&nbsp;" & 8 & "</font>"
                    End If
                End If
                tblCell.Controls.Add(hpkLink)
                tblCell.Controls.Add(lbl)
                tblrow.Controls.Add(tblCell)

                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                lbl = New Label
                hpkLink = New HyperLink
                hpkLink.ID = "hpkLinkSubStage" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                hpkLink.Text = "<font class='hyperlink'>Sub Stages</font>"
                hpkLink.CssClass = "hyperlink"
                hpkLink.Attributes.Add("onclick", "return OpenSubStage(" & dr.Item("OppStageID") & "," & lngOppId & "," & dr.Item("numStagePercentage") & "," & dr.Item("numstagedetailsID") & ");")
                'End If
                lbl.ID = "lblStg" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                If Not IsDBNull(dr.Item("SubStg")) Then
                    If dr.Item("SubStg") = "1" Then
                        lbl.Text = "<font color='red' face='Wingdings 2'>&nbsp;" & 8 & "</font>"
                    End If
                End If
                tblCell.Controls.Add(hpkLink)
                tblCell.Controls.Add(lbl)
                tblrow.Controls.Add(tblCell)
                tblMilestone.Controls.Add(tblrow)
            End If

            ''''Fourth Row
            tblrow = New TableRow
            If ViewState("CheckColor") = 1 Then
                tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
            End If
            tblCell = New TableCell
            tblCell.CssClass = "normal1"
            lbl = New Label
            lbl.Text = "&nbsp;Assign To"
            tblCell.Controls.Add(lbl)
            ddlAgginTo = New DropDownList
            ddlAgginTo.Width = Unit.Pixel(150)
            ddlAgginTo.ID = "ddlAgginTo~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
            ddlAgginTo.CssClass = "signup"
            ddlAgginTo.DataSource = dtAssignTo
            ddlAgginTo.DataTextField = "vcName"
            ddlAgginTo.DataValueField = "ContactID"
            ddlAgginTo.DataBind()
            ddlAgginTo.Items.Insert(0, "--Select One--")
            ddlAgginTo.Items.FindByText("--Select One--").Value = 0
            If Not IsDBNull(dr.Item("numAssignTo")) Then
                If Not ddlAgginTo.Items.FindByValue(dr.Item("numAssignTo")) Is Nothing Then
                    ddlAgginTo.Items.FindByValue(dr.Item("numAssignTo")).Selected = True
                End If
            End If

            tblCell.Controls.Add(ddlAgginTo)
            tblrow.Controls.Add(tblCell)

            tblCell = New TableCell
            tblCell.CssClass = "normal1"
            chkAlert = New CheckBox
            chkAlert.ID = "chkAlert~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
            'If dr.Item("bitAlert") = 0 Then
            '    chkAlert.Checked = False
            'Else
            '    chkAlert.Checked = True
            'End If
            lbl = New Label
            lbl.Text = "Alert"
            tblCell.Controls.Add(lbl)
            tblCell.Controls.Add(chkAlert)

            tblCell.CssClass = "normal1"
            lbl = New Label
            lbl.Text = "&nbsp; &nbsp; &nbsp; "
            tblCell.Controls.Add(lbl)
            tblrow.Cells.Add(tblCell)
            tblCell = New TableCell
            tblCell.CssClass = "normal1"
            tblCell.HorizontalAlign = HorizontalAlign.Right
            lbl = New Label
            lbl.Text = "Due Date: &nbsp;"
            tblCell.Controls.Add(lbl)
            tblrow.Cells.Add(tblCell)
            tblCell = New TableCell
            Dim bizCalendar As UserControl = LoadControl("../common/calandar.ascx")
            bizCalendar.ID = "cal" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
            tblCell.Controls.Add(bizCalendar)
            tblrow.Cells.Add(tblCell)



            tblCell = New TableCell

            tblCell.CssClass = "normal1"
            lbl = New Label
            lbl.Text = "Stage Completed : "
            tblCell.Controls.Add(lbl)
            lbl = New Label
            If Not IsDBNull(dr.Item("bintStageComDate")) Then
                If dr.Item("bitStageCompleted") = True Then
                    lbl.Text = FormattedDateFromDate(dr.Item("bintStageComDate"), Session("DateFormat"))
                End If
            End If
            tblCell.Controls.Add(lbl)
            tblrow.Controls.Add(tblCell)

            tblCell = New TableCell
            tblCell.CssClass = "normal1"
            lbl = New Label
            chkStage = New CheckBox
            txtChecKStage = New TextBox
            txtChecKStage.ID = "txtChecKStage~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
            txtChecKStage.Attributes.Add("style", "display:none")
            If dr.Item("bitStageCompleted") = False Then
                txtChecKStage.Text = 0
                lbl.Text = "Stage Closed"
            Else
                txtChecKStage.Text = 1
                lbl.Text = "<font color='#CC0000'><b>Stage Closed</b></font>"
                chkStage.Checked = True
            End If

            chkStage.ID = "chkStage~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
            tblCell.Controls.Add(lbl)
            tblCell.Controls.Add(txtChecKStage)
            tblCell.Controls.Add(chkStage)
            tblrow.Controls.Add(tblCell)
            tblMilestone.Controls.Add(tblrow)
            If Not IsDBNull(dr.Item("bintDueDate")) Then
                ' bizCalendar. = FormattedDateFromDate(dr.Item("bintDueDate"), Session("DateFormat"))
                Dim _myControlType As Type = bizCalendar.GetType()
                Dim _myUC_DueDate As PropertyInfo = _myControlType.GetProperty("SelectedDate")
                _myUC_DueDate.SetValue(bizCalendar, CStr(dr.Item("bintDueDate")), Nothing)
            End If


            If ViewState("CheckColor") = 1 Then
                ViewState("CheckColor") = 0
            Else
                ViewState("CheckColor") = 1
            End If
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadSalesProcess()
        Try
            Dim dtSalesProcess As DataTable
            If Session("OppType") = 1 Then
                objOpportunity.ProType = 0
            Else
                objOpportunity.ProType = 2
            End If
            objOpportunity.DomainID = Session("DomainID")
            dtSalesProcess = objOpportunity.BusinessProcess
            BindSaleProcess(dtSalesProcess)
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Sub BindSaleProcess(ByVal dtTable As DataTable)
        Try
            ddlProcessList.DataSource = dtTable
            ddlProcessList.DataTextField = "slp_name"
            ddlProcessList.DataValueField = "slp_id"
            ddlProcessList.DataBind()
            ddlProcessList.Items.Insert(0, "--Select One--")
            ddlProcessList.Items.FindByText("--Select One--").Value = 0
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadAssignTo()
        Try
            objOpportunity.DomainID = Session("DomainId")
            objOpportunity.ContactType = 92
            dtAssignTo = objOpportunity.AssignTo
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadItem(ByVal ddlitems As DropDownList, ByVal intOpptype As Integer, ByVal intDivisionID As Integer)
        Try
            If intOpptype = 1 Then
                Dim dtItem As DataTable
                objOpportunity.DomainID = Session("DomainId")
                dtItem = objOpportunity.ItemByDomainID
                ddlitems.DataSource = dtItem
                ddlitems.DataTextField = "vcItemname"
                ddlitems.DataValueField = "numItemcode"
                ddlitems.DataBind()
                ddlitems.Items.Insert(0, "--Select One--")
                ddlitems.Items.FindByText("--Select One--").Value = 0
            Else
                objCommon.sb_FillComboPurchaseItem(ddlitems, intDivisionID, Session("DomainID"))
            End If

        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgContact_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgContact.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim btnDelete As Button
                Dim lnkDelete As LinkButton
                lnkDelete = e.Item.FindControl("lnkDeleteCnt")
                btnDelete = e.Item.FindControl("btnDeleteCnt")
                ' If m_aryRightsForAssContacts(RIGHTSTYPE.DELETE) = 0 Then
                btnDelete.Visible = False
                lnkDelete.Visible = True
                lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                'Else
                '  btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                '  End If
                Dim lbl As Label
                lbl = e.Item.FindControl("lblContcRoleid")
                Dim ddlContRole As DropDownList
                ddlContRole = e.Item.FindControl("ddlContactRole")

                If lbl.Text <> "" Then
                    If Not ddlContRole.Items.FindByValue(lbl.Text) Is Nothing Then
                        ddlContRole.Items.FindByValue(lbl.Text).Selected = True
                    End If
                End If
                If e.Item.Cells(1).Text = 1 Then
                    CType(e.Item.FindControl("chkShare"), CheckBox).Checked = True
                End If

            End If
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgContact_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgContact.ItemCommand
        Try

        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub



    Sub btnAddClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim ctrlId As String = sender.id
            Dim strSplit As String()
            strSplit = ctrlId.Split("~")
            Dim dtSalesDetails As DataTable
            dtSalesDetails = Session("SalesProcessDetails")

            myRow = dtSalesDetails.NewRow
            myRow("numStagePercentage") = strSplit(1)
            myRow("numstagedetailsID") = CType(dtSalesDetails.Compute("MAX(numstagedetailsID)", ""), Integer) + 1
            myRow("vcstageDetail") = ""
            myRow("OppStageID") = 0
            myRow("numDomainId") = Session("DomainId")
            myRow("numCreatedBy") = Session("UserContactID")
            myRow("bintCreatedDate") = Now
            myRow("numModifiedBy") = Session("UserContactID")
            myRow("bintModifiedDate") = Now
            myRow("bintDueDate") = Now
            myRow("bintStageComDate") = System.DBNull.Value
            myRow("vcComments") = ""
            myRow("numAssignTo") = 0
            myRow("bitAlert") = 0
            myRow("bitStageCompleted") = 0
            myRow("Op_Flag") = 0
            myRow("numModifiedByName") = ""
            myRow("numStage") = 0
            dtSalesDetails.Rows.Add(myRow)
            Dim ds As New DataSet

            dtSalesDetails.TableName = "Table"
            ds.Tables.Add(dtSalesDetails.Copy)
            objOpportunity.strMilestone = ds.GetXml
            ds.Tables.Remove(ds.Tables(0))
            objOpportunity.OpportunityId = lngOppId


            If dtSalesDetails.Rows.Count = 0 Then
                objOpportunity.bytemode = 0
            Else
                objOpportunity.bytemode = 1
            End If
            If ViewState("byteMode") = 1 Then
                objOpportunity.bytemode = 0
                ViewState("byteMode") = Nothing
            End If
            objOpportunity.UserCntID = Session("UserContactID")
            objOpportunity.SaveMilestone()
            dtSalesDetails = objOpportunity.BusinessProcessByOpID

            Session("SalesProcessDetails") = dtSalesDetails
            CreatMilestone()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Sub btnDeleteClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Dim ctrlId As String = sender.id
            Dim strSplit As String()
            Dim i As Integer
            strSplit = ctrlId.Split("~")
            Dim dtSalesDetails As DataTable
            dtSalesDetails = Session("SalesProcessDetails")
            For i = 0 To dtSalesDetails.Rows.Count - 1
                If strSplit(1) = dtSalesDetails.Rows(i).Item("numStagePercentage") And dtSalesDetails.Rows(i).Item("Op_Flag") = 0 Then
                    Dim chk As CheckBox
                    chk = uwOppTab.FindControl("chkStage~" & strSplit(1) & "~" & dtSalesDetails.Rows(i).Item("numstagedetailsID"))
                    If chk.Checked = False Then
                    Else
                        dtSalesDetails.Rows(i).Item("Op_Flag") = 1
                    End If
                End If
            Next
            Session("SalesProcessDetails") = dtSalesDetails
            CreatMilestone()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub


    Sub SaveOpportunity()
        Try
            CreateTableMilestone()
            objOpportunity.OpportunityId = lngOppId
            objOpportunity.OppComments = txtOComments.Text
            objOpportunity.OpportunityName = txtName.Text.Trim
            objOpportunity.BillingTerms = IIf(chkBillinTerms.Checked = True, 1, 0)
            objOpportunity.BillingDays = IIf(txtNetdays.Text = "", 0, txtNetdays.Text)
            objOpportunity.InterestType = IIf(radPlus.Checked = True, 1, 0)
            objOpportunity.Interest = IIf(Replace(txtInterest.Text, ",", "") = "", 0, Replace(txtInterest.Text, ",", ""))
            objOpportunity.CampaignID = ddlCampaign.SelectedItem.Value
            objOpportunity.PublicFlag = 0
            objOpportunity.Source = ddlSource.SelectedItem.Value
            objOpportunity.UserCntID = Session("UserContactID")
            If calDue.SelectedDate <> "" Then
                objOpportunity.EstimatedCloseDate = calDue.SelectedDate
            End If
            objOpportunity.ConAnalysis = ddlClReason.SelectedItem.Value
            objOpportunity.DomainID = Session("DomainId")
            objOpportunity.OppType = Session("OppType")
            objOpportunity.Active = IIf(chkActive.Checked = True, 1, 0)
            objOpportunity.SalesorPurType = ddlSalesorPurType.SelectedValue

            objOpportunity.AssignedTo = ddlAssignedTo.SelectedValue
            If calShip.SelectedDate <> "" Then
                objOpportunity.ShipDate = calShip.SelectedDate
            End If
            If txtShipCost.Text <> "" Then
                objOpportunity.ShipCost = IIf(Replace(txtShipCost.Text, ",", "") = "", 0, Replace(txtShipCost.Text, ",", ""))
            End If
            If ddlShipCompany.SelectedIndex <> -1 Then
                objOpportunity.ShipVia = ddlShipCompany.SelectedItem.Value
            End If
            objOpportunity.TrackingURL = txttrackingURL.Text
            Dim dsNew As New DataSet

            Dim dtTable As DataTable
            If Not Session("SalesProcessDetails") Is Nothing Then

                dtTable = Session("SalesProcessDetails")
                If dtTable.Rows.Count > 0 Then
                    dtTable.TableName = "Table"
                    dsNew.Tables.Add(dtTable.Copy)
                    objOpportunity.strMilestone = dsNew.GetXml
                    dsNew.Tables.Remove(dsNew.Tables(0))
                End If
            End If
            If Not Session("Data") Is Nothing Then
                dsTemp = Session("Data")
                objOpportunity.strItems = dsTemp.GetXml
            End If


            arrOutPut = objOpportunity.Save()
            objOpportunity.OpportunityId = arrOutPut(0)
            If dtTable.Rows.Count = 0 Then
                objOpportunity.bytemode = 0
            Else
                objOpportunity.bytemode = 1
            End If
            If ViewState("byteMode") = 1 Then
                objOpportunity.bytemode = 0
                ViewState("byteMode") = Nothing
            End If
            objOpportunity.SaveMilestone()


            'sending and Email To all addinies
            'If txtTemplateId.Text <> "0" Then

            '    Dim arr As New ArrayList

            '    'Code to check for duplicate entries
            '    '------------------------------------------------------------------------------------------------------------------------------
            '    For Each row As DataRow In dtProcess.Rows
            '        If Not arr.Contains(row.Item("numAssignTo")) Then
            '            If row.Item("numAssignTo") <> 0 Then
            '                arr.Add(row.Item("numAssignTo"))
            '            End If
            '        End If
            '    Next
            '    '------------------------------------------------------------------------------------------------------------------------------
            '    Dim objAlerts As New CAlerts
            '    Dim dtDetails As New DataTable
            '    objAlerts.AlertDTLID = 5 'Alert DTL ID for sending alerts in opportunities
            '    dtDetails = objAlerts.GetIndividualAlertdtl
            '    Dim dtEmailTemplate As New DataTable
            '    Dim objDocuments As New DocumentList
            '    objDocuments.GenDocID = CInt(txtTemplateId.Text)
            '    dtEmailTemplate = objDocuments.GetDocumentsByGenDocID
            '    If dtEmailTemplate.Rows.Count > 0 Then
            '        If arr.Count > 0 Then
            '            Dim i As Integer
            '            For i = 0 To arr.Count - 1
            '                Dim objSendMail As New clsSendEmail
            '                Dim dtMergeFields As New DataTable
            '                Dim drNew As DataRow
            '                dtMergeFields.Columns.Add("Assignee")
            '                dtMergeFields.Columns.Add("OppID")
            '                dtMergeFields.Columns.Add("DueDate")
            '                dtMergeFields.Columns.Add("Stage")
            '                dtMergeFields.Columns.Add("CommName")
            '                drNew = dtMergeFields.NewRow
            '                drNew("Assignee") = arr(i)
            '                drNew("CommName") = txtOComments.Text
            '                drNew("OppID") = GetQueryStringVal(Request.QueryString("enc"),"OpId")
            '                drNew("DueDate") = calDue.SelectedDate
            '                drNew("Stage") = calDue.SelectedDate
            '                dtMergeFields.Rows.Add(drNew)
            '                Dim objCommon As New CCommon
            '                objCommon.byteMode = 0
            '                objCommon.ContactID = arr(i)
            '                objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), IIf(dtDetails.Rows(0).Item("tintCCManager") = 1, objCommon.GetManagerEmail, ""), Session("UserEmail"), objCommon.GetContactsEmail(), dtMergeFields)
            '            Next
            '        End If
            '        End If
            'End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Sub SendEmailStage()
    '    Try
    '        Dim dtSalesProcess As New DataTable
    '        Dim i, intModified As Integer
    '        If Not Session("SalesProcessDetails") Is Nothing Then
    '            dtSalesProcess = Session("SalesProcessDetails")
    '            If dtSalesProcess.Rows.Count <> 0 Then

    '            End If
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub



    Sub CreateTableMilestone()
        Try
            Dim dtSalesProcess As DataTable
            Dim i, intModified As Integer
            If Not Session("SalesProcessDetails") Is Nothing Then
                dtSalesProcess = Session("SalesProcessDetails")
                If dtSalesProcess.Rows.Count <> 0 Then
                    For i = 0 To dtSalesProcess.Rows.Count - 1
                        If dtSalesProcess.Rows(i).Item("numStagePercentage") <> 100 And dtSalesProcess.Rows(i).Item("numStagePercentage") <> 0 Then
                            If Not dtSalesProcess.Rows(i).Item("Op_Flag") = 1 Then
                                Dim txtchkStage As TextBox
                                Dim chkStage As CheckBox
                                Dim ddlStatus As DropDownList
                                ddlStatus = uwOppTab.FindControl("ddlStatus~" & dtSalesProcess.Rows(i).Item("numStagePercentage") & "~" & dtSalesProcess.Rows(i).Item("numstagedetailsID"))
                                txtchkStage = uwOppTab.FindControl("txtChecKStage~" & dtSalesProcess.Rows(i).Item("numStagePercentage") & "~" & dtSalesProcess.Rows(i).Item("numstagedetailsID"))
                                Dim txtStageDetails As TextBox
                                txtStageDetails = uwOppTab.FindControl("txtStage~" & dtSalesProcess.Rows(i).Item("numStagePercentage") & "~" & dtSalesProcess.Rows(i).Item("numstagedetailsID"))
                                If dtSalesProcess.Rows(i).Item("vcstageDetail") <> txtStageDetails.Text Then
                                    intModified = 1
                                End If
                                dtSalesProcess.Rows(i).Item("vcstageDetail") = txtStageDetails.Text
                                chkStage = uwOppTab.FindControl("chkStage~" & dtSalesProcess.Rows(i).Item("numStagePercentage") & "~" & dtSalesProcess.Rows(i).Item("numstagedetailsID"))
                                If txtchkStage.Text = 0 And chkStage.Checked = True Then
                                    dtSalesProcess.Rows(i).Item("bitStageCompleted") = True
                                    dtSalesProcess.Rows(i).Item("bintStageComDate") = Now

                                ElseIf txtchkStage.Text = 1 And chkStage.Checked = False Then
                                    dtSalesProcess.Rows(i).Item("bitStageCompleted") = False
                                    dtSalesProcess.Rows(i).Item("bintStageComDate") = System.DBNull.Value
                                End If
                                If dtSalesProcess.Rows(i).Item("numStage") <> ddlStatus.SelectedItem.Value Then
                                    intModified = 1
                                End If
                                dtSalesProcess.Rows(i).Item("numStage") = ddlStatus.SelectedItem.Value

                                Dim txtStageComments As TextBox
                                txtStageComments = uwOppTab.FindControl("txtComm~" & dtSalesProcess.Rows(i).Item("numStagePercentage") & "~" & dtSalesProcess.Rows(i).Item("numstagedetailsID"))
                                If dtSalesProcess.Rows(i).Item("vcComments") <> txtStageComments.Text Then
                                    intModified = 1
                                End If
                                dtSalesProcess.Rows(i).Item("vcComments") = txtStageComments.Text
                                Dim chkAlert As CheckBox
                                chkAlert = uwOppTab.FindControl("chkAlert~" & dtSalesProcess.Rows(i).Item("numStagePercentage") & "~" & dtSalesProcess.Rows(i).Item("numstagedetailsID"))
                                If chkAlert.Checked = True Then
                                    dtSalesProcess.Rows(i).Item("bitAlert") = 1
                                End If
                                Dim BizCalendar As UserControl
                                BizCalendar = uwOppTab.FindControl("cal" & dtSalesProcess.Rows(i).Item("numStagePercentage") & "~" & dtSalesProcess.Rows(i).Item("numstagedetailsID"))

                                Dim strDueDate As String
                                Dim _myControlType As Type = BizCalendar.GetType()
                                Dim _myUC_DueDate As PropertyInfo = _myControlType.GetProperty("SelectedDate")
                                strDueDate = _myUC_DueDate.GetValue(BizCalendar, Nothing)

                                'txtDueDate = uwOppTab.FindControl("cal" & dtSalesProcess.Rows(i).Item("numStagePercentage") & dtSalesProcess.Rows(i).Item("numstagedetailsID"))

                                If Not strDueDate = "" Then
                                    dtSalesProcess.Rows(i).Item("bintDueDate") = strDueDate
                                    If dtSalesProcess.Rows(i).Item("bintDueDate") <> strDueDate Then
                                        intModified = 1
                                    End If
                                Else
                                    If IsDBNull(dtSalesProcess.Rows(i).Item("bintDueDate")) Then
                                        intModified = 1
                                    End If
                                End If
                                Dim ddlAssignTo As DropDownList
                                ddlAssignTo = uwOppTab.FindControl("ddlAgginTo~" & dtSalesProcess.Rows(i).Item("numStagePercentage") & "~" & dtSalesProcess.Rows(i).Item("numstagedetailsID"))
                                If dtSalesProcess.Rows(i).Item("numAssignTo") <> ddlAssignTo.SelectedItem.Value Then
                                    intModified = 1
                                End If
                                dtSalesProcess.Rows(i).Item("numAssignTo") = ddlAssignTo.SelectedItem.Value
                                Dim txtStagePer As TextBox
                                txtStagePer = uwOppTab.FindControl("txtStagePer~" & dtSalesProcess.Rows(i).Item("numStagePercentage") & "~" & dtSalesProcess.Rows(i).Item("numstagedetailsID"))
                                dtSalesProcess.Rows(i).Item("tintPercentage") = IIf(txtStagePer.Text = "", 0, txtStagePer.Text)
                                'Sending Mail to the next Stage assignee
                                If txtchkStage.Text = 0 And chkStage.Checked = True Then
                                    Try
                                        intModified = 1
                                        Dim objAlerts As New CAlerts
                                        Dim dtDetails As DataTable
                                        objAlerts.AlertDTLID = 2 'Alert DTL ID for sending alerts in opportunities
                                        objAlerts.DomainID = Session("DomainID")
                                        dtDetails = objAlerts.GetIndAlertDTL
                                        If dtDetails.Rows.Count > 0 Then
                                            If dtDetails.Rows(0).Item("tintAlertOn") = 1 Then
                                                Dim dtEmailTemplate As DataTable
                                                Dim objDocuments As New DocumentList
                                                objDocuments.GenDocID = dtDetails.Rows(0).Item("numEmailTemplate")
                                                objDocuments.DomainID = Session("DomainID")
                                                dtEmailTemplate = objDocuments.GetDocByGenDocID
                                                If dtEmailTemplate.Rows.Count > 0 Then
                                                    Dim objSendMail As New clsSendEmail
                                                    Dim dtMergeFields As New DataTable
                                                    Dim drNew As DataRow
                                                    dtMergeFields.Columns.Add("Assignee")
                                                    dtMergeFields.Columns.Add("Stage")
                                                    dtMergeFields.Columns.Add("DueDate")
                                                    dtMergeFields.Columns.Add("OppID")
                                                    drNew = dtMergeFields.NewRow
                                                    drNew("Assignee") = ddlAssignTo.SelectedItem.Text
                                                    drNew("Stage") = txtStageDetails.Text
                                                    drNew("DueDate") = FormattedDateFromDate(strDueDate, Session("DateFormat"))
                                                    drNew("OppID") = txtName.Text
                                                    dtMergeFields.Rows.Add(drNew)
                                                    If dtSalesProcess.Rows(i + 1).Item("numStagePercentage") <> 100 Then
                                                        If dtSalesProcess.Rows(i + 1).Item("numStagePercentage") <> 0 Then
                                                            If Not dtSalesProcess.Rows(i + 1).Item("Op_Flag") = 1 Then
                                                                Dim ddlNAssignTo As DropDownList
                                                                ddlNAssignTo = uwOppTab.FindControl("ddlAgginTo~" & dtSalesProcess.Rows(i + 1).Item("numStagePercentage") & "~" & dtSalesProcess.Rows(i + 1).Item("numstagedetailsID"))

                                                                objCommon.byteMode = 1
                                                                objCommon.ContactID = ddlNAssignTo.SelectedItem.Value
                                                                objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), IIf(dtDetails.Rows(0).Item("tintCCManager") = 1, objCommon.GetManagerEmail, ""), Session("UserEmail"), objCommon.GetContactsEmail, dtMergeFields)
                                                            End If
                                                        End If
                                                    End If
                                                End If
                                            End If
                                        End If
                                    Catch ex As Exception

                                    End Try
                                End If
                                If txtProcessId.Text <> "0" And txtTemplateId.Text <> "0" Then
                                    Try
                                        intModified = 1
                                        Dim objAlerts As New CAlerts
                                        Dim dtDetails As DataTable
                                        objAlerts.AlertDTLID = 2 'Alert DTL ID for sending alerts in opportunities
                                        objAlerts.DomainID = Session("DomainID")
                                        dtDetails = objAlerts.GetIndAlertDTL
                                        If dtDetails.Rows.Count > 0 Then
                                            If dtDetails.Rows(0).Item("tintAlertOn") = 1 Then
                                                Dim dtEmailTemplate As DataTable
                                                Dim objDocuments As New DocumentList
                                                objDocuments.GenDocID = CInt(txtTemplateId.Text)
                                                objDocuments.DomainID = Session("DomainID")
                                                dtEmailTemplate = objDocuments.GetDocByGenDocID
                                                If dtEmailTemplate.Rows.Count > 0 Then
                                                    Dim objSendMail As New clsSendEmail
                                                    Dim dtMergeFields As New DataTable
                                                    Dim drNew As DataRow
                                                    dtMergeFields.Columns.Add("Assignee")
                                                    dtMergeFields.Columns.Add("Stage")
                                                    dtMergeFields.Columns.Add("DueDate")
                                                    dtMergeFields.Columns.Add("OppID")
                                                    drNew = dtMergeFields.NewRow
                                                    drNew("Assignee") = ddlAssignTo.SelectedItem.Text
                                                    drNew("Stage") = txtStageDetails.Text
                                                    drNew("DueDate") = FormattedDateFromDate(strDueDate, Session("DateFormat"))
                                                    drNew("OppID") = txtName.Text
                                                    dtMergeFields.Rows.Add(drNew)
                                                    If dtSalesProcess.Rows(i + 1).Item("numStagePercentage") <> 100 Then
                                                        If dtSalesProcess.Rows(i + 1).Item("numStagePercentage") <> 0 Then
                                                            If Not dtSalesProcess.Rows(i + 1).Item("Op_Flag") = 1 Then
                                                                Dim ddlNAssignTo As DropDownList
                                                                ddlNAssignTo = uwOppTab.FindControl("ddlAgginTo~" & dtSalesProcess.Rows(i + 1).Item("numStagePercentage") & "~" & dtSalesProcess.Rows(i + 1).Item("numstagedetailsID"))

                                                                objCommon.byteMode = 1
                                                                objCommon.ContactID = ddlNAssignTo.SelectedItem.Value
                                                                objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), IIf(dtDetails.Rows(0).Item("tintCCManager") = 1, objCommon.GetManagerEmail, ""), Session("UserEmail"), objCommon.GetContactsEmail, dtMergeFields)
                                                            End If
                                                        End If
                                                    End If
                                                End If
                                            End If
                                        End If
                                    Catch ex As Exception

                                    End Try
                                End If
                                If intModified = 1 Then
                                    dtSalesProcess.Rows(i).Item("numModifiedBy") = Session("UserContactID")
                                    dtSalesProcess.Rows(i).Item("bintModifiedDate") = Now
                                End If
                                intModified = 0
                                If chkAlert.Checked = True And ddlAssignTo.SelectedIndex > 0 Then
                                    Try

                                        Dim objAlerts As New CAlerts
                                        Dim dtDetails As DataTable
                                        objAlerts.AlertDTLID = 1 'Alert DTL ID for sending alerts in opportunities
                                        objAlerts.DomainID = Session("DomainID")
                                        dtDetails = objAlerts.GetIndAlertDTL
                                        If dtDetails.Rows.Count > 0 Then
                                            If dtDetails.Rows(0).Item("tintAlertOn") = 1 Then
                                                Dim dtEmailTemplate As DataTable
                                                Dim objDocuments As New DocumentList
                                                objDocuments.GenDocID = dtDetails.Rows(0).Item("numEmailTemplate")
                                                objDocuments.DomainID = Session("DomainID")
                                                dtEmailTemplate = objDocuments.GetDocByGenDocID
                                                If dtEmailTemplate.Rows.Count > 0 Then
                                                    Dim objSendMail As New clsSendEmail
                                                    Dim dtMergeFields As New DataTable
                                                    Dim drNew As DataRow
                                                    dtMergeFields.Columns.Add("Assignee")
                                                    dtMergeFields.Columns.Add("Stage")
                                                    dtMergeFields.Columns.Add("DueDate")
                                                    dtMergeFields.Columns.Add("OppID")
                                                    drNew = dtMergeFields.NewRow
                                                    drNew("Assignee") = ddlAssignTo.SelectedItem.Text
                                                    drNew("Stage") = txtStageDetails.Text
                                                    drNew("DueDate") = FormattedDateFromDate(strDueDate, Session("DateFormat"))
                                                    drNew("OppID") = txtName.Text
                                                    dtMergeFields.Rows.Add(drNew)

                                                    objCommon.byteMode = 1
                                                    objCommon.ContactID = ddlAssignTo.SelectedItem.Value
                                                    objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), IIf(dtDetails.Rows(0).Item("tintCCManager") = 1, objCommon.GetManagerEmail, ""), Session("UserEmail"), objCommon.GetContactsEmail, dtMergeFields)
                                                End If
                                            End If
                                        End If
                                    Catch ex As Exception

                                    End Try
                                End If
                            End If
                        ElseIf dtSalesProcess.Rows(i).Item("numStagePercentage") = 100 Then
                            Dim txtDealClosedComments As TextBox
                            Dim chkDC As CheckBox
                            txtDealClosedComments = uwOppTab.FindControl("txtDCComm")
                            chkDC = uwOppTab.FindControl("chkDClosed")
                            If chkDC.Checked = True Then
                                dtSalesProcess.Rows(i).Item("bitStageCompleted") = 1
                                ' Sending Mail to Record Owner's Manager
                                Dim objAlerts As New CAlerts
                                Try
                                    Dim dtDetails As DataTable
                                    objAlerts.AlertDTLID = 7 'Alert DTL ID for sending alerts in opportunities
                                    objAlerts.DomainID = Session("DomainID")
                                    dtDetails = objAlerts.GetIndAlertDTL
                                    If dtDetails.Rows.Count > 0 Then
                                        If dtDetails.Rows(0).Item("tintAlertOn") = 1 Then
                                            Dim dtEmailTemplate As DataTable
                                            Dim objDocuments As New DocumentList
                                            objDocuments.GenDocID = dtDetails.Rows(0).Item("numEmailTemplate")
                                            objDocuments.DomainID = Session("DomainID")
                                            dtEmailTemplate = objDocuments.GetDocByGenDocID
                                            If dtEmailTemplate.Rows.Count > 0 Then
                                                Dim objSendMail As New clsSendEmail
                                                Dim dtMergeFields As New DataTable
                                                Dim drNew As DataRow
                                                dtMergeFields.Columns.Add("Employee")
                                                dtMergeFields.Columns.Add("DealAmount")
                                                dtMergeFields.Columns.Add("Organization")
                                                drNew = dtMergeFields.NewRow
                                                drNew("Employee") = lblRecordOwner.Text
                                                drNew("DealAmount") = lblAmount.Text
                                                drNew("Organization") = lblCustomerName.Text


                                                Dim dtEmail As DataTable
                                                objAlerts.AlertDTLID = 7
                                                objAlerts.DomainID = Session("DomainId")
                                                dtEmail = objAlerts.GetAlertEmails
                                                Dim strCC As String = ""
                                                Dim p As Integer
                                                For p = 0 To dtEmail.Rows.Count - 1
                                                    strCC = strCC & dtEmail.Rows(p).Item("vcEmailID") & ","
                                                Next
                                                strCC = strCC.TrimEnd(",")
                                                dtMergeFields.Rows.Add(drNew)
                                                objCommon.byteMode = 1
                                                objCommon.ContactID = txtrecOwner.Text
                                                objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), strCC, Session("UserEmail"), objCommon.GetManagerEmail, dtMergeFields)
                                            End If
                                        End If
                                    End If
                                    ''Sending mail if the order is big

                                    Dim dtBigDealDetails As DataTable
                                    objAlerts.AlertDTLID = 9 'Alert DTL ID for sending alerts in opportunities
                                    objAlerts.DomainID = Session("DomainID")
                                    dtBigDealDetails = objAlerts.GetIndAlertDTL
                                    If dtBigDealDetails.Rows.Count > 0 Then
                                        If dtBigDealDetails.Rows(0).Item("tintAlertOn") = 1 Then
                                            If lblAmount.Text > dtBigDealDetails.Rows(0).Item("monAmount") Then
                                                Dim dtEmailTemplate As DataTable
                                                Dim objDocuments As New DocumentList
                                                objDocuments.GenDocID = dtBigDealDetails.Rows(0).Item("numEmailTemplate")
                                                objDocuments.DomainID = Session("DomainID")
                                                dtEmailTemplate = objDocuments.GetDocByGenDocID
                                                If dtEmailTemplate.Rows.Count > 0 Then
                                                    Dim objSendMail As New clsSendEmail
                                                    Dim dtMergeFields As New DataTable
                                                    Dim drNew As DataRow
                                                    dtMergeFields.Columns.Add("Employee")
                                                    dtMergeFields.Columns.Add("DealAmount")
                                                    dtMergeFields.Columns.Add("Organization")
                                                    drNew = dtMergeFields.NewRow
                                                    drNew("Employee") = lblRecordOwner.Text
                                                    drNew("DealAmount") = lblAmount.Text
                                                    drNew("Organization") = lblCustomerName.Text


                                                    Dim dtEmail As DataTable
                                                    objAlerts.AlertDTLID = 9
                                                    objAlerts.DomainID = Session("DomainId")
                                                    dtEmail = objAlerts.GetAlertEmails
                                                    Dim strCC As String = ""
                                                    Dim p As Integer
                                                    For p = 0 To dtEmail.Rows.Count - 1
                                                        strCC = strCC & dtEmail.Rows(p).Item("vcEmailID") & ";"
                                                    Next
                                                    strCC = strCC.TrimEnd(";")

                                                    dtMergeFields.Rows.Add(drNew)
                                                    objCommon.byteMode = 0
                                                    objCommon.ContactID = txtrecOwner.Text
                                                    objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), "", Session("UserEmail"), strCC, dtMergeFields)
                                                End If
                                            End If
                                        End If
                                    End If
                                Catch ex As Exception

                                End Try
                            Else
                                dtSalesProcess.Rows(i).Item("bitStageCompleted") = 0
                            End If
                            dtSalesProcess.Rows(i).Item("vcComments") = txtDealClosedComments.Text
                        ElseIf dtSalesProcess.Rows(i).Item("numStagePercentage") = 0 Then
                            Dim txtDealLostComm As TextBox
                            Dim chkDL As CheckBox
                            txtDealLostComm = uwOppTab.FindControl("txtDLComm")
                            chkDL = uwOppTab.FindControl("chkDlost")
                            If chkDL.Checked = True Then
                                dtSalesProcess.Rows(i).Item("bitStageCompleted") = True
                            Else
                                dtSalesProcess.Rows(i).Item("bitStageCompleted") = False
                            End If
                            dtSalesProcess.Rows(i).Item("vcComments") = txtDealLostComm.Text
                        End If
                    Next
                    txtProcessId.Text = "0"
                    Session("SalesProcessDetails") = dtSalesProcess
                End If
            End If
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub



    Private Overloads Sub imgCustGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles imgCustGo.Click
        Try
            FillCustomer(ddlcompany, CStr(txtComp.Text))
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Overloads Sub ImgbtnAddContact_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImgbtnAddContact.Click
        Try

        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            SaveOpportunity()
            SaveCusField()
            Session("SalesProcessDetails") = objOpportunity.BusinessProcessByOpID

            ' Response.Redirect("frmOpportunities.aspx?frm=" & GetQueryStringVal(Request.QueryString("enc"),"frm") & "&opId=" & arrOutPut(0) & "&SelectedIndex=" & uwOppTab.SelectedTabIndex)
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            SaveOpportunity()
            SaveCusField()
            sb_PageRedirect()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            sb_PageRedirect()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Sub sb_PageRedirect()
        If Session("EnableIntMedPage") = 1 Then
            If GetQueryStringVal(Request.QueryString("enc"), "frm") = "Oppdtl" Then
                Response.Redirect("../pagelayout/frmOppurtunitydtl.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
            Else
                Response.Redirect("../Opportunities/frmOpportunityList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
            End If
        Else
            Response.Redirect("../Opportunities/frmOpportunityList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
        End If
    End Sub

    Function ReturnMoney(ByVal Money)
        If Not IsDBNull(Money) Then
            Return String.Format("{0:#,###.00}", Money)
        End If
    End Function


    Private Sub btnReceivedOrShipped_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReceivedOrShipped.Click
        btnReceivedOrShipped.Visible = False
        btnSave.Enabled = False
        btnSaveClose.Enabled = False
        btnActdelete.Enabled = False
        objOpportunity.OpportunityId = lngOppId
        objOpportunity.ManageShippingReceived()
    End Sub

    Sub FillCustomer(ByVal ddlCombo As DropDownList, ByVal strName As String)
        Dim fillCombo As New COpportunities
        With fillCombo
            .DomainID = Session("DomainID")
            .UserCntID = Session("UserContactID")
            .CompFilter = Trim(strName) & "%"
            ddlCombo.DataSource = fillCombo.ListCustomer().Tables(0).DefaultView
            ddlCombo.DataTextField = "vcCompanyname"
            ddlCombo.DataValueField = "numDivisionID"
            ddlCombo.DataBind()
        End With
        ddlCombo.Items.Insert(0, New ListItem("--Select One--", "0"))
    End Sub

    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        FillCustomer(ddlShipCompany, txtCompName.Text.Trim)
    End Sub


    Sub DisplayDynamicFlds()
        'If Session("OppType") = 1 Then
        '    m_aryRightsForCustFlds = clsAuthorization.fn_ExternalPageLevelRights("frmOpportunityList.aspx", Session("UserContactID"), 10, 12)
        '    If m_aryRightsForCustFlds(RIGHTSTYPE.VIEW) = 0 Then
        '        Cache.Remove("CusFields")
        '        Exit Sub
        '    End If
        'Else
        '    m_aryRightsForCustFlds = clsAuthorization.fn_ExternalPageLevelRights("frmOpportunityList.aspx", Session("UserContactID"), 10, 13)
        '    If m_aryRightsForCustFlds(RIGHTSTYPE.VIEW) = 0 Then
        '        Cache.Remove("CusFields")
        '        Exit Sub
        '    End If
        'End If
        Dim strDate As String
        Dim bizCalendar As UserControl
        Dim _myUC_DueDate As PropertyInfo
        Dim PreviousRowID As Integer = 0
        Dim objRow As HtmlTableRow
        Dim objCell As HtmlTableCell
        Dim i, k As Integer
        Dim dtTable As New DataTable
        Dim ObjCus As New CustomFields
        If Session("OppType") = 1 Then
            ObjCus.locId = 2
        Else
            ObjCus.locId = 6
        End If
        ObjCus.RelId = 0
        ObjCus.RecordId = lngOppId
        ObjCus.DomainID = Session("DomainID")
        dtTable = ObjCus.GetCustFlds
        Session("CusFields") = dtTable
        If uwOppTab.Tabs.Count > 5 Then
            Dim iItemcount As Integer
            iItemcount = uwOppTab.Tabs.Count
            While uwOppTab.Tabs.Count > 5
                uwOppTab.Tabs.RemoveAt(iItemcount - 1)
                iItemcount = iItemcount - 1
            End While
        End If
        If dtTable.Rows.Count > 0 Then

            'Main Detail Section
            k = 0
            objRow = New HtmlTableRow
            For i = 0 To dtTable.Rows.Count - 1
                If dtTable.Rows(i).Item("TabId") = 0 Then
                    If k = 2 Then
                        k = 0
                        tblDetails.Rows.Add(objRow)

                        objRow = New HtmlTableRow
                    End If
                    objCell = New HtmlTableCell
                    objCell.Align = "Right"
                    objCell.Attributes.Add("class", "normal1")
                    If dtTable.Rows(i).Item("fld_type") <> "Link" Then
                        objCell.InnerText = dtTable.Rows(i).Item("fld_label")
                    End If
                    objRow.Cells.Add(objCell)
                    If dtTable.Rows(i).Item("fld_type") = "Text Box" Then
                        objCell = New HtmlTableCell
                        CreateTexBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))
                    ElseIf dtTable.Rows(i).Item("fld_type") = "Drop Down List Box" Then
                        objCell = New HtmlTableCell
                        CreateDropdown(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CInt(dtTable.Rows(i).Item("Value")), dtTable.Rows(i).Item("numlistid"))
                    ElseIf dtTable.Rows(i).Item("fld_type") = "Check box" Then
                        objCell = New HtmlTableCell
                        CreateChkBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CInt(dtTable.Rows(i).Item("Value")))
                    ElseIf dtTable.Rows(i).Item("fld_type") = "Text Area" Then
                        objCell = New HtmlTableCell
                        CreateTextArea(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))
                    ElseIf dtTable.Rows(i).Item("fld_type") = "Date Field" Then

                        objCell = New HtmlTableCell
                        bizCalendar = LoadControl("../common/calandar.ascx")
                        bizCalendar.ID = "cal" & dtTable.Rows(i).Item("fld_id")
                        objCell.Controls.Add(bizCalendar)
                        objRow.Cells.Add(objCell)

                    ElseIf dtTable.Rows(i).Item("fld_type") = "Link" Then
                        objCell = New HtmlTableCell
                        CreateLink(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("vcURL"), lngOppId, dtTable.Rows(i).Item("fld_label"))

                    End If
                    k = k + 1
                End If
            Next
            tblDetails.Rows.Add(objRow)
            'custom tabs
            Dim Tab As Tab
            Dim aspTable As HtmlTable
            Dim Table As Table
            Dim tblcell As TableCell
            Dim tblRow As TableRow
            k = 0
            ViewState("TabId") = dtTable.Rows(0).Item("TabId")
            ViewState("Check") = 0
            ViewState("FirstTabCreated") = 0
            ' Tabstrip3.Items.Clear()
            For i = 0 To dtTable.Rows.Count - 1
                If dtTable.Rows(i).Item("TabId") <> 0 Then
                    If ViewState("TabId") <> dtTable.Rows(i).Item("TabId") Or ViewState("FirstTabCreated") = 0 Then
                        If ViewState("Check") <> 0 Then
                            aspTable.Rows.Add(objRow)
                            tblcell.Controls.Add(aspTable)
                            tblRow.Cells.Add(tblcell)
                            Table.Rows.Add(tblRow)
                            Tab.ContentPane.Controls.Add(Table)
                        End If
                        k = 0
                        ViewState("FirstTabCreated") = 1
                        ViewState("Check") = 1
                        '   If Not IsPostBack Then
                        ViewState("TabId") = dtTable.Rows(i).Item("TabId")
                        Tab = New Tab
                        Tab.Text = "&nbsp;&nbsp;" & dtTable.Rows(i).Item("tabname") & "&nbsp;&nbsp;"
                        uwOppTab.Tabs.Add(Tab)
                        aspTable = New HtmlTable
                        Table = New Table
                        Table.Width = Unit.Percentage(100)
                        Table.BorderColor = System.Drawing.Color.FromName("black")
                        Table.GridLines = GridLines.None
                        Table.BorderWidth = Unit.Pixel(1)
                        Table.Height = Unit.Pixel(300)
                        Table.CssClass = "aspTable"
                        tblcell = New TableCell
                        tblRow = New TableRow
                        tblcell.VerticalAlign = VerticalAlign.Top
                        aspTable.Width = "100%"
                        objRow = New HtmlTableRow
                        objCell = New HtmlTableCell
                        objCell.InnerHtml = "<br>"
                        objRow.Cells.Add(objCell)
                        aspTable.Rows.Add(objRow)
                        objRow = New HtmlTableRow
                    End If

                    If k = 3 Then
                        k = 0
                        aspTable.Rows.Add(objRow)

                        objRow = New HtmlTableRow
                    End If
                    objCell = New HtmlTableCell
                    objCell.Align = "right"
                    objCell.Attributes.Add("class", "normal1")
                    If dtTable.Rows(i).Item("fld_type") <> "Link" Then
                        objCell.InnerText = dtTable.Rows(i).Item("fld_label")
                    End If
                    objRow.Cells.Add(objCell)
                    If dtTable.Rows(i).Item("fld_type") = "Text Box" Then
                        objCell = New HtmlTableCell
                        CreateTexBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))
                    ElseIf dtTable.Rows(i).Item("fld_type") = "Drop Down List Box" Then
                        objCell = New HtmlTableCell
                        CreateDropdown(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CInt(dtTable.Rows(i).Item("Value")), dtTable.Rows(i).Item("numlistid"))
                    ElseIf dtTable.Rows(i).Item("fld_type") = "Check box" Then
                        objCell = New HtmlTableCell
                        CreateChkBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CInt(dtTable.Rows(i).Item("Value")))
                    ElseIf dtTable.Rows(i).Item("fld_type") = "Text Area" Then
                        objCell = New HtmlTableCell
                        CreateTextArea(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))
                    ElseIf dtTable.Rows(i).Item("fld_type") = "Date Field" Then
                        PreviousRowID = i
                        objCell = New HtmlTableCell
                        bizCalendar = LoadControl("../include/calandar.ascx")
                        bizCalendar.ID = "cal" & dtTable.Rows(i).Item("fld_id")
                        objCell.Controls.Add(bizCalendar)
                        objRow.Cells.Add(objCell)

                    ElseIf dtTable.Rows(i).Item("fld_type") = "Link" Then
                        objCell = New HtmlTableCell
                        CreateLink(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("vcURL"), lngOppId, dtTable.Rows(i).Item("fld_label"))
                    ElseIf dtTable.Rows(i).Item("fld_type") = "Frame" Then
                        objCell = New HtmlTableCell
                        Dim strFrame As String
                        Dim URL As String
                        URL = dtTable.Rows(i).Item("vcURL")
                        URL = URL.Replace("RecordID", lngOppId)
                        strFrame = "<iframe src ='" & URL & "' width='100%' frameborder='0' height= '800px'></iframe>"
                        objCell.Controls.Add(New LiteralControl(strFrame))
                        objRow.Cells.Add(objCell)
                    End If
                    k = k + 1
                End If

            Next
            If ViewState("Check") = 1 Then
                aspTable.Rows.Add(objRow)
                tblcell.Controls.Add(aspTable)
                tblRow.Cells.Add(tblcell)
                Table.Rows.Add(tblRow)
                Tab.ContentPane.Controls.Add(Table)
            End If
        End If
        Dim dvCusFields As DataView
        dvCusFields = dtTable.DefaultView
        dvCusFields.RowFilter = "fld_type='Date Field'"
        Dim iViewCount As Integer
        For iViewCount = 0 To dvCusFields.Count - 1
            If Not IsDBNull(dvCusFields(iViewCount).Item("Value")) Then
                bizCalendar = uwOppTab.FindControl("cal" & dvCusFields(iViewCount).Item("fld_id"))
                Dim _myControlType As Type = bizCalendar.GetType()
                _myUC_DueDate = _myControlType.GetProperty("SelectedDate")
                strDate = dvCusFields(iViewCount).Item("Value")
                If strDate = "0" Then
                    strDate = ""
                End If
                If strDate <> "" Then
                    strDate = DateFromFormattedDate(strDate, Session("DateFormat"))
                    _myUC_DueDate.SetValue(bizCalendar, strDate, Nothing)
                End If
            End If

        Next
    End Sub

    Sub SaveCusField()
        If Not Session("CusFields") Is Nothing Then

            Dim dtTable As New DataTable
            Dim i As Integer
            dtTable = Session("CusFields")
            For i = 0 To dtTable.Rows.Count - 1
                If dtTable.Rows(i).Item("fld_type") = "Text Box" Then
                    Dim txt As TextBox
                    txt = uwOppTab.FindControl(dtTable.Rows(i).Item("fld_id"))
                    dtTable.Rows(i).Item("Value") = txt.Text
                ElseIf dtTable.Rows(i).Item("fld_type") = "Drop Down List Box" Then
                    Dim ddl As DropDownList
                    ddl = uwOppTab.FindControl(dtTable.Rows(i).Item("fld_id"))
                    dtTable.Rows(i).Item("Value") = CStr(ddl.SelectedItem.Value)
                ElseIf dtTable.Rows(i).Item("fld_type") = "Check box" Then
                    Dim chk As CheckBox
                    chk = uwOppTab.FindControl(dtTable.Rows(i).Item("fld_id"))
                    If chk.Checked = True Then
                        dtTable.Rows(i).Item("Value") = "1"
                    Else
                        dtTable.Rows(i).Item("Value") = "0"
                    End If
                ElseIf dtTable.Rows(i).Item("fld_type") = "Text Area" Then
                    Dim txt As TextBox
                    txt = uwOppTab.FindControl(dtTable.Rows(i).Item("fld_id"))
                    dtTable.Rows(i).Item("Value") = txt.Text
                ElseIf dtTable.Rows(i).Item("fld_type") = "Date Field" Then
                    Dim BizCalendar As UserControl
                    BizCalendar = uwOppTab.FindControl("cal" & dtTable.Rows(i).Item("fld_id"))

                    Dim strDueDate As String
                    Dim _myControlType As Type = BizCalendar.GetType()
                    Dim _myUC_DueDate As PropertyInfo = _myControlType.GetProperty("SelectedDate")
                    strDueDate = _myUC_DueDate.GetValue(BizCalendar, Nothing)
                    If strDueDate <> "" Then
                        dtTable.Rows(i).Item("Value") = FormattedDateFromDate(strDueDate, Session("DateFormat"))
                    Else
                        dtTable.Rows(i).Item("Value") = ""
                    End If
                End If
            Next

            Dim ds As New DataSet
            Dim strdetails As String
            dtTable.TableName = "Table"
            ds.Tables.Add(dtTable.Copy)
            strdetails = ds.GetXml
            ds.Tables.Remove(ds.Tables(0))

            Dim ObjCusfld As New CustomFields(Session("UserContactID"))
            ObjCusfld.strDetails = strdetails
            If Session("OppType") = 1 Then
                ObjCusfld.locId = 2
            Else
                ObjCusfld.locId = 6
            End If
            ObjCusfld.RecordId = lngOppId
            ObjCusfld.SaveCustomFldsByRecId()
        End If
    End Sub


    Private Sub btnActDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActDelete.Click
        Try
            Dim objOpport As New COpportunities
            With objOpport
                .OpportID = lngOppId
                .DomainID = Session("DomainID")
            End With
            objOpport.DelOpp()
            sb_PageRedirect()
        Catch ex As Exception
            litMessage.Text = "Dependent record exists. Cannot be Deleted."
        End Try
    End Sub

    Sub DisplayDynamicFldsItems()
        Dim objRow As HtmlTableRow
        Dim objCell As HtmlTableCell
        Dim i, k As Integer
        Dim dtTable As New DataTable
        Dim ObjCus As New CustomFields
        If Session("OppType") = 1 Then
            ObjCus.locId = 7
        Else
            ObjCus.locId = 8
        End If
        ObjCus.RelId = 0
        ObjCus.RecordId = lngOppId
        ObjCus.DomainID = Session("DomainID")
        dtTable = ObjCus.GetCustFlds
        Session("CusItemFields") = dtTable

        If dtTable.Rows.Count > 0 Then

            'Main Detail Section
            k = 0
            objRow = New HtmlTableRow
            For i = 0 To dtTable.Rows.Count - 1
                If dtTable.Rows(i).Item("TabId") = 0 Then
                    If k = 2 Then
                        k = 0
                        tblItems.Rows.Add(objRow)
                        objRow = New HtmlTableRow
                    End If
                    objCell = New HtmlTableCell
                    objCell.Align = "Right"
                    objCell.Attributes.Add("class", "normal1")
                    If dtTable.Rows(i).Item("fld_type") <> "Link" Then
                        objCell.InnerText = dtTable.Rows(i).Item("fld_label")
                    End If
                    objRow.Cells.Add(objCell)
                    If dtTable.Rows(i).Item("fld_type") = "Text Box" Then
                        objCell = New HtmlTableCell
                        CreateTexBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))
                    ElseIf dtTable.Rows(i).Item("fld_type") = "Drop Down List Box" Then
                        objCell = New HtmlTableCell
                        CreateDropdown(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CInt(dtTable.Rows(i).Item("Value")), dtTable.Rows(i).Item("numlistid"))
                    ElseIf dtTable.Rows(i).Item("fld_type") = "Check box" Then
                        objCell = New HtmlTableCell
                        CreateChkBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CInt(dtTable.Rows(i).Item("Value")))
                    ElseIf dtTable.Rows(i).Item("fld_type") = "Text Area" Then
                        objCell = New HtmlTableCell
                        CreateTextArea(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))
                    End If
                    k = k + 1
                End If
            Next
            tblItems.Rows.Add(objRow)
        End If
    End Sub
    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            dsTemp = Session("Data")
            Dim dtItem As DataTable
            Dim dtSerItem As DataTable
            dtItem = dsTemp.Tables(0)
            dtSerItem = dsTemp.Tables(1)
            Dim dr As DataRow
            Dim ugRow, ugRow1 As UltraGridRow
            If btnAdd.Text = "Add" Then
                If ddltype.SelectedValue = "P" Or ddltype.SelectedValue = "A" Then
                    If txtHidValue.Text = "False" Then
                        For Each ugRow In uwItemSel.Rows
                            dr = dtItem.NewRow
                            dr("numoppitemtCode") = CType(IIf(IsDBNull(dtItem.Compute("MAX(numoppitemtCode)", "")), 0, dtItem.Compute("MAX(numoppitemtCode)", "")), Integer) + 1
                            dr("numItemCode") = ddlItems.SelectedValue
                            dr("numUnitHour") = txtunits.Text
                            dr("monPrice") = txtprice.Text
                            dr("monTotAmount") = dr("numUnitHour") * dr("monPrice")
                            dr("vcItemDesc") = txtdesc.Text
                            dr("numWarehouseID") = ugRow.Cells.FromKey("numWareHouseID").Value
                            dr("vcItemName") = ddlItems.SelectedItem.Text
                            dr("Warehouse") = ugRow.Cells.FromKey("vcWarehouse").Value
                            dr("numWarehouseItmsID") = ugRow.Cells.FromKey("numWareHouseItemID").Value
                            dr("ItemType") = ddltype.SelectedItem.Text
                            dr("Attributes") = objCommon.GetAttributesForWarehouseItem(dr("numWarehouseItmsID"), 0)
                            dr("Op_Flag") = 1
                            ''dr("numRecurringId") = ddlMakeRecurring.SelectedValue
                            ''dr("vcRecurringDesc") = IIf(ddlMakeRecurring.SelectedValue = 0, "", ddlMakeRecurring.SelectedItem.Text)
                            If ddlMakeRecurring.SelectedValue <> "" Then
                                dr("vcRecurringDesc") = IIf(ddlMakeRecurring.SelectedValue = 0, "", ddlMakeRecurring.SelectedItem.Text)
                                dr("numRecurringId") = ddlMakeRecurring.SelectedValue
                            Else
                                dr("vcRecurringDesc") = ""
                                dr("numRecurringId") = 0
                            End If
                            dtItem.Rows.Add(dr)
                        Next
                    Else
                        Dim iCount As Integer = 0
                        For Each ugRow In uwItemSel.Rows
                            If ugRow.HasChildRows = True Then
                                Dim dr1 As DataRow
                                For Each ugRow1 In ugRow.Rows
                                    If ugRow1.Cells.FromKey("Select").Value = True Then
                                        iCount = iCount + 1
                                        If iCount = 1 Then
                                            dr = dtItem.NewRow
                                            dr("numoppitemtCode") = CType(IIf(IsDBNull(dtItem.Compute("MAX(numoppitemtCode)", "")), 0, dtItem.Compute("MAX(numoppitemtCode)", "")), Integer) + 1
                                            dr("numItemCode") = ddlItems.SelectedValue
                                            'dr("numUnitHour") = iCount
                                            dr("monPrice") = txtprice.Text
                                            'dr("monTotAmount") = dr("numUnitHour") * dr("monPrice")
                                            dr("vcItemDesc") = txtdesc.Text
                                            dr("numWarehouseID") = ugRow.Cells.FromKey("numWareHouseID").Value
                                            dr("Warehouse") = ugRow.Cells.FromKey("vcWarehouse").Value
                                            dr("vcItemName") = ddlItems.SelectedItem.Text
                                            dr("numWarehouseItmsID") = ugRow.Cells.FromKey("numWareHouseItemID").Value
                                            If ddlMakeRecurring.SelectedValue <> "" Then
                                                dr("vcRecurringDesc") = IIf(ddlMakeRecurring.SelectedValue = 0, "", ddlMakeRecurring.SelectedItem.Text)
                                                dr("numRecurringId") = ddlMakeRecurring.SelectedValue
                                            Else
                                                dr("vcRecurringDesc") = ""
                                                dr("numRecurringId") = 0
                                            End If
                                            dtItem.Rows.Add(dr)
                                        End If
                                        dr1 = dtSerItem.NewRow
                                        dr1("numWarehouseItmsDTLID") = ugRow1.Cells.FromKey("numWareHouseItmsDTLID").Value
                                        dr1("numoppitemtCode") = dr("numoppitemtCode")
                                        dr1("numWItmsID") = dr("numWarehouseItmsID")
                                        dr1("vcSerialNo") = ugRow1.Cells.FromKey("vcSerialNo").Value
                                        If uwItemSel.Bands(1).Columns.Count > 5 Then
                                            dr1("Attributes") = objCommon.GetAttributesForWarehouseItem(dr1("numWarehouseItmsDTLID"), 1)
                                        Else
                                            dr1("Attributes") = "-"
                                        End If

                                        dtSerItem.Rows.Add(dr1)
                                    End If
                                Next
                                If iCount > 0 Then
                                    dr("numUnitHour") = iCount
                                    dr("monPrice") = txtprice.Text
                                    dr("monTotAmount") = iCount * dr("monPrice")
                                    dr("ItemType") = ddltype.SelectedItem.Text
                                    dr("Op_Flag") = 1
                                    If ddlMakeRecurring.SelectedValue <> "" Then
                                        dr("vcRecurringDesc") = IIf(ddlMakeRecurring.SelectedValue = 0, "", ddlMakeRecurring.SelectedItem.Text)
                                        dr("numRecurringId") = ddlMakeRecurring.SelectedValue
                                    Else
                                        dr("vcRecurringDesc") = ""
                                        dr("numRecurringId") = 0
                                    End If
                                    dr.AcceptChanges()
                                    dtItem.AcceptChanges()
                                    iCount = 0
                                End If
                            End If

                        Next
                    End If
                Else
                    dr = dtItem.NewRow
                    dr("numoppitemtCode") = CType(IIf(IsDBNull(dtItem.Compute("MAX(numoppitemtCode)", "")), 0, dtItem.Compute("MAX(numoppitemtCode)", "")), Integer) + 1
                    dr("numItemCode") = ddlItems.SelectedValue
                    dr("numUnitHour") = txtunits.Text
                    dr("monPrice") = txtprice.Text
                    dr("monTotAmount") = dr("numUnitHour") * dr("monPrice")
                    dr("vcItemDesc") = txtdesc.Text
                    dr("numWarehouseID") = DBNull.Value
                    dr("numWarehouseItmsID") = DBNull.Value
                    dr("vcItemName") = ddlItems.SelectedItem.Text
                    dr("ItemType") = ddltype.SelectedItem.Text
                    dr("Op_Flag") = 1
                    If ddlMakeRecurring.SelectedValue <> "" Then
                        dr("vcRecurringDesc") = IIf(ddlMakeRecurring.SelectedValue = 0, "", ddlMakeRecurring.SelectedItem.Text)
                        dr("numRecurringId") = ddlMakeRecurring.SelectedValue
                    Else
                        dr("vcRecurringDesc") = ""
                        dr("numRecurringId") = 0
                    End If
                    dtItem.Rows.Add(dr)
                End If
                dsTemp.AcceptChanges()
            Else
                btnAdd.Text = "Add"
                dr = dtItem.Rows.Find(txtHidEditOppItem.Text)
                If ddltype.SelectedValue = "P" Or ddltype.SelectedValue = "A" Then
                    If txtHidValue.Text = "False" Then
                        For Each ugRow In uwItemSel.Rows
                            If ugRow.Hidden = False Then
                                If ugRow.Cells.FromKey("numWareHouseItemID").Value = dr("numWarehouseItmsID") Then
                                    dr("numUnitHour") = txtunits.Text
                                    dr("monPrice") = txtprice.Text
                                    dr("monTotAmount") = dr("numUnitHour") * dr("monPrice")
                                    If ddlMakeRecurring.SelectedValue <> "" Then
                                        dr("vcRecurringDesc") = IIf(ddlMakeRecurring.SelectedValue = 0, "", ddlMakeRecurring.SelectedItem.Text)
                                        dr("numRecurringId") = ddlMakeRecurring.SelectedValue
                                    Else
                                        dr("vcRecurringDesc") = ""
                                        dr("numRecurringId") = 0
                                    End If
                                    If dr("Op_Flag") <> 1 Then
                                        dr("Op_Flag") = 2
                                    End If
                                End If
                            End If
                        Next
                    Else
                        For Each ugRow In uwItemSel.Rows
                            If ugRow.Hidden = False Then
                                If ugRow.Cells.FromKey("numWareHouseItemID").Value = dr("numWarehouseItmsID") Then
                                    Dim iCount As Integer = 0
                                    Dim dr1 As DataRow
                                    For Each ugRow1 In ugRow.Rows
                                        For Each dr1 In dtSerItem.Rows
                                            If ugRow1.Cells.FromKey("numWareHouseItmsDTLID").Value = dr1("numWareHouseItmsDTLID") Then
                                                dr1.Delete()
                                            End If
                                        Next
                                        dtSerItem.AcceptChanges()
                                    Next
                                    For Each ugRow1 In ugRow.Rows
                                        If ugRow1.Cells.FromKey("Select").Value = True Then
                                            dr1 = dtSerItem.NewRow
                                            dr1("numWarehouseItmsDTLID") = ugRow1.Cells.FromKey("numWareHouseItmsDTLID").Value
                                            dr1("numoppitemtCode") = dr("numoppitemtCode")
                                            dr1("numWItmsID") = dr("numWarehouseItmsID")
                                            dr1("vcSerialNo") = ugRow1.Cells.FromKey("vcSerialNo").Value
                                            If uwItemSel.Bands(1).Columns.Count > 5 Then
                                                dr1("Attributes") = objCommon.GetAttributesForWarehouseItem(dr1("numWarehouseItmsDTLID"), 1)
                                            Else
                                                dr1("Attributes") = "-"
                                            End If
                                            dtSerItem.Rows.Add(dr1)
                                            iCount = iCount + 1
                                        End If
                                    Next
                                    dr("numUnitHour") = iCount
                                    dr("monPrice") = txtprice.Text
                                    dr("monTotAmount") = dr("numUnitHour") * dr("monPrice")
                                    If ddlMakeRecurring.SelectedValue <> "" Then
                                        dr("vcRecurringDesc") = IIf(ddlMakeRecurring.SelectedValue = 0, "", ddlMakeRecurring.SelectedItem.Text)
                                        dr("numRecurringId") = ddlMakeRecurring.SelectedValue
                                    Else
                                        dr("vcRecurringDesc") = ""
                                        dr("numRecurringId") = 0
                                    End If
                                    If dr("Op_Flag") <> 1 Then
                                        dr("Op_Flag") = 2
                                    End If
                                    If iCount = 0 Then
                                        dtItem.Rows.Remove(dr)
                                    End If
                                End If
                            End If
                        Next
                    End If
                Else
                    If dr("numItemCode") = ddlItems.SelectedValue Then
                        dr("numUnitHour") = txtunits.Text
                        dr("monPrice") = txtprice.Text
                        dr("monTotAmount") = dr("numUnitHour") * dr("monPrice")
                        If ddlMakeRecurring.SelectedValue <> "" Then
                            dr("vcRecurringDesc") = IIf(ddlMakeRecurring.SelectedValue = 0, "", ddlMakeRecurring.SelectedItem.Text)
                            dr("numRecurringId") = ddlMakeRecurring.SelectedValue
                        Else
                            dr("vcRecurringDesc") = ""
                            dr("numRecurringId") = 0
                        End If
                        If dr("Op_Flag") <> 1 Then
                            dr("Op_Flag") = 2
                        End If
                    End If
                End If
                dr.AcceptChanges()
                dsTemp.AcceptChanges()
            End If
            If dtSerItem.ParentRelations.Count = 0 Then
                dsTemp.Relations.Add("Item", dsTemp.Tables(0).Columns("numoppitemtCode"), dsTemp.Tables(1).Columns("numoppitemtCode"))
            End If
            Session("Data") = dsTemp
            tblProducts.Visible = True
            ucItem.DataSource = dsTemp
            ucItem.DataBind()
            uwItemSel.Rows.Clear()
            tblItemWareHouse.Visible = False
            tblOptSelection.Visible = False
            txtunits.Text = ""
            txtprice.Text = ""
            ddlItems.SelectedIndex = 0
            If ddlMakeRecurring.SelectedValue <> "" Then ddlMakeRecurring.SelectedIndex = 0
            txtdesc.Text = ""
            btnEditCancel.Visible = False
            phItems.Controls.Clear()
            ddlWarehouse.Items.Clear()
            ddltype.SelectedIndex = 0
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadMakeRecurring()
        Dim lobjChecks As New Checks
        lobjChecks.DomainId = Session("DomainID")
        ddlMakeRecurring.DataSource = lobjChecks.GetRecurringDetails()
        ddlMakeRecurring.DataTextField = "varRecurringTemplateName"
        ddlMakeRecurring.DataValueField = "numRecurringId"
        ddlMakeRecurring.DataBind()
        ddlMakeRecurring.Items.Insert(0, New ListItem("--Select One --", "0"))
    End Sub


    Private Sub ucItem_ClickCellButton(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.CellEventArgs) Handles ucItem.ClickCellButton
        Try
            '  If m_aryRightsForItems(RIGHTSTYPE.UPDATE) <> 0 Then


            ddlItems.SelectedItem.Selected = False
            If Not ddlItems.Items.FindByValue(e.Cell.Tag.Split("~")(0)) Is Nothing Then
                ddlItems.Items.FindByValue(e.Cell.Tag.Split("~")(0)).Selected = True
            End If
            LoadItemDetails()
            trWareHouse.Visible = False
            dsTemp = Session("Data")
            Dim dtItem As DataTable
            Dim dtSerItem As DataTable
            dtItem = dsTemp.Tables(0)
            dtSerItem = dsTemp.Tables(1)
            Dim dr As DataRow
            dr = dtItem.Rows.Find(e.Cell.Tag.Split("~")(2))
            txtHidEditOppItem.Text = e.Cell.Tag.Split("~")(2)
            Dim i, k As Integer
            If ddltype.SelectedValue = "P" Or ddltype.SelectedValue = "A" Then
                LoadDetailsOnWarehouseSel(e.Cell.Tag.Split("~")(1))
                Dim ugRow, ugRow1 As UltraGridRow
                If IIf(txtSerialize.Text = "", 0, txtSerialize.Text) = 0 Then
                    ddlMakeRecurring.Visible = True
                    lblMakeRecurring.Visible = True
                    LoadMakeRecurring()
                    If Not IsDBNull(dr("numRecurringId")) Then
                        If Not ddlMakeRecurring.Items.FindByValue(dr("numRecurringId")) Is Nothing Then
                            ddlMakeRecurring.Items.FindByValue(dr("numRecurringId")).Selected = True
                        End If
                    End If
                Else
                    ddlMakeRecurring.Visible = False
                    lblMakeRecurring.Visible = False
                End If

                If txtHidValue.Text = "False" Then
                    txtprice.Text = String.Format("{0:###,##0.00}", CDec(dr("monPrice")))
                    For Each ugRow In uwItemSel.Rows
                        If ugRow.Cells.FromKey("numWareHouseItemID").Value = e.Cell.Tag.Split("~")(1) Then
                            txtunits.Text = dr("numUnitHour")
                        Else
                            ugRow.Hidden = True
                        End If
                    Next
                Else
                    txtprice.Text = String.Format("{0:###,##0.00}", CDec(dr("monPrice")))
                    For Each ugRow In uwItemSel.Rows
                        If ugRow.Cells.FromKey("numWareHouseItemID").Value = e.Cell.Tag.Split("~")(1) Then
                            If Session("OppType") = 1 Then
                                If ugRow.HasChildRows = True Then
                                    ugRow.Expanded = True
                                End If
                                For Each ugRow1 In ugRow.Rows
                                    Dim dv1 As DataView
                                    dv1 = New DataView(dtSerItem)
                                    dv1.RowFilter = "numoppitemtCode=" & dr("numoppitemtCode")
                                    For k = 0 To dv1.Count - 1
                                        If ugRow1.Cells.FromKey("numWareHouseItmsDTLID").Value = dv1(k).Item("numWarehouseItmsDTLID") Then
                                            ugRow1.Cells.FromKey("Select").Value = True
                                        End If
                                    Next
                                Next
                            Else
                                txtunits.Text = dr("numUnitHour")
                            End If

                        Else
                            ugRow.Hidden = True
                        End If
                    Next
                End If
                tblItemWareHouse.Visible = True
            Else
                ddlMakeRecurring.Visible = True
                lblMakeRecurring.Visible = True
                LoadMakeRecurring()
                If Not IsDBNull(dr("numRecurringId")) Then
                    If Not ddlMakeRecurring.Items.FindByValue(dr("numRecurringId")) Is Nothing Then
                        ddlMakeRecurring.Items.FindByValue(dr("numRecurringId")).Selected = True
                    End If
                End If
                txtunits.Text = String.Format("{0:###,##0.00}", CDec(dr("numUnitHour")))
                txtprice.Text = String.Format("{0:###,##0.00}", CDec(dr("monPrice")))
                If ddlMakeRecurring.Items.FindByValue(dr("numRecurringId")) Is Nothing Then
                    ddlMakeRecurring.Items.FindByValue(dr("numRecurringId")).Selected = True
                End If
            End If
            btnAdd.Text = "Update"
            tblOptSelection.Visible = True
            btnEditCancel.Visible = True
            '  End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ucItem_DeleteRowBatch(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles ucItem.DeleteRowBatch
        Try
            dsTemp = Session("Data")
            Dim table As DataTable = dsTemp.Tables(e.Row.Band.BaseTableName)
            If table.ParentRelations.Count > 0 Then
                Dim row As DataRow = table.Rows.Find(e.Row.Cells.FromKey("numWarehouseItmsDTLID").Value)
                table.Rows.Remove(row)
            Else
                Dim row As DataRow = table.Rows.Find(e.Row.Cells.FromKey("numoppitemtCode").Value)
                table.Rows.Remove(row)
            End If
            dsTemp.AcceptChanges()
            Session("Data") = dsTemp
        Catch ex As Exception
            Throw ex
        End Try
    End Sub



    Private Sub ucItem_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles ucItem.InitializeRow
        Try
            If e.Row.HasParent = False Then
                e.Row.Cells.FromKey("Action").Tag = e.Row.Cells.FromKey("numItemCode").Value & "~" & e.Row.Cells.FromKey("numWareHouseItemID").Value & "~" & e.Row.Cells.FromKey("numoppitemtCode").Value
                e.Row.Cells.FromKey("Action").Value = "Edit"
                e.Row.Cells.FromKey("Action").Column.CellButtonStyle.CssClass = "button"
                e.Row.Cells.FromKey("Action").Column.Width = Unit.Pixel(50)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub uwItemSel_InitializeLayout(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.LayoutEventArgs) Handles uwItemSel.InitializeLayout
        uwItemSel.Bands(1).Columns(1).Hidden = True
        uwItemSel.Bands(1).Columns(2).Hidden = True
        uwItemSel.Bands(1).Columns(3).HeaderText = "Serial No"
        uwItemSel.Bands(1).Columns(4).Hidden = True
        Try
            Dim dr As DataRow
            Dim dtAttributes As DataTable
            dtAttributes = ds.Tables(2)
            For Each dr In dtAttributes.Rows
                If dr("fld_type") = "Drop Down List Box" Then
                    uwItemSel.Bands(1).Columns.FromKey(dr("Fld_label")).Type = ColumnType.DropDownList
                    Dim attVlist As New Infragistics.WebUI.UltraWebGrid.ValueList
                    Dim dtValues As DataTable
                    dtValues = objCommon.GetMasterListItems(dr("numlistid"), Session("DomainID"))
                    attVlist.ValueListItems.Add("0", "-")
                    Dim drValueList As DataRow
                    For Each drValueList In dtValues.Rows
                        attVlist.ValueListItems.Add(CStr(drValueList("numListItemID")), drValueList("vcData"))
                    Next
                    uwItemSel.Bands(1).Columns.FromKey(dr("Fld_label")).ValueList = attVlist
                ElseIf dr("fld_type") = "Check box" Then
                    uwItemSel.Bands(1).Columns.FromKey(dr("Fld_label")).Type = ColumnType.CheckBox
                End If
            Next
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnEditCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditCancel.Click
        tblProducts.Visible = True
        tblOptSelection.Visible = False
        tblItemWareHouse.Visible = False
        txtunits.Text = ""
        txtprice.Text = ""
        ddlItems.SelectedIndex = 0
        txtdesc.Text = ""
        btnEditCancel.Visible = False
        btnAdd.Text = "Add"
    End Sub


    Private Sub ddlWarehouse_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlWarehouse.SelectedIndexChanged
        LoadDetailsOnWarehouseSel(ddlWarehouse.SelectedValue)
        tblItemWareHouse.Visible = True
    End Sub

    Sub LoadDetailsOnWarehouseSel(ByVal lngWareHouseItemID As Long)
        strValues = ""
        Dim i As Integer
        Dim ddl As DropDownList
        Dim senderID, controlID As String
        If Not dtOppAtributes Is Nothing Then
            For i = 0 To dtOppAtributes.Rows.Count - 1
                controlID = "Attr" & dtOppAtributes.Rows(i).Item("fld_id")
                ddl = phItems.FindControl(controlID)
                If ddl.SelectedIndex > 0 Then
                    strValues = strValues & ddl.SelectedValue & ","
                End If
            Next
        End If

        Dim objItems As New CItems
        objItems.ItemCode = ddlItems.SelectedValue
        objItems.WareHouseItemID = lngWareHouseItemID
        objItems.str = strValues.TrimEnd(",")
        ds = objItems.GetItemsOnAttributesAndWarehouse()
        If Session("OppType") = 2 Then
            ds.Tables(1).Rows.Clear()
            ds.AcceptChanges()
        End If
        If ds.Relations.Count < 1 Then
            ds.Tables(0).TableName = "WareHouse"
            ds.Tables(1).TableName = "SerializedItems"
            ds.Tables(0).PrimaryKey = New DataColumn() {ds.Tables(0).Columns("numWareHouseItemID")}
            ds.Tables(1).PrimaryKey = New DataColumn() {ds.Tables(1).Columns("numWareHouseItmsDTLID")}
            ds.Relations.Add("Customers", ds.Tables(0).Columns("numWareHouseItemID"), ds.Tables(1).Columns("numWareHouseItemID"))
        End If
        If txtHidValue.Text = "True" Then
            Dim dr As DataRow
            Dim dtAttributes As DataTable
            dtAttributes = ds.Tables(2)
            If uwItemSel.Bands(1).Columns.Count = 5 Then
                Dim ucColumn As UltraGridColumn
                For Each dr In dtAttributes.Rows
                    ucColumn = New UltraGridColumn
                    ucColumn.Key = dr("Fld_label")
                    ucColumn.BaseColumnName = dr("Fld_label")
                    ucColumn.HeaderText = dr("Fld_label")
                    uwItemSel.Bands(1).Columns.Add(ucColumn)
                Next
            End If
        End If
        tblItemWareHouse.Visible = False
        uwItemSel.DataSource = ds
        uwItemSel.DataBind()
    End Sub

    Private Sub ddlOptItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOptItem.SelectedIndexChanged
        btnOptAdd.Text = "Add"
        LoadOptItemDetails()
        If ddlOptItem.SelectedIndex > 0 And txtHidOptValue.Text <> "" Then
            phOptItem.Controls.Clear()
            CreateOptAttributes()
        Else
            btnOptAdd.Attributes.Add("onclick", "return AddOption('')")
        End If
        dsTemp = Session("Data")
        Dim dtTable As DataTable
        dtTable = dsTemp.Tables(0)
        Dim dr As DataRow
        txtAddedItems.Text = ""
        For Each dr In dtTable.Rows
            txtAddedItems.Text = txtAddedItems.Text & dr("numWarehouseItmsID") & ","
        Next
        txtAddedItems.Text = txtAddedItems.Text.Trim(",")

        Dim lobjChecks As New Checks
        lobjChecks.DomainId = Session("DomainID")
        ddlOptMakeRecurring.DataSource = lobjChecks.GetRecurringDetails()
        ddlOptMakeRecurring.DataTextField = "varRecurringTemplateName"
        ddlOptMakeRecurring.DataValueField = "numRecurringId"
        ddlOptMakeRecurring.DataBind()
        ddlOptMakeRecurring.Items.Insert(0, New ListItem("--Select One --", "0"))

    End Sub


    Sub LoadOptItemDetails()
        Try
            If ddlOptItem.SelectedIndex > 0 Then
                Dim uwCoumn As UltraGridColumn
                Dim dtTable As DataTable
                Dim objItems As New CItems
                objItems.ItemCode = ddlOptItem.SelectedItem.Value
                dtTable = objItems.ItemDetails
                If dtTable.Rows.Count = 1 Then
                    txtOptDesc.Text = dtTable.Rows(0).Item("txtItemDesc")
                    If Not IsDBNull(dtTable.Rows(0).Item("vcPathForImage")) Then
                        If dtTable.Rows(0).Item("vcPathForImage") <> "" Then
                            imgOptItem.Visible = True
                            hplOptImage.Visible = True
                            imgOptItem.ImageUrl = Session("SiteType") & "//" & Request.ServerVariables("SERVER_NAME") & "/" & ConfigurationManager.AppSettings("PortalVirtualDirectoryName") & "/Documents/Docs/" & dtTable.Rows(0).Item("vcPathForImage")
                            hplOptImage.Attributes.Add("onclick", "return OpenImage(" & ddlItems.SelectedItem.Value & ")")
                        Else
                            imgOptItem.Visible = False
                            hplOptImage.Visible = False
                        End If
                    Else
                        imgOptItem.Visible = False
                        hplOptImage.Visible = False
                    End If
                    If dtTable.Rows(0).Item("charItemType") = "P" Or dtTable.Rows(0).Item("charItemType") = "A" Then
                        trOptWareHouse.Visible = True
                        txtOptUnits.Visible = True
                        hplOptPrice.Visible = True
                        txtHidOptValue.Text = "False"
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub CreateOptAttributes()
        Dim strAttributes As String = ""
        strValues = ""
        Dim boolLoadNextdropdown As Boolean
        Dim objItems As New CItems
        objItems.ItemCode = ddlOptItem.SelectedValue
        dtOppOptAttributes = objItems.GetOppItemAttributes()
        If dtOppOptAttributes.Rows.Count > 0 Then
            Dim i As Integer
            Dim lbl As Label
            Dim table As New Table
            Dim tblrow As TableRow
            Dim tblcell As TableCell
            Dim ddl As DropDownList
            tblrow = New TableRow
            For i = 0 To dtOppAtributes.Rows.Count - 1
                tblcell = New TableCell
                tblcell.CssClass = "normal1"
                tblcell.HorizontalAlign = HorizontalAlign.Right
                lbl = New Label
                lbl.Text = dtOppOptAttributes.Rows(i).Item("Fld_label") & IIf(txtHidOptValue.Text = "True", "", "<font color='red'>*</font>")
                tblcell.Controls.Add(lbl)
                tblrow.Cells.Add(tblcell)

                If dtOppOptAttributes.Rows(i).Item("fld_type") = "Drop Down List Box" Then
                    tblcell = New TableCell
                    ddl = New DropDownList
                    ddl.CssClass = "signup"
                    ddl.ID = "AttrOpp" & dtOppOptAttributes.Rows(i).Item("fld_id")

                    tblcell.Controls.Add(ddl)
                    ddl.AutoPostBack = True
                    AddHandler ddl.SelectedIndexChanged, AddressOf Me.LoadOptItemsonChangeofAttr
                    tblrow.Cells.Add(tblcell)
                    strAttributes = strAttributes & ddl.ID & "~" & dtOppOptAttributes.Rows(i).Item("Fld_label") & ","
                    If ddl.SelectedIndex > 0 Then
                        strValues = strValues & ddl.SelectedValue & ","
                    End If

                    If i = 0 Then
                        objCommon.sb_FillAttibuesFromDB(ddl, dtOppOptAttributes.Rows(i).Item("numlistid"), ddlOptItem.SelectedValue, "")
                    Else
                        objCommon.sb_FillAttibuesFromDB(ddl, IIf(boolLoadNextdropdown = True, dtOppOptAttributes.Rows(i).Item("numlistid"), 0), ddlOptItem.SelectedValue, strValues.TrimEnd(","))
                    End If
                    If ddl.SelectedIndex > 0 Then
                        boolLoadNextdropdown = True
                    End If
                End If

            Next
            strAttributes = strAttributes.TrimEnd(",")
            btnOptAdd.Attributes.Add("onclick", "return AddOption('" & strAttributes & "')")
            table.Rows.Add(tblrow)
            phOptItem.Controls.Add(tblrow)
        Else
            If ddlOptWarehouse.Items.Count = 0 Then
                ddlOptWarehouse.DataSource = objCommon.GetWarehouseOnAttrSel(ddlOptItem.SelectedValue, "")
                ddlOptWarehouse.DataTextField = "vcWareHouse"
                ddlOptWarehouse.DataValueField = "numWareHouseItemId"
                ddlOptWarehouse.DataBind()
                ddlOptWarehouse.Items.Insert(0, "--Select One--")
                ddlOptWarehouse.Items.FindByText("--Select One--").Value = "0"
            End If
            btnOptAdd.Attributes.Add("onclick", "return AddOption('')")
        End If
    End Sub


    Sub LoadOptItemsonChangeofAttr(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            strValues = ""
            Dim i As Integer
            Dim ddl As DropDownList
            Dim boolEnable As Boolean = True
            Dim boolLoadnextDropdown As Boolean = False
            Dim senderID, controlID As String
            senderID = sender.ID
            For i = 0 To dtOppOptAttributes.Rows.Count - 1
                controlID = "AttrOpp" & dtOppOptAttributes.Rows(i).Item("fld_id")
                ddl = phOptItem.FindControl(controlID)
                If ddl.SelectedIndex > 0 And boolLoadnextDropdown = False Then
                    strValues = strValues & ddl.SelectedValue & ","
                End If
                If boolLoadnextDropdown = True Then
                    objCommon.sb_FillAttibuesFromDB(ddl, dtOppOptAttributes.Rows(i).Item("numlistid"), ddlOptItem.SelectedValue, strValues.TrimEnd(","))
                    boolLoadnextDropdown = False
                End If
                If senderID = controlID Then
                    boolLoadnextDropdown = True
                    If (i = dtOppOptAttributes.Rows.Count - 1 And ddl.SelectedIndex > 0) Or txtHidOptValue.Text = "True" Then
                        Dim lngWareHouseTempID As Long
                        If ddlOptWarehouse.SelectedIndex > 0 Then
                            lngWareHouseTempID = ddlOptWarehouse.SelectedValue
                        End If
                        ddlOptWarehouse.DataSource = objCommon.GetWarehouseOnAttrSel(ddlOptItem.SelectedValue, strValues.TrimEnd(","))
                        ddlOptWarehouse.DataTextField = "vcWareHouse"
                        ddlOptWarehouse.DataValueField = "numWareHouseItemId"
                        ddlOptWarehouse.DataBind()
                        ddlOptWarehouse.Items.Insert(0, "--Select One--")
                        ddlOptWarehouse.Items.FindByText("--Select One--").Value = "0"
                        If lngWareHouseTempID > 0 Then
                            If Not ddlOptWarehouse.Items.FindByValue(lngWareHouseTempID) Is Nothing Then
                                ddlOptWarehouse.Items.FindByValue(lngWareHouseTempID).Selected = True
                                LoadOptDetailsOnWarehouseSel()
                            End If
                        End If
                    Else
                        ddlOptWarehouse.Items.Clear()
                    End If

                    If ddlOptWarehouse.Items.Count = 2 Then
                        'ddlWarehouse.SelectedIndex = 1
                        'ddlWarehouse_SelectedIndexChanged(ddlWarehouse, "")
                    End If
                    'wcWarehouse.DataSource = objCommon.GetWarehouseOnAttrSel(ddlItems.SelectedValue, strValues.TrimEnd(","))
                    'wcWarehouse.DataBind()
                End If
            Next
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadOptDetailsOnWarehouseSel()
        strValues = ""
        Dim i As Integer
        Dim ddl As DropDownList
        Dim senderID, controlID As String
        If Not dtOppOptAttributes Is Nothing Then
            For i = 0 To dtOppOptAttributes.Rows.Count - 1
                controlID = "AttrOpp" & dtOppOptAttributes.Rows(i).Item("fld_id")
                ddl = phOptItem.FindControl(controlID)
                If ddl.SelectedIndex > 0 Then
                    strValues = strValues & ddl.SelectedValue & ","
                End If
            Next
        End If
        Dim objItems As New CItems
        objItems.ItemCode = ddlOptItem.SelectedValue
        objItems.WareHouseItemID = ddlOptWarehouse.SelectedValue
        objItems.str = strValues.TrimEnd(",")
        ds = objItems.GetItemsOnAttributesAndWarehouse()

        If ds.Relations.Count < 1 Then
            ds.Tables(0).TableName = "WareHouse"
            ds.Tables(0).PrimaryKey = New DataColumn() {ds.Tables(0).Columns("numWareHouseItemID")}
        End If
        uwOptionItem.DataSource = ds
        uwOptionItem.DataBind()
    End Sub

    Private Sub ddlOptWarehouse_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOptWarehouse.SelectedIndexChanged
        LoadOptDetailsOnWarehouseSel()
        uwOptionItem.Visible = True
    End Sub

    Private Sub btnOptAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOptAdd.Click
        dsTemp = Session("Data")
        Dim dtItem As DataTable
        Dim dtSerItem As DataTable
        dtItem = dsTemp.Tables(0)
        dtSerItem = dsTemp.Tables(1)
        Dim dr As DataRow
        Dim ugRow, ugRow1 As UltraGridRow
        For Each ugRow In uwOptionItem.Rows
            dr = dtItem.NewRow
            dr("numoppitemtCode") = CType(IIf(IsDBNull(dtItem.Compute("MAX(numoppitemtCode)", "")), 0, dtItem.Compute("MAX(numoppitemtCode)", "")), Integer) + 1
            dr("numItemCode") = ddlOptItem.SelectedValue
            dr("numUnitHour") = txtOptUnits.Text
            dr("monPrice") = txtOptPrice.Text
            dr("monTotAmount") = dr("numUnitHour") * dr("monPrice")
            dr("vcItemDesc") = txtOptDesc.Text
            dr("numWarehouseID") = ugRow.Cells.FromKey("numWareHouseID").Value
            dr("vcItemName") = ddlOptItem.SelectedItem.Text
            dr("Warehouse") = ugRow.Cells.FromKey("vcWarehouse").Value
            dr("numWarehouseItmsID") = ugRow.Cells.FromKey("numWareHouseItemID").Value
            dr("ItemType") = "Options & Accessories "
            dr("Attributes") = objCommon.GetAttributesForWarehouseItem(dr("numWarehouseItmsID"), 0)
            dr("numRecurringId") = ddlOptMakeRecurring.SelectedValue
            dr("vcRecurringDesc") = IIf(ddlOptMakeRecurring.SelectedValue = 0, "", ddlOptMakeRecurring.SelectedItem.Text)

            dtItem.Rows.Add(dr)

        Next
        dsTemp.AcceptChanges()
        ucItem.DataSource = dsTemp
        ucItem.DataBind()
        uwOptionItem.Rows.Clear()
        uwOptionItem.Visible = False
        ddlOptItem.SelectedIndex = 0
        phOptItem.Controls.Clear()
        ddlOptWarehouse.Items.Clear()
        txtOptDesc.Text = ""
        txtOptUnits.Text = ""
        txtOptPrice.Text = ""
        tblProducts.Visible = True
    End Sub


    Sub LoadItemsonChangeofAttr(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            strValues = ""
            btnAdd.Text = "Add"
            Dim i As Integer
            Dim ddl As DropDownList
            Dim boolEnable As Boolean = True
            Dim boolLoadnextDropdown As Boolean = False
            Dim senderID, controlID As String
            senderID = sender.ID
            For i = 0 To dtOppAtributes.Rows.Count - 1
                controlID = "Attr" & dtOppAtributes.Rows(i).Item("fld_id")
                ddl = phItems.FindControl(controlID)
                If ddl.SelectedIndex > 0 And boolLoadnextDropdown = False Then
                    strValues = strValues & ddl.SelectedValue & ","
                End If
                If boolLoadnextDropdown = True Then
                    objCommon.sb_FillAttibuesFromDB(ddl, dtOppAtributes.Rows(i).Item("numlistid"), ddlItems.SelectedValue, strValues.TrimEnd(","))
                    boolLoadnextDropdown = False
                End If
                If senderID = controlID Then
                    boolLoadnextDropdown = True
                    If (i = dtOppAtributes.Rows.Count - 1 And ddl.SelectedIndex > 0) Or txtHidValue.Text = "True" Then
                        Dim lngWareHouseTempID As Long
                        If ddlWarehouse.SelectedIndex > 0 Then
                            lngWareHouseTempID = ddlWarehouse.SelectedValue
                        End If
                        ddlWarehouse.DataSource = objCommon.GetWarehouseOnAttrSel(ddlItems.SelectedValue, strValues.TrimEnd(","))
                        ddlWarehouse.DataTextField = "vcWareHouse"
                        ddlWarehouse.DataValueField = "numWareHouseItemId"
                        ddlWarehouse.DataBind()
                        ddlWarehouse.Items.Insert(0, "--Select One--")
                        ddlWarehouse.Items.FindByText("--Select One--").Value = "0"
                        If lngWareHouseTempID > 0 Then
                            If Not ddlWarehouse.Items.FindByValue(lngWareHouseTempID) Is Nothing Then
                                ddlWarehouse.Items.FindByValue(lngWareHouseTempID).Selected = True
                                LoadDetailsOnWarehouseSel(ddlWarehouse.SelectedValue)
                            End If
                        End If
                    Else
                        ddlWarehouse.Items.Clear()
                    End If

                    If ddlWarehouse.Items.Count = 2 Then
                        'ddlWarehouse.SelectedIndex = 1
                        'ddlWarehouse_SelectedIndexChanged(ddlWarehouse, "")
                    End If
                    'wcWarehouse.DataSource = objCommon.GetWarehouseOnAttrSel(ddlItems.SelectedValue, strValues.TrimEnd(","))
                    'wcWarehouse.DataBind()
                End If
            Next
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub



    Private Sub ddlItems_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlItems.SelectedIndexChanged
        Try
            btnAdd.Text = "Add"
            LoadItemDetails()
            uwItemSel.Rows.Clear()
            If ddlItems.SelectedIndex > 0 And txtHidValue.Text <> "" Then
                phItems.Controls.Clear()
                If Session("OppType") = 2 And txtSerialize.Text = 1 Then
                    If ddlWarehouse.Items.Count = 0 Then
                        ddlWarehouse.DataSource = objCommon.GetWarehouseOnAttrSel(ddlItems.SelectedValue, "")
                        ddlWarehouse.DataTextField = "vcWareHouse"
                        ddlWarehouse.DataValueField = "numWareHouseItemId"
                        ddlWarehouse.DataBind()
                        ddlWarehouse.Items.Insert(0, "--Select One--")
                        ddlWarehouse.Items.FindByText("--Select One--").Value = "0"
                    End If
                Else
                    CreateAttributes()
                End If
            Else
                btnAdd.Attributes.Add("onclick", "return Add('')")
            End If
            Dim dtTable As DataTable
            dsTemp = Session("Data")
            dtTable = dsTemp.Tables(0)
            Dim dr As DataRow
            txtAddedItems.Text = ""
            For Each dr In dtTable.Rows
                txtAddedItems.Text = txtAddedItems.Text & dr("numWarehouseItmsID") & ","
            Next
            txtAddedItems.Text = txtAddedItems.Text.Trim(",")
            If IIf(txtSerialize.Text = "", 0, txtSerialize.Text) = 0 Then
                ddlMakeRecurring.Visible = True
                lblMakeRecurring.Visible = True

                Dim lobjChecks As New Checks
                lobjChecks.DomainId = Session("DomainID")
                ddlMakeRecurring.DataSource = lobjChecks.GetRecurringDetails()
                ddlMakeRecurring.DataTextField = "varRecurringTemplateName"
                ddlMakeRecurring.DataValueField = "numRecurringId"
                ddlMakeRecurring.DataBind()
                ddlMakeRecurring.Items.Insert(0, New ListItem("--Select One --", "0"))
            Else
                ddlMakeRecurring.Visible = False
                lblMakeRecurring.Visible = False
            End If

        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadItemDetails()
        Try
            If ddlItems.SelectedIndex > 0 Then
                Dim uwCoumn As UltraGridColumn
                Dim dtTable As DataTable
                Dim objItems As New CItems
                objItems.ItemCode = ddlItems.SelectedItem.Value
                dtTable = objItems.ItemDetails
                If dtTable.Rows.Count = 1 Then
                    ddltype.SelectedItem.Selected = False
                    ddltype.Items.FindByValue(dtTable.Rows(0).Item("charItemType")).Selected = True
                    txtdesc.Text = dtTable.Rows(0).Item("txtItemDesc")
                    If Not IsDBNull(dtTable.Rows(0).Item("vcPathForImage")) Then
                        If dtTable.Rows(0).Item("vcPathForImage") <> "" Then
                            imgItem.Visible = True
                            hplImage.Visible = True
                            imgItem.ImageUrl = Session("SiteType") & "//" & Request.ServerVariables("SERVER_NAME") & "/" & ConfigurationManager.AppSettings("PortalVirtualDirectoryName") & "/Documents/Docs/" & dtTable.Rows(0).Item("vcPathForImage")
                            hplImage.Attributes.Add("onclick", "return OpenImage(" & ddlItems.SelectedItem.Value & ")")
                        Else
                            imgItem.Visible = False
                            hplImage.Visible = False
                        End If
                    Else
                        imgItem.Visible = False
                        hplImage.Visible = False
                    End If
                    If dtTable.Rows(0).Item("charItemType") = "P" Or dtTable.Rows(0).Item("charItemType") = "A" Then
                        txtunits.Visible = False
                        hplUnit.Visible = False
                        trWareHouse.Visible = True
                        If dtTable.Rows(0).Item("bitSerialized") = True Then
                            txtSerialize.Text = 1
                        Else
                            txtSerialize.Text = 0
                        End If
                        If dtTable.Rows(0).Item("bitSerialized") = True And Session("OppType") = 1 Then
                            txtHidValue.Text = "True"
                            ddlWarehouse.DataSource = objCommon.GetWarehouseOnAttrSel(ddlItems.SelectedValue, "")
                            ddlWarehouse.DataTextField = "vcWareHouse"
                            ddlWarehouse.DataValueField = "numWareHouseItemId"
                            ddlWarehouse.DataBind()
                            ddlWarehouse.Items.Insert(0, "--Select One--")
                            ddlWarehouse.Items.FindByText("--Select One--").Value = "0"
                        Else
                            txtunits.Visible = True
                            hplUnit.Visible = True
                            txtHidValue.Text = "False"
                        End If
                    Else
                        If dtTable.Rows(0).Item("bitSerialized") = True Then
                            txtSerialize.Text = 1
                        Else
                            txtSerialize.Text = 0
                        End If
                        txtunits.Visible = True
                        hplUnit.Visible = True
                        txtHidValue.Text = ""
                        trWareHouse.Visible = False
                    End If
                End If
                objItems.ItemCode = ddlItems.SelectedValue
                dtTable = objItems.GetOptAccWareHouses
                If dtTable.Rows.Count > 0 Then
                    tblOptSelection.Visible = True
                    ddlOptItem.DataSource = dtTable
                    ddlOptItem.DataTextField = "vcItemName"
                    ddlOptItem.DataValueField = "numItemCode"
                    ddlOptItem.DataBind()
                    ddlOptItem.Items.Insert(0, "--Select One--")
                    ddlOptItem.Items.FindByText("--Select One--").Value = "0"
                Else
                    tblOptSelection.Visible = False
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Sub CreateAttributes()
        Dim strAttributes As String = ""
        strValues = ""
        Dim boolLoadNextdropdown As Boolean
        Dim objItems As New CItems
        objItems.ItemCode = ddlItems.SelectedValue
        dtOppAtributes = objItems.GetOppItemAttributes()
        If dtOppAtributes.Rows.Count > 0 Then
            Dim i As Integer
            Dim lbl As Label
            Dim table As New Table
            Dim tblrow As TableRow
            Dim tblcell As TableCell
            Dim ddl As DropDownList
            tblrow = New TableRow
            For i = 0 To dtOppAtributes.Rows.Count - 1
                tblcell = New TableCell
                tblcell.CssClass = "normal1"
                tblcell.HorizontalAlign = HorizontalAlign.Right
                lbl = New Label
                lbl.Text = dtOppAtributes.Rows(i).Item("Fld_label") & IIf(txtHidValue.Text = "True", "", "<font color='red'>*</font>")
                tblcell.Controls.Add(lbl)
                tblrow.Cells.Add(tblcell)

                If dtOppAtributes.Rows(i).Item("fld_type") = "Drop Down List Box" Then
                    tblcell = New TableCell
                    ddl = New DropDownList
                    ddl.CssClass = "signup"
                    ddl.ID = "Attr" & dtOppAtributes.Rows(i).Item("fld_id")

                    tblcell.Controls.Add(ddl)
                    ddl.AutoPostBack = True
                    AddHandler ddl.SelectedIndexChanged, AddressOf Me.LoadItemsonChangeofAttr
                    tblrow.Cells.Add(tblcell)
                    strAttributes = strAttributes & ddl.ID & "~" & dtOppAtributes.Rows(i).Item("Fld_label") & ","
                    If ddl.SelectedIndex > 0 Then
                        strValues = strValues & ddl.SelectedValue & ","
                    End If

                    If i = 0 Then
                        objCommon.sb_FillAttibuesFromDB(ddl, dtOppAtributes.Rows(i).Item("numlistid"), ddlItems.SelectedValue, "")
                    Else
                        objCommon.sb_FillAttibuesFromDB(ddl, IIf(boolLoadNextdropdown = True, dtOppAtributes.Rows(i).Item("numlistid"), 0), ddlItems.SelectedValue, strValues.TrimEnd(","))
                    End If
                    If ddl.SelectedIndex > 0 Then
                        boolLoadNextdropdown = True
                    End If
                End If

            Next
            strAttributes = strAttributes.TrimEnd(",")
            btnAdd.Attributes.Add("onclick", "return Add('" & strAttributes & "')")
            table.Rows.Add(tblrow)
            phItems.Controls.Add(tblrow)
        Else
            If ddlWarehouse.Items.Count = 0 Then
                ddlWarehouse.DataSource = objCommon.GetWarehouseOnAttrSel(ddlItems.SelectedValue, "")
                ddlWarehouse.DataTextField = "vcWareHouse"
                ddlWarehouse.DataValueField = "numWareHouseItemId"
                ddlWarehouse.DataBind()
                ddlWarehouse.Items.Insert(0, "--Select One--")
                ddlWarehouse.Items.FindByText("--Select One--").Value = "0"
            End If
            btnAdd.Attributes.Add("onclick", "return Add('')")
        End If
    End Sub


End Class