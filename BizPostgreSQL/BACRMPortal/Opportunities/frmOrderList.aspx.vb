Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item
Partial Class frmOrderList
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Dim strColumn As String
    Dim m_aryRightsForPage() As Integer
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Session("UserContactID") = Nothing Then
            Response.Redirect("../Common/frmLogout.aspx")
        End If
        m_aryRightsForPage = clsAuthorization.fn_ExternalPageLevelRights("frmOrderList.aspx", Session("UserContactID"), 15, 6)
        If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
            Response.Redirect("../Common/frmAuthorization.aspx")
        End If
        'If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
        '    btnNew.Visible = False
        'End If
        If Not IsPostBack Then
            txtCurrrentPage.Text = 1
            BindDatagrid()
            Dim objItems As New CItems
            Dim lngCredit As Long
            objItems.DivisionID = Session("DivId")
            lngCredit = objItems.GetCreditStatusofCompany
            lblBalDue.Text = String.Format("{0:#,##0.00}", objItems.GetAmountDue.Rows(0).Item("DueAmount"))
            lblRemCredit.Text = String.Format("{0:#,##0.00}", objItems.GetAmountDue.Rows(0).Item("RemainingCredit"))
            lblAmtPastDue.Text = String.Format("{0:#,##0.00}", objItems.GetAmountDue.Rows(0).Item("PastDueAmount"))
        End If
        If txtSortChar.Text <> "" Then
            Viewstate("SortChar") = txtSortChar.Text
            Viewstate("Column") = "BizDocName"
            Session("Asc") = 0
            BindDatagrid()
            txtSortChar.Text = ""
        End If
        'btnNew.Attributes.Add("onclick", "return goto('../Order/frmOrder.aspx','cntOpenItem1','divOrder');")
    End Sub

    Sub BindDatagrid()
        Dim dtOpportunity As New Datatable
        Dim objOpportunity As New COpportunities
        Dim SortChar As Char
        If Viewstate("SortChar") <> "" Then
            SortChar = Viewstate("SortChar")
        Else
            SortChar = "0"
        End If
        With objOpportunity
            .UserCntID = Session("UserContactID")
            .DomainID = Session("DomainID")
            .SortCharacter = SortChar
            .CompanyID = Session("CompID")
            If txtCurrrentPage.Text.Trim <> "" Then
                .CurrentPage = txtCurrrentPage.Text
            Else
                .CurrentPage = 1
            End If
            .PageSize = Session("PagingRows")
            .TotalRecords = 0
            If Viewstate("Column") <> "" Then
                .columnName = Viewstate("Column")
            Else
                .columnName = "dtCreatedDate"
            End If
            If Session("Asc") = 1 Then
                .columnSortOrder = "Asc"
            Else
                .columnSortOrder = "Desc"
            End If
        End With
        dtOpportunity = objOpportunity.GetInvoiceList
        If objOpportunity.TotalRecords = 0 Then
            hidenav.Visible = False
        Else
            hidenav.Visible = True
            lblRecordCount.Text = String.Format("{0:#,###}", objOpportunity.TotalRecords)
            Dim strTotalPage As String()
            Dim decTotalPage As Decimal
            decTotalPage = lblRecordCount.Text / Session("PagingRows")
            decTotalPage = Math.Round(decTotalPage, 2)
            strTotalPage = CStr(decTotalPage).Split(".")
            If (lblRecordCount.Text Mod Session("PagingRows")) = 0 Then
                lblTotal.Text = strTotalPage(0)
                txtTotalPage.Text = strTotalPage(0)
            Else
                lblTotal.Text = strTotalPage(0) + 1
                txtTotalPage.Text = strTotalPage(0) + 1
            End If
            txtTotalRecords.Text = lblRecordCount.Text
        End If
        dgOpportunity.DataSource = dtOpportunity
        dgOpportunity.DataBind()
    End Sub

    Private Sub lnkLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkLast.Click
        txtCurrrentPage.Text = txtTotalPage.Text
        BindDatagrid()
    End Sub

    Private Sub lnkPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkPrevious.Click
        If txtCurrrentPage.Text = 1 Then
            Exit Sub
        Else
            txtCurrrentPage.Text = txtCurrrentPage.Text - 1
        End If
        BindDatagrid()
    End Sub

    Private Sub lnkFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkFirst.Click
        txtCurrrentPage.Text = 1
        BindDatagrid()
    End Sub

    Private Sub lnkNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkPrevious.Click, lnkNext.Click
        If txtCurrrentPage.Text = txtTotalPage.Text Then
            Exit Sub
        Else
            txtCurrrentPage.Text = txtCurrrentPage.Text + 1
        End If
        BindDatagrid()
    End Sub

    Private Sub lnk2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk2.Click
        If txtCurrrentPage.Text + 1 = txtTotalPage.Text Or txtCurrrentPage.Text + 1 > txtTotalPage.Text Then
            Exit Sub
        Else
            txtCurrrentPage.Text = txtCurrrentPage.Text + 2
        End If
        BindDatagrid()
    End Sub

    Private Sub lnk3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk3.Click
        If txtCurrrentPage.Text + 2 = txtTotalPage.Text Or txtCurrrentPage.Text + 2 > txtTotalPage.Text Then
            Exit Sub
        Else
            txtCurrrentPage.Text = txtCurrrentPage.Text + 3
        End If
        BindDatagrid()
    End Sub

    Private Sub lnk4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk4.Click
        If txtCurrrentPage.Text + 3 = txtTotalPage.Text Or txtCurrrentPage.Text + 3 > txtTotalPage.Text Then
            Exit Sub
        Else
            txtCurrrentPage.Text = txtCurrrentPage.Text + 4
        End If
        BindDatagrid()
    End Sub

    Private Sub lnk5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk5.Click
        If txtCurrrentPage.Text + 4 = txtTotalPage.Text Or txtCurrrentPage.Text + 4 > txtTotalPage.Text Then
            Exit Sub
        Else
            txtCurrrentPage.Text = txtCurrrentPage.Text + 5
        End If
        BindDatagrid()
    End Sub

    Private Sub txtCurrrentPage_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCurrrentPage.TextChanged
        BindDatagrid()
    End Sub

    Function ReturnName(ByVal bintCreatedDate As Date) As String
        Dim strCreateDate As String
        strCreateDate = FormattedDateFromDate(bintCreatedDate, Session("DateFormat"))
        Return strCreateDate
    End Function


    Function ReturnDateTime(ByVal CloseDate) As String

        Try
            Dim strTargetResolveDate As String = ""
            Dim temp As String = ""
            If Not IsDBNull(CloseDate) Then
                strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))

                Dim timePart As String = CloseDate.ToShortTimeString.Substring(0, CloseDate.ToShortTimeString.Length - 1)
                ' remove gaps
                If timePart.Split(" ").Length >= 2 Then
                    timePart = timePart.Split(" ").GetValue(0) + timePart.Split(" ").GetValue(1)
                End If

                ' check Today date components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                ' if both are same it is today
                If (CloseDate.Date = Now.Date And CloseDate.Month = Now.Month And CloseDate.Year = Now.Year) Then
                    strTargetResolveDate = "<font color=red><b>Today</b></font>"
                    Return strTargetResolveDate


                    ' check [ TodayDate + 1 ] Date.... components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                    ' if both are same it was Yesterday
                ElseIf (CloseDate.Date.AddDays(1).Date = Now.Date And CloseDate.AddDays(1).Month = Now.Month And CloseDate.AddDays(1).Year = Now.Year) Then
                    strTargetResolveDate = "<font color=purple><b>Yesterday</b></font>"
                    Return strTargetResolveDate


                    ' check TodayDate .... components [ Date , Month , Year ] with Parameter [ CloseDate + 1 ] Date....  components [ Date , Month , Year ] 
                    ' if both are same it will Tomorrow
                ElseIf (CloseDate.Date = Now.AddDays(1).Date And CloseDate.Month = Now.AddDays(1).Month And CloseDate.Year = Now.AddDays(1).Year) Then
                    temp = CloseDate.Hour.ToString + ":" + CloseDate.Minute.ToString
                    strTargetResolveDate = "<font color=orange><b>Tomorrow</b></font>"
                    Return strTargetResolveDate


                    ' display day name for next 4 days from now
                ElseIf Format(CloseDate, "yyyyMMdd") = Format(DateTime.Now.AddDays(2), "yyyyMMdd") Or Format(CloseDate, "yyyyMMdd") = Format(DateTime.Now.AddDays(3), "yyyyMMdd") Or Format(CloseDate, "yyyyMMdd") = Format(DateTime.Now.AddDays(4), "yyyyMMdd") Or Format(CloseDate, "yyyyMMdd") = Format(DateTime.Now.AddDays(5), "yyyyMMdd") Then
                    strTargetResolveDate = "<b>" & CloseDate.DayOfWeek.ToString & "</b>"
                    Return strTargetResolveDate
                Else
                    strTargetResolveDate = strTargetResolveDate
                    Return strTargetResolveDate
                End If

               
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Function ReturnMoney(ByVal Money)
        Return Math.Round(Money, 2)
    End Function

    Private Sub dgOpportunity_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgOpportunity.SortCommand
        strColumn = e.SortExpression.ToString()
        If Viewstate("Column") <> strColumn Then
            Viewstate("Column") = strColumn
            Session("Asc") = 0
        Else
            If Session("Asc") = 0 Then
                Session("Asc") = 1
            Else
                Session("Asc") = 0
            End If
        End If
        BindDatagrid()
    End Sub

    Private Sub dgOpportunity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgOpportunity.ItemCommand
        Dim OpporID As Long
        If Not e.CommandName = "Sort" Then
            OpporID = e.Item.Cells(0).Text()
        End If
        If e.CommandName = "Customer" Then
            Response.Redirect("../common/frmAccounts.aspx")
        ElseIf e.CommandName = "Contact" Then
            Dim objCommon As New CCommon
            objCommon.OppID = OpporID
            objCommon.charModule = "O"
            objCommon.GetCompanySpecificValues1()
            Session("ContactId") = objCommon.ContactID
            Response.Redirect("../common/frmContacts.aspx")
        ElseIf e.CommandName = "Name" Then
            Session("OppID") = OpporID
            Response.Redirect("../Opportunities/frmOpportunities.aspx?frm=Orderlist")
        End If
    End Sub

    Private Sub dgOpportunity_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgOpportunity.ItemDataBound
        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
            Dim lngGrandTotal, lngAmtPaid As Decimal
            lngGrandTotal = CalTotalAmt(e.Item.Cells(1).Text, e.Item.Cells(0).Text)
            lngAmtPaid = e.Item.Cells(8).Text
            e.Item.Cells(7).Text = String.Format("{0:#,##0.00}", lngGrandTotal)
            e.Item.Cells(9).Text = String.Format("{0:#,##0.00}", lngGrandTotal - lngAmtPaid)
            e.Item.Cells(2).Attributes.Add("onclick", "return OpenBizInvoice('" & e.Item.Cells(0).Text & "','" & e.Item.Cells(1).Text & "')")
        End If
    End Sub

    Function CalTotalAmt(ByVal BizDocID As Long, ByVal OppID As Long) As String
        Dim dtOppBiDocDtl As New Datatable
        Dim objOppBizDocs As New OppBizDocs
        objOppBizDocs.OppBizDocId = BizDocID
        objOppBizDocs.OppId = OppID
        dtOppBiDocDtl = objOppBizDocs.GetOppBizDocDtl
        Dim dblDisc, dblShipping As Double
        If dtOppBiDocDtl.Rows.Count <> 0 Then
            If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("decDiscount")) Then
                dblDisc = Math.Round(dtOppBiDocDtl.Rows(0).Item("decDiscount"), 2)
            Else
                dblDisc = "0.00"
            End If
            If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("monShipCost")) Then
                dblShipping = Math.Round(dtOppBiDocDtl.Rows(0).Item("monShipCost"), 2)
            Else
                dblShipping = "0.00"
            End If
        End If
        Dim dtOppBiDocItems As New Datatable
        objOppBizDocs.OppBizDocId = BizDocID
        objOppBizDocs.OppId = OppID
        dtOppBiDocItems = objOppBizDocs.GetOppInItems.tables(0)
        Dim decTotalAmount As Decimal
        Dim decTaxAmount As Decimal
        Dim i As Integer
        For i = 0 To dtOppBiDocItems.Rows.Count - 1
            decTotalAmount = decTotalAmount + dtOppBiDocItems.Rows(i).Item("amount")
            decTaxAmount = decTaxAmount + dtOppBiDocItems.Rows(i).Item("amount") * dtOppBiDocItems.Rows(i).Item("Tax") / 100
        Next
        Dim decDisc, decInterest As Decimal
        If dtOppBiDocDtl.Rows(0).Item("tintInterestType") = 0 Then
            If DateAdd(DateInterval.Day, dtOppBiDocDtl.Rows(0).Item("numBillingDays"), dtOppBiDocDtl.Rows(0).Item("bintAccountClosingDate")) > Now() Then
                decDisc = 0
            Else
                decDisc = dtOppBiDocDtl.Rows(0).Item("fltInterest")
            End If
        Else
            If DateAdd(DateInterval.Day, dtOppBiDocDtl.Rows(0).Item("numBillingDays"), dtOppBiDocDtl.Rows(0).Item("bintAccountClosingDate")) > Now() Then
                decInterest = 0
            Else
                decInterest = dtOppBiDocDtl.Rows(0).Item("fltInterest")
            End If
        End If
        Dim decDiscAmount, decTotalDiscount, decTotalAmtDisc, decLateCharge As Decimal

        If dblDisc <> 0 Then
            Try

                decDiscAmount = decTotalAmount * (100 - dblDisc) / 100

                If decDisc > 0 Then
                    decTotalDiscount = decTotalAmount * dblDisc / 100 + decDiscAmount * decDisc / 100
                    decTotalAmtDisc = decTotalAmount - decTotalDiscount + dblShipping + decTaxAmount
                    decLateCharge = "0.00"
                Else
                    decTotalDiscount = decTotalAmount * dblDisc / 100
                    If decInterest * decDiscAmount = 0 Then
                        decLateCharge = "0.00"
                    Else
                        decLateCharge = decInterest * decDiscAmount / 100
                    End If
                    decTotalAmtDisc = decTotalAmount + decInterest * decDiscAmount / 100 - decTotalDiscount + dblShipping + decTaxAmount
                End If

            Catch ex As Exception

            End Try

        Else
            If decDisc > 0 Then
                decTotalDiscount = decTotalAmount * decDisc / 100
                decTotalAmtDisc = decTotalAmount - decTotalDiscount + dblShipping + decTaxAmount
                decLateCharge = "0.00"
            Else
                decTotalDiscount = "0.00"
                decTotalAmtDisc = decTotalAmount + decTotalAmount * decInterest / 100 + dblShipping + decTaxAmount
            End If

        End If
        Return String.Format("{0:#,##0.00}", decTotalAmtDisc)
    End Function

    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        BindDatagrid()

    End Sub
End Class
