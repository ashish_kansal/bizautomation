<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmLinkedProjects.aspx.vb" Inherits="BACRMPortal.frmLinkedProjects" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
         <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
        <link rel="stylesheet" href="~/CSS/lists.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table width="100%">
								<tr>
									<td align="right">
										<asp:button id="btnProOK" Runat="server" CssClass="button" Text="Close" Width="50" OnClientClick="javascript:window.close()"></asp:button></td>
								</tr>
							</table>
    <asp:table id="table4" Runat="server" BorderWidth="1" Width="100%" BackColor="white" CellSpacing="0"
					CellPadding="0" BorderColor="black" GridLines="None" Height="150">
					<asp:tableRow>
						<asp:tableCell VerticalAlign="Top">
							
								<table width="100%">
								<tr>
									<td>
										<asp:datagrid id="dgLinkProjects" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
											BorderColor="white">
											<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
											<ItemStyle CssClass="is"></ItemStyle>
											<HeaderStyle CssClass="hs"></HeaderStyle>
											<Columns>
												
												<asp:BoundColumn HeaderText="Project Name" DataField="vcProjectName" ></asp:BoundColumn>
											</Columns>
										</asp:datagrid>
									</td>
								</tr>
							</table>
						</asp:tableCell>
					</asp:tableRow>
				</asp:table>
    </div>
    </form>
</body>
</html>
