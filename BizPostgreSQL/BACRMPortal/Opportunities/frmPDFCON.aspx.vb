Imports System
Imports System.Diagnostics
Imports System.Drawing
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing.Imaging
Imports Syncfusion.Pdf.Shared
Imports Syncfusion.Pdf
Imports Syncfusion.HtmlConverter
Partial Public Class PDFCON
    Inherits System.Web.UI.Page

    Private m_converter As HtmlConverter

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Public Function ToPDF(ByVal img As Image) As String
        Dim doc As PDFDocument = New PDFDocument
        doc.Compression = CompressionLevel.Normal
        Dim properties As IPDFGraphicState = doc.CreateGraphicState()
        properties.SplitImages = True
        properties.BreakBehavior = AutoBreakBehavior.Cropping

        If Not img Is Nothing Then
            doc.LastPage.Margins.All = 0
            Dim dest As SizeF = New Size
            dest = New SizeF(doc.LastPage.DrawingWidth, img.Height)
            doc.LastPage.Graphics.DrawImage(PointF.Empty, img, dest, properties)
            'Response.Write(img.Width)
        End If

        Dim FilePath As String = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs"
        Dim path As String = "File" & Format(Now, "ddmmyyyyhhmmss") & ".pdf"
        doc.Save(FilePath & "\" & path)

        Return path
    End Function

    Public Function Convert(ByVal url As String)
        Dim html As HtmlConverter = New HtmlConverter
        Dim path As String
        Try

            m_converter = html

            Dim bmp As System.Drawing.Image

            'bmp.FromFile("http://localhost/SyncFusionPDF/default.aspx")
            bmp = html.ConvertToImage(url, ImageType.Bitmap, 700)
            'bmp.Save("C:\Test\Test.bmp")

            Try
                path = ToPDF(bmp)
            Finally
                If TypeOf bmp Is IDisposable Then
                    Dim disp As IDisposable = bmp
                    disp.Dispose()
                End If
            End Try
            m_converter = Nothing
        Finally
            If TypeOf html Is IDisposable Then
                Dim disp As IDisposable = html
                disp.Dispose()
            End If
        End Try
        Return path
    End Function

    Private Function GetImageType() As ImageType
        Return (ImageType.Bitmap)
    End Function

    Protected Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Convert(TextBox1.Text)
    End Sub
End Class