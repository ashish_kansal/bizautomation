'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Opportunities


Partial Class frmOppTime
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddlBizDoc As System.Web.UI.WebControls.DropDownList
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    ''Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
    ''    Try
    ''        If Not IsPostBack Then
    ''            Dim objOppTimeExpense As New OppTimeExpense(Session("UserContactID"))
    ''            Dim dtBizDoc As New DataTable
    ''            dtBizDoc = objOppTimeExpense.GetBizDoc(ConfigurationManager.AppSettings("ConnectionString"))
    ''            ddlBizDoc.DataSource = dtBizDoc
    ''            ddlBizDoc.DataValueField = "numListItemID"
    ''            ddlBizDoc.DataTextField = "vcData"
    ''            ddlBizDoc.DataBind()
    ''            ddlBizDoc.Items.Insert(0, "--Select One--")
    ''            ddlBizDoc.Items.FindByText("--Select One--").Value = 0
    ''            getdetails()
    ''        End If
    ''        btnCancel.Attributes.Add("onclick", "return Close();")
    ''    Catch ex As Exception
    ''        Response.Write(ex)
    ''    End Try
    ''    Try
    ''        If Not IsPostBack Then
    ''            getdetails()
    ''        End If
    ''        btnCancel.Attributes.Add("onclick", "return Close();")
    ''        btnSaveClose.Attributes.Add("onclick", "return Save();")
    ''        Dim objCommon As New CCommon
    ''        objCommon.OppID = GetQueryStringVal(Request.QueryString("enc"),"OpID")
    ''        objCommon.GetCompanySpecificValues()
    ''        hplRec.Attributes.Add("onclick", "return OpenRec(" & objCommon.DivisionID & ")")
    ''    Catch ex As Exception
    ''        Response.Write(ex)
    ''    End Try
    ''End Sub

    ''Sub getdetails()
    ''    Try
    ''        Dim objOppTimeExpense As New OppTimeExpense(Session("UserContactID"))
    ''        Dim dtTimeDetails As New DataTable
    ''        objOppTimeExpense.OppID = Request("Opid")
    ''        objOppTimeExpense.OppStageID = Request("OPPStageID")
    ''        dtTimeDetails = objOppTimeExpense.GetTimeDetails
    ''        If dtTimeDetails.Rows.Count <> 0 Then
    ''            ddlBizDoc.Items.FindByValue(dtTimeDetails.Rows(0).Item("numBizDocId")).Selected = True
    ''            txtRate.Text = dtTimeDetails.Rows(0).Item("numRate")
    ''            txtHour.Text = dtTimeDetails.Rows(0).Item("numHours")
    ''            If Not dtTimeDetails.Rows(0).Item("numMins") = 0 Then
    ''                ddlMin.Items.FindByValue(dtTimeDetails.Rows(0).Item("numMins")).Selected = True
    ''            End If
    ''            txtDesc.Text = IIf(IsDBNull(dtTimeDetails.Rows(0).Item("vcDesc")), "", dtTimeDetails.Rows(0).Item("vcDesc"))
    ''        End If
    ''    Catch ex As Exception
    ''        Response.Write(ex)
    ''    End Try
    ''End Sub



    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not IsPostBack Then
                getdetails()
            End If
            btnCancel.Attributes.Add("onclick", "return Close();")
            btnSaveClose.Attributes.Add("onclick", "return Save();")
            '' ''Dim objCommon As New BACRM.BusinessLogic.Common -- Commented by siva
            ''objCommon.OppID = GetQueryStringVal(Request.QueryString("enc"),"OpID")
            ''objCommon.GetCompanySpecificValues()
            ''hplRec.Attributes.Add("onclick", "return OpenRec(" & objCommon.DivisionID & ")")
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Sub getdetails()
        Try
            Dim objOppTimeExpense As New OppTimeExpense(Session("UserContactID"))
            Dim dtTimeDetails As New DataTable
            objOppTimeExpense.OppID = GetQueryStringVal(Request.QueryString("enc"), "Opid")
            objOppTimeExpense.OppStageID = GetQueryStringVal(Request.QueryString("enc"), "OPPStageID")
            dtTimeDetails = objOppTimeExpense.GetTimeDetails
            If dtTimeDetails.Rows.Count <> 0 Then
                txtRate.Text = String.Format("{0:#,##0.00}", dtTimeDetails.Rows(0).Item("numRate"))
                txtHour.Text = dtTimeDetails.Rows(0).Item("numHours")
                If Not dtTimeDetails.Rows(0).Item("numMins") = 0 Then
                    ddlMin.Items.FindByValue(dtTimeDetails.Rows(0).Item("numMins")).Selected = True
                End If
                txtDesc.Text = IIf(IsDBNull(dtTimeDetails.Rows(0).Item("vcDesc")), "", dtTimeDetails.Rows(0).Item("vcDesc"))
                txtItemID.Text = dtTimeDetails.Rows(0).Item("numItemID")
            End If
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Sub saveDetails()
        Try
            Dim objOppTimeExpense As New OppTimeExpense
            objOppTimeExpense.OppID = GetQueryStringVal(Request.QueryString("enc"), "Opid")
            objOppTimeExpense.OppStageID = GetQueryStringVal(Request.QueryString("enc"), "OPPStageID")
            objOppTimeExpense.Rate = txtRate.Text
            objOppTimeExpense.Hour = CInt(txtHour.Text)
            objOppTimeExpense.Min = ddlMin.SelectedItem.Value
            objOppTimeExpense.Desc = txtDesc.Text
            objOppTimeExpense.ItemID = IIf(txtItemID.Text = "", 0, txtItemID.Text)
            objOppTimeExpense.SaveTimeDetails()
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            saveDetails()
            Response.Write("<script>window.close();</script>")
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btndelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelete.Click
        Try
            Dim objOppTimeExpense As New OppTimeExpense
            objOppTimeExpense.OppID = GetQueryStringVal(Request.QueryString("enc"), "Opid")
            objOppTimeExpense.OppStageID = GetQueryStringVal(Request.QueryString("enc"), "OPPStageID")
            objOppTimeExpense.DeleteTimeDetails()
            txtDesc.Text = ""
            txtHour.Text = ""
            txtItemID.Text = ""
            txtRate.Text = ""
            ddlBizDoc.SelectedIndex = 0
            ddlMin.SelectedIndex = 0
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub
End Class


