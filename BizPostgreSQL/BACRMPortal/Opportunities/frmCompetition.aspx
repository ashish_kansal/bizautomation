<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmCompetition.aspx.vb" Inherits="BACRMPortal.frmCompetition" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" src="../common/calandar.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Competition</title>
		<script language="javascript">
		function DeleteRecord()
		{
			if(confirm('Are you sure, you want to delete the selected record?'))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		function Add()
			{
		
			if (document.Form1.ddlCompany.value==0)
						{
							alert("Select Company")
							document.Form1.ddlCompany.focus()
							return false;
						}
			if (document.Form1.ddlCompany.selectedIndex==-1)
						{
							alert("Select Company")
							document.Form1.ddlCompany.focus()
							return false;
						}
			if (document.Form1.ddlDivision.selectedIndex==-1)
						{
							alert("Select Division")
							document.Form1.ddlDivision.focus()
							return false;
						}
			if (document.Form1.ddlDivision.value==0)
						{
							alert("Select Division")
							document.Form1.ddlDivision.focus()
							return false;
						}
			}
		function Save()
		{
		
			var str='';
			for (i = 1; i <= dgCompetitor.rows.length; i++)
				{
				 if (typeof(document.all['dgCompetitor__ctl'+(i+1)+'_txtPrice'])!='undefined')
					  {
						str= str + document.all['dgCompetitor_ctl'+strI+'_lblCompetitorID'].innerText+',' + document.all['dgCompetitor_ctl'+strI+'_txtPrice'].value +',' + document.all['dgCompetitor_ctl'+strI+'_cal_txtDate'].value +'~'
					  }
				}
			
			document.Form1.txtHidden.value=str;
		}
		</script>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
		<asp:ScriptManager ID="sc" runat="server" EnablePartialRendering ="true"></asp:ScriptManager>
			<table width="100%">
				<tr>
					<td class="normal1" align="right">Competitor
					</td>
					<td><asp:textbox id="txtSearch" CssClass="signup" Runat="server"></asp:textbox></td>
					<td><asp:button id="btnGo" CssClass="button" Runat="server" Text="Go" Width="25"></asp:button></td>
					<td><asp:dropdownlist id="ddlCompany" CssClass="signup" Runat="server" Width="130" AutoPostBack="True"></asp:dropdownlist></td>
					
					<td><asp:button id="btnAdd" CssClass="button" Runat="server" Text="Add" Width="50"></asp:button></td>
					<td ><asp:button id="btnSave" CssClass="button" Runat="server" Text="Save" Width="50"></asp:button></td>
					<td><asp:button id="btnProOK" Runat="server" CssClass="button" Text="Close" Width="50" OnClientClick="javascript:window.close()"></asp:button></td>
				</tr>
			</table>
			<asp:datagrid id="dgCompetitor" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
				BorderColor="white">
				<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
				<ItemStyle CssClass="is"></ItemStyle>
				<HeaderStyle CssClass="hs"></HeaderStyle>
				<Columns>
					<asp:BoundColumn Visible="False" DataField="numCompetitorID"></asp:BoundColumn>
					<asp:BoundColumn DataField="Competitor" HeaderText="Competitor Name, Division"></asp:BoundColumn>
					<asp:TemplateColumn HeaderText="Price / Notes">
						<ItemTemplate>
							<asp:TextBox ID="txtPrice" Runat="server" Width=250 CssClass="signup" Text='<%# DataBinder.Eval(Container.DataItem, "VcPrice") %>'>
							</asp:TextBox>
							<asp:Label ID=lblCompetitorID Runat=server Text='<%# DataBinder.Eval(Container.DataItem, "numCompetitorID") %>' style="display:none" >
							</asp:Label>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn HeaderText="Date price was entered">
						<ItemTemplate>
							 <BizCalendar:Calendar runat="server" SelectedDate='<%# DataBinder.Eval(Container.DataItem, "dtDateEntered") %>' ID="cal" />
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:BoundColumn DataField="Phone" HeaderText="Phone Number, Ext"></asp:BoundColumn>
					<asp:TemplateColumn>
						<HeaderTemplate>
							<asp:Button ID="btnHdelete" Runat="server" CssClass="Delete" Text="r"></asp:Button>
						</HeaderTemplate>
						<ItemTemplate>
							<asp:Button ID="btnDelete" Runat="server" CssClass="Delete" Text="r" CommandName="Delete"></asp:Button>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
			</asp:datagrid>
			<table width="100%">
				<tr>
					<td class="normal4" align="center">
						<asp:Literal ID="litMessage" Runat="server"></asp:Literal></td>
				</tr>
			</table>
			<asp:TextBox ID="txtHidden" Runat="server" style="DISPLAY:none"></asp:TextBox>
	
		</form>
	</body>
</HTML>
