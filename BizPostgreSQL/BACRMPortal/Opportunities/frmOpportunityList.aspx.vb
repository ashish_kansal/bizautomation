
'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.common
Imports Infragistics.WebUI.UltraWebTab
Partial Class frmOpportunityList
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lnk1Sales As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lnk1Purchase As System.Web.UI.WebControls.LinkButton
    Dim strColumn As String
    Dim m_aryRightsForPage() As Integer
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Dim SI As Integer = 0
    Dim SI1 As Integer = 0
    Dim SI2 As Integer = 0
    Dim frm As String = ""
    Dim frm1 As String = ""
    Dim frm2 As String = ""
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            If Session("UserContactID") = Nothing Then
                Response.Redirect("../Common/frmLogout.aspx")
            End If
            m_aryRightsForPage = clsAuthorization.fn_ExternalPageLevelRights("frmOpportunityList.aspx", Session("UserContactID"), 15, 4)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../Common/frmAuthorization.aspx")
            End If
            If Not GetQueryStringVal(Request.QueryString("enc"), "SI") Is Nothing Then
                SI = GetQueryStringVal(Request.QueryString("enc"), "SI")
            End If
            If Not GetQueryStringVal(Request.QueryString("enc"), "SI1") Is Nothing Then
                SI1 = GetQueryStringVal(Request.QueryString("enc"), "SI1")
            Else
                SI1 = 0
            End If
            If Not GetQueryStringVal(Request.QueryString("enc"), "SI2") Is Nothing Then
                SI2 = GetQueryStringVal(Request.QueryString("enc"), "SI2")
            Else
                SI2 = 0
            End If
            If Not GetQueryStringVal(Request.QueryString("enc"), "frm") Is Nothing Then
                frm = ""
                frm = GetQueryStringVal(Request.QueryString("enc"), "frm")
            Else
                frm = ""
            End If
            If Not GetQueryStringVal(Request.QueryString("enc"), "frm1") Is Nothing Then
                frm1 = ""
                frm1 = GetQueryStringVal(Request.QueryString("enc"), "frm1")
            Else
                frm1 = ""
            End If
            If Not GetQueryStringVal(Request.QueryString("enc"), "SI2") Is Nothing Then
                frm2 = ""
                frm2 = GetQueryStringVal(Request.QueryString("enc"), "frm2")
            Else
                frm2 = ""
            End If
            If Not IsPostBack Then
                Session("Asc") = 1
                txtCurrrentPageSales.Text = 1
                txtCurrrentPagePurchase.Text = 1
                BindDatagrid()
            End If
            If txtSortCharSales.Text <> "" Then
                ViewState("SortCharS") = txtSortCharSales.Text
                ViewState("Column") = "vcPOppName"
                Session("Asc") = 0
                BindDatagrid()
                txtSortCharSales.Text = ""
            End If
            If txtSortCharPurchase.Text <> "" Then
                ViewState("SortCharP") = txtSortCharPurchase.Text
                ViewState("Column") = "vcPOppName"
                Session("Asc") = 0
                BindDatagrid()
                txtSortCharPurchase.Text = ""
            End If
            If Not IsPostBack Then
                If uwOppTab.Tabs.Count > SI Then
                    uwOppTab.SelectedTabIndex = SI
                End If
            End If
            If uwOppTab.SelectedTabIndex = 0 Then
                hidePurchase.Visible = False
                hideSales.Visible = True
                pnlSales.Visible = True
                pnlPurchase.Visible = False
                lblRecordsSales.Visible = True
                lblRecordsPurchase.Visible = False
            Else
                lblRecordsSales.Visible = False
                lblRecordsPurchase.Visible = True
                hidePurchase.Visible = True
                hideSales.Visible = False
                pnlSales.Visible = False
                pnlPurchase.Visible = True
            End If
            litMessage.Text = ""
            ' btnNew.Attributes.Add("onclick", "return goto('../Opportunities/frmNewOpprtunity.aspx','cntOpenItem','divNew');")
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Sub BindDatagrid()
        Try
            Dim dtOpportunity As New Datatable
            Dim objOpportunity As New COpportunities
            Dim SortChar As Char
            objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            If uwOppTab.SelectedTabIndex = 0 Then
                If ViewState("SortCharS") <> "" Then
                    SortChar = ViewState("SortCharS")
                Else
                    SortChar = "0"
                End If
            Else
                If ViewState("SortCharP") <> "" Then
                    SortChar = ViewState("SortCharP")
                Else
                    SortChar = "0"
                End If
            End If

            With objOpportunity
                .UserCntID = Session("UserContactID")
                .DomainID = Session("DomainID")
                .endDate = Now()
                .SortCharacter = SortChar
                .CompanyID = Session("CompID")
                If uwOppTab.SelectedTabIndex = 0 Then
                    .OppType = 1
                    If txtCurrrentPageSales.Text.Trim <> "" Then
                        .CurrentPage = txtCurrrentPageSales.Text
                    Else
                        .CurrentPage = 1
                    End If
                Else
                    .OppType = 2
                    If txtCurrrentPagePurchase.Text.Trim <> "" Then
                        .CurrentPage = txtCurrrentPagePurchase.Text
                    Else
                        .CurrentPage = 1
                    End If
                End If
                .PageSize = Session("PagingRows")
                .TotalRecords = 0
                If ViewState("Column") <> "" Then
                    .columnName = ViewState("Column")
                Else
                    .columnName = "opp.bintCreatedDate"
                End If
                If Session("Asc") = 1 Then
                    .columnSortOrder = "Desc"
                Else
                    .columnSortOrder = "Asc"
                End If

            End With
            dtOpportunity = objOpportunity.GetOpportunityList
            If objOpportunity.TotalRecords = 0 Then
                If uwOppTab.SelectedTabIndex = 0 Then
                    hideSales.Visible = False
                    lblRecordsSales.Text = 0
                Else
                    hidePurchase.Visible = False
                    lblRecordsPurchase.Text = 0
                End If
            Else
                If uwOppTab.SelectedTabIndex = 0 Then
                    hideSales.Visible = True
                    lblRecordsSales.Text = String.Format("{0:#,###}", objOpportunity.TotalRecords)
                    Dim strTotalPage As String()
                    Dim decTotalPage As Decimal
                    decTotalPage = lblRecordsSales.Text / Session("PagingRows")
                    decTotalPage = Math.Round(decTotalPage, 2)
                    strTotalPage = CStr(decTotalPage).Split(".")
                    If (lblRecordsSales.Text Mod Session("PagingRows")) = 0 Then
                        lblTotalSales.Text = strTotalPage(0)
                        txtTotalPageSales.Text = strTotalPage(0)
                    Else
                        lblTotalSales.Text = strTotalPage(0) + 1
                        txtTotalPageSales.Text = strTotalPage(0) + 1
                    End If
                    txtTotalRecordsSales.Text = lblRecordsSales.Text
                Else
                    hidePurchase.Visible = True
                    lblRecordsPurchase.Text = String.Format("{0:#,###}", objOpportunity.TotalRecords)
                    Dim strTotalPage As String()
                    Dim decTotalPage As Decimal
                    decTotalPage = lblRecordsPurchase.Text / Session("PagingRows")
                    decTotalPage = Math.Round(decTotalPage, 2)
                    strTotalPage = CStr(decTotalPage).Split(".")
                    If (lblRecordsPurchase.Text Mod Session("PagingRows")) = 0 Then
                        lblTotalPurchase.Text = strTotalPage(0)
                        txtTotalPagePurchase.Text = strTotalPage(0)
                    Else
                        lblTotalPurchase.Text = strTotalPage(0) + 1
                        txtTotalPagePurchase.Text = strTotalPage(0) + 1
                    End If
                    txtTotalPagePurchase.Text = lblRecordsPurchase.Text
                End If
            End If
            If uwOppTab.SelectedTabIndex = 0 Then
                dgSales.DataSource = dtOpportunity
                dgSales.DataBind()
            Else
                dgPurchase.DataSource = dtOpportunity
                dgPurchase.DataBind()
            End If
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub




    Function ReturnName(ByVal CreatedDate As Date) As String
        Dim strCreateDate As String
        If Not IsDBNull(CreatedDate) Then
            strCreateDate = FormattedDateFromDate(CreatedDate, Session("DateFormat"))
        End If
        Return strCreateDate
    End Function
    Function ReturnDateTime(ByVal CloseDate) As String

        Try
            Dim strTargetResolveDate As String = ""
            Dim temp As String = ""
            If Not IsDBNull(CloseDate) Then
                strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))

                Dim timePart As String = CloseDate.ToShortTimeString.Substring(0, CloseDate.ToShortTimeString.Length - 1)
                ' remove gaps
                If timePart.Split(" ").Length >= 2 Then
                    timePart = timePart.Split(" ").GetValue(0) + timePart.Split(" ").GetValue(1)
                End If

                ' check Today date components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                ' if both are same it is today
                If (CloseDate.Date = Now.Date And CloseDate.Month = Now.Month And CloseDate.Year = Now.Year) Then
                    strTargetResolveDate = "<font color=red><b>Today</b></font>"
                    Return strTargetResolveDate


                    ' check [ TodayDate + 1 ] Date.... components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                    ' if both are same it was Yesterday
                ElseIf (CloseDate.Date.AddDays(1).Date = Now.Date And CloseDate.AddDays(1).Month = Now.Month And CloseDate.AddDays(1).Year = Now.Year) Then
                    strTargetResolveDate = "<font color=purple><b>Yesterday</b></font>"
                    Return strTargetResolveDate


                    ' check TodayDate .... components [ Date , Month , Year ] with Parameter [ CloseDate + 1 ] Date....  components [ Date , Month , Year ] 
                    ' if both are same it will Tomorrow
                ElseIf (CloseDate.Date = Now.AddDays(1).Date And CloseDate.Month = Now.AddDays(1).Month And CloseDate.Year = Now.AddDays(1).Year) Then
                    temp = CloseDate.Hour.ToString + ":" + CloseDate.Minute.ToString
                    strTargetResolveDate = "<font color=orange><b>Tomorrow</b></font>"
                    Return strTargetResolveDate


                    ' display day name for next 4 days from now
                ElseIf Format(CloseDate, "yyyyMMdd") = Format(DateTime.Now.AddDays(2), "yyyyMMdd") Or Format(CloseDate, "yyyyMMdd") = Format(DateTime.Now.AddDays(3), "yyyyMMdd") Or Format(CloseDate, "yyyyMMdd") = Format(DateTime.Now.AddDays(4), "yyyyMMdd") Or Format(CloseDate, "yyyyMMdd") = Format(DateTime.Now.AddDays(5), "yyyyMMdd") Then
                    strTargetResolveDate = "<b>" & CloseDate.DayOfWeek.ToString & "</b>"
                    Return strTargetResolveDate
                Else
                    strTargetResolveDate = strTargetResolveDate
                    Return strTargetResolveDate
                End If


            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Function ReturnMoney(ByVal Money)
        If Not IsDBNull(Money) Then
            Return String.Format("{0:#,###.00}", Money)
        End If
    End Function




    Private Sub dgPurchase_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgPurchase.SortCommand
        Try
            strColumn = e.SortExpression.ToString()
            If ViewState("Column") <> strColumn Then
                ViewState("Column") = strColumn
                Session("Asc") = 0
            Else
                If Session("Asc") = 0 Then
                    Session("Asc") = 1
                Else
                    Session("Asc") = 0
                End If
            End If
            BindDatagrid()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgSales_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgSales.SortCommand
        Try
            strColumn = e.SortExpression.ToString()
            If ViewState("Column") <> strColumn Then
                ViewState("Column") = strColumn
                Session("Asc") = 0
            Else
                If Session("Asc") = 0 Then
                    Session("Asc") = 1
                Else
                    Session("Asc") = 0
                End If
            End If
            BindDatagrid()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkLastSales_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLastSales.Click
        Try
            txtCurrrentPageSales.Text = txtTotalPageSales.Text
            BindDatagrid()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkPreviousSales_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPreviousSales.Click
        Try
            If txtCurrrentPageSales.Text = 1 Then
                Exit Sub
            Else
                txtCurrrentPageSales.Text = txtCurrrentPageSales.Text - 1
            End If
            BindDatagrid()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkFirstSales_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFirstSales.Click
        Try
            txtCurrrentPageSales.Text = 1
            BindDatagrid()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkNextSales_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNextSales.Click
        Try
            If txtCurrrentPageSales.Text = txtTotalPageSales.Text Then
                Exit Sub
            Else
                txtCurrrentPageSales.Text = txtCurrrentPageSales.Text + 1
            End If
            BindDatagrid()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub


    Private Sub lnk2Sales_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk2Sales.Click
        Try
            If txtCurrrentPageSales.Text + 1 = txtTotalPageSales.Text Or txtCurrrentPageSales.Text + 1 > txtTotalPageSales.Text Then
                Exit Sub
            Else
                txtCurrrentPageSales.Text = txtCurrrentPageSales.Text + 2
            End If
            BindDatagrid()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk3Sales_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk3Sales.Click
        Try
            If txtCurrrentPageSales.Text + 2 = txtTotalPageSales.Text Or txtCurrrentPageSales.Text + 2 > txtTotalPageSales.Text Then
                Exit Sub
            Else
                txtCurrrentPageSales.Text = txtCurrrentPageSales.Text + 3
            End If
            BindDatagrid()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk4Sales_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk4Sales.Click
        Try
            If txtCurrrentPageSales.Text + 3 = txtTotalPageSales.Text Or txtCurrrentPageSales.Text + 3 > txtTotalPageSales.Text Then
                Exit Sub
            Else
                txtCurrrentPageSales.Text = txtCurrrentPageSales.Text + 4
            End If
            BindDatagrid()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk5Sales_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk5Sales.Click
        Try
            If txtCurrrentPageSales.Text + 4 = txtTotalPageSales.Text Or txtCurrrentPageSales.Text + 4 > txtTotalPageSales.Text Then
                Exit Sub
            Else
                txtCurrrentPageSales.Text = txtCurrrentPageSales.Text + 5
            End If
            BindDatagrid()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

   

    Private Sub dgPurchase_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgPurchase.ItemCommand
        Try
            Dim lngOppID As Long
            If Not e.CommandName = "Sort" Then
                lngOppID = e.Item.Cells(0).Text()
            End If
            If e.CommandName = "Customer" Then
                If Session("EnableIntMedPage") = 1 Then
                    Response.Redirect("../pagelayout/frmAccountdtl.aspx?frm=OppList" & "&SI1=" & uwOppTab.SelectedTabIndex & "&SI2=" & SI1 & "&frm1=" & frm2)
                Else
                    Response.Redirect("../common/frmAccounts.aspx?frm=OppList" & "&SI1=" & uwOppTab.SelectedTabIndex & "&SI2=" & SI1 & "&frm1=" & frm2)
                End If

            ElseIf e.CommandName = "Contact" Then
                Dim objCommon As New CCommon
                objCommon.OppID = lngOppID
                objCommon.charModule = "O"
                objCommon.GetCompanySpecificValues1()
                Session("ContactId") = objCommon.ContactID
                If Session("EnableIntMedPage") = 1 Then
                    Response.Redirect("../pagelayout/frmContactdtl.aspx?frm=OppList" & "&SI1=" & uwOppTab.SelectedTabIndex & "&SI2=" & SI1 & "&frm1=" & frm2)
                Else
                    Response.Redirect("../Contacts/frmContacts.aspx?frm=OppList" & "&SI1=" & uwOppTab.SelectedTabIndex & "&SI2=" & SI1 & "&frm1=" & frm2)
                End If

            ElseIf e.CommandName = "Name" Then
                Session("OppID") = lngOppID
                If Session("EnableIntMedPage") = 1 Then
                    Response.Redirect("../pagelayout/frmOppurtunitydtl.aspx?frm=OppList" & "&SI1=" & uwOppTab.SelectedTabIndex & "&SI2=" & SI1 & "&frm1=" & frm2)
                Else
                    Response.Redirect("../Opportunities/frmOpportunities.aspx?frm=OppList" & "&SI1=" & uwOppTab.SelectedTabIndex & "&SI2=" & SI1 & "&frm1=" & frm2)
                End If

            ElseIf e.CommandName = "Delete" Then
                Try
                    Dim objOpport As New COpportunities
                    With objOpport
                        .OpportID = lngOppID
                        .DomainID = Session("DomainID")
                    End With
                    objOpport.DelOpp()
                    BindDatagrid()
                Catch ex As Exception
                    litMessage.Text = "Dependent record exists. Cannot be Deleted."
                End Try
            End If
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgSales_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgSales.ItemCommand
        Try
            Dim lngOppID As Long
            If Not e.CommandName = "Sort" Then
                lngOppID = e.Item.Cells(0).Text()

            End If
            If e.CommandName = "Customer" Then
                If Session("EnableIntMedPage") = 1 Then
                    Response.Redirect("../pagelayout/frmAccountdtl.aspx?frm=OppList" & "&SI1=" & uwOppTab.SelectedTabIndex & "&SI2=" & SI1 & "&frm1=" & frm2)
                Else
                    Response.Redirect("../common/frmAccounts.aspx?frm=OppList" & "&SI1=" & uwOppTab.SelectedTabIndex & "&SI2=" & SI1 & "&frm1=" & frm2)
                End If

            ElseIf e.CommandName = "Contact" Then
                Dim objCommon As New CCommon
                objCommon.OppID = lngOppID
                objCommon.charModule = "O"
                objCommon.GetCompanySpecificValues1()
                Session("ContactId") = objCommon.ContactID
                If Session("EnableIntMedPage") = 1 Then
                    Response.Redirect("../pagelayout/frmContactdtl.aspx?frm=OppList" & "&SI1=" & uwOppTab.SelectedTabIndex & "&SI2=" & SI1 & "&frm1=" & frm2)
                Else
                    Response.Redirect("../Contacts/frmContacts.aspx?frm=OppList" & "&SI1=" & uwOppTab.SelectedTabIndex & "&SI2=" & SI1 & "&frm1=" & frm2)
                End If

            ElseIf e.CommandName = "Name" Then
                Session("OppID") = lngOppID
                If Session("EnableIntMedPage") = 1 Then
                    Response.Redirect("../pagelayout/frmOppurtunitydtl.aspx?frm=OppList" & "&SI1=" & uwOppTab.SelectedTabIndex & "&SI2=" & SI1 & "&frm1=" & frm2)
                Else
                    Response.Redirect("../Opportunities/frmOpportunities.aspx?frm=OppList" & "&SI1=" & uwOppTab.SelectedTabIndex & "&SI2=" & SI1 & "&frm1=" & frm2)
                End If


            ElseIf e.CommandName = "Delete" Then
                Try
                    Dim objOpport As New COpportunities
                    With objOpport
                        .OpportID = lngOppID
                        .DomainID = Session("DomainID")
                    End With
                    objOpport.DelOpp()
                    BindDatagrid()
                Catch ex As Exception
                    litMessage.Text = "Dependent record exists. Cannot be Deleted."
                End Try
            End If
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkPreviousPurchase_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPreviousPurchase.Click
        Try
            If txtCurrrentPagePurchase.Text = 1 Then
                Exit Sub
            Else
                txtCurrrentPagePurchase.Text = txtCurrrentPagePurchase.Text - 1
            End If
            BindDatagrid()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkFirstPurchase_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFirstPurchase.Click
        Try
            txtCurrrentPagePurchase.Text = 1
            BindDatagrid()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkNextPurchase_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNextPurchase.Click
        Try
            If txtCurrrentPagePurchase.Text = txtTotalPagePurchase.Text Then
                Exit Sub
            Else
                txtCurrrentPagePurchase.Text = txtTotalPagePurchase.Text + 1
            End If
            BindDatagrid()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkLastPurchase_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLastPurchase.Click
        Try
            txtCurrrentPagePurchase.Text = txtTotalPagePurchase.Text
            BindDatagrid()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk2Purchase_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk2Purchase.Click
        Try
            If txtCurrrentPagePurchase.Text + 1 = txtTotalPagePurchase.Text Or txtCurrrentPagePurchase.Text + 1 > txtTotalPagePurchase.Text Then
                Exit Sub
            Else
                txtCurrrentPagePurchase.Text = txtCurrrentPagePurchase.Text + 2
            End If
            BindDatagrid()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk3Purchase_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk3Purchase.Click
        Try
            If txtCurrrentPagePurchase.Text + 2 = txtTotalPagePurchase.Text Or txtCurrrentPagePurchase.Text + 2 > txtTotalPagePurchase.Text Then
                Exit Sub
            Else
                txtCurrrentPagePurchase.Text = txtCurrrentPagePurchase.Text + 3
            End If
            BindDatagrid()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk4Purchase_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk4Purchase.Click
        Try
            If txtCurrrentPagePurchase.Text + 3 = txtTotalPagePurchase.Text Or txtCurrrentPagePurchase.Text + 3 > txtTotalPagePurchase.Text Then
                Exit Sub
            Else
                txtCurrrentPagePurchase.Text = txtCurrrentPagePurchase.Text + 4
            End If
            BindDatagrid()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk5Purchase_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk5Purchase.Click
        Try
            If txtCurrrentPagePurchase.Text + 4 = txtTotalPagePurchase.Text Or txtCurrrentPagePurchase.Text + 4 > txtTotalPagePurchase.Text Then
                Exit Sub
            Else
                txtCurrrentPagePurchase.Text = txtCurrrentPagePurchase.Text + 5
            End If
            BindDatagrid()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgPurchase_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgPurchase.ItemDataBound
        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then

            Dim btnDelete As Button
            Dim lnkDelete As LinkButton
            lnkDelete = e.Item.FindControl("lnkDeletePurchase")
            btnDelete = e.Item.FindControl("btnDeletePurchase")
            If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                btnDelete.Visible = False
                lnkDelete.Visible = True
                lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
            Else
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            End If
        End If
    End Sub

    Private Sub dgSales_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgSales.ItemDataBound
        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then

            Dim btnDelete As Button
            Dim lnkDelete As LinkButton
            lnkDelete = e.Item.FindControl("lnkdeleteSales")
            btnDelete = e.Item.FindControl("btnDeleteSales")
            If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                btnDelete.Visible = False
                lnkDelete.Visible = True
                lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
            Else
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            End If
        End If
    End Sub

    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        BindDatagrid()

    End Sub

    Private Sub uwOppTab_TabClick(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebTab.WebTabEvent) Handles uwOppTab.TabClick
        Try
            If uwOppTab.SelectedTabIndex = 0 Then
                txtCurrrentPageSales.Text = 1
            Else
                txtCurrrentPagePurchase.Text = 1
            End If
            BindDatagrid()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub
End Class
