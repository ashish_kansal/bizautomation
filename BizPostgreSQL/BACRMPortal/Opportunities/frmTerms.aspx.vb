'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common

Partial Class frmTerms
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            Dim objCommon As New CCommon
            objCommon.sb_FillComboFromDBwithSel(ddlTerms, 3, Session("DomainID"))
            If GetQueryStringVal(Request.QueryString("enc"), "Ter") <> "" Then
                ddlTerms.Items.FindByValue(GetQueryStringVal(Request.QueryString("enc"), "Ter")).Selected = True
            End If
        End If
        btnCancel.Attributes.Add("onclick", "return WindowClose()")
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If ddlTerms.SelectedIndex > 0 Then
            Dim objOppInvoice As New OppInvoice(Session("UserContactID"))
            objOppInvoice.Terms = ddlTerms.SelectedItem.Value
            objOppInvoice.UserId = Session("UserContactID")
            objOppInvoice.OppBizDocId = GetQueryStringVal(Request.QueryString("enc"), "OppBizDocID")
            objOppInvoice.UpdateTerms()
        End If
        Response.Write("<script>opener.location.reload(true); self.close();</script>")
    End Sub
End Class


