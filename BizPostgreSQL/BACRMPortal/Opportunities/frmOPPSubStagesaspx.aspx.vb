'***************************************************************************************************************************
'     Author Name				 :  Anoop Jayaraj
'     Date Written				 :  18/2/2005
'***************************************************************************************************************************

Imports BACRM.BusinessLogic.Opportunities



Partial Class frmOPPSubStagesaspx
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
        Try
            If Not IsPostBack Then

                Dim objSubStages As New SubStages(Session("UserContactID"))
                Dim dtSubStages As New Datatable
                dtSubStages = objSubStages.GetSubStage
                ddlSubStage.DataSource = dtSubStages
                ddlSubStage.DataTextField = "vcSubStageName"
                ddlSubStage.DataValueField = "numSubStageHdrID"
                ddlSubStage.DataBind()
                ddlSubStage.Items.Insert(0, "--Select One--")
                ddlSubStage.Items.FindByText("--Select One--").Value = 0

                GetDetails()
            End If

            BuildSubStages()
            btnCancel.Attributes.Add("onclick", "return Close()")
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Sub GetDetails()
        Try
            Dim dtSubStages As New Datatable
            Dim objOpportunity As New MOpportunity(Session("UserContactID"))
            objOpportunity.OppStageID = GetQueryStringVal(Request.QueryString("enc"), "OPPStageID")
            objOpportunity.OpportunityId = GetQueryStringVal(Request.QueryString("enc"), "Opid")
            dtSubStages = objOpportunity.GetSubStage
            Session("SubStages") = dtSubStages
            If dtSubStages.Rows.Count <> 0 Then
                ddlSubStage.Items.FindByValue(dtSubStages.Rows(0).Item("numSubStageID")).Selected = True
            End If
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub


    Sub BuildSubStages()
        Try
            Dim dtSubStages As New DataTable
            Dim tblCell As TableCell
            Dim tblrow As TableRow
            Dim chk As CheckBox
            Dim i As Integer
            dtSubStages = Session("SubStages")
            tblSubStage.Rows.Clear()
            If dtSubStages.Rows.Count <> 0 Then
                For i = 0 To dtSubStages.Rows.Count - 1
                    tblrow = New TableRow
                    tblCell = New TableCell
                    tblCell.CssClass = "normal1"
                    chk = New CheckBox
                    chk.ID = "chkStage~" & i
                    chk.Text = dtSubStages.Rows(i).Item("vcSubStageDetail")
                    If dtSubStages.Rows(i).Item("bitStageCompleted") = True Then
                        dtSubStages.Rows(i).Item("bitStageCompleted") = 1
                        chk.Checked = True
                    Else
                        dtSubStages.Rows(i).Item("bitStageCompleted") = 0
                        chk.Checked = False
                    End If
                    tblCell.Controls.Add(chk)
                    tblrow.Cells.Add(tblCell)
                    tblSubStage.Rows.Add(tblrow)
                Next
            End If
            Session("SubStages") = dtSubStages
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub


    Private Sub ddlSubStage_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlSubStage.SelectedIndexChanged
        Try
            If ddlSubStage.SelectedIndex <> 0 Then
                Dim dtSubStages As New DataTable
                Dim objOpportunity As New MOpportunity(Session("UserContactID"))
                objOpportunity.SalesProcessId = ddlSubStage.SelectedItem.Value
                dtSubStages = objOpportunity.GetSubStageBySalesProId
                Session("SubStages") = dtSubStages
                BuildSubStages()
            End If
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

  
End Class
