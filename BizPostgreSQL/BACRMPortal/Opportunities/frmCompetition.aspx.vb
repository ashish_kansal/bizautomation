Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Opportunities
    Partial Class frmCompetition
        Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Dim lngItemCode As Long
        Protected WithEvents a As System.Web.UI.HtmlControls.HtmlAnchor

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lngItemCode = GetQueryStringVal(Request.QueryString("enc"), "ItemCode")
            If Not IsPostBack Then
                BindGrid()
            End If
            btnAdd.Attributes.Add("onclick", "return Add()")
            btnSave.Attributes.Add("onclick", "return Save()")
        End Sub

        Sub BindGrid()
            Dim objItems As New CItems
        Dim dtCompetitor As New Datatable
            objItems.DomainID = Session("DomainID")
            objItems.ItemCode = lngItemCode
            dtCompetitor = objItems.GetCompetitors
            dgCompetitor.DataSource = dtCompetitor
            dgCompetitor.DataBind()
        End Sub

        Private Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Dim objOpportunities As New COpportunities
            With objOpportunities
                .DomainID = Session("DomainID")
                .CompanyID = 0
                .CompFilter = Trim(txtSearch.Text) & "%"
            ddlCompany.DataSource = objOpportunities.ListCustomer().tables(0).DefaultView
            ddlCompany.DataTextField = "vcCompanyname"
            ddlCompany.DataValueField = "numCompanyid"
            ddlCompany.DataBind()
            ddlCompany.Items.Insert(0, "--Select One--")
            ddlCompany.Items.FindByText("--Select One--").Value = 0
        End With
    End Sub

    Private Sub ddlCompany_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Dim objOpportunities As New COpportunities
        With objOpportunities
            .DomainID = Session("DomainID")
            .CompanyID = ddlCompany.SelectedItem.Value
            .CompFilter = ""
            'ddlDivision.DataSource = objOpportunities.ListCustomer().tables(0).DefaultView
            'ddlDivision.DataTextField = "vcDivisionName"
            'ddlDivision.DataValueField = "numdivisionId"
            'ddlDivision.DataBind()
            'ddlDivision.Items.Insert(0, "--Select One--")
            'ddlDivision.Items.FindByText("--Select One--").Value = 0
        End With
    End Sub

        Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
            Dim objItems As New CItems
        objItems.DivisionID = ddlCompany.SelectedItem.Value
            objItems.DomainID = Session("DomainID")
            objItems.ItemCode = lngItemCode
            objItems.AddCompetitor()
            BindGrid()
        End Sub



        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim dtCompetitorDetails As New Datatable
        Dim i As Integer
        Dim dr As DataRow
        dtCompetitorDetails.Columns.Add("CompetitorID")
        dtCompetitorDetails.Columns.Add("Price")
        dtCompetitorDetails.Columns.Add("Date")
        Dim str As String() = txtHidden.Text.Split("~")
        Dim strValues As String()
        Dim strEditedfldlst As String

        For i = 0 To str.Length - 2
            strValues = str(i).Split(",")
            dr = dtCompetitorDetails.NewRow
            dr("CompetitorID") = strValues(0)
            dr("Price") = strValues(1)
            If strValues(2) <> "" Then
                dr("Date") = DateFromFormattedDate(strValues(2), Session("DateFormat"))
            End If
            dtCompetitorDetails.Rows.Add(dr)
        Next

        dtCompetitorDetails.tableName = "table"
            Dim ds As New DataSet
        ds.tables.Add(dtCompetitorDetails)
        strEditedfldlst = ds.GetXml()
        ds.tables.Remove(dtCompetitorDetails)

            Dim objItems As New CItems
            objItems.strFieldList = strEditedfldlst
            objItems.ItemCode = lngItemCode
            objItems.UpdateCompetitor()
        End Sub

    Function ReturnName(ByVal bintCreatedDate As Date) As String
        Dim strCreateDate As String
        strCreateDate = FormattedDateFromDate(bintCreatedDate, Session("DateFormat"))
        Return strCreateDate
    End Function

        Private Sub dgCompetitor_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgCompetitor.ItemCommand
            If e.CommandName = "Delete" Then
                Dim objItems As New CItems
                objItems.CompetitorID = e.Item.Cells(0).Text
                objItems.DomainID = Session("DomainID")
                objItems.ItemCode = lngItemCode
                If objItems.DeleteCompetitors = False Then
                    litMessage.Text = "Dependent Records Exists. Cannot Be deleted"
                Else
                    BindGrid()
                End If
            End If
        End Sub

        Private Sub dgCompetitor_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgCompetitor.ItemDataBound
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
               
                Dim btnDelete As Button
                btnDelete = e.Item.FindControl("btnDelete")
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
               
            End If
        End Sub
    End Class

