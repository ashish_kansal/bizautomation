<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmOppExpense.aspx.vb" Inherits="BACRMPortal.frmOppExpense" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Billable Expense</title>
		<script language="javascript">
			function Close()
			{
				window.close();
			}
			function Save()
			{
				if (document.Form1.txtAmount.value=="")
				{
					alert("Enter Amount")
					document.Form1.txtAmount.focus();
					return false;
				}
			}
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table width="100%" >
				<tr>
					<td class="text_bold" colSpan="2">Billable Expense</td>
					<td align="right" colSpan="2">
					    <asp:Button ID="btndelete" Runat="server" CssClass="button" Text="Delete" Width=50></asp:Button>
					    <asp:Button ID="btnSave" Runat="server" CssClass="button" Text="Save &amp; Close"></asp:Button>
						<asp:Button ID="btnCancel" Runat="server" CssClass="button" Text="Close"></asp:Button>
						<br>
						<br>
					</td>
				</tr>
				<tr>
					<td class="normal1" align="right">Amount
					</td>
					<td class="normal1"><asp:textbox id="txtAmount" Runat="server" CssClass=signup></asp:textbox></td>
					
				</tr>
				<tr>
					<td>
						<br>
					</td>
				</tr>
				<tr>
					<td class="normal1" align="right">Description
					</td>
					<td colSpan="3"><asp:TextBox TextMode="MultiLine" Runat="server" Width="400" Height="50" id="txtDesc" CssClass=signup></asp:TextBox></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
