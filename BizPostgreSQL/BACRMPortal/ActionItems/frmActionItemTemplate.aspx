<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmActionItemTemplate.aspx.vb" Inherits="BACRMPortal.frmActionItemTemplate" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head  runat="server">
       <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <script>
        
        function CheckSave()
        {
         var objName = document.getElementById( 'templateName' )   
         if( objName != null && objName.value.length <= 0 )
         {
          objName.focus();  
          alert( 'Please enter a template name')
          return false ;
         }
         
         if( CheckStatusList() == false )
         {
          return false ;
         }
         
         if( CheckStatusType() == false )
         {
            return false ;
         }
         
         if( CheckStatusActivity() == false )
         {
         return false ;
         }
        }
        
        function CheckDelete()
        {
         var objName = document.getElementById( 'templateName' )   
         if( objName != null && objName.value.length <= 0 )
         {
          alert( 'Please select a template') 
          objName.focus();
          return false ;
         }
        }
        
        function CheckStatusList()
        {
         var objList = document.getElementById( 'listStatus' )   
         if( objList != null && objList.selectedIndex <= 0 )
         {
          
          alert( 'Please select a Priority')
          objList.focus();
          return false ;
         }
        }
        
        function CheckStatusType()
        {
         var objList = document.getElementById( 'listType' )   
         if( objList != null && objList.selectedIndex <= 0 )
         {
          alert( 'Please select a Type')
          objList.focus();
          return false ;
         }
        }
        
        function CheckStatusActivity()
        {
         var objList = document.getElementById( 'listActivity' )   
         if( objList != null && objList.selectedIndex <= 0 )
         {
          alert( 'Please select a Activity')
          objList.focus();
          return false ;
         }
        }
        
        function CheckAndClosePopUp( key )
        {
         alert( key );
        }
        
    </script>
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <table class="aspTable" width="100%"><tr><td valign="top">
        <div style="width:600px;height:350px;">
		
		    <div style='width:100%;margin-top:10px;margin-bottom:10px;'></div>	
			<div style="width:127%" align=center>
			
				<div style='width:90%;' align=center>
				    <div style="clear:both;float:left;width:30%;" align=left>
				        <asp:Button ID="newTemplate" runat="server" Text="Create New Template" CssClass="button" />
				    </div>
				    <div style="float:left;width:60%;vertical-align:bottom;line-height:18px;" align="right">
				        <asp:Button ID="save" runat="server" Text="Save" CssClass="button" />
				        <asp:Button ID="saveAndClose" runat="server" Text="Save & Close" CssClass="button" />
				        <asp:Button ID="cancel" runat="server" Text="Cancel" CssClass="button" />
				        <asp:Button ID="delete" runat="server" CssClass="Delete" Text="X" />				    
				    </div>
				</div>
				
				<div style='margin-top:10px;'></div>
				
				<div style='clear:both;width:100%;'>
					<div style='float:left;width:100px;margin-right:10px;' align=right class="normal1">Template Name</div>
					<div style='float:left;'><asp:TextBox Width=180 id="templateName" cssclass="normal1" runat="server"></asp:TextBox></div>
				    <div style='float:left;margin-left:10px;margin-right:10px;' class="normal1">&nbsp;Set due date</div>
				    <div style='float:left;'><asp:TextBox cssclass="normal1" Width=30px  id="dueDays" runat="server"></asp:TextBox></div>
				    <div style='float:left;width:120px;' class="normal1">&nbsp;days from action item creation date.</div>
				</div>		
				
				<div style='margin-top:10px;'></div>
				
				<div style='clear:both;width:100%'>
					<div style='float:left;width:100px;margin-right:10px;' align=right class="normal1">Priority</div>
					<div style='float:left;'><asp:DropDownList CssClass="signup" ID="listStatus" Width="180" runat="server"></asp:DropDownList></div>
				</div>
				
				<div style='margin-top:10px;'></div>
				
				<div style='clear:both;width:100%'>
					<div style='float:left;width:100px;margin-right:10px;' align=right class="normal1">Type</div>
					<div style='float:left;'>
					    <asp:DropDownList Width="180" runat="server"  CssClass="signup" ID="listType">
					        <asp:ListItem value="-1"  Text="---Select One---" Selected=True></asp:ListItem>
					        <asp:ListItem value="1"  Text="Communication"></asp:ListItem>
					        <asp:ListItem value="3" Text="Task"></asp:ListItem>
					        <asp:ListItem value="4" Text="Notes"></asp:ListItem>
					        <asp:ListItem value="5" Text="Follow-up Anytime"></asp:ListItem>
					    </asp:DropDownList>
					</div>
				</div>
				
				<div style='margin-top:10px;'></div>
				
				<div style='clear:both;width:100%'>
					<div style='float:left;width:100px;margin-right:10px;' align=right class="normal1">Activity</div>
					<div style='float:left;'><asp:DropDownList width="180px" runat="server" CssClass="signup" ID="listActivity"></asp:DropDownList></div>
				</div>
				
				<div style='margin-top:10px;'></div>
		<asp:Panel ID="Panel1" Visible ="false" runat ="server" >
				<div>
				<div style='clear:both;width:100%'>
				<table border ="0" width ="100%">
					<tr class= "normal1" >
					
					<td  valign ="top" style="width: 104px; height: 53px" >
                        &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; 
					Email Template
					</td>
					<td  valign ="top" style="width: 94px; height: 53px" >
					 <asp:DropDownList Width="180" runat="server"  CssClass="signup" ID="ddlEmailTemplate"></asp:DropDownList>
					</td>
					<td valign ="top" style="width: 49px; height: 53px;" align="center"   >
                        &nbsp;<asp:TextBox ID='TextBox3' width="30" runat="server" cssclass="normal1" ></asp:TextBox></td>
					<td valign ="top" style="width: 138px; height: 53px;" >
						<asp:radiobuttonlist id="Radiobuttonlist1" CssClass="normal1" Runat="server" AutoPostBack="True" Width="136px" >
					    	<asp:ListItem Value="1" Selected="True">Days after due date</asp:ListItem>
							<asp:ListItem Value="2">Days before due date</asp:ListItem></asp:radiobuttonlist>
					</td>
					<td valign ="top" style="height: 53px; width: 23px;" align="left"  >
					
					<IMG src="../images/PS_icon.gif" /></td>
				<td style="width: 38px" valign ="top">
									<asp:dropdownlist id="Dropdownlist2" CssClass="signup" Runat="server" >
										<asp:ListItem Selected="False" Value="23">12:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="24">12:30</asp:ListItem>
										<asp:ListItem Selected="False" Value="1">1:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="2">1:30</asp:ListItem>
										<asp:ListItem Selected="False" Value="3">2:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="4">2:30</asp:ListItem>
										<asp:ListItem Selected="False" Value="5">3:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="6">3:30</asp:ListItem>
										<asp:ListItem Selected="False" Value="7">4:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="8">4:30</asp:ListItem>
										<asp:ListItem Selected="False" Value="9">5:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="10">5:30</asp:ListItem>
										<asp:ListItem Selected="False" Value="11">6:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="12">6:30</asp:ListItem>
										<asp:ListItem Selected="False" Value="13">7:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="14">7:30</asp:ListItem>
										<asp:ListItem Selected="true" Value="15">8:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="16">8:30</asp:ListItem>
										<asp:ListItem Selected="False" Value="17">9:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="18">9:30</asp:ListItem>
										<asp:ListItem Selected="False" Value="19">10:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="20">10:30</asp:ListItem>
										<asp:ListItem Selected="False" Value="21">11:00</asp:ListItem>
										<asp:ListItem Selected="False" Value="22">11:30</asp:ListItem>
									</asp:dropdownlist></td><td valign ="top" style="width: 171px">AM <input id="Radio1" type="radio" CHECKED value="0" name="EndAM" runat="server">
									PM <input id="rdoEmailTemplateTimePM" type="radio" value="1" name="EndAM" runat="server"></td>
					
					</tr>
					</table>	
				
				
						
									
				</div>
				</asp:Panel>
				</div> 
			
				<div style='clear:both;width:100%'>
				<div style='float:left;width:100px;margin-right:10px;' align=right class="normal1">Comments</div>
					<div style='float:left;'>
					   <asp:textbox ID='comments'   Width=360  Height="140" runat="server" cssclass="normal1" TextMode=MultiLine Wrap="true"></asp:TextBox>
					  </div>
				</div>	
				
			</div>
		
		</td></tr></table>
    </form>
</body>
</html>
