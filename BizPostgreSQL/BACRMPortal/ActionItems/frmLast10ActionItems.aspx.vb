Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Common
Partial Public Class frmLast10ActionItems : Inherits BACRMPage

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Dim dtActionItems As DataTable
            Dim objProspects As New CProspects
            With objProspects
                .ContactId = GetQueryStringVal( "CntID")
                .KeyWord = ""
                .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                .CurrentPage = 1
                .PageSize = 10
                .TotalRecords = 0
                .ByteMode = IIf(GetQueryStringVal( "Type") = 0, 0, 1)
            End With
            dtActionItems = objProspects.GetActionItems
            dgAction.DataSource = dtActionItems
            dgAction.DataBind()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Function ReturnDateTime(ByVal CloseDate) As String
        Try
            Dim strTargetResolveDate As String = ""

            strTargetResolveDate = FormattedDateTimeFromDate(CloseDate, Session("DateFormat"))
            If Format(CloseDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                strTargetResolveDate = "<font color=red>Today</font>"
            ElseIf Format(CloseDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, 1, Now()), "yyyyMMdd") Then
                strTargetResolveDate = "<font color=orange>" & strTargetResolveDate & "</font>"
            End If
            Return strTargetResolveDate
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class