<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmLast10ActionItems.aspx.vb" Inherits="BACRMPortal.frmLast10ActionItems" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head  runat="server">
       <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
		<title>Action Item</title>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<br>
			 <table class="aspTable" width="100%" class="aspTable" style="height:150px"><tr><td valign="top">
			<asp:datagrid id="dgAction" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
				BorderColor="white">
				<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
				<ItemStyle CssClass="is"></ItemStyle>
				<HeaderStyle CssClass="hs"></HeaderStyle>
				<Columns>
					<asp:TemplateColumn HeaderText="Due Date" SortExpression="CloseDate">
						<ItemTemplate>
							<%# ReturnDateTime(DataBinder.Eval(Container.DataItem, "CloseDate")) %>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:BoundColumn DataField="bitTask" HeaderText="Type"></asp:BoundColumn>
					<asp:BoundColumn DataField="textDetails" HeaderText="Description"></asp:BoundColumn>

				</Columns>
			</asp:datagrid>
			</td></tr></table>
		</form>
	</body>
</HTML>
