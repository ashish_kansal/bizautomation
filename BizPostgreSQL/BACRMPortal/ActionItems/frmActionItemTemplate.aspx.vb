Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Partial Public Class frmActionItemTemplate
    Inherits BACRMPage

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ' call once while page load
            If IsPostBack = False Then
                save.Attributes.Add("onClick", "return CheckSave();")
                saveAndClose.Attributes.Add("onClick", "return CheckSave();")
                delete.Attributes.Add("onClick", "return CheckDelete();")
                BindTemplateUIData()    ' will bind onl drop down list
            End If

            If (Not Request.QueryString.Get("ID") Is Nothing) Then If Request.QueryString.Get("ID") = -1 Then Return
            If (ViewState("FormPopulated") Is Nothing) Then ViewState("FormPopulated") = "No"
            If (ViewState("FormPopulated").ToString() = "Yes") Then Return ' do not bind form other wise we will not be able to Button_Click

            If (Not Request.QueryString.Get("Mode") Is Nothing) And (Not Request.QueryString.Get("ID") Is Nothing) Then
                Dim actionItemID As Int32
                Dim thisActionItemTable As DataTable
                actionItemID = Request.QueryString.Get("ID")
                thisActionItemTable = LoadThisActionItemData(Convert.ToInt32(actionItemID))
                BindTemplateData(thisActionItemTable)
                ViewState("FormPopulated") = "Yes"
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub BindTemplateData(ByVal thisActionItemTable As DataTable)
        Try
            templateName.Text = thisActionItemTable.Rows(0)("TemplateName")
            dueDays.Text = thisActionItemTable.Rows(0)("DueDays")
            comments.Text = thisActionItemTable.Rows(0)("Comments")
            listStatus.SelectedValue = thisActionItemTable.Rows(0)("Priority")
            listActivity.SelectedValue = thisActionItemTable.Rows(0)("Activity")
            Dim _type As String = thisActionItemTable.Rows(0)("Type")
            listType.SelectedIndex = -1
            listType.Items.FindByText(_type.Trim()).Selected = True
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub InsertTemplateData()
        Try
            Dim objActionItem As New ActionItem
            With objActionItem
                .TemplateName = templateName.Text
                If (Me.dueDays.Text.Trim() = "") Then
                    Me.dueDays.Text = "0"
                    .DueDays = Convert.ToInt32(Me.dueDays.Text)
                End If
                .StatusActionItem = Convert.ToInt32(Me.listStatus.SelectedItem.Value)
                .TypeActionItem = Me.listType.SelectedItem.Text
                .ActivityActionItem = Convert.ToInt32(Me.listActivity.SelectedItem.Value)
                .Comments = Me.comments.Text
            End With
            objActionItem.InsertActionTemplateData()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UpdateTemplateData()
        Try
            If (Request.QueryString.Get("ID") Is Nothing) Then Return
            Dim objActionItem As New ActionItem
            Dim actionItemID As Int32
            actionItemID = Request.QueryString.Get("ID")
            With objActionItem
                .RowID = actionItemID
                .TemplateName = templateName.Text
                If (Me.dueDays.Text.Trim() = "") Then
                    Me.dueDays.Text = "0"
                    .DueDays = Convert.ToInt32(Me.dueDays.Text)
                End If
                .DueDays = Convert.ToInt32(Me.dueDays.Text)
                .StatusActionItem = Convert.ToInt32(Me.listStatus.SelectedItem.Value)
                .TypeActionItem = Me.listType.SelectedItem.Text
                .ActivityActionItem = Convert.ToInt32(Me.listActivity.SelectedItem.Value)
                .Comments = Me.comments.Text
            End With
            objActionItem.UpdateActionTemplateData()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function LoadThisActionItemData(ByVal actionItemID As Int32) As DataTable
        Try
            Dim objActionItem As New ActionItem
            With objActionItem
                .RowID = actionItemID
            End With
            Return objActionItem.LoadThisActionItemTemplateData()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub BindTemplateUIData()
        Try
            Dim objActionItem As New ActionItem
            Dim dsActionItems As DataSet
            dsActionItems = objActionItem.GetActionTemplateUIData()
            If dsActionItems.Tables.Count >= 1 Then
                Me.listStatus.DataSource = dsActionItems.Tables(0)
                Me.listStatus.DataTextField = "vcData"
                Me.listStatus.DataValueField = "numListItemID"
                Me.listStatus.DataBind()
                Me.listStatus.Items.Insert(0, New ListItem("---Select One---", "-1"))

                Me.listActivity.DataSource = dsActionItems.Tables(1)
                Me.listActivity.DataTextField = "vcData"
                Me.listActivity.DataValueField = "numListItemID"
                Me.listActivity.DataBind()
                Me.listActivity.Items.Insert(0, New ListItem("---Select One---", "-1"))
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SaveAndClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveAndClose.Click
        Try
            'Dim responsejavaScriptSaved As String = "<script>alert('Item saved successfully')</script>"
            'Dim responsejavaScriptUpdated As String = "<script>alert('Item Updated successfully')</script>"

            Dim responsejavaScript As String = _
                "self.opener.location = self.opener.location + '&doPostback=yes';" & _
                "self.close();"

            '' check whether we need to insert new values based on Qstring param
            If (Not Request.QueryString.Get("ID") Is Nothing And Request.QueryString.Get("ID") = -1) Then

                If templateName.Text.Trim() = "" Then   ' template name should be there
                    Return
                End If

                InsertTemplateData() ' this will insert data into tlActionItemData 
                'Page.RegisterStartupScript("Page_Saved", responsejavaScriptSaved)
                Page.RegisterStartupScript("Page_Closed", "<Script>" & responsejavaScript & "</Script>")
                'Response.Write()
                Return
            End If

            ' case insert new template
            If (Request.QueryString.Get("Mode") Is Nothing) And (Not Request.QueryString.Get("ID") Is Nothing) Then
                InsertTemplateData()
                'Page.RegisterStartupScript("Page_Saved", responsejavaScriptSaved)
                Page.RegisterStartupScript("Page_Closed", "<Script>" & responsejavaScript & "</Script>")
                Return
            End If

            If (Not Request.QueryString.Get("Mode") Is Nothing) And (Not Request.QueryString.Get("ID") Is Nothing) Then
                UpdateTemplateData()
                'Page.RegisterStartupScript("Page_Updated", responsejavaScriptUpdated)
                Page.RegisterStartupScript("Page_Closed", "<script>" & responsejavaScript & "</script>")
                Return
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub Delete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles delete.Click
        Try
            Dim responsejavaScript As String = _
                "self.opener.location = self.opener.location + '&doPostback=yes';" & _
                "self.close();"
            'Dim responsejavaScriptDeleted As String = "alert('Item deleted successfully')"

            DeleteTemplateData()
            'Page.RegisterStartupScript("Page_Deleted", responsejavaScriptDeleted)
            Page.RegisterStartupScript("Page_Closed_Item_Deleted", "<script>" & responsejavaScript & "</script>")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub DeleteTemplateData()
        Try
            If (Request.QueryString.Get("ID") Is Nothing) Then Return

            ' we can not delete row with value -1 
            If (Not Request.QueryString.Get("ID") Is Nothing And Request.QueryString.Get("ID") = -1) Then Return

            Dim objActionItem As New ActionItem
            Dim actionItemID As Int32
            actionItemID = Request.QueryString.Get("ID")
            With objActionItem
                .RowID = actionItemID
            End With
            objActionItem.DeleteActionTemplateData() ' delete this item data
            templateName.Text = ""
            dueDays.Text = ""
            comments.Text = ""
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cancel.Click
        Try
            templateName.Text = ""
            dueDays.Text = ""
            comments.Text = ""
            Response.Redirect("../ActionItems/frmActionItemTemplate.aspx")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles save.Click
        Try

            'Dim responsejavaScriptSaved As String = "<script>alert('Item saved successfully')</script>"
            'Dim responsejavaScriptUpdated As String = "<script>alert('Item Updated successfully')</script>"

            If templateName.Text.Trim() = "" Then   ' template name should be there
                Return
            End If

            '' check whether we need to insert new values based on Qstring param
            If (Not Request.QueryString.Get("ID") Is Nothing And Request.QueryString.Get("ID") = -1) Then
                InsertTemplateData() ' this will insert data into tlActionItemData
                'Page.RegisterStartupScript("Page_Saved", responsejavaScriptSaved)
                Return
            End If

            ' case insert new template
            If (Request.QueryString.Get("Mode") Is Nothing) And (Not Request.QueryString.Get("ID") Is Nothing) Then
                InsertTemplateData()
                'Page.RegisterStartupScript("Page_Saved", responsejavaScriptSaved)
                Return
            End If

            If (Not Request.QueryString.Get("Mode") Is Nothing) And (Not Request.QueryString.Get("ID") Is Nothing) Then
                UpdateTemplateData()
                'Page.RegisterStartupScript("Page_Updated", responsejavaScriptUpdated)
            Else
                InsertTemplateData() ' this will insert data into tlActionItemData 
                'Page.RegisterStartupScript("Page_Saved", responsejavaScriptSaved)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub newTemplate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles newTemplate.Click
        Try
            Dim objActionItem As New ActionItem
            Dim max As Int32 = objActionItem.GetMaxTemplateID()
            Response.Redirect("../ActionItems/frmActionItemTemplate.aspx?ID=" & max.ToString)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class