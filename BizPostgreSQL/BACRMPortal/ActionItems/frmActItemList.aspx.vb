Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common

Partial Public Class ActionItems_frmActItemList : Inherits BACRMPage

    Dim m_aryRightsForActItem() As Integer
    Dim objcommon As New CCommon

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            m_aryRightsForActItem = clsAuthorization.fn_GetPageListUserRights(objcommon, "frmActItemList.aspx", Session("UserContactID"), 26, 1)
            If m_aryRightsForActItem(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AC")
            If Not IsPostBack Then LoadActItem()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadActItem()
        Try
            Dim objTickler As New Tickler
            Dim dtActIems As DataTable
            objTickler.UserCntID = Session("UserContactID")
            objTickler.DomainID = Session("DomainID")
            objTickler.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset") 'Added By Debasish
            objTickler.StartDate = DateAdd(DateInterval.DayOfYear, -100, Now)
            objTickler.EndDate = DateAdd(DateInterval.DayOfYear, 1, Now)
            dtActIems = objTickler.GetActionItems
            Dim dataRowItem As DataRow
            ' traverse all the rows and take appropriate values
            For Each dataRowItem In dtActIems.Rows
                If dataRowItem("Task").ToString() = "Communication" Then
                    dataRowItem("Task") = "<font color='red'><b>" & dataRowItem("Task").ToString().Substring(0, 1) & "</b></font>"
                End If
                If dataRowItem("Task").ToString() = "Task" Then
                    dataRowItem("Task") = "<font color='purple'><b>" & dataRowItem("Task").ToString().Substring(0, 1) & "</b></font>"
                End If
                If dataRowItem("Task").ToString() = "Follow-up Anytime" Then
                    dataRowItem("Task") = "<font color='#000000'><b>" & dataRowItem("Task").ToString().Substring(0, 1) & "</b></font>"
                End If
                If dataRowItem("Task").ToString() = "Unknown" Then
                    dataRowItem("Task") = "<font color='#000000'><b>" & dataRowItem("Task").ToString().Substring(0, 1) & "</b></font>"
                End If

                Dim _act As String
                Dim _Status As String
                If (Not dataRowItem("Activity") Is Nothing And dataRowItem("Activity").ToString().Length > 0) Then
                    _act = dataRowItem("Activity").ToString()
                    If (_act.Trim = "") Then
                        _act = "-"
                    End If
                Else : _act = "-"
                End If

                ' take max 12 chars from activity
                If _act <> "-" And _act.Length > 12 Then _act = _act.Substring(0, 12) & ".."

                If (Not dataRowItem("Status") Is Nothing And dataRowItem("Status").ToString().Length > 0) Then
                    _Status = dataRowItem("Status").ToString()
                    If (_Status.Trim = "") Then
                        _Status = "-"
                    ElseIf _Status.Trim().ToLower = "high" Then
                        _Status = "<font color='red'><b>" & _Status & "</b></font>"
                    ElseIf _Status.Trim().ToLower = "normal" Then
                        _Status = "<font color='#000000'>" & _Status & "</font>"
                    ElseIf _Status.Trim().ToLower = "critical" Then
                        _Status = "<font color='orange'><b>" & _Status & "</b></font>"
                    End If
                Else : _Status = "-"
                End If
                dataRowItem("Task") = dataRowItem("Task") & ", " & _act & ", " & _Status
            Next
            dgAction.DataSource = dtActIems
            dgAction.DataBind()
            Session("dtActIems") = dtActIems
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function ReturnDateTime(ByVal CloseDate) As String
        Try
            Dim strTargetResolveDate As String = ""
            Dim temp As String = ""
            If Not IsDBNull(CloseDate) Then
                strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))

                Dim timePart As String = CloseDate.ToShortTimeString.Substring(0, CloseDate.ToShortTimeString.Length - 1)
                ' remove gaps
                If timePart.Split(" ").Length >= 2 Then
                    timePart = timePart.Split(" ").GetValue(0) + timePart.Split(" ").GetValue(1)
                End If

                ' check Today date components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                ' if both are same it is today
                If (CloseDate.Date = Now.Date And CloseDate.Month = Now.Month And CloseDate.Year = Now.Year) Then
                    strTargetResolveDate = "<font color=red><b>Today</b>" & ", " + timePart & "</font>"
                    Return strTargetResolveDate

                    ' check [ TodayDate + 1 ] Date.... components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                    ' if both are same it was Yesterday
                ElseIf (CloseDate.Date.AddDays(1).Date = Now.Date And CloseDate.AddDays(1).Month = Now.Month And CloseDate.AddDays(1).Year = Now.Year) Then
                    strTargetResolveDate = "<font color=purple><b>Yesterday</b>" & ", " + timePart & "</font>"
                    Return strTargetResolveDate

                    ' check TodayDate .... components [ Date , Month , Year ] with Parameter [ CloseDate + 1 ] Date....  components [ Date , Month , Year ] 
                    ' if both are same it will Tomorrow
                ElseIf (CloseDate.Date = Now.AddDays(1).Date And CloseDate.Month = Now.AddDays(1).Month And CloseDate.Year = Now.AddDays(1).Year) Then
                    temp = CloseDate.Hour.ToString + ":" + CloseDate.Minute.ToString
                    strTargetResolveDate = "<font color=orange><b>Tomorrow</b>" & ", " & timePart + "</font>"
                    Return strTargetResolveDate

                    ' display day name for next 4 days from now
                ElseIf Format(CloseDate, "yyyyMMdd") = Format(DateTime.Now.AddDays(2), "yyyyMMdd") Or Format(CloseDate, "yyyyMMdd") = Format(DateTime.Now.AddDays(3), "yyyyMMdd") Or Format(CloseDate, "yyyyMMdd") = Format(DateTime.Now.AddDays(4), "yyyyMMdd") Or Format(CloseDate, "yyyyMMdd") = Format(DateTime.Now.AddDays(5), "yyyyMMdd") Then
                    strTargetResolveDate = "<b>" & CloseDate.DayOfWeek.ToString & "</b>" & ", " & timePart
                    Return strTargetResolveDate
                Else
                    strTargetResolveDate = strTargetResolveDate & ", " + timePart
                    Return strTargetResolveDate
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub dgAction_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgAction.ItemCommand
        Try
            Dim lngCommId As Long
            Dim strEmail As String
            If Not e.CommandName = "Sort" Then lngCommId = e.Item.Cells(0).Text
            If e.CommandName = "Type" Then
                Response.Redirect("../admin/actionitemdetails.aspx?CommId=" & lngCommId & "&frm=CommList")
            ElseIf e.CommandName = "Contact" Then
                Dim objCommon As New CCommon
                objCommon.CommID = lngCommId
                objCommon.charModule = "A"
                objCommon.GetCompanySpecificValues1()
                If (Session("EnableIntMedPage") = 1) Then
                    Response.Redirect("../pagelayout/frmContact.aspx?frm=CommList&CntId=" & objCommon.ContactID)
                Else : Response.Redirect("../Contact/frmContacts.aspx?frm=CommList&CntId=" & objCommon.ContactID)
                End If
            ElseIf e.CommandName = "Company" Then
                Dim objCommon As New CCommon
                objCommon.CommID = lngCommId
                objCommon.charModule = "A"
                objCommon.GetCompanySpecificValues1()
                If objCommon.CRMType = 0 Then
                    If (Session("EnableIntMedPage") = 1) Then
                        Response.Redirect("../pagelayout/frmLeadDtl.aspx?frm=CommList&DivID=" & objCommon.DivisionID)
                    Else : Response.Redirect("../Leads/frmLeads.aspx?frm=CommList&DivID=" & objCommon.DivisionID)
                    End If
                ElseIf objCommon.CRMType = 1 Then
                    If (Session("EnableIntMedPage") = 1) Then
                        Response.Redirect("../pagelayout/frmProspectDtl.aspx?frm=CommList&DivID=" & objCommon.DivisionID)
                    Else : Response.Redirect("../Organization/frmPartnerProsDtl.aspx?frm=CommList&DivID=" & objCommon.DivisionID)
                    End If
                ElseIf objCommon.CRMType = 2 Then
                    If (Session("EnableIntMedPage") = 1) Then
                        Response.Redirect("../pagelayout/frmAccountDtl.aspx?frm=CommList&DivID=" & objCommon.DivisionID)
                    Else : Response.Redirect("../Organization/frmPartnerActDtl.aspx?frm=CommList&DivID=" & objCommon.DivisionID)
                    End If
                End If
            ElseIf e.CommandName = "Delete" Then
                Dim objActionItem As New ActionItem
                objActionItem.CommID = lngCommId
                If objActionItem.DeleteActionItem = False Then
                    litMessage.Text = "Dependent Record Exists. Cannot be Deleted."
                Else : LoadActItem()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgAction_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgAction.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                ''    Dim hplActEmail As HyperLink
                ''    hplActEmail = e.Item.FindControl("hplActEmail")
                ''    If ConfigurationManager.AppSettings("EmailLink") = 1 Then
                ''        hplActEmail.NavigateUrl = "mailto:" & hplActEmail.Text
                ''    Else
                ''        hplActEmail.NavigateUrl = "../common/callemail.aspx?Lsemail=" & hplActEmail.Text & "&ContID=" & e.Item.Cells(0).Text
                ''    End If

                If m_aryRightsForActItem(RIGHTSTYPE.DELETE) = 0 Then
                    Dim btnDeleteAction As Button
                    btnDeleteAction = e.Item.FindControl("btnDeleteAction")
                    btnDeleteAction.Visible = False
                    Dim lnkDeleteAction As LinkButton
                    lnkDeleteAction = e.Item.FindControl("lnkDeleteAction")
                    lnkDeleteAction.Visible = True
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgAction_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgAction.SortCommand
        Try
            Dim dt As DataTable
            dt = Session("dtActIems")

            Dim dv As DataView = New DataView(dt)
            If Session("Order") = "ASC" Then
                dv.Sort = e.SortExpression.ToString() & " DESC"
                Session("Order") = "DESC"
            Else
                dv.Sort = e.SortExpression.ToString() & " ASC"
                Session("Order") = "ASC"
            End If
            dgAction.DataSource = dv
            dgAction.DataBind()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class