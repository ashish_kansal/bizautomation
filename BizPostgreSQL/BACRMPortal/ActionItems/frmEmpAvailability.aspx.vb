Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Outlook
Imports BACRM.BusinessLogic.Common

Partial Class frmEmpAvailability : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtDate As System.Web.UI.WebControls.TextBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    'Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    '    Try
    '        If Not IsPostBack Then
    '            If GetQueryStringVal( "frm") = "Tickler" Then dgDaily.Columns(3).Visible = False
    '            If GetQueryStringVal( "Date") <> "" Then
    '                ViewState("Date") = DateFromFormattedDate(GetQueryStringVal( "Date"), Session("DateFormat"))
    '                cal.SelectedDate = ViewState("Date")
    '            Else
    '                ViewState("Date") = Now
    '                cal.SelectedDate = Now()
    '            End If
    '            BindGrid()
    '        End If
    '        If uwOppTab.SelectedTab = 0 Then
    '            tdDate.Visible = True
    '            tdDate1.Visible = True
    '            tdWeek.Visible = False
    '            btnGo.Style("display") = ""
    '            'If GetQueryStringVal( "Date") <> "" Then
    '            '    btnShow.Visible = True
    '            'End If
    '        Else
    '            btnShow.Visible = False
    '            tdDate.Visible = False
    '            tdDate1.Visible = False
    '            btnGo.Style("display") = "none"
    '            tdWeek.Visible = True
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Sub BindGrid()
    '    Try
    '        Dim objActItem As New ActionItem
    '        Dim dtTable As DataTable
    '        Dim dtTable1 As DataTable
    '        Dim ds As DataSet
    '        Dim strSchedule As String = ""
    '        objActItem.UserCntID = Session("UserContactID")
    '        objActItem.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
    '        objActItem.DomainID = Session("DomainID")
    '        objActItem.Type = 1
    '        If uwOppTab.SelectedTab = 0 Then
    '            objActItem.StartTime = cal.SelectedDate
    '            ds = objActItem.GetEmpAvailability
    '            Dim dr As DataRow
    '            dtTable = ds.Tables(0)
    '            dtTable1 = ds.Tables(1)
    '            dtTable1.Merge(ds.Tables(2))
    '            dtTable.Columns.Add("Comm")
    '            Dim dv As New DataView(dtTable1)
    '            Dim drv As DataRowView

    '            For Each dr In dtTable.Rows
    '                dv.RowFilter = "numContactID=" & dr("numContactID")
    '                For Each drv In dv
    '                    strSchedule = strSchedule & drv("vcData") & " - " & drv("Schedule") & ",<br>"
    '                Next

    '                strSchedule = strSchedule.TrimEnd(",<br>")
    '                dr("Comm") = strSchedule
    '                strSchedule = ""
    '            Next
    '            dgDaily.DataSource = dtTable
    '            dgDaily.DataBind()
    '        Else
    '            objActItem.StartTime = ViewState("Date")
    '            ds = objActItem.GetEmpWeeklyAvailability
    '            Dim dr As DataRow
    '            dtTable = ds.Tables(0)
    '            dtTable1 = ds.Tables(1)
    '            dtTable1.Merge(ds.Tables(2))
    '            dtTable.Columns.Add("firstday")
    '            dtTable.Columns.Add("secondday")
    '            dtTable.Columns.Add("thirdday")
    '            dtTable.Columns.Add("fourthday")
    '            dtTable.Columns.Add("fifthday")
    '            dtTable.Columns.Add("sixthday")
    '            dtTable.Columns.Add("seventhday")
    '            Dim dv As New DataView(dtTable1)
    '            Dim drv As DataRowView
    '            For Each dr In dtTable.Rows
    '                Dim strDate As Date = ViewState("Date")
    '                dv.RowFilter = "numContactID=" & dr("numContactID") & " and dtStartTime >= '" & strDate & "' and dtEndTime <='" & DateAdd(DateInterval.Day, 1, strDate) & "'"
    '                For Each drv In dv
    '                    strSchedule = strSchedule & drv("vcData") & " - " & drv("Schedule") & ",<br>"
    '                Next
    '                strSchedule = strSchedule.TrimEnd(",<br>")
    '                dr("firstday") = strSchedule
    '                strSchedule = ""

    '                dv.RowFilter = "numContactID=" & dr("numContactID") & " and dtStartTime >= '" & DateAdd(DateInterval.Day, 1, strDate) & "' and dtEndTime <='" & DateAdd(DateInterval.Day, 2, strDate) & "'"
    '                For Each drv In dv
    '                    strSchedule = strSchedule & drv("vcData") & " - " & drv("Schedule") & ",<br>"
    '                Next
    '                strSchedule = strSchedule.TrimEnd(",<br>")
    '                dr("secondday") = strSchedule
    '                strSchedule = ""

    '                dv.RowFilter = "numContactID=" & dr("numContactID") & " and dtStartTime >= '" & DateAdd(DateInterval.Day, 2, strDate) & "' and dtEndTime <='" & DateAdd(DateInterval.Day, 3, strDate) & "'"
    '                For Each drv In dv
    '                    strSchedule = strSchedule & drv("vcData") & " - " & drv("Schedule") & ",<br>"
    '                Next
    '                strSchedule = strSchedule.TrimEnd(",<br>")
    '                dr("thirdday") = strSchedule
    '                strSchedule = ""

    '                dv.RowFilter = "numContactID=" & dr("numContactID") & " and dtStartTime >= '" & DateAdd(DateInterval.Day, 3, strDate) & "' and dtEndTime <='" & DateAdd(DateInterval.Day, 4, strDate) & "'"
    '                For Each drv In dv
    '                    strSchedule = strSchedule & drv("vcData") & " - " & drv("Schedule") & ",<br>"
    '                Next
    '                strSchedule = strSchedule.TrimEnd(",<br>")
    '                dr("fourthday") = strSchedule
    '                strSchedule = ""

    '                dv.RowFilter = "numContactID=" & dr("numContactID") & " and dtStartTime >= '" & DateAdd(DateInterval.Day, 4, strDate) & "' and dtEndTime <='" & DateAdd(DateInterval.Day, 5, strDate) & "'"
    '                For Each drv In dv
    '                    strSchedule = strSchedule & drv("vcData") & " - " & drv("Schedule") & ",<br>"
    '                Next
    '                strSchedule = strSchedule.TrimEnd(",<br>")
    '                dr("fifthday") = strSchedule
    '                strSchedule = ""

    '                dv.RowFilter = "numContactID=" & dr("numContactID") & " and dtStartTime >= '" & DateAdd(DateInterval.Day, 5, strDate) & "' and dtEndTime <='" & DateAdd(DateInterval.Day, 6, strDate) & "'"
    '                For Each drv In dv
    '                    strSchedule = strSchedule & drv("vcData") & " - " & drv("Schedule") & ",<br>"
    '                Next
    '                strSchedule = strSchedule.TrimEnd(",<br>")
    '                dr("sixthday") = strSchedule
    '                strSchedule = ""

    '                dv.RowFilter = "numContactID=" & dr("numContactID") & " and dtStartTime >= '" & DateAdd(DateInterval.Day, 6, strDate) & "' and dtEndTime <='" & DateAdd(DateInterval.Day, 7, strDate) & "'"
    '                For Each drv In dv
    '                    strSchedule = strSchedule & drv("vcData") & " - " & drv("Schedule") & ",<br>"
    '                Next
    '                strSchedule = strSchedule.TrimEnd(",<br>")
    '                dr("seventhday") = strSchedule
    '                strSchedule = ""
    '            Next
    '            dgWeekly.DataSource = dtTable
    '            dgWeekly.DataBind()
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub lnkNextDay_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNextDay.Click
    '    Try
    '        ViewState("Date") = DateAdd(DateInterval.Day, 1, ViewState("Date"))
    '        cal.SelectedDate = ViewState("Date")
    '        BindGrid()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub lnkNextWeek_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNextWeek.Click
    '    Try
    '        ViewState("Date") = DateAdd(DateInterval.Day, 7, ViewState("Date"))
    '        BindGrid()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub lnkPrevious_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPrevious.Click
    '    Try
    '        ViewState("Date") = DateAdd(DateInterval.Day, -1, ViewState("Date"))
    '        cal.SelectedDate = ViewState("Date")
    '        BindGrid()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub lnkPreviousWeek_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPreviousWeek.Click
    '    Try
    '        ViewState("Date") = DateAdd(DateInterval.Day, -7, ViewState("Date"))
    '        BindGrid()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub dgWeekly_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgWeekly.ItemDataBound
    '    Try
    '        If e.Item.ItemType = ListItemType.Header Then
    '            e.Item.Cells(2).Text = "Mon (" & FormattedDateFromDate(ViewState("Date"), Session("DateFormat")) & ")"
    '            e.Item.Cells(3).Text = "Tue (" & FormattedDateFromDate(DateAdd(DateInterval.Day, 1, ViewState("Date")), Session("DateFormat")) & ")"
    '            e.Item.Cells(4).Text = "Wed (" & FormattedDateFromDate(DateAdd(DateInterval.Day, 2, ViewState("Date")), Session("DateFormat")) & ")"
    '            e.Item.Cells(5).Text = "Thu (" & FormattedDateFromDate(DateAdd(DateInterval.Day, 3, ViewState("Date")), Session("DateFormat")) & ")"
    '            e.Item.Cells(6).Text = "Fri (" & FormattedDateFromDate(DateAdd(DateInterval.Day, 4, ViewState("Date")), Session("DateFormat")) & ")"
    '            e.Item.Cells(7).Text = "Sat (" & FormattedDateFromDate(DateAdd(DateInterval.Day, 5, ViewState("Date")), Session("DateFormat")) & ")"
    '            e.Item.Cells(8).Text = "Sun (" & FormattedDateFromDate(DateAdd(DateInterval.Day, 6, ViewState("Date")), Session("DateFormat")) & ")"
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
    '    Try
    '        ViewState("Date") = cal.SelectedDate
    '        cal.SelectedDate = ViewState("Date")
    '        BindGrid()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub dgDaily_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgDaily.ItemDataBound
    '    Try
    '        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
    '            If GetQueryStringVal( "Date") <> "" Then
    '                Dim btn As Button
    '                btn = e.Item.FindControl("btnAssign")
    '                btn.Attributes.Add("onclick", "return Assign(" & e.Item.Cells(2).Text & ")")
    '            End If
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    ''Private Sub btnShow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnShow.Click
    ''    Dim dg As DataGrid
    ''    Dim i, k As Integer
    ''    dg = Page.FindControl("dgDaily")
    ''    Dim dateFrom, dateTo, dateFrom1, dateTo1 As Date
    ''    dateFrom = GetQueryStringVal( "Time").Split("-")(0)
    ''    dateTo = GetQueryStringVal( "Time").Split("-")(1)
    ''    For i = 0 To dg.Items.Count - 1
    ''        Dim str, strTime As String()
    ''        str = dg.Items(i).Cells(2).Text.Split(",")
    ''        If str.Length > 0 Then
    ''            For k = 0 To str.Length - 1
    ''                strTime = str(k).Split("-")
    ''                If strTime.Length = 2 Then
    ''                    strTime(0) = strTime(0).Replace("P", " P")
    ''                    strTime(0) = strTime(0).Replace("A", " A")
    ''                    strTime(1) = strTime(1).Replace("P", " P")
    ''                    strTime(1) = strTime(1).Replace("A", " A")
    ''                    dateFrom1 = strTime(0)
    ''                    dateTo1 = strTime(1)
    ''                    If dateFrom1 <= dateFrom And dateFrom <= dateTo1 Then
    ''                        dg.Items(i).Visible = False
    ''                        Exit For
    ''                    End If
    ''                    If dateFrom1 <= dateTo And dateTo <= dateTo1 Then
    ''                        dg.Items(i).Visible = False
    ''                        Exit For
    ''                    End If
    ''                End If
    ''            Next
    ''        End If
    ''        str = dg.Items(i).Cells(3).Text.Split(",")
    ''        If str.Length > 0 Then
    ''            For k = 0 To str.Length - 1
    ''                strTime = str(k).Split("-")
    ''                If strTime.Length = 2 Then
    ''                    strTime(0) = strTime(0).Replace("P", " P")
    ''                    strTime(0) = strTime(0).Replace("A", " A")
    ''                    strTime(1) = strTime(1).Replace("P", " P")
    ''                    strTime(1) = strTime(1).Replace("A", " A")
    ''                    dateFrom1 = strTime(0)
    ''                    dateTo1 = strTime(1)
    ''                    If dateFrom1 <= dateFrom And dateFrom <= dateTo1 Then
    ''                        dg.Items(i).Visible = False
    ''                        Exit For
    ''                    End If
    ''                    If dateFrom1 <= dateTo And dateTo <= dateTo1 Then
    ''                        dg.Items(i).Visible = False
    ''                        Exit For
    ''                    End If
    ''                End If
    ''            Next
    ''        End If
    ''        str = dg.Items(i).Cells(4).Text.Split(",")
    ''        If str.Length > 0 Then
    ''            For k = 0 To str.Length - 1
    ''                strTime = str(k).Split("-")
    ''                If strTime.Length = 2 Then
    ''                    strTime(0) = strTime(0).Replace("P", " P")
    ''                    strTime(0) = strTime(0).Replace("A", " A")
    ''                    strTime(1) = strTime(1).Replace("P", " P")
    ''                    strTime(1) = strTime(1).Replace("A", " A")
    ''                    dateFrom1 = strTime(0)
    ''                    dateTo1 = strTime(1)
    ''                    If dateFrom1 <= dateFrom And dateFrom <= dateTo1 Then
    ''                        dg.Items(i).Visible = False
    ''                        Exit For
    ''                    End If
    ''                    If dateFrom1 <= dateTo And dateTo <= dateTo1 Then
    ''                        dg.Items(i).Visible = False
    ''                        Exit For
    ''                    End If
    ''                End If
    ''            Next
    ''        End If
    ''    Next

    ''End Sub

    'Private Sub uwOppTab_TabClick(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebTab.WebTabEvent) Handles uwOppTab.TabClick
    '    Try
    '        Dim strNow As Date
    '        strNow = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
    '        If uwOppTab.SelectedTab = 0 Then
    '            ViewState("Date") = strNow
    '            cal.SelectedDate = strNow
    '        Else
    '            Dim k As Integer
    '            k = strNow.DayOfWeek
    '            ViewState("Date") = DateAdd(DateInterval.Day, -(k - 1), strNow)
    '        End If
    '        BindGrid()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

End Class
