<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmEmpAvailability.aspx.vb" Inherits="BACRMPortal.frmEmpAvailability"%>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" src="../include/calandar.ascx" %>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1"  runat="server">
		<title>Availablity</title>
        <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
          <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
		<script language="javascript">
		function Assign(a)
		{
		  	window.opener.AssignTo(a,document.Form1.cal_txtDate.value)
			window.close();
			return false;
		}
		</script>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
		<asp:ScriptManager runat="server" EnablePartialRendering="true" ></asp:ScriptManager>
			<br>
			<table cellSpacing="0" cellPadding="0" width="100%">
				<tr>
					<td valign="bottom">
				
					
				</td>
					<td id="tdDate" runat="server" align="right" ><BizCalendar:Calendar ID="cal" runat="server" />
					</td>
					<td>
						&nbsp;<asp:button id="btnGo" Runat="server" CssClass="button" Text="Go"></asp:button>
					</td>
					<td>
						<asp:button id="btnShow" Width="180" Runat="server" Visible=false CssClass="button" Text="Show Only Available Employees"></asp:button>
					</td>
					<td id="tdDate1" runat="server" class="normal1">
						<asp:linkbutton id="lnkPrevious" Runat="server">Previous Day</asp:linkbutton>&nbsp;&nbsp;&nbsp;
						<asp:linkbutton id="lnkNextDay" Runat="server">Next Day</asp:linkbutton>
					</td>
					<td id="tdWeek" runat="server" class="normal1">
						<asp:linkbutton id="lnkPreviousWeek" Runat="server">Previous Week</asp:linkbutton>&nbsp;&nbsp;&nbsp;<asp:linkbutton id="lnkNextWeek" Runat="server">Next Week</asp:linkbutton>
					</td>
					<td align="right">
					    <asp:Button ID="btnclose" runat="server" OnClientClick="javascript:self.close()" Text="Close" CssClass="button" />&nbsp;&nbsp;
					</td>
				</tr>
			</table>
				<igtab:ultrawebtab  AutoPostBack="true" ImageDirectory=""  id="uwOppTab" runat="server" ThreeDEffect="True" BorderStyle="Solid" Width="100%" BarHeight="0"  BorderWidth="0">
                       <DefaultTabStyle Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Arial" >
                        </DefaultTabStyle>
                        <RoundedImage LeftSideWidth="7" RightSideWidth="8" ShiftOfImages="0" SelectedImage="../images/ig_tab_winXPs3.gif" NormalImage="../images/ig_tab_winXP3.gif" HoverImage="../images/ig_tab_winXPs3.gif" FillStyle="LeftMergedWithCenter" ></RoundedImage>
                     <SelectedTabStyle Height="23px"  ForeColor="white">
                        </SelectedTabStyle>
                        <HoverTabStyle Height="23px" ForeColor="white"></HoverTabStyle>
                        <Tabs>
                        <igtab:Tab Text="&nbsp;&nbsp;Daily&nbsp;&nbsp;" >
                            <ContentTemplate>
                          
					<asp:table id="Table1" Width="100%" Runat="server" Height="350" GridLines="None" BorderColor="black" CssClass="aspTable"
						BorderWidth="1" CellSpacing="0" CellPadding="0">
						<asp:TableRow>
							<asp:TableCell VerticalAlign="Top">
								<asp:datagrid id="dgDaily" runat="server" Width="100%" BorderColor="white" AutoGenerateColumns="False"
									CssClass="dg">
									<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
									<ItemStyle CssClass="is"></ItemStyle>
									<HeaderStyle CssClass="hs"></HeaderStyle>
									<Columns>
										<asp:BoundColumn DataField="vcMailNickName" Visible="False"></asp:BoundColumn>
										<asp:BoundColumn DataField="Name" HeaderText="Employee"></asp:BoundColumn>
										<asp:BoundColumn DataField="Comm" HeaderText="Communication Scheduled"></asp:BoundColumn>
										<asp:BoundColumn DataField="Task" HeaderText="Tasks Scheduled"></asp:BoundColumn>
										<asp:BoundColumn HeaderText="Outlook Calendar Schedule"></asp:BoundColumn>
										<asp:BoundColumn DataField="numContactID" Visible="False"></asp:BoundColumn>
										<asp:TemplateColumn>
											<ItemTemplate>
												<asp:Button ID="btnAssign" Runat="server" Text="Assign" CssClass="button"></asp:Button>
											</ItemTemplate>
										</asp:TemplateColumn>
									</Columns>
								</asp:datagrid>
							</asp:TableCell>
						</asp:TableRow>
					</asp:table>
			  </ContentTemplate>
                         </igtab:Tab>
                         
                          <igtab:Tab Text="&nbsp;&nbsp;Weekly&nbsp;&nbsp;" >
                            <ContentTemplate>
                           
					<asp:table id="Table2" Width="100%" Runat="server" Height="350" GridLines="None" BorderColor="black" CssClass="aspTable"
						BorderWidth="1" CellSpacing="0" CellPadding="0">
						<asp:TableRow>
							<asp:TableCell VerticalAlign="Top">
								<asp:datagrid id="dgWeekly" runat="server" Width="100%" BorderColor="white" AutoGenerateColumns="False"
									CssClass="dg" AllowSorting="True">
									<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
									<ItemStyle CssClass="is"></ItemStyle>
									<HeaderStyle CssClass="hs"></HeaderStyle>
									<Columns>
										<asp:BoundColumn DataField="vcMailNickName" Visible="False"></asp:BoundColumn>
										<asp:BoundColumn DataField="Name" HeaderText="Employee"></asp:BoundColumn>
										<asp:BoundColumn DataField="firstday"></asp:BoundColumn>
										<asp:BoundColumn DataField="secondday"></asp:BoundColumn>
										<asp:BoundColumn DataField="thirdday"></asp:BoundColumn>
										<asp:BoundColumn DataField="fourthday"></asp:BoundColumn>
										<asp:BoundColumn DataField="fifthday"></asp:BoundColumn>
										<asp:BoundColumn DataField="sixthday"></asp:BoundColumn>
										<asp:BoundColumn DataField="seventhday"></asp:BoundColumn>
									</Columns>
								</asp:datagrid>
							</asp:TableCell>
						</asp:TableRow>
					</asp:table>
				 </ContentTemplate>
                 </igtab:Tab>
              </Tabs>
          </igtab:ultrawebtab></form>
	</body>
</HTML>
--%>