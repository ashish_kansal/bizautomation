<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmActItemList.aspx.vb" Inherits="BACRMPortal.ActionItems_frmActItemList" %>
<%@ Register TagPrefix="menu1" TagName="PartnerPoint" src="../Common/PartnerPoint.ascx" %>  
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head  runat="server">
       <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <title>Partner Point</title>
</head>
<body  style="margin:0">

    <form id="form1" runat="server" >
    <menu1:PartnerPoint id="PartnerPoint1" runat="server"></menu1:PartnerPoint>
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
   <asp:updatepanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" ><ContentTemplate>
    <asp:table id="tblAction" cellpadding="0" cellspacing="0" BorderWidth="1" Runat="server" Width="100%"
										BorderColor="black" GridLines="None" Height="300" CssClass="aspTable">
										<asp:tableRow>
											<asp:tableCell VerticalAlign="Top">
												<asp:datagrid id="dgAction" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
													BorderColor="white" AllowSorting="True">
													<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
													<ItemStyle CssClass="is"></ItemStyle>
													<HeaderStyle CssClass="hs"></HeaderStyle>
													<Columns>
														<asp:BoundColumn Visible="False" DataField="ID"></asp:BoundColumn>
														<asp:BoundColumn DataField="numCreatedBy" Visible="false"></asp:BoundColumn>
														<asp:BoundColumn Visible="False" DataField="itemDesc"></asp:BoundColumn>
														<asp:TemplateColumn HeaderText="<font color=white>Due Date</font>" SortExpression="CloseDate">
															<ItemTemplate>
																<%# ReturnDateTime(DataBinder.Eval(Container.DataItem, "CloseDate")) %>
															</ItemTemplate>
														</asp:TemplateColumn>
														<asp:ButtonColumn DataTextField="Task" SortExpression="Task" HeaderText="<font color=white>Type, Activity, Priority</font>"
															CommandName="Type"></asp:ButtonColumn>
														<asp:ButtonColumn DataTextField="Name" SortExpression="Name" HeaderText="<font color=white>Name</font>"
															CommandName="Contact"></asp:ButtonColumn>
																<asp:BoundColumn   DataField="Phone"  HeaderText="<font color=white>Phone - Ext</font>" SortExpression="Phone" ></asp:BoundColumn>
														<asp:ButtonColumn DataTextField="vcCompanyName" SortExpression="vcCompanyName" HeaderText="<font color=white>Customer</font>"
															CommandName="Company"></asp:ButtonColumn>
														<%--<asp:TemplateColumn SortExpression="vcEmail" HeaderText="<font color=white>Email</font>">
															<ItemTemplate>
																<asp:HyperLink ID="hplActEmail" Runat="server" Target="_blank" Text ='<%# DataBinder.Eval(Container.DataItem, "vcEmail") %>'>
																</asp:HyperLink>
															</ItemTemplate> 
															
														</asp:TemplateColumn>--%>
														<asp:HyperLinkColumn DataTextField="vcEmail" SortExpression="vcEmail" DataNavigateUrlField="vcEmail" HeaderText="<font color=white>Email Address</font>" Target=_blank  DataNavigateUrlFormatString=mailto:{0}></asp:HyperLinkColumn>
														
														<asp:BoundColumn HeaderText="<font color=white>Assigned To/By</font>" DataField="AssignedTo" SortExpression="AssignedTo"></asp:BoundColumn>
														<asp:TemplateColumn>
															<ItemTemplate>    
																<asp:Button ID="btnDeleteAction" Runat="server" CssClass="Delete" Text="X" CommandName="Delete"></asp:Button>
																<asp:LinkButton ID="lnkDeleteAction" Runat="server" Visible="false">
																	<font color="#730000">*</font></asp:LinkButton>
															</ItemTemplate>
														</asp:TemplateColumn>
													</Columns>
												</asp:datagrid>
											</asp:tableCell>
										</asp:tableRow>
									</asp:table>
			<table width="100%">
				<tr>
					<td class="normal4" align="center">
						<asp:Literal ID="litMessage" Runat="server" EnableViewState="False"></asp:Literal></td>
				</tr>
			</table>
			</ContentTemplate>
	</asp:updatepanel>	
    </form>
</body>
</html>
