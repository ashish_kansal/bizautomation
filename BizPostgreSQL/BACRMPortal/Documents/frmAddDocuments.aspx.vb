Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Imports System.IO
Partial Class frmAddDocuments
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Dim strDocName As String                                        'To Store the information where the File is stored.
    Dim strFileType As String
    Dim strFileName As String
    Dim URLType As String
    Dim lngRecID As Long
    Dim lngDocID As Long
    Dim strType As String
    Dim m_aryRightsForPage() As Integer
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lngRecID = GetQueryStringVal(Request.QueryString("enc"), "RecID")
        strType = GetQueryStringVal(Request.QueryString("enc"), "Type")
        lngDocID = GetQueryStringVal(Request.QueryString("enc"), "DocID")
        If Not IsPostBack Then
            m_aryRightsForPage = clsAuthorization.fn_ExternalPageLevelRights("frmAddDocuments.aspx", Session("UserContactID"), 15, 13)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../Common/frmAuthorization.aspx")
            End If
            If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                btnSaveCLose.Visible = False
            End If
            Dim objCommon As New CCommon
            objCommon.sb_FillComboFromDBwithSel(ddlCategory, 29, Session("DomainID"))
            objCommon.sb_FillComboFromDBwithSel(ddlClass, 28, Session("DomainID"))

            LoadDocuments()
            If lngDocID > 0 Then
                getDetails()
            End If
        End If

        If radUpload.Checked = True Then
            pnlUpload.Visible = True
            pnlDocuments.Visible = False
            pnlSurMgmt.Visible = False
        End If
        If radDocumentLib.Checked = True Then
            pnlUpload.Visible = False
            pnlDocuments.Visible = True
            pnlSurMgmt.Visible = False

        End If
        If radMgmtSurvery.Checked = True Then
            pnlSurMgmt.Visible = True
            pnlUpload.Visible = False
            pnlDocuments.Visible = False
        End If
        If lngDocID = 0 Then
            btnSaveCLose.Attributes.Add("onclick", "return Save()")
        Else
            btnApprovers.Attributes.Add("onclick", "return openApp(" & lngDocID & ",'" & strType & "')")
        End If
    End Sub

    Sub getDetails()
        Dim objDocuments As New DocumentList
        Dim dttable As DataTable
        objDocuments.SpecDocID = lngDocID
        objDocuments.UserCntID = Session("UserContactID")
        objDocuments.DomainID = Session("DomainID")
        dttable = objDocuments.GetSpecdocDtls1
        If dttable.Rows.Count > 0 Then
            hplDoc.NavigateUrl = dttable.Rows(0).Item("vcFileName")
            hplDoc.Visible = True
            txtDocName.Text = dttable.Rows(0).Item("vcDocName")
            If Not ddlCategory.Items.FindByValue(dttable.Rows(0).Item("numDocCategory")) Is Nothing Then
                ddlCategory.Items.FindByValue(dttable.Rows(0).Item("numDocCategory")).Selected = True
            End If
            If Not ddlClass.Items.FindByValue(dttable.Rows(0).Item("numBusClass")) Is Nothing Then
                ddlClass.Items.FindByValue(dttable.Rows(0).Item("numBusClass")).Selected = True
            End If
            If dttable.Rows(0).Item("AppReq") = 1 Then
                pnlApprove.Visible = True
            Else
                pnlApprove.Visible = False
            End If
            txtdesc.Text = dttable.Rows(0).Item("vcDocDesc")
            txtRecOwner.Text = dttable.Rows(0).Item("numCreatedBy")
            txtHidden.Text = dttable.Rows(0).Item("vcFileName") & "~" & dttable.Rows(0).Item("vcFileType") & "~" & dttable.Rows(0).Item("cUrlType")
        End If
    End Sub

    Sub LoadDocuments()
        Dim objDocuments As New DocumentList
        Dim dtDocuments As New Datatable
        dtDocuments = objDocuments.GetGenericDocList
        ddldocuments.DataSource = dtDocuments
        ddldocuments.DataTextField = "vcdocname"
        ddldocuments.DataValueField = "numGenericDocid"
        ddldocuments.DataBind()
        ddldocuments.Items.Insert(0, "--Select One--")
        ddldocuments.Items.FindByText("--Select One--").Value = 0
    End Sub

    Sub UploadFile()


        Dim strFName As String()
        Dim strFilePath As String
        Try
            If (fileupload.PostedFile.ContentLength > 0) Then
                strFileName = Path.GetFileName(fileupload.PostedFile.FileName)      'Getting the File Name
                If Directory.Exists(ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs") = False Then ' If Folder Does not exists create New Folder.
                    Directory.CreateDirectory(ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs")
                End If
                strFilePath = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs" & "\" & strFileName
                strFName = Split(strFileName, ".")
                strFileType = strFName(strFName.Length - 1)                   'Getting the Extension of the File
                strDocName = strFName(0)                  'Getting the Name of the File without Extension
                fileupload.PostedFile.SaveAs(strFilePath)
                strFileName = "../documents/docs/" & strFileName
                URLType = "L"
            Else
                strFileName = txtPath.Text    'Getting the File Name
                URLType = "U"
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSaveCLose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveCLose.Click
        If radUpload.Checked = True Then
            If fileupload.PostedFile.ContentLength = 0 And txtPath.Text = "" Then
                strFileName = txtHidden.Text.Split("~")(0)
                URLType = txtHidden.Text.Split("~")(2)
                strFileType = Replace(txtHidden.Text.Split("~")(1), ".", "")
            Else
                UploadFile()
            End If

            Dim arrOutPut As String()
            Dim objDocuments As New DocumentList
            With objDocuments
                .SpecDocID = lngDocID
                .DomainID = Session("DomainId")
                .ContactID = Session("UserContactID")
                .UrlType = URLType
                .BussClass = ddlClass.SelectedItem.Value
                .DocCategory = ddlCategory.SelectedItem.Value
                .FileType = "." & strFileType
                .DocName = txtDocName.Text
                .DocDesc = txtDesc.Text
                .FileName = strFileName
                If lngDocID = 0 Then
                    .strType = "A"
                    .RecID = Session("DivId")
                Else
                    .strType = strType
                    .RecID = lngRecID
                End If
                .byteMode = 0
                arrOutPut = .SaveSpecDocuments()
            End With
        End If
        If radDocumentLib.Checked = True Then
            Dim arrOutPut As String()
            Dim objDocuments As New DocumentList
            With objDocuments
                .SpecDocID = lngDocID
                .DomainID = Session("DomainId")
                .ContactID = Session("UserContactID")
                .GenDocID = ddldocuments.SelectedItem.Value
                If lngDocID = 0 Then
                    .strType = "A"
                    .RecID = Session("DivId")
                Else
                    .strType = strType
                    .RecID = lngRecID
                End If
                .byteMode = 1
                arrOutPut = .SaveSpecDocuments()
            End With
        End If
        Response.Redirect("../documents/frmDocumentList.aspx")
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Response.Redirect("../documents/frmDocumentList.aspx")
    End Sub

    Private Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        If lngDocID > 0 Then
            Dim objDocuments As New DocumentList
            objDocuments.GenDocID = lngDocID
            objDocuments.ContactID = Session("UserContactID")
            objDocuments.CDocType = strType
            objDocuments.byteMode = 3
            objDocuments.Comments = txtComment.Text
            objDocuments.ManageApprovers()
            pnlApprove.Visible = False
            Try
                Dim objSendMail As New clsSendEmail
                Dim objCommon As New CCommon
                objCommon.ContactID = txtRecOwner.Text
                objSendMail.SendSimpleEmail(Session("ContactName") & " just approved the document " & txtdocName.Text, "", "", Session("UserEmail"), objCommon.GetContactsEmail)
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub btnDecline_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDecline.Click
        If lngDocID > 0 Then
            Dim objDocuments As New DocumentList
            objDocuments.GenDocID = lngDocID
            objDocuments.ContactID = Session("UserContactID")
            objDocuments.CDocType = strType
            objDocuments.byteMode = 4
            objDocuments.Comments = txtComment.Text
            objDocuments.ManageApprovers()
            pnlApprove.Visible = False
            Try
                Dim objSendMail As New clsSendEmail
                Dim objCommon As New CCommon
                objCommon.ContactID = txtRecOwner.Text
                objSendMail.SendSimpleEmail(Session("ContactName") & " just declined the document " & txtdocName.Text, "", "", Session("UserEmail"), objCommon.GetContactsEmail)
            Catch ex As Exception

            End Try
        End If
    End Sub
End Class

