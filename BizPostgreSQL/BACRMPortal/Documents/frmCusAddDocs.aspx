<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCusAddDocs.aspx.vb"
    Inherits="BACRMPortal.frmCusAddDocs" %>

<%--<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../common/topbar.ascx" %>--%>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <title>View Documents</title>
    <script language="javascript" type="text/javascript">
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function OpenLink(Link) {
            window.open(Link);
            return false;
        }
        function OpenURL(Link) {
            window.open(Link);
            return false;
        }
        function Save(cint) {
            if (document.Form1.txtDocName.value == "") {
                alert("Enter Document Name")
                document.Form1.txtDocName.focus()
                return false;
            }
            if (document.Form1.ddlCategory.value == 0) {
                alert("Select Document Category")
                document.Form1.ddlCategory.focus()
                return false;
            }
            if (document.Form1.ddlClass.value == 0) {
                alert("Select Document Status")
                document.Form1.ddlClass.focus()
                return false;
            }
        }
        function openApp(a, b) {
            window.open('../Documents/frmDocApprovers.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&DocID=' + a + '&DocType=' + b + "&DocName=" + document.Form1.txtDocName.value, '', 'toolbar=no,titlebar=no,left=300,top=450,width=800,height=400,scrollbars=yes,resizable=yes')
            return false;
        }
        function MarketingDept() {
            //afdas
            if (document.getElementById('ddlCategory').value == 369) {

                document.getElementById('forMarketingDept').disabled = false


            }
            else {
                document.getElementById('forMarketingDept').disabled = true

            }
            return false
        }
        function OnClientCommandExecuting(editor, args) {
            var name = args.get_name();
            var val = args.get_value();
            if (name == "MergeField") {
                editor.pasteHtml(val);
                //Cancel the further execution of the command as such a command does not exist in the editor command list
                args.set_cancel(true);
            }
        }
    </script>
</head>
<body>
    <form id="Form1" method="post" enctype="multipart/form-data" runat="server">
    <asp:ScriptManager runat="server" ID="sc1" />
    <table cellspacing="0" cellpadding="0" width="100%" align="center">
        <tr>
            <td>
                <table id="tblMenu" bordercolor="black" cellspacing="0" cellpadding="0" width="100%"
                    border="0" runat="server">
                    <tr>
                        <td class="tr1" align="center">
                            <b>Record Owner: </b>
                            <asp:Label ID="lblRecordOwner" runat="server" ForeColor="Black"></asp:Label>
                        </td>
                        <td class="td1" width="1" height="18">
                        </td>
                        <td class="tr1" align="center">
                            <b>Created By: </b>
                            <asp:Label ID="lblCreatedBy" runat="server" ForeColor="Black"></asp:Label>
                        </td>
                        <td class="td1" width="1" height="18">
                        </td>
                        <td class="tr1" align="center">
                            <b>Last Modified By: </b>
                            <asp:Label ID="lblLastModifiedBy" runat="server" ForeColor="Black"></asp:Label>
                        </td>
                        <td class="td1" width="1" height="18">
                        </td>
                        <td class="tr1" align="center">
                            <b>Last Checked-Out By,On: </b>
                            <asp:Label ID="lblLastCheckedOutByOn" runat="server" ForeColor="Black"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td>
                        </td>
                        <td align="right">
                            <asp:Button ID="btnCheckOut" runat="server" CssClass="ybutton" Text="Checkout" Visible="false" />
                            <asp:Button ID="btnApprovers" runat="server" CssClass="button" Text="List of Approvers" />
                            <asp:Button ID="btnSaveCLose" Text="Save &amp; Close" runat="server" CssClass="button">
                            </asp:Button>
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel"></asp:Button>
                            <asp:Button ID="btnActDelete" runat="server" CssClass="Delete" Text="X" Visible="false">
                            </asp:Button>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="bottom" align="left" colspan="2">
                <table class="TabStyle">
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;Document Details&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <asp:Table ID="tblOppr" runat="server" GridLines="None" BorderColor="black" Width="100%"
                    BorderWidth="1" CssClass="aspTable">
                    <asp:TableRow>
                        <asp:TableCell>
                            <table width="100%" border="0">
                                <tr>
                                    <td rowspan="8" valign="top">
                                        <br />
                                        <img src="../images/Document-48.gif" />
                                    </td>
                                    <td class="normal1" align="right">
                                        Document Name<font color="red">*</font>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDocName" runat="server" CssClass="signup" Width="400"></asp:TextBox>
                                    </td>
                                    <td align="left" class="normal1">
                                        <asp:Label runat="server" ID="lblDownloadFile" Text="Download file : " Visible="false"></asp:Label>
                                        <asp:HyperLink ID="hplDoc" runat="server" CssClass="hyperlink" Target="_blank">
													Open Document</asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right">
                                        Document Category<font color="red">*</font>
                                    </td>
                                    <td colspan="2">
                                        <asp:DropDownList ID="ddlCategory" AutoPostBack="True" CssClass="signup" runat="server"
                                            Width="180px">
                                        </asp:DropDownList>
                                        <asp:CheckBox ID='forMarketingDept' CssClass="signup" runat="server" Text="Marketing Dept" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" valign="top" align="right">
                                        Document Status<font color="red">*</font>
                                    </td>
                                    <td valign="top" colspan="2">
                                        <asp:DropDownList ID="ddlClass" CssClass="signup" runat="server" Width="180px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr id="trSubject" runat="server">
                                    <td class="normal1" align="right">
                                        Subject(Email Templates)
                                    </td>
                                    <td valign="top" colspan="2">
                                        <asp:TextBox ID="txtSubject" runat="server" Width="400px" CssClass="signup"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right">
                                        Description
                                    </td>
                                    <td id="tdHTMLEditor" runat="server">
                                        <telerik:RadEditor ID="RadEditor1" runat="server" Height="350px" OnClientPasteHtml="OnClientPasteHtml"
                                            OnClientCommandExecuting="OnClientCommandExecuting" ToolsFile="~/Marketing/EditorTools.xml">
                                            <Content>
                                            </Content>
                                            <Tools>
                                                <telerik:EditorToolGroup>
                                                    <telerik:EditorDropDown Name="MergeField" Text="Merge Field">
                                                    </telerik:EditorDropDown>
                                                </telerik:EditorToolGroup>
                                            </Tools>
                                        </telerik:RadEditor>
                                    </td>
                                    <td id="tdSimpleEditor" runat="server">
                                        <asp:TextBox ID="txtDesc" runat="server" Width="400" TextMode="MultiLine" CssClass="signup"></asp:TextBox>
                                    </td>
                                </tr>
                                <asp:Panel runat="server" ID="pnlDocumentUpload">
                                    <tr id="trUploadFile" runat="server">
                                        <td class="normal1" align="right">
                                            Upload Document from
                                            <br>
                                            Your Local Network
                                        </td>
                                        <td colspan="2">
                                            <input class="signup" id="fileupload" type="file" name="fileupload" runat="server">
                                        </td>
                                    </tr>
                                    <tr id="trOr" runat="server">
                                        <td>
                                        </td>
                                        <td class="normal1" align="left" colspan="2">
                                            Or
                                        </td>
                                    </tr>
                                    <tr id="trURLOfDocument" runat="server">
                                        <td class="normal1" align="right">
                                            Path/URL to Document
                                        </td>
                                        <td colspan="2">
                                            <asp:TextBox ID="txtPath" runat="server" Width="400" TextMode="MultiLine" CssClass="signup"></asp:TextBox>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <table>
                                            <tr>
                                                <td class="text_bold" colspan="3" style="text-decoration: underline">
                                                    Approval Status
                                                </td>
                                            </tr>
                                            <tr class="normal1">
                                                <td>
                                                    Pending :
                                                    <asp:Label ID="lblPending" runat="server"></asp:Label>
                                                </td>
                                                <td>
                                                    Approved :
                                                    <asp:Label ID="lblApproved" runat="server"></asp:Label>
                                                </td>
                                                <td>
                                                    Declined :
                                                    <asp:Label ID="lblDeclined" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <asp:Panel ID="pnlApprove" runat="server">
                                    <tr>
                                        <td class="normal1" align="right">
                                            Comments&nbsp;&nbsp;
                                        </td>
                                        <td class="normal1" align="left">
                                            <asp:TextBox runat="server" ID="txtComment" CssClass="signup" Width="340" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="normal4" align="right">
                                            Document is Ready for Approval&nbsp;&nbsp;
                                        </td>
                                        <td class="normal4" align="left">
                                            <asp:Button ID="btnApprove" runat="server" CssClass="button" Text="Approve" />
                                            <asp:Button ID="btnDecline" runat="server" CssClass="button" Text="Decline" />
                                        </td>
                                    </tr>
                                </asp:Panel>
                            </table>
                            <br />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
    <asp:TextBox ID="txtRecOwner" runat="server" Style="display: none"></asp:TextBox>
    <asp:HiddenField ID="hdnCheckoutStatus" runat="server" />
    <asp:HiddenField ID="hdnLastCheckedOutBy" runat="server" />
    </form>
</body>
</html>
