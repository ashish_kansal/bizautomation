Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Imports System.IO
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Marketing
Partial Public Class frmCusAddDocs : Inherits BACRMPage

    Dim strDocName As String                                        'To Store the information where the File is stored.
    Dim strFileType As String
    Dim strFileName As String
    Dim URLType As String
    Dim lngRecID As Long
    Dim lngDocID As Long

    Dim strDocumentSection As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngRecID = CCommon.ToLong(GetQueryStringVal("RecID"))
            lngDocID = CCommon.ToLong(GetQueryStringVal("DocID"))
            strDocumentSection = CCommon.ToString(GetQueryStringVal("Type"))
            If Not IsPostBack Then
                Dim objCommon As New CCommon
                m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmAddDocuments.aspx", Session("UserContactID"), 15, 13)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../Common/frmAuthorization.aspx")
                If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then btnSaveCLose.Visible = False

                objCommon.sb_FillComboFromDBwithSel(ddlCategory, 29, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlClass, 28, Session("DomainID"))

                If lngDocID > 0 Then LoadInformation()

            End If
            'Set Image Manage and Template Manager
            Campaign.SetRadEditorPath(RadEditor1, CCommon.ToLong(Session("DomainID")), Page)
            If lngDocID = 0 Then
                btnSaveCLose.Attributes.Add("onclick", "return Save()")
                Session("DocID") = 0
                pnlApprove.Visible = False
                forMarketingDept.Enabled = False
                trSubject.Visible = False
                hplDoc.Visible = True
                pnlDocumentUpload.Visible = True
                tdSimpleEditor.Visible = True
                tdHTMLEditor.Visible = False
            Else : btnApprovers.Attributes.Add("onclick", "return openApp(" & lngDocID & ",'" & strDocumentSection & "')")
            End If

            ddlCategory.Attributes.Add("onchange", "return MarketingDept()")
            btnActDelete.Attributes.Add("onclick", "return DeleteRecord()")
            btnApprovers.Attributes.Add("onclick", "return openApp(" & Session("DocID") & ",'D');") '" & IIf(lngRecID > 0, strDocumentSection, "D") & "')")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub


    Sub LoadInformation()
        Try
            Dim dtDocDetails As DataTable
            Dim objDocuments As New DocumentList
            With objDocuments
                .GenDocID = lngDocID
                .UserCntID = Session("UserContactID")
                .DomainID = Session("DomainID")
                .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dtDocDetails = .GetDocByGenDocID
            End With
            txtRecOwner.Text = dtDocDetails.Rows(0).Item("numCreatedBy")
            txtDocName.Text = dtDocDetails.Rows(0).Item("vcDocName")
            If Not ddlCategory.Items.FindByValue(dtDocDetails.Rows(0).Item("numDocCategory")) Is Nothing Then
                ddlCategory.Items.FindByValue(dtDocDetails.Rows(0).Item("numDocCategory")).Selected = True
            End If
            If dtDocDetails.Rows(0).Item("isForMarketingDept") = "true" Then
                forMarketingDept.Checked = True
            Else : forMarketingDept.Checked = False
            End If
            If dtDocDetails.Rows(0).Item("cUrlType") = "L" Then
                If Not IsDBNull(dtDocDetails.Rows(0).Item("VcFileName")) Then
                    hplDoc.NavigateUrl = "frmViewAttachment.aspx?docid=" & Session("DocID")  'Attributes.Add("onclick", "return OpenLink('" & CCommon.GetDocumentPath(Session("DomainID")) & dtDocDetails.Rows(0).Item("VcFileName") & "')")
                    hplDoc.Text = CCommon.ToString(dtDocDetails.Rows(0).Item("VcFileName"))
                End If
                lblDownloadFile.Visible = True
                trUploadFile.Visible = True
                trURLOfDocument.Visible = False
            Else
                hplDoc.Attributes.Add("onclick", "return OpenURL('" & dtDocDetails.Rows(0).Item("VcFileName") & "')")
                txtPath.Text = CCommon.ToString(dtDocDetails.Rows(0).Item("VcFileName"))
                trURLOfDocument.Visible = True
                trUploadFile.Visible = False
            End If
            trOr.Visible = False
            txtSubject.Text = IIf(IsDBNull(dtDocDetails.Rows(0).Item("vcSubject")), "", dtDocDetails.Rows(0).Item("vcSubject"))

            'hplDoc.NavigateUrl = "../Documents/Docs/" & dtDocDetails.Rows(0).Item("VcFileName")
            If Not ddlClass.Items.FindByValue(dtDocDetails.Rows(0).Item("numDocStatus")) Is Nothing Then
                ddlClass.Items.FindByValue(dtDocDetails.Rows(0).Item("numDocStatus")).Selected = True
            End If
            If ddlCategory.SelectedValue = "369" Then
                RadEditor1.Content = CCommon.ToString(dtDocDetails.Rows(0).Item("vcDocdesc"))
            Else
                txtDesc.Text = CCommon.ToString(dtDocDetails.Rows(0).Item("vcDocdesc"))
            End If
            hdnCheckoutStatus.Value = dtDocDetails.Rows(0).Item("tintCheckOutStatus") ' 1-chekedout,0-checked in
            hdnLastCheckedOutBy.Value = dtDocDetails.Rows(0).Item("numLastCheckedOutBy")
            ChangeRepositaryStatus()
            ViewState("FileName") = IIf(IsDBNull(dtDocDetails.Rows(0).Item("VcFileName")), "", dtDocDetails.Rows(0).Item("VcFileName"))
            ViewState("URLType") = IIf(IsDBNull(dtDocDetails.Rows(0).Item("cUrlType")), "", dtDocDetails.Rows(0).Item("cUrlType"))
            ViewState("FileType") = IIf(IsDBNull(dtDocDetails.Rows(0).Item("vcfiletype")), "", dtDocDetails.Rows(0).Item("vcfiletype"))
            lblRecordOwner.Text = IIf(IsDBNull(dtDocDetails.Rows(0).Item("RecordOwner")), "", dtDocDetails.Rows(0).Item("RecordOwner"))
            lblCreatedBy.Text = dtDocDetails.Rows(0).Item("Created")
            lblLastModifiedBy.Text = dtDocDetails.Rows(0).Item("Modified")
            lblLastCheckedOutByOn.Text = CCommon.ToString(dtDocDetails.Rows(0).Item("CheckedOut"))
            If dtDocDetails.Rows(0).Item("AppReq") = 1 Then
                pnlApprove.Visible = True
            Else : pnlApprove.Visible = False
            End If
            lblPending.Text = dtDocDetails.Rows(0).Item("Pending")
            lblApproved.Text = dtDocDetails.Rows(0).Item("Approved")
            lblDeclined.Text = dtDocDetails.Rows(0).Item("Declined")
            If dtDocDetails.Rows(0).Item("numDocCategory") = 369 Then
                trSubject.Visible = True
                hplDoc.Visible = False
                pnlDocumentUpload.Visible = False
                tdSimpleEditor.Visible = False
                tdHTMLEditor.Visible = True
            Else
                forMarketingDept.Enabled = False
                trSubject.Visible = False
                hplDoc.Visible = True
                pnlDocumentUpload.Visible = True
                tdSimpleEditor.Visible = True
                tdHTMLEditor.Visible = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub UploadFile()
        Try
            Dim strFName As String()
            Dim strFilePath As String
            If (fileupload.PostedFile.ContentLength > 0) Then
                strFileName = Path.GetFileName(fileupload.PostedFile.FileName)      'Getting the File Name
                If Directory.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID"))) = False Then ' If Folder Does not exists create New Folder.
                    Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(Session("DomainID")))
                End If
                strFilePath = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName
                strFName = Split(strFileName, ".")
                strFileType = "." & strFName(strFName.Length - 1)                     'Getting the Extension of the File
                strDocName = strFName(0)                  'Getting the Name of the File without Extension
                fileupload.PostedFile.SaveAs(strFilePath)
                strFileName = strFileName
                URLType = "L"
            ElseIf txtPath.Text <> "" Then
                strFileName = txtPath.Text    'Getting the File Name
                URLType = "U"
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSaveCLose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveCLose.Click
        Try
            If Not fileupload.PostedFile Is Nothing Then
                If Not (fileupload.PostedFile.ContentLength = 0 And txtPath.Text = "") Then
                    UploadFile()
                Else
                    URLType = ViewState("URLType")
                    strFileName = ViewState("FileName")
                    strFileType = ViewState("FileType")
                End If
            Else
                URLType = ViewState("URLType")
                strFileName = ViewState("FileName")
                strFileType = ViewState("FileType")
            End If
            If CCommon.ToLong(Session("DomainId")) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AS")
            End If
            Dim arrOutPut As String()
            Dim objDocuments As New DocumentList
            With objDocuments
                .DomainID = Session("DomainId")
                .GenDocID = Session("DocID")
                .UserCntID = Session("UserContactID")
                .UrlType = URLType
                If ddlCategory.SelectedValue = "369" Then
                    .DocDesc = RadEditor1.Content
                Else
                    .DocDesc = txtDesc.Text
                End If

                .DocumentStatus = ddlClass.SelectedItem.Value
                .DocCategory = ddlCategory.SelectedItem.Value
                .DocumentSection = strDocumentSection
                .RecID = lngRecID
                .DocumentType = IIf(lngRecID > 0, 2, 1) '1=generic,2=specific
                If ddlCategory.SelectedValue = 369 Then
                    .FileType = ""
                    .FileName = ""
                Else
                    .FileType = strFileType
                    .FileName = IIf(URLType = "U", txtPath.Text, strFileName)
                End If
                .DocName = txtDocName.Text
                .Subject = txtSubject.Text
                If ddlCategory.SelectedValue = "369" Then
                    If forMarketingDept.Checked = True Then
                        .DocumentSection = "M"
                    End If
                End If
                arrOutPut = .SaveDocuments()
            End With
            Session("DocID") = arrOutPut(0)
            PageRedirect()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub PageRedirect()
        Try
            Dim SI1 As Long
            Dim SI2 As Long
            Dim BizDocDate As String
            Dim TabID As Long

            If Not GetQueryStringVal("SI1") Is Nothing Then
                SI1 = GetQueryStringVal("SI1")
            End If

            If Not GetQueryStringVal("SI2") Is Nothing Then
                SI2 = GetQueryStringVal("SI2")
            End If
            If Not GetQueryStringVal("BizDocDate") Is Nothing Then
                BizDocDate = GetQueryStringVal("BizDocDate")
            End If
            If Not GetQueryStringVal("TabID") Is Nothing Then
                TabID = GetQueryStringVal("TabID")
            End If

            If GetQueryStringVal("frm") = "dashboard" Then
                Response.Redirect("../CommonPages/frmDashboard.aspx")
            Else
                Response.Redirect("../CommonPages/frmDocumentList.aspx")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        PageRedirect()
    End Sub

    Private Sub ddlCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
        Try
            If ddlCategory.SelectedItem.Value = 369 Then
                trSubject.Visible = True
                hplDoc.Visible = False
            Else
                trSubject.Visible = False
                hplDoc.Visible = True
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnActDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActDelete.Click
        Try
            Dim objDocuments As New DocumentList()
            With objDocuments
                .GenDocID = Session("DocID")
                .DomainID = Session("DomainId")
                .UserCntID = Session("UserContactId")
            End With
            If objDocuments.DelDocumentsByGenDocID() = False Then
                litMessage.Text = "Dependent Records Exists.Cannot be deleted."
            Else
                PageRedirect()
                'Response.Redirect("../Documents/frmDocList.aspx")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Try
            Dim objDocuments As New DocumentList
            objDocuments.GenDocID = Session("DocID")
            objDocuments.ContactID = Session("UserContactID")
            objDocuments.CDocType = "D"
            objDocuments.byteMode = 3
            objDocuments.Comments = txtComment.Text
            objDocuments.UserCntID = Session("UserContactID")
            objDocuments.ManageApprovers()
            pnlApprove.Visible = False

            SendEmail(True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnDecline_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDecline.Click
        Try
            Dim objDocuments As New DocumentList
            objDocuments.GenDocID = Session("DocID")
            objDocuments.ContactID = Session("UserContactID")
            objDocuments.CDocType = "D"
            objDocuments.byteMode = 4
            objDocuments.Comments = txtComment.Text
            objDocuments.UserCntID = Session("UserContactID")
            objDocuments.ManageApprovers()
            pnlApprove.Visible = False

            SendEmail(False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnCheckOut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheckOut.Click
        Try
            Dim objDocuments As New DocumentList
            objDocuments.GenDocID = Session("DocID")
            objDocuments.byteMode = IIf(hdnCheckoutStatus.Value = 0, 1, 2)
            objDocuments.UserCntID = Session("UserContactID")
            objDocuments.DomainID = Session("DomainID")
            objDocuments.ManageDocumentRepositary()
            LoadInformation()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub FillCustomTags()
        Try
            Dim dtDocDetails As DataTable
            Dim objDocuments As New DocumentList()
            dtDocDetails = objDocuments.getCustomTags()

            Dim ddl As EditorDropDown = CType(RadEditor1.FindTool("MergeField"), EditorDropDown)
            ddl.Items.Clear()
            For Each row As DataRow In dtDocDetails.Rows
                ddl.Items.Add(row("vcMergeField").ToString, row("vcMergeFieldValue").ToString)
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub BindEmailTemplateMergeFields()
        Try
            Dim AlertDTLID As Long = 0
            Dim objDocument As New DocumentList
            Dim dtTable As DataTable
            objDocument.ModuleID = 0
            objDocument.byteMode = 1
            dtTable = objDocument.GetMergeFieldsByModuleID()

            Dim ddl As EditorDropDown = CType(RadEditor1.FindTool("MergeField"), EditorDropDown)
            ddl.Items.Clear()
            For Each row As DataRow In dtTable.Rows
                ddl.Items.Add(row("vcMergeField").ToString, row("vcMergeFieldValue").ToString)
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub ChangeRepositaryStatus()
        If hdnCheckoutStatus.Value = "0" Then
            btnCheckOut.Text = "CheckOut"
        ElseIf hdnCheckoutStatus.Value = "1" Then
            btnCheckOut.Text = "CheckIn"
            If Session("UserContactID") = hdnLastCheckedOutBy.Value Then
                btnSaveCLose.Visible = True
            Else
                btnSaveCLose.Visible = False
                btnCheckOut.Visible = False
            End If
        End If
    End Sub
    Private Sub SendEmail(ByVal boolApproved As Boolean)
        Try
            Dim objSendEmail As New Email

            objSendEmail.DomainID = Session("DomainID")
            objSendEmail.TemplateCode = "#SYS#DOC_APPROVAL/DECLINED_NOTIFICATION"
            objSendEmail.ModuleID = 1
            objSendEmail.RecordIds = txtRecOwner.Text
            objSendEmail.AdditionalMergeFields.Add("DocumentApprovalStatus", IIf(boolApproved, "Approved", "Declined"))
            objSendEmail.AdditionalMergeFields.Add("DocumentApprovalComment", txtComment.Text.Trim)
            objSendEmail.AdditionalMergeFields.Add("DocumentName", txtDocName.Text.Trim)
            objSendEmail.AdditionalMergeFields.Add("LoggedInUser", Session("ContactName"))
            objSendEmail.FromEmail = Session("UserEmail")
            objSendEmail.ToEmail = "##ContactFirstName## <##ContactEmail##>"
            objSendEmail.SendEmailTemplate()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class