'Created by Siva
Imports System.Xml
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common

Partial Public Class frmSpecDocuments : Inherits BACRMPage

    Dim objDocuments As DocumentList

#Region "Variables"
    Dim lngRecID As Long
    Dim strType As String
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            lngRecID = GetQueryStringVal( "RecID")
            If Not GetQueryStringVal( "yunWE") Is Nothing Then lngRecID = GetQueryStringVal( "yunWE")

            strType = GetQueryStringVal( "Type")
            If Not IsPostBack Then
                Session("Help") = "Documents"
                Dim objCommon As New CCommon
                objCommon.sb_FillComboFromDBwithSel(ddlCategory, 29, Session("DomainID"))
                bindGrid()
            End If
            btnClose.Attributes.Add("onclick", "return Close()")
            hplNew.NavigateUrl = "../documents/frmAddSpecificDocuments.aspx?Type=" & strType & "&RecID=" & lngRecID
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
        Try
            bindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub bindGrid()
        Try
            If objDocuments Is Nothing Then objDocuments = New DocumentList
            objDocuments.RecID = lngRecID
            objDocuments.strType = strType
            objDocuments.DocCategory = ddlCategory.SelectedItem.Value
            objDocuments.DomainID = Session("DomainID")
            objDocuments.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            dgDocs.DataSource = objDocuments.GetSpecificDocuments1
            dgDocs.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgDocs_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgDocs.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As Button
                Dim hplFileName As HyperLink
                Dim hpl As HyperLink
                Dim hplEdit As HyperLink
                hplEdit = e.Item.FindControl("hplEdit")
                hpl = e.Item.FindControl("hplLink")
                hplFileName = e.Item.FindControl("hplFileName")
                btnDelete = e.Item.FindControl("btnDelete")
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                If e.Item.Cells(1).Text.Trim = ".xml" Then
                    Dim strUrl As String
                    If strType = "C" Then
                        strUrl = "../Documents/frmMapSpecDocConFields.aspx?SpecID=" & e.Item.Cells(0).Text.Trim & "&Filetype=" & e.Item.Cells(1).Text.Trim & "&FileName=" & Replace(e.Item.Cells(2).Text, "..", Session("SiteType") & "//" & Request.ServerVariables("SERVER_NAME") & "/" & ConfigurationManager.AppSettings("PortalVirtualDirectoryName"))
                    Else : strUrl = "../Documents/frmMapSpecAccFields.aspx?SpecID=" & e.Item.Cells(0).Text.Trim & "&Filetype=" & e.Item.Cells(1).Text.Trim & "&FileName=" & Replace(e.Item.Cells(2).Text, "..", Session("SiteType") & "//" & Request.ServerVariables("SERVER_NAME") & "/" & ConfigurationManager.AppSettings("PortalVirtualDirectoryName"))
                    End If
                    hplFileName.Attributes.Add("onclick", "return openFile('" & strUrl & "')")
                    If strType = "O" Then hplFileName.Visible = False
                Else : hplFileName.Visible = False
                End If
                hpl.NavigateUrl = Replace(e.Item.Cells(2).Text, "..", "../../" & "/" & ConfigurationManager.AppSettings("PortalVirtualDirectoryName"))
                hplEdit.NavigateUrl = "../documents/frmAddSpecificDocuments.aspx?Type=" & strType & "&RecID=" & lngRecID & "&DocID=" & e.Item.Cells(0).Text
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgDocs_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgDocs.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                objDocuments = New DocumentList
                With objDocuments
                    .SpecDocID = e.Item.Cells(0).Text()
                    .DelSpecDocuments()
                End With
                bindGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class