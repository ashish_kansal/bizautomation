<%@ Page Language="vb" EnableEventValidation="false"  AutoEventWireup="false" CodeBehind="frmDocApprovers.aspx.vb" Inherits="BACRMPortal.frmDocApprovers" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <title>Document Workflow</title>
</head>
<body>
    <form id="form1" runat="server">
                      <br />
								<asp:datagrid id="dgApprovers" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
							BorderColor="white">
							<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							<ItemStyle CssClass="is"></ItemStyle>
							<HeaderStyle CssClass="hs"></HeaderStyle>
							<Columns>
								<asp:BoundColumn Visible="False" DataField="numDocID" ></asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="numContactID" ></asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="cDocType" ></asp:BoundColumn>
								<asp:BoundColumn DataField="Name" HeaderText="Name"></asp:BoundColumn>
								<asp:BoundColumn DataField="vcCompanyName" HeaderText="Company"></asp:BoundColumn>
								<asp:BoundColumn DataField="Status" HeaderText="Status"></asp:BoundColumn>
								<asp:BoundColumn DataField="vcComment" HeaderText="Comments" ></asp:BoundColumn>
								<asp:TemplateColumn HeaderText="<font color=white>On</font>">
															<ItemTemplate>
																<%#ReturnDateTime(DataBinder.Eval(Container.DataItem, "dtApprovedOn"))%>
															</ItemTemplate>
														</asp:TemplateColumn>
								<%--<asp:TemplateColumn>
							
									<ItemTemplate>
										<asp:Button ID="btnDelete" Runat="server" CssClass="Delete" Text="X" CommandName="Delete"></asp:Button>
									</ItemTemplate>
								</asp:TemplateColumn>--%>
							</Columns>
						</asp:datagrid>
    </form>
</body>
</html>
