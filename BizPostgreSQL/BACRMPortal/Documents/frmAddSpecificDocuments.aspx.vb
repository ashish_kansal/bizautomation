Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Imports System.IO
Imports Aspose.Words
Imports Aspose.Pdf
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Account
Imports BACRM.BusinessLogic.Opportunities

Public Class frmAddSpecificDocuments : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Dim strDocName As String                                        'To Store the information where the File is stored.
    Dim strFileType As String
    Dim strFileName As String
    Dim URLType As String
    Dim lngRecID As Long
    Dim lngDocID As Long
    Dim strType As String

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            lngRecID = GetQueryStringVal( "yunWE") ' RecordID
            strType = GetQueryStringVal( "Type")
            lngDocID = GetQueryStringVal( "tyuTN")
            If Not IsPostBack Then
                Session("Help") = "Documents"
                Dim objCommon As New CCommon
                objCommon.sb_FillComboFromDBwithSel(ddlCategory, 29, Session("DomainID")) ''Document Category
                objCommon.sb_FillComboFromDBwithSel(ddldocumentsCtgr, 29, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlClass, 28, Session("DomainID")) ''Business Classification
                LoadDocuments()
                If lngDocID > 0 Then getDetails()
            End If

            If radUpload.Checked = True Or radUploadMrg.Checked = True Then
                pnlUpload.Visible = True
                pnlDocuments.Visible = False
            End If
            If radDocumentLib.Checked = True Then
                pnlUpload.Visible = False
                pnlDocuments.Visible = True
            End If

            If lngDocID = 0 Then
                btnSaveCLose.Attributes.Add("onclick", "return Save()")
                pnlApprove.Visible = False
            Else : btnApprovers.Attributes.Add("onclick", "return openApp(" & lngDocID & ",'" & strType & "'," & lngRecID & ")")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub getDetails()
        Try
            Dim objDocuments As New DocumentList
            Dim dtTable As DataTable
            objDocuments.SpecDocID = lngDocID
            objDocuments.UserCntID = Session("UserContactID")
            objDocuments.DomainID = Session("DomainID")
            dtTable = objDocuments.GetSpecdocDtls1
            If dtTable.Rows.Count > 0 Then
                hplDoc.NavigateUrl = Replace(dtTable.Rows(0).Item("vcFileName"), "..", Session("SiteType") & "//" & Request.ServerVariables("SERVER_NAME") & "/" & ConfigurationManager.AppSettings("PortalVirtualDirectoryName"))
                hplDoc.Visible = True
                txtDocName.Text = dtTable.Rows(0).Item("vcDocName")
                If Not ddlCategory.Items.FindByValue(dtTable.Rows(0).Item("numDocCategory")) Is Nothing Then
                    ddlCategory.Items.FindByValue(dtTable.Rows(0).Item("numDocCategory")).Selected = True
                End If
                If Not ddlClass.Items.FindByValue(dtTable.Rows(0).Item("numDocStatus")) Is Nothing Then
                    ddlClass.Items.FindByValue(dtTable.Rows(0).Item("numDocStatus")).Selected = True
                End If
                If dtTable.Rows(0).Item("AppReq") = 1 Then
                    pnlApprove.Visible = True
                Else : pnlApprove.Visible = False
                End If
                lblPending.Text = dtTable.Rows(0).Item("Pending")
                lblApproved.Text = dtTable.Rows(0).Item("Approved")
                lblDeclined.Text = dtTable.Rows(0).Item("Declined")
                txtDesc.Text = dtTable.Rows(0).Item("vcDocDesc")
                txtRecOwner.Text = dtTable.Rows(0).Item("numCreatedBy")
                txtHidden.Text = dtTable.Rows(0).Item("vcFileName") & "~" & dtTable.Rows(0).Item("vcFileType") & "~" & dtTable.Rows(0).Item("cUrlType")
                Dim strFiletype As String = dtTable.Rows(0).Item("vcFileName").ToString.Split(".")(3)
                If strFiletype = "doc" Or strFiletype = "htm" Or strFiletype = "html" Or strFiletype = "xml" Or strFiletype = "docx" Or strFiletype = "rtf" Then
                    btnSavePdf.Visible = True
                Else : btnSavePdf.Visible = False
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadDocuments()
        Try
            Dim objDocuments As New DocumentList
            Dim dtDocuments As DataTable
            objDocuments.DocCategory = ddldocumentsCtgr.SelectedValue
            objDocuments.DomainID = Session("DomainID")
            dtDocuments = objDocuments.GetGenericDocList
            ddldocuments.DataSource = dtDocuments
            ddldocuments.DataTextField = "vcdocname"
            ddldocuments.DataValueField = "numGenericDocid"
            ddldocuments.DataBind()
            ddldocuments.Items.Insert(0, "--Select One--")
            ddldocuments.Items.FindByText("--Select One--").Value = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub UploadFile()
        Dim strFName As String()
        Dim strFilePath As String
        Try
            If (fileupload.PostedFile.ContentLength > 0) Then
                strFileName = Path.GetFileName(fileupload.PostedFile.FileName)      'Getting the File Name
                If Directory.Exists(ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs") = False Then ' If Folder Does not exists create New Folder.
                    Directory.CreateDirectory(ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs")
                End If
                strFilePath = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs" & "\" & strFileName
                strFName = Split(strFileName, ".")
                strFileType = strFName(strFName.Length - 1)                   'Getting the Extension of the File
                strDocName = strFName(0)                  'Getting the Name of the File without Extension
                fileupload.PostedFile.SaveAs(strFilePath)
                strFileName = "../documents/docs/" & strFileName
                URLType = "L"
                If radUploadMrg.Checked = True Then

                    Dim dtTable As DataTable
                    If (strType = "C") Then
                        Dim objPageLayout As New CPageLayout
                        objPageLayout.ContactID = lngRecID
                        objPageLayout.DomainID = Session("DomainID")
                        'dtTable = objPageLayout.GetContactInfoEditIP
                    ElseIf (strType = "A") Then
                        Dim objCompany As New CAccounts
                        objCompany.DivisionID = lngRecID
                        dtTable = objCompany.GetCompanyPrimaryContact
                    ElseIf (strType = "O") Then
                        Dim objOpp As New MOpportunity
                        objOpp.OpportunityId = lngRecID
                        dtTable = objOpp.getPrimaryContactInfoOpp()
                    End If

                    If dtTable.Rows.Count > 0 Then
                        Dim doc As Document = New Document(strFilePath)

                        doc.Range.Replace("<<FirstName>>", IIf(IsDBNull(dtTable.Rows(0).Item("vcFirstName")), "", _
                        dtTable.Rows(0).Item("vcFirstName")), False, False)

                        doc.Range.Replace("<<LastName>>", IIf(IsDBNull(dtTable.Rows(0).Item("vcLastName")), "", _
                        dtTable.Rows(0).Item("vcLastName")), False, False)

                        doc.Range.Replace("<<CompanyName>>", IIf(IsDBNull(dtTable.Rows(0).Item("vcCompanyName")), "", _
                        dtTable.Rows(0).Item("vcCompanyName")), False, False)

                        doc.Range.Replace("<<DOB>>", IIf(IsDBNull(dtTable.Rows(0).Item("bintDOB")), "", _
                        dtTable.Rows(0).Item("bintDOB")), False, False)

                        doc.Range.Replace("<<AsstName>>", IIf(IsDBNull(dtTable.Rows(0).Item("vcAsstName")), "", _
                        dtTable.Rows(0).Item("vcAsstName")), False, False)

                        doc.Range.Replace("<<AsstPhone>>", IIf(IsDBNull(dtTable.Rows(0).Item("numAsstPhone")), "", _
                        dtTable.Rows(0).Item("numAsstPhone")), False, False)

                        doc.Range.Replace("<<Phone>>", IIf(IsDBNull(dtTable.Rows(0).Item("numPhone")), "", _
                        dtTable.Rows(0).Item("numPhone")), False, False)

                        doc.Range.Replace("<<Comments>>", IIf(IsDBNull(dtTable.Rows(0).Item("txtNotes")), "", _
                        dtTable.Rows(0).Item("txtNotes")), False, False)

                        doc.Range.Replace("<<Address>>", IIf(IsDBNull(dtTable.Rows(0).Item("Address")), "", _
                        dtTable.Rows(0).Item("Address")), False, False)

                        doc.Range.Replace("<<Date>>", Format(Now(), Session("DateFormat")), False, False)

                        Dim strSavePath As String = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs" & "\" & strFName(0) & ".xml"

                        doc.Save(strSavePath, SaveFormat.WordML)
                        If File.Exists(strFilePath) Then File.Delete(strFilePath)
                    End If
                End If
            ElseIf txtPath.Text <> "" Then
                strFileName = txtPath.Text    'Getting the File Name
                URLType = "U"
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub FillMergeFields(ByVal strFilePath As String)
        Try
            Dim strFName As String()
            Dim Name As String()
            strFName = Split(strFilePath, ".")
            Name = Split(strFName(2), "/")
            Dim dtTable As DataTable
            If (strType = "C") Then
                Dim objPageLayout As New CPageLayout
                objPageLayout.ContactID = lngRecID
                objPageLayout.DomainID = Session("DomainID")
                'dtTable = objPageLayout.GetContactInfoEditIP
            ElseIf (strType = "A") Then
                Dim objCompany As New CAccounts
                objCompany.DivisionID = lngRecID
                dtTable = objCompany.GetCompanyPrimaryContact
            End If

            If dtTable.Rows.Count > 0 Then
                Dim doc As Document = New Document(ConfigurationManager.AppSettings("PortalLocation") & strFName(2) & "." & strFName(3))

                doc.Range.Replace("<<FirstName>>", IIf(IsDBNull(dtTable.Rows(0).Item("vcFirstName")), "", _
                dtTable.Rows(0).Item("vcFirstName")), False, False)

                doc.Range.Replace("<<LastName>>", IIf(IsDBNull(dtTable.Rows(0).Item("vcLastName")), "", _
                dtTable.Rows(0).Item("vcLastName")), False, False)

                doc.Range.Replace("<<CompanyName>>", IIf(IsDBNull(dtTable.Rows(0).Item("vcCompanyName")), "", _
                dtTable.Rows(0).Item("vcCompanyName")), False, False)

                doc.Range.Replace("<<DOB>>", IIf(IsDBNull(dtTable.Rows(0).Item("bintDOB")), "", _
                dtTable.Rows(0).Item("bintDOB")), False, False)

                doc.Range.Replace("<<AsstName>>", IIf(IsDBNull(dtTable.Rows(0).Item("vcAsstName")), "", _
                dtTable.Rows(0).Item("vcAsstName")), False, False)

                doc.Range.Replace("<<AsstPhone>>", IIf(IsDBNull(dtTable.Rows(0).Item("numAsstPhone")), "", _
                dtTable.Rows(0).Item("numAsstPhone")), False, False)

                doc.Range.Replace("<<Phone>>", IIf(IsDBNull(dtTable.Rows(0).Item("numPhone")), "", _
                dtTable.Rows(0).Item("numPhone")), False, False)

                doc.Range.Replace("<<Comments>>", IIf(IsDBNull(dtTable.Rows(0).Item("txtNotes")), "", _
                dtTable.Rows(0).Item("txtNotes")), False, False)

                doc.Range.Replace("<<Address>>", IIf(IsDBNull(dtTable.Rows(0).Item("Address")), "", _
                dtTable.Rows(0).Item("Address")), False, False)

                Dim strSavePath As String = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs" & "\" & Name(3) & ".xml"

                doc.Save(strSavePath, SaveFormat.WordML)
                'Dim docRem As Document = New Document(strFilePath)
                'docRem.Remove()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSaveCLose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveCLose.Click
        Try
            If radUpload.Checked = True Then
                If fileupload.PostedFile.ContentLength = 0 And txtPath.Text = "" Then
                    If txtHidden.Text <> "" And txtHidden.Text.Split("~").Length = 3 Then
                        strFileName = txtHidden.Text.Split("~")(0)
                        URLType = txtHidden.Text.Split("~")(2)
                        strFileType = Replace(txtHidden.Text.Split("~")(1), ".", "")
                    End If
                Else : UploadFile()
                End If

                Dim arrOutPut As String()
                Dim objDocuments As New DocumentList
                With objDocuments
                    .SpecDocID = lngDocID
                    .DomainID = Session("DomainId")
                    .ContactID = Session("UserContactID")
                    .UrlType = URLType
                    .DocumentStatus = ddlClass.SelectedItem.Value
                    .DocCategory = ddlCategory.SelectedItem.Value
                    .FileType = "." & strFileType
                    .DocName = txtDocName.Text
                    .DocDesc = txtDesc.Text
                    .FileName = strFileName
                    .RecID = lngRecID
                    .strType = strType
                    .byteMode = 0
                    arrOutPut = .SaveSpecDocuments()
                End With
            End If
            If radUploadMrg.Checked = True Then

                If fileupload.PostedFile.ContentLength = 0 And txtPath.Text = "" Then
                    strFileName = txtHidden.Text.Split("~")(0)
                    URLType = txtHidden.Text.Split("~")(2)
                    strFileType = Replace(txtHidden.Text.Split("~")(1), ".", "")
                Else : UploadFile()
                End If

                Dim arrOutPut As String()
                Dim objDocuments As New DocumentList
                With objDocuments
                    .SpecDocID = lngDocID
                    .DomainID = Session("DomainId")
                    .ContactID = Session("UserContactID")
                    .UrlType = URLType
                    .DocumentStatus = ddlClass.SelectedItem.Value
                    .DocCategory = ddlCategory.SelectedItem.Value
                    .FileType = "." & strFileType
                    .DocName = txtDocName.Text
                    .DocDesc = txtDesc.Text
                    .FileName = ".." & strFileName.Split(".")(2) & ".xml"
                    .RecID = lngRecID
                    .strType = strType
                    .byteMode = 0
                    arrOutPut = .SaveSpecDocuments()
                End With
            End If
            If radDocumentLib.Checked = True Then
                Dim objDocuments As New DocumentList
                Dim dtDocuments As DataTable
                objDocuments.GenDocID = ddldocuments.SelectedItem.Value
                objDocuments.UserCntID = 0
                objDocuments.DomainID = Session("DomainID")
                dtDocuments = objDocuments.GetDocByGenDocID
                Dim arrOutPut As String()
                If dtDocuments.Rows.Count > 0 Then

                    Dim filetypechk As String = dtDocuments.Rows(0).Item("vcFileName").ToString.Split(".")(3)
                    If filetypechk = "doc" Or filetypechk = "rtf" Or filetypechk = "htm" Or filetypechk = "html" Or filetypechk = "xml" Then

                        FillMergeFields(dtDocuments.Rows(0).Item("vcFileName"))

                        With objDocuments
                            .SpecDocID = lngDocID
                            .DomainID = Session("DomainId")
                            .ContactID = Session("UserContactID")
                            .GenDocID = ddldocuments.SelectedItem.Value
                            .UrlType = URLType
                            .DocumentStatus = IIf(IsDBNull(dtDocuments.Rows(0).Item("numDocStatus")), "-", dtDocuments.Rows(0).Item("numDocStatus"))
                            .DocCategory = IIf(IsDBNull(dtDocuments.Rows(0).Item("numDocCategory")), "-", dtDocuments.Rows(0).Item("numDocCategory"))
                            .FileType = ".doc"
                            .DocName = IIf(IsDBNull(dtDocuments.Rows(0).Item("vcDocName")), "", dtDocuments.Rows(0).Item("vcDocName"))
                            .DocDesc = IIf(IsDBNull(dtDocuments.Rows(0).Item("vcDocdesc")), "", dtDocuments.Rows(0).Item("vcDocdesc"))
                            .FileName = ".." & dtDocuments.Rows(0).Item("vcFileName").ToString.Split(".")(2) & ".xml"
                            .RecID = lngRecID
                            .strType = strType
                            .byteMode = 0
                            arrOutPut = .SaveSpecDocuments()
                        End With
                    Else
                        With objDocuments
                            .SpecDocID = lngDocID
                            .DomainID = Session("DomainId")
                            .ContactID = Session("UserContactID")
                            .GenDocID = ddldocuments.SelectedItem.Value
                            .RecID = lngRecID
                            .strType = strType
                            .byteMode = 1
                            arrOutPut = .SaveSpecDocuments()
                        End With
                    End If

                Else
                    With objDocuments
                        .SpecDocID = lngDocID
                        .DomainID = Session("DomainId")
                        .ContactID = Session("UserContactID")
                        .GenDocID = ddldocuments.SelectedItem.Value
                        .RecID = lngRecID
                        .strType = strType
                        .byteMode = 1
                        arrOutPut = .SaveSpecDocuments()
                    End With
                End If
            End If
            Response.Redirect("../documents/frmSpecDocuments.aspx?Type=" & strType & "&yunWE=" & lngRecID)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("../documents/frmSpecDocuments.aspx?Type=" & strType & "&yunWE=" & lngRecID)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Try
            If lngDocID > 0 Then
                Dim objDocuments As New DocumentList
                objDocuments.GenDocID = lngDocID
                objDocuments.ContactID = Session("UserContactID")
                objDocuments.CDocType = strType
                objDocuments.byteMode = 3
                objDocuments.Comments = txtComment.Text
                objDocuments.UserCntID = Session("UserContactID")
                objDocuments.ManageApprovers()
                pnlApprove.Visible = False
                SendEmail(True)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnDecline_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDecline.Click
        Try
            If lngDocID > 0 Then
                Dim objDocuments As New DocumentList
                objDocuments.GenDocID = lngDocID
                objDocuments.ContactID = Session("UserContactID")
                objDocuments.CDocType = strType
                objDocuments.byteMode = 4
                objDocuments.Comments = txtComment.Text
                objDocuments.UserCntID = Session("UserContactID")
                objDocuments.ManageApprovers()
                pnlApprove.Visible = False

                SendEmail(False)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSavePdf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSavePdf.Click
        Try
            If lngDocID > 0 Then
                Dim strPath As String() = txtHidden.Text.Split("~")(0).Split(".")
                Dim strFilePath As String = ConfigurationManager.AppSettings("PortalLocation") & strPath(2) & "." & strPath(3)
                Dim strFName As String = strPath(2).Split("/")(3)
                Dim dtTable As DataTable
                Dim doc As Document = New Document(strFilePath)
                Dim strSavePathTemp As String = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs" & "\" & strFName & "~.xml"
                Dim strSavePathPDF As String = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs" & "\" & strFName & ".Pdf"

                doc.Save(strSavePathTemp, SaveFormat.AsposePdf)

                Dim pdf As Aspose.Pdf.Pdf = New Aspose.Pdf.Pdf()
                pdf.BindXML(strSavePathTemp, Nothing)
                pdf.IsImagesInXmlDeleteNeeded = True
                pdf.Save(strSavePathPDF)

                If File.Exists(strSavePathTemp) Then File.Delete(strSavePathTemp)

                Dim arrOutPut As String()
                Dim objDocuments As New DocumentList
                With objDocuments
                    .SpecDocID = 0
                    .DomainID = Session("DomainId")
                    .ContactID = Session("UserContactID")
                    .UrlType = URLType
                    .DocumentStatus = ddlClass.SelectedItem.Value
                    .DocCategory = ddlCategory.SelectedItem.Value
                    .FileType = ".pdf"
                    .DocName = txtDocName.Text
                    .DocDesc = txtDesc.Text
                    .FileName = "../documents/docs/" & strFName & ".Pdf"
                    .RecID = lngRecID
                    .strType = strType
                    .byteMode = 0
                    arrOutPut = .SaveSpecDocuments()
                End With
            End If
            Response.Redirect("../documents/frmSpecDocuments.aspx?Type=" & strType & "&yunWE=" & lngRecID)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddldocumentsCtgr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddldocumentsCtgr.SelectedIndexChanged
        Try
            LoadDocuments()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub SendEmail(ByVal boolApproved As Boolean)
        Try
            Dim objSendEmail As New Email

            objSendEmail.DomainID = Session("DomainID")
            objSendEmail.TemplateCode = "#SYS#DOC_APPROVAL/DECLINED_NOTIFICATION"
            objSendEmail.ModuleID = 1
            objSendEmail.RecordIds = txtRecOwner.Text
            objSendEmail.AdditionalMergeFields.Add("DocumentApprovalStatus", IIf(boolApproved, "Approved", "Declined"))
            objSendEmail.AdditionalMergeFields.Add("DocumentApprovalComment", txtComment.Text.Trim)
            objSendEmail.AdditionalMergeFields.Add("DocumentName", txtDocName.Text.Trim)
            objSendEmail.AdditionalMergeFields.Add("LoggedInUser", Session("ContactName"))
            objSendEmail.FromEmail = Session("UserEmail")
            objSendEmail.ToEmail = "##ContactFirstName## <##ContactEmail##>"
            objSendEmail.SendEmailTemplate()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class
