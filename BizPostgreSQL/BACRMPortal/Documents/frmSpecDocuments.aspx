<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSpecDocuments.aspx.vb" Inherits="BACRMPortal.frmSpecDocuments" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
		<title>Specific Documents</title>
		<script language="javascript">
		function Close()
		{
			opener.location.reload(true); 
			window.close()
			return false;
		}
		function DeleteRecord()
		{
			if(confirm('Are you sure, you want to delete the selected record?'))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		function openFile(url)
		{
			window.open(url,'',"width=850,height=600,status=no,top=100,left=100,scrollbars=yes,resizable=yes");
			return false;
		}
		</script>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
			<br>
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td vAlign="bottom">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;Documents&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td class="normal1"><asp:hyperlink id="hplNew" CssClass="hyperlink" Runat="server">
							<font color="#180073">Add Document</font>
						</asp:hyperlink></td>
					<TD class="normal1" align="right">Filter by Document Category&nbsp;&nbsp;&nbsp;</TD>
					<td><asp:dropdownlist id="ddlCategory" AutoPostBack="True" CssClass="signup" Runat="server"></asp:dropdownlist></td>
					<td align="right"><asp:button id="btnClose" CssClass="button" Runat="server" Width="50" Text="Close"></asp:button></td>
				</tr>
			</table>
			<asp:table id="Table3" Runat="server" Height="350" GridLines="None" BorderColor="black" Width="100%" CssClass="aspTable"
				BorderWidth="1" CellSpacing="0" CellPadding="0">
				<asp:TableRow>
					<asp:TableCell VerticalAlign="Top">
						<asp:datagrid id="dgDocs" runat="server" CssClass="dg" BorderColor="white" Width="100%" AutoGenerateColumns="False">
							<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							<ItemStyle CssClass="is"></ItemStyle>
							<HeaderStyle CssClass="hs"></HeaderStyle>
							<Columns>
								<asp:BoundColumn Visible="False" DataField="numGenericDocID" HeaderText="numGenericDocID"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="vcfiletype" HeaderText="vcfiletype"></asp:BoundColumn>
								<asp:BoundColumn Visible="False" DataField="VcFileName" HeaderText="VcFileName"></asp:BoundColumn>
								<asp:TemplateColumn HeaderText ="">
									<ItemTemplate>
										<asp:HyperLink ID=hplFileName NavigateUrl="#" Runat=server CssClass=hyperlink ><font face="Wingdings 2" size=4 color=blue>$</font>
										</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText ="Document Name">
									<ItemTemplate>
									<asp:HyperLink ID="hplEdit" NavigateUrl="#" Runat=server CssClass=hyperlink Text='<%# DataBinder.Eval(Container.DataItem, "vcDocName") %>'>
										</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="Category" HeaderText="Document Category"></asp:BoundColumn>
							<asp:TemplateColumn SortExpression="vcfiletype" HeaderText="<font color=white>File Type</font>">
								<ItemTemplate>
								<asp:HyperLink ID=hplLink Target=_blank  Runat=server Text='<%# DataBinder.Eval(Container.DataItem, "vcfiletype") %>'>
								</asp:HyperLink>
								</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="BusClass" HeaderText="Business Classification"></asp:BoundColumn>
								<asp:BoundColumn DataField="Mod" HeaderText="Last Modified By, Last Modified On"></asp:BoundColumn>
								<asp:TemplateColumn>
									<ItemTemplate>
										<asp:Button ID="btnDelete" Runat="server" CssClass="Delete" Text="X" CommandName="Delete"></asp:Button>
										<asp:LinkButton ID="lnkDelete" Runat="server" Visible="false">
											<font color="#730000">*</font></asp:LinkButton>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid>
					</asp:TableCell>
				</asp:TableRow>
			</asp:table>
		</form>
	</body>
</HTML>
