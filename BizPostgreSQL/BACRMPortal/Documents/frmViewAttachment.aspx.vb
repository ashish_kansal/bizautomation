﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Documents
Public Class frmViewAttachment
    Inherits BACRMPage

    Dim lngDocumentID As Long
    Dim IsFileFound As Boolean = False
    Dim strFileName As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Try
        lngDocumentID = CCommon.ToLong(Request.QueryString("docid"))
        If CCommon.ToLong(Session("DomainID")) > 0 Then
            Dim dtDocDetails As DataTable
            Dim objDocuments As New DocumentList
            With objDocuments
                .GenDocID = lngDocumentID
                .DomainID = Session("DomainID")
                .byteMode = 2
                dtDocDetails = .GetDocByGenDocID
            End With
            If dtDocDetails.Rows.Count > 0 Then
                strFileName = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & CCommon.ToString(dtDocDetails.Rows(0).Item("VcFileName"))
                If System.IO.File.Exists(strFileName) Then
                    Response.AddHeader("Content-Type", "application/octet-stream")
                    Response.AddHeader("content-disposition", "attachment; filename=" & CCommon.ToString(dtDocDetails.Rows(0).Item("VcFileName")).Replace(" ", "_"))
                    Response.TransmitFile(strFileName)
                    Response.End()
                End If
            End If
        Else
            Server.Transfer("../admin/authentication.aspx?mesg=AC", False)
            Exit Sub
        End If
        If IsFileFound = False Then
            FileNotFound()
        End If
        'Catch ex As Exception
        '    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '    Response.Write(ex)
        'End Try
    End Sub
    Private Sub FileNotFound()
        Response.Write("<style>.normal13{    font-family: Arial;    font-size: 10pt;    font-style: normal;    font-variant: normal;    font-weight: bold;    color: red;}</style>")
        Response.Write("<div class=normal13><b>Requested file is not found</b></div>")
        If ConfigurationManager.AppSettings("IsDebugMode") = "true" Then
            Response.Write(strFileName)
        End If
        'Response.StatusCode = 404'removed as production overwrites it with 404.htm
        Response.End()
    End Sub
End Class