<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmAddSpecificDocuments.aspx.vb" Inherits="BACRMPortal.frmAddSpecificDocuments"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
        <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
		<title>Add Specific Documents</title>
		<script language="javascript">
		function Save()
		{
					
				if (document.Form1.radUpload.checked==true)
				{
					if (document.Form1.txtDocName.value=="")
					{
						alert("Enter Name")
						document.Form1.txtDocName.focus()
						return false;
					}
					if (document.Form1.ddlCategory.value==0)
					{
						alert("Select Document Category")
						document.Form1.ddlCategory.focus()
						return false;
					}
					if (document.Form1.ddlClass.value==0)
					{
						alert("Select Business Classification")
						document.Form1.ddlClass.focus()
						return false;
					}
					
				}
				if (document.Form1.radDocumentLib.checked==true)
				{
					if (document.Form1.ddldocuments.value==0)
					{
						alert("Select Document")
						document.Form1.ddldocuments.focus()
						return false;
					}
				}
				
		
			
		}
		function openApp(a,b,c)
		{
			window.open('../Documents/frmDocApprovers.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&DocID='+a+'&DocType='+b+'&RecID='+c+ "&DocName=" + document.Form1.txtDocName.value,'','toolbar=no,titlebar=no,left=300,top=450,width=800,height=400,scrollbars=yes,resizable=yes')
		    return false;
		}
		</script>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0" width="100%" align="center">
				<tr>
					<td>
						<table width="100%">
							<tr>
								<td></td>
								<TD align="right">
								    <asp:Button ID="btnSavePdf" runat="server" CssClass="button"  Visible="false" Text="Save as PDF"/>
								    <asp:Button ID="btnApprovers" runat="server" CssClass="button"  Text="List of Approvers"/>
								    <asp:button id="btnSaveCLose" text="Save &amp; Close" Runat="server" CssClass="button"></asp:button>
									<asp:button id="btnClose" Runat="server" CssClass="button" Text="Close"></asp:button></TD>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td vAlign="bottom" align="left" colSpan="2">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;Documents&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td vAlign="top"><asp:table id="tblOppr" Runat="server" GridLines="None" BorderColor="black" Width="100%" Height="300" CssClass="aspTable"
							BorderWidth="1">
							<asp:TableRow>
								<asp:TableCell VerticalAlign="Top">
									<br>
									<TABLE width="100%" border="0">
										<tr>
											<td>
											</td>
											<td class="normal1">
											    <asp:RadioButton ID="radUpload" AutoPostBack="True" Checked="True" Runat="server" cssclass="normal1"
												GroupName="rad" Text="Upload a Document"></asp:RadioButton>
												<br/>
												<asp:RadioButton ID="radUploadMrg" AutoPostBack="True"  Runat="server" cssclass="normal1"
													GroupName="rad" Text="Upload a Document With Merge Fields (from your PC or LAN) "></asp:RadioButton>
				
												<br>
												<asp:RadioButton ID="radDocumentLib" AutoPostBack="True" Runat="server" cssclass="normal1" GroupName="rad"
													Text="Select a document from library (with or without merge fields)"></asp:RadioButton>
											</td>
										</tr>
								
										<asp:Panel ID="pnlDocuments" Runat="server" Visible="false">
										    <tr>
										        <TD class="normal1" align="right">Select Category
												</TD>
												<TD>
													<asp:DropDownList id="ddldocumentsCtgr" AutoPostBack="true" CssClass="signup" Runat="server" Width="130"></asp:DropDownList></TD>
												
											</TR>
											<TR>
												    <TD class="normal1" align="right">Select a document from library
												    </TD>
												    <TD>
													<asp:DropDownList id="ddldocuments" CssClass="signup" Runat="server" Width="130"></asp:DropDownList></TD>
											</tr>
											
										</asp:Panel>
										<asp:Panel ID="pnlUpload" Runat="server" Visible="false">
											<TR>
												<TD class="normal1" align="right">Document Name<FONT color="red">*</FONT>
												</TD>
												<TD class="normal1">
													<asp:textbox id="txtDocName" Runat="server" cssclass="signup"></asp:textbox>
													&nbsp;&nbsp;&nbsp;&nbsp;
													<asp:HyperLink ID="hplDoc" Target=_blank  Runat="server" CssClass="hyperlink" Visible="False">
													Open Document</asp:HyperLink>
												</TD>
											</TR>
											<TR>
												<TD class="normal1" align="right">Document Category<FONT color="red">*</FONT></TD>
												<TD colSpan="2">
													<asp:dropdownlist id="ddlCategory" CssClass="signup" Runat="server" Width="130px"></asp:dropdownlist></TD>
											</TR>
											<TR>
												<TD class="normal1" vAlign="top" align="right">Business Classification<FONT color="red">*</FONT></TD>
												<TD vAlign="top" colSpan="2">
													<asp:dropdownlist id="ddlClass" CssClass="signup" Runat="server" Width="130px"></asp:dropdownlist></TD>
											</TR>
											<TR>
												<TD class="normal1" align="right">Description
												</TD>
												<TD colSpan="2">
													<asp:textbox id="txtDesc" runat="server" CssClass="signup" Width="304px" TextMode="MultiLine"></asp:textbox></TD>
											</TR>
											<TR>
												<TD class="normal1" align="right">Upload Document from
													<BR>
													Your Local Network
												</TD>
												<TD colSpan="2"><INPUT class="signup" id="fileupload" type="file" name="fileupload" runat="server"></TD>
											</TR>
											<TR>
												<TD></TD>
												<TD class="normal1" align="left" colSpan="2">Or</TD>
											</TR>
											<TR>
												<TD class="normal1" align="right">Path/URL to Document
												</TD>
												<TD colSpan="2">
													<asp:TextBox id="txtPath" runat="server" CssClass="signup" Width="304px" TextMode="MultiLine"></asp:TextBox></TD>
											</TR>
										</asp:Panel>
									</TABLE>
									<br/>
									 <table>
			                            <tr>
			                                <td class="text_bold" colspan="3" style="text-decoration:underline">
			                                Approval Status
			                                </td>
			                            </tr>
			                            <tr class="normal1">
			                                <td>
			                                Pending : <asp:Label ID="lblPending" runat="server"></asp:Label>
			                                </td>
			                                 <td>
			                                Approved : <asp:Label ID="lblApproved" runat="server"></asp:Label>
			                                </td>
			                                 <td>
			                                Declined : <asp:Label ID="lblDeclined" runat="server"></asp:Label>
			                                </td>
			                            </tr>
			                        </table>
									<asp:Panel ID="pnlApprove" runat="server">
									  <table width="100%">
									   <tr>
									            <td class="normal1" align="right">
									            Comments&nbsp;&nbsp; 
									            </td>
									            <td class="normal1" align="left">
									            <asp:TextBox  runat="server" ID="txtComment" CssClass="signup" Width="340"  TextMode="MultiLine"></asp:TextBox>
									            </td>
									         </tr>
				                            <tr>
					                            <td class="normal4" align="right" >
						                           Document is Ready for Approval &nbsp;&nbsp;&nbsp;
						                            </td>
									            <td class="normal1" align="left">
						                           <asp:Button ID="btnApprove" runat="server" CssClass="button"  Text="Approve"/>
						                           <asp:Button ID="btnDecline" runat="server" CssClass="button"  Text="Decline"/>
						                           </td>
				                            </tr>
			                            </table>
									</asp:Panel>
								</asp:TableCell>
							</asp:TableRow>
						</asp:table>
					</td>
				</tr>
			</table>
			<asp:textbox ID="txtHidden" Runat="server" style="DISPLAY:none"></asp:textbox>
			<asp:textbox ID="txtRecOwner" Runat="server" style="DISPLAY:none"></asp:textbox>
		</form>
	</body>
</HTML>
