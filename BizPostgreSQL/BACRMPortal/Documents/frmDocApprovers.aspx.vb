Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Partial Public Class frmDocApprovers
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindGrid()
        Try
            Dim objDocuments As New DocumentList
            objDocuments.GenDocID = GetQueryStringVal( "DocID")
            objDocuments.CDocType = GetQueryStringVal( "DocType")
            dgApprovers.DataSource = objDocuments.GetApprovers
            dgApprovers.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgApprovers_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgApprovers.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                Dim objDocuments As New DocumentList
                objDocuments.GenDocID = GetQueryStringVal( "DocID")
                objDocuments.ContactID = e.Item.Cells(1).Text
                objDocuments.CDocType = GetQueryStringVal( "DocType")
                objDocuments.byteMode = 2
                objDocuments.UserCntID = Session("UserContactID")
                objDocuments.ManageApprovers()
                BindGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Function ReturnDateTime(ByVal CloseDate) As String
        Try
            Dim strTargetResolveDate As String = ""
            Dim temp As String = ""
            If Not IsDBNull(CloseDate) Then
                strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))

                Dim timePart As String = CloseDate.ToShortTimeString.Substring(0, CloseDate.ToShortTimeString.Length - 1)
                ' remove gaps
                If timePart.Split(" ").Length >= 2 Then
                    timePart = timePart.Split(" ").GetValue(0) + timePart.Split(" ").GetValue(1)
                End If

                ' check Today date components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                ' if both are same it is today
                If (CloseDate.Date = Now.Date And CloseDate.Month = Now.Month And CloseDate.Year = Now.Year) Then
                    strTargetResolveDate = "<font color=red><b>Today</b>" & ", " + timePart & "</font>"
                    Return strTargetResolveDate

                    ' check [ TodayDate + 1 ] Date.... components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                    ' if both are same it was Yesterday
                ElseIf (CloseDate.Date.AddDays(1).Date = Now.Date And CloseDate.AddDays(1).Month = Now.Month And CloseDate.AddDays(1).Year = Now.Year) Then
                    strTargetResolveDate = "<font color=purple><b>Yesterday</b>" & ", " + timePart & "</font>"
                    Return strTargetResolveDate

                    ' check TodayDate .... components [ Date , Month , Year ] with Parameter [ CloseDate + 1 ] Date....  components [ Date , Month , Year ] 
                    ' if both are same it will Tomorrow
                ElseIf (CloseDate.Date = Now.AddDays(1).Date And CloseDate.Month = Now.AddDays(1).Month And CloseDate.Year = Now.AddDays(1).Year) Then
                    temp = CloseDate.Hour.ToString + ":" + CloseDate.Minute.ToString
                    strTargetResolveDate = "<font color=orange><b>Tomorrow</b>" & ", " & timePart + "</font>"
                    Return strTargetResolveDate

                    ' display day name for next 4 days from now
                ElseIf Format(CloseDate, "yyyyMMdd") = Format(DateTime.Now.AddDays(2), "yyyyMMdd") Or Format(CloseDate, "yyyyMMdd") = Format(DateTime.Now.AddDays(3), "yyyyMMdd") Or Format(CloseDate, "yyyyMMdd") = Format(DateTime.Now.AddDays(4), "yyyyMMdd") Or Format(CloseDate, "yyyyMMdd") = Format(DateTime.Now.AddDays(5), "yyyyMMdd") Then
                    strTargetResolveDate = "<b>" & CloseDate.DayOfWeek.ToString & "</b>" & ", " & timePart
                    Return strTargetResolveDate
                Else
                    strTargetResolveDate = strTargetResolveDate & ", " + timePart
                    Return strTargetResolveDate
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class