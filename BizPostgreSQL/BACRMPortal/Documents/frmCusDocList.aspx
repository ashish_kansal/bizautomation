<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCusDocList.aspx.vb"
    Inherits="BACRMPortal.frmCusDocList" %>

<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../common/topbar.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <title>Specific Documents</title>

    <script language="javascript">
        function OpenBizInvoice(a, b) {
            window.open('../Opportunity/frmBizInvoice.aspx?OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,titlebar=no,menubar=yes,left=100,width=720,height=700,scrollbars=yes,resizable=yes');
            return false;
        }
        function fnSortByChar(varSortChar) {
            document.Form1.txtSortChar.value = varSortChar;
            if (document.Form1.txtCurrrentPage != null) {
                document.Form1.txtCurrrentPage.value = 1;
            }
            document.Form1.btnGo.click();
        }
        function Close() {
            opener.location.reload(true);
            window.close()
            return false;
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function openFile(url) {
            window.open(url, '', "width=850,height=600,status=no,top=100,left=100,scrollbars=yes,resizable=yes");
            return false;
        }
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <menu1:webmenu ID="webmenu1" runat="server"></menu1:webmenu>
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
        EnableViewState="true">
        <ContentTemplate>
            <table align="center" border="0">
                <tr>
                    <asp:Button runat="server" ID="btnGo" Text="" Style="display: none"></asp:Button>
                    <td class="normal1" width="150">
                        No of Records:
                        <asp:Label ID="lblRecordCount" runat="server"></asp:Label>
                    </td>
                    <td class="normal1" align="right">
                        Filter by Document Section&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="ddlDocumentSection"
                            runat="server" CssClass="signup" AutoPostBack="True">
                            <asp:ListItem Value="0">--Select One--</asp:ListItem>
                            <asp:ListItem Value="A" Selected="True">Organization</asp:ListItem>
                            <asp:ListItem Value="C">Contacts</asp:ListItem>
                            <asp:ListItem Value="O">Opportunities</asp:ListItem>
                            <asp:ListItem Value="P">Projects</asp:ListItem>
                            <asp:ListItem Value="B">BizDocs</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td class="normal1" id="tdCategory" align="right" runat="server">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Filter by Document Category&nbsp;&nbsp;&nbsp;<asp:DropDownList
                            ID="ddlCategory" runat="server" CssClass="signup" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td class="normal1" id="tdBizdocs" align="right" runat="server">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BizDocs&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="ddlBizDocs"
                            runat="server" CssClass="signup" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td valign="bottom">
                        <table class="TabStyle">
                            <tr>
                                <td>
                                    &nbsp;&nbsp;&nbsp;Documents&nbsp;&nbsp;&nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="normal1">
                        <asp:HyperLink ID="hplNew" runat="server" CssClass="hyperlink">
							Add Document
                        </asp:HyperLink>
                    </td>
                    <td id="hidenav" nowrap align="right" colspan="4" runat="server">
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="lblNext" runat="server" CssClass="Text_bold">Next:</asp:Label>
                                </td>
                                <td class="normal1">
                                    <asp:LinkButton ID="lnk2" runat="server">2</asp:LinkButton>
                                </td>
                                <td class="normal1">
                                    <asp:LinkButton ID="lnk3" runat="server">3</asp:LinkButton>
                                </td>
                                <td class="normal1">
                                    <asp:LinkButton ID="lnk4" runat="server">4</asp:LinkButton>
                                </td>
                                <td class="normal1">
                                    <asp:LinkButton ID="lnk5" runat="server">5</asp:LinkButton>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkFirst" runat="server"><div class="LinkArrow"><<</div>
                                    </asp:LinkButton>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkPrevious" runat="server"><div class="LinkArrow"><</div>
                                    </asp:LinkButton>
                                </td>
                                <td class="normal1">
                                    <asp:Label ID="lblPage" runat="server">Page</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCurrrentPage" runat="server" Width="28px" CssClass="signup" AutoPostBack="True"
                                        MaxLength="5"></asp:TextBox>
                                </td>
                                <td class="normal1">
                                    <asp:Label ID="lblOf" runat="server">of</asp:Label>
                                </td>
                                <td class="normal1">
                                    <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkNext" runat="server" CssClass="LinkArrow"><div class="LinkArrow">></div>
                                    </asp:LinkButton>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkLast" runat="server"><div class="LinkArrow">>></div>
                                    </asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table cellspacing="1" cellpadding="1" width="100%" border="0">
                <tr>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="a" href="javascript:fnSortByChar('a')">
                            <div class="A2Z">
                                A</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="b" href="javascript:fnSortByChar('b')">
                            <div class="A2Z">
                                B</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="c" href="javascript:fnSortByChar('c')">
                            <div class="A2Z">
                                C</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="d" href="javascript:fnSortByChar('d')">
                            <div class="A2Z">
                                D</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="e" href="javascript:fnSortByChar('e')">
                            <div class="A2Z">
                                E</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="f" href="javascript:fnSortByChar('f')">
                            <div class="A2Z">
                                F</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="g" href="javascript:fnSortByChar('g')">
                            <div class="A2Z">
                                G</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="h" href="javascript:fnSortByChar('h')">
                            <div class="A2Z">
                                H</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="I" href="javascript:fnSortByChar('i')">
                            <div class="A2Z">
                                I</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="j" href="javascript:fnSortByChar('j')">
                            <div class="A2Z">
                                J</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="k" href="javascript:fnSortByChar('k')">
                            <div class="A2Z">
                                K</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="l" href="javascript:fnSortByChar('l')">
                            <div class="A2Z">
                                L</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="m" href="javascript:fnSortByChar('m')">
                            <div class="A2Z">
                                M</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="n" href="javascript:fnSortByChar('n')">
                            <div class="A2Z">
                                N</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="o" href="javascript:fnSortByChar('o')">
                            <div class="A2Z">
                                O</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="p" href="javascript:fnSortByChar('p')">
                            <div class="A2Z">
                                P</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="q" href="javascript:fnSortByChar('q')">
                            <div class="A2Z">
                                Q</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="r" href="javascript:fnSortByChar('r')">
                            <div class="A2Z">
                                R</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="s" href="javascript:fnSortByChar('s')">
                            <div class="A2Z">
                                S</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="t" href="javascript:fnSortByChar('t')">
                            <div class="A2Z">
                                T</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="u" href="javascript:fnSortByChar('u')">
                            <div class="A2Z">
                                U</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="v" href="javascript:fnSortByChar('v')">
                            <div class="A2Z">
                                V</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="w" href="javascript:fnSortByChar('w')">
                            <div class="A2Z">
                                W</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="x" href="javascript:fnSortByChar('x')">
                            <div class="A2Z">
                                X</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="y" href="javascript:fnSortByChar('y')">
                            <div class="A2Z">
                                Y</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="z" href="javascript:fnSortByChar('z')">
                            <div class="A2Z">
                                Z</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="all" href="javascript:fnSortByChar('0')">
                            <div class="A2Z">
                                All</div>
                        </a>
                    </td>
                </tr>
            </table>
            <asp:Table ID="table3" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
                Width="100%" BorderColor="black" GridLines="None" Height="350" CssClass="aspTable">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Top">
                        <asp:DataGrid ID="dgDocs" runat="server" Width="100%" CssClass="dg" AllowSorting="True"
                            AutoGenerateColumns="False" BorderColor="white">
                            <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                            <ItemStyle CssClass="is"></ItemStyle>
                            <HeaderStyle CssClass="hs"></HeaderStyle>
                            <Columns>
                                <asp:BoundColumn Visible="False" DataField="numGenericDocID"></asp:BoundColumn>
                                <asp:BoundColumn Visible="False" DataField="vcDocumentSection"></asp:BoundColumn>
                                <asp:BoundColumn Visible="False" DataField="numRecID"></asp:BoundColumn>
                                <asp:BoundColumn Visible="False" DataField="VcFileName"></asp:BoundColumn>
                                <asp:ButtonColumn CommandName="Name" SortExpression="vcDocName" DataTextField="vcDocName"
                                    HeaderText="<font color=white>Document Name</font>"></asp:ButtonColumn>
                                <asp:BoundColumn DataField="Category" SortExpression="Category" HeaderText="<font color=white>Document Category</font>">
                                </asp:BoundColumn>
                                <%-- <asp:BoundColumn DataField="CreatedDate" SortExpression="CreatedDate" HeaderText="<font color=white>Created On</font>"></asp:BoundColumn> --%>
                                <asp:TemplateColumn HeaderText="<font color=white>Date On</font>" SortExpression="CreatedDate">
                                    <ItemTemplate>
                                        <%#ReturnDateTime(DataBinder.Eval(Container.DataItem, "CreatedDate"))%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="Approved" HeaderText="Approved" ItemStyle-HorizontalAlign="Center">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="Declined" HeaderText="Declined" ItemStyle-HorizontalAlign="Center">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="Pending" HeaderText="Pending" ItemStyle-HorizontalAlign="Center">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="Modified" SortExpression="[Date]" HeaderText="<font color=white>Last Modified On, By</font>">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="Section" SortExpression="Section" HeaderText="<font color=white>Related Section</font>">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="Name" SortExpression="Name" HeaderText="<font color=white>Name</font>">
                                </asp:BoundColumn>
                                <asp:TemplateColumn SortExpression="vcFileType" HeaderText="<font color=white>File Type</font>">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hplLink" Target="_blank" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "vcFileType") %>'>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn>
                                    <ItemTemplate>
                                        <asp:Button ID="btnDelete" runat="server" CssClass="Delete" Text="X" CommandName="Delete">
                                        </asp:Button>
                                        <asp:LinkButton ID="lnkDelete" runat="server" Visible="false">
											<font color="#730000">*</font></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
            <table width="100%">
                <tr>
                    <td class="normal4" align="center">
                        <asp:Literal ID="litMessage" runat="server"></asp:Literal>
                    </td>
                </tr>
            </table>
            <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtSortChar" runat="server" Style="display: none"></asp:TextBox>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
