Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common

Partial Public Class frmCusDocList : Inherits BACRMPage

    Dim strColumn As String
    

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim objCommon As New CCommon
            If Session("UserContactID") = Nothing Then Response.Redirect("../Common/frmLogout.aspx")
            m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmDocumentList.aspx", Session("UserContactID"), 15, 12)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../Common/frmAuthorization.aspx")
            If Not IsPostBack Then
                If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then hplNew.Visible = False
                Session("Asc") = 1

                objCommon.sb_FillComboFromDBwithSel(ddlCategory, 29, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlBizDocs, 27, Session("DomainID"))
                'If ddlBizDocs.Items.Count > 0 Then
                '    If Not ddlBizDocs.Items.FindByText("Invoice") Is Nothing Then
                '        ddlBizDocs.Items.FindByText("Invoice").Selected = True
                '    End If
                'End If

                txtCurrrentPage.Text = 1
                BindDatagrid()
            End If
            If txtSortChar.Text <> "" Then
                ViewState("SortChar") = txtSortChar.Text
                ViewState("Column") = "vcDocName"
                Session("Asc") = 0
                BindDatagrid()
                txtSortChar.Text = ""
            End If
            If ddlDocumentSection.SelectedItem.Value = "B" Then
                tdCategory.Visible = False
                tdBizdocs.Visible = True
            Else
                tdCategory.Visible = True
                tdBizdocs.Visible = False
            End If
            hplNew.NavigateUrl = "../documents/frmCusAddDocs.aspx?Type=A&RecID=" & Session("DivId")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindDatagrid()
        Try
            Dim dtDocuments As DataTable
            Dim objDocuments As New DocumentList
            Dim SortChar As Char
            If ViewState("SortChar") <> "" Then
                SortChar = ViewState("SortChar")
            Else : SortChar = "0"
            End If
            With objDocuments
                .DivisionID = Session("DivId")
                .DocumentSection = ddlDocumentSection.SelectedItem.Value
                .SortCharacter = SortChar
                If ddlDocumentSection.SelectedItem.Value = "B" Then
                    .DocCategory = ddlBizDocs.SelectedItem.Value
                Else : .DocCategory = ddlCategory.SelectedItem.Value
                End If
                If txtCurrrentPage.Text.Trim <> "" Then
                    .CurrentPage = txtCurrrentPage.Text
                Else : .CurrentPage = 1
                End If
                .PageSize = Session("PagingRows")
                .TotalRecords = 0
                If ViewState("Column") <> "" Then
                    .columnName = ViewState("Column")
                Else : .columnName = "[Date]"
                End If
                If Session("Asc") = 1 Then
                    .columnSortOrder = "Desc"
                Else : .columnSortOrder = "Asc"
                End If
                .byteMode = 0
            End With
            dtDocuments = objDocuments.ExtGetDocuments
            If objDocuments.TotalRecords = 0 Then
                hidenav.Visible = False
                lblRecordCount.Text = 0
            Else
                hidenav.Visible = True
                lblRecordCount.Text = String.Format("{0:#,###}", objDocuments.TotalRecords)
                Dim strTotalPage As String()
                Dim decTotalPage As Decimal
                decTotalPage = lblRecordCount.Text / Session("PagingRows")
                decTotalPage = Math.Round(decTotalPage, 2)
                strTotalPage = CStr(decTotalPage).Split(".")
                If (lblRecordCount.Text Mod Session("PagingRows")) = 0 Then
                    lblTotal.Text = strTotalPage(0)
                    txtTotalPage.Text = strTotalPage(0)
                Else
                    lblTotal.Text = strTotalPage(0) + 1
                    txtTotalPage.Text = strTotalPage(0) + 1
                End If
                txtTotalRecords.Text = lblRecordCount.Text
            End If
            dgDocs.DataSource = dtDocuments
            dgDocs.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub lnkLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkLast.Click
        Try
            txtCurrrentPage.Text = txtTotalPage.Text
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkPrevious.Click
        Try
            If txtCurrrentPage.Text = 1 Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text - 1
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkFirst.Click
        Try
            txtCurrrentPage.Text = 1
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkNext.Click
        Try
            If txtCurrrentPage.Text = txtTotalPage.Text Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 1
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk2.Click
        Try
            If txtCurrrentPage.Text + 1 = txtTotalPage.Text Or txtCurrrentPage.Text + 1 > txtTotalPage.Text Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 2
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk3.Click
        Try
            If txtCurrrentPage.Text + 2 = txtTotalPage.Text Or txtCurrrentPage.Text + 2 > txtTotalPage.Text Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 3
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk4.Click
        Try
            If txtCurrrentPage.Text + 3 = txtTotalPage.Text Or txtCurrrentPage.Text + 3 > txtTotalPage.Text Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 4
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk5.Click
        Try
            If txtCurrrentPage.Text + 4 = txtTotalPage.Text Or txtCurrrentPage.Text + 4 > txtTotalPage.Text Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 5
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub txtCurrrentPage_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCurrrentPage.TextChanged
        Try
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgDocs_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgDocs.ItemCommand
        Try
            If e.CommandName = "Name" Then
                Session("DocID") = e.Item.Cells(0).Text()
                Response.Redirect("../Documents/frmCusAddDocs.aspx?Type=" & e.Item.Cells(1).Text & "&RecID=" & e.Item.Cells(2).Text & "&DocID=" & e.Item.Cells(0).Text, False)
            End If
            If e.CommandName = "Delete" Then
                Dim objDocuments As New DocumentList
                With objDocuments
                    .SpecDocID = e.Item.Cells(0).Text()
                    .DelSpecDocuments()
                End With
                BindDatagrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgDocs_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgDocs.SortCommand
        Try
            strColumn = e.SortExpression.ToString()
            If ViewState("Column") <> strColumn Then
                ViewState("Column") = strColumn
                Session("Asc") = 0
            Else
                If Session("Asc") = 0 Then
                    Session("Asc") = 1
                Else : Session("Asc") = 0
                End If
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgDocs_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgDocs.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As Button
                Dim lnkDelete As LinkButton
                Dim hplLink As HyperLink

                lnkDelete = e.Item.FindControl("lnkDelete")
                btnDelete = e.Item.FindControl("btnDelete")
                hplLink = e.Item.FindControl("hplLink")
                hplLink.NavigateUrl = "frmViewAttachment.aspx?docid=" & CCommon.ToLong(e.Item.Cells(0).Text.Trim)
                If e.Item.Cells(1).Text = "B" Then
                    e.Item.Cells(4).Attributes.Add("onclick", "return OpenBizInvoice('" & e.Item.Cells(0).Text & "','" & e.Item.Cells(2).Text & "')")
                    lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                    btnDelete.Visible = False
                    lnkDelete.Visible = True
                Else
                    If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                        btnDelete.Visible = False
                        lnkDelete.Visible = True
                        lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                    Else : btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function ReturnDateTime(ByVal CloseDate As Date) As String
        Try
            If IsDBNull(CloseDate) = False Then Return FormattedDateTimeFromDate(CloseDate, Session("DateFormat"))
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub ddlDocumentSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDocumentSection.SelectedIndexChanged
        Try
            txtCurrrentPage.Text = 1
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
        Try
            txtCurrrentPage.Text = 1
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlBizDocs_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBizDocs.SelectedIndexChanged
        Try
            txtCurrrentPage.Text = 1
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class

