Imports BACRM.BusinessLogic.Common
Public Class clsAuthorization

    Public Shared Function fn_GetPageListUserRights(ByVal objCommon As Object, ByVal strPageName As String, ByVal lngContactID As Long, ByVal numModuleID As Long, ByVal numPageID As Long) As Array
        Dim aryPageRights(4) As Integer
        If objCommon Is Nothing Then
            objCommon = New CCommon
        End If
        objCommon.ContactID = lngContactID
        objCommon.PageName = strPageName
        objCommon.ModuleID = CInt(numModuleID)
        objCommon.PageID = CInt(numPageID)
        aryPageRights = objCommon.ExtPageLevelUserRights
        Return aryPageRights
    End Function

End Class
