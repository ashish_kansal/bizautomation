Imports System.Web.SessionState
Imports System.Web.HttpRequest
Imports BACRM.BusinessLogic.Common
Imports System.Globalization

Public Class SessionTracker
    Private _context As HttpContext
    Private _expires As Date

    Private _visitCount As String

    Private _userHostAddress As String
    Private _userAgent As String

    Private _originalReferrer As String
    Private _originalURL As String

    Private _sessionReferrer As String = ""
    Private _sessionURL As String

    Private _IsVisited As Boolean

    Private _IsRegistered As Boolean

    Private _TrackID As Long

    Private _browser As HttpBrowserCapabilities

    Private _pages As New ArrayList()

    Public Sub New()
        'HttpContext.Current allows us to gain access to all 
        'the intrinsic ASP context objects like Request, Response, Session, etc
        _context = HttpContext.Current

        'provides a default expiration for cookies
        _expires = Now.AddYears(1)

        'load up the tracker

        Dim CookieEnabled As Boolean

        CookieEnabled = _context.Request.Browser.Cookies

        If CookieEnabled = True Then
            IncrementVisitCount()
            SetRegister(False)
        End If



        _userHostAddress = _context.Request.UserHostAddress.ToString
        _userAgent = _context.Request.UserAgent.ToString

        If CCommon.ToString(_context.Request.QueryString("Ref")).Length > 1 Then
            _sessionReferrer = HttpUtility.UrlDecode(CCommon.ToString(_context.Request.QueryString("Ref")))
            'set original referrer if not set
            If CookieEnabled = True Then
                SetOriginalReferrer(_sessionReferrer)
            Else
                _originalReferrer = ""
            End If
        End If

        If CCommon.ToString(_context.Request.QueryString("URL")).Length > 1 Then
            _sessionURL = HttpUtility.UrlDecode(CCommon.ToString(_context.Request.QueryString("URL")))
            'set original url if not set
            If CookieEnabled = True Then
                SetOriginalURL(_sessionURL)
            Else
                _originalURL = ""
                _IsVisited = False
            End If
        End If

        If Not _context.Request.QueryString("TrackID") Is Nothing AndAlso CCommon.ToString(_context.Request.QueryString("TrackID")).Length > 1 Then
            _TrackID = HttpUtility.UrlDecode(CCommon.ToString(_context.Request.QueryString("TrackID")))
            'set original url if not set
            If CookieEnabled = True Then
                SetTrackID(_TrackID)
            Else
                _TrackID = ""
                _IsVisited = False
            End If
        End If

        'set the browser capabilities
        _browser = _context.Request.Browser

    End Sub


    'add the page to the member arraylist 
    Public Sub AddPage(ByVal pageName As String)
        'create a new page tracker item 
        Dim pti As New SessionTrackerPage()
        pti.PageName = pageName
        'set a time stamp
        pti.Time = Now

        'add the page tracker item to the array list
        _pages.Add(pti)
    End Sub

    'increment the visit count and save in a cookie
    Public Sub IncrementVisitCount()
        Const KEY = "VisitCount"

        'check is cookie has been set yet
        If IsNothing(_context.Request.Cookies.Get(KEY)) Then
            _visitCount = 1
        Else
            _visitCount = _context.Request.Cookies.Get(KEY).Value + 1
        End If

        'set or reset the cookie
        addCookie(KEY, _visitCount)
    End Sub

    'set the original referrer to a cookie
    Public Sub SetOriginalReferrer(ByVal val As String)
        Const KEY = "OriginalReferrer"

        _originalReferrer = val
        'set or reset the cookie
        addCookie(KEY, val)
    End Sub

    'set the original url to a cookie
    Public Sub SetOriginalURL(ByVal val As String)
        Const KEY = "OriginalURL"

        'check is cookie has been set yet
        If Not IsNothing(_context.Request.Cookies.Get(KEY)) Then
            _IsVisited = True
        Else
            _IsVisited = False
        End If
        _originalURL = val
        'set or reset the cookie
        addCookie(KEY, val)
    End Sub

    Public Sub SetTrackID(ByVal val As Long)
        Const KEY As String = "TrackID"

        'check is cookie has been set yet
        If Not IsNothing(_context.Request.Cookies.Get(KEY)) Then
            _IsVisited = True
        Else
            _IsVisited = False
        End If

        _TrackID = val
        'set or reset the cookie
        addCookie(KEY, val)
    End Sub

    Public Sub SetRegister(ByVal val As Boolean)
        Const KEY = "Register"

        'check is cookie has been set yet
        If Not IsNothing(_context.Request.Cookies.Get(KEY)) Then
            _IsRegistered = _context.Request.Cookies.Get(KEY).Value
        Else
            addCookie(KEY, val)
        End If

    End Sub

    Private Sub addCookie(ByVal key As String, ByVal value As String)
        Dim cookie As HttpCookie
        cookie = New HttpCookie(key, value)
        cookie.Expires = _expires
        _context.Response.Cookies.Set(cookie)
    End Sub

    Public Sub EditCookie(ByVal value As Boolean)
        Const KEY = "Register"
        'check is cookie has been set yet
        'If Not IsNothing(_context.Request.Cookies.Get(KEY)) Then
        '    _context.Request.Cookies.Get(KEY).Value = value
        '    _context.Response.Cookies(KEY).Value = value
        'End If
        addCookie(KEY, value)
    End Sub


#Region "Properties"

    'Visit Count 
    ReadOnly Property VisitCount() As Integer
        Get
            Return _visitCount
        End Get
    End Property

    'Original Referrer 
    ReadOnly Property OriginalReferrer() As String
        Get
            Return _originalReferrer
        End Get
    End Property



    'Original URL 
    ReadOnly Property OriginalURL() As String
        Get
            Return _originalURL
        End Get
    End Property

    'Session Referrer
    Property SessionReferrer() As String
        Get
            Return _sessionReferrer
        End Get
        Set(ByVal Value As String)
            _sessionReferrer = Value
        End Set
    End Property



    'Session URL
    ReadOnly Property SessionURL() As String
        Get
            Return _sessionURL
        End Get
    End Property

    'Session User Host Address (IP)
    ReadOnly Property SessionUserHostAddress() As String
        Get
            Return _userHostAddress
        End Get
    End Property

    'Session User Agent
    ReadOnly Property SessionUserAgent() As String
        Get
            Return _userAgent
        End Get
    End Property

    'Pages - array list
    ReadOnly Property Pages() As ArrayList
        Get
            Return _pages
        End Get
    End Property

    'Browser Cap
    ReadOnly Property Browser() As HttpBrowserCapabilities
        Get
            Return _browser
        End Get
    End Property

    ReadOnly Property IsVisited() As Boolean
        Get
            Return _IsVisited
        End Get
    End Property

    ReadOnly Property IsRegistered() As Boolean
        Get
            Return _IsRegistered
        End Get
    End Property

    ReadOnly Property TrackID() As Long
        Get
            Return _TrackID
        End Get
    End Property

#End Region

End Class
Public Class SessionTrackerPage
    Public PageName As String
    Public Time As Date

End Class