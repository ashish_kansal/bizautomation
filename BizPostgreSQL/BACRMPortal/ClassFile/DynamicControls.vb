Imports BACRM.BusinessLogic.Admin
Module DynamicControls


    Public Function CreateTexBox(ByVal tblRow As HtmltableRow, ByVal tblCell As HtmltableCell, ByVal strId As String, ByVal strText As String)

        Dim txt As New TextBox
        txt.CssClass = "signup"
        txt.ID = strId
        If strText <> "0" Then
            txt.Text = strText
        Else
            txt.Text = ""
        End If
        tblCell.Controls.Add(txt)
        tblRow.Cells.Add(tblCell)
    End Function

    Public Function CreateDropdown(ByVal tblRow As HtmltableRow, ByVal tblCell As HtmltableCell, ByVal strId As String, ByVal intSelValue As Integer, ByVal intListId As Integer)

        Dim ddl As New DropDownList
        ddl.CssClass = "signup"
        Dim dttable As DataTable
        Dim ObjCusfld As New CustomFields
        ObjCusfld.ListId = CInt(intListId)
        dttable = ObjCusfld.GetMasterListByListId
        ddl.ID = strId
        ddl.DataSource = dttable
        ddl.DataTextField = "vcData"
        ddl.DataValueField = "numListItemID"
        ddl.DataBind()
        ddl.Items.Insert(0, "--Select One--")
        ddl.Items.FindByText("--Select One--").Value = 0
        ddl.Width = Unit.Pixel(130)

        tblCell.Controls.Add(ddl)
        tblRow.Cells.Add(tblCell)
        If intSelValue > 0 Then
            ddl.Items.FindByValue(intSelValue).Selected = True
        End If
    End Function

    Public Function CreateTextArea(ByVal tblRow As HtmltableRow, ByVal tblCell As HtmltableCell, ByVal strId As String, ByVal strText As String)


        Dim txt As New TextBox
        txt.CssClass = "signup"
        txt.Width = Unit.Pixel(200)
        txt.Height = Unit.Pixel(40)
        txt.TextMode = TextBoxMode.MultiLine
        txt.ID = strId
        If strText <> "0" Then
            txt.Text = strText
        Else
            txt.Text = ""
        End If
        tblCell.Controls.Add(txt)
        tblRow.Cells.Add(tblCell)
    End Function

    Public Function CreateChkBox(ByVal tblRow As HtmltableRow, ByVal tblCell As HtmltableCell, ByVal strId As String, ByVal intSel As Integer)

        Dim chk As New CheckBox
        chk.ID = strId
        If intSel = 1 Then
            chk.Checked = True
        Else
            chk.Checked = False
        End If
        tblCell.Controls.Add(chk)
        tblRow.Cells.Add(tblCell)
    End Function

    Public Function CreateLink(ByVal tblRow As HtmlTableRow, ByVal tblCell As HtmlTableCell, ByVal strId As String, ByVal URL As String, ByVal RecID As Long, ByVal Label As String)
        Dim hpl As New HyperLink
        hpl.ID = "hpl" & strId
        hpl.Text = Label
        URL = URL.Replace("RecordID", RecID)
        hpl.NavigateUrl = URL
        hpl.Target = "_blank"
        hpl.CssClass = "hyperlink"
        tblCell.Controls.Add(hpl)
        tblRow.Cells.Add(tblCell)
    End Function

End Module
