Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Account
Imports BACRM.BusinessLogic.Contacts
Partial Public Class Organization_frmCompanyList
    Inherits System.Web.UI.Page
    Dim strColumn As String
    Dim SI As Integer = 0
    Dim SI1 As Integer = 0
    Dim SI2 As Integer = 0
    Dim frm As String = ""
    Dim frm1 As String = ""
    Dim frm2 As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not GetQueryStringVal(Request.QueryString("enc"), "SI") Is Nothing Then
            SI = GetQueryStringVal(Request.QueryString("enc"), "SI")

        End If
        If Not GetQueryStringVal(Request.QueryString("enc"), "SI1") Is Nothing Then
            SI1 = GetQueryStringVal(Request.QueryString("enc"), "SI1")
        Else
            SI1 = 0
        End If
        If Not GetQueryStringVal(Request.QueryString("enc"), "SI2") Is Nothing Then
            SI2 = GetQueryStringVal(Request.QueryString("enc"), "SI2")
        Else
            SI2 = 0
        End If
        If Not GetQueryStringVal(Request.QueryString("enc"), "frm") Is Nothing Then
            frm = ""
            frm = GetQueryStringVal(Request.QueryString("enc"), "frm")
        Else

            frm = ""
        End If
        If Not GetQueryStringVal(Request.QueryString("enc"), "frm1") Is Nothing Then
            frm1 = ""
            frm1 = GetQueryStringVal(Request.QueryString("enc"), "frm1")
        Else
            frm1 = ""
        End If
        If Not GetQueryStringVal(Request.QueryString("enc"), "SI2") Is Nothing Then
            frm2 = ""
            frm2 = GetQueryStringVal(Request.QueryString("enc"), "frm2")
        Else
            frm2 = ""
        End If
        If Not IsPostBack Then
            'If uwOppTab.Tabs.Count > SI Then
            '    uwOppTab.SelectedTabIndex = SI
            'End If
            Session("RelId") = GetQueryStringVal(Request.QueryString("enc"), "RelId")
            'Session("RelId") = GetQueryStringVal(Request.QueryString("enc"),"RelId").Replace("%",.Replace(" ", "")
            Dim objCommon As New CCommon
            objCommon.sb_FillComboFromDBwithSel(ddlRelationship, 5, Session("DomainID"))
            If GetQueryStringVal(Request.QueryString("enc"), "RelId") <> "" Then
                ddlRelationship.Items.FindByValue(GetQueryStringVal(Request.QueryString("enc"), "RelId")).Selected = True
            End If
            LoadProfile()
            If Session("List") <> "Organization" Then
                Session("ListDetails") = Nothing
                txtCurrrentPage.Text = 1
                Session("Asc") = 1
            Else
                Dim str As String()
                str = Session("ListDetails").split(",")
                ViewState("Column") = str(5)
                ViewState("SortChar") = str(0)
                txtCustomer.Text = str(3)
                txtFirstName.Text = str(1)
                txtLastName.Text = str(2)
                txtCurrrentPage.Text = str(4)
                Session("Asc") = str(6)
                If Not ddlFilter.Items.FindByValue(str(7)) Is Nothing Then
                    ddlFilter.Items.FindByValue(str(7)).Selected = True
                End If
                If Not ddlProfile.Items.FindByValue(str(8)) Is Nothing Then
                    ddlProfile.Items.FindByValue(str(8)).Selected = True
                End If
            End If
            Session("List") = "Organization"
            BindDatagrid()

        End If
        Dim dtTab As New DataTable
        dtTab = Session("DefaultTab")
        If ddlRelationship.SelectedIndex = 0 Then
            If GetQueryStringVal(Request.QueryString("enc"), "CRMType") = 1 Then
                If dtTab.Rows.Count > 0 Then
                    lblRelationship.Text = dtTab.Rows(0).Item("vcProspect") & "s"
                Else
                    lblRelationship.Text = "Prospects"
                End If
            ElseIf GetQueryStringVal(Request.QueryString("enc"), "CRMType") = 2 Then

                If dtTab.Rows.Count > 0 Then
                    lblRelationship.Text = dtTab.Rows(0).Item("vcAccount") & "s"
                Else
                    lblRelationship.Text = "Accounts"
                End If
            Else
                lblRelationship.Text = "Organization"
            End If

        Else
            lblRelationship.Text = ddlRelationship.SelectedItem.Text
        End If

        If txtSortChar.Text <> "" Then
            ViewState("SortChar") = txtSortChar.Text
            ViewState("Column") = "vcCompanyName"
            Session("Asc") = 0
            BindDatagrid()
            txtSortChar.Text = ""
        End If

    End Sub


    Sub LoadProfile()
        Dim objUserAccess As New UserAccess
        objUserAccess.RelID = ddlRelationship.SelectedItem.Value
        objUserAccess.DomainID = Session("DomainID")
        ddlProfile.DataSource = objUserAccess.GetRelProfileD
        ddlProfile.DataTextField = "ProName"
        ddlProfile.DataValueField = "numProfileID"
        ddlProfile.DataBind()
        ddlProfile.Items.Insert(0, New ListItem("---Select One---", "0"))
    End Sub

    Sub BindDatagrid()
        Dim dtCompanyList As DataTable
        Dim objAccounts As New CAccounts
        Dim SortChar As Char
        If ViewState("SortChar") <> "" Then
            SortChar = ViewState("SortChar")
        Else
            SortChar = "0"
        End If
        With objAccounts
            .RelType = Session("RelId")
            .UserCntID = Session("UserContactID")
            .UserRightType = 1
            .Profile = ddlProfile.SelectedItem.Value
            .SortOrder = ddlFilter.SelectedItem.Value
            .FirstName = txtFirstName.Text
            .LastName = txtLastName.Text
            .SortCharacter = SortChar
            .CustName = txtCustomer.Text
            .DomainID = Session("DomainID")
            If txtCurrrentPage.Text.Trim <> "" Then
                .CurrentPage = txtCurrrentPage.Text
            Else
                .CurrentPage = 1
            End If
            .PageSize = Session("PagingRows")
            .TotalRecords = 0
            .CRMType = IIf(GetQueryStringVal(Request.QueryString("enc"), "CRMType") = "", 0, GetQueryStringVal(Request.QueryString("enc"), "CRMType"))
            .bitPartner = 1
            If ViewState("Column") <> "" Then
                .columnName = ViewState("Column")
            Else
                .columnName = "DM.bintcreateddate"
            End If
            If Session("Asc") = 1 Then
                .columnSortOrder = "Desc"
            Else
                .columnSortOrder = "Asc"
            End If
        End With
        Session("ListDetails") = SortChar & "," & txtFirstName.Text & "," & txtLastName.Text & "," & txtCustomer.Text & "," & txtCurrrentPage.Text & "," & ViewState("Column") & "," & Session("Asc") & "," & ddlFilter.SelectedValue & "," & ddlProfile.SelectedValue
        dtCompanyList = objAccounts.GetCompanyList
        If objAccounts.TotalRecords = 0 Then
            hidenav.Visible = False
            lblRecordCount.Text = 0
        Else
            hidenav.Visible = True
            lblRecordCount.Text = String.Format("{0:#,###}", objAccounts.TotalRecords)
            Dim strTotalPage As String()
            Dim decTotalPage As Decimal
            decTotalPage = lblRecordCount.Text / Session("PagingRows")
            decTotalPage = Math.Round(decTotalPage, 2)
            strTotalPage = CStr(decTotalPage).Split(".")
            If (lblRecordCount.Text Mod Session("PagingRows")) = 0 Then
                lblTotal.Text = strTotalPage(0)
                txtTotalPage.Text = strTotalPage(0)
            Else
                lblTotal.Text = strTotalPage(0) + 1
                txtTotalPage.Text = strTotalPage(0) + 1
            End If
            txtTotalRecords.Text = lblRecordCount.Text
        End If
        dtgAccount.DataSource = dtCompanyList
        dtgAccount.DataBind()
    End Sub

    Private Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo.Click
        BindDatagrid()
    End Sub


    Private Sub lnkLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkLast.Click
        txtCurrrentPage.Text = txtTotalPage.Text
        BindDatagrid()
    End Sub

    Private Sub lnkPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkPrevious.Click
        If txtCurrrentPage.Text = 1 Then
            Exit Sub
        Else
            txtCurrrentPage.Text = txtCurrrentPage.Text - 1
        End If
        BindDatagrid()
    End Sub

    Private Sub lnkFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkFirst.Click
        txtCurrrentPage.Text = 1
        BindDatagrid()
    End Sub

    Private Sub lnkNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkNext.Click
        If txtCurrrentPage.Text = txtTotalPage.Text Then
            Exit Sub
        Else
            txtCurrrentPage.Text = txtCurrrentPage.Text + 1
        End If
        BindDatagrid()
    End Sub

    Private Sub lnk2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk2.Click
        If txtCurrrentPage.Text + 1 = txtTotalPage.Text Or txtCurrrentPage.Text + 1 > txtTotalPage.Text Then
            Exit Sub
        Else
            txtCurrrentPage.Text = txtCurrrentPage.Text + 2
        End If
        BindDatagrid()
    End Sub

    Private Sub lnk3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk3.Click
        If txtCurrrentPage.Text + 2 = txtTotalPage.Text Or txtCurrrentPage.Text + 2 > txtTotalPage.Text Then
            Exit Sub
        Else
            txtCurrrentPage.Text = txtCurrrentPage.Text + 3
        End If
        BindDatagrid()
    End Sub

    Private Sub lnk4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk4.Click
        If txtCurrrentPage.Text + 3 = txtTotalPage.Text Or txtCurrrentPage.Text + 3 > txtTotalPage.Text Then
            Exit Sub
        Else
            txtCurrrentPage.Text = txtCurrrentPage.Text + 4
        End If
        BindDatagrid()
    End Sub

    Private Sub lnk5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk5.Click
        If txtCurrrentPage.Text + 4 = txtTotalPage.Text Or txtCurrrentPage.Text + 4 > txtTotalPage.Text Then
            Exit Sub
        Else
            txtCurrrentPage.Text = txtCurrrentPage.Text + 5
        End If
        BindDatagrid()
    End Sub

    Private Sub txtCurrrentPage_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCurrrentPage.TextChanged
        BindDatagrid()
    End Sub

    Private Sub dtgAccount_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgAccount.ItemDataBound
        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
            ' ''Dim hplEmail As HyperLink
            ' ''hplEmail = e.Item.FindControl("hplEmail")
            ' ''If ConfigurationManager.AppSettings("EmailLink") = 1 Then
            ' ''    hplEmail.NavigateUrl = "mailto:" & hplEmail.Text
            ' ''Else
            ' ''    hplEmail.NavigateUrl = "../common/callemail.aspx?Lsemail=" & hplEmail.Text & "&ContID=" & e.Item.Cells(0).Text
            ' ''End If
            Dim DivisionID As Integer
            Dim btnDelete As Button
            Dim lnkDelete As LinkButton
            lnkDelete = e.Item.FindControl("lnkDelete")
            btnDelete = e.Item.FindControl("btnDelete")
            DivisionID = CLng(e.Item.Cells(0).Text)
            If DivisionID = 1 Then
                btnDelete.Visible = False
                lnkDelete.Visible = True
                lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                Exit Sub
            Else
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            End If
        End If
    End Sub

    Private Sub dtgAccount_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgAccount.ItemCommand
        Dim DivisionID As Integer
        Dim strEmail As String
        Try
            DivisionID = CLng(e.Item.Cells(0).Text)
            strEmail = e.Item.Cells(4).Text
            If e.CommandName = "Company" Then
                Dim objCommon As New CCommon
                objCommon.DivisionID = DivisionID
                objCommon.charModule = "D"
                objCommon.GetCompanySpecificValues1()
                If objCommon.CRMType = 0 Then
                    If (Session("EnableIntMedPage") = 1) Then
                        Response.Redirect("../pagelayout/frmLeadDtl.aspx?frm=CompanyList&DivID=" & DivisionID & "&CRMType=" & objCommon.CRMType & "&RelId=" & Session("RelId") & "&SI1=0&SI2=" & SI1 & "&frm1=" & frm1 & "&frm2=" & frm2)
                    Else
                        Response.Redirect("../Leads/frmLeads.aspx?frm=CompanyList&DivID=" & DivisionID & "&CRMType=" & objCommon.CRMType & "&RelId=" & Session("RelId") & "&SI1=0&SI2=" & SI1 & "&frm1=" & frm1 & "&frm2=" & frm2)
                    End If

                ElseIf objCommon.CRMType = 1 Then
                    If (Session("EnableIntMedPage") = 1) Then
                        Response.Redirect("../pagelayout/frmPartnerProspectDtl.aspx?frm=CompanyList&DivID=" & DivisionID & "&CRMType=" & objCommon.CRMType & "&RelId=" & Session("RelId") & "&SI1=0&SI2=" & SI1 & "&frm1=" & frm1 & "&frm2=" & frm2)
                    Else
                        Response.Redirect("../Organization/frmPartnerProsDtl.aspx?frm=CompanyList&DivID=" & DivisionID & "&CRMType=" & objCommon.CRMType & "&RelId=" & Session("RelId") & "&SI1=0&SI2=" & SI1 & "&frm1=" & frm1 & "&frm2=" & frm2)
                    End If

                ElseIf objCommon.CRMType = 2 Then
                    If (Session("EnableIntMedPage") = 1) Then
                        Response.Redirect("../pagelayout/frmPartnerAccountDtl.aspx?frm=CompanyList&DivID=" & DivisionID & "&CRMType=" & objCommon.CRMType & "&RelId=" & Session("RelId") & "&SI1=0&SI2=" & SI1 & "&frm1=" & frm1 & "&frm2=" & frm2)
                    Else
                        Response.Redirect("../Organization/frmPartnerActDtl.aspx?frm=CompanyList&DivID=" & DivisionID & "&CRMType=" & objCommon.CRMType & "&RelId=" & Session("RelId") & "&SI1=0&SI2=" & SI1 & "&frm1=" & frm1 & "&frm2=" & frm2)
                    End If

                End If
            ElseIf e.CommandName = "Contact" Then
                Dim objCommon As New CCommon
                objCommon.DivisionID = DivisionID
                objCommon.charModule = "D"
                objCommon.GetCompanySpecificValues1()
                If (Session("EnableIntMedPage") = 1) Then
                    Response.Redirect("../pagelayout/frmContact.aspx?frm=CompanyList&CntID=" & objCommon.ContactID & "&CRMType=" & objCommon.CRMType & "&RelId=" & Session("RelId") & "&SI1=0&SI2=" & SI1 & "&frm1=" & frm1 & "&frm2=" & frm2)
                Else

                    Response.Redirect("../pagelayout/frmPartnerContDtl.aspx?frm=CompanyList&CntID=" & objCommon.ContactID & "&CRMType=" & objCommon.CRMType & "&RelId=" & Session("RelId") & "&SI1=0&SI2=" & SI1 & "&frm1=" & frm1 & "&frm2=" & frm2)
                End If

            ElseIf e.CommandName = "Email" Then
                Response.Write("<script>" & vbCrLf)
                Response.Write("open(""mailto:" & strEmail & " "",""ThankYou"",""width=550,height=450,scrollbars=no"");" & vbCrLf)
                Response.Write("</script>")
            ElseIf e.CommandName = "Delete" Then
                If ddlFilter.SelectedItem.Value = 7 Then
                    Dim objContacts As New CContacts
                    objContacts.byteMode = 1
                    objContacts.UserCntID = Session("UserContactID")
                    objContacts.ContactID = CLng(DivisionID)
                    objContacts.ManageFavorites()
                    litMessage.Text = "Deleted from Favorites"
                    BindDatagrid()
                Else
                    Dim objAccount As New CAccounts
                    With objAccount
                        .DivisionID = DivisionID
                        .DomainID = Session("DomainID")
                    End With
                    If objAccount.DeleteOrg = False Then
                        litMessage.Text = "Dependent Records Exists.Cannot be deleted."
                    Else
                        BindDatagrid()
                        litMessage.Text = ""
                    End If
                End If

            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub dtgAccount_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dtgAccount.SortCommand
        strColumn = e.SortExpression.ToString()
        If ViewState("Column") <> strColumn Then
            ViewState("Column") = strColumn
            Session("Asc") = 0
        Else
            If Session("Asc") = 0 Then
                Session("Asc") = 1
            Else
                Session("Asc") = 0
            End If
        End If
        BindDatagrid()
    End Sub

    Private Sub ddlRelationship_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRelationship.SelectedIndexChanged
        Session("RelId") = ddlRelationship.SelectedValue
        LoadProfile()
        BindDatagrid()
    End Sub

    Private Sub ddlFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFilter.SelectedIndexChanged
        BindDatagrid()
    End Sub

    Private Sub ddlProfile_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProfile.SelectedIndexChanged
        BindDatagrid()
    End Sub
End Class
