<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCompanyList.aspx.vb" Inherits="BACRMPortal.Organization_frmCompanyList" %>
<%@ Register TagPrefix="menu1" TagName="PartnerPoint" src="../Common/PartnerPoint.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Partner Ponit</title>
</head>
<script language="javascript" type="text/javascript">
	function fnSortByChar(varSortChar)
			{
				document.form1.txtSortChar.value = varSortChar;
				if (typeof(document.form1.txtCurrrentPage)!='undefined')
				{
					document.form1.txtCurrrentPage.value=1;
				}
				document.form1.btnGo.click();
			}
</script>
<body>

    <form id="form1" runat="server">
    <menu1:PartnerPoint id="PartnerPoint1" runat="server"></menu1:PartnerPoint>
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
    <asp:updatepanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" ><ContentTemplate>
    <table align="right">
				<TR>
					<TD class="normal1" width="150">No of Records:
						<asp:label id="lblRecordCount" runat="server"></asp:label></TD>
					<TD class="normal1">Organization
					</TD>
					<TD><asp:textbox id="txtCustomer" runat="server" CssClass="signup" Width="55px"></asp:textbox></TD>
					<TD class="normal1">First Name
					</TD>
					<TD><asp:textbox id="txtFirstName" runat="server" CssClass="signup" Width="55px"></asp:textbox></TD>
					<TD class="normal1">Last Name</TD>
					<TD><asp:textbox id="txtLastName" runat="server" CssClass="signup" Width="55px"></asp:textbox></TD>
					<TD><asp:button id="btnGo" CssClass="button" Width="25" Text="Go" Runat="server"></asp:button></TD>
				</TR>
			</table>
			<br>
			<br>
			<table cellspacing="0" cellpadding="0" width="100%" border="0">
				<tr>
					<td valign="bottom" width="150">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;<asp:label id="lblRelationship" Runat="server"></asp:label>&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<TD class="normal1">Filter&nbsp;<asp:dropdownlist id="ddlFilter" runat="server" cssclass="signup" AutoPostBack="True">
							<asp:ListItem Value="0">--Select One--</asp:ListItem>
							<asp:ListItem Value="1">Favorites</asp:ListItem>
						</asp:dropdownlist>
						<asp:dropdownlist id="ddlRelationship" runat="server" cssclass="signup" AutoPostBack="True" Visible="False"></asp:dropdownlist>&nbsp;&nbsp;&nbsp;Profile&nbsp;
						<asp:dropdownlist id="ddlProfile" runat="server" cssclass="signup" AutoPostBack="True"></asp:dropdownlist></TD>
					<td id="hidenav" noWrap align="right" runat="server">
						<table>
							<tr>
								<td><asp:label id="lblNext" runat="server" cssclass="Text_bold">Next:</asp:label></td>
								<td class="normal1"><asp:linkbutton id="lnk2" runat="server">2</asp:linkbutton></td>
								<td class="normal1"><asp:linkbutton id="lnk3" runat="server">3</asp:linkbutton></td>
								<td class="normal1"><asp:linkbutton id="lnk4" runat="server">4</asp:linkbutton></td>
								<td class="normal1"><asp:linkbutton id="lnk5" runat="server">5</asp:linkbutton></td>
								<td><asp:linkbutton id="lnkFirst" runat="server"><div class="LinkArrow">9</div>
									</asp:linkbutton></td>
								<td><asp:linkbutton id="lnkPrevious" runat="server"><div class="LinkArrow">3</div>
									</asp:linkbutton></td>
								<td class="normal1"><asp:label id="lblPage" runat="server">Page</asp:label></td>
								<td><asp:textbox id="txtCurrrentPage" runat="server" CssClass="signup" Width="28px" AutoPostBack="True"
										MaxLength="5"></asp:textbox></td>
								<td class="normal1"><asp:label id="lblOf" runat="server">of</asp:label></td>
								<td class="normal1"><asp:label id="lblTotal" runat="server"></asp:label></td>
								<td><asp:linkbutton id="lnkNext" runat="server" CssClass="LinkArrow"><div class="LinkArrow">4</div>
									</asp:linkbutton></td>
								<td><asp:linkbutton id="lnkLast" runat="server"><div class="LinkArrow">:</div>
									</asp:linkbutton></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table cellspacing="1" cellpadding="1" width="100%" border="0">
				<tr>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="a" href="javascript:fnSortByChar('a')">
							<div class="A2Z">A</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="b" href="javascript:fnSortByChar('b')">
							<div class="A2Z">B</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="c" href="javascript:fnSortByChar('c')">
							<div class="A2Z">C</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="d" href="javascript:fnSortByChar('d')">
							<div class="A2Z">D</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="e" href="javascript:fnSortByChar('e')">
							<div class="A2Z">E</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="f" href="javascript:fnSortByChar('f')">
							<div class="A2Z">F</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="g" href="javascript:fnSortByChar('g')">
							<div class="A2Z">G</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="h" href="javascript:fnSortByChar('h')">
							<div class="A2Z">H</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="I" href="javascript:fnSortByChar('i')">
							<div class="A2Z">I</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="j" href="javascript:fnSortByChar('j')">
							<div class="A2Z">J</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="k" href="javascript:fnSortByChar('k')">
							<div class="A2Z">K</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="l" href="javascript:fnSortByChar('l')">
							<div class="A2Z">L</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="m" href="javascript:fnSortByChar('m')">
							<div class="A2Z">M</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="n" href="javascript:fnSortByChar('n')">
							<div class="A2Z">N</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="o" href="javascript:fnSortByChar('o')">
							<div class="A2Z">O</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="p" href="javascript:fnSortByChar('p')">
							<div class="A2Z">P</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="q" href="javascript:fnSortByChar('q')">
							<div class="A2Z">Q</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="r" href="javascript:fnSortByChar('r')">
							<div class="A2Z">R</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="s" href="javascript:fnSortByChar('s')">
							<div class="A2Z">S</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="t" href="javascript:fnSortByChar('t')">
							<div class="A2Z">T</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="u" href="javascript:fnSortByChar('u')">
							<div class="A2Z">U</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="v" href="javascript:fnSortByChar('v')">
							<div class="A2Z">V</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="w" href="javascript:fnSortByChar('w')">
							<div class="A2Z">W</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="x" href="javascript:fnSortByChar('x')">
							<div class="A2Z">X</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="y" href="javascript:fnSortByChar('y')">
							<div class="A2Z">Y</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="z" href="javascript:fnSortByChar('z')">
							<div class="A2Z">Z</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="all" href="javascript:fnSortByChar('0')">
							<div class="A2Z">All</div>
						</A>
					</td>
				</tr>
			</table>
			<asp:table id="table2" Width="100%" Runat="server" Height="350" GridLines="None" BorderColor="black"
				BorderWidth="1" cellspacing="0" cellpadding="0"  CssClass="aspTable">
				<asp:tableRow>
					<asp:tableCell VerticalAlign="Top">
					
						<asp:datagrid id="dtgAccount" runat="server" Width="100%" CssClass="dg" AllowSorting="True" AutoGenerateColumns="False"
							BorderColor="white">
							<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							<ItemStyle CssClass="is"></ItemStyle>
							<HeaderStyle CssClass="hs"></HeaderStyle>
							<Columns>
								<asp:BoundColumn Visible="False" DataField="numDivisionID" HeaderText="numDivisionID"></asp:BoundColumn>
								<asp:ButtonColumn DataTextField="CompanyName" SortExpression="vcCompanyName" HeaderText="<font color=white>Organization Name</font>"
									CommandName="Company"></asp:ButtonColumn>
								<asp:ButtonColumn DataTextField="PrimaryContact" SortExpression="vcFirstName" HeaderText="<font color=white>Primary Contact</font>"
									CommandName="Contact"></asp:ButtonColumn>
								<asp:BoundColumn DataField="Phone" SortExpression="numPhone" HeaderText="<font color=white>Phone - Ext</font>"></asp:BoundColumn>
								<%--<asp:TemplateColumn SortExpression="vcEmail" HeaderText="<font color=white>Email</font>">
									<ItemTemplate>
										<asp:HyperLink ID="hplEmail" Runat="server" Target="_blank" Text ='<%# DataBinder.Eval(Container.DataItem, "vcEmail") %>'>
										</asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>--%>
								<asp:HyperLinkColumn DataTextField="vcEmail" SortExpression="vcEmail" DataNavigateUrlField="vcEmail" HeaderText="<font color=white>Email Address</font>" Target=_blank  DataNavigateUrlFormatString=mailto:{0}></asp:HyperLinkColumn>
								<asp:BoundColumn DataField="vcRating" SortExpression="LD.vcData" HeaderText="<font color=white>Rating</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="Follow" SortExpression="LD1.vcData" HeaderText="<font color=white>Follow-up Status</font>"></asp:BoundColumn>
								<asp:BoundColumn DataField="AccountType" SortExpression="tintCRMType" HeaderText="<font color=white>Account Type</font>"></asp:BoundColumn>
								<asp:TemplateColumn>
									<ItemTemplate>
										<asp:Button ID="btnDelete" Runat="server" CssClass="Delete" Text="r" CommandName="Delete"></asp:Button>
										<asp:LinkButton ID="lnkdelete" Runat="server" Visible="false">
											<font color="#730000">*</font></asp:LinkButton>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid>
						
					</asp:tableCell>
				</asp:tableRow>
			</asp:table>
			<table width="100%">
				<tr>
					<td class="normal4" align="center"><asp:literal id="litMessage" Runat="server" EnableViewState="False" ></asp:literal></td>
				</tr>
			</table>
			<asp:textbox id="txtTotalPage" style="DISPLAY: none" Runat="server"></asp:textbox>
			<asp:TextBox ID="txtTotalRecords" Runat="server" style="DISPLAY:none"></asp:TextBox>
			<asp:TextBox ID="txtSortChar" Runat="server" style="DISPLAY:none"></asp:TextBox>
		</ContentTemplate>
	</asp:updatepanel>
			
			
    </form>
</body>
</html>
