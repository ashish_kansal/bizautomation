<%@ Page Language="vb" AspCompat="true"  AutoEventWireup="false" CodeBehind="frmCustomReport.aspx.vb" Inherits="BACRMPortal.frmCustomReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
 <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
   	<asp:Table Runat="server" ID="tblReport" width="100%" BorderWidth="1" cellspacing="0" 
			cellpadding="0" BorderColor="Black" Height="300" CssClass="aspTable">
			<asp:TableRow VerticalAlign="Top">
			    <asp:TableCell HorizontalAlign="Center">
			        <table class="normal7">
			            <tr>
			                <td align="right">
			                    Report Name : 
			                </td>
			                <td align="left">
			                     <asp:Label runat="server" ID="lblReportName" CssClass="normal7"></asp:Label>
			                </td>
			            </tr>
			            <tr>
			                <td align="right">
			                    Report Description :
			                </td>
			                <td align="left">
			                    <asp:Label runat="server" ID="lblReportDesc" CssClass="normal7"></asp:Label>
			                </td>
			            </tr>
			            <tr>
			                <td align="right">
    			            	Report Generated Date :	            
			                </td>
			                <td align="left">
			                    <asp:Label runat="server" ID="lblGeneratedOn" CssClass="normal7"></asp:Label>        
			                </td>
			            </tr>
			        </table>
			    </asp:TableCell>
			</asp:TableRow>
			<asp:TableRow  VerticalAlign="Top">
			<asp:TableCell>
			
			    <asp:datagrid id="dgReport" AllowSorting="true" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="false"
				    BorderColor="white">
				     <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
				    <ItemStyle CssClass="is"></ItemStyle>
				    <HeaderStyle CssClass="hs1"></HeaderStyle>
				    <FooterStyle CssClass="hs" />
			    </asp:datagrid>
			</asp:TableCell>			    
			</asp:TableRow >
			<asp:TableRow>
			    <asp:TableCell>
			        <asp:Table Runat="server" ID="tblSummaryGrid" width="100%" BorderWidth="1" cellspacing="1" 
	                    cellpadding="0"  BorderColor="Black" ></asp:Table> 
			    </asp:TableCell>
			</asp:TableRow>
		</asp:Table>
    </form>
</body>
</html>
