Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.common
Imports Aspose.Pdf
Imports System.IO
Imports System.Net

Partial Public Class frmCustomReport : Inherits BACRMPage

    Dim ReportId As Long
    Dim DomainId As Long
    Dim UserContactId As Long
    Dim DateFormat As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ReportId = GetQueryStringVal( "ReportId")
            DomainId = GetQueryStringVal( "DomainId")
            UserContactId = GetQueryStringVal( "UserContactId")
            If Not IsPostBack Then BindDataGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindDataGrid()
        Try
            Dim ObjCommon As New CCommon
            Dim objReports As New CustomReports
            Dim dttable As DataTable
            Dim dtDomain As DataTable
            Dim GrpFliter As Integer
            ObjCommon.DomainID = DomainId
            dtDomain = ObjCommon.GetDomainDetails()
            If dtDomain.Rows.Count > 0 Then
                DateFormat = dtDomain.Rows(0).Item("vcDateFormat")
            Else : DateFormat = "MM/DD/YYYY"
            End If
            'Getting the Report Details
            objReports.ReportID = ReportId
            objReports.DomainID = DomainId
            dttable = objReports.getReportDetails()
            GrpFliter = IIf(dttable.Rows(0).Item("varGrpFlt") = "", 0, dttable.Rows(0).Item("varGrpFlt"))
            lblReportDesc.Text = IIf(IsDBNull(dttable.Rows(0).Item("vcReportDescription")), "", dttable.Rows(0).Item("vcReportDescription"))
            lblReportName.Text = IIf(IsDBNull(dttable.Rows(0).Item("vcReportName")), "", dttable.Rows(0).Item("vcReportName"))
            lblGeneratedOn.Text = FormattedDateFromDate(Now, DateFormat)
            If dttable.Rows(0).Item("bitGridType") = True Then
                If dttable.Rows(0).Item("textQuery") <> "" Then

                    Dim objCustomReports As New CustomReports
                    'Dim bField As BoundField
                    objCustomReports.UserCntID = UserContactId
                    objCustomReports.DomainID = DomainId
                    objCustomReports.DynamicQuery = dttable.Rows(0).Item("textQuery")
                    objCustomReports.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    Dim ds As DataSet
                    ds = objCustomReports.ExecuteDynamicSql()
                    If ds.Tables.Count = 2 Then
                        dgReport.ShowFooter = True
                    Else : dgReport.ShowFooter = False
                    End If
                    dgReport.AutoGenerateColumns = False
                    Dim icol As Integer = 0
                    For Each column As DataColumn In ds.Tables(0).Columns
                        If Not column.ColumnName.Contains("##") Then
                            Dim bc1 As New BoundColumn()
                            bc1.DataField = column.ColumnName
                            bc1.HeaderText = "<font color=white>" & column.ColumnName & "</font>"
                            If ds.Tables.Count = 2 Then

                                Dim tbF As DataTable = ds.Tables(1)
                                If tbF.Rows.Count > 0 Then
                                    If icol = 0 Then
                                        If tbF.Columns.Contains("RecCount") Then
                                            bc1.FooterText = bc1.FooterText & "<font color=White> Total Records =" & IIf(IsDBNull(tbF.Rows(0).Item("RecCount")), 0, tbF.Rows(0).Item("RecCount")) & "</font>&nbsp;"
                                        End If
                                    End If
                                    If tbF.Columns.Contains("Sum" & column.ColumnName) Then
                                        bc1.FooterText = bc1.FooterText & "<font color=White> Total =" & Format(IIf(IsDBNull(tbF.Rows(0).Item("Sum" & column.ColumnName)), 0, tbF.Rows(0).Item("Sum" & column.ColumnName)), "f") & "</font>"
                                    End If
                                    If tbF.Columns.Contains("Avg" & column.ColumnName) Then
                                        bc1.FooterText = bc1.FooterText & "<font color=White> Avg=" & Format(IIf(IsDBNull(tbF.Rows(0).Item("Avg" & column.ColumnName)), 0, tbF.Rows(0).Item("Avg" & column.ColumnName)), "f") & "</font>"
                                    End If
                                    If tbF.Columns.Contains("Max" & column.ColumnName) Then
                                        bc1.FooterText = bc1.FooterText & "<font color=White> Max=" & Format(IIf(IsDBNull(tbF.Rows(0).Item("Max" & column.ColumnName)), 0, tbF.Rows(0).Item("Max" & column.ColumnName)), "f") & "</font>"
                                    End If
                                    If tbF.Columns.Contains("Min" & column.ColumnName) Then
                                        bc1.FooterText = bc1.FooterText & "<font color=White> Min=" & Format(IIf(IsDBNull(tbF.Rows(0).Item("Min" & column.ColumnName)), 0, tbF.Rows(0).Item("Min" & column.ColumnName)), "f") & "</font>"
                                    End If
                                End If
                            Else
                            End If
                            dgReport.Columns.Add(bc1)
                            icol = icol + 1
                        End If
                    Next
                    dgReport.DataSource = ds.Tables(0)
                    dgReport.DataBind()
                End If
            Else
                If dttable.Rows(0).Item("textQueryGrp") <> "" Then
                    Dim SelGroupFldName As String
                    Dim SelGroupFldValue As String = dttable.Rows(0).Item("varGrpfld")
                    Dim objCustomReports As New CustomReports

                    objCustomReports.strGrpfld = dttable.Rows(0).Item("varGrpfld").ToString.Split("~")(1)
                    objCustomReports.GroupId = dttable.Rows(0).Item("varGrpfld").ToString.Split("~")(2)
                    SelGroupFldName = objCustomReports.getFieldName()
                    objCustomReports.UserCntID = UserContactId
                    objCustomReports.DomainID = DomainId
                    objCustomReports.DynamicQuery = dttable.Rows(0).Item("textQueryGrp")
                    objCustomReports.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    Dim ds As DataSet
                    ds = objCustomReports.ExecuteDynamicSql()

                    Dim dtGroupTable As DataTable
                    Dim dtFooter As DataTable
                    dtGroupTable = ds.Tables(0)
                    If ds.Tables.Count = 2 Then dtFooter = ds.Tables(1)
                    Dim dtOrderTable As DataTable
                    Dim dtsummationTable As DataTable
                    objCustomReports.ReportID = ReportId
                    dtOrderTable = objCustomReports.getReportOrderList()
                    dtsummationTable = objCustomReports.getReportSummationList
                    Dim TRH As New TableRow
                    Dim TR As TableRow
                    Dim TD As TableCell
                    Dim strfields(dtOrderTable.Rows.Count - 1) As String
                    Dim i As Integer
                    For Each row As DataRow In dtOrderTable.Rows
                        TD = New TableCell
                        TD.Text = "<font color=white>" & row.Item("vcFieldText") & "</font>"
                        TRH.CssClass = "hs"
                        TRH.Cells.Add(TD)
                        strfields(i) = row.Item("vcFieldText")
                        i = i + 1
                    Next
                    tblSummaryGrid.Rows.Add(TRH)

                    For Each row As DataRow In dtGroupTable.Rows
                        TR = New TableRow
                        TD = New TableCell
                        'TD.Text = IIf(IsDBNull(row.Item(SelGroupFldName)), "-", row.Item(SelGroupFldName))
                        Select Case GrpFliter
                            Case 3
                                If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                    TD.Text = row.Item(SelGroupFldName)
                                Else : TD.Text = "-"
                                End If
                            Case 5
                                If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                    TD.Text = row.Item(SelGroupFldName)
                                Else : TD.Text = "-"
                                End If
                            Case 9
                                Dim week As Integer
                                week = getWeeknumber.Weeknumbers(Now)
                                'week = datepart(weNow
                                If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                    If row.Item(SelGroupFldName) = week Then
                                        TD.Text = "This week"
                                    Else : TD.Text = "Last week"
                                    End If
                                End If
                                'TD.Text = IIf(IsDBNull(row.Item(SelGroupFldName)), "-", row.Item(SelGroupFldName))
                            Case 10
                                Dim month1 As Integer
                                month1 = DatePart(DateInterval.Month, Now)
                                If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                    If row.Item(SelGroupFldName) = month1 Then
                                        TD.Text = "This Month"
                                    Else : TD.Text = "Last Month"
                                    End If
                                End If
                                'TD.Text = IIf(IsDBNull(row.Item(SelGroupFldName)), "-", row.Item(SelGroupFldName))
                            Case 11
                                Dim year1 As Integer
                                year1 = DatePart(DateInterval.Year, Now)
                                If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                    If row.Item(SelGroupFldName) = year1 Then
                                        TD.Text = "This Year"
                                    Else : TD.Text = "Last Year"
                                    End If
                                End If
                            Case 12
                                Dim quarter As Integer
                                quarter = DatePart(DateInterval.Quarter, Now)
                                'week = datepart(weNow
                                If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                    If row.Item(SelGroupFldName) = quarter Then
                                        TD.Text = "This Quarter"
                                    Else : TD.Text = "Last Quarter"
                                    End If
                                End If
                            Case Else
                                TD.Text = IIf(IsDBNull(row.Item(SelGroupFldName)), "-", row.Item(SelGroupFldName))
                        End Select
                        Dim dvSumm As DataView
                        dvSumm = dtsummationTable.DefaultView
                        dvSumm.RowFilter = "vcFieldName = 'chkRecCount'"
                        For Each drv As DataRowView In dvSumm
                            If drv.Item("bitSum") = True Then TD.Text = TD.Text & " ( " & row.Item("RecCount") & " )"
                        Next

                        Dim ifields As Integer = 0
                        Dim str As String = ""

                        If dtGroupTable.Columns.Contains("sum" & strfields(0)) Then str = str & " Total = " & Format(IIf(IsDBNull(row.Item("sum" & strfields(0))), 0, row.Item("sum" & strfields(0))), "f")
                        If dtGroupTable.Columns.Contains("Avg" & strfields(0)) Then str = str & " Avg = " & Format(IIf(IsDBNull(row.Item("Avg" & strfields(0))), 0, row.Item("Avg" & strfields(0))), "f")
                        If dtGroupTable.Columns.Contains("Max" & strfields(0)) Then str = str & " Max =" & Format(IIf(IsDBNull(row.Item("Max" & strfields(0))), 0, row.Item("Max" & strfields(0))), "f")
                        If dtGroupTable.Columns.Contains("Min" & strfields(0)) Then str = str & " Min = " & Format(IIf(IsDBNull(row.Item("Min" & strfields(0))), 0, row.Item("Min" & strfields(0))), "f")

                        TD.Text = TD.Text & str
                        TR.Cells.Add(TD)

                        For ifields = 1 To strfields.Length - 1
                            TD = New TableCell
                            Dim str1 As String = ""

                            If dtGroupTable.Columns.Contains("sum" & strfields(ifields)) Then str1 = str1 & " Total = " & Format(IIf(IsDBNull(row.Item("sum" & strfields(ifields))), 0, row.Item("sum" & strfields(ifields))), "f")
                            If dtGroupTable.Columns.Contains("Avg" & strfields(ifields)) Then str1 = str1 & " Avg = " & Format(IIf(IsDBNull(row.Item("Avg" & strfields(ifields))), 0, row.Item("Avg" & strfields(ifields))), "f")
                            If dtGroupTable.Columns.Contains("Max" & strfields(ifields)) Then str1 = str1 & " Max = " & Format(IIf(IsDBNull(row.Item("Max" & strfields(ifields))), 0, row.Item("Max" & strfields(ifields))), "f")
                            If dtGroupTable.Columns.Contains("Min" & strfields(ifields)) Then str1 = str1 & " Min = " & Format(IIf(IsDBNull(row.Item("Min" & strfields(ifields))), 0, row.Item("Min" & strfields(ifields))), "f")

                            TD.Text = str1
                            TR.Cells.Add(TD)
                        Next
                        TR.CssClass = "reportG"
                        tblSummaryGrid.Rows.Add(TR)
                        Dim SqlSummGrid1 As New System.Text.StringBuilder

                        If SelGroupFldValue.Split("~")(4) = 1 Then
                            Dim strCustFunVal As String
                            Dim tblAlias, CFWColumn, strCustFieldID As String

                            Select Case SelGroupFldValue.Split("~")(2)
                                Case 6
                                    strCustFunVal = ",4,ACI.numContactId"
                                    tblAlias = "CFW_FLD_Values_Cont"
                                Case 7, 8
                                    strCustFunVal = ",1,DivisionMaster.numDivisionId"
                                    tblAlias = "CFW_FLD_Values"
                                Case 10, 11, 12, 13, 14
                                    strCustFunVal = ",1,ProMas.numProId"
                                    tblAlias = "CFW_FLD_Values"
                                Case 15, 16, 17
                                    strCustFunVal = ",3,Cases.numCaseId"
                                    tblAlias = "CFW_FLD_Values_Case"
                                Case 18, 19, 20, 21, 22, 23, 25, 26
                                    strCustFunVal = ",6,OppMas.numOppid"
                                    tblAlias = "CFW_Fld_Values_Opp"
                                Case 9
                                    strCustFunVal = ",11,ProMas.numProId"
                                    tblAlias = "CFW_FLD_Values_Pro"
                                Case 27
                                    strCustFunVal = ",5,Item.numItemCode"
                                    tblAlias = "CFW_FLD_Values_Item"
                                Case Else
                                    strCustFunVal = ",1,DivisionMaster.numDivisionId"
                                    tblAlias = "CFW_FLD_Values"
                            End Select

                            strCustFieldID = SelGroupFldValue.Split("~")(1).Split("d")(1)
                            CFWColumn = tblAlias & ".[" & strCustFieldID & "]"

                            If SelGroupFldValue.Split("~")(3) = "Date Field" Or SelGroupFldValue.Split("~")(3) = "Date Field U" Then
                                Select Case GrpFliter
                                    Case 0
                                        SqlSummGrid1.Append("  and  FormatedDateFromDate(" & CFWColumn & ",@numDomainId)")
                                        If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        SqlSummGrid1.Append("  and  year(" & CFWColumn & ")")
                                        If Not IsDBNull(row.Item("year")) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item("year") & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    Case 1
                                        SqlSummGrid1.Append(" and   FormatedDateFromDate(get_week_start(" & CFWColumn & "),@numDomainID)+' - '+ FormatedDateFromDate(get_week_end( " & CFWColumn & "),@numDomainID)")
                                        If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        SqlSummGrid1.Append("  and  year(" & CFWColumn & ")")
                                        If Not IsDBNull(row.Item("year")) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item("year") & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    Case 2
                                        SqlSummGrid1.Append(" and   DATENAME(month," & CFWColumn & ")+'-'+convert(varchar(4),year(" & CFWColumn & "))")
                                        If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        SqlSummGrid1.Append("  and  year(" & CFWColumn & ")")
                                        If Not IsDBNull(row.Item("year")) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item("year") & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    Case 3
                                        SqlSummGrid1.Append(" and   fn_getCustomDataValue(DATEPART(q," & CFWColumn & "),13)+'-'+convert(varchar(4),year(" & CFWColumn & "))")
                                        If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        SqlSummGrid1.Append("  and  year(" & CFWColumn & ")")
                                        If Not IsDBNull(row.Item("year")) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item("year") & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    Case 4
                                        SqlSummGrid1.Append("  and  DATEPART(year," & CFWColumn & ")")
                                        If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        SqlSummGrid1.Append("  and  year(" & CFWColumn & ")")
                                        If Not IsDBNull(row.Item("year")) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item("year") & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    Case 5
                                        SqlSummGrid1.Append(" and  fn_getCustomDataValue(GetFiscalQuarter(" & CFWColumn & ",@numDomainId),13)")
                                        SqlSummGrid1.Append(" +' '+FormatedDateFromDate(GetFQDate(" & CFWColumn & ", GetFiscalQuarter(" & CFWColumn & ",@numDomainId),'S',@numDomainId),@numDomainId)")
                                        SqlSummGrid1.Append("+'- '+FormatedDateFromDate(GetFQDate(" & CFWColumn & ", GetFiscalQuarter(" & CFWColumn & ",@numDomainId),'E',@numDomainId),@numDomainId) ")
                                        ' SqlSummGrid1.Append(" and fn_getCustomDataValue(GetFiscalQuarter(" & CFWColumn & ",@numDomainId),13)")
                                        If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        SqlSummGrid1.Append(" and FormatedDateFromDate(GetFiscalStartDate(getFiscalYear(" & CFWColumn & ",@numDomainId),@numDomainId),@numDomainId)")
                                        If Not IsDBNull(row.Item("Fstart")) Then
                                            SqlSummGrid1.Append(" = '" & row.Item("Fstart") & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    Case 6
                                        SqlSummGrid1.Append(" and  FormatedDateFromDate(GetFiscalStartDate(GetFiscalyear(" & CFWColumn & ",@numDomainId),@numDomainId),@numDomainId)")
                                        SqlSummGrid1.Append(" +'-'+ FormatedDateFromDate(dateadd(year,1,GetFiscalStartDate(GetFiscalyear(" & CFWColumn & ",@numDomainId),@numDomainId)),@numDomainId)")
                                        If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    Case 7
                                        SqlSummGrid1.Append(" and   DATENAME(month," & CFWColumn & ")+'-'+convert(varchar(4),year(" & CFWColumn & "))")
                                        If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        SqlSummGrid1.Append("and year(" & CFWColumn & ") = year(getdate())")
                                    Case 8
                                        SqlSummGrid1.Append("  and  FormatedDateFromDate(" & CFWColumn & ",@numDomainId)")
                                        If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        SqlSummGrid1.Append("  and  year(" & CFWColumn & ")")
                                        If Not IsDBNull(row.Item("year")) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item("year") & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        ' SqlSummGrid1.Append("and year(ACI." & CFWColumn & ") = year(getdate())")
                                    Case 9
                                        SqlSummGrid1.Append("  and  DATEPART(week," & CFWColumn & ")")
                                        If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        SqlSummGrid1.Append("  and  year(" & CFWColumn & ")")
                                        If Not IsDBNull(row.Item("year")) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item("year") & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        ' SqlSummGrid1.Append("and year(" & CFWColumn & ") = year(getdate())")
                                        SqlSummGrid1.Append("and DATEPART(week," & CFWColumn & ") in (datepart(week,getdate())-1,datepart(week,getdate()))")
                                    Case 10
                                        SqlSummGrid1.Append("  and  DATEPART(month," & CFWColumn & ")")
                                        If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        SqlSummGrid1.Append("  and year(" & CFWColumn & ")")
                                        If Not IsDBNull(row.Item("year")) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item("year") & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        ' SqlSummGrid1.Append("and year(" & CFWColumn & ") = year(getdate())")
                                        SqlSummGrid1.Append("and month(" & CFWColumn & ") in (datepart(month,getdate())-1,datepart(month,getdate()))")
                                    Case 11
                                        SqlSummGrid1.Append("  and  DATEPART(year," & CFWColumn & ")")
                                        If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        SqlSummGrid1.Append("  and  year(" & CFWColumn & ")")
                                        If Not IsDBNull(row.Item("year")) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item("year") & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        'SqlSummGridFilter.Append("and year(ACI." & CFWColumn & ") = year(getdate())")
                                        ' SqlSummGrid1.Append("and year(" & CFWColumn & ") in (datepart(year,getdate())-1,datepart(year,getdate()))")
                                    Case 12
                                        SqlSummGrid1.Append("  and  DATEPART(q," & CFWColumn & ")")
                                        If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        SqlSummGrid1.Append("  and  year(" & CFWColumn & ")")
                                        If Not IsDBNull(row.Item("year")) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item("year") & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        SqlSummGrid1.Append("and year(" & CFWColumn & ") = year(getdate())")
                                        SqlSummGrid1.Append("and DATEPART(q," & CFWColumn & ") in (datepart(q,getdate())-1,datepart(q,getdate()))")
                                End Select
                                ' SqlSummGrid1.Append("  FormatedDateFromDate(" & CFWColumn & ",@numDomainId) as  '" & SelGroupFldName & "'")
                                ' SqlSummGrid2.Append(" group by FormatedDateFromDate(" & CFWColumn & ",@numDomainId) ")
                            ElseIf SelGroupFldValue.Split("~")(3) = "Drop Down List" Then
                                SqlSummGrid1.Append("  and fn_GetListItemName(" & CFWColumn & ")")
                                If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                    SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                Else : SqlSummGrid1.Append(" is null")
                                End If
                            ElseIf SelGroupFldValue.Split("~")(3) = "Customdata" Then
                                SqlSummGrid1.Append(" and fn_getCustomDataValue(" & CFWColumn & "," & SelGroupFldValue.Split("~")(5) & ")")
                                If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                    SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                Else : SqlSummGrid1.Append(" is null")
                                End If
                            ElseIf SelGroupFldValue.Split("~")(3) = "Contact" Then
                                SqlSummGrid1.Append("  and fn_GetContactName(" & CFWColumn & ")")
                                If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                    SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                Else : SqlSummGrid1.Append(" is null")
                                End If
                            ElseIf SelGroupFldValue.Split("~")(3) = "Company" Then
                                SqlSummGrid1.Append("  and fn_GetComapnyName(" & CFWColumn & ")")
                                If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                    SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                Else : SqlSummGrid1.Append(" is null")
                                End If
                            ElseIf SelGroupFldValue.Split("~")(3) = "Group" Then
                                SqlSummGrid1.Append("  and fn_GetGroupName(" & CFWColumn & ")")
                                If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                    SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                Else : SqlSummGrid1.Append(" is null")
                                End If
                            Else
                                SqlSummGrid1.Append(" and " & CFWColumn)
                                If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                    SqlSummGrid1.Append(" = '" & row.Item(SelGroupFldName) & "'")
                                Else : SqlSummGrid1.Append(" is null")
                                End If
                            End If
                            ' SqlSummGrid1.Append(" fn_GetListItemName(ACI." & SelGroupFldValue.Split("~")(1) & ") as  '" & ddlGrp1List.SelectedItem.Text & "'")
                        Else
                            If SelGroupFldValue.Split("~")(3) = "Date Field" Or SelGroupFldValue.Split("~")(3) = "Date Field U" Then
                                Dim Field As String
                                If SelGroupFldValue.Split("~")(3) = "Date Field" Then
                                    Field = SelGroupFldValue.Split("~")(1)
                                ElseIf SelGroupFldValue.Split("~")(3) = "Date Field U" Then
                                    Field = "DateAdd(minute,-@ClientTimeZoneOffset," & SelGroupFldValue.Split("~")(1) & ")"
                                End If
                                Select Case GrpFliter
                                    Case 0
                                        SqlSummGrid1.Append("  and  FormatedDateFromDate(" & Field & ",@numDomainId)")
                                        If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        SqlSummGrid1.Append("  and  year(" & Field & ")")
                                        If Not IsDBNull(row.Item("year")) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item("year") & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    Case 1
                                        SqlSummGrid1.Append(" and   FormatedDateFromDate(get_week_start(" & Field & "),@numDomainID)+' - '+ FormatedDateFromDate(get_week_end( " & Field & "),@numDomainID)")
                                        If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        SqlSummGrid1.Append("  and  year(" & Field & ")")
                                        If Not IsDBNull(row.Item("year")) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item("year") & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    Case 2
                                        SqlSummGrid1.Append(" and   DATENAME(month," & Field & ")+'-'+convert(varchar(4),year(" & Field & "))")
                                        If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        SqlSummGrid1.Append("  and  year(" & Field & ")")
                                        If Not IsDBNull(row.Item("year")) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item("year") & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    Case 3
                                        SqlSummGrid1.Append(" and   fn_getCustomDataValue(DATEPART(q," & Field & "),13)+'-'+convert(varchar(4),year(" & Field & "))")
                                        If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        SqlSummGrid1.Append("  and  year(" & Field & ")")
                                        If Not IsDBNull(row.Item("year")) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item("year") & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    Case 4
                                        SqlSummGrid1.Append("  and  DATEPART(year," & Field & ")")
                                        If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        SqlSummGrid1.Append("  and  year(" & Field & ")")
                                        If Not IsDBNull(row.Item("year")) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item("year") & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    Case 5
                                        SqlSummGrid1.Append(" and  fn_getCustomDataValue(GetFiscalQuarter(" & Field & ",@numDomainId),13)")
                                        SqlSummGrid1.Append(" +' '+FormatedDateFromDate(GetFQDate(" & Field & ", GetFiscalQuarter(" & Field & ",@numDomainId),'S',@numDomainId),@numDomainId)")
                                        SqlSummGrid1.Append("+'- '+FormatedDateFromDate(GetFQDate(" & Field & ", GetFiscalQuarter(" & Field & ",@numDomainId),'E',@numDomainId),@numDomainId) ")
                                        ' SqlSummGrid1.Append(" and fn_getCustomDataValue(GetFiscalQuarter(" & Field & ",@numDomainId),13)")
                                        If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        SqlSummGrid1.Append(" and FormatedDateFromDate(GetFiscalStartDate(getFiscalYear(" & Field & ",@numDomainId),@numDomainId),@numDomainId)")
                                        If Not IsDBNull(row.Item("Fstart")) Then
                                            SqlSummGrid1.Append(" = '" & row.Item("Fstart") & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    Case 6
                                        SqlSummGrid1.Append(" and  FormatedDateFromDate(GetFiscalStartDate(GetFiscalyear(" & Field & ",@numDomainId),@numDomainId),@numDomainId)")
                                        SqlSummGrid1.Append(" +'-'+ FormatedDateFromDate(dateadd(year,1,GetFiscalStartDate(GetFiscalyear(" & Field & ",@numDomainId),@numDomainId)),@numDomainId)")
                                        If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                    Case 7
                                        SqlSummGrid1.Append(" and   DATENAME(month," & Field & ")+'-'+convert(varchar(4),year(" & Field & "))")
                                        If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        SqlSummGrid1.Append("and year(" & Field & ") = year(getdate())")
                                    Case 8
                                        SqlSummGrid1.Append("  and  FormatedDateFromDate(" & Field & ",@numDomainId)")
                                        If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        SqlSummGrid1.Append("  and  year(" & Field & ")")
                                        If Not IsDBNull(row.Item("year")) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item("year") & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        ' SqlSummGrid1.Append("and year(ACI." & Field & ") = year(getdate())")
                                    Case 9
                                        SqlSummGrid1.Append("  and  DATEPART(week," & Field & ")")
                                        If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        SqlSummGrid1.Append("  and  year(" & Field & ")")
                                        If Not IsDBNull(row.Item("year")) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item("year") & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        ' SqlSummGrid1.Append("and year(" & Field & ") = year(getdate())")
                                        SqlSummGrid1.Append("and DATEPART(week," & Field & ") in (datepart(week,getdate())-1,datepart(week,getdate()))")
                                    Case 10
                                        SqlSummGrid1.Append("  and  DATEPART(month," & Field & ")")
                                        If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        SqlSummGrid1.Append("  and year(" & Field & ")")
                                        If Not IsDBNull(row.Item("year")) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item("year") & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        ' SqlSummGrid1.Append("and year(" & Field & ") = year(getdate())")
                                        SqlSummGrid1.Append("and month(" & Field & ") in (datepart(month,getdate())-1,datepart(month,getdate()))")
                                    Case 11
                                        SqlSummGrid1.Append("  and  DATEPART(year," & Field & ")")
                                        If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        SqlSummGrid1.Append("  and  year(" & Field & ")")
                                        If Not IsDBNull(row.Item("year")) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item("year") & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        'SqlSummGridFilter.Append("and year(ACI." & Field & ") = year(getdate())")
                                        ' SqlSummGrid1.Append("and year(" & Field & ") in (datepart(year,getdate())-1,datepart(year,getdate()))")
                                    Case 12
                                        SqlSummGrid1.Append("  and  DATEPART(q," & Field & ")")
                                        If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        SqlSummGrid1.Append("  and  year(" & Field & ")")
                                        If Not IsDBNull(row.Item("year")) Then
                                            SqlSummGrid1.Append(" ilike '" & row.Item("year") & "'")
                                        Else : SqlSummGrid1.Append(" is null")
                                        End If
                                        SqlSummGrid1.Append("and year(" & Field & ") = year(getdate())")
                                        SqlSummGrid1.Append("and DATEPART(q," & Field & ") in (datepart(q,getdate())-1,datepart(q,getdate()))")
                                End Select
                                ' SqlSummGrid1.Append("  FormatedDateFromDate(" & Field & ",@numDomainId) as  '" & ddlGrp1List.SelectedItem.Text & "'")
                                ' SqlSummGrid2.Append(" group by FormatedDateFromDate(" & Field & ",@numDomainId) ")
                            ElseIf SelGroupFldValue.Split("~")(3) = "Drop Down List" Then
                                SqlSummGrid1.Append("  and fn_GetListItemName(" & SelGroupFldValue.Split("~")(1) & ")")
                                If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                    SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                Else : SqlSummGrid1.Append(" is null")
                                End If
                            ElseIf SelGroupFldValue.Split("~")(3) = "Customdata" Then
                                SqlSummGrid1.Append(" and fn_getCustomDataValue(" & SelGroupFldValue.Split("~")(1) & "," & SelGroupFldValue.Split("~")(5) & ")")
                                If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                    SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                Else : SqlSummGrid1.Append(" is null")
                                End If
                            ElseIf SelGroupFldValue.Split("~")(3) = "Contact" Then
                                SqlSummGrid1.Append("  and fn_GetContactName(" & SelGroupFldValue.Split("~")(1) & ")")
                                If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                    SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                Else : SqlSummGrid1.Append(" is null")
                                End If
                            ElseIf SelGroupFldValue.Split("~")(3) = "Company" Then
                                SqlSummGrid1.Append("  and fn_GetComapnyName(" & SelGroupFldValue.Split("~")(1) & ")")
                                If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                    SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                Else : SqlSummGrid1.Append(" is null")
                                End If
                            ElseIf SelGroupFldValue.Split("~")(3) = "Group" Then
                                SqlSummGrid1.Append("  and fn_GetGroupName(" & SelGroupFldValue.Split("~")(1) & ")")
                                If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                    SqlSummGrid1.Append(" ilike '" & row.Item(SelGroupFldName) & "'")
                                Else : SqlSummGrid1.Append(" is null")
                                End If
                            Else
                                SqlSummGrid1.Append(" and " & SelGroupFldValue.Split("~")(1))
                                If Not IsDBNull(row.Item(SelGroupFldName)) Then
                                    SqlSummGrid1.Append(" = '" & row.Item(SelGroupFldName) & "'")
                                Else : SqlSummGrid1.Append(" is null")
                                End If
                            End If
                        End If
                        Dim sql As String = " "
                        sql = dttable.Rows(0).Item("textQuery") & " " & SqlSummGrid1.ToString
                        objCustomReports.DynamicQuery = sql
                        ' Dim ds As DataSet
                        objCustomReports.UserCntID = UserContactId
                        objCustomReports.DomainID = DomainId
                        objCustomReports.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

                        ds = objCustomReports.ExecuteDynamicSql()
                        Dim dtDtlTable As DataTable
                        dtDtlTable = ds.Tables(0)
                        Dim iTr As Integer = 0
                        For Each rowDtl As DataRow In dtDtlTable.Rows
                            TR = New TableRow
                            For i = 0 To strfields.Length - 1
                                TD = New TableCell
                                TD.Text = IIf(IsDBNull(rowDtl.Item(strfields(i))), "", rowDtl.Item(strfields(i)))
                                If iTr Mod 2 = 0 Then
                                    TR.CssClass = "report1"
                                Else : TR.CssClass = "report2"
                                End If
                                TR.Cells.Add(TD)
                            Next
                            tblSummaryGrid.Rows.Add(TR)
                            iTr = iTr + 1
                        Next
                    Next
                    If Not dtFooter Is Nothing Then
                        'Dim dtFooter As DataTable = dtFooterTable
                        Dim TRF As New TableRow
                        Dim TRfo As TableRow
                        Dim TDfo As TableCell
                        'Dim strfields() As String = txtOrderListText.Text.Split("|")
                        Dim ifo As Integer

                        For ifo = 0 To strfields.Length - 1
                            Dim str As String = " "
                            If ifo = 0 Then
                                If dtFooter.Columns.Contains("RecCount") Then str = str & " Total Records = " & IIf(IsDBNull(dtFooter.Rows(0).Item("RecCount")), 0, dtFooter.Rows(0).Item("RecCount"))
                            End If

                            If dtFooter.Columns.Contains("sum" & strfields(ifo)) Then str = str & " Total = " & Format(IIf(IsDBNull(dtFooter.Rows(0).Item("sum" & strfields(ifo))), 0, dtFooter.Rows(0).Item("sum" & strfields(ifo))), "f")
                            If dtFooter.Columns.Contains("Avg" & strfields(ifo)) Then str = str & " Avg = " & Format(IIf(IsDBNull(dtFooter.Rows(0).Item("Avg" & strfields(ifo))), 0, dtFooter.Rows(0).Item("Avg" & strfields(ifo))), "f")
                            If dtFooter.Columns.Contains("Max" & strfields(ifo)) Then str = str & " Max = " & Format(IIf(IsDBNull(dtFooter.Rows(0).Item("Max" & strfields(ifo))), 0, dtFooter.Rows(0).Item("Max" & strfields(ifo))), "f")
                            If dtFooter.Columns.Contains("Min" & strfields(ifo)) Then str = str & " Min = " & Format(IIf(IsDBNull(dtFooter.Rows(0).Item("Min" & strfields(ifo))), 0, dtFooter.Rows(0).Item("Min" & strfields(ifo))), "f")

                            TDfo = New TableCell
                            TDfo.Text = TDfo.Text & "<font color=white>" & str & "</font>"
                            TRF.CssClass = "hs"
                            TRF.Cells.Add(TDfo)
                        Next
                        tblSummaryGrid.Rows.Add(TRF)
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class

Public Class getWeeknumber

    Shared Function Weeknumbers(ByVal inDate As DateTime) As Integer
        Try
            Const JAN As Integer = 1
            Const DEC As Integer = 12
            Const LASTDAYOFDEC As Integer = 31
            Const FIRSTDAYOFJAN As Integer = 1
            Const THURSDAY As Integer = 4
            Dim ThursdayFlag As Boolean = False

            ' Get the day number since the beginning of the year
            Dim DayOfYear As Integer = inDate.DayOfYear

            ' Get the numeric weekday of the first day of the
            ' year (using sunday as FirstDay)
            Dim StartWeekDayOfYear As Integer = _
               DirectCast(New DateTime(inDate.Year, JAN, FIRSTDAYOFJAN).DayOfWeek, Integer)
            Dim EndWeekDayOfYear As Integer = _
                DirectCast(New DateTime(inDate.Year, DEC, LASTDAYOFDEC).DayOfWeek, Integer)

            ' Compensate for the fact that we are using monday
            ' as the first day of the week
            If StartWeekDayOfYear = 0 Then StartWeekDayOfYear = 7
            If EndWeekDayOfYear = 0 Then EndWeekDayOfYear = 7

            ' Calculate the number of days in the first and last week
            Dim DaysInFirstWeek As Integer = 8 - StartWeekDayOfYear
            Dim DaysInLastWeek As Integer = 8 - EndWeekDayOfYear

            ' If the year either starts or ends on a thursday it will have a 53rd week
            If StartWeekDayOfYear = THURSDAY OrElse EndWeekDayOfYear = THURSDAY Then ThursdayFlag = True

            ' We begin by calculating the number of FULL weeks between the start of the year and
            ' our date. The number is rounded up, so the smallest possible value is 0.
            Dim FullWeeks As Integer = _
                CType(Math.Ceiling((DayOfYear - DaysInFirstWeek) / 7), Integer)

            Dim WeekNumber As Integer = FullWeeks

            ' If the first week of the year has at least four days, then the actual week number for our date
            ' can be incremented by one.
            If DaysInFirstWeek >= THURSDAY Then WeekNumber = WeekNumber + 1

            ' If week number is larger than week 52 (and the year doesn't either start or end on a thursday)
            ' then the correct week number is 1.
            If WeekNumber > 52 AndAlso Not ThursdayFlag Then WeekNumber = 1

            'If week number is still 0, it means that we are trying to evaluate the week number for a
            'week that belongs in the previous year (since that week has 3 days or less in our date's year).
            'We therefore make a recursive call using the last day of the previous year.
            If WeekNumber = 0 Then WeekNumber = Weeknumbers(New DateTime(inDate.Year - 1, DEC, LASTDAYOFDEC))
            Return WeekNumber
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class