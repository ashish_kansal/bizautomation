Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Common
Partial Public Class frmFollowUpHstr
    Inherits BACRMPage

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not IsPostBack Then BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindGrid()
        Try
            Dim objLeads As New CLeads
            Dim dtFollowHstr As DataTable
            objLeads.DivisionID = GetQueryStringVal( "Div")
            dtFollowHstr = objLeads.GetFollowUpHstr
            dgFollow.DataSource = dtFollowHstr
            dgFollow.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function ReturnName(ByVal bintCreatedDate As Date) As String
        Try
            Dim strCreateDate As String
            strCreateDate = FormattedDateFromDate(bintCreatedDate, Session("DateFormat"))
            Return strCreateDate
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub dgFollow_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgFollow.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                Dim objLeads As New CLeads
                objLeads.FollowUpHstrID = e.Item.Cells(0).Text
                objLeads.DeleteFollowUpHstr()
                BindGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class