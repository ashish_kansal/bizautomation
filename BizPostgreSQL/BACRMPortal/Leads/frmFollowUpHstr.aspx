<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmFollowUpHstr.aspx.vb" Inherits="BACRMPortal.frmFollowUpHstr" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
		<title>FollowUp History</title>
		<script language="javascript">
		function ShowWindow(Page,q,att) 
		{
			alert(Page)
			if (att=='show')
			{
				document.getElementById(Page).style.visibility = "visible";
				return false;
		
			}
			if (att=='hide')
			{
				document.getElementById(Page).style.visibility = "hidden";
				return false;
		
			}
		}
		</script>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
		<br />
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
		
				<tr>
					<td colSpan="3"><asp:table id="tblMile" Runat="server" BorderWidth="1" CellPadding="0" CellSpacing="0" Width="100%"
							GridLines="None" BorderColor="black">
							<asp:TableRow>
								<asp:TableCell>
									<asp:datagrid id="dgFollow" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
										BorderColor="white">
										<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
										<ItemStyle CssClass="is"></ItemStyle>
										<HeaderStyle CssClass="hs"></HeaderStyle>
										<Columns>
											<asp:BoundColumn Visible="False" DataField="numFollowUpStatusID"></asp:BoundColumn>
											<asp:BoundColumn HeaderText="Follow-Up Status" DataField="vcdata"></asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Changed On">
												<ItemTemplate>
													<%# ReturnName(DataBinder.Eval(Container.DataItem, "bintAddedDate")) %>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn>
												<HeaderTemplate>
													<asp:Button ID="btnHdelete" Runat="server" CssClass="Delete" Text="X"></asp:Button>
												</HeaderTemplate>
												<ItemTemplate>
													<asp:Button ID="btnDelete" Runat="server" CssClass="Delete" Text="X" CommandName="Delete"></asp:Button>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid>
								</asp:TableCell>
							</asp:TableRow>
						</asp:table></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
