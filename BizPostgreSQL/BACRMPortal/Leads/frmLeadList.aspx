<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmLeadList.aspx.vb" Inherits="BACRMPortal.frmLeadList"%>
<%@ Register TagPrefix="menu1" TagName="PartnerPoint" src="../Common/PartnerPoint.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1"    runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Leads</title>
	
		<script language="javascript">
			function OpenSetting()
		{
		    window.open('../Prospects/frmConfCompanyList.aspx?RelId=1','','toolbar=no,titlebar=no,top=200,left=200,width=550,height=370,scrollbars=yes,resizable=yes')
		    return false
		}
		
		 function GoImport()
		{
			window.location.href("../admin/importfromputlook.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=prospects");
			return false;
		}
		
		function DeleteMessage()
		{
			alert("You Are not Authorized to Delete the Selected Record !");
			return false;
		}

		function OpenSelTeam()
		{
		
			window.open("../Forecasting/frmSelectTeams.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=2",'','toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
			return false;
		}
	    function PopupCheck()
		{
		    document.Form1.btnGo.click()
		}
	function DeleteRecord()
		{   
		    var ContactId='';
		      for(var i=2; i<=gvSearch.rows.length; i++)
			{
			    if (i<10)
		        {
		         str='0'+i   
		         }
		         else
		         {
		           str=i
		         }
		    
		        if  (document.all('gvSearch_ctl'+str+'_chk').checked==true)
		       {
		           
		            if (ContactId == '')
		            {
		                ContactId = document.all('gvSearch_ctl'+str+'_lbl1').innerHTML+'~'+document.all('gvSearch_ctl'+str+'_lbl2').innerHTML+'~'+document.all('gvSearch_ctl'+str+'_lbl3').innerHTML
		            }
		            else
		            {
		                 ContactId =ContactId+','+document.all('gvSearch_ctl'+str+'_lbl1').innerHTML+'~'+document.all('gvSearch_ctl'+str+'_lbl2').innerHTML+'~'+document.all('gvSearch_ctl'+str+'_lbl3').innerHTML
		            }
		       } 
			}
			  document.all('txtDelContactIds').value = ContactId
			  
			return true
		}
		    function OpemEmail(a,b)
		{
		   
			window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail=' + a + '&pqwRT=' + b,'','toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')
			return false;
		}
		function OpenContact(a,b)
    {
      
        var str;
        if (b==1)
            {
            str="../pagelayout/frmContact.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=LeadsList&ujfda=tyuu&CntId=" + a;
            }
            else
            {
             str="../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=LeadsList&ft6ty=oiuy&CntId=" + a;
             }
        
            document.location.href=str;
      
    }
		function DeleteRecord1()
		{
			var str;
			if (document.Form1.ddlSort.value==9)
			{
				str='Are you sure, you want to delete the selected organization from favorites?'
			}
			else
			{
				str='Are you sure, you want to delete the selected record?'
			}
			if(confirm(str))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		function DeleteMessage()
		{
			alert("You Are not Authorized to Delete the Selected Record !");
			return false;
		}
		function fnSortByChar(varSortChar)
			{
				document.Form1.txtSortChar.value = varSortChar;
				if (typeof(document.Form1.txtCurrrentPage)!='undefined')
				{
					document.Form1.txtCurrrentPage.value=1;
				}
				document.Form1.btnGo1.click();
			}
			   function SortColumn(a)
    {
       document.Form1.txtSortColumn.value=a;
       document.Form1.submit();
       return false;
    }
   
    function SelectAll(a)
    {
        var str;

        if (typeof(a)=='string')
        {
            a=document.all[a]
        }
        if (a.checked==true)
        {
             for(var i=1; i<=gvSearch.rows.length; i++)
			{
			    if (i<10)
		        {
		         str='0'+i   
		         }
		         else
		         {
		           str=i
		         }
		         document.all('gvSearch_ctl'+str+'_chk').checked=true;
			}
        }
        else if (a.checked==false)
        {
             for(var i=1; i<=gvSearch.rows.length; i++)
			{
			    if (i<10)
		        {
		         str='0'+i   
		         }
		         else
		         {
		           str=i
		         }
		         document.all('gvSearch_ctl'+str+'_chk').checked=false;
			}
        }
       
        //gvSearch_ctl02_chk
    }
    function OpenWindow(a,b,c)
    {
        var str;
        if (b==0)
        {   
            if (c==1)
            {
            str="../pagelayout/frmLeaddtl.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=LeadsList&DivID=" + a;
            }
            else{
            str="../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=LeadsList&DivID=" + a;
            }
        }
        else if (b==1)
        {
            if (c==1)
            {
            str="../pagelayout/frmProspectdtl.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=LeadsList&DivID=" + a;
            }
            else
            {
              str="../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=LeadsList&DivID=" + a;
            }
        }
         else if (b==2)
        {
            if (c==1)
            {
            str="../pagelayout/frmAccountdtl.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=LeadsList&klds+7kldf=fjk-las&DivId=" + a;
            }
            else
            {
              str="../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=LeadsList&klds+7kldf=fjk-las&DivId=" + a;
            }
        }
       
            document.location.href=str;
       
    }
	 
		</script>
	</HEAD>
	<body>
		
		<form id="Form1" method="post" runat="server">		
		<asp:ScriptManager runat="server" ID="sm" EnablePartialRendering="true"></asp:ScriptManager>
		<menu1:PartnerPoint runat="server" ID="menu" />

				<asp:UpdatePanel ID="updatepanel1" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" ><ContentTemplate>
				<table width="100%">
			    <tr>
			        <td>
			        <asp:HyperLink Text="Settings" CssClass="hyperlink" runat="server" ID="hplSettings"></asp:HyperLink>
			        </td>
			       <td align="right">
			<table align="right">
				<tr>
				
				    <td class="normal1" id="tdRadios" runat="server" >
				    <asp:radiobutton  ID="radMyself" Checked="true" AutoPostBack="true"   GroupName="rad" runat=server Text="Myself" ></asp:radiobutton>
				    <asp:radiobutton  ID="radTeamSelected" AutoPostBack="true"  GroupName="rad" runat=server Text="Team Selected" ></asp:radiobutton>
				    </td>
				    <td id="tdTeams" runat="server" width="150" align=center >
				    <asp:button id="btnChooseTeams"  CssClass="button" Text="Choose Teams" Runat="server"></asp:button>
				    
				    </td>
					<td class="normal1" width="130">No of Records:
						<asp:label id="lblRecordCount" runat="server"></asp:label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td class="normal1">Organization
					</td>
					<td><asp:textbox id="txtCustomer" runat="server" CssClass="signup" Width="55px"></asp:textbox></td>
					<td class="normal1">First Name
					</td>
					<td><asp:textbox id="txtFirstName" runat="server" CssClass="signup" Width="55px"></asp:textbox></td>
					<td class="normal1">Last Name</td>
					<td><asp:textbox id="txtLastName" runat="server" CssClass="signup" Width="55px"></asp:textbox></td>
					<td><asp:button id="btnGo" CssClass="button" Width="25" Text="Go" Runat="server"></asp:button></td>
					<td><asp:button id="btnDelete" Width="50" CssClass="button" Runat="server" Text="Delete"></asp:button></td>
				</tr>
			</table>
					</td></tr></table>
		
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td vAlign="bottom">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;<asp:Label ID="lbLeads" runat="server" ></asp:Label>&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
			
					<td class="normal1" noWrap align="right" id=tdTeamM runat=server>Team Members&nbsp;</td>
					<td id=tdTeamsDrop runat=server><asp:dropdownlist id="ddlTeamMembers" runat="server" CssClass="signup" AutoPostBack=true  ></asp:dropdownlist></td>
					<td><asp:dropdownlist id="ddlGroup" style="display:none" runat="server" CssClass="signup" Width="130px" AutoPostBack="true"></asp:dropdownlist></td>
					<td class="normal1" noWrap align="right">Follow-Up Status&nbsp;</td>
					<td><asp:dropdownlist id="ddlFollow" runat="server" CssClass="signup" Width="230px" AutoPostBack="true"></asp:dropdownlist></td>
			
					<td id="hidenav" noWrap align="right" runat="server">
						<table>
							<tr>
								<td><asp:label id="lblNext" runat="server" cssclass="Text_bold">Next:</asp:label></td>
								<td class="normal1"><asp:linkbutton id="lnk2" runat="server">2</asp:linkbutton></td>
								<td class="normal1"><asp:linkbutton id="lnk3" runat="server">3</asp:linkbutton></td>
								<td class="normal1"><asp:linkbutton id="lnk4" runat="server">4</asp:linkbutton></td>
								<td class="normal1"><asp:linkbutton id="lnk5" runat="server">5</asp:linkbutton></td>
								<td><asp:linkbutton id="lnkFirst" runat="server"><div class="LinkArrow">9</div>
									</asp:linkbutton></td>
								<td><asp:linkbutton id="lnkPrevious" runat="server"><div class="LinkArrow">3</div>
									</asp:linkbutton></td>
								<td class="normal1"><asp:label id="lblPage" runat="server">Page</asp:label></td>
								<td><asp:textbox id="txtCurrrentPage" runat="server" CssClass="signup" Width="28px" AutoPostBack="true"
										MaxLength="5"></asp:textbox></td>
								<td class="normal1"><asp:label id="lblOf" runat="server">of</asp:label></td>
								<td class="normal1"><asp:label id="lblTotal" runat="server"></asp:label></td>
								<td><asp:linkbutton id="lnkNext" runat="server" CssClass="LinkArrow"><div class="LinkArrow">4</div>
									</asp:linkbutton></td>
								<td><asp:linkbutton id="lnkLast" runat="server"><div class="LinkArrow">:</div>
									</asp:linkbutton></td>
							</tr>
						</table>
					</td>
					
				</tr>
			</table>
			<table cellSpacing="1" cellPadding="1" width="100%" border="0">
				<tr>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="a" href="javascript:fnSortByChar('a')">
							<div class="A2Z">A</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="b" href="javascript:fnSortByChar('b')">
							<div class="A2Z">B</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="c" href="javascript:fnSortByChar('c')">
							<div class="A2Z">C</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="d" href="javascript:fnSortByChar('d')">
							<div class="A2Z">D</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="e" href="javascript:fnSortByChar('e')">
							<div class="A2Z">E</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="f" href="javascript:fnSortByChar('f')">
							<div class="A2Z">F</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="g" href="javascript:fnSortByChar('g')">
							<div class="A2Z">G</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="h" href="javascript:fnSortByChar('h')">
							<div class="A2Z">H</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="I" href="javascript:fnSortByChar('i')">
							<div class="A2Z">I</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="j" href="javascript:fnSortByChar('j')">
							<div class="A2Z">J</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="k" href="javascript:fnSortByChar('k')">
							<div class="A2Z">K</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="l" href="javascript:fnSortByChar('l')">
							<div class="A2Z">L</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="m" href="javascript:fnSortByChar('m')">
							<div class="A2Z">M</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="n" href="javascript:fnSortByChar('n')">
							<div class="A2Z">N</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="o" href="javascript:fnSortByChar('o')">
							<div class="A2Z">O</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="p" href="javascript:fnSortByChar('p')">
							<div class="A2Z">P</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="q" href="javascript:fnSortByChar('q')">
							<div class="A2Z">Q</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="r" href="javascript:fnSortByChar('r')">
							<div class="A2Z">R</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="s" href="javascript:fnSortByChar('s')">
							<div class="A2Z">S</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="t" href="javascript:fnSortByChar('t')">
							<div class="A2Z">T</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="u" href="javascript:fnSortByChar('u')">
							<div class="A2Z">U</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="v" href="javascript:fnSortByChar('v')">
							<div class="A2Z">V</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="w" href="javascript:fnSortByChar('w')">
							<div class="A2Z">W</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="x" href="javascript:fnSortByChar('x')">
							<div class="A2Z">X</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="y" href="javascript:fnSortByChar('y')">
							<div class="A2Z">Y</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="z" href="javascript:fnSortByChar('z')">
							<div class="A2Z">Z</div>
						</A>
					</td>
					<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
						bgColor="#52658c"><A id="all" href="javascript:fnSortByChar('0')">
							<div class="A2Z">All</div>
						</A>
					</td>
				</tr>
			</table>
		
							<asp:table id="table1" Width="100%" Runat="server" Height="350" GridLines="None" BorderColor="black" CssClass="aspTable"
				BorderWidth="1" CellSpacing="0" CellPadding="0">
				<asp:tableRow>
					<asp:tableCell VerticalAlign="Top">
						    <asp:GridView ID="gvSearch" runat="server" EnableViewState="true"  AutoGenerateColumns="false"  CssClass="dg" Width="100%">
                                    <AlternatingRowStyle CssClass="ais"/>
                                    <RowStyle CssClass="is"/>
                                    <HeaderStyle CssClass="hs"/>
						                        <Columns>
						                   
						                        </Columns>		
                                </asp:GridView>
					</asp:tableCell>
				</asp:tableRow>
			</asp:table>
			<table width="100%">
				<tr>
					<td class="normal4" align="center"><asp:literal id="litMessage" Runat="server" EnableViewState=False></asp:literal></td>
				</tr>
			</table>
							 <asp:TextBox ID="txtDelContactIds" runat="server" style="display:none"></asp:TextBox>
			   <asp:TextBox ID="txtSortColumn" runat="server" style="display:none"></asp:TextBox>
			<asp:textbox id="txtTotalPage" style="DISPLAY: none" Runat="server"></asp:textbox>
			<asp:textbox id="txtTotalRecords" style="DISPLAY: none" Runat="server"></asp:textbox>
			<asp:textbox id="txtSortChar" style="DISPLAY: none" Runat="server"></asp:textbox>
					<asp:button id="btnGo1" Width="25" Runat="server" style="DISPLAY:none" />
			</ContentTemplate>
			</asp:updatepanel>
			
		
		</form>
	</body>
</HTML>
