' Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Account
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Common
Imports Infragistics.WebUI.UltraWebTab
Imports System.Reflection

Partial Public Class frmCusAccounts : Inherits BACRMPage

    Dim SI As Integer = 0
    Dim SI1 As Integer = 0
    Dim SI2 As Integer = 0
    Dim frm As String = ""
    Dim frm1 As String = ""
    Dim frm2 As String = ""
    Dim lngDivId As Long
    Dim FromDate, ToDate As String
    
    Dim objCommon As CCommon
    Dim dtCompanyTaxTypes As DataTable

    'Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '    Try
    '        If Not GetQueryStringVal( "SI") Is Nothing Then
    '            SI = GetQueryStringVal( "SI")
    '        End If
    '        If Not GetQueryStringVal( "SI1") Is Nothing Then
    '            SI1 = GetQueryStringVal( "SI1")
    '        Else : SI1 = 0
    '        End If
    '        If Not GetQueryStringVal( "SI2") Is Nothing Then
    '            SI2 = GetQueryStringVal( "SI2")
    '        Else : SI2 = 0
    '        End If
    '        If Not GetQueryStringVal( "frm") Is Nothing Then
    '            frm = ""
    '            frm = GetQueryStringVal( "frm")
    '        Else : frm = ""
    '        End If
    '        If Not GetQueryStringVal( "frm1") Is Nothing Then
    '            frm1 = ""
    '            frm1 = GetQueryStringVal( "frm1")
    '        Else : frm1 = ""
    '        End If
    '        If Not GetQueryStringVal( "SI2") Is Nothing Then
    '            frm2 = ""
    '            frm2 = GetQueryStringVal( "frm2")
    '        Else : frm2 = ""
    '        End If
    '        If Session("UserContactID") = Nothing Then
    '            Response.Redirect("../Common/frmLogout.aspx")
    '        End If
    '        lngDivId = Session("DivId")
    '        hplAddress.Attributes.Add("onclick", "return OpenAdd(" & lngDivId & ");")

    '        If Session("EnableIntMedPage") = 1 Then
    '            btnSaveClose.Visible = True
    '            btnCancel.Visible = True
    '        Else
    '            btnSaveClose.Visible = False
    '            btnCancel.Visible = False
    '        End If
    '        ' Checking the rights to view Contacts

    '        If Not IsPostBack Then
    '            objCommon = New CCommon
    '            m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmAccounts.aspx", Session("UserContactID"), 15, 1)
    '            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../Common/frmAuthorization.aspx")
    '            If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
    '                btnSave.Visible = False
    '                btnSaveClose.Visible = False
    '            End If
    '            sb_CompanyInfo()
    '        End If
    '        If ddlRelationhip.SelectedItem.Value <> 0 Then lblCustomer.Text = ddlRelationhip.SelectedItem.Text

    '        DisplayDynamicFlds()
    '        If Not IsPostBack Then If uwOppTab.Tabs.Count > SI Then uwOppTab.SelectedTabIndex = SI
    '        litMessage.Text = ""
    '        hplTo.Attributes.Add("onclick", "return OpenTo(" & lngDivId & ");")
    '        hplFrom.Attributes.Add("onclick", "return OpenFrom(" & lngDivId & ");")
    '        btnEditWebLnk.Attributes.Add("onclick", "return fn_EditLabels('" & "../admin/AdminWebLinkLabels.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&rtyWR=" & lngDivId & "')")
    '        btnWebGo1.Attributes.Add("onclick", "return fn_GoToURL(txtWebLink1.value);")
    '        btnWebGo2.Attributes.Add("onclick", "return fn_GoToURL(txtWebLink2.value);")
    '        btnWebGo3.Attributes.Add("onclick", "return fn_GoToURL(txtWebLink3.value);")
    '        btnWebGo4.Attributes.Add("onclick", "return fn_GoToURL(txtWebLink4.value);")
    '        btnGo.Attributes.Add("onclick", "return fn_GoToURL(txtWeb.value);")
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Sub LoadProfile()
    '    Try
    '        Dim objUserAccess As New UserAccess
    '        objUserAccess.RelID = ddlRelationhip.SelectedItem.Value
    '        objUserAccess.DomainID = Session("DomainID")
    '        ddlProfile.DataSource = objUserAccess.GetRelProfileD
    '        ddlProfile.DataTextField = "ProName"
    '        ddlProfile.DataValueField = "numProfileID"
    '        ddlProfile.DataBind()
    '        ddlProfile.Items.Insert(0, New ListItem("---Select One---", "0"))

    '        objUserAccess.RelID = ddlRelationhip.SelectedItem.Value
    '        ddlFollow.DataSource = objUserAccess.GetRelFollowD
    '        ddlFollow.DataTextField = "Follow"
    '        ddlFollow.DataValueField = "numFollowID"
    '        ddlFollow.DataBind()
    '        ddlFollow.Items.Insert(0, New ListItem("---Select One---", "0"))
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub sb_CompanyInfo()
    '    Try
    '        objCommon.sb_FillComboFromDBwithSel(ddlRating, 2, Session("DomainID"))
    '        objCommon.sb_FillComboFromDBwithSel(ddlStatus, 1, Session("DomainID"))
    '        objCommon.sb_FillComboFromDBwithSel(ddlRelationhip, 5, Session("DomainID"))
    '        objCommon.sb_FillComboFromDBwithSel(ddlIndustry, 4, Session("DomainID"))
    '        objCommon.sb_FillComboFromDBwithSel(ddlInfoSource, 18, Session("DomainID"))
    '        objCommon.sb_FillComboFromDBwithSel(ddlAnnualRevenue, 6, Session("DomainID"))
    '        objCommon.sb_FillComboFromDBwithSel(ddlCredit, 3, Session("DomainID"))
    '        objCommon.sb_FillComboFromDBwithSel(ddlNoOfEmp, 7, Session("DomainID"))
    '        objCommon.sb_FillComboFromDBwithSel(ddlTerriory, 78, Session("DomainID"))
    '        objCommon.sb_FillComboFromDBwithSel(ddlCampaign, 24, Session("DomainID"))

    '        Dim objProspects As New CProspects
    '        Dim dtComInfo As DataTable
    '        objProspects.DivisionID = lngDivId
    '        objProspects.DomainID = Session("DomainID")
    '        objProspects.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
    '        dtComInfo = objProspects.GetCompanyInfoForEdit

    '        lblAssoCountF.Text = dtComInfo.Rows(0).Item("AssociateCountFrom")
    '        lblAssoCountT.Text = dtComInfo.Rows(0).Item("AssociateCountTo")

    '        lblRecordOwner.Text = dtComInfo.Rows(0).Item("RecOwner")
    '        lblCreatedBy.Text = dtComInfo.Rows(0).Item("vcCreatedBy")
    '        lblLastModifiedBy.Text = dtComInfo.Rows(0).Item("vcModifiedBy")
    '        txtrelationShipName.Text = dtComInfo.Rows(0).Item("vcCompanyName")
    '        If Not IsDBNull(dtComInfo.Rows(0).Item("numCampaignID")) Then
    '            If Not ddlCampaign.Items.FindByValue(dtComInfo.Rows(0).Item("numCampaignID")) Is Nothing Then
    '                ddlCampaign.Items.FindByValue(dtComInfo.Rows(0).Item("numCampaignID")).Selected = True
    '            End If
    '        End If
    '        'Division

    '        ''lblCustID.Text = "C" & Format(drdrCompany("numDivisionID"), "000000")
    '        lblCustomerId.Text = lngDivId

    '        txtdivision.Text = dtComInfo.Rows(0).Item("vcDivisionName")

    '        'Territory
    '        If Not IsDBNull(dtComInfo.Rows(0).Item("numTerID")) Then
    '            If Not ddlTerriory.Items.FindByValue(dtComInfo.Rows(0).Item("numTerID")) Is Nothing Then
    '                ddlTerriory.Items.FindByValue(dtComInfo.Rows(0).Item("numTerID")).Selected = True
    '            End If
    '        End If

    '        If Session("PopulateUserCriteria") = 1 Then
    '            ddlTerriory.AutoPostBack = True
    '            objCommon.sb_FillConEmpFromTerritories(ddlAssignedTo, Session("DomainID"), 1, IIf(IsDBNull(dtComInfo.Rows(0).Item("numAssignedTo")), 0, dtComInfo.Rows(0).Item("numAssignedTo")), dtComInfo.Rows(0).Item("numTerID"))
    '        Else : objCommon.sb_FillConEmpFromDBSel(ddlAssignedTo, Session("DomainID"), 1, IIf(IsDBNull(dtComInfo.Rows(0).Item("numAssignedTo")), 0, dtComInfo.Rows(0).Item("numAssignedTo")))
    '        End If

    '        If Not IsDBNull(dtComInfo.Rows(0).Item("numAssignedTo")) Then
    '            If Not ddlAssignedTo.Items.FindByValue(dtComInfo.Rows(0).Item("numAssignedTo")) Is Nothing Then
    '                ddlAssignedTo.Items.FindByValue(dtComInfo.Rows(0).Item("numAssignedTo")).Selected = True
    '            End If
    '        End If
    '        'Public Flag
    '        If Not IsDBNull(dtComInfo.Rows(0).Item("bitPublicFlag")) Then
    '            If dtComInfo.Rows(0).Item("bitPublicFlag") = 0 Then
    '                chkPrivate.Checked = False
    '            Else : chkPrivate.Checked = True
    '            End If
    '        End If

    '        'Rating
    '        If Not IsDBNull(dtComInfo.Rows(0).Item("numCompanyRating")) Then
    '            If Not ddlRating.Items.FindByValue(dtComInfo.Rows(0).Item("numCompanyRating")) Is Nothing Then
    '                ddlRating.Items.FindByValue(dtComInfo.Rows(0).Item("numCompanyRating")).Selected = True
    '            End If
    '        End If

    '        If Not IsDBNull(dtComInfo.Rows(0).Item("numCompanyStatus")) Then
    '            If Not ddlStatus.Items.FindByValue(dtComInfo.Rows(0).Item("numCompanyStatus")) Is Nothing Then
    '                ddlStatus.ClearSelection()
    '                ddlStatus.Items.FindByValue(dtComInfo.Rows(0).Item("numCompanyStatus")).Selected = True
    '            End If
    '        End If

    '        If Not IsDBNull(dtComInfo.Rows(0).Item("numCompanyIndustry")) Then
    '            If Not ddlIndustry.Items.FindByValue(dtComInfo.Rows(0).Item("numCompanyIndustry")) Is Nothing Then
    '                ddlIndustry.Items.FindByValue(dtComInfo.Rows(0).Item("numCompanyIndustry")).Selected = True
    '            End If
    '        End If

    '        If Not IsDBNull(dtComInfo.Rows(0).Item("numAnnualRevID")) Then
    '            If Not ddlAnnualRevenue.Items.FindByValue(dtComInfo.Rows(0).Item("numAnnualRevID")) Is Nothing Then
    '                ddlAnnualRevenue.Items.FindByValue(dtComInfo.Rows(0).Item("numAnnualRevID")).Selected = True
    '            End If
    '        End If
    '        If Not IsDBNull(dtComInfo.Rows(0).Item("numCompanyCredit")) Then
    '            If Not ddlCredit.Items.FindByValue(dtComInfo.Rows(0).Item("numCompanyCredit")) Is Nothing Then
    '                ddlCredit.Items.FindByValue(dtComInfo.Rows(0).Item("numCompanyCredit")).Selected = True
    '            End If
    '        End If

    '        Dim strType As String
    '        If Not IsDBNull(dtComInfo.Rows(0).Item("numCompanyType")) Then
    '            If Not ddlRelationhip.Items.FindByValue(dtComInfo.Rows(0).Item("numCompanyType")) Is Nothing Then
    '                strType = ddlRelationhip.Items.FindByValue(dtComInfo.Rows(0).Item("numCompanyType")).Text
    '                ddlRelationhip.Items.FindByValue(dtComInfo.Rows(0).Item("numCompanyType")).Selected = True
    '            End If
    '        End If
    '        LoadProfile()
    '        If Not IsDBNull(dtComInfo.Rows(0).Item("vcProfile")) Then
    '            If Not ddlProfile.Items.FindByValue(dtComInfo.Rows(0).Item("vcProfile")) Is Nothing Then
    '                ddlProfile.Items.FindByValue(dtComInfo.Rows(0).Item("vcProfile")).Selected = True
    '            End If
    '        End If

    '        If Not IsDBNull(dtComInfo.Rows(0).Item("numFollowUpStatus")) Then
    '            If Not ddlFollow.Items.FindByValue(dtComInfo.Rows(0).Item("numFollowUpStatus")) Is Nothing Then
    '                ddlFollow.Items.FindByValue(dtComInfo.Rows(0).Item("numFollowUpStatus")).Selected = True
    '            End If
    '        End If

    '        If Not IsDBNull(dtComInfo.Rows(0).Item("vcWebSite")) Then
    '            txtWeb.Text = IIf(dtComInfo.Rows(0).Item("vcWebSite") = "", "http://", dtComInfo.Rows(0).Item("vcWebSite"))
    '        Else : txtWeb.Text = "http://"
    '        End If

    '        If Not IsDBNull(dtComInfo.Rows(0).Item("numNoOfEmployeesID")) Then
    '            If Not ddlNoOfEmp.Items.FindByValue(dtComInfo.Rows(0).Item("numNoOfEmployeesID")) Is Nothing Then
    '                ddlNoOfEmp.Items.FindByValue(dtComInfo.Rows(0).Item("numNoOfEmployeesID")).Selected = True
    '            End If
    '        End If
    '        If Not IsDBNull(dtComInfo.Rows(0).Item("fltInterest")) Then
    '            txtInterest.Text = String.Format("{0:#,##0.00}", dtComInfo.Rows(0).Item("fltInterest"))
    '        End If
    '        If Not IsDBNull(dtComInfo.Rows(0).Item("numBillingDaysName")) Then
    '            txtNetdays.Text = String.Format("{0:#,###}", dtComInfo.Rows(0).Item("numBillingDaysName"))
    '        End If
    '        If Not IsDBNull(dtComInfo.Rows(0).Item("tintBillingTerms")) Then
    '            If dtComInfo.Rows(0).Item("tintBillingTerms") = 0 Then
    '                chkBillinTerms.Checked = False
    '            Else
    '                chkBillinTerms.Checked = True
    '                txtSummary.Text = "Net " & dtComInfo.Rows(0).Item("numBillingDaysName") & " , " & IIf(dtComInfo.Rows(0).Item("tintInterestType") = 0, "-", "+") & dtComInfo.Rows(0).Item("fltInterest") & " %"
    '            End If
    '        End If

    '        lblAddress.Text = IIf(dtComInfo.Rows(0).Item("vcBillStreet") = "", "", dtComInfo.Rows(0).Item("vcBillStreet") & ", ") & IIf(dtComInfo.Rows(0).Item("vcBillCity") = "", "", dtComInfo.Rows(0).Item("vcBillCity") & ", ") & IIf(dtComInfo.Rows(0).Item("vcBilState") = "", "", dtComInfo.Rows(0).Item("vcBilState") & ", ") & IIf(IsDBNull(dtComInfo.Rows(0).Item("vcBillPostCode")), "", dtComInfo.Rows(0).Item("vcBillPostCode") & ", ") & dtComInfo.Rows(0).Item("vcBillCountry")

    '        txtComments.Text = IIf(IsDBNull(dtComInfo.Rows(0).Item("txtComments")) = False, Server.HtmlDecode(dtComInfo.Rows(0).Item("txtComments")), "")
    '        txtPhone.Text = IIf(IsDBNull(dtComInfo.Rows(0).Item("vcComPhone")) = False, dtComInfo.Rows(0).Item("vcComPhone"), "")
    '        txtFax.Text = IIf(IsDBNull(dtComInfo.Rows(0).Item("vcComFax")) = False, dtComInfo.Rows(0).Item("vcComFax"), "")

    '        lblWebLink1.Text = IIf(IsDBNull(dtComInfo.Rows(0).Item("vcWebLabel1")), "", dtComInfo.Rows(0).Item("vcWebLabel1"))
    '        lblWebLink2.Text = IIf(IsDBNull(dtComInfo.Rows(0).Item("vcWebLabel2")), "", dtComInfo.Rows(0).Item("vcWebLabel2"))
    '        lblWebLink3.Text = IIf(IsDBNull(dtComInfo.Rows(0).Item("vcWebLabel3")), "", dtComInfo.Rows(0).Item("vcWebLabel3"))
    '        lblWebLink4.Text = IIf(IsDBNull(dtComInfo.Rows(0).Item("vcWeblabel4")), "", dtComInfo.Rows(0).Item("vcWeblabel4"))
    '        txtWebLink1.Text = IIf(IsDBNull(dtComInfo.Rows(0).Item("vcWebLink1")), "", dtComInfo.Rows(0).Item("vcWebLink1"))
    '        txtWebLink2.Text = IIf(IsDBNull(dtComInfo.Rows(0).Item("vcWebLink2")), "", dtComInfo.Rows(0).Item("vcWebLink2"))
    '        txtWebLink3.Text = IIf(IsDBNull(dtComInfo.Rows(0).Item("vcWebLink3")), "", dtComInfo.Rows(0).Item("vcWebLink3"))
    '        txtWebLink4.Text = IIf(IsDBNull(dtComInfo.Rows(0).Item("vcWebLink4")), "", dtComInfo.Rows(0).Item("vcWebLink4"))
    '        If Not IsDBNull(dtComInfo.Rows(0).Item("vcHow")) Then
    '            If Not ddlInfoSource.Items.FindByValue(dtComInfo.Rows(0).Item("vcHow")) Is Nothing Then
    '                ddlInfoSource.Items.FindByValue(dtComInfo.Rows(0).Item("vcHow")).Selected = True
    '            End If
    '        End If
    '        objProspects.DomainID = Session("DomainID")
    '        dtCompanyTaxTypes = objProspects.GetCompanyTaxTypes
    '        Dim dr As DataRow
    '        dr = dtCompanyTaxTypes.NewRow
    '        dr("numTaxItemID") = 0
    '        dr("vcTaxName") = "Sales Tax(Default)"
    '        dr("bitApplicable") = IIf(dtComInfo.Rows(0).Item("bitNoTax") = True, False, True)
    '        dtCompanyTaxTypes.Rows.Add(dr)


    '        chkTaxItems.DataTextField = "vcTaxName"
    '        chkTaxItems.DataValueField = "numTaxItemID"
    '        chkTaxItems.DataSource = dtCompanyTaxTypes
    '        chkTaxItems.DataBind()
    '        Dim i As Integer
    '        For i = 0 To dtCompanyTaxTypes.Rows.Count - 1
    '            If Not IsDBNull(dtCompanyTaxTypes.Rows(i).Item("bitApplicable")) Then
    '                If dtCompanyTaxTypes.Rows(i).Item("bitApplicable") = True Then
    '                    chkTaxItems.Items.FindByValue(dtCompanyTaxTypes.Rows(i).Item("numTaxItemID")).Selected = True
    '                Else
    '                    chkTaxItems.Items.FindByValue(dtCompanyTaxTypes.Rows(i).Item("numTaxItemID")).Selected = False
    '                End If
    '            End If
    '        Next
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub


    'Sub SaveTaxTypes()
    '    Try
    '        dtCompanyTaxTypes = New DataTable
    '        dtCompanyTaxTypes.Columns.Add("numTaxItemID")
    '        dtCompanyTaxTypes.Columns.Add("bitApplicable")
    '        Dim dr As DataRow
    '        Dim i As Integer
    '        For i = 0 To chkTaxItems.Items.Count - 1
    '            If chkTaxItems.Items(i).Selected = True Then
    '                dr = dtCompanyTaxTypes.NewRow
    '                dr("numTaxItemID") = chkTaxItems.Items(i).Value
    '                dr("bitApplicable") = 1
    '                dtCompanyTaxTypes.Rows.Add(dr)
    '            End If
    '        Next
    '        Dim ds As New DataSet
    '        Dim strdetails As String
    '        dtCompanyTaxTypes.TableName = "Table"
    '        ds.Tables.Add(dtCompanyTaxTypes)
    '        strdetails = ds.GetXml
    '        ds.Tables.Remove(ds.Tables(0))
    '        Dim objProspects As New CProspects
    '        objProspects.DivisionID = lngDivId
    '        objProspects.strCompanyTaxTypes = strdetails
    '        objProspects.ManageCompanyTaxTypes()
    '        ds.Dispose()
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    '    Try
    '        SaveAccounts()
    '        SaveTaxTypes()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Sub SaveAccounts()
    '    Try
    '        Dim objLeads As New CLeads
    '        With objLeads
    '            .CompanyID = Session("CompID")
    '            .CompanyName = txtrelationShipName.Text
    '            .InfoSource = ddlInfoSource.SelectedItem.Value
    '            .DomainID = Session("DomainID")
    '            .TerritoryID = ddlTerriory.SelectedItem.Value
    '            .CompanyType = ddlRelationhip.SelectedItem.Value
    '            .PublicFlag = chkPrivate.Checked
    '            .DivisionID = lngDivId
    '            .CompanyRating = ddlRating.SelectedItem.Value
    '            .UserCntID = Session("UserContactID")
    '            .AnnualRevenue = ddlAnnualRevenue.SelectedItem.Value
    '            .CompanyIndustry = ddlIndustry.SelectedItem.Value
    '            .AnnualRevenue = ddlAnnualRevenue.SelectedItem.Value
    '            .CompanyCredit = ddlCredit.SelectedItem.Value
    '            .WebSite = txtWeb.Text
    '            .NumOfEmp = ddlNoOfEmp.SelectedItem.Value
    '            .Profile = ddlProfile.SelectedItem.Value
    '            .CampaignID = ddlCampaign.SelectedItem.Value
    '            .FollowUpStatus = ddlFollow.SelectedValue
    '            .AssignedTo = ddlAssignedTo.SelectedValue
    '            .Comments = txtComments.Text
    '            .WebLink1 = txtWebLink1.Text
    '            .WebLink2 = txtWebLink2.Text
    '            .WebLink3 = txtWebLink3.Text
    '            .WebLink4 = txtWebLink4.Text
    '            .CRMType = Session("CRMType")
    '            .DivisionName = txtdivision.Text
    '            .StatusID = ddlStatus.SelectedItem.Value
    '            .BillingTerms = IIf(chkBillinTerms.Checked = True, 1, 0)
    '            .BillingDays = IIf(txtNetdays.Text = "", 0, txtNetdays.Text)
    '            .InterestType = IIf(radPlus.Checked = True, 1, 0)
    '            .Interest = IIf(Replace(txtInterest.Text, ",", "") = "", 0, Replace(txtInterest.Text, ",", ""))
    '            .ComPhone = txtPhone.Text
    '            .ComFax = txtFax.Text
    '            .NoTax = IIf(chkTaxItems.Items.FindByValue(0).Selected = True, False, True)
    '        End With
    '        objLeads.CompanyID = objLeads.CreateRecordCompanyInfo
    '        objLeads.DivisionID = objLeads.ManageCompanyDivisionsInfo1
    '        SaveCusField()
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Sub DisplayDynamicFlds()
    '    Try
    '        Dim strDate As String
    '        Dim bizCalendar As UserControl
    '        Dim _myUC_DueDate As PropertyInfo
    '        Dim PreviousRowID As Integer = 0
    '        Dim objRow As HtmlTableRow
    '        Dim objCell As HtmlTableCell
    '        Dim i, k As Integer
    '        Dim dttable As DataTable
    '        Dim ObjCus As New CustomFields
    '        ObjCus.locId = 1
    '        ObjCus.RelId = ddlRelationhip.SelectedItem.Value
    '        ObjCus.RecordId = lngDivId
    '        ObjCus.DomainID = Session("DomainID")
    '        dttable = ObjCus.GetCustFlds.Tables(0)
    '        Session("CusFields") = dttable

    '        If uwOppTab.Tabs.Count > 1 Then
    '            Dim iItemcount As Integer
    '            iItemcount = uwOppTab.Tabs.Count
    '            While uwOppTab.Tabs.Count > 1
    '                uwOppTab.Tabs.RemoveAt(iItemcount - 1)
    '                iItemcount = iItemcount - 1
    '            End While
    '        End If

    '        If dttable.Rows.Count > 0 Then
    '            'Main Detail Section
    '            k = 0
    '            objRow = New HtmlTableRow
    '            For i = 0 To dttable.Rows.Count - 1
    '                If dttable.Rows(i).Item("TabId") = 0 Then
    '                    If k = 3 Then
    '                        k = 0
    '                        tblDetails.Rows.Add(objRow)
    '                        objRow = New HtmlTableRow
    '                    End If

    '                    objCell = New HtmlTableCell
    '                    objCell.Align = "Right"
    '                    objCell.Attributes.Add("class", "normal1")
    '                    If dttable.Rows(i).Item("fld_type") <> "Link" Then
    '                        objCell.InnerText = dttable.Rows(i).Item("fld_label")
    '                    End If

    '                    objRow.Cells.Add(objCell)
    '                    If dttable.Rows(i).Item("fld_type") = "Text Box" Then
    '                        objCell = New HtmlTableCell
    '                        CreateTexBox(objRow, objCell, dttable.Rows(i).Item("fld_id"), dttable.Rows(i).Item("Value"))
    '                    ElseIf dttable.Rows(i).Item("fld_type") = "Drop Down List Box" Then
    '                        objCell = New HtmlTableCell
    '                        CreateDropdown(objRow, objCell, dttable.Rows(i).Item("fld_id"), CInt(dttable.Rows(i).Item("Value")), dttable.Rows(i).Item("numlistid"))
    '                    ElseIf dttable.Rows(i).Item("fld_type") = "Check box" Then
    '                        objCell = New HtmlTableCell
    '                        CreateChkBox(objRow, objCell, dttable.Rows(i).Item("fld_id"), CInt(dttable.Rows(i).Item("Value")))
    '                    ElseIf dttable.Rows(i).Item("fld_type") = "Text Area" Then
    '                        objCell = New HtmlTableCell
    '                        CreateTextArea(objRow, objCell, dttable.Rows(i).Item("fld_id"), dttable.Rows(i).Item("Value"))
    '                    ElseIf dttable.Rows(i).Item("fld_type") = "Date Field" Then
    '                        PreviousRowID = i
    '                        objCell = New HtmlTableCell
    '                        bizCalendar = LoadControl("../include/calandar.ascx")
    '                        bizCalendar.ID = "cal" & dttable.Rows(i).Item("fld_id")
    '                        objCell.Controls.Add(bizCalendar)
    '                        objRow.Cells.Add(objCell)
    '                    ElseIf dttable.Rows(i).Item("fld_type") = "Link" Then
    '                        objCell = New HtmlTableCell
    '                        CreateLink(objRow, objCell, dttable.Rows(i).Item("fld_id"), dttable.Rows(i).Item("vcURL"), lngDivId, dttable.Rows(i).Item("fld_label"))
    '                    End If
    '                    k = k + 1
    '                End If
    '            Next
    '            tblDetails.Rows.Add(objRow)

    '            'CustomField Section
    '            Dim Tab As Tab
    '            'Dim pageView As PageView
    '            Dim aspTable As HtmlTable
    '            Dim Table As Table
    '            Dim tblcell As TableCell
    '            Dim tblRow As TableRow

    '            k = 0
    '            ViewState("TabId") = dttable.Rows(0).Item("TabId")
    '            ViewState("Check") = 0
    '            ViewState("FirstTabCreated") = 0

    '            For i = 0 To dttable.Rows.Count - 1
    '                If dttable.Rows(i).Item("TabId") <> 0 Then
    '                    If ViewState("TabId") <> dttable.Rows(i).Item("TabId") Or ViewState("FirstTabCreated") = 0 Then
    '                        If ViewState("Check") <> 0 Then
    '                            aspTable.Rows.Add(objRow)
    '                            tblcell.Controls.Add(aspTable)
    '                            tblRow.Cells.Add(tblcell)
    '                            Table.Rows.Add(tblRow)
    '                            Tab.ContentPane.Controls.Add(Table)
    '                            ' mpages.Controls.Add(pageView)
    '                        End If
    '                        k = 0
    '                        ViewState("Check") = 1
    '                        '  If Not IsPostBack Then
    '                        ViewState("FirstTabCreated") = 1
    '                        ViewState("TabId") = dttable.Rows(i).Item("TabId")
    '                        Tab = New Tab
    '                        Tab.Text = "&nbsp;&nbsp;" & dttable.Rows(i).Item("tabname") & "&nbsp;&nbsp;"
    '                        uwOppTab.Tabs.Add(Tab)
    '                        'End If
    '                        'pageView = New PageView
    '                        aspTable = New HtmlTable
    '                        Table = New Table
    '                        Table.Width = Unit.Percentage(100)
    '                        Table.BorderColor = System.Drawing.Color.FromName("black")
    '                        Table.GridLines = GridLines.None
    '                        Table.BorderWidth = Unit.Pixel(1)
    '                        Table.Height = Unit.Pixel(300)
    '                        Table.CssClass = "aspTable"
    '                        tblcell = New TableCell
    '                        tblRow = New TableRow
    '                        aspTable.Width = "100%"
    '                        tblcell.VerticalAlign = VerticalAlign.Top
    '                        aspTable.Width = "100%"
    '                        objRow = New HtmlTableRow
    '                        objCell = New HtmlTableCell
    '                        objCell.InnerHtml = "<br>"
    '                        objRow.Cells.Add(objCell)
    '                        aspTable.Rows.Add(objRow)
    '                        objRow = New HtmlTableRow
    '                    End If

    '                    'pageView.Controls.Add("")
    '                    If k = 3 Then
    '                        k = 0
    '                        aspTable.Rows.Add(objRow)
    '                        objRow = New HtmlTableRow
    '                    End If
    '                    objCell = New HtmlTableCell
    '                    objCell.Align = "right"
    '                    objCell.Attributes.Add("class", "normal1")
    '                    If dttable.Rows(i).Item("fld_type") <> "Link" Then
    '                        objCell.InnerText = dttable.Rows(i).Item("fld_label")
    '                    End If

    '                    objRow.Cells.Add(objCell)
    '                    If dttable.Rows(i).Item("fld_type") = "Text Box" Then
    '                        objCell = New HtmlTableCell
    '                        CreateTexBox(objRow, objCell, dttable.Rows(i).Item("fld_id"), dttable.Rows(i).Item("Value"))
    '                    ElseIf dttable.Rows(i).Item("fld_type") = "Drop Down List Box" Then
    '                        objCell = New HtmlTableCell
    '                        CreateDropdown(objRow, objCell, dttable.Rows(i).Item("fld_id"), CInt(dttable.Rows(i).Item("Value")), dttable.Rows(i).Item("numlistid"))
    '                    ElseIf dttable.Rows(i).Item("fld_type") = "Check box" Then
    '                        objCell = New HtmlTableCell
    '                        CreateChkBox(objRow, objCell, dttable.Rows(i).Item("fld_id"), CInt(dttable.Rows(i).Item("Value")))
    '                    ElseIf dttable.Rows(i).Item("fld_type") = "Text Area" Then
    '                        objCell = New HtmlTableCell
    '                        CreateTextArea(objRow, objCell, dttable.Rows(i).Item("fld_id"), dttable.Rows(i).Item("Value"))
    '                    ElseIf dttable.Rows(i).Item("fld_type") = "Date Field" Then
    '                        PreviousRowID = i
    '                        objCell = New HtmlTableCell
    '                        bizCalendar = LoadControl("../include/calandar.ascx")
    '                        bizCalendar.ID = "cal" & dttable.Rows(i).Item("fld_id")
    '                        objCell.Controls.Add(bizCalendar)
    '                        objRow.Cells.Add(objCell)
    '                    ElseIf dttable.Rows(i).Item("fld_type") = "Frame" Then
    '                        objCell = New HtmlTableCell
    '                        Dim strFrame As String
    '                        Dim URL As String
    '                        URL = dttable.Rows(i).Item("vcURL")
    '                        URL = URL.Replace("RecordID", lngDivId)
    '                        strFrame = "<iframe src ='" & URL & "' width='100%' frameborder='0' height= '800px'></iframe>"
    '                        objCell.Controls.Add(New LiteralControl(strFrame))
    '                        objRow.Cells.Add(objCell)
    '                    ElseIf dttable.Rows(i).Item("fld_type") = "Link" Then
    '                        objCell = New HtmlTableCell
    '                        CreateLink(objRow, objCell, dttable.Rows(i).Item("fld_id"), dttable.Rows(i).Item("vcURL"), lngDivId, dttable.Rows(i).Item("fld_label"))
    '                    End If
    '                    k = k + 1
    '                End If
    '            Next
    '            If ViewState("Check") = 1 Then
    '                aspTable.Rows.Add(objRow)
    '                tblcell.Controls.Add(aspTable)
    '                tblRow.Cells.Add(tblcell)
    '                Table.Rows.Add(tblRow)
    '                Tab.ContentPane.Controls.Add(Table)
    '                ' mpages.Controls.Add(pageView)
    '            End If
    '        End If
    '        Dim dvCusFields As DataView
    '        dvCusFields = dttable.DefaultView
    '        dvCusFields.RowFilter = "fld_type='Date Field'"
    '        Dim iViewCount As Integer
    '        For iViewCount = 0 To dvCusFields.Count - 1
    '            If Not IsDBNull(dvCusFields(iViewCount).Item("Value")) Then
    '                bizCalendar = uwOppTab.FindControl("cal" & dvCusFields(iViewCount).Item("fld_id"))
    '                Dim _myControlType As Type = bizCalendar.GetType()
    '                _myUC_DueDate = _myControlType.GetProperty("SelectedDate")
    '                strDate = dvCusFields(iViewCount).Item("Value")
    '                If strDate = "0" Then strDate = ""
    '                If strDate <> "" Then
    '                    'strDate = DateFromFormattedDate(strDate, Session("DateFormat"))
    '                    _myUC_DueDate.SetValue(bizCalendar, strDate, Nothing)
    '                End If
    '            End If
    '        Next
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Sub SaveCusField()
    '    Try
    '        If Not Session("CusFields") Is Nothing Then
    '            Dim dttable As DataTable
    '            Dim i As Integer
    '            dttable = Session("CusFields")
    '            For i = 0 To dttable.Rows.Count - 1
    '                If dttable.Rows(i).Item("fld_type") = "Text Box" Then
    '                    Dim txt As TextBox
    '                    txt = uwOppTab.FindControl(dttable.Rows(i).Item("fld_id"))
    '                    dttable.Rows(i).Item("Value") = txt.Text
    '                ElseIf dttable.Rows(i).Item("fld_type") = "Drop Down List Box" Then
    '                    Dim ddl As DropDownList
    '                    ddl = uwOppTab.FindControl(dttable.Rows(i).Item("fld_id"))
    '                    dttable.Rows(i).Item("Value") = CStr(ddl.SelectedItem.Value)
    '                ElseIf dttable.Rows(i).Item("fld_type") = "Check box" Then
    '                    Dim chk As CheckBox
    '                    chk = uwOppTab.FindControl(dttable.Rows(i).Item("fld_id"))
    '                    If chk.Checked = True Then
    '                        dttable.Rows(i).Item("Value") = "1"
    '                    Else : dttable.Rows(i).Item("Value") = "0"
    '                    End If
    '                ElseIf dttable.Rows(i).Item("fld_type") = "Text Area" Then
    '                    Dim txt As TextBox
    '                    txt = uwOppTab.FindControl(dttable.Rows(i).Item("fld_id"))
    '                    dttable.Rows(i).Item("Value") = txt.Text
    '                ElseIf dttable.Rows(i).Item("fld_type") = "Date Field" Then
    '                    Dim BizCalendar As UserControl
    '                    BizCalendar = uwOppTab.FindControl("cal" & dttable.Rows(i).Item("fld_id"))

    '                    Dim strDueDate As String
    '                    Dim _myControlType As Type = BizCalendar.GetType()
    '                    Dim _myUC_DueDate As PropertyInfo = _myControlType.GetProperty("SelectedDate")
    '                    strDueDate = _myUC_DueDate.GetValue(BizCalendar, Nothing)
    '                    If strDueDate <> "" Then
    '                        dttable.Rows(i).Item("Value") = strDueDate
    '                    Else : dttable.Rows(i).Item("Value") = ""
    '                    End If
    '                End If
    '            Next

    '            Dim ds As New DataSet
    '            Dim strdetails As String
    '            dttable.TableName = "Table"
    '            ds.Tables.Add(dttable.Copy)
    '            strdetails = ds.GetXml
    '            ds.Tables.Remove(ds.Tables(0))

    '            Dim ObjCusfld As New CustomFields
    '            ObjCusfld.strDetails = strdetails
    '            ObjCusfld.RecordId = lngDivId
    '            ObjCusfld.locId = 1
    '            ObjCusfld.SaveCustomFldsByRecId()
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    ''Private Sub btnSupportKey_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSupportKey.Click
    ''    Dim objAccounts As New CAccounts
    ''    objAccounts.DivisionID = Session("DivId")
    ''    objAccounts.GenerateSupportKey()
    ''    lblSupportKey.Text = "U" & Format(objAccounts.supportKey, "00000000000")
    ''    btnSupportKey.Visible = False
    ''End Sub

    'Private Sub ddlRelationhip_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRelationhip.SelectedIndexChanged
    '    Try
    '        LoadProfile()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    '    Try
    '        pageRediect()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Sub pageRediect()
    '    Try
    '        If GetQueryStringVal( "frm") = "ContactList" Then
    '            Response.Redirect("../Contacts/frmCustContactList.aspx")
    '        ElseIf GetQueryStringVal( "frm") = "contactdetail" Then
    '            If Session("EnableIntMedPage") = 1 Then
    '                Response.Redirect("../pagelayout/frmCustContactdtl.aspx?frm=" & GetQueryStringVal( "frm1") & "&CntID=" & GetQueryStringVal( "CntID") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm2)
    '            Else : Response.Redirect("../Contact/frmCstContacts.aspx?frm=" & GetQueryStringVal( "frm1") & "&CntID=" & GetQueryStringVal( "CntID") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm2)
    '            End If
    '        ElseIf GetQueryStringVal( "frm") = "oppdetail" Then
    '            If Session("EnableIntMedPage") = 1 Then
    '                Response.Redirect("../pagelayout/frmCustOppurtunitydtl.aspx?frm=" & GetQueryStringVal( "frm1") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm2)
    '            Else : Response.Redirect("../opportunity/frmCusOpportunities.aspx?frm=" & GetQueryStringVal( "frm1") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm2)
    '            End If
    '        ElseIf GetQueryStringVal( "frm") = "OppList" Then
    '            Response.Redirect("../opportunity/frmCusOppList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
    '        ElseIf GetQueryStringVal( "frm") = "CaseList" Then
    '            Response.Redirect("../Cases/frmCusCaseList.aspx")
    '        ElseIf GetQueryStringVal( "frm") = "ProjectDetails" Then
    '            If Session("EnableIntMedPage") = 1 Then
    '                Response.Redirect("../pagelayout/frmCustProjectdtl.aspx?frm=" & GetQueryStringVal( "frm1") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm2)
    '            Else : Response.Redirect("../Projects/frmCusProDTL.aspx?frm=" & GetQueryStringVal( "frm1") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm2)
    '            End If
    '        ElseIf GetQueryStringVal( "frm") = "CaseDetails" Then
    '            If Session("EnableIntMedPage") = 1 Then
    '                Response.Redirect("../pagelayout/frmCustCasedtl.aspx?frm=" & GetQueryStringVal( "frm1") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm2)
    '            Else : Response.Redirect("../Cases/frmCusCaseDTL.aspx?frm=" & GetQueryStringVal( "frm1") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm2)
    '            End If
    '        Else : If Session("EnableIntMedPage") = 1 Then Response.Redirect("../pagelayout/frmCustAccountdtl.aspx?frm=" & GetQueryStringVal( "frm1") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm2)
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
    '    Try
    '        SaveAccounts()
    '        pageRediect()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

End Class
