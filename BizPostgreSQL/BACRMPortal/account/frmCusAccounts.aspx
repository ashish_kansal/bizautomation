<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCusAccounts.aspx.vb"
    Inherits="BACRMPortal.frmCusAccounts" %>
<%--
<%@ Register TagPrefix="menu1" TagName="Menu" Src="../common/topbar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <title>Accounts</title>

    <script language="javascript" type="text/javascript">
        function FillAddress(a) {
            if (document.all) {
                document.getElementById('uwOppTab__ctl0_lblAddress').innerText = a;
            } else {
                document.getElementById('uwOppTab__ctl0_lblAddress').textContent = a;
            }
            return false;
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function fn_GoToURL(varURL) {

            if ((varURL != '') && (varURL.substr(0, 7) == 'http://') && (varURL.length > 7)) {
                var LoWindow = window.open(varURL, "", "");
                LoWindow.focus();
            }
            return false;
        }
        function fn_EditLabels(URL) {
            window.open(URL, 'WebLinkLabel', 'width=500,height=70,status=no,scrollbar=yes,top=110,left=150');
            return false;
        }
        function Opentransfer(url) {
            window.open(url, '', "width=340,height=200,status=no,top=100,left=150");
            return false;
        }
        function OpenLst(url) {
            window.open(url, '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenAdd() {
            window.open("../prospects/frmProspectsAdd.aspx", '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=no,resizable=no')
            return false;
        }
        function OpenTo() {
            window.open("../Admin/frmToCompanyAssociation.aspx", '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenFrom() {
            window.open("../Admin/frmFromCompanyAssociation.aspx", '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server">
    </asp:ScriptManager>
    <menu1:Menu ID="webmenu1" runat="server">
    </menu1:Menu>
    <asp:UpdatePanel ID="updatepanel1" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
        EnableViewState="true">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td>
                        <table id="tblMenu" bordercolor="black" cellspacing="0" cellpadding="0" width="100%"
                            border="0" runat="server">
                            <tr>
                                <td class="tr1" align="center">
                                    <b>Record Owner: </b>
                                    <asp:Label ID="lblRecordOwner" runat="server" ForeColor="Black"></asp:Label>
                                </td>
                                <td class="td1" width="1" height="18">
                                </td>
                                <td class="tr1" align="center">
                                    <b>Created By: </b>
                                    <asp:Label ID="lblCreatedBy" runat="server" ForeColor="Black"></asp:Label>
                                </td>
                                <td class="td1" width="1" height="18">
                                </td>
                                <td class="tr1" align="center">
                                    <b>Last Modified By: </b>
                                    <asp:Label ID="lblLastModifiedBy" runat="server" ForeColor="Black"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td class="normal1" align="center">
                                    Organization ID :
                                    <asp:Label ID="lblCustomerId" runat="server"></asp:Label>
                                </td>
                                <td align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" Width="60">
                                    </asp:Button>
                                    <asp:Button ID="btnSaveClose" runat="server" Text="Save & close" CssClass="button">
                                    </asp:Button>
                                    <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel"></asp:Button>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="100%" align="center">
                <tr>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <igtab:UltraWebTab ImageDirectory="" ID="uwOppTab" runat="server" ThreeDEffect="True"
                            BorderStyle="Solid" Width="100%" BarHeight="0" BorderWidth="0">
                            <DefaultTabStyle Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Arial">
                            </DefaultTabStyle>
                            <RoundedImage LeftSideWidth="7" RightSideWidth="8" ShiftOfImages="0" SelectedImage="../images/ig_tab_winXPs3.gif"
                                NormalImage="../images/ig_tab_winXP3.gif" HoverImage="../images/ig_tab_winXPs3.gif"
                                FillStyle="LeftMergedWithCenter"></RoundedImage>
                            <SelectedTabStyle Height="23px" ForeColor="white">
                            </SelectedTabStyle>
                            <HoverTabStyle Height="23px" ForeColor="white">
                            </HoverTabStyle>
                            <Tabs>
                                <igtab:Tab Text="&nbsp;&nbsp;&nbsp;Organization Detail&nbsp;&nbsp;&nbsp;">
                                    <ContentTemplate>
                                        <asp:Table ID="tblProspects" runat="server" GridLines="None" BorderColor="black"
                                            Width="100%" Height="300" BorderWidth="1" CellSpacing="0" CellPadding="0" CssClass="aspTable">
                                            <asp:TableRow>
                                                <asp:TableCell VerticalAlign="Top">
                                                    <br>
                                                    <table id="tblDetails" runat="server" cellspacing="0" cellpadding="1" width="100%"
                                                        border="0">
                                                        <tr>
                                                            <td rowspan="30" valign="top">
                                                                <img src="../images/Building-48.gif" />
                                                            </td>
                                                            <td class="normal1" align="right">
                                                                <asp:Label ID="lblCustomer" runat="server">Name</asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtrelationShipName" TabIndex="1" runat="server" CssClass="signup"
                                                                    Width="180"></asp:TextBox>
                                                            </td>
                                                            <td class="normal1" align="right">
                                                                Phone/Fax
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtPhone" runat="server" CssClass="signup" Width="90" TabIndex="7"></asp:TextBox>
                                                                <asp:TextBox ID="txtFax" runat="server" CssClass="signup" Width="90" TabIndex="8"></asp:TextBox>
                                                            </td>
                                                            <td class="normal1" align="right">
                                                                Assigned To
                                                            </td>
                                                            <td class="normal1">
                                                                <asp:DropDownList ID="ddlAssignedTo" CssClass="signup" runat="server" Width="180px">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="normal1" align="right">
                                                                Division
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtdivision" runat="server" TabIndex="2" CssClass="signup" Width="180"></asp:TextBox>
                                                            </td>
                                                            <td class="normal1" align="right">
                                                                Rating
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlRating" TabIndex="9" runat="server" CssClass="signup" Width="180">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="normal1" align="right">
                                                                Employees
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlNoOfEmp" TabIndex="14" runat="server" CssClass="signup"
                                                                    Width="180">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="normal1" align="right">
                                                                Industry
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlIndustry" TabIndex="3" runat="server" CssClass="signup"
                                                                    Width="180">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="normal1" align="right">
                                                                Annual Revenue
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlAnnualRevenue" TabIndex="10" runat="server" CssClass="signup"
                                                                    Width="180">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="normal1" align="right">
                                                                Org. Status
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlStatus" TabIndex="15" runat="server" CssClass="signup" Width="180">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="normal1" align="right">
                                                                Relationship
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlRelationhip" TabIndex="4" AutoPostBack="true" runat="server"
                                                                    CssClass="signup" Width="180">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="normal1" align="right">
                                                                Web
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtWeb" runat="server" TabIndex="11" CssClass="signup" Width="145"></asp:TextBox>&nbsp;
                                                                <asp:Button ID="btnGo" runat="server" TabIndex="12" CssClass="button" Text="Go">
                                                                </asp:Button>
                                                            </td>
                                                            <td class="normal1" align="right">
                                                                Campaign
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlCampaign" TabIndex="16" runat="server" CssClass="signup"
                                                                    Width="180">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="normal1" align="right">
                                                                Org. Profile
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlProfile" TabIndex="5" runat="server" CssClass="signup" Width="180">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="normal1" align="right">
                                                                Territory
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlTerriory" TabIndex="13" runat="server" CssClass="signup"
                                                                    Width="180">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="normal1" align="right">
                                                                Follow-up Status
                                                            </td>
                                                            <td class="normal1">
                                                                <asp:DropDownList ID="ddlFollow" runat="server" CssClass="signup" Width="180" TabIndex="19">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="normal1" align="right">
                                                                Info. Source
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlInfoSource" TabIndex="6" runat="server" CssClass="signup"
                                                                    Width="180">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="normal1" align="right">
                                                                Private
                                                            </td>
                                                            <td>
                                                                <asp:CheckBox ID="chkPrivate" TabIndex="17" runat="server"></asp:CheckBox>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td class="normal1">
                                                                Associations:&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <asp:HyperLink ID="hplTo" TabIndex="18" runat="server" CssClass="hyperlink">
							                        <font color="#180073">To</font></asp:HyperLink>(<asp:Label runat="server" Text=""
                                                        ID="lblAssoCountT"></asp:Label>)&nbsp;/&nbsp;
                                                                <asp:HyperLink ID="hplFrom" TabIndex="19" runat="server" CssClass="hyperlink">
							                        <font color="#180073">From</font></asp:HyperLink>(<asp:Label runat="server" Text=""
                                                        ID="lblAssoCountF"></asp:Label>)
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="normal1" align="right">
                                                                <asp:HyperLink ID="hplAddress" runat="server" CssClass="hyperlink">
							                        <font color="#180073">Address</font></asp:HyperLink>
                                                            </td>
                                                            <td class="normal1" colspan="5">
                                                                <asp:Label ID="lblAddress" runat="server" Width="100%" Text=""></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table cellspacing="1" cellpadding="1" width="100%" border="0">
                                                        <tr>
                                                            <td class="normal1" align="right">
                                                                Comments
                                                            </td>
                                                            <td class="normal1" colspan="3">
                                                                <asp:TextBox ID="txtComments" runat="server" CssClass="signup" Width="750" TextMode="MultiLine"
                                                                    Rows="3"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="normal7" align="right">
                                                                Web Links
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnEditWebLnk" runat="server" Text="Edit Web Links" CssClass="button">
                                                                </asp:Button>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="TEXT" align="right">
                                                                <asp:Label ID="lblWebLink1" runat="server"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtWebLink1" runat="server" CssClass="signup" Width="250px" value="http://"></asp:TextBox>&nbsp;
                                                                <asp:Button ID="btnWebGo1" runat="server" Text="Go" Width="25" CssClass="button">
                                                                </asp:Button>
                                                            </td>
                                                            <td class="TEXT" align="right">
                                                                <asp:Label ID="lblWebLink2" runat="server"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtWebLink2" runat="server" CssClass="signup" Width="250px" value="http://"></asp:TextBox>&nbsp;
                                                                <asp:Button ID="btnWebGo2" runat="server" Text="Go" Width="25" CssClass="button">
                                                                </asp:Button>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="TEXT" align="right">
                                                                <asp:Label ID="lblWebLink3" runat="server"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtWebLink3" runat="server" CssClass="signup" Width="250px" value="http://"></asp:TextBox>&nbsp;
                                                                <asp:Button ID="btnWebGo3" runat="server" Text="Go" Width="25" CssClass="button">
                                                                </asp:Button>
                                                            </td>
                                                            <td class="TEXT" align="right">
                                                                <asp:Label ID="lblWebLink4" runat="server"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtWebLink4" runat="server" CssClass="signup" Width="250px" value="http://"></asp:TextBox>&nbsp;
                                                                <asp:Button ID="btnWebGo4" runat="server" Text="Go" Width="25" CssClass="button">
                                                                </asp:Button>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </ContentTemplate>
                                </igtab:Tab>
                            </Tabs>
                        </igtab:UltraWebTab>
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td class="normal4" align="center">
                        <asp:Literal ID="litMessage" runat="server"></asp:Literal>
                    </td>
                </tr>
            </table>
            <asp:TextBox ID="txtHidden" Style="display: none" runat="server"></asp:TextBox><asp:TextBox
                ID="txtrOwner" Style="display: none" runat="server"></asp:TextBox>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table id="Table1" width="100%" runat="server" visible="false">
        <tr>
            <td class="normal1" align="right">
                Billing Terms
            </td>
            <td class="normal1">
                <asp:DropDownList ID="ddlCredit" runat="server" CssClass="signup" Width="130" Visible="False">
                </asp:DropDownList>
                <asp:CheckBox ID="chkBillinTerms" runat="server"></asp:CheckBox>&nbsp; Msg.
                <asp:TextBox ID="txtSummary" runat="server" CssClass="signup"></asp:TextBox>
            </td>
            <td class="normal1" align="right">
                Net
            </td>
            <td class="normal1">
                <asp:TextBox ID="txtNetdays" runat="server" Width="40" CssClass="signup"></asp:TextBox>
                days
                <asp:RadioButton ID="radPlus" GroupName="rad" Checked="true" runat="server" Text="Plus">
                </asp:RadioButton>
                <asp:RadioButton ID="radMinus" runat="server" GroupName="rad" Text="Minus"></asp:RadioButton>&nbsp;
                <asp:TextBox ID="txtInterest" runat="server" Width="40" CssClass="signup"></asp:TextBox>
                %
                <td class="normal1" align="right">
                </td>
                <td>
                    <asp:CheckBoxList ID="chkTaxItems" CssClass="normal1" runat="server" RepeatDirection="Horizontal"
                        RepeatColumns="3">
                    </asp:CheckBoxList>
                </td>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
--%>