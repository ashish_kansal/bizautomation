Imports BACRM.BusinessLogic.Account
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin

Public Class frmAccountList : Inherits System.Web.UI.Page

    Dim strColumn As String
    Dim m_aryRightsForPage() As Integer
    'Dim m_aryRightsForOulook() As Integer
    Dim objCommon As New CCommon

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            ' Checking Rights for View
            m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmCompanyList.aspx", Session("UserContactID"), 25, 3)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AC")
            hplSettings.Attributes.Add("onclick", "return OpenSetting()")
            btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            If Not IsPostBack Then
                Session("Help") = "Organization"
                ddlSort.SelectedIndex = CInt(Session("SAccounts"))
                ' Checking rights for Import from outlook
                'm_aryRightsForOulook = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmAccountList.aspx", Session("userID"), 4, 3)
                'If m_aryRightsForOulook(RIGHTSTYPE.VIEW) = 0 Then
                '    '  btnImport.Visible = False
                'End If
                Dim dtTab As DataTable
                dtTab = Session("DefaultTab")
                If dtTab.Rows.Count > 0 Then
                    lbAccount.Text = IIf(IsDBNull(dtTab.Rows(0).Item("vcAccount")), "Accounts", dtTab.Rows(0).Item("vcAccount").ToString & "s")
                Else : lbAccount.Text = "Accounts"
                End If
                LoadProfile()
                If GetQueryStringVal(Request.QueryString("enc"), "FilterId") <> "" Then
                    If Not ddlSort.Items.FindByValue(GetQueryStringVal(Request.QueryString("enc"), "FilterId")) Is Nothing Then
                        ddlSort.SelectedItem.Selected = False
                        ddlSort.Items.FindByValue(GetQueryStringVal(Request.QueryString("enc"), "FilterId")).Selected = True
                    End If
                End If
                If Not GetQueryStringVal(Request.QueryString("enc"), "numProfile") Is Nothing Then
                    If Not ddlProfile.Items.FindByValue(GetQueryStringVal(Request.QueryString("enc"), "numProfile")) Is Nothing Then
                        ddlProfile.SelectedItem.Selected = False
                        ddlProfile.Items.FindByValue(GetQueryStringVal(Request.QueryString("enc"), "numProfile")).Selected = True
                    End If
                End If
                If GetQueryStringVal(Request.QueryString("enc"), "profileid") <> "" Then
                    If Not ddlProfile.Items.FindByValue(GetQueryStringVal(Request.QueryString("enc"), "profileid")) Is Nothing Then
                        ddlProfile.SelectedItem.Selected = False
                        ddlProfile.Items.FindByValue(GetQueryStringVal(Request.QueryString("enc"), "profileid")).Selected = True
                    End If
                End If
                Dim objcontact As New CContacts
                Dim dtTable As DataTable
                objcontact.DomainID = Session("DomainId")
                objcontact.UserCntID = Session("UsercontactId")
                objcontact.FormId = 11
                objcontact.ContactType = 2
                dtTable = objcontact.GetDefaultfilter()
                If dtTable.Rows.Count = 1 Then
                    If Not IsDBNull(dtTable.Rows(0).Item("numfilterId")) Then
                        If Not ddlSort.Items.FindByValue(dtTable.Rows(0).Item("numfilterId")) Is Nothing Then
                            ddlSort.SelectedItem.Selected = False
                            ddlSort.Items.FindByValue(dtTable.Rows(0).Item("numfilterId")).Selected = True
                        End If
                    End If
                End If
            End If

            If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then btnDelete.Visible = False
            '                btnDelete.Visible = False
            '                lnkDelete.Visible = True
            '                lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")

            If Not IsPostBack Then
                If Session("List") <> "Accounts" Then
                    Session("ListDetails") = Nothing
                    txtCurrrentPage.Text = 1
                    Session("Asc") = 1
                Else
                    Dim str As String()
                    str = Session("ListDetails").split(",")
                    txtSortColumn.Text = str(5)
                    ViewState("SortChar") = str(0)
                    txtCustomer.Text = str(3)
                    txtFirstName.Text = str(1)
                    txtLastName.Text = str(2)
                    txtCurrrentPage.Text = str(4)
                    Session("Asc") = str(6)
                End If
                Session("List") = "Accounts"
                BindDatagrid()
            End If
            If txtSortChar.Text <> "" Then
                ViewState("SortChar") = txtSortChar.Text
                ViewState("Column") = "vcCompanyName"
                Session("Asc") = 0
                BindDatagrid()
                txtSortChar.Text = ""
            End If
            If txtSortColumn.Text <> "" Then
                If Session("Asc") = 0 Then
                    Session("Asc") = 1
                Else : Session("Asc") = 0
                End If
                BindDatagrid()
                txtSortColumn.Text = ""
            End If
            '   btnImport.Attributes.Add("onclick", "return GoImport()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadProfile()
        Try
            Dim objUserAccess As New UserAccess
            objUserAccess.RelID = 46
            objUserAccess.DomainID = Session("DomainID")
            ddlProfile.DataSource = objUserAccess.GetRelProfileD
            ddlProfile.DataTextField = "ProName"
            ddlProfile.DataValueField = "numProfileID"
            ddlProfile.DataBind()
            ddlProfile.Items.Insert(0, New ListItem("---Select One---", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindDatagrid(Optional ByVal CreateCol As Boolean = True)
        Try
            Dim dtAccounts As DataTable
            Dim objAccounts As New CAccounts
            Dim SortChar As Char
            If ViewState("SortChar") <> "" Then
                SortChar = ViewState("SortChar")
            Else : SortChar = "0"
            End If
            With objAccounts
                .CRMType = 2
                .UserCntID = Session("UserContactID")
                .UserRightType = m_aryRightsForPage(RIGHTSTYPE.VIEW)
                .SortOrder = ddlSort.SelectedItem.Value
                .FirstName = txtFirstName.Text
                .LastName = txtLastName.Text
                .SortCharacter = SortChar
                .CustName = txtCustomer.Text
                .DomainID = Session("DomainID")
                .bitPartner = True
                If txtCurrrentPage.Text.Trim <> "" Then
                    .CurrentPage = txtCurrrentPage.Text
                Else : .CurrentPage = 1
                End If
                .PageSize = Session("PagingRows")
                .TotalRecords = 0
                .bitPartner = False
                .Profile = ddlProfile.SelectedValue
                If txtSortColumn.Text <> "" Then
                    .columnName = txtSortColumn.Text
                Else : .columnName = "DM.bintcreateddate"
                End If
                If Session("Asc") = 1 Then
                    .columnSortOrder = "Desc"
                Else : .columnSortOrder = "Asc"
                End If
            End With
            Session("ListDetails") = SortChar & "," & txtFirstName.Text & "," & txtLastName.Text & "," & txtCustomer.Text & "," & txtCurrrentPage.Text & "," & txtSortColumn.Text & "," & Session("Asc")
            dtAccounts = objAccounts.GetAccounts
            If objAccounts.TotalRecords = 0 Then
                hidenav.Visible = False
                lblRecordCount.Text = 0
            Else
                hidenav.Visible = True
                lblRecordCount.Text = String.Format("{0:#,###}", objAccounts.TotalRecords)
                Dim strTotalPage As String()
                Dim decTotalPage As Decimal
                decTotalPage = lblRecordCount.Text / Session("PagingRows")
                decTotalPage = Math.Round(decTotalPage, 2)
                strTotalPage = CStr(decTotalPage).Split(".")
                If (lblRecordCount.Text Mod Session("PagingRows")) = 0 Then
                    lblTotal.Text = strTotalPage(0)
                    txtTotalPage.Text = strTotalPage(0)
                Else
                    lblTotal.Text = strTotalPage(0) + 1
                    txtTotalPage.Text = strTotalPage(0) + 1
                End If
                txtTotalRecords.Text = lblRecordCount.Text
            End If
            ' dtgAccount.DataSource = dtAccounts
            '  dtgAccount.DataBind()
            Dim i As Integer
            If CreateCol = True Then
                Dim bField As BoundField
                Dim Tfield As TemplateField
                gvSearch.Columns.Clear()
                For i = 0 To dtAccounts.Columns.Count - 1
                    If dtAccounts.Columns(i).ColumnName = "Contact Email" Or dtAccounts.Columns(i).ColumnName = "Organization Website" Or dtAccounts.Columns(i).ColumnName = "Organization Name" Or dtAccounts.Columns(i).ColumnName = "First Name" Or dtAccounts.Columns(i).ColumnName = "Last Name" Then
                        Tfield = New TemplateField
                        Dim str As String()
                        str = dtAccounts.Columns(i).ColumnName.Split("~")

                        Tfield.HeaderTemplate = New MyTempAccounts(ListItemType.Header, str(0), str(1), dtAccounts.Columns(0).ColumnName, dtAccounts.Columns(1).ColumnName, dtAccounts.Columns(4).ColumnName, dtAccounts.Columns(2).ColumnName, dtAccounts.Columns(3).ColumnName)
                        Tfield.ItemTemplate = New MyTempAccounts(ListItemType.Item, str(0), str(1), dtAccounts.Columns(0).ColumnName, dtAccounts.Columns(1).ColumnName, dtAccounts.Columns(4).ColumnName, dtAccounts.Columns(2).ColumnName, dtAccounts.Columns(3).ColumnName)
                        gvSearch.Columns.Add(Tfield)
                    Else
                        If i < 6 Then
                            bField = New BoundField
                            bField.DataField = dtAccounts.Columns(i).ColumnName
                            bField.Visible = False
                            gvSearch.Columns.Add(bField)
                        Else
                            Tfield = New TemplateField
                            Dim str As String()
                            str = dtAccounts.Columns(i).ColumnName.Split("~")
                            Tfield.HeaderTemplate = New MyTempAccounts(ListItemType.Header, str(0), str(1), dtAccounts.Columns(0).ColumnName, dtAccounts.Columns(1).ColumnName, dtAccounts.Columns(4).ColumnName, dtAccounts.Columns(2).ColumnName, dtAccounts.Columns(3).ColumnName)

                            Tfield.ItemTemplate = New MyTempAccounts(ListItemType.Item, str(0), str(1), dtAccounts.Columns(0).ColumnName, dtAccounts.Columns(1).ColumnName, dtAccounts.Columns(4).ColumnName, dtAccounts.Columns(2).ColumnName, dtAccounts.Columns(3).ColumnName)
                            gvSearch.Columns.Add(Tfield)
                        End If
                    End If
                Next
                If i >= 6 Then
                    Tfield = New TemplateField
                    Tfield.HeaderTemplate = New MyTempAccounts(ListItemType.Header, "CheckBox", "", dtAccounts.Columns(0).ColumnName, dtAccounts.Columns(1).ColumnName, dtAccounts.Columns(4).ColumnName, dtAccounts.Columns(2).ColumnName, dtAccounts.Columns(3).ColumnName)
                    Tfield.ItemTemplate = New MyTempAccounts(ListItemType.Item, "CheckBox", "", dtAccounts.Columns(0).ColumnName, dtAccounts.Columns(1).ColumnName, dtAccounts.Columns(4).ColumnName, dtAccounts.Columns(2).ColumnName, dtAccounts.Columns(3).ColumnName)
                    gvSearch.Columns.Add(Tfield)
                End If
            End If
            gvSearch.DataSource = dtAccounts
            gvSearch.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            txtCurrrentPage.Text = 1
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlSort_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlSort.SelectedIndexChanged
        Try
            txtCurrrentPage.Text = 1
            BindDatagrid()
            Session("SAccounts") = ddlSort.SelectedIndex
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkLast.Click
        Try
            txtCurrrentPage.Text = txtTotalPage.Text
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkPrevious.Click
        Try
            If txtCurrrentPage.Text = 1 Then
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text - 1
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkFirst.Click
        Try
            txtCurrrentPage.Text = 1
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkNext.Click
        Try
            If txtCurrrentPage.Text = txtTotalPage.Text Then
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 1
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk2.Click
        Try
            If txtCurrrentPage.Text + 1 = txtTotalPage.Text Or txtCurrrentPage.Text + 1 > txtTotalPage.Text Then
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 2
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk3.Click
        Try
            If txtCurrrentPage.Text + 2 = txtTotalPage.Text Or txtCurrrentPage.Text + 2 > txtTotalPage.Text Then
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 3
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk4.Click
        Try
            If txtCurrrentPage.Text + 3 = txtTotalPage.Text Or txtCurrrentPage.Text + 3 > txtTotalPage.Text Then
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 4
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk5.Click
        Try
            If txtCurrrentPage.Text + 4 = txtTotalPage.Text Or txtCurrrentPage.Text + 4 > txtTotalPage.Text Then
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 5
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub txtCurrrentPage_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCurrrentPage.TextChanged
        Try
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    'Private Sub dtgAccount_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dtgAccount.ItemDataBound
    '    Try

    '        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
    '            Dim hplEmail As HyperLink
    '            hplEmail = e.Item.FindControl("hplEmail")
    '            hplEmail.NavigateUrl = "#"
    '            If Session("CompWindow") = 1 Then
    '                hplEmail.NavigateUrl = "mailto:" & hplEmail.Text
    '            Else
    '                hplEmail.Attributes.Add("onclick", "return OpemEmail('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail=" & hplEmail.Text & "&DivID=" & e.Item.Cells(0).Text & "')")
    '            End If
    '            Dim lngDivID As Long
    '            Dim btnDelete As Button
    '            Dim lnkDelete As LinkButton
    '            lnkDelete = e.Item.FindControl("lnkDelete")
    '            btnDelete = e.Item.FindControl("btnDelete")
    '            lngDivID = CLng(e.Item.Cells(0).Text)
    '            If lngDivID = Session("UserDivisionID") Then
    '                btnDelete.Visible = False
    '                lnkDelete.Visible = True
    '                lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
    '                Exit Sub
    '            End If
    '            If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
    '                btnDelete.Visible = False
    '                lnkDelete.Visible = True
    '                lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
    '            ElseIf m_aryRightsForPage(RIGHTSTYPE.DELETE) = 1 Then
    '                Try
    '                    If e.Item.Cells(1).Text = Session("UserContactID") Then
    '                        btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
    '                    Else
    '                        btnDelete.Visible = False
    '                        lnkDelete.Visible = True
    '                        lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
    '                    End If
    '                Catch ex As Exception

    '                End Try
    '            ElseIf m_aryRightsForPage(RIGHTSTYPE.DELETE) = 2 Then
    '                Try
    '                    Dim i As Integer
    '                    Dim dtTerritory As DataTable
    '                    dtTerritory = Session("UserTerritory")
    '                    If e.Item.Cells(2).Text = 0 Then
    '                        btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
    '                    Else
    '                        Dim chkDelete As Boolean = False
    '                        For i = 0 To dtTerritory.Rows.Count - 1
    '                            If e.Item.Cells(2).Text = dtTerritory.Rows(i).Item("numTerritoryId") Then
    '                                chkDelete = True
    '                            End If
    '                        Next
    '                        If chkDelete = True Then
    '                            btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
    '                        Else
    '                            btnDelete.Visible = False
    '                            lnkDelete.Visible = True
    '                            lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
    '                        End If
    '                    End If
    '                Catch ex As Exception

    '                End Try
    '            ElseIf m_aryRightsForPage(RIGHTSTYPE.DELETE) = 3 Then
    '                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
    '            End If
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub dtgAccount_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgAccount.ItemCommand
    '    Dim lngDivID As Long
    '    Dim strEmail As String
    '    Try
    '        If e.CommandName <> "Sort" Then
    '            lngDivID = CLng(e.Item.Cells(0).Text)
    '            strEmail = e.Item.Cells(9).Text
    '        End If
    '        If e.CommandName = "Company" Then
    '            If Session("EnableIntMedPage") = 1 Then
    '                Response.Redirect("../pageLayout/frmAccountdtl.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&DivId=" & lngDivID)
    '            Else
    '                Response.Redirect("../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&DivId=" & lngDivID)
    '            End If
    '            ' Response.Redirect("../Account/frmAccounts.aspx?DivID=" & lngDivID)
    '        ElseIf e.CommandName = "Contact" Then
    '            Dim objCommon As New CCommon
    '            objCommon.DivisionID = lngDivID
    '            objCommon.charModule = "D"
    '            objCommon.GetCompanySpecificValues1()
    '            If Session("EnableIntMedPage") = 1 Then
    '                Response.Redirect("../pageLayout/frmContact.aspx?frm=accountlist&fdas89iu=098jfd&CntId=" & objCommon.ContactID)
    '            Else
    '                Response.Redirect("../contact/frmContacts.aspx?frm=accountlist&fdas89iu=098jfd&CntId=" & objCommon.ContactID)
    '            End If

    '        ElseIf e.CommandName = "Email" Then
    '            Response.Write("<scr" & "ipt>" & vbCrLf)
    '            Response.Write("open(""mailto:" & strEmail & " "",""ThankYou"",""width=550,height=450,scrollbars=no"");" & vbCrLf)
    '            Response.Write("</scr" & "ipt>")
    '        ElseIf e.CommandName = "Delete" Then
    '            Dim objAccount As New CAccounts
    '            If ddlSort.SelectedItem.Value = 9 Then
    '                Dim objContacts As New CContacts
    '                objContacts.byteMode = 1
    '                objContacts.UserCntID = Session("UserContactID")
    '                objContacts.ContactID = lngDivID '' passing division id instead of contact if for deleting compnay from favorites
    '                objContacts.ManageFavorites()
    '                litMessage.Text = "Deleted from Favorites"
    '                BindDatagrid()
    '            Else
    '                With objAccount
    '                    .DivisionID = lngDivID
    '                    .UserCntID = Session("UserContactID")
    '                    .DomainID = Session("DomainID")
    '                End With
    '                If objAccount.DeleteOrg = False Then
    '                    litMessage.Text = "Company cannot be deleted, because it has an open balance or authoritative BizDocs."
    '                Else
    '                    BindDatagrid()
    '                End If
    '            End If

    '        End If
    '    Catch ex As Exception
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub dtgAccount_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dtgAccount.SortCommand
    '    Try
    '        strColumn = e.SortExpression.ToString()
    '        If Viewstate("Column") <> strColumn Then
    '            Viewstate("Column") = strColumn
    '            Session("Asc") = 0
    '        Else
    '            If Session("Asc") = 0 Then
    '                Session("Asc") = 1
    '            Else
    '                Session("Asc") = 0
    '            End If
    '        End If
    '        BindDatagrid()
    '    Catch ex As Exception
    '        Response.Write(ex)
    '    End Try

    'End Sub

    Private Sub ddlProfile_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProfile.SelectedIndexChanged
        Try
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
        Try
            BindDatagrid(False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            Dim strDivid As String() = txtDelContactIds.Text.Split(",")
            Dim i As Int16 = 0
            Dim lngDivID As Long
            Dim lngRecOwnID As Long
            Dim lngTerrID As Long
            Dim objAccount As New CAccounts
            For i = 0 To strDivid.Length - 1
                lngDivID = strDivid(i).Split("~")(0)
                lngRecOwnID = strDivid(i).Split("~")(1)
                lngTerrID = strDivid(i).Split("~")(2)
                If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 1 Then
                    Try
                        If lngRecOwnID = Session("UserContactID") Then
                            If ddlSort.SelectedItem.Value = 9 Then
                                Dim objContacts As New CContacts
                                objContacts.byteMode = 1
                                objContacts.UserCntID = Session("UserContactID")
                                objContacts.ContactID = lngDivID '' passing division id instead of contact if for deleting compnay from favorites
                                objContacts.ManageFavorites()
                                litMessage.Text = "Deleted from Favorites"
                            Else
                                If lngDivID <> Session("UserDivisionID") Then
                                    With objAccount
                                        .DivisionID = lngDivID
                                        .UserCntID = Session("UserContactID")
                                        .DomainID = Session("DomainID")
                                    End With
                                    If objAccount.DeleteOrg = False Then litMessage.Text = "Some Company's cannot be deleted."
                                Else : litMessage.Text = "Some Company's cannot be deleted."
                                End If
                            End If
                        End If
                    Catch ex As Exception

                    End Try
                ElseIf m_aryRightsForPage(RIGHTSTYPE.DELETE) = 2 Then
                    Try
                        Dim j As Integer
                        Dim dtTerritory As New DataTable
                        dtTerritory = Session("UserTerritory")
                        Dim chkDelete As Boolean = False
                        If lngTerrID = 0 Then
                            chkDelete = False
                        Else
                            For j = 0 To dtTerritory.Rows.Count - 1
                                If lngTerrID = dtTerritory.Rows(j).Item("numTerritoryId") Then chkDelete = True
                            Next
                        End If
                        If chkDelete = True Then
                            If ddlSort.SelectedItem.Value = 9 Then
                                Dim objContacts As New CContacts
                                objContacts.byteMode = 1
                                objContacts.UserCntID = Session("UserContactID")
                                objContacts.ContactID = lngDivID '' passing division id instead of contact if for deleting compnay from favorites
                                objContacts.ManageFavorites()
                                litMessage.Text = "Deleted from Favorites"
                            Else
                                If lngDivID <> Session("UserDivisionID") Then
                                    With objAccount
                                        .DivisionID = lngDivID
                                        .UserCntID = Session("UserContactID")
                                        .DomainID = Session("DomainID")
                                    End With
                                    If objAccount.DeleteOrg = False Then litMessage.Text = "Some Company's cannot be deleted."
                                Else : litMessage.Text = "Some Company's cannot be deleted."
                                End If
                            End If
                        End If
                    Catch ex As Exception

                    End Try
                ElseIf m_aryRightsForPage(RIGHTSTYPE.DELETE) = 3 Then
                    If ddlSort.SelectedItem.Value = 9 Then
                        Dim objContacts As New CContacts
                        objContacts.byteMode = 1
                        objContacts.UserCntID = Session("UserContactID")
                        objContacts.ContactID = lngDivID '' passing division id instead of contact if for deleting compnay from favorites
                        objContacts.ManageFavorites()
                        litMessage.Text = "Deleted from Favorites"
                    Else
                        If lngDivID <> Session("UserDivisionID") Then
                            With objAccount
                                .DivisionID = lngDivID
                                .UserCntID = Session("UserContactID")
                                .DomainID = Session("DomainID")
                            End With
                            If objAccount.DeleteOrg = False Then litMessage.Text = "Some Company's cannot be deleted."
                        Else : litMessage.Text = "Some Company's cannot be deleted."
                        End If
                    End If
                End If
            Next
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class

Public Class MyTempAccounts : Implements ITemplate

    Dim TemplateType As ListItemType
    Dim Field1, Field2, Field3, Field4, Field5, Field6, Field7 As String

    Sub New(ByVal type As ListItemType, ByVal fld1 As String, ByVal fld2 As String, ByVal fld3 As String, ByVal fld4 As String, ByVal fld5 As String, ByVal fld6 As String, ByVal fld7 As String)
        Try
            TemplateType = type
            Field1 = fld1
            Field2 = fld2
            Field3 = fld3
            Field4 = fld4
            Field5 = fld5
            Field6 = fld6
            Field7 = fld7
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub InstantiateIn(ByVal Container As Control) Implements ITemplate.InstantiateIn
        Try
            Dim lbl1 As Label = New Label()
            Dim lbl2 As Label = New Label()
            Dim lbl3 As Label = New Label()
            Dim lnkButton As New LinkButton
            Dim lnk As New HyperLink
            Select Case TemplateType

                Case ListItemType.Header
                    If Field1 <> "CheckBox" Then
                        '   lnk.Attributes.Add("onclick", "return FilterWithinRecords('" & Field1 & "','" & Field2 & "','divColHeader')")
                        '   lnk.CssClass = "LinkArrow1"
                        '   lnk.Text = "8"
                        '   Container.Controls.Add(lnk)
                        lnkButton.ID = Field2
                        lnkButton.Text = Field1
                        lnkButton.ForeColor = Color.White
                        lnkButton.Attributes.Add("onclick", "return SortColumn('" & Field2 & "')")
                        Container.Controls.Add(lnkButton)
                    Else
                        Dim chk As New CheckBox
                        chk.ID = "chk"
                        chk.Attributes.Add("onclick", "return SelectAll('gvSearch_ctl01_chk')")
                        Container.Controls.Add(chk)
                    End If
                Case ListItemType.Item
                    If Field1 <> "CheckBox" Then
                        AddHandler lbl1.DataBinding, AddressOf BindStringColumn
                        Container.Controls.Add(lbl1)
                    Else
                        Dim chk As New CheckBox
                        chk.ID = "chk"
                        AddHandler lbl1.DataBinding, AddressOf Bindvalue
                        AddHandler lbl2.DataBinding, AddressOf BindvalueTerr
                        AddHandler lbl3.DataBinding, AddressOf BindvalueROwnr
                        Container.Controls.Add(chk)
                        Container.Controls.Add(lbl1)
                        Container.Controls.Add(lbl2)
                        Container.Controls.Add(lbl3)
                    End If
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub Bindvalue(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lbl1 As Label = CType(Sender, Label)
            Dim Container As GridViewRow = CType(lbl1.NamingContainer, GridViewRow)
            lbl1.ID = "lbl1"
            lbl1.Text = DataBinder.Eval(Container.DataItem, Field4)
            lbl1.Attributes.Add("style", "display:none")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindvalueTerr(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lbl2 As Label = CType(Sender, Label)
            Dim Container As GridViewRow = CType(lbl2.NamingContainer, GridViewRow)
            lbl2.ID = "lbl2"
            lbl2.Text = DataBinder.Eval(Container.DataItem, Field6)
            lbl2.Attributes.Add("style", "display:none")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindvalueROwnr(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lbl3 As Label = CType(Sender, Label)
            Dim Container As GridViewRow = CType(lbl3.NamingContainer, GridViewRow)
            lbl3.ID = "lbl3"
            lbl3.Text = DataBinder.Eval(Container.DataItem, Field7)
            lbl3.Attributes.Add("style", "display:none")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindStringColumn(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lbl1 As Label = CType(Sender, Label)
            Dim Container As GridViewRow = CType(lbl1.NamingContainer, GridViewRow)
            If Field1 = "Contact Email" Or Field1 = "Organization Website" Or Field1 = "Organization Name" Or Field1 = "First Name" Or Field1 = "Last Name" Then
                lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, Field1 & "~" & Field2)), "", DataBinder.Eval(Container.DataItem, Field1 & "~" & Field2))
                Dim intermediatory As Integer
                intermediatory = IIf(System.Web.HttpContext.Current.Session("EnableIntMedPage") = 1, 1, 0)
                If Field1 = "Organization Name" Then
                    If DataBinder.Eval(Container.DataItem, Field5) = 0 Then
                        lbl1.Text = "<a  href='javascript:OpenWindow(" & DataBinder.Eval(Container.DataItem, Field4) & ",0," & intermediatory & ")'>" & lbl1.Text & "</a> &nbsp;&nbsp;&nbsp;"
                    ElseIf DataBinder.Eval(Container.DataItem, Field5) = 1 Then
                        lbl1.Text = "<a  href='javascript:OpenWindow(" & DataBinder.Eval(Container.DataItem, Field4) & ",1," & intermediatory & ")'>" & lbl1.Text & "</a> &nbsp;&nbsp;&nbsp;"
                    ElseIf DataBinder.Eval(Container.DataItem, Field5) = 2 Then
                        lbl1.Text = "<a  href='javascript:OpenWindow(" & DataBinder.Eval(Container.DataItem, Field4) & ",2," & intermediatory & ")'>" & lbl1.Text & "</a> &nbsp;&nbsp;&nbsp;"
                    End If
                ElseIf Field1 = "First Name" Or Field1 = "Last Name" Then
                    lbl1.Text = "<a  href='javascript:OpenContact(" & DataBinder.Eval(Container.DataItem, Field3) & "," & intermediatory & ")'>" & lbl1.Text & "</a> &nbsp;&nbsp;&nbsp;"
                ElseIf Field1 = "Contact Email" Then
                    lbl1.Attributes.Add("onclick", "return OpemEmail('" & lbl1.Text.ToString & "'," & DataBinder.Eval(Container.DataItem, Field3) & ")")
                    lbl1.Text = "<a  href=#>" & lbl1.Text & "</a> &nbsp;&nbsp;&nbsp;"
                ElseIf Field1 = "Organization Website" Then
                    lbl1.Attributes.Add("onclick", "return fn_GoToURL('" & lbl1.Text.ToString & "')")
                    lbl1.Text = "<a  href=#>" & lbl1.Text & "</a> &nbsp;&nbsp;&nbsp;"
                End If
            Else : lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, Field1 & "~" & Field2)), "", DataBinder.Eval(Container.DataItem, Field1 & "~" & Field2))
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindStringColumn1(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lbl1 As Label = CType(Sender, Label)
            Dim Container As GridViewRow = CType(lbl1.NamingContainer, GridViewRow)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class
