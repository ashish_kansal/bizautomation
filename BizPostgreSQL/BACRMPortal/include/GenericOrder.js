/*
Purpose:	Class for storing the form data
Created By: Debasish Tapan Nag
Parameter:	1) vcDbColumnName: The table columns where the data entered in these fields will be entered
			2) vcFieldName: The form field text
			3) vcDbColumnValue: The form field value
			4) numRowNum: The row number in which this control will appear
			5) numColumnNum: The column number in which this control will appear
			6) vcAssociatedControlType: The control type (EditBox/ SelectBox/ CheckBox etc)
			7) boolAOIField: Indicates if the field is AOI or not
Return		1) Nothing
*/
function classFormFieldConfig(vcDbColumnName,vcFieldName,vcDbColumnValue,vcDbColumnValueText,numRowNum,numColumnNum,vcAssociatedControlType,boolAOIField)
{
	this.vcDbColumnName = vcDbColumnName;
	this.vcFieldName = vcFieldName;
	this.vcDbColumnValue = vcDbColumnValue;
	this.vcDbColumnValueText = vcDbColumnValue;
	this.numRowNum = numRowNum;
	this.numColumnNum = numColumnNum;
	this.vcAssociatedControlType = vcAssociatedControlType;
	this.boolAOIField = boolAOIField;
}

/*
Purpose:	encodes the string
Created By: Debasish Tapan Nag
Parameter:	1) fBoxString: The source string
Return		1) The htmlencoded string
*/
function encodeMyHtml(fBoxString)
{
     encodedHtml = unescape(fBoxString);
     encodedHtml = encodedHtml.replace(/&/g,"\&amp;");
     encodedHtml = encodedHtml.replace(/\>/g,"&gt;");
     encodedHtml = encodedHtml.replace(/\</g,"&lt;");
     encodedHtml = encodedHtml.replace(/'/g,"\&apos;");
     encodedHtml = encodedHtml.replace(/"/g,"\&quot;");
     return encodedHtml;
} 
/*
Purpose:	Disables the buttons when submit takes place
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function disableAnotherSubmitClick(frmForm)
{
	frmForm.btnSubmit.disabled=true;
	frmForm.btnCancel.disabled=true;
}
/*
Purpose:	The processing required before the form is saved
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function PreSaveProcess(frmForm)
{
	var sXMLString = '<FormFields>'+'\n';
	var vcDbColumnValue;
	var vcDbColumnValueText;
	//Create xml for available fields and their data
	for(iIndex=0;iIndex<arrFieldConfig.length;iIndex++)
	{
			if(arrFieldConfig[iIndex].vcAssociatedControlType == 'EditBox' || arrFieldConfig[iIndex].vcAssociatedControlType == 'TextArea' || arrFieldConfig[iIndex].vcAssociatedControlType == 'TextBox')
			{
				vcDbColumnValueText = document.getElementById(arrFieldConfig[iIndex].vcDbColumnName).value;
				vcDbColumnValue = 0;
			}
			else if(arrFieldConfig[iIndex].vcAssociatedControlType == 'SelectBox')
			{
				vcDbColumnValue = eval('frmForm.'+arrFieldConfig[iIndex].vcDbColumnName+'.options[frmForm.'+arrFieldConfig[iIndex].vcDbColumnName+'.selectedIndex].value');
				if(vcDbColumnValue == '')
					vcDbColumnValue = 0;
				if(vcDbColumnValue != 0)
					vcDbColumnValueText = eval('frmForm.'+arrFieldConfig[iIndex].vcDbColumnName+'.options[frmForm.'+arrFieldConfig[iIndex].vcDbColumnName+'.selectedIndex].text');
				else
					vcDbColumnValueText = 0;

			}
			else if(arrFieldConfig[iIndex].vcAssociatedControlType == 'RadioBox' || arrFieldConfig[iIndex].vcAssociatedControlType == 'CheckBox')
			{	
				vcDbColumnValue = document.getElementById(arrFieldConfig[iIndex].vcDbColumnName).checked ? 1 : 0;
				vcDbColumnValueText = document.getElementById(arrFieldConfig[iIndex].vcDbColumnName).checked ? arrFieldConfig[iIndex].vcFieldName : '';
			}
			if(arrFieldConfig[iIndex].vcAssociatedControlType == 'CheckBox' && arrFieldConfig[iIndex].vcDbColumnName.substring(0,12) == 'vcInterested')
			{
				document.getElementById(arrFieldConfig[iIndex].vcDbColumnName).id = 'vcInterested' + parseInt(arrFieldConfig[iIndex].numRowNum+1);
				arrFieldConfig[iIndex].vcDbColumnName = 'vcInterested' + parseInt(arrFieldConfig[iIndex].numRowNum+1);
			}
			sXMLString += '<FormField>'+'\n';
			sXMLString += '<vcDbColumnName>' + arrFieldConfig[iIndex].vcDbColumnName + '</vcDbColumnName>'+'\n';
			sXMLString += '<vcFieldName>' + encodeMyHtml(arrFieldConfig[iIndex].vcFieldName) + '</vcFieldName>'+'\n';
			sXMLString += '<vcDbColumnValue>' + vcDbColumnValue + '</vcDbColumnValue>'+'\n';
			sXMLString += '<vcDbColumnValueText>' + encodeMyHtml(vcDbColumnValueText) + '</vcDbColumnValueText>'+'\n';
			sXMLString += '<numRowNum>' + arrFieldConfig[iIndex].numRowNum + '</numRowNum>'+'\n';
			sXMLString += '<numColumnNum>' + arrFieldConfig[iIndex].numColumnNum + '</numColumnNum>'+'\n';
			sXMLString += '<vcAssociatedControlType>' + arrFieldConfig[iIndex].vcAssociatedControlType + '</vcAssociatedControlType>'+'\n';
			sXMLString += '<boolAOIField>' + arrFieldConfig[iIndex].boolAOIField + '</boolAOIField>'+'\n';
			sXMLString += '</FormField>'+'\n';

	}
	sXMLString += '</FormFields>'+'\n';
	frmForm.hdXMLString.value = sXMLString;
	frmForm.hdEmail.value = document.getElementById('vcEmail').value;
}
/*
Purpose:	Call to send the password to the email address
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function sendPassword()
{
	target = 'frmSendPassword.aspx?sEmail=' + document.getElementById('hdEmail').value;
	frames['cntOpenItem'].location.href=target;
	//document.getElementById('divNew').style.visibility= 'visible';
}
/*
Purpose:	After the password is sent. the window is to be hidden
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function DoneSendingPassword()
{
	document.getElementById('divNew').style.visibility= 'hidden';
}

/*
Purpose:	The function to set the shipping address same as bitting address
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function MakeAddressShippingSameAsBilling(thisObject)
{
	if(thisObject.checked)
	{
		var arrBillFields = new Array('vcBillCountry','vcBillStreet','vcBilState','vcBillPostCode','vcBillCity');
		var arrShipFields = new Array('vcShipCountry','vcShipStreet','vcShipState','vcShipPostCode','vcShipCity');
		for(var iIndex = 0;iIndex < arrBillFields.length;iIndex++)
		{
			try{
				objElement = document.getElementById(arrBillFields[iIndex]);
				if(objElement.getAttribute('type').toLowerCase() == 'text')
				{
					document.getElementById(arrShipFields[iIndex]).value = document.getElementById(arrBillFields[iIndex]).value;
				}
				else if(objElement.getAttribute('type').toLowerCase() == 'select-one')
				{
					if(arrBillFields[iIndex] == 'vcBillCountry')
					{
					   objBillElement=document.getElementById('vcBilState')
					    objShipElement = document.getElementById('vcShipState');
					     Remove(objShipElement)
					    for(var i=0; i<objBillElement.options.length; i++)
				        {
        				  
				            var no = new Option();
					        no.value = objBillElement.options[i].value
					        no.text = objBillElement.options[i].text
				            objShipElement.options[i]=no;
        				 
				        } 
				    }
				    document.getElementById(arrShipFields[iIndex]).selectedIndex = document.getElementById(arrBillFields[iIndex]).selectedIndex;
				}
			}catch(e){}
		}
	}
}

	function Remove(box) 
			{
			for(var i=0; i<box.options.length; i++) 
			{
			
				for(var j=i; j<box.options.length-1; j++)  
				{
				box.options[j].value = box.options[j+1].value;
				box.options[j].text = box.options[j+1].text;
				}
				var ln = i;
				break;
			}
			if(ln < box.options.length) 
			{
			box.options.length -= 1;
			Remove(box);
			}
			}