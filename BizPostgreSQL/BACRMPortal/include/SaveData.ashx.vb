﻿Imports System.Web
Imports System.Web.Services
Imports System.Text
Imports BACRM.BusinessLogic.Common
Imports System.Web.Script.Serialization
Imports System.Collections.Specialized
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Accounting
Public Class SaveData
    Implements System.Web.IHttpHandler
    Implements System.Web.SessionState.IRequiresSessionState

    Dim objCommon As New CCommon

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim nvc As NameValueCollection = context.Request.Form

        If nvc("id") Is Nothing Or nvc("value") Is Nothing Then
            Exit Sub
        End If

        Dim id As String = nvc("id")

        Dim str As String() = id.Split("~")

        If str(0).ToLower = "organization" Then
            objCommon.ContactID = str(3)
            objCommon.DivisionID = str(4)

            objCommon.RecordId = str(4)
        ElseIf str(0).ToLower = "contact" Then
            objCommon.ContactID = str(3)
            objCommon.DivisionID = str(4)

            objCommon.RecordId = str(3)
        ElseIf str(0).ToLower = "wo" Then
            objCommon.numWOId = str(3)
            objCommon.numWODetailId = str(4)

            objCommon.RecordId = str(3)
        ElseIf str(0).ToLower = "project" Then
            objCommon.ProID = str(3)

            objCommon.RecordId = str(3)
        ElseIf str(0).ToLower = "opp" Then
            objCommon.OppID = str(3)

            objCommon.RecordId = str(3)
        ElseIf str(0).ToLower = "case" Then
            objCommon.CaseID = str(3)

            objCommon.RecordId = str(3)
        ElseIf str(0).ToLower = "tickler" Then
            objCommon.CommID = str(4)

        End If

        objCommon.PageName = str(0).ToLower

        objCommon.FormFieldId = str(1)
        objCommon.CustomField = str(2)
        objCommon.UserCntID = context.Session("UserContactID")
        objCommon.DomainID = context.Session("DomainID")
        objCommon.InlineEditValue = nvc("value")

        Dim returnValue As String = ""
        returnValue = CCommon.ToString(objCommon.SaveInLineEditData())
        context.Response.Write(returnValue)
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property




End Class