<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="webmenu.ascx.vb" Inherits="webmenu" %>

<script language="javascript" type="text/javascript">	
	function goto(target,frame,div,a,b)
	    {
	            target = target + '?DivID=' + document.getElementById('webmenu1_txtDivision').value+'&CntID=' + document.getElementById('webmenu1_txtContact').value+'&ProID=' + document.getElementById('webmenu1_txtPro').value+'&OpID=' + document.getElementById('webmenu1_txtOpp').value+'&CaseID=' + document.getElementById('webmenu1_txtCase').value
	            window.open(target,'','toolbar=no,titlebar=no,left=150, top=150,width='+a+',height='+b+',scrollbars=yes,resizable=yes')   
        }
    function OpenPop(target)
	    {
	            window.open(target,'','toolbar=no,titlebar=no,left=150, top=150,width=750,height=450,scrollbars=yes,resizable=yes')   
        }
            
		function PreSetSearchCriteria()
		{
			if(document.getElementById('webmenu1_ddlSearch').options.length > 1)
			{
				return true;
			}else{
				alert('Search fields has not been configured. please contact Administrator');
				return false;
			}
		}
  		function fun_Search()
		{
		PreSetSearchCriteria()
			
		}
      
        function onOk()
        {
            alert("hi")
        }
        function OpenConf()
        {
          window.open("../admin/frmShortcutMngUsr.aspx",'','toolbar=no,titlebar=no,left=150, top=150,width=1000,height=500,scrollbars=yes,resizable=yes')   
          return false
        }
        function fnRedirect(a)
        {
          
            document.location.href = a
            return false
        }
</script>

<body>
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server">
    </asp:ScriptManager>
    <table width="100%" bgcolor="#f3f4f5" border="0">
        <tr>
            <td class="normal1">
                <asp:Table ID="tblRecentItems" runat="server" GridLines="None" EnableViewState="true"
                    CellPadding="0" CellSpacing="0">
                </asp:Table>
            </td>
            <td align="right">
                <asp:UpdateProgress ID="up1" runat="server" DynamicLayout="False">
                    <ProgressTemplate>
                        <asp:Image ID="Image1" ImageUrl="~/images/updating.gif" runat="server" ImageAlign="Top" />
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
        <tr>
            <td>
                <input id="txtDivision" style="display: none" type="text" name="txtDivision" runat="server" />
                <input id="txtContact" style="display: none" type="text" name="txtContact" runat="server" />
                <input id="txtOpp" style="display: none" type="text" name="txtOpp" runat="server" />
                <input id="txtPro" style="display: none" type="text" name="txtPro" runat="server" />
                <input id="txtCase" style="display: none" type="text" name="txtCase" runat="server" />
            </td>
        </tr>
    </table>
    <br />
</body>
