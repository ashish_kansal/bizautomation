Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Partial Class webmenu : Inherits BACRMUserControl

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Public intPopupHeight As Integer
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'hypConfMenu.Attributes.Add("onclick", "return OpenConf()")
            Dim objAdmin As New ActionItem
            If GetQueryStringVal("DivID") <> "" Then txtDivision.Value = GetQueryStringVal("DivID")
            Dim divid As String = GetQueryStringVal("DivId")
            If divid <> "" And divid <> Nothing Then txtDivision.Value = GetQueryStringVal("DivId")
            If GetQueryStringVal("CntID") <> "" Then txtContact.Value = GetQueryStringVal("CntID")
            Dim CntID As String = GetQueryStringVal("CntId")
            If CntID <> "" And CntID <> Nothing Then txtContact.Value = GetQueryStringVal("CntId")
            If GetQueryStringVal("ProID") <> "" Then txtPro.Value = GetQueryStringVal("ProID")
            If GetQueryStringVal("CaseID") <> "" Then txtCase.Value = GetQueryStringVal("CaseID")
            If GetQueryStringVal("OpID") <> "" Then txtOpp.Value = GetQueryStringVal("OpID")
            objAdmin.UserCntID = Session("UserContactID")
            DisplayRecentItems(objAdmin.GetRecentItems)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub DisplayRecentItems(ByVal dtRecentItems As DataTable)
        Try
            Dim i As Integer
            Dim hpl As HyperLink
            Dim tblCell As TableCell
            Dim lbl As Label
            Dim tblRow As TableRow
            tblRow = New TableRow
            tblCell = New TableCell
            tblCell.CssClass = "normal1"
            tblCell.Text = "Last Viewed :&nbsp;&nbsp;"
            tblCell.VerticalAlign = VerticalAlign.Bottom
            tblRow.Cells.Add(tblCell)
            For i = 0 To dtRecentItems.Rows.Count - 1
                Dim strlink As String = "'"
                lbl = New Label
                hpl = New HyperLink
                tblCell = New TableCell
                hpl.CssClass = "normal1"
                lbl.Text = "&nbsp;&nbsp;"
                If dtRecentItems.Rows(i).Item("Type") = "C" Then
                    If dtRecentItems.Rows(i).Item("tintCRMType") = 0 Then
                        hpl.Text = dtRecentItems.Rows(i).Item("RecName")
                        If Session("EnableIntMedPage") = 1 Then
                            strlink = "../pagelayout/frmLeaddtl.aspx?DivID=" & dtRecentItems.Rows(i).Item("numDivisionID")
                        Else : strlink = "../Leads/frmLeads.aspx?DivID=" & dtRecentItems.Rows(i).Item("numDivisionID")
                        End If
                    ElseIf dtRecentItems.Rows(i).Item("tintCRMType") = 1 Then
                        hpl.Text = dtRecentItems.Rows(i).Item("RecName")
                        If Session("EnableIntMedPage") = 1 Then
                            strlink = "../pagelayout/frmProspectdtl.aspx?DivID=" & dtRecentItems.Rows(i).Item("numDivisionID")
                        Else : strlink = "../prospects/frmProspects.aspx?DivID=" & dtRecentItems.Rows(i).Item("numDivisionID")
                        End If
                    ElseIf dtRecentItems.Rows(i).Item("tintCRMType") = 2 Then
                        hpl.Text = dtRecentItems.Rows(i).Item("RecName")
                        If Session("EnableIntMedPage") = 1 Then
                            strlink = "../pageLayout/frmAccountdtl.aspx?klds+7kldf=fjk-las&DivId=" & dtRecentItems.Rows(i).Item("numDivisionID")
                        Else : strlink = "../Account/frmAccounts.aspx?klds+7kldf=fjk-las&DivId=" & dtRecentItems.Rows(i).Item("numDivisionID")
                        End If
                    End If
                    lbl.Text = "&nbsp;&nbsp;<img style='position:relative' src='../images/Building-16.gif'/>"
                ElseIf dtRecentItems.Rows(i).Item("Type") = "U" Then
                    hpl.Text = dtRecentItems.Rows(i).Item("RecName")
                    If Session("EnableIntMedPage") = 1 Then
                        strlink = "../pageLayout/frmContact.aspx?CntId=" & dtRecentItems.Rows(i).Item("numContactID")
                    Else : strlink = "../contact/frmContacts.aspx?CntId=" & dtRecentItems.Rows(i).Item("numContactID")
                    End If
                    lbl.Text = "&nbsp;&nbsp;<img style='position:relative' src='../images/Contact-16.gif'/>"
                ElseIf dtRecentItems.Rows(i).Item("Type") = "O" Then
                    hpl.Text = dtRecentItems.Rows(i).Item("RecName")
                    If Session("EnableIntMedPage") = 1 Then
                        strlink = "../pagelayout/frmOppurtunitydtl.aspx?opId=" & dtRecentItems.Rows(i).Item("numRecID")
                    Else : strlink = "../opportunity/frmOpportunities.aspx?opId=" & dtRecentItems.Rows(i).Item("numRecID")
                    End If
                    lbl.Text = "&nbsp;&nbsp;<img style='position:relative' src='../images/Dart-16.gif'/>"
                ElseIf dtRecentItems.Rows(i).Item("Type") = "S" Then
                    hpl.Text = dtRecentItems.Rows(i).Item("RecName")
                    If Session("EnableIntMedPage") = 1 Then
                        strlink = "../pageLayout/frmCasesdtl.aspx?CaseID=" & dtRecentItems.Rows(i).Item("numRecID")
                    Else : strlink = "../cases/frmCases.aspx?CaseID=" & dtRecentItems.Rows(i).Item("numRecID")
                    End If
                    lbl.Text = "&nbsp;&nbsp;<img style='position:relative' src='../images/briefcase-16.gif'/>"
                ElseIf dtRecentItems.Rows(i).Item("Type") = "P" Then
                    hpl.Text = dtRecentItems.Rows(i).Item("RecName")
                    If Session("EnableIntMedPage") = 1 Then
                        strlink = "../pageLayout//frmProjectsdtl.aspx?ProId=" & dtRecentItems.Rows(i).Item("numRecID")
                    Else : strlink = "../projects/frmProjects.aspx?ProId=" & dtRecentItems.Rows(i).Item("numRecID")
                    End If
                    lbl.Text = "&nbsp;&nbsp;<img style='position:relative' src='../images/Compass-16.gif'/>"
                ElseIf dtRecentItems.Rows(i).Item("Type") = "A" Then
                    hpl.Text = dtRecentItems.Rows(i).Item("RecName")
                    Dim str As String
                    str = "../admin/actionitemdetails.aspx?"
                    str = str & "CommId=" & dtRecentItems.Rows(i).Item("numRecID") & "&CaseId=" & dtRecentItems.Rows(i).Item("CaseID") & "&CaseTimeId=" & dtRecentItems.Rows(i).Item("caseTimeId") & "&CaseExpId=" & dtRecentItems.Rows(i).Item("caseExpId")
                    strlink = str
                    lbl.Text = "&nbsp;&nbsp;<img style='position:relative' src='../images/MasterList-16.gif'/>"
                End If
                hpl.Attributes.Add("onclick", "return fnRedirect('" & strlink & "')")
                ' hpl.NavigateUrl = "#"
                hpl.Text = "<font color='#180073'>" & hpl.Text & "</font>"
                hpl.CssClass = "hyperlink"
                tblCell.Controls.Add(lbl)
                tblCell.Controls.Add(hpl)
                tblRow.Cells.Add(tblCell)
            Next
            If dtRecentItems.Rows.Count > 0 Then tblRecentItems.Rows.Add(tblRow)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class
