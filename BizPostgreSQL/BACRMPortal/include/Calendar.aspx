<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Calendar.aspx.vb" Inherits="BACRMPortal.Calendar" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Calendar</title>
    <style type="text/css">
        BODY
        {
            border-right: #000000 1px solid;
            border-top: #000000 1px solid;
            margin: 0pt;
            border-left: #000000 1px solid;
            border-bottom: #000000 1px solid;
        }
    </style>
    <script language="javascript" type="text/javascript" >
        function ClearCross() {
            window.option
        }
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <table width="100%" align="center" bgcolor="#c6d3e7">
        <tr>
            <td align="center">
                <asp:LinkButton ID="btnFirst" Style="text-decoration: none" AccessKey="P" runat="server"
                    Font-Names="Webdings" ForeColor="Black" CausesValidation="False" ToolTip="Previous Year"
                    EnableViewState="False">9</asp:LinkButton>
                <asp:LinkButton ID="btnPrev" Style="text-decoration: none" runat="server" Font-Names="Webdings"
                    ForeColor="Black" ToolTip="Previous Month" EnableViewState="False">3</asp:LinkButton>
                <asp:DropDownList ID="ddlMonth" runat="server" Font-Names="Arial" Font-Size="11px"
                    ForeColor="White" Width="80px" BackColor="#8ca2bd" Font-Bold="True" AutoPostBack="True"
                    CssClass="slt9Normal">
                    <asp:ListItem Value="1">January</asp:ListItem>
                    <asp:ListItem Value="2">February</asp:ListItem>
                    <asp:ListItem Value="3">March</asp:ListItem>
                    <asp:ListItem Value="4">April</asp:ListItem>
                    <asp:ListItem Value="5">May</asp:ListItem>
                    <asp:ListItem Value="6">June</asp:ListItem>
                    <asp:ListItem Value="7">July</asp:ListItem>
                    <asp:ListItem Value="8">August</asp:ListItem>
                    <asp:ListItem Value="9">September</asp:ListItem>
                    <asp:ListItem Value="10">October</asp:ListItem>
                    <asp:ListItem Value="11">November</asp:ListItem>
                    <asp:ListItem Value="12">December</asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList ID="ddlYear" Font-Size="11px" runat="server" Font-Names="Arial"
                    ForeColor="White" Width="55px" BackColor="#8ca2bd" Font-Bold="True" AutoPostBack="True"
                    CssClass="slt9Normal">
                </asp:DropDownList>
                <asp:LinkButton ID="btnNext" Style="text-decoration: none" runat="server" Font-Names="Webdings"
                    ForeColor="Black" ToolTip="Next Month" EnableViewState="False">4</asp:LinkButton>
                <asp:LinkButton ID="btnLast" Style="text-decoration: none" runat="server" Font-Names="Webdings"
                    ForeColor="Black" ToolTip="Next Year" EnableViewState="False">:</asp:LinkButton>
                <asp:LinkButton ID="LinkButton1" Font-Bold="true" ForeColor="black" Font-Size="10px"
                    Font-Names="Arial" runat="server">X</asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Calendar ID="Calendar1" BorderWidth="0" runat="server" ForeColor="#00000" Width="220px"
                    Font-Size="9" Height="167px" ShowTitle="False" DayNameFormat="FirstTwoLetters"
                    SelectionMode="Day" Font-Name="arial" SelectedDayStyle-BackColor="red" OtherMonthDayStyle-ForeColor="white">
                    <DayStyle Font-Size="10pt" ForeColor="DimGray" BackColor="white"></DayStyle>
                    <NextPrevStyle Font-Size="10pt" Font-Names="arial" Font-Bold="True" ForeColor="White"
                        BackColor="#00A2FF"></NextPrevStyle>
                    <DayHeaderStyle ForeColor="Black" BackColor="#8ca2bd"></DayHeaderStyle>
                    <TitleStyle Font-Size="10pt" Font-Names="arial" Font-Bold="True" ForeColor="White"
                        BackColor="#00A2FF"></TitleStyle>
                    <OtherMonthDayStyle ForeColor="Silver"></OtherMonthDayStyle>
                    <SelectedDayStyle BackColor="DimGray" />
                </asp:Calendar>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
