////////////////////////////////////////////////////////////////
////////////////////////  SURVEY REGISTRATION //////////////////
////////////////////////////////////////////////////////////////
/*
Purpose:	Class for storing the form data
Created By: Debasish Tapan Nag
Parameter:	1) vcDbColumnName: The table columns where the data entered in these fields will be entered
2) vcFieldName: The form field text
3) vcDbColumnValue: The form field value
4) numRowNum: The row number in which this control will appear
5) numColumnNum: The column number in which this control will appear
6) vcAssociatedControlType: The control type (EditBox/ SelectBox/ CheckBox etc)
7) boolAOIField: Indicates if the field is AOI or not
Return		1) Nothing
*/
function classFormFieldConfig(vcDbColumnName, vcFieldName, vcDbColumnValue, vcDbColumnValueText, numRowNum, numColumnNum, vcAssociatedControlType, boolAOIField) {
    this.vcDbColumnName = vcDbColumnName;
    this.vcFieldName = vcFieldName;
    this.vcDbColumnValue = vcDbColumnValue;
    this.vcDbColumnValueText = vcDbColumnValue;
    this.numRowNum = numRowNum;
    this.numColumnNum = numColumnNum;
    this.vcAssociatedControlType = vcAssociatedControlType;
    this.boolAOIField = boolAOIField;
}
/*
Purpose:	encodes the string
Created By: Debasish Tapan Nag
Parameter:	1) fBoxString: The source string
Return		1) The htmlencoded string
*/
function encodeMyHtml(fBoxString) {
    encodedHtml = unescape(fBoxString);
    encodedHtml = encodedHtml.replace(/&/g, "\&amp;");
    encodedHtml = encodedHtml.replace(/\>/g, "&gt;");
    encodedHtml = encodedHtml.replace(/\</g, " &lt;");
    encodedHtml = encodedHtml.replace(/'/g, "\&apos;");
    encodedHtml = encodedHtml.replace(/"/g, "\&quot;");
    return encodedHtml;
}
/*
Purpose:	Users who are already registered will use this and proceed to surveys
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
var boolNewUserReg = false;
function EnableAlreadyRegisteredUsers(thisElement) {
    if (boolNewUserReg) {
        thisElement.value = 'Existing Users click here';
        document.getElementById('btnContinue').style.display = 'inline';
        document.getElementById('btnSignUp').style.display = 'none';
        document.getElementById('tblProspects').style.display = 'inline';
        document.getElementById('tblAlreadyRegisteredUser').style.display = 'none';
    } else {
        thisElement.value = 'New Registrants click here';
        document.getElementById('btnContinue').style.display = 'none';
        document.getElementById('btnSignUp').style.display = 'inline';
        document.getElementById('tblProspects').style.display = 'none';
        document.getElementById('tblAlreadyRegisteredUser').style.display = 'inline';
    }
    boolNewUserReg = !boolNewUserReg;
    return false;
}
/*
Purpose:	Check if the email is entered
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) True: Email is entered/ False: Email not entered
*/
function CheckIfEmailIsEntered() {
    if (document.getElementById('txtExistingContactEmail').value == '') {
        alert('Please check the following value(s)\nPlease enter Email.');
        return false;
    }
    return true;
}
/*
Purpose:	The processing required before the form is saved
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function PreSaveProcess(frmForm) {
}

////////////////////////////////////////////////////////////////
///////////////////////  SURVEY EXECUTION //////////////////////
////////////////////////////////////////////////////////////////
/*
Purpose:	Class for storing the form answer element list
Created By: Debasish Tapan Nag
Parameter:	1) vcAssociatedControlType: The control type (TextBox/ SelectBox/ CheckBox etc)
2) vcFieldName: The filed element name
Return		1) Nothing
*/
function SurveyAnswerElements(vcAssociatedControlType, vcFieldName) {
    this.vcAssociatedControlType = vcAssociatedControlType;
    this.vcFieldName = vcFieldName;
}

function SurveyRatingElements(vcAssociatedControlType, vcFieldName, numRating) {
    this.vcAssociatedControlType = vcAssociatedControlType;
    this.vcFieldName = vcFieldName;
    this.numRating = numRating;
}

var arrayElementPerTR = new Array(); //Array which holds the new status of rows per tr that is displayed
var IsSinglePageSurvey; //Stores the flag which indicates that the Survey is Single Page or not
/*
Purpose:	Intializes the array for the 1st survey question tablerow
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) Nothing
*/
function initArrayOnPageLoad() {
    arrayElementPerTR.length = 1;
    arrayElementPerTR[0] = SurveyMaintableRowIdsReplica.join();
    SurveyMaintableRowIds = SurveyMaintableRowIdsReplica;
    IsSinglePageSurvey = (document.getElementById('hdSinglePage').value == 'True') ? true : false;
    if (document.getElementById('hdSinglePage').value == 'False') {
        for (iOrigArrayIndex = SurveyMaintableRowIds.length - 1; iOrigArrayIndex >= 0; iOrigArrayIndex--) {
            if (iOrigArrayIndex != 0) {
                if ((document.getElementById('trQ_' + SurveyMaintableRowIds[iOrigArrayIndex]).style.display) = 'inline') {
                    document.getElementById('trQ_' + SurveyMaintableRowIds[iOrigArrayIndex]).style.display = 'none';
                    document.getElementById('trA_' + SurveyMaintableRowIds[iOrigArrayIndex]).style.display = 'none';
                    ClearFormControlSelection('SurveyRowIdsAnsControls' + SurveyMaintableRowIds[iOrigArrayIndex]);
                }
            } else {
                break;
            }
        }
    }

}
/*
Purpose:	Clears the form
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) Nothing
*/
function ClearAllSelection() {
    if (confirm('All your answers will be cleared and you will be taken to the first question. Do you want to continue ?')) {
        for (var iIndex = SurveyMaintableAllPossibleRowIds.length - 1; iIndex > 0; iIndex--) {
            ClearFormControlSelection('SurveyRowIdsAnsControls' + SurveyMaintableAllPossibleRowIds[iIndex]);
            if (!IsSinglePageSurvey) {
                document.getElementById('trQ_' + SurveyMaintableAllPossibleRowIds[iIndex]).style.display = 'none';
                document.getElementById('trA_' + SurveyMaintableAllPossibleRowIds[iIndex]).style.display = 'none';
            }
        }
        ClearFormControlSelection('SurveyRowIdsAnsControls' + SurveyMaintableAllPossibleRowIds[0]);
        initArrayOnPageLoad();
        ResetDBEntryParams();
    }
}
/*
Purpose:	Toggles the display of answers for the referenced tr
Created By: Debasish Tapan Nag
Parameter:	1) sTRRef:The reference tr whose answers are to be hidden/ displayed
Return		1) Nothing
*/
function ToggleAnswerDisplay(sTRRef) {
    if (document.getElementById('trA_' + sTRRef).style.display == 'none') {
        document.getElementById('trA_' + sTRRef).style.display = '';
        document.getElementById('spQ_' + sTRRef).innerHTML = '&uarr;';
    }
    else {
        document.getElementById('trA_' + sTRRef).style.display = 'none';
        document.getElementById('spQ_' + sTRRef).innerHTML = '&darr;';
    }
}
/*
Purpose:	Also clears the selections made for teh form controls
Created By: Debasish Tapan Nag
Parameter:	1) arrName:The Array containg the form element names for the tr
Return		1) Nothing
*/
function ClearFormControlSelection(arrName) {
    for (var iControlIndex = 0; iControlIndex < eval(arrName + '.length'); iControlIndex++) {
        switch (eval(arrName + '[' + iControlIndex + '].vcAssociatedControlType')) {
            case 'Radio':
                {
                    document.getElementById(eval(arrName + '[' + iControlIndex + '].vcFieldName')).checked = false;
                    break;
                }
            case 'TextBox':
                {
                    document.getElementById(eval(arrName + '[' + iControlIndex + '].vcFieldName')).value = '';
                    break;
                }
            case 'SelectBox':
                {
                    document.getElementById(eval(arrName + '[' + iControlIndex + '].vcFieldName')).selectedIndex = 0;
                    break;
                }
            case 'CheckBox':
                {
                    document.getElementById(eval(arrName + '[' + iControlIndex + '].vcFieldName')).checked = false;
                    break;
                }
        }
    }
}
/*
Purpose:	Set focus to the last control in the tr
Created By: Debasish Tapan Nag
Parameter:	1) Nothing
Return		1) Nothing
*/
function DisplayFocusToBottomOftable() {
    document.location.href = '#FocusBottomReference';
}
/*
Purpose:	Set focus to the top of the screen
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) Nothing
*/
function GoTop() {
    document.location.href = '#FocusTopReference';
}
/*
Purpose:	Return the position of the element in the array
Created By: Debasish Tapan Nag
Parameter:	1) arrValue: Element being searched
Return		1) The position of the element
*/
function getArrayElementPosition(arrName, arrValue) {
    var iPos = -1;
    for (var iOrigArrayIndex = 0; iOrigArrayIndex < arrName.length; iOrigArrayIndex++) {
        if (arrName[iOrigArrayIndex] == arrValue) {
            iPos = iOrigArrayIndex;
            break;
        }
    }
    return iPos;
}

/*
Purpose:	Displays the next row
Created By: Debasish Tapan Nag
Parameter:	1) vcReferenceTr: The Ref Tr beyond which one tr is to be displayed
2) flagWhichRow: The position of the reference tr
Return		1) Nothing
*/
function ShowNextRow(vcReferenceTr, flagWhichRow) {
    if (!IsSinglePageSurvey) {
        var iOrigArrayIndex;
        var boolRefInAllPossibleRowsSurpassed;
        boolRefInAllPossibleRows = false;
        if (SurveyMaintableRowIds.join().indexOf(vcReferenceTr) < 0) {
            for (iOrigArrayIndex = SurveyMaintableAllPossibleRowIds.length - 1; iOrigArrayIndex >= 0; iOrigArrayIndex--) {
                if (SurveyMaintableAllPossibleRowIds[iOrigArrayIndex] == vcReferenceTr)
                    boolRefInAllPossibleRowsSurpassed = true;

                if (boolRefInAllPossibleRowsSurpassed) {
                    var iPos = getArrayElementPosition(SurveyMaintableRowIds, SurveyMaintableAllPossibleRowIds[iOrigArrayIndex]);
                    if (iPos >= 0) {
                        SurveyMaintableRowIds.splice(iPos + 1, 0, vcReferenceTr);
                        break;
                    }
                }
            }
        }

        for (iOrigArrayIndex = SurveyMaintableRowIds.length - 1; iOrigArrayIndex >= 0; iOrigArrayIndex--) {
            if (SurveyMaintableRowIds[iOrigArrayIndex] != vcReferenceTr) {
                if ((document.getElementById('trQ_' + SurveyMaintableRowIds[iOrigArrayIndex]).style.display) = 'inline') {
                    document.getElementById('trQ_' + SurveyMaintableRowIds[iOrigArrayIndex]).style.display = 'none';
                    document.getElementById('trA_' + SurveyMaintableRowIds[iOrigArrayIndex]).style.display = 'none';
                    ClearFormControlSelection('SurveyRowIdsAnsControls' + SurveyMaintableRowIds[iOrigArrayIndex]);
                }
            } else {
                break;
            }
        }
        if (flagWhichRow == 'ThisRow')//Current Row is passed; find the next row in the array and display
        {
            for (iIndex = 0; iIndex < SurveyMaintableRowIds.length; iIndex++) {
                if (SurveyMaintableRowIds[iIndex] == vcReferenceTr) {
                    if (SurveyMaintableRowIdsModified == 1) {
                        SurveyMaintableRowIds = arrayElementPerTR[iIndex + 1].split(',');
                    } else {
                        SurveyMaintableRowIds = arrayElementPerTR[iIndex].split(',');
                    }
                    iIndex++;
                    break;
                }
            }
        }
        if (document.getElementById('trQ_' + SurveyMaintableRowIds[iIndex])) {
//            document.getElementById('trQ_' + SurveyMaintableRowIds[iIndex]).style.display = 'inline';
            //            document.getElementById('trA_' + SurveyMaintableRowIds[iIndex]).style.display = 'inline';
            document.getElementById('trQ_' + SurveyMaintableRowIds[iIndex]).style.display = '';
            document.getElementById('trA_' + SurveyMaintableRowIds[iIndex]).style.display = '';

            document.getElementById('spNum_' + SurveyMaintableRowIds[iIndex]).innerHTML = iIndex + 1;
            DisplayFocusToBottomOftable(); //Call to set focus to the next row
        }
        arrayElementPerTR[iIndex] = SurveyMaintableRowIds.join();
        iIndex++;
        while (iIndex < SurveyMaintableRowIds.length) {
            if (document.getElementById('trQ_' + SurveyMaintableRowIds[iIndex]).style.display != 'none') {
                if ((document.getElementById('trQ_' + SurveyMaintableRowIds[iOrigArrayIndex]).style.display) == 'none') {
                    document.getElementById('trQ_' + SurveyMaintableRowIds[iIndex]).style.display = 'none';
                    document.getElementById('trA_' + SurveyMaintableRowIds[iIndex]).style.display = 'none';
                    ClearFormControlSelection('SurveyRowIdsAnsControls' + SurveyMaintableRowIds[iIndex]);
                }
            }
            iIndex++;
        }
        SurveyMaintableRowIdsModified = 0;
    }
}
/*
Purpose:	Hides the current row
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) Nothing
*/
function HideNextRow(vcReferenceTr, flagWhichRow) {
    if (!IsSinglePageSurvey) {
        if (flagWhichRow == 'ThisRow')//Current Row is passed; find the next row in the array and hide
        {
            for (iIndex = 0; iIndex < SurveyMaintableRowIds.length; iIndex++) {
                if (SurveyMaintableRowIds[iIndex] == vcReferenceTr) {
                    ShowNextRow(SurveyMaintableRowIds[iIndex - 1], flagWhichRow)
                    break;
                }
            }
        }
    }
}
/*
Purpose:	opens the URL in a popup
Created By: Debasish Tapan Nag
Parameter:	1) sURL: The URL to be opened
Return		1) Nothing
*/
function OpenURL(sURL) {
    hndSurveyPopUpURL = window.open(sURL, '', 'toolbar=no,titlebar=no,left=50, top=50,width=800,height=500,scrollbars=yes,resizable=yes');
    hndSurveyPopUpURL.focus();
}
/*
Purpose:	Adds a Rating
Created By: Debasish Tapan Nag
Parameter:	1) numRating: The Rating to be added
Return		1) Nothing
*/
function AddRating(numRating, controlId) {


    if (controlId.type == 'checkbox') {
        if (controlId.checked)
            document.getElementById('hdSurveyRating').value = numRating + Number(document.getElementById('hdSurveyRating').value);
        else
            document.getElementById('hdSurveyRating').value = Number(document.getElementById('hdSurveyRating').value) - numRating;
    }

    else if (controlId.type == 'radio') {
        if (controlId.checked)
            document.getElementById('hdSurveyRating').value = numRating + Number(document.getElementById('hdSurveyRating').value);
        else
            document.getElementById('hdSurveyRating').value = Number(document.getElementById('hdSurveyRating').value) - numRating;
    }

    else if (controlId.type == 'text') {
        if (controlId.value != '')
            document.getElementById('hdSurveyRating').value = numRating + Number(document.getElementById('hdSurveyRating').value);
        else
            document.getElementById('hdSurveyRating').value = Number(document.getElementById('hdSurveyRating').value) - numRating;
    }

    else if (controlId.type == 'text') {
        if (controlId.value != '')
            document.getElementById('hdSurveyRating').value = numRating + Number(document.getElementById('hdSurveyRating').value);
        else
            document.getElementById('hdSurveyRating').value = Number(document.getElementById('hdSurveyRating').value) - numRating;
    }

    else if (controlId.type == 'select-one') {
        if (controlId.selectedIndex != 0)
            document.getElementById('hdSurveyRating').value = numRating + Number(document.getElementById('hdSurveyRating').value);
        else
            document.getElementById('hdSurveyRating').value = Number(document.getElementById('hdSurveyRating').value) - numRating;
    }

}
/*
Purpose:	Adds a Database Entry
Created By: Debasish Tapan Nag
Parameter:	1) numRating: The Rating to be added
Return		1) Nothing
*/
function CreateDBEntry(tIntCRMType, numGrpID, numCompanyType, numRecOwner) {
    document.getElementById('hdCRMType').value = tIntCRMType;
    document.getElementById('hdGroupId').value = numGrpID;
    document.getElementById('hdRelationShip').value = numCompanyType;
    document.getElementById('hdRecOwner').value = numRecOwner;
    document.getElementById('hdCreateDBEntry').value = 1;
}
/*
Purpose:	Resets thd DB Parameters since the Clear Selection button is clicked
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) Nothing
*/
function ResetDBEntryParams() {
    document.getElementById('hdSurveyRating').value = 0;
    document.getElementById('hdCreateDBEntry').value = 0;
}
/*
Purpose:	Indicates that the requested questions are to be skipped
Created By: Debasish Tapan Nag
Parameter:	1) vcCurrentTrId: The current tr id after which the new array elements are to be included
2) vcQuestionList: The questions to be included
3) numSurID: the Survey ID under consideration
Return		1) Nothing
*/
var SurveyMaintableRowIdsModified = 0;
function SkipQuestion(vcCurrentTrId, vcQuestionList, numSurID) {
    if (!IsSinglePageSurvey) {
        for (iMainArrayIndex = 0; iMainArrayIndex < SurveyMaintableRowIds.length; iMainArrayIndex++) {
            if (SurveyMaintableRowIds[iMainArrayIndex] == vcCurrentTrId) {
                if (SurveyMaintableRowIdsModified == 1)
                    SurveyMaintableRowIds = arrayElementPerTR[iMainArrayIndex + 1].split(',');
                else
                    SurveyMaintableRowIds = arrayElementPerTR[iMainArrayIndex].split(',');

                iMainArrayIndex++;
                break;
            }
        }
        var numQuestionIds = new Array();
        numQuestionIds = vcQuestionList.split(',');
        for (jIndex = numQuestionIds.length - 1; jIndex >= 0; jIndex--) {
            if (SurveyMaintableRowIds.join().indexOf(numSurID + '_' + numQuestionIds[jIndex] + '_' + numQuestionIds[jIndex]) >= 0) {
                for (iIndex = SurveyMaintableRowIds.length - 1; iIndex >= 0; iIndex--) {
                    if (SurveyMaintableRowIds[iIndex] == numSurID + '_' + numQuestionIds[jIndex] + '_' + numQuestionIds[jIndex]) {
                        SurveyMaintableRowIds.splice(iIndex, 1);
                        iIndex = -1;
                    }
                }
            } else {
                for (iIndex = SurveyMaintableRowIds.length - 1; iIndex >= 0; iIndex--) {
                    if (SurveyMaintableRowIds[iIndex] < numSurID + '_' + numQuestionIds[jIndex] + '_' + numQuestionIds[jIndex]) {
                        SurveyMaintableRowIds.splice(iIndex + 1, 0, numSurID + '_' + numQuestionIds[jIndex] + '_' + numQuestionIds[jIndex]);
                        iIndex = -1;
                    }
                }
            }
        }
        arrayElementPerTR[iMainArrayIndex] = SurveyMaintableRowIds.join();
        SurveyMaintableRowIdsModified = 1;
    }
}
/*
Purpose:	Includes the questions in the survey (from other surveys)
Created By: Debasish Tapan Nag
Parameter:	1) vcCurrentTrId: The current tr id after which the new array elements are to be included
2) vcQuestionList: The questions to be included
3) numSurID: the Survey ID under consideration
Return		1) Nothing
*/
function IncludeQuestions(vcCurrentTrId, numThisSurveyQuestionNumber, vcQuestionList, numSurID) {
    if (!IsSinglePageSurvey) {
        var numQuestionIds = new Array();
        numQuestionIds = vcQuestionList.split(',');
        var iIndex;
        for (iIndex = SurveyMaintableRowIds.length - 1; iIndex >= 0; iIndex--) {
            if (SurveyMaintableRowIds[iIndex] != vcCurrentTrId) {
                document.getElementById('trQ_' + SurveyMaintableRowIds[iIndex]).style.display = 'none';
                document.getElementById('trA_' + SurveyMaintableRowIds[iIndex]).style.display = 'none';
            } else {
                break;
            }
        }
        for (iIndex = 0; iIndex < SurveyMaintableRowIds.length; iIndex++) {
            if (SurveyMaintableRowIds[iIndex] == vcCurrentTrId) {
                if (SurveyMaintableRowIdsModified == 1)
                    SurveyMaintableRowIds = arrayElementPerTR[iIndex + 1].split(',');
                else
                    SurveyMaintableRowIds = arrayElementPerTR[iIndex].split(',');
                iIndex++;
                break;
            }
        }
        if (SurveyMaintableRowIds.join().indexOf(numSurID + '_' + numThisSurveyQuestionNumber + '_' + numQuestionIds[0]) < 0) {
            for (jIndex = numQuestionIds.length - 1; jIndex >= 0; jIndex--) {
                SurveyMaintableRowIds.splice(iIndex, 0, numSurID + '_' + numThisSurveyQuestionNumber + '_' + numQuestionIds[jIndex])
            }
        } else {
            for (jIndex = numQuestionIds.length - 1; jIndex >= 0; jIndex--) {
                SurveyMaintableRowIds.splice(iIndex, 1, numSurID + '_' + numThisSurveyQuestionNumber + '_' + numQuestionIds[jIndex])
            }
        }
        arrayElementPerTR[iIndex] = SurveyMaintableRowIds.join();
        SurveyMaintableRowIdsModified = 1;
    }
}
/*
Purpose:	Confirm the submission of the Survey
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) Nothing
*/
function ConfirmSurveySubmission(flagAskConfirmation) {
    if (flagAskConfirmation == true) {
        if (confirm('You have requested to conclude this survey. Please confirm.'))
            return ValidateEntries(); //Call to validate the Survey entries
        return false;
    } else {
        return ValidateEntries(); //Call to validate the Survey entries	    
    }
}
/*
Purpose:	Ask for Registration before confirming the submission of the Survey
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) Nothing
*/
function DoPostRegistration(numSurId, numDomainId) {
    if (ValidateEntries())	//Call to validate the Survey entries
    {
        hndRegWin = window.open('frmGenericFormSurvey.aspx?numSurId=' + numSurId + '&D=' + numDomainId + '&boolOnlyRegistration=N&bitPostRegistration=1');
        hndRegWin.focus();
        return false;
    }
}
function RegistrationCompleted(numRespondentId) {
    opener.focus();
    opener.document.getElementById('btnConcludeSurvey').onclick = function () { setTimeout("return ConfirmSurveySubmission(false)", 1500); };
    var sFormAction = opener.document.getElementById('frmSurveyExecutionSurvey').action;
    opener.document.getElementById('frmSurveyExecutionSurvey').action = sFormAction.replace('numRespondantID=0', 'numRespondantID=' + numRespondentId);
    opener.document.getElementById('btnConcludeSurvey').click();
    this.close();
}
/*
Purpose:	This makes a list of controls which can be then tracked by the server script
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) Nothing
*/
function ValidateEntries() {
    document.getElementById('hdTrackableSurveyControlEntries').value = '';
    var vcTrackableSurveyControlEntries = '';
    var ArrayNameofControlsTypes;
    var ArrayNameofControls;
    var ArrayRating;

    SurveyMaintableRowIds = arrayElementPerTR[arrayElementPerTR.length - 1].split(',');
    var SurveyMaintableRowIdsLength = SurveyMaintableRowIds.length;

    document.getElementById('hdSurveyRating').value =0;

    for (var itableRowIndex = 0; itableRowIndex < SurveyMaintableRowIdsLength; itableRowIndex++) {
        SurveyQuesAnsLength = eval('SurveyRowIdsAnsControls' + SurveyMaintableRowIds[itableRowIndex] + '.length');
        for (var iControlIndex = 0; iControlIndex < SurveyQuesAnsLength; iControlIndex++) {
            ArrayNameofControlsTypes = eval('SurveyRowIdsAnsControls' + SurveyMaintableRowIds[itableRowIndex] + '[' + iControlIndex + '].vcAssociatedControlType');
            ArrayNameofControls = eval('SurveyRowIdsAnsControls' + SurveyMaintableRowIds[itableRowIndex] + '[' + iControlIndex + '].vcFieldName');

            switch (ArrayNameofControlsTypes) {

                case 'Radio':
                    {
                        if (document.getElementById(ArrayNameofControls).checked) {
                            vcTrackableSurveyControlEntries += ',' + ArrayNameofControls;

                            ArrayRating = eval('SurveyRating' + SurveyMaintableRowIds[itableRowIndex] + '[' + iControlIndex + '].numRating');

                            document.getElementById('hdSurveyRating').value = ArrayRating + Number(document.getElementById('hdSurveyRating').value);
                        }
                        break;
                    }
                case 'TextBox':
                    {
                        if (document.getElementById(ArrayNameofControls).value != '') {
                            vcTrackableSurveyControlEntries += ',' + ArrayNameofControls;

                            ArrayRating = eval('SurveyRating' + SurveyMaintableRowIds[itableRowIndex] + '[' + iControlIndex + '].numRating');

                            document.getElementById('hdSurveyRating').value = ArrayRating + Number(document.getElementById('hdSurveyRating').value);

                        }
                        break;
                    }
                case 'SelectBox':
                    {
                        if (document.getElementById(ArrayNameofControls).selectedIndex != 0) {
                            ArrayRating = eval('SurveyRating' + SurveyMaintableRowIds[itableRowIndex] + '[' + document.getElementById(ArrayNameofControls).selectedIndex + '].numRating');
                            document.getElementById('hdSurveyRating').value = ArrayRating + Number(document.getElementById('hdSurveyRating').value);

                            var iSelectOptionsLength = document.getElementById(ArrayNameofControls).length;
                            vcTrackableSurveyControlEntries += ',' + ArrayNameofControls;
                            for (var iSelectOptions = 0; iSelectOptions < iSelectOptionsLength; iSelectOptions++) {
                                document.getElementById(ArrayNameofControls).options[iSelectOptions].value = iSelectOptions;
                            }
                        }
                        break;
                    }
                case 'CheckBox':
                    {
                        if (document.getElementById(ArrayNameofControls).checked) {
                            vcTrackableSurveyControlEntries += ',' + ArrayNameofControls;

                            ArrayRating = eval('SurveyRating' + SurveyMaintableRowIds[itableRowIndex] + '[' + iControlIndex + '].numRating');

                            document.getElementById('hdSurveyRating').value = ArrayRating + Number(document.getElementById('hdSurveyRating').value);

                        }
                        break;
                    }
            }
        }
    }

    document.getElementById('hdTrackableSurveyControlEntries').value = vcTrackableSurveyControlEntries.substring(1, vcTrackableSurveyControlEntries.length);
    //alert(document.getElementById('hdTrackableSurveyControlEntries').value)
    if (document.getElementById('hdTrackableSurveyControlEntries').value == '') {
        alert('You have not made any selection. Please specify your choices before concluding.');
        return false;
    }
    return true;
}

/*
Purpose:	The function to set the shipping address same as bitting address
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function MakeAddressShippingSameAsBilling(thisObject) {
    if (thisObject.checked) {
        var arrBillFields = new Array('numBillCountry', 'vcBillStreet', 'numBillState', 'vcBillPostCode', 'vcBillCity');
        var arrShipFields = new Array('numShipCountry', 'vcShipStreet', 'numShipState', 'vcShipPostCode', 'vcShipCity');
        for (var iIndex = 0; iIndex < arrBillFields.length; iIndex++) {
            try {
                objElement = document.getElementById(arrBillFields[iIndex]);
                if (objElement.getAttribute('type').toLowerCase() == 'text') {
                    document.getElementById(arrShipFields[iIndex]).value = document.getElementById(arrBillFields[iIndex]).value;
                }
                else if (objElement.getAttribute('type').toLowerCase() == 'select-one') {
                    document.getElementById(arrShipFields[iIndex]).selectedIndex = document.getElementById(arrBillFields[iIndex]).selectedIndex;
                    if (arrBillFields[iIndex] == 'numBillCountry')
                        filterStates4Country(document.getElementById(arrShipFields[iIndex]));
                }
            } catch (e) { }
        }
    }
}

function SetFocusOnFirstEmptyTextBoxControl() 
{
    if (document.getElementById('tblFormLeadBoxNonAOITable') != null) {
        // check the alignment on a number of cells in a table. 
        var table = document.getElementById("tblFormLeadBoxNonAOITable");
        var inputs = table.getElementsByTagName("input");
        for (var i = 0; i < inputs.length; i++) {
            if (inputs[i].type == "text") {
                if (inputs[i].value == "") {
                    document.getElementById(inputs[i].id.toString()).focus();
                    return true;
                }
            }
        }
    }
}
