<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCustomisePageLayout.aspx.vb" Inherits="BACRMPortal.frmCustomisePageLayout" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1"    runat="server">		
        <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <title>Customise PageLayout</title>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
         <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)"> 
 <link rel="stylesheet" href="../css/lists.css" type="text/css"/>

<script language="JavaScript" type="text/javascript" src="../javascript/coordinates.js"></script>
<script language="JavaScript" type="text/javascript" src="../javascript/drag.js"></script>
<script language="JavaScript" type="text/javascript" src="../javascript/dragdrop.js"></script>
<script language="JavaScript" type="text/javascript"><!--
   
		  

       

    window.onload = function() {
       // alert(document.getElementById("MaxRows").nodeValue);
       // alert(document.getElementById("rows").value);
        
        var max = document.getElementById("rows").value;
        //alert(max)
        for(var x=0;x <= max;x++){
                if (typeof(document.getElementById("x"+x))== 'object')
                    {
                        list = document.getElementById("x"+x);
                        DragDrop.makeListContainer( list, 'g2' );
                        list.onDragOver = function() { this.style["background"] = "none"; };
                        list.onDragOut = function() {this.style["background"] = "none"; };
                    }
                }
                
               
               
               
    };
        
        function getSort()
        {
          order = document.getElementById("order");
          order.value = DragDrop.serData('g2', null);
           
        }
        
        function showValue()
        {
          order = document.getElementById("order");
          alert(order.value);
        }
         function show()
        {
        document.getElementById("btnUpdate").click();
        return false;
          
        }
         function show1()
        {
        document.getElementById("btnUpdate").click();
        opener.location.reload(true); 
			window.close()
			return false;
        }
        function AddColumn()
        {
                order = document.getElementById("order");
                order.value = DragDrop.serData('g2', null);
                document.getElementById("btnAddColumn").click();
                return false;               
           
        }
        function DeleteColumn()
        {
                
                var max = document.getElementById("rows").value;
                if(max> 1)
                {   
                    order = document.getElementById("order");
                    order.value = DragDrop.serData('g2', null);
                    document.getElementById("btnDeleteColumn").click();
                    return false;
                }
                else
                {
                    alert("Columns Cannot Be Less Than One");
                    return false;
                }
        }
        function Close()
		{
			opener.location.reload(true); 
			window.close()
			return false;
		}
		function dlChange()
		{
		var mylist=document.getElementById("dlh")
		order = document.getElementById("type");
		order.value=mylist.options[mylist.selectedIndex].value
		 document.getElementById("btnBindData").click();
		return false;  
		}

 //-->
</script>
</head>
<body  >

    <form id="form1" runat="server"  >
    
       
                <asp:HiddenField ID="order" runat="server"  />
                <asp:TextBox runat="server" ID="rows" style=" display:none " ></asp:TextBox>
                <asp:Button ID="maxRow"   runat="server" style=" display:none "  />
               <asp:Button ID="btnUpdate"  Text="update" runat="server" style=" display:none "  />
               <asp:Button ID="btnAddColumn"  Text="update" runat="server" style=" display:none "  />
               <asp:Button ID="btnDeleteColumn"  Text="update" runat="server" style=" display:none "  />
               <asp:textbox id="type" style=" display:none "   Runat="server" Text="0"></asp:textbox>
              <asp:Button ID="btnBindData"  Text="update" runat="server" style=" display:none "  />        
               
               
             </form>
             
            
    <script type="text/xml-script">
        <page xmlns:script="http://schemas.microsoft.com/xml-script/2005">
            <references>
            </references>
            <components>
            </components>
        </page>
    </script>
</body>


</html>