' Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Account
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Common
Imports Infragistics.WebUI.UltraWebTab
Imports System.Reflection

Partial Class frmCustAccountdtl : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Dim lngDivId As Long
    Dim FromDate, ToDate As String
    

    Private designerPlaceholderDeclaration As System.Object
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim SI As Integer = 0
    Dim SI1 As Integer = 0
    Dim SI2 As Integer = 0
    Dim frm As String = ""
    Dim frm1 As String = ""
    Dim frm2 As String = ""

    'Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
    '    Try
    '        If Not GetQueryStringVal( "SI") Is Nothing Then
    '            SI = GetQueryStringVal( "SI")
    '        End If
    '        If Not GetQueryStringVal( "SI1") Is Nothing Then
    '            SI1 = GetQueryStringVal( "SI1")
    '        Else : SI1 = 0
    '        End If
    '        If Not GetQueryStringVal( "SI2") Is Nothing Then
    '            SI2 = GetQueryStringVal( "SI2")
    '        Else : SI2 = 0
    '        End If
    '        If Not GetQueryStringVal( "frm") Is Nothing Then
    '            frm = ""
    '            frm = GetQueryStringVal( "frm")
    '        Else : frm = ""
    '        End If
    '        If Not GetQueryStringVal( "frm1") Is Nothing Then
    '            frm1 = ""
    '            frm1 = GetQueryStringVal( "frm1")
    '        Else : frm1 = ""
    '        End If
    '        If Not GetQueryStringVal( "SI2") Is Nothing Then
    '            frm2 = ""
    '            frm2 = GetQueryStringVal( "frm2")
    '        Else : frm2 = ""
    '        End If
    '        lngDivId = Session("DivId")
    '        If Not IsPostBack Then
    '            Dim objCommon As New CCommon
    '            m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmAccounts.aspx", Session("UserContactID"), 15, 1)
    '            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../Common/frmAuthorization.aspx")
    '            If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then btnEdit.Visible = False
    '            If uwOppTab.Tabs.Count > SI Then uwOppTab.SelectedTabIndex = SI
    '        End If
    '        LoadTableInformation()
    '        DisplayDynamicFlds()
    '        litMessage.Text = ""
    '        btnLayout.Attributes.Add("onclick", "return ShowLayout('z','" & lngDivId & "','" & type.Text & "');")
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Sub LoadTableInformation()
    '    Try
    '        Dim dtTableInfo As DataTable
    '        Dim ds As New DataSet
    '        Dim objPageLayout As New CPageLayout
    '        Dim fields() As String
    '        Dim idcolumn As String = ""

    '        Dim dtAccountInfo As DataTable
    '        'Dim objProspectdtl As New CProspectsDtl
    '        'objProspectdtl.DivisionID = lngDivId
    '        'objProspectdtl.DomainID = Session("DomainID")
    '        'objProspectdtl.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
    '        'dtAccountInfo = objProspectdtl.GetCompanyInfoDtl1()            ' getting the details

    '        Dim objProspects As New CProspects
    '        objProspects.DivisionID = lngDivId
    '        objProspects.DomainID = Session("DomainID")
    '        objProspects.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
    '        dtAccountInfo = objProspects.GetCompanyInfoForEdit

    '        lblRecordOwner.Text = dtAccountInfo.Rows(0).Item("RecOwner")
    '        lblCreatedBy.Text = dtAccountInfo.Rows(0).Item("vcCreatedBy")
    '        lblLastModifiedBy.Text = dtAccountInfo.Rows(0).Item("vcModifiedBy")
    '        lblCustomerId.Text = lngDivId

    '        type.Text = IIf(IsDBNull(dtAccountInfo.Rows(0).Item("numCompanyType")), "0", dtAccountInfo.Rows(0).Item("numCompanyType").ToString)

    '        objPageLayout.CoType = "z"
    '        objPageLayout.UserCntID = Session("UserContactID")
    '        objPageLayout.RecordId = lngDivId
    '        objPageLayout.DomainID = Session("DomainID")
    '        objPageLayout.PageId = 1
    '        objPageLayout.numRelCntType = type.Text
    '        ds = objPageLayout.GetTableInfoDefault()  ' getting the table structure 
    '        dtTableInfo = ds.Tables(0)

    '        txtrOwner.Text = dtAccountInfo.Rows(0).Item("numRecOwner")
    '        lblCustomerId.Text = lngDivId
    '        lblRecordOwner.Text = dtAccountInfo.Rows(0).Item("RecOwner")
    '        lblCreatedBy.Text = dtAccountInfo.Rows(0).Item("vcCreatedBy")
    '        lblLastModifiedBy.Text = dtAccountInfo.Rows(0).Item("vcModifiedBy")

    '        Dim numrows As Integer = dtTableInfo.Compute("Max(tintrow)", String.Empty)
    '        Dim numcells As Integer = dtTableInfo.Compute("Max(intcoulmn)", String.Empty)
    '        Dim i As Integer = 0
    '        Dim nr As Integer
    '        Dim noRowsToLoop As Integer
    '        noRowsToLoop = (dtTableInfo.Rows.Count - (numrows * numcells)) / numcells
    '        noRowsToLoop = noRowsToLoop + numrows + 1
    '        For nr = 0 To noRowsToLoop
    '            Dim r As New TableRow()
    '            Dim nc As Integer
    '            Dim ro As Integer = nr
    '            For nc = 1 To numcells
    '                If dtTableInfo.Rows.Count <> i Then
    '                    If dtTableInfo.Rows(i).Item("tintrow") = nr + 1 And dtTableInfo.Rows(i).Item("intcoulmn") = nc Then
    '                        Dim column1 As New TableCell
    '                        Dim column2 As New TableCell
    '                        Dim fieldId As Integer
    '                        Dim bitDynFld As String
    '                        fieldId = CInt(dtTableInfo.Rows(i).Item("numFieldID").ToString)
    '                        bitDynFld = dtTableInfo.Rows(i).Item("bitCustomField")
    '                        column1.CssClass = "normal7"
    '                        If (bitDynFld <> "1") Then
    '                            If (fieldId = "188") Then
    '                                If (IIf(IsDBNull(dtAccountInfo.Rows(0).Item("vcWebLabel1")), "", dtAccountInfo.Rows(0).Item("vcWebLabel1").ToString) <> "") Then
    '                                    column1.Text = IIf(IsDBNull(dtAccountInfo.Rows(0).Item("vcWebLabel1")), "", dtAccountInfo.Rows(0).Item("vcWebLabel1")) & "&nbsp;:"
    '                                Else : column1.Text = "WebLink 1 :"
    '                                End If

    '                            ElseIf (fieldId = "189") Then
    '                                If (IIf(IsDBNull(dtAccountInfo.Rows(0).Item("vcWebLabel2")), "", dtAccountInfo.Rows(0).Item("vcWebLabel2").ToString) <> "") Then
    '                                    column1.Text = IIf(IsDBNull(dtAccountInfo.Rows(0).Item("vcWebLabel2")), "", dtAccountInfo.Rows(0).Item("vcWebLabel2")) & "&nbsp;:"
    '                                Else : column1.Text = "WebLink 2 :"
    '                                End If
    '                            ElseIf (fieldId = "190") Then
    '                                If (IIf(IsDBNull(dtAccountInfo.Rows(0).Item("vcWebLabel3")), "", dtAccountInfo.Rows(0).Item("vcWebLabel3").ToString) <> "") Then
    '                                    column1.Text = IIf(IsDBNull(dtAccountInfo.Rows(0).Item("vcWebLabel3")), "", dtAccountInfo.Rows(0).Item("vcWebLabel3")) & "&nbsp;:"
    '                                Else : column1.Text = "WebLink 3 :"
    '                                End If
    '                            ElseIf (fieldId = "191") Then
    '                                If (IIf(IsDBNull(dtAccountInfo.Rows(0).Item("vcWebLabel4")), "", dtAccountInfo.Rows(0).Item("vcWebLabel4").ToString) <> "") Then
    '                                    column1.Text = IIf(IsDBNull(dtAccountInfo.Rows(0).Item("vcWebLabel4")), "", dtAccountInfo.Rows(0).Item("vcWebLabel4")) & "&nbsp;:"
    '                                Else : column1.Text = "WebLink 4 :"
    '                                End If
    '                            Else : column1.Text = dtTableInfo.Rows(i).Item("vcFieldName").ToString & "&nbsp;:"
    '                            End If

    '                            'cell2 for a table row
    '                            If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName").ToString) Then
    '                                Dim temp As String
    '                                temp = dtTableInfo.Rows(i).Item("vcDBColumnName").ToString
    '                                fields = temp.Split(",")
    '                                Dim j As Integer = 0

    '                                While (j < fields.Length)
    '                                    If (fieldId = "187") Then

    '                                        If (fields(j) = "AssociateCountTo") Then
    '                                            Dim h As New HyperLink()
    '                                            h.CssClass = "hyperlink"
    '                                            h.Text = "To" & "(" & IIf(IsDBNull(dtAccountInfo.Rows(0).Item("AssociateCountTo")), "", dtAccountInfo.Rows(0).Item("AssociateCountTo")) & ")" & "/"
    '                                            h.Attributes.Add("onclick", "return OpenTo(" & lngDivId & ");")
    '                                            h.Attributes.Add("CssClass", "hyperlink")
    '                                            column2.Controls.Add(h)
    '                                        ElseIf (fields(j) = "AssociateCountFrom") Then
    '                                            Dim h As New HyperLink()
    '                                            h.CssClass = "hyperlink"
    '                                            h.Text = "From" & "(" & IIf(IsDBNull(dtAccountInfo.Rows(0).Item("AssociateCountFrom")), "", dtAccountInfo.Rows(0).Item("AssociateCountFrom")) & ")"
    '                                            h.Attributes.Add("onclick", "return OpenFrom(" & lngDivId & ");")
    '                                            column2.Controls.Add(h)
    '                                        End If
    '                                    ElseIf (fieldId = "178") Then
    '                                        Dim h As New HyperLink()
    '                                        h.CssClass = "hyperlink"
    '                                        ' h.NavigateUrl = "http://" & IIf(IsDBNull(dtAccountInfo.Rows(0).Item("vcWebSite")), "", dtAccountInfo.Rows(0).Item("vcWebSite"))
    '                                        h.Text = IIf(IsDBNull(dtAccountInfo.Rows(0).Item("vcWebSite")), "", dtAccountInfo.Rows(0).Item("vcWebSite"))
    '                                        h.Attributes.Add("onclick", "return fn_GoToURL('" & h.Text & "');")
    '                                        column2.Controls.Add(h)
    '                                    ElseIf (fieldId = "188") Then
    '                                        Dim h As New HyperLink()
    '                                        h.CssClass = "hyperlink"
    '                                        ' h.NavigateUrl = IIf(IsDBNull(dtAccountInfo.Rows(0).Item("vcWebLink1")), "", dtAccountInfo.Rows(0).Item("vcWebLink1"))
    '                                        h.Text = IIf(IsDBNull(dtAccountInfo.Rows(0).Item("vcWebLink1")), "", dtAccountInfo.Rows(0).Item("vcWebLink1"))
    '                                        h.Attributes.Add("onclick", "return fn_GoToURL('" & h.Text & "');")
    '                                        column2.Controls.Add(h)
    '                                    ElseIf (fieldId = "189") Then
    '                                        Dim h As New HyperLink()
    '                                        h.CssClass = "hyperlink"
    '                                        ' h.NavigateUrl = IIf(IsDBNull(dtAccountInfo.Rows(0).Item("vcWebLink2")), "", dtAccountInfo.Rows(0).Item("vcWebLink2"))
    '                                        h.Text = IIf(IsDBNull(dtAccountInfo.Rows(0).Item("vcWebLink2")), "", dtAccountInfo.Rows(0).Item("vcWebLink2"))
    '                                        h.Attributes.Add("onclick", "return fn_GoToURL('" & h.Text & "');")
    '                                        column2.Controls.Add(h)
    '                                    ElseIf (fieldId = "190") Then
    '                                        Dim h As New HyperLink()
    '                                        h.CssClass = "hyperlink"
    '                                        'h.NavigateUrl = IIf(IsDBNull(dtAccountInfo.Rows(0).Item("vcWebLink3")), "", dtAccountInfo.Rows(0).Item("vcWebLink3"))
    '                                        h.Text = IIf(IsDBNull(dtAccountInfo.Rows(0).Item("vcWebLink3")), "", dtAccountInfo.Rows(0).Item("vcWebLink3"))
    '                                        h.Attributes.Add("onclick", "return fn_GoToURL('" & h.Text & "');")
    '                                        column2.Controls.Add(h)
    '                                    ElseIf (fieldId = "191") Then
    '                                        Dim h As New HyperLink()
    '                                        h.CssClass = "hyperlink"
    '                                        'h.NavigateUrl = IIf(IsDBNull(dtAccountInfo.Rows(0).Item("vcWebLink4")), "", dtAccountInfo.Rows(0).Item("vcWebLink4"))
    '                                        h.Text = IIf(IsDBNull(dtAccountInfo.Rows(0).Item("vcWebLink4")), "", dtAccountInfo.Rows(0).Item("vcWebLink4"))
    '                                        h.Attributes.Add("onclick", "return fn_GoToURL('" & h.Text & "');")
    '                                        column2.Controls.Add(h)
    '                                    ElseIf (fieldId = "180") Then
    '                                        Dim l As New Label
    '                                        l.CssClass = "cell"
    '                                        If Not (IsDBNull(dtAccountInfo.Rows(0).Item("bitPublicFlag"))) Then
    '                                            l.Text = IIf(dtAccountInfo.Rows(0).Item("bitPublicFlag"), "a", "r")
    '                                        Else : l.Text = "r"
    '                                        End If
    '                                        column2.Controls.Add(l)
    '                                    Else : column2.Text = IIf(IsDBNull(dtAccountInfo.Rows(0).Item(fields(j))), "", dtAccountInfo.Rows(0).Item(fields(j)))
    '                                    End If
    '                                    j += 1
    '                                End While
    '                            Else : column1.Text = ""
    '                            End If ' end of table cell2
    '                        Else
    '                            If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Link" Then
    '                                column1.Text = "Custom Web Link :"
    '                                Dim h As New HyperLink()
    '                                h.CssClass = "hyperlink"
    '                                Dim url As String = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcURL")), "", dtTableInfo.Rows(i).Item("vcURL"))
    '                                url = url.Replace("RecordID", lngDivId)
    '                                h.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "-", dtTableInfo.Rows(i).Item("vcFieldName"))
    '                                h.Attributes.Add("onclick", "return fn_GoToURL('" & "http://" & url & "');")
    '                                column2.Controls.Add(h)
    '                            ElseIf IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Date Field" Then
    '                                column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
    '                                Dim strDate As String
    '                                strDate = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "", dtTableInfo.Rows(i).Item("vcDBColumnName"))
    '                                If strDate = "0" Then strDate = ""
    '                                If strDate <> "" Then column2.Text = FormattedDateFromDate(strDate, Session("DateFormat"))
    '                            Else
    '                                column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
    '                                If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Check box" Then
    '                                    If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
    '                                        Dim l As New Label
    '                                        l.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "a", "r")
    '                                        l.CssClass = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "cell1", "cell")
    '                                        column2.Controls.Add(l)
    '                                    End If
    '                                Else
    '                                    If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
    '                                        column2.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "-", dtTableInfo.Rows(i).Item("vcDBColumnName"))
    '                                    End If
    '                                End If
    '                            End If
    '                        End If

    '                        column2.CssClass = "normal1"
    '                        column1.HorizontalAlign = HorizontalAlign.Right
    '                        column2.HorizontalAlign = HorizontalAlign.Left
    '                        column1.Width = 250
    '                        column2.Width = 300
    '                        column2.ColumnSpan = 1
    '                        column1.ColumnSpan = 1
    '                        r.Cells.Add(column1)
    '                        r.Cells.Add(column2)
    '                        i += 1
    '                    ElseIf dtTableInfo.Rows(i).Item("tintrow") = 0 And dtTableInfo.Rows(i).Item("intcoulmn") = 0 Then
    '                        Dim column1 As New TableCell
    '                        Dim column2 As New TableCell
    '                        column1.CssClass = "normal7"
    '                        If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Link" Then
    '                            column1.Text = "Custom Web Link :"
    '                            Dim h As New HyperLink()
    '                            h.CssClass = "hyperlink"
    '                            Dim url As String = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcURL")), "", dtTableInfo.Rows(i).Item("vcURL"))
    '                            url = url.Replace("RecordID", lngDivId)
    '                            h.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "-", dtTableInfo.Rows(i).Item("vcFieldName"))
    '                            h.Attributes.Add("onclick", "return fn_GoToURL('" & "http://" & url & "');")
    '                            column2.Controls.Add(h)
    '                        ElseIf IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Date Field" Then
    '                            column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
    '                            Dim strDate As String
    '                            strDate = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "", dtTableInfo.Rows(i).Item("vcDBColumnName"))
    '                            If strDate = "0" Then strDate = ""
    '                            If strDate <> "" Then column2.Text = FormattedDateFromDate(strDate, Session("DateFormat"))
    '                        Else
    '                            column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
    '                            If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Check box" Then
    '                                If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
    '                                    Dim l As New Label
    '                                    l.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "a", "r")
    '                                    l.CssClass = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "cell1", "cell")
    '                                    column2.Controls.Add(l)
    '                                End If
    '                            Else
    '                                If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
    '                                    column2.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "-", dtTableInfo.Rows(i).Item("vcDBColumnName"))
    '                                End If
    '                            End If
    '                        End If

    '                        column2.CssClass = "normal1"
    '                        column1.HorizontalAlign = HorizontalAlign.Right
    '                        column2.HorizontalAlign = HorizontalAlign.Left
    '                        column1.Width = 250
    '                        column2.Width = 300
    '                        column2.ColumnSpan = 1
    '                        column1.ColumnSpan = 1
    '                        r.Cells.Add(column1)
    '                        r.Cells.Add(column2)
    '                        i += 1
    '                    Else
    '                        Dim column1 As New TableCell
    '                        Dim column2 As New TableCell
    '                        column1.Text = ""
    '                        column2.Text = ""
    '                        r.Cells.Add(column1)
    '                        r.Cells.Add(column2)
    '                    End If
    '                End If
    '            Next nc
    '            tabledetail.Rows.Add(r)
    '        Next nr

    '        If (dtAccountInfo.Rows.Count > 0) Then
    '            Dim column1 As New TableCell
    '            Dim column2 As New TableCell
    '            Dim r As New TableRow
    '            column1.CssClass = "normal7"
    '            column2.CssClass = "normal1"
    '            column1.HorizontalAlign = HorizontalAlign.Right
    '            column2.HorizontalAlign = HorizontalAlign.Justify
    '            column2.ColumnSpan = 5
    '            Dim l As New Label
    '            l.CssClass = "normal7"
    '            l.Text = "Comments" & "&nbsp;:"
    '            column1.Controls.Add(l)
    '            column2.Text = IIf(IsDBNull(dtAccountInfo.Rows(0).Item("txtComments")), "-", dtAccountInfo.Rows(0).Item("txtComments"))
    '            column1.Width = 150
    '            r.Cells.Add(column1)
    '            r.Cells.Add(column2)
    '            tableComment.Rows.Add(r)
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Sub DisplayDynamicFlds()
    '    Try
    '        'm_aryRightsForCusFlds = clsAuthorization.fn_GetPageListUserRights("frmContacts.aspx", Session("UserContactID"), 11, 9)
    '        'If m_aryRightsForCusFlds(RIGHTSTYPE.VIEW) = 0 Then
    '        '    Exit Sub
    '        'End If
    '        Dim strDate As String
    '        Dim bizCalendar As UserControl
    '        Dim _myUC_DueDate As PropertyInfo
    '        Dim PreviousRowID As Integer = 0
    '        Dim objRow As HtmlTableRow
    '        Dim objCell As HtmlTableCell
    '        Dim i, k, j As Integer
    '        Dim dtTable As DataTable
    '        Dim count As Integer = uwOppTab.Tabs.Count
    '        ' Tabstrip4.Items.Clear()
    '        Dim objPageLayout As New CPageLayout

    '        'objPageLayout.locId = 0
    '        objPageLayout.DomainID = Session("DomainID")
    '        objPageLayout.RelId = type.Text
    '        objPageLayout.locId = 1
    '        objPageLayout.RecordId = lngDivId
    '        objPageLayout.CoType = "z"
    '        dtTable = objPageLayout.GetCustFlds

    '        If uwOppTab.Tabs.Count > 1 Then
    '            Dim iItemcount As Integer
    '            iItemcount = uwOppTab.Tabs.Count
    '            While uwOppTab.Tabs.Count > 1
    '                uwOppTab.Tabs.RemoveAt(iItemcount - 1)
    '                iItemcount = iItemcount - 1
    '            End While
    '        End If

    '        If dtTable.Rows.Count > 0 Then
    '            'CustomField Section
    '            Dim Tab As Tab
    '            ' Dim pageView As PageView
    '            Dim aspTable As HtmlTable
    '            Dim Table As Table
    '            Dim tblcell As TableCell
    '            Dim tblRow As TableRow
    '            Dim up As UpdatePanel
    '            Dim apt As AsyncPostBackTrigger
    '            k = 0
    '            ViewState("TabId") = dtTable.Rows(0).Item("TabId")
    '            ViewState("Check") = 0
    '            ViewState("FirstTabCreated") = 0
    '            'Tabstrip4.Items.Clear()
    '            For i = 0 To dtTable.Rows.Count - 1
    '                If dtTable.Rows(i).Item("TabId") <> 0 Then
    '                    If ViewState("TabId") <> dtTable.Rows(i).Item("TabId") Or ViewState("FirstTabCreated") = 0 Then
    '                        If ViewState("Check") <> 0 Then
    '                            aspTable.Rows.Add(objRow)
    '                            tblcell.Controls.Add(aspTable)
    '                            tblRow.Cells.Add(tblcell)
    '                            Table.Rows.Add(tblRow)

    '                            up = New UpdatePanel
    '                            apt = New AsyncPostBackTrigger
    '                            up.ChildrenAsTriggers = True
    '                            up.UpdateMode = UpdatePanelUpdateMode.Conditional
    '                            apt.ControlID = "btnEdit"
    '                            up.ContentTemplateContainer.Controls.Add(Table)
    '                            up.Triggers.Add(apt)
    '                            Tab.ContentPane.Controls.Add(up)
    '                            'pageView.Controls.Add(up)
    '                            ' mpages.Controls.Add(pageView)
    '                            ' mpages.Controls.Add(pageView)
    '                        End If
    '                        k = 0
    '                        ViewState("Check") = 1
    '                        'If Not IsPostBack Then
    '                        ViewState("FirstTabCreated") = 1
    '                        ViewState("TabId") = dtTable.Rows(i).Item("TabId")
    '                        Tab = New Tab
    '                        Tab.Text = "&nbsp;&nbsp;" & dtTable.Rows(i).Item("tabname") & "&nbsp;&nbsp;"
    '                        uwOppTab.Tabs.Add(Tab)
    '                        'End If
    '                        'pageView = New PageView
    '                        aspTable = New HtmlTable
    '                        Table = New Table
    '                        Table.Width = Unit.Percentage(100)
    '                        Table.BorderColor = System.Drawing.Color.FromName("black")
    '                        Table.GridLines = GridLines.None
    '                        Table.BorderWidth = Unit.Pixel(1)
    '                        Table.Height = Unit.Pixel(300)
    '                        Table.CssClass = "aspTable"
    '                        tblcell = New TableCell
    '                        tblRow = New TableRow
    '                        tblcell.VerticalAlign = VerticalAlign.Top
    '                        aspTable.Width = "100%"
    '                        objRow = New HtmlTableRow
    '                        objCell = New HtmlTableCell
    '                        objCell.InnerHtml = "<br>"
    '                        objRow.Cells.Add(objCell)
    '                        aspTable.Rows.Add(objRow)
    '                        objRow = New HtmlTableRow
    '                    End If

    '                    If k = 3 Then
    '                        k = 0
    '                        aspTable.Rows.Add(objRow)
    '                        objRow = New HtmlTableRow
    '                    End If
    '                    objCell = New HtmlTableCell
    '                    objCell.Align = "right"
    '                    objCell.Width = 100

    '                    objCell.Attributes.Add("class", "normal7")
    '                    If dtTable.Rows(i).Item("fld_type") <> "Frame" Then
    '                        If dtTable.Rows(i).Item("fld_type") <> "Link" Then
    '                            objCell.InnerText = dtTable.Rows(i).Item("fld_label") & " :"
    '                        End If
    '                        objRow.Cells.Add(objCell)
    '                    End If
    '                    objCell = New HtmlTableCell
    '                    objCell.Attributes.Add("class", "normal1")
    '                    objCell.Align = "left"
    '                    If dtTable.Rows(i).Item("fld_type") = "Link" Then

    '                        Dim h As New HyperLink
    '                        h.CssClass = "hyperlink"
    '                        Dim URL As String = IIf(IsDBNull(dtTable.Rows(i).Item("vcURL")), "", dtTable.Rows(i).Item("vcURL"))
    '                        URL = URL.Replace("RecordID", lngDivId)
    '                        h.Text = IIf(IsDBNull(dtTable.Rows(i).Item("fld_label")), "", dtTable.Rows(i).Item("fld_label"))
    '                        h.Attributes.Add("onclick", "fn_GoToURL('" & "http://" & URL & "')")
    '                        objCell.Controls.Add(h)
    '                        'CreateLink(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("vcURL"), lngCntID, dtTable.Rows(i).Item("fld_label"))
    '                    ElseIf dtTable.Rows(i).Item("fld_type") = "Check box" Then
    '                        If Not IsDBNull(dtTable.Rows(i).Item("Value")) Then
    '                            Dim l As New Label
    '                            l.Text = IIf(dtTable.Rows(i).Item("Value") = "1", "a", "r")
    '                            l.CssClass = IIf(dtTable.Rows(i).Item("Value") = "1", "cell1", "cell")
    '                            objCell.Controls.Add(l)
    '                        ElseIf IsDBNull(dtTable.Rows(i).Item("Value")) Then
    '                            Dim l As New Label
    '                            l.Text = "r"
    '                            l.CssClass = "cell"
    '                            objCell.Controls.Add(l)
    '                        End If
    '                    ElseIf dtTable.Rows(i).Item("fld_type") = "Frame" Then
    '                        objCell = New HtmlTableCell
    '                        Dim strFrame As String
    '                        Dim URL As String
    '                        URL = dtTable.Rows(i).Item("vcURL")
    '                        URL = URL.Replace("RecordID", lngDivId)
    '                        strFrame = "<iframe src ='" & URL & "' width='100%' frameborder='0' height= '800px'></iframe>"
    '                        objCell.Controls.Add(New LiteralControl(strFrame))
    '                        objRow.Cells.Add(objCell)
    '                    Else
    '                        If Not IsDBNull(dtTable.Rows(i).Item("Value")) Then
    '                            objCell.InnerText = IIf(dtTable.Rows(i).Item("Value") = "0" Or dtTable.Rows(i).Item("Value") = Nothing, "-", dtTable.Rows(i).Item("Value"))
    '                        ElseIf IsDBNull(dtTable.Rows(i).Item("Value")) Then
    '                            objCell.InnerText = "-"
    '                        End If
    '                    End If
    '                    objRow.Cells.Add(objCell)
    '                    k = k + 1
    '                End If

    '            Next
    '            If ViewState("Check") = 1 Then
    '                objRow.Align = "left"
    '                aspTable.Rows.Add(objRow)
    '                tblcell.Controls.Add(aspTable)
    '                tblRow.Cells.Add(tblcell)
    '                Table.Rows.Add(tblRow)
    '                up = New UpdatePanel
    '                apt = New AsyncPostBackTrigger
    '                up.ChildrenAsTriggers = True
    '                up.UpdateMode = UpdatePanelUpdateMode.Conditional
    '                apt.ControlID = "btnEdit"
    '                up.ContentTemplateContainer.Controls.Add(Table)
    '                up.Triggers.Add(apt)
    '                Tab.ContentPane.Controls.Add(up)
    '                'pageView.Controls.Add(up)
    '                'mpages.Controls.Add(pageView)
    '                ' mpages.Controls.Add(pageView)
    '            End If
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    '    Try
    '        If GetQueryStringVal( "frm") = "ContactList" Then
    '            Response.Redirect("../Contacts/frmCustContactList.aspx")
    '        ElseIf GetQueryStringVal( "frm") = "contactdetail" Then
    '            If Session("EnableIntMedPage") = 1 Then
    '                Response.Redirect("../pagelayout/frmCustContactdtl.aspx?frm=" & GetQueryStringVal( "frm1") & "&CntID=" & GetQueryStringVal( "CntID") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm2)
    '            Else : Response.Redirect("../Contact/frmCstContacts.aspx?frm=" & GetQueryStringVal( "frm1") & "&CntID=" & GetQueryStringVal( "CntID") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm2)
    '            End If
    '        ElseIf GetQueryStringVal( "frm") = "oppdetail" Then
    '            If Session("EnableIntMedPage") = 1 Then
    '                Response.Redirect("../pagelayout/frmCustOppurtunitydtl.aspx?frm=" & GetQueryStringVal( "frm1") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm2)
    '            Else : Response.Redirect("../opportunity/frmCusOpportunities.aspx?frm=" & GetQueryStringVal( "frm1") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm2)
    '            End If
    '        ElseIf GetQueryStringVal( "frm") = "OppList" Then
    '            Response.Redirect("../opportunity/frmCusOppList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
    '        ElseIf GetQueryStringVal( "frm") = "CaseList" Then
    '            Response.Redirect("../Cases/frmCusCaseList.aspx")
    '        ElseIf GetQueryStringVal( "frm") = "ProjectDetails" Then
    '            If Session("EnableIntMedPage") = 1 Then
    '                Response.Redirect("../pagelayout/frmCustProjectdtl.aspx?frm=" & GetQueryStringVal( "frm1") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm2)
    '            Else : Response.Redirect("../Projects/frmCusProDTL.aspx?frm=" & GetQueryStringVal( "frm1") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm2)
    '            End If
    '        ElseIf GetQueryStringVal( "frm") = "CaseDetails" Then
    '            If Session("EnableIntMedPage") = 1 Then
    '                Response.Redirect("../pagelayout/frmCustCasedtl.aspx?frm=" & GetQueryStringVal( "frm1") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm2)
    '            Else : Response.Redirect("../Cases/frmCusCaseDTL.aspx?frm=" & GetQueryStringVal( "frm1") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm2)
    '            End If
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
    '    Try
    '        Response.Redirect("../account/frmCusAccounts.aspx?frm=accountdtl" & "&SI=" & uwOppTab.SelectedTabIndex & "&SI1=" & uwOppTab.SelectedTabIndex & "&SI2=" & SI1 & "&frm1=" & frm & "&frm2=" & frm1)
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

End Class