<%@ Page Language="vb" AutoEventWireup="false"   EnableEventValidation="false" Codebehind="frmOppurtunityDtl.aspx.vb" Inherits="BACRMPortal.frmOppurtunityDtl" %>
<%@ Register Assembly="Infragistics35.WebUI.UltraWebTab.v8.1, Version=8.1.20081.2046, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebTab" TagPrefix="igtab" %>
<%@ Register Assembly="RadGrid.Net2" Namespace="Telerik.WebControls" TagPrefix="rad" %>
<%@ Register TagPrefix="menu1" TagName="Menu" src="../common/topbar.ascx" %>
<%@ Register Assembly="Infragistics35.WebUI.UltraWebGrid.v8.1, Version=8.1.20081.2046, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1"    runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
       <link href="../css/lists.css" type="text/css" rel="STYLESHEET"/>
		<title>Opportunities</title>
		<script language="javascript" type="text/javascript">
		function reDirectPage(url)
            {
                window.location.href =url
            }
		function openTrackAsset(a,b)
		{
		    window.open("../opportunity/frmTrackAsset.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&opId="+a+"&DivId="+b,'','toolbar=no,titlebar=no,left=300,top=200,width=800,height=500,scrollbars=yes,resizable=yes')
		    return false;
		}
		function OpenConfSerItem(a)
		{
		    window.open('../opportunity/frmAddSerializedItem.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppID='+a,'','toolbar=no,titlebar=no,left=100,top=100,width=1000,height=700,scrollbars=yes,resizable=yes')
		    return false;
		}
		function ShowlinkedProjects(a)
		        {
		        window.open("../opportunity/frmLinkedProjects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&opId="+a,'','toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')
		        return false;
		        }
		function fn_GoToURL(varURL)
		{
			
			if ((varURL!='') && (varURL.substr(0,7)=='http://') && (varURL.length > 7))
			{
				var LoWindow=window.open(varURL,"","");
				LoWindow.focus();
			}
			return false;
		}
			function DeleteRecord()
				{
					if(confirm('Are you sure, you want to delete the selected record?'))
					{
						return true;
					}
					else
					{
						return false;
					}
				}
		function DeletMsg()
			{
				var bln=confirm("You�re about to remove the Stage from this Process, all stage data will be deleted")
				if (bln==true)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		function OpenDocuments(a)
		{
			window.open("../Documents/frmSpecDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=O&yunWE="+a,'','toolbar=no,titlebar=no,top=200,width=700,height=450,left=200,scrollbars=yes,resizable=yes')
			return false;
		}
		function DealCompleted()
		{
		
			alert("This Deal will now be removed from the 'Open Deals' section, and will reside only in the 'Closed Deals' section within the Organization the deal is for. Except for BizDocs or any Projects that depend on BizDocs - Modifications to Deal Details, Milestones & Stages, Associated Contacts, and Products / Services, will no longer be allowed.")
			return false;
		}
		function CannotShip()
		{
		
			alert("You can't ship at this time because you don't have enough quantity on hand to support your shipment. Your options are to modify your order, or replenish inventory (to check inventory click on the edit link within the line item, then the value in the 'Products/Services' column):")
			return false;
		}
		function AlertMsg()
		{
			if (confirm("Please note that after a 'Received' or 'Shipped' request is executed, except for the BizDocs and any Projects that depend on BizDocs - Additional changes will not be permitted on this Deal (i.e. it will be frozen). Do you wish to continue ?")) 
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		function OpenImage(a)
		{
			window.open('../opportunity/frmFullImage.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ItemCode='+a,'','toolbar=no,titlebar=no,left=300, top=100,width=600,height=500,scrollbars=yes,resizable=yes');
			return false;
		}
		function Update(ddl,txt,txtPrice)
		{
			if (ddl.value==0)
			{
				alert("Select Item")
				ddl.focus()
				return false;
			}
			if (txt.value=='')
			{
				alert("Enter Units")
				txt.focus()
				return false;
			}
			if (txtPrice.value=='')
			{
				alert("Enter Price")
				txtPrice.focus()
				return false;
			}
		}

		function AddPrice(a,b,c)
		{
			if (b==1)
			{
			document.Form1.txtprice.value=a;
			return false;
			}
			else
			{
				document.all(c).value=a;
				return false;
			}
		}
		function openCompetition(a)
		{
			document.all['IframeComp'].src="../opportunity/frmCompetition.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ItemCode="+a ;
			document.all['divCompetition'].style.visibility = "visible";
			return false;
		}
		
		function openUnit(a)
		{
			document.all['IframeUnit'].src="../opportunity/frmUnitdtlsForItem.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ItemCode="+a ;
			document.all['divUnits'].style.visibility = "visible";
			return false;
		}
		function openItem(a,b,c)
		{
			b=document.Form1.txtunits.value
			///document.all['cntdoc'].src="../opportunity/frmItemPriceRecommd.aspx?ItemCode="+a+"&Unit="+b ;
			//document.all['divItemPrice'].style.visibility = "visible";
			//return false;
			window.open('../opportunity/frmItemPriceRecommd.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ItemCode='+a+'&Unit='+b+'&OppID='+c,'','toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')
		}
		function OpenEPrice(a,b,c,d)
		{
			window.open('../opportunity/frmItemPriceRecommd.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ItemCode='+b.value+'&Unit='+a.value+'&Type=Edit&txtName='+c+'&OppID='+d,'','toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')
			return false;
		}
		function openEUnit(a)
		{
			document.all['IframeUnit'].src="../opportunity/frmUnitdtlsForItem.aspx?ItemCode="+a.value ;
			document.all['divUnits'].style.visibility = "visible";
			return false;
		}
		function ShowWindow(Page,q,att) 
		{
		
			if (att=='show')
			{
				document.all[Page].style.visibility = "visible";
				return false;
		
			}
			if (att=='hide')
			{
				document.all[Page].style.visibility = "hidden";
				return false;
		
			}
		}
			function CheckNumber(cint)
					{
						if (cint==1)
						{
							if (!(window.event.keyCode > 47 && window.event.keyCode < 58 || window.event.keyCode==44 || window.event.keyCode==46))
							{
								window.event.keyCode=0;
							}
						}
						if (cint==2)
						{
							if (!(window.event.keyCode > 47 && window.event.keyCode < 58))
							{
								window.event.keyCode=0;
							}
						}
						
					}
		function Save(cint)
		{
		
		if (cint==1)
		{
			if (document.Form1.ddlCompanyName.value==0)
			{
				alert("Select Customer");
				tsVert.selectedIndex=0;
				document.Form1.ddlCompanyName.focus();
				return false;
			}
			if (document.Form1.ddlTaskContact.selectedIndex==0)
			{
				alert("Select Contact");
				tsVert.selectedIndex=0;
				document.Form1.ddlTaskContact.focus();
				return false;
			}
		}
	
		if (document.Form1.calDue_txtDate.value=='')
			{
				alert("Enter Due Date");
				tsVert.selectedIndex=0;
				return false;
			}
			if (typeof(dgItem)!='undefined')
			{
				if (dgItem.rows.length==1)
				{
					alert("Select an Item");
					tsVert.selectedIndex=3;
					document.Form1.ddlItems.focus();
					return false;
				}
			}
			if (typeof(dgItem)=='undefined')
			{
					alert("Select an Item");
					tsVert.selectedIndex=3;
					document.Form1.ddlItems.focus();
					return false;
			}
		if (typeof(document.all['chkDClosed'])!='undefined')
		{
			if (document.all['chkDClosed'].checked==true)
				{
				    if (document.Form1.ddlClReason.value==0)
						{
							alert("Select Conclusion Analysis")
							tsVert.selectedIndex=1;
							document.Form1.ddlClReason.focus()
							return false;
						}
				}
		}
		if (typeof(document.all['chkDlost'])!='undefined')
		{
			if (document.all['chkDlost'].checked==true)
				{
					if (document.Form1.ddlClReason.value==0)
						{
							alert("Select Conclusion Analysis")
							tsVert.selectedIndex=1;
							document.Form1.ddlClReason.focus()
							return false;
						}
					
				}
		}		
	
		}
		function AddContact()
		{
			if (document.Form1.ddlcompany.value==0)
			{
				alert("Select Customer");
				tsVert.selectedIndex=2;
				document.Form1.ddlcompany.focus();
				return false;
			}
			if (document.Form1.ddlAssocContactId.value==0)
			{
				alert("Select Contact");
				tsVert.selectedIndex=2;
				document.Form1.ddlAssocContactId.focus();
				return false;
			}
			var str;
			for(i=0;i<document.Form1.elements.length;i++)
				{
				if (i<=9)
				{
				    str='0'+(i+1)
				}
				else
				{
				    str=i+1
				}
				if (typeof(document.Form1.all['dgContact_ctl'+str+'_txtContactID'])!='undefined')
					{
						if (document.Form1.all['dgContact_ctl'+str+'_txtContactID'].value==document.Form1.ddlAssocContactId.value)
							{
								alert("Associated contact is already added");
								return false;
							}
					} 
				}
			
		}
		function AddItems()
		{
			if (document.Form1.ddlItems.value==0)
			{
				alert("Select Item");
				tsVert.selectedIndex=3;
				document.Form1.ddlItems.focus();
				return false;
			}
			if (document.Form1.txtunits.value=="")
			{
				alert("Enter Units");
				tsVert.selectedIndex=3;
				document.Form1.txtunits.focus();
				return false;
			}
			if (document.Form1.txtprice.value=="")
			{
				alert("Enter Price");
				tsVert.selectedIndex=3;
				document.Form1.txtprice.focus();
				return false;
			}
		}
		
		function deleteItem()
		{
			var bln;
			bln=window.confirm("Delete Seleted Row - Are You Sure ?")
			if(bln==true) 
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		function OpenBiz(a)
		{
			window.open('../opportunity/frmBizDocs.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID='+a,'','toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes');
			return false;
		}
		
	
		function OpenDependency(a,b,c,d)
		{
			window.open('../opportunity/frmOppDependency.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OPPStageID='+a+'&Opid='+b+'&PerID='+c+'&StgDtlId='+d,'','toolbar=no,titlebar=no,left=500, top=300,width=500,height=200,scrollbars=yes,resizable=yes');
			return false;
		}
		function OpenExpense(a,b,c,d,e)
		{
			window.open('../opportunity/frmOppExpense.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OPPStageID='+a+'&Opid='+b+'&PerID='+c+'&StgDtlId='+d+'&DivId='+e,'','toolbar=no,titlebar=no,left=500, top=300,width=650,height=300,scrollbars=yes,resizable=yes');
			return false;
		}
		function OpenTime(a,b,c,d,e)
		{
			window.open('../opportunity/frmOppTime.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OPPStageID='+a+'&Opid='+b+'&PerID='+c+'&StgDtlId='+d+'&DivId='+e,'','toolbar=no,titlebar=no,left=500, top=300,width=650,height=300,scrollbars=yes,resizable=yes');
			return false;
		}
		function OpenSubStage(a,b,c,d)
		{
			window.open('../opportunity/frmOPPSubStagesaspx.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OPPStageID='+a+'&Opid='+b+'&PerID='+c+'&StgDtlId='+d,'','toolbar=no,titlebar=no,left=500, top=300,width=500,height=200,scrollbars=yes,resizable=yes');
			return false;
		}
		function CheckBoxCon(a,b,c)
		{
			if (parseInt(c)==1)
			{
				document.all['uwOppTab__ctl1_chkStage~'+a+'~'+b].checked=true
			}
			else
			{
				document.all['uwOppTab__ctl1_chkStage~'+a+'~'+b].checked=false
			}
		}
		function ValidateCheckBox(cint)
		{
			if (cint==1)
			{	
				if (document.all['chkDClosed'].checked==true)
				{
					if (document.all['chkDlost'].checked==true)
					{
					alert("The Deal is already Lost !")
					document.all['chkDClosed'].checked=false
					return false;
					}
				
				}
			}
			if (cint==2)
			{	
				if (document.all['chkDlost'].checked==true)
				{
					if (document.all['chkDClosed'].checked==true)
					{
						alert("The Deal is already Closed !")
						document.all['chkDlost'].checked=false
						return false;
					}
					document.Form1.chkActive.checked=false;
				}
			}
			
		}
		function ShowWindow(Page,q,att) 
		{
			if (att=='show')
			{
				document.all[Page].style.visibility = "visible";
				return false;
		
			}
			if (att=='hide')
			{
				document.all[Page].style.visibility = "hidden";
				return false;
		
			}
			
		}
			function DeleteRecord()
		{
			if(confirm('Are you sure, you want to delete the selected record?'))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		function DeleteMessage()
		{
			alert("You Are not Authorized to Delete the Selected Record !");
			return false;
		}
        function openPurOpp(a)
		{
			
			window.open('../opportunity/frmSelectPurOpp.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppID='+a,'','toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')
		    return false;
		}
		function ShowLayout(a,b)
		{
			window.open("../pagelayout/frmCustomisePageLayout.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Ctype="+a,'','toolbar=no,titlebar=no,top=200,scrollbars=yes,resizable=yes');
			return false;
		}
		function OpenCreateOpp(a,b)
		{
		    window.open('../opportunity/frmCreateSalesPurFromOpp.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppID='+a+'&OppType='+b,'','toolbar=no,titlebar=no,left=100,top=100,width=1000,height=700,scrollbars=yes,resizable=yes')
		    return false;
		}
		function OpenTransfer(url)
		{
			window.open(url,'',"width=340,height=150,status=no,top=100,left=150");
			return false;	
		}
		 function beforeSelectEvent(owner, item, evt)
        {
           // alert(item.getIndex())
            if (item.getIndex() == 4)
            {
                var iframe  = document.getElementById('uwOppTab__ctl4_IframeBiz')
                if (iframe.src == '')               
                {                    
                   // alert('../opportunity/frmBizDocs.aspx?OpID='+document.getElementById('txtOppId').value)
                    iframe.src = '../opportunity/frmBizDocs.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID='+document.getElementById('txtOppId').value                    
                }
                 return false;
            }        
        }
		</script>
	</head>
	<body>
<form id="Form1" method="post" runat="server">
	<menu1:menu id="webmenu1" runat="server"></menu1:menu>
	<asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
		
		<asp:updatepanel ID="updatepanel1" runat="server" ChildrenAstriggers="true" UpdateMode="Conditional" EnableViewState="true" >
		    <ContentTemplate>

			<table width="100%" align="center">
				<tr>
					<td>
						<table id="tblMenu" borderColor="black" cellSpacing="0" cellPadding="0" width="100%" border="0"
							runat="server">
							<tr>
								<td class="tr1" align="center"><b>Record Owner: </b>
									<asp:label id="lblRecordOwner" runat="server" ForeColor="Black"></asp:label></td>
								<td class="td1" width="1" height="18"></td>
								<td class="tr1" align="center"><b>Created By: </b>
									<asp:label id="lblCreatedBy" runat="server" ForeColor="Black"></asp:label></td>
								<td class="td1" width="1" height="18"></td>
								<td class="tr1" align="center"><b>Last Modified By: </b>
									<asp:label id="lblLastModifiedBy" runat="server" ForeColor="Black"></asp:label></td>
							</tr>
						</table>
					</td>
				</tr>
			
				<tr>
					<td>
						<table width="100%">
							<tr>
								<td class="normal1" align="center"><asp:label id="lblCustomerType" Runat="server"></asp:label> :
								        <asp:HyperLink ID="hplCustomer" runat="server" CssClass="hyperlink"></asp:HyperLink>
								        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblDealCompletedDate" Runat="server" CssClass="text"></asp:Label>
								        
								        </td>
								<td align="right">
								    <asp:button id="btnTransfer" Runat="server" Text="Transfer Ownership" CssClass="button" Width="120"></asp:button>
								    <asp:Button ID="btnTrackAsset" runat="server" CssClass="button" text="Track As Customer Asset" Width="175"/>
									<asp:button id="btnCreateOpp" Runat="server" CssClass="button" Visible="false"></asp:button>
									<asp:button id="btnConfSerItems" Runat="server" CssClass="button" Visible="false" Text="Configure Serialized Item"></asp:button>
									<asp:button id="btnEdit" Runat="server" CssClass="button" Text="Edit" Width="50px"></asp:button>									
									<asp:button id="btnCancel" Runat="server" CssClass="button" Text="Close"></asp:button>
									<asp:Button ID="btnActdelete" Runat="server" CssClass="Delete" Text="r"></asp:Button>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
					  <igtab:ultrawebtab  id="uwOppTab" runat="server" BorderWidth="0" ThreeDEffect="True" BorderStyle="Solid" Width="100%" BarHeight="0">
                        <DefaultTabStyle Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Arial" >
                        </DefaultTabStyle>
                        <ClientSideEvents  BeforeSelectedTabChange="beforeSelectEvent" />
                        <RoundedImage LeftSideWidth="7" RightSideWidth="8" ShiftOfImages="0" SelectedImage="../images/ig_tab_winXPs3.gif" NormalImage="../images/ig_tab_winXP3.gif" HoverImage="../images/ig_tab_winXPs3.gif" FillStyle="LeftMergedWithCenter" ></RoundedImage>
                          <SelectedTabStyle Height="23px"  ForeColor="white">
                        </SelectedTabStyle>
                        <HoverTabStyle Height="23px" ForeColor="white">
                        </HoverTabStyle>
                            <Tabs>
                            <igtab:Tab Text="Opportunity Details"  >
                                <ContentTemplate>
                                    
								    <asp:table id="tblOppr"  BorderWidth="1" Runat="server" Width="100%" CssClass="aspTableDTL"
									    BorderColor="black" GridLines="none"  Height="300">
									    <asp:TableRow>
    									<asp:TableCell  VerticalAlign=Top >
									    <asp:Table Width="100%" ID="tbl12" runat="server" >
									       <asp:TableRow>
									            <asp:TableCell ColumnSpan="2" HorizontalAlign="Right" >
									                <asp:button id="btnLayout" Runat="server" CssClass="button" Text="Layout" ></asp:button>
									            </asp:TableCell>
									       </asp:TableRow>
									       <asp:TableRow>
									            <asp:TableCell VerticalAlign="Top" > 
						                                <img src="../images/Dart-32.gif" />
									            </asp:TableCell>
									            <asp:TableCell>
									                 <asp:Table runat="server" ID="tabledetail"   BorderWidth="0"    GridLines="none" CellPadding="2" CellSpacing="0"   HorizontalAlign="Center" ></asp:Table>
    											
									            </asp:TableCell>
									       </asp:TableRow>
									       <asp:TableRow>
									            <asp:TableCell>
									               <asp:Table runat="server" ID="tableComment" BorderWidth="" Width="100%"  GridLines="none"   HorizontalAlign="Center" ></asp:Table>	
									            </asp:TableCell>
									       </asp:TableRow>
									    </asp:Table>									
									    </asp:TableCell></asp:TableRow>
								    </asp:table>
			                         </ContentTemplate> 
                            </igtab:Tab>
                            <igtab:Tab Text="Milestones & Stages">
                                <ContentTemplate>
                             <asp:table id="tblMile" BorderWidth="1" CellPadding="0" CellSpacing="0" Runat="server" Width="100%" CssClass="aspTableDTL"
									    BorderColor="black" GridLines="None" Height="300">
									    <asp:tableRow>
										    <asp:tableCell VerticalAlign="Top" HorizontalAlign="left" >
											    <br>
											    <table width="400">
												    <tr>
													    <td class="normal8" align="right">Conclusion Reason :
													    </td>
													    <td>
													    <asp:Label ID="lblClReason" runat="server" CssClass="normal1"></asp:Label>
													    </td>
													    <td class="normal8" align="right"><font color="green">Total Progress :</font>
													    </td>
													    <td Class="normal1">
													        <font color="green"><asp:Label ID="lblTProgress" runat="server" ></asp:Label>&nbsp;%</font>
													    </td>
												    </tr>
											    </table>
											    <br />
											    <asp:table id="tblMilestone" Runat="server" Width="100%" GridLines="none" CellSpacing="0" ></asp:table>
										    </asp:tableCell>
									    </asp:tableRow>
								    </asp:table>
                                </ContentTemplate>
                            </igtab:Tab>
                            <igtab:Tab Text="Associated Contacts">
                                <ContentTemplate>
                      	    <asp:table id="tblCont" BorderWidth="1" CellPadding="0" CellSpacing="0" Runat="server" Width="100%" CssClass="aspTableDTL"
									    BorderColor="black" GridLines="None" Height="300">
									    <asp:tableRow>
										    <asp:tableCell VerticalAlign="Top" >
											    <asp:datagrid id="dgContact" runat="server" CssClass="dg" Width="100%" BorderColor="white" AutoGenerateColumns="False">
												    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
												    <ItemStyle CssClass="is"></ItemStyle>
												    <HeaderStyle CssClass="hs"></HeaderStyle>
												    <Columns>
													    <asp:BoundColumn DataField="numContactId" Visible="false"></asp:BoundColumn>
													    <asp:BoundColumn DataField="bitPartner" Visible="false"></asp:BoundColumn>
													    <asp:BoundColumn HeaderText="Organization,Relationship" DataField="Company"></asp:BoundColumn>
													    <asp:BoundColumn HeaderText="First & Last Name" DataField="Name"></asp:BoundColumn>
													    <asp:BoundColumn HeaderText="Phone - Ext" DataField="Phone"></asp:BoundColumn>
													    <asp:BoundColumn HeaderText="Email" DataField="Email"></asp:BoundColumn>
													    <asp:BoundColumn HeaderText="Contact Role" DataField="ContactRole"></asp:BoundColumn>
    													
														    <asp:TemplateColumn HeaderText="Share Opportunity via Partner Point ?"  >
														    <ItemTemplate >														            
														                <asp:Label ID="lblShare" runat="server" CssClass="cell"  Font-Size="Large" ></asp:Label>
														    </ItemTemplate>
													    </asp:TemplateColumn>
													    <asp:TemplateColumn Visible="false" >
														    <HeaderTemplate>
															    <asp:Button ID="btnHdeleteCnt" Runat="server" CssClass="Delete" Text="r"></asp:Button>
														    </HeaderTemplate>
														    <ItemTemplate>
															    <asp:TextBox id="txtContactID" Runat="server" style="display:none" Text='<%# DataBinder.Eval(Container.DataItem, "numContactId") %>'>
															    </asp:TextBox>
															    <asp:Button ID="btnDeleteCnt" Runat="server" CssClass="Delete" Text="r" CommandName="Delete"></asp:Button>
															    <asp:LinkButton ID="lnkDeleteCnt" Runat="server" Visible="false">
																    <font color="#730000">*</font></asp:LinkButton>
														    </ItemTemplate>
													    </asp:TemplateColumn>
												    </Columns>
											    </asp:datagrid>
    											
    											
											    <br>
										    </asp:tableCell>
									    </asp:tableRow>
								    </asp:table>
                                </ContentTemplate>
                            </igtab:Tab>
                             <igtab:Tab Text="Product/Service">
                                <ContentTemplate>
                                <asp:table id="tblProducts" BorderWidth="1" CellPadding="0" CellSpacing="0" Runat="server" CssClass="aspTableDTL"
									    Width="100%" BorderColor="black" GridLines="None" Height="300">
									    <asp:tableRow>
										    <asp:tableCell ID="TableCell1" runat="server" VerticalAlign="Top" >
											      <rad:RadGrid ID="RadGrid1"  Skin="Windows" runat="server" Width="100%" AutoGenerateColumns="False"
                                               GridLines="None" ShowFooter="false" AllowMultiRowSelection="true"  >
                                               <MasterTableView HierarchyLoadMode="Client"  HierarchyDefaultExpanded="true"    DataMember="Item"  DataKeyNames="numoppitemtCode"  Width="100%">
                                                    <Columns>
                                                        <rad:GridClientSelectColumn  UniqueName="Select" ></rad:GridClientSelectColumn>
                                                        <rad:GridBoundColumn SortExpression="numoppitemtCode" HeaderText="" HeaderButtonType="TextButton"
                                                            DataField="numoppitemtCode" Display="false"  >
                                                        </rad:GridBoundColumn>
                                                         <rad:GridBoundColumn SortExpression="numWarehouseItmsID" HeaderText="" HeaderButtonType="TextButton"
                                                            DataField="numWarehouseItmsID" Display="false"  ></rad:GridBoundColumn>
                                                             <rad:GridBoundColumn SortExpression="numWareHouseID" HeaderText="" HeaderButtonType="TextButton"
                                                            DataField="numWareHouseID" Display="false"  >
                                                        </rad:GridBoundColumn>
                                                         <rad:GridBoundColumn SortExpression="numWarehouseItmsID" HeaderText="" HeaderButtonType="TextButton"
                                                            DataField="numWarehouseItmsID" Display="false"  ></rad:GridBoundColumn>
                                                             <rad:GridBoundColumn SortExpression="numItemCode" HeaderText="" HeaderButtonType="TextButton"
                                                            DataField="numItemCode" Display="false"  >
                                                        </rad:GridBoundColumn>
                                                         <rad:GridBoundColumn SortExpression="vcItemName" HeaderText="Item" HeaderButtonType="TextButton"
                                                            DataField="vcItemName">
                                                        </rad:GridBoundColumn>
                                                         <rad:GridBoundColumn SortExpression="Warehouse" HeaderText="WareHouse" HeaderButtonType="TextButton"
                                                            DataField="Warehouse">
                                                        </rad:GridBoundColumn>
                                                        <rad:GridBoundColumn SortExpression="ItemType" HeaderText="Type" HeaderButtonType="TextButton"
                                                            DataField="ItemType"> 
                                                            </rad:GridBoundColumn>
                                                             <rad:GridBoundColumn SortExpression="vcItemDesc" HeaderText="Description" HeaderButtonType="TextButton"
                                                            DataField="vcItemDesc">
                                                             </rad:GridBoundColumn>
                                                              <rad:GridBoundColumn SortExpression="Attributes" HeaderText="Attributes" HeaderButtonType="TextButton"
                                                            DataField="Attributes">
                                                             </rad:GridBoundColumn>
                                                               <rad:GridBoundColumn SortExpression="DropShip" HeaderText="Drop Ship" HeaderButtonType="TextButton"
                                                            DataField="DropShip">
                                                             </rad:GridBoundColumn>
                                                               <rad:GridBoundColumn SortExpression="numUnitHour" HeaderText="Units" HeaderButtonType="TextButton"
                                                            DataField="numUnitHour">
                                                             </rad:GridBoundColumn>
                                                              <rad:GridBoundColumn SortExpression="monPrice" HeaderText="Unit Price" HeaderButtonType="TextButton"
                                                            DataField="monPrice">
                                                             </rad:GridBoundColumn>
                                                          <rad:GridBoundColumn SortExpression="monTotAmount" HeaderText="Amount" HeaderButtonType="TextButton"
                                                            DataField="monTotAmount">
                                                             </rad:GridBoundColumn>
                                                    </Columns>
                                                    <DetailTables>
                                                        <rad:GridTableView DataMember="SerialNo" HierarchyLoadMode="Client"   Width="100%">
                                                            <ParentTableRelation>
                                                            
                                                                <rad:GridRelationFields DetailKeyField="numoppitemtCode" MasterKeyField="numoppitemtCode" />
                                                            </ParentTableRelation>
                                                            <Columns>
                                                             <rad:GridBoundColumn SortExpression="numWarehouseItmsDTLID" HeaderText="" HeaderButtonType="TextButton"
                                                            DataField="numWarehouseItmsDTLID" Display="false"  ></rad:GridBoundColumn>
                                                             <rad:GridBoundColumn SortExpression="numoppitemtCode" HeaderText="" HeaderButtonType="TextButton"
                                                            DataField="numoppitemtCode" Display="false"  >
                                                        </rad:GridBoundColumn>
                                                            <rad:GridBoundColumn SortExpression="vcSerialNo" HeaderText="Serial No" HeaderButtonType="TextButton"
                                                            DataField="vcSerialNo"></rad:GridBoundColumn>
                                                              <rad:GridBoundColumn SortExpression="Comments" HeaderText="Comments" HeaderButtonType="TextButton"
                                                            DataField="Comments"></rad:GridBoundColumn>
                                                              <rad:GridBoundColumn SortExpression="Attributes" HeaderText="Attributes" HeaderButtonType="TextButton"
                                                            DataField="Attributes"></rad:GridBoundColumn>
                                                            </Columns>
                                                        </rad:GridTableView>
                                                        <rad:GridTableView DataMember="ChildItems" HierarchyLoadMode="Client"  Width="100%">
                                                            <ParentTableRelation>
                                                                <rad:GridRelationFields DetailKeyField="numoppitemtCode" MasterKeyField="numoppitemtCode" />
                                                            </ParentTableRelation>
                                                            <Columns>
                                                               <rad:GridBoundColumn SortExpression="vcItemName" HeaderText="Item" HeaderButtonType="TextButton"
                                                            DataField="vcItemName"></rad:GridBoundColumn>
                                                             <rad:GridBoundColumn SortExpression="txtItemDesc" HeaderText="Description" HeaderButtonType="TextButton"
                                                            DataField="txtItemDesc"></rad:GridBoundColumn>
                                                           <rad:GridBoundColumn SortExpression="charItemType" HeaderText="Type" HeaderButtonType="TextButton"
                                                            DataField="charItemType"></rad:GridBoundColumn>
                                                               <rad:GridBoundColumn SortExpression="UnitPrice" HeaderText="Unit Price" HeaderButtonType="TextButton"
                                                            DataField="UnitPrice"></rad:GridBoundColumn>
                                                              <rad:GridBoundColumn SortExpression="numQtyItemReq" HeaderText="Quantity" HeaderButtonType="TextButton"
                                                            DataField="numQtyItemsReq"></rad:GridBoundColumn>
                                                             
                                                     
                                                            </Columns>
                                                        </rad:GridTableView>
                                                    </DetailTables>
                                                </MasterTableView>
                                                <ClientSettings >
                                                    <Selecting  AllowRowSelect="true" />
                                                </ClientSettings>
                                            </rad:RadGrid>
											    <br>
										    </asp:tableCell>
									    </asp:tableRow>
								    </asp:table>
                                </ContentTemplate>
                            </igtab:Tab>
                            <igtab:Tab Text="BizDocs" >
                              <ContentTemplate>
                                  <asp:table id="table5" Runat="server" BorderWidth="1" Width="100%" BackColor="white" cellspacing="0"
					                        cellpadding="0" BorderColor="black" GridLines="None" Height="300" CssClass="aspTable">
					                        <asp:tableRow>
						                        <asp:tableCell VerticalAlign="Top">
							                        <table width="100%">
								                        <tr>
									                        <td>
										                        <div id="divBizDocsDtl" runat="server">
											                        <iframe id="IframeBiz" frameborder="0"  width="100%" scrolling="auto" runat="server" height="300"></iframe>
										                        </div>
									                        </td>
								                        </tr>
							                        </table>
						                        </asp:tableCell>
					                        </asp:tableRow>
				                        </asp:table>
				                </ContentTemplate>
                            </igtab:Tab>
                        </Tabs>
                    </igtab:ultrawebtab>
						</td>
				</tr>
			</table>
			
			
			<table width="100%">
				<tr>
					<td class="normal4" align="center"><asp:literal id="litMessage" Runat="server"></asp:literal></td>
				</tr>
			</table>
			    <asp:textbox id="txtrecOwner" style="DISPLAY: none" Runat="server"></asp:textbox>
				<asp:textbox id="txtName" Runat="server" style="DISPLAY: none" ></asp:textbox>
				<asp:Label ID="lblAmount" Runat="server" style="DISPLAY: none"></asp:Label>
				<asp:TextBox runat="server" ID="txtDivId" style="display:none"></asp:TextBox>
					<asp:TextBox runat="server" ID="txtOppId" style="display:none"></asp:TextBox>
		    </ContentTemplate>
	</asp:updatepanel>		  
			
				
				
			</form>
	</body>
</html>

