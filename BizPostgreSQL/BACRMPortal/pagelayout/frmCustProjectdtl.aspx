<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmCustProjectdtl.aspx.vb"
    Inherits="BACRMPortal.frmCustProjectdtl" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="~/include/calandar.ascx" %>
<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../common/topbar.ascx" %>
<%@ Register Src="../Common/frmMilestone.ascx" TagName="frmMilestone" TagPrefix="uc1" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <title>Projects</title>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <script language="javascript" type="text/javascript">
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeletMsg() {
            var bln = confirm("You're about to remove the Stage from this Process, all stage data will be deleted")
            if (bln == true) {
                return true;
            }
            else {
                return false;
            }
        }
        function OpenDocuments(a) {
            window.open("../Documents/frmSpecDocuments.aspx?Type=P&RecID=" + a, '', 'toolbar=no,titlebar=no,top=200,width=700,height=450,left=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function CheckNumber() {
            if (!(window.event.keyCode > 47 && window.event.keyCode < 58)) {
                window.event.keyCode = 0;
            }
        }

        function Save(cint) {

            if (cint == 1) {
                if (document.Form1.ddlIntPrgMgr.selectedIndex == 0) {
                    alert("Select Internal Project Manager");
                    tsVert.selectedIndex = 0;
                    document.Form1.ddlTaskContact.focus();
                    return false;
                }
            }
            if (document.Form1.ddlCustPrjMgr.value == 0) {
                alert("Select Customer Project Manager");
                tsVert.selectedIndex = 0;
                document.Form1.ddlCustPrjMgr.focus();
                return false;
            }

        }
        function AddContact() {
            if (document.Form1.ddlcompany.value == 0) {
                alert("Select Customer");
                tsVert.selectedIndex = 2;
                document.Form1.ddlcompany.focus();
                return false;
            }
            if (document.Form1.ddlAssocContactId.value == 0) {
                alert("Select Contact");
                tsVert.selectedIndex = 2;
                document.Form1.ddlAssocContactId.focus();
                return false;
            }
            var str;
            for (i = 0; i < document.Form1.elements.length; i++) {
                if (i <= 9) {
                    str = '0' + (i + 1)
                }
                else {
                    str = i + 1
                }
                if (document.getElementById('dgContact_ctl' + str + '_txtContactID') != null) {
                    if (document.getElementById('dgContact_ctl' + str + '_txtContactID').value == document.Form1.ddlAssocContactId.value) {
                        alert("Associated contact is already added");
                        return false;
                    }
                }
            }

        }

        function deleteItem() {
            var bln;
            bln = window.confirm("Delete Seleted Row - Are You Sure ?")
            if (bln == true) {
                return true;
            }
            else {
                return false;
            }
        }


        function OpenTemplate(a, b) {
            window.open('../common/templates.aspx?pageid=' + a + '&id=' + b, '', 'toolbar=no,titlebar=no,left=500, top=300,width=350,height=200,scrollbars=no,resizable=yes');
            return false;
        }
        function OpenDependency(a, b, c, d) {
            window.open('../projects/frmProDependency.aspx?ProStageID=' + a + '&Proid=' + b + '&PerID=' + c + '&StgDtlId=' + d, '', 'toolbar=no,titlebar=no,left=500, top=300,width=500,height=200,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenExpense(a, b, c) {
            window.open('../projects/frmProExpense.aspx?ProStageID=' + a + '&Proid=' + b + '&Divid=' + c, '', 'toolbar=no,titlebar=no,left=500, top=300,width=650,height=300,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenTime(a, b, c) {
            window.open('../projects/frmProTime.aspx?ProStageID=' + a + '&Proid=' + b + '&Divid=' + c, '', 'toolbar=no,titlebar=no,left=500, top=300,width=650,height=300,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenSubStage(a, b, c, d) {
            window.open('../projects/frmprosubstages.aspx?ProStageID=' + a + '&Proid=' + b + '&PerID=' + c + '&StgDtlId=' + d, '', 'toolbar=no,titlebar=no,left=500, top=300,width=500,height=200,scrollbars=yes,resizable=yes');
            return false;
        }
        function CheckBoxCon(a, b, c) {
            if (parseInt(c) == 1) {
                document.getElementById('chkStage~' + a + '~' + b).checked = true
            }
            else {
                document.getElementById('chkStage~' + a + '~' + b).checked = false
            }
        }
        function ValidateCheckBox(cint) {
            if (cint == 1) {
                if (document.getElementById('chkDClosed').checked == true) {
                    if (document.getElementById('chkDlost').checked == true) {
                        alert("The Deal is already Lost !")
                        document.getElementById('chkDClosed').checked = false
                        return false;
                    }
                }
            }
            if (cint == 2) {
                if (document.getElementById('chkDlost').checked == true) {
                    if (document.getElementById('chkDClosed').checked == true) {
                        alert("The Deal is already Closed !")
                        document.getElementById('chkDlost').checked = false
                        return false;
                    }
                }
            }
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function ShowWindow1(Page, q, att) {
            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                //window.location.reload(true);
                return false;

            }

        }

        function ShowLayout(a, b) {
            window.open("../pagelayout/frmCustomisePageLayout.aspx?Ctype=j", '', 'toolbar=no,titlebar=no,width=800,height=500,top=200,scrollbars=yes,resizable=yes');
            return false;
        }
    </script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <menu1:webmenu ID="webmenu1" runat="server"></menu1:webmenu>
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server">
    </asp:ScriptManager>
   
    <div style="float: right; margin-right: 10px;">
        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button"></asp:Button></div>
    <table width="100%" align="center">
        <tr>
            <td>
                <telerik:RadTabStrip ID="radOppTab" runat="server" UnSelectChildren="True" Skin="Vista"
                    ClickSelectedTab="True" SelectedIndex="0" MultiPageID="radMultiPage_OppTab"
                    AutoPostBack="true">
                    <Tabs>
                        <telerik:RadTab Text="&nbsp;&nbsp;Milestones And Stages&nbsp;&nbsp;" Value="Milestone"
                            PageViewID="radPageView_Milestone" PostBack="false">
                        </telerik:RadTab>
                        <telerik:RadTab Text="&nbsp;&nbsp;Project Income & Expense&nbsp;&nbsp;" Value="Report"
                            PageViewID="radPageView_Report" PostBack="true">
                        </telerik:RadTab>
                    </Tabs>
                </telerik:RadTabStrip>
                <telerik:RadMultiPage ID="radMultiPage_OppTab" runat="server" CssClass="pageView"
                    Width="100%">
                    <telerik:RadPageView ID="radPageView_Milestone" runat="server">
                        <asp:Table ID="tblMile" BorderWidth="1" runat="server" Width="100%" CellPadding="0"
                            CellSpacing="0" BorderColor="black" GridLines="None" Height="300">
                            <asp:TableRow>
                                <asp:TableCell VerticalAlign="Top">
                                    <uc1:frmMilestone ID="Milestone1" runat="server" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="radPageView_Report" runat="server">
                        <asp:Table ID="table1" runat="server" BorderWidth="1" Width="100%" BackColor="white"
                            CellSpacing="0" CellPadding="0" BorderColor="black" GridLines="None" Height="350"
                            CssClass="aspTable">
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="4" Height="5%">&nbsp;</asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Top">
                                <asp:TableCell VerticalAlign="Top" HorizontalAlign="Center" CssClass="normal1" Height="10%">
                                    <b>Total Income:</b>
                                    <asp:Label ID="lblReportTotalIncome" runat="server"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell VerticalAlign="Top" HorizontalAlign="Center" CssClass="normal1">
                                    <b>Total Expense:</b>
                                    <asp:Label ID="lblReportTotalExpense" runat="server"></asp:Label>
                                </asp:TableCell>
                                <asp:TableCell VerticalAlign="Top" HorizontalAlign="Center" CssClass="normal1">
                                    <b>Balance:</b>
                                    <asp:Label ID="lblReportBalance" runat="server"></asp:Label>&nbsp;
                                    <asp:Image ID="imgUPDown" runat="server" Width="10" Height="10" />
                                </asp:TableCell>
                                <asp:TableCell VerticalAlign="Top" HorizontalAlign="Right" CssClass="normal1" Width="10%">
                                    <asp:DropDownList ID="ddlReportsType" AutoPostBack="true" Width="180" runat="server"
                                        CssClass="signup">
                                        <asp:ListItem Value="0">Use Order Amounts</asp:ListItem>
                                        <asp:ListItem Value="1">Use Auth BizDoc Amounts</asp:ListItem>
                                    </asp:DropDownList>
                                    <%--  <asp:Button ID="btnReportExportToExcel" Width="120" CssClass="button" Text="Export to Excel"
                                                runat="server"></asp:Button>&nbsp;--%>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow VerticalAlign="Top">
                                <asp:TableCell VerticalAlign="Top" ColumnSpan="4">
                                    <asp:GridView ID="gvReports" runat="server" AutoGenerateColumns="False" CssClass="dg"
                                        Width="100%">
                                        <AlternatingRowStyle CssClass="ais" />
                                        <RowStyle CssClass="is" />
                                        <HeaderStyle CssClass="hs" />
                                        <Columns>
                                            <asp:BoundField HeaderText="Created On" DataField="dtDateEntered" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField HeaderText="Created By" DataField="vcCreatedBy" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField HeaderText="From" DataField="vcFrom" ItemStyle-HorizontalAlign="Center" />
                                            <asp:BoundField HeaderText="Item Name" DataField="vcItemName" ItemStyle-HorizontalAlign="Center" />
                                            <asp:TemplateField HeaderText="Type" HeaderStyle-ForeColor="White" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="hplDetails" runat="server" CssClass="hyperlink" Text='<%# Eval("vcTimeExpenseType") %>'></asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Units/Hours" DataField="numUnitHour" ItemStyle-HorizontalAlign="Center" />
                                            <asp:TemplateField HeaderText="Unit Price" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <%# String.Format("{0:##,#00.00}", Eval("monPrice"))%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Amount" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <%# String.Format("{0:##,#00.00}", Eval("ExpenseAmount"))%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:Image ID="imgUPDown" runat="server" Width="10" Height="10" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="BizDoc">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBizDoc" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </telerik:RadPageView>
                </telerik:RadMultiPage>
                <%-- <igtab:UltraWebTab ImageDirectory="" ID="uwOppTab" runat="server" ThreeDEffect="True"
                    BorderStyle="Solid" Width="100%" BarHeight="0" BorderWidth="0" AsyncMode="On">
                    <defaulttabstyle height="23px" font-bold="true" font-size="11px" font-names="Arial">
                    </defaulttabstyle>
                    <roundedimage leftsidewidth="7" rightsidewidth="8" shiftofimages="0" selectedimage="../images/ig_tab_winXPs3.gif"
                        normalimage="../images/ig_tab_winXP3.gif" hoverimage="../images/ig_tab_winXPs3.gif"
                        fillstyle="LeftMergedWithCenter"></roundedimage>
                    <selectedtabstyle height="23px" forecolor="white">
                    </selectedtabstyle>
                    <hovertabstyle height="23px" forecolor="white">
                    </hovertabstyle>
                    <asyncoptions requestcontext="IncludeAllTabs" enableloadondemand="True" enableprogressindicator="true"
                        triggers="SelectedTabChanged" />
                    <tabs>
                       
                        <igtab:Tab Text="&nbsp;&nbsp;Project Income & Expense&nbsp;&nbsp;" AsyncOption="LoadOnDemand"
                            Key="Report">
                            <ContentTemplate>
                                <asp:Table ID="Table1" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
                                    CssClass="aspTable" Width="100%" BorderColor="black" GridLines="None" Height="300">
                                    <asp:TableRow VerticalAlign="Top" Height="30">
                                        <asp:TableCell HorizontalAlign="Right" Width="70%">
                                                <strong>View From :</strong>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <BizCalendar:Calendar ID="calDateFrom" runat="server" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                                                <strong>To:</strong>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <BizCalendar:Calendar ID="calDateTo" runat="server" />
                                        </asp:TableCell><asp:TableCell>
                                            <asp:Button ID="btnCreate" runat="server" Text="Create Report" CssClass="button" />
                                        </asp:TableCell></asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell VerticalAlign="Top">
                                            <igtab:UltraWebTab ID="uwReportTab" runat="server" ThreeDEffect="false">
                                                <DefaultTabStyle Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Arial"
                                                    ForeColor="Gray" BorderStyle="Solid" BorderWidth="1px" BorderColor="Gray">
                                                </DefaultTabStyle>
                                                <%--<RoundedImage LeftSideWidth="7" RightSideWidth="8" ShiftOfImages="0" SelectedImage="../images/ig_tab_winXPs3.gif"
                                                    NormalImage="../images/ig_tab_winXP3.gif" HoverImage="../images/ig_tab_winXPs3.gif"
                                                    FillStyle="LeftMergedWithCenter"></RoundedImage>-%>
                                                <BorderDetails WidthTop="2px" WidthLeft="2px" WidthRight="2px" ColorLeft="Gray" WidthBottom="2px"
                                                    ColorRight="Gray" ColorTop="Gray" />
                                                <SelectedTabStyle Height="23px" ForeColor="Black">
                                                </SelectedTabStyle>
                                                <HoverTabStyle Height="23px" ForeColor="Gray">
                                                </HoverTabStyle>
                                                <Tabs>
                                                    <igtab:Tab Text="&nbsp;Timeline&nbsp;">
                                                        <ContentPane>
                                                            <igchart:UltraChart ID="ucTimeLine" runat="server" ChartType="GanttChart" Width="800"
                                                                Height="550">
                                                                <Border CornerRadius="0" DrawStyle="Solid" Raised="False" Color="Black" Thickness="1">
                                                                </Border>
                                                                <TitleRight Font="Microsoft Sans Serif, 7.8pt" Visible="False" Text="" FontSizeBestFit="False"
                                                                    Orientation="VerticalRightFacing" WrapText="False" Extent="26" FontColor="Black"
                                                                    HorizontalAlign="Near" VerticalAlign="Center" Location="Right">
                                                                    <Margins Bottom="5" Left="5" Top="5" Right="5"></Margins>
                                                                </TitleRight>
                                                                <Data DataMember="" SwapRowsAndColumns="False" UseMinMax="False" UseRowLabelsColumn="False"
                                                                    MinValue="-1.7976931348623157E+308" RowLabelsColumn="-1" ZeroAligned="False"
                                                                    MaxValue="1.7976931348623157E+308">
                                                                    <EmptyStyle Text="Empty" EnableLineStyle="False" ShowInLegend="False" EnablePE="False"
                                                                        EnablePoint="False">
                                                                        <PointPE FillGradientStyle="None" FillOpacity="255" FillStopOpacity="255" ElementType="SolidFill"
                                                                            Fill="Transparent" Hatch="None" Texture="LightGrain" ImageFitStyle="StretchedFit"
                                                                            FillStopColor="Transparent" StrokeOpacity="255" ImagePath="" Stroke="Black" StrokeWidth="1"
                                                                            ImageWrapMode="Tile" TextureApplication="Normal"></PointPE>
                                                                        <PointStyle CharacterFont="Microsoft Sans Serif, 7.8pt"></PointStyle>
                                                                        <LineStyle MidPointAnchors="False" EndStyle="NoAnchor" DrawStyle="Dash" StartStyle="NoAnchor">
                                                                        </LineStyle>
                                                                        <PE FillGradientStyle="None" FillOpacity="255" FillStopOpacity="255" ElementType="SolidFill"
                                                                            Fill="Transparent" Hatch="None" Texture="LightGrain" ImageFitStyle="StretchedFit"
                                                                            FillStopColor="Transparent" StrokeOpacity="255" ImagePath="" Stroke="Black" StrokeWidth="1"
                                                                            ImageWrapMode="Tile" TextureApplication="Normal"></PE>
                                                                    </EmptyStyle>
                                                                </Data>
                                                                <TitleLeft Font="Microsoft Sans Serif, 7.8pt" Visible="False" Text="" FontSizeBestFit="False"
                                                                    Orientation="VerticalLeftFacing" WrapText="False" Extent="26" FontColor="Black"
                                                                    HorizontalAlign="Near" VerticalAlign="Center" Location="Left">
                                                                    <Margins Bottom="5" Left="5" Top="5" Right="5"></Margins>
                                                                </TitleLeft>
                                                                <ColorModel ColorBegin="DarkGoldenrod" ColorEnd="Navy" AlphaLevel="150" ModelStyle="CustomSkin"
                                                                    Grayscale="False" Scaling="None">
                                                                    <Skin ApplyRowWise="False">
                                                                        <PEs>
                                                                            <igchartprop:PaintElement FillGradientStyle="Vertical" FillOpacity="255" FillStopOpacity="255"
                                                                                ElementType="Gradient" Fill="108, 162, 36" Hatch="None" Texture="LightGrain"
                                                                                ImageFitStyle="StretchedFit" FillStopColor="148, 244, 17" StrokeOpacity="255"
                                                                                ImagePath="" Stroke="Black" StrokeWidth="0" ImageWrapMode="Tile" TextureApplication="Normal">
                                                                            </igchartprop:PaintElement>
                                                                            <igchartprop:PaintElement FillGradientStyle="Vertical" FillOpacity="255" FillStopOpacity="255"
                                                                                ElementType="Gradient" Fill="7, 108, 176" Hatch="None" Texture="LightGrain" ImageFitStyle="StretchedFit"
                                                                                FillStopColor="53, 200, 255" StrokeOpacity="255" ImagePath="" Stroke="Black"
                                                                                StrokeWidth="0" ImageWrapMode="Tile" TextureApplication="Normal"></igchartprop:PaintElement>
                                                                            <igchartprop:PaintElement FillGradientStyle="Vertical" FillOpacity="255" FillStopOpacity="255"
                                                                                ElementType="Gradient" Fill="230, 190, 2" Hatch="None" Texture="LightGrain" ImageFitStyle="StretchedFit"
                                                                                FillStopColor="255, 255, 81" StrokeOpacity="255" ImagePath="" Stroke="Black"
                                                                                StrokeWidth="0" ImageWrapMode="Tile" TextureApplication="Normal"></igchartprop:PaintElement>
                                                                            <igchartprop:PaintElement FillGradientStyle="Vertical" FillOpacity="255" FillStopOpacity="255"
                                                                                ElementType="Gradient" Fill="215, 0, 5" Hatch="None" Texture="LightGrain" ImageFitStyle="StretchedFit"
                                                                                FillStopColor="254, 117, 16" StrokeOpacity="255" ImagePath="" Stroke="Black"
                                                                                StrokeWidth="0" ImageWrapMode="Tile" TextureApplication="Normal"></igchartprop:PaintElement>
                                                                            <igchartprop:PaintElement FillGradientStyle="Vertical" FillOpacity="255" FillStopOpacity="255"
                                                                                ElementType="Gradient" Fill="252, 122, 10" Hatch="None" Texture="LightGrain"
                                                                                ImageFitStyle="StretchedFit" FillStopColor="255, 108, 66" StrokeOpacity="255"
                                                                                ImagePath="" Stroke="Black" StrokeWidth="0" ImageWrapMode="Tile" TextureApplication="Normal">
                                                                            </igchartprop:PaintElement>
                                                                        </PEs>
                                                                    </Skin>
                                                                </ColorModel>
                                                                <Legend Font="Microsoft Sans Serif, 7.8pt" Visible="False" AlphaLevel="150" BorderThickness="1"
                                                                    BorderStyle="Solid" SpanPercentage="25" BorderColor="Navy" FontColor="Black"
                                                                    BackgroundColor="FloralWhite" DataAssociation="DefaultData" Location="Right"
                                                                    FormatString="&lt;ITEM_LABEL&gt;">
                                                                    <Margins Bottom="5" Left="5" Top="5" Right="5"></Margins>
                                                                </Legend>
                                                                <Axis BackColor="Cornsilk">
                                                                    <Y LineEndCapStyle="NoAnchor" LineDrawStyle="Solid" Visible="True" RangeMin="0" LineColor="Black"
                                                                        RangeType="Automatic" TickmarkInterval="0" LineThickness="1" Extent="110" LogBase="10"
                                                                        RangeMax="0" TickmarkStyle="Percentage" TickmarkPercentage="10" NumericAxisType="Linear">
                                                                        <StripLines Interval="2" Visible="False">
                                                                            <PE FillGradientStyle="None" FillOpacity="255" FillStopOpacity="255" ElementType="SolidFill"
                                                                                Fill="Transparent" Hatch="None" Texture="LightGrain" ImageFitStyle="StretchedFit"
                                                                                FillStopColor="Transparent" StrokeOpacity="255" ImagePath="" Stroke="Black" StrokeWidth="1"
                                                                                ImageWrapMode="Tile" TextureApplication="Normal"></PE>
                                                                        </StripLines>
                                                                        <ScrollScale Scale="1" Scroll="0" Height="10" Width="15" Visible="False"></ScrollScale>
                                                                        <Labels ItemFormatString="&lt;ITEM_LABEL&gt;" VerticalAlign="Center" WrapText="False"
                                                                            FontSizeBestFit="False" SeriesFormatString="&lt;SERIES_LABEL&gt;" ClipText="True"
                                                                            Font="Verdana, 7pt" Flip="False" ItemFormat="ItemLabel" FontColor="Black" Orientation="Horizontal"
                                                                            Visible="True" OrientationAngle="0" HorizontalAlign="Center">
                                                                            <SeriesLabels Font="Verdana, 7pt" Visible="False" HorizontalAlign="Center" FontSizeBestFit="False"
                                                                                ClipText="True" FormatString="&lt;SERIES_LABEL&gt;" Orientation="VerticalLeftFacing"
                                                                                WrapText="False" Flip="False" FontColor="Black" VerticalAlign="Center" OrientationAngle="0">
                                                                            </SeriesLabels>
                                                                        </Labels>
                                                                        <MajorGridLines AlphaLevel="255" DrawStyle="Dot" Color="Gainsboro" Visible="True"
                                                                            Thickness="1"></MajorGridLines>
                                                                        <MinorGridLines AlphaLevel="255" DrawStyle="Dot" Color="LightGray" Visible="False"
                                                                            Thickness="1"></MinorGridLines>
                                                                        <TimeAxisStyle TimeAxisStyle="Continuous"></TimeAxisStyle>
                                                                        <Margin>
                                                                            <Far MarginType="Percentage" Value="0"></Far>
                                                                            <Near MarginType="Percentage" Value="0"></Near>
                                                                        </Margin>
                                                                    </Y>
                                                                    <Y2 LineEndCapStyle="NoAnchor" LineDrawStyle="Solid" Visible="False" RangeMin="0"
                                                                        LineColor="Black" RangeType="Automatic" TickmarkInterval="0" LineThickness="2"
                                                                        Extent="80" LogBase="10" RangeMax="0" TickmarkStyle="Percentage" TickmarkPercentage="10"
                                                                        NumericAxisType="Linear">
                                                                        <StripLines Interval="2" Visible="False">
                                                                            <PE FillGradientStyle="None" FillOpacity="255" FillStopOpacity="255" ElementType="SolidFill"
                                                                                Fill="Transparent" Hatch="None" Texture="LightGrain" ImageFitStyle="StretchedFit"
                                                                                FillStopColor="Transparent" StrokeOpacity="255" ImagePath="" Stroke="Black" StrokeWidth="1"
                                                                                ImageWrapMode="Tile" TextureApplication="Normal"></PE>
                                                                        </StripLines>
                                                                        <ScrollScale Scale="1" Scroll="0" Height="10" Width="15" Visible="False"></ScrollScale>
                                                                        <Labels ItemFormatString="&lt;ITEM_LABEL&gt;" VerticalAlign="Center" WrapText="False"
                                                                            FontSizeBestFit="False" SeriesFormatString="&lt;SERIES_LABEL&gt;" ClipText="True"
                                                                            Font="Microsoft Sans Serif, 7.8pt" Flip="False" ItemFormat="ItemLabel" FontColor="Black"
                                                                            Orientation="Horizontal" Visible="True" OrientationAngle="0" HorizontalAlign="Far">
                                                                            <SeriesLabels Font="Microsoft Sans Serif, 7.8pt" Visible="True" HorizontalAlign="Far"
                                                                                FontSizeBestFit="False" ClipText="True" FormatString="&lt;SERIES_LABEL&gt;" Orientation="VerticalLeftFacing"
                                                                                WrapText="False" Flip="False" FontColor="Black" VerticalAlign="Center" OrientationAngle="0">
                                                                            </SeriesLabels>
                                                                        </Labels>
                                                                        <MajorGridLines AlphaLevel="255" DrawStyle="Dot" Color="Gainsboro" Visible="True"
                                                                            Thickness="1"></MajorGridLines>
                                                                        <MinorGridLines AlphaLevel="255" DrawStyle="Dot" Color="LightGray" Visible="False"
                                                                            Thickness="1"></MinorGridLines>
                                                                        <TimeAxisStyle TimeAxisStyle="Continuous"></TimeAxisStyle>
                                                                        <Margin>
                                                                            <Far MarginType="Percentage" Value="0"></Far>
                                                                            <Near MarginType="Percentage" Value="0"></Near>
                                                                        </Margin>
                                                                    </Y2>
                                                                    <X2 LineEndCapStyle="NoAnchor" LineDrawStyle="Solid" Visible="False" RangeMin="0"
                                                                        LineColor="Black" RangeType="Automatic" TickmarkInterval="0" LineThickness="2"
                                                                        Extent="80" LogBase="10" RangeMax="0" TickmarkStyle="Percentage" TickmarkPercentage="10"
                                                                        NumericAxisType="Linear">
                                                                        <StripLines Interval="2" Visible="False">
                                                                            <PE FillGradientStyle="None" FillOpacity="255" FillStopOpacity="255" ElementType="SolidFill"
                                                                                Fill="Transparent" Hatch="None" Texture="LightGrain" ImageFitStyle="StretchedFit"
                                                                                FillStopColor="Transparent" StrokeOpacity="255" ImagePath="" Stroke="Black" StrokeWidth="1"
                                                                                ImageWrapMode="Tile" TextureApplication="Normal"></PE>
                                                                        </StripLines>
                                                                        <ScrollScale Scale="1" Scroll="0" Height="10" Width="15" Visible="False"></ScrollScale>
                                                                        <Labels ItemFormatString="&lt;ITEM_LABEL:MM-dd-yy&gt;" VerticalAlign="Center" WrapText="False"
                                                                            FontSizeBestFit="False" SeriesFormatString="" ClipText="True" Font="Microsoft Sans Serif, 7.8pt"
                                                                            Flip="False" ItemFormat="Custom" FontColor="Black" Orientation="VerticalLeftFacing"
                                                                            Visible="True" OrientationAngle="0" HorizontalAlign="Near">
                                                                            <SeriesLabels Font="Microsoft Sans Serif, 7.8pt" Visible="True" HorizontalAlign="Near"
                                                                                FontSizeBestFit="False" ClipText="True" FormatString="" Orientation="Horizontal"
                                                                                WrapText="False" Flip="False" FontColor="Black" VerticalAlign="Center" OrientationAngle="0">
                                                                            </SeriesLabels>
                                                                        </Labels>
                                                                        <MajorGridLines AlphaLevel="255" DrawStyle="Dot" Color="Gainsboro" Visible="True"
                                                                            Thickness="1"></MajorGridLines>
                                                                        <MinorGridLines AlphaLevel="255" DrawStyle="Dot" Color="LightGray" Visible="False"
                                                                            Thickness="1"></MinorGridLines>
                                                                        <TimeAxisStyle TimeAxisStyle="Continuous"></TimeAxisStyle>
                                                                        <Margin>
                                                                            <Far MarginType="Percentage" Value="0"></Far>
                                                                            <Near MarginType="Percentage" Value="0"></Near>
                                                                        </Margin>
                                                                    </X2>
                                                                    <Z2 LineEndCapStyle="NoAnchor" LineDrawStyle="Solid" Visible="False" RangeMin="0"
                                                                        LineColor="Black" RangeType="Automatic" TickmarkInterval="0" LineThickness="2"
                                                                        Extent="80" LogBase="10" RangeMax="0" TickmarkStyle="Percentage" TickmarkPercentage="10"
                                                                        NumericAxisType="Linear">
                                                                        <StripLines Interval="2" Visible="False">
                                                                            <PE FillGradientStyle="None" FillOpacity="255" FillStopOpacity="255" ElementType="SolidFill"
                                                                                Fill="Transparent" Hatch="None" Texture="LightGrain" ImageFitStyle="StretchedFit"
                                                                                FillStopColor="Transparent" StrokeOpacity="255" ImagePath="" Stroke="Black" StrokeWidth="1"
                                                                                ImageWrapMode="Tile" TextureApplication="Normal"></PE>
                                                                        </StripLines>
                                                                        <ScrollScale Scale="1" Scroll="0" Height="10" Width="15" Visible="False"></ScrollScale>
                                                                        <Labels ItemFormatString="" VerticalAlign="Center" WrapText="False" FontSizeBestFit="False"
                                                                            SeriesFormatString="&lt;SERIES_LABEL&gt;" ClipText="True" Font="Microsoft Sans Serif, 7.8pt"
                                                                            Flip="False" ItemFormat="None" FontColor="Black" Orientation="Horizontal" Visible="True"
                                                                            OrientationAngle="0" HorizontalAlign="Near">
                                                                            <SeriesLabels Font="Microsoft Sans Serif, 7.8pt" Visible="True" HorizontalAlign="Near"
                                                                                FontSizeBestFit="False" ClipText="True" FormatString="&lt;SERIES_LABEL&gt;" Orientation="Horizontal"
                                                                                WrapText="False" Flip="False" FontColor="Black" VerticalAlign="Center" OrientationAngle="0">
                                                                            </SeriesLabels>
                                                                        </Labels>
                                                                        <MajorGridLines AlphaLevel="255" DrawStyle="Dot" Color="Gainsboro" Visible="True"
                                                                            Thickness="1"></MajorGridLines>
                                                                        <MinorGridLines AlphaLevel="255" DrawStyle="Dot" Color="LightGray" Visible="False"
                                                                            Thickness="1"></MinorGridLines>
                                                                        <TimeAxisStyle TimeAxisStyle="Continuous"></TimeAxisStyle>
                                                                        <Margin>
                                                                            <Far MarginType="Percentage" Value="0"></Far>
                                                                            <Near MarginType="Percentage" Value="0"></Near>
                                                                        </Margin>
                                                                    </Z2>
                                                                    <Z LineEndCapStyle="NoAnchor" LineDrawStyle="Solid" Visible="False" RangeMin="0"
                                                                        LineColor="Black" RangeType="Automatic" TickmarkInterval="0" LineThickness="2"
                                                                        Extent="80" LogBase="10" RangeMax="0" TickmarkStyle="Percentage" TickmarkPercentage="10"
                                                                        NumericAxisType="Linear">
                                                                        <StripLines Interval="2" Visible="False">
                                                                            <PE FillGradientStyle="None" FillOpacity="255" FillStopOpacity="255" ElementType="SolidFill"
                                                                                Fill="Transparent" Hatch="None" Texture="LightGrain" ImageFitStyle="StretchedFit"
                                                                                FillStopColor="Transparent" StrokeOpacity="255" ImagePath="" Stroke="Black" StrokeWidth="1"
                                                                                ImageWrapMode="Tile" TextureApplication="Normal"></PE>
                                                                        </StripLines>
                                                                        <ScrollScale Scale="1" Scroll="0" Height="10" Width="15" Visible="False"></ScrollScale>
                                                                        <Labels ItemFormatString="" VerticalAlign="Center" WrapText="False" FontSizeBestFit="False"
                                                                            SeriesFormatString="&lt;SERIES_LABEL&gt;" ClipText="True" Font="Microsoft Sans Serif, 7.8pt"
                                                                            Flip="False" ItemFormat="None" FontColor="Black" Orientation="Horizontal" Visible="True"
                                                                            OrientationAngle="0" HorizontalAlign="Near">
                                                                            <SeriesLabels Font="Microsoft Sans Serif, 7.8pt" Visible="True" HorizontalAlign="Near"
                                                                                FontSizeBestFit="False" ClipText="True" FormatString="&lt;SERIES_LABEL&gt;" Orientation="Horizontal"
                                                                                WrapText="False" Flip="False" FontColor="Black" VerticalAlign="Center" OrientationAngle="0">
                                                                            </SeriesLabels>
                                                                        </Labels>
                                                                        <MajorGridLines AlphaLevel="255" DrawStyle="Dot" Color="Gainsboro" Visible="True"
                                                                            Thickness="1"></MajorGridLines>
                                                                        <MinorGridLines AlphaLevel="255" DrawStyle="Dot" Color="LightGray" Visible="False"
                                                                            Thickness="1"></MinorGridLines>
                                                                        <TimeAxisStyle TimeAxisStyle="Continuous"></TimeAxisStyle>
                                                                        <Margin>
                                                                            <Far MarginType="Percentage" Value="0"></Far>
                                                                            <Near MarginType="Percentage" Value="0"></Near>
                                                                        </Margin>
                                                                    </Z>
                                                                    <X LineEndCapStyle="NoAnchor" LineDrawStyle="Solid" Visible="True" RangeMin="0" LineColor="Black"
                                                                        RangeType="Automatic" TickmarkInterval="0" LineThickness="1" Extent="60" LogBase="10"
                                                                        RangeMax="0" TickmarkStyle="Percentage" TickmarkPercentage="10" NumericAxisType="Linear">
                                                                        <StripLines Interval="2" Visible="False">
                                                                            <PE FillGradientStyle="None" FillOpacity="255" FillStopOpacity="255" ElementType="SolidFill"
                                                                                Fill="Transparent" Hatch="None" Texture="LightGrain" ImageFitStyle="StretchedFit"
                                                                                FillStopColor="Transparent" StrokeOpacity="255" ImagePath="" Stroke="Black" StrokeWidth="1"
                                                                                ImageWrapMode="Tile" TextureApplication="Normal"></PE>
                                                                        </StripLines>
                                                                        <ScrollScale Scale="1" Scroll="0" Height="10" Width="15" Visible="False"></ScrollScale>
                                                                        <Labels ItemFormatString="&lt;ITEM_LABEL:MM-dd-yy&gt;" VerticalAlign="Center" WrapText="False"
                                                                            FontSizeBestFit="False" SeriesFormatString="" ClipText="True" Font="Verdana, 7pt"
                                                                            Flip="False" ItemFormat="Custom" FontColor="Black" Orientation="VerticalLeftFacing"
                                                                            Visible="True" OrientationAngle="0" HorizontalAlign="Near">
                                                                            <SeriesLabels Font="Microsoft Sans Serif, 7.8pt" Visible="false" HorizontalAlign="Far"
                                                                                FontSizeBestFit="False" ClipText="True" FormatString="" Orientation="VerticalLeftFacing"
                                                                                WrapText="False" Flip="False" FontColor="Black" VerticalAlign="Center" OrientationAngle="0">
                                                                            </SeriesLabels>
                                                                        </Labels>
                                                                        <MajorGridLines AlphaLevel="255" DrawStyle="Dot" Color="Gainsboro" Visible="True"
                                                                            Thickness="1"></MajorGridLines>
                                                                        <MinorGridLines AlphaLevel="255" DrawStyle="Dot" Color="LightGray" Visible="False"
                                                                            Thickness="1"></MinorGridLines>
                                                                        <TimeAxisStyle TimeAxisStyle="Continuous"></TimeAxisStyle>
                                                                        <Margin>
                                                                            <Far MarginType="Percentage" Value="0"></Far>
                                                                            <Near MarginType="Percentage" Value="0"></Near>
                                                                        </Margin>
                                                                    </X>
                                                                </Axis>
                                                                <TitleBottom Font="Microsoft Sans Serif, 7.8pt" Visible="False" Text="" FontSizeBestFit="False"
                                                                    Orientation="Horizontal" WrapText="False" Extent="26" FontColor="Black" HorizontalAlign="Far"
                                                                    VerticalAlign="Center" Location="Bottom">
                                                                    <Margins Bottom="5" Left="5" Top="5" Right="5"></Margins>
                                                                </TitleBottom>
                                                                <TitleTop Font="Microsoft Sans Serif, 7.8pt" Visible="False" Text="" FontSizeBestFit="False"
                                                                    Orientation="Horizontal" WrapText="False" Extent="33" FontColor="Black" HorizontalAlign="Near"
                                                                    VerticalAlign="Center" Location="Top">
                                                                    <Margins Bottom="5" Left="5" Top="5" Right="5"></Margins>
                                                                </TitleTop>
                                                                <DeploymentScenario ImageURL="ChartImages/Chart_#SEQNUM(100).png" ImageType="Png"
                                                                    FilePath="ChartImages" />
                                                                <GanttChart LinkLineColor="Black">
                                                                    <LinkLineStyle MidPointAnchors="False" EndStyle="ArrowAnchor" DrawStyle="Solid" StartStyle="NoAnchor">
                                                                    </LinkLineStyle>
                                                                    <CompletePercentagesPE FillGradientStyle="None" FillOpacity="255" FillStopOpacity="255"
                                                                        ElementType="SolidFill" Fill="Yellow" Hatch="None" Texture="LightGrain" ImageFitStyle="StretchedFit"
                                                                        FillStopColor="Transparent" StrokeOpacity="255" ImagePath="" Stroke="Black" StrokeWidth="1"
                                                                        ImageWrapMode="Tile" TextureApplication="Normal"></CompletePercentagesPE>
                                                                    <OwnersLabelStyle Font="Microsoft Sans Serif, 7.8pt" Dy="0" HorizontalAlign="Center"
                                                                        FontSizeBestFit="False" ClipText="False" Dx="0" RotationAngle="0" Orientation="Horizontal"
                                                                        WrapText="False" Flip="False" FontColor="Black" VerticalAlign="Center"></OwnersLabelStyle>
                                                                    <EmptyPercentagesPE FillGradientStyle="None" FillOpacity="255" FillStopOpacity="255"
                                                                        ElementType="SolidFill" Fill="White" Hatch="None" Texture="LightGrain" ImageFitStyle="StretchedFit"
                                                                        FillStopColor="Transparent" StrokeOpacity="255" ImagePath="" Stroke="Black" StrokeWidth="1"
                                                                        ImageWrapMode="Tile" TextureApplication="Normal"></EmptyPercentagesPE>
                                                                </GanttChart>
                                                                <Tooltips BorderThickness="1" Overflow="None" FormatString="&lt;DATA_VALUE:00.##&gt;"
                                                                    EnableFadingEffect="False" Format="DataValue" FontColor="White" BorderColor="Black"
                                                                    Font-Names="verdana" Font-Size="Small" Display="MouseMove" BackColor="Gray" Padding="0">
                                                                </Tooltips>
                                                            </igchart:UltraChart>
                                                        </ContentPane>
                                                    </igtab:Tab>
                                                    <igtab:TabSeparator>
                                                    </igtab:TabSeparator>
                                                    <igtab:Tab Text="&nbsp;Income & Expense&nbsp;">
                                                        <ContentPane>
                                                            <table align="left">
                                                                <tr>
                                                                    <td class="normal1" colspan="10" align="right">
                                                                        <b>
                                                                            <asp:Label ID="lblBalance" runat="server"></asp:Label></b>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="normal1">
                                                                    </td>
                                                                    <td class="normal1" style="color: Green" colspan="2">
                                                                        <span runat="server" id="spnIncome"><u>Income:<asp:Label ID="lblIncome" runat="server"></asp:Label></u></span>
                                                                    </td>
                                                                    <td class="normal1" style="color: Red" colspan="2">
                                                                        <span runat="server" id="spnExpense"><u>Expense:<asp:Label ID="lblExpense" runat="server"></asp:Label></u></span>
                                                                    </td>
                                                                    <td class="normal1" align="right">
                                                                        Billable Hours:
                                                                    </td>
                                                                    <td class="normal1" align="left">
                                                                        <asp:Label ID="lblBillableHours" runat="server"></asp:Label>
                                                                    </td>
                                                                    <td class="normal1" align="right">
                                                                        Non-Billable Hours:
                                                                    </td>
                                                                    <td class="normal1" align="left">
                                                                        <asp:Label ID="lblNonBillableHours" runat="server"></asp:Label>
                                                                    </td>
                                                                    <td class="normal1">
                                                                    </td>
                                                                </tr>
                                                                <tr runat="server" id="trContract">
                                                                    <td class="normal1" align="right">
                                                                        Contract:
                                                                    </td>
                                                                    <td class="normal1">
                                                                        <asp:Label ID="lblContract" runat="server"></asp:Label>
                                                                    </td>
                                                                    <td class="normal1" align="right">
                                                                        Amount Balance:
                                                                    </td>
                                                                    <td class="normal1">
                                                                        <asp:Label ID="lblAmountBalance" runat="server"></asp:Label>
                                                                    </td>
                                                                    <td class="normal1" align="right">
                                                                        Days Remaining:
                                                                    </td>
                                                                    <td class="normal1">
                                                                        <asp:Label ID="lblDays" runat="server"></asp:Label>
                                                                    </td>
                                                                    <td class="normal1" align="right">
                                                                        Incidents Remaining:
                                                                    </td>
                                                                    <td class="normal1">
                                                                        <asp:Label ID="lblIncidents" runat="server"></asp:Label>
                                                                    </td>
                                                                    <td class="normal1" align="right">
                                                                        Hours Remaining:
                                                                    </td>
                                                                    <td class="normal1">
                                                                        <asp:Label ID="lblHours" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="normal1" align="right">
                                                                        Comments:
                                                                    </td>
                                                                    <td colspan="9" class="normal1" align="left">
                                                                        <asp:Label ID="lblComments" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="5" align="center">
                                                                        <igchart:UltraChart ID="ultraChartIncome" runat="server" ChartType="PieChart" BackgroundImageFileName=""
                                                                            BorderColor="Black" BorderWidth="0px" EmptyChartText="Data Not Available. Please call UltraChart.Data.DataBind() after setting valid Data.DataSource"
                                                                            Version="9.1">
                                                                            <Legend Font="Microsoft Sans Serif, 7pt" Location="Top" SpanPercentage="35" Visible="True">
                                                                            </Legend>
                                                                            <ColorModel AlphaLevel="150" ColorBegin="Pink" ColorEnd="DarkRed" ModelStyle="CustomLinear">
                                                                                <Skin>
                                                                                    <PEs>
                                                                                        <igchartprop:PaintElement ElementType="Gradient" Fill="108, 162, 36" FillGradientStyle="Horizontal"
                                                                                            FillStopColor="148, 244, 17" StrokeWidth="0" />
                                                                                        <igchartprop:PaintElement ElementType="Gradient" Fill="7, 108, 176" FillGradientStyle="Horizontal"
                                                                                            FillStopColor="53, 200, 255" StrokeWidth="0" />
                                                                                        <igchartprop:PaintElement ElementType="Gradient" Fill="230, 190, 2" FillGradientStyle="Horizontal"
                                                                                            FillStopColor="255, 255, 81" StrokeWidth="0" />
                                                                                        <igchartprop:PaintElement ElementType="Gradient" Fill="215, 0, 5" FillGradientStyle="Horizontal"
                                                                                            FillStopColor="254, 117, 16" StrokeWidth="0" />
                                                                                        <igchartprop:PaintElement ElementType="Gradient" Fill="252, 122, 10" FillGradientStyle="Horizontal"
                                                                                            FillStopColor="255, 108, 66" StrokeWidth="0" />
                                                                                    </PEs>
                                                                                </Skin>
                                                                            </ColorModel>
                                                                            <PieChart>
                                                                                <Labels Font="Verdana, 7pt" />
                                                                            </PieChart>
                                                                            <Axis>
                                                                                <Y LineThickness="1" TickmarkInterval="0" TickmarkStyle="Smart" Visible="True">
                                                                                    <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                                                                        Visible="True" />
                                                                                    <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                                                                        Visible="False" />
                                                                                    <Labels Font="Verdana, 7pt" FontColor="DimGray" HorizontalAlign="Near" ItemFormatString="&lt;DATA_VALUE:00.##&gt;"
                                                                                        Orientation="Horizontal" VerticalAlign="Center">
                                                                                        <SeriesLabels Font="Verdana, 7pt" FontColor="DimGray" FormatString="" HorizontalAlign="Near"
                                                                                            Orientation="Horizontal" VerticalAlign="Center">
                                                                                            <Layout Behavior="Auto">
                                                                                            </Layout>
                                                                                        </SeriesLabels>
                                                                                        <Layout Behavior="Auto">
                                                                                        </Layout>
                                                                                    </Labels>
                                                                                </Y>
                                                                                <PE ElementType="None" Fill="Cornsilk" />
                                                                                <X LineThickness="1" TickmarkInterval="0" TickmarkStyle="Smart" Visible="True">
                                                                                    <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                                                                        Visible="True" />
                                                                                    <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                                                                        Visible="False" />
                                                                                    <Labels Font="Verdana, 7pt" FontColor="DimGray" HorizontalAlign="Near" ItemFormatString="&lt;ITEM_LABEL&gt;"
                                                                                        Orientation="Horizontal" VerticalAlign="Center">
                                                                                        <SeriesLabels Font="Verdana, 7pt" FontColor="DimGray" FormatString="" HorizontalAlign="Near"
                                                                                            Orientation="Horizontal" VerticalAlign="Center">
                                                                                            <Layout Behavior="Auto">
                                                                                            </Layout>
                                                                                        </SeriesLabels>
                                                                                        <Layout Behavior="Auto">
                                                                                        </Layout>
                                                                                    </Labels>
                                                                                </X>
                                                                                <Y2 LineThickness="1" TickmarkInterval="0" TickmarkStyle="Smart" Visible="False">
                                                                                    <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                                                                        Visible="True" />
                                                                                    <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                                                                        Visible="False" />
                                                                                    <Labels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Near" ItemFormatString=""
                                                                                        Orientation="Horizontal" VerticalAlign="Center">
                                                                                        <SeriesLabels Font="Verdana, 7pt" FontColor="Gray" FormatString="" HorizontalAlign="Near"
                                                                                            Orientation="Horizontal" VerticalAlign="Center">
                                                                                            <Layout Behavior="Auto">
                                                                                            </Layout>
                                                                                        </SeriesLabels>
                                                                                        <Layout Behavior="Auto">
                                                                                        </Layout>
                                                                                    </Labels>
                                                                                </Y2>
                                                                                <X2 LineThickness="1" TickmarkInterval="0" TickmarkStyle="Smart" Visible="False">
                                                                                    <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                                                                        Visible="True" />
                                                                                    <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                                                                        Visible="False" />
                                                                                    <Labels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Near" ItemFormatString=""
                                                                                        Orientation="Horizontal" VerticalAlign="Center">
                                                                                        <SeriesLabels Font="Verdana, 7pt" FontColor="Gray" FormatString="" HorizontalAlign="Near"
                                                                                            Orientation="Horizontal" VerticalAlign="Center">
                                                                                            <Layout Behavior="Auto">
                                                                                            </Layout>
                                                                                        </SeriesLabels>
                                                                                        <Layout Behavior="Auto">
                                                                                        </Layout>
                                                                                    </Labels>
                                                                                </X2>
                                                                                <Z LineThickness="1" TickmarkInterval="0" TickmarkStyle="Smart" Visible="False">
                                                                                    <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                                                                        Visible="True" />
                                                                                    <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                                                                        Visible="False" />
                                                                                    <Labels Font="Verdana, 7pt" FontColor="DimGray" HorizontalAlign="Near" ItemFormatString=""
                                                                                        Orientation="Horizontal" VerticalAlign="Center">
                                                                                        <SeriesLabels Font="Verdana, 7pt" FontColor="DimGray" HorizontalAlign="Near" Orientation="Horizontal"
                                                                                            VerticalAlign="Center">
                                                                                            <Layout Behavior="Auto">
                                                                                            </Layout>
                                                                                        </SeriesLabels>
                                                                                        <Layout Behavior="Auto">
                                                                                        </Layout>
                                                                                    </Labels>
                                                                                </Z>
                                                                                <Z2 LineThickness="1" TickmarkInterval="0" TickmarkStyle="Smart" Visible="False">
                                                                                    <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                                                                        Visible="True" />
                                                                                    <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                                                                        Visible="False" />
                                                                                    <Labels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Near" ItemFormatString=""
                                                                                        Orientation="Horizontal" VerticalAlign="Center">
                                                                                        <SeriesLabels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Near" Orientation="Horizontal"
                                                                                            VerticalAlign="Center">
                                                                                            <Layout Behavior="Auto">
                                                                                            </Layout>
                                                                                        </SeriesLabels>
                                                                                        <Layout Behavior="Auto">
                                                                                        </Layout>
                                                                                    </Labels>
                                                                                </Z2>
                                                                            </Axis>
                                                                            <TitleTop Extent="33" Font="Microsoft Sans Serif, 9pt" Location="Left" Visible="True"
                                                                                Text="Income">
                                                                            </TitleTop>
                                                                            <Effects>
                                                                                <Effects>
                                                                                    <igchartprop:gradienteffect />
                                                                                </Effects>
                                                                            </Effects>
                                                                            <Data>
                                                                                <EmptyStyle>
                                                                                    <LineStyle DrawStyle="Dash" />
                                                                                </EmptyStyle>
                                                                            </Data>
                                                                            <Border Thickness="0" />
                                                                        </igchart:UltraChart>
                                                                    </td>
                                                                    <td colspan="5" align="center">
                                                                        <igchart:UltraChart ID="ultraChartExpense" runat="server" ChartType="PieChart" BackgroundImageFileName=""
                                                                            BorderColor="Black" BorderWidth="0px" EmptyChartText="Data Not Available. Please call UltraChart.Data.DataBind() after setting valid Data.DataSource"
                                                                            Version="9.1">
                                                                            <Legend Font="Microsoft Sans Serif, 7pt" Location="Top" SpanPercentage="35" Visible="True">
                                                                            </Legend>
                                                                            <ColorModel AlphaLevel="150" ColorBegin="Pink" ColorEnd="DarkRed" ModelStyle="CustomLinear">
                                                                                <Skin>
                                                                                    <PEs>
                                                                                        <igchartprop:PaintElement ElementType="Gradient" Fill="108, 162, 36" FillGradientStyle="Horizontal"
                                                                                            FillStopColor="148, 244, 17" StrokeWidth="0" />
                                                                                        <igchartprop:PaintElement ElementType="Gradient" Fill="7, 108, 176" FillGradientStyle="Horizontal"
                                                                                            FillStopColor="53, 200, 255" StrokeWidth="0" />
                                                                                        <igchartprop:PaintElement ElementType="Gradient" Fill="230, 190, 2" FillGradientStyle="Horizontal"
                                                                                            FillStopColor="255, 255, 81" StrokeWidth="0" />
                                                                                        <igchartprop:PaintElement ElementType="Gradient" Fill="215, 0, 5" FillGradientStyle="Horizontal"
                                                                                            FillStopColor="254, 117, 16" StrokeWidth="0" />
                                                                                        <igchartprop:PaintElement ElementType="Gradient" Fill="252, 122, 10" FillGradientStyle="Horizontal"
                                                                                            FillStopColor="255, 108, 66" StrokeWidth="0" />
                                                                                    </PEs>
                                                                                </Skin>
                                                                            </ColorModel>
                                                                            <PieChart>
                                                                                <Labels Font="Verdana, 7pt" />
                                                                            </PieChart>
                                                                            <Axis>
                                                                                <Y LineThickness="1" TickmarkInterval="0" TickmarkStyle="Smart" Visible="True">
                                                                                    <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                                                                        Visible="True" />
                                                                                    <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                                                                        Visible="False" />
                                                                                    <Labels Font="Verdana, 7pt" FontColor="DimGray" HorizontalAlign="Near" ItemFormatString="&lt;DATA_VALUE:00.##&gt;"
                                                                                        Orientation="Horizontal" VerticalAlign="Center">
                                                                                        <SeriesLabels Font="Verdana, 7pt" FontColor="DimGray" FormatString="" HorizontalAlign="Near"
                                                                                            Orientation="Horizontal" VerticalAlign="Center">
                                                                                            <Layout Behavior="Auto">
                                                                                            </Layout>
                                                                                        </SeriesLabels>
                                                                                        <Layout Behavior="Auto">
                                                                                        </Layout>
                                                                                    </Labels>
                                                                                </Y>
                                                                                <PE ElementType="None" Fill="Cornsilk" />
                                                                                <X LineThickness="1" TickmarkInterval="0" TickmarkStyle="Smart" Visible="True">
                                                                                    <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                                                                        Visible="True" />
                                                                                    <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                                                                        Visible="False" />
                                                                                    <Labels Font="Verdana, 7pt" FontColor="DimGray" HorizontalAlign="Near" ItemFormatString="&lt;ITEM_LABEL&gt;"
                                                                                        Orientation="Horizontal" VerticalAlign="Center">
                                                                                        <SeriesLabels Font="Verdana, 7pt" FontColor="DimGray" FormatString="" HorizontalAlign="Near"
                                                                                            Orientation="Horizontal" VerticalAlign="Center">
                                                                                            <Layout Behavior="Auto">
                                                                                            </Layout>
                                                                                        </SeriesLabels>
                                                                                        <Layout Behavior="Auto">
                                                                                        </Layout>
                                                                                    </Labels>
                                                                                </X>
                                                                                <Y2 LineThickness="1" TickmarkInterval="0" TickmarkStyle="Smart" Visible="False">
                                                                                    <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                                                                        Visible="True" />
                                                                                    <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                                                                        Visible="False" />
                                                                                    <Labels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Near" ItemFormatString=""
                                                                                        Orientation="Horizontal" VerticalAlign="Center">
                                                                                        <SeriesLabels Font="Verdana, 7pt" FontColor="Gray" FormatString="" HorizontalAlign="Near"
                                                                                            Orientation="Horizontal" VerticalAlign="Center">
                                                                                            <Layout Behavior="Auto">
                                                                                            </Layout>
                                                                                        </SeriesLabels>
                                                                                        <Layout Behavior="Auto">
                                                                                        </Layout>
                                                                                    </Labels>
                                                                                </Y2>
                                                                                <X2 LineThickness="1" TickmarkInterval="0" TickmarkStyle="Smart" Visible="False">
                                                                                    <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                                                                        Visible="True" />
                                                                                    <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                                                                        Visible="False" />
                                                                                    <Labels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Near" ItemFormatString=""
                                                                                        Orientation="Horizontal" VerticalAlign="Center">
                                                                                        <SeriesLabels Font="Verdana, 7pt" FontColor="Gray" FormatString="" HorizontalAlign="Near"
                                                                                            Orientation="Horizontal" VerticalAlign="Center">
                                                                                            <Layout Behavior="Auto">
                                                                                            </Layout>
                                                                                        </SeriesLabels>
                                                                                        <Layout Behavior="Auto">
                                                                                        </Layout>
                                                                                    </Labels>
                                                                                </X2>
                                                                                <Z LineThickness="1" TickmarkInterval="0" TickmarkStyle="Smart" Visible="False">
                                                                                    <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                                                                        Visible="True" />
                                                                                    <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                                                                        Visible="False" />
                                                                                    <Labels Font="Verdana, 7pt" FontColor="DimGray" HorizontalAlign="Near" ItemFormatString=""
                                                                                        Orientation="Horizontal" VerticalAlign="Center">
                                                                                        <SeriesLabels Font="Verdana, 7pt" FontColor="DimGray" HorizontalAlign="Near" Orientation="Horizontal"
                                                                                            VerticalAlign="Center">
                                                                                            <Layout Behavior="Auto">
                                                                                            </Layout>
                                                                                        </SeriesLabels>
                                                                                        <Layout Behavior="Auto">
                                                                                        </Layout>
                                                                                    </Labels>
                                                                                </Z>
                                                                                <Z2 LineThickness="1" TickmarkInterval="0" TickmarkStyle="Smart" Visible="False">
                                                                                    <MajorGridLines AlphaLevel="255" Color="Gainsboro" DrawStyle="Dot" Thickness="1"
                                                                                        Visible="True" />
                                                                                    <MinorGridLines AlphaLevel="255" Color="LightGray" DrawStyle="Dot" Thickness="1"
                                                                                        Visible="False" />
                                                                                    <Labels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Near" ItemFormatString=""
                                                                                        Orientation="Horizontal" VerticalAlign="Center">
                                                                                        <SeriesLabels Font="Verdana, 7pt" FontColor="Gray" HorizontalAlign="Near" Orientation="Horizontal"
                                                                                            VerticalAlign="Center">
                                                                                            <Layout Behavior="Auto">
                                                                                            </Layout>
                                                                                        </SeriesLabels>
                                                                                        <Layout Behavior="Auto">
                                                                                        </Layout>
                                                                                    </Labels>
                                                                                </Z2>
                                                                            </Axis>
                                                                            <TitleTop Extent="33" Font="Microsoft Sans Serif, 9pt" Location="Left" Visible="True"
                                                                                Text="Expenses">
                                                                            </TitleTop>
                                                                            <Effects>
                                                                                <Effects>
                                                                                    <igchartprop:gradienteffect />
                                                                                </Effects>
                                                                            </Effects>
                                                                            <Data>
                                                                                <EmptyStyle>
                                                                                    <LineStyle DrawStyle="Dash" />
                                                                                </EmptyStyle>
                                                                            </Data>
                                                                            <Border Thickness="0" />
                                                                        </igchart:UltraChart>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ContentPane>
                                                    </igtab:Tab>
                                                </Tabs>
                                            </igtab:UltraWebTab>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </ContentTemplate>
                        </igtab:Tab>
                    </tabs>
                </igtab:UltraWebTab>--%>
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
    <asp:TextBox ID="txtTemplateId" Text="0" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtProcessId" Text="0" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox runat="server" ID="txtDivId" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtEmailTotalPage" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtEmailTotalRecords" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtProName" Style="display: none" runat="server"></asp:TextBox>
    </form>
</body>
</html>
