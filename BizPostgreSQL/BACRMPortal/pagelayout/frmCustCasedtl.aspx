<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmCustCasedtl.aspx.vb"
    Inherits="BACRMPortal.frmCustCasedtl" %>

<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="~/include/calandar.ascx" %>
<%@ Register Src="../common/frmComments.ascx" TagName="frmComments" TagPrefix="uc2" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <title>Case</title>
    <script language="javascript" type="text/javascript">
        function openActionItem(a, b, c, d) {
            window.location.href = "../Admin/actionitemdetails.aspx?frm=contactdetails&CommId=" + a + "&CaseId=" + b + "&CaseTimeId=" + c + "&CaseExpId=" + d;
            return false;
        }
        function OpenEmailMessage(a, b) {
            window.open("../contact/frmEmailMessage.aspx?Email=" + a + "&Date=" + b, '', 'width=750,height=525,status=no,titlebar=no,scrollbars=yes,top=110,left=250')
            return false;
        }
        function AddContact() {
            if (document.Form1.ddlcompany.value == 0) {
                alert("Select Customer");
                tsCases.selectedIndex = 2;
                document.Form1.ddlcompany.focus();
                return false;
            }
            if (document.Form1.ddlAssocContactId.value == 0) {
                alert("Select Contact");
                tsCases.selectedIndex = 2;
                document.Form1.ddlAssocContactId.focus();
                return false;
            }
            var str;
            for (i = 0; i < document.Form1.elements.length; i++) {
                if (i <= 9) {
                    str = '0' + (i + 1)
                }
                else {
                    str = i + 1
                }
                if (document.getElementById('dgContact_ctl' + str + '_txtContactID') != null) {
                    if (document.getElementById('dgContact_ctl' + str + '_txtContactID').value == document.Form1.ddlAssocContactId.value) {
                        alert("Associated contact is already added");
                        return false;
                    }
                }
            }

        }

        function ShowWindow(Page, q, att) {
            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                //document.Form1.ddlStatus.style.display='none';
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                //document.Form1.ddlStatus.style.display='';
                return false;

            }
        }
        function SelectAll1(a) {
            var str;

            if (typeof (a) == 'string') {

                a = document.getElementById(a);

            }
            var dgLinkedItems = document.getElementById('uwOppTab__ctl0_dgLinkedItems');
            if (a.checked == true) {
                for (var i = 1; i <= dgLinkedItems.rows.length; i++) {
                    if (i < 10) {
                        str = '0' + i
                    }
                    else {
                        str = i
                    }
                    document.getElementById('uwOppTab__ctl0_dgLinkedItems_ctl' + str + '_chk').checked = true;

                }
            }
            else if (a.checked == false) {
                for (var i = 1; i <= dgLinkedItems.rows.length; i++) {
                    if (i < 10) {
                        str = '0' + i
                    }
                    else {
                        str = i
                    }

                    document.getElementById('uwOppTab__ctl0_dgLinkedItems_ctl' + str + '_chk').checked = false;
                }
            }
        }
        function GetSelectedItems() {
            var dgLinkedItems = document.getElementById('uwOppTab__ctl0_dgLinkedItems');
            var OppItemID = '';
            for (var i = 1; i <= dgLinkedItems.rows.length; i++) {
                if (i < 10) {
                    str = '0' + i
                }
                else {
                    str = i
                }
                if (str != '01')
                    if (document.getElementById('uwOppTab__ctl0_dgLinkedItems_ctl' + str + '_chk').checked) {
                        OppItemID = OppItemID + document.getElementById('uwOppTab__ctl0_dgLinkedItems_ctl' + str + '_lblOppItemID').innerHTML + ',';
                    }
            }
            document.getElementById('hdnOppItemIDs').value = OppItemID;
            return true;
        }
        function ShowLayout(a, b) {
            window.open("../pagelayout/frmCustomisePageLayout.aspx?Ctype=e", '', 'toolbar=no,titlebar=no,width=800,height=500,top=200,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenItems(a) {
            window.open("../cases/frmLinkedOppItems.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&CaseID=" + a, '', 'width=800,height=500,status=no,titlebar=no,scrollbar=yes,top=110,left=150,resizable=yes')
            return false;

        }
        function OpenEmail(txtMailAddr, a, b, c) {
            window.open('mailto:' + txtMailAddr);
            return false;
        }
    </script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server">
    </asp:ScriptManager>
    <table width="100%" align="center">
        <tr>
            <td>
                <table width="100%">
                    <tr>
                        <td class="normal1" align="left">
                            Organization : <u>
                                <asp:HyperLink ID="hplCustomer" runat="server" CssClass="hyperlink"></asp:HyperLink>
                        </td>
                        <td class="normal1" align="right">
                            Case Number :
                        </td>
                        <td class="normal1">
                            <asp:Label ID="lblcasenumber" ForeColor="Red" runat="server"></asp:Label>
                        </td>
                        <td class="normal1" align="right">
                            Status :
                        </td>
                        <td class="normal1">
                            <asp:Label runat="server" ID="lblCaseStatus"></asp:Label>
                        </td>
                        <td align="right">
                            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close"></asp:Button>
                            <asp:Button ID="btnActDelete" runat="server" CssClass="Delete" Text="X"></asp:Button>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:Panel runat="server" ID="pnlContract">
        <table width="100%" class="normal1" align="center">
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td width="10%" align="right">
                                Contract :
                            </td>
                            <td width="20%">
                                <asp:Label ID="lblContractName" runat="server" CssClass="normal1"></asp:Label>
                            </td>
                            <td width="10%" align="right">
                                Amount Balance :
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblRemAmount" CssClass="normal1"></asp:Label>
                            </td>
                            <td width="10%" align="right">
                                Days Remaining :
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblRemDays" CssClass="normal1"></asp:Label>
                            </td>
                            <td width="10%" align="right">
                                Incidents Remaining :
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblRemInci" CssClass="normal1"></asp:Label>
                            </td>
                            <td width="10%" align="right">
                                Hours Remaining :
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblRemHours" CssClass="normal1"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <telerik:RadTabStrip ID="radCaseTab" runat="server" UnSelectChildren="True" Skin="Vista"
        ClickSelectedTab="True" AutoPostBack="True" SelectedIndex="0" MultiPageID="radMultiPage_OppTab">
        <Tabs>
            <telerik:RadTab Text="&nbsp;&nbsp;Case Details&nbsp;&nbsp;" Value="Cases" PageViewID="radPageView_Case">
            </telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage ID="radMultiPage_CaseTab" runat="server" SelectedIndex="0" CssClass="pageView"
        Width="100%">
        <telerik:RadPageView ID="radPageView_Case" runat="server">
            <asp:Table ID="tblOppr" runat="server" BorderColor="black" BorderWidth="1" CellPadding="0"
                CellSpacing="0" CssClass="aspTable" GridLines="None" Height="300" Width="100%">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Top">
                        <br />
                        <table width="100%" style="background-image: url('../images/SuitCase-32.gif'); background-repeat: no-repeat">
                            <tr>
                                <td colspan="2" valign="top">
                                    <asp:Table ID="tblMain" CellPadding="3" CellSpacing="3" align="center" Width="100%"
                                        GridLines="none" border="0" runat="server">
                                    </asp:Table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="left">
                                    <table width="100%" border="0">
                                        <asp:Panel ID="pnlKnowledgeBase" runat="server">
                                            <tr>
                                                <td>
                                                    <asp:DataGrid ID="dgSolution" runat="server" CssClass="dg" Width="100%" BorderColor="white"
                                                        ShowFooter="False" AutoGenerateColumns="false">
                                                        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                                        <ItemStyle CssClass="is"></ItemStyle>
                                                        <HeaderStyle CssClass="hs"></HeaderStyle>
                                                        <Columns>
                                                            <asp:BoundColumn Visible="False" DataField="numSolnID"></asp:BoundColumn>
                                                            <asp:ButtonColumn DataTextField="Solution Name" HeaderText="Solution Name" CommandName="Solution">
                                                            </asp:ButtonColumn>
                                                            <asp:TemplateColumn HeaderText="Solution Description">
                                                                <ItemTemplate>
                                                                    <%#Eval("ShortDesc").ToString()%>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                    <br>
                                                </td>
                                            </tr>
                                        </asp:Panel>
                                        <tr>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="normal1" align="left">
                                                <b>Linked Items</b>
                                            </td>
                                            <td align="right">
                                                <asp:Button ID="btnAddItem" runat="server" Text="Add" CssClass="button"></asp:Button>&nbsp;
                                                <asp:Button ID="btnRemove" runat="server" Text="Remove Selected" CssClass="button"
                                                    OnClientClick="GetSelectedItems();"></asp:Button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="normal1" align="right" colspan="2">
                                                <asp:DataGrid ID="dgLinkedItems" runat="server" CssClass="dg" Width="100%" BorderColor="white"
                                                    AutoGenerateColumns="False">
                                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                                    <ItemStyle CssClass="is"></ItemStyle>
                                                    <HeaderStyle CssClass="hs"></HeaderStyle>
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderText="">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chk" runat="server" onclick="SelectAll1('uwOppTab__ctl0_dgLinkedItems_ctl01_chk')" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chk" runat="server" />
                                                                <asp:Label runat="server" ID="lblOppItemID" Text='<%# Eval("numoppitemtCode") %>'
                                                                    Style="display: none"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateColumn>
                                                        <asp:BoundColumn DataField="numoppitemtCode" Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="numOppId" Visible="false"></asp:BoundColumn>
                                                        <asp:TemplateColumn HeaderText="Image">
                                                            <ItemTemplate>
                                                                <%#CCommon.GetImageHTML(Eval("vcPathForTImage"), 1)%>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateColumn>
                                                        <asp:BoundColumn HeaderText="Item Name" DataField="vcItemName"></asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="Model #" DataField="vcModelId"></asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="Description" DataField="vcItemDesc"></asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="Units" DataField="numUnitHour"></asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="Total Amount" DataField="monTotAmount" DataFormatString="{0:#,###.00}">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="Serial No" DataField="vcSerialNo"></asp:BoundColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                                <br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <uc2:frmComments ID="frmComments1" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </telerik:RadPageView>
    </telerik:RadMultiPage>
   <%-- <igtab:UltraWebTab AutoPostBack="true" ImageDirectory="" ID="uwOppTab" runat="server"
        ThreeDEffect="True" BorderStyle="Solid" Width="100%" BarHeight="0" BorderWidth="0">
        <defaulttabstyle height="23px" font-bold="true" font-size="11px" font-names="Arial">
        </defaulttabstyle>
        <roundedimage leftsidewidth="7" rightsidewidth="8" shiftofimages="0" selectedimage="../images/ig_tab_winXPs3.gif"
            normalimage="../images/ig_tab_winXP3.gif" hoverimage="../images/ig_tab_winXPs3.gif"
            fillstyle="LeftMergedWithCenter"></roundedimage>
        <selectedtabstyle height="23px" forecolor="white">
        </selectedtabstyle>
        <hovertabstyle height="23px" forecolor="white">
        </hovertabstyle>
        <tabs>
            <igtab:Tab Text="&nbsp;&nbsp;Case Details&nbsp;&nbsp;">
                <ContentTemplate>
                    
                </ContentTemplate>
            </igtab:Tab>
        </tabs>
    </igtab:UltraWebTab>--%>
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
    <asp:TextBox ID="txtTotalPage" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtDivId" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtCntId" Style="display: none" runat="server"></asp:TextBox>
    <asp:HiddenField ID="hdnOppItemIDs" runat="server" />
    </form>
</body>
</html>
