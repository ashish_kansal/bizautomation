Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
'Imports Infragistics.WebUI.UltraWebTab

Partial Class frmCustContactdtl : Inherits BACRMPage

    Dim lngCntID, lngDivID As Long
    Dim FromDate, ToDate As String
    Dim m_aryRightsForCusFlds(), m_aryRightsForEmail(), m_aryRightsForOpp(), m_aryRightsForPage(), m_aryRightsForAOI() As Integer
    Dim objContacts As New ContactIP
    Dim objPageControls As New PageControls
    Dim boolIntermediatoryPage As Boolean = True
    Dim objCommon As New CCommon
    Dim dtTableInfo As DataTable
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents btnGo As System.Web.UI.WebControls.Button
    Protected WithEvents btnAssGo As System.Web.UI.WebControls.Button
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    'Dim SI As Integer = 0
    'Dim SI1 As Integer = 0
    'Dim SI2 As Integer = 0
    'Dim frm As String = ""
    'Dim frm1 As String = ""
    'Dim frm2 As String = ""

    'Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    '    Try
    '        If Session("UserContactID") = Nothing Then
    '            Response.Redirect("../Common/frmLogout.aspx")
    '        End If
    '        If Not GetQueryStringVal( "SI") Is Nothing Then
    '            SI = GetQueryStringVal( "SI")
    '        End If
    '        If Not GetQueryStringVal( "SI1") Is Nothing Then
    '            SI1 = GetQueryStringVal( "SI1")
    '        Else : SI1 = 0
    '        End If
    '        If Not GetQueryStringVal( "SI2") Is Nothing Then
    '            SI2 = GetQueryStringVal( "SI2")
    '        Else : SI2 = 0
    '        End If
    '        If Not GetQueryStringVal( "frm") Is Nothing Then
    '            frm = ""
    '            frm = GetQueryStringVal( "frm")
    '        Else : frm = ""
    '        End If
    '        If Not GetQueryStringVal( "frm1") Is Nothing Then
    '            frm1 = ""
    '            frm1 = GetQueryStringVal( "frm1")
    '        Else : frm1 = ""
    '        End If
    '        If Not GetQueryStringVal( "SI2") Is Nothing Then
    '            frm2 = ""
    '            frm2 = GetQueryStringVal( "frm2")
    '        Else : frm2 = ""
    '        End If
    '        If Session("UserContactID") = Nothing Then Response.Redirect("../Common/frmLogout.aspx")

    '        lngDivID = Session("DivId")
    '        lngCntID = Session("ContactId")

    '        LoadTableInformation()
    '        If Not IsPostBack Then
    '            If uwOppTab.Tabs.Count > SI Then uwOppTab.SelectedTabIndex = SI
    '            Dim objCommon As New CCommon
    '            m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmContacts.aspx", Session("UserContactID"), 15, 3)
    '            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../Common/frmAuthorization.aspx")
    '            If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then btnEdit.Visible = False
    '            Dim dtTab As DataTable
    '            dtTab = Session("DefaultTab")
    '            If dtTab.Rows.Count > 0 Then
    '                uwOppTab.Tabs(0).Text = dtTab.Rows(0).Item("vcContact") & " Details"
    '            Else : uwOppTab.Tabs(0).Text = "Contact Details"
    '            End If
    '        End If
    '        btnLayout.Attributes.Add("onclick", "return ShowLayout('N','" & lngCntID & "','" & type.Text & "');")
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Sub LoadTableInformation()
    '    Try

    '        'Dim dtTablecust As DataTable
    '        'Dim ds As New DataSet
    '        'Dim objPageLayout As New CPageLayout
    '        'Dim check As String
    '        'Dim fields() As String
    '        'Dim idcolumn As String = ""
    '        'Dim count1 As Integer
    '        'Dim x As Integer

    '        Dim dtContactInfo As DataTable
    '        Dim objContact As New CContacts
    '        objContact.ContactID = lngCntID
    '        objContact.DomainID = Session("DomainID")
    '        objContact.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
    '        dtContactInfo = objContact.GetCntInfoForEdit1

    '        objContacts.GetContactInfoEditIP()

    '        lblRecordOwner.Text = dtContactInfo.Rows(0).Item("RecordOwner")
    '        lblCreatedBy.Text = dtContactInfo.Rows(0).Item("CreatedBy")
    '        lblModifiedBy.Text = dtContactInfo.Rows(0).Item("ModifiedBy")
    '        hplCustomer.Text = dtContactInfo.Rows(0).Item("vcCompanyName")

    '        hidCompName.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcCompanyName")), "", dtContactInfo.Rows(0).Item("vcCompanyName"))
    '        hidEml.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcGivenName")), "", dtContactInfo.Rows(0).Item("vcGivenName"))

    '        If Session("EnableIntMedPage") = 1 Then
    '            hplCustomer.NavigateUrl = "../pagelayout/frmCustAccountdtl.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=contactdetail&DivID=" & dtContactInfo.Rows(0).Item("numDivisionID") & "&CntID=" & lngCntID & "&frm1=" & GetQueryStringVal( "frm")
    '        Else : hplCustomer.NavigateUrl = "../account/frmCusAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=contactedit&DivID=" & dtContactInfo.Rows(0).Item("numDivisionID") & "&CntID=" & lngCntID & "&frm1=" & GetQueryStringVal( "frm")
    '        End If

    '        Dim strcharSex As String   'hold the sex of the contact
    '        strcharSex = IIf(IsDBNull(dtContactInfo.Rows(0).Item("charSex")), "M", dtContactInfo.Rows(0).Item("charSex"))

    '        If Trim(strcharSex) = "0" Or Trim(strcharSex) = "" Then strcharSex = "M"

    '        type.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("numContactType")), 0, dtContactInfo.Rows(0).Item("numContactType"))
    '        txtEmail.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcEmail")), "", dtContactInfo.Rows(0).Item("vcEmail"))

    '        LoadControls()

    '        'objPageLayout.CoType = "N"
    '        'objPageLayout.UserCntID = Session("UserContactID")
    '        'objPageLayout.RecordId = lngCntID
    '        'objPageLayout.DomainID = Session("DomainID")
    '        'objPageLayout.PageId = 4
    '        'objPageLayout.numRelCntType = IIf(IsDBNull(dtContactInfo.Rows(0).Item("numContactType")), 0, dtContactInfo.Rows(0).Item("numContactType"))
    '        'ds = objPageLayout.GetTableInfoDefault()  ' getting the table structure 
    '        'dtTableInfo = ds.Tables(0)

    '        ''If ds.Tables.Count = 2 Then
    '        ''    dtTablecust = ds.Tables(1)
    '        ''    dtTableInfo.Merge(dtTablecust)
    '        ''End If
    '        ''Dim dv As DataView
    '        ''dv = New DataView(dtTableInfo)

    '        'Dim numrows As Integer = dtTableInfo.Compute("Max(tintrow)", String.Empty)
    '        'Dim numcells As Integer = dtTableInfo.Compute("Max(intcoulmn)", String.Empty)
    '        'Dim i As Integer = 0
    '        'Dim nr As Integer
    '        'Dim noRowsToLoop As Integer
    '        'noRowsToLoop = (dtTableInfo.Rows.Count - (numrows * numcells)) / numcells
    '        'noRowsToLoop = noRowsToLoop + numrows + 1

    '        'For nr = 0 To noRowsToLoop
    '        '    Dim r As New TableRow()
    '        '    Dim nc As Integer
    '        '    Dim ro As Integer = nr
    '        '    For nc = 1 To numcells
    '        '        If dtTableInfo.Rows.Count <> i Then
    '        '            If dtTableInfo.Rows(i).Item("tintrow") = nr + 1 And dtTableInfo.Rows(i).Item("intcoulmn") = nc Then
    '        '                Dim column1 As New TableCell
    '        '                Dim column2 As New TableCell
    '        '                Dim fieldId As Integer
    '        '                Dim bitDynFld As String
    '        '                fieldId = CInt(dtTableInfo.Rows(i).Item("numFieldID").ToString)
    '        '                bitDynFld = dtTableInfo.Rows(i).Item("bitCustomField")
    '        '                column1.CssClass = "normal7"
    '        '                If (bitDynFld <> "1") Then
    '        '                    column1.Text = dtTableInfo.Rows(i).Item("vcFieldName").ToString & "&nbsp;:"
    '        '                    If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName").ToString) And dtTableInfo.Rows(i).Item("vcDBColumnName").ToString() <> "" Then
    '        '                        Dim temp As String
    '        '                        temp = dtTableInfo.Rows(i).Item("vcDBColumnName").ToString
    '        '                        fields = temp.Split(",")
    '        '                        Dim j As Integer
    '        '                        j = 0

    '        '                        While (j < fields.Length)
    '        '                            If (fieldId = "162") Then

    '        '                                Dim listvalue As String
    '        '                                Dim value As String = ""
    '        '                                listvalue = IIf(IsDBNull(dtContactInfo.Rows(0).Item(fields(j))), "-", dtContactInfo.Rows(0).Item(fields(j)))

    '        '                                Select Case fieldId   ' Must be a primitive data type
    '        '                                    Case 162
    '        '                                        If listvalue = "M" Then
    '        '                                            value = "Male"
    '        '                                        ElseIf listvalue = "F" Then
    '        '                                            value = "Female"
    '        '                                        Else : value = "-"
    '        '                                        End If
    '        '                                    Case Else
    '        '                                End Select
    '        '                                column2.Text = value
    '        '                            ElseIf (fieldId = "164") Then
    '        '                                If (fields(j) = "AssociateCountTo") Then
    '        '                                    Dim h As New HyperLink()
    '        '                                    h.CssClass = "hyperlink"
    '        '                                    h.Text = "To" & "(" & IIf(IsDBNull(dtContactInfo.Rows(0).Item("AssociateCountTo")), "", dtContactInfo.Rows(0).Item("AssociateCountTo")) & ")" & "/"
    '        '                                    h.Attributes.Add("onclick", "return OpenTo();")
    '        '                                    column2.Controls.Add(h)
    '        '                                ElseIf (fields(j) = "AssociateCountFrom") Then
    '        '                                    Dim h As New HyperLink()
    '        '                                    h.CssClass = "hyperlink"
    '        '                                    h.Text = "From" & "(" & IIf(IsDBNull(dtContactInfo.Rows(0).Item("AssociateCountFrom")), "", dtContactInfo.Rows(0).Item("AssociateCountFrom")) & ")"
    '        '                                    h.Attributes.Add("onclick", "return OpenFrom();")
    '        '                                    column2.Controls.Add(h)
    '        '                                End If
    '        '                            ElseIf (fieldId = "141" Or fieldId = "142" Or fieldId = "165") Then
    '        '                                Dim email As String = ""
    '        '                                Dim h As New HyperLink
    '        '                                ' h.NavigateUrl = "#"
    '        '                                h.CssClass = "hyperlink"
    '        '                                h.ID = "email" & fieldId
    '        '                                email = IIf(IsDBNull(dtContactInfo.Rows(0).Item(fields(j))), "-", dtContactInfo.Rows(0).Item(fields(j).ToString))
    '        '                                h.Text = email
    '        '                                h.Attributes.Add("onclick", "return fn_Mail('" & email & "','" & ConfigurationManager.AppSettings("EmailLink") & "','" & lngCntID & "');")
    '        '                                column2.Controls.Add(h)
    '        '                            ElseIf (fieldId = "163") Then
    '        '                                Dim l As New Label
    '        '                                l.CssClass = "cell"
    '        '                                If Not (IsDBNull(dtContactInfo.Rows(0).Item("bitOptOut"))) Then
    '        '                                    l.Text = IIf(dtContactInfo.Rows(0).Item("bitOptOut"), "a", "r")
    '        '                                Else : l.Text = "r"
    '        '                                End If
    '        '                                column2.Controls.Add(l)
    '        '                            Else : column2.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item(fields(j))), "-", dtContactInfo.Rows(0).Item(fields(j)))
    '        '                            End If
    '        '                            j += 1
    '        '                        End While
    '        '                    Else : column1.Text = ""
    '        '                    End If ' end of table cell2
    '        '                Else
    '        '                    If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Link" Then
    '        '                        column1.Text = "Custom Web Link :"
    '        '                        Dim h As New HyperLink()
    '        '                        h.CssClass = "hyperlink"
    '        '                        Dim url As String = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcURL")), "", dtTableInfo.Rows(i).Item("vcURL"))
    '        '                        url = url.Replace("RecordID", lngCntID)
    '        '                        h.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "-", dtTableInfo.Rows(i).Item("vcFieldName"))
    '        '                        h.Attributes.Add("onclick", "return fn_GoToURL('" & "http://" & url & "');")
    '        '                        column2.Controls.Add(h)
    '        '                    ElseIf IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Date Field" Then
    '        '                        column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
    '        '                        Dim strDate As String
    '        '                        strDate = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "", dtTableInfo.Rows(i).Item("vcDBColumnName"))
    '        '                        If strDate = "0" Then
    '        '                            strDate = ""
    '        '                        End If
    '        '                        If strDate <> "" Then
    '        '                            column2.Text = FormattedDateFromDate(strDate, Session("DateFormat"))
    '        '                        End If
    '        '                    Else
    '        '                        column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
    '        '                        If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Check box" Then
    '        '                            If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
    '        '                                Dim l As New Label
    '        '                                l.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "a", "r")
    '        '                                l.CssClass = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "cell1", "cell")
    '        '                                column2.Controls.Add(l)
    '        '                            End If
    '        '                        Else
    '        '                            If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
    '        '                                column2.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "-", dtTableInfo.Rows(i).Item("vcDBColumnName"))
    '        '                            End If
    '        '                        End If
    '        '                    End If
    '        '                End If

    '        '                column2.CssClass = "normal1"
    '        '                column1.HorizontalAlign = HorizontalAlign.Right
    '        '                column2.HorizontalAlign = HorizontalAlign.Left
    '        '                column1.Width = 250
    '        '                column2.Width = 300
    '        '                column2.ColumnSpan = 1
    '        '                column1.ColumnSpan = 1
    '        '                r.Cells.Add(column1)
    '        '                r.Cells.Add(column2)
    '        '                i += 1
    '        '            ElseIf dtTableInfo.Rows(i).Item("tintrow") = 0 And dtTableInfo.Rows(i).Item("intcoulmn") = 0 Then
    '        '                Dim column1 As New TableCell
    '        '                Dim column2 As New TableCell
    '        '                column1.CssClass = "normal7"
    '        '                If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Link" Then
    '        '                    column1.Text = "Custom Web Link :"
    '        '                    Dim h As New HyperLink()
    '        '                    h.CssClass = "hyperlink"
    '        '                    Dim url As String = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcURL")), "", dtTableInfo.Rows(i).Item("vcURL"))
    '        '                    url = url.Replace("RecordID", lngCntID)
    '        '                    h.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "-", dtTableInfo.Rows(i).Item("vcFieldName"))
    '        '                    h.Attributes.Add("onclick", "return fn_GoToURL('" & "http://" & url & "');")
    '        '                    column2.Controls.Add(h)
    '        '                ElseIf IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Date Field" Then
    '        '                    column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
    '        '                    Dim strDate As String
    '        '                    strDate = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "", dtTableInfo.Rows(i).Item("vcDBColumnName"))
    '        '                    If strDate = "0" Then
    '        '                        strDate = ""
    '        '                    End If
    '        '                    If strDate <> "" Then
    '        '                        column2.Text = FormattedDateFromDate(strDate, Session("DateFormat"))
    '        '                    End If
    '        '                Else
    '        '                    column1.Text = IIf(IsDBNull(dtTableInfo.Rows(i).Item("vcFieldName")), "", dtTableInfo.Rows(i).Item("vcFieldName")) & "&nbsp;:"
    '        '                    If IIf(IsDBNull(dtTableInfo.Rows(i).Item("fld_type")), "", dtTableInfo.Rows(i).Item("fld_type")) = "Check box" Then
    '        '                        If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
    '        '                            Dim l As New Label
    '        '                            l.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "a", "r")
    '        '                            l.CssClass = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = 1, "cell1", "cell")
    '        '                            column2.Controls.Add(l)
    '        '                        End If
    '        '                    Else
    '        '                        If Not IsDBNull(dtTableInfo.Rows(i).Item("vcDBColumnName")) Then
    '        '                            column2.Text = IIf(dtTableInfo.Rows(i).Item("vcDBColumnName") = "0", "-", dtTableInfo.Rows(i).Item("vcDBColumnName"))
    '        '                        End If
    '        '                    End If
    '        '                End If
    '        '                column2.CssClass = "normal1"
    '        '                column1.HorizontalAlign = HorizontalAlign.Right
    '        '                column2.HorizontalAlign = HorizontalAlign.Left
    '        '                column1.Width = 250
    '        '                column2.Width = 300
    '        '                column2.ColumnSpan = 1
    '        '                column1.ColumnSpan = 1
    '        '                r.Cells.Add(column1)
    '        '                r.Cells.Add(column2)
    '        '                i += 1
    '        '            Else
    '        '                Dim column1 As New TableCell
    '        '                Dim column2 As New TableCell
    '        '                column1.Text = ""
    '        '                r.Cells.Add(column1)
    '        '                r.Cells.Add(column2)
    '        '            End If
    '        '        End If
    '        '    Next nc
    '        '    tabledetail.Rows.Add(r)
    '        'Next nr

    '        'If uwOppTab.Tabs.Count > 1 Then
    '        '    Dim iItemcount As Integer
    '        '    iItemcount = uwOppTab.Tabs.Count
    '        '    While uwOppTab.Tabs.Count > 1
    '        '        uwOppTab.Tabs.RemoveAt(iItemcount - 1)
    '        '        iItemcount = iItemcount - 1
    '        '    End While
    '        'End If
    '        'i = 0
    '        'Dim ObjCus As New CustomFields
    '        'ObjCus.locId = 4
    '        'ObjCus.DomainID = Session("DomainID")
    '        'ObjCus.RelId = IIf(IsDBNull(dtContactInfo.Rows(0).Item("numContactType")), 0, dtContactInfo.Rows(0).Item("numContactType"))
    '        'ObjCus.RecordId = Session("UsercontactId")
    '        'dtTablecust = ObjCus.GetCustFlds.Tables(0)

    '        'If dtTablecust.Rows.Count > 0 Then
    '        '    Dim Tab As Tab
    '        '    'Dim pageView As PageView
    '        '    Dim aspTable As HtmlTable
    '        '    Dim Table As Table
    '        '    Dim tblcell As TableCell
    '        '    Dim tblRow As TableRow
    '        '    Dim objRow As HtmlTableRow
    '        '    Dim objCell As HtmlTableCell

    '        '    Dim k As Integer


    '        '    ' CustomField(Section)

    '        '    k = 0
    '        '    objRow = New HtmlTableRow
    '        '    ViewState("TabId") = dtTablecust.Rows(0).Item("TabId")
    '        '    ViewState("Check") = 0
    '        '    ViewState("FirstTabCreated") = 0
    '        '    For i = 0 To dtTablecust.Rows.Count - 1
    '        '        If dtTablecust.Rows(i).Item("TabId") <> 0 Then
    '        '            If ViewState("TabId") <> dtTablecust.Rows(i).Item("TabId") Or ViewState("FirstTabCreated") = 0 Then
    '        '                If ViewState("Check") <> 0 Then
    '        '                    aspTable.Rows.Add(objRow)
    '        '                    tblcell.Controls.Add(aspTable)
    '        '                    tblRow.Cells.Add(tblcell)
    '        '                    Table.Rows.Add(tblRow)
    '        '                    Tab.ContentPane.Controls.Add(Table)
    '        '                End If
    '        '                k = 0
    '        '                ViewState("Check") = 1
    '        '                '  If Not IsPostBack Then
    '        '                ViewState("FirstTabCreated") = 1
    '        '                ViewState("TabId") = dtTablecust.Rows(i).Item("TabId")
    '        '                Tab = New Tab
    '        '                Tab.Text = "&nbsp;&nbsp;" & dtTablecust.Rows(i).Item("tabname") & "&nbsp;&nbsp;"
    '        '                uwOppTab.Tabs.Add(Tab)
    '        '                'End If
    '        '                'pageView = New PageView
    '        '                aspTable = New HtmlTable
    '        '                Table = New Table
    '        '                Table.Width = Unit.Percentage(100)
    '        '                Table.BorderColor = System.Drawing.Color.FromName("black")
    '        '                Table.GridLines = GridLines.None
    '        '                Table.BorderWidth = Unit.Pixel(1)
    '        '                Table.Height = Unit.Pixel(300)
    '        '                Table.CssClass = "aspTable"
    '        '                tblcell = New TableCell
    '        '                tblRow = New TableRow
    '        '                aspTable.Width = "100%"
    '        '                tblcell.VerticalAlign = VerticalAlign.Top
    '        '                aspTable.Width = "100%"
    '        '                objRow = New HtmlTableRow
    '        '                objCell = New HtmlTableCell
    '        '                objCell.InnerHtml = "<br>"
    '        '                objRow.Cells.Add(objCell)
    '        '                aspTable.Rows.Add(objRow)
    '        '                objRow = New HtmlTableRow
    '        '            End If

    '        '            If k = 3 Then
    '        '                k = 0
    '        '                aspTable.Rows.Add(objRow)
    '        '                objRow = New HtmlTableRow
    '        '            End If
    '        '            objCell = New HtmlTableCell
    '        '            objCell.Align = "right"
    '        '            objCell.Width = 100
    '        '            objCell.Attributes.Add("class", "normal7")
    '        '            ' objCell.Text = dttablecust.Rows(i).Item("vcfieldName") & "&nbsp;:"
    '        '            If dtTablecust.Rows(i).Item("fld_type") <> "Frame" Then
    '        '                If dtTablecust.Rows(i).Item("fld_type") <> "Link" Then
    '        '                    objCell.InnerText = dtTablecust.Rows(i).Item("fld_label") & " :"
    '        '                End If
    '        '                objRow.Cells.Add(objCell)
    '        '            End If
    '        '            objCell = New HtmlTableCell
    '        '            objCell.Align = "Left"
    '        '            objCell.Attributes.Add("class", "normal1")
    '        '            If dtTablecust.Rows(i).Item("fld_type") = "Link" Then
    '        '                Dim h As New HyperLink()
    '        '                h.Text = IIf(IsDBNull(dtTablecust.Rows(i).Item("fld_label")), "-", dtTablecust.Rows(i).Item("fld_label"))
    '        '                'h.NavigateUrl = IIf(IsDBNull(dttablecust.Rows(i).Item("vcURL")), "", dttablecust.Rows(i).Item("vcURL"))
    '        '                h.CssClass = "hyperlink"
    '        '                Dim url As String = IIf(IsDBNull(dtTablecust.Rows(i).Item("vcURL")), "", dtTablecust.Rows(i).Item("vcURL"))
    '        '                url = url.Replace("RecordID", lngCntID)
    '        '                h.Attributes.Add("onclick", "fn_GoToURL('" & "http://" & url & "')")
    '        '                objCell.Controls.Add(h)
    '        '                ' CreateLink(objRow, objCell, dtTablecust.Rows(i).Item("fld_id"), dtTablecust.Rows(i).Item("vcURL"), lngCntid, dtTablecust.Rows(i).Item("fld_label"))
    '        '            ElseIf dtTablecust.Rows(i).Item("fld_type") = "Check box" Then
    '        '                If Not IsDBNull(dtTablecust.Rows(i).Item("Value")) Then
    '        '                    Dim l As New Label
    '        '                    l.Text = IIf(dtTablecust.Rows(i).Item("Value") = "1", "a", "r")
    '        '                    l.CssClass = IIf(dtTablecust.Rows(i).Item("Value") = "1", "cell1", "cell")
    '        '                    objCell.Controls.Add(l)
    '        '                ElseIf IsDBNull(dtTablecust.Rows(i).Item("Value")) Then
    '        '                    Dim l As New Label
    '        '                    l.Text = "r"
    '        '                    l.CssClass = "cell"
    '        '                    objCell.Controls.Add(l)
    '        '                End If
    '        '            ElseIf dtTablecust.Rows(i).Item("fld_type") = "Frame" Then
    '        '                objCell = New HtmlTableCell
    '        '                Dim strFrame As String
    '        '                Dim URL As String
    '        '                URL = dtTablecust.Rows(i).Item("vcURL")
    '        '                URL = URL.Replace("RecordID", lngCntID)
    '        '                strFrame = "<iframe src ='" & URL & "' width='100%' frameborder='0' height= '800px'></iframe>"
    '        '                objCell.Controls.Add(New LiteralControl(strFrame))
    '        '                objRow.Cells.Add(objCell)
    '        '            Else
    '        '                If Not IsDBNull(dtTablecust.Rows(i).Item("Value")) Then
    '        '                    objCell.InnerText = IIf(dtTablecust.Rows(i).Item("Value") = "0" Or dtTablecust.Rows(i).Item("Value") = Nothing, "-", dtTablecust.Rows(i).Item("Value"))
    '        '                ElseIf IsDBNull(dtTablecust.Rows(i).Item("Value")) Then
    '        '                    objCell.InnerText = "-"
    '        '                End If
    '        '            End If
    '        '            objRow.Cells.Add(objCell)

    '        '            k = k + 1
    '        '        End If
    '        '    Next
    '        '    If ViewState("Check") = 1 Then
    '        '        aspTable.Rows.Add(objRow)
    '        '        tblcell.Controls.Add(aspTable)
    '        '        tblRow.Cells.Add(tblcell)
    '        '        Table.Rows.Add(tblRow)
    '        '        Tab.ContentPane.Controls.Add(Table)
    '        '    End If
    '        'End If

    '        'If (dtContactInfo.Rows.Count > 0) Then
    '        '    Dim column1 As New TableCell
    '        '    Dim column2 As New TableCell
    '        '    Dim r As New TableRow
    '        '    column1.CssClass = "normal7"
    '        '    column2.CssClass = "normal1"
    '        '    column1.HorizontalAlign = HorizontalAlign.Right
    '        '    column2.HorizontalAlign = HorizontalAlign.Justify
    '        '    column2.ColumnSpan = 5
    '        '    Dim l As New Label
    '        '    l.CssClass = "normal7"
    '        '    l.Text = "Comments" & "&nbsp;:"
    '        '    column1.Controls.Add(l)
    '        '    column2.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("txtNotes")), "-", dtContactInfo.Rows(0).Item("txtNotes"))
    '        '    column1.Width = 150
    '        '    r.Cells.Add(column1)
    '        '    r.Cells.Add(column2)
    '        '    tableComment.Rows.Add(r)
    '        'End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Sub LoadControls()


    '    tabledetail.Controls.Clear()
    '    Dim ds As DataSet
    '    Dim objPageLayout As New CPageLayout
    '    Dim fields() As String

    '    objPageLayout.CoType = "C"
    '    objPageLayout.UserCntID = Session("UserContactId")
    '    objPageLayout.RecordId = lngCntID
    '    'objPageLayout.ContactID = lngCntID
    '    objPageLayout.DomainID = Session("DomainID")
    '    objPageLayout.PageId = 4
    '    objPageLayout.numRelCntType = objContacts.ContactType
    '    ds = objPageLayout.GetTableInfoDefault()  ' getting the table structure 

    '    dtTableInfo = ds.Tables(0)


    '    Dim tblRow As TableRow
    '    Const NoOfColumns As Short = 2
    '    Dim ColCount As Int32 = 0
    '    ''If intermediatory Page is enabled then Add the text "Name" to dropdown fields
    '    objPageControls.CreateTemplateRow(tabledetail)
    '    For Each dr As DataRow In dtTableInfo.Rows
    '        If boolIntermediatoryPage = True Then
    '            If Not IsDBNull(dr("vcPropertyName")) And dr("fld_type") = "Drop Down List Box" And dr("bitCustomField") = False Then
    '                dr("vcPropertyName") = dr("vcPropertyName") & "Name"
    '            End If
    '        End If

    '        If Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False Then
    '            dr("vcValue") = objContacts.GetType.GetProperty(dr("vcPropertyName")).GetValue(objContacts, Nothing)
    '        End If

    '        If ColCount = 0 Then
    '            tblRow = New TableRow
    '        End If
    '        If (dr("fld_type") = "Drop Down List Box" Or dr("fld_type") = "List Box") Then
    '            Dim dtData As DataTable

    '            If Not IsDBNull(dr("numListID")) Then
    '                If boolIntermediatoryPage = False Then
    '                    If dr("ListRelID") > 0 Then
    '                        objCommon.Mode = 3
    '                        objCommon.DomainID = Session("DomainID")
    '                        objCommon.PrimaryListItemID = objPageControls.GetPrimaryListItemID(dr("ListRelID"), dtTableInfo, objContacts)
    '                        objCommon.SecondaryListID = dr("numListId")
    '                        dtData = objCommon.GetFieldRelationships.Tables(0)
    '                    Else
    '                        dtData = objCommon.GetMasterListItems(dr("numListID"), Session("DomainID"))
    '                    End If
    '                End If
    '            ElseIf dr("numFieldId") = 23 Then
    '                If boolIntermediatoryPage = False Then
    '                    dtData = New DataTable
    '                    dtData.Columns.Add("Value")
    '                    dtData.Columns.Add("Text")
    '                    Dim dr1 As DataRow
    '                    dr1 = dtData.NewRow
    '                    dr1("Value") = "M"
    '                    dr1("Text") = "Male"
    '                    dtData.Rows.Add(dr1)

    '                    dr1 = dtData.NewRow
    '                    dr1("Value") = "F"
    '                    dr1("Text") = "Female"
    '                    dtData.Rows.Add(dr1)

    '                End If
    '            ElseIf dr("numFieldId") = 6 Then
    '                If boolIntermediatoryPage = False Then
    '                    objCommon.ContactID = lngCntID
    '                    objCommon.charModule = "C"
    '                    objCommon.GetCompanySpecificValues1()
    '                    dtData = objCommon.GetManagers(Session("DomainID"), lngCntID, objCommon.DivisionID)
    '                End If
    '            End If


    '            If boolIntermediatoryPage Then
    '                If dr("numFieldID") = 5 Then
    '                    Dim boolEnabled As Boolean = True
    '                    If objContacts.ContactType = 70 Then boolEnabled = False
    '                    objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData, boolEnabled:=boolEnabled)
    '                Else
    '                    objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData)
    '                End If
    '            Else
    '                Dim ddl As DropDownList
    '                If dr("numFieldID") = 5 Then
    '                    Dim boolEnabled As Boolean = True
    '                    If objContacts.ContactType = 70 Then boolEnabled = False
    '                    ddl = objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData, boolEnabled:=boolEnabled)
    '                Else
    '                    ddl = objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData)
    '                End If
    '                If CLng(dr("DependentFields")) > 0 And Not boolIntermediatoryPage Then
    '                    ddl.AutoPostBack = True
    '                    AddHandler ddl.SelectedIndexChanged, AddressOf PopulateDependentDropdown
    '                End If
    '            End If

    '        Else
    '            objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, RecordID:=lngCntID)
    '        End If


    '        ColCount = ColCount + 1
    '        If NoOfColumns = ColCount Then
    '            ColCount = 0
    '            tabledetail.Rows.Add(tblRow)
    '        End If
    '    Next

    '    If ColCount > 0 Then
    '        tabledetail.Rows.Add(tblRow)
    '    End If

    '    objPageControls.CreateComments(tabledetail, objContacts.Comments, boolIntermediatoryPage)
    'End Sub
    'Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    '    Try
    '        ''Response.Redirect("../Contacts/frmContactList.aspx")
    '        PageRedirect()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub PageRedirect()
    '    Try
    '        If GetQueryStringVal( "frm") = "OppList" Then
    '            Response.Redirect("../opportunity/frmCusOppList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
    '        ElseIf GetQueryStringVal( "frm") = "CaseList" Then
    '            Response.Redirect("../Cases/frmCusCaseList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
    '        ElseIf GetQueryStringVal( "frm") = "ProjectList" Then
    '            Response.Redirect("../Projects/frmCusProList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
    '        ElseIf GetQueryStringVal( "frm") = "Contactdtl" Then
    '            Response.Redirect("../pagelayout/frmCustContactdtl.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
    '        Else : Response.Redirect("../Contact/frmCustContactList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
    '    Try
    '        Response.Redirect("../Contact/frmCstContacts.aspx?frm=Contactdtl" & "&SI=" & uwOppTab.SelectedTabIndex & "&SI1=" & uwOppTab.SelectedTabIndex & "&SI2=" & SI1 & "&frm1=" & frm & "&frm2=" & frm1)
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub
    'Sub PopulateDependentDropdown(ByVal sender As Object, ByVal e As EventArgs)
    '    objPageControls.PopulateDropdowns(CType(sender, DropDownList), dtTableInfo, objCommon, uwOppTab, Session("DomainID"))
    'End Sub

End Class