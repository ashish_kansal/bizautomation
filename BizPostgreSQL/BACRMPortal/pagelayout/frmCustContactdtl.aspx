<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../common/topbar.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmCustContactdtl.aspx.vb"
    Inherits="BACRMPortal.frmCustContactdtl" %>

<%--<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <link rel="stylesheet" href="~/CSS/lists.css" type="text/css" />
    <title>Contacts</title>

    <script language="JavaScript" src="../include/DATE-PICKER.JS" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
        function openActionItem(a, b, c, d) {
            window.location.href = "../admin/actionitemdetails.aspx?frm=contactdetails&CommId=" + a + "&CaseId=" + b + "&CaseTimeId=" + c + "&CaseExpId=" + d;
            return false;
        }
        function OpenEmailMessage(a, b) {
            window.open("../contact/frmEmailMessage.aspx?Email=" + a + "&Date=" + b, '', 'width=750,height=525,status=no,titlebar=no,scrollbars=yes,top=110,left=250')
            return false;
        }
        function fn_GoToURL(varURL) {

            if ((varURL != '') && (varURL.substr(0, 7) == 'http://') && (varURL.length > 7)) {
                var LoWindow = window.open(varURL, "", "");
                LoWindow.focus();
            }
            return false;
        }
        function FillAddress(a) {
            if (document.all) {
                document.getElementById('lblStreet').innerText = a;
                document.getElementById('lblCity').innerText = "";
                document.getElementById('lblPostal').innerText = "";
                document.getElementById('lblState').innerText = "";
                document.getElementById('lblCountry').innerText = "";
            } else {
                document.getElementById('lblStreet').textContent = a;
                document.getElementById('lblCity').textContent = "";
                document.getElementById('lblPostal').textContent = "";
                document.getElementById('lblState').textContent = "";
                document.getElementById('lblCountry').innerText = "";
            }
            return false;
        }
        function ShowLayout(a, b, c) {
            window.open("../pagelayout/frmCustomisePageLayout.aspx?Ctype=" + a + "&type=" + c, '', 'toolbar=no,titlebar=no,width=800,height=500,top=200,scrollbars=yes,resizable=yes');
            return false;
        }
        function openAddress(CntID) {
            window.open("../contacts/frmContactAddress.aspx?pqwRT=" + CntID, '', 'toolbar=no,titlebar=no,top=300,width=850,height=350,scrollbars=no,resizable=no')
            return false;
        }
        function ShowWindow(Page, q, att) {
            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                //window.location.reload(true);
                return false;

            }

        }
        function fn_Mail(txtMailAddr, a, b) {
            if (txtMailAddr != '') {
                window.open('mailto:' + txtMailAddr);
            }

            return false;
        }
        function fn_SendMail(txtMailAddr) {
            if (txtMailAddr != '') {
                window.open('../common/callemail.aspx?LsEmail=' + txtMailAddr, 'mail');
            }
            return false;
        }
        function OpenTo() {
            window.open("../Admin/frmToCompanyAssociation.aspx", '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenFrom() {
            window.open("../Admin/frmFromCompanyAssociation.aspx", '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenLast10OpenAct(a) {
            window.open("../admin/DisplayActionItem.aspx?cntid=" + a + "&type=1", '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenLst10ClosedAct(a) {
            window.open("../admin/DisplayActionItem.aspx?cntid=" + a + "&type=2", '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function Disableddl() {
            if (Tabstrip2.selectedIndex == 0) {
                document.Form1.ddlOppStatus.style.display = "none";
            }
            else {
                document.Form1.ddlOppStatus.style.display = "";
            }
        }
        function Save() {
            if (document.Form1.txtFirstname.value == "") {
                alert("Enter First Name")
                document.Form1.txtFirstname.focus();
                return false;
            }
            if (document.Form1.txtLastName.value == "") {
                alert("Enter Last Name")
                document.Form1.txtLastName.focus();
                return false;
            }
            if (document.Form1.txtContactType.value == 70) {
                if (document.Form1.ddlType.value != 70) {
                    alert("You can't change the contact type 'Primary Contact' from this record, because there must be  one Primary Contact for every Organization record. Go to another contact record within the same Organization, and change its 'type' value to Primary Contact.")
                    document.Form1.ddlType.focus();
                    return false;
                }
            }
        }
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <menu1:webmenu ID="webmenu1" runat="server">
    </menu1:webmenu>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
        EnableViewState="true">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td>
                        <table bordercolor="black" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td class="tr1" align="center">
                                    <b>Record Owner : </b>
                                    <asp:Label ID="lblRecordOwner" runat="server" ForeColor="Black"></asp:Label>
                                </td>
                                <td class="td1" width="1" height="18">
                                </td>
                                <td class="tr1" align="center">
                                    <b>Created By : </b>
                                    <asp:Label ID="lblCreatedBy" runat="server" ForeColor="Black"></asp:Label>
                                </td>
                                <td class="td1" width="1" height="18">
                                </td>
                                <td class="tr1" align="center">
                                    <b>Last Modified By : </b>
                                    <asp:Label ID="lblModifiedBy" runat="server" ForeColor="Black"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td>
                    </td>
                    <td class="normal1" align="center">
                        Organization : <u>
                            <asp:HyperLink ID="hplCustomer" runat="server" CssClass="hyperlink"></asp:HyperLink>
                    </td>
                    <td align="right">
                        <asp:Button ID="btnEdit" runat="server" CssClass="button" Width="50" Text="Edit" />
                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close"></asp:Button>
                    </td>
                </tr>
            </table>
            </td> </tr>
            <tr>
                <td>
                    <igtab:UltraWebTab ImageDirectory="" ID="uwOppTab" runat="server" ThreeDEffect="True"
                        BorderStyle="Solid" Width="100%" BarHeight="0" BorderWidth="0">
                        <DefaultTabStyle Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Arial">
                        </DefaultTabStyle>
                        <RoundedImage LeftSideWidth="7" RightSideWidth="8" ShiftOfImages="0" SelectedImage="../images/ig_tab_winXPs3.gif"
                            NormalImage="../images/ig_tab_winXP3.gif" HoverImage="../images/ig_tab_winXPs3.gif"
                            FillStyle="LeftMergedWithCenter"></RoundedImage>
                        <SelectedTabStyle Height="23px" ForeColor="white">
                        </SelectedTabStyle>
                        <HoverTabStyle Height="23px" ForeColor="white">
                        </HoverTabStyle>
                        <Tabs>
                            <igtab:Tab Text="&nbsp;&nbsp;Contact Details&nbsp;&nbsp;">
                                <ContentTemplate>
                                    <asp:Table ID="tblContacts" runat="server" BorderWidth="1" Width="100%" GridLines="none"
                                        BorderColor="black" CssClass="aspTableDTL" Height="200">
                                        <asp:TableRow>
                                            <asp:TableCell VerticalAlign="top">
                                                <asp:Table Width="100%" ID="tbl12" runat="server">
                                                    <asp:TableRow>
                                                        <asp:TableCell HorizontalAlign="Right">
                                                            <asp:Button ID="btnLayout" runat="server" CssClass="button" Text="Layout"></asp:Button>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell>
                                                            <asp:Table runat="server" ID="tabledetail" BorderWidth="0" GridLines="none" CellPadding="2"
                                                                CellSpacing="0" HorizontalAlign="Center">
                                                            </asp:Table>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell>
                                                            <asp:Table runat="server" ID="tableComment" BorderWidth="" Width="100%" GridLines="none"
                                                                HorizontalAlign="Center">
                                                            </asp:Table>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>
                                            </asp:TableCell></asp:TableRow>
                                    </asp:Table>
                                </ContentTemplate>
                            </igtab:Tab>
                        </Tabs>
                    </igtab:UltraWebTab>
                </td>
            </tr>
            </table>
            <asp:TextBox ID="hidEml" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="hidCompName" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtContactType" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="type" Style="display: none" runat="server" Text="0"></asp:TextBox>
            <asp:TextBox runat="server" ID="rows" Style="display: none"></asp:TextBox>
            <asp:TextBox runat="server" ID="txtEmail" Style="display: none"></asp:TextBox>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
--%>