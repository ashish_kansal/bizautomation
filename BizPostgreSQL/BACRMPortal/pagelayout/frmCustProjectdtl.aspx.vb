Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Contract
Imports BACRM.BusinessLogic.Admin
Imports System.Reflection
Imports Telerik.Web.UI

Public Class frmCustProjectdtl
    Inherits BACRMPage
    Dim objContacts As CContacts
    Dim objOpportunity As MOpportunity
    Dim objProject As New ProjectIP
    Dim objCommon As New CCommon
    Dim SI As Integer = 0
    Dim SI1 As Integer = 0
    Dim SI2 As Integer = 0
    Dim frm As String = ""
    Dim frm1 As String = ""
    Dim frm2 As String = ""
    Dim myRow As DataRow
    Dim arrOutPut() As String = New String(2) {}
    Dim m_aryRightsForAssContacts(), m_aryRightsForPage(), m_aryRightsForContracts() As Integer
    Dim lngProID As Long
    Dim lngDivId As Long
    Dim strColumn As String
    Dim boolIntermediatoryPage As Boolean = False
    Dim dtCustomFieldTable, dtTableInfo, dtProjectProcess As DataTable
    Dim objPageControls As New PageControls
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not IsPostBack Then
                If Session("EnableIntMedPage") = 1 Then
                    ViewState("IntermediatoryPage") = True
                Else
                    ViewState("IntermediatoryPage") = False
                End If
            End If
            boolIntermediatoryPage = ViewState("IntermediatoryPage")

            If Not GetQueryStringVal( "SI") Is Nothing Then
                SI = GetQueryStringVal( "SI")
            End If
            If Not GetQueryStringVal( "SI1") Is Nothing Then
                SI1 = GetQueryStringVal( "SI1")
            Else : SI1 = 0
            End If
            If Not GetQueryStringVal( "SI2") Is Nothing Then
                SI2 = GetQueryStringVal( "SI2")
            Else : SI2 = 0
            End If
            If Not GetQueryStringVal( "frm") Is Nothing Then
                frm = ""
                frm = GetQueryStringVal( "frm")
            Else : frm = ""
            End If
            If Not GetQueryStringVal( "frm1") Is Nothing Then
                frm1 = ""
                frm1 = GetQueryStringVal( "frm1")
            Else : frm1 = ""
            End If
            If Not GetQueryStringVal( "SI2") Is Nothing Then
                frm2 = ""
                frm2 = GetQueryStringVal( "frm2")
            Else : frm2 = ""
            End If
            lngProID = Session("ProID")

            m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmProjects.aspx", Session("UserContactID"), 15, 11)

            If Not IsPostBack Then

                'm_aryRightsForContracts = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmCustContract.aspx", Session("userID"), 15, 15)

                'If m_aryRightsForContracts(RIGHTSTYPE.VIEW) = 0 Then
                '    pnlContract.Visible = False
                'Else
                '    pnlContract.Visible = True
                'End If

                If radOppTab.Tabs.Count > SI Then radOppTab.SelectedIndex = SI
                objContacts = New CContacts
                objContacts.RecID = lngProID
                objContacts.Type = "P"
                objContacts.UserCntID = Session("UserContactID")
                objContacts.AddVisiteddetails()
                'calFrom.SelectedDate = Session("StartDate")
                'calTo.SelectedDate = Session("EndDate")
                Session("Help") = Request.Url.Segments(Request.Url.Segments.Length - 1) ' = "Project"

                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AC")
                End If
                radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
            End If

            Milestone1._ModuleType = Milestone.ModuleType.Projects
            MileStone1.RightsForPage = m_aryRightsForPage
            Milestone1.ProjectId = lngProID
            MileStone1.DivisionID = lngDivId
            MileStone1.ProType = 1


            'ClientScript.RegisterClientScriptBlock(Me.GetType, "SetTab", "if (parent.parent.frames.length > 0) { parent.parent.frames['topframe'].SelectTabByValue('5');}else{ parent.frames['topframe'].SelectTabByValue('5'); } ", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub BindProjectIncomeAndExpense()
        Try
            Dim objProject As New Project
            Dim ds As DataSet
            objProject.DomainID = Session("DomainID")
            objProject.ProjectID = lngProID
            objProject.bytemode = CCommon.ToInteger(Session("ProjectIncomeExpenseType"))
            ds = objProject.GetProjectExpenseReports
            gvReports.DataSource = ds
            gvReports.DataBind()

            ddlReportsType.ClearSelection()
            ddlReportsType.SelectedValue = CCommon.ToInteger(Session("ProjectIncomeExpenseType"))

            Dim ReportTotalIncome, ReportTotalExpense, ReportBalance As Decimal
            ReportTotalIncome = CCommon.ToDecimal(ds.Tables(0).Compute("SUM(ExpenseAmount)", "iTEType=1"))
            ReportTotalExpense = CCommon.ToDecimal(ds.Tables(0).Compute("SUM(ExpenseAmount)", "iTEType=2 or iTEType=3 or iTEType=4 or iTEType=5"))
            ReportBalance = ReportTotalIncome - ReportTotalExpense

            lblReportTotalIncome.Text = String.Format("{0:##,#00.00}", ReportTotalIncome)
            lblReportTotalExpense.Text = String.Format("{0:##,#00.00}", ReportTotalExpense)
            lblReportBalance.Text = "<font color='" & IIf(ReportBalance > 0, "green", "red") & "'>" & String.Format("{0:##,#00.00}", ReportBalance) & "</font>"
            imgUPDown.ImageUrl = "../images/" & IIf(ReportBalance > 0, "BalanceUp.png", "BalanceDown.png")
            'Dim objProject As New Project
            'Dim ds As DataSet
            'objProject.DomainID = Session("DomainID")
            'objProject.ProjectID = lngProID
            'objProject.Mode = 0
            'ds = objProject.GetProjectIncomeExpense
            'If ds.Tables(0).Rows.Count > 0 Then
            '    lblBillableHours.Text = ds.Tables(0).Rows(0)("BillableHours")
            '    lblNonBillableHours.Text = ds.Tables(0).Rows(0)("NonBillableHours")
            '    lblIncome.Text = String.Format("{0:##,#00.00}", ds.Tables(0).Rows(0)("Income"))
            '    lblExpense.Text = String.Format("{0:##,#00.00}", ds.Tables(0).Rows(0)("Expense"))
            '    lblBalance.Text = "Balance: " & String.Format("{0:##,#00.00}", ds.Tables(0).Rows(0)("Income") - ds.Tables(0).Rows(0)("Expense"))
            '    If ds.Tables(0).Rows(0)("Income") > ds.Tables(0).Rows(0)("Expense") Then
            '        lblBalance.ForeColor = Color.Green
            '    Else
            '        lblBalance.ForeColor = Color.Red
            '    End If
            'End If

            'If ds.Tables(1).Rows.Count > 0 Then
            '    lblComments.Text = ds.Tables(1).Rows(0)("txtComments")
            '    If CCommon.ToLong(ds.Tables(1).Rows(0)("numContractId")) > 0 Then
            '        lblContract.Text = ds.Tables(1).Rows(0)("vcContractName")
            '        Dim objContract As New CContracts
            '        Dim dtTable As DataTable
            '        objContract.ContractID = ds.Tables(1).Rows(0)("numContractId")
            '        objContract.UserCntId = Session("UserContactId")
            '        objContract.DomainId = Session("DomainId")
            '        dtTable = objContract.GetContractDtl()
            '        If dtTable.Rows.Count > 0 Then
            '            lblAmountBalance.Text = IIf(IsDBNull(dtTable.Rows(0).Item("RemAmount")), 0, String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("RemAmount")))
            '            lblHours.Text = IIf(IsDBNull(dtTable.Rows(0).Item("RemHours")), 0, String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("RemHours")))
            '            lblDays.Text = IIf(IsDBNull(dtTable.Rows(0).Item("days")), 0, String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("days")))
            '            If dtTable.Rows(0).Item("bitincidents") = True Then
            '                lblIncidents.Text = IIf(IsDBNull(dtTable.Rows(0).Item("Incidents")), 0, dtTable.Rows(0).Item("Incidents"))
            '            Else : lblIncidents.Text = 0
            '            End If
            '        Else
            '            lblAmountBalance.Text = 0
            '            lblHours.Text = 0
            '            lblDays.Text = 0
            '            lblIncidents.Text = 0
            '        End If
            '    Else
            '        trContract.Visible = False
            '    End If
            'End If
            'If ds.Tables(2).Rows.Count > 0 Then
            '    ultraChartIncome.DataSource = ds.Tables(2)
            '    ultraChartIncome.DataBind()
            '    ultraChartIncome.Visible = True
            'Else
            '    ultraChartIncome.Visible = False
            'End If
            'If ds.Tables(3).Rows.Count > 0 Then
            '    ultraChartExpense.DataSource = ds.Tables(3)
            '    ultraChartExpense.DataBind()
            '    ultraChartExpense.Visible = True
            'Else
            '    ultraChartExpense.Visible = False
            'End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Sub BindProjectIncomeAndExpense()
    '    Try
    '        Dim objProject As New Project
    '        Dim ds As DataSet
    '        objProject.DomainID = Session("DomainID")
    '        objProject.ProjectID = lngProID
    '        objProject.Mode = 0
    '        ds = objProject.GetProjectIncomeExpense
    '        If ds.Tables(0).Rows.Count > 0 Then
    '            lblBillableHours.Text = ds.Tables(0).Rows(0)("BillableHours")
    '            lblNonBillableHours.Text = ds.Tables(0).Rows(0)("NonBillableHours")
    '            lblIncome.Text = String.Format("{0:##,#00.00}", ds.Tables(0).Rows(0)("Income"))
    '            lblExpense.Text = String.Format("{0:##,#00.00}", ds.Tables(0).Rows(0)("Expense"))
    '            lblBalance.Text = "Balance: " & String.Format("{0:##,#00.00}", ds.Tables(0).Rows(0)("Income") - ds.Tables(0).Rows(0)("Expense"))
    '            If ds.Tables(0).Rows(0)("Income") > ds.Tables(0).Rows(0)("Expense") Then
    '                lblBalance.ForeColor = Color.Green
    '            Else
    '                lblBalance.ForeColor = Color.Red
    '            End If
    '        End If

    '        If ds.Tables(1).Rows.Count > 0 Then
    '            lblComments.Text = ds.Tables(1).Rows(0)("txtComments")
    '            If CCommon.ToLong(ds.Tables(1).Rows(0)("numContractId")) > 0 Then
    '                lblContract.Text = ds.Tables(1).Rows(0)("vcContractName")
    '                Dim objContract As New CContracts
    '                Dim dtTable As DataTable
    '                objContract.ContractID = ds.Tables(1).Rows(0)("numContractId")
    '                objContract.UserCntId = Session("UserContactId")
    '                objContract.DomainId = Session("DomainId")
    '                dtTable = objContract.GetContractDtl()
    '                If dtTable.Rows.Count > 0 Then
    '                    lblAmountBalance.Text = IIf(IsDBNull(dtTable.Rows(0).Item("RemAmount")), 0, String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("RemAmount")))
    '                    lblHours.Text = IIf(IsDBNull(dtTable.Rows(0).Item("RemHours")), 0, String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("RemHours")))
    '                    lblDays.Text = IIf(IsDBNull(dtTable.Rows(0).Item("days")), 0, String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("days")))
    '                    If dtTable.Rows(0).Item("bitincidents") = True Then
    '                        lblIncidents.Text = IIf(IsDBNull(dtTable.Rows(0).Item("Incidents")), 0, dtTable.Rows(0).Item("Incidents"))
    '                    Else : lblIncidents.Text = 0
    '                    End If
    '                Else
    '                    lblAmountBalance.Text = 0
    '                    lblHours.Text = 0
    '                    lblDays.Text = 0
    '                    lblIncidents.Text = 0
    '                End If
    '            Else
    '                trContract.Visible = False
    '            End If
    '        End If
    '        If ds.Tables(2).Rows.Count > 0 Then
    '            ultraChartIncome.DataSource = ds.Tables(2)
    '            ultraChartIncome.DataBind()
    '            ultraChartIncome.Visible = True
    '        Else
    '            ultraChartIncome.Visible = False
    '        End If
    '        If ds.Tables(3).Rows.Count > 0 Then
    '            ultraChartExpense.DataSource = ds.Tables(3)
    '            ultraChartExpense.DataBind()
    '            ultraChartExpense.Visible = True
    '        Else
    '            ultraChartExpense.Visible = False
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub uwOppTab_TabClick(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebTab.WebTabEvent) Handles uwOppTab.TabClick
    '    If e.Tab.Key = "Report" Then
    '        calDateFrom.SelectedDate = DateTime.Now.AddYears(-2).Date
    '        calDateTo.SelectedDate = DateTime.Now.AddYears(2).Date
    '        BindProjectIncomeAndExpense()
    '        BindTimeLineChart()
    '    End If
    'End Sub
    'Sub BindTimeLineChart()
    '    Try
    '        Dim dt As DataTable
    '        Dim objAdmin As New CAdmin
    '        objAdmin.ProjectID = lngProID
    '        objAdmin.DomainID = Session("DomainID")
    '        objAdmin.UserCntID = Session("UserContactID")
    '        objAdmin.StartDate = calDateFrom.SelectedDate
    '        objAdmin.EndDate = calDateTo.SelectedDate
    '        dt = objAdmin.GetGanttChartData()

    '        If dt.Rows.Count > 0 Then
    '            ucTimeLine.Visible = True

    '            ucTimeLine.BackgroundImageFileName = Server.MapPath("~") & "\pagelayout\ChartImages\chart_gray_bg.jpg"

    '            ucTimeLine.Data.DataSource = dt
    '            ucTimeLine.Data.DataBind()

    '            ucTimeLine.GanttChart.ShowCompletePercentages = True
    '            ucTimeLine.GanttChart.ShowLinks = True
    '            ucTimeLine.GanttChart.ShowOwners = True

    '            ucTimeLine.GanttChart.ItemSpacing = 1
    '            ucTimeLine.GanttChart.SeriesSpacing = 1


    '            ucTimeLine.GanttChart.Columns.SeriesLabelsColumnIndex = 0
    '            ucTimeLine.GanttChart.Columns.ItemLabelsColumnIndex = 1
    '            ucTimeLine.GanttChart.Columns.StartTimeColumnIndex = 2
    '            ucTimeLine.GanttChart.Columns.EndTimeColumnIndex = 3
    '            ucTimeLine.GanttChart.Columns.IDColumnIndex = 4
    '            ucTimeLine.GanttChart.Columns.LinkToColumnIndex = 5
    '            ucTimeLine.GanttChart.Columns.PercentCompleteColumnIndex = 6
    '            ucTimeLine.GanttChart.Columns.OwnerColumnIndex = 7
    '        Else
    '            ucTimeLine.Visible = False
    '        End If

    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
    '    BindProjectIncomeAndExpense()
    '    BindTimeLineChart()
    'End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            If frm = "dashboard" Then
                Response.Redirect("../DashBoard/frmPortalDashBoard.aspx", False)
            Else
                Response.Redirect("../Projects/frmCusProList.aspx", False)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class


