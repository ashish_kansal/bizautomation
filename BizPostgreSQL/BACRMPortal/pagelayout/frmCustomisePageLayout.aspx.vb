Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Survey
Imports BACRM.BusinessLogic.Common
Imports Microsoft.Web.UI.WebControls

Partial Public Class frmCustomisePageLayout : Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                If Not (GetQueryStringVal( "type") = "") Then type.Text = GetQueryStringVal( "type")
                getMax()
                binddata()
            End If
            btnUpdate.Attributes.Add("onclick", "getSort()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Try
            savedata()
            binddata()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub getMax()
        Try
            Dim i As Integer
            Dim objlayout As New CcustPageLayout
            objlayout.DomainID = Session("domainId")
            objlayout.UserCntId = Session("UserContactID")
            objlayout.CoType = GetQueryStringVal( "Ctype").ToString
            Dim x As String
            x = objlayout.GetMaxRows()
            rows.Text = x
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub binddata()
        Try
            Dim contacttype As String = GetQueryStringVal( "Ctype").ToString
            Dim str As String

            Dim objCommon As New CCommon
            Dim dtddlContact As New DataTable
            If (contacttype = "c" Or contacttype = "N") Then
                dtddlContact = objCommon.GetMasterListItems(8, Session("DomainID"))
            ElseIf (contacttype = "A" Or contacttype = "P" Or contacttype = "L" Or contacttype = "z") Then
                dtddlContact = objCommon.GetMasterListItems(5, Session("DomainID"))
            End If

            Dim objlayout As New CcustPageLayout
            Dim dttable1 As New DataTable
            Dim i, count, x, r As Integer
            Dim pageid As Integer = 0

            str = "<link rel='stylesheet' href='~/CSS/master.css' type='text/css' /><link rel='stylesheet' href='~/CSS/lists.css' type='text/css' />"
            str = str & "<br><br><table cellSpacing='0' cellPadding='0' width='100%'><tr><td style='WIDTH: 25%' vAlign='bottom'><table class='TabStyle' borderColor='black' cellSpacing='0' border='0'>	<tr><td class='Text_bold_White' height='23'>&nbsp;&nbsp;&nbsp;Customise Pagelayout&nbsp;&nbsp;&nbsp;</td></tr></table></td>"
            Select Case contacttype
                Case "c", "N"
                    pageid = 4
                    str = str & "<td align='left' style='WIDTH: 30%' vAlign='bottom' class ='normal1'>Contact Type : <select class ='normal1' id='dlh' onchange='dlChange()'>"
                Case "A", "P", "L", "z"
                    pageid = 1
                    str = str & "<td align='left' style='WIDTH: 30%' vAlign='bottom' class ='normal1'>Relationship : <select class ='normal1' id='dlh' onchange='dlChange()'>"
                Case "S" : pageid = 3
                Case "T", "O", "d", "u"
                    If Session("OppType") = 1 Then
                        pageid = 2
                    Else : pageid = 6
                    End If
                Case "R", "j"
                    pageid = 11
            End Select
            If (contacttype = "c" Or contacttype = "A" Or contacttype = "P" Or contacttype = "L" Or contacttype = "N" Or contacttype = "z" Or contacttype = "R" Or contacttype = "j") Then
                If (type.Text = "0") Then
                    str = str & "<option value ='0' selected='selected'>--Select One--</option>"
                Else : str = str & "<option value ='0' >--Select One--</option>"
                End If

                For r = 0 To dtddlContact.Rows.Count - 1
                    If (type.Text = dtddlContact.Rows(r).Item("numListItemID").ToString) Then
                        str = str & "<option value ='" + dtddlContact.Rows(r).Item("numListItemID").ToString + "' selected='selected'>" + CStr(dtddlContact.Rows(r).Item("vcdata").ToString) + "</option>"
                    Else : str = str & "<option value ='" + dtddlContact.Rows(r).Item("numListItemID").ToString + "' >" + CStr(dtddlContact.Rows(r).Item("vcdata").ToString) + "</option>"
                    End If
                Next
                str = str & "</select></td>"
            End If

            str = str & "<td align='right' height='23'>&nbsp;&nbsp;&nbsp;<input class='button' id='btnColumn' style='width:100' onclick='AddColumn()' type='button' value='Add Column'>&nbsp;<input class='button' id='btnDeleteColumn1' style='width:100' onclick='DeleteColumn()' type='button' value='Delete Column'>&nbsp;<input class='button' id='btnsave1' style='width:50' onclick='show()' type='button' value='Save'>&nbsp;<input class='button' id='btnsave1' style='width:100' onclick='show1()' type='button' value='Save & Close'>&nbsp;<input class='button' id='btnClose' style='width:50' onclick='Close()' type='button' value='Close'>&nbsp;</td></tr></table>      "
            str = str & "<table border='1' style='border-color:Black;' class='aspTableDTL' cellpadding='0' cellspacing='0' width='100%' height='500px'><tr valign='top'><td>"
            str = str & "<br/><table align='center'valign='top' ><tr valign='top'><td class='hs' align='center'>Available Fields</td></tr><tr valign='top' border='1'>"
            x = 0
            Dim max As String
            max = rows.Text

            While (x <= max)
                objlayout.DomainID = Session("domainId")
                objlayout.UserCntId = Session("UserContactID")
                objlayout.ColumnID = x
                objlayout.CoType = GetQueryStringVal( "Ctype").ToString
                If (contacttype = "c" Or contacttype = "j" Or contacttype = "A" Or contacttype = "P" Or contacttype = "L" Or contacttype = "S" Or contacttype = "N" Or contacttype = "z" Or contacttype = "T" Or contacttype = "O" Or contacttype = "d" Or contacttype = "R") Then
                    Dim ds As New DataSet
                    Dim dttable2 As New DataTable
                    objlayout.PageId = pageid
                    objlayout.numRelation = CInt(type.Text)
                    ds = objlayout.getValuesWithddl()
                    If (ds.Tables.Count = 2) Then
                        dttable1 = ds.Tables(0)
                        dttable2 = ds.Tables(1)
                    Else : dttable1 = ds.Tables(0)
                    End If
                    If (dttable2.Rows.Count > 0) Then dttable1.Merge(dttable2)
                Else : dttable1 = objlayout.getValues()
                End If
                Dim dv As DataView
                If (contacttype = "c" Or contacttype = "j" Or contacttype = "A" Or contacttype = "P" Or contacttype = "L" Or contacttype = "S" Or contacttype = "N" Or contacttype = "z" Or contacttype = "T" Or contacttype = "O" Or contacttype = "d" Or contacttype = "R") Then

                    dv = New DataView(dttable1)
                    If (x <> 0) Then
                        dv.Sort = "tintRow"
                    ElseIf (x = 0) Then
                        dv.Sort = "vcFieldName"
                    End If
                    dv.Table.AcceptChanges()
                    i = 0
                    count = 0
                    count = dv.Table.Rows.Count
                    'count = dttable1.Rows.Count
                    str = str & "<td valign='top'>"
                    str = str & "<ul id='x" + x.ToString + "' class='sortable boxy'>"
                    While i < count
                        str = str & "<li id='" + dv(i).Item("numFieldId").ToString + "/" + dv(i).Item("bitCustomField").ToString + "' >" + dv(i).Item("vcFieldName") + "</li>"
                        i += 1
                    End While
                    Response.Write("</ul>")
                    Response.Write(" </td>")
                    x += 1
                    dttable1.Rows.Clear()
                    ' Response.Write(" </tr></table></td></tr></table>")
                Else
                    i = 0
                    count = 0
                    count = dttable1.Rows.Count
                    str = str & "<td valign='top'>"
                    str = str & "<ul id='x" + x.ToString + "' class='sortable boxy'>"
                    While i < count
                        str = str & "<li id='" + dttable1.Rows(i).Item("numFieldId").ToString + "' >" + dttable1.Rows(i).Item("vcFieldName") + "</li>"
                        i += 1
                    End While

                    str = str & "</ul>"
                    str = str & " </td>"
                    x += 1
                    dttable1.Rows.Clear()
                End If
            End While
            str = str & " </tr></table></td></tr></table>"
            Response.Write(str)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub savedata()
        Try
            Dim contacttype As String = GetQueryStringVal( "Ctype").ToString
            Dim container(3) As String
            Dim header(10) As String
            Dim headerdata(10) As String
            Dim data1XML As String
            Dim temp(10) As String
            Dim data1(2) As String
            Dim i As Integer = 1
            Dim j As Integer = 0

            Dim objDT As System.Data.DataTable
            Dim objDT1 As System.Data.DataTable
            Dim objDR As System.Data.DataRow
            objDT = New System.Data.DataTable("Table")
            objDT1 = New System.Data.DataTable("Table")

            objDT.Columns.Add("numFieldID", GetType(Integer))
            objDT.Columns.Add("tintRow", GetType(Integer))
            objDT.Columns.Add("intColumn", GetType(Integer))
            objDT.Columns.Add("numUserCntId", GetType(Integer))
            objDT.Columns.Add("numDomainID", GetType(Integer))
            objDT.Columns.Add("bitCustomField", GetType(Integer))
            objDT.Columns.Add("numRelCntType", GetType(Integer))
            objDT.Columns.Add("Ctype", GetType(Char))

            Dim ds As New DataSet
            Dim objlayout As New CcustPageLayout
            Dim data As String = order.Value
            container = data.Split(":")
            Dim max As String
            max = rows.Text
            While (i <= max)
                temp = container(i).Split("(")
                header(i) = temp(0)
                temp = temp(1).Split(")")
                headerdata = temp(0).Split(",")
                j = 0
                While (j < headerdata.Length)
                    If (headerdata(j) <> "") Then
                        objDR = objDT.NewRow
                        data1 = headerdata(j).Split("/")
                        objDR("numFieldID") = data1(0)
                        objDR("tintRow") = j + 1
                        objDR("intColumn") = i
                        objDR("numUserCntId") = Session("UserContactID")
                        objDR("numDomainID") = Session("domainId")
                        If (contacttype = "c" Or contacttype = "j" Or contacttype = "A" Or contacttype = "P" Or contacttype = "L" Or contacttype = "S" Or contacttype = "N" Or contacttype = "z" Or contacttype = "T" Or contacttype = "O" Or contacttype = "d" Or contacttype = "R") Then
                            If (data1(1) = "False" Or data1(1) = "0") Then
                                objDR("bitCustomField") = 0
                            ElseIf (data1(1) = "True" Or data1(1) = "1") Then
                                objDR("bitCustomField") = 1
                            End If
                            objDR("numRelCntType") = CInt(type.Text)
                        Else
                            objDR("numRelCntType") = 0
                            objDR("bitCustomField") = 0
                        End If
                        objDR("Ctype") = contacttype
                        objDT.Rows.Add(objDR)
                    End If
                    j += 1
                End While

                ds.Tables.Add(objDT)

                data1XML = ds.GetXml()
                objlayout.DomainID = Session("domainId")
                objlayout.UserCntId = Session("UserContactID")
                'objlayout.tintRow = i
                objlayout.ColumnID = i
                objlayout.CoType = GetQueryStringVal( "Ctype")
                objlayout.RowString = data1XML
                If (contacttype = "c" Or contacttype = "A" Or contacttype = "P" Or contacttype = "L" Or contacttype = "N" Or contacttype = "z") Then
                    objlayout.numRelCntType = CInt(type.Text)
                End If
                objlayout.SaveList()
                objDT.Rows.Clear()
                If ds.Tables.Count > 0 Then ds.Tables.Remove(ds.Tables(0))
                i += 1
            End While
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnAddColumn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddColumn.Click
        Try
            Dim max As String
            rows.Text = CStr(CInt(rows.Text) + 1)
            binddata()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnDeleteColumn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteColumn.Click
        Try
            rows.Text = CStr(CInt(rows.Text) - 1)
            savedata()
            binddata()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnBindData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBindData.Click
        Try
            binddata()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class