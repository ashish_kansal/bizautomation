<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmCustOppurtunitydtl.aspx.vb"
    Inherits="BACRMPortal.frmCustOppurtunitydtl" %>

<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../common/topbar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <link href="../css/lists.css" type="text/css" rel="STYLESHEET" />
    <title>Opportunities</title>

    <script language="javascript" type="text/javascript">
        function openTrackAsset(a, b) {
            window.open("../opportunity/frmTrackAsset.aspx?opId=" + a + "&DivId=" + b, '', 'toolbar=no,titlebar=no,left=300,top=200,width=800,height=500,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenConfSerItem(a) {
            window.open('../opportunity/frmAddSerializedItem.aspx?OppID=' + a, '', 'toolbar=no,titlebar=no,left=100,top=100,width=1000,height=700,scrollbars=yes,resizable=yes')
            return false;
        }
        function ShowlinkedProjects(a) {
            window.open("../opportunity/frmLinkedProjects.aspx?opId=" + a, '', 'toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function fn_GoToURL(varURL) {

            if ((varURL != '') && (varURL.substr(0, 7) == 'http://') && (varURL.length > 7)) {
                var LoWindow = window.open(varURL, "", "");
                LoWindow.focus();
            }
            return false;
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeletMsg() {
            var bln = confirm("You�re about to remove the Stage from this Process, all stage data will be deleted")
            if (bln == true) {
                return true;
            }
            else {
                return false;
            }
        }
        function OpenDocuments(a) {
            window.open("../Documents/frmSpecDocuments.aspx?Type=O&RecID=" + a, '', 'toolbar=no,titlebar=no,top=200,width=700,height=450,left=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function DealCompleted() {

            alert("This Deal will now be removed from the 'Open Deals' section, and will reside only in the 'Closed Deals' section within the Organization the deal is for. Except for BizDocs or any Projects that depend on BizDocs - Modifications to Deal Details, Milestones & Stages, Associated Contacts, and Products / Services, will no longer be allowed.")
            return false;
        }
        function CannotShip() {

            alert("You can't ship at this time because you don't have enough quantity on hand to support your shipment. Your options are to modify your order, or replenish inventory (to check inventory click on the edit link within the line item, then the value in the 'Products/Services' column):")
            return false;
        }
        function AlertMsg() {
            if (confirm("Please note that after a 'Received' or 'Shipped' request is executed, except for the BizDocs and any Projects that depend on BizDocs - Additional changes will not be permitted on this Deal (i.e. it will be frozen). Do you wish to continue ?")) {
                return true;
            }
            else {
                return false;
            }
        }
        function OpenImage(a) {
            window.open('../opportunity/frmFullImage.aspx?ItemCode=' + a, '', 'toolbar=no,titlebar=no,left=300, top=100,width=600,height=500,scrollbars=yes,resizable=yes');
            return false;
        }
        function Update(ddl, txt, txtPrice) {
            if (ddl.value == 0) {
                alert("Select Item")
                ddl.focus()
                return false;
            }
            if (txt.value == '') {
                alert("Enter Units")
                txt.focus()
                return false;
            }
            if (txtPrice.value == '') {
                alert("Enter Price")
                txtPrice.focus()
                return false;
            }
        }

        function AddPrice(a, b, c) {
            if (b == 1) {
                document.Form1.txtprice.value = a;
                return false;
            }
            else {
                document.getElementById(c).value = a;
                return false;
            }
        }
        function openCompetition(a) {
            document.getElementById('IframeComp').src = "../opportunities/frmCompetition.aspx?ItemCode=" + a;
            document.getElementById('divCompetition').style.visibility = "visible";
            return false;
        }

        function openUnit(a) {
            document.getElementById('IframeUnit').src = "../opportunities/frmUnitdtlsForItem.aspx?ItemCode=" + a;
            document.getElementById('divUnits').style.visibility = "visible";
            return false;
        }
        function openItem(a, b, c) {
            b = document.Form1.txtunits.value
            window.open('../opportunities/frmItemPriceRecommd.aspx?ItemCode=' + a + '&Unit=' + b + '&OppID=' + c, '', 'toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')
        }
        function OpenEPrice(a, b, c, d) {
            window.open('../opportunities/frmItemPriceRecommd.aspx?ItemCode=' + b.value + '&Unit=' + a.value + '&Type=Edit&txtName=' + c + '&OppID=' + d, '', 'toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function openEUnit(a) {
            document.getElementById('IframeUnit').src = "../opportunities/frmUnitdtlsForItem.aspx?ItemCode=" + a.value;
            document.getElementById('divUnits').style.visibility = "visible";
            return false;
        }
        function ShowWindow(Page, q, att) {

            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                return false;

            }
        }
        function CheckNumber(cint) {
            if (cint == 1) {
                if (!(window.event.keyCode > 47 && window.event.keyCode < 58 || window.event.keyCode == 44 || window.event.keyCode == 46)) {
                    window.event.keyCode = 0;
                }
            }
            if (cint == 2) {
                if (!(window.event.keyCode > 47 && window.event.keyCode < 58)) {
                    window.event.keyCode = 0;
                }
            }

        }
        function Save(cint) {

            if (cint == 1) {
                if (document.Form1.ddlCompanyName.value == 0) {
                    alert("Select Customer");
                    tsVert.selectedIndex = 0;
                    document.Form1.ddlCompanyName.focus();
                    return false;
                }
                if (document.Form1.ddlTaskContact.selectedIndex == 0) {
                    alert("Select Contact");
                    tsVert.selectedIndex = 0;
                    document.Form1.ddlTaskContact.focus();
                    return false;
                }
            }

            if (document.Form1.calDue_txtDate.value == '') {
                alert("Enter Due Date");
                tsVert.selectedIndex = 0;
                return false;
            }
            if (document.getElementById('dgItem') != null) {
                if (document.getElementById('dgItem').rows.length == 1) {
                    alert("Select an Item");
                    tsVert.selectedIndex = 3;
                    document.Form1.ddlItems.focus();
                    return false;
                }
            }
            if (document.getElementById('dgItem') == null) {
                alert("Select an Item");
                tsVert.selectedIndex = 3;
                document.Form1.ddlItems.focus();
                return false;
            }
            if (document.getElementById('chkDClosed') != null) {
                if (document.getElementById('chkDClosed').checked == true) {
                    if (document.Form1.ddlClReason.value == 0) {
                        alert("Select Conclusion Analysis")
                        tsVert.selectedIndex = 1;
                        document.Form1.ddlClReason.focus()
                        return false;
                    }
                }
            }
            if (document.getElementById('chkDlost') != null) {
                if (document.getElementById('chkDlost').checked == true) {
                    if (document.Form1.ddlClReason.value == 0) {
                        alert("Select Conclusion Analysis")
                        tsVert.selectedIndex = 1;
                        document.Form1.ddlClReason.focus()
                        return false;
                    }

                }
            }

        }
        function AddContact() {
            if (document.Form1.ddlcompany.value == 0) {
                alert("Select Customer");
                tsVert.selectedIndex = 2;
                document.Form1.ddlcompany.focus();
                return false;
            }
            if (document.Form1.ddlAssocContactId.value == 0) {
                alert("Select Contact");
                tsVert.selectedIndex = 2;
                document.Form1.ddlAssocContactId.focus();
                return false;
            }
            var str;
            for (i = 0; i < document.Form1.elements.length; i++) {
                if (i <= 9) {
                    str = '0' + (i + 1)
                }
                else {
                    str = i + 1
                }
                if (document.getElementById('dgContact_ctl' + str + '_txtContactID') != null) {
                    if (document.getElementById('dgContact_ctl' + str + '_txtContactID').value == document.Form1.ddlAssocContactId.value) {
                        alert("Associated contact is already added");
                        return false;
                    }
                }
            }

        }
        function AddItems() {
            if (document.Form1.ddlItems.value == 0) {
                alert("Select Item");
                tsVert.selectedIndex = 3;
                document.Form1.ddlItems.focus();
                return false;
            }
            if (document.Form1.txtunits.value == "") {
                alert("Enter Units");
                tsVert.selectedIndex = 3;
                document.Form1.txtunits.focus();
                return false;
            }
            if (document.Form1.txtprice.value == "") {
                alert("Enter Price");
                tsVert.selectedIndex = 3;
                document.Form1.txtprice.focus();
                return false;
            }
        }

        function deleteItem() {
            var bln;
            bln = window.confirm("Delete Seleted Row - Are You Sure ?")
            if (bln == true) {
                return true;
            }
            else {
                return false;
            }
        }

        function OpenBiz(a) {
            window.open('../opportunity/frmBizDocs.aspx?OpID=' + a, '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes');
            return false;
        }


        function OpenDependency(a, b, c, d) {
            window.open('../opportunities/frmOppDependency.aspx?OPPStageID=' + a + '&Opid=' + b + '&PerID=' + c + '&StgDtlId=' + d, '', 'toolbar=no,titlebar=no,left=500, top=300,width=500,height=200,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenExpense(a, b, c, d) {
            window.open('../opportunities/frmOppExpense.aspx?OPPStageID=' + a + '&Opid=' + b + '&PerID=' + c + '&StgDtlId=' + d, '', 'toolbar=no,titlebar=no,left=500, top=300,width=500,height=200,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenTime(a, b, c, d) {
            window.open('../opportunities/frmOppTime.aspx?OPPStageID=' + a + '&Opid=' + b + '&PerID=' + c + '&StgDtlId=' + d, '', 'toolbar=no,titlebar=no,left=500, top=300,width=650,height=300,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenSubStage(a, b, c, d) {
            window.open('../opportunities/frmOPPSubStagesaspx.aspx?OPPStageID=' + a + '&Opid=' + b + '&PerID=' + c + '&StgDtlId=' + d, '', 'toolbar=no,titlebar=no,left=500, top=300,width=500,height=200,scrollbars=yes,resizable=yes');
            return false;
        }
        function CheckBoxCon(a, b, c) {
            if (parseInt(c) == 1) {
                document.getElementById('chkStage~' + a + '~' + b).checked = true
            }
            else {
                document.getElementById('chkStage~' + a + '~' + b).checked = false
            }
        }
        function ValidateCheckBox(cint) {
            if (cint == 1) {
                if (document.getElementById('chkDClosed').checked == true) {
                    if (document.getElementById('chkDlost').checked == true) {
                        alert("The Deal is already Lost !")
                        document.getElementById('chkDClosed').checked = false
                        return false;
                    }

                }
            }
            if (cint == 2) {
                if (document.getElementById('chkDlost').checked == true) {
                    if (document.getElementById('chkDClosed').checked == true) {
                        alert("The Deal is already Closed !")
                        document.getElementById('chkDlost').checked = false
                        return false;
                    }
                    document.Form1.chkActive.checked = false;
                }
            }

        }
        function ShowWindow(Page, q, att) {
            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                return false;

            }

        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function openPurOpp(a) {

            window.open('../opportunities/frmSelectPurOpp.aspx?OppID=' + a, '', 'toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function ShowLayout(a, b) {
            window.open("../pagelayout/frmCustomisePageLayout.aspx?Ctype=" + a, '', 'toolbar=no,titlebar=no,width=800,height=500,top=200,scrollbars=yes,resizable=yes');
            return false;
        }
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <menu1:webmenu ID="webmenu1" runat="server"></menu1:webmenu>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="updatepanel1" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
        EnableViewState="true">
        <ContentTemplate>
            <table width="100%" align="center">
                <tr>
                    <td>
                        <table id="tblMenu" bordercolor="black" cellspacing="0" cellpadding="0" width="100%"
                            border="0" runat="server">
                            <tr>
                                <td class="tr1" align="center">
                                    <b>Record Owner: </b>
                                    <asp:Label ID="lblRecordOwner" runat="server" ForeColor="Black"></asp:Label>
                                </td>
                                <td class="td1" width="1" height="18">
                                </td>
                                <td class="tr1" align="center">
                                    <b>Created By: </b>
                                    <asp:Label ID="lblCreatedBy" runat="server" ForeColor="Black"></asp:Label>
                                </td>
                                <td class="td1" width="1" height="18">
                                </td>
                                <td class="tr1" align="center">
                                    <b>Last Modified By: </b>
                                    <asp:Label ID="lblLastModifiedBy" runat="server" ForeColor="Black"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td class="normal1" align="center">
                                    <asp:Label ID="lblCustomerType" runat="server"></asp:Label>
                                    <u>
                                        <asp:HyperLink ID="hplCustomer" runat="server" CssClass="hyperlink"></asp:HyperLink></u>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblDealCompletedDate" runat="server"
                                        CssClass="text"></asp:Label>
                                </td>
                                <td align="right">
                                    <asp:Button ID="btnTrackAsset" Visible="false" runat="server" CssClass="button" Text="Track As Customer Asset"
                                        Width="175" />
                                    <asp:Button ID="btnConfSerItems" runat="server" CssClass="button" Visible="false"
                                        Text="Configure Serialized Item"></asp:Button>
                                    <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" Width="50px">
                                    </asp:Button>
                                    <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close"></asp:Button>
                                    <asp:Button ID="btnActdelete" runat="server" CssClass="Delete" Text="X"></asp:Button>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <%--<tr>
                    <td>
                        <igtab:UltraWebTab ID="uwOppTab" runat="server" BorderWidth="0" ThreeDEffect="True"
                            BorderStyle="Solid" Width="100%" BarHeight="0">
                            <DefaultTabStyle Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Arial">
                            </DefaultTabStyle>
                            <RoundedImage LeftSideWidth="7" RightSideWidth="8" ShiftOfImages="0" SelectedImage="../images/ig_tab_winXPs3.gif"
                                NormalImage="../images/ig_tab_winXP3.gif" HoverImage="../images/ig_tab_winXPs3.gif"
                                FillStyle="LeftMergedWithCenter"></RoundedImage>
                            <SelectedTabStyle Height="23px" ForeColor="white">
                            </SelectedTabStyle>
                            <HoverTabStyle Height="23px" ForeColor="white">
                            </HoverTabStyle>
                            <Tabs>
                                <igtab:Tab Text="Opportunity Details">
                                    <ContentTemplate>
                                        <asp:Table ID="tblOppr" BorderWidth="1" runat="server" Width="100%" CssClass="aspTableDTL"
                                            BorderColor="black" GridLines="none" Height="200">
                                            <asp:TableRow>
                                                <asp:TableCell VerticalAlign="top">
                                                    <asp:Table Width="100%" ID="tbl12" runat="server">
                                                        <asp:TableRow>
                                                            <asp:TableCell ColumnSpan="2" HorizontalAlign="Right">
                                                                <asp:Button ID="btnLayout" runat="server" CssClass="button" Text="Layout"></asp:Button>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell VerticalAlign="Top">
						                                <img src="../images/Dart-32.gif" />
                                                            </asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:Table runat="server" ID="tabledetail" BorderWidth="0" GridLines="none" CellPadding="2"
                                                                    CellSpacing="0" HorizontalAlign="Center">
                                                                </asp:Table>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell ColumnSpan="2">
                                                                <asp:Table runat="server" ID="tableComment" BorderWidth="" Width="100%" GridLines="none"
                                                                    HorizontalAlign="Center">
                                                                </asp:Table>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                    </asp:Table>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </ContentTemplate>
                                </igtab:Tab>
                                <igtab:Tab Text="Milestones & Stages">
                                    <ContentTemplate>
                                        <asp:Table ID="tblMile" BorderWidth="1" CellPadding="0" CellSpacing="0" runat="server"
                                            Width="100%" CssClass="aspTableDTL" BorderColor="black" GridLines="None" Height="300">
                                            <asp:TableRow>
                                                <asp:TableCell VerticalAlign="Top" HorizontalAlign="left">
                                                    <br>
                                                    <table width="400">
                                                        <tr>
                                                            <td class="normal8" align="right">
                                                                Conclusion Reason :
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblClReason" runat="server" CssClass="normal1"></asp:Label>
                                                            </td>
                                                            <td class="normal8" align="right">
                                                                <font color="green">Total Progress :</font>
                                                            </td>
                                                            <td class="normal1">
                                                                <font color="green">
                                                                    <asp:Label ID="lblTProgress" runat="server"></asp:Label>&nbsp;%</font>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br />
                                                    <asp:Table ID="tblMilestone" runat="server" Width="100%" GridLines="none" CellSpacing="0">
                                                    </asp:Table>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </ContentTemplate>
                                </igtab:Tab>
                                <igtab:Tab Text="Associated Contacts">
                                    <ContentTemplate>
                                        <asp:Table ID="tblCont" BorderWidth="1" CellPadding="0" CellSpacing="0" runat="server"
                                            Width="100%" CssClass="aspTableDTL" BorderColor="black" GridLines="None" Height="300">
                                            <asp:TableRow>
                                                <asp:TableCell VerticalAlign="Top">
                                                    <br />
                                                    <asp:DataGrid ID="dgContact" runat="server" CssClass="dg" Width="100%" BorderColor="white"
                                                        AutoGenerateColumns="False">
                                                        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                                        <ItemStyle CssClass="is"></ItemStyle>
                                                        <HeaderStyle CssClass="hs"></HeaderStyle>
                                                        <Columns>
                                                            <asp:BoundColumn DataField="numContactId" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="bitPartner" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn HeaderText="Organization,Relationship" DataField="Company"></asp:BoundColumn>
                                                            <asp:BoundColumn HeaderText="First & Last Name" DataField="Name"></asp:BoundColumn>
                                                            <asp:BoundColumn HeaderText="Phone - Ext" DataField="Phone"></asp:BoundColumn>
                                                            <asp:BoundColumn HeaderText="Email" DataField="Email"></asp:BoundColumn>
                                                            <asp:BoundColumn HeaderText="Contact Role" DataField="ContactRole"></asp:BoundColumn>
                                                            <asp:TemplateColumn HeaderText="Share Opportunity via Partner Point ?">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblShare" runat="server" CssClass="cell" Font-Size="Large"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn Visible="false">
                                                                <HeaderTemplate>
                                                                    <asp:Button ID="btnHdeleteCnt" runat="server" CssClass="Delete" Text="X"></asp:Button>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtContactID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "numContactId") %>'>
                                                                    </asp:TextBox>
                                                                    <asp:Button ID="btnDeleteCnt" runat="server" CssClass="Delete" Text="X" CommandName="Delete">
                                                                    </asp:Button>
                                                                    <asp:LinkButton ID="lnkDeleteCnt" runat="server" Visible="false">
																    <font color="#730000">*</font></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                    <br>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </ContentTemplate>
                                </igtab:Tab>
                                <igtab:Tab Text="Product/Service">
                                    <ContentTemplate>
                                        <asp:Table ID="tblProducts" BorderWidth="1" CellPadding="0" CellSpacing="0" runat="server"
                                            CssClass="aspTableDTL" Width="100%" BorderColor="black" GridLines="None" Height="300">
                                            <asp:TableRow>
                                                <asp:TableCell ID="TableCell1" runat="server" VerticalAlign="Top">
                                                    <br>
                                                    <igtbl:UltraWebGrid Width="100%" DisplayLayout-AutoGenerateColumns="false" ID="ucItem"
                                                        DisplayLayout-AllowRowNumberingDefault="ByDataIsland" runat="server" Browser="Xml">
                                                        <DisplayLayout RowHeightDefault="20px" Version="3.00" SelectTypeRowDefault="Extended"
                                                            ViewType="Hierarchical" BorderCollapseDefault="Separate" Name="ucItem">
                                                            <HeaderStyleDefault VerticalAlign="Middle" Font-Size="8pt" Font-Names="Arial" BorderStyle="Solid"
                                                                HorizontalAlign="Left" ForeColor="white" BackColor="#52658C">
                                                                <Padding Left="5px" Right="5px"></Padding>
                                                                <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
                                                                </BorderDetails>
                                                            </HeaderStyleDefault>
                                                            <RowSelectorStyleDefault BackColor="White">
                                                            </RowSelectorStyleDefault>
                                                            <FrameStyle Width="100%" Cursor="Default" BorderWidth="3px" Font-Size="8pt" Font-Names="Arial"
                                                                BorderStyle="Double">
                                                            </FrameStyle>
                                                            <FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
                                                                <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
                                                                </BorderDetails>
                                                            </FooterStyleDefault>
                                                            <EditCellStyleDefault BorderWidth="0px" BorderStyle="None">
                                                            </EditCellStyleDefault>
                                                            <SelectedRowStyleDefault ForeColor="White" BackColor="#666666">
                                                            </SelectedRowStyleDefault>
                                                            <RowStyleDefault BorderWidth="1px" Font-Size="8pt" Font-Names="Arial" BorderColor="Gray"
                                                                BorderStyle="Solid" BackColor="White">
                                                                <Padding Left="5px" Right="5px"></Padding>
                                                                <BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
                                                            </RowStyleDefault>
                                                            <RowExpAreaStyleDefault BackColor="LightSteelBlue">
                                                            </RowExpAreaStyleDefault>
                                                        </DisplayLayout>
                                                        <Bands>
                                                            <igtbl:UltraGridBand AllowDelete="Yes" BaseTableName="Item" Key="Item">
                                                                <Columns>
                                                                    <igtbl:UltraGridColumn Hidden="true" Width="100%" IsBound="true" BaseColumnName="numoppitemtCode"
                                                                        Key="numoppitemtCode">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn Hidden="true" IsBound="true" BaseColumnName="numWarehouseItmsID"
                                                                        Key="numWareHouseItemID">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn Hidden="true" IsBound="true" BaseColumnName="numWareHouseID"
                                                                        Key="numWareHouseID">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn Hidden="true" IsBound="true" BaseColumnName="numItemCode"
                                                                        Key="numItemCode">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn HeaderText="Item" Width="15%" AllowUpdate="No" IsBound="true"
                                                                        BaseColumnName="vcItemName" Key="vcItemName">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn HeaderText="Warehouse" Width="15%" AllowUpdate="No" IsBound="true"
                                                                        BaseColumnName="Warehouse" Key="Warehouse">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn HeaderText="Type" Width="5%" AllowUpdate="No" IsBound="true"
                                                                        BaseColumnName="ItemType" Key="ItemType">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn CellMultiline="Yes" Width="20%" HeaderText="Desc" AllowUpdate="No"
                                                                        IsBound="true" BaseColumnName="vcItemDesc" Key="vcItemDesc">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn CellMultiline="Yes" Width="15%" HeaderText="Attributes" AllowUpdate="No"
                                                                        IsBound="true" BaseColumnName="Attributes" Key="Attributes">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn HeaderText="Unit/Hours" Width="10%" Format="###,##0.00" AllowUpdate="No"
                                                                        IsBound="true" BaseColumnName="numUnitHour" Key="numUnitHour">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn HeaderText="Unit Price" Width="10%" Format="###,##0.00" AllowUpdate="No"
                                                                        IsBound="true" BaseColumnName="monPrice" Key="monPrice">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn HeaderText="Amount" Width="10%" Format="###,##0.00" AllowUpdate="No"
                                                                        IsBound="true" BaseColumnName="monTotAmount" Key="monTotAmount">
                                                                    </igtbl:UltraGridColumn>
                                                                </Columns>
                                                            </igtbl:UltraGridBand>
                                                            <igtbl:UltraGridBand AllowDelete="No" AllowUpdate="No" BaseTableName="SerialNo" Key="SerialNo">
                                                                <Columns>
                                                                    <igtbl:UltraGridColumn Hidden="true" IsBound="true" BaseColumnName="numWarehouseItmsDTLID"
                                                                        Key="numWarehouseItmsDTLID">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn Hidden="true" IsBound="true" BaseColumnName="numoppitemtCode"
                                                                        Key="numoppitemtCode">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn HeaderText="Serial No" Key="vcSerialNo" IsBound="True" Width="200px"
                                                                        BaseColumnName="vcSerialNo">
                                                                        <Header Key="vcSerialNo" Caption="Serial No">
                                                                        </Header>
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn HeaderText="Attributes" Key="Attributes" Width="200px" BaseColumnName="Attributes">
                                                                    </igtbl:UltraGridColumn>
                                                                </Columns>
                                                            </igtbl:UltraGridBand>
                                                        </Bands>
                                                    </igtbl:UltraWebGrid>
                                                    <br>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </ContentTemplate>
                                </igtab:Tab>
                                <igtab:Tab Text="BizDocs">
                                    <ContentTemplate>
                                        <asp:Table ID="table5" runat="server" BorderWidth="1" Width="100%" BackColor="white"
                                            CellSpacing="0" CellPadding="0" BorderColor="black" GridLines="None" Height="300"
                                            CssClass="aspTable">
                                            <asp:TableRow>
                                                <asp:TableCell VerticalAlign="Top">
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <div id="divBizDocsDtl" runat="server">
                                                                    <iframe id="IframeBiz" frameborder="0" width="100%" scrolling="auto" runat="server"
                                                                        height="300"></iframe>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </ContentTemplate>
                                </igtab:Tab>
                            </Tabs>
                        </igtab:UltraWebTab>
                    </td>
                </tr>--%>
            </table>
            <table width="100%">
                <tr>
                    <td class="normal4" align="center">
                        <asp:Literal ID="litMessage" runat="server"></asp:Literal>
                    </td>
                </tr>
            </table>
            <asp:TextBox ID="txtName" runat="server" Style="display: none"></asp:TextBox>
            <asp:Label ID="lblAmount" runat="server" Style="display: none"></asp:Label>
            <asp:TextBox ID="txtrecOwner" Style="display: none" runat="server"></asp:TextBox>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
