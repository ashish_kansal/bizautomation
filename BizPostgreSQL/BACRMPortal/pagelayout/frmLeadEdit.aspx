<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmLeadEdit.aspx.vb" Inherits="BACRMPortal.frmLeadEdit" %>
<%@ Register Assembly="Infragistics35.WebUI.UltraWebTab.v8.1, Version=8.1.20081.2046, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebTab" TagPrefix="igtab" %>
<%@ Register TagPrefix="menu1" TagName="PartnerPoint" src="../Common/PartnerPoint.ascx" %>  
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Infragistics35.WebUI.UltraWebGrid.v8.1, Version=8.1.20081.2046, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1"    runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Lead Details</title>
        <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
         <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)"> 
		<LINK href="../css/lists.css" type="text/css" rel="STYLESHEET">
		
		<script language="javascript">
			function DeleteRecord()
				{
					if(confirm('Are you sure, you want to delete the selected record?'))
					{
						return true;
					}
					else
					{
						return false;
					}
				}
		function DeleteMessage()
		    {
			    alert("You Are not Authorized to Delete the Selected Record !");
			    return false;
		    }
		function OpenTransfer(url)
		{
			window.open(url,'',"width=340,height=150,status=no,top=100,left=150");
			return false;	
		}
		function  OpenEmailMessage(a,b)
		{
			window.open("../contact/frmEmailMessage.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Email="+a+"&Date="+b,'','width=750,height=525,status=no,titlebar=no,scrollbars=yes,top=110,left=250')
			return false;
		}
		function ShowWindow(Page,q,att) 
		{
		
			if (att=='show')
			{
				document.all[Page].style.visibility = "visible";
				return false;
		
			}
			if (att=='hide')
			{
				document.all[Page].style.visibility = "hidden";
				return false;
		
			}
		}
	
		
		function openFollow(a)
		{
		    window.open("../Leads/frmFollowUpHstr.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&rtyWR="+a ,'','toolbar=no,titlebar=no,top=300,width=700,height=200,left=200,scrollbars=yes,resizable=yes')
//			document.all['cntdoc'].src="../Leads/frmFollowUpHstr.aspx?Div="+a ;
//			document.all['divFollow'].style.visibility = "visible";
			return false;
		}
		function OpenAdd(a)
		{
			window.open("../prospects/frmProspectsAdd.aspx?pwer=dfsdfdsfdsfdsf&rtyWR="+a,'','toolbar=no,titlebar=no,top=300,width=700,height=300,scrollbars=no,resizable=no')
			return false;
		}
			function fn_GoToURL(varURL)
		{
			var url = document.getElementById('uwOppTab$_ctl0$'+varURL).value
			if ((url!='') && (url.substr(0,7)=='http://') && (url.length > 7))
			{
				var LoWindow=window.open(url,"","");
				LoWindow.focus();
			}
			return false;
		}
		
		function FillAddress(a)
		{
		 document.getElementById('uwOppTab__ctl0_lblAddress').innerText=a;
		 return false;
		}
		function fn_SendMail(a)
		{ 
			if(document.Form1.uwOppTab$_ctl0$txtEmail.value!='')
				{
				window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&LsEmail=' + document.Form1.uwOppTab$_ctl0$txtEmail.value+'&pqwRT='+a,'','toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')
				}
				return false;
		}
			function CheckNumber()
					{
							if (!(window.event.keyCode > 47 && window.event.keyCode < 58))
							{
								window.event.keyCode=0;
							}
					}
		function GoOrgDetails(numDivisionId)
		{
			frames['IfrOpenOrgContact'].document.getElementById('hdRedirectionEntity').value='OrganizationFromDiv';
			frames['IfrOpenOrgContact'].document.getElementById('hdDivisionId').value = numDivisionId;
			frames['IfrOpenOrgContact'].document.forms['frmOrgContactRedirect'].submit();
		}
		function CheckTabSel(a)
		{
		    if (document.getElementById('uwOppTab').selectedIndex==4&&a==0)
		    {
		        document.Form1.submit()
		    }
		    return false;
		}
		function Save()
		{
		   if (document.Form1.uwOppTab$_ctl0$ddlGroup.value==0)
		   {
		    alert("Please Select Group")
		    document.Form1.uwOppTab$_ctl0$ddlGroup.focus();
		    return false;
		   } 
		}
		</script>
	</HEAD>
	<body>
		
		<form id="Form1" method="post" runat="server">
				<asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
		
  <menu1:PartnerPoint id="PartnerPoint1" runat="server"></menu1:PartnerPoint>
        
		<asp:updatepanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" ><ContentTemplate>
			<table width="100%" align="center">
				<tr>
					<td>
						<table id="tblMenu" borderColor="black" cellspacing="0" cellpadding="0" width="100%" border="0"
							runat="server">
							<tr>
								<td class="tr1" align="center"><b>Record Owner: </b>
									<asp:label id="lblRecordOwner" runat="server" ForeColor="Black"></asp:label></td>
								<td class="td1" width="1" height="18"></td>
								<td class="tr1" align="center"><b>Created By: </b>
									<asp:label id="lblCreatedBy" runat="server" ForeColor="Black"></asp:label></td>
								<td class="td1" width="1" height="18"></td>
								<td class="tr1" align="center"><b>Last Modified By: </b>
									<asp:label id="lblLastModifiedBy" runat="server" ForeColor="Black"></asp:label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%">
							<tr>
								<td class="normal1">Organization ID :
									<asp:label id="lblCustID" runat="Server"></asp:label></td>
								<td class="normal1">
									<asp:label id="lblAssociation" runat="Server"></asp:label>
									<iframe id="IfrOpenOrgContact" src="../Marketing/frmOrgContactRedirect.aspx" frameBorder="0"
										width="10" scrolling="no" height="10" left="0" right="0"></iframe>
								</td>
								<td align="right">
									
									<asp:button id="btnSave" Text="Save" Runat="server" CssClass="button"></asp:button>
									<asp:button id="btnSaveClose" Runat="server" CssClass="button" text="Save &amp; Close"></asp:button>
									<asp:button id="btnCancel" Text="Close" Runat="server" CssClass="button"></asp:button>
									<asp:Button ID="btnActdelete" Runat="server" CssClass="Delete" Text="r" ></asp:Button>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
					<igtab:ultrawebtab   ImageDirectory=""  id="uwOppTab" runat="server" ThreeDEffect="True" BorderStyle="Solid" Width="100%" BarHeight="0"  BorderWidth="0">
                       <DefaultTabStyle Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Arial" >
                        </DefaultTabStyle>
                        <RoundedImage LeftSideWidth="7" RightSideWidth="8" ShiftOfImages="0" SelectedImage="../images/ig_tab_winXPs3.gif" NormalImage="../images/ig_tab_winXP3.gif" HoverImage="../images/ig_tab_winXPs3.gif" FillStyle="LeftMergedWithCenter" ></RoundedImage>
                     <SelectedTabStyle Height="23px"  ForeColor="white">
                        </SelectedTabStyle>
                        <HoverTabStyle Height="23px" ForeColor="white"></HoverTabStyle>
                        <Tabs>
                             <igtab:Tab Text="&nbsp;&nbsp;Lead Details&nbsp;&nbsp;" >
                                <ContentTemplate>
                           				
								<asp:table id="Table5" Runat="server" BorderWidth="1" Width="100%" Height="300" BorderColor="black" CssClass="aspTableDTL" 
									GridLines="None">
									<asp:TableRow>
										<asp:TableCell VerticalAlign="Top">
											<br>
											<table width="100%" id="tblDetails" runat="server">
												<tr>
													<td colspan="2">
														<table>
															<tr>
															<td rowspan="30" valign="top" >
									                                <img src="../images/Building-48.gif" />
											                    </td>
																<td class="normal1" align="right">
																	Organization</td>
																<td>
																	<asp:textbox id="txtCompanyName" CssClass="signup" Runat="server" Width="180px"></asp:textbox></td>
															</tr>
															<tr>
																<td class="normal1" align="right">
																	</td>
																<td>
																	</td>
															</tr>
															<tr>
																<td class="normal1" align="right">Phone/Fax</td>
																<td>
																	<asp:textbox id="txtComPhone" Runat="server" CssClass="signup" Width="90" TabIndex="7"></asp:textbox>&nbsp;
																	<asp:textbox id="txtComFax" Runat="server" CssClass="signup" Width="90" TabIndex="8"></asp:textbox></td>
															</tr>
															<tr>
																<td class="normal1" align="right">
																	Profile</td>
																<td>
																	<asp:dropdownlist id="ddlProfile" CssClass="signup" Runat="server" Width="180px"></asp:dropdownlist></td>
															</tr>
															<tr>
																<td class="normal1" align="right">
																	Relationship</td>
																<td>
																	<asp:dropdownlist id="ddlRelationhip" Runat="server" AutoPostBack="True" CssClass="signup" Width="180"></asp:dropdownlist></td>
															</tr>
															<tr>
																<td class="normal1" align="right">
																	Campaign</td>
																<td>
																	<asp:dropdownlist id="ddlCampaign" runat="Server" CssClass="signup" Width="180px"></asp:dropdownlist></td>
															</tr>
																<tr>
																<td class="normal1" align="right">
																	Territory</td>
																<td>
																	<asp:dropdownlist id="ddlTerritory" CssClass="signup" Runat="server" Width="180px"></asp:dropdownlist></td>
															</tr>
														</table>
													</td>
													<td colspan="2">
														<asp:table id="Table7" Runat="server" BorderWidth="1" Width="100%" BorderColor="black" GridLines="None" >
															<asp:TableRow>
																<asp:TableCell VerticalAlign="Top">
																	<table>
																		<tr>
																			<td class="normal1" align="right" nowrap>
																				First/Last Name<FONT color="red">*</FONT></td>
																			<td>
																				<asp:textbox id="txtFirstname" CssClass="signup" Runat="server" Width="90"></asp:textbox>&nbsp;
																				<asp:textbox id="txtLastName" CssClass="signup" Runat="server" Width="90"></asp:textbox>
																			</td>
																		</tr>
																		<tr>
																			<td class="normal1" align="right">
																				Phone/Ext</td>
																			<td>
																				<asp:textbox id="txtPhone" CssClass="signup" Runat="server" Width="130"></asp:textbox>&nbsp;
																				<asp:textbox id="txtExt" CssClass="signup" Runat="server" Width="50px"></asp:textbox>
																			</td>
																		</tr>
																		<tr>
																			<td class="normal1" align="right">
																				Email</td>
																			<td>
																				<asp:textbox id="txtEmail" CssClass="signup" Runat="server" Width="145px"></asp:textbox>&nbsp;
																				<asp:button id="btnEmailGo" CssClass="button" Text="Go" Runat="server" Width="25"></asp:button></td>
																		</tr>
																		<tr>
																			<td class="normal1" align="right">
																				Position</td>
																			<td>
																				<asp:dropdownlist id="ddlPosition" CssClass="signup" Runat="server" Width="180"></asp:dropdownlist></td>
																		</tr>
																		<tr>
																			<td class="normal1" align="right">Title</td>
																			<td>
																				<asp:textbox id="txtTitle" runat="server" Width="180" TabIndex="20" CssClass="signup"></asp:textbox></td>
																		</tr>
																		<tr>
																			<td>
																			</td>
																			<td>
																				<asp:Button ID="btnContactdetails" Runat="server" CssClass="button" Text="Contact Details"></asp:Button>
																			</td>
																		</tr>
																	</table>
																</asp:TableCell>
															</asp:TableRow>
														</asp:table>
													</td>
													<td colspan="2">
														<table>
														<tr>
																<td class="normal1" align="right">Assigned To</td>
																<td class="normal1">
																	<asp:dropdownlist id="ddlAssignedTo" CssClass="signup" Runat="server" Width="180px"></asp:dropdownlist>
																</td>
															</tr>
															<tr>
																<td class="normal1" align="right">
																	Employees</td>
																<td>
																	<asp:dropdownlist id="ddlNoOfEmp" CssClass="signup" Runat="server" Width="180px"></asp:dropdownlist></td>
															</tr>
															<tr>
																<td class="normal1" align="right">
																	Annual Revenue</td>
																<td>
																	<asp:dropdownlist id="ddlAnnualRev" CssClass="signup" Runat="server" Width="180px"></asp:dropdownlist></td>
															</tr>
															<tr>
																<td class="normal1" align="right">
																	Web URL</td>
																<td>
																	<asp:textbox id="txtWebURL" text="http://" CssClass="signup" Runat="server" Width="145px"></asp:textbox>&nbsp;
																	<asp:button id="btnWebGo" CssClass="button" Text="Go" Runat="server" Width="25"></asp:button></td>
															</tr>
															<tr>
																<td class="normal1" align="right">
																	Follow-up Status</td>
																<td class="normal1">
																	<asp:dropdownlist id="ddlFollow" CssClass="signup" Runat="server" Width="180px"></asp:dropdownlist>
																	<asp:HyperLink ID="hplFollowUpHstr" Runat="server" CssClass="hyperlink">
																		Hstr</asp:HyperLink>
																	</td>
																<td></td>
																</tr> 
															
															<tr>
																<td class="normal1" align="right">
																	Group<FONT color="red">*</FONT></td>
																<td>
																	<asp:dropdownlist id="ddlGroup" CssClass="signup" Runat="server" Width="180px"></asp:dropdownlist></td>
															</tr>
															<tr>
																<td class="normal1" align="right">
																	Info. Source</td>
																<td>
																	<asp:dropdownlist id="ddlInfoSource" CssClass="signup" Runat="server" Width="180px"></asp:dropdownlist></td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
											<table width="100%">
												<tr>
													<td class="normal1" align="right">
														<asp:HyperLink id="hplAddress" Runat="server" CssClass="hyperlink">
															Address</asp:HyperLink></td>
													<td class="normal1" colspan="5">
														<asp:label id="lblAddress" runat="server" Width="100%" text=""></asp:label></td>
												</tr>
												<tr>
													<td class="normal1" align="right" vAlign="top" width="100">
														Comments</td>
													<td colspan="5">
														<asp:TextBox ID="txtComments" Runat="server" CssClass="signup" height="50" Width="400" TextMode="MultiLine"></asp:TextBox></td>
												</tr>
											</table>
										</asp:TableCell>
									</asp:TableRow>
								</asp:table>
							     </ContentTemplate>
                             </igtab:Tab>                             
                             <igtab:Tab Text="&nbsp;&nbsp;Areas of Interest&nbsp;&nbsp;" >
                                <ContentTemplate>
                               
								<asp:table id="Table2" Runat="server" BorderWidth="1" Width="100%" GridLines="None" BorderColor="black" CssClass="aspTable"
									Height="300">
									<asp:TableRow>
										<asp:TableCell VerticalAlign="Top" CssClass="normal1">
											<br>
											<asp:CheckBoxList ID="chkAOI" CellSpacing="20" runat="server" RepeatColumns="3" RepeatDirection="Vertical" ></asp:CheckBoxList>
										</asp:TableCell>
									</asp:TableRow>
								</asp:table>
						 </ContentTemplate>
                             </igtab:Tab>
                         </Tabs>                         
                         </igtab:ultrawebtab>	</td>
				</tr>
			</table>
			
			
			<table width="100%">
				<tr>
					<td class="normal4" align="center"><asp:literal id="litMessage" Runat="server"></asp:literal></td>
				</tr>
			</table>
			<asp:textbox id="txtTotalPage" style="DISPLAY: none" Runat="server"></asp:textbox>
			<asp:textbox id="txtTotalRecords" style="DISPLAY: none" Runat="server"></asp:textbox>
			<asp:TextBox ID="txtEmailTotalPage" Runat="server" style="DISPLAY:none"></asp:TextBox>
			<asp:TextBox ID="txtEmailTotalRecords" Runat="server" style="DISPLAY:none"></asp:TextBox>
			</ContentTemplate>
			</asp:updatepanel>
		
		</form>
	</body>
</HTML>
