<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmProspectDtl.aspx.vb" Inherits="BACRMPortal.frmProspectsDTL"%>
<%@ Register Assembly="Infragistics35.WebUI.UltraWebTab.v8.1, Version=8.1.20081.2046, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebTab" TagPrefix="igtab" %>
<%@ Register TagPrefix="menu1" TagName="PartnerPoint" src="../Common/PartnerPoint.ascx" %>  
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" src="../include/calandar.ascx" %>
<%@ Register Assembly="Infragistics35.WebUI.UltraWebGrid.v8.1, Version=8.1.20081.2046, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1"    runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
       <link href="../css/lists.css" type="text/css" rel="stylesheet"/>
		<title>Prospects</title>
		<script language="JavaScript" src="../javascript/date-picker.js" type="text/javascript"></script>
		<script language="javascript" type="text/javascript">
		function openActionItem(a,b,c,d,e,f)
		{
		    if (e=='Email')
		    {
		        window.open("../contact/frmEmailMessage.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Email="+a+"&Date="+f,'','width=750,height=525,status=no,titlebar=no,scrollbars=yes,top=110,left=250')
			    return false;
		    }
		    else
		    {
		        window.location.href="../admin/actionitemdetails.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=prospects&CommId=" +a +"&CaseId=" +b+ "&CaseTimeId="+c+ "&CaseExpId=" +d;
		        return false;
		    }
		
		}
		function chkAll()
		{
		 for (i = 1; i <= 20; i++)
				{
				   var str;
				   if (i<10) 
				   {
				   str='0'+i
				   }
				   else
				   {
				   str=i
				   }
				   if (document.all['uwOppTab__ctl6_rptCorr_ctl00_chkDelete'].checked==true)
				   {
				     if (typeof(document.all['uwOppTab__ctl6_rptCorr_ctl'+str+'_chkADelete'])!='undefined')
				     {
				        document.all['uwOppTab__ctl6_rptCorr_ctl'+str+'_chkADelete'].checked=true;
				     }
				   }
				   else
				   {
				     if (typeof(document.all['uwOppTab__ctl6_rptCorr_ctl'+str+'_chkADelete'])!='undefined')
				     {
				        document.all['uwOppTab__ctl6_rptCorr_ctl'+str+'_chkADelete'].checked=false;
				     }
				   }
				}
		}
		function  OpenEmailMessage(a,b)
		{
			window.open("../contact/frmEmailMessage.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Email="+a+"&Date="+b,'','width=750,height=525,status=no,titlebar=no,scrollbars=yes,top=110,left=250')
			return false;
		}
			function DeleteRecord()
				{
					if(confirm('Are you sure, you want to delete the selected record?'))
					{
						return true;
					}
					else
					{
						return false;
					}
				}
			function DeleteMessage()
		    {
			    alert("You Are not Authorized to Delete the Selected Record !");
			    return false;
		    }
		function ShowWindow(Page,q,att) 
		{
		
			if (att=='show')
			{
				document.all[Page].style.visibility = "visible";
				return false;
		
			}
			if (att=='hide')
			{
				document.all[Page].style.visibility = "hidden";
				return false;
		
			}
		}
	
		function DeleteRecord()
		{
			if(confirm('Are you sure, you want to delete the selected record?'))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		function openFollow(a)
		{
		    window.open("../Leads/frmFollowUpHstr.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&rtyWR="+a ,'','toolbar=no,titlebar=no,top=300,width=700,height=200,left=200,scrollbars=yes,resizable=yes')
//			document.all['cntDoc'].src="../Leads/frmFollowUpHstr.aspx?Div="+a ;
//			document.all['divFollow'].style.visibility = "visible";
			return false;
		}
		function DeleteMessage()
		{
			alert("You Are not Authorized to Delete the Selected Record !");
			return false;
		}
		function fn_GoToURL(varURL)
		{
			
			if ((varURL!='') && (varURL.substr(0,7)=='http://') && (varURL.length > 7))
			{
			
				var LoWindow=window.open(varURL,"","");
				LoWindow.focus();
			}
			return false;
		}
		function fn_EditLabels(URL)
		{
			window.open(URL,'WebLinkLabel','width=500,height=70,status=no,scrollbar=yes,top=110,left=150');
			return false;
		}
		function OpenTransfer(url)
		{
			window.open(url,'',"width=340,height=150,status=no,top=100,left=150");
			return false;	
		}
		function OpenLst(url)
		{
			window.open(url,'','toolbar=no,titlebar=no,top=300,width=700,height=200,left=200,scrollbars=yes,resizable=yes')
			return false;	
		}
		function OpenAdd(a)
		{
			window.open("../prospects/frmProspectsAdd.aspx?pwer=dfsdfdsfdsfdsf&rtyWR="+a,'','toolbar=no,titlebar=no,top=300,width=700,height=300,scrollbars=no,resizable=no')
		
			return false;
		}
		function OpenDocuments(a)
		{
			window.open("../Documents/frmSpecDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=A&yunWE="+a,'','toolbar=no,titlebar=no,top=200,width=700,height=450,left=200,scrollbars=yes,resizable=yes')
			return false;
		}
		function OpenTo(a)
		{
			window.open("../admin/frmcomAssociationTo.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&boolAssociateDiv=true&numDivisionId="+a,'','toolbar=no,left=200,titlebar=no,top=300,width=800,height=500,scrollbars=yes,resizable=yes')
			return false;
		}


		function OpenFrom(a)
		{
			window.open("../admin/frmCompanyAssociationFrom.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&DivId="+a,'','toolbar=no,titlebar=no,top=300,left=200,width=700,height=200,scrollbars=yes,resizable=yes')
			return false;
		}
		function OpenGroup(a,b,c)
		{
			window.open('../admin/addNewGroup.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=prospectsdisp&CmpID='+a+'&DivID='+b+'&CRMType=1&CntID='+c,'','toolbar=no,titlebar=no,top=300,width=300,height=150,scrollbars=yes,resizable=yes')
			return false;
		}
	
		function Disableddl()
		{
			if (Tabstrip6.selectedIndex==0)
			{
				document.Form1.ddlOppStatus.style.display="none";
			}
			else
			{
				document.Form1.ddlOppStatus.style.display="";
			} 
		}
		function CheckNumber(cint)
					{
						if (cint==1)
						{
							if (!(window.event.keyCode > 47 && window.event.keyCode < 58 || window.event.keyCode==44 || window.event.keyCode==46))
							{
								window.event.keyCode=0;
							}
						}
						if (cint==2)
						{
							if (!(window.event.keyCode > 47 && window.event.keyCode < 58))
							{
								window.event.keyCode=0;
							}
						}
						
					}
	function FillAddress(a)
		{
		 document.getElementById('uwOppTab__ctl0_lblAddress').innerText=a;
		 return false;
		}

		/*
		Purpose:	Lead to Organization Details Page
		Created By: Debasish Tapan Nag
		Parameter:	1) numContactId: The Contact Id
		Return		1) None
		*/
		function GoOrgDetails(numDivisionId)
		{
			frames['IfrOpenOrgContact'].document.getElementById('hdRedirectionEntity').value='OrganizationFromDiv';
			frames['IfrOpenOrgContact'].document.getElementById('hdDivisionId').value = numDivisionId;
			frames['IfrOpenOrgContact'].document.forms['frmOrgContactRedirect'].submit();
		}
		function CheckTabSel(a)
		{
		    if (document.getElementById('uwOppTab').selectedIndex==5&&a==0)
		    {
		        document.Form1.submit()
		    }
		    return false;
		}
		function ShowLayout(a,b,c)
		{
			window.open("../pagelayout/frmCustomisePageLayout.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Ctype="+a+"&type="+c,'','toolbar=no,titlebar=no,top=200,scrollbars=yes,resizable=yes');
			return false;
		}
		function fn_Mail(txtMailAddr,a,b)
		{ 
            if (txtMailAddr!='')
			{
				if (a==1)
				{
				
					window.open('mailto:' + txtMailAddr);
				}
				else if (a==2)
				{
				window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&LsEmail='+ txtMailAddr+'&pqwRT='+b,'','toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')
				
				
				}
				
			}
			
			return false;	
		}
		</script>
	</head>
	<body>
	<form id="Form1" method="post" runat="server">		
			<asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
  <menu1:PartnerPoint id="PartnerPoint1" runat="server"></menu1:PartnerPoint>
            <asp:updatepanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" >
            <ContentTemplate>
			<table width="100%" align="center">

					<tr>
						<td>
							<table id="tblMenu" borderColor="black" cellSpacing="0" cellPadding="0" width="100%" border="0"
								runat="server">
								<tr>
									<td class="tr1" align="center"><b>Record Owner: </b>
										<asp:label id="lblRecordOwner" runat="server" ForeColor="Black"></asp:label></td>
									<td class="td1" width="1" height="18"></td>
									<td class="tr1" align="center"><b>Created By: </b>
										<asp:label id="lblCreatedBy" runat="server" ForeColor="Black"></asp:label></td>
									<td class="td1" width="1" height="18"></td>
									<td class="tr1" align="center"><b>Last Modified By: </b>
										<asp:label id="lblLastModifiedBy" runat="server" ForeColor="Black"></asp:label></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table width="100%">
								<tr>
								    <td class="normal1" align="left">Organization ID :
						                <asp:label id="lblCustomerId" Runat="server"></asp:label>
						            </td>
									<td class="normal1">
										<asp:label id="lblAssociation" runat="Server"></asp:label>
										
									</td>
									<TD align="right">
										<asp:button id="btnFav" CssClass="button" Runat="server" Text="Add to Favorites" Width="105"></asp:button>
										<asp:button id="btnTransfer" Runat="server" Text="Transfer Ownership" CssClass="button"></asp:button>
										<asp:button id="btnDemote" Runat="server" Text="Demote" CssClass="button"></asp:button>
										&nbsp;&nbsp;&nbsp;<asp:button id="btnEdit" Runat="server" Text="Edit" Width="50" CssClass="button"></asp:button>
										<asp:button id="btnCancel" Runat="server" Text="Close" CssClass="button"></asp:button>
										<asp:Button ID="btnActDelete" Runat="server" CssClass="Delete" Text="r" ></asp:Button>
									</TD>
								</tr>
							</table>
						</td>
					</tr>
			</table>
			
			
			<table cellpadding="0" cellspacing="0" width="100%">
				
					<td>
				<igtab:ultrawebtab  AutoPostBack="true"  ImageDirectory=""  id="uwOppTab" runat="server" ThreeDEffect="True" BorderStyle="Solid" Width="100%" BarHeight="0"  BorderWidth="0">
                     <DefaultTabStyle Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Arial" >
                        </DefaultTabStyle>
                        <RoundedImage LeftSideWidth="7" RightSideWidth="8" ShiftOfImages="0" SelectedImage="../images/ig_tab_winXPs3.gif" NormalImage="../images/ig_tab_winXP3.gif" HoverImage="../images/ig_tab_winXPs3.gif" FillStyle="LeftMergedWithCenter" ></RoundedImage>
                     <SelectedTabStyle Height="23px"  ForeColor="white">
                        </SelectedTabStyle>
                        <HoverTabStyle Height="23px" ForeColor="white"></HoverTabStyle>
                        <Tabs>
                             <igtab:Tab Text="&nbsp;&nbsp;Prospect Details&nbsp;&nbsp;" >
                                <ContentTemplate>
								<asp:table id="tblProspects" Runat="server" BorderWidth="1" Width="100%" GridLines="None" BorderColor="black" CssClass="aspTableDTL"
									Height="200"  >
									<asp:TableRow >
									<asp:TableCell VerticalAlign="top">
									<asp:Table Width="100%" ID="tbl12" runat="server" >
									   <asp:TableRow>
									        <asp:TableCell  ColumnSpan="2" HorizontalAlign="Right" >
									             <asp:button id="btnLayout" Runat="server" CssClass="button" Text="Layout" ></asp:button>
									        </asp:TableCell>
									   </asp:TableRow>
									   <asp:TableRow>
									        <asp:TableCell VerticalAlign="Top"><img src="../images/Building-48.gif" /></asp:TableCell>
									        <asp:TableCell>
									            <asp:Table runat="server" ID="tabledetail"   BorderWidth="0"    GridLines="none" CellPadding="2" CellSpacing="0"   HorizontalAlign="Center" ></asp:Table>
									        </asp:TableCell>
									   </asp:TableRow>
									   <asp:TableRow>
									        <asp:TableCell ColumnSpan="2">
									            <asp:Table runat="server" ID="tableComment" BorderWidth="" Width="100%"  GridLines="none"   HorizontalAlign="Center" ></asp:Table>
									        </asp:TableCell>
									   </asp:TableRow>
									</asp:Table>							
									</asp:TableCell></asp:TableRow>
								</asp:table>
							  </ContentTemplate>
                             </igtab:Tab>
                               <igtab:Tab Text="&nbsp;&nbsp;Contacts&nbsp;&nbsp;" >
                                <ContentTemplate>
								<asp:table id="Table1" BorderWidth="1" CellPadding="0" CellSpacing="0" Height="300" Runat="server" CssClass="aspTableDTL"
									Width="100%" BorderColor="black" GridLines="None">
									<asp:TableRow>
									    <asp:TableCell VerticalAlign="Top">
									    	<asp:datagrid id="dgContacts" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
												BorderColor="white">
												<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
												<ItemStyle CssClass="is"></ItemStyle>
												<HeaderStyle CssClass="hs"></HeaderStyle>
												<Columns>
													<asp:BoundColumn DataField="numContactId" Visible="False"></asp:BoundColumn>
													<asp:BoundColumn DataField="numCreatedBy" Visible="false"></asp:BoundColumn>
													<asp:BoundColumn DataField="numTerid" Visible="False"></asp:BoundColumn>
													<asp:ButtonColumn HeaderText="First & Last Name" DataTextField="Name" CommandName="Name"></asp:ButtonColumn>
													
													<asp:BoundColumn HeaderText="Position,Type" DataField="Pos"></asp:BoundColumn>
													<asp:TemplateColumn HeaderText="Email">
														<ItemTemplate>
															<asp:HyperLink ID="hplEmail" Runat="server" CssClass="hyperlink" Text='<%# DataBinder.Eval(Container.DataItem, "Email") %>' Target=_blank >
															</asp:HyperLink>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn HeaderText="Phone - Ext" DataField="Phone"></asp:BoundColumn>
													<asp:BoundColumn HeaderText="Assistant's Name" DataField="Asst"></asp:BoundColumn>
													<asp:BoundColumn HeaderText="Assistant's Phone - Ext" DataField="AsstPhone"></asp:BoundColumn>
													<asp:TemplateColumn HeaderText="<font color=white>New Action Item</font>" >
														<ItemTemplate>
															<asp:HyperLink ID="hplNewActItem" Runat="server" CssClass="hyperlink">New Action Item</asp:HyperLink>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn HeaderText="<font color=white>Last 10 Action Item Opened</font>">
														<ItemTemplate>
															<asp:HyperLink ID="hplLast10Opened" Runat="server" CssClass="hyperLink">Last 10 Action Item Opened</asp:HyperLink>/
															<asp:HyperLink ID="hplLst10Closed" Runat="server" CssClass="hyperLink">Closed</asp:HyperLink>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn Visible="false">
														<HeaderTemplate>
															<asp:Button ID="btnHdelete" Runat="server" CssClass="Delete" Text="r"></asp:Button>
														</HeaderTemplate>
														<ItemTemplate>
															<asp:Button ID="btnDelete" Runat="server" CssClass="Delete" Text="r" CommandName="Delete"></asp:Button>
															<asp:LinkButton ID="lnkdelete" Runat="server" Visible="false">
																<font color="#730000">*</font></asp:LinkButton>
														</ItemTemplate>
													</asp:TemplateColumn>
												</Columns>
											</asp:datagrid>
											
										</asp:TableCell>
									</asp:TableRow>
								</asp:table>
						  </ContentTemplate>
                             </igtab:Tab>
                               <igtab:Tab Text="&nbsp;&nbsp;Opportunities&nbsp;&nbsp;" >
                                <ContentTemplate>
								<asp:table id="Table5" BorderWidth="1" Runat="server" Height="300" Width="100%" BorderColor="black" CssClass="aspTableDTL"
									GridLines="None">
									<asp:TableRow>
										<asp:TableCell VerticalAlign="Top">
											<br>
											<table cellpadding="0" cellspacing="0" width="70%">
												<tr valign="top">
													<td>
													    <table>
													        <tr class="normal1">
													            <td><asp:RadioButton ID="radOppOpen" runat="server" AutoPostBack="true"  GroupName="radOpp" Checked="true"/></td>
													            <td>&nbsp;Open Opportunities&nbsp;&nbsp;</td>													
													            <td><asp:RadioButton ID="radOppClose" runat="server" AutoPostBack="true"  GroupName="radOpp"/></td>
													            <td>&nbsp;Closed Deals&nbsp;&nbsp;</td>
													           
													        </tr>
													    </table>
														
													</td>
													<td>
														<asp:DropDownList ID="ddlOppType" Runat="server" CssClass="signup" AutoPostBack="True">
															<asp:ListItem Value="1">Sales Opportunity</asp:ListItem>
															<asp:ListItem Value="2">Purchase Opportunity</asp:ListItem>
														</asp:DropDownList>
													</td>
													<td>
														<asp:DropDownList ID="ddlOppStatus" Runat="server" CssClass="signup" AutoPostBack="True">
															<asp:ListItem Value="1">Won</asp:ListItem>
															<asp:ListItem Value="2">Lost</asp:ListItem>
														</asp:DropDownList>
													</td>
												</tr>
											</table>
											<table width="100%" cellpadding="0" cellspacing="0">
												<tr valign="top">
													<td>
													<asp:table id="Table7" BorderWidth="1" CellPadding="0" CellSpacing="0" Runat="server" Width="100%"
																	BorderColor="black" GridLines="None" Height="250" >
																	<asp:TableRow>
																		<asp:TableCell VerticalAlign="Top">
																			<asp:datagrid id="dgOpenOpportunty" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
																				BorderColor="white">
																				<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
																				<ItemStyle CssClass="is"></ItemStyle>
																				<HeaderStyle CssClass="hs"></HeaderStyle>
																				<Columns>
																					<asp:BoundColumn DataField="numOppId" Visible="false"></asp:BoundColumn>
																					<asp:BoundColumn DataField="numContactID" Visible="false"></asp:BoundColumn>
																					<asp:ButtonColumn HeaderText="Name" DataTextField="vcPOppname" CommandName="Name"></asp:ButtonColumn>
																					<asp:ButtonColumn HeaderText="Contact" DataTextField="vcGivenName" CommandName="Contact"></asp:ButtonColumn>
																					<asp:BoundColumn HeaderText="Phone - Ext" DataField="PhoneNo"></asp:BoundColumn>
																					<asp:BoundColumn HeaderText="Milestone, Stage" DataField="status"></asp:BoundColumn>
																					<asp:BoundColumn HeaderText="Amount" DataFormatString="{0:#,###.00}" DataField="monPAmount"></asp:BoundColumn>
																				</Columns>
																			</asp:datagrid>
																		
																			<asp:datagrid id="dgClosedOpp" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
																				BorderColor="white">
																				<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
																				<ItemStyle CssClass="is"></ItemStyle>
																				<HeaderStyle CssClass="hs"></HeaderStyle>
																				<Columns>
																					<asp:BoundColumn DataField="numOppId" Visible="false" ></asp:BoundColumn>
																					<asp:BoundColumn DataField="numContactID" Visible="false"></asp:BoundColumn>
																					<asp:ButtonColumn HeaderText="Name" DataTextField="vcPOppname" CommandName="Name"></asp:ButtonColumn>
																					<asp:ButtonColumn HeaderText="Contact" DataTextField="vcGivenName" CommandName="Contact"></asp:ButtonColumn>
																					<asp:BoundColumn HeaderText="Phone - Ext" DataField="PhoneNo"></asp:BoundColumn>
																					<asp:BoundColumn HeaderText="Status" DataField="status"></asp:BoundColumn>
																					<asp:TemplateColumn HeaderText="Completed Date">
																						<ItemTemplate>
																							<%#ReturnName(DataBinder.Eval(Container.DataItem, "bintAccountClosingDate"))%>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn HeaderText="Amount" DataFormatString="{0:#,###.00}" DataField="monPAmount"></asp:BoundColumn>
																				</Columns>
																			</asp:datagrid>
																		</asp:TableCell>
																	</asp:TableRow>
														</asp:table>
													</td>
												</tr>
											</table>
										</asp:TableCell>
									</asp:TableRow>
								</asp:table>
								
							   </ContentTemplate>
                             </igtab:Tab>
                               <igtab:Tab Text="&nbsp;&nbsp;Projects&nbsp;&nbsp;" >
                                <ContentTemplate>
                             
								<asp:table id="Table8" BorderWidth="1" CellPadding="0" CellSpacing="0" Height="300" Runat="server" CssClass="aspTableDTL"
									Width="100%" BorderColor="black" GridLines="None">
									<asp:TableRow>
										<asp:TableCell VerticalAlign="Top">
											<br>
											<table>
												<tr class="normal1">
												  <td><asp:RadioButton ID="radPro" runat="server" AutoPostBack="true"  GroupName="radProject" Checked="true"/></td>
												  <td>&nbsp;Projects&nbsp;&nbsp;</td>													
												  <td><asp:RadioButton ID="radProCmp" runat="server" AutoPostBack="true"  GroupName="radProject"/></td>
												  <td>&nbsp;Projects Completed&nbsp;&nbsp;</td>
												</tr>
											</table>
											
											
													<asp:table id="Table13" BorderWidth="1" CellPadding="0" CellSpacing="0" Runat="server" Width="100%"
														BorderColor="black" GridLines="None">
														<asp:TableRow>
															<asp:TableCell>
															
																<asp:datagrid id="dgProjectsOpen" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
																	BorderColor="white">
																	<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
																	<ItemStyle CssClass="is"></ItemStyle>
																	<HeaderStyle CssClass="hs"></HeaderStyle>
																	<Columns>
																		<asp:BoundColumn Visible="False" DataField="numProId"></asp:BoundColumn>
																		<asp:BoundColumn Visible="False" DataField="numContactId"></asp:BoundColumn>
																		<asp:ButtonColumn DataTextField="Name" HeaderText="Name" CommandName="Name"></asp:ButtonColumn>
																		<asp:BoundColumn DataField="DealID" HeaderText="Opportunity Or Deal ID"></asp:BoundColumn>
																		<asp:ButtonColumn DataTextField="CustJobContact" HeaderText="Cust. Prime Job Contact" CommandName="Contact"></asp:ButtonColumn>
																		<asp:BoundColumn DataField="LstCmtMilestone" HeaderText="Last Completed Milestone"></asp:BoundColumn>
																		<asp:BoundColumn DataField="lstCompStage" HeaderText="Last completed Stage, Completed vs. Due Date"></asp:BoundColumn>
																	</Columns>
																	<PagerStyle Visible="False" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
																</asp:datagrid>
																
															</asp:TableCell>
														</asp:TableRow>
													</asp:table>
												
												
										</asp:TableCell>
									</asp:TableRow>
								</asp:table>
							   </ContentTemplate>
                             </igtab:Tab>
                               <igtab:Tab Text="&nbsp;&nbsp;Accounting&nbsp;&nbsp;" >
                                <ContentTemplate>
                             
								<asp:table id="Table11" BorderWidth="1" Height="300" Runat="server" Width="100%" BorderColor="black" CssClass="aspTableDTL"
									GridLines="None">
									<asp:TableRow>
										<asp:TableCell VerticalAlign="Top">
											<br>
												<table>
												<tr class="normal1">
												  <td><asp:RadioButton ID="rdbFinancialOverview" runat="server" AutoPostBack="true"  GroupName="radProject" Checked="true"/></td>
												  <td>&nbsp;Financial Overview&nbsp;&nbsp;</td>													
												  <td><asp:RadioButton ID="rdbTransaction" runat="server" AutoPostBack="true"  GroupName="radProject"/></td>
												  <td>&nbsp;Transactions&nbsp;&nbsp;</td>
												</tr>
											</table>
					    				    <asp:Panel ID="PnlFinancialOverview" runat ="server" >
											<table border="0" width="70%" >
												<tr align="center">
													<td class="normal7" align="right">
														Credit Limit :
													</td>
													<td  align="left">
													 <asp:Label ID="lblCreditLimit" runat="server" CssClass="normal1"></asp:Label>														
													</td>
													<td class="normal7" align="right">
														No Tax :
													</td>
													
													<td valign="top" style="width:25%">
													  <asp:Label ID="lblTax" runat="server" CssClass="cell" ></asp:Label>
													</td>
												</tr>
												<tr align="center">
													<td class="normal7" align="right">Billing Terms :</td>
													<td class="normal1"  align="left">
													<asp:Label ID="lblBillinTerms" Runat="server" CssClass="cell" Font-Bold="false"></asp:Label>&nbsp;<b>  Msg. :</b>&nbsp;
														<asp:Label ID="lblSummary" Runat="server"></asp:Label></td>
												</tr>	
											</table>
											</asp:Panel>
											<asp:Panel ID="PnlTransaction" runat ="server" Visible="false">
											<asp:table id="Table3" BorderWidth="1" CellPadding="0" CellSpacing="0" Runat="server" Width="100%" CssClass="aspTableDTL" 
														BorderColor="black" GridLines="None" Height="250">
														<asp:TableRow>
															<asp:TableCell VerticalAlign="top">
																<asp:datagrid id="dgTransaction" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
																	BorderColor="white">
																	<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
																	<ItemStyle CssClass="is"></ItemStyle>
																	<HeaderStyle CssClass="hs"></HeaderStyle>
																	<Columns>
																	    <asp:BoundColumn DataField="Name" HeaderText="Authoritative BizDoc"></asp:BoundColumn>
																		<asp:BoundColumn DataField="OppType" HeaderText="Type"></asp:BoundColumn>
																		<asp:TemplateColumn HeaderText ="Due Date">
																		<ItemTemplate>
																		<%#ReturnDateTime(DataBinder.Eval(Container.DataItem, "DueDate"))%>
																		</ItemTemplate>
																		</asp:TemplateColumn>
																		<asp:TemplateColumn HeaderText="Total Amount">
																		<ItemTemplate >
																		  <%#ReturnMoney(DataBinder.Eval(Container.DataItem, "TotalAmt"))%>
																		</ItemTemplate>
																	 </asp:TemplateColumn>
																	 
																	 <asp:TemplateColumn HeaderText="Amount Paid">
																		<ItemTemplate >
																		  <%#ReturnMoney(DataBinder.Eval(Container.DataItem, "Amount"))%>
																		</ItemTemplate>
																		</asp:TemplateColumn>
																		<asp:TemplateColumn HeaderText ="Date Paid">
																		<ItemTemplate>
																		<%#ReturnDateTime(DataBinder.Eval(Container.DataItem, "datePaid"))%>
																		</ItemTemplate>
																		</asp:TemplateColumn> 
																		
																		<asp:TemplateColumn HeaderText ="Deposited to Bank account on">
																		<ItemTemplate>
																		<%#ReturnDateTime(DataBinder.Eval(Container.DataItem, "DepositedDate"))%>
																		</ItemTemplate>
																		</asp:TemplateColumn>
																		<asp:TemplateColumn HeaderText ="Balance Due">
																		<ItemTemplate >
																		  <%#ReturnMoney(DataBinder.Eval(Container.DataItem, "BalanceAmt"))%>
																		</ItemTemplate>
																		</asp:TemplateColumn>
																	 
																	</Columns>
																	<PagerStyle Visible="False" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
																</asp:datagrid>
															</asp:TableCell>
														</asp:TableRow>
													</asp:table>
											</asp:Panel> 
										</asp:TableCell>
									</asp:TableRow>
								</asp:table>
						   </ContentTemplate>
                             </igtab:Tab>
                               <igtab:Tab Text="&nbsp;&nbsp;Web Analysis&nbsp;&nbsp;" >
                                <ContentTemplate>
                             
							
								<asp:table id="Table12" BorderWidth="1" Height="300" Runat="server" Width="100%" BorderColor="black" CssClass="aspTableDTL"
									GridLines="None">
									<asp:TableRow>
										<asp:TableCell VerticalAlign="Top">
										<table width="700">
										    <tr>
    										    <td class="text_bold" align="right" >
    										    Original Referring Page : 
    										    </td>
    										    <td >
    										        <asp:Label ID="lblReferringPage" runat="server" cssclass="normal1"></asp:Label>
    										    </td>
    										     <td class="text_bold" align="right" >
    										    Original Referring Key Word Used : 
    										    </td>
    										    <td>
    										        <asp:Label ID="lblKeyword" runat="server" cssclass="normal1"></asp:Label>
    										    </td>
										    </tr>
										    <tr>
    										    <td class="text_bold" align="right" >
    										    First / Last Date visited : 
    										    </td>
    										    <td>
    										        <asp:Label ID="lblDatesvisited" runat="server" cssclass="normal1"></asp:Label>
    										    </td>
    										     <td class="text_bold" align="right" >
    										    Total Number of times visited : 
    										    </td>
    										    <td>
    										        <asp:Label ID="lblNoofTimes" runat="server" cssclass="normal1"></asp:Label>
    										    </td>
										    </tr>
										</table>
										
										
										<asp:DataList ID="dlWebAnlys" Runat="server" Width="100%" CellPadding="0" CellSpacing="2">
										<ItemTemplate>
										<table width="100%" class="hs">
										    <tr>
										        <td align="right" >
										     
										        Date Visitied:
										        </td>
										        <td>
										        <asp:Label ID="lblCreated" Runat="server"  Text= '<%# DataBinder.Eval(Container,"DataItem.dtCreated") %>'>
															</asp:Label>
										        </td>
										        <td align="right">
										        <asp:Label ID="Label4" Runat="server"  Text= '<%# DataBinder.Eval(Container,"DataItem.NoOfTimes") %>'>
															</asp:Label>&nbsp;Pages Viewed : 
										        </td>
										        <td >
										        <asp:LinkButton ID="lnk" CommandName="Pages" Runat="server" style="TEXT-DECORATION: none">
													<font color="white">Click Here</font></asp:LinkButton>
										        </td>
										        <td align="right">
										        Total Time on site : 
										        </td>
										        <td>
										        <asp:Label ID="lblTotalTime" Runat="server"  Text= '<%# DataBinder.Eval(Container,"DataItem.vcTotalTime") %>'>
															</asp:Label>
										        </td>
										    </tr>
										</table>
										</ItemTemplate>
										<SelectedItemTemplate>
										<table width="100%" cellpadding="0" cellspacing="0" >
											
										    <tr class="hs">
										        <td align="right">
										        
										        <asp:Label ID="lblID" Runat="server" Text= '<%# DataBinder.Eval(Container,"DataItem.numTrackingID") %>' Visible="false">
														</asp:Label>
										        Date Visitied:
										        </td>
										        <td>
										        <asp:Label ID="lblCreated" Runat="server"  Text= '<%# DataBinder.Eval(Container,"DataItem.dtCreated") %>'>
															</asp:Label>
										        </td>
										        <td align="right">
										        <asp:Label ID="Label4" Runat="server"  Text= '<%# DataBinder.Eval(Container,"DataItem.NoOfTimes") %>'>
															</asp:Label>&nbsp;Pages Viewed : 
										        </td>
										    
										        <td align="right">
										        Total Time on site : 
										        </td>
										        <td>
										        <asp:Label ID="lblTotalTime" Runat="server"  Text= '<%# DataBinder.Eval(Container,"DataItem.vcTotalTime") %>'>
															</asp:Label>
										        </td>
										    </tr>
												<tr>
													<td colspan="5">
														<asp:datagrid id="dgWebAnlys" runat="server" AllowSorting="True" CssClass="dg" Width="100%" BorderColor="white"
							                                    AutoGenerateColumns="False">
							                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							                                    <ItemStyle CssClass="is"></ItemStyle>
							                                    <HeaderStyle CssClass="hs"></HeaderStyle>
							                                    <Columns>
								                                    <asp:BoundColumn Visible="False" DataField="numTracVisitorsHDRID"></asp:BoundColumn>
								                                    <asp:BoundColumn HeaderText="Page" DataField="vcPageName"></asp:BoundColumn>
								                                    <asp:BoundColumn HeaderText="Time Spent on the Page"  DataField="vcElapsedTime"></asp:BoundColumn>
								                                    <asp:BoundColumn  HeaderText="Time Visited" DataField="TimeVisited"></asp:BoundColumn>
							                                    </Columns>
						                                    </asp:datagrid>
													</td>
												</tr>
											</table>
										
										</SelectedItemTemplate>
									</asp:DataList>
										</asp:TableCell>
									</asp:TableRow>
								</asp:table>
							   </ContentTemplate>
                             </igtab:Tab>
                               <igtab:Tab Text="&nbsp;&nbsp;Correspondence&nbsp;&nbsp;" >
                                <ContentTemplate>
                                
							  <asp:table id="Table4" CellPadding="0" CellSpacing="0" Runat="server" BorderWidth="1" Width="100%" CssClass="aspTable"
									    GridLines="None" BorderColor="black" Height="300">
									   <asp:TableRow>
									    <asp:TableCell VerticalAlign="Top">
									           <table width="100%" border="0">
									            <tr align="center" valign="top">
									                    
														<td align="right">
														    <table>
														        <tr>
													                <td class="normal1" align="right">From</td>
													                <td align="left">
													                <BizCalendar:Calendar ID="Calendar1" runat="server" />
													                </td>
													                <td class="normal1" align="right">To</td>
													                <td>
														                <BizCalendar:Calendar ID="Calendar2" runat="server" />
                														
													                </td>
													                <td>
													                
													                </td>	
														        </tr>
														    </table>
														</td>
														<td align="right">
														    <table class="normal1">
														        <tr>
													                <td>
													                Search
													                </td>	
													                <td>
													                <asp:TextBox ID="txtSearchCorr" runat="server"  CssClass="signup" ></asp:TextBox>
													                </td>
													                <td>
													                <asp:DropDownList ID="ddlSrchCorr" runat="server" CssClass="signup">
													                    <asp:ListItem Text="Show All" Value="0"></asp:ListItem>
        									                            <asp:ListItem Text="Received Messages" Value="1"></asp:ListItem>
        									                            <asp:ListItem Text="Sent Messages" Value="2"></asp:ListItem>
        									                            <asp:ListItem Text="Communications by Due Date" Value="3"></asp:ListItem>
        									                            <asp:ListItem Text="Communications by Created Date" Value="4"></asp:ListItem>
        									                            <asp:ListItem Text="Tasks by Due Date" Value="5"></asp:ListItem>
        									                            <asp:ListItem Text="Tasks by Created Date" Value="6"></asp:ListItem>
        									                            <asp:ListItem Text="Notes by Created Date" Value="7"></asp:ListItem>
													                </asp:DropDownList>
													                </td>
													                <td>
													                <asp:button id="btnCorresGo"  Runat="server" CssClass="button" Text="Go"></asp:button>&nbsp;
													                <asp:button id="btnCorrDelete" Runat="server" CssClass="button" Text="Delete"></asp:button>
													                </td>
														        </tr>
														    </table>
														</td>
													</tr>
													<tr>
													    <td colspan="2" align="right" >
													        <table cellpadding="0" cellspacing="0">
											                    <tr >
												                    <td class="normal1">
        									                                Filter :
        									                                <asp:DropDownList ID="ddlFilterCorr" runat="server" AutoPostBack="true" CssClass="signup">
        									                                <asp:ListItem Text="Show All" Value="0"></asp:ListItem>
        									                                <asp:ListItem Text="Received Messages" Value="1"></asp:ListItem>
        									                                <asp:ListItem Text="Sent Messages" Value="2"></asp:ListItem>
        									                                <asp:ListItem Text="Communications by Due Date" Value="3"></asp:ListItem>
        									                                <asp:ListItem Text="Communications by Created Date" Value="4"></asp:ListItem>
        									                                <asp:ListItem Text="Tasks by Due Date" Value="5"></asp:ListItem>
        									                                <asp:ListItem Text="Tasks by Created Date" Value="6"></asp:ListItem>
        									                                <asp:ListItem Text="Notes by Created Date" Value="7"></asp:ListItem>
        									                                </asp:DropDownList>
									                                        </td>
									                                        <td align="center" class="normal1">&nbsp;&nbsp;&nbsp;No of Records :
														                    <asp:Label ID="lblNoOfRecordsCorr" Runat="server" CssClass="text"></asp:Label>&nbsp;&nbsp;&nbsp;</td>
													                    <td id="tdCorr" runat="server">
                    													
											                        <table >													
												                    <tr  >
													                    <td>
														                    <asp:label id="lblNextCorr" runat="server" cssclass="text_bold">&nbsp;&nbsp;&nbsp;Next:</asp:label></td>
													                    <td class="normal1">
														                    <asp:linkbutton id="lnk2Corr" runat="server" CausesValidation="False">2</asp:linkbutton></td>
													                    <td class="normal1">
														                    <asp:linkbutton id="lnk3Corr" runat="server" CausesValidation="False">3</asp:linkbutton></td>
													                    <td class="normal1">
														                    <asp:linkbutton id="lnk4Corr" runat="server" CausesValidation="False">4</asp:linkbutton></td>
													                    <td class="normal1">
														                    <asp:linkbutton id="lnk5Corr" runat="server" CausesValidation="False">5</asp:linkbutton></td>
													                    <td>
														                    <asp:linkbutton id="lnkFirstCorr" runat="server" CausesValidation="False">
															                    <div class="LinkArrow">9</div>
														                    </asp:linkbutton></td>
													                    <td>
														                    <asp:linkbutton id="lnkPreviousCorr" runat="server" CausesValidation="False">
															                    <div class="LinkArrow">3</div>
														                    </asp:linkbutton></td>
													                    <td class="normal1">
														                    <asp:label id="lblPageCorr" runat="server">Page</asp:label></td>
													                    <td>
														                    <asp:textbox id="txtCurrentPageCorr" runat="server" Text="1" Width="28px" CssClass="signup"
															                    MaxLength="5" AutoPostBack="true"></asp:textbox></td>
													                    <td class="normal1">
														                    <asp:label id="lblOfCorr" runat="server">of</asp:label></td>
													                    <td class="normal1">
														                    <asp:label id="lblTotalCorr" runat="server"></asp:label></td>
													                    <td>
														                    <asp:linkbutton id="lnkNextCorr" runat="server" CssClass="LinkArrow" CausesValidation="False">
															                    <div class="LinkArrow">4</div>
														                    </asp:linkbutton></td>
													                    <td>
														                    <asp:linkbutton id="lnkLastCorr" runat="server" CausesValidation="False">
															                    <div class="LinkArrow">:</div>
														                    </asp:linkbutton></td>
												                    </tr>
                    										
											                    </table>
											                    </td>
										                    </tr>
									                    </table>
													    </td>
													</tr>
												</table>
										
								            <table width="100%">
												<tr>
													<td>
													<asp:Repeater ID="rptCorr"  runat="server" >
													    
													    <HeaderTemplate>
													        <table cellspacing="0"  class="dg" width="100%">
													               <tr class="hs">
													                    <td style="display:none">
    													                   numEmailHstrId
													                    </td>
													                    <td style="display:none">
    													                   tintType
													                    </td>
													                    <td align="left" >
													                        <asp:LinkButton CommandName="Sort" ID="lnkDate" runat="server" ><font color="white">Date</font></asp:LinkButton>
													                    </td>
													                    <td align="left" >
													                         <asp:LinkButton CommandName="Sort" ID="lnkType" runat="server" ><font color="white">Type</font></asp:LinkButton>
													                    </td>
													                    <td align="left" >
													                       <asp:LinkButton CommandName="Sort" ID="lnkFrom" runat="server" ><font color="white">From ,To</font></asp:LinkButton>
													                    </td>
													                    <td align="left" >
													                        <asp:LinkButton CommandName="Sort" ID="lnkName" runat="server" ><font color="white">Name /Phone ,& Ext.</font></asp:LinkButton>
													                    </td>
													                    <td align="left" >
													                        <asp:LinkButton CommandName="Sort" ID="lnkAssigned" runat="server" ><font color="white">Assigned To</font></asp:LinkButton>
													                    </td>
													                    <td align="left" >
													                        <asp:CheckBox ID="chkDelete" onclick="chkAll()" runat="server"  />
													                    </td>
													               </tr>
													    </HeaderTemplate>
													    <AlternatingItemTemplate>
													          <tr  class="ais" align="center" >
													                    <td style="display:none">
    													                   
    													                   <%#Container.DataItem("numEmailHstrId")%>
													                    </td>
													                    <td style="display:none">
    													                   <%#Container.DataItem("tintType")%>
													                    </td>
													                    <td>
													                        <%#Container.DataItem("date")%>
													                    </td>
													                    <td>
													                    <asp:HyperLink ID="hplAType" CssClass="hyperlink" NavigateUrl="#"  onclick="openActionItem('<%#Container.DataItem("numEmailHstrId")%>','<%#Container.DataItem("caseid")%>','<%#Container.DataItem("CaseTimeId")%>','<%#Container.DataItem("CaseExpId")%>','<%#Container.DataItem("Type")%>','<%#Container.DataItem("dtCreatedDate")%>')"  runat="server" ><u style="cursor:hand"><%#Container.DataItem("Type")%></u></asp:HyperLink>
													                    </td>
													                    <td>
													                       <%#Container.DataItem("From")%>
													                    </td>
													                    <td>
													                        <%#Container.DataItem("Phone")%>
													                    </td>
													                    <td>
													                       <%#Container.DataItem("assignedto")%>
													                    </td>
													                <td>
													                    <asp:CheckBox ID="chkADelete" style="color:#C6D3E7;" runat="server" />
													                <asp:Label ID="lblDelete" Visible="false"  runat="server" Text='<%#Container.DataItem("DelData")%>' ></asp:Label>
													                </td>
													               </tr>
													               <tr  class="ais">
													                    <td colspan="8">
    													                     <%#Container.DataItem("Subject")%>
													                    </td>
													               </tr>
													    </AlternatingItemTemplate>
													    <ItemTemplate>
													          <tr  class="is">
													                    <td style="display:none">
    													                   
    													                   <%#Container.DataItem("numEmailHstrId")%>
													                    </td>
													                    <td style="display:none">
    													                   <%#Container.DataItem("tintType")%>
													                    </td>
													                    <td>
													                       <%#Container.DataItem("date")%>
													                    </td>
													                    <td>
													                    <asp:HyperLink ID="hplType" CssClass="hyperlink" NavigateUrl="#"  onclick="openActionItem('<%#Container.DataItem("numEmailHstrId")%>','<%#Container.DataItem("caseid")%>','<%#Container.DataItem("CaseTimeId")%>','<%#Container.DataItem("CaseExpId")%>','<%#Container.DataItem("Type")%>','<%#Container.DataItem("dtCreatedDate")%>')"  runat="server" ><u style="cursor:hand"><%#Container.DataItem("Type")%></u></asp:HyperLink>
													                    </td>
													                    <td>
													                       <%#Container.DataItem("From")%>
													                    </td>
													                    <td>
													                        <%#Container.DataItem("Phone")%>
													                    </td>
													                    <td>
													                       <%#Container.DataItem("assignedto")%>
													                    </td>
													                    <td>
													                    <asp:CheckBox ID="chkADelete"   runat="server" />
													                    <asp:Label ID="lblDelete" Visible="false"  runat="server" Text='<%#Container.DataItem("DelData")%>' ></asp:Label>
													                    </td>
													               </tr>
													               <tr  class="is">
													                    <td colspan="8">
    													                     <%#Container.DataItem("Subject")%>
													                    </td>
													               </tr>
													    </ItemTemplate>
													    <FooterTemplate>
													        </table>
													    </FooterTemplate>
													</asp:Repeater>
											</td>
											</tr>
											</table>
									    </asp:TableCell>
									   </asp:TableRow>
								    </asp:table>
							</ContentTemplate>
                             </igtab:Tab>
                               <igtab:Tab Text="&nbsp;&nbsp;Assets&nbsp;" >
                                <ContentTemplate>
                              
								    <asp:table id="Table2" CellPadding="0" CellSpacing="0" Runat="server" BorderWidth="1" Width="100%" CssClass="aspTable"
									    GridLines="None" BorderColor="black" Height="300">
								        <asp:TableRow>
									        <asp:TableCell VerticalAlign="Top">
									             <igtbl:ultrawebgrid id="uwItem" Width="100%" DisplayLayout-AllowRowNumberingDefault="ByDataIsland"  runat="server" Browser="Xml"   Height="100%">
				                                    <DisplayLayout AutoGenerateColumns="false"  RowHeightDefault="18"  AllowAddNewDefault="Yes" Version="3.00" SelectTypeRowDefault="Single"
				                                    ViewType="Hierarchical" TableLayout="Auto" SelectTypeCellDefault="Extended" BorderCollapseDefault="Separate" AllowColSizingDefault="Free" 
				                                    Name="uwItem" EnableClientSideRenumbering="true" SelectTypeColDefault="Extended" AllowUpdateDefault="Yes">
				                                    <HeaderStyleDefault VerticalAlign="Middle" Font-Size="8pt" Font-Bold="true"  Font-Names="Arial" BorderStyle="Solid" HorizontalAlign="Left" ForeColor="white" BackColor="#52658C">
                                                    <Padding Left="2px" Right="2px"></Padding>
                                                    <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
                                                    </HeaderStyleDefault>
                                                    <RowSelectorStyleDefault BackColor="White"></RowSelectorStyleDefault>
                                                     <FrameStyle Width="100%" Cursor="Default" BorderWidth="0px" Font-Size="8pt" Font-Names="Arial" BorderStyle="Double"></FrameStyle>
                                                    <FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
                                                    <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
                                                    </FooterStyleDefault>
                                                    <EditCellStyleDefault BorderWidth="0px" BorderStyle="None"></EditCellStyleDefault>
                                                    <SelectedRowStyleDefault ForeColor="White" BackColor="#666666"></SelectedRowStyleDefault>
                                                    <RowStyleDefault BorderWidth="1px" Font-Size="8pt" Font-Names="Arial" BorderColor="Gray" BorderStyle="Solid" BackColor="White">
                                                    <Padding Left="5px" Right="5px"></Padding>
                                                    <BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
                                                    </RowStyleDefault>
                                                    <RowExpAreaStyleDefault BackColor="LightSteelBlue"></RowExpAreaStyleDefault>
				                                    </DisplayLayout>
					                                    <Bands>
						                                    <igtbl:UltraGridBand AllowDelete="No" AllowAdd="Yes"   >
								                                    <Columns>
									                                  
									                                    <igtbl:UltraGridColumn Hidden="true" IsBound="false" BaseColumnName="numAItemCode" Key="numAItemCode" >
									                                    </igtbl:UltraGridColumn>									                                    
									                                    <igtbl:UltraGridColumn HeaderText="Item Name" Width="41%" AllowUpdate="No" IsBound="false" BaseColumnName="vcitemName" Key="vcitemName" >
									                                    </igtbl:UltraGridColumn>
									                                    <igtbl:UltraGridColumn HeaderText="Serial No" Width="35%" AllowUpdate="No" IsBound="false" BaseColumnName="vcserialno" Key="vcserialno" >
									                                    </igtbl:UltraGridColumn>												                        
									                                    <igtbl:UltraGridColumn HeaderText="Units" Width="20%"  AllowUpdate="No" IsBound="false" Format="###,##0.00"  BaseColumnName="unit" Key="unit" >
									                                    </igtbl:UltraGridColumn>
									                                     <igtbl:UltraGridColumn  Width="4%"  CellButtonDisplay="Always" Type="Button"  Key="Action" >
									                                    </igtbl:UltraGridColumn>
							                                    </Columns>
						                                    </igtbl:UltraGridBand>												                   
					                                    </Bands>
                                                    </igtbl:ultrawebgrid>
									        </asp:TableCell>
								        </asp:TableRow>
								    </asp:table>
								
						 </ContentTemplate>
                             </igtab:Tab>
                         </Tabs>
                         </igtab:ultrawebtab></td>
				</tr>
				<tr>
					<td class="normal4" align="center">
						<asp:Literal ID="litMessage" Runat="server" EnableViewState="False"></asp:Literal>
					</td>
				</tr>
			</table>
			<table width="100%">
				
			</table>
			<asp:TextBox ID="txtTotalPage" Runat="server" style="DISPLAY:none"></asp:TextBox>
			<asp:TextBox ID="txtTotalRecords" Runat="server" style="DISPLAY:none"></asp:TextBox>
			<asp:textbox id="type" style="DISPLAY: none" Runat="server" Text="0"></asp:textbox>
			<asp:textbox id="txtCorrTotalPage" style="DISPLAY: none" Runat="server"></asp:textbox>
			<asp:textbox id="txtCorrTotalRecords" style="DISPLAY: none" Runat="server"></asp:textbox>
			<asp:textbox id="txtContactType" style="DISPLAY: none" Runat="server"></asp:textbox>
			<asp:textbox id="txtContId" style="DISPLAY: none" Runat="server"></asp:textbox>
					<asp:textbox id="txtHidden" style="DISPLAY: none" Runat="server"></asp:textbox>
			<asp:textbox id="txtROwner" style="DISPLAY: none" Runat="server"></asp:textbox>
			</ContentTemplate>
			</asp:updatepanel>
		</form>
	</body>
</HTML>
