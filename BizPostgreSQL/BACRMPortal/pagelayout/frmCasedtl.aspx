<%@ Register TagPrefix="menu1" TagName="webmenu" src="../common/topbar.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmCasedtl.aspx.vb" Inherits="BACRMPortal.frmCasedtl"%>
<%@ Register Assembly="Infragistics2.WebUI.UltraWebTab.v7.3, Version=7.3.20073.38, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebTab" TagPrefix="igtab" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
       <link rel="stylesheet" href="~/CSS/lists.css" type="text/css" />
		<title>Case</title>
		<script language="JavaScript" src="../include/DATE-PICKER.JS" type="text/javascript"></script>    
	<script language="javascript" type="text/javascript" >
	 function openActionItem(a,b,c,d)
		{
		window.location.href="../ActionItems/frmActionItemDtl.aspx?frm=contactdetails&CommId=" +a +"&CaseId=" +b+ "&CaseTimeId="+c+ "&CaseExpId=" +d;
		return false;
		}
		function  OpenEmailMessage(a,b)
		{
			window.open("../contact/frmEmailMessage.aspx?Email="+a+"&Date="+b,'','width=750,height=525,status=no,titlebar=no,scrollbars=yes,top=110,left=250')
			return false;
		}
	function AddContact()
		{
			if (document.Form1.ddlcompany.value==0)
			{
				alert("Select Customer");
				tsCases.selectedIndex=2;
				document.Form1.ddlcompany.focus();
				return false;
			}
			if (document.Form1.ddlAssocContactId.value==0)
			{
				alert("Select Contact");
				tsCases.selectedIndex=2;
				document.Form1.ddlAssocContactId.focus();
				return false;
			}
			var str;
			for(i=0;i<document.Form1.elements.length;i++)
				{
				if (i<=9)
				{
				    str='0'+(i+1)
				}
				else
				{
				    str=i+1
				}
				if (typeof(document.Form1.all['dgContact_ctl'+str+'_txtContactID'])!='undefined')
					{
						if (document.Form1.all['dgContact_ctl'+str+'_txtContactID'].value==document.Form1.ddlAssocContactId.value)
							{
								alert("Associated contact is already added");
								return false;
							}
					} 
				}
			
		}
		
		function ShowWindow(Page,q,att) 
		{
			if (att=='show')
			{
				document.all[Page].style.visibility = "visible";
				//document.Form1.ddlStatus.style.display='none';
				return false;
		
			}
			if (att=='hide')
			{
				document.all[Page].style.visibility = "hidden";
				//document.Form1.ddlStatus.style.display='';
				return false;
		
			}
		}
		function ShowLayout(a,b)
		{
			window.open("../pagelayout/frmCustomisePageLayout.aspx?CntID="+b+"&Ctype=e",'','toolbar=no,titlebar=no,top=200,scrollbars=yes,resizable=yes');
			return false;
		}
		
	</script>
	</HEAD>
	<body>

		<form id="Form1" method="post" runat="server">
		<menu1:webmenu id="webmenu1" runat="server"></menu1:webmenu>
		<asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
		         <asp:updatepanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" ><ContentTemplate>
			<table id="tblMenu" borderColor="black" cellspacing="0" cellpadding="0" width="100%" border="0"
				runat="server">
				<tr>
				<td class="tr1" align="center"><b>Record Owner: </b>
						<asp:label id="lblRecordOwner" runat="server" ForeColor="Black"></asp:label></td>
						<td class="td1" width="1" height="18"></td>
					<td class="tr1" align="center"><b>Created By: </b>
						<asp:label id="lblCreatedBy" runat="server" ForeColor="Black"></asp:label></td>
					<td class="td1" width="1" height="18"></td>
					<td class="tr1" align="center"><b>Last Modified By: </b>
						<asp:label id="lblLastModifiedBy" runat="server" ForeColor="Black"></asp:label></td>
				</tr>
			</table>
			<table width="70%">
				<tr>
					<td valign="top"  rowspan="30">
                        <img src="../images/SuitCase-32.gif" />
					</td>
				    <td class="normal1" align="center">Organization:
					<asp:hyperlink id="lblCustomerId" onclick="ShowWindow('Layer3','','show')" Runat="server" NavigateUrl="#"></asp:hyperlink>
					<TD class="normal1" align="right">Status :</TD>
					<TD class="normal1">
					<asp:Label ID="lblStatus" runat="server" CssClass="normal1"></asp:Label>
						
					<td class="normal1" align="right">Case Number :
					</td>
					<td class="normal1">
						<asp:label id="lblcasenumber" ForeColor="Red" Runat="server"></asp:label></td>
				</tr>
			</table>
			
			<table width="100%" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td valign="bottom"></td>
					<td>
						<table width="100%">
							<tr>
								<td align="right"><asp:button id="btnEdit" Runat="server" Text="Edit" CssClass="button" Width="50"></asp:button>								
									<asp:button id="btnCancel" Runat="server" Text="Cancel" CssClass="button"></asp:button></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
				
				<igtab:ultrawebtab   ImageDirectory="" style="POSITION:absolute;z-index:101;" id="uwOppTab" runat="server" ThreeDEffect="True" BorderStyle="Solid" Width="100%" BarHeight="0"  BorderWidth="0">
                       <DefaultTabStyle Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Arial" >
                        </DefaultTabStyle>
                        <RoundedImage LeftSideWidth="7" RightSideWidth="8" ShiftOfImages="0" SelectedImage="../images/ig_tab_winXPs3.gif" NormalImage="../images/ig_tab_winXP3.gif" HoverImage="../images/ig_tab_winXPs3.gif" FillStyle="LeftMergedWithCenter" ></RoundedImage>
                     <SelectedTabStyle Height="23px"  ForeColor="white">
                        </SelectedTabStyle>
                        <HoverTabStyle Height="23px" ForeColor="white"></HoverTabStyle>
                        <Tabs>
                             <igtab:Tab Text="&nbsp;&nbsp;Case Details&nbsp;&nbsp;">
                                <ContentTemplate>
                             
				    <asp:table id="tblCase" Runat="server" BorderWidth="1" Width="100%" GridLines="None" BorderColor="black"
				Height="300" cellspacing="0" cellpadding="0" CssClass="aspTableDTL">
				<asp:tableRow>
					<asp:tableCell VerticalAlign="Top">
					<table width="100%"  ><tr>
					<td>
						<asp:Table Width="100%" ID="tbl12" runat="server" >
									   <asp:TableRow>
									        <asp:TableCell HorizontalAlign="Right" >
									             <asp:button id="btnLayout" Runat="server" CssClass="button" Text="Layout" ></asp:button>
									        </asp:TableCell>
									   </asp:TableRow>
									   <asp:TableRow>
									        <asp:TableCell>
									            <asp:Table runat="server" ID="tabledetail"   BorderWidth="0"    GridLines="none" CellPadding="2" CellSpacing="0"   HorizontalAlign="Center" ></asp:Table>
									        </asp:TableCell>
									   </asp:TableRow>
									   <asp:TableRow>
									        <asp:TableCell>
									            <asp:Table runat="server" ID="tableComment" BorderWidth="" Width="100%"  GridLines="none"   HorizontalAlign="Center" ></asp:Table>
									        </asp:TableCell>
									   </asp:TableRow>
									</asp:Table>
								</td>
							</tr>
							</table>
							<table width="100%" >
							<asp:Panel ID="pnlKnowledgeBase" Runat="server">
								<TR style="width:"100%">
									<TD class="normal1" align="right" style="width:20%">Solutions added from<BR>
										the Knowledge Base</TD>
									<TD colSpan="5" style="width:80%; " >
										<asp:DataGrid id="dgSolution" runat="server" CssClass="dg" BorderColor="white" Width="100%" ShowFooter="False">
											<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
											<ItemStyle CssClass="is"></ItemStyle>
											<HeaderStyle CssClass="hs"></HeaderStyle>
										</asp:DataGrid></TD>
								</TR>
							</asp:Panel>
							<tr  valign="top">
								<td colSpan="6" class="normal1">
									<asp:linkbutton id="lnkbtnNoofCom" Runat="server" CommandName="Show"></asp:linkbutton>&nbsp;&nbsp;&nbsp;
									<asp:linkbutton id="lnlbtnPost" Runat="server" CommandName="Post">Post Comment</asp:linkbutton>&nbsp;&nbsp;&nbsp;</td>
							</tr>
							<tr  valign="top">
								<td colSpan="8">
									<asp:table ID="tblComments" cellpadding="0" cellspacing="0" Runat="server" Width="100%" Visible="False"></asp:table></td>
							</tr>
							<asp:Panel ID="pnlPostComment" Runat="server" Visible="False">
								<TR>
									<TD class="normal1" align="right">Heading
									</TD>
									<TD colspan="5">
										<asp:TextBox id="txtHeading" Runat="server" Width="500" CssClass="signup"></asp:TextBox></TD>
								</TR>
								<TR>
									<TD class="normal1" align="right">Comment
									</TD>
									<TD colspan="5">
										<asp:TextBox id="txtComments" Runat="server" Width="500" CssClass="signup" Height="90" TextMode="MultiLine"></asp:TextBox></TD>
								</TR>
								<TR>
									<TD align="center" colSpan="6">
										<asp:Button id="btnSubmit" Runat="server" Width="50" CssClass="button" Text="Submit"></asp:Button></TD>
								</TR>
							</asp:Panel>
						</table>
					</asp:tableCell>
				</asp:tableRow>
			</asp:table>
				   </ContentTemplate>
                             </igtab:Tab>
                              <igtab:Tab Text="&nbsp;&nbsp;Associated Contacts&nbsp;&nbsp;" >
                                <ContentTemplate>
                              
				    <asp:table id="Table1" BorderWidth="1" Height="300" Runat="server" Width="100%" BorderColor="black" CssClass="aspTableDTL"
						GridLines="None">
						<asp:TableRow>
							<asp:TableCell VerticalAlign="Top">
											<asp:datagrid id="dgContact" runat="server" CssClass="dg" Width="100%" BorderColor="white" AutoGenerateColumns="False">
												<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
												<ItemStyle CssClass="is"></ItemStyle>
												<HeaderStyle CssClass="hs"></HeaderStyle>
													<Columns>
													<asp:BoundColumn DataField="numContactId" Visible="false"></asp:BoundColumn>
													<asp:BoundColumn DataField="bitPartner" Visible="false"></asp:BoundColumn>
													<asp:BoundColumn HeaderText="Organization,Relationship" DataField="Company"></asp:BoundColumn>
													<asp:BoundColumn HeaderText="First & Last Name" DataField="Name"></asp:BoundColumn>
													<asp:BoundColumn HeaderText="Phone - Ext" DataField="Phone"></asp:BoundColumn>
													<asp:BoundColumn HeaderText="Email" DataField="Email"></asp:BoundColumn>
													
													<asp:TemplateColumn HeaderText="Contact Role">
														<ItemTemplate>
															<asp:Label ID="Label1" runat="server" CssClass="normal1" Text='<%# DataBinder.Eval(Container.DataItem, "ContactRole") %>'></asp:Label>														
														</ItemTemplate>
													</asp:TemplateColumn >
														<asp:TemplateColumn HeaderText="Share Opportunity via Partner Point ?" >
														<ItemTemplate>
														<asp:Label ID="lblShare" runat="server" CssClass="cell"></asp:Label>														            
														</ItemTemplate>
													</asp:TemplateColumn>
													
												</Columns>
											</asp:datagrid>
											<br>
							</asp:TableCell>
						</asp:TableRow>	
					</asp:table>
				  </ContentTemplate>
                             </igtab:Tab>
                        </Tabs>
                        </igtab:ultrawebtab>
			<asp:textbox id="txtLstUptCom" style="DISPLAY: none" Runat="server"></asp:textbox>
			</ContentTemplate>
			</asp:updatepanel>
			<div id="Layer3" style="Z-INDEX: 2; LEFT: 96px; VISIBILITY: hidden; POSITION: absolute; TOP: 88px"><asp:table id="Table11" Runat="server" BorderWidth="1" Height="96px" GridLines="None" BorderColor="black"
					CellPadding="0" CellSpacing="0" BackColor="white">
					<asp:TableRow>
						<asp:TableCell VerticalAlign="Top">
							<table cellSpacing="1" cellPadding="1" width="100%" border="0">
								<tr>
									<td class="normal7" colSpan="2"><b>&nbsp;Customer Information</b></td>
									<td vAlign="top" align="right" colSpan="4">
										<asp:button id="btnCusOk" Runat="server" CssClass="button" Text="Ok" Width="25"></asp:button></td>
								</tr>
								<tr>
									<td class="normal7" align="right">&nbsp;Customer:</td>
									<td class="normal1">
										<asp:hyperlink id="hplCustName" Runat="server"></asp:hyperlink></td>
									<td class="normal7" align="right">&nbsp;Division:</td>
									<td>
										<asp:label id="lblcustdivision" runat="server" cssclass="normal1" width="100px"></asp:label></td>
									<td class="normal7" align="right">&nbsp;Territory:</td>
									<td>
										<asp:label id="lbltrtry" runat="server" cssclass="normal1" width="100px"></asp:label></td>
								</tr>
								<tr>
									<td class="normal7" align="right">&nbsp;Rating:</td>
									<td>
										<asp:label id="lblrat" runat="server" cssclass="normal1" width="100px"></asp:label></td>
									<td class="normal7" align="right">&nbsp;Status:</td>
									<td>
										<asp:label id="lblstat" runat="server" cssclass="normal1" width="100px"></asp:label></td>
									<td class="normal7" align="right">&nbsp;Industry:</td>
									<td>
										<asp:label id="lblind" runat="server" cssclass="normal1" width="100px"></asp:label></td>
								</tr>
								<tr>
									<td class="normal7" align="right">&nbsp;Type:</td>
									<td>
										<asp:label id="lbltype" runat="server" cssclass="normal1" width="100px"></asp:label></td>
									<td class="normal7" align="right">&nbsp;Credit:</td>
									<td>
										<asp:label id="lblcredit" runat="server" cssclass="normal1" width="100px"></asp:label></td>
									<td class="normal7" align="right">&nbsp;Web:</td>
									<td>
										<asp:label id="lblweb" runat="server" cssclass="normal1" width="100px"></asp:label></td>
								</tr>
								<tr>
									<td class="normal7" align="right">&nbsp;Profile:</td>
									<td>
										<asp:label id="lblprofile" runat="server" cssclass="normal1" width="100px"></asp:label></td>
									<td class="normal7" align="right">&nbsp;Group:</td>
									<td>
										<asp:label id="lblgrp" runat="server" cssclass="normal1" width="100px"></asp:label></td>
								</tr>
							</table>
						</asp:TableCell>
					</asp:TableRow>
				</asp:table><br>
			</div>
			
		</form>
	</body>
</HTML>
