<%@ Register Assembly="Infragistics2.WebUI.UltraWebTab.v7.3, Version=7.3.20073.38, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebTab" TagPrefix="igtab" %>
<%@ Register TagPrefix="menu1" TagName="webmenu" src="../common/topbar.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmProjectdtl.aspx.vb" Inherits="BACRMPortal.frmProjectdtl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1"    runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
        <link rel="stylesheet" href="~/CSS/lists.css" type="text/css" />
		<title>Projects</title>
		<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
         <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)"> 
		
		<script language="JavaScript" src="../include/date-picker.js" type="text/javascript"></SCRIPT>
		<script language="javascript" type="text/javascript" >
			function DeleteRecord()
				{
					if(confirm('Are you sure, you want to delete the selected record?'))
					{
						return true;
					}
					else
					{
						return false;
					}
				}
		function DeletMsg()
			{
				var bln=confirm("You're about to remove the Stage from this Process, all stage data will be deleted")
				if (bln==true)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		function OpenDocuments(a)
		{
			window.open("../Documents/frmSpecDocuments.aspx?Type=P&RecID="+a,'','toolbar=no,titlebar=no,top=200,width=700,height=450,left=200,scrollbars=yes,resizable=yes')
			return false;
		}
		function CheckNumber()
		{
			if (!(window.event.keyCode > 47 && window.event.keyCode < 58))
			{
				window.event.keyCode=0;
			}
		}

		function Save(cint)
		{
		
		if (cint==1)
		{
			if (document.Form1.ddlIntPrgMgr.selectedIndex==0)
			{
				alert("Select Internal Project Manager");
				tsVert.selectedIndex=0;
				document.Form1.ddlTaskContact.focus();
				return false;
			}
		}
			if (document.Form1.ddlCustPrjMgr.value==0)
			{
				alert("Select Customer Project Manager");
				tsVert.selectedIndex=0;
				document.Form1.ddlCustPrjMgr.focus();
				return false;
			}
	
		}
		function AddContact()
		{
			if (document.Form1.ddlcompany.value==0)
			{
				alert("Select Customer");
				tsVert.selectedIndex=2;
				document.Form1.ddlcompany.focus();
				return false;
			}
			if (document.Form1.ddlAssocContactId.value==0)
			{
				alert("Select Contact");
				tsVert.selectedIndex=2;
				document.Form1.ddlAssocContactId.focus();
				return false;
			}
			var str;
			for(i=0;i<document.Form1.elements.length;i++)
				{
				if (i<=9)
				{
				    str='0'+(i+1)
				}
				else
				{
				    str=i+1
				}
				if (typeof(document.Form1.all['dgContact_ctl'+str+'_txtContactID'])!='undefined')
					{
						if (document.Form1.all['dgContact_ctl'+str+'_txtContactID'].value==document.Form1.ddlAssocContactId.value)
							{
								alert("Associated contact is already added");
								return false;
							}
					} 
				}
			
		}
		
		function deleteItem()
		{
			var bln;
			bln=window.confirm("Delete Seleted Row - Are You Sure ?")
			if(bln==true) 
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		
		function OpenTemplate(a,b)
		{
			window.open('../common/templates.aspx?pageid='+a+'&id='+b,'','toolbar=no,titlebar=no,left=500, top=300,width=350,height=200,scrollbars=no,resizable=yes');
			return false;
		}
		function OpenDependency(a,b,c,d)
		{
			window.open('../projects/frmProDependency.aspx?ProStageID='+a+'&Proid='+b+'&PerID='+c+'&StgDtlId='+d,'','toolbar=no,titlebar=no,left=500, top=300,width=500,height=200,scrollbars=yes,resizable=yes');
			return false;
		}
		function OpenExpense(a,b,c,d)
		{
			window.open('../projects/frmProExpense.aspx?ProStageID='+a+'&Proid='+b+'&PerID='+c+'&StgDtlId='+d,'','toolbar=no,titlebar=no,left=500, top=300,width=500,height=200,scrollbars=yes,resizable=yes');
			return false;
		}
		function OpenTime(a,b,c,d)
		{
			window.open('../projects/frmProTime.aspx?ProStageID='+a+'&Proid='+b+'&PerID='+c+'&StgDtlId='+d,'','toolbar=no,titlebar=no,left=500, top=300,width=500,height=200,scrollbars=yes,resizable=yes');
			return false;
		}
		function OpenSubStage(a,b,c,d)
		{
			window.open('../projects/frmprosubstages.aspx?ProStageID='+a+'&Proid='+b+'&PerID='+c+'&StgDtlId='+d,'','toolbar=no,titlebar=no,left=500, top=300,width=500,height=200,scrollbars=yes,resizable=yes');
			return false;
		}
		function CheckBoxCon(a,b,c)
		{
			if (parseInt(c)==1)
			{
				document.all['chkStage~'+a+'~'+b].checked=true
			}
			else
			{
				document.all['chkStage~'+a+'~'+b].checked=false
			}
		}
		function ValidateCheckBox(cint)
		{
			if (cint==1)
			{	
				if (document.all['chkDClosed'].checked==true)
				{
					if (document.all['chkDlost'].checked==true)
					{
					alert("The Deal is already Lost !")
					document.all['chkDClosed'].checked=false
					return false;
					}
				}
			}
			if (cint==2)
			{	
				if (document.all['chkDlost'].checked==true)
				{
					if (document.all['chkDClosed'].checked==true)
					{
						alert("The Deal is already Closed !")
						document.all['chkDlost'].checked=false
						return false;
					}
				}
			}
		}
		function DeleteRecord()
		{
			if(confirm('Are you sure, you want to delete the selected record?'))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		function DeleteMessage()
		{
			alert("You Are not Authorized to Delete the Selected Record !");
			return false;
		}
		function ShowWindow1(Page,q,att) 
		{
			if (att=='show')
			{
				document.all[Page].style.visibility = "visible";
				return false;
		
			}
			if (att=='hide')
			{
				document.all[Page].style.visibility = "hidden";
				//window.location.reload(true);
				return false;
		
			}
			
		}
		
		function ShowLayout(a,b)
		{
			window.open("../pagelayout/frmCustomisePageLayout.aspx?CntID="+b+"&Ctype=j",'','toolbar=no,titlebar=no,top=200,scrollbars=yes,resizable=yes');
			return false;
		}
		</script>
	</head>
	<body>
		
		<form id="Form1" method="post" runat="server">
	<menu1:webmenu id="webmenu1" runat="server"></menu1:webmenu>
		<asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
		
		<asp:updatepanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" >
		    <ContentTemplate>
			<table width="100%" align="center">
				<tr>
					<td>
						<table id="tblMenu" borderColor="black" cellSpacing="0" cellPadding="0" width="100%" border="0"
							runat="server">
							<tr>
								<td class="tr1" align="center"><b>Record Owner: </b>
									<asp:label id="lblRecordOwner" runat="server" ForeColor="Black"></asp:label></td>
								<td class="td1" width="1" height="18"></td>
								<td class="tr1" align="center"><b>Created By: </b>
									<asp:label id="lblCreatedBy" runat="server" ForeColor="Black"></asp:label></td>
								<td class="td1" width="1" height="18"></td>
								<td class="tr1" align="center"><b>Last Modified By: </b>
									<asp:label id="lblLastModifiedBy" runat="server" ForeColor="Black"></asp:label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="HEIGHT: 46px">
						<table width="100%">
							<tr>
								<td class="normal1">Organization : <u>
										<asp:label id="lblOrg" onclick="ShowWindow1('Layer3','','show')" CssClass="hyperlink" Runat="server"></asp:label></u></td>
								<TD align="right">
									<asp:Button ID="btnEdit" Runat="server" CssClass="button" Text="Edit" Width="50px"></asp:Button>
									
									<asp:Button ID="btnTCancel" Runat="server" CssClass="button" Text="Cancel"></asp:Button>
									
								</TD>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
					
					<igtab:ultrawebtab   ImageDirectory="" style="POSITION:absolute;z-index:101;" id="uwOppTab" runat="server" ThreeDEffect="True" BorderStyle="Solid" Width="100%" BarHeight="0"  BorderWidth="0">
                       <DefaultTabStyle Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Arial" >
                        </DefaultTabStyle>
                        <RoundedImage LeftSideWidth="7" RightSideWidth="8" ShiftOfImages="0" SelectedImage="../images/ig_tab_winXPs3.gif" NormalImage="../images/ig_tab_winXP3.gif" HoverImage="../images/ig_tab_winXPs3.gif" FillStyle="LeftMergedWithCenter" ></RoundedImage>
                     <SelectedTabStyle Height="23px"  ForeColor="white">
                        </SelectedTabStyle>
                        <HoverTabStyle Height="23px" ForeColor="white"></HoverTabStyle>
                        <Tabs>
                             <igtab:Tab Text="&nbsp;&nbsp;Project Details&nbsp;&nbsp;" >
                                <ContentTemplate>
                             
					
						    <asp:table id="tblOppr" Runat="server" BorderWidth="1" Width="100%" GridLines="None" BorderColor="black" CssClass="aspTableDTL" Height="300" >
									<asp:TableRow>
									<asp:TableCell VerticalAlign="top">
									<asp:Table Width="100%" ID="tbl12" runat="server" >
									   <asp:TableRow>
									        <asp:TableCell HorizontalAlign="Right" ColumnSpan="2" >
									             <asp:button id="btnLayout" Runat="server" CssClass="button" Text="Layout" ></asp:button>
									        </asp:TableCell>
									   </asp:TableRow>
									   <asp:TableRow>
									   <asp:TableCell>
                                              <img src="../images/Compass-32.gif" />
									   </asp:TableCell>
									        <asp:TableCell>
									            <asp:Table runat="server" ID="tabledetail"   BorderWidth="0"    GridLines="none" CellPadding="2" CellSpacing="0"   HorizontalAlign="Center" ></asp:Table>
									        </asp:TableCell>
									   </asp:TableRow>
									   <asp:TableRow>
									        <asp:TableCell ColumnSpan="2">
									            <asp:Table runat="server" ID="tableComment" BorderWidth="" Width="100%"  GridLines="none"   HorizontalAlign="Center" ></asp:Table>
									        </asp:TableCell>
									   </asp:TableRow>
									</asp:Table>        	
																
									</asp:TableCell>
									</asp:TableRow>
								</asp:table>
						   </ContentTemplate>
                            </igtab:Tab>
                               <igtab:Tab Text="&nbsp;&nbsp;Milestones And Stages&nbsp;&nbsp;" >
                                <ContentTemplate>
                                       
								<asp:table id="tblMile" BorderWidth="1" Runat="server" CssClass="aspTableDTL"  Width="100%" cellpadding="0" cellspacing="0" BorderColor="black" GridLines="None" Height="300">
									<asp:TableRow VerticalAlign="Top">
										<asp:TableCell VerticalAlign="Top">
										 <br>
											 <table width="400">
												  <tr>
													 <td class="normal8" align="right"><font color="green">Total Progress :</font>
													 </td>
													  <td Class="normal1">
													        <font color="green"><asp:Label ID="lblTProgress" runat="server" ></asp:Label>&nbsp;%</font>
													   </td>
												    </tr>
											    </table>
											 <br />
									
											    <asp:table id="tblMilestone" Runat="server" Width="100%" CellPadding="0" CellSpacing="0" GridLines="None"></asp:table>
											<br>
										</asp:TableCell>
									</asp:TableRow>
								</asp:table>
						   </ContentTemplate>
                            </igtab:Tab>
                               <igtab:Tab Text="&nbsp;&nbsp;Associated Contacts&nbsp;&nbsp;" >
                                <ContentTemplate>
                              
								<asp:table id="tblCont" BorderWidth="1" Runat="server" CellPadding="0" CellSpacing="0" Width="100%" CssClass="aspTableDTL"
									BorderColor="black" GridLines="None" Height="300">
									<asp:TableRow>
										<asp:TableCell VerticalAlign="Top">
									
											<asp:datagrid id="dgContact" runat="server" CssClass="dg" Width="100%" BorderColor="white" AutoGenerateColumns="False">
												<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
												<ItemStyle CssClass="is"></ItemStyle>
												<HeaderStyle CssClass="hs"></HeaderStyle>
												<Columns>
													<asp:BoundColumn DataField="numContactId" Visible="false"></asp:BoundColumn>
													<asp:BoundColumn DataField="bitPartner" Visible="false"></asp:BoundColumn>
													<asp:BoundColumn HeaderText="Organization,Relationship" DataField="Company"></asp:BoundColumn>
													<asp:BoundColumn HeaderText="First & Last Name" DataField="Name"></asp:BoundColumn>
													<asp:BoundColumn HeaderText="Phone - Ext" DataField="Phone"></asp:BoundColumn>
													<asp:BoundColumn HeaderText="Email" DataField="Email"></asp:BoundColumn>
													<asp:BoundColumn HeaderText="Contact Role" DataField="ContactRole"></asp:BoundColumn>
												
														<asp:TemplateColumn HeaderText="Share Project via Partner Point ?" >
														<ItemTemplate>
														<asp:Label runat="server" CssClass="cell1"  ID="lblShare"></asp:Label>
														       
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:TemplateColumn  Visible="false">
														<HeaderTemplate>
															<asp:Button ID="btnHdeleteCnt" Runat="server" CssClass="Delete" Text="r"></asp:Button>
														</HeaderTemplate>
														<ItemTemplate>
															<asp:TextBox id=txtContactID Runat=server style="display:none" Text='<%# DataBinder.Eval(Container.DataItem, "numContactId") %>'>
															</asp:TextBox>
															<asp:Button ID="btnDeleteCnt" Runat="server" CssClass="Delete" Text="r" CommandName="Delete"></asp:Button>
															<asp:LinkButton ID="lnkDeleteCnt" Runat="server" Visible="false">
																<font color="#730000">*</font></asp:LinkButton>
														</ItemTemplate>
													</asp:TemplateColumn>
												</Columns>
											</asp:datagrid>
											<br>
										</asp:TableCell>
									</asp:TableRow>
								</asp:table>
							  </ContentTemplate>
                            </igtab:Tab>
                        </Tabs>
                        </igtab:ultrawebtab></td>
				</tr>
			</table>
			
			<div id="Layer3" style="Z-INDEX: 2; LEFT: 96px; VISIBILITY: hidden; POSITION: absolute; TOP: 88px"><asp:table id="Table11" Runat="server" BorderWidth="1" BackColor="white" CellSpacing="0" CellPadding="0"
					BorderColor="black" GridLines="None" Height="96px">
					<asp:TableRow>
						<asp:TableCell VerticalAlign="Top">
							<table cellSpacing="1" cellPadding="1" width="100%" border="0">
								<tr>
									<td class="normal1" colSpan="2"><b>&nbsp;Customer Information</b></td>
									<td vAlign="top" align="right" colSpan="4">
										<asp:button id="btnCusOk" CssClass="button" Runat="server" Text="Ok" Width="25"></asp:button></td>
								</tr>
								<tr>
									<td class="normal1" align="right">&nbsp;<b>Customer:</b></td>
									<td class="normal1">
										<asp:HyperLink ID="hplCustName" Runat="server" CssClass="hyperlink"></asp:HyperLink>
									</td>
									<td class="normal1" align="right">&nbsp;<b>Division:</b></td>
									<td>
										<asp:label id="lblcustdivision" runat="server" width="100px" cssclass="normal1"></asp:label></td>
									<td class="normal1" align="right">&nbsp;<b>Territory:</b></td>
									<td>
										<asp:label id="lbltrtry" runat="server" width="100px" cssclass="normal1"></asp:label></td>
								</tr>
								<tr>
									<td class="normal1" align="right">&nbsp;<b>Rating:</b></td>
									<td>
										<asp:label id="lblrat" runat="server" width="100px" cssclass="normal1"></asp:label></td>
									<td class="normal1" align="right">&nbsp;<b>Status:</b></td>
									<td>
										<asp:label id="lblstat" runat="server" width="100px" cssclass="normal1"></asp:label></td>
									<td class="normal1" align="right">&nbsp;<b>Industry:</b></td>
									<td>
										<asp:label id="lblind" runat="server" width="100px" cssclass="normal1"></asp:label></td>
								</tr>
								<tr>
									<td class="normal1" align="right">&nbsp;<b>Type:</b></td>
									<td>
										<asp:label id="lbltype" runat="server" width="100px" cssclass="normal1"></asp:label></td>
									<td class="normal1" align="right">&nbsp;<b>Credit:</b></td>
									<td>
										<asp:label id="lblcredit" runat="server" width="100px" cssclass="normal1"></asp:label></td>
									<td class="normal1" align="right">&nbsp;<b>Web:</b></td>
									<td>
										<asp:label id="lblweb" runat="server" width="100px" cssclass="normal1"></asp:label></td>
								</tr>
								<tr>
									<td class="normal1" align="right">&nbsp;<b>Profile:</b></td>
									<td>
										<asp:label id="lblprofile" runat="server" width="100px" cssclass="normal1"></asp:label></td>
									<td class="normal1" align="right">&nbsp;<b>Group:</b></td>
									<td>
										<asp:label id="lblgrp" runat="server" width="100px" cssclass="normal1"></asp:label></td>
								</tr>
							</table>
						</asp:TableCell>
					</asp:TableRow>
				</asp:table><br>
			</div>
			<table width="100%">
				<tr>
					<td class="normal4" align="center"><asp:literal id="litMessage" Runat="server"></asp:literal></td>
				</tr>
			</table>
			</ContentTemplate>
			</asp:updatepanel>
			
		</form>
	</body>
</HTML>
