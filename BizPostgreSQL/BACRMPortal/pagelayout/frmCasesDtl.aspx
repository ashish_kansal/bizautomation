<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmCasesDtl.aspx.vb" Inherits="BACRMPortal.frmCasesDtl"%>
<%@ Register Assembly="Infragistics35.WebUI.UltraWebTab.v8.1, Version=8.1.20081.2046, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebTab" TagPrefix="igtab" %>
<%@ Register TagPrefix="menu1" TagName="PartnerPoint" src="../Common/PartnerPoint.ascx" %>  
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" src="../include/calandar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1"    runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
       <LINK href="../css/lists.css" type="text/css" rel="stylesheet">
		<title>Case</title>
        <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
         <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)"> 

		<script>
		function openActionItem(a,b,c,d,e,f)
		{
		    if (e=='Email')
		    {
		        window.open("../contact/frmEmailMessage.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Email="+a+"&Date="+f,'','width=750,height=525,status=no,titlebar=no,scrollbars=yes,top=110,left=250')
			    return false;
		    }
		    else
		    {
		        window.location.href="../admin/actionitemdetails.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=Cases&CommId=" +a +"&CaseId=" +b+ "&CaseTimeId="+c+ "&CaseExpId=" +d;
		        return false;
		    }
		
		}
		function chkAll()
		{
		 for (i = 1; i <= 20; i++)
				{
				   var str;
				   if (i<10) 
				   {
				   str='0'+i
				   }
				   else
				   {
				   str=i
				   }
				   if (document.all['uwOppTab__ctl3_rptCorr_ctl00_chkDelete'].checked==true)
				   {
				     if (typeof(document.all['uwOppTab__ctl3_rptCorr_ctl'+str+'_chkADelete'])!='undefined')
				     {
				        document.all['uwOppTab__ctl3_rptCorr_ctl'+str+'_chkADelete'].checked=true;
				     }
				   }
				   else
				   {
				     if (typeof(document.all['uwOppTab__ctl3_rptCorr_ctl'+str+'_chkADelete'])!='undefined')
				     {
				        document.all['uwOppTab__ctl3_rptCorr_ctl'+str+'_chkADelete'].checked=false;
				     }
				   }
				}
		}
		
			function DeleteRecord()
				{
					if(confirm('Are you sure, you want to delete the selected record?'))
					{
						return true;
					}
					else
					{
						return false;
					}
				}
		function  OpenEmailMessage(a,b)
		{
			window.open("../contact/frmEmailMessage.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Email="+a+"&Date="+b,'','width=650,height=350,status=no,titlebar=no,scrollbar=yes,top=110,left=150')
			return false;
		}
		function  OpenItemDtls(OpID)
		{
			window.open("../Items/frmItemsforCases.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID="+OpID,'','width=650,height=350,status=no,titlebar=no,scrollbar=yes,top=110,left=150')
			return false;
		}
	function ShowWindow(Page,q,att) 
		{
			if (att=='show')
			{
				document.all[Page].style.visibility = "visible";
				//document.Form1.ddlStatus.style.display='none';
				return false;
		
			}
			if (att=='hide')
			{
				document.all[Page].style.visibility = "hidden";
				//document.Form1.ddlStatus.style.display='';
				return false;
		
			}
		}
		function openSol(a)
		{
		 window.open("../cases/frmCaseLinkSol.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&CaseID="+a ,'','width=800,height=500,status=no,titlebar=no,scrollbar=yes,top=110,left=150,resizable=yes')
//			document.all['cntDoc'].src="../cases/frmCaseLinkSol.aspx?CaseID="+a 
//			document.all['divSol'].style.visibility = "visible";
			return false;
			
		}
		function AddContact()
		{
			if (document.Form1.ddlcompany.value==0)
			{
				alert("Select Customer");
				tsCases.selectedIndex=2;
				document.Form1.ddlcompany.focus();
				return false;
			}
			if (document.Form1.ddlAssocContactId.value==0)
			{
				alert("Select Contact");
				tsCases.selectedIndex=2;
				document.Form1.ddlAssocContactId.focus();
				return false;
			}
			var str;
			for(i=0;i<document.Form1.elements.length;i++)
				{
				if (i<=9)
				{
				    str='0'+(i+1)
				}
				else
				{
				    str=i+1
				}
				if (typeof(document.Form1.all['dgContact_ctl'+str+'_txtContactID'])!='undefined')
					{
						if (document.Form1.all['dgContact_ctl'+str+'_txtContactID'].value==document.Form1.ddlAssocContactId.value)
							{
								alert("Associated contact is already added");
								return false;
							}
					} 
				}
			
		}
			function NewTask(a,b)
				{
//				alert("cntid : "+a);
//				alert("caseId : "+b);
				window.location="../Admin/newaction.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=Cases&CntID="+a+"&CaseID="+b;

				
				}
		function ShowLayout(a,b)
		{
			window.open("../pagelayout/frmCustomisePageLayout.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Ctype=S",'','toolbar=no,titlebar=no,top=200,scrollbars=yes,resizable=yes');
			return false;
		}
		</script>
	</HEAD>
	<body >
		
		<form id="Form1" method="post" runat="server">
    <menu1:PartnerPoint id="PartnerPoint1" runat="server"></menu1:PartnerPoint>
            <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>
		
		<asp:updatepanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" >
		<ContentTemplate>
			<table id="tblMenu" borderColor="black" cellSpacing="0" cellPadding="0" width="100%" border="0"
				runat="server">
				<tr>
				<td class="tr1" align="center"><b>Record Owner: </b>
						<asp:label id="lblRecordOwner" runat="server" ForeColor="Black"></asp:label></td>
						<td class="td1" width="1" height="18"></td>
					<td class="tr1" align="center"><b>Created By: </b>
						<asp:label id="lblCreatedBy" runat="server" ForeColor="Black"></asp:label></td>
					<td class="td1" width="1" height="18"></td>
					<td class="tr1" align="center"><b>Last Modified By: </b>
						<asp:label id="lblLastModifiedBy" runat="server" ForeColor="Black"></asp:label></td>
				</tr>
			</table>
			<table width="100%" align="center">
				<tr>
					<td>
						<table width="100%">
							<tr>
								<td class="normal1" align="center">Organization : <u>
								        <asp:HyperLink ID="hplCustomer" runat="server" CssClass="hyperlink"></asp:HyperLink>
								</td>
								<td class="normal1" align="right">Case Number :
								</td>
								<td class="normal1"><asp:label id="lblcasenumber" ForeColor="Red" Runat="server"></asp:label></td>
								<TD class="normal1" align="right">Status :</TD>
								<TD class="normal1"><asp:Label ID="lblStatus" runat="server" CssClass="norrmal1"></asp:Label></TD>
								<TD align="right">
									<asp:button id="btnLinkSol" Runat="server" CssClass="button" Text="Link Solution"></asp:button>
									<asp:button id="btnEdit" Runat="server" CssClass="button" Text="Edit" Width="50"></asp:button>
									
									<asp:button id="btnCancel" Runat="server" CssClass="button" Text="Close"></asp:button>
									<asp:Button ID="btnActDelete" Runat="server" CssClass="Delete" Text="r" ></asp:Button>
								</TD>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<asp:Panel runat="server" ID="pnlContract">
			<table width="100%" class="normal1" align="center">
				<tr>
					<td>
					       <table  width="100%">
							<tr>
							    <td width="10%" align="right">Contract :</td> 
							    <td width="20%"><asp:DropDownList ID="ddlContract" AutoPostBack="true" Width="180" runat="server"  CssClass="signup"></asp:DropDownList></td> 
							    <td width="10%" align="right">Amount Balance :</td>
							    <td><asp:Label runat="server" ID="lblRemAmount" CssClass="normal1" ></asp:Label></td>
							    <td width="10%" align="right">Days Remaining :</td>
							    <td><asp:Label runat="server" ID="lblRemDays" CssClass="normal1" ></asp:Label></td>
							    <td width="10%" align="right">Incidents Remaining :</td>
							    <td><asp:Label runat="server" ID="lblRemInci" CssClass="normal1" ></asp:Label></td>
							    <td width="10%" align="right">Hours Remaining :</td>
							    <td><asp:Label runat="server" ID="lblRemHours" CssClass="normal1" ></asp:Label></td>
							</tr>
						    </table>
					</td>
				</tr>
			</table>
			</asp:Panel>
			<igtab:ultrawebtab AutoPostBack="true"    ImageDirectory=""  id="uwOppTab" runat="server" ThreeDEffect="True" BorderStyle="Solid" Width="100%" BarHeight="0"  BorderWidth="0">
                        <DefaultTabStyle Height="23px" CssClass="InfraDefaultTabStyle">
                        </DefaultTabStyle>
                        <RoundedImage LeftSideWidth="7" RightSideWidth="8" ShiftOfImages="0" SelectedImage="../images/ig_tab_winXPs3.gif" NormalImage="../images/ig_tab_winXP3.gif" HoverImage="../images/ig_tab_winXPs3.gif" FillStyle="LeftMergedWithCenter" ></RoundedImage>
                       <SelectedTabStyle Height="23px"  CssClass="InfraHoverSelTabStyle"  >
                        </SelectedTabStyle>
                        <HoverTabStyle Height="23px"  CssClass="InfraHoverSelTabStyle"  >
                        </HoverTabStyle>
                        <Tabs>
                              <igtab:Tab Text="&nbsp;&nbsp;Case Details&nbsp;&nbsp;">
                                <ContentTemplate>
                               
		
						<asp:updatepanel ID="updatepanel3" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" >
		<ContentTemplate>
					<asp:table id="tblCase" CellPadding="1" CellSpacing="1" BorderWidth="1"  Height="300" Runat="server" CssClass="aspTableDTL"
						Width="100%" BorderColor="black" GridLines="None">
						<asp:TableRow >
							    <asp:TableCell  VerticalAlign="top">
									<asp:Table Width="100%" ID="tbl12" runat="server" >
									   <asp:TableRow>
									        <asp:TableCell ColumnSpan="2" HorizontalAlign="Right" >
									             <asp:button id="btnLayout" Runat="server" CssClass="button" Text="Layout" ></asp:button>
									        </asp:TableCell>
									   </asp:TableRow>
									   <asp:TableRow>
									       <asp:TableCell VerticalAlign="top">
                                                <img src="../images/SuitCase-32.gif" />
									       </asp:TableCell>
									        <asp:TableCell >
									            <asp:Table runat="server" ID="tabledetail"   BorderWidth="0"    GridLines="none" CellPadding="2" CellSpacing="0"   HorizontalAlign="Center" ></asp:Table>
									        </asp:TableCell>
									   </asp:TableRow>
									   <asp:TableRow>
									        <asp:TableCell ColumnSpan="2">
									            <asp:Table runat="server" ID="tableComment" BorderWidth="" Width="100%"  GridLines="none"   HorizontalAlign="Center" ></asp:Table>
									        </asp:TableCell>
									   </asp:TableRow>
									</asp:Table>							
									</asp:TableCell>
							    </asp:TableRow>
						        <asp:TableRow>
							<asp:TableCell VerticalAlign="Top">
								<br>
								<table width="100%">
								
									<asp:Panel ID="pnlKnowledgeBase" Runat="server">
										<TR  style="width:100%">
											<TD class="normal1" align="right"  width="10%" >Solutions added from<BR>
												the Knowledge Base</TD>
											<TD colSpan="5" >
												<asp:DataGrid id="dgSolution" runat="server" CssClass="dg" Width="100%" BorderColor="white" ShowFooter="False">
													<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
													<ItemStyle CssClass="is"></ItemStyle>
													<HeaderStyle CssClass="hs"></HeaderStyle>
												</asp:DataGrid>
												<br>
											</TD>
										</TR>
									</asp:Panel>
									</table>
									<table style="width:100%">
									<tr style="width:100%"><td width="10%"></td>
										<td colSpan="6" class="normal1">
											<asp:linkbutton id="lnkbtnNoofCom" Runat="server" CommandName="Show"></asp:linkbutton>&nbsp;&nbsp;&nbsp;
											<asp:linkbutton id="lnlbtnPost" Runat="server" CommandName="Post">Post Comment</asp:linkbutton>&nbsp;&nbsp;&nbsp;</td>
									</tr>
									
									<tr>
										<td colSpan="8">
											<asp:Table ID="tblComments" CellPadding="0" CellSpacing="0" Runat="server" Width="100%" Visible="False"></asp:Table></td>
									</tr>
									<asp:Panel ID="pnlPostComment" Runat="server" Visible="False">
										<TR>
											<TD class="normal1" align="right">Heading
											</TD>
											<TD colspan="5">
												<asp:TextBox id="txtHeading" Runat="server" Width="500" CssClass="signup"></asp:TextBox></TD>
										</TR>
										<TR>
											<TD class="normal1" align="right">Comment
											</TD>
											<TD colspan="5">
												<asp:TextBox id="txtComments" Runat="server" Width="500" CssClass="signup" Height="90" TextMode="MultiLine"></asp:TextBox></TD>
										</TR>
										<TR>
											<TD align="center" colSpan="6">
												<asp:Button id="btnSubmit" Runat="server" Width="50" CssClass="button" Text="Submit"></asp:Button></TD>
										</TR>
									</asp:Panel>
								</table>
								<br>
							</asp:TableCell>
						</asp:TableRow>
					</asp:table>
						
		            </ContentTemplate>
			</asp:updatepanel>
				 </ContentTemplate>
                            </igtab:Tab>
				             
				              <igtab:Tab Text="&nbsp;&nbsp;Associated Contacts&nbsp;&nbsp;">
                                <ContentTemplate>
                               
					<asp:updatepanel ID="updatepanel2" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" >
				<ContentTemplate>
					<asp:table id="Table1" BorderWidth="1" Height="300" Runat="server" Width="100%" BorderColor="black" CssClass="aspTableDTL"
						GridLines="None">
						<asp:TableRow>
							<asp:TableCell VerticalAlign="Top">
								
											<br />
											<asp:datagrid id="dgContact" runat="server" CssClass="dg" Width="100%" BorderColor="white" AutoGenerateColumns="False">
												<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
												<ItemStyle CssClass="is"></ItemStyle>
												<HeaderStyle CssClass="hs"></HeaderStyle>
												<Columns>
													<asp:BoundColumn DataField="numContactId" Visible="false"></asp:BoundColumn>
													<asp:BoundColumn DataField="bitPartner" Visible="false"></asp:BoundColumn>
													<asp:BoundColumn HeaderText="Organization,Relationship" DataField="Company"></asp:BoundColumn>
													<asp:BoundColumn HeaderText="First & Last Name" DataField="Name"></asp:BoundColumn>
													<asp:BoundColumn HeaderText="Phone - Ext" DataField="Phone"></asp:BoundColumn>
													<asp:BoundColumn HeaderText="Email" DataField="Email"></asp:BoundColumn>
													
													<asp:TemplateColumn HeaderText="Contact Role">
														<ItemTemplate>
															<asp:Label ID="Label1" runat="server" CssClass="normal1" Text='<%# DataBinder.Eval(Container.DataItem, "ContactRole") %>'></asp:Label>														
														</ItemTemplate>
													</asp:TemplateColumn >
														<asp:TemplateColumn HeaderText="Share Opportunity via Partner Point ?" >
														<ItemTemplate>
														<asp:Label ID="lblShare" runat="server" CssClass="cell"></asp:Label>														            
														</ItemTemplate>
													</asp:TemplateColumn>
													
												</Columns>
											</asp:datagrid>
											<br>
							</asp:TableCell>
						</asp:TableRow>	
					</asp:table>
					</ContentTemplate>
					</asp:updatepanel>
				 </ContentTemplate>
                            </igtab:Tab>
				              <igtab:Tab Text="&nbsp;&nbsp;Tasks&nbsp;&nbsp;">
                                <ContentTemplate>
                               
					    <asp:updatepanel ID="updatepanel5" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" >
				            <ContentTemplate>
					             <asp:table id="Table4" CellPadding="0" CellSpacing="0" Runat="server" BorderWidth="1" Width="100%" CssClass="aspTable"
									    GridLines="None" BorderColor="black" Height="300">
									   <asp:TableRow>
									    <asp:TableCell VerticalAlign="Top">
									           <table width="100%" border="0">
									            <tr align="center" valign="top">
									                    
														<td align="right">
														    <table>
														        <tr>
													                <td class="normal1" align="right">From</td>
													                <td align="left">
													                <BizCalendar:Calendar ID="Calendar1" runat="server" />
													                </td>
													                <td class="normal1" align="right">To</td>
													                <td>
														                <BizCalendar:Calendar ID="Calendar2" runat="server" />
                														
													                </td>
													                <td>
													                
													                </td>	
														        </tr>
														    </table>
														</td>
														<td align="right">
														    <table class="normal1">
														        <tr>
													                <td>
													                Search
													                </td>	
													                <td>
													                <asp:TextBox ID="txtSearchCorr" runat="server"  CssClass="signup" ></asp:TextBox>
													                </td>
													                <td>
													                <asp:DropDownList ID="ddlSrchCorr" runat="server" CssClass="signup">
													                    
        									                            <asp:ListItem Text="Tasks by Due Date" Value="5"></asp:ListItem>
        									                            <asp:ListItem Text="Tasks by Created Date" Value="6"></asp:ListItem>
        									                         
													                </asp:DropDownList>
													                </td>
													                <td>
													                <asp:button id="btnCorresGo"  Runat="server" CssClass="button" Text="Go"></asp:button>&nbsp;
													                <asp:button id="btnAddTask" Runat="server" CssClass="button" Text="Add Task"  ></asp:button>&nbsp;
													                <asp:button id="btnCorrDelete" Runat="server" CssClass="button" Text="Delete"></asp:button>
													                </td>
														        </tr>
														    </table>
														</td>
													</tr>
													<tr>
													    <td colspan="2" align="right" >
													        <table cellpadding="0" cellspacing="0">
											                    <tr >
												                    <td class="normal1">
        									                                Filter :
        									                                <asp:DropDownList ID="ddlFilterCorr" runat="server" AutoPostBack="true" CssClass="signup">
        									                               
        									                                <asp:ListItem Text="Tasks by Due Date" Value="5"></asp:ListItem>
        									                                <asp:ListItem Text="Tasks by Created Date" Value="6"></asp:ListItem>
        									                                
        									                                </asp:DropDownList>
									                                        </td>
									                                        <td align="center" class="normal1">&nbsp;&nbsp;&nbsp;No of Records :
														                    <asp:Label ID="lblNoOfRecordsCorr" Runat="server" CssClass="text"></asp:Label>&nbsp;&nbsp;&nbsp;</td>
													                    <td id="tdCorr" runat="server">
                    													
											                        <table >													
												                    <tr  >
													                    <td>
														                    <asp:label id="lblNextCorr" runat="server" cssclass="text_bold">&nbsp;&nbsp;&nbsp;Next:</asp:label></td>
													                    <td class="normal1">
														                    <asp:linkbutton id="lnk2Corr" runat="server" CausesValidation="False">2</asp:linkbutton></td>
													                    <td class="normal1">
														                    <asp:linkbutton id="lnk3Corr" runat="server" CausesValidation="False">3</asp:linkbutton></td>
													                    <td class="normal1">
														                    <asp:linkbutton id="lnk4Corr" runat="server" CausesValidation="False">4</asp:linkbutton></td>
													                    <td class="normal1">
														                    <asp:linkbutton id="lnk5Corr" runat="server" CausesValidation="False">5</asp:linkbutton></td>
													                    <td>
														                    <asp:linkbutton id="lnkFirstCorr" runat="server" CausesValidation="False">
															                    <div class="LinkArrow">9</div>
														                    </asp:linkbutton></td>
													                    <td>
														                    <asp:linkbutton id="lnkPreviousCorr" runat="server" CausesValidation="False">
															                    <div class="LinkArrow">3</div>
														                    </asp:linkbutton></td>
													                    <td class="normal1">
														                    <asp:label id="lblPageCorr" runat="server">Page</asp:label></td>
													                    <td>
														                    <asp:textbox id="txtCurrentPageCorr" runat="server" Text="1" Width="28px" CssClass="signup"
															                    MaxLength="5" AutoPostBack="true"></asp:textbox></td>
													                    <td class="normal1">
														                    <asp:label id="lblOfCorr" runat="server">of</asp:label></td>
													                    <td class="normal1">
														                    <asp:label id="lblTotalCorr" runat="server"></asp:label></td>
													                    <td>
														                    <asp:linkbutton id="lnkNextCorr" runat="server" CssClass="LinkArrow" CausesValidation="False">
															                    <div class="LinkArrow">4</div>
														                    </asp:linkbutton></td>
													                    <td>
														                    <asp:linkbutton id="lnkLastCorr" runat="server" CausesValidation="False">
															                    <div class="LinkArrow">:</div>
														                    </asp:linkbutton></td>
												                    </tr>
                    										
											                    </table>
											                    </td>
										                    </tr>
									                    </table>
													    </td>
													</tr>
												</table>
										
								            <table width="100%">
												<tr>
													<td>
													<asp:Repeater ID="rptCorr"  runat="server" >
													    
													    <HeaderTemplate>
													        <table cellspacing="0"  class="dg" width="100%">
													               <tr class="hs">
													                    <td style="display:none">
    													                   numEmailHstrId
													                    </td>
													                    <td style="display:none">
    													                   tintType
													                    </td>
													                    <td align="left" >
													                        <asp:LinkButton CommandName="Sort" ID="lnkDate" runat="server" ><font color="white">Date</font></asp:LinkButton>
													                    </td>
													                    <td align="left" >
													                         <asp:LinkButton CommandName="Sort" ID="lnkType" runat="server" ><font color="white">Type</font></asp:LinkButton>
													                    </td>
													                    <td align="left" >
													                       <asp:LinkButton CommandName="Sort" ID="lnkFrom" runat="server" ><font color="white">From ,To</font></asp:LinkButton>
													                    </td>
													                    <td align="left" >
													                        <asp:LinkButton CommandName="Sort" ID="lnkName" runat="server" ><font color="white">Name /Phone ,& Ext.</font></asp:LinkButton>
													                    </td>
													                    <td align="left" >
													                        <asp:LinkButton CommandName="Sort" ID="lnkAssigned" runat="server" ><font color="white">Assigned To</font></asp:LinkButton>
													                    </td>
													                    <td align="left" >
													                        <asp:CheckBox ID="chkDelete" onclick="chkAll()" runat="server"  />
													                    </td>
													               </tr>
													    </HeaderTemplate>
													    <AlternatingItemTemplate>
													          <tr  class="ais" align="center" >
													                    <td style="display:none">
    													                   
    													                   <%#Container.DataItem("numEmailHstrId")%>
													                    </td>
													                    <td style="display:none">
    													                   <%#Container.DataItem("tintType")%>
													                    </td>
													                    <td>
													                        <%#Container.DataItem("date")%>
													                    </td>
													                    <td>
													                    <asp:HyperLink ID="hplAType" CssClass="hyperlink" NavigateUrl="#"  onclick="openActionItem('<%#Container.DataItem("numEmailHstrId")%>','<%#Container.DataItem("caseid")%>','<%#Container.DataItem("CaseTimeId")%>','<%#Container.DataItem("CaseExpId")%>','<%#Container.DataItem("Type")%>','<%#Container.DataItem("dtCreatedDate")%>')"  runat="server" ><u style="cursor:hand"><%#Container.DataItem("Type")%></u></asp:HyperLink>
													                    </td>
													                    <td>
													                       <%#Container.DataItem("From")%>
													                    </td>
													                    <td>
													                        <%#Container.DataItem("Phone")%>
													                    </td>
													                    <td>
													                       <%#Container.DataItem("assignedto")%>
													                    </td>
													                <td>
													                    <asp:CheckBox ID="chkADelete" style="color:#C6D3E7;" runat="server" />
													                <asp:Label ID="lblDelete" Visible="false"  runat="server" Text='<%#Container.DataItem("DelData")%>' ></asp:Label>
													                </td>
													               </tr>
													               <tr  class="ais">
													                    <td colspan="8">
    													                     <%#Container.DataItem("Subject")%>
													                    </td>
													               </tr>
													    </AlternatingItemTemplate>
													    <ItemTemplate>
													          <tr  class="is">
													                    <td style="display:none">
    													                   
    													                   <%#Container.DataItem("numEmailHstrId")%>
													                    </td>
													                    <td style="display:none">
    													                   <%#Container.DataItem("tintType")%>
													                    </td>
													                    <td>
													                       <%#Container.DataItem("date")%>
													                    </td>
													                    <td>
													                    <asp:HyperLink ID="hplType" CssClass="hyperlink" NavigateUrl="#"  onclick="openActionItem('<%#Container.DataItem("numEmailHstrId")%>','<%#Container.DataItem("caseid")%>','<%#Container.DataItem("CaseTimeId")%>','<%#Container.DataItem("CaseExpId")%>','<%#Container.DataItem("Type")%>','<%#Container.DataItem("dtCreatedDate")%>')"  runat="server" ><u style="cursor:hand"><%#Container.DataItem("Type")%></u></asp:HyperLink>
													                    </td>
													                    <td>
													                       <%#Container.DataItem("From")%>
													                    </td>
													                    <td>
													                        <%#Container.DataItem("Phone")%>
													                    </td>
													                    <td>
													                       <%#Container.DataItem("assignedto")%>
													                    </td>
													                    <td>
													                    <asp:CheckBox ID="chkADelete"   runat="server" />
													                    <asp:Label ID="lblDelete" Visible="false"  runat="server" Text='<%#Container.DataItem("DelData")%>' ></asp:Label>
													                    </td>
													               </tr>
													               <tr  class="is">
													                    <td colspan="8">
    													                     <%#Container.DataItem("Subject")%>
													                    </td>
													               </tr>
													    </ItemTemplate>
													    <FooterTemplate>
													        </table>
													    </FooterTemplate>
													</asp:Repeater>
											</td>
											</tr>
											</table>
									    </asp:TableCell>
									   </asp:TableRow>
								    </asp:table>
								    <asp:textbox id="txtCorrTotalPage" style="DISPLAY: none" Runat="server"></asp:textbox>
			                        <asp:textbox id="txtCorrTotalRecords" style="DISPLAY: none" Runat="server"></asp:textbox>
				            </ContentTemplate>
					    </asp:UpdatePanel>
				 </ContentTemplate>
                            </igtab:Tab>
                             <igtab:Tab Text="&nbsp;&nbsp;Email History&nbsp;&nbsp;">
                                <ContentTemplate>
                          
				<asp:updatepanel ID="updatepanel1" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" >
				<ContentTemplate>
					<asp:table id="tblEmail" CellPadding="0" CellSpacing="0" BorderWidth="1" Height="300" Runat="server" CssClass="aspTableDTL"
						Width="100%" BorderColor="black" GridLines="None" >
						<asp:TableRow>
							<asp:TableCell VerticalAlign="Top">
								<br>
								<table width="100%" border="0">
									<tr>
										<td class="normal1" align="right">From</td>
										<td align="left">
											<BizCalendar:Calendar ID="calFrom" runat="server" />
										</td>
										<td class="normal1" align="right">To</td>
										<td>
											<BizCalendar:Calendar runat="server" ID="calTo" />
											
										</td>
										<td class="normal1">
											Search
											<asp:TextBox ID="txtSearchEmail" Runat="server" CssClass="signup"></asp:TextBox>
											&nbsp;
											<asp:DropDownList ID="ddlFields" Runat="server" Width="130" CssClass="signup">
												<asp:ListItem Value="0">All Email Fields</asp:ListItem>
												<asp:ListItem Value="1">Message Number</asp:ListItem>
												<asp:ListItem Value="2">From</asp:ListItem>
												<asp:ListItem Value="3">To</asp:ListItem>
												<asp:ListItem Value="4">Subject</asp:ListItem>
												<asp:ListItem Value="5">Body</asp:ListItem>
											</asp:DropDownList>
											&nbsp;
											<asp:button id="btnEmailGo" Runat="server" CssClass="button" Text="Go" Width="25"></asp:button>
										</td>
									</tr>
								</table>
								<table width="100%">
									<tr>
										<td align="center" class="normal1">No of messages :
											<asp:Label ID="lblEmailNoofRecords" Runat="server" CssClass="text"></asp:Label></td>
										<td id="tdEmailNav" runat="server">
											<table align="right">
												<tr>
													<td>
														<asp:label id="Label1" runat="server" cssclass="Text_bold">Next:</asp:label></td>
													<td class="normal1">
														<asp:linkbutton id="lnkEmail2" runat="server" CausesValidation="False">2</asp:linkbutton></td>
													<td class="normal1">
														<asp:linkbutton id="lnkEmal3" runat="server" CausesValidation="False">3</asp:linkbutton></td>
													<td class="normal1">
														<asp:linkbutton id="lnkEmail4" runat="server" CausesValidation="False">4</asp:linkbutton></td>
													<td class="normal1">
														<asp:linkbutton id="lnkEmail5" runat="server" CausesValidation="False">5</asp:linkbutton></td>
													<td>
														<asp:linkbutton id="lnkEmailFirst" runat="server" CausesValidation="False">
															<div class="LinkArrow">9</div>
														</asp:linkbutton></td>
													<td>
														<asp:linkbutton id="lnkEmailPrevious" runat="server" CausesValidation="False">
															<div class="LinkArrow">3</div>
														</asp:linkbutton></td>
													<td class="normal1">
														<asp:label id="Label2" runat="server">Page</asp:label></td>
													<td>
														<asp:textbox id="txtEmailPage" runat="server" Text="1" Width="28px" CssClass="signup" MaxLength="5"
															AutoPostBack="True"></asp:textbox></td>
													<td class="normal1">
														<asp:label id="Label3" runat="server">of</asp:label></td>
													<td class="normal1">
														<asp:label id="lblEmailPageNo" runat="server"></asp:label></td>
													<td>
														<asp:linkbutton id="lnkEmailNext" runat="server" CssClass="LinkArrow" CausesValidation="False">
															<div class="LinkArrow">4</div>
														</asp:linkbutton></td>
													<td>
														<asp:linkbutton id="linkEmailLast" runat="server" CausesValidation="False">
															<div class="LinkArrow">:</div>
														</asp:linkbutton></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
								<table width="100%">
									<tr>
										<td>
											<asp:datagrid id="dgEmail" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
												BorderColor="white" AllowSorting="True">
												<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
												<ItemStyle CssClass="is"></ItemStyle>
												<HeaderStyle CssClass="hs"></HeaderStyle>
												<Columns>
													<asp:BoundColumn DataField="CreatedOn" Visible="false"></asp:BoundColumn>
																<asp:BoundColumn DataField="numEmailHstrID" Visible="false"></asp:BoundColumn>
																<asp:BoundColumn DataField="bintCreatedOn" SortExpression="bintCreatedOn" HeaderText="<font color=white>Date/Time</font>"></asp:BoundColumn>
																<asp:BoundColumn DataField="vcFromEmail" SortExpression="vcFromEmail" HeaderText="<font color=white>From</font>"></asp:BoundColumn>
																<asp:BoundColumn DataField="vcMessageTo" SortExpression="vcEmail" HeaderText="<font color=white>To</font>"></asp:BoundColumn>
																<asp:BoundColumn DataField="vcSubject" SortExpression="vcSubject" HeaderText="<font color=white>Subject</font>"></asp:BoundColumn>
												</Columns>
												<PagerStyle Visible="False" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
											</asp:datagrid>
										</td>
									</tr>
								</table>
							</asp:TableCell>
						</asp:TableRow>
					</asp:table>
					</ContentTemplate>
					</asp:updatepanel>
			      </ContentTemplate>
                            </igtab:Tab>
                        </Tabs>
                        </igtab:ultrawebtab>
			<table width="100%">
				<tr>
					<td class="normal4" align="center">
						<asp:Literal ID="litMessage" Runat="server"></asp:Literal></td>
								<asp:textbox id="txtTotalPage" style="DISPLAY: none" Runat="server"></asp:textbox>
			<asp:textbox id="txtTotalRecords" style="DISPLAY: none" Runat="server"></asp:textbox>
			<asp:textbox id="txtDivId" style="DISPLAY: none" Runat="server"></asp:textbox>
			
				</tr>
			</table>
				<asp:textbox id="txtLstUptCom" style="DISPLAY: none" Runat="server"></asp:textbox>
			<asp:textbox id="txtEmailTotalPage" style="DISPLAY: none" Runat="server"></asp:textbox>
			<asp:textbox id="txtEmailTotalRecords" style="DISPLAY: none" Runat="server"></asp:textbox>
			</ContentTemplate>
			</asp:updatepanel>
		
			</form>
	</body>
</HTML>
