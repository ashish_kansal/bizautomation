Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Imports Infragistics.WebUI.UltraWebTab
Imports BACRM.BusinessLogic.Contacts
Partial Class frmCasedtl
    Inherits System.Web.UI.Page
    Dim SI As Integer = 0
    Dim SI1 As Integer = 0
    Dim SI2 As Integer = 0
    Dim frm As String = ""
    Dim frm1 As String = ""
    Dim frm2 As String = ""

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Dim lngCaseId As Long
    Dim m_aryRightsForPage() As Integer
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Session("ConID") = Nothing Then
            Response.Redirect("../Common/frmLogout.aspx")
        End If
        If Not GetQueryStringVal(Request.QueryString("enc"), "SI") Is Nothing Then
            SI = GetQueryStringVal(Request.QueryString("enc"), "SI")
        End If
        If Not GetQueryStringVal(Request.QueryString("enc"), "SI1") Is Nothing Then
            SI1 = GetQueryStringVal(Request.QueryString("enc"), "SI1")
        Else
            SI1 = 0
        End If
        If Not GetQueryStringVal(Request.QueryString("enc"), "SI2") Is Nothing Then
            SI2 = GetQueryStringVal(Request.QueryString("enc"), "SI2")
        Else
            SI2 = 0
        End If
        If Not GetQueryStringVal(Request.QueryString("enc"), "frm") Is Nothing Then
            frm = ""
            frm = GetQueryStringVal(Request.QueryString("enc"), "frm")
        Else
            frm = ""
        End If
        If Not GetQueryStringVal(Request.QueryString("enc"), "frm1") Is Nothing Then
            frm1 = ""
            frm1 = GetQueryStringVal(Request.QueryString("enc"), "frm1")
        Else
            frm1 = ""
        End If
        If Not GetQueryStringVal(Request.QueryString("enc"), "SI2") Is Nothing Then
            frm2 = ""
            frm2 = GetQueryStringVal(Request.QueryString("enc"), "frm2")
        Else
            frm2 = ""
        End If

        lngCaseId = Session("CaseID")
        LoadTableInformation()
        If Not IsPostBack Then
            Session("show") = 1
            m_aryRightsForPage = clsAuthorization.fn_ExternalPageLevelRights("frmCases.aspx", Session("ConID"), 15, 8)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../Common/frmAuthorization.aspx")
            End If
            'If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
            '    btnSave.Visible = False
            '    btnSaveClose.Visible = False
            'End If

        End If
        If Session("show") = 1 Then
            tblComments.Visible = True
            ShowComments()
        End If
        If Not IsPostBack Then
            If uwOppTab.Tabs.Count > SI Then
                uwOppTab.SelectedTabIndex = SI
            End If
        End If
        btnLayout.Attributes.Add("onclick", "return ShowLayout('e','" & lngCaseId & "');")
    End Sub
    Sub LoadTableInformation()
        Dim dtTableInfo As New DataTable
        Dim dtTablecust As New DataTable
        Dim ds As New DataSet
        Dim objPageLayout As New CPageLayout
        Dim fields() As String
        Dim idcolumn As String = ""

        Dim x As Integer
        'lngCaseId = GetQueryStringVal(Request.QueryString("enc"),"CaseID")

        objPageLayout.CoType = "e"
        objPageLayout.numUserCntID = Session("ConId")
        objPageLayout.RecordId = lngCaseId
        objPageLayout.DomainID = Session("DomainID")
        objPageLayout.PageId = 3
        ds = objPageLayout.GetTableInfoDefault()  ' getting the table structure 
        dtTableInfo = ds.Tables(0)

        'If ds.Tables.Count = 2 Then
        '    dtTablecust = ds.Tables(1)
        '    dtTableInfo.Merge(dtTablecust)
        'End If

        Dim dv As DataView
        dv = New DataView(dtTableInfo)
        Dim dtDetails As New DataTable
        objPageLayout.CaseID = lngCaseId
        objPageLayout.DomainID = Session("DomainID")
        objPageLayout.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
        dtDetails = objPageLayout.GetCaseDetails

        lblcasenumber.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("vcCaseNumber")), "", dtDetails.Rows(0).Item("vcCaseNumber"))
        lnkbtnNoofCom.Text = "  Comments (" & IIf(IsDBNull(dtDetails.Rows(0).Item("NoofCases")), "", dtDetails.Rows(0).Item("NoofCases")) & " )"

        lblStatus.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("numStatus")), "", dtDetails.Rows(0).Item("numStatus"))



        LoadCustomerInfo(dtDetails.Rows(0).Item("numContactID"))
        lblCreatedBy.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("CreatedBy")), "", dtDetails.Rows(0).Item("CreatedBy"))
        lblRecordOwner.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("RecOwner")), "", dtDetails.Rows(0).Item("RecOwner"))
        lblLastModifiedBy.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("ModifiedBy")), "", dtDetails.Rows(0).Item("ModifiedBy"))

        Dim numrows As Integer = dtTableInfo.Compute("Max(tintrow)", String.Empty)
        Dim numcells As Integer = dtTableInfo.Compute("Max(intcoulmn)", String.Empty)
        Dim nr As Integer
        For nr = 0 To numrows - 1
            Dim r As New TableRow()
            Dim nc As Integer
            Dim ro As Integer = nr
            For nc = 0 To numcells - 1

                dv.RowFilter = "tintrow =" & nr + 1 & "and intcoulmn=" & nc + 1
                If dv.Count > 0 Then
                    Dim dvi As Integer
                    For dvi = 0 To dv.Count - 1

                        Dim column1 As New TableCell
                        Dim column2 As New TableCell
                        Dim fieldId As Integer
                        fieldId = CInt(dv(dvi).Item("numFieldID").ToString)
                        Dim bitDynFld As Boolean
                        bitDynFld = dv(dvi).Item("bitCustomField")
                        column1.CssClass = "normal7"
                        If (bitDynFld <> True) Then
                            'column 1 data binding
                            column1.Text = IIf(IsDBNull(dv(dvi).Item("vcFieldName")), "", dv(dvi).Item("vcFieldName").ToString) & "&nbsp;:"

                            'column 2 data binding
                            If Not IsDBNull(dv(dvi).Item("vcDBColumnName").ToString) And dv(dvi).Item("vcDBColumnName").ToString() <> "" Then
                                Dim temp As String
                                temp = dv(dvi).Item("vcDBColumnName").ToString
                                fields = temp.Split(",")
                                Dim j As Integer
                                j = 0

                                While (j < fields.Length)
                                    If (fieldId = "223") Then

                                        Dim listvalue As String
                                        Dim value As String = ""
                                        listvalue = IIf(IsDBNull(dtDetails.Rows(0).Item(fields(j))), "", dtDetails.Rows(0).Item(fields(j)))

                                        Select Case fieldId   ' Must be a primitive data type
                                            Case 132
                                                If Not IsDBNull(dtDetails.Rows(0).Item("intTargetResolveDate")) Then
                                                    value = FormattedDateFromDate(IIf(IsDBNull(dtDetails.Rows(0).Item("intTargetResolveDate")), "", dtDetails.Rows(0).Item("intTargetResolveDate")), Session("DateFormat"))
                                                End If

                                            Case Else

                                        End Select
                                        column2.Text = value
                                    ElseIf (fieldId = "226") Then
                                        Dim l As New Label
                                        l.CssClass = "normal1"
                                        l.ForeColor = Color.Red
                                        l.Text = "U" & Format(IIf(IsDBNull(dtDetails.Rows(0).Item("numUniversalSupportKey")), "", dtDetails.Rows(0).Item("numUniversalSupportKey")), "00000000000")
                                        column2.Controls.Add(l)
                                    Else

                                        column2.Text = IIf(IsDBNull(dtDetails.Rows(0).Item(fields(j))), "", dtDetails.Rows(0).Item(fields(j)))

                                    End If
                                    j += 1
                                End While

                            Else
                                column1.Text = ""
                            End If ' end of table cell2

                        End If


                        column2.CssClass = "normal1"
                        column1.HorizontalAlign = HorizontalAlign.Right
                        column2.HorizontalAlign = HorizontalAlign.Left
                        column1.Width = 250
                        column2.Width = 300
                        column2.ColumnSpan = 1
                        column1.ColumnSpan = 1
                        r.Cells.Add(column1)
                        r.Cells.Add(column2)

                    Next dvi
                Else
                    Dim column1 As New TableCell
                    Dim column2 As New TableCell
                    column1.Text = ""
                    column2.Text = ""
                    r.Cells.Add(column1)
                    r.Cells.Add(column2)
                End If

            Next nc
            tabledetail.Rows.Add(r)
        Next nr

        If (dtDetails.Rows.Count > 0) Then
            Dim column1 As New TableCell
            Dim column2 As New TableCell
            Dim r As New TableRow
            column1.CssClass = "normal7"
            column2.CssClass = "normal1"
            column1.HorizontalAlign = HorizontalAlign.Right
            column2.HorizontalAlign = HorizontalAlign.Justify
            column2.ColumnSpan = 5
            Dim l As New Label
            l.CssClass = "normal7"
            l.Text = "Description" & "&nbsp;:"
            column1.Controls.Add(l)
            column2.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("textDesc")), "-", dtDetails.Rows(0).Item("textDesc"))
            column1.Width = 150
            r.Cells.Add(column1)
            r.Cells.Add(column2)
            tableComment.Rows.Add(r)
        End If


        Dim ObjSolution As New Solution
        Dim dtSolution As New DataTable
        ObjSolution.CaseID = lngCaseId
        dtSolution = ObjSolution.GetSolutionForCases
        If dtSolution.Rows.Count > 0 Then
            pnlKnowledgeBase.Visible = True
            dgSolution.DataSource = dtSolution
            dgSolution.DataBind()
        Else
            pnlKnowledgeBase.Visible = False
        End If

        Dim objCases As New CCases
        objCases.DomainID = Session("DomainID")
        objCases.CaseID = lngCaseId
        dgContact.DataSource = objCases.AssCntsByCaseID
        dgContact.DataBind()
    End Sub
    'Sub LoadInformation()
    '    Dim ObjCases As New CCases
    '    Dim dtCaseDetails As New DataTable
    '    ObjCases.CaseID = lngCaseId
    '    dtCaseDetails = ObjCases.GetCaseDetails
    '    Dim objCommon As New CCommon
    '    objCommon.sb_FillComboFromDBwithSel(ddlStatus, 14, Session("DomainID"))
    '    'objCommon.sb_FillComboFromDBwithSel(ddlOrgin, 15, Session("DomainID"))
    '    'objCommon.sb_FillComboFromDBwithSel(ddlPriority, 13, Session("DomainID"))
    '    'objCommon.sb_FillComboFromDBwithSel(ddlType, 16, Session("DomainID"))
    '    'objCommon.sb_FillComboFromDBwithSel(ddlCommonReasons, 17, Session("DomainID"))

    '    'If dtCaseDetails.Rows.Count > 0 Then
    '    '    If dtCaseDetails.Rows(0).Item("tintSupportKeyType") = 0 Then
    '    '        If Not IsDBNull(dtCaseDetails.Rows(0).Item("numUniversalSupportKey")) Then
    '    '            lblsupportkey.Text = "U" & Format(dtCaseDetails.Rows(0).Item("numUniversalSupportKey"), "00000000000")
    '    '        End If
    '    '    Else
    '    '        lblsupportkey.Text = "D" & Format(dtCaseDetails.Rows(0).Item("numOppID"), "00000000000")
    '    '    End If
    '    '    If dtCaseDetails.Rows(0).Item("numOppID") = 0 Then
    '    '        hplItemDTLS.Visible = False
    '    '    Else
    '    '        hplItemDTLS.Attributes.Add("onclick", "return OpenItemDtls(" & dtCaseDetails.Rows(0).Item("numOppID") & ")")
    '    '    End If
    '    lblcasenumber.Text = dtCaseDetails.Rows(0).Item("vcCaseNumber")
    '    'objCommon.sb_FillConEmpFromDBSel(ddlAssignedTo, Session("DomainID"), 1, IIf(IsDBNull(dtCaseDetails.Rows(0).Item("numAssignedTo")), 0, dtCaseDetails.Rows(0).Item("numAssignedTo")))
    '    'If Not IsDBNull(dtCaseDetails.Rows(0).Item("numAssignedTo")) Then
    '    '    If Not ddlAssignedTo.Items.FindByValue(dtCaseDetails.Rows(0).Item("numAssignedTo")) Is Nothing Then
    '    '        ddlAssignedTo.ClearSelection()
    '    '        ddlAssignedTo.Items.FindByValue(dtCaseDetails.Rows(0).Item("numAssignedTo")).Selected = True
    '    '    End If
    '    'End If
    '    LoadCustomerInfo(dtCaseDetails.Rows(0).Item("numContactID"))
    '    lnkbtnNoofCom.Text = "  Comments (" & dtCaseDetails.Rows(0).Item("NoofCases") & " )"
    '    'hLinkContact.Text = dtCaseDetails.Rows(0).Item("vcName")
    '    'hLinkEmailID.Text = dtCaseDetails.Rows(0).Item("vcEmail")
    '    'Label8.Text = dtCaseDetails.Rows(0).Item("Phone")
    '    'txtSubject.Text = IIf(IsDBNull(dtCaseDetails.Rows(0).Item("textSubject")), "", dtCaseDetails.Rows(0).Item("textSubject"))
    '    'If Not IsDBNull(dtCaseDetails.Rows(0).Item("intTargetResolveDate")) Then
    '    '    txtdisplaydate.Text = FormattedDateFromDate(dtCaseDetails.Rows(0).Item("intTargetResolveDate"), Session("DateFormat"))
    '    'End If
    '    If Not IsDBNull(dtCaseDetails.Rows(0).Item("numStatus")) Then
    '        If Not ddlStatus.Items.FindByValue(dtCaseDetails.Rows(0).Item("numStatus")) Is Nothing Then
    '            ddlStatus.ClearSelection()
    '            ddlStatus.Items.FindByValue(dtCaseDetails.Rows(0).Item("numStatus")).Selected = True
    '        End If
    '    End If
    '    'If Not IsDBNull(dtCaseDetails.Rows(0).Item("numPriority")) Then
    '    '    If Not ddlPriority.Items.FindByValue(dtCaseDetails.Rows(0).Item("numPriority")) Is Nothing Then
    '    '        ddlPriority.ClearSelection()
    '    '        ddlPriority.Items.FindByValue(dtCaseDetails.Rows(0).Item("numPriority")).Selected = True
    '    '    End If
    '    'End If
    '    'If Not IsDBNull(dtCaseDetails.Rows(0).Item("numType")) Then
    '    '    If Not ddlType.Items.FindByValue(dtCaseDetails.Rows(0).Item("numType")) Is Nothing Then
    '    '        ddlType.ClearSelection()
    '    '        ddlType.Items.FindByValue(dtCaseDetails.Rows(0).Item("numType")).Selected = True
    '    '    End If
    '    'End If
    '    'If Not IsDBNull(dtCaseDetails.Rows(0).Item("numOrigin")) Then
    '    '    If Not ddlOrgin.Items.FindByValue(dtCaseDetails.Rows(0).Item("numOrigin")) Is Nothing Then
    '    '        ddlOrgin.ClearSelection()
    '    '        ddlOrgin.Items.FindByValue(dtCaseDetails.Rows(0).Item("numOrigin")).Selected = True
    '    '    End If
    '    'End If
    '    'If Not IsDBNull(dtCaseDetails.Rows(0).Item("numOrigin")) Then
    '    '    If Not ddlCommonReasons.Items.FindByValue(dtCaseDetails.Rows(0).Item("numReason")) Is Nothing Then
    '    '        ddlCommonReasons.ClearSelection()
    '    '        ddlCommonReasons.Items.FindByValue(dtCaseDetails.Rows(0).Item("numReason")).Selected = True
    '    '    End If
    '    'End If
    '    'txtDescription.Text = IIf(IsDBNull(dtCaseDetails.Rows(0).Item("textDesc")), "", dtCaseDetails.Rows(0).Item("textDesc"))
    '    'txtIntComments.Text = IIf(IsDBNull(dtCaseDetails.Rows(0).Item("textInternalComments")), "", dtCaseDetails.Rows(0).Item("textInternalComments"))
    '    lblCustomerId.Text = "<u>" & dtCaseDetails.Rows(0).Item("vcCompanyName") & "</u>"
    '    LoadCustomerInfo(dtCaseDetails.Rows(0).Item("numContactID"))
    '    lblCreatedBy.Text = dtCaseDetails.Rows(0).Item("CreatedBy")
    '    lblLastModifiedBy.Text = dtCaseDetails.Rows(0).Item("ModifiedBy")
    '    'End If

    '    Dim dtContactInfo As New DataTable
    '    dtContactInfo = ObjCases.AssCntByCaseId
    '    dgContact.DataSource = dtContactInfo
    '    dgContact.DataBind()
    '    Session("dtContactInfo") = dtContactInfo

    '    Dim ObjSolution As New Solution
    '    Dim dtSolution As New DataTable
    '    ObjSolution.CaseID = lngCaseId
    '    dtSolution = ObjSolution.GetSolutionForCases
    '    If dtSolution.Rows.Count > 0 Then
    '        pnlKnowledgeBase.Visible = True
    '        dgSolution.DataSource = dtSolution
    '        dgSolution.DataBind()
    '    Else
    '        pnlKnowledgeBase.Visible = False
    '    End If

    'End Sub

    Sub LoadCustomerInfo(ByVal intContactID As Integer)
        Try


            Dim intCRMType, intCntId, intCmpId, intDivId As Integer
            Dim dtCompanyInfo As New DataTable
            Dim objContacts As New CContacts(Session("UserId"))
            objContacts.ContactID = intContactID
            dtCompanyInfo = objContacts.GetCompnyDetailsByConID
            If Session("EnableIntMedPage") = 1 Then
                hplCustName.NavigateUrl = "../pagelayout/frmAccountdtl.aspx"
            Else
                hplCustName.NavigateUrl = "../common/frmAccounts.aspx"
            End If


            If dtCompanyInfo.Rows.Count > 0 Then
                intCRMType = dtCompanyInfo.Rows(0).Item("tintCRMType")
                intCmpId = dtCompanyInfo.Rows(0).Item("numCompanyID")
                intDivId = dtCompanyInfo.Rows(0).Item("numDivisionID")
                intCntId = dtCompanyInfo.Rows(0).Item("numContactID")
                If Session("EnableIntMedPage") = 1 Then
                    hplCustName.NavigateUrl = "../pagelayout/frmAccountdtl.aspx?frm=CaseDetails&frm1=" & GetQueryStringVal(Request.QueryString("enc"), "frm")
                Else
                    hplCustName.NavigateUrl = "../Common/frmAccounts.aspx?frm=CaseDetails&frm1=" & GetQueryStringVal(Request.QueryString("enc"), "frm")
                End If

                lblCustomerId.Text = dtCompanyInfo.Rows(0).Item("vcCompanyName")
                lblcustdivision.Text = dtCompanyInfo.Rows(0).Item("vcDivisionName")
                hplCustName.Text = dtCompanyInfo.Rows(0).Item("vcCompanyName")
                If IsDBNull(dtCompanyInfo.Rows(0).Item("Territory")) = False Then lbltrtry.Text = dtCompanyInfo.Rows(0).Item("Territory")
                If IsDBNull(dtCompanyInfo.Rows(0).Item("Rating")) = False Then lblrat.Text = dtCompanyInfo.Rows(0).Item("Rating")
                If IsDBNull(dtCompanyInfo.Rows(0).Item("Status")) = False Then lblstat.Text = dtCompanyInfo.Rows(0).Item("Status")
                If IsDBNull(dtCompanyInfo.Rows(0).Item("industry")) = False Then lblind.Text = dtCompanyInfo.Rows(0).Item("industry")
                If IsDBNull(dtCompanyInfo.Rows(0).Item("Type")) = False Then lbltype.Text = dtCompanyInfo.Rows(0).Item("Type")
                If IsDBNull(dtCompanyInfo.Rows(0).Item("Credit")) = False Then lblcredit.Text = dtCompanyInfo.Rows(0).Item("Credit")
                If IsDBNull(dtCompanyInfo.Rows(0).Item("vcwebsite")) = False Then lblweb.Text = dtCompanyInfo.Rows(0).Item("vcwebsite")
                If IsDBNull(dtCompanyInfo.Rows(0).Item("vcprofile")) = False Then lblprofile.Text = dtCompanyInfo.Rows(0).Item("vcprofile")
                If IsDBNull(dtCompanyInfo.Rows(0).Item("grpname")) = False Then lblgrp.Text = dtCompanyInfo.Rows(0).Item("grpname")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub




    Sub FormatContacts()
        Try
            Dim dtContact As New DataTable
            dtContact = Session("dtContactInfo")
            Dim i As Integer = 0
            Dim dgConItem As DataGridItem
            dtContact = Session("dtContactInfo")
            For Each dgConItem In dgContact.Items
                dtContact.Rows(i).Item("ContRoleId") = CType(dgConItem.FindControl("ddlContactRole"), DropDownList).SelectedValue
                dtContact.Rows(i).Item("bitPartner") = IIf(CType(dgConItem.FindControl("chkShare"), CheckBox).Checked = True, 1, 0)
                i = i + 1
            Next
            Session("dtContactInfo") = dtContact
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub FillContact(ByVal ddlCombo As DropDownList, ByVal lngDivision As Long)
        Dim fillCombo As New COpportunities
        With fillCombo
            .DivisionID = lngDivision
            ddlCombo.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
            ddlCombo.DataTextField = "Name"
            ddlCombo.DataValueField = "numcontactId"
            ddlCombo.DataBind()
        End With
        ddlCombo.Items.Insert(0, New ListItem("---Select One---", "0"))
    End Sub

    'Private Sub ddlcompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlcompany.SelectedIndexChanged
    '    Try
    '        FillContact(ddlAssocContactId, CInt(ddlcompany.SelectedItem.Value))
    '    Catch ex As Exception
    '        Response.Write(ex)
    '    End Try
    'End Sub

    Private Sub dgContact_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgContact.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then


                If e.Item.Cells(1).Text = 1 Then
                    CType(e.Item.FindControl("lblShare"), Label).Text = "a"
                    CType(e.Item.FindControl("lblShare"), Label).CssClass = "cell1"
                ElseIf e.Item.Cells(1).Text <> 1 Then
                    CType(e.Item.FindControl("lblShare"), Label).Text = "r"
                    CType(e.Item.FindControl("lblShare"), Label).CssClass = "cell"
                End If

            End If
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub


    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        ''Response.Redirect("../Cases/frmCaseList.aspx")
        PageRedirect()
    End Sub
    Private Sub PageRedirect()
        If GetQueryStringVal(Request.QueryString("enc"), "frm") = "CaseList" Then
            Response.Redirect("../Cases/frmCaseList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
        Else
            Response.Redirect("../Cases/frmCaseList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
        End If
    End Sub
    Private Sub lnkbtnNoofCom_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles lnkbtnNoofCom.Command
        If e.CommandName = "Show" Then
            tblComments.Visible = True
            pnlPostComment.Visible = False
            ShowComments()
            Session("show") = 1
            LoadTableInformation()
        End If
    End Sub

    Private Sub lnlbtnPost_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles lnlbtnPost.Command
        If e.CommandName = "Post" Then
            tblComments.Visible = False
            pnlPostComment.Visible = True
            txtComments.Text = ""
            txtHeading.Text = ""
            txtLstUptCom.Text = ""
            LoadTableInformation()
        End If
    End Sub

    Sub ShowComments()
        tblComments.Rows.Clear()
        Dim dtCaseComments As New DataTable
        Dim objCases As New CCases
        objCases.CaseID = lngCaseId
        objCases.byteMode = 0
        dtCaseComments = objCases.ManageCaseComments
        Dim i As Integer
        If dtCaseComments.Rows.Count > 0 Then
            Dim tblRow As TableRow
            Dim tblCell As TableCell
            Dim lbl As Label
            Dim btnUpdate As Button
            Dim k As Integer = 0
            For i = 0 To dtCaseComments.Rows.Count - 1

                tblRow = New TableRow
                If k <> 1 Then
                    tblRow.CssClass = "tr1"
                End If
                tblCell = New TableCell
                tblCell.Text = dtCaseComments.Rows(i).Item("vcHeading")
                tblCell.CssClass = "text_bold"
                tblRow.Cells.Add(tblCell)
                tblCell = New TableCell
                If dtCaseComments.Rows(i).Item("numCommentID") = IIf(txtLstUptCom.Text = "", 0, txtLstUptCom.Text) Then
                    btnUpdate = New Button
                    btnUpdate.Text = "Edit"
                    AddHandler btnUpdate.Click, AddressOf Me.btnUpdate_Click
                    btnUpdate.Width = Unit.Pixel(50)
                    btnUpdate.ID = "btnUpdate~" & dtCaseComments.Rows(i).Item("numCommentID")
                    btnUpdate.CssClass = "button"
                    tblCell.Controls.Add(btnUpdate)
                End If
                lbl = New Label
                lbl.Text = "&nbsp;&nbsp;"
                tblCell.Controls.Add(lbl)

                'btnDelete = New Button
                'btnDelete.Text = "Delete"
                'AddHandler btnDelete.Click, AddressOf Me.btnDeleteComm_Click
                'btnDelete.Width = Unit.Pixel(50)
                'btnDelete.ID = dtCaseComments.Rows(i).Item("numCommentID") * 100
                'btnDelete.CssClass = "button"
                'tblCell.Controls.Add(btnDelete)
                'lbl = New Label
                'lbl.Text = "&nbsp;&nbsp;"
                'tblCell.Controls.Add(lbl)

                tblCell.HorizontalAlign = HorizontalAlign.Right
                tblRow.Cells.Add(tblCell)
                tblComments.Rows.Add(tblRow)

                tblRow = New TableRow
                If k <> 1 Then
                    tblRow.CssClass = "tr1"
                End If
                tblCell = New TableCell
                tblCell.ColumnSpan = 2
                tblCell.Text = dtCaseComments.Rows(i).Item("txtComments")
                tblCell.CssClass = "normal1"
                tblRow.Cells.Add(tblCell)
                tblComments.Rows.Add(tblRow)
                If k <> 0 Then
                    k = 0
                Else
                    k = 1
                End If
            Next
        End If
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        pnlPostComment.Visible = False
        tblComments.Visible = True
        Dim objCases As New CCases
        objCases.CaseID = lngCaseId
        objCases.Comments = txtComments.Text
        objCases.Heading = txtHeading.Text
        objCases.ContactID = Session("ConID")
        objCases.byteMode = 1
        If txtLstUptCom.Text <> "" Then
            objCases.CommentID = txtLstUptCom.Text
        End If
        txtLstUptCom.Text = objCases.ManageCaseComments().Rows(0).Item(0)
        ShowComments()
        LoadTableInformation()
    End Sub

    Sub btnDeleteComm_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objCases As New CCases
        objCases.CaseID = lngCaseId
        objCases.byteMode = 2
        objCases.CommentID = (sender.id) / 100
        objCases.ManageCaseComments()
        ShowComments()
        LoadTableInformation()
    End Sub

    Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pnlPostComment.Visible = True
        tblComments.Visible = False
    End Sub

    'Private Sub btnCompGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCompGo.Click
    '    Try
    '        FillCustomer(ddlcompany, CStr(txtComp.Text))
    '    Catch ex As Exception
    '        Response.Write(ex)
    '    End Try
    'End Sub

    Sub FillCustomer(ByVal ddlCombo As DropDownList, ByVal strName As String)
        Dim fillCombo As New COpportunities
        With fillCombo
            .DomainID = Session("DomainID")
            .UserCntID = Session("ContID")
            .CompFilter = Trim(strName) & "%"
            ddlCombo.DataSource = fillCombo.ListCustomer().Tables(0).DefaultView
            ddlCombo.DataTextField = "vcCompanyname"
            ddlCombo.DataValueField = "numDivisionID"
            ddlCombo.DataBind()
        End With
        ddlCombo.Items.Insert(0, New ListItem("--Select One--", "0"))
    End Sub

    'Private Sub btnAddContact_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddContact.Click
    '    Try
    '        Dim dtContactInfo As New DataTable
    '        Dim objOpportunity As New MOpportunity
    '        objOpportunity.ContactID = CInt(ddlAssocContactId.SelectedItem.Value)
    '        dtContactInfo = objOpportunity.ContactInfo
    '        If Session("dtContactInfo") Is Nothing Then
    '            Session("dtContactInfo") = dtContactInfo
    '        Else
    '            If dtContactInfo.Rows.Count <> 0 Then
    '                Dim dtNw As New DataTable
    '                dtNw = Session("dtContactInfo")
    '                Dim dsNew As DataRow
    '                dsNew = dtNw.NewRow
    '                dsNew.Item(0) = dtContactInfo.Rows(0).Item(0)
    '                dsNew.Item(1) = dtContactInfo.Rows(0).Item(1)
    '                dsNew.Item(2) = dtContactInfo.Rows(0).Item(2)
    '                dsNew.Item(3) = dtContactInfo.Rows(0).Item(3)
    '                dsNew.Item(4) = dtContactInfo.Rows(0).Item(4)
    '                dsNew.Item(5) = dtContactInfo.Rows(0).Item(5)
    '                dsNew.Item(6) = dtContactInfo.Rows(0).Item(6)
    '                dsNew.Item(7) = dtContactInfo.Rows(0).Item(7)
    '                dtNw.Rows.Add(dsNew)
    '                Session("dtContactInfo") = dtNw
    '            End If
    '        End If
    '        BindContactInfo()
    '    Catch ex As Exception
    '        Response.Write(ex)
    '    End Try
    'End Sub

    Sub BindContactInfo()
        Try
            Dim dtContactInfo As New DataTable
            dtContactInfo = Session("dtContactInfo")
            dgContact.DataSource = dtContactInfo
            dgContact.DataBind()
            Session("dtContactInfo") = dtContactInfo
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Response.Redirect("../cases/frmcases.aspx?frm=casedtl" & "&SI=" & uwOppTab.SelectedTabIndex & "&SI1=" & uwOppTab.SelectedTabIndex & "&SI2=" & SI1 & "&frm1=" & frm & "&frm2=" & frm1)
    End Sub
End Class
