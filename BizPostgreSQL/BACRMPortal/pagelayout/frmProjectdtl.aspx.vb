
'***************************************************************************************************************************
'     Author Name				 :  Anoop Jayaraj
'     Date Written				 :  20/3/2005
'***************************************************************************************************************************
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts


Public Class frmProjectdtl
    Inherits System.Web.UI.Page
    Dim SI As Integer = 0
    Dim SI1 As Integer = 0
    Dim SI2 As Integer = 0
    Dim frm As String = ""
    Dim frm1 As String = ""
    Dim frm2 As String = ""
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Dim myRow As DataRow
    Dim arrOutPut() As String = New String(2) {}
    Dim m_aryRightsForAssContacts(), m_aryRightsForPage() As Integer
    Dim lngProID As Long

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not GetQueryStringVal(Request.QueryString("enc"), "SI") Is Nothing Then
                SI = GetQueryStringVal(Request.QueryString("enc"), "SI")
            End If
            If Not GetQueryStringVal(Request.QueryString("enc"), "SI1") Is Nothing Then
                SI1 = GetQueryStringVal(Request.QueryString("enc"), "SI1")
            Else
                SI1 = 0
            End If
            If Not GetQueryStringVal(Request.QueryString("enc"), "SI2") Is Nothing Then
                SI2 = GetQueryStringVal(Request.QueryString("enc"), "SI2")
            Else
                SI2 = 0
            End If
            If Not GetQueryStringVal(Request.QueryString("enc"), "frm") Is Nothing Then
                frm = ""
                frm = GetQueryStringVal(Request.QueryString("enc"), "frm")
            Else
                frm = ""
            End If
            If Not GetQueryStringVal(Request.QueryString("enc"), "frm1") Is Nothing Then
                frm1 = ""
                frm1 = GetQueryStringVal(Request.QueryString("enc"), "frm1")
            Else
                frm1 = ""
            End If
            If Not GetQueryStringVal(Request.QueryString("enc"), "SI2") Is Nothing Then
                frm2 = ""
                frm2 = GetQueryStringVal(Request.QueryString("enc"), "frm2")
            Else
                frm2 = ""
            End If
            lngProID = Session("ProID")
            '  btnAddContact.Attributes.Add("onclick", "return AddContact()")
            'hplDocuments.Attributes.Add("onclick", "return OpenDocuments(" & lngProID & ");")
            btnCusOk.Attributes.Add("onclick", "return ShowWindow1('Layer3','','hide',0)")
            btnLayout.Attributes.Add("onclick", "return ShowLayout('j','" & lngProID & "');")

            If Not IsPostBack Then
                LoadTableInformation()
                Dim objContacts As New CContacts
                objContacts.RecID = lngProID
                objContacts.Type = "P"
                objContacts.UserCntID = Session("ConID")
                objContacts.AddVisiteddetails()

                Session("Help") = "Project"


                LoadSalesProcess()
                If GetQueryStringVal(Request.QueryString("enc"), "SelectedIndex") <> "" Then
                    uwOppTab.SelectedTabIndex = GetQueryStringVal(Request.QueryString("enc"), "SelectedIndex")
                End If
                If lngProID <> 0 Then
                    LoadSavedInformation()

                    tblMenu.Visible = True
                    'If ViewState("MileCheck") <> 1 Then
                    '    trSalesProcess.Visible = False
                    'End If


                Else


                    tblMenu.Visible = False
                End If
            End If
            Dim strDate As String = FormattedDateFromDate(Now(), Session("DateFormat"))
            If Not Session("ProProcessDetails") Is Nothing Then
                CreatMilestone()
            End If
            If Not IsPostBack Then
                If uwOppTab.Tabs.Count > SI Then
                    uwOppTab.SelectedTabIndex = SI
                End If
            End If

        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadTableInformation()
        Dim dtTableInfo As New DataTable
        Dim ds As New DataSet
        Dim objPageLayout As New CPageLayout

        Dim fields() As String
        Dim idcolumn As String = ""

        Dim x As Integer



        objPageLayout.CoType = "j"
        objPageLayout.numUserCntID = Session("ConId")
        objPageLayout.RecordId = lngProID
        objPageLayout.DomainID = Session("DomainID")
        ds = objPageLayout.GetTableInfoDefault()  ' getting the table structure 
        dtTableInfo = ds.Tables(0)


        Dim dtDetails As New DataTable
        objPageLayout.ProjectId = lngProID
        objPageLayout.DomainID = Session("DomainID")
        objPageLayout.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
        dtDetails = objPageLayout.ProjectDtlPL
        lblTProgress.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("TProgress")), 0, dtDetails.Rows(0).Item("TProgress").ToString)


        Dim dv As DataView
        dv = New DataView(dtTableInfo)
        Dim numrows As Integer = dtTableInfo.Compute("Max(tintrow)", String.Empty)
        Dim numcells As Integer = dtTableInfo.Compute("Max(intcoulmn)", String.Empty)
        Dim nr As Integer
        For nr = 0 To numrows - 1
            Dim r As New TableRow()
            Dim nc As Integer
            Dim ro As Integer = nr
            For nc = 0 To numcells - 1

                dv.RowFilter = "tintrow =" & nr + 1 & "and intcoulmn=" & nc + 1
                If dv.Count > 0 Then
                    Dim dvi As Integer
                    For dvi = 0 To dv.Count - 1

                        Dim column1 As New TableCell
                        Dim column2 As New TableCell
                        Dim fieldId As Integer
                        fieldId = CInt(dv(dvi).Item("numFieldID").ToString)
                        column1.CssClass = "normal7"
                        column1.Text = dv(dvi).Item("vcFieldName").ToString & "&nbsp;:"


                        If Not IsDBNull(dv(dvi).Item("vcDBColumnName").ToString) And dv(dvi).Item("vcDBColumnName").ToString() <> "" Then
                            Dim temp As String
                            temp = dv(dvi).Item("vcDBColumnName").ToString
                            fields = temp.Split(",")
                            Dim j As Integer
                            j = 0

                            While (j < fields.Length)
                                If (fieldId = "215") Then

                                    Dim listvalue As String
                                    Dim value As String = ""
                                    listvalue = IIf(IsDBNull(dtDetails.Rows(0).Item(fields(j))), "", dtDetails.Rows(0).Item(fields(j)))

                                    Select Case fieldId   ' Must be a primitive data type
                                        Case 215
                                            If Not IsDBNull(dtDetails.Rows(0).Item("intDueDate")) Then
                                                value = FormattedDateFromDate(dtDetails.Rows(0).Item("intDueDate"), Session("DateFormat"))
                                            End If

                                        Case Else
                                    End Select
                                    column2.Text = value
                                Else

                                    column2.Text = IIf(IsDBNull(dtDetails.Rows(0).Item(fields(j))), "", dtDetails.Rows(0).Item(fields(j)))

                                End If
                                j += 1
                            End While

                        Else
                            column1.Text = ""
                        End If ' end of table cell2

                        column2.CssClass = "normal1"
                        column1.HorizontalAlign = HorizontalAlign.Right
                        column2.HorizontalAlign = HorizontalAlign.Left


                        column1.Width = 250
                        column2.Width = 300
                        column2.ColumnSpan = 1
                        column1.ColumnSpan = 1
                        r.Cells.Add(column1)
                        r.Cells.Add(column2)
                    Next dvi
                Else
                    Dim column1 As New TableCell
                    Dim column2 As New TableCell
                    column1.Text = ""
                    column2.Text = ""
                    r.Cells.Add(column1)
                    r.Cells.Add(column2)
                End If

            Next nc
            tabledetail.Rows.Add(r)
        Next nr

        If (dtDetails.Rows.Count > 0) Then
            Dim column1 As New TableCell
            Dim column2 As New TableCell
            Dim r As New TableRow
            column1.CssClass = "normal7"
            column2.CssClass = "normal1"
            column1.HorizontalAlign = HorizontalAlign.Right
            column2.HorizontalAlign = HorizontalAlign.Justify
            column2.ColumnSpan = 5
            Dim l As New Label
            l.CssClass = "normal7"
            l.Text = "Comments" & "&nbsp;:"
            column1.Controls.Add(l)
            column2.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("txtComments")), "-", dtDetails.Rows(0).Item("txtComments"))
            column1.Width = 150
            r.Cells.Add(column1)
            r.Cells.Add(column2)
            tableComment.Rows.Add(r)
        End If
    End Sub
    Sub LoadCustomerInfo(ByVal inDivID As Integer)
        Dim dtCompanyInfo As New DataTable
        Dim objContacts As New CContacts
        objContacts.DivisionID = inDivID
        dtCompanyInfo = objContacts.GetCompnyDetailsByConID
        If dtCompanyInfo.Rows.Count > 0 Then
            If Session("EnableIntMedPage") = 1 Then
                hplCustName.NavigateUrl = "../pagelayout/frmAccountdtl.aspx?frm=ProjectDetails&frm1=" & GetQueryStringVal(Request.QueryString("enc"), "frm")
            Else
                hplCustName.NavigateUrl = "../common/frmAccounts.aspx?frm=ProjectDetails&frm1=" & GetQueryStringVal(Request.QueryString("enc"), "frm")
            End If

            lblOrg.Text = dtCompanyInfo.Rows(0).Item("vcCompanyName")
            lblcustdivision.Text = dtCompanyInfo.Rows(0).Item("vcDivisionName")
            hplCustName.Text = dtCompanyInfo.Rows(0).Item("vcCompanyName")
            If IsDBNull(dtCompanyInfo.Rows(0).Item("Territory")) = False Then lbltrtry.Text = dtCompanyInfo.Rows(0).Item("Territory")
            If IsDBNull(dtCompanyInfo.Rows(0).Item("Rating")) = False Then lblrat.Text = dtCompanyInfo.Rows(0).Item("Rating")
            If IsDBNull(dtCompanyInfo.Rows(0).Item("Status")) = False Then lblstat.Text = dtCompanyInfo.Rows(0).Item("Status")
            If IsDBNull(dtCompanyInfo.Rows(0).Item("industry")) = False Then lblind.Text = dtCompanyInfo.Rows(0).Item("industry")
            If IsDBNull(dtCompanyInfo.Rows(0).Item("Type")) = False Then lbltype.Text = dtCompanyInfo.Rows(0).Item("Type")
            If IsDBNull(dtCompanyInfo.Rows(0).Item("Credit")) = False Then lblcredit.Text = dtCompanyInfo.Rows(0).Item("Credit")
            If IsDBNull(dtCompanyInfo.Rows(0).Item("vcwebsite")) = False Then lblweb.Text = dtCompanyInfo.Rows(0).Item("vcwebsite")
            If IsDBNull(dtCompanyInfo.Rows(0).Item("vcprofile")) = False Then lblprofile.Text = dtCompanyInfo.Rows(0).Item("vcprofile")
            If IsDBNull(dtCompanyInfo.Rows(0).Item("grpname")) = False Then lblgrp.Text = dtCompanyInfo.Rows(0).Item("grpname")

        End If
    End Sub



    Sub LoadSavedInformation()
        Try

            Dim objProject As New ProjectsList
            Dim dtDetails As DataTable
            objProject.ProjectId = lngProID
            objProject.DomainID = Session("DomainID")
            objProject.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            dtDetails = objProject.ProjectDetail
            LoadCustomerInfo(dtDetails.Rows(0).Item("numDivisionID"))
            'txtName.Text = dtDetails.Rows(0).Item("vcProjectName")
            lblLastModifiedBy.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("vcModifiedby")), "", dtDetails.Rows(0).Item("vcModifiedby"))
            lblCreatedBy.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("vcCreatedBy")), "", dtDetails.Rows(0).Item("vcCreatedBy"))
            lblRecordOwner.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("vcRecOwner")), "", dtDetails.Rows(0).Item("vcRecOwner"))
            ' txtOComments.Text = dtDetails.Rows(0).Item("txtComments")
            ' lblDocCount.Text = "(" & dtDetails.Rows(0).Item("DocumentCount") & ")"
            LoadContacts(dtDetails.Rows(0).Item("numDivisionId"))

            'loading the Opportunities Dropdown

            dgContact.DataSource = objProject.AssCntsByProId
            dgContact.DataBind()


            Dim dtMilestone As DataTable
            objProject.DomainID = Session("DomainId")
            objProject.ContactID = Session("ConID")
            dtMilestone = objProject.SalesProcessDtlByProId
            Session("ProProcessDetails") = dtMilestone
            If dtMilestone.Rows.Count = 0 Then
                ViewState("MileCheck") = 1
            End If
            CreatMilestone()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub




    Sub LoadContacts(ByVal intDivision As Integer)
        Try


        Catch ex As Exception
            Throw ex
        End Try
    End Sub




    Sub CreatMilestone()
        Try
            Dim dtProjectProcess As New DataTable
            dtProjectProcess = Session("ProProcessDetails")
            tblMilestone.Rows.Clear()
            If dtProjectProcess.Rows.Count <> 0 Then
                LoadAssignTo()
                Dim i As Integer
                Dim tblCell As TableCell
                Dim tblrow As TableRow
                Dim chkDlost As CheckBox
                Dim chkDClosed As CheckBox
                Dim chkDclosed1 As Label
                Dim chkDclosed2 As Label
                Dim btnAdd As New Button
                Dim btnDelete As New Button
                Dim InitialPercentage As Integer = dtProjectProcess.Rows(0).Item(0)
                If Not dtProjectProcess.Rows(0).Item(0) = 100 Then
                    If Not dtProjectProcess.Rows(0).Item("Op_Flag") = 1 Then
                        tblrow = New TableRow
                        tblCell = New TableCell
                        tblCell.Text = "<font color=white>&nbsp;&nbsp;&nbsp;&nbsp;   Milestone - " & InitialPercentage & "%</font>"
                        tblCell.Height = Unit.Pixel(20)
                        tblCell.CssClass = "text_bold"
                        tblCell.ColumnSpan = 4
                        tblCell.BackColor = System.Drawing.Color.FromName("#52658C")
                        tblrow.Controls.Add(tblCell)
                        tblMilestone.Controls.Add(tblrow)



                    End If
                End If
                ViewState("CheckColor") = 0
                For i = 0 To dtProjectProcess.Rows.Count - 1
                    If dtProjectProcess.Rows(i).Item(0) <> 100 Then
                        If dtProjectProcess.Rows(i).Item(0) <> 0 Then
                            If InitialPercentage = dtProjectProcess.Rows(i).Item(0) Then
                                If dtProjectProcess.Rows(i).Item("Op_Flag") <> 1 Then
                                    createStages(dtProjectProcess.Rows(i))
                                    InitialPercentage = dtProjectProcess.Rows(i).Item(0)
                                End If
                            Else
                                If dtProjectProcess.Rows(i).Item("Op_Flag") <> 1 Then
                                    InitialPercentage = dtProjectProcess.Rows(i).Item(0)
                                    tblrow = New TableRow
                                    tblCell = New TableCell
                                    tblCell.Text = "<font color=white>&nbsp;&nbsp;&nbsp;&nbsp;   Milestone - " & InitialPercentage & "%</font>"
                                    tblCell.Height = Unit.Pixel(20)
                                    tblCell.CssClass = "text_bold"
                                    tblCell.ColumnSpan = 4
                                    tblCell.BackColor = System.Drawing.Color.FromName("#52658C")
                                    tblrow.Controls.Add(tblCell)
                                    tblMilestone.Controls.Add(tblrow)


                                    ' ''  If Trim(GetQueryStringVal(Request.QueryString("enc"),"opId")) = "" Or Viewstate("MileCheck") = 1 Then  
                                    ''tblrow = New TableRow
                                    ''tblCell = New TableCell
                                    ''tblCell.ColumnSpan = 4
                                    ''btnAdd = New Button
                                    ''btnDelete = New Button
                                    ''btnAdd.ID = "btnAdd~" & InitialPercentage
                                    ''btnDelete.ID = "btnDelete~" & InitialPercentage
                                    ''btnAdd.Text = "Add"
                                    ''btnDelete.Text = "Delete"
                                    ''btnAdd.Width = Unit.Pixel(50)
                                    ''btnDelete.Width = Unit.Pixel(50)
                                    ''btnAdd.CssClass = "button"
                                    ''btnDelete.CssClass = "button"
                                    ''btnDelete.Attributes.Add("onclick", "return DeletMsg()")
                                    ''AddHandler btnAdd.Click, AddressOf btnAddClick
                                    ''AddHandler btnDelete.Click, AddressOf btnDeleteClick
                                    ''tblCell.HorizontalAlign = HorizontalAlign.Right
                                    ''Dim lblSBs As New Label
                                    ''lblSBs.ID = "lblSBs" & InitialPercentage
                                    ''lblSBs.Text = "&nbsp;"
                                    ''tblCell.Controls.Add(btnAdd)
                                    ''tblCell.Controls.Add(lblSBs)
                                    ''tblCell.Controls.Add(btnDelete)
                                    ''tblrow.Controls.Add(tblCell)
                                    ''tblMilestone.Controls.Add(tblrow)
                                    'End If
                                    ViewState("CheckColor") = 0
                                    createStages(dtProjectProcess.Rows(i))
                                End If
                            End If
                        End If
                    End If
                Next

                ''Project Conclusion
                tblrow = New TableRow
                Dim txtDComments As Label
                Dim lblComm As Label
                tblCell = New TableCell
                tblCell.Text = "<font color=white>&nbsp;&nbsp;&nbsp;&nbsp; Conclusion</font>"
                tblCell.Height = Unit.Pixel(20)
                tblCell.CssClass = "text_bold"
                tblCell.ColumnSpan = 4
                tblCell.BackColor = System.Drawing.Color.FromName("#52658C")
                tblrow.Controls.Add(tblCell)
                tblMilestone.Controls.Add(tblrow)

                '' Project Completion Details
                tblrow = New TableRow
                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                tblCell.ColumnSpan = 4
                ' chkDClosed = New CheckBox
                chkDclosed1 = New Label
                chkDclosed2 = New Label
                '    chkDClosed.ID = "chkDClosed"
                chkDclosed1.Text = "Project Completed &nbsp;"
                chkDclosed1.CssClass = "normal8"
                If (dtProjectProcess.Rows(dtProjectProcess.Rows.Count - 1).Item("bitStageCompleted")) = True Then
                    'chkDclosed2.Text = "a"
                    'chkDclosed2.CssClass = "cell1"
                    chkDclosed1.Text = "&nbsp; Project Completed &nbsp;"
                Else
                    'chkDclosed2.Text = "r"
                    'chkDclosed2.CssClass = "cell"
                    chkDclosed1.Text = "&nbsp; Project Not Completed &nbsp;"
                End If
                lblComm = New Label
                lblComm.Text = "&nbsp;&nbsp;Comments : &nbsp; "
                lblComm.CssClass = "normal7"
                txtDComments = New Label
                txtDComments.CssClass = "normal1"
                txtDComments.ID = "txtDCComm"
                txtDComments.Text = dtProjectProcess.Rows(dtProjectProcess.Rows.Count - 1).Item("vcComments")
                txtDComments.Width = Unit.Pixel(600)
                tblCell.Controls.Add(chkDclosed1)
                tblCell.Controls.Add(chkDclosed2)
                tblCell.CssClass = "normal1"
                tblCell.Controls.Add(lblComm)
                tblCell.Controls.Add(txtDComments)
                tblrow.Controls.Add(tblCell)
                tblMilestone.Controls.Add(tblrow)

            End If
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Sub createStages(ByVal dr As DataRow)
        Try
            Dim tblCell As TableCell
            Dim tblrow As TableRow
            Dim lbl As Label
            Dim chkStage As CheckBox
            Dim chkStage1 As Label
            Dim chkStage2 As Label
            Dim txtStage As Label
            Dim lblStageCM As Label
            Dim lblInform As Label
            Dim txtComm As Label
            Dim lblStage As Label

            Dim ddlStatus As Label

            Dim ddlAgginTo As Label
            'Dim chkAlert As Label
            Dim chkAlert1 As Label
            Dim chkAlert2 As Label
            Dim txtChecKStage As Label
            Dim hpkLink As HyperLink
            Dim txtDueDate As Label

            Dim lblAssignTo As Label
            Dim dtAssignTo As DataTable
            dtAssignTo = Session("AssignTo")


            tblrow = New TableRow
            If ViewState("CheckColor") = 1 Then
                tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
            End If
            tblCell = New TableCell
            'tblCell.CssClass = "normal1"
            tblCell.ColumnSpan = 4
            lblStage = New Label
            If Not IsDBNull(dr.Item("vcstageDetail")) Then
                lblStage.Text = dr.Item("vcstageDetail")
            End If

            lblStage.CssClass = "normal9"
            Dim strImage As String

            strImage = "<img src='../images/mileStone.gif'  > "
            tblCell.Controls.Add(New LiteralControl(strImage))
            tblCell.Controls.Add(lblStage)
            tblCell.CssClass = "normal9"
            tblrow.Controls.Add(tblCell)
            tblMilestone.Controls.Add(tblrow)

            '''First row
            tblrow = New TableRow
            If ViewState("CheckColor") = 1 Then
                tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
            End If
            tblCell = New TableCell
            'txtStage = New Label
            'txtStage.ID = "txtStage~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
            'txtStage.Text = "&nbsp;" & dr.Item("vcstageDetail")
            'txtStage.CssClass = "normal8"
            'txtStage.Width = Unit.Pixel(250)
            'tblCell.ColumnSpan = 2
            'tblCell.CssClass = "normal8"
            'tblCell.Controls.Add(txtStage)

            lblStageCM = New Label
            lblStageCM.Text = "&nbsp;Stage Status&nbsp;&nbsp;&nbsp;:"
            lblStageCM.ID = "lblStatus" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
            lblStageCM.CssClass = "normal8"
            ddlStatus = New Label
            Dim objCommon As New CCommon
            'objCommon.sb_FillComboFromDBwithSel(ddlStatus, 42, Session("DomainID"))


            ddlStatus.Text = IIf(IsDBNull(dr.Item("vcStage")), "", dr.Item("vcStage"))

            ddlStatus.CssClass = "normal1"
            ' ddlStatus.ID = "ddlStatus~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
            tblCell.CssClass = "normal1"
            tblCell.Controls.Add(lblStageCM)
            tblCell.Controls.Add(ddlStatus)
            tblrow.Controls.Add(tblCell)


            tblCell = New TableCell
            lblStageCM = New Label
            lblStageCM.Text = "Last Modified By :  "
            lblStageCM.CssClass = "normal8"
            lblStageCM.ID = "lblStageModifiedBy" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
            lblInform = New Label
            If Not IsDBNull(dr.Item("numModifiedBy")) Then
                lblInform.Text = IIf(dr.Item("numModifiedBy") = 0, "", dr.Item("numModifiedByName"))
            End If
            lblInform.ID = "lblInformBy" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
            tblCell.CssClass = "normal1"
            tblCell.Controls.Add(lblStageCM)
            tblCell.Controls.Add(lblInform)
            tblrow.Controls.Add(tblCell)


            tblCell = New TableCell
            lblStageCM = New Label
            lblStageCM.Text = "Last Modified Date :  "
            lblStageCM.CssClass = "normal8"
            lblStageCM.ID = "lblStageModified" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
            lblInform = New Label
            lblInform.Text = FormattedDateFromDate(dr.Item("bintModifiedDate"), Session("DateFormat"))
            lblInform.ID = "lblInform" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
            tblCell.CssClass = "normal1"
            tblCell.Controls.Add(lblStageCM)
            tblCell.Controls.Add(lblInform)
            tblrow.Controls.Add(tblCell)
            tblMilestone.Controls.Add(tblrow)


            'Second Row
            tblrow = New TableRow
            If ViewState("CheckColor") = 1 Then
                tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
            End If
            tblCell = New TableCell
            tblCell.CssClass = "normal1"
            tblCell.ColumnSpan = 4
            lbl = New Label
            txtComm = New Label
            txtComm.ID = "txtComm~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
            txtComm.Width = Unit.Pixel(750)
            txtComm.CssClass = "normal1"
            txtComm.Text = dr.Item("vcComments")
            lbl.Text = "&nbsp;Comments &nbsp;:"
            lbl.CssClass = "normal8"
            tblCell.Controls.Add(lbl)
            tblCell.Controls.Add(txtComm)
            tblCell.CssClass = "normal1"
            tblrow.Controls.Add(tblCell)
            tblMilestone.Controls.Add(tblrow)

            ''Third Row



            If ViewState("MileCheck") = 0 Then
                tblrow = New TableRow
                If ViewState("CheckColor") = 1 Then
                    tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
                End If
                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                hpkLink = New HyperLink
                hpkLink.ID = "hpkLinkTime" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")

                hpkLink.Attributes.Add("onclick", "return OpenTime(" & dr.Item("ProStageID") & "," & lngProID & "," & dr.Item("numStagePercentage") & "," & dr.Item("numstagedetailsID") & ");")

                hpkLink.Text = "<font class='hyperlink'>Time</font>"
                hpkLink.CssClass = "hyperlink"
                lbl = New Label
                lbl.ID = "lblTime" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                If Not IsDBNull(dr.Item("Time")) Then
                    lbl.Text = "<font color=red>&nbsp;" & String.Format("{0:#,##0.00}", CDec(dr.Item("Time"))) & "</font>"
                End If
                tblCell.Controls.Add(hpkLink)
                tblCell.Controls.Add(lbl)
                tblrow.Controls.Add(tblCell)



                tblCell = New TableCell
                hpkLink = New HyperLink
                hpkLink.ID = "hpkLinkExpense" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                'If Not dr.Item("numOppStageID") Is Nothing Then
                hpkLink.Attributes.Add("onclick", "return OpenExpense(" & dr.Item("ProStageID") & "," & lngProID & "," & dr.Item("numStagePercentage") & "," & dr.Item("numstagedetailsID") & ");")

                hpkLink.Text = "<font class='hyperlink'>Expense</font>"
                hpkLink.CssClass = "hyperlink"
                tblCell.CssClass = "normal1"
                lbl = New Label
                lbl.ID = "lblExp" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                If Not IsDBNull(dr.Item("Expense")) Then
                    lbl.Text = "<font color=red>&nbsp;" & String.Format("{0:#,##0.00}", CDec(dr.Item("Expense"))) & "</font>"
                End If
                tblCell.Controls.Add(hpkLink)
                tblCell.Controls.Add(lbl)
                tblrow.Controls.Add(tblCell)

                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                hpkLink = New HyperLink
                hpkLink.ID = "hpkLinkDependency" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")

                hpkLink.Attributes.Add("onclick", "return OpenDependency(" & dr.Item("ProStageID") & "," & lngProID & "," & dr.Item("numStagePercentage") & "," & dr.Item("numstagedetailsID") & ");")
                'End If
                hpkLink.Text = "<font class='hyperlink'>Dependency</font>"
                hpkLink.CssClass = "hyperlink"
                lbl = New Label
                lbl.ID = "lblDep" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                If Not IsDBNull(dr.Item("Depend")) Then
                    If dr.Item("Depend") = "1" Then
                        lbl.Text = "<font color='red' face='Wingdings 2'>&nbsp;" & 8 & "</font>"
                    End If
                End If
                tblCell.Controls.Add(hpkLink)
                tblCell.Controls.Add(lbl)
                tblrow.Controls.Add(tblCell)

                tblCell = New TableCell
                tblCell.CssClass = "normal1"
                lbl = New Label
                hpkLink = New HyperLink
                hpkLink.ID = "hpkLinkSubStage" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                hpkLink.Text = "<font class='hyperlink'>Sub Stages</font>"
                hpkLink.CssClass = "hyperlink"
                hpkLink.Attributes.Add("onclick", "return OpenSubStage(" & dr.Item("ProStageID") & "," & lngProID & "," & dr.Item("numStagePercentage") & "," & dr.Item("numstagedetailsID") & ");")
                'End If
                lbl.ID = "lblStg" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
                If Not IsDBNull(dr.Item("SubStg")) Then
                    If dr.Item("SubStg") = "1" Then
                        lbl.Text = "<font color='red' face='Wingdings 2'>&nbsp;" & 8 & "</font>"
                    End If
                End If
                tblCell.Controls.Add(hpkLink)
                tblCell.Controls.Add(lbl)
                tblrow.Controls.Add(tblCell)
                tblMilestone.Controls.Add(tblrow)
            End If

            ''''Fourth Row
            tblrow = New TableRow
            If ViewState("CheckColor") = 1 Then
                tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
            End If
            tblCell = New TableCell
            tblCell.CssClass = "normal1"
            lbl = New Label
            lbl.Text = "&nbsp;Assign To :"
            lbl.CssClass = "normal7"
            tblCell.Controls.Add(lbl)
            'ddlAgginTo = New DropDownList
            'ddlAgginTo.Width = Unit.Pixel(150)
            'ddlAgginTo.ID = "ddlAgginTo~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
            'ddlAgginTo.CssClass = "signup"
            'ddlAgginTo.DataSource = dtAssignTo
            'ddlAgginTo.DataTextField = "vcName"
            'ddlAgginTo.DataValueField = "ContactID"
            'ddlAgginTo.DataBind()
            'ddlAgginTo.Items.Insert(0, "--Select One--")
            'ddlAgginTo.Items.FindByText("--Select One--").Value = 0
            lblAssignTo = New Label
            'If Not IsDBNull(dr.Item("vcAssignTo")) Then
            '    'If Not ddlAgginTo.Items.FindByValue(dr.Item("numAssignTo")) Is Nothing Then
            '    lblAssignTo.Text = dr.Item("vcAssignTo")
            '    'End If
            'End If
            lblAssignTo.Text = IIf(IsDBNull(dr.Item("vcAssignTo")), "", dr.Item("vcAssignTo"))
            tblCell.Controls.Add(lblAssignTo)
            tblrow.Controls.Add(tblCell)

            tblCell = New TableCell
            tblCell.CssClass = "normal1"
            chkAlert1 = New Label
            chkAlert2 = New Label
            chkAlert2.Text = ""
            chkAlert1.ID = "chkAlert~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
            chkAlert1.Text = "Alert :"
            chkAlert1.CssClass = "normal8"
            If dr.Item("bitAlert") = 0 Then
                chkAlert2.Text = "a"
                chkAlert2.CssClass = "cell1"
            ElseIf (dr.Item("bitAlert") = 1) Then
                chkAlert2.Text = "r"
                chkAlert2.CssClass = "cell"
            End If


            tblCell.Controls.Add(chkAlert1)
            tblCell.Controls.Add(chkAlert2)
            tblrow.Controls.Add(tblCell)

            tblCell.CssClass = "normal1"
            lbl = New Label
            lbl.Text = "&nbsp; &nbsp; &nbsp; "

            tblCell.Controls.Add(lbl)
            lbl = New Label
            lbl.Text = "Due Date: &nbsp;"
            hpkLink = New HyperLink
            hpkLink.ID = "hpkLinkDueDate" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
            txtDueDate = New Label
            txtDueDate.CssClass = "signup"
            txtDueDate.ID = "txtDueDate :" & dr.Item("numStagePercentage") & dr.Item("numstagedetailsID")
            If Not IsDBNull(dr.Item("bintDueDate")) Then
                txtDueDate.Text = FormattedDateFromDate(dr.Item("bintDueDate"), Session("DateFormat"))
            End If


            tblCell.Controls.Add(lbl)
            tblCell.Controls.Add(txtDueDate)

            tblrow.Controls.Add(tblCell)

            tblCell = New TableCell

            tblCell.CssClass = "normal1"
            lbl = New Label
            lbl.Text = "Stage Completed : "
            lbl.CssClass = "normal8"
            tblCell.Controls.Add(lbl)
            lbl = New Label
            If Not IsDBNull(dr.Item("bintStageComDate")) Then
                If dr.Item("bitStageCompleted") = True Then
                    lbl.Text = FormattedDateFromDate(dr.Item("bintStageComDate"), Session("DateFormat"))
                End If
            End If
            tblCell.Controls.Add(lbl)
            tblrow.Controls.Add(tblCell)

            tblCell = New TableCell
            tblCell.CssClass = "normal1"
            lbl = New Label
            lbl.CssClass = "normal7"
            'chkStage = New CheckBox
            'txtChecKStage = New Label
            ' txtChecKStage.ID = "txtChecKStage~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
            ' txtChecKStage.Attributes.Add("style", "display:none")
            If dr.Item("bitStageCompleted") = False Then
                lbl.Text = "Stage Open"
            Else
                ' txtChecKStage.Text = 1
                lbl.Text = "<font color='#CC0000'><b>Stage Closed</b></font> : " & dr.Item("tintPercentage") & " %"
                'chkStage.Checked = True
            End If

            ' chkStage.ID = "chkStage~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
            tblCell.Controls.Add(lbl)
            ' tblCell.Controls.Add(txtChecKStage)
            '  tblCell.Controls.Add(chkStage)
            tblrow.Controls.Add(tblCell)
            tblMilestone.Controls.Add(tblrow)

            If ViewState("CheckColor") = 1 Then
                ViewState("CheckColor") = 0
            Else
                ViewState("CheckColor") = 1
            End If
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub







    Sub LoadSalesProcess()
        Try
            Dim dtProjectProcess As New DataTable
            Dim objOpportunity As New MOpportunity
            objOpportunity.ProType = 1
            objOpportunity.DomainID = Session("DomainID")
            dtProjectProcess = objOpportunity.BusinessProcess
            BindSaleProcess(dtProjectProcess)
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Sub BindSaleProcess(ByVal dtTable As DataTable)
        'Try
        '    ddlProcessList.DataSource = dtTable
        '    ddlProcessList.DataTextField = "slp_name"
        '    ddlProcessList.DataValueField = "slp_id"
        '    ddlProcessList.DataBind()
        '    ddlProcessList.Items.Insert(0, "--Select One--")
        '    ddlProcessList.Items.FindByText("--Select One--").Value = 0
        'Catch ex As Exception
        '    Response.Write(ex)
        'End Try
    End Sub

    Sub LoadAssignTo()
        Try
            Dim dtAssignTo As New DataTable
            Dim objOpportunity As New MOpportunity
            objOpportunity.DomainID = Session("DomainId")
            objOpportunity.ContactType = 92
            dtAssignTo = objOpportunity.AssignTo
            Session("AssignTo") = dtAssignTo
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub



    Private Sub dgContact_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgContact.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim btnDelete As Button
                Dim lnkDelete As LinkButton
                lnkDelete = e.Item.FindControl("lnkDeleteCnt")
                btnDelete = e.Item.FindControl("btnDeleteCnt")

                'Dim lbl As Label
                'lbl = e.Item.FindControl("lblContcRoleid")
                'Dim ddlContRole As DropDownList
                'ddlContRole = e.Item.FindControl("ddlContactRole")
                'Dim objCommon As New CCommon
                'objCommon.sb_FillComboFromDBwithSel(ddlContRole, 26, Session("DomainID"))
                'If lbl.Text <> "" Then
                '    If Not ddlContRole.Items.FindByValue(lbl.Text) Is Nothing Then
                '        ddlContRole.Items.FindByValue(lbl.Text).Selected = True
                '    End If
                'End If
                If e.Item.Cells(1).Text = 1 Then
                    CType(e.Item.FindControl("lblShare"), Label).Text = "a"
                    CType(e.Item.FindControl("lblShare"), Label).CssClass = "cell1"
                    'CType(e.Item.FindControl("chkShare"), CheckBox).Checked = True
                Else
                    CType(e.Item.FindControl("lblShare"), Label).Text = "r"
                    CType(e.Item.FindControl("lblShare"), Label).CssClass = "cell"
                End If
            End If
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgContact_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgContact.ItemCommand
        'Try
        '    If e.CommandName = "Delete" Then
        '        Dim dtContactInfo As New DataTable
        '        dtContactInfo = Session("dtContactInfo")
        '        Dim Count As Integer
        '        For Each myRow In dtContactInfo.Rows
        '            If Count = e.Item.ItemIndex Then
        '                dtContactInfo.Rows.Remove(myRow)
        '                dtContactInfo.AcceptChanges()
        '                Exit For
        '            End If
        '            Count = Count + 1
        '        Next
        '        dtContactInfo.AcceptChanges()
        '        Session("dtContactInfo") = dtContactInfo
        '        BindContactInfo()
        '    End If
        'Catch ex As Exception
        '    Response.Write(ex)
        'End Try
    End Sub




    'Sub btnAddClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim ctrlId As String = sender.id
    '        Dim strSplit As String()
    '        Dim i As Integer
    '        ViewState("Reach") = Nothing
    '        strSplit = ctrlId.Split("~")
    '        Dim dtSalesDetails As New DataTable
    '        Dim dtNew As New DataTable
    '        dtNew.Columns.Add("numStagePercentage")
    '        dtNew.Columns.Add("numstagedetailsID")
    '        dtNew.Columns.Add("vcstageDetail")
    '        dtNew.Columns.Add("ProStageID")
    '        dtNew.Columns.Add("numDomainId")
    '        dtNew.Columns.Add("numCreatedBy")
    '        dtNew.Columns.Add("bintCreatedDate")
    '        dtNew.Columns.Add("numModifiedBy")
    '        dtNew.Columns.Add("bintModifiedDate")
    '        dtNew.Columns.Add("bintDueDate")
    '        dtNew.Columns.Add("bintStageComDate")
    '        dtNew.Columns.Add("vcComments")
    '        dtNew.Columns.Add("numAssignTo")
    '        dtNew.Columns.Add("bitAlert", GetType(Int16))
    '        dtNew.Columns.Add("bitStageCompleted", GetType(Int16))
    '        dtNew.Columns.Add("Op_Flag")
    '        dtNew.Columns.Add("numModifiedByName")
    '        dtNew.Columns.Add("Depend")
    '        dtNew.Columns.Add("Expense")
    '        dtNew.Columns.Add("Time")
    '        dtNew.Columns.Add("SubStg")
    '        dtNew.Columns.Add("numStage")
    '        dtSalesDetails = Session("ProProcessDetails")
    '        For i = 0 To dtSalesDetails.Rows.Count - 1
    '            If strSplit(1) = dtSalesDetails.Rows(i).Item("numStagePercentage") Then
    '                ViewState("Reach") = 1
    '            End If
    '            If ViewState("Reach") = 1 And strSplit(1) <> dtSalesDetails.Rows(i).Item("numStagePercentage") Then
    '                ViewState("Reach") = Nothing
    '                myRow = dtNew.NewRow
    '                myRow("numStagePercentage") = strSplit(1)
    '                Dim k, j As Integer
    '                For k = 0 To dtSalesDetails.Rows.Count - 1
    '                    j = j + dtSalesDetails.Rows(k).Item("numstagedetailsID")
    '                Next
    '                myRow("numstagedetailsID") = j
    '                myRow("vcstageDetail") = ""
    '                myRow("ProStageID") = 0
    '                myRow("numDomainId") = Session("DomainId")
    '                myRow("numCreatedBy") = session("ConID")
    '                myRow("bintCreatedDate") = Now
    '                myRow("numModifiedBy") = session("ConID")
    '                myRow("bintModifiedDate") = Now
    '                myRow("bintDueDate") = DBNull.Value
    '                myRow("bintStageComDate") = DBNull.Value
    '                myRow("vcComments") = ""
    '                myRow("numAssignTo") = 0
    '                myRow("bitAlert") = 0
    '                myRow("bitStageCompleted") = 0
    '                myRow("Op_Flag") = 0
    '                myRow("numModifiedByName") = ""
    '                myRow("numStage") = 0
    '                dtNew.Rows.Add(myRow)
    '            End If
    '            myRow = dtNew.NewRow
    '            myRow("numStagePercentage") = dtSalesDetails.Rows(i).Item("numStagePercentage")
    '            myRow("numstagedetailsID") = dtSalesDetails.Rows(i).Item("numstagedetailsID")
    '            myRow("vcstageDetail") = dtSalesDetails.Rows(i).Item("vcstageDetail")
    '            myRow("ProStageID") = dtSalesDetails.Rows(i).Item("ProStageID")
    '            myRow("numDomainId") = dtSalesDetails.Rows(i).Item("numDomainId")
    '            myRow("numCreatedBy") = dtSalesDetails.Rows(i).Item("numCreatedBy")
    '            myRow("bintCreatedDate") = dtSalesDetails.Rows(i).Item("bintCreatedDate")
    '            myRow("numModifiedBy") = dtSalesDetails.Rows(i).Item("numModifiedBy")
    '            myRow("bintModifiedDate") = dtSalesDetails.Rows(i).Item("bintModifiedDate")
    '            myRow("bintDueDate") = dtSalesDetails.Rows(i).Item("bintDueDate")
    '            myRow("bintStageComDate") = dtSalesDetails.Rows(i).Item("bintStageComDate")
    '            myRow("vcComments") = dtSalesDetails.Rows(i).Item("vcComments")
    '            myRow("numAssignTo") = dtSalesDetails.Rows(i).Item("numAssignTo")
    '            myRow("bitAlert") = dtSalesDetails.Rows(i).Item("bitAlert")
    '            myRow("bitStageCompleted") = dtSalesDetails.Rows(i).Item("bitStageCompleted")
    '            myRow("Op_Flag") = dtSalesDetails.Rows(i).Item("Op_Flag")
    '            myRow("numModifiedByName") = dtSalesDetails.Rows(i).Item("numModifiedByName")
    '            myRow("Depend") = dtSalesDetails.Rows(i).Item("Depend")
    '            myRow("Expense") = dtSalesDetails.Rows(i).Item("Expense")
    '            myRow("Time") = dtSalesDetails.Rows(i).Item("Time")
    '            myRow("SubStg") = dtSalesDetails.Rows(i).Item("SubStg")
    '            myRow("numStage") = dtSalesDetails.Rows(i).Item("numStage")
    '            dtNew.Rows.Add(myRow)
    '        Next
    '        Session("ProProcessDetails") = dtNew
    '        CreatMilestone()
    '    Catch ex As Exception
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Sub btnDeleteClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim ctrlId As String = sender.id
    '        Dim strSplit As String()
    '        Dim i As Integer
    '        strSplit = ctrlId.Split("~")
    '        Dim dtSalesDetails As New DataTable
    '        Dim dtNew As New DataTable
    '        dtNew.Columns.Add("numStagePercentage")
    '        dtNew.Columns.Add("numstagedetailsID")
    '        dtNew.Columns.Add("vcstageDetail")
    '        dtNew.Columns.Add("ProStageID")
    '        dtNew.Columns.Add("numDomainId")
    '        dtNew.Columns.Add("numCreatedBy")
    '        dtNew.Columns.Add("bintCreatedDate")
    '        dtNew.Columns.Add("numModifiedBy")
    '        dtNew.Columns.Add("bintModifiedDate")
    '        dtNew.Columns.Add("bintDueDate")
    '        dtNew.Columns.Add("bintStageComDate")
    '        dtNew.Columns.Add("vcComments")
    '        dtNew.Columns.Add("numAssignTo")
    '        dtNew.Columns.Add("bitAlert", GetType(Int16))
    '        dtNew.Columns.Add("bitStageCompleted", GetType(Int16))
    '        dtNew.Columns.Add("Op_Flag")
    '        dtNew.Columns.Add("numModifiedByName")
    '        dtNew.Columns.Add("Depend")
    '        dtNew.Columns.Add("Expense")
    '        dtNew.Columns.Add("Time")
    '        dtNew.Columns.Add("SubStg")
    '        dtNew.Columns.Add("numStage")
    '        dtSalesDetails = Session("ProProcessDetails")
    '        For i = 0 To dtSalesDetails.Rows.Count - 1
    '            If strSplit(1) = dtSalesDetails.Rows(i).Item("numStagePercentage") And dtSalesDetails.Rows(i).Item("Op_Flag") = 0 Then
    '                Dim chk As CheckBox
    '                chk = Page.FindControl("chkStage~" & strSplit(1) & "~" & dtSalesDetails.Rows(i).Item("numstagedetailsID"))
    '                myRow = dtNew.NewRow
    '                myRow("numStagePercentage") = dtSalesDetails.Rows(i).Item("numStagePercentage")
    '                myRow("numstagedetailsID") = dtSalesDetails.Rows(i).Item("numstagedetailsID")
    '                myRow("vcstageDetail") = dtSalesDetails.Rows(i).Item("vcstageDetail")
    '                myRow("ProStageID") = dtSalesDetails.Rows(i).Item("ProStageID")
    '                myRow("numDomainId") = dtSalesDetails.Rows(i).Item("numDomainId")
    '                myRow("numCreatedBy") = dtSalesDetails.Rows(i).Item("numCreatedBy")
    '                myRow("bintCreatedDate") = dtSalesDetails.Rows(i).Item("bintCreatedDate")
    '                myRow("numModifiedBy") = dtSalesDetails.Rows(i).Item("numModifiedBy")
    '                myRow("bintModifiedDate") = dtSalesDetails.Rows(i).Item("bintModifiedDate")
    '                myRow("bintDueDate") = dtSalesDetails.Rows(i).Item("bintDueDate")
    '                myRow("bintStageComDate") = dtSalesDetails.Rows(i).Item("bintStageComDate")
    '                myRow("vcComments") = dtSalesDetails.Rows(i).Item("vcComments")
    '                myRow("numAssignTo") = dtSalesDetails.Rows(i).Item("numAssignTo")
    '                myRow("bitAlert") = dtSalesDetails.Rows(i).Item("bitAlert")
    '                myRow("bitStageCompleted") = dtSalesDetails.Rows(i).Item("bitStageCompleted")
    '                myRow("numModifiedByName") = dtSalesDetails.Rows(i).Item("numModifiedByName")
    '                myRow("Depend") = dtSalesDetails.Rows(i).Item("Depend")
    '                myRow("Expense") = dtSalesDetails.Rows(i).Item("Expense")
    '                myRow("Time") = dtSalesDetails.Rows(i).Item("Time")
    '                myRow("SubStg") = dtSalesDetails.Rows(i).Item("SubStg")
    '                myRow("numStage") = dtSalesDetails.Rows(i).Item("numStage")
    '                If chk.Checked = False Then
    '                    myRow("Op_Flag") = dtSalesDetails.Rows(i).Item("Op_Flag")
    '                Else
    '                    myRow("Op_Flag") = 1
    '                End If
    '                dtNew.Rows.Add(myRow)

    '            Else
    '                myRow = dtNew.NewRow
    '                myRow("numStagePercentage") = dtSalesDetails.Rows(i).Item("numStagePercentage")
    '                myRow("numstagedetailsID") = dtSalesDetails.Rows(i).Item("numstagedetailsID")
    '                myRow("vcstageDetail") = dtSalesDetails.Rows(i).Item("vcstageDetail")
    '                myRow("ProStageID") = dtSalesDetails.Rows(i).Item("ProStageID")
    '                myRow("numDomainId") = dtSalesDetails.Rows(i).Item("numDomainId")
    '                myRow("numCreatedBy") = dtSalesDetails.Rows(i).Item("numCreatedBy")
    '                myRow("bintCreatedDate") = dtSalesDetails.Rows(i).Item("bintCreatedDate")
    '                myRow("numModifiedBy") = dtSalesDetails.Rows(i).Item("numModifiedBy")
    '                myRow("bintModifiedDate") = dtSalesDetails.Rows(i).Item("bintModifiedDate")
    '                myRow("bintDueDate") = dtSalesDetails.Rows(i).Item("bintDueDate")
    '                myRow("bintStageComDate") = dtSalesDetails.Rows(i).Item("bintStageComDate")
    '                myRow("vcComments") = dtSalesDetails.Rows(i).Item("vcComments")
    '                myRow("numAssignTo") = dtSalesDetails.Rows(i).Item("numAssignTo")
    '                myRow("bitAlert") = dtSalesDetails.Rows(i).Item("bitAlert")
    '                myRow("bitStageCompleted") = dtSalesDetails.Rows(i).Item("bitStageCompleted")
    '                myRow("Op_Flag") = dtSalesDetails.Rows(i).Item("Op_Flag")
    '                myRow("numModifiedByName") = dtSalesDetails.Rows(i).Item("numModifiedByName")
    '                myRow("Depend") = dtSalesDetails.Rows(i).Item("Depend")
    '                myRow("Expense") = dtSalesDetails.Rows(i).Item("Expense")
    '                myRow("Time") = dtSalesDetails.Rows(i).Item("Time")
    '                myRow("SubStg") = dtSalesDetails.Rows(i).Item("SubStg")
    '                myRow("numStage") = dtSalesDetails.Rows(i).Item("numStage")
    '                dtNew.Rows.Add(myRow)
    '            End If
    '        Next
    '        Session("ProProcessDetails") = dtNew
    '        CreatMilestone()
    '    Catch ex As Exception
    '        Response.Write(ex)
    '    End Try
    'End Sub




    Sub FormatContacts()
        Dim dtContact As New DataTable
        dtContact = Session("dtContactInfo")
        Dim i As Integer = 0
        Dim dgConItem As DataGridItem
        dtContact = Session("dtContactInfo")
        For Each dgConItem In dgContact.Items
            'dtContact.Rows(i).Item("ContRoleId") = CType(dgConItem.FindControl("ddlContactRole"), DropDownList).SelectedValue
            dtContact.Rows(i).Item("bitPartner") = IIf(CType(dgConItem.FindControl("chkShare"), CheckBox).Checked = True, 1, 0)
            i = i + 1
        Next
        Session("dtContactInfo") = dtContact
    End Sub


    Sub CreateTableMilestone()
        'Try
        '    Dim dtProjectProcess As New DataTable
        '    Dim i, intModified As Integer
        '    If Not Session("ProProcessDetails") Is Nothing Then
        '        dtProjectProcess = Session("ProProcessDetails")
        '        If dtProjectProcess.Rows.Count <> 0 Then
        '            For i = 0 To dtProjectProcess.Rows.Count - 1
        '                If dtProjectProcess.Rows(i).Item("numStagePercentage") <> 100 Then
        '                    If dtProjectProcess.Rows(i).Item("numStagePercentage") <> 0 Then
        '                        If Not dtProjectProcess.Rows(i).Item("Op_Flag") = 1 Then
        '                            Dim txtchkStage As TextBox
        '                            Dim chkStage As CheckBox
        '                            Dim ddlStatus As DropDownList
        '                            ddlStatus = Page.FindControl("ddlStatus~" & dtProjectProcess.Rows(i).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i).Item("numstagedetailsID"))
        '                            txtchkStage = Page.FindControl("txtChecKStage~" & dtProjectProcess.Rows(i).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i).Item("numstagedetailsID"))
        '                            Dim txtStageDetails As TextBox
        '                            txtStageDetails = Page.FindControl("txtStage~" & dtProjectProcess.Rows(i).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i).Item("numstagedetailsID"))
        '                            If dtProjectProcess.Rows(i).Item("vcstageDetail") <> txtStageDetails.Text Then
        '                                intModified = 1
        '                            End If
        '                            If dtProjectProcess.Rows(i).Item("numStage") <> ddlStatus.SelectedItem.Value Then
        '                                intModified = 1
        '                            End If
        '                            dtProjectProcess.Rows(i).Item("numStage") = ddlStatus.SelectedItem.Value
        '                            dtProjectProcess.Rows(i).Item("vcstageDetail") = txtStageDetails.Text
        '                            chkStage = Page.FindControl("chkStage~" & dtProjectProcess.Rows(i).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i).Item("numstagedetailsID"))
        '                            If txtchkStage.Text = 0 And chkStage.Checked = True Then
        '                                dtProjectProcess.Rows(i).Item("bitStageCompleted") = True
        '                                dtProjectProcess.Rows(i).Item("bintStageComDate") = Now

        '                                ' Sending Mail to Internal prjoject's Manager and external project manager on Completeing a Stage
        '                                Try
        '                                    Dim dtDetails As New DataTable
        '                                    Dim objAlerts As New CAlerts
        '                                    Dim objCommon As New CCommon
        '                                    objAlerts.AlertID = 9
        '                                    dtDetails = objAlerts.GetAlertDetails
        '                                    Dim strTo As String = ""
        '                                    If dtDetails.Rows(4).Item("tintAlertOn") = 1 Then
        '                                        objCommon.ContactID = ddlIntPrgMgr.SelectedItem.Value
        '                                        If ddlIntPrgMgr.SelectedItem.Value > 0 Then
        '                                            strTo = objCommon.GetContactsEmail
        '                                        End If
        '                                    End If
        '                                    Dim dtEmailTemplate As New DataTable
        '                                    Dim objDocuments As New DocumentList
        '                                    objDocuments.GenDocID = dtDetails.Rows(4).Item("numEmailTemplate")
        '                                    dtEmailTemplate = objDocuments.GetDocumentsByGenDocID
        '                                    If dtEmailTemplate.Rows.Count > 0 Then
        '                                        Dim objSendMail As New clsSendEmail
        '                                        Dim dtMergeFields As New DataTable
        '                                        Dim drNew As DataRow
        '                                        dtMergeFields.Columns.Add("OppID")
        '                                        dtMergeFields.Columns.Add("Organization")
        '                                        dtMergeFields.Columns.Add("Stage")
        '                                        drNew = dtMergeFields.NewRow
        '                                        drNew("OppID") = txtName.Text
        '                                        drNew("Organization") = lblOrg.Text
        '                                        drNew("Stage") = txtStageDetails.Text

        '                                        If strTo <> "" Then
        '                                            If dtDetails.Rows(5).Item("tintAlertOn") = 1 Then
        '                                                objCommon.ContactID = ddlCustPrjMgr.SelectedItem.Value
        '                                                If ddlCustPrjMgr.SelectedItem.Value > 0 Then
        '                                                    strTo = strTo & "," & objCommon.GetContactsEmail
        '                                                Else
        '                                                    strTo = objCommon.GetContactsEmail
        '                                                End If
        '                                            End If
        '                                        Else
        '                                            objCommon.ContactID = ddlCustPrjMgr.SelectedItem.Value
        '                                            strTo = objCommon.GetContactsEmail
        '                                        End If

        '                                        Dim dtEmail As New DataTable
        '                                        objAlerts.AlertDTLID = 12
        '                                        dtEmail = objAlerts.GetAlertEmails
        '                                        objCommon.byteMode = 0
        '                                        objCommon.ContactID = ddlIntPrgMgr.SelectedItem.Value
        '                                        Dim strCC As String = ""
        '                                        Dim p As Integer
        '                                        For p = 0 To dtEmail.Rows.Count - 1
        '                                            strCC = strCC & dtEmail.Rows(p).Item("vcEmailID") & ","
        '                                        Next
        '                                        strCC = strCC.TrimEnd(",")
        '                                        If dtDetails.Rows(2).Item("tintCCManager") = 1 Then
        '                                            Dim strCCAssignee As String = objCommon.GetManagerEmail
        '                                            If strCCAssignee <> "" Then
        '                                                strCC = strCC & "," & strCCAssignee
        '                                            End If
        '                                        End If
        '                                        dtMergeFields.Rows.Add(drNew)

        '                                        objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), strCC, Session("UserEmail"), strTo, dtMergeFields)
        '                                    End If
        '                                Catch ex As Exception

        '                                End Try
        '                            ElseIf txtchkStage.Text = 1 And chkStage.Checked = False Then
        '                                dtProjectProcess.Rows(i).Item("bitStageCompleted") = False
        '                                dtProjectProcess.Rows(i).Item("bintStageComDate") = System.DBNull.Value
        '                            End If
        '                            Dim txtStageComments As TextBox
        '                            txtStageComments = Page.FindControl("txtComm~" & dtProjectProcess.Rows(i).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i).Item("numstagedetailsID"))
        '                            If dtProjectProcess.Rows(i).Item("vcComments") <> txtStageComments.Text Then
        '                                intModified = 1
        '                            End If
        '                            dtProjectProcess.Rows(i).Item("vcComments") = txtStageComments.Text
        '                            Dim chkAlert As CheckBox
        '                            chkAlert = Page.FindControl("chkAlert~" & dtProjectProcess.Rows(i).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i).Item("numstagedetailsID"))
        '                            If chkAlert.Checked = True Then
        '                                dtProjectProcess.Rows(i).Item("bitAlert") = 1
        '                            End If
        '                            Dim txtDueDate As TextBox
        '                            txtDueDate = Page.FindControl("txtDueDate" & dtProjectProcess.Rows(i).Item("numStagePercentage") & dtProjectProcess.Rows(i).Item("numstagedetailsID"))
        '                            If Not txtDueDate.Text = "" Then
        '                                dtProjectProcess.Rows(i).Item("bintDueDate") = DateFromFormattedDate(txtDueDate.Text, Session("DateFormat"))
        '                                If dtProjectProcess.Rows(i).Item("bintDueDate") <> DateFromFormattedDate(txtDueDate.Text, Session("DateFormat")) Then
        '                                    intModified = 1
        '                                End If
        '                            Else
        '                                If Not IsDBNull(dtProjectProcess.Rows(i).Item("bintDueDate")) Then
        '                                    intModified = 1
        '                                End If
        '                            End If
        '                            Dim ddlAssignTo As DropDownList
        '                            ddlAssignTo = Page.FindControl("ddlAgginTo~" & dtProjectProcess.Rows(i).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i).Item("numstagedetailsID"))
        '                            If dtProjectProcess.Rows(i).Item("numAssignTo") <> ddlAssignTo.SelectedItem.Value Then
        '                                intModified = 1
        '                            End If
        '                            dtProjectProcess.Rows(i).Item("numAssignTo") = ddlAssignTo.SelectedItem.Value

        '                            'Sending Mail to the next Stage assignee
        '                            If txtchkStage.Text = 0 And chkStage.Checked = True Then
        '                                Try
        '                                    intModified = 1
        '                                    Dim objAlerts As New CAlerts
        '                                    Dim dtDetails As New DataTable
        '                                    objAlerts.AlertDTLID = 2 'Alert DTL ID for sending alerts in opportunities
        '                                    dtDetails = objAlerts.GetIndividualAlertdtl
        '                                    If dtDetails.Rows(0).Item("tintAlertOn") = 1 Then
        '                                        Dim dtEmailTemplate As New DataTable
        '                                        Dim objDocuments As New DocumentList
        '                                        objDocuments.GenDocID = dtDetails.Rows(0).Item("numEmailTemplate")
        '                                        dtEmailTemplate = objDocuments.GetDocumentsByGenDocID
        '                                        If dtEmailTemplate.Rows.Count > 0 Then
        '                                            Dim objSendMail As New clsSendEmail
        '                                            Dim dtMergeFields As New DataTable
        '                                            Dim drNew As DataRow
        '                                            dtMergeFields.Columns.Add("Assignee")
        '                                            dtMergeFields.Columns.Add("Stage")
        '                                            dtMergeFields.Columns.Add("DueDate")
        '                                            dtMergeFields.Columns.Add("OppID")
        '                                            drNew = dtMergeFields.NewRow
        '                                            drNew("Assignee") = ddlAssignTo.SelectedItem.Text
        '                                            drNew("Stage") = txtStageDetails.Text
        '                                            drNew("DueDate") = txtDueDate.Text
        '                                            drNew("OppID") = txtName.Text
        '                                            dtMergeFields.Rows.Add(drNew)
        '                                            If dtProjectProcess.Rows(i + 1).Item("numStagePercentage") <> 100 Then
        '                                                If dtProjectProcess.Rows(i + 1).Item("numStagePercentage") <> 0 Then
        '                                                    If Not dtProjectProcess.Rows(i + 1).Item("Op_Flag") = 1 Then
        '                                                        Dim ddlNAssignTo As DropDownList
        '                                                        ddlNAssignTo = Page.FindControl("ddlAgginTo~" & dtProjectProcess.Rows(i + 1).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i + 1).Item("numstagedetailsID"))
        '                                                        Dim objCommon As New CCommon
        '                                                        objCommon.byteMode = 1
        '                                                        objCommon.ContactID = ddlNAssignTo.SelectedItem.Value
        '                                                        objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), IIf(dtDetails.Rows(0).Item("tintCCManager") = 1, objCommon.GetManagerEmail, ""), Session("UserEmail"), objCommon.GetContactsEmail, dtMergeFields)
        '                                                    End If
        '                                                End If
        '                                            End If
        '                                        End If
        '                                    End If
        '                                Catch ex As Exception

        '                                End Try
        '                            End If
        '                            If intModified = 1 Then
        '                                dtProjectProcess.Rows(i).Item("numModifiedBy") = session("ConID")
        '                                dtProjectProcess.Rows(i).Item("bintModifiedDate") = Now
        '                            End If
        '                            intModified = 0
        '                            If chkAlert.Checked = True And ddlAssignTo.SelectedIndex > 0 Then
        '                                Try
        '                                    Dim objAlerts As New CAlerts
        '                                    Dim dtDetails As New DataTable
        '                                    objAlerts.AlertDTLID = 1 'Alert DTL ID for sending alerts in opportunities
        '                                    dtDetails = objAlerts.GetIndividualAlertdtl
        '                                    If dtDetails.Rows(0).Item("tintAlertOn") = 1 Then
        '                                        Dim dtEmailTemplate As New DataTable
        '                                        Dim objDocuments As New DocumentList
        '                                        objDocuments.GenDocID = dtDetails.Rows(0).Item("numEmailTemplate")
        '                                        dtEmailTemplate = objDocuments.GetDocumentsByGenDocID
        '                                        If dtEmailTemplate.Rows.Count > 0 Then
        '                                            Dim objSendMail As New clsSendEmail
        '                                            Dim dtMergeFields As New DataTable
        '                                            Dim drNew As DataRow
        '                                            dtMergeFields.Columns.Add("Assignee")
        '                                            dtMergeFields.Columns.Add("Stage")
        '                                            dtMergeFields.Columns.Add("DueDate")
        '                                            dtMergeFields.Columns.Add("OppID")
        '                                            drNew = dtMergeFields.NewRow
        '                                            drNew("Assignee") = ddlAssignTo.SelectedItem.Text
        '                                            drNew("Stage") = txtStageDetails.Text
        '                                            drNew("DueDate") = txtDueDate.Text
        '                                            drNew("OppID") = txtName.Text
        '                                            dtMergeFields.Rows.Add(drNew)
        '                                            Dim objCommon As New CCommon
        '                                            objCommon.byteMode = 1
        '                                            objCommon.ContactID = ddlAssignTo.SelectedItem.Value
        '                                            objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), IIf(dtDetails.Rows(0).Item("tintCCManager") = 1, objCommon.GetManagerEmail, ""), Session("UserEmail"), objCommon.GetContactsEmail, dtMergeFields)
        '                                        End If
        '                                    End If
        '                                Catch ex As Exception

        '                                End Try
        '                            End If
        '                        End If
        '                    End If
        '                End If
        '            Next

        '            Dim txtDealClosedComments As TextBox
        '            Dim chkDC As CheckBox
        '            txtDealClosedComments = Page.FindControl("txtDCComm")
        '            chkDC = Page.FindControl("chkDClosed")
        '            If chkDC.Checked = True Then
        '                dtProjectProcess.Rows(dtProjectProcess.Rows.Count - 1).Item("bitStageCompleted") = 1

        '                ' Sending Mail to Internal prjoject's Manager and external project manager
        '                Try
        '                    Dim dtDetails As New DataTable
        '                    Dim objAlerts As New CAlerts
        '                    Dim objCommon As New CCommon
        '                    objAlerts.AlertID = 9
        '                    dtDetails = objAlerts.GetAlertDetails
        '                    Dim strTo As String = ""
        '                    If dtDetails.Rows(2).Item("tintAlertOn") = 1 Then
        '                        objCommon.ContactID = ddlIntPrgMgr.SelectedItem.Value
        '                        If ddlIntPrgMgr.SelectedItem.Value > 0 Then
        '                            strTo = objCommon.GetContactsEmail
        '                        End If
        '                    End If
        '                    Dim dtEmailTemplate As New DataTable
        '                    Dim objDocuments As New DocumentList
        '                    objDocuments.GenDocID = dtDetails.Rows(2).Item("numEmailTemplate")
        '                    dtEmailTemplate = objDocuments.GetDocumentsByGenDocID
        '                    If dtEmailTemplate.Rows.Count > 0 Then
        '                        Dim objSendMail As New clsSendEmail
        '                        Dim dtMergeFields As New DataTable
        '                        Dim drNew As DataRow
        '                        dtMergeFields.Columns.Add("OppID")
        '                        dtMergeFields.Columns.Add("Organization")
        '                        drNew = dtMergeFields.NewRow
        '                        drNew("OppID") = txtName.Text
        '                        drNew("Organization") = lblOrg.Text

        '                        If strTo <> "" Then
        '                            If dtDetails.Rows(3).Item("tintAlertOn") = 1 Then
        '                                objCommon.ContactID = ddlCustPrjMgr.SelectedItem.Value
        '                                If ddlCustPrjMgr.SelectedItem.Value > 0 Then
        '                                    strTo = strTo & "," & objCommon.GetContactsEmail
        '                                Else
        '                                    strTo = objCommon.GetContactsEmail
        '                                End If
        '                            End If
        '                        Else
        '                            objCommon.ContactID = ddlCustPrjMgr.SelectedItem.Value
        '                            strTo = objCommon.GetContactsEmail
        '                        End If

        '                        Dim dtEmail As New DataTable
        '                        objAlerts.AlertDTLID = 14
        '                        dtEmail = objAlerts.GetAlertEmails
        '                        objCommon.byteMode = 0
        '                        objCommon.ContactID = ddlIntPrgMgr.SelectedItem.Value
        '                        Dim strCC As String = ""
        '                        Dim p As Integer
        '                        For p = 0 To dtEmail.Rows.Count - 1
        '                            strCC = strCC & dtEmail.Rows(p).Item("vcEmailID") & ","
        '                        Next
        '                        strCC = strCC.TrimEnd(",")
        '                        If dtDetails.Rows(2).Item("tintCCManager") = 1 Then
        '                            Dim strCCAssignee As String = objCommon.GetManagerEmail
        '                            If strCCAssignee <> "" Then
        '                                strCC = strCC & "," & strCCAssignee
        '                            End If
        '                        End If
        '                        dtMergeFields.Rows.Add(drNew)

        '                        objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), strCC, Session("UserEmail"), strTo, dtMergeFields)
        '                    End If
        '                Catch ex As Exception

        '                End Try
        '            Else
        '                dtProjectProcess.Rows(dtProjectProcess.Rows.Count - 1).Item("bitStageCompleted") = 0
        '            End If
        '            dtProjectProcess.Rows(dtProjectProcess.Rows.Count - 1).Item("vcComments") = txtDealClosedComments.Text
        '            Session("ProProcessDetails") = dtProjectProcess
        '        End If
        '    End If
        'Catch ex As Exception
        '    Response.Write(ex)
        'End Try
    End Sub



    Private Sub btnTCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTCancel.Click
        Try
            PageRedirect()
        Catch ex As Exception
            Response.Write(ex)
        End Try
    End Sub

    'Private Sub btnCustGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCustGo.Click
    '    Try
    '        FillCustomer(ddlcompany, txtComp.Text)
    '    Catch ex As Exception
    '        Response.Write(ex)
    '    End Try
    'End Sub

    Function FillCustomer(ByVal ddlCombo As DropDownList, ByVal strName As String)
        Dim fillCombo As New COpportunities
        With fillCombo
            .DomainID = Session("DomainID")
            .CompanyID = 0
            .CompFilter = Trim(strName) & "%"
            ddlCombo.DataSource = fillCombo.ListCustomer().Tables(0).DefaultView
            ddlCombo.DataTextField = "vcCompanyname"
            ddlCombo.DataValueField = "numDivisionID"
            ddlCombo.DataBind()
        End With
        ddlCombo.Items.Insert(0, New ListItem("--Select One--", "0"))
    End Function

    'Private Sub btnAddContact_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddContact.Click
    '    Try
    '        Dim dtContactInfo As New DataTable
    '        Dim objOpportunity As New MOpportunity(Session("userid"))
    '        objOpportunity.ContactID = CInt(ddlAssocContactId.SelectedItem.Value)
    '        dtContactInfo = objOpportunity.ContactInfo
    '        If Session("dtContactInfo") Is Nothing Then
    '            Session("dtContactInfo") = dtContactInfo
    '        Else
    '            If dtContactInfo.Rows.Count <> 0 Then
    '                Dim dtNw As New DataTable
    '                dtNw = Session("dtContactInfo")
    '                Dim dsNew As DataRow
    '                dsNew = dtNw.NewRow
    '                dsNew.Item(0) = dtContactInfo.Rows(0).Item(0)
    '                dsNew.Item(1) = dtContactInfo.Rows(0).Item(1)
    '                dsNew.Item(2) = dtContactInfo.Rows(0).Item(2)
    '                dsNew.Item(3) = dtContactInfo.Rows(0).Item(3)
    '                dsNew.Item(4) = dtContactInfo.Rows(0).Item(4)
    '                dsNew.Item(5) = dtContactInfo.Rows(0).Item(5)
    '                dsNew.Item(6) = dtContactInfo.Rows(0).Item(6)
    '                dsNew.Item(7) = dtContactInfo.Rows(0).Item(7)
    '                dtNw.Rows.Add(dsNew)
    '                Session("dtContactInfo") = dtNw
    '            End If
    '        End If
    '        BindContactInfo()
    '    Catch ex As Exception
    '        Response.Write(ex)
    '    End Try
    'End Sub

    Sub PageRedirect()
        If GetQueryStringVal(Request.QueryString("enc"), "frm") = "ProjectList" Then
            Response.Redirect("../projects/frmProjectList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
        ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "Projectdtl" Then
            Response.Redirect("../pagelayout/frmProjectdtl.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
        End If
    End Sub



    Public Function FillContact(ByVal ddl As DropDownList, ByVal DivisionID As Long)
        Dim objOpport As New COpportunities
        With objOpport
            .DivisionID = DivisionID
        End With
        With ddl
            .DataSource = objOpport.ListContact().Tables(0).DefaultView()
            .DataTextField = "Name"
            .DataValueField = "numcontactId"
            .DataBind()
            .ClearSelection()
        End With

    End Function

    
    Private Sub updatepanel_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles UpdatePanel.Load

    End Sub

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Response.Redirect("../projects/frmProjects.aspx?frm=Projectdtl" & "&SI=" & uwOppTab.SelectedTabIndex & "&SI1=" & uwOppTab.SelectedTabIndex & "&SI2=" & SI1 & "&frm1=" & frm & "&frm2=" & frm1)

    End Sub
End Class


