<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmCustAccountdtl.aspx.vb"
    Inherits="BACRMPortal.frmCustAccountdtl" %>

<%@ Register TagPrefix="menu1" TagName="Menu" Src="../common/topbar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <link rel="stylesheet" href="~/CSS/lists.css" type="text/css" />
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <title>Accounts</title>
    <script language="javascript" type="text/javascript">
        function ShowLayout(a, b, c) {
            window.open("../pagelayout/frmCustomisePageLayout.aspx?Ctype=" + a + "&type=" + c, '', 'toolbar=no,titlebar=no,width=800,height=500,top=200,scrollbars=yes,resizable=yes');
            return false;
        }
        function FillAddress(a) {
            if (document.all) {
                document.getElementById('lblAddress').innerText = a;
            } else {
                document.getElementById('lblAddress').textContent = a;
            }
            return false;
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function fn_GoToURL(varURL) {

            if ((varURL != '') && (varURL.substr(0, 7) == 'http://') && (varURL.length > 7)) {
                var LoWindow = window.open(varURL, "", "");
                LoWindow.focus();
            }
            return false;
        }
        function fn_EditLabels(URL) {
            window.open(URL, 'WebLinkLabel', 'width=500,height=70,status=no,scrollbar=yes,top=110,left=150');
            return false;
        }
        function Opentransfer(url) {
            window.open(url, '', "width=340,height=200,status=no,top=100,left=150");
            return false;
        }
        function OpenLst(url) {
            window.open(url, '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenAdd(a) {
            window.open("../prospects/frmProspectsAdd.aspx", '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=no,resizable=no')
            return false;
        }

        function OpenTo() {
            window.open("../Admin/frmToCompanyAssociation.aspx", '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenFrom() {
            window.open("../Admin/frmFromCompanyAssociation.aspx", '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
    </script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <menu1:Menu ID="webmenu1" runat="server"></menu1:Menu>
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="updatepanel1" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
        EnableViewState="true">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td>
                        <table id="tblMenu" bordercolor="black" cellspacing="0" cellpadding="0" width="100%"
                            border="0" runat="server">
                            <tr>
                                <td class="tr1" align="center">
                                    <b>Record Owner: </b>
                                    <asp:Label ID="lblRecordOwner" runat="server" ForeColor="Black"></asp:Label>
                                </td>
                                <td class="td1" width="1" height="18">
                                </td>
                                <td class="tr1" align="center">
                                    <b>Created By: </b>
                                    <asp:Label ID="lblCreatedBy" runat="server" ForeColor="Black"></asp:Label>
                                </td>
                                <td class="td1" width="1" height="18">
                                </td>
                                <td class="tr1" align="center">
                                    <b>Last Modified By: </b>
                                    <asp:Label ID="lblLastModifiedBy" runat="server" ForeColor="Black"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br>
            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td>
                                </td>
                                <td class="normal1" align="center">
                                    Organization ID :
                                    <asp:Label ID="lblCustomerId" runat="server"></asp:Label>
                                </td>
                                <td align="right">
                                    <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit" Width="50">
                                    </asp:Button>
                                    <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close"></asp:Button>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            <%--    <tr>
                    <td>
                        <igtab:UltraWebTab ImageDirectory="" ID="uwOppTab" runat="server" ThreeDEffect="True"
                            BorderStyle="Solid" Width="100%" BarHeight="0" BorderWidth="0">
                            <DefaultTabStyle Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Arial">
                            </DefaultTabStyle>
                            <RoundedImage LeftSideWidth="7" RightSideWidth="8" ShiftOfImages="0" SelectedImage="../images/ig_tab_winXPs3.gif"
                                NormalImage="../images/ig_tab_winXP3.gif" HoverImage="../images/ig_tab_winXPs3.gif"
                                FillStyle="LeftMergedWithCenter"></RoundedImage>
                            <SelectedTabStyle Height="23px" ForeColor="white">
                            </SelectedTabStyle>
                            <HoverTabStyle Height="23px" ForeColor="white">
                            </HoverTabStyle>
                            <Tabs>
                                <igtab:Tab Text="&nbsp;&nbsp;&nbsp;Organization Detail&nbsp;&nbsp;&nbsp;">
                                    <ContentTemplate>
                                        <asp:Table ID="tblProspects" runat="server" BorderWidth="1" Width="100%" GridLines="None"
                                            BorderColor="black" CssClass="aspTableDTL" Height="200">
                                            <asp:TableRow>
                                                <asp:TableCell VerticalAlign="top">
                                                    <asp:Table Width="100%" ID="tbl12" runat="server">
                                                        <asp:TableRow>
                                                            <asp:TableCell HorizontalAlign="Right" ColumnSpan="2">
                                                                <asp:Button ID="btnLayout" runat="server" CssClass="button" Text="Layout"></asp:Button>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell VerticalAlign="Top"><img src="../images/Building-48.gif" /></asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:Table runat="server" ID="tabledetail" BorderWidth="0" GridLines="none" CellPadding="2"
                                                                    CellSpacing="0" HorizontalAlign="Center">
                                                                </asp:Table>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell ColumnSpan="2">
                                                                <asp:Table runat="server" ID="tableComment" BorderWidth="" Width="100%" GridLines="none"
                                                                    HorizontalAlign="Center">
                                                                </asp:Table>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                    </asp:Table>
                                                </asp:TableCell></asp:TableRow>
                                        </asp:Table>
                                    </ContentTemplate>
                                </igtab:Tab>
                            </Tabs>
                        </igtab:UltraWebTab>
                    </td>
                </tr>--%>
            </table>
            <table width="100%">
                <tr>
                    <td class="normal4" align="center">
                        <asp:Literal ID="litMessage" runat="server"></asp:Literal>
                    </td>
                </tr>
            </table>
            <asp:TextBox ID="txtHidden" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtrOwner" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="type" Style="display: none" runat="server" Text="0"></asp:TextBox>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
