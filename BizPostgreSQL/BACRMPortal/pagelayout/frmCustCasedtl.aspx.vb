Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Opportunities
Imports Infragistics.WebUI.UltraWebTab
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Contract
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents
Imports System.Reflection

Partial Class frmCustCasedtl : Inherits BACRMPage

    Dim objCommon As New CCommon
    Dim objContacts As CContacts
    Dim objCases As New CaseIP
    Dim objSolution As Solution
    Dim objCus As CustomFields
    Dim objContracts As CContracts
    Dim SI As Integer = 0
    Dim SI1 As Integer = 0
    Dim SI2 As Integer = 0
    Dim frm As String = ""
    Dim frm1 As String = ""
    Dim frm2 As String = ""
    Dim lngCaseId As Long
    Dim lngCntId As Long
    Dim m_aryRightsForCusFlds() As Integer 'm_aryRightsForLinkSol()
    Dim strColumn As String
    Dim boolIntermediatoryPage As Boolean = False
    Dim dtCustomFieldTable As DataTable
    Dim dtTableInfo As DataTable
    Dim objPageControls As New PageControls

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("UserContactID") = Nothing Then
                Response.Redirect("../Common/frmLogout.aspx")
            End If
            If Not IsPostBack Then
                If Session("EnableIntMedPage") = 1 Then
                    ViewState("IntermediatoryPage") = True
                Else
                    ViewState("IntermediatoryPage") = False
                End If
            End If
            boolIntermediatoryPage = ViewState("IntermediatoryPage")
            'ControlSettings()
            If Not GetQueryStringVal( "SI") Is Nothing Then
                SI = GetQueryStringVal( "SI")
            End If
            If Not GetQueryStringVal( "SI1") Is Nothing Then
                SI1 = GetQueryStringVal( "SI1")
            Else : SI1 = 0
            End If
            If Not GetQueryStringVal( "SI2") Is Nothing Then
                SI2 = GetQueryStringVal( "SI2")
            Else : SI2 = 0
            End If
            If Not GetQueryStringVal( "frm") Is Nothing Then
                frm = ""
                frm = GetQueryStringVal( "frm")
            Else : frm = ""
            End If
            If Not GetQueryStringVal( "frm1") Is Nothing Then
                frm1 = ""
                frm1 = GetQueryStringVal( "frm1")
            Else : frm1 = ""
            End If
            If Not GetQueryStringVal( "SI2") Is Nothing Then
                frm2 = ""
                frm2 = GetQueryStringVal( "frm2")
            Else : frm2 = ""
            End If
            lngCaseId = Session("CaseID")
            
            m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmCases.aspx", Session("UserContactID"), 15, 8)

            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AC")
            Else
                'If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                '    btnSave.Visible = False
                '    btnSaveClose.Visible = False
                'End If
                If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                    btnAddItem.Visible = False
                    btnRemove.Visible = False
                    frmComments1.HasEditRights = False
                Else
                    frmComments1.HasEditRights = True
                End If
                If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then btnActDelete.Visible = False
            End If

            If Not IsPostBack Then
                If radCaseTab.Tabs.Count > SI Then radCaseTab.SelectedIndex = SI
                If objContacts Is Nothing Then objContacts = New CContacts

                objContacts.RecID = lngCaseId
                objContacts.Type = "S"
                objContacts.UserCntID = Session("UserContactID")
                objContacts.AddVisiteddetails()
                Session("show") = 1
                Session("Help") = Request.Url.Segments(Request.Url.Segments.Length - 1) ' = Request.Url.Segments(Request.Url.Segments.Length - 1) ' = "Support"

                LoadInformation()
            End If

            If IsPostBack Then
                LoadInformation()
            End If
            m_aryRightsForCusFlds = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmCases.aspx", Session("UserContactID"), 7, 5)
            If m_aryRightsForCusFlds(RIGHTSTYPE.VIEW) <> 0 Then
                DisplayDynamicFlds()
            End If
            frmComments1.CaseID = lngCaseId
            'btnLayout.Attributes.Add("onclick", "return ShowLayout('S','" & lngCaseId & "');")
            btnActDelete.Attributes.Add("onclick", "return DeleteRecord()")
            btnAddItem.Attributes.Add("onclick", "return OpenItems(" & lngCaseId & ");")
            'btnSave.Attributes.Add("onclick", "return Save()")
            'btnSaveClose.Attributes.Add("onclick", "return Save()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    'Sub LoadDetails()
    '    Try
    '        If radCaseTab.SelectedTabIndex = 0 Then
    '            If IsPostBack Then LoadInformation()
    '            'ElseIf radCaseTab.SelectedTabIndex = 3 Then
    '            '    EmailHistory()
    '        ElseIf radCaseTab.SelectedTabIndex = 2 Then
    '            ClientScript.RegisterStartupScript(Me.GetType, "ifrm", "document.getElementById('radCaseTab__ctl2_IframeCorrespondence').src='../admin/frmCorrespondense.aspx?RecordID=" & lngCaseId.ToString & "&Mode=6';", True)
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Private Sub BindSolutions()
        Try
            If objSolution Is Nothing Then objSolution = New Solution
            Dim dtSolution As DataTable
            objSolution.CaseID = lngCaseId
            dtSolution = objSolution.GetSolutionForCases
            dgSolution.DataSource = dtSolution
            dgSolution.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadInformation()
        Try

            objCases.CaseID = lngCaseId
            objCases.DomainID = Session("DomainID")
            objCases.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            objCases.GetCaseDetails()

            If Not IsPostBack Then


                Dim dtCaseDetails As DataTable

                dtCaseDetails = objCases.GetCaseDTL
                If dtCaseDetails.Rows.Count > 0 Then
                    hplCustomer.Text = dtCaseDetails.Rows(0).Item("vcCompanyName")
                    txtCntId.Text = dtCaseDetails.Rows(0).Item("numContactID")
                    lngCntId = dtCaseDetails.Rows(0).Item("numContactID")
                    txtDivId.Text = dtCaseDetails.Rows(0).Item("numDivisionID")
                    If dtCaseDetails.Rows(0).Item("tintCRMType") = 0 Then
                        hplCustomer.NavigateUrl = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=CaseDetail&DivID=" & dtCaseDetails.Rows(0).Item("numDivisionID") & "&CaseID=" & lngCaseId & "&frm1=" & GetQueryStringVal( "frm")

                    ElseIf dtCaseDetails.Rows(0).Item("tintCRMType") = 1 Then
                        hplCustomer.NavigateUrl = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=CaseDetail&DivID=" & dtCaseDetails.Rows(0).Item("numDivisionID") & "&CaseID=" & lngCaseId & "&frm1=" & GetQueryStringVal( "frm")

                    ElseIf dtCaseDetails.Rows(0).Item("tintCRMType") = 2 Then
                        hplCustomer.NavigateUrl = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=CaseDetail&klds+7kldf=fjk-las&DivId=" & dtCaseDetails.Rows(0).Item("numDivisionID") & "&CaseID=" & lngCaseId & "&frm1=" & GetQueryStringVal( "frm")

                    End If

                    If CCommon.ToLong(dtCaseDetails.Rows(0).Item("numContractID")) > 0 Then
                        lblContractName.Text = dtCaseDetails.Rows(0).Item("vcContractName")
                        BindContractinfo(CCommon.ToLong(dtCaseDetails.Rows(0).Item("numContractID")))
                    Else
                        pnlContract.Visible = False
                    End If
                    lblcasenumber.Text = dtCaseDetails.Rows(0).Item("vcCaseNumber")
                    lblCaseStatus.Text = dtCaseDetails.Rows(0).Item("vcCaseStatus")

                End If

                BindSolutions()

                BindLinkedItems()

            End If
            LoadControls()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Sub LoadControls()
        Try
            tblMain.Controls.Clear()
            Dim ds As DataSet
            Dim objPageLayout As New CPageLayout
            Dim fields() As String

            objPageLayout.CoType = "S"
            objPageLayout.UserCntID = Session("UserContactId")
            objPageLayout.RecordId = lngCaseId
            'objPageLayout.ContactID = lngCaseId
            objPageLayout.DomainID = Session("DomainID")
            objPageLayout.PageId = 3
            ds = objPageLayout.GetTableInfoDefault()  ' getting the table structure 
            dtTableInfo = ds.Tables(0)


            Dim tblRow As TableRow
            Const NoOfColumns As Short = 2
            Dim ColCount As Int32 = 0
            ''If intermediatory Page is enabled then Add the text "Name" to dropdown fields
            objPageControls.CreateTemplateRow(tblMain)
            For Each dr As DataRow In dtTableInfo.Rows
                If boolIntermediatoryPage = True Then
                    If Not IsDBNull(dr("vcPropertyName")) And dr("fld_type") = "Drop Down List Box" And dr("bitCustomField") = False Then
                        dr("vcPropertyName") = dr("vcPropertyName") & "Name"
                    End If
                End If

                If Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False Then
                    dr("vcValue") = objCases.GetType.GetProperty(dr("vcPropertyName")).GetValue(objCases, Nothing)
                End If

                If ColCount = 0 Then
                    tblRow = New TableRow
                End If
                If (dr("fld_type") = "Drop Down List Box" Or dr("fld_type") = "List Box") Then
                    Dim dtData As DataTable
                    Dim dtSelOpportunity As DataTable

                    If Not IsDBNull(dr("numListID")) Then
                        If boolIntermediatoryPage = False Then
                            If dr("ListRelID") > 0 Then
                                objCommon.Mode = 3
                                objCommon.DomainID = Session("DomainID")
                                objCommon.PrimaryListItemID = objPageControls.GetPrimaryListItemID(dr("ListRelID"), dtTableInfo, objCases)
                                objCommon.SecondaryListID = dr("numListId")
                                dtData = objCommon.GetFieldRelationships.Tables(0)
                            Else
                                dtData = objCommon.GetMasterListItems(dr("numListID"), Session("DomainID"))
                            End If
                        End If
                    ElseIf dr("numFieldId") = 255 Then
                        objCommon.CaseID = lngCaseId
                        objCommon.charModule = "S"
                        objCommon.GetCompanySpecificValues1()
                        objCases.DomainID = Session("DomainId")
                        objCases.DivisionID = objCommon.DivisionID
                        objCases.byteMode = 1
                        objCases.CaseID = lngCaseId
                        If boolIntermediatoryPage = False Then
                            dtData = objCases.GetOpportunities
                        End If
                        dtSelOpportunity = objCases.GetCaseOpportunities
                    ElseIf dr("numFieldId") = 137 Then
                        If boolIntermediatoryPage = False Then
                            If Session("PopulateUserCriteria") = 1 Then
                                objCommon.CaseID = lngCaseId
                                objCommon.charModule = "S"
                                objCommon.GetCompanySpecificValues1()
                                dtData = objCommon.ConEmpListFromTerritories(Session("DomainID"), 0, 0, objCommon.TerittoryID)
                            ElseIf Session("PopulateUserCriteria") = 2 Then
                                dtData = objCommon.ConEmpList(Session("DomainID"), Session("UserContactID"))
                            Else
                                dtData = objCommon.ConEmpList(Session("DomainID"), 0, 0)
                            End If

                        End If
                    End If
                    If boolIntermediatoryPage Then
                        objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData, dtSelOpportunity, lngCaseID:=lngCaseId)
                    Else
                        Dim ddl As DropDownList
                        ddl = objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData, dtSelOpportunity, lngCaseID:=lngCaseId)
                        If CLng(dr("DependentFields")) > 0 And Not boolIntermediatoryPage Then
                            ddl.AutoPostBack = True
                            AddHandler ddl.SelectedIndexChanged, AddressOf PopulateDependentDropdown
                        End If
                    End If
                Else
                    objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, RecordID:=lngCntId, lngCaseID:=lngCaseId)
                End If


                ColCount = ColCount + 1
                If NoOfColumns = ColCount Then
                    ColCount = 0
                    tblMain.Rows.Add(tblRow)
                End If
            Next

            If ColCount > 0 Then
                tblMain.Rows.Add(tblRow)
            End If

            objPageControls.CreateComments(tblMain, objCases.Comments, boolIntermediatoryPage)
            objPageControls.CreateInternalComments(tblMain, objCases.InternalComments, boolIntermediatoryPage)
            ''Add Client Side validation for custom fields
            'If Not boolIntermediatoryPage Then
            '    Dim strValidation As String = objPageControls.GenerateValidationScript(dtTableInfo)
            '    ClientScript.RegisterClientScriptBlock(Me.GetType, "CFvalidation", strValidation, True)
            'End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Sub DisplayDynamicFlds()
        Try
            objPageControls.DisplayDynamicFlds(lngCaseId, 0, Session("DomainID"), objPageControls.Location.Support, radCaseTab, 12)


        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub SaveCusField()
        Try
            objPageControls.SaveCusField(lngCaseId, 0, Session("DomainID"), objPageControls.Location.Support, radCaseTab)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            PageRedirect()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub PageRedirect()
        Try
            If GetQueryStringVal( "frm") = "Solution" Then
                Response.Redirect("../cases/frmSolution.aspx?SolId=" & GetQueryStringVal( "SolId") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
            ElseIf GetQueryStringVal( "frm") = "tickler" Then
                Response.Redirect("../common/frmticklerdisplay.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
            ElseIf GetQueryStringVal( "Index") <> "" Then
                Response.Redirect("../Cases/frmCusCaseList.aspx?Index=" & GetQueryStringVal( "Index") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
            ElseIf GetQueryStringVal( "frm") = "accounts" Then
                objCommon.CaseID = lngCaseId
                objCommon.charModule = "S"
                objCommon.GetCompanySpecificValues1()
                Response.Redirect("../Account/frmAccounts.aspx?klds+7kldf=fjk-las&DivId=" & objCommon.DivisionID & "&CaseID=" & lngCaseId & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
            ElseIf GetQueryStringVal( "frm") = "CaseReport" Then
                Response.Redirect("../reports/frmCases.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
            ElseIf GetQueryStringVal( "frm") = "AdvSer" Then
                Response.Redirect("../Admin/frmAdvCaseRes.aspx")
            ElseIf GetQueryStringVal( "frm") = "Dashboard" Then
                Response.Redirect("../Dashboard/frmPortalDashboard.aspx")
            Else : Response.Redirect("../Cases/frmCusCaseList.aspx")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub btnActDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActDelete.Click
        Try

            objCases.CaseID = lngCaseId
            objCases.DomainID = Session("DomainID")
            If objCases.DeleteCase() = False Then
                litMessage.Text = "Dependent Records Exists.Cannot be deleted."
            Else : Response.Redirect("../cases/frmCaseList.aspx")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub FillCustomer(ByVal ddlCombo As DropDownList, ByVal strName As String)
        Try

            If objCommon Is Nothing Then objCommon = New CCommon
            With objCommon
                .DomainID = Session("DomainID")
                .UserCntID = Session("UserContactID")
                .Filter = Trim(strName) & "%"
                ddlCombo.DataSource = objCommon.PopulateOrganization.Tables(0).DefaultView
                ddlCombo.DataTextField = "vcCompanyname"
                ddlCombo.DataValueField = "numDivisionID"
                ddlCombo.DataBind()
            End With
            ddlCombo.Items.Insert(0, New ListItem("--Select One--", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Public Sub FillContact(ByVal ddlCombo As DropDownList, ByVal lngDivision As Long)
        Try
            Dim fillCombo As New COpportunities
            With fillCombo
                .DivisionID = lngDivision
                ddlCombo.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
                ddlCombo.DataTextField = "Name"
                ddlCombo.DataValueField = "numcontactId"
                ddlCombo.DataBind()
            End With
            ddlCombo.Items.Insert(0, New ListItem("---Select One---", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Sub BindContractinfo(ByVal ContractID As Long)
        Try
            If objContracts Is Nothing Then objContracts = New CContracts
            Dim dtTable As DataTable
            objContracts.ContractID = ContractID
            objContracts.UserCntId = Session("UserContactId")
            objContracts.DomainId = Session("DomainId")
            dtTable = objContracts.GetContractDtl()
            If dtTable.Rows.Count > 0 Then
                lblRemAmount.Text = IIf(IsDBNull(dtTable.Rows(0).Item("RemAmount")), 0, String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("RemAmount")))
                lblRemHours.Text = IIf(IsDBNull(dtTable.Rows(0).Item("RemHours")), 0, String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("RemHours")))
                lblRemDays.Text = IIf(IsDBNull(dtTable.Rows(0).Item("days")), 0, String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("days")))
                If dtTable.Rows(0).Item("bitincidents") = True Then
                    lblRemInci.Text = IIf(IsDBNull(dtTable.Rows(0).Item("Incidents")), 0, dtTable.Rows(0).Item("Incidents"))
                Else : lblRemInci.Text = 0
                End If
            Else
                lblRemAmount.Text = 0
                lblRemHours.Text = 0
                lblRemDays.Text = 0
                lblRemInci.Text = 0
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub PopulateDependentDropdown(ByVal sender As Object, ByVal e As EventArgs)
        objPageControls.PopulateDropdowns(CType(sender, DropDownList), dtTableInfo, objCommon, radCaseTab, Session("DomainID"))
    End Sub

    Private Sub BindLinkedItems()
        Try
            Dim objCase As New CCases
            objCase.DomainID = Session("DomainID")
            objCase.CaseID = lngCaseId
            objCase.strOppSel = ""
            dgLinkedItems.DataSource = objCase.GetCaseOpportunities()
            dgLinkedItems.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        Try
            If hdnOppItemIDs.Value.Trim() <> "" Then
                Dim objCase As New CCases
                objCase.DomainID = Session("DomainID")
                objCase.CaseID = lngCaseId
                objCase.byteMode = 1 ' delete selected Opp items
                objCase.strOppSel = hdnOppItemIDs.Value
                objCase.SaveCaseOpportunities()
            End If
            BindLinkedItems()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub dgSolution_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgSolution.ItemCommand
        Try
            If e.CommandName = "Solution" Then
                Dim lngSolId As Long = e.Item.Cells(0).Text
                Response.Redirect("../KnowledgeBase/frmSoution.aspx?SolId=" & lngSolId & "&SI1=" & radCaseTab.SelectedIndex & "&SI2=" & SI1 & "&frm=frmCases" & "&frm1=" & frm1 & "&frm1=" & frm2 & "&CaseId=" & lngCaseId.ToString, False)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class
