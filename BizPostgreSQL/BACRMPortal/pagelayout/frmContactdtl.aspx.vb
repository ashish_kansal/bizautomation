

Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports Infragistics.WebUI.UltraWebTab
Partial Class frmContactdtl
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Dim intCRMType, intCntId, intCmpId, intDivId, intCommId, RcNo, lngcntid As Integer
    Dim FromDate, ToDate As String
    Dim m_aryRightsForCusFlds(), m_aryRightsForEmail(), m_aryRightsForOpp(), m_aryRightsForPage(), m_aryRightsForAOI() As Integer
    Protected WithEvents btnGo As System.Web.UI.WebControls.Button
    Protected WithEvents btnAssGo As System.Web.UI.WebControls.Button
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Dim SI As Integer = 0
    Dim SI1 As Integer = 0
    Dim SI2 As Integer = 0
    Dim frm As String = ""
    Dim frm1 As String = ""
    Dim frm2 As String = ""
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Session("ConID") = Nothing Then
            Response.Redirect("../Common/frmLogout.aspx")
        End If
        If Not GetQueryStringVal(Request.QueryString("enc"), "SI") Is Nothing Then
            SI = GetQueryStringVal(Request.QueryString("enc"), "SI")
        End If
        If Not GetQueryStringVal(Request.QueryString("enc"), "SI1") Is Nothing Then
            SI1 = GetQueryStringVal(Request.QueryString("enc"), "SI1")
        Else
            SI1 = 0
        End If
        If Not GetQueryStringVal(Request.QueryString("enc"), "SI2") Is Nothing Then
            SI2 = GetQueryStringVal(Request.QueryString("enc"), "SI2")
        Else
            SI2 = 0
        End If
        If Not GetQueryStringVal(Request.QueryString("enc"), "frm") Is Nothing Then
            frm = ""
            frm = GetQueryStringVal(Request.QueryString("enc"), "frm")
        Else
            frm = ""
        End If
        If Not GetQueryStringVal(Request.QueryString("enc"), "frm1") Is Nothing Then
            frm1 = ""
            frm1 = GetQueryStringVal(Request.QueryString("enc"), "frm1")
        Else
            frm1 = ""
        End If
        If Not GetQueryStringVal(Request.QueryString("enc"), "SI2") Is Nothing Then
            frm2 = ""
            frm2 = GetQueryStringVal(Request.QueryString("enc"), "frm2")
        Else
            frm2 = ""
        End If
        If Session("ConID") = Nothing Then
            Response.Redirect("../Common/frmLogout.aspx")
        End If
        intCRMType = Session("CRMType")
        intCmpId = Session("CompID")
        intDivId = Session("DivId")
        intCntId = Session("ContactId")
        lngCntid = Session("conid")

        LoadTableInformation()
        If Not IsPostBack Then
            m_aryRightsForPage = clsAuthorization.fn_ExternalPageLevelRights("frmContacts.aspx", Session("ConID"), 15, 3)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../Common/frmAuthorization.aspx")
            End If
            LoadInformation()
            Dim dtTab As New DataTable
            dtTab = Session("DefaultTab")
            If dtTab.Rows.Count > 0 Then
                uwOppTab.Tabs(0).Text = dtTab.Rows(0).Item("vcContact") & " Details"
            Else
                uwOppTab.Tabs(0).Text = "Contact Details"
            End If
        End If
        btnLayout.Attributes.Add("onclick", "return ShowLayout('N','" & intCntId & "','" & type.Text & "');")
        DisplayDynamicFlds()
        If Not IsPostBack Then
            If uwOppTab.Tabs.Count > SI Then
                uwOppTab.SelectedTabIndex = SI
            End If
        End If
    End Sub
    Sub LoadTableInformation()

        Dim dtTableInfo As New DataTable
        Dim dtTablecust As New DataTable
        Dim ds As New DataSet
        Dim objPageLayout As New CPageLayout
        Dim check As String
        Dim fields() As String
        Dim idcolumn As String = ""
        Dim count1 As Integer
        Dim x As Integer

        Dim dtContactInfo As DataTable
        Dim objContacts As New CContacts
        objPageLayout.ContactID = intCntId
        objPageLayout.DomainID = Session("DomainID")
        dtContactInfo = objPageLayout.GetContactInfoEditIP      ' getting the details

        Type.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("numContactType")), 0, dtContactInfo.Rows(0).Item("numContactType"))
        txtEmail.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcEmail")), "", dtContactInfo.Rows(0).Item("vcEmail"))
        objPageLayout.CoType = "N"
        objPageLayout.numUserCntID = Session("ConId")
        objPageLayout.RecordId = intCntId
        objPageLayout.DomainID = Session("DomainID")
        objPageLayout.PageId = 4
        objPageLayout.numRelCntType = IIf(IsDBNull(dtContactInfo.Rows(0).Item("numContactType")), 0, dtContactInfo.Rows(0).Item("numContactType"))
        ds = objPageLayout.GetTableInfoDefaultPortal()  ' getting the table structure 
        dtTableInfo = ds.Tables(0)
        Dim lngCntid As Integer
        lngCntid = intCntId
        If ds.Tables.Count = 2 Then
            dtTablecust = ds.Tables(1)
            dtTableInfo.Merge(dtTablecust)
        End If
        Dim dv As DataView
        dv = New DataView(dtTableInfo)

        Dim numrows As Integer = dtTableInfo.Compute("Max(tintrow)", String.Empty)
        Dim numcells As Integer = dtTableInfo.Compute("Max(intcoulmn)", String.Empty)
        Dim nr As Integer
        For nr = 0 To numrows - 1
            Dim r As New TableRow()
            Dim nc As Integer
            Dim ro As Integer = nr
            For nc = 0 To numcells - 1

                dv.RowFilter = "tintrow =" & nr + 1 & "and intcoulmn=" & nc + 1
                If dv.Count > 0 Then
                    Dim dvi As Integer
                    For dvi = 0 To dv.Count - 1

                        Dim column1 As New TableCell
                        Dim column2 As New TableCell
                        Dim fieldId As Integer
                        Dim bitDynFld As String
                        fieldId = CInt(dv(dvi).Item("numFieldID").ToString)
                        bitDynFld = dv(dvi).Item("bitCustomField")
                        column1.CssClass = "normal7"
                        If (bitDynFld <> "1") Then



                            column1.Text = dv(dvi).Item("vcFieldName").ToString & "&nbsp;:"


                            If Not IsDBNull(dv(dvi).Item("vcDBColumnName").ToString) And dv(dvi).Item("vcDBColumnName").ToString() <> "" Then
                                Dim temp As String
                                temp = dv(dvi).Item("vcDBColumnName").ToString
                                fields = temp.Split(",")
                                Dim j As Integer
                                j = 0

                                While (j < fields.Length)
                                    If (fieldId = "162") Then

                                        Dim listvalue As String
                                        Dim value As String = ""
                                        listvalue = IIf(IsDBNull(dtContactInfo.Rows(0).Item(fields(j))), "-", dtContactInfo.Rows(0).Item(fields(j)))

                                        Select Case fieldId   ' Must be a primitive data type
                                            Case 162

                                                If listvalue = "M" Then
                                                    value = "Male"
                                                ElseIf listvalue = "F" Then
                                                    value = "Female"
                                                Else
                                                    value = "-"
                                                End If


                                            Case Else

                                        End Select
                                        column2.Text = value
                                    ElseIf (fieldId = "164") Then
                                        If (fields(j) = "AssociateCountTo") Then
                                            Dim h As New HyperLink()
                                            h.CssClass = "hyperlink"
                                            h.Text = "To" & "(" & IIf(IsDBNull(dtContactInfo.Rows(0).Item("AssociateCountTo")), "", dtContactInfo.Rows(0).Item("AssociateCountTo")) & ")" & "/"
                                            h.Attributes.Add("onclick", "return OpenTo();")
                                            column2.Controls.Add(h)
                                        ElseIf (fields(j) = "AssociateCountFrom") Then
                                            Dim h As New HyperLink()
                                            h.CssClass = "hyperlink"
                                            h.Text = "From" & "(" & IIf(IsDBNull(dtContactInfo.Rows(0).Item("AssociateCountFrom")), "", dtContactInfo.Rows(0).Item("AssociateCountFrom")) & ")"
                                            h.Attributes.Add("onclick", "return OpenFrom();")
                                            column2.Controls.Add(h)
                                        End If

                                    ElseIf (fieldId = "141" Or fieldId = "142" Or fieldId = "165") Then
                                        Dim email As String = ""
                                        Dim h As New HyperLink
                                        ' h.NavigateUrl = "#"
                                        h.CssClass = "hyperlink"
                                        h.ID = "email" & fieldId
                                        email = IIf(IsDBNull(dtContactInfo.Rows(0).Item(fields(j))), "-", dtContactInfo.Rows(0).Item(fields(j).ToString))
                                        h.Text = email
                                        h.Attributes.Add("onclick", "return fn_Mail('" & email & "','" & ConfigurationManager.AppSettings("EmailLink") & "','" & lngCntid & "');")
                                        column2.Controls.Add(h)
                                    ElseIf (fieldId = "163") Then
                                        Dim l As New Label
                                        l.CssClass = "cell"
                                        If Not (IsDBNull(dtContactInfo.Rows(0).Item("bitOptOut"))) Then
                                            l.Text = IIf(dtContactInfo.Rows(0).Item("bitOptOut"), "a", "r")
                                        Else
                                            l.Text = "r"
                                        End If
                                        column2.Controls.Add(l)
                                    Else

                                        column2.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item(fields(j))), "-", dtContactInfo.Rows(0).Item(fields(j)))

                                    End If
                                    j += 1
                                End While

                            Else
                                column1.Text = ""
                            End If ' end of table cell2
                        Else
                            If (dtTablecust.Rows(0).Item("tabletype") = "0") Then
                                Dim dv2 As New DataView(dtTablecust)
                                dv2.RowFilter = "numFieldId = " & fieldId
                                If (dv2.Count > 0) Then
                                    If IIf(IsDBNull(dv2(0).Item("fld_type")), "", dv2(0).Item("fld_type")) = "Link" Then
                                        column1.Text = "Custom Web Link :"
                                        Dim h As New HyperLink()
                                        h.CssClass = "hyperlink"
                                        Dim url As String = IIf(IsDBNull(dv2(0).Item("vcURL")), "", dv2(0).Item("vcURL"))
                                        url = url.Replace("RecordID", lngCntid)
                                        h.Text = IIf(IsDBNull(dv2(0).Item("vcFieldName")), "-", dv2(0).Item("vcFieldName"))
                                        h.Attributes.Add("onclick", "return fn_GoToURL('" & "http://" & url & "');")
                                        column2.Controls.Add(h)
                                    Else
                                        column1.Text = IIf(IsDBNull(dv2(0).Item("vcFieldName")), "", dv2(0).Item("vcFieldName")) & "&nbsp;:"
                                        If IIf(IsDBNull(dv2(0).Item("fld_type")), "", dv2(0).Item("fld_type")) = "Check box" Then
                                            If Not IsDBNull(dv2(0).Item("vcDBColumnName")) Then
                                                Dim l As New Label
                                                l.Text = IIf(dv2(0).Item("vcDBColumnName") = 1, "a", "r")
                                                l.CssClass = IIf(dv2(0).Item("vcDBColumnName") = 1, "cell1", "cell")
                                                column2.Controls.Add(l)
                                            End If
                                        Else
                                            If Not IsDBNull(dv2(0).Item("vcDBColumnName")) Then
                                                column2.Text = IIf(dv2(0).Item("vcDBColumnName") = "0", "-", dv2(0).Item("vcDBColumnName"))
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If

                        column2.CssClass = "normal1"
                        column1.HorizontalAlign = HorizontalAlign.Right
                        column2.HorizontalAlign = HorizontalAlign.Left
                        column1.Width = 250
                        column2.Width = 300
                        column2.ColumnSpan = 1
                        column1.ColumnSpan = 1
                        r.Cells.Add(column1)
                        r.Cells.Add(column2)
                    Next dvi
                ElseIf (nr <= numrows - 1) Then

                    Dim column1 As New TableCell
                    Dim column2 As New TableCell
                    column1.Text = ""
                    r.Cells.Add(column1)
                    r.Cells.Add(column2)
                End If

            Next nc

            tabledetail.Rows.Add(r)
        Next nr

        If (dtTablecust.Rows.Count > 0) Then
            If (dtTablecust.Rows(0).Item("tabletype") = "1") Then
                x = 1

                Dim ro1 As Integer = numrows - 1
                Dim co As Integer = numcells
                Dim cust As Integer = 0
                Dim i As Integer = 1
                For x = ro1 To ro1 + 1
                    Dim y As Integer = 0
                    Dim nc As Integer
                    Dim ro As Integer = nr
                    While (y < co)
                        dv.RowFilter = "tintrow =" & x + 1 & "and intcoulmn=" & y + 1
                        If Not (dv.Count > 0) Then
                            If (cust < dtTablecust.Rows.Count) Then
                                If (IIf(IsDBNull(dtTablecust.Rows(cust).Item("TabId")), "1", dtTablecust.Rows(cust).Item("TabId")) = "0") Then


                                    Dim dimcol As Integer = 0
                                    dimcol = y
                                    dimcol += dimcol
                                    If IIf(IsDBNull(dtTablecust.Rows(cust).Item("fld_type")), "", dtTablecust.Rows(cust).Item("fld_type")) = "Link" Then
                                        tabledetail.Rows(x).Cells(dimcol).CssClass = "normal7"
                                        tabledetail.Rows(x).Cells(dimcol + 1).CssClass = "normal1"
                                        tabledetail.Rows(x).Cells(dimcol).HorizontalAlign = HorizontalAlign.Right
                                        tabledetail.Rows(x).Cells(dimcol + 1).HorizontalAlign = HorizontalAlign.Left
                                        tabledetail.Rows(x).Cells(dimcol).Text = "Custom web Link :"
                                        Dim h As New HyperLink()
                                        h.CssClass = "hyperlink"
                                        Dim url As String = IIf(IsDBNull(dtTablecust.Rows(cust).Item("vcURL")), "", dtTablecust.Rows(cust).Item("vcURL"))
                                        url = url.Replace("RecordID", lngCntid)
                                        h.Text = IIf(IsDBNull(dtTablecust.Rows(cust).Item("vcFieldName")), "", dtTablecust.Rows(cust).Item("vcFieldName"))
                                        h.Attributes.Add("onclick", "return fn_GoToURL('" & "http://" & url & "');")
                                        tabledetail.Rows(x).Cells(dimcol + 1).Controls.Add(h)

                                    Else
                                        tabledetail.Rows(x).Cells(dimcol).CssClass = "normal7"
                                        tabledetail.Rows(x).Cells(dimcol + 1).CssClass = "normal1"
                                        tabledetail.Rows(x).Cells(dimcol).HorizontalAlign = HorizontalAlign.Right
                                        tabledetail.Rows(x).Cells(dimcol + 1).HorizontalAlign = HorizontalAlign.Left
                                        tabledetail.Rows(x).Cells(dimcol).Text = IIf(IsDBNull(dtTablecust.Rows(cust).Item("vcfieldName")), "-", dtTablecust.Rows(cust).Item("vcfieldName")) & "&nbsp;:"
                                        If IIf(IsDBNull(dtTablecust.Rows(cust).Item("fld_type")), "", dtTablecust.Rows(cust).Item("fld_type")) = "Check box" Then
                                            If Not IsDBNull(dtTablecust.Rows(cust).Item("vcDBColumnName")) Then
                                                Dim l As New Label
                                                l.Text = IIf(dtTablecust.Rows(cust).Item("vcDBColumnName") = 1, "a", "r")
                                                l.CssClass = IIf(dtTablecust.Rows(cust).Item("vcDBColumnName") = 1, "cell1", "cell")
                                                tabledetail.Rows(x).Cells(dimcol + 1).Controls.Add(l)
                                            End If
                                        Else
                                            If Not IsDBNull(dtTablecust.Rows(cust).Item("vcDBColumnName")) Then
                                                tabledetail.Rows(x).Cells(dimcol + 1).Text = IIf(dtTablecust.Rows(cust).Item("vcDBColumnName") = "0", "-", dtTablecust.Rows(cust).Item("vcDBColumnName"))
                                            End If
                                        End If
                                    End If
                                End If
                                cust += 1
                                If (y = co - 1) Then
                                    Dim ra As New TableRow
                                    For i = 1 To numcells

                                        Dim column1 As New TableCell
                                        Dim column2 As New TableCell
                                        column1.Text = ""
                                        column2.Text = ""
                                        ra.Cells.Add(column1)
                                        ra.Cells.Add(column2)

                                    Next
                                    tabledetail.Rows().Add(ra)
                                End If
                            End If
                        End If
                        y += 1
                    End While
                Next
            End If
        End If
        If uwOppTab.Tabs.Count > 1 Then
            Dim iItemcount As Integer
            iItemcount = uwOppTab.Tabs.Count
            While uwOppTab.Tabs.Count > 1
                uwOppTab.Tabs.RemoveAt(iItemcount - 1)
                iItemcount = iItemcount - 1
            End While
        End If
        If dtTablecust.Rows.Count > 0 Then
            Dim Tab As Tab
            'Dim pageView As PageView
            Dim aspTable As HtmlTable
            Dim Table As Table
            Dim tblcell As TableCell
            Dim tblRow As TableRow
            Dim objRow As HtmlTableRow
            Dim objCell As HtmlTableCell


            Dim i, k As Integer
            'Dim dttable As New DataTable

            ' CustomField(Section)

            k = 0
            objRow = New HtmlTableRow
            ViewState("TabId") = dtTablecust.Rows(0).Item("TabId")
            ViewState("Check") = 0
            ViewState("FirstTabCreated") = 0
            For i = 0 To dtTablecust.Rows.Count - 1
                If dtTablecust.Rows(i).Item("TabId") <> 0 Then
                    If ViewState("TabId") <> dtTablecust.Rows(i).Item("TabId") Or ViewState("FirstTabCreated") = 0 Then
                        If ViewState("Check") <> 0 Then
                            aspTable.Rows.Add(objRow)
                            tblcell.Controls.Add(aspTable)
                            tblRow.Cells.Add(tblcell)
                            Table.Rows.Add(tblRow)
                            Tab.ContentPane.Controls.Add(Table)
                        End If
                        k = 0
                        ViewState("Check") = 1
                        '  If Not IsPostBack Then
                        ViewState("FirstTabCreated") = 1
                        ViewState("TabId") = dtTablecust.Rows(i).Item("TabId")
                        Tab = New Tab
                        Tab.Text = "&nbsp;&nbsp;" & dtTablecust.Rows(i).Item("tabname") & "&nbsp;&nbsp;"
                        uwOppTab.Tabs.Add(Tab)
                        'End If
                        'pageView = New PageView
                        aspTable = New HtmlTable
                        Table = New Table
                        Table.Width = Unit.Percentage(100)
                        Table.BorderColor = System.Drawing.Color.FromName("black")
                        Table.GridLines = GridLines.None
                        Table.BorderWidth = Unit.Pixel(1)
                        Table.Height = Unit.Pixel(300)
                        Table.CssClass = "aspTable"
                        tblcell = New TableCell
                        tblRow = New TableRow
                        aspTable.Width = "100%"
                        tblcell.VerticalAlign = VerticalAlign.Top
                        aspTable.Width = "100%"
                        objRow = New HtmlTableRow
                        objCell = New HtmlTableCell
                        objCell.InnerHtml = "<br>"
                        objRow.Cells.Add(objCell)
                        aspTable.Rows.Add(objRow)
                        objRow = New HtmlTableRow
                    End If

                    If k = 3 Then
                        k = 0
                        aspTable.Rows.Add(objRow)
                        objRow = New HtmlTableRow
                    End If
                    objCell = New HtmlTableCell
                    objCell.Align = "right"
                    objCell.Width = 100
                    objCell.Attributes.Add("class", "normal7")
                    ' objCell.Text = dttablecust.Rows(i).Item("vcfieldName") & "&nbsp;:"
                    If dtTablecust.Rows(i).Item("fld_type") <> "Frame" Then
                        If dtTablecust.Rows(i).Item("fld_type") <> "Link" Then
                            objCell.InnerText = dtTablecust.Rows(i).Item("vcfieldName") & " :"
                        End If
                        objRow.Cells.Add(objCell)
                    End If
                    objCell = New HtmlTableCell
                    objCell.Align = "Left"
                    objCell.Attributes.Add("class", "normal1")
                    If dtTablecust.Rows(i).Item("fld_type") = "Link" Then
                        Dim h As New HyperLink()
                        h.Text = IIf(IsDBNull(dtTablecust.Rows(i).Item("vcfieldName")), "-", dtTablecust.Rows(i).Item("vcfieldName"))
                        'h.NavigateUrl = IIf(IsDBNull(dttablecust.Rows(i).Item("vcURL")), "", dttablecust.Rows(i).Item("vcURL"))
                        h.CssClass = "hyperlink"
                        Dim url As String = IIf(IsDBNull(dtTablecust.Rows(i).Item("vcURL")), "", dtTablecust.Rows(i).Item("vcURL"))
                        url = url.Replace("RecordID", lngCntid)
                        h.Attributes.Add("onclick", "fn_GoToURL('" & "http://" & url & "')")
                        objCell.Controls.Add(h)
                        ' CreateLink(objRow, objCell, dtTablecust.Rows(i).Item("fld_id"), dtTablecust.Rows(i).Item("vcURL"), lngCntid, dtTablecust.Rows(i).Item("fld_label"))
                    ElseIf dtTablecust.Rows(i).Item("fld_type") = "Check box" Then
                        If Not IsDBNull(dtTablecust.Rows(i).Item("vcDBColumnName")) Then
                            Dim l As New Label
                            l.Text = IIf(dtTablecust.Rows(i).Item("vcDBColumnName") = "1", "a", "r")
                            l.CssClass = IIf(dtTablecust.Rows(i).Item("vcDBColumnName") = "1", "cell1", "cell")
                            objCell.Controls.Add(l)
                        ElseIf IsDBNull(dtTablecust.Rows(i).Item("vcDBColumnName")) Then
                            Dim l As New Label
                            l.Text = "r"
                            l.CssClass = "cell"
                            objCell.Controls.Add(l)
                        End If
                    ElseIf dtTablecust.Rows(i).Item("fld_type") = "Frame" Then
                        objCell = New HtmlTableCell
                        Dim strFrame As String
                        Dim URL As String
                        URL = dtTablecust.Rows(i).Item("vcURL")
                        URL = URL.Replace("RecordID", lngCntid)
                        strFrame = "<iframe src ='" & URL & "' width='100%' frameborder='0' height= '800px'></iframe>"
                        objCell.Controls.Add(New LiteralControl(strFrame))
                        objRow.Cells.Add(objCell)
                    Else
                        If Not IsDBNull(dtTablecust.Rows(i).Item("vcDBColumnName")) Then
                            objCell.InnerText = IIf(dtTablecust.Rows(i).Item("vcDBColumnName") = "0" Or dtTablecust.Rows(i).Item("vcDBColumnName") = Nothing, "-", dtTablecust.Rows(i).Item("vcDBColumnName"))
                        ElseIf IsDBNull(dtTablecust.Rows(i).Item("vcDBColumnName")) Then
                            objCell.InnerText = "-"
                        End If

                    End If


                    objRow.Cells.Add(objCell)

                    'If dttable.Rows(i).Item("fld_type") = "Text Box" Then
                    '    objCell = New HtmlTableCell
                    '    CreateTexBox(objRow, objCell, dttable.Rows(i).Item("fld_id"), dttable.Rows(i).Item("Value"))
                    'ElseIf dttable.Rows(i).Item("fld_type") = "Drop Down List Box" Then
                    '    objCell = New HtmlTableCell
                    '    CreateDropdown(objRow, objCell, dttable.Rows(i).Item("fld_id"), CInt(dttable.Rows(i).Item("Value")), dttable.Rows(i).Item("numlistid"))
                    'ElseIf dttable.Rows(i).Item("fld_type") = "Check box" Then
                    '    objCell = New HtmlTableCell
                    '    CreateChkBox(objRow, objCell, dttable.Rows(i).Item("fld_id"), CInt(dttable.Rows(i).Item("Value")))
                    'ElseIf dttable.Rows(i).Item("fld_type") = "Text Area" Then
                    '    objCell = New HtmlTableCell
                    '    CreateTextArea(objRow, objCell, dttable.Rows(i).Item("fld_id"), dttable.Rows(i).Item("Value"))
                    'End If
                    k = k + 1
                End If

            Next
            If ViewState("Check") = 1 Then
                aspTable.Rows.Add(objRow)
                tblcell.Controls.Add(aspTable)
                tblRow.Cells.Add(tblcell)
                Table.Rows.Add(tblRow)
                Tab.ContentPane.Controls.Add(Table)
            End If
        End If

        If (dtContactInfo.Rows.Count > 0) Then
            Dim column1 As New TableCell
            Dim column2 As New TableCell
            Dim r As New TableRow
            column1.CssClass = "normal7"
            column2.CssClass = "normal1"
            column1.HorizontalAlign = HorizontalAlign.Right
            column2.HorizontalAlign = HorizontalAlign.Justify
            column2.ColumnSpan = 5
            Dim l As New Label
            l.CssClass = "normal7"
            l.Text = "Comments" & "&nbsp;:"
            column1.Controls.Add(l)
            column2.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("txtNotes")), "-", dtContactInfo.Rows(0).Item("txtNotes"))
            column1.Width = 150
            r.Cells.Add(column1)
            r.Cells.Add(column2)
            tableComment.Rows.Add(r)
        End If
    End Sub


    Sub LoadInformation()
        Dim dtContactInfo As DataTable
        Dim objContacts As New CContacts
        objContacts.ContactID = intCntId
        objContacts.DomainID = Session("DomainID")
        dtContactInfo = objContacts.GetCntInfoForEdit

        'lblAssoCountF.Text = dtContactInfo.Rows(0).Item("AssociateCountFrom")
        'lblAssoCountT.Text = dtContactInfo.Rows(0).Item("AssociateCountTo")

        lblRecordOwner.Text = dtContactInfo.Rows(0).Item("RecordOwner")
        lblCreatedBy.Text = dtContactInfo.Rows(0).Item("CreatedBy")
        lblModifiedBy.Text = dtContactInfo.Rows(0).Item("ModifiedBy")

        'txtFirstname.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcFirstName")), "", dtContactInfo.Rows(0).Item("vcFirstName"))  'display First Name
        'txtLastName.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcLastName")), "", dtContactInfo.Rows(0).Item("vcLastName")) ' Display Last Name
        'txtPhone.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("numPhone")), "", dtContactInfo.Rows(0).Item("numPhone"))  'display Phone
        'txtEmail.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcEmail")), "", dtContactInfo.Rows(0).Item("vcEmail"))   'Display Email
        'If Not IsDBNull(dtContactInfo.Rows(0).Item("vcCategory")) Then
        '    If Not ddlcategory.Items.FindByValue(dtContactInfo.Rows(0).Item("vcCategory")) Is Nothing Then
        '        ddlcategory.Items.FindByValue(dtContactInfo.Rows(0).Item("vcCategory")).Selected = True
        '    End If
        'End If
        'If Not IsDBNull(dtContactInfo.Rows(0).Item("vcPosition")) Then
        '    If Not ddlPosition.Items.FindByValue(dtContactInfo.Rows(0).Item("vcPosition")) Is Nothing Then
        '        ddlPosition.Items.FindByValue(dtContactInfo.Rows(0).Item("vcPosition")).Selected = True
        '    End If
        'End If
        'If Not IsDBNull(dtContactInfo.Rows(0).Item("bintDOB")) Then
        '    txtAg.Text = Date.Today.Year - Year(dtContactInfo.Rows(0).Item("bintDOB"))
        '    txtDOB.Text = FormattedDateFromDate(dtContactInfo.Rows(0).Item("bintDOB"), Session("DateFormat"))
        'End If

        'If IsDBNull(dtContactInfo.Rows(0).Item("txtNotes")) Then
        '    txtComments.Text = ""   'Dispalay Comments
        'Else
        '    txtComments.Text = Server.HtmlDecode(dtContactInfo.Rows(0).Item("txtNotes"))   'Dispalay Comments
        'End If

        'txtExtension.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("numPhoneExtension")), "", dtContactInfo.Rows(0).Item("numPhoneExtension")) 'Display Extension
        'txtMobile.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("numCell")), "", dtContactInfo.Rows(0).Item("numCell"))
        'txtHome.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("NumHomePhone")), "", dtContactInfo.Rows(0).Item("NumHomePhone"))
        'txtFax.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcFax")), "", dtContactInfo.Rows(0).Item("vcFax"))
        'If IsDBNull(dtContactInfo.Rows(0).Item("bitOptOut")) Then
        '    optout.Checked = False
        'Else
        '    If dtContactInfo.Rows(0).Item("bitOptOut") = 0 Then
        '        optout.Checked = False
        '    Else
        '        optout.Checked = True
        '    End If
        'End If
        hidCompName.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcCompanyName")), "", dtContactInfo.Rows(0).Item("vcCompanyName"))
        hidEml.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcGivenName")), "", dtContactInfo.Rows(0).Item("vcGivenName"))
        'txtAsstFName.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcAsstFirstName")), "", dtContactInfo.Rows(0).Item("vcAsstFirstName"))
        'txtAsstLName.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcAsstLastName")), "", dtContactInfo.Rows(0).Item("vcAsstLastName"))
        'txtAsstPhone.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("numAsstPhone")), "", dtContactInfo.Rows(0).Item("numAsstPhone"))
        'txtAsstExt.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("numAsstExtn")), "", dtContactInfo.Rows(0).Item("numAsstExtn"))
        'txtAsstEmail.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcAsstEmail")), "", dtContactInfo.Rows(0).Item("vcAsstEmail"))
        'lblStreet.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcpStreet")), "", dtContactInfo.Rows(0).Item("vcpStreet") & ", ")
        'lblCity.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcPCity")), "", dtContactInfo.Rows(0).Item("vcpCity") & ", ")
        'lblPostal.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcPPostalCode")), "", dtContactInfo.Rows(0).Item("vcPPostalCode") & ", ")
        'lblState.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("State")), "", dtContactInfo.Rows(0).Item("State") & ", ")
        'lblCountry.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("Country")), "", dtContactInfo.Rows(0).Item("Country"))
        'txtTitle.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcTitle")) = False, dtContactInfo.Rows(0).Item("vcTitle"), "")
        'txtAltEmail.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcAltEmail")) = False, dtContactInfo.Rows(0).Item("vcAltEmail"), "")



        Dim strcharSex As String   'hold the sex of the contact
        strcharSex = IIf(IsDBNull(dtContactInfo.Rows(0).Item("charSex")), "M", dtContactInfo.Rows(0).Item("charSex"))

        If Trim(strcharSex) = "0" Or Trim(strcharSex) = "" Then
            strcharSex = "M"
        End If

        'ddlSex.Items.FindByValue(strcharSex).Selected = True

        'If Not ddlType.Items.FindByValue(dtContactInfo.Rows(0).Item("numContactType")) Is Nothing Then
        '    ddlType.Items.FindByValue(dtContactInfo.Rows(0).Item("numContactType")).Selected = True 'Show the contact type as selected
        '    txtContactType.Text = dtContactInfo.Rows(0).Item("numContactType")
        'Else
        '    txtContactType.Text = 0
        'End If
        'If Not IsDBNull(dtContactInfo.Rows(0).Item("numTeam")) Then
        '    If Not ddlTeam.Items.FindByValue(dtContactInfo.Rows(0).Item("numTeam")) Is Nothing Then
        '        ddlTeam.Items.FindByValue(dtContactInfo.Rows(0).Item("numTeam")).Selected = True 'Show the contact type as selected
        '    End If
        'End If

        'If Not IsDBNull(dtContactInfo.Rows(0).Item("vcDepartment")) Then
        '    If Not ddlDepartment.Items.FindByValue(dtContactInfo.Rows(0).Item("vcDepartment")) Is Nothing Then
        '        ddlDepartment.Items.FindByValue(dtContactInfo.Rows(0).Item("vcDepartment")).Selected = True 'Show the contact type as selected
        '    End If
        'End If


        'If Not IsDBNull(dtContactInfo.Rows(0).Item("numManagerID")) Then
        '    If Not ddlManagers.Items.FindByValue(dtContactInfo.Rows(0).Item("numManagerID")) Is Nothing Then
        '        ddlManagers.Items.FindByValue(dtContactInfo.Rows(0).Item("numManagerID")).Selected = True
        '    End If
        'End If
        ''Added by Debasish Nag on 23rd Jan 2006 to incorporate Emp Status of the Contact
        'If Not IsDBNull(dtContactInfo.Rows(0).Item("numEmpStatus")) Then
        '    If Not ddlEmpStatus.Items.FindByValue(dtContactInfo.Rows(0).Item("numEmpStatus")) Is Nothing Then
        '        ddlEmpStatus.Items.FindByValue(dtContactInfo.Rows(0).Item("numEmpStatus")).Selected = True 'Show the contact type as selected
        '    End If
        'End If
    End Sub

    'Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    '    txtContactType.Text = ddlType.SelectedItem.Value
    '    SaveContactInfo()
    '    SaveCusField()
    '    LoadInformation()
    'End Sub

    'Sub SaveContactInfo()
    '    Dim objContacts As New CContacts
    '    With objContacts
    '        .ContactID = intCntId
    '        .Department = ddlDepartment.SelectedItem.Value
    '        .FirstName = txtFirstname.Text
    '        .LastName = txtLastName.Text
    '        .Email = txtEmail.Text
    '        .Sex = ddlSex.SelectedItem.Value
    '        .Position = ddlPosition.SelectedItem.Value
    '        .Phone = txtPhone.Text
    '        .PhoneExt = txtExtension.Text

    '        If (txtDOB.Text <> "") Then
    '            .DOB = DateFromFormattedDate(txtDOB.Text, Session("DateFormat"))

    '        End If
    '        .Comments = txtComments.Text
    '        .Category = ddlcategory.SelectedItem.Value
    '        .CellPhone = txtMobile.Text
    '        .HomePhone = txtHome.Text
    '        .Fax = txtFax.Text
    '        .AssFirstName = txtAsstFName.Text
    '        .AssLastName = txtAsstLName.Text
    '        .AssPhone = txtAsstPhone.Text
    '        .AssPhoneExt = txtAsstExt.Text
    '        .AssEmail = txtAsstEmail.Text
    '        .ContactType = ddlType.SelectedItem.Value
    '        .OptOut = IIf(optout.Checked = True, 1, 0)
    '        .ManagerID = ddlManagers.SelectedItem.Value
    '        .Team = ddlTeam.SelectedItem.Value
    '        .UserCntID = Session("ConID")
    '        .EmpStatus = ddlEmpStatus.SelectedItem.Value
    '        .AltEmail = txtAltEmail.Text.Trim
    '        .Title = txtTitle.Text.Trim
    '    End With
    '    objContacts.ManageContactInfo()
    'End Sub

    'Private Sub btnSaveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
    '    SaveContactInfo()
    '    SaveCusField()
    '    Response.Redirect("../Contacts/frmContactList.aspx")
    'End Sub




    'Sub LoadDropDowns()
    '    Dim objCommon As New CCommon
    '    objCommon.sb_FillManagerFromDBSel(ddlManagers, Session("DomainID"), intCntId, intDivId)
    '    objCommon.sb_FillComboFromDBwithSel(ddlPosition, 41, Session("DomainID"))
    '    objCommon.sb_FillComboFromDBwithSel(ddlType, 8, Session("DomainID"))
    '    objCommon.sb_FillComboFromDBwithSel(ddlTeam, 35, Session("DomainID"))
    '    objCommon.sb_FillComboFromDBwithSel(ddlDepartment, 19, Session("DomainID"))
    '    objCommon.sb_FillComboFromDBwithSel(ddlcategory, 25, Session("DomainID"))
    '    objCommon.sb_FillComboFromDBwithSel(ddlEmpStatus, 44, Session("DomainID"))  'Fill the Contacts Status drop down
    'End Sub

    Sub DisplayDynamicFlds()
        'm_aryRightsForCusFlds = clsAuthorization.fn_GetPageListUserRights("frmContacts.aspx", Session("ConID"), 11, 9)
        'If m_aryRightsForCusFlds(RIGHTSTYPE.VIEW) = 0 Then
        'Exit Sub
        'End If
        'Dim objRow As HtmlTableRow
        'Dim objCell As HtmlTableCell
        'Dim i, k As Integer
        'Dim dttable As New DataTable
        ''Tabstrip4.Items.Clear()

        'Dim ObjCus As New CustomFields
        'ObjCus.locId = 4
        'ObjCus.RelId = CInt(Type.Text)
        'ObjCus.RecordId = intCntId
        'ObjCus.DomainID = Session("DomainID")
        'dttable = ObjCus.GetCustFlds
        'Session("CusFields") = dttable




        'If dttable.Rows.Count > 0 Then

        'Main Detail Section
        'k = 0
        'objRow = New HtmlTableRow
        'For i = 0 To dttable.Rows.Count - 1
        '    If dttable.Rows(i).Item("TabId") = 0 Then
        '        If k = 3 Then
        '            k = 0
        '            tblMain.Rows.Add(objRow)
        '            objRow = New HtmlTableRow
        '        End If


        '        objCell = New HtmlTableCell
        '        objCell.Align = "Right"
        '        objCell.Attributes.Add("class", "normal1")
        '        objCell.InnerText = dttable.Rows(i).Item("fld_label")
        '        objRow.Cells.Add(objCell)
        '        If dttable.Rows(i).Item("fld_type") = "Text Box" Then
        '            objCell = New HtmlTableCell
        '            CreateTexBox(objRow, objCell, dttable.Rows(i).Item("fld_id"), dttable.Rows(i).Item("Value"))
        '        ElseIf dttable.Rows(i).Item("fld_type") = "Drop Down List Box" Then
        '            objCell = New HtmlTableCell
        '            CreateDropdown(objRow, objCell, dttable.Rows(i).Item("fld_id"), CInt(dttable.Rows(i).Item("Value")), dttable.Rows(i).Item("numlistid"))
        '        ElseIf dttable.Rows(i).Item("fld_type") = "Check box" Then
        '            objCell = New HtmlTableCell
        '            CreateChkBox(objRow, objCell, dttable.Rows(i).Item("fld_id"), CInt(dttable.Rows(i).Item("Value")))
        '        ElseIf dttable.Rows(i).Item("fld_type") = "Text Area" Then
        '            objCell = New HtmlTableCell
        '            CreateTextArea(objRow, objCell, dttable.Rows(i).Item("fld_id"), dttable.Rows(i).Item("Value"))
        '        End If
        '        k = k + 1
        '    End If
        'Next
        'tblMain.Rows.Add(objRow)


        'CustomField Section

        '    k = 0
        '    objRow = New HtmlTableRow
        '    ViewState("TabId") = dttable.Rows(0).Item("TabId")
        '    ViewState("Check") = 0
        '    ViewState("FirstTabCreated") = 0
        '    For i = 0 To dttable.Rows.Count - 1
        '        If dttable.Rows(i).Item("TabId") <> 0 Then
        '            If ViewState("TabId") <> dttable.Rows(i).Item("TabId") Or ViewState("FirstTabCreated") = 0 Then
        '                If ViewState("Check") <> 0 Then
        '                    tabledetail.Rows.Add(objRow)
        '                End If
        '                k = 0
        '                ViewState("Check") = 1
        '                ViewState("FirstTabCreated") = 1
        '                ViewState("TabId") = dttable.Rows(i).Item("TabId")
        '                objRow = New HtmlTableRow
        '                objCell = New HtmlTableCell
        '                objCell.Attributes.Add("class", "text_bold")
        '                objCell.InnerHtml = "<br> " & "&nbsp;&nbsp;" & dttable.Rows(i).Item("tabname") & "&nbsp;&nbsp;"
        '                objRow.Cells.Add(objCell)
        '                tabledetail.Rows.Add(objRow)
        '                objRow = New HtmlTableRow
        '            End If

        '            If k = 3 Then
        '                k = 0
        '                tabledetail.Rows.Add(objRow)
        '                objRow = New HtmlTableRow
        '            End If
        '            objCell = New HtmlTableCell
        '            objCell.Align = "right"
        '            objCell.Attributes.Add("class", "normal1")
        '            objCell.InnerText = dttable.Rows(i).Item("fld_label")
        '            objRow.Cells.Add(objCell)
        '            If dttable.Rows(i).Item("fld_type") = "Text Box" Then
        '                objCell = New HtmlTableCell
        '                CreateTexBox(objRow, objCell, dttable.Rows(i).Item("fld_id"), dttable.Rows(i).Item("Value"))
        '            ElseIf dttable.Rows(i).Item("fld_type") = "Drop Down List Box" Then
        '                objCell = New HtmlTableCell
        '                CreateDropdown(objRow, objCell, dttable.Rows(i).Item("fld_id"), CInt(dttable.Rows(i).Item("Value")), dttable.Rows(i).Item("numlistid"))
        '            ElseIf dttable.Rows(i).Item("fld_type") = "Check box" Then
        '                objCell = New HtmlTableCell
        '                CreateChkBox(objRow, objCell, dttable.Rows(i).Item("fld_id"), CInt(dttable.Rows(i).Item("Value")))
        '            ElseIf dttable.Rows(i).Item("fld_type") = "Text Area" Then
        '                objCell = New HtmlTableCell
        '                CreateTextArea(objRow, objCell, dttable.Rows(i).Item("fld_id"), dttable.Rows(i).Item("Value"))
        '            End If
        '            k = k + 1
        '        End If

        '    Next
        '    If ViewState("Check") = 1 Then
        '        tabledetail.Rows.Add(objRow)
        '    End If

        'End If
    End Sub

    'Sub SaveCusField()
    '    Try
    '        Dim dttable As New DataTable
    '        Dim i As Integer
    '        If Not Session("CusFields") Is Nothing Then

    '            dttable = Session("CusFields")
    '            For i = 0 To dttable.Rows.Count - 1
    '                If dttable.Rows(i).Item("fld_type") = "Text Box" Then
    '                    Dim txt As TextBox
    '                    txt = Page.FindControl(dttable.Rows(i).Item("fld_id"))
    '                    dttable.Rows(i).Item("Value") = txt.Text
    '                ElseIf dttable.Rows(i).Item("fld_type") = "Drop Down List Box" Then
    '                    Dim ddl As DropDownList
    '                    ddl = Page.FindControl(dttable.Rows(i).Item("fld_id"))
    '                    dttable.Rows(i).Item("Value") = CStr(ddl.SelectedItem.Value)
    '                ElseIf dttable.Rows(i).Item("fld_type") = "Check box" Then
    '                    Dim chk As CheckBox
    '                    chk = Page.FindControl(dttable.Rows(i).Item("fld_id"))
    '                    If chk.Checked = True Then
    '                        dttable.Rows(i).Item("Value") = "1"
    '                    Else
    '                        dttable.Rows(i).Item("Value") = "0"
    '                    End If
    '                ElseIf dttable.Rows(i).Item("fld_type") = "Text Area" Then
    '                    Dim txt As TextBox
    '                    txt = Page.FindControl(dttable.Rows(i).Item("fld_id"))
    '                    dttable.Rows(i).Item("Value") = txt.Text
    '                End If
    '            Next

    '            Dim ds As New DataSet
    '            Dim strdetails As String
    '            dttable.TableName = "table"
    '            ds.Tables.Add(dttable.Copy)
    '            strdetails = ds.GetXml
    '            ds.Tables.Remove(ds.Tables(0))

    '            Dim ObjCusfld As New CustomFields
    '            ObjCusfld.strDetails = strdetails
    '            ObjCusfld.locId = 4
    '            ObjCusfld.RecordId = intCntId
    '            ObjCusfld.SaveCustomFldsByRecId()

    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Sub SaveAOI()
    '    Try
    '        Dim dttable As New DataTable
    '        Dim i As Integer
    '        If Not Session("AOIForContact") Is Nothing Then

    '            dttable = Session("AOIForContact")
    '            For i = 0 To dttable.Rows.Count - 1
    '                Dim chk As CheckBox
    '                chk = Page.FindControl("chkAOI" & dttable.Rows(i).Item("numAOIId"))
    '                If chk.Checked = True Then
    '                    dttable.Rows(i).Item("Status") = 1
    '                Else
    '                    dttable.Rows(i).Item("Status") = 0
    '                End If
    '            Next
    '            Dim ds As New DataSet
    '            Dim strdetails As String
    '            dttable.TableName = "table"
    '            ds.Tables.Add(dttable.Copy)
    '            strdetails = ds.GetXml
    '            ds.Tables.Remove(ds.Tables(0))

    '            Dim ObjContacts As New CContacts
    '            ObjContacts.strAOI = strdetails
    '            ObjContacts.ContactID = intCntId
    '            ObjContacts.SaveAOI()
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        ''Response.Redirect("../Contacts/frmContactList.aspx")
        PageRedirect()
    End Sub
    Private Sub PageRedirect()
        If GetQueryStringVal(Request.QueryString("enc"), "frm") = "OppList" Then
            ''If Not IsNothing(GetQueryStringVal(Request.QueryString("enc"),"frm1")) And GetQueryStringVal(Request.QueryString("enc"),"frm1") <> "" Then
            ''    Response.Redirect("../Opportunities/frmOpportunities.aspx")
            ''Else
            Response.Redirect("../Opportunities/frmOpportunityList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
        ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "CaseList" Then
            Response.Redirect("../Cases/frmCaseList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
        ElseIf GetQueryStringVal(Request.QueryString("enc"), "frm") = "ProjectList" Then
            Response.Redirect("../Projects/frmProjectList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
        Else

            Response.Redirect("../Contacts/frmContactList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
        End If
    End Sub
   

    Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Response.Redirect("../Contacts/frmContacts.aspx?frm=Contactdtl" & "&SI=" & uwOppTab.SelectedTabIndex & "&SI1=" & uwOppTab.SelectedTabIndex & "&SI2=" & SI1 & "&frm1=" & frm & "&frm2=" & frm1)
    End Sub
End Class