Imports BACRM.BusinessLogic.Common
Partial Public Class frmLogout
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            btnLogin.Attributes.Add("onclick", "return Login()")
            Session.Abandon()
            If GetQueryStringVal( "Redirect") = 1 Then
                Table2.Visible = False
                Response.Redirect("../login.aspx")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class