﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class frmCaseList

    '''<summary>
    '''ddlSort control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlSort As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''lblRecordCountCases control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRecordCountCases As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''ddlStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlStatus As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''btnGoCases control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnGoCases As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''bizPager control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents bizPager As Global.Wuqi.Webdiyer.AspNetPager

    '''<summary>
    '''frmBizSorting2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents frmBizSorting2 As Global.BACRMPortal.frmBizSorting

    '''<summary>
    '''table2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents table2 As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''gvSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents gvSearch As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''txtSortColumn control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSortColumn As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtSortOrder control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSortOrder As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtTotalPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTotalPage As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtTotalRecords control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTotalRecords As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtSortChar control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSortChar As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtGridColumnFilter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtGridColumnFilter As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''btnGo1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnGo1 As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''txtCurrrentPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCurrrentPage As Global.System.Web.UI.WebControls.TextBox
End Class
