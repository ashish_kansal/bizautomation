﻿Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Common
Imports System.Configuration

Public Class frmProjectList
    Inherits BACRMPage

    Dim strColumn As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim objCommon As New CCommon
            If Session("UserContactID") = Nothing Then Response.Redirect("../CommonPages/frmLogout.aspx")

            If Not IsPostBack Then
                Session("Asc") = 1
                objCommon.sb_FillComboFromDBwithSel(ddlProjectStatus, 439, Session("DomainID"))
                BindDatagrid()
            End If

            'If txtSortChar.Text <> "" Then
            '    ViewState("SortChar") = txtSortChar.Text
            '    ViewState("Column") = "Name"
            '    Session("Asc") = 0
            '    BindDatagrid()
            '    txtSortChar.Text = ""
            'End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindDatagrid(Optional ByVal CreateCol As Boolean = True)
        Try
            Dim dtProjects As DataTable
            Dim objProjects As New Project

            With objProjects
                .DomainID = Session("DomainID")
                .UserCntID = 0 'Session("UserContactID")
                .SortCharacter = txtSortChar.Text.Trim()
                .DivisionID = Session("DivId")
                .UserRightType = m_aryRightsForPage(RIGHTSTYPE.VIEW)
                .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

                If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                If txtCurrrentPage.Text.Trim <> "" Then
                    .CurrentPage = txtCurrrentPage.Text
                Else : .CurrentPage = 1
                End If
                .PageSize = Session("PagingRows")
                .TotalRecords = 0
                If ViewState("Column") <> "" Then
                    .columnName = ViewState("Column")
                Else : .columnName = "pro.bintcreateddate"
                End If

                If txtSortOrder.Text = "D" Then
                    .columnSortOrder = "Desc"
                Else : .columnSortOrder = "Asc"
                End If
                .sMode = 0
                .SortOrder = ddlSort.SelectedItem.Value
                .ProjectStatus = CCommon.ToLong(ddlProjectStatus.SelectedValue)
            End With

            Dim dsList As DataSet
            dsList = objProjects.GetProjectListPortal
            dtProjects = dsList.Tables(0)

            Dim dtTableInfo As DataTable
            dtTableInfo = dsList.Tables(1)
            Dim i As Integer

            For i = 0 To dtProjects.Columns.Count - 1
                dtProjects.Columns(i).ColumnName = dtProjects.Columns(i).ColumnName.Replace(".", "")
            Next

            If CreateCol = True Then
                Dim bField As BoundField
                Dim Tfield As TemplateField
                gvSearch.Columns.Clear()

                For Each drRow As DataRow In dtTableInfo.Rows
                    Tfield = New TemplateField
                    Tfield.HeaderTemplate = New GridTemplatePortal(ListItemType.Header, drRow, 0, Nothing, 82, objProjects.columnName, objProjects.columnSortOrder)
                    Tfield.ItemTemplate = New GridTemplatePortal(ListItemType.Item, drRow, 0, Nothing, 82, objProjects.columnName, objProjects.columnSortOrder)
                    gvSearch.Columns.Add(Tfield)
                Next
            End If

            bizPager.PageSize = Session("PagingRows")
            bizPager.RecordCount = objProjects.TotalRecords
            bizPager.CurrentPageIndex = txtCurrrentPage.Text
            lblRecordCount.Text = bizPager.RecordCount

            gvSearch.DataSource = dtProjects
            gvSearch.DataBind()

            'dtProjects = objProjects.GetProjectListPortal
            'If objProjects.TotalRecords = 0 Then
            '    lblRecordCount.Text = 0
            'Else
            '    lblRecordCount.Text = String.Format("{0:#,###}", objProjects.TotalRecords)
            '    Dim strTotalPage As String()
            '    Dim decTotalPage As Decimal
            '    decTotalPage = lblRecordCount.Text / Session("PagingRows")
            '    decTotalPage = Math.Round(decTotalPage, 2)
            '    strTotalPage = CStr(decTotalPage).Split(".")
            '    If (lblRecordCount.Text Mod Session("PagingRows")) = 0 Then
            '        txtTotalPage.Text = strTotalPage(0)
            '    Else
            '        txtTotalPage.Text = strTotalPage(0) + 1
            '    End If
            '    txtTotalRecords.Text = lblRecordCount.Text
            'End If
            'gvSearch.DataSource = dtProjects
            'gvSearch.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Private Sub dgProjects_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgProjects.SortCommand
    '    Try
    '        strColumn = e.SortExpression.ToString()
    '        If ViewState("Column") <> strColumn Then
    '            ViewState("Column") = strColumn
    '            Session("Asc") = 0
    '        Else
    '            If Session("Asc") = 0 Then
    '                Session("Asc") = 1
    '            Else : Session("Asc") = 0
    '            End If
    '        End If
    '        BindDatagrid()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlProjectStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProjectStatus.SelectedIndexChanged
        Try
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlSort_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlSort.SelectedIndexChanged
        Try
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class
