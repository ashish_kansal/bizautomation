﻿Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Common

Public Class frmCaseList
    Inherits BACRMPage

    Dim strColumn As String
    Dim RegularSearch As String
    Dim CustomSearch As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("UserContactID") = Nothing Then
                Response.Redirect("../CommonPages/frmLogout.aspx")
            End If

            Dim objCommon As New CCommon

            If Not IsPostBack Then
                objCommon.sb_FillComboFromDB(ddlStatus, 14, Session("DomainID"))
                Dim lstNewItem As New ListItem
                lstNewItem.Text = "-- Select One --"
                lstNewItem.Value = 0
                ddlStatus.Items.Insert(0, lstNewItem)

                Session("Asc") = 1
                txtCurrrentPage.Text = 1
                BindDatagrid()
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindDatagrid(Optional ByVal CreateCol As Boolean = True)
        Try
            Dim dtCases As DataTable
            Dim objCases As New CCases

            With objCases
                .UserCntID = 0 'Session("UserContactID")
                .DomainID = Session("DomainID")
                .DivisionID = Session("DivId")
                .UserRightType = m_aryRightsForPage(RIGHTSTYPE.VIEW)
                .SortOrder = ddlSort.SelectedItem.Value
                .FirstName = ""
                .LastName = ""
                .SortCharacter = txtSortChar.Text.Trim()
                .CustName = ""

                If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                .CurrentPage = txtCurrrentPage.Text.Trim()

                .PageSize = Session("PagingRows")
                .TotalRecords = 0

                If txtSortColumn.Text <> "" Then
                    .columnName = txtSortColumn.Text
                Else : .columnName = "cs.bintcreateddate"
                End If

                If txtSortOrder.Text = "D" Then
                    .columnSortOrder = "Desc"
                Else : .columnSortOrder = "Asc"
                End If

                GridColumnSearchCriteria()

                .RegularSearchCriteria = RegularSearch
                .CustomSearchCriteria = CustomSearch
                .sMode = 0
            End With

            objCases.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            objCases.Status = CCommon.ToLong(ddlStatus.SelectedValue)

            Dim dsList As DataSet

            dsList = objCases.GetCaseListForPortal
            dtCases = dsList.Tables(0)

            Dim dtTableInfo As DataTable
            dtTableInfo = dsList.Tables(1)

            bizPager.PageSize = Session("PagingRows")
            bizPager.RecordCount = objCases.TotalRecords
            bizPager.CurrentPageIndex = txtCurrrentPage.Text

            If objCases.TotalRecords = 0 Then
                lblRecordCountCases.Text = 0
            Else
                lblRecordCountCases.Text = String.Format("{0:#,###}", objCases.TotalRecords)
                Dim strTotalPage As String()
                Dim decTotalPage As Decimal
                decTotalPage = lblRecordCountCases.Text / Session("PagingRows")
                decTotalPage = Math.Round(decTotalPage, 2)
                strTotalPage = CStr(decTotalPage).Split(".")
                If (lblRecordCountCases.Text Mod Session("PagingRows")) = 0 Then
                    txtTotalPage.Text = strTotalPage(0)
                Else
                    txtTotalPage.Text = strTotalPage(0) + 1
                End If
                txtTotalRecords.Text = lblRecordCountCases.Text
            End If

            'Dim m_aryRightsForInlineEdit() As Integer = GetUserRightsForPage_Other(7, 3)

            Dim i As Integer

            For i = 0 To dtCases.Columns.Count - 1
                dtCases.Columns(i).ColumnName = dtCases.Columns(i).ColumnName.Replace(".", "")
            Next

            Dim htGridColumnSearch As New Hashtable

            If txtGridColumnFilter.Text.Trim.Length > 0 Then
                Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")
                Dim strIDValue() As String

                For i = 0 To strValues.Length - 1
                    strIDValue = strValues(i).Split(":")

                    htGridColumnSearch.Add(strIDValue(0), strIDValue(1))
                Next
            End If

            If CreateCol = True Then
                Dim bField As BoundField
                Dim Tfield As TemplateField
                gvSearch.Columns.Clear()

                For Each drRow As DataRow In dtTableInfo.Rows
                    Tfield = New TemplateField
                    Tfield.HeaderTemplate = New GridTemplatePortal(ListItemType.Header, drRow, 1, htGridColumnSearch, 80, objCases.columnName, objCases.columnSortOrder)
                    Tfield.ItemTemplate = New GridTemplatePortal(ListItemType.Item, drRow, 1, htGridColumnSearch, 80, objCases.columnName, objCases.columnSortOrder)
                    gvSearch.Columns.Add(Tfield)
                Next

                'Dim dr As DataRow
                'dr = dtTableInfo.NewRow()
                'dr("vcAssociatedControlType") = "DeleteCheckBox"
                'dr("intColumnWidth") = "30"

                'Tfield = New TemplateField
                'Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, dr, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, 12)
                'Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, dr, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, 12)
                'gvSearch.Columns.Add(Tfield)
            End If

            gvSearch.DataSource = dtCases
            gvSearch.DataBind()

            Dim objPageControls As New PageControls
            Dim strValidation As String = objPageControls.GenerateInlineEditValidationScript(dtTableInfo)
            ClientScript.RegisterClientScriptBlock(Me.GetType, "InlineEditValidation", strValidation, True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function ReturnName(ByVal CreatedDate) As String
        Try
            Dim strCreateDate As String
            If IsDBNull(CreatedDate) = False Then
                strCreateDate = FormattedDateFromDate(CreatedDate, Session("DateFormat"))
                If Format(CreatedDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                    strCreateDate = "<font color=red>Today</font>"
                ElseIf Format(CreatedDate, "yyyyMMdd") < Format(Now(), "yyyyMMddhhmmss") Then
                    strCreateDate = "<font color=red>" & strCreateDate & "</font>"
                End If
            Else : strCreateDate = "-"
            End If
            Return strCreateDate
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function ReturnDateTime(ByVal CloseDate) As String
        Try
            Dim strTargetResolveDate As String = ""
            Dim temp As String = ""
            If Not IsDBNull(CloseDate) Then
                strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))

                Dim timePart As String = CloseDate.ToShortTimeString.Substring(0, CloseDate.ToShortTimeString.Length - 1)
                ' remove gaps
                If timePart.Split(" ").Length >= 2 Then timePart = timePart.Split(" ").GetValue(0) + timePart.Split(" ").GetValue(1)

                ' check Today date components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                ' if both are same it is today
                If (CloseDate.Date = Now.Date And CloseDate.Month = Now.Month And CloseDate.Year = Now.Year) Then
                    strTargetResolveDate = "<font color=red><b>Today</b></font>"
                    Return strTargetResolveDate

                    ' check [ TodayDate + 1 ] Date.... components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                    ' if both are same it was Yesterday
                ElseIf (CloseDate.Date.AddDays(1).Date = Now.Date And CloseDate.AddDays(1).Month = Now.Month And CloseDate.AddDays(1).Year = Now.Year) Then
                    strTargetResolveDate = "<font color=purple><b>Yesterday</b></font>"
                    Return strTargetResolveDate

                    ' check TodayDate .... components [ Date , Month , Year ] with Parameter [ CloseDate + 1 ] Date....  components [ Date , Month , Year ] 
                    ' if both are same it will Tomorrow
                ElseIf (CloseDate.Date = Now.AddDays(1).Date And CloseDate.Month = Now.AddDays(1).Month And CloseDate.Year = Now.AddDays(1).Year) Then
                    temp = CloseDate.Hour.ToString + ":" + CloseDate.Minute.ToString
                    strTargetResolveDate = "<font color=orange><b>Tomorrow</b></font>"
                    Return strTargetResolveDate

                    ' display day name for next 4 days from now
                ElseIf Format(CloseDate, "yyyyMMdd") = Format(DateTime.Now.AddDays(2), "yyyyMMdd") Or Format(CloseDate, "yyyyMMdd") = Format(DateTime.Now.AddDays(3), "yyyyMMdd") Or Format(CloseDate, "yyyyMMdd") = Format(DateTime.Now.AddDays(4), "yyyyMMdd") Or Format(CloseDate, "yyyyMMdd") = Format(DateTime.Now.AddDays(5), "yyyyMMdd") Then
                    strTargetResolveDate = "<b>" & CloseDate.DayOfWeek.ToString & "</b>"
                    Return strTargetResolveDate
                Else
                    strTargetResolveDate = strTargetResolveDate
                    Return strTargetResolveDate
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
        Try
            BindDatagrid(True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub GridColumnSearchCriteria()
        Try
            If txtGridColumnFilter.Text.Trim.Length > 0 Then
                Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")

                Dim strIDValue(), strID(), strCustom As String
                Dim strRegularCondition As New ArrayList
                Dim strCustomCondition As New ArrayList


                For i As Integer = 0 To strValues.Length - 1
                    strIDValue = strValues(i).Split(":")
                    strID = strIDValue(0).Split("~")

                    If strID(0).Contains("CFW.Cust") Then

                        Select Case strID(3).Trim()
                            Case "TextBox"
                                strCustom = " ilike '%" & strIDValue(1).Replace("'", "''") & "%'"
                            Case "SelectBox"
                                strCustom = "=" & strIDValue(1)
                        End Select

                        strCustomCondition.Add("CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & strCustom)
                    Else
                        Select Case strID(3).Trim()
                            Case "Website", "Email", "TextBox"
                                strRegularCondition.Add(strID(0) & " ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                            Case "SelectBox"
                                strRegularCondition.Add(strID(0) & "=" & strIDValue(1))
                            Case "TextArea"
                                strRegularCondition.Add(" Cast(" & strID(0) & " as varchar(5000)) ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                            Case "DateField"
                                If strID(4) = "From" Then
                                    Dim fromDate As Date
                                    If Date.TryParse(strIDValue(1), fromDate) Then
                                        strRegularCondition.Add(strID(0) & " >= '" & fromDate.ToString("MM/dd/yyyy") & "'")
                                    End If
                                ElseIf strID(4) = "To" Then
                                    Dim toDate As Date
                                    If Date.TryParse(strIDValue(1), toDate) Then
                                        strRegularCondition.Add(strID(0) & " <= '" & toDate.ToString("MM/dd/yyyy") & "'")
                                    End If
                                End If
                        End Select
                    End If
                Next
                RegularSearch = String.Join(" and ", strRegularCondition.ToArray())
                CustomSearch = String.Join(" and ", strCustomCondition.ToArray())
            Else
                RegularSearch = ""
                CustomSearch = ""
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnGoCases_Click(sender As Object, e As EventArgs) Handles btnGoCases.Click
        Try
            BindDatagrid(True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlSort_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSort.SelectedIndexChanged
        Try
            txtCurrrentPage.Text = 1
            BindDatagrid(True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class

