﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCaseList.aspx.vb" MasterPageFile="~/Common/PortalMaster.Master" Inherits="BACRMPortal.frmCaseList" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Src="~/Common/frmBizSorting.ascx" TagName="frmBizSorting" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Case List</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="view-cases">
            <label>
                View</label>
            <asp:DropDownList ID="ddlSort" runat="server" CssClass="signup" AutoPostBack="True">
                <asp:ListItem Value="0">My Cases</asp:ListItem>
                <asp:ListItem Value="1">All Cases</asp:ListItem>
                <asp:ListItem Value="2">Added in Last 7 Days</asp:ListItem>
                <asp:ListItem Value="3">Last 20 Added by me</asp:ListItem>
                <asp:ListItem Value="4">Last 20 Modified by me</asp:ListItem>
                <asp:ListItem Value="5">Close Cases</asp:ListItem>
            </asp:DropDownList>
            <%--<a href="#" class="view-case-edit">Edit</a>--%>
        </div>
        <div class="right-input">
            <table>
                <tr>
                    <td class="normal1" width="150" style="color: white !important">No of Records:
                        <asp:Label ID="lblRecordCountCases" runat="server"></asp:Label>
                    </td>
                    <td>
                        <label>
                            Status</label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="signup"></asp:DropDownList>
                    </td>
                    <td class="leftBorder" valign="bottom">
                        <asp:Button ID="btnGoCases" CssClass="button" runat="server" Text="Go"></asp:Button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Cases
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server" Direction="RightToLeft" HorizontalAlign="Right"
        LayoutType="div" UrlPaging="false" CssClass="pagn" ShowMoreButtons="true" ShowPageIndexBox="Never"
        Width="" AlwaysShow="true" ShowCustomInfoSection="Left" CustomInfoHTML="Showing records %startrecordindex% to %endrecordindex% of %recordcount% "
        CustomInfoSectionWidth="300px" CustomInfoStyle="line-height:20px;margin-right:3px;text-align:right !important;">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="BizSorting" runat="server"
    ClientIDMode="Static">
    <uc1:frmbizsorting id="frmBizSorting2" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:Table ID="table2" Width="100%" Height="350" GridLines="None" BorderColor="black"
        runat="server" BorderWidth="1" CellSpacing="0" CellPadding="0" CssClass="aspTable">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:GridView ID="gvSearch" runat="server" EnableViewState="true" AutoGenerateColumns="false"
                    CssClass="tbl" Width="100%" ShowHeaderWhenEmpty="true">
                    <Columns>
                    </Columns>
                    <EmptyDataTemplate>
                        No records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtGridColumnFilter" Style="display: none" runat="server"></asp:TextBox>
    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
</asp:Content>
