﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmKnowledgeBaseList.aspx.vb"
    MasterPageFile="~/Common/PortalMaster.Master" Inherits="BACRMPortal.frmKnowledgeBaseList" %>

<%@ Register Src="~/Common/frmBizSorting.ascx" TagName="frmBizSorting" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>KnowledgeBase List</title>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <%--<div class="view-cases">
        </div>--%>
        <div class="right-input" style="color: white !important">
            <table cellspacing="2" cellpadding="2" width="100%" border="0">
                <tr>
                    <td class="normal1" align="right">Find Solution using Name or Keyword
                    </td>
                    <td>
                        <asp:TextBox ID="txtKeyWord" runat="server" Width="200" CssClass="signup"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="btnGoBases" runat="server" CssClass="button" Text="Go"></asp:Button>
                    </td>
                    <td class="normal1" width="150" style="text-align: center">No of Records:&nbsp;<asp:Label ID="lblRecordSolution" runat="server"></asp:Label>
                    </td>
                    <td class="normal1" align="right" style="padding-right: 10px">Solution Category
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCategory" runat="server" Width="100" CssClass="signup" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Knowledge Base
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="BizSorting" runat="server"
    ClientIDMode="Static">
    <uc1:frmbizsorting id="frmBizSorting2" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:Table ID="table1" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="100%" GridLines="None">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:DataGrid ID="dgBases" runat="server" Width="100%" CssClass="dg" AllowSorting="True"
                    AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="numSolnID"></asp:BoundColumn>
                        <%--<asp:ButtonColumn DataTextField="vcSolnTitle" SortExpression="vcSolnTitle" HeaderText="Solution Name"
                            CommandName="Solution"></asp:ButtonColumn>--%>
                        <asp:BoundColumn DataField="vcSolnTitle" SortExpression="vcSolnTitle" HeaderText="Solution Name"></asp:BoundColumn>
                        <asp:BoundColumn DataField="txtSolution" SortExpression="txtSolution" HeaderText="Solution Description"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" runat="server" Style="display: none"></asp:TextBox>
</asp:Content>
