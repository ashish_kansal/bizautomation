﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports System.Web.Services

Public Class frmDashboard
    Inherits BACRMPage

    Private _IsControlAdded As Boolean
    Public Property IsControlAdded() As Boolean
        Get
            If ViewState("IsControlAdded") Is Nothing Then
                ViewState("IsControlAdded") = False
                Return CBool(ViewState("IsControlAdded"))
            End If

        End Get
        Set(ByVal value As Boolean)
            ViewState("IsControlAdded") = value
            _IsControlAdded = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("UserContactID") = Nothing Then Response.Redirect("../CommonPages/frmLogout.aspx")
            'If Not IsPostBack Then
            If Not IsControlAdded Then
                BindData()
            End If
            'End If
            hdnDomain.Value = Session("DomainID")
            hdnRelationship.Value = Session("RelationShip")
            hdnProfile.Value = Session("Profile")
            hdnU.Value = Session("UserContactID")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub BindData()
        Try
            Dim objAdmin As New UserAccess
            objAdmin.DomainID = Session("DomainID")
            objAdmin.Relationship = Session("RelationShip")
            objAdmin.ProfileID = Session("Profile")
            objAdmin.ContactID = Session("UserContactID") 'get portlet as per user customized order
            Dim ds As DataSet = objAdmin.GetPortalDashboardReports

            If ds.Tables(1).Rows.Count = 0 And ds.Tables(2).Rows.Count = 0 Then
                objAdmin.ContactID = 0 'get portlet in default sorting order
                ds = objAdmin.GetPortalDashboardReports
            End If

            For Each dr As DataRow In ds.Tables(1).Rows
                LoadControl(dr("numReportID"), phColumn1)
            Next

            For Each dr As DataRow In ds.Tables(2).Rows
                LoadControl(dr("numReportID"), phColumn2)
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub LoadControl(ByVal intReportID As Int32, ByVal phColumn As PlaceHolder)
        Try
            Dim userControl As New Control
            Select Case intReportID
                Case 1
                    userControl = Page.LoadControl("~/Dashboard/Assets.ascx")
                Case 2
                    userControl = Page.LoadControl("~/Dashboard/Contracts.ascx")
                Case 3
                    userControl = Page.LoadControl("~/Dashboard/Documents.ascx")
                Case 4
                    userControl = Page.LoadControl("~/Dashboard/FinancialStatus.ascx")
                Case 5
                    userControl = Page.LoadControl("~/Dashboard/BizdocAmtDue.ascx")
                    'Case 6
                    '    userControl = Page.LoadControl("~/Dashboard/ItemsToReturn.ascx")
                Case 7
                    userControl = Page.LoadControl("~/Dashboard/OpenCases.ascx")
                Case 8
                    userControl = Page.LoadControl("~/Dashboard/OpenProjects.ascx")
                Case 9
                    userControl = Page.LoadControl("~/Dashboard/BillBizdocAmtDue.ascx")
                Case 10
                    userControl = Page.LoadControl("~/Dashboard/AccountDetails.ascx")
                    'Case 11
                    '    userControl = Page.LoadControl("~/Dashboard/frmCommission.ascx")
            End Select
            phColumn.Controls.Add(userControl)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    <WebMethod()>
    Public Shared Function UpdateOrder(ByVal Column1Values As String, ByVal Column2Values As String, ByVal RelID As String, ByVal ProfID As String, ByVal DID As String, ByVal UID As String) As String
        Try
            Dim dt As New DataTable
            dt.Columns.Add("numRelationshipID")
            dt.Columns.Add("numProfileID")
            dt.Columns.Add("numReportID")
            dt.Columns.Add("tintColumnNumber")
            dt.Columns.Add("tintRowOrder")

            Dim strCol1() As String = Column1Values.Split(New String() {","}, StringSplitOptions.RemoveEmptyEntries)
            Dim strCol2() As String = Column2Values.Split(New String() {","}, StringSplitOptions.RemoveEmptyEntries)
            Dim dr As DataRow

            For i As Integer = 0 To strCol1.Length - 1
                If CCommon.ToLong(strCol1(i)) > 0 Then
                    dr = dt.NewRow()
                    dr("numReportID") = strCol1(i)
                    dr("numRelationshipID") = CCommon.ToInteger(RelID)
                    dr("numProfileID") = CCommon.ToInteger(ProfID)
                    dr("tintRowOrder") = i + 1
                    dr("tintColumnNumber") = "1"
                    dt.Rows.Add(dr)
                End If
            Next

            For i As Integer = 0 To strCol2.Length - 1
                If CCommon.ToLong(strCol2(i)) > 0 Then
                    dr = dt.NewRow()
                    dr("numReportID") = strCol2(i)
                    dr("numRelationshipID") = CCommon.ToInteger(RelID)
                    dr("numProfileID") = CCommon.ToInteger(ProfID)
                    dr("tintRowOrder") = i + 1
                    dr("tintColumnNumber") = "2"
                    dt.Rows.Add(dr)
                End If
            Next
            Dim ds As New DataSet
            ds.Tables.Add(dt)

            If CCommon.ToLong(UID) > 0 Then
                Dim objAdmin As New UserAccess
                objAdmin.StrItems = ds.GetXml
                objAdmin.DomainID = CCommon.ToLong(DID)
                objAdmin.Relationship = CCommon.ToInteger(RelID)
                objAdmin.ProfileID = CCommon.ToInteger(ProfID)
                objAdmin.ContactID = CCommon.ToLong(UID)
                Return objAdmin.ManagePortalDashboardReports()
            End If
            Return False
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Private Sub lnkReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkReset.Click
    '    Try
    '        Dim objAdmin As New UserAccess
    '        objAdmin.StrItems = "Reset"
    '        objAdmin.DomainID = CCommon.ToLong(Session("DomainID"))
    '        objAdmin.Relationship = CCommon.ToInteger(Session("Relationship"))
    '        objAdmin.ProfileID = CCommon.ToInteger(Session("Profile"))
    '        objAdmin.ContactID = CCommon.ToLong(Session("UserContactID"))
    '        objAdmin.ManagePortalDashboardReports()
    '        Response.Redirect("frmPortaldashboard.aspx", False)
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

End Class
