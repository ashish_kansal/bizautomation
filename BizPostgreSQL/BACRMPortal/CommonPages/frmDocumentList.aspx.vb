﻿Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common

Public Class frmDocumentList
    Inherits BACRMPage
    Dim strColumn As String
    Dim ds As New DataSet
    Public lngCategory As Long
    Public lngStatus As Long

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            Dim objCommon As New CCommon
            If Session("UserContactID") = Nothing Then Response.Redirect("../CommonPages/frmLogout.aspx")

            m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmDocumentList.aspx", Session("UserContactID"), 15, 12)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../Common/frmAuthorization.aspx")
            If Not IsPostBack Then
                Session("Asc") = 1
                objCommon.sb_FillComboFromDBwithSel(ddlCategory, 29, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlBizDocs, 27, Session("DomainID"))

                txtCurrrentPage.Text = 1
                BindDatagrid()
            End If

            If txtSortChar.Text <> "" Then
                ViewState("SortChar") = txtSortChar.Text
                ViewState("Column") = "vcDocName"
                Session("Asc") = 0
                BindDatagrid()
                txtSortChar.Text = ""
            End If

            If ddlDocumentSection.SelectedItem.Value = "B" Then
                tdCategory.Visible = False
                tdBizdocs.Visible = True
            Else
                tdCategory.Visible = True
                tdBizdocs.Visible = False
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindDatagrid()
        Try
            Dim dtDocuments As DataTable
            Dim objDocuments As New DocumentList
            Dim SortChar As Char
            If ViewState("SortChar") <> "" Then
                SortChar = ViewState("SortChar")
            Else : SortChar = "0"
            End If
            With objDocuments
                .DivisionID = Session("DivId")
                .DocumentSection = ddlDocumentSection.SelectedItem.Value
                .SortCharacter = SortChar
                If ddlDocumentSection.SelectedItem.Value = "B" Then
                    .DocCategory = ddlBizDocs.SelectedItem.Value
                Else : .DocCategory = ddlCategory.SelectedItem.Value
                End If
                If txtCurrrentPage.Text.Trim <> "" Then
                    .CurrentPage = txtCurrrentPage.Text
                Else : .CurrentPage = 1
                End If
                .PageSize = Session("PagingRows")
                .TotalRecords = 0

                If txtSortColumn.Text <> "" Then
                    .columnName = txtSortColumn.Text
                Else : .columnName = "[Date]"
                End If

                'If ViewState("Column") <> "" Then
                '    .columnName = ViewState("Column")
                'Else : .columnName = "[Date]"
                'End If

                If txtSortOrder.Text = "D" Then
                    .columnSortOrder = "Desc"
                Else : .columnSortOrder = "Asc"
                End If

                .byteMode = 0
            End With

            dtDocuments = objDocuments.ExtGetDocuments

            If objDocuments.TotalRecords = 0 Then
                lblRecordCount.Text = 0
            Else
                lblRecordCount.Text = String.Format("{0:#,###}", objDocuments.TotalRecords)
                Dim strTotalPage As String()
                Dim decTotalPage As Decimal
                decTotalPage = lblRecordCount.Text / Session("PagingRows")
                decTotalPage = Math.Round(decTotalPage, 2)
                strTotalPage = CStr(decTotalPage).Split(".")
                If (lblRecordCount.Text Mod Session("PagingRows")) = 0 Then
                    txtTotalPage.Text = strTotalPage(0)
                Else
                    txtTotalPage.Text = strTotalPage(0) + 1
                End If
                txtTotalRecords.Text = lblRecordCount.Text
            End If

            bizPager.PageSize = Session("PagingRows")
            bizPager.RecordCount = objDocuments.TotalRecords
            bizPager.CurrentPageIndex = txtCurrrentPage.Text

            dgDocs.DataSource = dtDocuments
            dgDocs.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            txtCurrrentPage.Text = 1
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgDocs_ItemCommand(source As Object, e As DataGridCommandEventArgs) Handles dgDocs.ItemCommand
        Try
            If e.CommandName = "Name" Then
                Session("DocID") = e.Item.Cells(0).Text()
                Response.Redirect("../Documents/frmCusAddDocs.aspx?Type=" & e.Item.Cells(1).Text & "&RecID=" & e.Item.Cells(2).Text & "&DocID=" & e.Item.Cells(0).Text, False)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgDocs_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgDocs.SortCommand
        Try
            strColumn = e.SortExpression.ToString()
            If ViewState("Column") <> strColumn Then
                ViewState("Column") = strColumn
                Session("Asc") = 0
            Else
                If Session("Asc") = 0 Then
                    Session("Asc") = 1
                Else : Session("Asc") = 0
                End If
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
        Try
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Function ReturnDate(ByVal CloseDate) As String
        Try
            If CloseDate Is Nothing Then Exit Function
            Dim strTargetResolveDate As String = ""
            Dim temp As String = ""
            If Not IsDBNull(CloseDate) Then
                strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))

                ' check Today date components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                ' if both are same it is today
                Dim strNow As Date
                strNow = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                If (CloseDate.Date = strNow.Date And CloseDate.Month = strNow.Month And CloseDate.Year = strNow.Year) Then
                    strTargetResolveDate = "<font color=red><b>Today</b></font>"
                    Return strTargetResolveDate

                    ' check [ TodayDate + 1 ] Date.... components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                    ' if both are same it was Yesterday
                ElseIf (CloseDate.Date.AddDays(1).Date = strNow.Date And CloseDate.AddDays(1).Month = strNow.Month And CloseDate.AddDays(1).Year = strNow.Year) Then
                    strTargetResolveDate = "<font color=purple><b>Yesterday</b></font>"
                    Return strTargetResolveDate

                    ' check TodayDate .... components [ Date , Month , Year ] with Parameter [ CloseDate + 1 ] Date....  components [ Date , Month , Year ] 
                    ' if both are same it will Tomorrow
                ElseIf (CloseDate.Date = strNow.AddDays(1).Date And CloseDate.Month = strNow.AddDays(1).Month And CloseDate.Year = strNow.AddDays(1).Year) Then
                    temp = CloseDate.Hour.ToString + ":" + CloseDate.Minute.ToString
                    strTargetResolveDate = "<font color=orange><b>Tomorrow</b></font>"
                    Return strTargetResolveDate
                Else
                    strTargetResolveDate = strTargetResolveDate
                    Return strTargetResolveDate
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function ReturnDateTime(ByVal CloseDate As Date) As String
        Try
            If IsDBNull(CloseDate) = False Then Return FormattedDateTimeFromDate(CloseDate, Session("DateFormat"))
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub ddlDocumentSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDocumentSection.SelectedIndexChanged
        Try
            txtCurrrentPage.Text = 1
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
        Try
            txtCurrrentPage.Text = 1
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlBizDocs_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBizDocs.SelectedIndexChanged
        Try
            txtCurrrentPage.Text = 1
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class