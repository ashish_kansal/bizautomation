﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmDocumentList.aspx.vb" MasterPageFile="~/Common/PortalMaster.Master" Inherits="BACRMPortal.frmDocumentList" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Src="~/Common/frmBizSorting.ascx" TagName="frmBizSorting" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Documents List</title>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)" />
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)" />
    <script language="javascript" type="text/javascript">

        function reLoad() {
            document.getElementById("btnGo1").click();
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="view-cases" style="width: 60% !important; margin-top: -4px !important;color: white !important">
        <table align="center" border="0" cellpadding="2" cellspacing="2">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td class="normal1" align="right">No of Records:
                            </td>
                            <td>
                                <asp:Label ID="lblRecordCount" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table>
                        <tr>
                            <td class="normal1" align="right">Filter by Document Section
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlDocumentSection"
                                    runat="server" CssClass="signup" AutoPostBack="True">
                                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                    <asp:ListItem Value="A" Selected="True">Organization</asp:ListItem>
                                    <asp:ListItem Value="C">Contacts</asp:ListItem>
                                    <asp:ListItem Value="O">Opportunities</asp:ListItem>
                                    <asp:ListItem Value="P">Projects</asp:ListItem>
                                    <%--<asp:ListItem Value="B">BizDocs</asp:ListItem>--%>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
                <td id="tdBizdocs" runat="server">
                    <table>
                        <tr>
                            <td class="normal1" align="right">BizDocs
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlBizDocs"
                                    runat="server" CssClass="signup" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
                <td id="tdCategory" runat="server">
                    <table>
                        <tr>
                            <td class="normal1" align="right">Filter by Document Category
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlCategory" runat="server" CssClass="signup" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>

            </tr>
        </table>
    </div>
    <div class="right-input" style="width: 30% !important">
        <table align="right">
            <tr>
                <td></td>
                <td class="normal1" align="right">
                    <label>
                        Keyword Search
                    </label>
                </td>
                <td>
                    <asp:TextBox ID="txtKeyWord" CssClass="signup" runat="server"></asp:TextBox>&nbsp;
                </td>
                <td>
                    <asp:Button ID="btnGo" CssClass="button" runat="server" Text="Go"></asp:Button>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Document Templates
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server" Direction="RightToLeft" HorizontalAlign="Right"
        LayoutType="div" UrlPaging="false" CssClass="pagn" ShowMoreButtons="true" ShowPageIndexBox="Never"
        Width="" AlwaysShow="true" ShowCustomInfoSection="Left" CustomInfoHTML="Showing records %startrecordindex% to %endrecordindex% of %recordcount% "
        CustomInfoSectionWidth="300px" CustomInfoStyle="line-height:20px;margin-right:3px;text-align:right !important;">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="BizSorting" runat="server"
    ClientIDMode="Static">
    <uc1:frmbizsorting id="frmBizSorting2" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td>
                <asp:DataGrid ID="dgDocs" runat="server" Width="100%" CssClass="dg" AllowSorting="True"
                    AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="numGenericDocID"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="vcDocumentSection"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="numRecID"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="VcFileName"></asp:BoundColumn>
                        <%--<asp:BoundColumn SortExpression="vcDocName" HeaderText="<font>Document Name</font>" ItemStyle-HorizontalAlign="Center" DataField="vcDocName"></asp:BoundColumn>--%>
                        <asp:ButtonColumn CommandName="Name" SortExpression="vcDocName" DataTextField="vcDocName"
                            HeaderText="Document Name"></asp:ButtonColumn>
                        <asp:BoundColumn DataField="Category" SortExpression="Category" HeaderText="<font>Document Category</font>"></asp:BoundColumn>
                        <%-- <asp:BoundColumn DataField="CreatedDate" SortExpression="CreatedDate" HeaderText="<font color=white>Created On</font>"></asp:BoundColumn> --%>
                        <asp:TemplateColumn HeaderText="<font>Date On</font>" SortExpression="CreatedDate">
                            <ItemTemplate>
                                <%#ReturnDateTime(DataBinder.Eval(Container.DataItem, "CreatedDate"))%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="Approved" HeaderText="Approved" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Declined" HeaderText="Declined" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Pending" HeaderText="Pending" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Modified" SortExpression="[Date]" HeaderText="<font >Last Modified On, By</font>"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Section" SortExpression="Section" HeaderText="<font>Related Section</font>"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Name" SortExpression="Name" HeaderText="<font>Name</font>"></asp:BoundColumn>
                        <asp:BoundColumn DataField="vcfiletype" SortExpression="vcfiletype" HeaderText="<font>File Type</font>"></asp:BoundColumn>
                        <%--<asp:TemplateColumn SortExpression="vcfiletype" HeaderText="<font>File Type</font>">
                            <ItemTemplate>
                                <asp:HyperLink ID="hplLink" Target="_blank" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "vcfiletype") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>--%>
                        <%--<asp:BoundColumn DataField="BusClass" SortExpression="BusClass" HeaderText="<font>Document Status</font>"></asp:BoundColumn>--%>
                        <asp:BoundColumn DataField="Modified" SortExpression="Modified" HeaderText="<font>Last Modified By, Last Modified On</font>"></asp:BoundColumn>
                        <%--<asp:BoundColumn DataField="CheckedOut" SortExpression="CheckedOut" HeaderText="<font >Last Checked-Out By,On</font>"></asp:BoundColumn>--%>
                        <%-- <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete">
                                </asp:Button>
                                <asp:LinkButton ID="lnkDelete" runat="server" Visible="false">
											<font color="#730000">*</font></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>--%>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
    <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:Button ID="btnGo1" runat="server" Style="display: none" />
</asp:Content>
