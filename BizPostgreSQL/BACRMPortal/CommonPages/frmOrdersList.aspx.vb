﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Contacts

Public Class frmOrdersList
    Inherits BACRMPage

    Dim SI As Integer = 0
    Dim SI1 As Integer = 0
    Dim SI2 As Integer = 0
    Dim frm As String = ""
    Dim frm1 As String = ""
    Dim frm2 As String = ""
    Dim intOrder As Integer = 0
    Dim strColumn As String
    Dim RegularSearch As String
    Dim CustomSearch As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim objCommon As New CCommon
            If Session("UserContactID") = Nothing Then Response.Redirect("../CommonPages/frmLogout.aspx")

            If Not IsPostBack Then

                If GetQueryStringVal("type") = "1" Then
                    ddlSortDeals.Items.FindByValue("1").Selected = True
                    lblDeal.Text = "Sales Orders"
                    'hplSettings.Attributes.Add("onclick", "return OpenSetting(78)")
                    txtFormID.Text = "78"
                    ddlFilterBy.Items(1).Text = "Partially Invoiced Orders"
                    ddlFilterBy.Items(2).Text = "Fully Invoiced Orders"
                    ddlFilterBy.Items(3).Text = "Orders without Invoices"
                    ddlFilterBy.Items(4).Text = "Orders with items yet to be added to Invoices"
                ElseIf GetQueryStringVal("type") = "2" Then
                    ddlSortDeals.Items.FindByValue("2").Selected = True
                    lblDeal.Text = "Purchase Orders"
                    'hplSettings.Attributes.Add("onclick", "return OpenSetting(79)")
                    txtFormID.Text = "79"
                    ddlFilterBy.Items(1).Text = "Partially P.O.d Orders"
                    ddlFilterBy.Items(2).Text = "Fully P.O.d Orders"
                    ddlFilterBy.Items(3).Text = "Orders without P.O.(s)"
                    ddlFilterBy.Items(4).Text = "Orders with items yet to be added to P.O.s"
                End If

            End If
            If GetQueryStringVal("partner") = "1" Then
                intOrder = 1
            ElseIf GetQueryStringVal("partner") = "2" Then
                intOrder = 2
            End If

            If Not IsPostBack Then
                Session("Data") = Nothing
                Session("SalesProcessDetails") = Nothing
                Session("List") = "Opp"

                objCommon.sb_FillComboFromDBwithSel(ddlOrderStatus, 176, Session("DomainID"))

                If (CCommon.ToInteger(txtCurrrentPage.Text) = 0) Then
                    txtCurrrentPage.Text = "1"
                End If

                BindDatagrid()
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindDatagrid(Optional ByVal CreateCol As Boolean = True)
        Try
            Dim dtOpportunity As DataTable
            Dim objOpportunity As New COpportunities
            objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            Dim SortChar As Char = ""

            ' Set Default Filter 
            With objOpportunity
                .UserCntID = 0 ' Session("UserContactID")
                .DomainID = Session("DomainID")
                .intOrder = intOrder
                .DivisionID = Session("DivID")
                .SortOrder = ddlSortDeals.SelectedItem.Value
                .FilterBy = ddlFilterBy.SelectedItem.Value
                .OrderStatus = ddlOrderStatus.SelectedValue
                .endDate = Now()
                .SortCharacter = txtSortChar.Text.Trim()
                .Shipped = IIf(radOpen.Checked, 0, 1)

                If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                .CurrentPage = txtCurrrentPage.Text.Trim()

                .PageSize = Session("PagingRows")
                .TotalRecords = 0

                If txtSortOrder.Text = "D" Then
                    .columnSortOrder = "Desc"
                Else : .columnSortOrder = "Asc"
                End If

                .RegularSearchCriteria = RegularSearch
                .CustomSearchCriteria = CustomSearch
            End With

            If txtSortColumn.Text <> "" Then
                objOpportunity.columnName = txtSortColumn.Text
            Else : objOpportunity.columnName = "opp.bintcreateddate"
            End If

            If objOpportunity.FilterBy = 5 Then
                objOpportunity.columnName = "vcPOppName"
                objOpportunity.columnSortOrder = "Asc"
            End If

            If ViewState("ClosedDeals") = "True" Then
                objOpportunity.intType = ddlSortDeals.SelectedValue
            Else
                objOpportunity.intType = ddlSortDeals.SelectedValue
            End If

            Dim dsList As DataSet
            dsList = objOpportunity.GetDealsListForPortal()
            dtOpportunity = dsList.Tables(0)

            Dim dtTableInfo As DataTable
            dtTableInfo = dsList.Tables(1)

            If objOpportunity.TotalRecords = 0 Then
                lblRecordsDeals.Text = 0
            Else
                lblRecordsDeals.Text = String.Format("{0:#,###}", objOpportunity.TotalRecords)
                Dim strTotalPage As String()
                Dim decTotalPage As Decimal
                decTotalPage = lblRecordsDeals.Text / Session("PagingRows")
                decTotalPage = Math.Round(decTotalPage, 2)
                strTotalPage = CStr(decTotalPage).Split(".")
                If (lblRecordsDeals.Text Mod Session("PagingRows")) = 0 Then
                    txtTotalPageDeals.Text = strTotalPage(0)
                Else
                    txtTotalPageDeals.Text = strTotalPage(0) + 1
                End If
                txtTotalRecordsDeals.Text = lblRecordsDeals.Text
            End If

            Dim i As Integer
            For i = 0 To dtOpportunity.Columns.Count - 1
                dtOpportunity.Columns(i).ColumnName = dtOpportunity.Columns(i).ColumnName.Replace(".", "")
            Next

            If CreateCol = True Then
                Dim bField As BoundField
                Dim Tfield As TemplateField
                gvSearch.Columns.Clear()

                For Each drRow As DataRow In dtTableInfo.Rows
                    Tfield = New TemplateField
                    Tfield.HeaderTemplate = New GridTemplatePortal(ListItemType.Header, drRow, 0, Nothing, IIf(ddlSortDeals.SelectedValue = 1, 78, 79), objOpportunity.columnName, objOpportunity.columnSortOrder)
                    Tfield.ItemTemplate = New GridTemplatePortal(ListItemType.Item, drRow, 0, Nothing, IIf(ddlSortDeals.SelectedValue = 1, 78, 79), objOpportunity.columnName, objOpportunity.columnSortOrder)
                    gvSearch.Columns.Add(Tfield)
                Next
            End If

            bizPager.PageSize = Session("PagingRows")
            bizPager.RecordCount = objOpportunity.TotalRecords
            bizPager.CurrentPageIndex = txtCurrrentPage.Text

            gvSearch.DataSource = dtOpportunity
            gvSearch.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub txtCurrrentPage_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCurrrentPage.TextChanged
        Try
            BindDatagrid(False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Function ReturnDateTime(ByVal CloseDate) As String
        Try
            Dim strTargetResolveDate As String = ""
            If Not IsDBNull(CloseDate) Then strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
            Return strTargetResolveDate
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
        Try
            BindDatagrid(True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub radClosed_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radClosed.CheckedChanged
        Try
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub radOpen_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radOpen.CheckedChanged
        Try
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlFilterBy_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFilterBy.SelectedIndexChanged
        Try
            If GetQueryStringVal("type") = "1" Then
                Session("SOFilterBy") = ddlFilterBy.SelectedIndex
            Else
                Session("POFilterBy") = ddlFilterBy.SelectedIndex
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlOrderStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOrderStatus.SelectedIndexChanged
        Try
            If GetQueryStringVal("type") = "1" Then
                Session("SOOrderStatus") = ddlOrderStatus.SelectedIndex
            Else
                Session("POOrderStatus") = ddlOrderStatus.SelectedIndex
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class
