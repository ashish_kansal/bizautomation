﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmOrdersList.aspx.vb" MasterPageFile="~/Common/PortalMaster.Master" Inherits="BACRMPortal.frmOrdersList" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Src="~/Common/frmBizSorting.ascx" TagName="frmBizSorting" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Orders List</title>
    <script language="javascript" type="text/javascript">

        function OpenWindow(a, b, c) {
            var str;
            if (b == 0) {

                str = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=deallist&DivID=" + a;

            }
            else if (b == 1) {

                str = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=deallist&DivID=" + a;

            }
            else if (b == 2) {

                str = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=deallist&klds+7kldf=fjk-las&DivId=" + a;

            }

            document.location.href = str;

        }
        function OpenContact(a, b) {

            var str;

            str = "../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=deallist&ft6ty=oiuy&CntId=" + a;


            document.location.href = str;

        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function SelWarOpenDeals(a) {
            window.open("../opportunity/frmSelWhouseOpenDls.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Opid=" + a)
            return false;
        }
        function OpenSetting() {
            window.open('../Opportunity/frmConfOppList.aspx?FormId=' + document.getElementById("txtFormID").value, '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=400,scrollbars=yes,resizable=yes')
            return false
        }
        function OpenOpp(a, b) {

            var str;

            str = "../opportunity/frmOpportunities.aspx?frm=deallist&OpID=" + a;

            document.location.href = str;
        }
        //var prg_width = 60;
        function progress(ProgInPercent, Container, progress, prg_width) {
            //Set Total Width
            var OuterDiv = document.getElementById(Container);
            OuterDiv.style.width = prg_width + 'px';
            OuterDiv.style.height = '5px';

            if (ProgInPercent > 100) {
                ProgInPercent = 100;
            }
            //Set Progress Percentage
            var node = document.getElementById(progress);
            node.style.width = parseInt(ProgInPercent) * prg_width / 100 + 'px';
        }

        function DeleteRecord() {
            var RecordIDs = GetCheckedRowValues()
            document.getElementById('txtDelOppId').value = RecordIDs;
            if (RecordIDs.length > 0)
                return true
            else {
                alert('Please select atleast one record!!');
                return false
            }
        }

        function OpenBizInvoice(a, b) {
            window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
        }

        function OpenMirrorBizDoc(a, b) {
            window.open('../opportunity/frmMirrorBizDoc.aspx?RefID=' + a + '&RefType=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=1000,height=600,scrollbars=yes,resizable=yes');
        }
    </script>

    <style type="text/css">
        .ProgressBar {
            height: 5px;
            width: 0px;
            background-color: Green;
        }

        .ProgressContainer {
            border: 1px solid black;
            height: 5px;
            font-size: 1px;
            background-color: White;
            line-height: 0;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="view-cases">
            <label>
                View</label>
            <asp:DropDownList ID="ddlFilterBy" runat="server" AutoPostBack="True">
                <asp:ListItem Value="0">--Select One--</asp:ListItem>
                <asp:ListItem Value="1">Partially Fulfilled Orders</asp:ListItem>
                <asp:ListItem Value="2">Fulfilled Orders</asp:ListItem>
                <asp:ListItem Value="3">Orders without Authoritative-BizDocs</asp:ListItem>
                <asp:ListItem Value="4">Orders with items yet to be added to Invoices</asp:ListItem>
                <asp:ListItem Value="5">Sort Alphabetically</asp:ListItem>
                <asp:ListItem Value="6">Orders with deferred Income/Invoices</asp:ListItem>
            </asp:DropDownList>
            <%--<a href="#" class="view-case-edit">Edit</a>--%>
        </div>
        <div class="right-input">
            <table align="right">
                <tr>
                    <td>
                        <label>
                            Order Status</label>
                    </td>
                    <td align="right">
                        <asp:DropDownList ID="ddlOrderStatus" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList Visible="false" ID="ddlSortDeals" runat="server" AutoPostBack="True">
                            <asp:ListItem Value="1">Sales Deals</asp:ListItem>
                            <asp:ListItem Value="2">Purchase Deals</asp:ListItem>
                        </asp:DropDownList>
                        <asp:RadioButton ID="radOpen" GroupName="rad" Checked="true" AutoPostBack="true"
                            runat="server" Text="Open" />
                        <asp:RadioButton ID="radClosed" GroupName="rad" AutoPostBack="true" runat="server"
                            Text="Closed" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table cellspacing="0" cellpadding="2" width="100%" border="0">
        <tr>
            <td class="normal4" align="left">
                <asp:Label ID="lblRecordsDeals" runat="server" Visible="false"></asp:Label>
            </td>
            <td align="center" class="normal4">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblDeal" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server" Direction="RightToLeft" HorizontalAlign="Right"
        LayoutType="div" UrlPaging="false" CssClass="pagn" ShowMoreButtons="true" ShowPageIndexBox="Never"
        Width="" AlwaysShow="true" ShowCustomInfoSection="Left" CustomInfoHTML="Showing records %startrecordindex% to %endrecordindex% of %recordcount% "
        CustomInfoSectionWidth="300px" CustomInfoStyle="line-height:20px;margin-right:3px;text-align:right !important;">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="BizSorting" runat="server"
    ClientIDMode="Static">
    <uc1:frmbizsorting id="frmBizSorting2" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:GridView ID="gvSearch" runat="server" EnableViewState="true" AutoGenerateColumns="false"
        CssClass="tbl" Width="100%" ShowHeaderWhenEmpty="true">
        <Columns>
        </Columns>
        <EmptyDataTemplate>
            No records Found.
        </EmptyDataTemplate>
    </asp:GridView>
    <asp:TextBox ID="txtDelOppId" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalPageDeals" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecordsDeals" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtGridColumnFilter" Style="display: none" runat="server"></asp:TextBox>
    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtFormID" runat="server" Style="display: none"></asp:TextBox>
</asp:Content>
