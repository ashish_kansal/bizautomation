﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmProjectList.aspx.vb" MasterPageFile="~/Common/PortalMaster.Master" Inherits="BACRMPortal.frmProjectList" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Src="~/Common/frmBizSorting.ascx" TagName="frmBizSorting" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Projects</title>

    <script language="javascript" type="text/javascript">
        function fnSortByChar(varSortChar) {
            document.Form1.txtSortChar.value = varSortChar;
            if (document.Form1.txtCurrrentPage != null) {
                document.Form1.txtCurrrentPage.value = 1;
            }
            document.Form1.btnGo.click();
        }
        function GoImport() {
            window.location.href = "../admin/importfromputlook.aspx?frm=prospects";
            return false;
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <%--<div class="view-cases" >
        <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
            </tr>
        </table>
    </div>--%>
    <div class="input-part">
        <div class="view-cases">
            <label>
                View</label>
            <asp:DropDownList ID="ddlSort" runat="server" AutoPostBack="True">
                <asp:ListItem Value="1">My Projects</asp:ListItem>
                <asp:ListItem Value="3">All Projects</asp:ListItem>
                <asp:ListItem Value="2">My Subordinates</asp:ListItem>
                <asp:ListItem Value="4">Added in Last 7 days</asp:ListItem>
                <asp:ListItem Value="5">Last 20 Added by me</asp:ListItem>
                <asp:ListItem Value="6">Last 20 Modified by me</asp:ListItem>
            </asp:DropDownList>
            <%--<a href="#" class="view-case-edit">Edit</a>--%>
        </div>
        <div class="right-input">
            <table align="right">
                <tr>
                    <asp:Button runat="server" ID="btnGo" Text="" Style="display: none"></asp:Button>
                    <td class="normal1" align="right" style="color: white">No of Records:
                    </td>
                    <td>
                        <asp:Label ID="lblRecordCount" runat="server" style="color: white"></asp:Label>
                    </td>
                    <td>
                        <label>
                            Project Status</label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlProjectStatus" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Projects
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server" Direction="RightToLeft" HorizontalAlign="Right"
        LayoutType="div" UrlPaging="false" CssClass="pagn" ShowMoreButtons="true" ShowPageIndexBox="Never"
        Width="" AlwaysShow="true" ShowCustomInfoSection="Left" CustomInfoHTML="Showing records %startrecordindex% to %endrecordindex% of %recordcount% "
        CustomInfoSectionWidth="300px" CustomInfoStyle="line-height:20px;margin-right:3px;text-align:right !important;">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="BizSorting" runat="server"
    ClientIDMode="Static">
    <uc1:frmBizSorting ID="frmBizSorting2" runat="server" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:Table ID="table3" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="100%" BorderColor="black" GridLines="None" Height="350" CssClass="aspTable">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:GridView ID="gvSearch" runat="server" EnableViewState="true" AutoGenerateColumns="false"
                    CssClass="tbl" Width="100%" ShowHeaderWhenEmpty="true">
                    <Columns>
                    </Columns>
                    <EmptyDataTemplate>
                        No records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
                <%--<asp:DataGrid ID="dgProjects" runat="server" Width="100%" CssClass="dg" AllowSorting="True"
                    AutoGenerateColumns="False" BorderColor="white">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="numProId"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="numOppId"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="numRecOwner"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="numTerid"></asp:BoundColumn>
                        <asp:BoundColumn DataField="Name" SortExpression="Name" HeaderText="<font>Name</font>"></asp:BoundColumn>
                        <%--<asp:ButtonColumn DataTextField="DealID" SortExpression="DealID" HeaderText="<font color=white>Opportunity Or Deal ID</font>"
                                    CommandName="Opportunity"></asp:ButtonColumn>--%>
                <%--<asp:ButtonColumn DataTextField="CustJobContact" SortExpression="CustJobContact"
                                    HeaderText="<font color=white>Cust. Prime Job Contact</font>" CommandName="Contact">
                                </asp:ButtonColumn>
                                <asp:BoundColumn DataField="LstCmtMilestone" SortExpression="LstCmtMilestone" HeaderText="<font color=white>Last Completed Milestone</font>">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="lstCompStage" SortExpression="lstCompStage" HeaderText="<font color=white>Last completed Stage, Completed vs. Due Date</font>">
                                </asp:BoundColumn>
                                <asp:BoundColumn DataField="AssignedToBy" SortExpression="AssignedToBy" HeaderText="<font color=white>Assigned To/ By</font>">
                                </asp:BoundColumn>--%>
                <%--<asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="Delete" Text="X" CommandName="Delete"></asp:Button>
                                <asp:LinkButton ID="lnkDelete" runat="server" Visible="false">
											<font color="#730000">*</font></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>--%>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" runat="server" Style="display: none" Text="0"></asp:TextBox>
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
</asp:Content>
