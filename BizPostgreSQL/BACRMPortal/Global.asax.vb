Imports System.Web
Imports System.Web.SessionState
Imports System.Net.Dns
Imports BACRM.BusinessLogic.Tracking
Imports BACRMPortal.QueryStringVal
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports System.Globalization
Imports System.IO

Public Class [Global]
    Inherits System.Web.HttpApplication

#Region " Component Designer Generated Code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container()
    End Sub

#End Region

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)

        'Load Portal Name (Sub Domains) into Application variable
        Dim dtTable As DataTable
        Dim objUserAccess As New UserAccess
        objUserAccess.DomainID = -1 'Will bring all domains
        dtTable = objUserAccess.GetDomainDetails()
        Application("Subscribers") = dtTable

        Application("IsConfigValid") = False
        If Not ValidateWebConfigSettings() Then
            Application("IsConfigValid") = False
        Else
            Application("IsConfigValid") = True
        End If
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        'compare requested URL, ie. host name
        'On mataching set Domain ID in session
        If CCommon.ToLong(Session("DomainID")) = 0 Then
            Dim dt As DataTable = Application("Subscribers")
            If dt.Rows.Count > 0 Then
                Dim SelectedRows As DataRow() = dt.Select(" vcPortalName = '" & IIf(Request.Url.Host.IndexOf(".") > -1, Request.Url.Host.Split(".")(0).ToString, Request.Url.Host) & "'")
                If SelectedRows.Length > 0 Then
                    Session("DomainID") = SelectedRows(0)("numDomainID")
                Else
                    Session("DomainID") = 1
                End If
            End If
        End If

        If Request.Url.ToString.ToLower.Contains("/webtrack.aspx") Then
            ' Intialize tracker object and store it into session
            Dim tracker As SessionTracker = New SessionTracker
            Dim objcommon As New CCommon
            Session("Tracker") = tracker

            'Set Campaign in session
            FindCampaignNameFromURL()


            If Not Request.Cookies.Get("DivID") Is Nothing Then
                Session("DivID") = Request.Cookies.Get("DivID").Value
            End If
        End If

    End Sub

    Sub Application_PreRequestHandlerExecute(ByVal sender As Object, ByVal e As EventArgs)
        'Remarks: Customizable UI for portal is handled here 
        If TypeOf (Me.Context.Handler) Is System.Web.UI.Page Then
            Dim page As Page = Me.Context.Handler
            If Not page Is Nothing Then
                AddHandler page.InitComplete, AddressOf Page_InitComplete
            End If
        End If
        'Below code is for Web Analytics
        If Not IsNothing(Context.Session) Then
            If Not Session("Tracker") Is Nothing Then
                Dim tracker As SessionTracker
                tracker = Session("Tracker")
                If Not IsNothing(tracker) Then
                    If Request.Url.ToString.ToLower.Contains("/webtrack.aspx") Then

                        Dim strURL As String
                        strURL = CCommon.ToString(Request.QueryString("Url"))
                        strURL = Replace(Replace(Replace(Replace(strURL, "%3A", ":"), "%3F", "?"), "%3D", "="), "%26", "&")
                        tracker.AddPage(strURL)

                        'Set Campaign in session, Will be used when on session start Campaign variable is not found in URL
                        FindCampaignNameFromURL()
                    End If
                End If
            End If
        End If
    End Sub
    Private Sub Page_InitComplete(ByVal sender As Object, ByVal e As System.EventArgs)
        CCommon.CustomizePortalUI(sender)
    End Sub
    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        If Application("IsConfigValid") = False Then
            If Not ValidateWebConfigSettings() Then
                Application("IsConfigValid") = False
                Response.StatusCode = 200
                Response.End()
            Else
                Application("IsConfigValid") = True
            End If
        End If

        If Not IsNothing(Context.Session) Then
            If Not Session("Tracker") Is Nothing Then
                If String.Compare(Right(Request.Url.AbsoluteUri, 5), "Reg=1") = 0 Then
                    Dim CookieEnabled As Boolean
                    CookieEnabled = Request.Browser.Cookies
                    If CookieEnabled = True Then
                        Dim cookie As HttpCookie
                        cookie = New HttpCookie("Register", True)
                        cookie.Expires = Now.AddYears(1)
                        Response.Cookies.Set(cookie)
                    End If
                End If
            End If
        End If
    End Sub



    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        Try
            ' Fires when the session ends
            If Not Session("Tracker") Is Nothing Then
                Dim tracker As SessionTracker
                tracker = Session("Tracker")
                'kick out if there is no session data
                If IsNothing(tracker) Then
                    Exit Sub
                Else
                    Dim ElapsedTime As TimeSpan
                    Dim PreviousTime As Date
                    Dim FirstTime As Date
                    Dim first As Boolean = True
                    Dim lastPage As String
                    Dim dr As DataRow
                    Dim objBusinessClass As New BusinessClass
                    objBusinessClass.UserHostAddress = tracker.SessionUserHostAddress

                    Try
                        objBusinessClass.UserDomain = System.Net.Dns.GetHostEntry(tracker.SessionUserHostAddress).HostName
                    Catch ex As Exception
                        objBusinessClass.UserDomain = "-"
                    End Try

                    objBusinessClass.UserAgent = tracker.SessionUserAgent
                    objBusinessClass.UserBrowser = tracker.Browser.Browser
                    objBusinessClass.UserCrawler = tracker.Browser.Crawler
                    objBusinessClass.URL = tracker.SessionURL
                    objBusinessClass.DomainID = tracker.TrackID
                    objBusinessClass.Referrer = CCommon.ToString(tracker.SessionReferrer)
                    objBusinessClass.NoOfVisits = tracker.VisitCount
                    objBusinessClass.OrgURL = tracker.OriginalURL
                    objBusinessClass.OrgRef = tracker.OriginalReferrer
                    objBusinessClass.IsVisited = tracker.IsVisited
                    objBusinessClass.DivisionID = Session("DivID")
                    'Added new by chintan
                    objBusinessClass.SourceCampaign = Session("Campaign")
                    objBusinessClass.IPNo = objBusinessClass.GetIPNumberFromIPAddress(tracker.SessionUserHostAddress)
                    objBusinessClass.Country = objBusinessClass.GetCountryFromIPaddress(ConfigurationManager.AppSettings("ConnectionString"))

                    If Not IsNothing(tracker.Pages) Then
                        Dim pages As ArrayList = tracker.Pages
                        objBusinessClass.NoofPages = pages.Count
                        Dim pti As SessionTrackerPage
                        Dim dtTable As New DataTable
                        dtTable.Columns.Add("URL")
                        dtTable.Columns.Add("ElapsedTime")
                        dtTable.Columns.Add("Time")
                        For Each pti In pages
                            If first Then
                                FirstTime = pti.Time
                                first = False
                            Else
                                ElapsedTime = pti.Time.Subtract(PreviousTime)
                                dr = dtTable.NewRow
                                dr("URL") = lastPage
                                dr("ElapsedTime") = ElapsedTime.ToString.Substring(0, 8)
                                dr("Time") = pti.Time.ToString(New CultureInfo("en-US")) 'Use MM DD YYYY otherwise it throws overflow error in sql server
                                dtTable.Rows.Add(dr)
                            End If
                            lastPage = pti.PageName
                            PreviousTime = pti.Time
                        Next
                        ElapsedTime = pti.Time.Subtract(PreviousTime)
                        dr = dtTable.NewRow
                        dr("URL") = lastPage
                        dr("ElapsedTime") = ElapsedTime.ToString.Substring(0, 8)
                        dr("Time") = pti.Time.ToString(New CultureInfo("en-US"))
                        dtTable.Rows.Add(dr)
                        dtTable.TableName = "Table"
                        Dim dsNew As New DataSet
                        dsNew.Tables.Add(dtTable.Copy)
                        objBusinessClass.strURL = dsNew.GetXml
                        dsNew.Tables.Remove(dsNew.Tables(0))

                        ElapsedTime = PreviousTime.Subtract(FirstTime)
                        objBusinessClass.TotalTime = ElapsedTime.ToString
                    End If
                    objBusinessClass.ManageTrackingVisitors(ConfigurationManager.AppSettings("ConnectionString"))
                End If
            End If
        Catch ex As Exception
            'Send email notifying about error
            If CCommon.ToLong(ConfigurationManager.AppSettings("SendExceptionMail")) = 1 Then
                Dim objEmail As New Email
                objEmail.SendSystemEmail("Error in Web Tracking:" & ex.Message, ex.StackTrace, "", "noreply@bizautomation.com", ConfigurationManager.AppSettings("SendExceptionMailAddress"), "BizAutomation", "")
            End If
        End Try
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application ends
    End Sub
    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires upon attempting to authenticate the use
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when an error occurs
    End Sub
    Private Sub FindCampaignNameFromURL()
        If Request.Url.ToString.ToLower.Contains("/webtrack.aspx") Then
            If CCommon.ToString(Session("Campaign")).Length = 0 Then
                Dim intIndex As Integer = Request.Url.ToString.ToLower.IndexOf("campaign=")
                If intIndex > 0 Then
                    Dim StrCampaign As String = Request.Url.ToString.ToLower.Substring(intIndex + 9)
                    Session("Campaign") = StrCampaign.Substring(0, IIf(StrCampaign.IndexOf("&") > 0, StrCampaign.IndexOf("&"), StrCampaign.Length))
                End If
            End If
        End If
    End Sub
    Private Function ValidateWebConfigSettings() As Boolean
        If Not ConfigurationSettings.AppSettings("PortalLocation") Is Nothing Then
            If Not Directory.Exists(ConfigurationSettings.AppSettings("PortalLocation")) Then
                Throw New Exception("Invalid configuration in Key: PortalLocation")
                Return False
            End If
        End If
        Return True
    End Function
End Class
