﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item

Public Class frmAssetList
    Inherits BACRMPage
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                FillFilter()
                LoadAssets()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub FillFilter()
        Try
            Dim objCommon As New CCommon
            objCommon.sb_FillComboFromDBwithSel(ddlFilter, 36, Session("DomainID"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub LoadAssets()
        Try
            Dim ds As DataSet
            Dim objItems As New CItems
            Dim SortChar As Char

            If ViewState("SortCharITems") <> "" Then
                SortChar = ViewState("SortCharITems")
            Else : SortChar = "0"
            End If
            With objItems

                If ddlFilter.SelectedItem Is Nothing Then
                    FillFilter()
                End If

                Table1.CssClass = "aspTable"
                .DivisionID = Session("DivId")

                .ItemClassification = ddlFilter.SelectedItem.Value
                .SortCharacter = SortChar
                If txtCurrrentPageItems.Text.Trim <> "" Then
                    .CurrentPage = txtCurrrentPageItems.Text
                Else : .CurrentPage = 1
                    txtCurrrentPageItems.Text = 1
                End If
                If ddlSearch.SelectedItem.Value <> "" And txtSearch.Text <> "" And ddlSearch.SelectedItem.Value <> "vcWarehouse" Then
                    .KeyWord = ddlSearch.SelectedItem.Value & " ilike '%" & txtSearch.Text.Trim & "%'"
                Else
                    .KeyWord = ""
                End If

                .DomainID = Session("DomainID")
                .PageSize = Session("PagingRows")
                .TotalRecords = 0

                If txtSortColumn.Text <> "" Then
                    If txtSortColumn.Text.Split("~").Length = 2 Then
                        .columnName = txtSortColumn.Text.Split("~")(1)
                    Else
                        .columnName = txtSortColumn.Text
                    End If
                Else : .columnName = "vcItemName"
                End If

                If Session("Asc") = 1 Then
                    .columnSortOrder = "Desc"
                Else : .columnSortOrder = "Asc"
                End If

                ds = .getCompanyAssetsSerial

            End With

            If objItems.TotalRecords <> 0 Then
                hideItems.Visible = True
                lblRecordsItems.Text = objItems.TotalRecords
                Dim strTotalPage As String()
                Dim decTotalPage As Decimal
                decTotalPage = lblRecordsItems.Text / Session("PagingRows")
                decTotalPage = Math.Round(decTotalPage, 2)
                strTotalPage = CStr(decTotalPage).Split(".")
                If (lblRecordsItems.Text Mod Session("PagingRows")) = 0 Then
                    lblTotalItems.Text = strTotalPage(0)
                    txtTotalPageItems.Text = strTotalPage(0)
                Else
                    lblTotalItems.Text = strTotalPage(0) + 1
                    txtTotalPageItems.Text = strTotalPage(0) + 1
                End If
                txtTotalRecordsItems.Text = lblRecordsItems.Text
            End If

            If ds.Relations.Count < 1 Then
                ds.Tables(0).TableName = "Asset"
                ds.Tables(1).TableName = "AssetSerial"
                ds.Relations.Add("Asset", ds.Tables(0).Columns("numItemcode"), ds.Tables(1).Columns("numAssetItemId"))
            End If

            gvAssetItem.DataSource = ds
            gvAssetItem.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            LoadAssets()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub txtCurrrentPageItems_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCurrrentPageItems.TextChanged
        Try
            LoadAssets()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFilter.SelectedIndexChanged
        Try
            LoadAssets()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub


    Private Sub lnkLastItems_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLastItems.Click
        Try
            txtCurrrentPageItems.Text = txtTotalPageItems.Text
            LoadAssets()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkPreviousItems_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPreviousItems.Click
        Try
            If txtCurrrentPageItems.Text = 1 Then
                Exit Sub
            Else : txtCurrrentPageItems.Text = txtCurrrentPageItems.Text - 1
            End If
            LoadAssets()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkFirstItems_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFirstItems.Click
        Try
            txtCurrrentPageItems.Text = 1
            LoadAssets()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkNextItems_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNextItems.Click
        Try
            If txtCurrrentPageItems.Text = txtTotalPageItems.Text Then
                Exit Sub
            Else : txtCurrrentPageItems.Text = Val(txtCurrrentPageItems.Text) + 1
            End If
            LoadAssets()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk2Items_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk2Items.Click
        Try
            If Val(txtCurrrentPageItems.Text) + 1 = txtTotalPageItems.Text Or Val(txtCurrrentPageItems.Text) + 1 > txtTotalPageItems.Text Then
                Exit Sub
            Else : txtCurrrentPageItems.Text = Val(txtCurrrentPageItems.Text) + 2
            End If
            LoadAssets()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk3Items_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk3Items.Click
        Try
            If Val(txtCurrrentPageItems.Text) + 2 = txtTotalPageItems.Text Or Val(txtCurrrentPageItems.Text) + 2 > txtTotalPageItems.Text Then
                Exit Sub
            Else : txtCurrrentPageItems.Text = Val(txtCurrrentPageItems.Text) + 3
            End If
            LoadAssets()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk4Items_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk4Items.Click
        Try
            If Val(txtCurrrentPageItems.Text) + 3 = txtTotalPageItems.Text Or Val(txtCurrrentPageItems.Text + 3) > txtTotalPageItems.Text Then
                Exit Sub
            Else : txtCurrrentPageItems.Text = Val(txtCurrrentPageItems.Text) + 4
            End If
            LoadAssets()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk5Items_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk5Items.Click
        Try
            If Val(txtCurrrentPageItems.Text) + 4 = txtTotalPageItems.Text Or Val(txtCurrrentPageItems.Text + 4) > txtTotalPageItems.Text Then
                Exit Sub
            Else : txtCurrrentPageItems.Text = Val(txtCurrrentPageItems.Text) + 5
            End If
            LoadAssets()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    'Private Sub uwItem_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwItem.InitializeRow
    '    Try

    '        If e.Row.HasParent = False Then
    '            e.Row.Cells(8).Value = FormatCurrency(e.Row.Cells(8).Value, 2)
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Function ReturnDate(ByVal CloseDate) As String
        Try
            If CloseDate Is Nothing Then Exit Function
            Dim strTargetResolveDate As String = ""
            Dim temp As String = ""
            If Not IsDBNull(CloseDate) Then
                strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
                If strTargetResolveDate = "01/01/1900" Then Return ""
                ' check Today date components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                ' if both are same it is today
                Dim strNow As Date
                strNow = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                If (CloseDate.Date = strNow.Date And CloseDate.Month = strNow.Month And CloseDate.Year = strNow.Year) Then
                    strTargetResolveDate = "<font color=red><b>Today</b></font>"
                    Return strTargetResolveDate

                    ' check [ TodayDate + 1 ] Date.... components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                    ' if both are same it was Yesterday
                ElseIf (CloseDate.Date.AddDays(1).Date = strNow.Date And CloseDate.AddDays(1).Month = strNow.Month And CloseDate.AddDays(1).Year = strNow.Year) Then
                    strTargetResolveDate = "<font color=purple><b>Yesterday</b></font>"
                    Return strTargetResolveDate

                    ' check TodayDate .... components [ Date , Month , Year ] with Parameter [ CloseDate + 1 ] Date....  components [ Date , Month , Year ] 
                    ' if both are same it will Tomorrow
                ElseIf (CloseDate.Date = strNow.AddDays(1).Date And CloseDate.Month = strNow.AddDays(1).Month And CloseDate.Year = strNow.AddDays(1).Year) Then
                    temp = CloseDate.Hour.ToString + ":" + CloseDate.Minute.ToString
                    strTargetResolveDate = "<font color=orange><b>Tomorrow</b></font>"
                    Return strTargetResolveDate
                Else
                    strTargetResolveDate = strTargetResolveDate
                    Return strTargetResolveDate
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class