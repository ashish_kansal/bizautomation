﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAddReturn.aspx.vb"
    Inherits="BACRMPortal.frmAddReturn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <script type="text/javascript">
        function Close() {
            window.opener.location.reload(true);
            window.close()
            return false;
        }
        function SelectAll(a) {
            var str;

            if (typeof (a) == 'string') {

                a = document.getElementById(a);

            }
            var gvReturns = document.getElementById('gvReturns');
            if (a.checked == true) {
                for (var i = 1; i <= gvReturns.rows.length; i++) {
                    if (i < 10) {
                        str = '0' + i
                    }
                    else {
                        str = i
                    }
                    document.getElementById('gvReturns_ctl' + str + '_chk').checked = true;
                    if (document.getElementById('gvReturns_ctl' + str + '_rfvReason') != null)
                        ValidatorEnable(document.getElementById('gvReturns_ctl' + str + '_rfvReason'), true);
                }
            }
            else if (a.checked == false) {
                for (var i = 1; i <= gvReturns.rows.length; i++) {
                    if (i < 10) {
                        str = '0' + i
                    }
                    else {
                        str = i
                    }

                    document.getElementById('gvReturns_ctl' + str + '_chk').checked = false;
                    if (document.getElementById('gvReturns_ctl' + str + '_rfvReason') != null)
                        ValidatorEnable(document.getElementById('gvReturns_ctl' + str + '_rfvReason'), false);
                }
            }
        }
        function validateReq(a, b) {
            if (document.getElementById(a).checked == true)
                ValidatorEnable(document.getElementById(b), true);
            else
                ValidatorEnable(document.getElementById(b), false);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <br />
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td class="normal1">
                <asp:ValidationSummary ID="valSummary" runat="server" DisplayMode="BulletList" ShowMessageBox="true"
                    ShowSummary="false" />
            </td>
        </tr>
        <tr valign="bottom">
            <td>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="TabLeft">
                            Return Items
                        </td>
                        <td class="TabRight">
                        </td>
                    </tr>
                </table>
            </td>
            <td align="right" class="normal1">
                Select a sales order:
            </td>
            <td align="left">
                <asp:DropDownList runat="server" ID="ddlOrder" CssClass="signup" AutoPostBack="true">
                </asp:DropDownList>
            </td>
            <td align="right">
                <asp:Button ID="btnAddtoReturnQueue" runat="server" CssClass="button" Text="Submit selected items for return & close" />&nbsp;
            </td>
        </tr>
        <tr valign="top">
            <td colspan="4">
                <asp:Table ID="table3" Width="100%" runat="server" Height="100" GridLines="None"
                    BorderColor="black" CssClass="aspTable" BorderWidth="1" CellSpacing="0" CellPadding="0">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:GridView ID="gvReturns" runat="server" AutoGenerateColumns="false" CssClass="dg"
                                Width="100%" DataKeyNames="numItemCode,monPrice,numoppitemtCode">
                                <AlternatingRowStyle CssClass="ais" />
                                <RowStyle CssClass="is" />
                                <HeaderStyle CssClass="hs" />
                                <Columns>
                                    <asp:BoundField HeaderText="ItemCode" DataField="numItemCode" Visible="false" />
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:CheckBox Text="" runat="server" ID="chk" onclick="SelectAll('gvReturns_ctl01_chk');" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox Text="" runat="server" ID="chk" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Item to return" DataField="vcItemName" />
                                    <asp:BoundField HeaderText="Item Description" DataField="vcItemDesc" />
                                    <%--<asp:BoundField HeaderText="Serial #" DataField="Serial" />--%>
                                    <asp:BoundField HeaderText="Total Units" DataField="numUnitHour" ItemStyle-HorizontalAlign="Center"
                                        DataFormatString="{0:0}" />
                                    <asp:TemplateField HeaderText="Qty to Return" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" ID="txtQtyReturnCmp" Text='<%# String.Format("{0:0}",Eval("numUnitHour")) %>'
                                                Style="display: none" AUTOCOMPLETE=OFF></asp:TextBox>
                                            <asp:TextBox runat="server" ID="txtQtyReturn" Text='<%# String.Format("{0:0}",Eval("numUnitHour")) %>'
                                                Width="50px" CssClass="signup" AUTOCOMPLETE=OFF></asp:TextBox>
                                            <asp:CompareValidator Operator="LessThanEqual" ControlToValidate="txtQtyReturn" ControlToCompare="txtQtyReturnCmp"
                                                Type="Integer" Display="Dynamic" ID="cvQty" runat="server" ErrorMessage="Qty to Return can not be greater that Total Units"
                                                Text="*"></asp:CompareValidator>
                                            <asp:RequiredFieldValidator ID="rfvQty" runat="server" ErrorMessage="Enter Quantity to Return"
                                                Display="Dynamic" Text="*" ControlToValidate="txtQtyReturn"></asp:RequiredFieldValidator>
                                            <asp:CompareValidator ID="cvQty1" runat="server" ErrorMessage="Quantity to Return is numeic(1-9)"
                                                Operator="DataTypeCheck" Display="Dynamic" Text="*" ControlToValidate="txtQtyReturn"
                                                Type="Integer"></asp:CompareValidator>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Reason for Return">
                                        <ItemTemplate>
                                            <asp:DropDownList runat="server" ID="ddlReason" CssClass="signup">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="rfvReason" runat="server" ErrorMessage="Select Reason for Return"
                                                Enabled="false" Display="Dynamic" Text="*" ControlToValidate="ddlReason" InitialValue="0"></asp:RequiredFieldValidator>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:DropDownList runat="server" ID="ddlStatus" Enabled="false" CssClass="signup">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
