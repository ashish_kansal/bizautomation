﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAssetList.aspx.vb"
    Inherits="BACRMPortal.frmAssetList" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" id="sm1"/>
    <asp:Table ID="Table1" Width="100%" runat="server" BorderWidth="1" Height="350" GridLines="None"
        BorderColor="black" CellSpacing="0" CellPadding="0" CssClass="aspTable">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <table align="right" width="100%">
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td class="normal1" width="150">
                            No of Records:
                            <asp:Label ID="lblRecordsItems" runat="server"></asp:Label><asp:Label ID="lblRecordsKit"
                                runat="server"></asp:Label>
                        </td>
                        <td class="normal1" align="right">
                            Search For
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="signup" Width="130"></asp:TextBox>
                            <asp:DropDownList ID="ddlSearch" runat="server" CssClass="signup">
                                <asp:ListItem Text="-Select One--" Value=""></asp:ListItem>
                                <asp:ListItem Text="Item" Value="vcItemName"></asp:ListItem>
                                <%--<asp:ListItem Text="Warehouse" Value="vcWarehouse"></asp:ListItem>--%>
                                <asp:ListItem Text="Serial No" Value="vcSKU"></asp:ListItem>
                                <asp:ListItem Text="ModelID" Value="vcModelID"></asp:ListItem>
                                <asp:ListItem Text="Vendor" Value="vcCompanyName"></asp:ListItem>
                                <asp:ListItem Text="Description" Value="txtItemDesc"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:Button ID="btnGo" CssClass="button" Width="25" Text="Go" runat="server"></asp:Button>
                        </td>
                    </tr>
                </table>
                <br>
                <br>
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td valign="bottom">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="TabLeft">
                                        Assets
                                    </td>
                                    <td class="TabRight">
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="normal1" align="left">
                            Filter&nbsp;&nbsp;
                            <asp:DropDownList ID="ddlFilter" runat="server" Width="150" AutoPostBack="True" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                        <td id="hideItems" nowrap align="right" runat="server">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblNextItems" runat="server" CssClass="Text_bold">Next:</asp:Label>
                                    </td>
                                    <td class="normal1">
                                        <asp:LinkButton ID="lnk2Items" runat="server">2</asp:LinkButton>
                                    </td>
                                    <td class="normal1">
                                        <asp:LinkButton ID="lnk3Items" runat="server">3</asp:LinkButton>
                                    </td>
                                    <td class="normal1">
                                        <asp:LinkButton ID="lnk4Items" runat="server">4</asp:LinkButton>
                                    </td>
                                    <td class="normal1">
                                        <asp:LinkButton ID="lnk5Items" runat="server">5</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lnkFirstItems" runat="server">
										<div class="LinkArrow"><<</div>
                                        </asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lnkPreviousItems" runat="server">
										<div class="LinkArrow"><</div>
                                        </asp:LinkButton>
                                    </td>
                                    <td class="normal1">
                                        <asp:Label ID="lblPageItems" runat="server">Page</asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCurrrentPageItems" runat="server" CssClass="signup" Width="28px"
                                            AutoPostBack="True" MaxLength="5"></asp:TextBox>
                                    </td>
                                    <td class="normal1">
                                        <asp:Label ID="lblOfItems" runat="server">of</asp:Label>
                                    </td>
                                    <td class="normal1">
                                        <asp:Label ID="lblTotalItems" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lnkNextItems" runat="server" CssClass="LinkArrow">
										<div class="LinkArrow">></div>
                                        </asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lnkLastItems" runat="server">
										<div class="LinkArrow">>></div>
                                        </asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table cellspacing="1" cellpadding="1" width="100%" border="0">
                    <tr>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="A1" href="javascript:fnSortByCharItem('a')">
                                <div class="A2Z">
                                    A</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="b" href="javascript:fnSortByCharItem('b')">
                                <div class="A2Z">
                                    B</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="c" href="javascript:fnSortByCharItem('c')">
                                <div class="A2Z">
                                    C</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="d" href="javascript:fnSortByCharItem('d')">
                                <div class="A2Z">
                                    D</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="e" href="javascript:fnSortByCharItem('e')">
                                <div class="A2Z">
                                    E</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="f" href="javascript:fnSortByCharItem('f')">
                                <div class="A2Z">
                                    F</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="g" href="javascript:fnSortByCharItem('g')">
                                <div class="A2Z">
                                    G</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="h" href="javascript:fnSortByCharItem('h')">
                                <div class="A2Z">
                                    H</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="I" href="javascript:fnSortByCharItem('i')">
                                <div class="A2Z">
                                    I</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="j" href="javascript:fnSortByCharItem('j')">
                                <div class="A2Z">
                                    J</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="k" href="javascript:fnSortByCharItem('k')">
                                <div class="A2Z">
                                    K</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="l" href="javascript:fnSortByCharItem('l')">
                                <div class="A2Z">
                                    L</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="m" href="javascript:fnSortByCharItem('m')">
                                <div class="A2Z">
                                    M</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="n" href="javascript:fnSortByCharItem('n')">
                                <div class="A2Z">
                                    N</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="o" href="javascript:fnSortByCharItem('o')">
                                <div class="A2Z">
                                    O</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="p" href="javascript:fnSortByCharItem('p')">
                                <div class="A2Z">
                                    P</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="q" href="javascript:fnSortByCharItem('q')">
                                <div class="A2Z">
                                    Q</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="r" href="javascript:fnSortByCharItem('r')">
                                <div class="A2Z">
                                    R</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="s" href="javascript:fnSortByCharItem('s')">
                                <div class="A2Z">
                                    S</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="t" href="javascript:fnSortByCharItem('t')">
                                <div class="A2Z">
                                    T</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="u" href="javascript:fnSortByCharItem('u')">
                                <div class="A2Z">
                                    U</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="v" href="javascript:fnSortByCharItem('v')">
                                <div class="A2Z">
                                    V</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="w" href="javascript:fnSortByCharItem('w')">
                                <div class="A2Z">
                                    W</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="x" href="javascript:fnSortByCharItem('x')">
                                <div class="A2Z">
                                    X</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="y" href="javascript:fnSortByCharItem('y')">
                                <div class="A2Z">
                                    Y</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="z" href="javascript:fnSortByCharItem('z')">
                                <div class="A2Z">
                                    Z</div>
                            </a>
                        </td>
                        <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                            bgcolor="#52658c">
                            <a id="all" href="javascript:fnSortByCharItem('0')">
                                <div class="A2Z">
                                    All</div>
                            </a>
                        </td>
                    </tr>
                </table>
                <telerik:RadGrid ID="gvAssetItem" runat="server" Width="100%" AutoGenerateColumns="False"
                    GridLines="None" ShowFooter="false" Skin="windows" EnableEmbeddedSkins="false"
                    CssClass="dg" AlternatingItemStyle-CssClass="ais" ItemStyle-CssClass="is" HeaderStyle-CssClass="hs">
                    <MasterTableView DataKeyNames="numItemcode" HierarchyLoadMode="Client" DataMember="Asset">
                        <DetailTables>
                            <telerik:GridTableView Width="100%" runat="server" ItemStyle-HorizontalAlign="Center"
                                AlternatingItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                ShowFooter="true" DataKeyNames="numAssetItemId" DataMember="AssetSerial" CssClass="dg"
                                AlternatingItemStyle-CssClass="ais" ItemStyle-CssClass="is" HeaderStyle-CssClass="hs">
                                <ParentTableRelation>
                                    <telerik:GridRelationFields DetailKeyField="numAssetItemId" MasterKeyField="numItemcode" />
                                </ParentTableRelation>
                                <Columns>
                                    <telerik:GridBoundColumn HeaderText="Serial No" ItemStyle-Width="10%" DataField="vcSerialNo">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="Model Id" ItemStyle-Width="10%" DataField="vcModelId">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="BarCode Id" ItemStyle-Width="10%" DataField="vcBarCodeId">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn HeaderText="Purchase Date" ItemStyle-Width="10%">
                                        <ItemTemplate>
                                            <%#ReturnDate(Eval("dtPurchase"))%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Warrante Expiration" ItemStyle-Width="10%">
                                        <ItemTemplate>
                                            <%#ReturnDate(Eval("dtWarrante"))%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn HeaderText="Location" ItemStyle-Width="10%" DataField="vcLocation">
                                    </telerik:GridBoundColumn>
                                </Columns>
                            </telerik:GridTableView>
                        </DetailTables>
                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="Image" ItemStyle-Width="15%">
                                <ItemTemplate>
                                    <%#CCommon.GetImageHTML(Eval("vcPathForTImage"), 1)%>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Item Name" ItemStyle-Width="25%">
                                <ItemTemplate>
                                    <%#Eval("vcitemName")%>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn HeaderText="Model Id" ItemStyle-Width="15%" DataField="vcModelId">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Description" ItemStyle-Width="25%" DataField="txtItemDesc">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Serial No" ItemStyle-Width="10%" DataField="vcSKU">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Vendor" ItemStyle-Width="10%" DataField="Vendor">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="UPC" ItemStyle-Width="10%" DataField="numBarCodeId">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Cost" ItemStyle-Width="10%" DataField="Cost">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Department" ItemStyle-Width="10%" DataField="Department">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn HeaderText="Purchase Date" ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <%#ReturnDate(Eval("dtPurchase"))%>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Warrante Expiration" ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <%#ReturnDate(Eval("dtWarrentyTill"))%>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings AllowExpandCollapse="true" />
                </telerik:RadGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
    <asp:TextBox ID="txtDelItemIds" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSaveAPIItemIds" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalPageItems" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecordsItems" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtSortCharItems" Style="display: none" runat="server"></asp:TextBox>
    </form>
</body>
</html>
