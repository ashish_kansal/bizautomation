﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Projects
Public Class frmAddReturn
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                loadClosedOrders()
                Session("Help") = Request.Url.Segments(Request.Url.Segments.Length - 1)
            End If
            lblMessage.Text = ""
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub loadClosedOrders()
        Try
            Dim objProjectList As New Project
            objProjectList.DomainID = Session("DomainId")
            objProjectList.DivisionID = Session("DivId")
            objProjectList.bytemode = 4 'Select only closed sales orders
            ddlOrder.DataSource = objProjectList.GetOpportunities()
            ddlOrder.DataTextField = "vcPOppName"
            ddlOrder.DataValueField = "numOppId"
            ddlOrder.DataBind()
            ddlOrder.Items.Insert(0, "--Select One--")
            ddlOrder.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub BindGrid()
        Try
            Dim dtReturns As DataTable
            Dim objOpportunity As New MOpportunity
            objOpportunity.DomainID = Session("DomainID")
            objOpportunity.Mode = 1
            objOpportunity.OpportunityId = ddlOrder.SelectedValue
            dtReturns = objOpportunity.GetOrderItems().Tables(0)
            gvReturns.DataSource = dtReturns
            gvReturns.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub gvReturns_OnRowDataBound(ByVal o As Object, ByVal e As GridViewRowEventArgs) Handles gvReturns.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim rowView As DataRowView = CType(e.Row.DataItem, DataRowView)
                Dim ddlReason As DropDownList = CType(e.Row.FindControl("ddlReason"), DropDownList)
                Dim objCommon As New CCommon
                objCommon.sb_FillComboFromDBwithSel(ddlReason, 48, Session("DomainID"))
                Dim ddlStatus As DropDownList = CType(e.Row.FindControl("ddlStatus"), DropDownList)
                objCommon.sb_FillComboFromDBwithSel(ddlStatus, 49, Session("DomainID"))
                ddlStatus.SelectedIndex = 1
                Dim rfvReason As RequiredFieldValidator = CType(e.Row.FindControl("rfvReason"), RequiredFieldValidator)
                Dim chk As CheckBox = CType(e.Row.FindControl("chk"), CheckBox)
                chk.Attributes.Add("onchange", "validateReq('" & chk.ClientID & "','" & rfvReason.ClientID & "') ")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub btnAddtoReturnQueue_OnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddtoReturnQueue.Click
        Try
            Dim boolFlag As Boolean = True
            Dim objOpportunity As New MOpportunity
            For Each gvr As GridViewRow In gvReturns.Rows
                If CType(gvr.FindControl("chk"), CheckBox).Checked = True Then
                    If CInt(CType(gvr.FindControl("txtQtyReturn"), TextBox).Text.Trim) > 0 Then
                        With objOpportunity
                            .OpportunityId = ddlOrder.SelectedValue
                            .OppItemCode = gvReturns.DataKeys(gvr.DataItemIndex)(2).ToString()
                            .ItemCode = gvReturns.DataKeys(gvr.DataItemIndex)(0).ToString()
                            .Units = gvr.Cells(4).Text.Trim()
                            .UnitsReturned = CType(gvr.FindControl("txtQtyReturn"), TextBox).Text.Trim
                            .UnitPrice = gvReturns.DataKeys(gvr.DataItemIndex)(1).ToString()
                            .ReasonForReturn = CType(gvr.FindControl("ddlReason"), DropDownList).SelectedValue
                            .ReturnStatus = CType(gvr.FindControl("ddlStatus"), DropDownList).SelectedValue
                            .UserID = Session("UserContactID")
                        End With
                        If Not objOpportunity.InsertReturns() = True Then
                            boolFlag = False
                        End If
                    End If
                End If
            Next
            If boolFlag Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "close", "<script>Close();</script>")
            End If
        Catch ex As Exception
            If ex.Message.StartsWith("NOT_ALLOWED") = True Then
                lblMessage.Text = "Total Qty to Return should be less than Total Units"
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End If
        End Try
    End Sub

    Private Sub ddlOrder_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOrder.SelectedIndexChanged
        Try
            BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class