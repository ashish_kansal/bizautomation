Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Common

Partial Class frmSoution : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Dim lngSolId As Long
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Session("UserContactID") = Nothing Then Response.Redirect("../Common/frmLogout.aspx")
            lngSolId = GetQueryStringVal( "SolId")
            If Not IsPostBack Then

                btnSave.Visible = False
                btnSaveClose.Visible = False
                btnlnkCase.Visible = False
                LoadDropDowns()
                LoadInformation()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadInformation()
        Try
            Dim objSolution As New Solution
            Dim dtSolDetails As DataTable
            objSolution.SolID = lngSolId
            dtSolDetails = objSolution.GetSolutionForEdit
            txtSolution.Text = dtSolDetails.Rows(0).Item("vcSolnTitle")
            If Not ddlCategory.Items.FindByValue(dtSolDetails.Rows(0).Item("numCategory")) Is Nothing Then
                ddlCategory.Items.FindByValue(dtSolDetails.Rows(0).Item("numCategory")).Selected = True
            End If
            txtSolDesc.Text = dtSolDetails.Rows(0).Item("txtSolution")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadDropDowns()
        Try
            Dim i As Integer
            Dim ObjCases As New CCases
            Dim dtCaseNo As DataTable
            dtCaseNo = ObjCases.GetOpenCases
            For i = 0 To dtCaseNo.Rows.Count - 1
                ddlCaseNo.Items.Add(New ListItem(dtCaseNo.Rows(i).Item("vcCaseNumber"), dtCaseNo.Rows(i).Item("numCaseId")))
            Next
            ddlCaseNo.Items.Insert(0, "--Select One--")
            ddlCaseNo.Items.FindByText("--Select One--").Value = 0
            Dim objCommon As New CCommon
            objCommon.sb_FillComboFromDBwithSel(ddlCategory, 34, Session("DomainID"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnlnkCase_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnlnkCase.Click
        Try
            Dim str As String()
            str = CStr(ddlCaseNo.SelectedItem.Value).Split("~")
            Dim objSolution As New Solution
            objSolution.CaseID = str(0)
            objSolution.SolID = lngSolId
            objSolution.LinkSolToCases()
            Response.Redirect("../cases/frmCases.aspx?frm=Solution&frm=cases&CaseID=" & str(0) & "&DivID=" & str(1) & "&CntID=" & str(2) & "&SolId=" & lngSolId & "&Index=1", False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub Save()
        Try
            Dim objSolution As New Solution
            objSolution.Category = ddlCategory.SelectedItem.Value
            objSolution.SolName = txtSolution.Text
            objSolution.SolDesc = txtSolDesc.Text
            objSolution.SolID = lngSolId
            objSolution.DomainID = Session("DomainID")
            objSolution.SaveSolution()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Save()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            Save()
            Response.Redirect("../KnowledgeBase/frmKnowledgeList.aspx", False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            If GetQueryStringVal( "frm") = "frmCases" Then
                Session("CaseID") = GetQueryStringVal( "CaseId")
                Response.Redirect("../pagelayout/frmCustCasedtl.aspx?frm=CaseList", False)
            Else
                Response.Redirect("../KnowledgeBase/frmKnowledgeList.aspx", False)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class