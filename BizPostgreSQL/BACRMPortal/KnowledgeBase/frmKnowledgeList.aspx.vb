'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Common

Partial Class frmKnowledgeList : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Dim strColumn As String
    
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Dim objCommon As New CCommon
            m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmKnowledgeList.aspx", Session("UserContactID"), 15, 9)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../Common/frmAuthorization.aspx")
            If Not IsPostBack Then
                Session("Asc") = 1
                objCommon.sb_FillComboFromDBwithSel(ddlCategory, 34, Session("DomainID"))
                BindDatagrid()
            End If
            If txtSortChar.Text <> "" Then
                ViewState("SortChar") = txtSortChar.Text
                ViewState("Column") = "vcSolnTitle"
                Session("Asc") = 0
                BindDatagrid()
                txtSortChar.Text = ""
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindDatagrid()
        Try
            Dim SortChar As Char
            If ViewState("SortChar") <> "" Then
                SortChar = ViewState("SortChar")
            Else : SortChar = "0"
            End If
            Dim dtSolution As DataTable
            Dim objSolution As New Solution
            objSolution.Category = ddlCategory.SelectedItem.Value
            objSolution.KeyWord = txtKeyWord.Text
            objSolution.SortCharacter = SortChar
            If ViewState("Column") <> "" Then objSolution.columnName = ViewState("Column")
            If Session("Asc") = 1 Then
                objSolution.columnSortOrder = "Desc"
            Else : objSolution.columnSortOrder = "Asc"
            End If
            objSolution.DomainID = Session("DomainID")
            dtSolution = objSolution.GetKnowledgeBases
            lblRecordSolution.Text = String.Format("{0:#,###}", objSolution.TotalRecords)
            dgBases.DataSource = dtSolution
            dgBases.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgBases_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgBases.SortCommand
        Try
            strColumn = e.SortExpression.ToString()
            If ViewState("Column") <> strColumn Then
                ViewState("Column") = strColumn
                Session("Asc") = 0
            Else
                If Session("Asc") = 0 Then
                    Session("Asc") = 1
                Else : Session("Asc") = 0
                End If
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Function ReturnName(ByVal CreatedDate As Date) As String
        Try
            Dim strCreateDate As String
            strCreateDate = FormattedDateFromDate(CreatedDate, Session("DateFormat"))
            If Format(CreatedDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                strCreateDate = "<font color=red>Today</font>"
            ElseIf Format(CreatedDate, "yyyyMMdd") < Format(Now(), "yyyyMMdd") Then
                strCreateDate = "<font color=red>" & strCreateDate & "</font>"
            End If
            Return strCreateDate
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub dgBases_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgBases.ItemCommand
        Try
            If e.CommandName = "Solution" Then
                Dim lngSolId As Long = e.Item.Cells(0).Text
                Response.Redirect("../KnowledgeBase/frmSoution.aspx?SolId=" & lngSolId, False)
            End If
            If e.CommandName = "Delete" Then
                Dim lngSolId As Long = e.Item.Cells(0).Text
                Dim objSolution As New Solution
                objSolution.SolID = lngSolId
                If objSolution.DeleteSolution = False Then
                    litMessage.Text = "Dependent Records Exists.Cannot be deleted."
                Else
                    BindDatagrid()
                    litMessage.Text = ""
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
        Try
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnGoBases_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGoBases.Click
        Try
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class