<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../common/topbar.ascx" %>
<%--<%@ Register TagPrefix="ie" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls" %>--%>

<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmKnowledgeList.aspx.vb"
    Inherits="BACRMPortal.frmKnowledgeList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <title>Knowledge Base</title>
    <script language="javascript">
        function fnSortByChar(varSortChar) {
            document.Form1.txtSortChar.value = varSortChar;
            if (document.Form1.txtCurrrentPage != null) {
                document.Form1.txtCurrrentPage.value = 1;
            }
            document.Form1.btnGoBases.click();
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
    </script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <menu1:webmenu ID="webmenu1" runat="server"></menu1:webmenu>
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
        EnableViewState="true">
        <ContentTemplate>
            <table align="right">
                <tr>
                    <td class="normal1" nowrap align="right">
                        &nbsp;Find Solution using Name or Keyword
                    </td>
                    <td>
                        &nbsp;
                        <asp:TextBox ID="txtKeyWord" runat="server" Width="200" CssClass="signup"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="btnGoBases" runat="server" CssClass="button" Text="Go" Width="25">
                        </asp:Button>
                    </td>
                </tr>
            </table>
            <br>
            <br>
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td valign="bottom">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="TabLeft">
                                    Knowledge Base
                                </td>
                                <td class="TabRight">
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="normal1" width="150">
                        No of Records:
                        <asp:Label ID="lblRecordSolution" runat="server"></asp:Label>
                    </td>
                    <td class="normal1" nowrap align="right">
                        &nbsp;Solution Category
                    </td>
                    <td>
                        &nbsp;
                        <asp:DropDownList ID="ddlCategory" runat="server" Width="130" CssClass="signup" AutoPostBack="True">
                        </asp:DropDownList>
                        &nbsp;
                    </td>
                </tr>
            </table>
            <table cellspacing="1" cellpadding="1" width="100%" border="0">
                <tr valign="top">
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="a" href="javascript:fnSortByChar('a')">
                            <div class="A2Z">
                                A</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="b" href="javascript:fnSortByChar('b')">
                            <div class="A2Z">
                                B</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="c" href="javascript:fnSortByChar('c')">
                            <div class="A2Z">
                                C</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="d" href="javascript:fnSortByChar('d')">
                            <div class="A2Z">
                                D</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="e" href="javascript:fnSortByChar('e')">
                            <div class="A2Z">
                                E</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="f" href="javascript:fnSortByChar('f')">
                            <div class="A2Z">
                                F</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="g" href="javascript:fnSortByChar('g')">
                            <div class="A2Z">
                                G</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="h" href="javascript:fnSortByChar('h')">
                            <div class="A2Z">
                                H</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="I" href="javascript:fnSortByChar('i')">
                            <div class="A2Z">
                                I</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="j" href="javascript:fnSortByChar('j')">
                            <div class="A2Z">
                                J</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="k" href="javascript:fnSortByChar('k')">
                            <div class="A2Z">
                                K</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="l" href="javascript:fnSortByChar('l')">
                            <div class="A2Z">
                                L</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="m" href="javascript:fnSortByChar('m')">
                            <div class="A2Z">
                                M</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="n" href="javascript:fnSortByChar('n')">
                            <div class="A2Z">
                                N</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="o" href="javascript:fnSortByChar('o')">
                            <div class="A2Z">
                                O</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="p" href="javascript:fnSortByChar('p')">
                            <div class="A2Z">
                                P</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="q" href="javascript:fnSortByChar('q')">
                            <div class="A2Z">
                                Q</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="r" href="javascript:fnSortByChar('r')">
                            <div class="A2Z">
                                R</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="s" href="javascript:fnSortByChar('s')">
                            <div class="A2Z">
                                S</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="t" href="javascript:fnSortByChar('t')">
                            <div class="A2Z">
                                T</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="u" href="javascript:fnSortByChar('u')">
                            <div class="A2Z">
                                U</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="v" href="javascript:fnSortByChar('v')">
                            <div class="A2Z">
                                V</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="w" href="javascript:fnSortByChar('w')">
                            <div class="A2Z">
                                W</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="x" href="javascript:fnSortByChar('x')">
                            <div class="A2Z">
                                X</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="y" href="javascript:fnSortByChar('y')">
                            <div class="A2Z">
                                Y</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="z" href="javascript:fnSortByChar('z')">
                            <div class="A2Z">
                                Z</div>
                        </a>
                    </td>
                    <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                        bgcolor="#52658c">
                        <a id="all" href="javascript:fnSortByChar('0')">
                            <div class="A2Z">
                                All</div>
                        </a>
                    </td>
                </tr>
            </table>
            <asp:Table ID="table1" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
                Width="100%" BorderColor="black" GridLines="None" Height="350">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Top">
                        <asp:DataGrid ID="dgBases" runat="server" Width="100%" CssClass="dg" AllowSorting="True"
                            AutoGenerateColumns="False" BorderColor="white">
                            <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                            <ItemStyle CssClass="is"></ItemStyle>
                            <HeaderStyle CssClass="hs"></HeaderStyle>
                            <Columns>
                                <asp:BoundColumn Visible="False" DataField="numSolnID"></asp:BoundColumn>
                                <asp:ButtonColumn DataTextField="vcSolnTitle" SortExpression="vcSolnTitle" HeaderText="<font color=white>Solution Name</font>"
                                    CommandName="Solution"></asp:ButtonColumn>
                                <asp:BoundColumn DataField="txtSolution" SortExpression="txtSolution" HeaderText="<font color=white>Solution Description</font>">
                                </asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
            <table width="100%">
                <tr>
                    <td class="normal4" align="center">
                        <asp:Literal ID="litMessage" runat="server"></asp:Literal>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" runat="server" Style="display: none"></asp:TextBox></form>
</body>
</html>
