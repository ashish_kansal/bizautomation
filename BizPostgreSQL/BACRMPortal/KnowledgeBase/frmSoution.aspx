<%@ Register TagPrefix="menu1" TagName="Menu" Src="../common/topbar.ascx" %>

<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmSoution.aspx.vb"
    Inherits="BACRMPortal.frmSoution" %>

<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <title>Knowledge Base</title>
</head>
<body style="margin: 0 10px 0 10px;">
    <form id="Form1" method="post" runat="server">
    <menu1:Menu ID="webmenu1" runat="server"></menu1:Menu>
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
        EnableViewState="true">
        <ContentTemplate>
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td valign="bottom">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="TabLeft">
                                    Solution Details
                                </td>
                                <td class="TabRight">
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td align="right">
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button"></asp:Button>
                        <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Save &amp; Close">
                        </asp:Button>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="button"></asp:Button>
                    </td>
                </tr>
            </table>
            <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
                Width="100%" BorderColor="black" GridLines="None" CssClass="aspTable">
                <asp:TableRow>
                    <asp:TableCell>
                        <br>
                        <br>
                        <table border="0" width="100%">
                            <tr style="display: none">
                                <td class="normal1" align="right">
                                    Case No<font color="red">*</font>
                                    <td>
                                        <asp:DropDownList ID="ddlCaseNo" runat="server" Width="130" CssClass="signup">
                                        </asp:DropDownList>
                                        &nbsp;
                                        <asp:Button ID="btnlnkCase" CssClass="button" runat="server" Text="Link Solution to an Open Case">
                                        </asp:Button>
                                    </td>
                            </tr>
                            <tr>
                                <td class="normal1" align="right">
                                    Solution Category<font color="red">*</font>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlCategory" runat="server" Width="130" CssClass="signup">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="normal1" align="right">
                                    Soluton Name<font color="red">*</font>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSolution" CssClass="signup" runat="server" Width="400"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="normal1" align="right">
                                    Solution Description<font color="red">*</font>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSolDesc" CssClass="signup" runat="server" TextMode="MultiLine"
                                        Width="400" Height="100"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
