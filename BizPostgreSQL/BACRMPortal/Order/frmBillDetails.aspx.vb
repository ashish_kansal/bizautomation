Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Partial Class frmBillDetails
    Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnCancel As System.Web.UI.WebControls.Button
    Dim lngDivID As Long
    Protected WithEvents txtAmount As System.Web.UI.WebControls.TextBox
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not IsPostBack Then
                Dim objItems As New CItems
                objItems.OppId = 0
                objItems.DivisionID = Session("DivId")
                objItems.ContactID = Session("UserContactID")

                If Not Session("Data") Is Nothing Then
                    Dim dttable As DataTable
                    Dim ds As DataSet
                    ds = Session("Data")
                    dttable = ds.Tables(0)
                    For Each dr As DataRow In dttable.Rows
                        objItems.ItemCode = dr("numItemCode")
                        objItems.NoofUnits = dr("numUnitHour")
                        dr("monPrice") = String.Format("{0:#,##0.00}", objItems.GetItemPriceafterdiscount())
                        dr("monTotAmount") = String.Format("{0:#,##0.00}", IIf(IsDBNull(dr("numUnitHour")), 0, dr("numUnitHour")) * IIf(IsDBNull(dr("monPrice")), 0, dr("monPrice")))
                    Next
                    ds.AcceptChanges()
                    Session("Data") = ds
                    objItems.str = ds.GetXml
                    dgBasket.DataSource = dttable
                    dgBasket.DataBind()
                End If
                objItems.DomainID = Session("DomainID")
                objItems.OppName = Format(Now(), "MMMM")
                objItems.CreateOrderForECommerce()
                Session("OppID") = objItems.OppId

                Dim objOpportunities As New MOpportunity
                Dim dtItems As DataTable
                objOpportunities.OpportunityId = Session("OppID")
                objOpportunities.DomainID = Session("DomainId")
                dtItems = objOpportunities.ItemsByOppId.Tables(0)

                Dim Tax, TotalAmount, SubTotal As Decimal
                Dim i As Integer

                If dtItems.Rows.Count > 0 Then
                    SubTotal = CType(dtItems.Compute("SUM(monTotAmount)", ""), Decimal)
                    For i = 0 To dtItems.Rows.Count - 1
                        If dtItems.Rows(i).Item("Tax") > 0 Then
                            Tax = Tax + (dtItems.Rows(i).Item("Amount") * dtItems.Rows(i).Item("Tax") / 100)
                        End If
                    Next
                End If
                lblSubTotal.Text = String.Format("{0:#,##0.00}", SubTotal)
                lblTax.Text = String.Format("{0:#,##0.00}", Tax)
                lblTotal.Text = String.Format("{0:#,##0.00}", SubTotal + Tax)
                Session("TotalAmount") = String.Format("{0:#,##0.00}", SubTotal + Tax)
                CheckCreditStatus()
            End If
            btnClose.Attributes.Add("onclick", "return Close()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub CheckCreditStatus()
        Try
            Dim objItems As New CItems
            Dim lngCredit, lngRemainingCredit As Long
            objItems.DivisionID = lngDivID
            lngCredit = objItems.GetCreditStatusofCompany
            lngRemainingCredit = objItems.GetAmountDue.Rows(0).Item("RemainingCredit")
            If lngRemainingCredit <= 0 Then
                radBillMe.Enabled = False
            Else
                txtCredit.Text = lngCredit
                txtDueAmount.Text = objItems.GetAmountDue.Rows(0).Item("DueAmount")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            If radPayCheck.Checked = True Then
                Response.Redirect("../Order/frmPayment.aspx")
            Else
                Dim totalDue As Decimal
                totalDue = CDec(txtDueAmount.Text) + CDec(Session("TotalAmount"))
                If totalDue > txtCredit.Text Then
                    liMessage.Text = "You have exceeded the maximum amount you can place on a bill-to account by 'whatever the amount is' Please reduce your total amount to compensate, or if you prefer, execute the order by paying with a check. "
                Else
                    btnSubmit.Enabled = False
                    liMessage.Text = "Your purchase was processed successfully ! "
                    Dim objItems As New CItems
                    objItems.OppId = Session("OppID")
                    objItems.DomainID = Session("DomainID")
                    objItems.UserCntId = Session("UserContactID")
                    objItems.UpdateDealStatus1()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class
