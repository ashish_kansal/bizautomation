<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmBillDetails.aspx.vb" Inherits="BACRMPortal.frmBillDetails"%>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
		<title>Bill Details</title>
		<script language="javascript" type="text/javascript" >
           function Close()
		    {
		        window.close()
		        return false;
		    }
		</script>
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
			<asp:GridView ID="dgBasket" Runat="server" AutoGenerateColumns="False" Width="100%">
								<EmptyDataTemplate>
									&nbsp;-- No items in your basket --
								</EmptyDataTemplate>
								<Columns>
									<asp:BoundField DataField="numItemCode" Visible="False"></asp:BoundField>
									<asp:BoundField HeaderText="Item" DataField="vcItemName"></asp:BoundField>
									<asp:BoundField HeaderText="Units" DataField="numUnitHour"></asp:BoundField>
									<asp:BoundField HeaderText="Price/Unit" DataField="monPrice" DataFormatString="{0:C}">
										<ItemStyle HorizontalAlign="Right"></ItemStyle>
									</asp:BoundField>
									<asp:BoundField HeaderText="Total" DataField="monTotAmount" DataFormatString="{0:C}">
										<ItemStyle HorizontalAlign="Right"></ItemStyle>
									</asp:BoundField>
								</Columns>
							</asp:GridView>
								<table align="right">
							<tr>
								<td class="text_bold" align="right">Subtotal: &nbsp;
								</td>
								<td align="right">
									<asp:Label ID="lblSubTotal" Runat="server" CssClass="text"></asp:Label></td>
							</tr>
							<tr>
								<td class="text_bold" align="right">Tax: &nbsp;
								</td>
								<td align="right">
									<asp:Label ID="lblTax" Runat="server" CssClass="text"></asp:Label></td>
							</tr>
							<tr>
								<td class="text_bold" align="right">Total: &nbsp;
								</td>
								<td align="right">
									<asp:Label ID="lblTotal" Runat="server" CssClass="text"></asp:Label></td>
							</tr>
						</table>
		<table width="100%">
		    <tr>
		        <td align="right" >
		            <asp:button id="btnClose" Runat="server" CssClass="button" Text="Close" Width="50"></asp:button>
		        </td>
		    </tr>
		</table>
			<br>
			<br>
			<br>
			<br>
			<table width="500" align="center" border="0" cellpadding="5" cellspacing="5">
				
				<tr>
					<td align="right"><asp:RadioButton ID="radBillMe" Runat="server" GroupName="rad"></asp:RadioButton>
					</td>
					<td class="normal1"><b>Bill Me</b> &nbsp;(If you or your company has terms with 
						us.)
					</td>
				</tr>
				<tr>
					<td align="right"><asp:RadioButton ID="radPayCheck" Runat="server" GroupName="rad"></asp:RadioButton>
					</td>
					<td class="normal1"><b>Pay by Credit Card</b>
					</td>
				</tr>
				
				<tr>
					<td colspan="2" align="center">
						<asp:Button ID="btnSubmit" Runat="server" CssClass="button" Text="Submit"></asp:Button>
					</td>
				</tr>
				<tr>
					<td align="center" colspan="2" class="normal3">
						<asp:Literal ID="liMessage" Runat="server"></asp:Literal>
					</td>
				</tr>
			</table>
			<asp:TextBox ID="txtCredit" Runat="server" style="DISPLAY:none"></asp:TextBox>
			<asp:TextBox ID="txtDueAmount" Runat="server" style="DISPLAY:none"></asp:TextBox>
		</form>
	</body>
</HTML>
