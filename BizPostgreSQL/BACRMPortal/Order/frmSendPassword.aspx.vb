Imports System.Net.Mail.SmtpClient
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Namespace BACRM.BusinessLogic.Admin
    Public Class frmSendPassword : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents litClientScript As System.Web.UI.WebControls.Literal

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                'Put user code to initialize the page here
                EmailPassword()
                litClientScript.Text &= "<script language='javascript'>this.parent.DoneSendingPassword();this.parent.location.href='frmGenericFormOrder.aspx';</script>"
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This call emails the password to the addresse
        ''' </summary>
        ''' <remarks>
        '''     When the 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/29/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function EmailPassword() As Boolean
            Dim sEmail As String = GetQueryStringVal( "sEmail")                                    'Declare a string to store the email and init its value
            Dim frmRegistrationForOrder As New FormGenericRegistration                              'Instantiate an object for Generic Pre-Order Registration form
            Dim sPwd As String = frmRegistrationForOrder.GetPassword(sEmail)                        'Request for password
            Try
                Dim SmtpMail As New System.Net.Mail.SmtpClient
                Dim _from As System.Net.Mail.MailAddress = New System.Net.Mail.MailAddress("carl@bizautomation.com")
                Dim _to As System.Net.Mail.MailAddress = New System.Net.Mail.MailAddress("carl@bizautomation.com")
                Dim msg As New System.Net.Mail.MailMessage(_from, _to)                 'Create the email message

                msg.Subject = "An unknown user has requested for your password to be send to your address"
                msg.Body = "Sir," & vbCrLf & "Your Password is requested by an unknown person" _
                & vbCrLf & "Your Password is: " & sPwd _
                & vbCrLf & "You are requested to change your password on your next login." & vbCrLf

                SmtpMail.Host = "localhost"
                SmtpMail.Port = 80       'Specify the outgoing SMTP mail server

                SmtpMail.Send(msg)
                Return True
            Catch Ex As Exception
                Throw Ex
            End Try
        End Function

    End Class
End Namespace