<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmSendPassword.aspx.vb" Inherits="BACRMPortal.BACRM.BusinessLogic.Admin.frmSendPassword" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
		<title>Send Password</title>
	</HEAD>
	<body>
		<form id="frmSendPassword" runat="server">
			<table cellspacing="0" cellpadding="0" width="100%" border="0">
				<TR>
					<td valign="middle" class="normal1" align="center">
						Sending Password to the email address.
					</td>
				</TR>
			</table>
			<asp:literal id="litClientScript" Runat="server"></asp:literal>
		</form>
	</body>
</HTML>
