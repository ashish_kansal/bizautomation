<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmPayment.aspx.vb" Inherits="BACRMPortal.frmPayment" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
   <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
 <style >
   table {
	font-size: 1em; 
	margin-top: 1em; 
	margin-bottom: 1em;
	border-width: 0px;
}
.footer {
	color: #333333;
	background-color: whitesmoke;
	margin-top: 7em;
	border-top: .5em;
	font-size: .85em;
	font-style: normal;
	xborder-top: 1px solid #000000;
	xborder-bottom: 1px solid #000000;
}
</style>
</head>
<body>
    <form id="form1" runat="server">
        <cc1:Icharge ID="Icharge1" runat="server">
        </cc1:Icharge>
        
        <table id="Table2" width="90%" class="normal1">
					<tr>
					<td width="50%">
							<table id="Table3" cellspacing="2" cellpadding="5">
								<tr>
									<th nowrap colspan="2">
										Customer Information:
									</th>
							    </tr>
								<tr>
									<td nowrap align="right">Name:</td>
									<td>
										<asp:textbox id="txtCustomerFirstName" CssClass="signup"  runat="server" width="62px"></asp:textbox>
										<asp:textbox id="txtCustomerLastName" CssClass="signup"  runat="server" width="117px"></asp:textbox></td>
								</tr>
								<tr>
									<td nowrap align="right">Address:</td>
									<td>
										<asp:textbox id="txtCustomerAddress" CssClass="signup"  runat="server" width="181px"></asp:textbox></td>
								</tr>
								<tr>
									<td nowrap align="right">City / State:</td>
									<td>
										<asp:textbox id="txtCustomerCity" CssClass="signup"  runat="server" width="142px"></asp:textbox>
										<asp:textbox id="txtCustomerState" Visible="false" CssClass="signup"  runat="server" width="39px"></asp:textbox>
										<asp:DropDownList runat="server" ID="ddlState" AutoPostBack="true" CssClass="signup"></asp:DropDownList></td>		
								</tr>
								<tr>
									<td nowrap align="right">Country:</td>
									<td>
									      <asp:DropDownList runat="server" ID="ddlCountry" AutoPostBack="true" CssClass="signup"></asp:DropDownList>		
									</td>
								</tr>
								<tr>
									<td nowrap align="right">Zip:</td>
									<td>
										<asp:textbox id="txtCustomerZip"  CssClass="signup" runat="server" width="182px"></asp:textbox></td>
								</tr>
								<tr>
									<td nowrap align="right">Phone:</td>
									<td>
										<asp:textbox id="txtCustomerPhone" CssClass="signup"  runat="server" width="182px"></asp:textbox></td>
								</tr>
								<tr>
									<td nowrap align="right">Email:</td>
									<td>
										<asp:textbox id="txtCustomerEmail"  CssClass="signup" runat="server" width="182px"></asp:textbox></td>
								</tr>
								<tr>
									<td align="right">Customer ID:</td>
									<td>
										<asp:textbox id="txtCustomerID" CssClass="signup"  runat="server" width="182px"></asp:textbox></td>
								</tr>
							</table>
							<P align="center">
								<asp:button id="bCharge" runat="server" CssClass="signup" Width="70"  text="Submit"></asp:button></P>
							<P align="center">
								<asp:Label id="lblWarning" runat="server" ForeColor="Red" Visible="False" Font-Size="XX-Small">Supply a Login/Password</asp:Label></P>
						</td>
						<td width="50%" valign="top" >
							
							<table>
								<tr>
									<th nowrap colspan="2">
										Credit Card Information
									</th>
								</tr>
								<tr>
									<td nowrap align="right">Card Number:</td>
									<td>
										<asp:textbox id="txtCardNumber" CssClass="signup"  runat="server" width="144px"></asp:textbox></td>
								</tr>
								<tr>
									<td nowrap align="right">Card Type:</td>
									<td>
									    <asp:DropDownList runat="server" ID="ddlCardType" CssClass="signup">
									        <asp:listitem value="1">Visa </asp:listitem>
									        <asp:listitem value="2">MasterCard</asp:listitem>
									        <asp:listitem value="3">AMEX</asp:listitem>
									        <asp:listitem value="4">Discover</asp:listitem>
									        <asp:listitem value="5">Diners </asp:listitem>
									        <asp:listitem value="6">JCB</asp:listitem>
									        <asp:listitem value="7">BankCard</asp:listitem>									       
									    </asp:DropDownList>	
									</td>
								</tr>
								<tr>
									<td nowrap align="right">Exp Date (MM/YY):
									</td>
									<td>
										<asp:dropdownlist id="ddlCardExpMonth" CssClass="signup"  runat="server">
											<asp:listitem value="01">01</asp:listitem>
											<asp:listitem value="02">02</asp:listitem>
											<asp:listitem value="03">03</asp:listitem>
											<asp:listitem value="04">04</asp:listitem>
											<asp:listitem value="05">05</asp:listitem>
											<asp:listitem value="06">06</asp:listitem>
											<asp:listitem value="07">07</asp:listitem>
											<asp:listitem value="08">08</asp:listitem>
											<asp:listitem value="09">09</asp:listitem>
											<asp:listitem value="10">10</asp:listitem>
											<asp:listitem value="11">11</asp:listitem>
											<asp:listitem value="12">12</asp:listitem>
										</asp:dropdownlist>&nbsp;/&nbsp;
										<asp:dropdownlist id="ddlCardExpYear" CssClass="signup"  runat="server"></asp:dropdownlist></td>
								</tr>
								<tr>
									<td nowrap align="right" height="31">CVV2 Data:</td>
									<td height="31">
										<asp:textbox id="txtCardCVV2" CssClass="signup"  runat="server" width="117px"></asp:textbox></td>
								</tr>
								<tr>
									<th nowrap colspan="2">
										Transaction Information
									</th>
								<tr>
									<td nowrap align="right">Amount:</td>
									<td>
										<asp:textbox id="txtTransactionAmount" CssClass="signup"  runat="server" width="144px"></asp:textbox></td>
								</tr>
								
								<tr>
									<td nowrap align="right">Invoice #:</td>
									<td>
										<asp:textbox id="txtTransactionInvoice" CssClass="signup"  runat="server" width="117px"></asp:textbox></td>
								</tr>
							</table>
						</td>
						
					</tr>
				</table>
				<strong>
					<asp:literal id="litOutput" runat="server" enableviewstate="False"></asp:literal></strong>
				<asp:panel id="pnlAuthResults" runat="server" width="99.46%" enableviewstate="False" visible="False" height="72px">
					<TABLE id="Table4" class="normal1" cellSpacing="0" cellPadding="5" border="0">
						<TR>
							<TH width="259" colSpan="2">
								<STRONG>Transaction Response Info</STRONG></TH></TR>
						<TR>
							<TD noWrap align="right">Response Code :
							</TD>
							<TD width="128">
								<asp:label id="lblResponseCode" CssClass="signup"  runat="server" width="100%">
									
								</asp:label></TD>
						</TR>
						<TR>
							<TD noWrap align="right">Approval Code :
							</TD>
							<TD width="128">
								<asp:label id="lblResponseApprovalCode" CssClass="signup"  runat="server" width="100%">
									
								</asp:label></TD>
						</TR>
						<TR>
							<TD noWrap align="right">Response Text :
							</TD>
							<TD width="128">
								<asp:label id="ibiResponseText" CssClass="signup"  runat="server" width="100%">								
								</asp:label></TD>
						</TR>
						<TR>
							<TD noWrap align="right">Response AVS :
							</TD>
							<TD width="128">
								<asp:label id="lblResponseAVS" CssClass="signup"  runat="server" width="100%">
								
								</asp:label></TD>
						</TR>
						<TR>
							<TD noWrap align="right">Response CVV2 :
							</TD>
							<TD width="128">
								<asp:label id="lblResponseCVV2" CssClass="signup"  runat="server" width="100%">								
								</asp:label></TD>
						</TR>
						<TR>
							<TD noWrap align="right">Invoice Num :
							</TD>
							<TD width="128">
								<asp:label id="lblResponseInvoiceNumber" CssClass="signup"  runat="server" width="100%">
									
								</asp:label></TD>
						</TR>
						<TR>
							<TD noWrap align="right">Transaction Id :
							</TD>
							<TD width="128">
								<asp:label id="lblResponseTransactionId" CssClass="signup"  runat="server" width="100%">								
								</asp:label></TD>
						</TR>
					</TABLE>
				</asp:panel><br>
    </form>
</body>
</html>
