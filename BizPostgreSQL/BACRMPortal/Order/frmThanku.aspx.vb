Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
<Obsolete()>
Partial Class frmThanku : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Dim RanNum As New Random
            Dim strRanNO As String
            strRanNO = RanNum.Next(10000000, 99999999)
            Dim objUserAccess As New UserAccess
            objUserAccess.CompanyID = Session("CompID")
            objUserAccess.DivisionID = Session("DivId")
            objUserAccess.ContactID = Session("UserContactID")
            If objCommon Is Nothing Then
                objCommon = New CCommon()
            End If
            objUserAccess.Password = objCommon.Encrypt(strRanNO)
            objUserAccess.DomainID = Session("DomainID")
            objUserAccess.CreateExtAccessFromPortal()

            Dim strBody As String
            strBody = "Hi <br> <br>"
            strBody = strBody & "Your link to the Portal is " & ConfigurationManager.AppSettings("PortalURL") & "<br>"
            strBody = strBody & "Your Username is " & objUserAccess.Email & "<br>"
            strBody = strBody & "Your Password is " & strRanNO
            Dim objSendMail As New Email
            objSendMail.SendSimpleEmail("Your Password And Link to the portal!", strBody, "", ConfigurationManager.AppSettings("FromAddress"), objUserAccess.Email)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class
