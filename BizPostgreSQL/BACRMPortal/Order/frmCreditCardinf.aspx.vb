Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Common
Partial Class frmCreditCardinf
    Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Textbox1 As System.Web.UI.WebControls.TextBox
    Dim lngDivID As Long
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            lngDivID = Session("DivId")
            If Not IsPostBack Then
                lblAmount.Text = Session("TotalAmount")
                LoadAdddress()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadAdddress()
        Try
            Dim dttable As DataTable
            Dim objContacts As New CContacts
            objContacts.DivisionID = lngDivID
            objContacts.DomainID = Session("DomainID")
            dttable = objContacts.GetBillOrgorContAdd
            lBillToo.Text = IIf(IsDBNull(dttable.Rows(0).Item("vcBillstreet")), "", dttable.Rows(0).Item("vcBillstreet"))
            lblCity.Text = IIf(IsDBNull(dttable.Rows(0).Item("vcBillCity")), "", dttable.Rows(0).Item("vcBillCity"))
            lblState.Text = IIf(IsDBNull(dttable.Rows(0).Item("vcBilState")), "", dttable.Rows(0).Item("vcBilState"))
            lblZip.Text = IIf(IsDBNull(dttable.Rows(0).Item("vcBillPostCode")), "", dttable.Rows(0).Item("vcBillPostCode"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class
