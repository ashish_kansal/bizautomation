Imports nsoftware.IBizPay
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Item

Partial Public Class frmPayment : Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not (Page.IsPostBack) Then
                Dim Years As New ArrayList()
                Dim i As Integer
                For i = 1 To 20
                    Years.Add(DateTime.Now.AddYears(i - 1).Date.ToString("yy"))
                Next i
                ddlCardExpYear.DataSource = Years
                ddlCardExpYear.DataBind()
                ddlCardExpYear.Items.FindByValue(Now.Year.ToString("yy"))

                ddlCardExpMonth.SelectedIndex = (DateTime.Now.Month - 1)

                'ddlGateway.DataSource = System.Enum.GetValues(GetType(nsoftware.IBizPay.IchargeGateways))
                'ddlGateway.DataBind()
                ' ddlGateway.Items.Insert(0, "--Select One--")
                ' ddlGateway.Items(0).Value = "0"
                ' ddlGateway.SelectedIndex = 1 'set Authorze.Net as the default gateway
                ' ddlCardType.DataSource = System.Enum.GetValues(GetType(nsoftware.IBizPay.IchargeCardTypes))
                ' ddlCardType.DataBind()
                ' ddlCardType.Items.Insert(0, "--Select One--")
                ' ddlCardType.Items(0).Value = "0"

                Dim objCommon As New CCommon
                objCommon.sb_FillComboFromDBwithSel(ddlCountry, 40, Session("DomainID"))
                txtTransactionAmount.Text = Session("TotalAmount")
                txtTransactionInvoice.Text = Session("Oppid")
                txtCustomerFirstName.Text = Session("ContactName")
                txtCustomerID.Text = Session("DivId")

                Dim objContacts As New CContacts
                Dim dtTable As DataTable
                Dim divid As Integer = Session("DivId")
                objContacts.DivisionID = divid
                objContacts.DomainID = Session("DomainID")
                dtTable = objContacts.GetBillOrgorContAdd
                txtCustomerAddress.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcBillstreet")), "", dtTable.Rows(0).Item("vcBillstreet"))
                txtCustomerCity.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcBillCity")), "", dtTable.Rows(0).Item("vcBillCity"))
                If Not IsDBNull(dtTable.Rows(0).Item("vcBillCountry")) Then
                    If Not ddlCountry.Items.FindByValue(dtTable.Rows(0).Item("vcBillCountry")) Is Nothing Then
                        ddlCountry.Items.FindByValue(dtTable.Rows(0).Item("vcBillCountry")).Selected = True
                    End If
                End If
                If ddlCountry.SelectedIndex > 0 Then
                    FillState(ddlState, ddlCountry.SelectedItem.Value, Session("DomainID"))
                    If Not IsDBNull(dtTable.Rows(0).Item("vcBilState")) Then
                        If Not ddlState.Items.FindByValue(dtTable.Rows(0).Item("vcBilState")) Is Nothing Then
                            ddlState.Items.FindByValue(dtTable.Rows(0).Item("vcBilState")).Selected = True
                        End If
                    End If
                End If
                txtCustomerZip.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcBillPostCode")), "", dtTable.Rows(0).Item("vcBillPostCode"))
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCountry.SelectedIndexChanged
        Try
            FillState(ddlState, ddlCountry.SelectedItem.Value, Session("DomainID"))
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class