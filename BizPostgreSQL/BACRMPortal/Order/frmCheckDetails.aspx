<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmCheckDetails.aspx.vb" Inherits="BACRMPortal.frmCheckDetails"%>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Check Details</title>
		<script language="javascript">
		function Back()
		{
			window.location.href="../Order/frmBillDetails.aspx"
			return false;
		}
		function EnableSubmit()
		{
			if (document.Form1.chkAgree.checked==true)
			{
				document.Form1.btnSubmit.disabled=false;
			}
			else
			{
				document.Form1.btnSubmit.disabled=true;
			}
		}
           function Close()
		    {
		        window.close()
		        return false;
		    }
		</script>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
			<table width="100%">
		    <tr>
		        <td align="right" >
		            <asp:button id="btnClose" Runat="server" CssClass="button" Text="Close" Width="50"></asp:button>
		        </td>
		    </tr>
		</table>
			<asp:table HorizontalAlign="Center" ID="tbl" Runat="server" Width="649" BorderWidth="1" BorderColor="black">
				<asp:tableRow VerticalAlign="Top">
					<asp:tableCell>
						<table width="649" cellpadding="10" cellspacing="10" align="center" height="10" background="../images/Bk1.gif">
							<tr height="10">
								<td valign="top" align="center" class="normal1">
									<HR width="90" SIZE="1">
									<b><font size="2">Biz E-Check</font></b>
									<HR width="90" SIZE="1">
								</td>
								<td align="center" class="normal1">
									<font color="red">*</font>Name as it appears on check.<br>
									<asp:TextBox ID="txtName" Runat="server" Width="200" CssClass="signup"></asp:TextBox>
								</td>
								<td></td>
							</tr>
							<tr>
								<td align="center" colspan="2" class="normal1"><b>Pay to the Order of : <font color="blue">
											<asp:Label ID="lblCompany" Runat="server"></asp:Label></font></b>
									<HR width="250" SIZE="1">
								</td>
								<td align="right" class="normal1">
									<b>Amount : $ </b>&nbsp;&nbsp;
									<asp:TextBox ID="txtAmount"  Runat="server" Width="150" CssClass="signup"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td align="center" colspan="2" class="normal1">
									<asp:table HorizontalAlign="Center" ID="table1" Runat="server" BorderWidth="1" BorderColor="black">
										<asp:tableRow VerticalAlign="Top">
											<asp:tableCell CssClass="normal1">
												<font color="red">*</font>Bank Routing Code<br>
												<b>|:</b>
<asp:TextBox ID="txtRouting" Runat="server" Width="150" CssClass="signup"></asp:TextBox>
												</asp:tableCell>
											<asp:tableCell CssClass="normal1">
												<font color="red">*</font>Bank Account Number<br>
												<b>|:</b>
<asp:TextBox ID="txtBankAcc" Runat="server" Width="150" CssClass="signup"></asp:TextBox>
												</asp:tableCell>
										</asp:tableRow>
									</asp:table>
								</td>
								<td class="normal1" align="center">
									Transaction Time & Date
									<asp:Label ID="lblDate" Runat="server"></asp:Label>
								</td>
							</tr>
						</table>
					</asp:tableCell>
				</asp:tableRow>
			</asp:table>
			<table align="center" width="649">
				<tr>
					<td class="text_bold" colspan="2">
						Enter the numbers from the bottom of your check as illustrated below:
					</td>
				</tr>
				<tr>
					<td colspan="2" align=center>
						<IMG src="../images/RoutingNo.gif">
					</td>
				</tr>
				<tr>
					<td><font color="red">*</font><asp:CheckBox ID="chkAgree" Runat="server"></asp:CheckBox>
					</td>
					<td class="normal1">
						I have read &amp; agreed to the terms and conditions <a id="hplMasterTerms" runat="server" target="_blank">
							<font color="blue">Master Terms &amp; Conditions</font></a>&nbsp;and agree 
						to paying BizAutomation.com by check for selected items, in the amount 
						displayed within the check on this page.
					</td>
				</tr>
				<tr>
					<td align="center" colspan="2">
						<asp:Button ID="btnSubmit" Runat="server" Enabled="False" CssClass="Check" Text="Submit"></asp:Button>
						<asp:Button ID="btnBack" Runat="server" Width="50" CssClass="Check" Text="Back"></asp:Button>
					</td>
				</tr>
			</table>
			<table align="center" width="70%">
				<tr>
					<td class="normal4" align="center">
						<asp:Literal ID="litMessage" Runat="server"></asp:Literal>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
