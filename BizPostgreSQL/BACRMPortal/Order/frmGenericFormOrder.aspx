<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmGenericFormOrder.aspx.vb" Inherits="BACRMPortal.BACRM.UserInterface.Admin.frmGenericFormOrder" ValidateRequest="false" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
		<title>Registration for Orders</title>
		<script language="javascript" src="../include/GenericOrder.js"></script>
		<script language="javascript" type="text/javascript" >
	        function CheckNumber()
					{
							return true;
					}
		</script>
	</HEAD>
	<body >
		<form id="frmGenericFormOrder" method="post" runat="server">
		<br />
			<table cellspacing="0" cellpadding="0" width="100%">
				<tr>
					<td valign="bottom" style="WIDTH: 111px">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;Order&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td align="left" height="23" class="normal1">&nbsp;&nbsp;(<span class="normal4">*</span>) 
						Required
					</td>
					<td class="normal1" align="right" height="23">&nbsp;&nbsp;&nbsp;<asp:button id="btnSubmit" Text="Submit" Runat="server" cssclass="button"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
				</tr>
			</table>
			<asp:table id="tblProspects" Runat="server" GridLines="None" BorderColor="black" Width="100%"
							Height="300" BorderWidth="1" cellspacing="0" cellpadding="0" CssClass="aspTable">
							<asp:tableRow>
								<asp:tableCell VerticalAlign="Top" >
								<br />
								    <asp:placeholder id="plhFormControls" runat="server"></asp:placeholder>
								    <br />
								</asp:tableCell>
								
						</asp:tableRow>
			</asp:table>
			<div class="tr1" id="divNew" style="Z-INDEX: 10; LEFT: 150px; VISIBILITY: hidden; OVERFLOW: visible; WIDTH: 400px; POSITION: absolute; TOP: 150px; HEIGHT: 50px">
				<table borderColor="#000000" cellspacing="0" cellpadding="0" width="100%" border="1">
					<tr>
						<td>
							<table cellspacing="0" cellpadding="0" width="100%" border="0">
								<tr>
									<td>
										<iframe id="cntOpenItem" src="about:blank" frameBorder="0" width="400" scrolling="no" height="50"
											style="WIDTH: 100px; HEIGHT: 50px"></iframe>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
			<asp:validationsummary id="ValidationSummary" runat="server" HeaderText="Please check the following value(s)"
				ShowSummary="False" ShowMessageBox="True" DisplayMode="List"></asp:validationsummary>
			<input id="hdXMLString" type="hidden" name="hdXMLString" runat="server"> <input id="hdGroupId" type="hidden" name="hdGroupId" runat="server"><!---Store the Group ID--->
			<input id="hdEmail" type="hidden" name="hdEmail" runat="server"><!---Store the Email Id--->
			<input id="hdCompanyType" type="hidden" name="hdCompanyType" runat="server"><!---Store the Company Type --->
			<asp:literal id="litClientScript" Runat="server"></asp:literal>
		</form>
	</body>
</HTML>
