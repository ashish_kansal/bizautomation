Imports USAePayXChargeCom2
Imports BACRM.BusinessLogic.Item

Partial Class frmCheckDetails : Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not IsPostBack Then
                lblCompany.Text = ConfigurationManager.AppSettings("CheckCompanyName")
                lblDate.Text = Now()
                txtAmount.Text = Session("TotalAmount")
            End If
            If chkAgree.Checked = True Then
                btnSubmit.Enabled = True
            Else : btnSubmit.Enabled = False
            End If
            btnBack.Attributes.Add("onclick", "return Back()")
            chkAgree.Attributes.Add("onclick", "return EnableSubmit()")
            hplMasterTerms.HRef = ConfigurationManager.AppSettings("AgreementPage")
            btnClose.Attributes.Add("onclick", "return Close()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            Dim Xcharge As New USAePayXChargeCom2.XChargeCom2
            Xcharge.Command = USAePayXChargeCom2.CommandType.check

            Xcharge.SourceKey = ConfigurationManager.AppSettings("SourceKey")
            Xcharge.Pin = 51230
            Xcharge.IP = Request.ServerVariables.Get("REMOTE_ADDR")

            Xcharge.TestMode = False
            Xcharge.Amount = txtAmount.Text
            Xcharge.TransHolderName = txtName.Text
            Xcharge.Routing = txtRouting.Text
            Xcharge.Account = txtBankAcc.Text
            Xcharge.SSN = "000000000"

            Xcharge.Process()

            If Xcharge.ErrorExists = True Then
                For Each XError As clsError In Xcharge.Errors
                    litMessage.Text = "We're sorry but we were 	unable to process your order, due to an incorrect entry on the check form. 	Please try again, or contact your sales representative for assistance"
                Next
            End If

            Dim strResponseStatus As String = ""
            If Xcharge.ResponseStatus = StatusType.Approved Then
                strResponseStatus = "Approved"
                Dim objItems As New CItems
                objItems.OppId = Session("OppID")
                objItems.byteMode = 1
                objItems.Amount = Session("TotalAmount")
                objItems.DomainID = Session("DomainID")
                objItems.UserCntId = Session("UserContactID")
                objItems.UpdateDealStatus1()
                If Session("Internal") = 0 Then
                    Response.Redirect("../Order/frmThanku.aspx?RefNo=" & Xcharge.ResponseReferenceNum & "&AuthCode=" & Xcharge.ResponseAuthCode)
                Else
                    litMessage.Text = "Your purchase was processed successfully !"
                    btnSubmit.Enabled = False
                    btnBack.Enabled = False
                End If
                Exit Sub
            ElseIf Xcharge.ResponseStatus = StatusType.Declined Then
                strResponseStatus = "Declined"
            ElseIf Xcharge.ResponseStatus = StatusType.Error Then
                strResponseStatus = "Error"
            ElseIf Xcharge.ResponseStatus = StatusType.Verification Then
                strResponseStatus = "Verification"
            End If

            'strResponseStatus = strResponseStatus & ", AuthCode = " & Xcharge.ResponseReferenceNum & _
            '", Authentication Code = " & Xcharge.ResponseAuthCode & ", Reference No = " & Xcharge.ResponseReferenceNum & _
            '", Response Batch = " & Xcharge.ResponseBatch & ", Response Error = " & Xcharge.ResponseError
            litMessage.Text = "We're sorry but we were 	unable to process your order, due to an incorrect entry on the check form. 	Please try again, or contact your sales representative for assistance"
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class
