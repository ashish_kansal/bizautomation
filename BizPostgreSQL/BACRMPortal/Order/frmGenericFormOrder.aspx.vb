''' -----------------------------------------------------------------------------
''' Project	 : BACRM.UserInterface.Admin
''' Class	 : frmGenericFormOrder
''' 
''' -----------------------------------------------------------------------------
''' <summary>
'''     This is one of the output forms which is generated from the config set in thw BizForm Wizard
'''     1) Order
''' </summary>
''' <remarks>
''' </remarks>
''' <history>
''' 	[Debasish]	08/19/2005	Created
''' </history>
''' -----------------------------------------------------------------------------
''' 

Imports BACRM.BusinessLogic.Admin
Imports System.IO
Imports System.Reflection
Imports System.Math
Imports BACRM.BusinessLogic.alerts
Imports BACRM.BusinessLogic.documents
Imports BACRM.BusinessLogic.common
Imports BACRM.BusinessLogic.prospects
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Leads

Namespace BACRM.UserInterface.Admin
    Public Class frmGenericFormOrder : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.ID = "frmGenericFormOrder"

        End Sub
        Protected WithEvents plhFormControls As System.Web.UI.WebControls.PlaceHolder
        Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
        Protected WithEvents ValidationSummary As System.Web.UI.WebControls.ValidationSummary
        Protected WithEvents litClientScript As System.Web.UI.WebControls.Literal
        Protected WithEvents hdXMLString As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdGroupId As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdEmail As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hdCompanyType As System.Web.UI.HtmlControls.HtmlInputHidden
        Private numDomainId As Integer
        Dim dtGenericFormConfig As DataTable
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This event is fired each time thepage is called. In this event we will 
        '''     get the data from the DB create the form.
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/07/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        ''' 

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Trim(GetQueryStringVal( "D")) <> "" Then
                    numDomainId = GetQueryStringVal( "D")                                                  'Get the Domain Id from the web.config
                    'Put user code to initialize the page here
                    If Not IsPostBack Then                                                                  'The form loads for the first time
                        Dim dtGenericFormHeaderConfig As DataTable                                          'declare a datatable
                        Dim objConfigWizard As New FormGenericFormContents                                  'Create an object of class which encaptulates the functionaltiy
                        objConfigWizard.FormID = 4                                                          'Set the property of formId
                        objConfigWizard.DomainID = numDomainId                                              'Set the Domain Id
                        dtGenericFormHeaderConfig = objConfigWizard.getFormHeaderDetails()                  'call to get the form header details
                        If dtGenericFormHeaderConfig.Rows.Count = 0 Then                                    'if the form is not registered in the database
                            btnSubmit.Enabled = False                                                       'Disable any attempt to save/ submit
                            Exit Sub                                                                        'Exit the flow
                        End If
                        Dim objLeadBoxData As New ImplementLeadsAutoRoutingRules                            'Create an object of class which encaptulates the functionaltiy
                        objLeadBoxData.DomainID = numDomainId                                               'set the domain id
                        Dim numRoutId As Integer = objLeadBoxData.checkForDefaultAutoRule()                                     'Get the Default Rule Id
                        If numRoutId = 0 Then                                                               'if the xml for form fields has not been configured
                            btnSubmit.Enabled = False                                                       'Disable any attempt to save/ submit
                            plhFormControls.Controls.Add(New LiteralControl("Auto Rules are not created, please contact the Administrator.")) 'calls to create the form controls and add it to the form
                            Exit Sub                                                                        'and exit the flow
                        End If
                        hdGroupId.Value = dtGenericFormHeaderConfig.Rows(0).Item("numGrpId")                'Store the Group Id
                        hdCompanyType.Value = dtGenericFormHeaderConfig.Rows(0).Item("vcAdditionalParam")   'Store the Company type/ relationship

                        'Calls function for form generation and display for non AOI fields
                    End If
                    callFuncForFormGenerationNonAOI()
                    Dim dvConfig As DataView
                    dvConfig = New DataView(dtGenericFormConfig)
                    dvConfig.RowFilter = " vcDbColumnName like '%Country'"
                    If dvConfig.Count > 0 Then
                        Dim i As Integer
                        For i = 0 To dvConfig.Count - 1
                            If dvConfig(i).Item("vcDbColumnName") = "vcBillCountry" Then
                                If Not CType(plhFormControls.FindControl("vcBilState"), DropDownList) Is Nothing Then
                                    Dim dl As DropDownList
                                    dl = CType(plhFormControls.FindControl(dvConfig(i).Item("vcDbColumnName")), DropDownList)
                                    dl.AutoPostBack = True
                                    AddHandler dl.SelectedIndexChanged, AddressOf FillStateDyn
                                End If
                            Else
                                If Not CType(plhFormControls.FindControl(Replace(dvConfig(i).Item("vcDbColumnName"), "Country", "State")), DropDownList) Is Nothing Then
                                    Dim dl As DropDownList
                                    dl = CType(plhFormControls.FindControl(dvConfig(i).Item("vcDbColumnName")), DropDownList)
                                    dl.AutoPostBack = True
                                    AddHandler dl.SelectedIndexChanged, AddressOf FillStateDyn
                                End If
                            End If
                        Next
                    End If
                Else
                    plhFormControls.Controls.Add(New LiteralControl("The screen is not created. Please use BizForm Wizard to create this screen.")) 'calls to create the form controls and add it to the form
                    btnSubmit.Enabled = False                                                       'Disable any attempt to save/ submit
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub FillStateDyn(ByVal sender As Object, ByVal e As EventArgs)
            Try
                If sender.ID = "vcBillCountry" Then
                    FillState(CType(plhFormControls.FindControl("vcBilState"), DropDownList), sender.SelectedItem.Value, numDomainId)
                Else : FillState(CType(plhFormControls.FindControl(Replace(sender.ID, "Country", "State")), DropDownList), sender.SelectedItem.Value, numDomainId)
                End If
                'Response.Write(sender.ID)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is called to initiate the generation of the dynamic form.
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        '''-----------------------------------------------------------------------------
        Sub callFuncForFormGenerationNonAOI()
            Try
                GenerateGenericFormControls.FormID = 4                                            'set the form id to Order Form
                GenerateGenericFormControls.DomainID = numDomainId                                'Set the domain id
                GenerateGenericFormControls.sXMLFilePath = Server.MapPath("") & "\..\Documents\Docs\" 'set the file path
                GenerateGenericFormControls.boolAOIField = 0                                      'Set the AOI flag to non AOI
                dtGenericFormConfig = GenerateGenericFormControls.getFormControlConfig() 'Declare and pupulate a Datatable to store the form config
                If dtGenericFormConfig.Rows.Count = 0 Then                                          'if the xml for form fields has not been configured
                    btnSubmit.Enabled = False                                                       'Disable any attempt to save/ submit
                    plhFormControls.Controls.Add(New LiteralControl("The screen is not created. Please use BizForm Wizard to create this screen.")) 'calls to create the form controls and add it to the form
                    Exit Sub                                                                   'and exit the flow
                End If
                plhFormControls.Controls.Add(GenerateGenericFormControls.createFormControls(dtGenericFormConfig)) 'calls to create the form controls and add it to the form
                litClientScript.Text = GenerateGenericFormControls.getJavascriptArray(dtGenericFormConfig) 'Create teh javascript array and store
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to handle saving of the configuration after the 
        '''     "Submit" button is clicked
        ''' </summary>
        ''' <param name="sender">Represents the sender object.</param>
        ''' <param name="e">Represents the EventArgs.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/20/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
            Dim objLeads As New CLeads
            Dim lnDivID, lngCMPID, lnCntID As Long
            Dim lstItem As ListItem
            Dim dRow As DataRow
            Dim ds As New DataSet
            Try
                dtGenericFormConfig.Columns.Add("vcDbColumnText")
                objLeads.GroupID = 1
                objLeads.ContactType = 70
                objLeads.PrimaryContact = True

                objLeads.DivisionName = "-"
                Dim vcAssociatedControlType As String
                Dim vcFieldType As String
                For Each dr As DataRow In dtGenericFormConfig.Rows
                    vcFieldType = dr("vcFieldType")
                    If vcFieldType = "R" Then
                        vcAssociatedControlType = dr("vcAssociatedControlType")
                        Select Case vcAssociatedControlType
                            Case "EditBox"
                                dr("vcDbColumnText") = CType(plhFormControls.FindControl(dr("vcDbColumnName")), TextBox).Text
                                If dr("vcDbColumnText") <> "" Then AssignValuesEditBox(objLeads, dr("vcDbColumnText"), dr("vcDbColumnName"))
                            Case "SelectBox"
                                dr("vcDbColumnText") = CType(plhFormControls.FindControl(dr("vcDbColumnName")), DropDownList).SelectedItem.Value
                                If dr("vcDbColumnText") <> "" Then
                                    AssignValuesSelectBox(objLeads, dr("vcDbColumnText"), dr("vcDbColumnName"))
                                Else : dr("vcDbColumnText") = 0
                                End If
                            Case "TextBox"
                                dr("vcDbColumnText") = CType(plhFormControls.FindControl(dr("vcDbColumnName")), TextBox).Text
                                If dr("vcDbColumnText") <> "" Then AssignValuesTextBox(objLeads, dr("vcDbColumnText"), dr("vcDbColumnName"))
                        End Select
                    End If
                Next
                objLeads.DomainID = GetQueryStringVal( "D")

                Dim objAutoRoutRles As New AutoRoutingRules
                objAutoRoutRles.DomainID = GetQueryStringVal( "D")
                dtGenericFormConfig.TableName = "Table"
                ds.Tables.Add(dtGenericFormConfig.Copy)
                objAutoRoutRles.strValues = ds.GetXml
                ds.Tables.Remove(ds.Tables(0))

                objLeads.UserCntID = objAutoRoutRles.GetRecordOwner
                objLeads.CRMType = 1
                lngCMPID = objLeads.CreateRecordCompanyInfo
                objLeads.CompanyID = lngCMPID
                lnDivID = objLeads.CreateRecordDivisionsInfo
                objLeads.DivisionID = lnDivID

                lnCntID = objLeads.CreateRecordAddContactInfo()
                Session("CompID") = lngCMPID                                       'Set the Company Id in a session
                Session("DivID") = lnDivID
                Session("UserContactID") = lnCntID

                ''Saving CustomFields

                Dim dsViews As New DataView(dtGenericFormConfig)
                dsViews.RowFilter = "vcFieldType='C'"
                Dim i As Integer

                Dim dtCusTable As New DataTable
                dtCusTable.Columns.Add("FldDTLID")
                dtCusTable.Columns.Add("fld_id")
                dtCusTable.Columns.Add("Value")
                Dim strdetails As String

                If dsViews.Count > 0 Then
                    For i = 0 To dsViews.Count - 1
                        dRow = dtCusTable.NewRow
                        dRow("FldDTLID") = 0
                        dRow("fld_id") = Replace(dsViews(i).Item("numFormFieldId"), "C", "")
                        dRow("Value") = GetCustFldValue(dsViews(i).Item("vcAssociatedControlType"), dsViews(i).Item("vcDBColumnName"))
                        dtCusTable.Rows.Add(dRow)
                    Next

                    dtCusTable.TableName = "Table"
                    ds.Tables.Add(dtCusTable.Copy)
                    strdetails = ds.GetXml
                    ds.Tables.Remove(ds.Tables(0))

                    Dim ObjCusfld As New CustomFields
                    ObjCusfld.strDetails = strdetails
                    ObjCusfld.RecordId = lnCntID
                    ObjCusfld.locId = 4
                    ObjCusfld.SaveCustomFldsByRecId()
                End If
                dsViews.RowFilter = "vcFieldType='D'"
                dtCusTable.Rows.Clear()
                If dsViews.Count > 0 Then
                    For i = 0 To dsViews.Count - 1
                        dRow = dtCusTable.NewRow
                        dRow("FldDTLID") = 0
                        dRow("fld_id") = Replace(dsViews(i).Item("numFormFieldId"), "C", "")
                        dRow("Value") = GetCustFldValue(dsViews(i).Item("vcAssociatedControlType"), dsViews(i).Item("vcDBColumnName"))
                        dtCusTable.Rows.Add(dRow)
                    Next

                    dtCusTable.TableName = "Table"
                    ds.Tables.Add(dtCusTable.Copy)
                    strdetails = ds.GetXml
                    ds.Tables.Remove(ds.Tables(0))

                    Dim ObjCusfld As New CustomFields
                    ObjCusfld.strDetails = strdetails
                    ObjCusfld.RecordId = lnDivID
                    ObjCusfld.locId = 1
                    ObjCusfld.SaveCustomFldsByRecId()
                End If

                Try
                    Dim objAlerts As New CAlerts
                    Dim dtDetails As DataTable
                    objAlerts.AlertDTLID = 6 'Alert DTL ID for sending alerts in opportunities
                    objAlerts.DomainID = Session("DomainID")
                    dtDetails = objAlerts.GetIndAlertDTL
                    If dtDetails.Rows.Count > 0 Then
                        If dtDetails.Rows(0).Item("tintAlertOn") = 1 Then
                            Dim dtEmailTemplate As DataTable
                            Dim objDocuments As New DocumentList
                            objDocuments.GenDocID = dtDetails.Rows(0).Item("numEmailTemplate")
                            objDocuments.DomainID = Session("DomainID")
                            dtEmailTemplate = objDocuments.GetDocByGenDocID
                            If dtEmailTemplate.Rows.Count > 0 Then
                                Dim objProspects As New CProspects
                                Dim dtComInfo As DataTable
                                objProspects.DivisionID = lnDivID
                                objProspects.DomainID = Session("DomainID")
                                objProspects.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                                dtComInfo = objProspects.GetCompanyInfoForEdit

                                Dim objSendMail As New Email
                                Dim dtMergeFields As New DataTable
                                Dim drNew As DataRow
                                dtMergeFields.Columns.Add("NoofEmp")
                                dtMergeFields.Columns.Add("Organization")
                                dtMergeFields.Columns.Add("Assignee")
                                drNew = dtMergeFields.NewRow
                                drNew("NoofEmp") = dtComInfo.Rows(0).Item("NoofEmp")
                                drNew("Organization") = dtComInfo.Rows(0).Item("vcCompanyName")
                                drNew("Assignee") = dtComInfo.Rows(0).Item("vcCreatedBy")
                                dtMergeFields.Rows.Add(drNew)
                                Dim objCommon As New CCommon
                                objCommon.byteMode = 1
                                objCommon.UserCntID = objLeads.UserCntID
                                objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), IIf(dtDetails.Rows(0).Item("tintCCManager") = 1, objCommon.GetManagerEmail, ""), ConfigurationManager.AppSettings("FromAddress"), objCommon.GetContactsEmail, dtMergeFields)
                            End If
                        End If
                    End If
                Catch ex As Exception

                End Try
                Session("CompID") = lngCMPID                                       'Set the Company Id in a session
                Session("DivID") = lnDivID
                Session("UserContactID") = lnCntID                                          'Set the Contact Id in a session
                Response.Redirect("frmOrder.aspx", True)                                                'Redirect to the screen created by anoop
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Function GetCustFldValue(ByVal fldType As String, ByVal fld_Id As String) As String
            Try
                If fldType = "EditBox" Then
                    Dim txt As TextBox
                    txt = Page.FindControl(fld_Id)
                    Return txt.Text
                ElseIf fldType = "SelectBox" Then
                    Dim ddl As DropDownList
                    ddl = Page.FindControl(fld_Id)
                    Return CStr(IIf(ddl.SelectedItem.Value = "", 0, ddl.SelectedItem.Value))
                ElseIf fldType = "TextBox" Then
                    Dim txt As TextBox
                    txt = Page.FindControl(fld_Id)
                    Return txt.Text
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function IsBoolean(ByVal sTrg As String) As Boolean
            Try
                Dim intValue As Boolean = Convert.ToBoolean(sTrg)                                   'Try Casting to a boolean
            Catch Ex As Exception
                Return False                                                                        'if it throws an error then return false to indicate that it is non boolean
            End Try
            Return True                                                                             'Test passed, its booelan
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This call returns True if the value passed is Integer else False
        ''' </summary>
        ''' <param name="sTrg">Represents the value that is being evaluated for being an Integer.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/08/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function IsNumeric(ByVal sTrg As String) As Boolean
            Try
                Dim intValue As Decimal = Convert.ToInt64(sTrg)                                     'Try Casting to a Integer
            Catch Ex As Exception
                Return False
            End Try
            Return True
        End Function

    End Class
End Namespace
