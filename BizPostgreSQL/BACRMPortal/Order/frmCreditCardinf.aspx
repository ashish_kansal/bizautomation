<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmCreditCardinf.aspx.vb" Inherits="BACRMPortal.frmCreditCardinf"%>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
		<title>Credit Card Information</title>
  </HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
			<br>
			<br>
			<br>
			<table width="650" align="center" border="0" cellpadding="5" cellspacing="5">
				<tr>
					<td align="right" class="text_bold" width=50%>Bill To Address:
					</td>
					<td class="normal1"><asp:Label id="lBillToo" Runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td align="right" class="text_bold">City:
					</td>
					<td class="normal1"><asp:Label id="lblCity" Runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td align="right" class="text_bold">State/Province:
					</td>
					<td class="normal1"><asp:Label id="lblState" Runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td align="right" class="text_bold">Zip/PostalCode:
					</td>
					<td class="normal1"><asp:Label id="lblZip" Runat="server"></asp:Label>
					</td>
				</tr>
				<tr bgColor="buttonshadow">
					<td align="center" colspan="2" class="normal5"><b>Plan Confirmation and Credit Card Information</b>
						
					</td>
				</tr>
				<tr>
					<td align="right" class="normal1" colspan="2"><b>Amount Due:</b>&nbsp;
						<asp:Label ID="lblAmount" Runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td class="normal1" colspan="2"><b>Credit Card Information</b>
					</td>
				</tr>
				</table>
				<table align=center  width="650" cellpadding="5" cellspacing="5">
				<tr>
					<td class="normal1" align=right nowrap ><b>Credit Card Number</b>
					</td>
					<td>
						<asp:TextBox ID="txtCreditCardNumber" Runat="server" CssClass="signup" Width=200></asp:TextBox>
					</td>
					<td class="normal1" align=right nowrap><b>Type</b><br>
					</td>
					<td>
						<asp:DropDownList ID="ddlCreditCardType" Runat="server" Width=130 CssClass=signup></asp:DropDownList>
					</td>
				</tr>
				<tr>
					<td class="normal1" align=right nowrap><b>Full Name on Credit Card</b><br>
					</td>
					<td>
						<asp:TextBox ID="txtFullName" Runat="server" CssClass="signup" Width=200></asp:TextBox>
					</td>
					<td class="normal1" align=right nowrap><b>Expiration date</b><br>
					</td>
					<td>
						<asp:TextBox ID="txtExpirationDate" Runat="server" CssClass="signup" Width=130></asp:TextBox>&nbsp;(MM/YY)
					</td>
				</tr>
				<tr>
					
					<td class="normal1" colspan=4 align=right >
						<asp:Button ID=btnChargeCreditCard Runat=server CssClass=Credit Text="Charge Credit Card"></asp:Button>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
