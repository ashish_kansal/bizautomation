<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmOrder.aspx.vb"
    Inherits="BACRMPortal.frmOrder" %>

<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../common/topbar.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <link href="../App_Themes/White/White.css" type="text/css" rel="stylesheet">
    <title>Order</title>

    <script language="javascript" type="text/javascript">
        function Add(a, b, c, d, e) {


            if (document.getElementById(c).value == "False") {
                if (e != '') {
                    var ddlIDs = e.split(",");
                    for (i = 0; i < ddlIDs.length; i++) {
                        if (document.getElementById('ctl00_CphPage_' + ddlIDs[i].split("~")[0]).value == "0") {
                            alert("Select " + ddlIDs[i].split("~")[1])
                            document.getElementById('ctl00_CphPage_' + ddlIDs[i].split("~")[0]).focus();
                            return false;
                        }
                    }
                }

            }
            if (document.getElementById(c).value != "") {
                if (document.getElementById(b) != null) {
                    if (document.getElementById(b).value == 0) {
                        alert("Item is not present in Inventory")

                        return false;
                    }
                }
            }

            if (document.getElementById(c).value != "") {
                if (document.getElementById(d).value != '') {
                    var ddlIDs = document.getElementById(d).value.split(",");
                    for (i = 0; i < ddlIDs.length; i++) {
                        if (ddlIDs[i] == document.getElementById(b).value) {
                            alert("This Item is already added to opportunity. Please Edit the details")
                            return false;
                        }
                    }
                }

            }

            if (document.getElementById(a).value == "") {
                alert("Enter Units")
                document.getElementById(a).focus();
                return false;
            }

        }
        function AddOption(a, b, c, d, e, f) {
            if (document.getElementById(e).value == 0) {
                alert("Select Option Item")
                document.getElementById(e).focus();
                return false;
            }
            if (document.getElementById(c).value == "False") {
                if (f != '') {
                    var ddlIDs = f.split(",");
                    for (i = 0; i < ddlIDs.length; i++) {
                        if (document.getElementById('ctl00_CphPage_' + ddlIDs[i].split("~")[0]).value == "0") {
                            alert("Select " + ddlIDs[i].split("~")[1])
                            document.getElementById('ctl00_CphPage_' + ddlIDs[i].split("~")[0]).focus();
                            return false;
                        }
                    }
                }

            }
            if (document.getElementById(c).value != "") {
                if (document.getElementById(b) != null) {
                    if (document.getElementById(b).value == 0) {
                        alert("Item is not present in inventory")
                        document.getElementById(b).focus();
                        return false;
                    }
                }

            }

            if (document.getElementById(c).value != "") {
                if (document.getElementById(d).value != '') {
                    var ddlIDs = document.getElementById(d).value.split(",");
                    for (i = 0; i < ddlIDs.length; i++) {
                        if (ddlIDs[i] == document.getElementById(b).value) {
                            alert("This Item is already added to opportunity. Please Edit the details")
                            return false;
                        }
                    }
                }

            }
            if (document.getElementById(a).value == "") {
                alert("Enter Units")
                document.getElementById(a).focus();
                return false;
            }
        }
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <menu1:webmenu ID="webmenu1" runat="server"></menu1:webmenu>
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td valign="bottom">
                <table class="TabStyle">
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;Create Order&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td class="normal1" align="right">
                Customer :
            </td>
            <td>
                <asp:Label ID="lblCustomer" runat="server" CssClass="text"></asp:Label>
            </td>
            <td align="right">
                <asp:Button ID="btnPlaceOrder" runat="server" CssClass="button" Text="Place Order">
                </asp:Button>
                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel"></asp:Button>
            </td>
        </tr>
    </table>
    <asp:Table ID="table10" runat="server" BorderWidth="1" GridLines="None" BorderColor="black"
        CellPadding="0" CellSpacing="0" BackColor="white" Width="100%" Height="300">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:GridView ID="dgBasket" runat="server" AutoGenerateColumns="False" Width="100%">
                    <AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is" />
                    <HeaderStyle CssClass="hs" />
                    <EmptyDataTemplate>
                        &nbsp;-- No items in your basket --
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:BoundField DataField="numItemCode" Visible="False"></asp:BoundField>
                        <asp:BoundField HeaderText="Item" DataField="vcItemName"></asp:BoundField>
                        <asp:BoundField HeaderText="Units" DataField="numUnitHour"></asp:BoundField>
                        <asp:BoundField HeaderText="Price/Unit" DataField="monPrice" DataFormatString="{0:#,#0.00}">
                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Total" DataField="monTotAmount" DataFormatString="{0:C}">
                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        </asp:BoundField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="Delete" Text="X" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <br />
                <table border="0" width="100%">
                    <tr>
                        <td class="normal1">
                            Item&nbsp;
                            <asp:DropDownList ID="ddlItem" AutoPostBack="true" runat="server" CssClass="signup">
                            </asp:DropDownList>
                            &nbsp;&nbsp;List Price : $&nbsp;<asp:Label ID="lblUnitCost" runat="server"></asp:Label><asp:Label
                                ID="lblWeight" runat="server" Visible="False"></asp:Label>
                            <asp:Label ID="lblDiscountPrice" runat="server" Visible="False" Font-Bold="True"
                                ForeColor="#C00000"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblDescription" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Table ID="tblItemAttr" runat="server">
                            </asp:Table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Image ID="imgProduct" runat="server" />
                            <asp:Label ID="lblPrice" runat="server" Visible="False"></asp:Label>
                            <asp:Label ID="lblDPrice" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1">
                            No of Units
                            <asp:TextBox ID="txtUnits" runat="server" Width="90px"></asp:TextBox>
                            &nbsp;<asp:LinkButton ID="lnkAddToCart" runat="server">Add To Cart</asp:LinkButton>
                            <br />
                            <asp:Label ID="lblOutOfStock" runat="server" ForeColor="Gray" EnableViewState="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblText" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table id="tblOptSelection" runat="server" width="100%" visible="false">
                                <tr>
                                    <td colspan="2">
                                        <b>Choose Options And Accessories</b>
                                        <br />
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" rowspan="111111115">
                                        <asp:Image ID="imgOptItem" runat="server" BorderWidth="1" Width="100" BorderColor="black"
                                            Height="100"></asp:Image><br>
                                        <asp:HyperLink ID="hplOptImage" runat="server" CssClass="hyperlink" NavigateUrl="">View Full Image</asp:HyperLink>
                                    </td>
                                    <td>
                                        <table id="Table2" runat="server">
                                            <tr>
                                                <td class="normal1" valign="top" align="right">
                                                    Option Item
                                                </td>
                                                <td class="normal1" valign="top" colspan="5">
                                                    <asp:DropDownList ID="ddlOptItem" TabIndex="19" runat="server" CssClass="signup"
                                                        AutoPostBack="true">
                                                    </asp:DropDownList>
                                                    &nbsp;&nbsp;List Price : $ &nbsp;<asp:Label ID="lstOptPrice" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="6" style="white-space: nowrap">
                                                    <asp:Table ID="tblOptItemAttr" runat="server">
                                                    </asp:Table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblOptDesc" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="normal1" align="right">
                                                    No of Units&nbsp;&nbsp;&nbsp;
                                                </td>
                                                <td class="normal1" nowrap>
                                                    <asp:TextBox ID="txtOptUnits" runat="server" CssClass="signup" Width="90px"></asp:TextBox>
                                                    <asp:LinkButton ID="lnkOptItemAdd" runat="server">Add To Cart</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br />
                <asp:TextBox ID="txtHidValue" runat="server" Style="display: none"></asp:TextBox>
                <asp:TextBox ID="txtHidEditOppItem" runat="server" Style="display: none"></asp:TextBox>
                <asp:TextBox ID="txtHidOptValue" runat="server" Style="display: none"></asp:TextBox>
                <asp:TextBox ID="txtAddedItems" runat="server" Style="display: none"></asp:TextBox>
                <br />
                <br />
                <br />
                <asp:DropDownList ID="ddlWarehouse" runat="server" CssClass="signup" Style="display: none">
                </asp:DropDownList>
                <br />
                <asp:DropDownList ID="ddlOptWarehouse" runat="server" CssClass="signup" Style="display: none">
                </asp:DropDownList>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
