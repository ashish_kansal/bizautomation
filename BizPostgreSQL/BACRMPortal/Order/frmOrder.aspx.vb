Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common

Partial Class frmOrder : Inherits BACRMPage

    Dim objCommon As New CCommon
    Dim strValues As String
    Dim dtOppAtributes As DataTable
    Dim dsTemp As DataSet
    Dim dtOppOptAttributes As DataTable

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Dim lngDivID, lngContId, lngOppID As Long

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not IsPostBack Then
                Session("Data") = Nothing
                objCommon.sb_FillItemFromDB(ddlItem, Session("DomainId"))
                lblCustomer.Text = Session("CompName")
                imgProduct.Visible = False
            End If
            If ddlItem.SelectedValue > 0 And txtHidValue.Text <> "" Then
                CreateAttributes()
            Else : lnkAddToCart.Attributes.Add("onclick", "return Add('" & txtUnits.ClientID & "','" & ddlWarehouse.ClientID & "','" & txtHidValue.ClientID & "','" & txtAddedItems.ClientID & "','')")
            End If
            If ddlOptItem.SelectedIndex > 0 And txtHidOptValue.Text <> "" Then
                CreateOptAttributes()
            Else : lnkOptItemAdd.Attributes.Add("onclick", "return AddOption('" & txtOptUnits.ClientID & "','" & ddlOptWarehouse.ClientID & "','" & txtHidOptValue.ClientID & "','" & txtAddedItems.ClientID & "','" & ddlOptItem.ClientID & "','')")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub createSet()
        Try
            dsTemp = New DataSet
            Dim dtItem As New DataTable
            dtItem.Columns.Add("numoppitemtCode")
            dtItem.Columns.Add("numItemCode")
            dtItem.Columns.Add("numUnitHour")
            dtItem.Columns.Add("monPrice")
            dtItem.Columns.Add("monTotAmount", GetType(Decimal))
            dtItem.Columns.Add("numSourceID")
            dtItem.Columns.Add("vcItemDesc")
            dtItem.Columns.Add("numWarehouseID")
            dtItem.Columns.Add("vcItemName")
            dtItem.Columns.Add("Warehouse")
            dtItem.Columns.Add("numWarehouseItmsID")
            dtItem.Columns.Add("ItemType")
            dtItem.Columns.Add("Attributes")
            dtItem.Columns.Add("AttrValues")
            dtItem.Columns.Add("Op_Flag")
            dtItem.TableName = "Item"
            dsTemp.Tables.Add(dtItem)
            dtItem.PrimaryKey = New DataColumn() {dtItem.Columns("numoppitemtCode")}
            Session("Data") = dsTemp
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub CreateAttributes()
        Try
            Dim strAttributes As String = ""
            strValues = ""
            Dim boolLoadNextdropdown As Boolean
            Dim objItems As New CItems
            objItems.ItemCode = ddlItem.SelectedValue
            dtOppAtributes = objItems.GetOppItemAttributes()
            If dtOppAtributes.Rows.Count > 0 Then
                Dim i As Integer
                Dim lbl As Label
                Dim tblrow As TableRow
                Dim tblcell As TableCell
                Dim ddl As DropDownList
                tblrow = New TableRow
                For i = 0 To dtOppAtributes.Rows.Count - 1
                    tblcell = New TableCell
                    tblcell.CssClass = "normal1"
                    tblcell.HorizontalAlign = HorizontalAlign.Right
                    lbl = New Label
                    lbl.Text = dtOppAtributes.Rows(i).Item("Fld_label") & IIf(txtHidValue.Text = "True", "", "<font color='red'>*</font>")
                    tblcell.Controls.Add(lbl)
                    tblrow.Cells.Add(tblcell)
                    If dtOppAtributes.Rows(i).Item("fld_type") = "Drop Down List Box" Then
                        tblcell = New TableCell
                        ddl = New DropDownList
                        ddl.CssClass = "signup"
                        ddl.ID = "Attr" & dtOppAtributes.Rows(i).Item("fld_id")
                        tblcell.Controls.Add(ddl)
                        ddl.AutoPostBack = True
                        AddHandler ddl.SelectedIndexChanged, AddressOf Me.LoadItemsonChangeofAttr
                        tblrow.Cells.Add(tblcell)
                        strAttributes = strAttributes & ddl.ID & "~" & dtOppAtributes.Rows(i).Item("Fld_label") & ","
                        If ddl.SelectedIndex > 0 Then strValues = strValues & ddl.SelectedValue & ","
                        If i = 0 Then
                            objCommon.sb_FillAttibuesFromDB(ddl, dtOppAtributes.Rows(i).Item("numlistid"), ddlItem.SelectedValue, "")
                        Else : objCommon.sb_FillAttibuesFromDB(ddl, IIf(boolLoadNextdropdown = True, dtOppAtributes.Rows(i).Item("numlistid"), 0), ddlItem.SelectedValue, strValues.TrimEnd(","))
                        End If
                        If ddl.SelectedIndex > 0 Then boolLoadNextdropdown = True
                    End If
                Next
                strAttributes = strAttributes.TrimEnd(",")
                lnkAddToCart.Attributes.Add("onclick", "return Add('" & txtUnits.ClientID & "','" & ddlWarehouse.ClientID & "','" & txtHidValue.ClientID & "','" & txtAddedItems.ClientID & "','" & strAttributes & "')")
                tblItemAttr.Rows.Add(tblrow)
            Else
                If ddlWarehouse.Items.Count = 0 Then
                    ddlWarehouse.DataSource = objCommon.GetWarehouseOnAttrSel(ddlItem.SelectedValue, "")
                    ddlWarehouse.DataTextField = "vcWareHouse"
                    ddlWarehouse.DataValueField = "numWareHouseItemId"
                    ddlWarehouse.DataBind()
                    ddlWarehouse.Items.Insert(0, "--Select One--")
                    ddlWarehouse.Items.FindByText("--Select One--").Value = "0"
                    If ddlWarehouse.Items.Count > 1 Then
                        ddlWarehouse.SelectedIndex = 1
                    Else : Session("onHand") = 0
                    End If
                End If
                lnkAddToCart.Attributes.Add("onclick", "return Add('" & txtUnits.ClientID & "','" & ddlWarehouse.ClientID & "','" & txtHidValue.ClientID & "','" & txtAddedItems.ClientID & "','')")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadItemsonChangeofAttr(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            strValues = ""
            Dim i As Integer
            Dim ddl As DropDownList
            Dim boolEnable As Boolean = True
            Dim boolLoadnextDropdown As Boolean = False
            Dim senderID, controlID As String
            senderID = sender.ID
            For i = 0 To dtOppAtributes.Rows.Count - 1
                controlID = "Attr" & dtOppAtributes.Rows(i).Item("fld_id")
                ddl = tblItemAttr.FindControl(controlID)
                If ddl.SelectedIndex > 0 And boolLoadnextDropdown = False Then strValues = strValues & ddl.SelectedValue & ","
                If boolLoadnextDropdown = True Then
                    objCommon.sb_FillAttibuesFromDB(ddl, dtOppAtributes.Rows(i).Item("numlistid"), ddlItem.SelectedValue, strValues.TrimEnd(","))
                    boolLoadnextDropdown = False
                End If
                If senderID = controlID Then
                    boolLoadnextDropdown = True
                    If (i = dtOppAtributes.Rows.Count - 1 And ddl.SelectedIndex > 0) Or txtHidValue.Text = "True" Then
                        Dim lngWareHouseTempID As Long
                        If ddlWarehouse.SelectedIndex > 0 Then
                            lngWareHouseTempID = ddlWarehouse.SelectedValue
                        End If
                        ddlWarehouse.DataSource = objCommon.GetWarehouseOnAttrSel(ddlItem.SelectedValue, strValues.TrimEnd(","))
                        ddlWarehouse.DataTextField = "vcWareHouse"
                        ddlWarehouse.DataValueField = "numWareHouseItemId"
                        ddlWarehouse.DataBind()
                        ddlWarehouse.Items.Insert(0, "--Select One--")
                        ddlWarehouse.Items.FindByText("--Select One--").Value = "0"
                        If lngWareHouseTempID > 0 Then
                            If Not ddlWarehouse.Items.FindByValue(lngWareHouseTempID) Is Nothing Then
                                ddlWarehouse.Items.FindByValue(lngWareHouseTempID).Selected = True
                            End If
                        ElseIf ddlWarehouse.Items.Count > 1 Then
                            ddlWarehouse.SelectedIndex = 1
                        End If
                    Else : ddlWarehouse.Items.Clear()
                    End If
                End If
            Next
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkAddToCart_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddToCart.Click
        Try
            dsTemp = Session("Data")
            Dim dtItem As DataTable
            dtItem = dsTemp.Tables(0)
            Dim dr As DataRow
            dr = dtItem.NewRow
            dr("numoppitemtCode") = CType(IIf(IsDBNull(dtItem.Compute("MAX(numoppitemtCode)", "")), 0, dtItem.Compute("MAX(numoppitemtCode)", "")), Integer) + 1
            dr("numItemCode") = ddlItem.SelectedValue
            dr("numUnitHour") = txtUnits.Text

            Dim objPbook As New PriceBookRule
            objPbook.ItemID = ddlItem.SelectedValue
            objPbook.QntyofItems = dr("numUnitHour")
            objPbook.DomainID = Session("DomainID")
            objPbook.DivisionID = Session("DivId")
            Dim dtCalPrice As DataTable
            dtCalPrice = objPbook.GetPriceBasedonPriceBook
            If dtCalPrice.Rows.Count > 0 Then
                dr("monPrice") = dtCalPrice.Rows(0).Item("DiscItemPrice")
            Else : dr("monPrice") = lblUnitCost.Text
            End If

            dr("monTotAmount") = IIf(IsDBNull(dr("numUnitHour")), 0, dr("numUnitHour")) * IIf(IsDBNull(dr("monPrice")), 0, dr("monPrice"))
            dr("vcItemDesc") = ""
            dr("numWarehouseID") = ""
            dr("vcItemName") = ddlItem.SelectedItem.Text
            If txtHidValue.Text <> "" Then
                dr("Warehouse") = ddlWarehouse.SelectedItem.Text
                dr("numWarehouseItmsID") = ddlWarehouse.SelectedItem.Value
                If txtHidValue.Text = "False" Then dr("Attributes") = objCommon.GetAttributesForWarehouseItem(dr("numWarehouseItmsID"), 0)
                Dim i As Integer
                Dim controlID As String
                Dim ddl As DropDownList
                For i = 0 To dtOppAtributes.Rows.Count - 1
                    controlID = "Attr" & dtOppAtributes.Rows(i).Item("fld_id")
                    ddl = tblItemAttr.FindControl(controlID)
                    If ddl.SelectedIndex > 0 Then strValues = strValues & ddl.SelectedValue & ","
                Next
                dr("AttrValues") = strValues.TrimEnd(",")
            End If
            dr("ItemType") = ""
            dr("Op_Flag") = 1
            dtItem.Rows.Add(dr)
            dsTemp.AcceptChanges()
            dgBasket.DataSource = dsTemp.Tables(0)
            dgBasket.DataBind()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlOptItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOptItem.SelectedIndexChanged
        Try
            LoadOptItemDetails()
            If ddlOptItem.SelectedIndex > 0 And txtHidOptValue.Text <> "" Then
                tblOptItemAttr.Controls.Clear()
                CreateOptAttributes()
            Else : lnkOptItemAdd.Attributes.Add("onclick", "return AddOption('" & txtOptUnits.ClientID & "','" & ddlOptWarehouse.ClientID & "','" & txtHidOptValue.ClientID & "','" & txtAddedItems.ClientID & "','" & ddlOptItem.ClientID & "','')")
            End If
            dsTemp = Session("Data")
            Dim dtTable As DataTable
            dtTable = dsTemp.Tables(0)
            Dim dr As DataRow
            txtAddedItems.Text = ""
            For Each dr In dtTable.Rows
                txtAddedItems.Text = txtAddedItems.Text & dr("numWarehouseItmsID") & ","
            Next
            txtAddedItems.Text = txtAddedItems.Text.Trim(",")
            If ddlOptItem.SelectedIndex > 0 And txtHidOptValue.Text <> "" Then
                CreateOptAttributes()
            Else : lnkOptItemAdd.Attributes.Add("onclick", "return AddOption('" & txtOptUnits.ClientID & "','" & ddlOptWarehouse.ClientID & "','" & txtHidOptValue.ClientID & "','" & txtAddedItems.ClientID & "','" & ddlOptItem.ClientID & "','')")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadOptItemDetails()
        Try
            If ddlOptItem.SelectedIndex > 0 Then
                Dim dtTable As DataTable
                Dim objItems As New CItems
                objItems.ItemCode = ddlOptItem.SelectedItem.Value
                dtTable = objItems.ItemDetails
                If dtTable.Rows.Count > 0 Then
                    lstOptPrice.Text = dtTable.Rows(0).Item("monListPrice")
                    lblOptDesc.Text = dtTable.Rows(0).Item("txtItemDesc")
                    If Not IsDBNull(dtTable.Rows(0).Item("vcPathForImage")) Then
                        If dtTable.Rows(0).Item("vcPathForImage") <> "" Then
                            imgOptItem.Visible = True
                            hplOptImage.Visible = True
                            imgOptItem.ImageUrl = "../Documents/Docs/" & dtTable.Rows(0).Item("vcPathForImage")
                            hplOptImage.Attributes.Add("onclick", "return OpenImage(" & ddlItem.SelectedValue & ")")
                        Else
                            imgOptItem.Visible = False
                            hplOptImage.Visible = False
                        End If
                    Else
                        imgOptItem.Visible = False
                        hplOptImage.Visible = False
                    End If
                    If dtTable.Rows(0).Item("charItemType") = "P" Or dtTable.Rows(0).Item("charItemType") = "A" Then
                        txtOptUnits.Visible = True
                        txtHidOptValue.Text = "False"
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub CreateOptAttributes()
        Try
            Dim strAttributes As String = ""
            strValues = ""
            Dim boolLoadNextdropdown As Boolean
            Dim objItems As New CItems
            objItems.ItemCode = ddlOptItem.SelectedValue
            dtOppOptAttributes = objItems.GetOppItemAttributes()
            If dtOppOptAttributes.Rows.Count > 0 Then
                Dim i As Integer
                Dim lbl As Label
                Dim tblrow As TableRow
                Dim tblcell As TableCell
                Dim ddl As DropDownList
                tblrow = New TableRow
                For i = 0 To dtOppAtributes.Rows.Count - 1
                    tblcell = New TableCell
                    tblcell.CssClass = "normal1"
                    tblcell.HorizontalAlign = HorizontalAlign.Right
                    lbl = New Label
                    lbl.Text = dtOppOptAttributes.Rows(i).Item("Fld_label") & IIf(txtHidOptValue.Text = "True", "", "<font color='red'>*</font>")
                    tblcell.Controls.Add(lbl)
                    tblrow.Cells.Add(tblcell)

                    If dtOppOptAttributes.Rows(i).Item("fld_type") = "Drop Down List Box" Then
                        tblcell = New TableCell
                        ddl = New DropDownList
                        ddl.CssClass = "signup"
                        ddl.ID = "AttrOpp" & dtOppOptAttributes.Rows(i).Item("fld_id")

                        tblcell.Controls.Add(ddl)
                        ddl.AutoPostBack = True
                        AddHandler ddl.SelectedIndexChanged, AddressOf Me.LoadOptItemsonChangeofAttr
                        tblrow.Cells.Add(tblcell)
                        strAttributes = strAttributes & ddl.ID & "~" & dtOppOptAttributes.Rows(i).Item("Fld_label") & ","
                        If ddl.SelectedIndex > 0 Then strValues = strValues & ddl.SelectedValue & ","
                        If i = 0 Then
                            objCommon.sb_FillAttibuesFromDB(ddl, dtOppOptAttributes.Rows(i).Item("numlistid"), ddlOptItem.SelectedValue, "")
                        Else : objCommon.sb_FillAttibuesFromDB(ddl, IIf(boolLoadNextdropdown = True, dtOppOptAttributes.Rows(i).Item("numlistid"), 0), ddlOptItem.SelectedValue, strValues.TrimEnd(","))
                        End If
                        If ddl.SelectedIndex > 0 Then boolLoadNextdropdown = True
                    End If
                Next
                strAttributes = strAttributes.TrimEnd(",")
                lnkOptItemAdd.Attributes.Add("onclick", "return AddOption('" & txtOptUnits.ClientID & "','" & ddlOptWarehouse.ClientID & "','" & txtHidOptValue.ClientID & "','" & txtAddedItems.ClientID & "','" & ddlOptItem.ClientID & "','" & strAttributes & "')")
                tblOptItemAttr.Rows.Add(tblrow)
            Else
                If ddlOptWarehouse.Items.Count = 0 Then
                    ddlOptWarehouse.DataSource = objCommon.GetWarehouseOnAttrSel(ddlOptItem.SelectedValue, "")
                    ddlOptWarehouse.DataTextField = "vcWareHouse"
                    ddlOptWarehouse.DataValueField = "numWareHouseItemId"
                    ddlOptWarehouse.DataBind()
                    ddlOptWarehouse.Items.Insert(0, "--Select One--")
                    ddlOptWarehouse.Items.FindByText("--Select One--").Value = "0"
                    If ddlOptWarehouse.Items.Count > 1 Then ddlOptWarehouse.SelectedIndex = 1
                End If
                lnkOptItemAdd.Attributes.Add("onclick", "return AddOption('" & txtOptUnits.ClientID & "','" & ddlOptWarehouse.ClientID & "','" & txtHidOptValue.ClientID & "','" & txtAddedItems.ClientID & "','" & ddlOptItem.ClientID & "','')")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadOptItemsonChangeofAttr(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            strValues = ""
            Dim i As Integer
            Dim ddl As DropDownList
            Dim boolEnable As Boolean = True
            Dim boolLoadnextDropdown As Boolean = False
            Dim senderID, controlID As String
            senderID = sender.ID
            For i = 0 To dtOppOptAttributes.Rows.Count - 1
                controlID = "AttrOpp" & dtOppOptAttributes.Rows(i).Item("fld_id")
                ddl = tblOptItemAttr.FindControl(controlID)
                If ddl.SelectedIndex > 0 And boolLoadnextDropdown = False Then strValues = strValues & ddl.SelectedValue & ","
                If boolLoadnextDropdown = True Then
                    objCommon.sb_FillAttibuesFromDB(ddl, dtOppOptAttributes.Rows(i).Item("numlistid"), ddlOptItem.SelectedValue, strValues.TrimEnd(","))
                    boolLoadnextDropdown = False
                End If
                If senderID = controlID Then
                    boolLoadnextDropdown = True
                    If (i = dtOppOptAttributes.Rows.Count - 1 And ddl.SelectedIndex > 0) Or txtHidOptValue.Text = "True" Then
                        Dim lngWareHouseTempID As Long
                        If ddlOptWarehouse.SelectedIndex > 0 Then lngWareHouseTempID = ddlOptWarehouse.SelectedValue
                        ddlOptWarehouse.DataSource = objCommon.GetWarehouseOnAttrSel(ddlOptItem.SelectedValue, strValues.TrimEnd(","))
                        ddlOptWarehouse.DataTextField = "vcWareHouse"
                        ddlOptWarehouse.DataValueField = "numWareHouseItemId"
                        ddlOptWarehouse.DataBind()
                        ddlOptWarehouse.Items.Insert(0, "--Select One--")
                        ddlOptWarehouse.Items.FindByText("--Select One--").Value = "0"
                        If lngWareHouseTempID > 0 Then
                            If Not ddlOptWarehouse.Items.FindByValue(lngWareHouseTempID) Is Nothing Then
                                ddlOptWarehouse.Items.FindByValue(lngWareHouseTempID).Selected = True
                            End If
                        ElseIf ddlOptWarehouse.Items.Count > 1 Then
                            ddlOptWarehouse.SelectedIndex = 1
                        End If
                    Else : ddlOptWarehouse.Items.Clear()
                    End If
                End If
            Next
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkOptItemAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkOptItemAdd.Click
        Try
            dsTemp = Session("Data")
            Dim dtItem As DataTable
            dtItem = dsTemp.Tables(0)
            Dim dr As DataRow
            dr = dtItem.NewRow
            dr("numoppitemtCode") = CType(IIf(IsDBNull(dtItem.Compute("MAX(numoppitemtCode)", "")), 0, dtItem.Compute("MAX(numoppitemtCode)", "")), Integer) + 1
            dr("numItemCode") = ddlOptItem.SelectedValue
            dr("numUnitHour") = txtOptUnits.Text

            Dim objPbook As New PriceBookRule
            objPbook.ItemID = ddlOptItem.SelectedValue
            objPbook.QntyofItems = dr("numUnitHour")
            objPbook.DomainID = Session("DomainID")
            objPbook.DivisionID = Session("DivId")
            Dim dtCalPrice As DataTable
            dtCalPrice = objPbook.GetPriceBasedonPriceBook
            If dtCalPrice.Rows.Count > 0 Then
                dr("monPrice") = dtCalPrice.Rows(0).Item("DiscItemPrice")
            Else : dr("monPrice") = lstOptPrice.Text
            End If

            dr("monTotAmount") = IIf(IsDBNull(dr("numUnitHour")), 0, dr("numUnitHour")) * IIf(IsDBNull(dr("monPrice")), 0, dr("monPrice"))
            dr("vcItemDesc") = lblOptDesc.Text
            dr("numWarehouseID") = ""
            dr("vcItemName") = ddlOptItem.SelectedItem.Text
            dr("Warehouse") = ddlOptWarehouse.SelectedItem.Text
            dr("numWarehouseItmsID") = ddlOptWarehouse.SelectedItem.Value
            dr("ItemType") = "Options & Accessories "
            dr("Attributes") = objCommon.GetAttributesForWarehouseItem(dr("numWarehouseItmsID"), 0)
            dtItem.Rows.Add(dr)
            dsTemp.AcceptChanges()
            ddlOptItem.SelectedIndex = 0
            tblOptItemAttr.Controls.Clear()
            ddlOptWarehouse.Items.Clear()
            lblOptDesc.Text = ""
            txtOptUnits.Text = ""
            dgBasket.DataSource = dsTemp.Tables(0)
            dgBasket.DataBind()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlItem.SelectedIndexChanged
        Try
            tblItemAttr.Controls.Clear()
            If ddlItem.SelectedValue > 0 Then
                If Session("Data") Is Nothing Then createSet()
                Dim objItems As New CItems
                Dim dtItemDetails As DataTable
                objItems.ItemCode = ddlItem.SelectedValue
                dtItemDetails = objItems.ItemDetails
                hplOptImage.Visible = False
                imgOptItem.Visible = False
                If dtItemDetails.Rows.Count > 0 Then
                    lblDescription.Text = IIf(IsDBNull(dtItemDetails.Rows(0).Item("txtItemDesc")), "", dtItemDetails.Rows(0).Item("txtItemDesc"))
                    If Not IsDBNull(dtItemDetails.Rows(0).Item("monListPrice")) Then
                        lblUnitCost.Text = String.Format("{0:#,##0.00}", dtItemDetails.Rows(0).Item("monListPrice"))
                    End If
                    Session("onHand") = String.Format("{0:#,##0}", dtItemDetails.Rows(0).Item("numOnHand"))
                    If Not IsDBNull(dtItemDetails.Rows(0).Item("vcPathForImage")) Then
                        If dtItemDetails.Rows(0).Item("vcPathForImage") <> "" Then
                            imgProduct.ImageUrl = "../Documents/Docs/" & dtItemDetails.Rows(0).Item("vcPathForImage")
                            imgProduct.Visible = True
                        Else : imgProduct.Visible = False
                        End If
                    Else : imgProduct.Visible = False
                    End If
                    If dtItemDetails.Rows(0).Item("charItemType") = "P" Or dtItemDetails.Rows(0).Item("charItemType") = "A" Then
                        If dtItemDetails.Rows(0).Item("bitSerialized") = True Then
                            txtHidValue.Text = "True"
                            ddlWarehouse.DataSource = objCommon.GetWarehouseOnAttrSel(ddlItem.SelectedValue, "")
                            ddlWarehouse.DataTextField = "vcWareHouse"
                            ddlWarehouse.DataValueField = "numWareHouseItemId"
                            ddlWarehouse.DataBind()
                            ddlWarehouse.Items.Insert(0, "--Select One--")
                            ddlWarehouse.Items.FindByText("--Select One--").Value = "0"
                            If ddlWarehouse.Items.Count > 1 Then ddlWarehouse.SelectedIndex = 1
                        Else
                            txtHidValue.Text = "False"
                            ddlWarehouse.DataSource = objCommon.GetWarehouseOnAttrSel(ddlItem.SelectedValue, "")
                            ddlWarehouse.DataTextField = "vcWareHouse"
                            ddlWarehouse.DataValueField = "numWareHouseItemId"
                            ddlWarehouse.DataBind()
                            ddlWarehouse.Items.Insert(0, "--Select One--")
                            ddlWarehouse.Items.FindByText("--Select One--").Value = "0"
                            If ddlWarehouse.Items.Count > 1 Then ddlWarehouse.SelectedIndex = 1
                        End If
                    Else : txtHidValue.Text = ""
                    End If
                End If
                Dim dtTable As DataTable
                objItems.ItemCode = ddlItem.SelectedValue
                dtTable = objItems.GetOptAccWareHouses
                If dtTable.Rows.Count > 0 Then
                    tblOptSelection.Visible = True
                    ddlOptItem.DataSource = dtTable
                    ddlOptItem.DataTextField = "vcItemName"
                    ddlOptItem.DataValueField = "numItemCode"
                    ddlOptItem.DataBind()
                    ddlOptItem.Items.Insert(0, "--Select One--")
                    ddlOptItem.Items.FindByText("--Select One--").Value = "0"
                Else : tblOptSelection.Visible = False
                End If

                ''Header Text
                objItems.byteMode = 0
                objItems.ItemCode = GetQueryStringVal( "ItemID")
                objItems.UserCntId = Session("UserContactID")
                lblText.Text = objItems.ManageItemExtendedDesc
                If ddlItem.SelectedValue > 0 And txtHidValue.Text <> "" Then
                    CreateAttributes()
                Else : lnkAddToCart.Attributes.Add("onclick", "return Add('" & txtUnits.ClientID & "','" & ddlWarehouse.ClientID & "','" & txtHidValue.ClientID & "','" & txtAddedItems.ClientID & "','')")
                End If
            Else : imgProduct.Visible = False
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Session("Data") = Nothing
            Response.Redirect(Request.Url.PathAndQuery)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnPlaceOrder_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPlaceOrder.Click
        Try
            Response.Redirect("../Order/frmBillDetails.aspx")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class
