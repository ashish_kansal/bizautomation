Option Explicit On
Option Strict Off

Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Common

'Namespace BACRM.BusinessLogic.Common
Public Class BACRMPage
    Inherits System.Web.UI.Page


    'Declare common variables and properties used in all pages here
    Public objCommon As New CCommon
    Public m_aryRightsForPage(4) As Integer

    Private Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        Try
            'Set Currunt page url in session, so we can load help for current page in help management
            Session("Help") = Request.Url.Segments(Request.Url.Segments.Length - 1)
            'Throw user out when domainid = 0
            If Request.Url.Segments(Request.Url.Segments.Length - 1).ToUpper() <> "LOGIN.ASPX" And Request.Url.Segments(Request.Url.Segments.Length - 1).ToUpper() <> "ENDSESSION.ASPX" Then
                If CCommon.ToLong(Session("DomainID")) = 0 Then
                    Response.Write("<script language='javascript'> if (parent.frames['mainframe'] !=null) { parent.frames['mainframe'].location.href='../LogOff.htm' } else { document.location.href='../LogOff.htm'}</script>")
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(Session("DomainID")), CCommon.ToLong(Session("UserContactID")), Request)
            Response.Write(ex)
        End Try
    End Sub
    Public Sub AddToRecentlyViewed(ByVal RecordType As RecetlyViewdRecordType, RecordID As Long)
        Try
            If objCommon Is Nothing Then objCommon = New CCommon
            Select Case RecordType
                Case RecetlyViewdRecordType.Account_Pospect_Lead
                    objCommon.charModule = "C"
                Case RecetlyViewdRecordType.ActionItem
                    objCommon.charModule = "A"
                Case RecetlyViewdRecordType.Document
                    objCommon.charModule = "D"
                Case RecetlyViewdRecordType.Opportunity
                    objCommon.charModule = "O"
                Case RecetlyViewdRecordType.Support
                    objCommon.charModule = "S"
                Case RecetlyViewdRecordType.Project
                    objCommon.charModule = "P"
                Case RecetlyViewdRecordType.AssetItem
                    objCommon.charModule = "AI"
                Case RecetlyViewdRecordType.Contact
                    objCommon.charModule = "U"
                Case RecetlyViewdRecordType.Item
                    objCommon.charModule = "I"
                Case Else
            End Select
            objCommon.RecordId = RecordID
            objCommon.UserCntID = CCommon.ToLong(Session("UserContactID"))
            objCommon.AddVisiteddetails()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function GetUserRightsForPage(ByVal numModuleID As MODULEID, ByVal numPageID As Long) As Array
        Try
            'Check page access rights from Cached XML for current auth group
            Dim strApplicationDataSetName As String = "UserAuthRightsDataSet_" & CCommon.ToLong(Session("DomainID")).ToString()


            Dim dsAuthDetail As DataSet
            If Application(strApplicationDataSetName) Is Nothing Then
                objCommon.DomainID = CCommon.ToLong(Session("DomainID"))
                dsAuthDetail = objCommon.GetAuthDetailsForDomain()
                If dsAuthDetail.Tables(0).Rows.Count > 0 Then
                    Application(strApplicationDataSetName) = dsAuthDetail.Copy()
                    dsAuthDetail.Clear()
                End If
            End If
            Dim dr As DataRow
            If Not Application(strApplicationDataSetName) Is Nothing Then
                dsAuthDetail = CType(Application(strApplicationDataSetName), DataSet)
                If dsAuthDetail.Tables(0).Rows.Count > 0 Then
                    Dim drArray() As DataRow = dsAuthDetail.Tables(0).Select("numGroupID='" + CCommon.ToString(Session("GroupId")) + "' and numModuleID='" + CCommon.ToInteger(numModuleID).ToString() + "' and numPageID='" + CCommon.ToString(numPageID) + "'", "")
                    If drArray.Length = 1 Then
                        dr = drArray(0)
                        m_aryRightsForPage(RIGHTSTYPE.VIEW) = CInt(dr("intViewAllowed"))
                        m_aryRightsForPage(RIGHTSTYPE.ADD) = CInt(dr("intAddAllowed"))
                        m_aryRightsForPage(RIGHTSTYPE.DELETE) = CInt(dr("intDeleteAllowed"))
                        m_aryRightsForPage(RIGHTSTYPE.EXPORT) = CInt(dr("intExportAllowed"))
                        m_aryRightsForPage(RIGHTSTYPE.UPDATE) = CInt(dr("intUpdateAllowed"))

                    End If
                End If
            End If
            If dr Is Nothing Then
                m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0
                m_aryRightsForPage(RIGHTSTYPE.ADD) = 0
                m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0
                m_aryRightsForPage(RIGHTSTYPE.EXPORT) = 0
                m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0
            End If


            'When user has no view rights, show warning message
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AC")
            End If

            Return m_aryRightsForPage
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetUserRightsForPage_Other(ByVal numModuleID As MODULEID, ByVal numPageID As Long, Optional ByRef ModuleName As String = "", Optional ByRef PermissionName As String = "") As Array
        Try
            Dim m_aryRightsForPageTemp(4) As Integer

            'Check page access rights from Cached XML for current auth group
            Dim strApplicationDataSetName As String = "UserAuthRightsDataSet_" & CCommon.ToLong(Session("DomainID")).ToString()


            Dim dsAuthDetail As DataSet
            If Application(strApplicationDataSetName) Is Nothing Then
                objCommon.DomainID = CCommon.ToLong(Session("DomainID"))
                dsAuthDetail = objCommon.GetAuthDetailsForDomain()
                If dsAuthDetail.Tables(0).Rows.Count > 0 Then
                    Application(strApplicationDataSetName) = dsAuthDetail.Copy()
                    dsAuthDetail.Clear()
                End If
            End If
            Dim dr As DataRow
            If Not Application(strApplicationDataSetName) Is Nothing Then
                dsAuthDetail = CType(Application(strApplicationDataSetName), DataSet)
                If dsAuthDetail.Tables(0).Rows.Count > 0 Then
                    Dim drArray() As DataRow = dsAuthDetail.Tables(0).Select("numGroupID='" + CCommon.ToString(Session("GroupId")) + "' and numModuleID='" + CCommon.ToInteger(numModuleID).ToString() + "' and numPageID='" + CCommon.ToString(numPageID) + "'", "")
                    If drArray.Length = 1 Then
                        dr = drArray(0)
                        m_aryRightsForPageTemp(RIGHTSTYPE.VIEW) = CInt(dr("intViewAllowed"))
                        m_aryRightsForPageTemp(RIGHTSTYPE.ADD) = CInt(dr("intAddAllowed"))
                        m_aryRightsForPageTemp(RIGHTSTYPE.DELETE) = CInt(dr("intDeleteAllowed"))
                        m_aryRightsForPageTemp(RIGHTSTYPE.EXPORT) = CInt(dr("intExportAllowed"))
                        m_aryRightsForPageTemp(RIGHTSTYPE.UPDATE) = CInt(dr("intUpdateAllowed"))
                        ModuleName = CCommon.ToString(dr("vcModuleName"))
                        PermissionName = CCommon.ToString(dr("vcPageDesc"))
                    End If
                End If
            End If
            If dr Is Nothing Then
                m_aryRightsForPageTemp(RIGHTSTYPE.VIEW) = 0
                m_aryRightsForPageTemp(RIGHTSTYPE.ADD) = 0
                m_aryRightsForPageTemp(RIGHTSTYPE.DELETE) = 0
                m_aryRightsForPageTemp(RIGHTSTYPE.EXPORT) = 0
                m_aryRightsForPageTemp(RIGHTSTYPE.UPDATE) = 0
            End If

            Return m_aryRightsForPageTemp
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetQueryStringVal(ByVal Key As String, Optional ByVal boolReturnComplete As Boolean = False) As String
        Try
            Dim objQSV As QueryStringValues
            If Session("objQSV") Is Nothing Then
                objQSV = New QueryStringValues
                Session("objQSV") = objQSV
            Else
                objQSV = CType(Session("objQSV"), QueryStringValues)
            End If
            Return objQSV.GetQueryStringVal(Request.QueryString("enc"), Key, boolReturnComplete)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    'Protected Overridable Sub PagePreRender()
    '    Try
    '        If Not IsPostBack Then

    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(Session("DomainID")), CCommon.ToLong(Session("UserContactID")), Request)
    '        Response.Write(ex)
    '    End Try

    'End Sub

End Class


'End Namespace



''**********************************************************************************
'' <BACRMPage.vb>
'' 
'' 	CHANGE CONTROL:
''	
''	AUTHOR: Goyal	  DATE:03-Mar-05	VERSION	CHANGES	KEYSTRING:
''**********************************************************************************
'Option Explicit On 
'Option Strict On

'Imports BACRM.UserInterface.Interfaces

'Namespace BACRM.UserInterface

'    '**********************************************************************************
'    ' Module Name  : BACRM UI
'    ' Module Type  : User Interface
'    ' 
'    ' Description  : This is the base class for all the pages in the BACRM application 
'    '**********************************************************************************

'    Public Class BACRMPage
'        Inherits BACRMPage
'        Implements IPage


'#Region "Private members"
'        Private _intScreenId As Int32
'        'read the value from State
'        Private _intUserId As Int32
'        Private _dtmStartTime As DateTime
'#End Region


'#Region "The security properties "

'        Private _objPerm As SPermissions
'        'IsAllow read property
'        Public ReadOnly Property AllowRead() As Boolean
'            Get
'                If IsNothing(_objPerm) Then Return False
'                Return _objPerm.bRead
'            End Get
'        End Property
'        'Allow create property
'        Public ReadOnly Property AllowCreate() As Boolean
'            Get
'                If IsNothing(_objPerm) Then Return False
'                Return _objPerm.bCreate
'            End Get
'        End Property
'        'Allow update property
'        Public ReadOnly Property AllowUpdate() As Boolean
'            Get
'                If IsNothing(_objPerm) Then Return False
'                Return _objPerm.bUpdate
'            End Get
'        End Property
'        'Allow delete property
'        Public ReadOnly Property AllowDelete() As Boolean
'            Get
'                If IsNothing(_objPerm) Then Return False
'                Return _objPerm.bDelete
'            End Get
'        End Property

'        'Allow Export property
'        Public ReadOnly Property AllowExport() As Boolean
'            Get
'                If IsNothing(_objPerm) Then Return False
'                Return _objPerm.bExport
'            End Get
'        End Property

'        'Allow Print property
'        Public ReadOnly Property AllowPrint() As Boolean
'            Get
'                If IsNothing(_objPerm) Then Return False
'                Return _objPerm.bPrint
'            End Get
'        End Property

'        'Allow all property
'        Public ReadOnly Property AllowAll() As Boolean
'            Get
'                If (AllowRead And AllowCreate And AllowUpdate And AllowDelete And AllowExport And AllowPrint) Then
'                    Return True
'                Else
'                    Return False
'                End If
'            End Get
'        End Property
'        'Is authorized property
'        Protected ReadOnly Property IsAuthorized() As Boolean
'            Get
'                If ((Not AllowRead) And (Not AllowCreate) And (Not AllowUpdate) And (Not AllowDelete) And (Not AllowExport) And (Not AllowPrint)) Then
'                    Return False
'                Else
'                    Return True
'                End If
'            End Get
'        End Property

'        'Empty Property for ConnectionString
'        Public ReadOnly Property ConnectionString() As String
'            Get
'                Return ""
'            End Get
'        End Property

'#End Region

'#Region "Constructor"

'        '**********************************************************************************
'        ' Name       : New
'        ' Type       : Constructor
'        ' Scope      : Public
'        ' Returns    : N/A
'        ' Parameters : N/A
'        ' Description: Constructor for the class                
'        ' Created By   : Goyal       Created Date: 03-Mar-05
'        '**********************************************************************************
'        Public Sub New()


'        End Sub

'#End Region

'#Region "Event handlers and base class overrides"

'        '**********************************************************************************
'        ' Name         : Page_Init
'        ' Type         : Sub
'        ' Scope        : Private
'        ' Returns      : N/A
'        ' Parameters   : ByVal sender As Object
'        '                ByVal e As System.EventArgs           
'        ' Description  : This will handle the Page init functionality of the page
'        ' Created By   : Goyal       Created Date: 03-Mar-05
'        '**********************************************************************************
'        Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Init

'        End Sub

'        '**********************************************************************************
'        ' Name       : Page_Load
'        ' Type       : Event handler
'        ' Scope      : Private
'        ' Returns    : N/A 
'        ' Parameters : ByVal sender  As Object
'        '              ByVal e as EventArgs
'        ' Description: Handles the page load event of the page
'        ' Created By   :Goyal       Created Date: 03-Mar-05
'        '**********************************************************************************
'        Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

'            Try
'                If (ScreenId = 0) Then
'                    'Response.Write("<h3>Screen id is missing. Please give me a name :( </h3>")
'                End If

'                _pagePreLoad()

'                PagePreLoad(sender, e)

'                If (IsPostBack) Then
'                    PagePostBack(sender, e)
'                Else
'                    PageNonPostBack(sender, e)
'                End If

'                PageLoadComplete()

'            Catch ex As Exception

'            End Try

'        End Sub

'        '**********************************************************************************
'        ' Name         : Page_Error
'        ' Type         : Sub
'        ' Scope        : Private
'        ' Returns      : N/A
'        ' Parameters   : ByVal sender As Object
'        '                ByVal e As System.EventArgs
'        ' Description  : This will handle the page level unhandled errors
'        ' Created By   :Goyal       Created Date: 03-Mar-05
'        '**********************************************************************************
'        Private Sub Page_Error(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Error

'            'Get the source exception details
'            Dim objEx As Exception
'            objEx = Server.GetLastError()

'            'Store the exception in the session
'            MyBase.Session.Add("Exception", objEx)

'        End Sub

'        '**********************************************************************************
'        ' Name       : OnLoad
'        ' Type       : Event handler
'        ' Scope      : Protected Overriden
'        ' Returns    : N/A
'        ' Parameters : ByVal e as EventArgs
'        ' Description: Handles the OnLoad function of the base class
'        ' Created By   :Goyal       Created Date: 03-Mar-05
'        '**********************************************************************************
'        Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
'            MyBase.OnLoad(e)
'        End Sub

'        '**********************************************************************************
'        ' Name       : OnPreRender
'        ' Type       : Event handler
'        ' Scope      : Protected overriden
'        ' Returns    : N/A
'        ' Parameters : ByVal e as EventArgs
'        ' Description: Handles the OnPreRender function of the base class
'        ' Created By   :Goyal       Created Date: 03-Mar-05
'        '**********************************************************************************
'        Protected Overrides Sub OnPreRender(ByVal e As System.EventArgs)
'            'Call the base class
'            MyBase.OnPreRender(e)

'            Try
'                PagePreRender()
'            Catch ex As Exception
'            End Try
'        End Sub


'        '**********************************************************************************
'        ' Name         : RaisePostBackEvent
'        ' Type         : Event handler
'        ' Scope        : Protected
'        ' Returns      : N/A
'        ' Parameters   : ByVal sourceControl As IPostBackEventHandler
'        '                ByVal eventArgument As String
'        ' Created By   :Goyal       Created Date: 03-Mar-05
'        '**********************************************************************************
'        Protected Overrides Sub RaisePostBackEvent(ByVal sourceControl As IPostBackEventHandler, ByVal eventArgument As String)

'            Try

'                Page.Validate()
'                If Page.IsValid Then
'                    'do nothing
'                Else
'                    'The page is invalid becuase of another control whose validation 
'                    'has failed. so we terminate the processing and then exit the page
'                    Return
'                End If
'                'Now we raise the event
'                MyBase.RaisePostBackEvent(sourceControl, eventArgument)

'            Catch ex As Exception
'            End Try

'        End Sub

'#End Region

'#Region "Public Functions"
'        '**********************************************************************************
'        ' Name       : DisplayError
'        ' Type       : Sub (overloaded)
'        ' Scope      : Public
'        ' Returns    : N/A  
'        ' Parameters : ByVal ex As Exception
'        '
'        ' Description: Displays the error message 
'        ' Created By   :Goyal       Created Date: 03-Mar-05
'        '**********************************************************************************
'        Public Sub DisplayError(ByVal ex As Exception)

'        End Sub

'        '**********************************************************************************
'        ' Name       : DisplayError
'        ' Type       : Sub (overloaded)
'        ' Scope      : Public
'        ' Returns    : N/A
'        ' Parameters : ByVal strCode As String
'        ' Description: Displays the error message 
'        ' Created By   :Goyal       Created Date: 03-Mar-05
'        '**********************************************************************************
'        Public Sub DisplayError(ByVal strCode As String)
'        End Sub


'#End Region

'#Region "Protected Virtual functions for the child classes"

'        '**********************************************************************************
'        ' Name       : PagePreLoad
'        ' Type       : Sub
'        ' Scope      : Protected overridable
'        ' Returns    : N/A
'        ' Parameters : ByVal sender As System.Object
'        '			   ByVal e As System.EventArgs
'        ' Description: The child classes can use this function to write their code for the 
'        '				doing some processing before checking for postback.
'        ' Created By   :Goyal       Created Date: 03-Mar-05
'        '**********************************************************************************
'        Protected Overridable Sub PagePreLoad(ByVal sender As System.Object, ByVal e As System.EventArgs)
'            'This space is for the child classes
'        End Sub

'        '**********************************************************************************
'        ' Name       : PageNonPostBack
'        ' Type       : Sub
'        ' Scope      : Protected overridable
'        ' Returns    : N/A
'        ' Parameters : ByVal sender As System.Object
'        '			   ByVal e As System.EventArgs
'        ' Description: The child classes can use this function to write their code for the 
'        '				event when the page is not in post back mode
'        ' Created By   :Goyal       Created Date: 03-Mar-05
'        '**********************************************************************************
'        Protected Overridable Sub PageNonPostBack(ByVal sender As System.Object, ByVal e As System.EventArgs)
'            'This space is for the child classes
'        End Sub

'        '**********************************************************************************
'        ' Name       : PagePostBack
'        ' Type       : Sub
'        ' Scope      : Protected overridable
'        ' Returns    : N/A
'        ' Parameters : ByVal sender As System.Object
'        '			   ByVal e As System.EventArgs 
'        ' Description: The child classes can use this function to write their code for the
'        '				event when the post back is happening
'        ' Created By   :Goyal       Created Date: 03-Mar-05
'        '**********************************************************************************
'        Protected Overridable Sub PagePostBack(ByVal sender As System.Object, ByVal e As System.EventArgs)
'            'This space is for the child classes
'        End Sub

'        '**********************************************************************************
'        ' Name       : PageLoadComplete
'        ' Type       : Sub
'        ' Scope      : Protected overridable
'        ' Returns    : N/A
'        ' Parameters : N/A
'        ' Description: The child classes can use this function to write their code for the 
'        '				Load complete event
'        ' Created By   :Goyal       Created Date: 03-Mar-05
'        '**********************************************************************************
'        Protected Overridable Sub PageLoadComplete()
'            'This space is for the child classes
'        End Sub

'        '**********************************************************************************
'        ' Name       : PagePreRender
'        ' Type       : Sub
'        ' Scope      : Protected overridable
'        ' Returns    : N/A
'        ' Parameters : N/A 
'        ' Description: The child classes can use this function to write their code for the 
'        '				PreRender  event
'        ' Created By   :Goyal       Created Date: 03-Mar-05
'        '**********************************************************************************
'        Protected Overridable Sub PagePreRender()
'            'This space is for the child classes
'        End Sub

'#End Region

'#Region "Private Functions"
'        '**********************************************************************************
'        ' Name       : _pagePreLoad
'        ' Type       : Sub
'        ' Scope      : Private
'        ' Returns    : N/A
'        ' Parameters : N/A 
'        ' Description: This function will make the preload operations
'        ' Created By   :Goyal       Created Date: 03-Mar-05
'        '**********************************************************************************
'        Private Sub _pagePreLoad()

'        End Sub

'        '**********************************************************************************
'        ' Name       : _getForm
'        ' Type       : Function
'        ' Scope      : Private
'        ' Returns    : Control
'        ' Parameters : None           
'        ' Description: Get Form Control from the page
'       ' Created By   :Goyal       Created Date: 03-Mar-05
'        '**********************************************************************************
'        Private Function _getForm() As Control
'            Dim objCtrl As Control

'            ' Get the Form Control From Controls Collection of Page
'            For Each objCtrl In Page.Controls
'                If (TypeOf objCtrl Is HtmlForm) Then
'                    ' Form Object found, return it
'                    Return objCtrl
'                End If
'            Next

'        End Function

'        Private Function _getButton() As Control

'            Dim objCtrl As Control

'            ' Get the Form Control From Controls Collection of Page
'            For Each objCtrl In Page.Controls
'                If (TypeOf objCtrl Is Button) Then
'                    ' Form Object found, return it
'                    Return objCtrl
'                End If
'            Next
'        End Function



'        '**********************************************************************************
'        ' Name         : _checkCausesValidation
'        ' Type         : Function
'        ' Scope        : Private
'        ' Returns      : Boolean
'        ' Parameters   : ByVal objCtrl As Control
'        ' Description  : 
'        ' Created By   :Goyal       Created Date: 03-Mar-05
'        '**********************************************************************************
'        Private Function _checkCausesValidation(ByVal objCtrl As Control) As Boolean

'            Dim blnCheck As Boolean = False

'            If TypeOf objCtrl Is Button Then
'                blnCheck = DirectCast(objCtrl, Button).CausesValidation
'            ElseIf TypeOf objCtrl Is LinkButton Then
'                blnCheck = DirectCast(objCtrl, LinkButton).CausesValidation
'            ElseIf TypeOf objCtrl Is ImageButton Then
'                blnCheck = DirectCast(objCtrl, ImageButton).CausesValidation
'            ElseIf TypeOf objCtrl Is HtmlButton Then
'                blnCheck = DirectCast(objCtrl, HtmlButton).CausesValidation
'            ElseIf TypeOf objCtrl Is HtmlInputButton Then
'                blnCheck = DirectCast(objCtrl, HtmlInputButton).CausesValidation
'            ElseIf TypeOf objCtrl Is HtmlInputImage Then
'                blnCheck = DirectCast(objCtrl, HtmlInputImage).CausesValidation
'            Else
'                'we do nothing
'            End If

'            Return blnCheck

'        End Function


'#End Region

'#Region "IPage interface implementation"

'        Public Property ScreenName() As String Implements Interfaces.IPage.ScreenName
'            Get

'            End Get
'            Set(ByVal Value As String)

'            End Set
'        End Property

'        'The screen id of the current view
'        Public Property ScreenId() As Int32 Implements Interfaces.IPage.ScreenId
'            Get
'                Return _intScreenId
'            End Get
'            Set(ByVal Value As Int32)
'                _intScreenId = Value
'            End Set
'        End Property
'        'The user id of the logged in user
'        Public Property UserId() As Int32 Implements Interfaces.IPage.UserId
'            Get
'                If Not IsNothing(Session("userid")) Then
'                    _intUserId = CInt(Session("userid"))
'                Else
'                    _intUserId = 0
'                End If
'                Return _intUserId
'            End Get
'            Set(ByVal Value As Int32)
'                _intUserId = Value
'            End Set

'        End Property

'#End Region

'    End Class

'End Namespace


