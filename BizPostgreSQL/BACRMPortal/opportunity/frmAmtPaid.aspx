<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmAmtPaid.aspx.vb"
    Inherits="BACRMPortal.frmAmtPaid" %>

<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <title>Amount Paid</title>

    <script language="javascript" type="text/javascript">
        function Submit() {
            if (document.Form1.txtCCNo.value == '') {
                alert("Enter Credit Card No")
                document.Form1.txtCCNo.focus();
                return false;
            }
            if (document.Form1.txtCHName.value == '') {
                alert("Enter Card Holder Name")
                document.Form1.txtCHName.focus();
                return false;
            }
            if (document.Form1.txtCVV2.value == '') {
                alert("Enter CVV2")
                document.Form1.txtCVV2.focus();
                return false;
            }
            if (document.Form1.ddlCardType.value == 0) {
                alert("Select Card Type")
                document.Form1.ddlCardType.focus();
                return false;
            }
            if (confirm('Your credit card will now be charged the total amounts due as displayed next to the "Amount To Pay" footer. Press "Ok" to to charge the card, otherwise press "Cancel"') == true) {
                UpdateTotal();
                return true;
            }
            else {
                return false;
            }
        }

        function Save(d) {
            if (document.Form1.txtAmount.value == "" || document.Form1.txtAmount.value == 0.00) {
                alert("Enter the Amount");
                document.Form1.txtAmount.focus();
                return false;
            }
            var BalanceAmt;
            BalanceAmt = 0;
            //	             alert(d);
            //	             alert(window.document.getElementById('AmountPaid').value);
            if (d != 0) {
                BalanceAmt = window.document.getElementById('lblBalanceAmt').innerHTML.replace(/,/g, "");
                BalanceAmt = parseFloat(BalanceAmt) + parseFloat(window.document.getElementById('AmountPaid').value);
            }
            else {

                BalanceAmt = window.document.getElementById('lblBalanceAmt').innerHTML.replace(/,/g, "");
            }
            //	             alert(BalanceAmt);
            //	             alert(document.Form1.txtAmount.value);
            if (parseFloat(document.Form1.txtAmount.value) > parseFloat(BalanceAmt)) {
                alert("Amount is greater than Balance Due");
                document.Form1.txtAmount.focus();
                return false;
            }
//            if (window.document.getElementById('chkContractAmt').checked == true) {
//                if (window.document.getElementById('ddlContracts').value == 0) {
//                    alert('Select the Contract');
//                    window.document.getElementById('chkContractAmt').focus();
//                    return false;
//                }
//            }
        }
        function WindowClose() {
            window.close();
            return false;
        }
        function Close() {
            window.close();
        }
        function CheckNumber(cint) {
            if (cint == 1) {
                if (!(window.event.keyCode > 47 && window.event.keyCode < 58 || window.event.keyCode == 44 || window.event.keyCode == 46)) {
                    window.event.keyCode = 0;
                }
            }
            if (cint == 2) {
                if (!(window.event.keyCode > 47 && window.event.keyCode < 58)) {
                    window.event.keyCode = 0;
                }
            }

        }

        function OpenBizInvoice(a, b) {
            window.open('../Opportunity/frmBizInvoice.aspx?Show=True&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=0,titlebar=0,menubar=0,location=1,left=100,top=50,width=750,height=800,scrollbars=yes,resizable=yes');
            return false;
        }
        function IsNumeric(sText) {
            var ValidChars = "0123456789.";
            var IsNumber = true;
            var Char;


            for (i = 0; i < sText.length && IsNumber == true; i++) {
                Char = sText.charAt(i);
                if (ValidChars.indexOf(Char) == -1) {
                    IsNumber = false;
                }
            }
            return IsNumber;
        }
        function UpdateTotal() {
            var Total = 0.00;
            var elems = document.getElementsByTagName('input');
            for (var i = 0; i < elems.length; i++) {
                if (elems[i].type == 'text') {
                    if (elems[i].id.indexOf('txtAmountToPay') > 0) {
                        if (IsNumeric(elems[i].value)) {
                            Total = Total + parseFloat(elems[i].value);
                        }
                        else {
                            alert('Please enter numeric value');
                            return false;
                        }
                    }
                }
            }
            if (!isNaN(Total)) {
                document.getElementById('lblTotalAmount').innerHTML = Total;
            }
            else {
                alert('Please enter numeric values');
            }
            return true;
        }
        

    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <br />
    <table id="tblCreditForm" runat="server" border="0">
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
                <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Charge Credit Card"
                    OnClientClick="return Submit();"></asp:Button>
                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close"></asp:Button>
            </td>
        </tr>
        <tr>
            <td class="normal1" width="200" align="right">
                Method of Payment
            </td>
            <td align="left">
                <asp:DropDownList ID="ddlPayment" runat="server" Width="130" CssClass="signup" Enabled="false">
                </asp:DropDownList>
            </td>
            <td class="normal1" align="right">
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="normal1" width="200" align="right">
                Credit Card No
            </td>
            <td>
                <asp:TextBox Width="200" ID="txtCCNo" CssClass="signup" runat="server"></asp:TextBox>
            </td>
            <td class="normal1" align="right">
                Card Holder Name
            </td>
            <td>
                <asp:TextBox ID="txtCHName" Width="200" runat="server" CssClass="signup"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                CVV2
            </td>
            <td>
                <asp:TextBox ID="txtCVV2" CssClass="signup" runat="server"></asp:TextBox>
            </td>
            <td class="normal1" align="right">
                Valid Upto
            </td>
            <td>
                <asp:DropDownList CssClass="signup" ID="ddlMonth" runat="server">
                    <asp:ListItem Value="01">01</asp:ListItem>
                    <asp:ListItem Value="02">02</asp:ListItem>
                    <asp:ListItem Value="03">03</asp:ListItem>
                    <asp:ListItem Value="04">04</asp:ListItem>
                    <asp:ListItem Value="05">05</asp:ListItem>
                    <asp:ListItem Value="06">06</asp:ListItem>
                    <asp:ListItem Value="07">07</asp:ListItem>
                    <asp:ListItem Value="08">08</asp:ListItem>
                    <asp:ListItem Value="09">09</asp:ListItem>
                    <asp:ListItem Value="10">10</asp:ListItem>
                    <asp:ListItem Value="11">11</asp:ListItem>
                    <asp:ListItem Value="12">12</asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList CssClass="signup" ID="ddlYear" runat="server">
                    <asp:ListItem Value="08">08</asp:ListItem>
                    <asp:ListItem Value="09">09</asp:ListItem>
                    <asp:ListItem Value="10">10</asp:ListItem>
                    <asp:ListItem Value="11">11</asp:ListItem>
                    <asp:ListItem Value="12">12</asp:ListItem>
                    <asp:ListItem Value="13">13</asp:ListItem>
                    <asp:ListItem Value="14">14</asp:ListItem>
                    <asp:ListItem Value="15">15</asp:ListItem>
                    <asp:ListItem Value="16">16</asp:ListItem>
                    <asp:ListItem Value="17">17</asp:ListItem>
                    <asp:ListItem Value="18">18</asp:ListItem>
                    <asp:ListItem Value="19">19</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Card Type:
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlCardType" CssClass="signup">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <asp:Table ID="table3" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="100%" BorderColor="black" GridLines="None" Height="350" CssClass="aspTable">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:DataGrid ID="dgOpportunity" runat="server" Width="100%" CssClass="dg" AllowSorting="True"
                    AutoGenerateColumns="False" BorderColor="white">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="numOppId"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="numOppBizDocsId"></asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="numContactId"></asp:BoundColumn>
                        <asp:TemplateColumn>
                            <HeaderTemplate>
                                <font color="white">BizDoc</font></HeaderTemplate>
                            <ItemTemplate>
                                <a href="#" onclick="return OpenBizInvoice('<%# Eval("numOppId") %>','<%# Eval("numOppBizDocsId") %>');">
                                    <%#Eval("vcBizDocID")%></a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <HeaderTemplate>
                                <font color="white">Invoice Comments</font></HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox runat="server" ID="txtComments" CssClass="signup" Text="" Width="300"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataFormatString="{0:#,##0.00}" DataField="BalanceDue" HeaderText="<font color=white>Balance Due</font>"
                            ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                        <asp:TemplateColumn>
                            <HeaderTemplate>
                                <font color="white">Amount to Pay</font></HeaderTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                            <ItemTemplate>
                                <asp:TextBox runat="server" Width="60" ID="txtAmountToPay" CssClass="signup" Text='<%# String.Format("{0:0.00}",(Eval("BalanceDue")) ) %>'
                                    Style="text-align: right;" OnKeyUp="return UpdateTotal();"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                <table width="100%" align="right">
                    <tr>
                        <td class="normal1" align="right">
                            <b>Total Amount To Pay</b>
                        </td>
                        <td class="normal1" valign="bottom">
                            <asp:Label ID="lblTotalAmount" runat="server" Text="0.00"></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="AmountPaid" runat="Server" />
    </form>
</body>
</html>
