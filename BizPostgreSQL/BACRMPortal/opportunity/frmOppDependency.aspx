<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmOppDependency.aspx.vb"
    Inherits="BACRMPortal.frmOppDependency" %>

<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <title>Dependency</title>

    <script language="javascript">
        function Close() {
            window.close();
        }
        function Add() {
            if (document.Form1.ddlProcessList.value == 0) {
                alert("Select Process List ")
                document.Form1.ddlProcessList.focus();
                return false;
            }
            if (document.Form1.ddlMilestone.value == 0) {
                alert("Select Milestone ")
                document.Form1.ddlMilestone.focus();
                return false;
            }
            if (document.Form1.ddlStage.value == 0) {
                alert("Select Stage ")
                document.Form1.ddlStage.focus();
                return false;
            }
            for (i = 0; i < document.Form1.elements.length; i++) {
                if (document.getElementById('dgDependency__ctl' + i + '_txtStageId') != null) {
                    if (document.getElementById('dgDependency__ctl' + i + '_txtStageId').value == document.Form1.ddlStage.value) {
                        alert("Dependency Stage is already Added");
                        return false;
                    }
                }
            }
        }
        function Save() {
            if (document.getElementById('dgDependency') != null) {
                if (document.getElementById('dgDependency').rows.length == 1) {
                    alert("Select atleast one Dependency");
                    return false;
                }
            }
            if (document.getElementById('dgDependency') == null) {
                alert("Select atleast one Dependency");
                return false;
            }
        }
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <table width="100%">
        <tr>
            <td align="right" colspan="2">
                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close"></asp:Button><br>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:DataGrid ID="dgDependency" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
                    BorderColor="white">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn HeaderText="Process List" DataField="vcPOppName"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Milestone" DataField="numstagepercentage"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Stage" DataField="vcstagedetail" ItemStyle-Width="100">
                        </asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Status" DataField="Status"></asp:BoundColumn>
                        <asp:TemplateColumn Visible="false">
                            <ItemTemplate>
                                <asp:TextBox ID="txtStageId" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "numProcessStageId") %>'>
                                </asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
