<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmAddBizDocs.aspx.vb" Inherits="BACRMPortal.frmAddBizDocs" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
		<title>BizDocs</title>
		<script language="javascript">
			function Close()
			{
				window.close();
			}
			function Save()
			{
				if (document.Form1.txtAmount.value=="")
				{
					alert("Enter Amount")
					document.Form1.txtAmount.focus();
					return false;
				}
			}
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table width="100%">
				<tr>
					
					<td align="right" colSpan="2">
						<asp:Button ID="btnSaveClose" Runat="server" CssClass="button" Text="Save &amp; Close"></asp:Button>
						<asp:Button ID="btnCancel" Runat="server" CssClass="button" Text="Cancel"></asp:Button>
						<br>
						<br>
					</td>
				</tr>
				<tr>
					<td class="normal1" align="right">Select BizDoc
					</td>
					<td><asp:DropDownList ID="ddlBizDocs" Runat="server" Width="130" CssClass="signup"></asp:DropDownList></td>
				</tr>
			</table>
			<table align="center" width="100%">
				<tr>
					<td align="center" class="normal4">
						<asp:Literal ID="litMessage" Runat="server" EnableViewState="False"></asp:Literal>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
