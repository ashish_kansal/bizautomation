<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmOppExpense.aspx.vb"
    Inherits="BACRMPortal.frmOppExpense" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <title>Billable Expense</title>
    <script language="javascript">
        function Close() {
            window.close();
        }
        function Save() {
            if (document.Form1.txtAmount.value == "") {
                alert("Enter Amount")
                document.Form1.txtAmount.focus();
                return false;
            }
        }
    </script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <table width="100%">
        <tr>
            <td align="right" colspan="2">
                <asp:Button ID="btndelete" runat="server" CssClass="button" Text="Delete" Width="50">
                </asp:Button>
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save &amp; Close">
                </asp:Button>
                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close"></asp:Button>
                <br>
                <br>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="left" valign="middle">
                <asp:RadioButton ID="radBill" runat="server" CssClass="normal1" GroupName="Bill"
                    Text="Billable" Checked="true" />
                <asp:RadioButton ID="radNonBill" runat="server" CssClass="normal1" GroupName="Bill"
                    Text="Non-Billable" />
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Amount
            </td>
            <td class="normal1">
                <asp:TextBox ID="txtAmount" runat="server" CssClass="signup"></asp:TextBox>
                <asp:CheckBox runat="server" Text="Reimbursable" ID="chkReimb" CssClass="signup" />
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Created Date
            </td>
            <td>
                <table>
                    <tr>
                        <td>
                            <BizCalendar:Calendar ID="CalCreated" runat="server" />
                        </td>
                        <td>
                        </td>
                        <td class="text" colspan="2" id="td1" runat="server">
                            <img src="../images/Clock-16.gif" align="absMiddle">
                            <asp:DropDownList ID="ddlCreatedTime" runat="server" Width="55" CssClass="signup">
                                <asp:ListItem Selected="False" Value="23">12:00</asp:ListItem>
                                <asp:ListItem Selected="False" Value="24">12:30</asp:ListItem>
                                <asp:ListItem Selected="False" Value="1">1:00</asp:ListItem>
                                <asp:ListItem Selected="False" Value="2">1:30</asp:ListItem>
                                <asp:ListItem Selected="False" Value="3">2:00</asp:ListItem>
                                <asp:ListItem Selected="False" Value="4">2:30</asp:ListItem>
                                <asp:ListItem Selected="False" Value="5">3:00</asp:ListItem>
                                <asp:ListItem Selected="False" Value="6">3:30</asp:ListItem>
                                <asp:ListItem Selected="False" Value="7">4:00</asp:ListItem>
                                <asp:ListItem Selected="False" Value="8">4:30</asp:ListItem>
                                <asp:ListItem Selected="False" Value="9">5:00</asp:ListItem>
                                <asp:ListItem Selected="False" Value="10">5:30</asp:ListItem>
                                <asp:ListItem Selected="False" Value="11">6:00</asp:ListItem>
                                <asp:ListItem Selected="False" Value="12">6:30</asp:ListItem>
                                <asp:ListItem Selected="False" Value="13">7:00</asp:ListItem>
                                <asp:ListItem Selected="False" Value="14">7:30</asp:ListItem>
                                <asp:ListItem Selected="True" Value="15">8:00</asp:ListItem>
                                <asp:ListItem Selected="False" Value="16">8:30</asp:ListItem>
                                <asp:ListItem Selected="False" Value="17">9:00</asp:ListItem>
                                <asp:ListItem Selected="False" Value="18">9:30</asp:ListItem>
                                <asp:ListItem Selected="False" Value="19">10:00</asp:ListItem>
                                <asp:ListItem Selected="False" Value="20">10:30</asp:ListItem>
                                <asp:ListItem Selected="False" Value="21">11:00</asp:ListItem>
                                <asp:ListItem Selected="False" Value="22">11:30</asp:ListItem>
                            </asp:DropDownList>
                            &nbsp;AM<input id="chkCreatedAm" type="radio" checked value="0" name="AM" runat="server" />PM<input
                                id="chkCreatedPm" type="radio" value="1" name="AM" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Contract
            </td>
            <td class="normal1">
                <asp:DropDownList ID="ddlContract" AutoPostBack="true" Width="180" runat="server"
                    CssClass="signup">
                </asp:DropDownList>
                &nbsp;(Apply Expenses to Contract)
            </td>
        </tr>
        <tr>
            <td align="right" class="normal1">
                Contract Amount Balance :
            </td>
            <td>
                <asp:Label ID="lblRemAmount" runat="server" CssClass="normal1"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Description
            </td>
            <td>
                <asp:TextBox TextMode="MultiLine" runat="server" Width="400" Height="50" ID="txtDesc"
                    CssClass="signup"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2" class="normal4">
                <asp:Literal runat="server" ID="litMessage" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
    <asp:TextBox ID="txtCategoryHDRID" runat="server" Text="0" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtOppId" runat="server" Text="0" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtStageId" runat="server" Text="0" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtDivId" runat="server" Text="0" Style="display: none"></asp:TextBox>
    </form>
</body>
</html>
