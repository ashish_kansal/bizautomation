<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmOpportunityList.aspx.vb" Inherits="BACRMPortal.frmOpportunityList" %>
<%@ Register TagPrefix="menu1" TagName="PartnerPoint" src="../Common/PartnerPoint.ascx" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Infragistics35.WebUI.UltraWebTab.v8.1, Version=8.1.20081.2046, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebTab" TagPrefix="igtab" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1"    runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Opportunity</title>
        <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
         <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)"> 
		<script language="javascript">
	   function DeleteRecord()
		{   
		
		    var OppId='';
		      for(var i=2; i<=gvSearch.rows.length; i++)
			{
			    if (i<10)
		        {
		         str='0'+i   
		         }
		         else
		         {
		           str=i
		         }
		    
		        if  (document.all('gvSearch_ctl'+str+'_chk').checked==true)
		       {
		           
		            if (OppId == '')
		            {
		                OppId = document.all('gvSearch_ctl'+str+'_lbl1').innerHTML+'~'+document.all('gvSearch_ctl'+str+'_lbl2').innerHTML+'~'+document.all('gvSearch_ctl'+str+'_lbl3').innerHTML
		            }
		            else
		            {
		                 OppId =OppId+','+document.all('gvSearch_ctl'+str+'_lbl1').innerHTML+'~'+document.all('gvSearch_ctl'+str+'_lbl2').innerHTML+'~'+document.all('gvSearch_ctl'+str+'_lbl3').innerHTML
		            }
		       } 
			}
			  document.all('txtDelOppId').value = OppId
			  
			return true
		}
		function fnSortByChar(varSortChar)
			{
				document.Form1.txtSortCharSales.value = varSortChar;
				if (typeof(document.Form1.txtCurrrentPageSales)!='undefined')
				{
					document.Form1.txtCurrrentPageSales.value=1;
				}
				document.Form1.btnGo1.click();
			}
	
				 function SortColumn(a)
    {
       document.Form1.txtSortColumn.value=a;
       document.Form1.submit();
       return false;
    }
    function SelectAll(a)
    {
        var str;

        if (typeof(a)=='string')
        {
            a=document.all[a]
        }
        if (a.checked==true)
        {
             for(var i=1; i<=gvSearch.rows.length; i++)
			{
			    if (i<10)
		        {
		         str='0'+i   
		         }
		         else
		         {
		           str=i
		         }
		         document.all('gvSearch_ctl'+str+'_chk').checked=true;
			}
        }
        else if (a.checked==false)
        {
             for(var i=1; i<=gvSearch.rows.length; i++)
			{
			    if (i<10)
		        {
		         str='0'+i   
		         }
		         else
		         {
		           str=i
		         }
		         document.all('gvSearch_ctl'+str+'_chk').checked=false;
			}
        }
       
        //gvSearch_ctl02_chk
    }
    function OpenWindow(a,b,c)
    {
        var str;
        if (b==0)
        {   
            if (c==1)
            {
            str="../pagelayout/frmLeaddtl.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=opportunitylist&DivID=" + a;
            }
            else{
            str="../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=opportunitylistDivID=" + a;
            }
        }
        else if (b==1)
        {
            if (c==1)
            {
            str="../pagelayout/frmProspectdtl.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=opportunitylist&DivID=" + a;
            }
            else
            {
              str="../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=opportunitylist&DivID=" + a;
            }
        }
         else if (b==2)
        {
            if (c==1)
            {
            str="../pagelayout/frmAccountdtl.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=opportunitylist&klds+7kldf=fjk-las&DivId=" + a;
            }
            else
            {
              str="../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=opportunitylist&klds+7kldf=fjk-las&DivId=" + a;
            }
        }
       
            document.location.href=str;
       
    }
		function OpenContact(a,b)
    {
      
        var str;
        if (b==1)
            {
            str="../pagelayout/frmContact.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=opportunitylist&ujfda=tyuu&CntId=" + a;
            }
            else
            {
             str="../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=opportunitylist&ft6ty=oiuy&CntId=" + a;
             }
        
            document.location.href=str;
      
    }
	
		function DeleteMessage()
		{
			alert("You Are not Authorized to Delete the Selected Record !");
			return false;
		}
		function SelWarOpenDeals(a)
		{
		    window.open("../opportunity/frmSelWhouseOpenDls.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Opid="+a)
		    return false;
		}
			function OpenSetting()
		{
		      
                var selectedIndex = document.getElementById('txtOpptype').value
               var type = 1 
               if  (selectedIndex == 0 )
                    type = 1
               if  (selectedIndex == 1)
                    type = 2
               
		    window.open('../Opportunity/frmConfOppList.aspx?type='+type,'','toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
		    return false
		}
		function OpenOpp(a,b)
		{
		  
		 var str;
        if (b==1)
            {
            str="../pagelayout/frmOppurtunityDtl.aspx?frm=opportunitylist&OpID="+a;
            }
            else
            {
             str="../opportunity/frmOpportunities.aspx?frm=opportunitylist&OpID="+a;
             }    
              document.location.href=str; 
		}
		</script>
	</HEAD>
	<body>
		
		<form id="Form1" method="post" runat="server">
<asp:ScriptManager runat="server" ID="sm" EnablePartialRendering="true"></asp:ScriptManager>
<menu1:PartnerPoint ID="menu" runat="server" />
		
        <asp:updatepanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" ><ContentTemplate>
		
		<table width="100%">
			    <tr>
			        <td>
			        <asp:HyperLink Text="Settings" CssClass="hyperlink" runat="server" ID="hplSettings"></asp:HyperLink>
			        </td>
			       <td align="right">
			<TABLE align="right">
				<tr>
					<TD class="normal1" width="150">No of Records:
						<asp:label id="lblRecordsSales" runat="server"></asp:label>				
					</TD>
					<TD class="normal1">Organization
					</TD>
					<TD><asp:textbox id="txtCustomer" runat="server" Width="55px" CssClass="signup"></asp:textbox></TD>
					<TD class="normal1">First Name
					</TD>
					<TD><asp:textbox id="txtFirstName" runat="server" Width="55px" CssClass="signup"></asp:textbox></TD>
					<TD class="normal1">Last Name</TD>
					<TD><asp:textbox id="txtLastName" runat="server" Width="55px" CssClass="signup"></asp:textbox></TD>
					<TD><asp:button id="btnGo" Width="25" CssClass="button" Runat="server" Text="Go"></asp:button></TD>
				   <TD><asp:button id="btnDelete" OnClientClick="return DeleteRecord()" CssClass="button" Runat="server" Text="Delete"></asp:button></TD>
				</TR>
			
			</TABLE>
			</td></tr></table>
			
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td vAlign="bottom">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;<asp:Label ID="lbOpportunity" runat="server" ></asp:Label>&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
			
					    <td align="right" vAlign="bottom">
					    
					    
			<table align="right"  width="600" >
				<tr>
					<TD class="normal1" align="right">Filter&nbsp;&nbsp;</TD>
					<asp:Panel id="pnlSales" Runat="server">
						<TD  align="left">
							<asp:dropdownlist id="ddlSortSales" runat="server" cssclass="signup" AutoPostBack="True">
								<asp:ListItem Value="1">My Sales Opportunities</asp:ListItem>
								<asp:ListItem Value="3">All Sales Opportunities</asp:ListItem>
								<asp:ListItem Value="2">My Subordinates</asp:ListItem>
								<asp:ListItem Value="4">Added in Last 7 days</asp:ListItem>
								<asp:ListItem Value="5">Last 20 Added by me</asp:ListItem>
								<asp:ListItem Value="6">Last 20 Modified by me</asp:ListItem>
							</asp:dropdownlist></TD>						
					</asp:Panel>
					<asp:Panel id="pnlPurchase" Runat="server">
						<TD align="left">
							<asp:dropdownlist id="ddlSortPurchase" runat="server" cssclass="signup" AutoPostBack="True">
								<asp:ListItem Value="1">My Purchase Opportunities</asp:ListItem>
								<asp:ListItem Value="3">All Purchase Opportunities</asp:ListItem>
								<asp:ListItem Value="2">My Subordinates</asp:ListItem>
								<asp:ListItem Value="4">Added in Last 7 days</asp:ListItem>
								<asp:ListItem Value="5">Last 20 Added by me</asp:ListItem>
								<asp:ListItem Value="6">Last 20 Modified by me</asp:ListItem>
							</asp:dropdownlist></TD>						
					</asp:Panel>
					<TD id="hideSales" noWrap align="right" runat="server">
							<TABLE>
								<TR>
									<TD>
										<asp:label id="lblNextSales" runat="server" cssclass="Text_bold">Next:</asp:label></TD>
									<TD class="normal1">
										<asp:linkbutton id="lnk2Sales" runat="server">2</asp:linkbutton></TD>
									<TD class="normal1">
										<asp:linkbutton id="lnk3Sales" runat="server">3</asp:linkbutton></TD>
									<TD class="normal1">
										<asp:linkbutton id="lnk4Sales" runat="server">4</asp:linkbutton></TD>
									<TD class="normal1">
										<asp:linkbutton id="lnk5Sales" runat="server">5</asp:linkbutton></TD>
									<TD>
										<asp:linkbutton id="lnkFirstSales" runat="server">
											<div class="LinkArrow">9</div>
										</asp:linkbutton></TD>
									<TD>
										<asp:linkbutton id="lnkPreviousSales" runat="server">
											<div class="LinkArrow">3</div>
										</asp:linkbutton></TD>
									<TD class="normal1">
										<asp:label id="lblPageSales" runat="server">Page</asp:label></TD>
									<TD>
										<asp:textbox id="txtCurrrentPageSales" runat="server" CssClass="signup" Width="28px" AutoPostBack="True"
											MaxLength="5" Text="1"></asp:textbox></TD>
									<TD class="normal1">
										<asp:label id="lblOfSales" runat="server">of</asp:label></TD>
									<TD class="normal1">
										<asp:label id="lblTotalSales" runat="server"></asp:label></TD>
									<TD>
										<asp:linkbutton id="lnkNextSales" runat="server" CssClass="LinkArrow">
											<div class="LinkArrow">4</div>
										</asp:linkbutton></TD>
									<TD>
										<asp:linkbutton id="lnkLastSales" runat="server">
											<div class="LinkArrow">:</div>
										</asp:linkbutton></TD>
								</TR>
							</TABLE>
						</TD>
				</tr>
			</table>
            </td>
					</tr>
				</table>
			
         
					<table cellSpacing="1" cellPadding="1" width="100%" border="0">
						<tr>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="a" href="javascript:fnSortByChar('a')">
									<div class="A2Z">A</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="b" href="javascript:fnSortByChar('b')">
									<div class="A2Z">B</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="c" href="javascript:fnSortByChar('c')">
									<div class="A2Z">C</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="d" href="javascript:fnSortByChar('d')">
									<div class="A2Z">D</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="e" href="javascript:fnSortByChar('e')">
									<div class="A2Z">E</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="f" href="javascript:fnSortByChar('f')">
									<div class="A2Z">F</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="g" href="javascript:fnSortByChar('g')">
									<div class="A2Z">G</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="h" href="javascript:fnSortByChar('h')">
									<div class="A2Z">H</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="I" href="javascript:fnSortByChar('i')">
									<div class="A2Z">I</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="j" href="javascript:fnSortByChar('j')">
									<div class="A2Z">J</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="k" href="javascript:fnSortByChar('k')">
									<div class="A2Z">K</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="l" href="javascript:fnSortByChar('l')">
									<div class="A2Z">L</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="m" href="javascript:fnSortByChar('m')">
									<div class="A2Z">M</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="n" href="javascript:fnSortByChar('n')">
									<div class="A2Z">N</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="o" href="javascript:fnSortByChar('o')">
									<div class="A2Z">O</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="p" href="javascript:fnSortByChar('p')">
									<div class="A2Z">P</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="q" href="javascript:fnSortByChar('q')">
									<div class="A2Z">Q</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="r" href="javascript:fnSortByChar('r')">
									<div class="A2Z">R</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="s" href="javascript:fnSortByChar('s')">
									<div class="A2Z">S</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="t" href="javascript:fnSortByChar('t')">
									<div class="A2Z">T</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="u" href="javascript:fnSortByChar('u')">
									<div class="A2Z">U</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="v" href="javascript:fnSortByChar('v')">
									<div class="A2Z">V</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="w" href="javascript:fnSortByChar('w')">
									<div class="A2Z">W</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="x" href="javascript:fnSortByChar('x')">
									<div class="A2Z">X</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="y" href="javascript:fnSortByChar('y')">
									<div class="A2Z">Y</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="z" href="javascript:fnSortByChar('z')">
									<div class="A2Z">Z</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="all" href="javascript:fnSortByChar('0')">
									<div class="A2Z">All</div>
								</A>
							</td>
						</tr>
					</table>
					
            <asp:table id="table3" Width="100%" Runat="server" Height="350" GridLines="None" BorderColor="black" CssClass="aspTable"
				BorderWidth="1" CellSpacing="0" CellPadding="0">
				<asp:tableRow>
					<asp:tableCell VerticalAlign="Top">
						    <asp:GridView ID="gvSearch" runat="server" EnableViewState="true"  AutoGenerateColumns="false"  CssClass="dg" Width="100%">
                                    <AlternatingRowStyle CssClass="ais"/>
                                    <RowStyle CssClass="is"/>
                                    <HeaderStyle CssClass="hs"/>
						                        <Columns>
						                   
						                        </Columns>		
                                </asp:GridView>
					</asp:tableCell>
				</asp:tableRow>
			</asp:table>      
			<table width="100%">
				<tr>
					<td class="normal4" align="center">
						<asp:Literal ID="litMessage" Runat="server"></asp:Literal></td>
				</tr>
			</table>
			<asp:TextBox ID="txtDelOppId" runat="server" style="display:none"></asp:TextBox>
			   <asp:TextBox ID="txtSortColumn" runat="server" style="display:none"></asp:TextBox>		
			<asp:TextBox ID="txtTotalPageSales" Runat="server" style="DISPLAY:none"></asp:TextBox>
			<asp:TextBox ID="txtTotalRecordsSales" Runat="server" style="DISPLAY:none"></asp:TextBox>
			<asp:TextBox ID="txtSortCharSales" Runat="server" style="DISPLAY:none"></asp:TextBox>	
				<asp:TextBox ID="txtOpptype" Runat="server" Text="0" style="DISPLAY:none"></asp:TextBox>	
			<asp:button id="btnGo1" Width="25" Runat="server" style="DISPLAY:none" />
			</ContentTemplate>
			</asp:updatepanel>
	
		</form>
	</body>
</HTML>
