Imports System
Imports System.Diagnostics
Imports System.Drawing
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing.Imaging
Imports Syncfusion.Pdf.Shared
Imports Syncfusion.Pdf
Imports Syncfusion.HtmlConverter
Imports BACRM.BusinessLogic.Common
Partial Public Class PDFCON : Inherits System.Web.UI.Page

    Private m_converter As HtmlConverter

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Public Function ToPDF(ByVal img As Image) As String
        Try
            Dim doc As PDFDocument = New PDFDocument
            doc.Compression = CompressionLevel.Normal
            Dim properties As IPDFGraphicState = doc.CreateGraphicState()
            properties.SplitImages = True
            properties.BreakBehavior = AutoBreakBehavior.Cropping

            If Not img Is Nothing Then
                doc.LastPage.Margins.All = 0
                Dim dest As SizeF = New Size
                dest = New SizeF(doc.LastPage.DrawingWidth, img.Height)
                doc.LastPage.Graphics.DrawImage(PointF.Empty, img, dest, properties)
                'Response.Write(img.Width)
            End If

            Dim FilePath As String = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs"
            Dim path As String = "File" & Format(Now, "ddmmyyyyhhmmss") & ".pdf"
            doc.Save(FilePath & "\" & path)
            Return path
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Convert(ByVal url As String)
        Try
            Dim html As HtmlConverter = New HtmlConverter
            Dim path As String
            Try
                m_converter = html
                Dim bmp As System.Drawing.Image

                'bmp.FromFile("http://localhost/SyncFusionPDF/default.aspx")
                bmp = html.ConvertToImage(url, ImageType.Bitmap)
                'bmp.Save("C:\Test\Test.bmp")

                Try
                    path = ToPDF(bmp)
                Finally
                    If TypeOf bmp Is IDisposable Then
                        Dim disp As IDisposable = bmp
                        disp.Dispose()
                    End If
                End Try
                m_converter = Nothing
            Finally
                If TypeOf html Is IDisposable Then
                    Dim disp As IDisposable = html
                    disp.Dispose()
                End If
            End Try
            Return path
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function GetImageType() As ImageType
        Try
            Return (ImageType.Bitmap)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            Convert(TextBox1.Text)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class