<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="NewOpportunity.aspx.vb" Inherits="BACRMPortal.NewOpportunity" %>
    <%@ Register TagPrefix="menu1" TagName="PartnerPoint" src="../Common/PartnerPoint.ascx" %>
<<%--!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1"    runat="server">		
        <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
		<title>Opportunity</title>

		<script language="javascript" type="text/javascript" >
		function AddPrice(a,b,c)
		{
			document.Form1.txtPrice.value=a;
		}
		function CheckNumber(cint)
					{
						if (cint==1)
						{
							if (!(window.event.keyCode > 47 && window.event.keyCode < 58 || window.event.keyCode==44 || window.event.keyCode==46))
							{
								window.event.keyCode=0;
							}
						}
						if (cint==2)
						{
							if (!(window.event.keyCode > 47 && window.event.keyCode < 58))
							{
								window.event.keyCode=0;
							}
						}
						
					}
		function Save()
		{
			if ((document.Form1.ddlCompany.selectedIndex==-1 )||(document.Form1.ddlCompany.value==0))
			{
				alert("Select Company")
				document.Form1.txtCompName.focus();
				return false;
			}
			if ((document.Form1.ddlContacts.selectedIndex==-1 )||(document.Form1.ddlContacts.value==0 ))
			{
				alert("Select Contact")
				document.Form1.ddlContacts.focus();
				return false;
			}
			if (document.Form1.ddlOppType.value==0 )
			{
				alert("Select Opportunity Type")
				document.Form1.ddlOppType.focus();
				return false;
			}
		}
		
		function Add()
		{
			if (document.Form1.ddlOppType.value==1)
			{
			    if (document.Form1.chkInventory.checked==false)
			        {
			            alert("Please Check the Checkbox 'Select Product or Service Items from within our Company' to Add Items to the Opportunity")
			            return false;	
			        }
			}
			
			
			if (document.Form1.ddlItem.value==0 )
			{
				alert("Select Item")
				document.Form1.ddlItem.focus();
				return false;
			}
			if (document.Form1.txtUnits.value=="" )
			{
				alert("Enter Units")
				document.Form1.txtUnits.focus();
				return false;
			}
			if (document.Form1.txtPrice.value=="")
			{
				alert("Enter Price")
				document.Form1.txtPrice.focus();
				return false;
			}
		}
		function openUnit(a)
		{
			a=document.Form1.ddlItem.value
			window.open('../opportunity/frmUnitdtlsForItem.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ItemCode='+a,'','toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')
		}
		function openItem(a,b,c)
		{
			a=document.Form1.ddlItem.value
			b=document.Form1.txtUnits.value
			window.open('../opportunity/frmItemPriceRecommd.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ItemCode='+a+'&Unit='+b+'&DivID='+c,'','toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')
		}
		function ShowWindow(Page,q,att) 
		{
		
			if (att=='show')
			{
				document.getElementById(Page).style.visibility = "visible";
				return false;
		
			}
			if (att=='hide')
			{
				document.getElementById(Page).style.visibility = "hidden";
				return false;
		
			}
		}
		function Focus()
					{
						document.Form1.txtCompName.focus();
					}
	   	function openPurOpp()
		{
			
			window.open('../opportunity/frmSelectPurOpp.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppID=0','','toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')
		    return false;
		}
	function Close()
		{
		    window.close()
		    return false;
		}
		</script>
	</head>
	<body onload="Focus()" >
		<form id="Form1" method="post" runat="server">
		<asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
		<br />
	   	  
	    <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server">
				<tr>
					<td valign="bottom">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;New 
									Opportunity&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td align="right">
						<asp:Button ID="btnSave" Runat="server" Text="Save &amp; Close" CssClass="button"></asp:Button>
						<asp:button id="btnClose" Runat="server" CssClass="button" Text="Close" Width="50"></asp:button>
					</td>
				</tr>
			</table>
	        <asp:updatepanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" >
                <ContentTemplate>
              	</ContentTemplate>
			</asp:updatepanel>	
        
			<asp:table id="tblOppr" CellPadding="0" CellSpacing="0" BorderWidth="1" Runat="server" CssClass="aspTable"
				Width="100%" BorderColor="black" GridLines="None">
				<asp:TableRow>
					<asp:TableCell VerticalAlign="Top" >
		                <br />
						<table  width="100%" border="0">
							<tr>
								<td class="text" align="right" nowrap>Customer or Vendor<FONT color="#ff3333"> *</FONT></td>
								<td>
									<asp:textbox id="txtCompName" Runat="server" width="200" cssclass="signup"></asp:textbox>&nbsp;
								</td>
									<td align="center" ><asp:Button ID="btnGo" Runat="server" Text="Go" CssClass="button"></asp:Button>&nbsp;
									</td>
									<td><asp:dropdownlist id="ddlCompany" Runat="server" Width="200" AutoPostBack="True" CssClass="signup"></asp:dropdownlist>
									</td>
							</tr>
							<tr>
							
								<td class="text" align="right">Contact<FONT color="#ff3333"> *</FONT></td>
								<td>
									<asp:dropdownlist id="ddlContacts" Runat="server" Width="200" CssClass="signup"></asp:dropdownlist></td>
							    	<td class="text" align="right" > Opportunity Source</td>
								<td>
									<asp:dropdownlist id="ddlSource" Runat="server" CssClass="signup" Width="200"></asp:dropdownlist></td>
							</tr>
							<tr>
								<td class="text" align="right">Opportunity Type<FONT color="#ff3333"> *</FONT></td>
								<td>
									<asp:dropdownlist id="ddlOppType" Runat="server" CssClass="signup" Width="200" AutoPostBack="true" >
										<asp:ListItem Value="0">--Select One--</asp:ListItem>
										<asp:ListItem Value="1">Sales Opportunity</asp:ListItem>
										<asp:ListItem Value="2">Purchase Opportunity</asp:ListItem>
									</asp:dropdownlist></td>
									<asp:Panel ID="pnlPurrchase" runat="server"  Visible="false" >
								<td class="text" align="right"></td>
								<td class="normal1">
									<asp:CheckBox ID="chkDropShipped" Runat="server" Text="Outsourced or Drop Shipped"></asp:CheckBox></td>
									</asp:Panel>
									<asp:Panel ID="pnlSales" runat="server" Visible="false" >
								<td class="text" align="right">Campaign</td>
								<td>
									<asp:dropdownlist id="ddlCampaign" Runat="server" CssClass="signup" Width="200"></asp:dropdownlist></td>
								</asp:Panel>
							</tr>
						</table>
							<table align="center">
											
												<tr>
													<td class="normal1" rowSpan="111111115">
														<asp:image id="imgItem" Runat="server" BorderWidth="1" Width="100" BorderColor="black" Height="100"></asp:image><br>
														<asp:hyperlink id="hplImage" Runat="server" CssClass="hyperlink" NavigateUrl="">View Full Image</asp:hyperlink></td>
														<td>
														          <table id="tblItems" runat="server" >
														                <tr>
														                   <td class="normal1" vAlign="top" align="right">Item<FONT color="red">*</FONT></td>
													                        <td class="normal1" vAlign="top" colSpan="5">
														                        <asp:dropdownlist id="ddlItems" tabIndex="19" Runat="server" CssClass="signup" Width="450" AutoPostBack="true"></asp:dropdownlist>&nbsp;&nbsp;
														                        <asp:label id="Label6" Runat="server" CssClass="text_bold">List Price :</asp:label>
														                        <asp:label id="lblListPrice" Runat="server"></asp:label>
													                        </td>
														                </tr>
														                <tr>
														                    <td class="text_bold" align="right">Description </td>
													                        <td class="normal1" colSpan="5">
														                        <asp:TextBox ID="txtdesc" runat="server"  TextMode="MultiLine" Width="500" ></asp:TextBox>
														                     </td>
														                </tr>
														                <tr>
														                    <td class="normal1" align="right">Type</td>
													                        <td class="normal1">
														                        <asp:dropdownlist id="ddltype" Runat="server" CssClass="signup" Width="180" Enabled="False">
															                        <asp:ListItem Value="P">Product</asp:ListItem>
															                        <asp:ListItem Value="S">Service</asp:ListItem>
															                        <asp:ListItem Value="A">Accessory</asp:ListItem>
														                        </asp:dropdownlist>
														                    </td>
													                        <td class="normal1" align="right">
														                        <asp:hyperlink id="hplUnit" Runat="server" CssClass="hyperlink">Units/Hours</asp:hyperlink><FONT color="red">*</FONT>
														                        </td>
														                        <td class="normal1" nowrap>
														                            <asp:textbox id="txtunits" Runat="server" CssClass="signup" Width="30px" MaxLength="3"></asp:textbox>
														                            <asp:hyperlink id="hplPrice" Runat="server" CssClass="hyperlink">Price</asp:hyperlink><font color="red">*</font>
														                            <asp:textbox id="txtprice" Runat="server" CssClass="signup" Width="90px" MaxLength="11"></asp:textbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														                            <asp:button id="btnAdd" Runat="server" width="60px" CssClass="button" Text="Add"></asp:button>&nbsp;&nbsp;&nbsp;<asp:button id="btnEditCancel" Runat="server" Visible="false" CssClass="button" Text="Cancel"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														                     </td>
														                </tr>
														          </table>
														
														</td>
													
												</tr>
											</table>
											<table width="100%"  id="tblItemWareHouse" runat="server"  Visible="false">
											    <tr>
											        <td class="text_bold">
											            Choose Items From Warehouse
											        </td>
											    </tr>
											    <tr>
											        <td>
											          <igtbl:ultrawebgrid id="uwItemSel" Width="100%" DisplayLayout-AllowRowNumberingDefault="ByDataIsland"  runat="server" Browser="Xml"   Height="100%">
					                                    <DisplayLayout AutoGenerateColumns="true"  RowHeightDefault="18"  AllowAddNewDefault="Yes" Version="3.00" SelectTypeRowDefault="Single"
					                                    ViewType="Hierarchical" TableLayout="Auto" SelectTypeCellDefault="Extended" BorderCollapseDefault="Separate" AllowColSizingDefault="Free" 
					                                    Name="uwItemSel" EnableClientSideRenumbering="true" SelectTypeColDefault="Extended" AllowUpdateDefault="Yes">
					                                     <HeaderStyleDefault VerticalAlign="Middle" Font-Size="8pt" Font-Names="Arial" BorderStyle="Solid" HorizontalAlign="Left" ForeColor="white" BackColor="#52658C">
                                                            <Padding Left="2px" Right="2px"></Padding>
                                                            <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
                                                        </HeaderStyleDefault>
                                                        <RowSelectorStyleDefault BackColor="White"></RowSelectorStyleDefault>
                                                        <FrameStyle Width="100%" Cursor="Default" BorderWidth="3px" Font-Size="8pt" Font-Names="Arial" BorderStyle="Double"></FrameStyle>
                                                        <FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
                                                            <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
                                                        </FooterStyleDefault>
                                                        <EditCellStyleDefault BorderWidth="0px" BorderStyle="None"></EditCellStyleDefault>
                                                        <SelectedRowStyleDefault ForeColor="White" BackColor="#666666"></SelectedRowStyleDefault>
                                                        <RowStyleDefault BorderWidth="1px" Font-Size="8pt" Font-Names="Arial" BorderColor="Gray" BorderStyle="Solid" BackColor="White">
                                                            <Padding Left="5px" Right="5px"></Padding>
                                                            <BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
                                                        </RowStyleDefault>
                                                        <RowExpAreaStyleDefault BackColor="LightSteelBlue"></RowExpAreaStyleDefault>
				                                    </DisplayLayout>
											                    <Bands>
												                    <igtbl:UltraGridBand AllowDelete="No" AllowAdd="Yes"  AddButtonCaption="WareHouse" BaseTableName="WareHouse" Key="WareHouse">
												                    <Columns>
												                        <igtbl:UltraGridColumn  Hidden="true" Width="100%" IsBound="true" BaseColumnName="numWareHouseItemID" Key="numWareHouseItemID" >
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn Hidden="true" IsBound="true" BaseColumnName="numWareHouseID" Key="numWareHouseID" >
												                        </igtbl:UltraGridColumn>
												                         <igtbl:UltraGridColumn Hidden="true" IsBound="true" BaseColumnName="numItemCode" Key="numItemCode" >
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn HeaderText="Warehouse" AllowUpdate="No" IsBound="true" BaseColumnName="vcWarehouse" Key="vcWarehouse" >
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn HeaderText="On Hand" AllowUpdate="No" IsBound="true" BaseColumnName="OnHand" Key="OnHand" >
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn HeaderText="On Order" AllowUpdate="No" IsBound="true" BaseColumnName="OnOrder" Key="OnOrder" >
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn HeaderText="Reorder" AllowUpdate="No" IsBound="true" BaseColumnName="Reorder" Key="Reorder" >
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn HeaderText="Allocation" AllowUpdate="No" IsBound="true" BaseColumnName="Allocation" Key="Allocation" >
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn HeaderText="BackOrder" AllowUpdate="No" IsBound="true" BaseColumnName="BackOrder" Key="BackOrder" >
												                        </igtbl:UltraGridColumn>
												                          <igtbl:UltraGridColumn HeaderText="Units" AllowUpdate="Yes" Format="###,##0.00"  Key="Units" >
												                        </igtbl:UltraGridColumn>
												                    </Columns>
												                    </igtbl:UltraGridBand>
												                    <igtbl:UltraGridBand  AllowDelete="No" AllowAdd="Yes" AllowUpdate="No"    AddButtonCaption="Serialized Items" BaseTableName="SerializedItems" Key="SerializedItems">
													                    <Columns>
												                            <igtbl:UltraGridColumn AllowUpdate="Yes"  Type="CheckBox" Key="Select">
												                            </igtbl:UltraGridColumn>
													                    </Columns>
												                    </igtbl:UltraGridBand>
											                    </Bands>
										                    </igtbl:ultrawebgrid>
											        </td>
											    </tr>
											</table>
										<table width="100%" id="tblOptions" runat="server"  Visible="false">
										 <tr>
											        <td class="text_bold">
											            Choose Options And Accessories from Warehouse
											        </td>
											    </tr>
											    <tr>
											        <td>
											         <igtbl:ultrawebgrid id="uwOption" Height="100%" DisplayLayout-AllowRowNumberingDefault="ByDataIsland"  runat="server" Browser="Xml"  Width="100%" >
					                                            <DisplayLayout AutoGenerateColumns="false" AllowAddNewDefault="Yes" RowHeightDefault="18px" Version="3.00" SelectTypeRowDefault="Single"
					                                            ViewType="Hierarchical" SelectTypeCellDefault="Extended" BorderCollapseDefault="Separate" AllowColSizingDefault="Free"
					                                            Name="uwOption" EnableClientSideRenumbering="true"  TableLayout="Auto" SelectTypeColDefault="Extended" AllowUpdateDefault="Yes">
                        					                    <HeaderStyleDefault VerticalAlign="Middle" Font-Size="8pt" Font-Names="Arial" BorderStyle="Solid" HorizontalAlign="Left" ForeColor="white" BackColor="#52658C">
                                                                    <Padding Left="5px" Right="5px"></Padding>
                                                                    <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
                                                                </HeaderStyleDefault>
                                                                <RowSelectorStyleDefault BackColor="White"></RowSelectorStyleDefault>
                                                                <FrameStyle Width="100%" Cursor="Default" BorderWidth="3px" Font-Size="8pt" Font-Names="Arial" BorderStyle="Double"></FrameStyle>
                                                                <FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
                                                                    <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
                                                                </FooterStyleDefault>
                                                                <EditCellStyleDefault BorderWidth="0px" BorderStyle="None"></EditCellStyleDefault>
                                                                <SelectedRowStyleDefault ForeColor="White" BackColor="#666666"></SelectedRowStyleDefault>
                                                                <RowStyleDefault BorderWidth="1px" Font-Size="8pt" Font-Names="Arial" BorderColor="Gray" BorderStyle="Solid" BackColor="White">
                                                                    <Padding Left="2px" Right="2px"></Padding>
                                                                    <BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
                                                                </RowStyleDefault>
                                                                <RowExpAreaStyleDefault BackColor="LightSteelBlue"></RowExpAreaStyleDefault>
				                                            </DisplayLayout>
											               <Bands>
												                                            <igtbl:UltraGridBand AllowDelete="No" AllowAdd="Yes" AddButtonCaption="WareHouse" BaseTableName="WareHouse" Key="WareHouse">
												                                            <Columns>
												                                                <igtbl:UltraGridColumn Hidden="true"   BaseColumnName="numWareHouseItemID" Key="numWareHouseItemID" >
												                                                </igtbl:UltraGridColumn>
												                                                <igtbl:UltraGridColumn Hidden="true"  BaseColumnName="numWareHouseID" Key="numWareHouseID" >
												                                                </igtbl:UltraGridColumn>
												                                                 <igtbl:UltraGridColumn HeaderText="numItemCode" Hidden="true"  BaseColumnName="numItemCode"   Key="numItemCode" >
												                                                </igtbl:UltraGridColumn>
												                                                <igtbl:UltraGridColumn HeaderText="Item" AllowUpdate="No" BaseColumnName="vcItemName"  Key="vcItemName" >
												                                                </igtbl:UltraGridColumn>
												                                                <igtbl:UltraGridColumn HeaderText="Warehouse" AllowUpdate="No" BaseColumnName="vcWarehouse" Key="vcWarehouse" >
												                                                </igtbl:UltraGridColumn>
												                                                <igtbl:UltraGridColumn HeaderText="On Hand" AllowUpdate="No"  BaseColumnName="OnHand" Key="OnHand" >
												                                                </igtbl:UltraGridColumn>
												                                                <igtbl:UltraGridColumn HeaderText="On Order" AllowUpdate="No"  BaseColumnName="OnOrder" Key="OnOrder" >
												                                                </igtbl:UltraGridColumn>
												                                                <igtbl:UltraGridColumn HeaderText="Reorder" AllowUpdate="No"  BaseColumnName="Reorder" Key="Reorder" >
												                                                </igtbl:UltraGridColumn>
												                                                <igtbl:UltraGridColumn HeaderText="Allocation" AllowUpdate="No" BaseColumnName="Allocation" Key="Allocation" >
												                                                </igtbl:UltraGridColumn>
												                                                <igtbl:UltraGridColumn HeaderText="BackOrder"  AllowUpdate="No"  BaseColumnName="BackOrder" Key="BackOrder" >
												                                                </igtbl:UltraGridColumn>
												                                                  <igtbl:UltraGridColumn HeaderText="Units" Format="###,##0.00" AllowUpdate="Yes" Key="Units" >
												                                                </igtbl:UltraGridColumn>
												                                                 <igtbl:UltraGridColumn HeaderText="Price" Format="###,##0.00" AllowUpdate="Yes"   Key="Price" >
												                                                </igtbl:UltraGridColumn>
												                                               
												                                            </Columns>
												                                            </igtbl:UltraGridBand>
												                                        </Bands>
											                                  
										                                            </igtbl:ultrawebgrid>
											        </td>
											    </tr>	
							        </table>
										                    
										                    
									<br />       
									<table width="100%"  id="tblProducts" runat="server" visible="false">
											    <tr>
											        <td class="text_bold">
											            Items Added To Opportunity
											        </td>
											    </tr>
											    <tr>
											        <td>          
									                <igtbl:ultrawebgrid Width="100%"  DisplayLayout-AutoGenerateColumns="false"   id="ucItem" DisplayLayout-AllowRowNumberingDefault="ByDataIsland"  runat="server" Browser="Xml">
					                                    <DisplayLayout AllowAddNewDefault="Yes" RowHeightDefault="18px" Version="3.00" SelectTypeRowDefault="Single"
					                                    ViewType="Hierarchical" SelectTypeCellDefault="Extended" BorderCollapseDefault="Separate" AllowColSizingDefault="Free"
					                                    Name="UltraWebGrid1" EnableClientSideRenumbering="true"  TableLayout="Fixed"  SelectTypeColDefault="Extended" AllowUpdateDefault="Yes">
					                                   <HeaderStyleDefault VerticalAlign="Middle" Font-Size="8pt" Font-Names="Arial" BorderStyle="Solid" HorizontalAlign="Left" ForeColor="white" BackColor="#52658C">
                                                        <Padding Left="5px" Right="5px"></Padding>
                                                        <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
                                                    </HeaderStyleDefault>
                                                    <RowSelectorStyleDefault BackColor="White"></RowSelectorStyleDefault>
                                                    <FrameStyle Width="100%" Cursor="Default" BorderWidth="3px" Font-Size="8pt" Font-Names="Arial" BorderStyle="Double"></FrameStyle>
                                                    <FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
                                                        <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
                                                    </FooterStyleDefault>
                                                    <EditCellStyleDefault BorderWidth="0px" BorderStyle="None"></EditCellStyleDefault>
                                                    <SelectedRowStyleDefault ForeColor="White" BackColor="#666666"></SelectedRowStyleDefault>
                                                    <RowStyleDefault BorderWidth="1px" Font-Size="8pt" Font-Names="Arial" BorderColor="Gray" BorderStyle="Solid" BackColor="White">
                                                        <Padding Left="5px" Right="5px"></Padding>
                                                        <BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
                                                    </RowStyleDefault>
                                                    <RowExpAreaStyleDefault BackColor="LightSteelBlue"></RowExpAreaStyleDefault>
				                                           </DisplayLayout>
											                    <Bands >
												                    <igtbl:UltraGridBand  AllowDelete="Yes" BaseTableName="Item" Key="Item">
												                    <Columns>
												                        <igtbl:UltraGridColumn Hidden="true" Width="100%"  IsBound="true" BaseColumnName="numoppitemtCode" Key="numoppitemtCode" >
												                           
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn Hidden="true"   IsBound="true" BaseColumnName="numWarehouseItmsID" Key="numWareHouseItemID" >
												                           
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn Hidden="true"   IsBound="true" BaseColumnName="numWareHouseID" Key="numWareHouseID" >
												                           
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn Hidden="true"   IsBound="true" BaseColumnName="numItemCode" Key="numItemCode" >
												                           
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn HeaderText="Item" Width="20%" AllowUpdate="No" IsBound="true" BaseColumnName="vcItemName" Key="vcItemName" >
												                           
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn HeaderText="Warehouse" Width="20%" AllowUpdate="No" IsBound="true" BaseColumnName="Warehouse" Key="Warehouse" >
												                           
												                        </igtbl:UltraGridColumn>
												                           <igtbl:UltraGridColumn HeaderText="Type" Width="10%" AllowUpdate="No" IsBound="true" BaseColumnName="ItemType" Key="ItemType" >
												                           
												                        </igtbl:UltraGridColumn>
												                          <igtbl:UltraGridColumn CellMultiline="Yes" Width="20%" HeaderText="Desc" AllowUpdate="No" IsBound="true" BaseColumnName="vcItemDesc" Key="vcItemDesc" >
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn HeaderText="Unit/Hours" Width="10%" Format="###,##0.00" AllowUpdate="No" IsBound="true" BaseColumnName="numUnitHour" Key="numUnitHour" >
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn HeaderText="Unit Price" Width="10%" Format="###,##0.00" AllowUpdate="No" IsBound="true" BaseColumnName="monPrice" Key="monPrice" >
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn HeaderText="Amount" Width="10%" Format="###,##0.00" AllowUpdate="No" IsBound="true" BaseColumnName="monTotAmount" Key="monTotAmount" >
												                        </igtbl:UltraGridColumn>
												                        <igtbl:UltraGridColumn Type="Button" Key="Action" ServerOnly="false"  CellButtonDisplay="Always">
												                           
												                        </igtbl:UltraGridColumn>
												                    </Columns>
												                    </igtbl:UltraGridBand>
												                    <igtbl:UltraGridBand AllowDelete="No" AllowUpdate="No"  BaseTableName="SerialNo" Key="SerialNo">
													                    <Columns>
													                      <igtbl:UltraGridColumn Hidden="true"   IsBound="true" BaseColumnName="numWarehouseItmsDTLID" Key="numWarehouseItmsDTLID" ></igtbl:UltraGridColumn>
												                           <igtbl:UltraGridColumn Hidden="true"   IsBound="true" BaseColumnName="numoppitemtCode" Key="numoppitemtCode" >
												                        </igtbl:UltraGridColumn>
														                    <igtbl:UltraGridColumn  HeaderText="Serial No" Key="vcSerialNo" IsBound="True" Width="200px" BaseColumnName="vcSerialNo">
												
															                    <Header Key="vcSerialNo" Caption="Serial No"></Header>
														                    </igtbl:UltraGridColumn>
                    													
													                    </Columns>
												                    </igtbl:UltraGridBand>
											                    </Bands>
										                    </igtbl:ultrawebgrid>
						                                   </td>
											    </tr>
											</table>
					
				
						<br/>
					</asp:TableCell>
				</asp:TableRow>
			</asp:table>
				  	
					
		</form>
	</body>
</html>
--%>