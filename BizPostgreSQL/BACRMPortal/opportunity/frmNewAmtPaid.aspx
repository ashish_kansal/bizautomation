﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmNewAmtPaid.aspx.vb" Inherits="BACRMPortal.frmNewAmtPaid" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Receive Payment</title>
    <link href="../CSS/NewUiStyle.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script src="../JavaScript/jquery.min.js" type="text/javascript"></script>
    <script src="../JavaScript/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../JavaScript/jquery.Tooltip.js" type="text/javascript"></script>
    <script type="text/javascript" src="../JavaScript/jquery.validate.custom-script.js"></script>
    <script type="text/javascript" src="../JavaScript/jquery.validate.min.js"></script>
    <style type="text/css">
        .Button_POS
        {
            background: -webkit-gradient( linear, left top, left bottom, color-stop(0.05, #ededed), color-stop(1, #cfcdcf) );
            background: -moz-linear-gradient( center top, #ededed 5%, #cfcdcf 100% );
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed', endColorstr='#cfcdcf');
            background-color: #ededed;
            -moz-border-radius: 42px;
            -webkit-border-radius: 42px;
            border-radius: 42px;
            border: 1px solid #52658c;
            display: inline-block;
            color: #777777;
            font-family: arial;
            font-size: 15px;
            font-weight: bold;
            padding: 7px 15px;
            text-decoration: none;
        }

        .disabled
        {
            background: none !important;
        }
    </style>
    <script language="javascript" type="text/javascript">

        var bAmount = false;
        var bCreditApplied = false;
        var bInvoiceAmount = false;
        var bInvoiceChk = false;
        var flag = true;

        function UpdateTotal() {

            var Total = 0.00;
            var TotalBalDue = 0.00;
            var txtAmountToPay = 0;
            var hdnDepositeDetailID = 0;
            var hdnAmountPaidInDeposite = 0;
            var hdnExchangeRateOfOrder = 1;
            var hdnExchangeRateCurrent = 1;
            var Credits = 0;

            var lblBalanceAmt = 0.00;
            var lblBalDue = 0;

            $(".dg tr").not(".hs,.fs").each(function () {
                lblBalDue = Number($(this).find("#lblBalDue").text());
                lblBalanceAmt = lblBalanceAmt + lblBalDue;
            });

            $(".dg tr").not(".hs,.fs").each(function () {

                txtAmountToPay = Number($(this).find("#txtAmountToPay").val());
                hdnDepositeDetailID = Number($(this).find("#hdnDepositeDetailID").val());
                hdnAmountPaidInDeposite = Number($(this).find("#hdnAmountPaidInDeposite").val());
                lblBalDue = Number($(this).find("#lblBalDue").text());
                chk = $(this).find("[id*='chk']");

                if (bInvoiceAmount) {
                    if ($(chk).is(':checked')) {
                        if (txtAmountToPay > 0 && txtAmountToPay <= lblBalDue) {
                            //                        Total = Total + parseFloat(txtAmountToPay)
                            $(this).find("input[type = 'checkbox']").prop("checked", true)
                        }
                        else if (txtAmountToPay > lblBalDue) {
                            //it should automatically change amount to balance due
                            //if (hdnDepositeDetailID == 0)
                            $(this).find("#txtAmountToPay").val(lblBalDue.toFixed(2))
                            //                            else
                            //                                $(this).find("#txtAmountToPay").val(hdnAmountPaidInDeposite.toFixed(2))

                            $(this).find("input[type = 'checkbox']").prop("checked", true)
                            //                        txtAmountToPay = lblBalDue.toFixed(2)
                            //Total = Total + parseFloat($(this).find("#txtAmountToPay").val())
                        }
                    }
                }
                else if (bInvoiceChk) {
                    if ($(chk).is(':checked')) {
                        if (txtAmountToPay > 0 && txtAmountToPay <= lblBalDue) {
                            //Total = Total + parseFloat(txtAmountToPay)
                            $(this).find("input[type = 'checkbox']").prop("checked", true)
                        }
                        else if (txtAmountToPay > lblBalDue) {
                            //it should automatically change amount to balance due
                            //if (hdnDepositeDetailID == 0)
                            $(this).find("#txtAmountToPay").val(lblBalDue.toFixed(2))
                            //                            else
                            //                                $(this).find("#txtAmountToPay").val(hdnAmountPaidInDeposite.toFixed(2))

                            txtAmountToPay = lblBalDue.toFixed(2)
                            //Total = Total + parseFloat($(this).find("#txtAmountToPay").val())
                        }
                    }
                }

                Total = Total + Number($(this).find("#txtAmountToPay").val());

                if (lblBalDue > 0) {
                    TotalBalDue = TotalBalDue + parseFloat(lblBalDue)
                }
            });

            bInvoiceAmount = false;
            bInvoiceChk = false;
            bAmount = false;
            bCreditApplied = false;

            Total = Total.toFixed(2);
            TotalBalDue = TotalBalDue.toFixed(2);
            $('#lblAmountToPay').text(Total);
            $("#lblFooterTotalPayment").text(Total);
            $("#lblFooterTotalBalanceDue").text(TotalBalDue);
            $("#lblBalanceAmt").text(TotalBalDue);
            $("#hdnAmountPaid").val(Total);

            return true;
        }

        $(document).ready(function () {
            InitializeValidation();

            $("#chkAll").change(function () {
                $(".dg tr input[type = 'checkbox']").not("#chkAll").prop("checked", $("#chkAll").is(':checked'))
                flag = false;
                $(".dg tr input[type = 'checkbox']").not("#chkAll").change();
                flag = true;
                bInvoiceChk = true;
                UpdateTotal();
            });

            $(".dg tr input[type = 'checkbox']").not("#chkAll").change(function () {

                //console.log($(CurrentRow).find('#txtAmountToPay').val());
                var CurrentRow = $(this).parents("tr").filter(':first')
                var lblBalDue = parseFloat($(CurrentRow).find("#lblBalDue").text()).toFixed(2)
                if ($(this).is(':checked')) {
                    //                    if (Number($(CurrentRow).find("#hdnDepositeDetailID").val()) > 0)
                    //                        $(CurrentRow).find("#txtAmountToPay").val(Number($(CurrentRow).find("#hdnAmountPaidInDeposite").val()).toFixed(2))
                    //                    else
                    $(CurrentRow).find("#txtAmountToPay").val(lblBalDue)
                } else {
                    //console.log($(CurrentRow).find("#txtAmountToPay").val())
                    $(CurrentRow).find("#txtAmountToPay").val('')
                    //console.log($(CurrentRow).find("#txtAmountToPay").val())
                }

                bInvoiceChk = true;

                if (flag == true)
                    UpdateTotal();
                //
                //UpdateAmtToPayTextBox();
            });

            UpdateTotal();

            $(".AmountToPay").change(function () {
                bInvoiceAmount = true;

                $(this).val(Number($(this).val()).toFixed(2));

                var CurrentRow = $(this).parent().parent();
                txtAmountToPay = Number($(CurrentRow).find("#txtAmountToPay").val());
                chk = $(CurrentRow).find("[id*='chk']");

                if (txtAmountToPay > 0)
                    $(chk).prop("checked", true);
                else
                    $(chk).prop("checked", false);

                UpdateTotal();
            });
        });

        function Save(d) {
            if (document.getElementById('txtCCNo').value == '') {
                alert("Enter Credit Card No")
                document.getElementById('txtCCNo').focus();
                return false;
            }
            if (document.getElementById('txtCHName').value == '') {
                alert("Enter Card Holder Name")
                document.getElementById('txtCHName').focus();
                return false;
            }
            if (document.getElementById('txtCVV2').value == '') {
                alert("Enter CVV2")
                document.getElementById('txtCVV2').focus();
                return false;
            }
            if (document.getElementById('ddlCardType').value == 0) {
                alert("Select Card Type")
                document.getElementById('ddlCardType').focus();
                return false;
            }
            if (Number($("#hdnAmountPaid").val()) == 0) {
                alert("Please select invoice to pay amount.")
                return false;
            }

            if (confirm('Your credit card will now be charged the total amounts due as displayed next to the "Amount To Pay" footer. Press "Ok" to to charge the card, otherwise press "Cancel"') == true) {
                UpdateTotal();
                return true;
            }
            else {
                return false;
            }
        }

        function WindowClose() {
            window.close();
            return false;
        }
        function Close() {
            window.close();
        }
        function CheckNumber(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;

            if (cint == 1) {
                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16 || e.keyCode == 9)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16 || e.keyCode == 9)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
        }
        function IsNumeric(sText) {
            var ValidChars = "0123456789.";
            var IsNumber = true;
            var Char;
            if (sText == undefined) return true;
            for (i = 0; i < sText.length && IsNumber == true; i++) {
                Char = sText.charAt(i);
                if (ValidChars.indexOf(Char) == -1) {
                    IsNumber = false;
                }
            }
            return IsNumber;
        }

        function swipedCreditCard() {
            txtSwipe = document.getElementById('txtSwipe');

            txtCHName = document.getElementById('txtCHName');
            txtCCNo = document.getElementById('txtCCNo');
            ddlMonth = document.getElementById('ddlMonth');
            ddlYear = document.getElementById('ddlYear');
            txtCVV2 = document.getElementById('txtCVV2');

            //boolean values to determine which method to parse
            blnCarrotPresent = false;
            blnEqualPresent = false;
            blnBothPresent = false;

            strParse = txtSwipe.value;

            if (strParse.length <= 10) {
                alert("Error Parsing Card.\r\n");
                txtSwipe.value = '';
                return false;
            }

            strCarrotPresent = strParse.indexOf("^");
            strEqualPresent = strParse.indexOf("=");

            if (strCarrotPresent > 0) {
                blnCarrotPresent = true;
            }

            if (strEqualPresent > 0) {
                blnEqualPresent = true;
            }

            if (blnCarrotPresent == true) {
                //contains carrot
                strParse = '' + strParse + ' ';
                arrSwipe = new Array(4);
                arrSwipe = strParse.split("^");

                if (arrSwipe.length > 2) {
                    account = stripAlpha(arrSwipe[0].substring(1, arrSwipe[0].length));
                    account_name = arrSwipe[1];
                    exp_month = arrSwipe[2].substring(2, 4);
                    exp_year = arrSwipe[2].substring(0, 2);
                    namepieces = account_name.split('/');
                }
                else {
                    alert("Error Parsing Card.\r\n");
                    txtSwipe.value = '';
                    return false;
                }
            }
            else if (blnEqualPresent == true) {
                //equal only delimiter
                sCardNumber = strParse.substring(1, strEqualPresent);
                account = stripAlpha(sCardNumber);
                exp_month = strParse.substr(strEqualPresent + 1, 2);
                exp_year = strParse.substr(strEqualPresent + 3, 2);
            }

            if (isValidCreditCard(account) == false) {
                alert("Error Parsing Card.\r\n Invalid credit card. \r\n");
                txtSwipe.value = '';
                return false;
            }

            selectOptionByValue(ddlMonth, exp_month);
            selectOptionByValue(ddlYear, exp_year);
            txtCCNo.value = account;
            selectOptionByValue(document.getElementById('ddlCardType'), getCreditCardType(account));

            if (blnCarrotPresent == true) {
                var l = namepieces[0];
                var f = namepieces[1];
                txtCHName.value = namepieces[0] + " " + namepieces[1];
            }

            trackData = strParse.split('?');
            document.getElementById('hdnSwipeTrack1').value = trackData[0] + '?';

            if (trackData.length = 2) {
                document.getElementById('hdnSwipeTrack2').value = trackData[1] + '?';
            }

            txtCVV2.style.visibility = "hidden";
            //txtCVV2.focus();
            return true;
        }

        function stripAlpha(sInput) {
            if (sInput == null) return '';
            return sInput.replace(/[^0-9]/g, '');
        }

        function selectOptionByValue(selObj, val) {
            var A = selObj.options, L = A.length;
            while (L) {
                if (A[--L].value == val) {
                    selObj.selectedIndex = L;
                    L = 0;
                }
            }
        }

        function getCreditCardType(accountNumber) {

            //start without knowing the credit card type
            var result = "0";

            // Mastercard: length 16, prefix 51-55, dashes optional.
            if (/^5[1-5]\d{2}-?\d{4}-?\d{4}-?\d{4}$/.test(accountNumber)) {
                result = "14883";
            }

                // Visa: length 16, prefix 4, dashes optional.
            else if (/^4\d{3}-?\d{4}-?\d{4}-?\d{4}$/.test(accountNumber)) {
                result = "14882";
            }

                // Discover: length 16, prefix 6011, dashes optional.
            else if (/^6011-?\d{4}-?\d{4}-?\d{4}$/.test(accountNumber)) {
                result = "14885";
            }

                // American Express: length 15, prefix 34 or 37.
            else if (/^3[4,7]\d{13}$/.test(accountNumber)) {
                result = "14884";
            }

                // Diners: length 14, prefix 30, 36, or 38.
            else if (/^3[0,6,8]\d{12}$/.test(accountNumber)) {
                result = "14886";
            }
            return result;
        }

        function isValidCreditCard(ccnum) {
            var sum = 0, alt = false, i = ccnum.length - 1, num;

            if (ccnum.length < 13 || ccnum.length > 19) {
                return false;
            }

            // Remove all dashes for the checksum checks to eliminate negative numbers
            ccnum = ccnum.split("-").join("");

            // Checksum ("Mod 10")
            // Add even digits in even length strings or odd digits in odd length strings.
            var checksum = 0;
            for (var i = (2 - (ccnum.length % 2)) ; i <= ccnum.length; i += 2) {
                checksum += parseInt(ccnum.charAt(i - 1));
            }

            // Analyze odd digits in even length strings or even digits in odd length strings.
            for (var i = (ccnum.length % 2) + 1; i < ccnum.length; i += 2) {
                var digit = parseInt(ccnum.charAt(i - 1)) * 2;
                if (digit < 10) { checksum += digit; } else { checksum += (digit - 9); }
            }
            if ((checksum % 10) == 0) return true; else return false;
        }

        function windowResize() {
            var width = document.getElementById("resizeDiv").offsetWidth;
            var height = document.getElementById("resizeDiv").offsetHeight;
            window.resizeTo(width + 35, height + 150);
        }

        function OpenBizInvoice(a, b) {
            if (a > 0 && b > 0) {
                window.open('../Opportunity/frmBizInvoice.aspx?Show=True&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=0,titlebar=0,menubar=0,location=1,left=100,top=50,width=750,height=800,scrollbars=yes,resizable=yes');
                return false;
            }
            return false;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server" ID="Sc1" />
        <div id="resizeDiv" style="float: left; margin-top: 5px; margin-left: 5px;">
            <asp:Panel runat="server" ID="pnlCreditCardForm">
                <fieldset>
                    <legend class="text_bold">Pay By Credit Card</legend>
                    <table>
                        <tr>
                            <td align="right">Saved Credit Card #s
                            </td>
                            <td align="left">
                                <asp:DropDownList runat="server" ID="ddlCards" CssClass="signup" AutoPostBack="true">
                                </asp:DropDownList>
                                <small>(optional)</small>
                            </td>
                            <td align="right" id="tdSwipe1" runat="server">Swipe
                            </td>
                            <td id="tdSwipe2" runat="server">
                                <asp:TextBox ID="txtSwipe" Width="200" runat="server" CssClass="signup" MaxLength="250">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">Contact
                            </td>
                            <td>
                                <asp:DropDownList runat="server" ID="ddlContact">
                                </asp:DropDownList>
                            </td>

                        </tr>
                        <tr>
                            <td width="200" align="right">Credit Card No
                            </td>
                            <td>
                                <asp:TextBox Width="150" ID="txtCCNo" CssClass="signup" runat="server" MaxLength="16">
                                </asp:TextBox>
                            </td>
                            <td align="right">Card Holder Name
                            </td>
                            <td>
                                <asp:TextBox ID="txtCHName" Width="200" runat="server" CssClass="signup" MaxLength="100">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">CVV
                            </td>
                            <td>
                                <asp:TextBox ID="txtCVV2" CssClass="signup" runat="server" MaxLength="4">
                                </asp:TextBox>
                            </td>
                            <td align="right">Valid Upto
                            </td>
                            <td>
                                <asp:DropDownList CssClass="signup" ID="ddlMonth" runat="server">
                                    <asp:ListItem Value="01">01</asp:ListItem>
                                    <asp:ListItem Value="02">02</asp:ListItem>
                                    <asp:ListItem Value="03">03</asp:ListItem>
                                    <asp:ListItem Value="04">04</asp:ListItem>
                                    <asp:ListItem Value="05">05</asp:ListItem>
                                    <asp:ListItem Value="06">06</asp:ListItem>
                                    <asp:ListItem Value="07">07</asp:ListItem>
                                    <asp:ListItem Value="08">08</asp:ListItem>
                                    <asp:ListItem Value="09">09</asp:ListItem>
                                    <asp:ListItem Value="10">10</asp:ListItem>
                                    <asp:ListItem Value="11">11</asp:ListItem>
                                    <asp:ListItem Value="12">12</asp:ListItem>
                                </asp:DropDownList>
                                <asp:DropDownList CssClass="signup" ID="ddlYear" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">Card Type:
                            </td>
                            <td>
                                <asp:DropDownList runat="server" ID="ddlCardType" CssClass="signup" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </asp:Panel>
            <table width="100%">
                <tr>
                    <td class="normal4" align="center">
                        <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <strong>Invoices and outstanding transactions</strong><br />
                        <asp:DataGrid ID="dgOpportunity" runat="server" Width="1000px" CssClass="dg" AllowSorting="True"
                            AutoGenerateColumns="False" ClientIDMode="Static" ShowFooter="true">
                            <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                            <ItemStyle CssClass="is"></ItemStyle>
                            <HeaderStyle CssClass="hs"></HeaderStyle>
                            <FooterStyle CssClass="fs"></FooterStyle>
                            <Columns>
                                <asp:TemplateColumn>
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkAll" runat="server" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chk" CssClass="chkSelct" runat="server" Style="padding-left: 10px;"
                                            Checked='<%# Eval("numDepositeDetailID")%>' />
                                        <asp:HiddenField runat="server" ID="hdnOppBizDocID" Value='<%# Eval("numOppBizDocsId") %>' />
                                        <asp:HiddenField runat="server" ID="hdnOppID" Value='<%# Eval("numOppId") %>' />
                                        <asp:HiddenField runat="server" ID="hdnDepositeID" Value='<%# Eval("numDepositID")%>' />
                                        <asp:HiddenField runat="server" ID="hdnAmountPaidInDeposite" Value='<%# Eval("monAmountPaidInDeposite")%>' />
                                        <asp:HiddenField runat="server" ID="hdnDepositeDetailID" Value='<%# Eval("numDepositeDetailID")%>' />
                                        <asp:HiddenField runat="server" ID="hdnExchangeRateOfOrder" Value='<%# Eval("fltExchangeRateOfOrder")%>' />
                                        <asp:HiddenField runat="server" ID="hdnExchangeRateCurrent" Value='<%# Eval("fltExchangeRateCurrent")%>' />
                                        <asp:HiddenField runat="server" ID="hdnBizDocID" Value='<%# Eval("vcBizDocID")%>' />
                                        <asp:Label Text='<%# Eval("numContactID")%>' runat="server" Style="display: none"
                                            ID="lblOrderContactID" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="dtFromDate" HeaderText="Billing Date"></asp:BoundColumn>
                                <asp:BoundColumn DataField="dtDueDate" HeaderText="Due Date"></asp:BoundColumn>
                                <asp:TemplateColumn>
                                    <HeaderTemplate>
                                        BizDoc
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <a href="#" onclick="return OpenBizInvoice('<%# Eval("numOppId") %>','<%# Eval("numOppBizDocsId") %>');"
                                            title='<%#Eval("vcBizDocID")%>'>
                                            <%#Eval("vcBizDocID")%></a>
                                        <span class='gray'>
                                            <%# Eval("vcPOppName")%>
                                        </span>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Original Amount" SortExpression="">
                                    <ItemStyle HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#Eval("varCurrSymbol") + " " + String.Format("{0:###00.00}", Eval("monDealAmount"))%>
                                        <%#IIf(Eval("BaseCurrencySymbol") <> Eval("varCurrSymbol"), "<span class='gray'>(" + Eval("BaseCurrencySymbol") + " " + String.Format("{0:###00.00}", Eval("monDealAmount") * Eval("fltExchangeRateOfOrder")) + ")</span>", "")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Open Balance" SortExpression="">
                                    <ItemStyle HorizontalAlign="Right" />
                                    <FooterStyle HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#Eval("varCurrSymbol")%>
                                        <asp:Label ID="lblBalDue" Text='<%#  String.Format("{0:###00.00}", Eval("baldue"))%>'
                                            runat="server" />
                                        <%#IIf(Eval("BaseCurrencySymbol") <> Eval("varCurrSymbol"), "<span class='gray'>(" + Eval("BaseCurrencySymbol") + String.Format("{0:###00.00}", Eval("BalDue") * Eval("fltExchangeRateOfOrder"))+")</span>", "")%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label Text="" runat="server" ID="lblFooterTotalBalanceDue" />
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn>
                                    <HeaderTemplate>
                                        Payment
                                    </HeaderTemplate>
                                    <ItemStyle HorizontalAlign="Right" />
                                    <FooterStyle HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#Eval("varCurrSymbol")%>
                                        <asp:TextBox runat="server" Width="60px" ID="txtAmountToPay" class="AmountToPay"
                                            Style="text-align: right;" onkeypress="CheckNumber(1,event);" AutoComplete="OFF"
                                            Text='<%# String.Format("{0:###00.00}",Eval("monAmountPaidInDeposite"))%>'></asp:TextBox>
                                        <asp:Label ID="lblConversionDifference" Text='<%#IIf(Eval("BaseCurrencySymbol") <> Eval("varCurrSymbol"), Eval("BaseCurrencySymbol") + String.Format("{0:###00.00}", Eval("monAmountPaidInDeposite") * Eval("fltExchangeRateCurrent")) , "")%>'
                                            runat="server" />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label Text="" runat="server" ID="lblFooterTotalPayment" />
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </td>
                </tr>
            </table>
            <%--<table align="left">
                <tr runat="server" id="trDeposite">
                    <td align="left">
                        <asp:RadioButton Text="Deposit to:" runat="server" ID="radDepositeTo" GroupName="DepositeGroup"
                            Checked="true" />
                    </td>
                    <td colspan="3">
                        <asp:DropDownList ID="ddlDepositTo" runat="server" CssClass="required_numeric {required:'#radDepositeTo:checked', messages:{required:'Please select at least one Deposit To Account!'}}"
                            AutoPostBack="true">
                        </asp:DropDownList>
                        &nbsp;&nbsp;Balance:&nbsp;
                <asp:Label ID="lblOpeningBalance" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr runat="server" id="trDeposite1">
                    <td align="left" colspan="4">
                        <asp:RadioButton Text="Group with other undeposited funds" runat="server" ID="radUnderpositedFund"
                            GroupName="DepositeGroup" />
                    </td>
                </tr>
            </table>--%>
            <table align="right" id="tbl2" runat="server" border="0">
                <tr>
                    <td align="right">
                        <asp:Label ID="lblBalance" Text="Balance Due" runat="server" Visible="true"></asp:Label>:
                    </td>
                    <td align="left">
                        <asp:Label ID="lblBalanceAmt" runat="server" Visible="true"></asp:Label>
                        <asp:Label ID="lblTransChargeLabel" Text=" + (Transaction charge)" CssClass="signup"
                            runat="server" Visible="false" />
                        <asp:Label ID="lblTransCharge" Text="" CssClass="signup" runat="server" Visible="false" />&nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="right">Total Amount To Pay:
                    </td>
                    <td align="left">
                        <asp:Label ID="lblAmountToPay" runat="server" Text="0.00"></asp:Label>
                    </td>
                </tr>
                <tr>

                    <td>
                        <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Receive Payment"></asp:Button>
                    </td>
                    <td>
                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close" OnClientClick="window.close();"></asp:Button></td>
                </tr>
            </table>
            <asp:HiddenField ID="hdnOppType" runat="server" />
            <asp:HiddenField ID="hdnAmountPaid" runat="server" />
            <asp:HiddenField ID="hdnBizDocAmount" runat="server" />
            <asp:HiddenField ID="hdnSwipeTrack1" runat="server" />
            <asp:HiddenField ID="hdnSwipeTrack2" runat="server" />
            <asp:HiddenField ID="hdnAmountToCredit" runat="server" />
            <asp:HiddenField ID="hdnIsDepositedToAccount" runat="server" />
            <asp:HiddenField ID="hdnDepositePage" runat="server" />
            <asp:HiddenField ID="hdnDepositeID" runat="server" />
            <asp:HiddenField ID="hdnDefaultCreditCard" runat="server" />
            <asp:Button ID="btnGo" runat="server" Style="visibility: hidden" />
        </div>
    </form>
</body>
</html>
