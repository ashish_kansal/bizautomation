<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmCompetition.aspx.vb"
    Inherits="BACRMPortal.frmCompetition" %>

<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    	<link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <title>Competition</title>

    <script language="javascript">
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function Add() {

            if (document.Form1.ddlCompany.value == 0) {
                alert("Select Company")
                document.Form1.ddlCompany.focus()
                return false;
            }
            if (document.Form1.ddlCompany.selectedIndex == -1) {
                alert("Select Company")
                document.Form1.ddlCompany.focus()
                return false;
            }
            if (document.Form1.ddlDivision.selectedIndex == -1) {
                alert("Select Division")
                document.Form1.ddlDivision.focus()
                return false;
            }
            if (document.Form1.ddlDivision.value == 0) {
                alert("Select Division")
                document.Form1.ddlDivision.focus()
                return false;
            }
        }
        function Save() {

            var str = '';
            for (i = 1; i <= document.getElementById('dgCompetitor').rows.length; i++) {
                if (document.getElementById('dgCompetitor__ctl' + (i + 1) + '_txtPrice') != null) {
                    if (document.all) {
                        str = str + document.getElementById('dgCompetitor_ctl' + strI + '_lblCompetitorID').innerText + ',' + document.getElementById('dgCompetitor_ctl' + strI + '_txtPrice').value + ',' + document.getElementById('dgCompetitor_ctl' + strI + '_cal_txtDate').value + '~'
                    } else {
                        str = str + document.getElementById('dgCompetitor_ctl' + strI + '_lblCompetitorID').textContent + ',' + document.getElementById('dgCompetitor_ctl' + strI + '_txtPrice').value + ',' + document.getElementById('dgCompetitor_ctl' + strI + '_cal_txtDate').value + '~'
                    }
                }
            }

            document.Form1.txtHidden.value = str;
        }
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <asp:ScriptManager ID="sc" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <table width="100%">
        <tr>
            <td class="normal1" align="right">
                Competitor
            </td>
            <td>
                <asp:TextBox ID="txtSearch" CssClass="signup" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="btnGo" CssClass="button" runat="server" Text="Go"></asp:Button>
            </td>
            <td>
                <asp:DropDownList ID="ddlCompany" CssClass="signup" runat="server" Width="130" AutoPostBack="True">
                </asp:DropDownList>
            </td>
            <td>
                <asp:Button ID="btnAdd" CssClass="button" runat="server" Text="Add" Width="50"></asp:Button>
            </td>
            <td>
                <asp:Button ID="btnSave" CssClass="button" runat="server" Text="Save" Width="50">
                </asp:Button>
            </td>
            <td>
                <asp:Button ID="btnProOK" runat="server" CssClass="button" Text="Close" Width="50"
                    OnClientClick="javascript:window.close()"></asp:Button>
            </td>
        </tr>
    </table>
    <asp:DataGrid ID="dgCompetitor" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
        BorderColor="white">
        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
        <ItemStyle CssClass="is"></ItemStyle>
        <HeaderStyle CssClass="hs"></HeaderStyle>
        <Columns>
            <asp:BoundColumn Visible="False" DataField="numCompetitorID"></asp:BoundColumn>
            <asp:BoundColumn DataField="Competitor" HeaderText="Competitor Name, Division"></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Price / Notes">
                <ItemTemplate>
                    <asp:TextBox ID="txtPrice" runat="server" Width="250" CssClass="signup" Text='<%# DataBinder.Eval(Container.DataItem, "VcPrice") %>'>
                    </asp:TextBox>
                    <asp:Label ID="lblCompetitorID" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "numCompetitorID") %>'
                        Style="display: none">
                    </asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Date price was entered">
                <ItemTemplate>
                    <BizCalendar:Calendar runat="server" SelectedDate='<%# DataBinder.Eval(Container.DataItem, "dtDateEntered") %>'
                        ID="cal" />
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="Phone" HeaderText="Phone Number, Ext"></asp:BoundColumn>
            <asp:TemplateColumn>
                <HeaderTemplate>
                    <asp:Button ID="btnHdelete" runat="server" CssClass="Delete" Text="X"></asp:Button>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Button ID="btnDelete" runat="server" CssClass="Delete" Text="X" CommandName="Delete">
                    </asp:Button>
                </ItemTemplate>
            </asp:TemplateColumn>
        </Columns>
    </asp:DataGrid>
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
    <asp:TextBox ID="txtHidden" runat="server" Style="display: none"></asp:TextBox>
    </form>
</body>
</html>
