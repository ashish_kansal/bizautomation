'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Accounting
Imports System.Data


Partial Class frmBizInvoice
    Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim dtOppBiDocItems As New DataTable
    Dim lintJournalId As Integer
    Dim lngOppBizDocID, lngOppId As Long
    Dim lngDomainID, lngCntID As Long
    Dim strDateFormat, ResponseMessage As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            btnClose.Attributes.Add("onclick", "return Close()")
            btnPrint.Attributes.Add("onclick", "return PrintIt()")
            lngOppBizDocID = CCommon.ToLong(GetQueryStringVal("OppBizId"))
            lngOppId = CCommon.ToLong(GetQueryStringVal("OpID"))
            lngDomainID = IIf(GetQueryStringVal("DomainID") = "", Session("DomainID"), CCommon.ToLong(GetQueryStringVal("DomainID")))
            lngCntID = IIf(GetQueryStringVal("ConID") = "", Session("UserContactID"), CCommon.ToLong(GetQueryStringVal("ConID")))
            strDateFormat = IIf(GetQueryStringVal("DateFormat") Is Nothing, Session("DateFormat"), GetQueryStringVal("DateFormat"))
            If GetQueryStringVal("DomainID") <> "" Then
                tblOriginalBizDoc.Width = Unit.Pixel(590)
            End If

            If Not IsPostBack Then
                Try
                    If GetQueryStringVal("Show") = "True" Then
                        pnlShow.Visible = True
                        'tblCreditForm.Visible = True
                    End If
                Catch ex As Exception

                End Try
                btnCharge.Attributes.Add("onclick", "return Submit()")
                getAddDetails()
                getBizDocDetails()
                BindCardType()
            End If
            hplPaymentDetails.Attributes.Add("onclick", "return OpenPaymentDetails(" & lngOppBizDocID & "," & lngOppId & ")")
            'btnApprovers.Attributes.Add("onclick", "return openApp(" & lngOppBizDocID & ",'B'," & lngOppId & ")")
            'hplBizDocAtch.Attributes.Add("onclick", "OpenAtch(" & GetQueryStringVal("OpID") & "," & GetQueryStringVal("OppBizId") & "," & lngDomainID & "," & lngCntID & ")")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub getBizDocDetails()
        Try
            Dim dtOppBiDocDtl As DataTable
            Dim objOppBizDocs As New OppBizDocs
            objOppBizDocs.OppBizDocId = lngOppBizDocID
            objOppBizDocs.OppId = lngOppId
            objOppBizDocs.DomainID = lngDomainID
            objOppBizDocs.UserCntID = lngCntID
            objOppBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            dtOppBiDocDtl = objOppBizDocs.GetOppBizDocDtl
            If dtOppBiDocDtl.Rows.Count <> 0 Then

                If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("monAmountPaid")) Then
                    lblAmountPaid.Text = CCommon.GetDecimalFormat(dtOppBiDocDtl.Rows(0).Item("monAmountPaid"))
                Else : lblAmountPaid.Text = "0.00"
                End If
                If dtOppBiDocDtl.Rows(0).Item("AppReq") = 1 Then
                    pnlApprove.Visible = True
                Else : pnlApprove.Visible = False
                End If
                'lblPending.Text = dtOppBiDocDtl.Rows(0).Item("Pending")
                'lblApproved.Text = dtOppBiDocDtl.Rows(0).Item("Approved")
                'lblDeclined.Text = dtOppBiDocDtl.Rows(0).Item("Declined")
                'hplBillto.Attributes.Add("onclick", "return openeditAddress('Bill','" & lngOppId & "'," & lngReturnID & ")")
                'hplShipTo.Attributes.Add("onclick", "return openeditAddress('Ship','" & lngOppId & "'," & lngReturnID & ")")

                If dtOppBiDocDtl.Rows(0).Item("tintDeferred") = 1 Then
                    hplAmountPaid.Text = "Amount Paid: (Deferred)"
                End If

                If dtOppBiDocDtl.Rows(0).Item("tintOppType") = 1 Then
                    If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("BizdocFooter")) Then
                        imgFooter.Visible = True
                        imgFooter.ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) & dtOppBiDocDtl.Rows(0).Item("BizdocFooter")
                    Else : imgFooter.Visible = False
                    End If
                    lblPONo.Text = "P.O"

                Else
                    If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("PurBizdocFooter")) Then
                        imgFooter.Visible = True
                        imgFooter.ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) & dtOppBiDocDtl.Rows(0).Item("PurBizdocFooter")
                    Else : imgFooter.Visible = False
                    End If
                    lblPONo.Text = "Invoice"

                    hplAmountPaid.Attributes.Add("onclick", "#")
                End If

                If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("vcBizDocImagePath")) Then
                    imgLogo.Visible = True
                    imgLogo.ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) & dtOppBiDocDtl.Rows(0).Item("vcBizDocImagePath")
                    'imgLogo.ImageUrl = "../Documents/Docs/" & Session("DomainID") & "/" & dtOppBiDocDtl.Rows(0).Item("vcBizDocImagePath")
                Else : imgLogo.Visible = False
                End If

                lblBizDocIDValue.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("vcBizDocID")), "", dtOppBiDocDtl.Rows(0).Item("vcBizDocID"))
                'lblcreated.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("numCreatedby")), "", dtOppBiDocDtl.Rows(0).Item("numCreatedby")) & "  &nbsp;" & IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("dtCreatedDate")), "", dtOppBiDocDtl.Rows(0).Item("dtCreatedDate")) & "  &nbsp;" & IIf(CCommon.ToBool(dtOppBiDocDtl.Rows(0).Item("bitAutoCreated")), "WorkFlow", "")
                'lblModifiedby.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("numModifiedBy")), "", dtOppBiDocDtl.Rows(0).Item("numModifiedBy")) & "  &nbsp;" & IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("dtModifiedDate")), "", dtOppBiDocDtl.Rows(0).Item("dtModifiedDate"))
                lblOrganizationName.Text = CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrganizationName"))
                lblOrganizationContactName.Text = CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrgContactName"))
                Dim strDate As Date

                If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("dtFromDate")) Then
                    strDate = DateAdd(DateInterval.Day, CCommon.ToInteger(dtOppBiDocDtl.Rows(0).Item("numBillingDaysName")), dtOppBiDocDtl.Rows(0).Item("dtFromDate"))
                    lblDuedate.Text = FormattedDateFromDate(strDate, Session("DateFormat"))
                End If

                If dtOppBiDocDtl.Rows(0).Item("tintBillingTerms") = True Then
                    lblBillingTerms.Text = "Net " & dtOppBiDocDtl.Rows(0).Item("numBillingDaysName") & " , " & IIf(dtOppBiDocDtl.Rows(0).Item("tintInterestType") = False, "-", "+") & dtOppBiDocDtl.Rows(0).Item("fltInterest") & " %"
                    lblBillingTermsName.Text = dtOppBiDocDtl.Rows(0).Item("vcBillingTermsName") & " , " & IIf(dtOppBiDocDtl.Rows(0).Item("tintInterestType") = False, "-", "+") & dtOppBiDocDtl.Rows(0).Item("fltInterest") & " %"

                Else
                    lblBillingTerms.Text = "-"
                    lblBillingTermsName.Text = "-"

                End If

                If Not strDate = Nothing Then
                    hplDueDate.Attributes.Add("onclick", "return ChangeDate('" & lngOppBizDocID & "','" & strDate.ToString("yyyyMMdd") & "','" & lngOppId & "')")
                End If
                If dtOppBiDocDtl.Rows(0).Item("fltDiscount") > 0 Then
                    If dtOppBiDocDtl.Rows(0).Item("bitDiscountType") = False Then

                        lblDiscount.Text = dtOppBiDocDtl.Rows(0).Item("fltDiscount") & " %"
                    Else
                        lblDiscount.Text = dtOppBiDocDtl.Rows(0).Item("fltDiscount")
                    End If
                End If

                lblStatus.Text = dtOppBiDocDtl.Rows(0).Item("BizDocStatus")
                lblComments.Text = "<pre class=""WordWrap"">" & dtOppBiDocDtl.Rows(0).Item("vcComments") & "</pre>"
                lblNo.Text = dtOppBiDocDtl.Rows(0).Item("vcRefOrderNo")
                hdShipAmt.Value = CCommon.GetDecimalFormat(dtOppBiDocDtl.Rows(0).Item("monShipCost"))

                'Show capture amount button
                Dim ds As New DataSet
                Dim dtOppBiDocItems As DataTable
                objOppBizDocs.DomainID = Session("DomainID")
                objOppBizDocs.OppBizDocId = lngOppBizDocID
                objOppBizDocs.OppId = lngOppId
                ds = objOppBizDocs.GetBizDocItemsWithKitChilds
                Dim dtOppBizDocsItemsWithKitCHilds As DataTable = ds.Tables(0)

                If Not dtOppBizDocsItemsWithKitCHilds Is Nothing AndAlso dtOppBizDocsItemsWithKitCHilds.Columns.Contains("txtItemDesc") AndAlso dtOppBizDocsItemsWithKitCHilds.Rows.Count > 0 Then
                    For Each dr As DataRow In dtOppBizDocsItemsWithKitCHilds.Rows
                        dr("txtItemDesc") = CCommon.ToString(dr("txtItemDesc")).Replace(vbLf, "<br>")
                    Next
                End If

                If Not dtOppBizDocsItemsWithKitCHilds Is Nothing AndAlso dtOppBizDocsItemsWithKitCHilds.Rows.Count > 0 Then
                    dtOppBiDocItems = dtOppBizDocsItemsWithKitCHilds.Select("bitChildItem=0").CopyToDataTable()
                Else
                    dtOppBiDocItems = dtOppBizDocsItemsWithKitCHilds
                End If

                Dim objCalculateDealAmount As New CalculateDealAmount
                objCalculateDealAmount.CalculateDealAmount(lngOppId, lngOppBizDocID, CShort(dtOppBiDocDtl.Rows(0).Item("tintOppType")), Session("DomainID"), dtOppBiDocItems, FromBizInvoice:=True)

                hdLateCharge.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.TotalLateCharges)

                'Commented by chintan, reason: line item discount is display purpose only. take aggregate discount from BizDoc edit ->Discount Field
                hdDisc.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.TotalDiscount) ' + IIf(dtOppBiDocItems.Rows.Count > 0, dtOppBiDocItems.Compute("SUM(DiscAmt)", ""), 0)

                hdSubTotal.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.TotalAmount)

                hdGrandTotal.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.GrandTotal - objCalculateDealAmount.CreditAmount)

                hdTaxAmt.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.TotalTaxAmount - objCalculateDealAmount.TotalCRVTaxAmount)

                hdCRVTxtAmt.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.TotalCRVTaxAmount)

                hdnCreditAmount.Value = CCommon.GetDecimalFormat(IIf(objCalculateDealAmount.CreditAmount > 0, objCalculateDealAmount.CreditAmount * -1, 0))

                Session("Itemslist") = dtOppBizDocsItemsWithKitCHilds

                'Add columns to datagrid
                Dim i As Integer
                Dim dtdgColumns As DataTable
                dtdgColumns = ds.Tables(2)
                Dim bColumn As BoundColumn
                For i = 0 To dtdgColumns.Rows.Count - 1
                    bColumn = New BoundColumn
                    bColumn.HeaderText = dtdgColumns.Rows(i).Item("vcFormFieldName")
                    bColumn.DataField = dtdgColumns.Rows(i).Item("vcDbColumnName")
                    If dtdgColumns.Rows(i).Item("vcDbColumnName") = "SerialLotNo" Then
                        bColumn.ItemStyle.CssClass = "WordWrapSerialNo"
                    ElseIf dtdgColumns.Rows(i).Item("vcDbColumnName") = "vcItemName" Then
                        bColumn.ItemStyle.CssClass = "ItemName"
                    End If
                    If dtdgColumns.Rows(i).Item("vcFieldDataType") = "M" Then bColumn.DataFormatString = CCommon.GetDataFormatString()

                    dgBizDocs.Columns.Add(bColumn)
                Next
                BindItems()
                Dim objConfigWizard As New FormConfigWizard
                If dtOppBiDocDtl.Rows(0).Item("tintOppType") = 1 Then
                    objConfigWizard.FormID = 7
                Else
                    objConfigWizard.FormID = 8
                End If
                objConfigWizard.DomainID = Session("DomainID")
                objConfigWizard.BizDocID = dtOppBiDocDtl.Rows(0).Item("numBizDocId")
                objConfigWizard.BizDocTemplateID = dtOppBiDocDtl.Rows(0).Item("numBizDocTempID")

                Dim dsNew As DataSet
                Dim dtTable As DataTable
                dsNew = objConfigWizard.GetFieldFormListForBizDocsSumm
                dtTable = dsNew.Tables(1)
                Dim tblrow As HtmlTableRow
                Dim tblCell As HtmlTableCell
                Dim strLabelCellID As String
                For Each dr As DataRow In dtTable.Rows
                    tblrow = New HtmlTableRow
                    tblCell = New HtmlTableCell
                    tblCell.Attributes.Add("class", "normal1")
                    tblCell.InnerText = dr("vcFormFieldName") & ": "
                    tblrow.Cells.Add(tblCell)

                    tblCell = New HtmlTableCell
                    tblCell.Attributes.Add("class", "normal1")
                    If dr("vcDbColumnName") = "SubTotal" Then
                        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + hdSubTotal.Value
                    ElseIf dr("vcDbColumnName") = "ShippingAmount" Then
                        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + hdShipAmt.Value
                    ElseIf dr("vcDbColumnName") = "TotalSalesTax" Then
                        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + hdTaxAmt.Value
                    ElseIf dr("vcDbColumnName") = "TotalCRVTax" Then
                        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + hdCRVTxtAmt.Value
                    ElseIf dr("vcDbColumnName") = "LateCharge" Then
                        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + hdLateCharge.Value
                    ElseIf dr("vcDbColumnName") = "Discount" Then
                        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + hdDisc.Value
                    ElseIf dr("vcDbColumnName") = "CreditApplied" Then
                        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + hdnCreditAmount.Value
                    ElseIf dr("vcDbColumnName") = "GrandTotal" Then
                        tblCell.ID = "tdGradTotal"
                        strLabelCellID = tblCell.ID
                        'tblCell.InnerText = hdGrandTotal.Value
                    Else
                        If dtOppBiDocDtl.Rows(0).Item("tintOppType") = 1 Then
                            Dim taxAmt As Decimal
                            taxAmt = IIf(IsDBNull(dtOppBiDocItems.Compute("SUM([" & dr("vcFormFieldName") & "])", "[" & dr("vcFormFieldName") & "]>0")), 0, dtOppBiDocItems.Compute("SUM([" & dr("vcFormFieldName") & "])", "[" & dr("vcFormFieldName") & "]>0"))
                            tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + CCommon.GetDecimalFormat(taxAmt)
                        End If
                    End If
                    tblrow.Cells.Add(tblCell)
                    tblBizDocSumm.Rows.Add(tblrow)
                Next
                If strLabelCellID <> "" Then
                    CType(tblBizDocSumm.FindControl(strLabelCellID), HtmlTableCell).InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + hdGrandTotal.Value
                End If

                lblBalance.Text = CCommon.GetDecimalFormat(hdGrandTotal.Value - lblAmountPaid.Text)
                lblAmountPaidCurrency.Text = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString()
                lblBalanceDueCurrency.Text = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString()

                If dtOppBiDocDtl.Rows(0)("bitEnabled") = True And dtOppBiDocDtl.Rows(0)("txtBizDocTemplate").ToString().Length > 0 Then
                    'BizDoc UI modification 
                    Dim strCss As String = "<style>" & Server.HtmlDecode(dtOppBiDocDtl.Rows(0)("txtCSS").ToString()) & "</style>"
                    Dim strBizDocUI As String = strCss & HttpUtility.HtmlDecode(dtOppBiDocDtl.Rows(0)("txtBizDocTemplate").ToString())
                    If strBizDocUI.Length > 0 Then
                        strBizDocUI = strBizDocUI.Replace("#Logo#", CCommon.RenderControl(imgLogo))
                        strBizDocUI = strBizDocUI.Replace("#FooterImage#", CCommon.RenderControl(imgFooter))
                        strBizDocUI = strBizDocUI.Replace("#BizDocType#", CCommon.RenderControl(lblBizDoc))

                        Dim dsAddress As DataSet
                        objOppBizDocs.OppId = lngOppId
                        objOppBizDocs.DomainID = Session("DomainID")
                        objOppBizDocs.OppBizDocId = lngOppBizDocID
                        dsAddress = objOppBizDocs.GetOPPGetOppAddressDetails

                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        'Customer/Vendor Information
                        strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationName#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrganizationName")))
                        strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationPhone#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrganizationPhone")))

                        If dsAddress.Tables(0).Rows.Count = 0 Then
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCompanyName#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToAddress#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToStreet#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCity#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToPostal#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToState#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCountry#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToAddressName#", "")
                            strBizDocUI = strBizDocUI.Replace("#BillingContact#", "")
                        Else
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCompanyName#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcCompanyName")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToAddress#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcFullAddress")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToStreet#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcStreet")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCity#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcCity")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToPostal#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcPostalCode")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToState#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcState")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCountry#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcCountry")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToAddressName#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcAddressName")))
                            strBizDocUI = strBizDocUI.Replace("#BillingContact#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcContact")))
                        End If

                        If Not dsAddress Is Nothing AndAlso dsAddress.Tables.Count > 4 AndAlso dsAddress.Tables(4).Rows.Count > 0 Then
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCompanyName#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcCompanyName")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToAddress#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcFullAddress")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToStreet#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcStreet")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCity#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcCity")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToPostal#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcPostalCode")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToState#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcState")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCountry#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcCountry")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToAddressName#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcAddressName")))
                        Else
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCompanyName#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToAddress#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToStreet#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCity#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToPostal#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToState#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCountry#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToAddressName#", "")
                        End If

                        If dsAddress.Tables(1).Rows.Count = 0 Then
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCompanyName#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToAddress#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToStreet#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCity#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToPostal#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToState#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCountry#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToAddressName#", "")
                            strBizDocUI = strBizDocUI.Replace("#ShippingContact#", "")
                        Else
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCompanyName#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcCompanyName")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToAddress#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcFullAddress")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToStreet#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcStreet")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCity#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcCity")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToPostal#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcPostalCode")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToState#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcState")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCountry#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcCountry")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToAddressName#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcAddressName")))
                            strBizDocUI = strBizDocUI.Replace("#ShippingContact#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcContact")))
                        End If

                        If Not dsAddress Is Nothing AndAlso dsAddress.Tables.Count > 5 AndAlso dsAddress.Tables(5).Rows.Count > 0 Then
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCompanyName#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcCompanyName")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToAddress#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcFullAddress")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToStreet#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcStreet")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCity#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcCity")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToPostal#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcPostalCode")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToState#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcState")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCountry#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcCountry")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToAddressName#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcAddressName")))
                        Else
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCompanyName#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToAddress#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToStreet#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCity#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToPostal#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToState#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCountry#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToAddressName#", "")
                        End If

                        If dtOppBiDocDtl.Rows(0).Item("tintOppType") = 1 Then
                            If strBizDocUI.IndexOf("#OppOrderChangeBillToHeader(") > 0 Then
                                Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#OppOrderChangeBillToHeader(") + "#OppOrderChangeBillToHeader(".Length, strBizDocUI.LastIndexOf(")#") - strBizDocUI.IndexOf("#OppOrderChangeBillToHeader(") - "#OppOrderChangeBillToHeader(".Length)
                                hplBillto.InnerHtml = strLink '"<font color=""white"">" + strLink + "</font>"
                                strBizDocUI = strBizDocUI.Replace("#OppOrderChangeBillToHeader(" & strLink & ")#", CCommon.RenderControl(hplBillto))
                            Else
                                strBizDocUI = strBizDocUI.Replace("#OppOrderChangeBillToHeader#", CCommon.RenderControl(hplBillto))
                            End If

                            If strBizDocUI.IndexOf("#OppOrderChangeShipToHeader(") > 0 Then
                                Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#OppOrderChangeShipToHeader(") + "#OppOrderChangeShipToHeader(".Length, strBizDocUI.IndexOf(")#") - strBizDocUI.IndexOf("#OppOrderChangeShipToHeader(") - "#OppOrderChangeShipToHeader(".Length)
                                hplShipTo.InnerHtml = strLink '"<font color=""white"">" + strLink + "</font>"
                                strBizDocUI = strBizDocUI.Replace("#OppOrderChangeShipToHeader(" & strLink & ")#", CCommon.RenderControl(hplShipTo))
                            Else
                                strBizDocUI = strBizDocUI.Replace("#OppOrderChangeShipToHeader#", CCommon.RenderControl(hplShipTo))
                            End If
                        Else
                            If strBizDocUI.IndexOf("#OppOrderChangeBillToHeader(") > 0 Then
                                Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#OppOrderChangeBillToHeader(") + "#OppOrderChangeBillToHeader(".Length, strBizDocUI.LastIndexOf(")#") - strBizDocUI.IndexOf("#OppOrderChangeBillToHeader(") - "#OppOrderChangeBillToHeader(".Length)
                                strBizDocUI = strBizDocUI.Replace("#OppOrderChangeBillToHeader(" & strLink & ")#", strLink)
                            Else
                                strBizDocUI = strBizDocUI.Replace("#OppOrderChangeBillToHeader#", "Bill To")
                            End If

                            If strBizDocUI.IndexOf("#OppOrderChangeShipToHeader(") > 0 Then
                                Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#OppOrderChangeShipToHeader(") + "#OppOrderChangeShipToHeader(".Length, strBizDocUI.IndexOf(")#") - strBizDocUI.IndexOf("#OppOrderChangeShipToHeader(") - "#OppOrderChangeShipToHeader(".Length)
                                strBizDocUI = strBizDocUI.Replace("#OppOrderChangeShipToHeader(" & strLink & ")#", strLink)
                            Else
                                strBizDocUI = strBizDocUI.Replace("#OppOrderChangeShipToHeader#", "Ship To")
                            End If
                        End If

                        strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationComments#", dtOppBiDocDtl.Rows(0)("vcOrganizationComments").ToString())

                        strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationContactName#", CCommon.RenderControl(lblOrganizationContactName))
                        strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationContactEmail#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrgContactEmail")))
                        strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationContactPhone#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrgContactPhone")))
                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                        'Employer Information
                        strBizDocUI = strBizDocUI.Replace("#EmployerOrganizationName#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("EmployerOrganizationName")))
                        strBizDocUI = strBizDocUI.Replace("#EmployerOrganizationPhone#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("EmployerOrganizationPhone")))

                        If dsAddress.Tables(2).Rows.Count = 0 Then
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToCompanyName#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToAddress#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToStreet#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToCity#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToPostal#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToState#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToCountry#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToAddressName#", "")
                        Else
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToCompanyName#", dsAddress.Tables(2).Rows(0)("vcCompanyName"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToAddress#", dsAddress.Tables(2).Rows(0)("vcFullAddress"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToStreet#", dsAddress.Tables(2).Rows(0)("vcStreet"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToCity#", dsAddress.Tables(2).Rows(0)("vcCity"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToPostal#", dsAddress.Tables(2).Rows(0)("vcPostalCode"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToState#", dsAddress.Tables(2).Rows(0)("vcState"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToCountry#", dsAddress.Tables(2).Rows(0)("vcCountry"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToAddressName#", dsAddress.Tables(2).Rows(0)("vcAddressName"))
                        End If

                        If dsAddress.Tables(3).Rows.Count = 0 Then
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToCompanyName#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToAddress#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToStreet#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToCity#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToPostal#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToState#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToCountry#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToAddressName#", "")
                        Else
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToCompanyName#", dsAddress.Tables(3).Rows(0)("vcCompanyName"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToAddress#", dsAddress.Tables(3).Rows(0)("vcFullAddress"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToStreet#", dsAddress.Tables(3).Rows(0)("vcStreet"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToCity#", dsAddress.Tables(3).Rows(0)("vcCity"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToPostal#", dsAddress.Tables(3).Rows(0)("vcPostalCode"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToState#", dsAddress.Tables(3).Rows(0)("vcState"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToCountry#", dsAddress.Tables(3).Rows(0)("vcCountry"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToAddressName#", dsAddress.Tables(3).Rows(0)("vcAddressName"))
                        End If

                        If dtOppBiDocDtl.Rows(0).Item("tintOppType") = 2 Then
                            If strBizDocUI.IndexOf("#EmployerChangeBillToHeader(") > 0 Then
                                Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#EmployerChangeBillToHeader(") + "#EmployerChangeBillToHeader(".Length, strBizDocUI.LastIndexOf(")#") - strBizDocUI.IndexOf("#EmployerChangeBillToHeader(") - "#EmployerChangeBillToHeader(".Length)
                                hplBillto.InnerHtml = strLink '"<font color=""white"">" + strLink + "</font>"
                                strBizDocUI = strBizDocUI.Replace("#EmployerChangeBillToHeader(" & strLink & ")#", CCommon.RenderControl(hplBillto))
                            Else
                                strBizDocUI = strBizDocUI.Replace("#EmployerChangeBillToHeader#", CCommon.RenderControl(hplBillto))
                            End If

                            If strBizDocUI.IndexOf("#EmployerChangeShipToHeader(") > 0 Then
                                Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#EmployerChangeShipToHeader(") + "#EmployerChangeShipToHeader(".Length, strBizDocUI.IndexOf(")#") - strBizDocUI.IndexOf("#EmployerChangeShipToHeader(") - "#EmployerChangeShipToHeader(".Length)
                                hplShipTo.InnerHtml = strLink '"<font color=""white"">" + strLink + "</font>"
                                strBizDocUI = strBizDocUI.Replace("#EmployerChangeShipToHeader(" & strLink & ")#", CCommon.RenderControl(hplShipTo))
                            Else
                                strBizDocUI = strBizDocUI.Replace("#EmployerChangeShipToHeader#", CCommon.RenderControl(hplShipTo))
                            End If
                        Else
                            If strBizDocUI.IndexOf("#EmployerChangeBillToHeader(") > 0 Then
                                Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#EmployerChangeBillToHeader(") + "#EmployerChangeBillToHeader(".Length, strBizDocUI.LastIndexOf(")#") - strBizDocUI.IndexOf("#EmployerChangeBillToHeader(") - "#EmployerChangeBillToHeader(".Length)
                                strBizDocUI = strBizDocUI.Replace("#EmployerChangeBillToHeader(" & strLink & ")#", strBizDocUI)
                            Else
                                strBizDocUI = strBizDocUI.Replace("#EmployerChangeBillToHeader#", "Bill To")
                            End If

                            If strBizDocUI.IndexOf("#EmployerChangeShipToHeader(") > 0 Then
                                Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#EmployerChangeShipToHeader(") + "#EmployerChangeShipToHeader(".Length, strBizDocUI.IndexOf(")#") - strBizDocUI.IndexOf("#EmployerChangeShipToHeader(") - "#EmployerChangeShipToHeader(".Length)
                                strBizDocUI = strBizDocUI.Replace("#EmployerChangeShipToHeader(" & strLink & ")#", strLink)
                            Else
                                strBizDocUI = strBizDocUI.Replace("#EmployerChangeShipToHeader#", "Ship To")
                            End If
                        End If

                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                        strBizDocUI = strBizDocUI.Replace("#BizDocStatus#", lblStatus.Text)
                        strBizDocUI = strBizDocUI.Replace("#Currency#", lblBalanceDueCurrency.Text)
                        strBizDocUI = strBizDocUI.Replace("#AmountPaid#", lblAmountPaid.Text)
                        strBizDocUI = strBizDocUI.Replace("#BalanceDue#", CCommon.RenderControl(lblBalance)) 'used by amt paid js
                        strBizDocUI = strBizDocUI.Replace("#Discount#", lblDiscount.Text)
                        strBizDocUI = strBizDocUI.Replace("#BillingTerms#", lblBillingTerms.Text)
                        If CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("dtFromDate")).Length > 3 Then
                            strBizDocUI = strBizDocUI.Replace("#BillingTermFromDate#", FormattedDateFromDate(dtOppBiDocDtl.Rows(0).Item("dtFromDate"), Session("DateFormat")))
                        End If
                        strBizDocUI = strBizDocUI.Replace("#BillingTermsName#", lblBillingTermsName.Text)

                        strBizDocUI = strBizDocUI.Replace("#DueDate#", lblDuedate.Text)
                        strBizDocUI = strBizDocUI.Replace("#BizDocID#", CCommon.RenderControl(lblBizDocIDValue))
                        strBizDocUI = strBizDocUI.Replace("#P.O.NO#", lblNo.Text)
                        strBizDocUI = strBizDocUI.Replace("#OrderID#", CCommon.RenderControl(hplOppID))
                        strBizDocUI = strBizDocUI.Replace("#Comments#", lblComments.Text)
                        strBizDocUI = strBizDocUI.Replace("#Products#", CCommon.RenderControl(dgBizDocs))
                        strBizDocUI = strBizDocUI.Replace("#BizDocSummary#", CCommon.RenderControl(tblBizDocSumm))
                        'strBizDocUI = strBizDocUI.Replace("#BizDocCreatedDate#", lblDate.Text)
                        strBizDocUI = strBizDocUI.Replace("#CustomerPO##", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcCustomerPO#")))
                        strBizDocUI = strBizDocUI.Replace("#AmountPaidPopUp#", CCommon.RenderControl(hplAmountPaid))
                        strBizDocUI = strBizDocUI.Replace("#ChangeDueDate#", CCommon.RenderControl(hplDueDate))
                        strBizDocUI = strBizDocUI.Replace("#AssigneeName#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("AssigneeName")))
                        strBizDocUI = strBizDocUI.Replace("#AssigneeEmail#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("AssigneeEmail")))
                        strBizDocUI = strBizDocUI.Replace("#AssigneePhoneNo#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("AssigneePhone")))
                        strBizDocUI = strBizDocUI.Replace("#OrderRecOwner#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OnlyOrderRecOwner")))
                        strBizDocUI = strBizDocUI.Replace("#PartnerSource#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcPartner")))
                        strBizDocUI = strBizDocUI.Replace("#ReleaseDate#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcReleaseDate")))
                        strBizDocUI = strBizDocUI.Replace("#RequiredDate#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcRequiredDate")))
                        strBizDocUI = strBizDocUI.Replace("#ShippingCompany#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("ShipVia")))
                        strBizDocUI = strBizDocUI.Replace("#ShippingService#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcShippingService")))
                        strBizDocUI = strBizDocUI.Replace("#PackingSlipID#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcPackingSlip")))
                        strBizDocUI = strBizDocUI.Replace("#BizDocTemplateName#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcTemplateName")))
                        strBizDocUI = strBizDocUI.Replace("#TotalQuantity#", String.Format("{0:0.00}", CCommon.ToDecimal(dtOppBiDocItems.Compute("sum(numUnitHour)", ""))))
                        strBizDocUI = strBizDocUI.Replace("#TrackingNo#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcTrackingNo")))
                        strBizDocUI = strBizDocUI.Replace("#SOComments#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcSOComments")))
                        strBizDocUI = strBizDocUI.Replace("#ParcelShippingAccount#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcShippersAccountNo")))
                        strBizDocUI = strBizDocUI.Replace("#OrderCreatedDate#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrderCreatedDate")))
                        strBizDocUI = strBizDocUI.Replace("#VendorInvoice#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcVendorInvoice")))

                        Dim lNumericToWord As New NumericToWord
                        strBizDocUI = strBizDocUI.Replace("#GrandTotalinWords#", lNumericToWord.SpellNumber(Replace(hdGrandTotal.Value, ",", "")))

                        If dtOppBiDocDtl.Columns.Contains("vcTotalQtybyUOM") Then
                            strBizDocUI = strBizDocUI.Replace("#TotalQytbyUOM#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcTotalQtybyUOM")))
                        Else
                            strBizDocUI = strBizDocUI.Replace("#TotalQytbyUOM#", "")
                        End If

                        'Replace custom field values
                        Dim objCustomFields As New CustomFields
                        If dtOppBiDocDtl.Rows(0).Item("tintOppType") = 1 Then
                            objCustomFields.locId = 2
                        Else
                            objCustomFields.locId = 6
                        End If
                        objCustomFields.RelId = 0
                        objCustomFields.DomainID = Session("DomainID")
                        objCustomFields.RecordId = lngOppId
                        ds = objCustomFields.GetCustFlds
                        For Each drow As DataRow In ds.Tables(0).Rows
                            If CCommon.ToLong(drow("numListID")) > 0 Then
                                strBizDocUI = strBizDocUI.Replace("#" & drow("fld_label").ToString.Trim.Replace(" ", "") & "#", objCommon.GetListItemValue(drow("Value"), Session("DomainID")))
                            Else
                                strBizDocUI = strBizDocUI.Replace("#" & drow("fld_label").ToString.Trim.Replace(" ", "") & "#", drow("Value"))
                            End If
                        Next
                        litBizDocTemplate.Text = strBizDocUI
                        tblOriginalBizDoc.Visible = False
                        tblFormattedBizDoc.Visible = True
                    End If
                Else
                    tblOriginalBizDoc.Visible = True
                    tblFormattedBizDoc.Visible = False
                End If

                '    If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("monAmountPaid")) Then
                '        lblAmountPaid.Text = String.Format("{0:#,##0.00}", dtOppBiDocDtl.Rows(0).Item("monAmountPaid"))
                '    Else : lblAmountPaid.Text = "0.00"
                '    End If
                '    If dtOppBiDocDtl.Rows(0).Item("AppReq") = 1 Then
                '        pnlApprove.Visible = True
                '    Else : pnlApprove.Visible = False
                '    End If
                '    lblPending.Text = dtOppBiDocDtl.Rows(0).Item("Pending")
                '    lblApproved.Text = dtOppBiDocDtl.Rows(0).Item("Approved")
                '    lblDeclined.Text = dtOppBiDocDtl.Rows(0).Item("Declined")
                '    'hplBillto.Attributes.Add("onclick", "return openeditAddress('Bill','" & lngOppId & "')")
                '    'hplShipTo.Attributes.Add("onclick", "return openeditAddress('Ship','" & lngOppId & "')")

                '    If dtOppBiDocDtl.Rows(0).Item("tintDeferred") = 1 Then
                '        hplAmountPaid.Text = "Amount Paid: (Deferred)"
                '    End If

                '    If dtOppBiDocDtl.Rows(0).Item("tintOppType") = 1 Then
                '        If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("BizdocFooter")) Then
                '            imgFooter.Visible = True
                '            imgFooter.ImageUrl = "../Documents/Docs/" & Session("DomainID") & "/" & dtOppBiDocDtl.Rows(0).Item("BizdocFooter")
                '        Else : imgFooter.Visible = False
                '        End If
                '        lblPONo.Text = "P.O"
                '    Else
                '        If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("PurBizdocFooter")) Then
                '            imgFooter.Visible = True
                '            imgFooter.ImageUrl = "../Documents/Docs/" & Session("DomainID") & "/" & dtOppBiDocDtl.Rows(0).Item("PurBizdocFooter")
                '        Else : imgFooter.Visible = False
                '        End If
                '        lblPONo.Text = "Invoice"

                '    End If

                '    If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("vcBizDocImagePath")) Then
                '        imgLogo.Visible = True
                '        imgLogo.ImageUrl = "../Documents/Docs/" & Session("DomainID") & "/" & dtOppBiDocDtl.Rows(0).Item("vcBizDocImagePath")
                '    Else : imgLogo.Visible = False
                '    End If

                '    'If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("vcSignatureFile")) Then
                '    '    imgSignature.Visible = True
                '    '    imgSignature.ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) & dtOppBiDocDtl.Rows(0).Item("vcSignatureFile")
                '    'Else : imgSignature.Visible = False
                '    'End If

                '    lblBizDocIDValue.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("vcBizDocID")), "", dtOppBiDocDtl.Rows(0).Item("vcBizDocID"))
                '    lblcreated.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("numCreatedby")), "", dtOppBiDocDtl.Rows(0).Item("numCreatedby")) & "  &nbsp;" & IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("dtCreatedDate")), "", dtOppBiDocDtl.Rows(0).Item("dtCreatedDate"))
                '    lblModifiedby.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("numModifiedBy")), "", dtOppBiDocDtl.Rows(0).Item("numModifiedBy")) & "  &nbsp;" & IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("dtModifiedDate")), "", dtOppBiDocDtl.Rows(0).Item("dtModifiedDate"))
                '    'lblviewwedby.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("numViewedBy")), "", dtOppBiDocDtl.Rows(0).Item("numViewedBy")) & "  &nbsp;" & IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("dtViewedDate")), "", dtOppBiDocDtl.Rows(0).Item("dtViewedDate"))
                '    'lblDate.Text = FormattedDateFromDate(dtOppBiDocDtl.Rows(0).Item("dtCreatedDate"), strDateFormat)

                '    lblOrganizationName.Text = CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrganizationName"))
                '    lblOrganizationContactName.Text = CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrgContactName"))

                '    If dtOppBiDocDtl.Rows(0).Item("tintBillingTerms") = True Then
                '        If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("dtFromDate")) Then
                '            Dim strDate As Date
                '            strDate = DateAdd(DateInterval.Day, dtOppBiDocDtl.Rows(0).Item("numBillingDaysName"), dtOppBiDocDtl.Rows(0).Item("dtFromDate"))
                '            lblDuedate.Text = FormattedDateFromDate(strDate, strDateFormat)
                '        End If
                '        lblBillingTerms.Text = "Net " & dtOppBiDocDtl.Rows(0).Item("numBillingDaysName") & " , " & IIf(dtOppBiDocDtl.Rows(0).Item("tintInterestType") = False, "-", "+") & dtOppBiDocDtl.Rows(0).Item("fltInterest") & " %"
                '    Else
                '        If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("dtFromDate")) Then
                '            lblDuedate.Text = FormattedDateFromDate(dtOppBiDocDtl.Rows(0).Item("dtFromDate"), strDateFormat)
                '        End If
                '        lblBillingTerms.Text = "-"
                '    End If
                '    If dtOppBiDocDtl.Rows(0).Item("fltDiscount") > 0 Then
                '        If dtOppBiDocDtl.Rows(0).Item("bitDiscountType") = False Then

                '            lblDiscount.Text = dtOppBiDocDtl.Rows(0).Item("fltDiscount") & " %"
                '        Else
                '            lblDiscount.Text = dtOppBiDocDtl.Rows(0).Item("fltDiscount")
                '        End If
                '    End If



                '    lblStatus.Text = dtOppBiDocDtl.Rows(0).Item("BizDocStatus")
                '    lblComments.Text = dtOppBiDocDtl.Rows(0).Item("vcComments")
                '    lblNo.Text = dtOppBiDocDtl.Rows(0).Item("vcRefOrderNo")
                '    hdShipAmt.Value = String.Format("{0:#,##0.00}", dtOppBiDocDtl.Rows(0).Item("monShipCost"))
                'End If
                'Dim ds As New DataSet
                'Dim dtOppBiDocItems As DataTable
                'objOppBizDocs.DomainID = lngDomainID
                'objOppBizDocs.OppBizDocId = lngOppBizDocID
                'objOppBizDocs.OppId = lngOppId
                'ds = objOppBizDocs.GetOppInItems
                'dtOppBiDocItems = ds.Tables(0)


                'Dim objCalculateDealAmount As New CalculateDealAmount
                'objCalculateDealAmount.CalculateDealAmount(lngOppId, lngOppBizDocID, CShort(dtOppBiDocDtl.Rows(0).Item("tintOppType")), lngDomainID, dtOppBiDocItems, FromBizInvoice:=True)

                'hdLateCharge.Value = String.Format("{0:#,##0.00}", objCalculateDealAmount.TotalLateCharges)

                ''Commented by chintan, reason: line item discount is display purpose only. take aggregate discount from BizDoc edit ->Discount Field
                'hdDisc.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.TotalDiscount) ' + IIf(dtOppBiDocItems.Rows.Count > 0, dtOppBiDocItems.Compute("SUM(DiscAmt)", ""), 0)

                'hdSubTotal.Value = String.Format("{0:#,##0.00}", objCalculateDealAmount.TotalAmount)

                'hdGrandTotal.Value = String.Format("{0:#,##0.00}", objCalculateDealAmount.GrandTotal - objCalculateDealAmount.CreditAmount)

                'hdTaxAmt.Value = String.Format("{0:#,##0.00}", objCalculateDealAmount.TotalTaxAmount)

                'hdnCreditAmount.Value = CCommon.GetDecimalFormat(IIf(objCalculateDealAmount.CreditAmount > 0, objCalculateDealAmount.CreditAmount * -1, 0))

                'Session("Itemslist") = dtOppBiDocItems


                ' ''Add columns to datagrid
                'Dim i As Integer
                'Dim dtdgColumns As DataTable
                'dtdgColumns = ds.Tables(2)
                'Dim bColumn As BoundColumn
                'For i = 0 To dtdgColumns.Rows.Count - 1
                '    bColumn = New BoundColumn
                '    bColumn.HeaderText = dtdgColumns.Rows(i).Item("vcFormFieldName")
                '    bColumn.DataField = dtdgColumns.Rows(i).Item("vcDbColumnName")
                '    If dtdgColumns.Rows(i).Item("vcFieldDataType") = "M" Then bColumn.DataFormatString = "{0:#,##0.00}"
                '    dgBizDocs.Columns.Add(bColumn)
                'Next
                'BindItems()
                'Dim objConfigWizard As New FormConfigWizard
                'If dtOppBiDocDtl.Rows(0).Item("tintOppType") = 1 Then

                '    objConfigWizard.FormID = 7
                'Else
                '    objConfigWizard.FormID = 8

                'End If
                'objConfigWizard.DomainID = lngDomainID
                'objConfigWizard.BizDocID = dtOppBiDocDtl.Rows(0).Item("numBizDocId")
                'objConfigWizard.BizDocTemplateID = dtOppBiDocDtl.Rows(0).Item("numBizDocTempID")

                'Dim dsNew As DataSet
                'Dim dtTable As DataTable
                'dsNew = objConfigWizard.GetFieldFormListForBizDocsSumm
                'dtTable = dsNew.Tables(1)
                'If dtTable.Rows.Count = 0 Then dtTable = dsNew.Tables(0)
                'Dim tblrow As HtmlTableRow
                'Dim tblCell As HtmlTableCell

                'Dim strLabelCellID As String


                'For Each dr As DataRow In dtTable.Rows
                '    tblrow = New HtmlTableRow
                '    tblCell = New HtmlTableCell
                '    tblCell.Attributes.Add("class", "normal1")
                '    tblCell.InnerText = dr("vcFormFieldName") & ": "
                '    tblrow.Cells.Add(tblCell)

                '    tblCell = New HtmlTableCell
                '    tblCell.Attributes.Add("class", "normal1")
                '    If dr("vcDbColumnName") = "SubTotal" Then
                '        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + hdSubTotal.Value
                '    ElseIf dr("vcDbColumnName") = "ShippingAmount" Then
                '        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + hdShipAmt.Value
                '    ElseIf dr("vcDbColumnName") = "TotalSalesTax" Then
                '        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + hdTaxAmt.Value
                '    ElseIf dr("vcDbColumnName") = "LateCharge" Then
                '        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + hdLateCharge.Value
                '    ElseIf dr("vcDbColumnName") = "Discount" Then
                '        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + hdDisc.Value
                '    ElseIf dr("vcDbColumnName") = "CreditApplied" Then
                '        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + hdnCreditAmount.Value
                '    ElseIf dr("vcDbColumnName") = "GrandTotal" Then
                '        tblCell.ID = "tdGradTotal"
                '        strLabelCellID = tblCell.ID
                '        'tblCell.InnerText = hdGrandTotal.Value
                '    Else
                '        If dtOppBiDocDtl.Rows(0).Item("tintOppType") = 1 Then
                '            Dim taxAmt As Decimal
                '            taxAmt = IIf(IsDBNull(dtOppBiDocItems.Compute("SUM([" & dr("vcFormFieldName") & "])", "[" & dr("vcFormFieldName") & "]>0")), 0, dtOppBiDocItems.Compute("SUM([" & dr("vcFormFieldName") & "])", "[" & dr("vcFormFieldName") & "]>0"))
                '            tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + CCommon.GetDecimalFormat(taxAmt)
                '        End If
                '    End If
                '    tblrow.Cells.Add(tblCell)
                '    tblBizDocSumm.Rows.Add(tblrow)
                'Next
                'If strLabelCellID <> "" Then
                '    CType(tblBizDocSumm.FindControl(strLabelCellID), HtmlTableCell).InnerText = hdGrandTotal.Value
                'End If

                'lblBalance.Text = String.Format("{0:#,##0.00}", hdGrandTotal.Value - lblAmountPaid.Text)
                'lblAmountPaidCurrency.Text = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString()
                'lblBalanceDueCurrency.Text = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString()

                'If dtOppBiDocDtl.Rows(0)("bitEnabled") = True And dtOppBiDocDtl.Rows(0)("txtBizDocTemplate").ToString().Length > 0 Then
                '    'BizDoc UI modification 
                '    Dim strCss As String = "<style>" & Server.HtmlDecode(dtOppBiDocDtl.Rows(0)("txtCSS").ToString()) & "</style>"
                '    Dim strBizDocUI As String = strCss & HttpUtility.HtmlDecode(dtOppBiDocDtl.Rows(0)("txtBizDocTemplate").ToString())
                '    If strBizDocUI.Length > 0 Then
                '        strBizDocUI = strBizDocUI.Replace("#Logo#", CCommon.RenderControl(imgLogo))
                '        'strBizDocUI = strBizDocUI.Replace("#Signature#", CCommon.RenderControl(imgSignature))
                '        strBizDocUI = strBizDocUI.Replace("#OrganizationComments#", dtOppBiDocDtl.Rows(0)("vcOrganizationComments").ToString())

                '        strBizDocUI = strBizDocUI.Replace("#FooterImage#", CCommon.RenderControl(imgFooter))
                '        strBizDocUI = strBizDocUI.Replace("#BizDocName#", CCommon.RenderControl(lblBizDoc))
                '        strBizDocUI = strBizDocUI.Replace("#BillTo#", lblBillTo.Text)
                '        strBizDocUI = strBizDocUI.Replace("#ShipTo#", lblShipTo.Text)
                '        strBizDocUI = strBizDocUI.Replace("#BizDocStatus#", lblStatus.Text)
                '        strBizDocUI = strBizDocUI.Replace("#Currency#", lblBalanceDueCurrency.Text)
                '        strBizDocUI = strBizDocUI.Replace("#AmountPaid#", lblAmountPaid.Text)
                '        strBizDocUI = strBizDocUI.Replace("#BalanceDue#", CCommon.RenderControl(lblBalance)) 'used by amt paid js
                '        strBizDocUI = strBizDocUI.Replace("#Discount#", lblDiscount.Text)
                '        strBizDocUI = strBizDocUI.Replace("#BillingTerms#", lblBillingTerms.Text)
                '        If CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("dtFromDate")).Length > 3 Then
                '            strBizDocUI = strBizDocUI.Replace("#BillingTermFromDate#", FormattedDateFromDate(dtOppBiDocDtl.Rows(0).Item("dtFromDate"), Session("DateFormat")))
                '        End If
                '        strBizDocUI = strBizDocUI.Replace("#DueDate#", lblDuedate.Text)
                '        strBizDocUI = strBizDocUI.Replace("#BizDocID#", CCommon.RenderControl(lblBizDocIDValue))
                '        strBizDocUI = strBizDocUI.Replace("#P.O.NO#", lblNo.Text)
                '        strBizDocUI = strBizDocUI.Replace("#OrderID#", CCommon.RenderControl(hplOppID))
                '        strBizDocUI = strBizDocUI.Replace("#Comments#", lblComments.Text)
                '        strBizDocUI = strBizDocUI.Replace("#Products#", CCommon.RenderControl(dgBizDocs))
                '        strBizDocUI = strBizDocUI.Replace("#BizDocSummary#", CCommon.RenderControl(tblBizDocSumm))

                '        If strBizDocUI.IndexOf("#ChangeShipToAddress(") > 0 Then
                '            Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#ChangeShipToAddress(") + "#ChangeShipToAddress(".Length, strBizDocUI.LastIndexOf(")#") - strBizDocUI.IndexOf("#ChangeShipToAddress(") - "#ChangeShipToAddress(".Length)
                '            hplShipTo.InnerHtml = "<font color=""white"">" + strLink + "</font>"
                '            strBizDocUI = strBizDocUI.Replace("#ChangeShipToAddress(" & strLink & ")#", CCommon.RenderControl(hplShipTo))
                '        Else
                '            strBizDocUI = strBizDocUI.Replace("#ChangeShipToAddress#", CCommon.RenderControl(hplShipTo))
                '        End If

                '        If strBizDocUI.IndexOf("#ChangeBillToAddress(") > 0 Then
                '            Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#ChangeBillToAddress(") + "#ChangeBillToAddress(".Length, strBizDocUI.IndexOf(")#") - strBizDocUI.IndexOf("#ChangeBillToAddress(") - "#ChangeBillToAddress(".Length)
                '            hplBillto.InnerHtml = "<font color=""white"">" + strLink + "</font>"
                '            strBizDocUI = strBizDocUI.Replace("#ChangeBillToAddress(" & strLink & ")#", CCommon.RenderControl(hplBillto))
                '        Else
                '            strBizDocUI = strBizDocUI.Replace("#ChangeBillToAddress#", CCommon.RenderControl(hplBillto))
                '        End If

                '        strBizDocUI = strBizDocUI.Replace("#AmountPaidPopUp#", CCommon.RenderControl(hplAmountPaid))
                '        strBizDocUI = strBizDocUI.Replace("#ChangeDueDate#", CCommon.RenderControl(hplDueDate))
                '        strBizDocUI = strBizDocUI.Replace("#AssigneeName#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("AssigneeName")))
                '        strBizDocUI = strBizDocUI.Replace("#AssigneeEmail#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("AssigneeEmail")))
                '        strBizDocUI = strBizDocUI.Replace("#AssigneePhoneNo#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("AssigneePhone")))

                '        strBizDocUI = strBizDocUI.Replace("#OrganizationName#", CCommon.RenderControl(lblOrganizationName))
                '        strBizDocUI = strBizDocUI.Replace("#OrganizationContactName#", CCommon.RenderControl(lblOrganizationContactName))
                '        'strBizDocUI = strBizDocUI.Replace("#OrganizationName#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrganizationName")))
                '        'strBizDocUI = strBizDocUI.Replace("#OrganizationContactName#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrgContactName")))
                '        strBizDocUI = strBizDocUI.Replace("#OrganizationContactPhone#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrgContactPhone")))
                '        strBizDocUI = strBizDocUI.Replace("#OrganizationContactEmail#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrgContactEmail")))
                '        strBizDocUI = strBizDocUI.Replace("#OrganizationBillingAddress#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("CompanyBillingAddress")))
                '        strBizDocUI = strBizDocUI.Replace("#OrderRecOwner#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OnlyOrderRecOwner")))

                '        strBizDocUI = strBizDocUI.Replace("#BillToAddressName#", CCommon.RenderControl(lblBillToAddressName))
                '        strBizDocUI = strBizDocUI.Replace("#ShipToAddressName#", CCommon.RenderControl(lblShipToAddressName))

                '        strBizDocUI = strBizDocUI.Replace("#BillToCompanyName#", CCommon.RenderControl(lblBillToCompanyName))
                '        strBizDocUI = strBizDocUI.Replace("#ShipToCompanyName#", CCommon.RenderControl(lblShipToCompanyName))

                '        strBizDocUI = strBizDocUI.Replace("#ShippingCompany#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("ShipVia")))
                '        strBizDocUI = strBizDocUI.Replace("#BizDocTemplateName#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcTemplateName")))
                '        strBizDocUI = strBizDocUI.Replace("#TotalQuantity#", String.Format("{0:0.00}", CCommon.ToDecimal(dtOppBiDocItems.Compute("sum(numUnitHour)", ""))))

                '        Dim lNumericToWord As New NumericToWord

                '        strBizDocUI = strBizDocUI.Replace("#GrandTotalinWords#", lNumericToWord.SpellNumber(Replace(hdGrandTotal.Value, ",", "")))

                '        'Replace custom field values
                '        Dim objCustomFields As New CustomFields
                '        If dtOppBiDocDtl.Rows(0).Item("tintOppType") = 1 Then
                '            objCustomFields.locId = 2
                '        Else
                '            objCustomFields.locId = 6
                '        End If
                '        objCustomFields.RelId = 0
                '        objCustomFields.DomainID = Session("DomainID")
                '        objCustomFields.RecordId = lngOppId
                '        ds = objCustomFields.GetCustFlds
                '        For Each drow As DataRow In ds.Tables(0).Rows
                '            If CCommon.ToLong(drow("numListID")) > 0 Then
                '                strBizDocUI = strBizDocUI.Replace("#" & drow("fld_label").ToString.Trim.Replace(" ", "") & "#", objCommon.GetListItemValue(drow("Value"), Session("DomainID")))
                '            Else
                '                strBizDocUI = strBizDocUI.Replace("#" & drow("fld_label").ToString.Trim.Replace(" ", "") & "#", drow("Value"))
                '            End If
                '        Next
                '        litBizDocTemplate.Text = strBizDocUI
                '        tblOriginalBizDoc.Visible = False
                '        tblFormattedBizDoc.Visible = True
                '    End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub getAddDetails()
        Try
            Dim dtOppBizAddDtl As DataTable
            Dim objOppBizDocs As New OppBizDocs
            objOppBizDocs.OppBizDocId = lngOppBizDocID
            objOppBizDocs.OppId = lngOppId
            objOppBizDocs.DomainID = lngDomainID
            dtOppBizAddDtl = objOppBizDocs.GetOppAddressDetail
            If dtOppBizAddDtl.Rows.Count <> 0 Then
                'lblAddress.Text = IIf(IsDBNull(dtOppBizAddDtl.Rows(0).Item("CompName")), "", dtOppBizAddDtl.Rows(0).Item("CompName"))
                lblBillTo.Text = IIf(IsDBNull(dtOppBizAddDtl.Rows(0).Item("BillAdd")), "", dtOppBizAddDtl.Rows(0).Item("BillAdd"))
                lblShipTo.Text = IIf(IsDBNull(dtOppBizAddDtl.Rows(0).Item("ShipAdd")), "", dtOppBizAddDtl.Rows(0).Item("ShipAdd"))
                hplOppID.Text = IIf(IsDBNull(dtOppBizAddDtl.Rows(0).Item("OppName")), "", dtOppBizAddDtl.Rows(0).Item("OppName"))
                lblBizDoc.Text = IIf(IsDBNull(dtOppBizAddDtl.Rows(0).Item("BizDcocName")), "", dtOppBizAddDtl.Rows(0).Item("BizDcocName"))
                lblBizDocIDLabel.Text = lblBizDoc.Text & "#"
                'hdShipAmt.Value = String.Format("{0:#,##0.00}", IIf(IsDBNull(dtOppBizAddDtl.Rows(0).Item("ShipAmount")), "", dtOppBizAddDtl.Rows(0).Item("ShipAmount")))
                txtBizDoc.Text = dtOppBizAddDtl.Rows(0).Item("BizDoc")
                txtConEmail.Text = dtOppBizAddDtl.Rows(0).Item("vcEmail")
                txtOppOwner.Text = dtOppBizAddDtl.Rows(0).Item("Owner")
                txtCompName.Text = dtOppBizAddDtl.Rows(0).Item("CompName")
                txtConID.Text = dtOppBizAddDtl.Rows(0).Item("numContactID")
                txtBizDocRecOwner.Text = dtOppBizAddDtl.Rows(0).Item("BizDocOwner")
                'objOppBizDocs.BizDocId = dtOppBizAddDtl.Rows(0).Item("BizDoc")
                'objOppBizDocs.DomainID = lngDomainID
                'Dim i As Integer
                'i = objOppBizDocs.GetBizDocAttachments.Rows.Count
                'hplBizDocAtch.Text = "Attachments(" & IIf(i = 0, 1, i) & ")"
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindItems()
        Try
            Dim dtItems As New DataTable
            dtItems = Session("Itemslist")
            dgBizDocs.DataSource = dtItems
            dgBizDocs.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub



    Function ReturnMoney(ByVal Money)
        Try
            Return String.Format("{0:#,###.##}", Math.Round(Money, 2))
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Try
            If lngOppBizDocID > 0 Then
                Dim objDocuments As New DocumentList
                objDocuments.GenDocID = lngOppBizDocID
                objDocuments.ContactID = lngCntID
                objDocuments.CDocType = "B"
                objDocuments.byteMode = 3
                objDocuments.Comments = txtComment.Text
                objDocuments.UserCntID = Session("UserContactID")
                objDocuments.ManageApprovers()
                pnlApprove.Visible = False

                SendEmail(True)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnDecline_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDecline.Click
        Try
            If lngOppBizDocID > 0 Then
                Dim objDocuments As New DocumentList
                objDocuments.GenDocID = lngOppBizDocID
                objDocuments.ContactID = lngCntID
                objDocuments.CDocType = "B"
                objDocuments.byteMode = 4
                objDocuments.Comments = txtComment.Text
                objDocuments.UserCntID = Session("UserContactID")
                objDocuments.ManageApprovers()
                pnlApprove.Visible = False
                SendEmail(False)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Function UpdateAmountPaid() As Boolean
        Try
            Dim objOppInvoice As New OppInvoice
            objOppInvoice.AmtPaid = txtAmount.Text
            objOppInvoice.UserCntID = lngCntID
            objOppInvoice.OppBizDocId = CCommon.ToLong(GetQueryStringVal("OppBizId"))
            objOppInvoice.OppId = CCommon.ToLong(GetQueryStringVal("OpID"))
            objOppInvoice.PaymentMethod = 1
            objOppInvoice.BizDocsPaymentDetId = 0
            ''objOppInvoice.DepositTo = ddlAccounts.SelectedItem.Value
            objOppInvoice.Reference = ResponseMessage
            objOppInvoice.IntegratedToAcnt = False
            objOppInvoice.Memo = ""
            objOppInvoice.DomainID = lngDomainID
            objOppInvoice.DeferredIncomeStartDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
            objOppInvoice.bitDisable = True

            Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                If objOppInvoice.UpdateAmountPaid() = True Then
                    'Create Journal Entries Here if payment through CreditCard And if its Sales Order
                    lngJournalID = SaveDataToHeader(txtAmount.Text + (txtAmount.Text * TransactionCharge / 100), objOppInvoice.BizDocsPaymentDetId, lngOppBizDocID, lngOppId)
                    SaveDataToGeneralJournalDetailsForCreditCard(objOppInvoice.BizDocsPaymentDetId)
                End If

                objTransactionScope.Complete()
            End Using
        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Dim TransactionCharge As Double
    Dim TransactionChargeAccountID As Long
    Dim UndepositedFundAccountID As Long
    Dim lngCustomerARAccount As Long
    Dim lngJournalID As Long
    Dim AsmountToPay As Decimal
    Private Sub btnCharge_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCharge.Click
        Try
            'validate Card Type Mapping 
            'Get transaction charge for selected credit card type 
            Dim objOppBizDocs As New OppBizDocs
            Dim lShouldReturn As Boolean
            ValidateCardTypeMapping(TransactionCharge, TransactionChargeAccountID, lShouldReturn)
            If lShouldReturn Then
                Exit Sub
            End If
            UndepositedFundAccountID = ChartOfAccounting.GetDefaultAccount("UF", Session("DomainID"))
            If Not UndepositedFundAccountID > 0 Then
                litMessage.Text = "Please Map Default Undeposited Funds Account for Your Company from Administration->Global Settings->Accounting->Default Accounts."
                Exit Sub
            End If
            lngCustomerARAccount = objOppBizDocs.ValidateCustomerAR_APAccounts("AR", Session("DomainID"), Session("DivID"))
            If lngCustomerARAccount = 0 Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set AR and AP Relationship from Administration->Global Settings->Accounting->Accounts for RelationShip To Save Amount' );", True)
                Exit Sub
            End If

            Dim ResponseMessage As String = ""
            Dim objPaymentGateway As New PaymentGateway
            objPaymentGateway.DomainID = Session("DomainID")
            'Uncomment this part in production and replace with if condition, COmmmented for testing purpose
            'If objPaymentGateway.GatewayTransaction(IIf(IsNumeric(txtAmount.Text), txtAmount.Text, 0), txtCHName.Text, txtCCNo.Text, txtCVV2.Text, ddlMonth.SelectedValue, ddlYear.SelectedValue, txtConID.Text, ResponseMessage) Then
            If True Then
                UpdateAmountPaid()
            End If

            litMessage.Text = ResponseMessage
            getBizDocDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub BindCardType()
        Try

            Dim objCommon As New CCommon
            objCommon.sb_FillComboFromDB(ddlCardType, 120, CCommon.ToLong(Session("DomainID")))
            'objCommon.DomainID = Session("DomainID")
            'objCommon.tinyOrder = 0
            'ddlCardType.DataTextField = "CardType"
            'ddlCardType.DataValueField = "numListItemID"
            'ddlCardType.DataSource = objCommon.GetCardTypes()
            'ddlCardType.DataBind()
            'ddlCardType.Items.Insert(0, "--Select One--")
            'ddlCardType.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Function SaveDataToHeader(ByVal p_Amount As Decimal, ByVal p_BizDocsPaymentDetId As Integer, ByVal p_OppBizDocId As Integer, ByVal p_OppId As Integer) As Integer
        Try
            Dim objJEHeader As New JournalEntryHeader
            With objJEHeader
                .JournalId = lngJournalID
                .RecurringId = 0
                .EntryDate = Date.UtcNow
                .Description = ""
                .Amount = p_Amount
                .CheckId = 0
                .CashCreditCardId = 0
                .ChartAcntId = 0
                .OppId = p_OppId
                .OppBizDocsId = p_OppBizDocId
                .DepositId = 0
                .BizDocsPaymentDetId = p_BizDocsPaymentDetId
                .IsOpeningBalance = 0
                .LastRecurringDate = Date.Now
                .NoTransactions = 0
                .CategoryHDRID = 0
                .ReturnID = 0
                .CheckHeaderID = 0
                .BillID = 0
                .BillPaymentID = 0
                .UserCntID = Session("UserContactID")
                .DomainID = Session("DomainID")
            End With
            Return objJEHeader.Save()
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Sub SaveDataToGeneralJournalDetailsForCreditCard(ByVal lngBizDocsPaymentDetailsId As Long)
        Dim ldecAmount As Decimal

        Dim objOppBizDocs As New OppBizDocs
        Try
            Dim objJEList As New JournalEntryCollection

            Dim objJE As New JournalEntryNew()


            'Debit : Undeposit Fund (Amount*TransCharge/100)
            objJE = New JournalEntryNew()

            ldecAmount = txtAmount.Text + (txtAmount.Text * TransactionCharge / 100)

            objJE.TransactionId = 0
            objJE.DebitAmt = ldecAmount
            objJE.CreditAmt = 0
            objJE.ChartAcntId = UndepositedFundAccountID
            objJE.Description = "Credit card amount to Undeposited Fund"
            objJE.CustomerId = Session("DivID")
            objJE.MainDeposit = 0
            objJE.MainCheck = 0
            objJE.MainCashCredit = 0
            objJE.OppitemtCode = 0
            objJE.BizDocItems = ""
            objJE.Reference = ""
            objJE.PaymentMethod = 0
            objJE.Reconcile = False
            objJE.CurrencyID = 0
            objJE.FltExchangeRate = 0
            objJE.TaxItemID = 0
            objJE.BizDocsPaymentDetailsId = lngBizDocsPaymentDetailsId
            objJE.ContactID = 0
            objJE.ItemID = 0
            objJE.ProjectID = 0
            objJE.ClassID = 0
            objJE.CommissionID = 0
            objJE.ReconcileID = 0
            objJE.Cleared = 0
            objJE.ReferenceType = 0
            objJE.ReferenceID = 0

            objJEList.Add(objJE)

            'Credit: Customer A/R With (Amount)
            objJE = New JournalEntryNew()

            objJE.TransactionId = 0
            objJE.DebitAmt = 0
            objJE.CreditAmt = Math.Abs(CCommon.ToDecimal(txtAmount.Text))
            objJE.ChartAcntId = lngCustomerARAccount
            objJE.Description = "Credit Customer's AR account"
            objJE.CustomerId = Session("DivID")
            objJE.MainDeposit = 0
            objJE.MainCheck = 0
            objJE.MainCashCredit = 0
            objJE.OppitemtCode = 0
            objJE.BizDocItems = ""
            objJE.Reference = ""
            objJE.PaymentMethod = 0
            objJE.Reconcile = False
            objJE.CurrencyID = 0
            objJE.FltExchangeRate = 0
            objJE.TaxItemID = 0
            objJE.BizDocsPaymentDetailsId = lngBizDocsPaymentDetailsId
            objJE.ContactID = 0
            objJE.ItemID = 0
            objJE.ProjectID = 0
            objJE.ClassID = 0
            objJE.CommissionID = 0
            objJE.ReconcileID = 0
            objJE.Cleared = 0
            objJE.ReferenceType = 0
            objJE.ReferenceID = 0

            objJEList.Add(objJE)

            'Credit: CreditCard's transaction charge to Mapped Transaction Account(Whatever %) 
            objJE = New JournalEntryNew()

            objJE.TransactionId = 0
            objJE.DebitAmt = 0
            objJE.CreditAmt = Math.Abs(CCommon.ToDecimal((txtAmount.Text * TransactionCharge / 100)))
            objJE.ChartAcntId = TransactionChargeAccountID
            objJE.Description = "Transaction Charge"
            objJE.CustomerId = Session("DivID")
            objJE.MainDeposit = 0
            objJE.MainCheck = 0
            objJE.MainCashCredit = 0
            objJE.OppitemtCode = 0
            objJE.BizDocItems = ""
            objJE.Reference = ""
            objJE.PaymentMethod = 0
            objJE.Reconcile = False
            objJE.CurrencyID = 0
            objJE.FltExchangeRate = 0
            objJE.TaxItemID = 0
            objJE.BizDocsPaymentDetailsId = lngBizDocsPaymentDetailsId
            objJE.ContactID = 0
            objJE.ItemID = 0
            objJE.ProjectID = 0
            objJE.ClassID = 0
            objJE.CommissionID = 0
            objJE.ReconcileID = 0
            objJE.Cleared = 0
            objJE.ReferenceType = 0
            objJE.ReferenceID = 0

            objJEList.Add(objJE)

            'Debit: CreditCard's transaction charge
            objJE = New JournalEntryNew()

            objJE.TransactionId = 0
            objJE.DebitAmt = Math.Abs(CCommon.ToDecimal((txtAmount.Text * TransactionCharge / 100)))
            objJE.CreditAmt = 0
            objJE.ChartAcntId = TransactionChargeAccountID
            objJE.Description = "Transaction Charge"
            objJE.CustomerId = Session("DivID")
            objJE.MainDeposit = 0
            objJE.MainCheck = 0
            objJE.MainCashCredit = 0
            objJE.OppitemtCode = 0
            objJE.BizDocItems = ""
            objJE.Reference = ""
            objJE.PaymentMethod = 0
            objJE.Reconcile = False
            objJE.CurrencyID = 0
            objJE.FltExchangeRate = 0
            objJE.TaxItemID = 0
            objJE.BizDocsPaymentDetailsId = lngBizDocsPaymentDetailsId
            objJE.ContactID = 0
            objJE.ItemID = 0
            objJE.ProjectID = 0
            objJE.ClassID = 0
            objJE.CommissionID = 0
            objJE.ReconcileID = 0
            objJE.Cleared = 0
            objJE.ReferenceType = 0
            objJE.ReferenceID = 0

            objJEList.Add(objJE)

            'Credit: Undeposited Fund 
            objJE = New JournalEntryNew()

            objJE.TransactionId = 0
            objJE.DebitAmt = 0
            objJE.CreditAmt = Math.Abs(CCommon.ToDecimal((txtAmount.Text * TransactionCharge / 100)))
            objJE.ChartAcntId = UndepositedFundAccountID
            objJE.Description = "Credit card amount to Undeposited Fund"
            objJE.CustomerId = Session("DivID")
            objJE.MainDeposit = 0
            objJE.MainCheck = 0
            objJE.MainCashCredit = 0
            objJE.OppitemtCode = 0
            objJE.BizDocItems = ""
            objJE.Reference = ""
            objJE.PaymentMethod = 0
            objJE.Reconcile = False
            objJE.CurrencyID = 0
            objJE.FltExchangeRate = 0
            objJE.TaxItemID = 0
            objJE.BizDocsPaymentDetailsId = lngBizDocsPaymentDetailsId
            objJE.ContactID = 0
            objJE.ItemID = 0
            objJE.ProjectID = 0
            objJE.ClassID = 0
            objJE.CommissionID = 0
            objJE.ReconcileID = 0
            objJE.Cleared = 0
            objJE.ReferenceType = 0
            objJE.ReferenceID = 0

            objJEList.Add(objJE)

            objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalID, Session("DomainID"))

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub ValidateCardTypeMapping(ByRef TransactionCharge As Double, ByRef TransactionChargeAccountID As Long, ByRef shouldReturn As Boolean)
        shouldReturn = False
        If ddlCardType.SelectedValue > 0 Then
            Dim objCOA As New ChartOfAccounting
            Dim ds As DataSet
            With objCOA
                .TransChargeID = 0
                .CreditCardTypeId = ddlCardType.SelectedValue
                .DomainID = Session("DomainID")
                ds = .GetCOACreditCardCharge()
                If ds.Tables(0).Rows.Count > 0 Then
                    TransactionChargeAccountID = CCommon.ToLong(ds.Tables(0).Rows(0).Item("numAccountId"))
                    If TransactionChargeAccountID > 0 Then
                        TransactionCharge = CCommon.ToDouble(ds.Tables(0).Rows(0).Item("fltTransactionCharge"))
                    Else
                        litMessage.Text = "Please Set AP account for " & ddlCardType.SelectedItem.Text & " from Administration->Global Settings->Accounting->Accounts for Credit Card."
                        shouldReturn = True : Exit Sub
                    End If
                Else
                    litMessage.Text = "Please Set AP account for " & ddlCardType.SelectedItem.Text & " from Administration->Global Settings->Accounting->Accounts for Credit Card."
                    shouldReturn = True : Exit Sub
                End If
            End With
        Else
            litMessage.Text = "Please Select a Card Type"
            shouldReturn = True : Exit Sub
        End If
    End Sub

    Private Sub SendEmail(ByVal boolApproved As Boolean)
        Try
            Dim objSendEmail As New Email

            objSendEmail.DomainID = Session("DomainID")
            objSendEmail.TemplateCode = "#SYS#DOC_APPROVAL/DECLINED_NOTIFICATION"
            objSendEmail.ModuleID = 1
            objSendEmail.RecordIds = txtBizDocRecOwner.Text
            objSendEmail.AdditionalMergeFields.Add("DocumentApprovalStatus", IIf(boolApproved, "Approved", "Declined"))
            objSendEmail.AdditionalMergeFields.Add("DocumentApprovalComment", txtComment.Text.Trim)
            objSendEmail.AdditionalMergeFields.Add("DocumentName", lblBizDocIDValue.Text.Trim)
            objSendEmail.AdditionalMergeFields.Add("LoggedInUser", Session("ContactName"))
            objSendEmail.FromEmail = Session("UserEmail")
            objSendEmail.ToEmail = "##ContactFirstName## <##ContactEmail##>"
            objSendEmail.SendEmailTemplate()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgBizDocs_ItemDataBound(sender As Object, e As DataGridItemEventArgs) Handles dgBizDocs.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            e.Item.Attributes.Add("style", "page-break-inside : avoid;")

            If e.Item.ItemType = ListItemType.Item Then
                Select Case CCommon.ToInteger(DirectCast(e.Item.DataItem, System.Data.DataRowView)("tintLevel"))
                    Case 0
                        e.Item.CssClass = "ItemStyle Level0"
                    Case 1
                        e.Item.CssClass = "ItemStyle Level1"
                    Case 2
                        e.Item.CssClass = "ItemStyle Level2"
                End Select
            ElseIf e.Item.ItemType = ListItemType.AlternatingItem Then
                Select Case CCommon.ToInteger(DirectCast(e.Item.DataItem, System.Data.DataRowView)("tintLevel"))
                    Case 0
                        e.Item.CssClass = "AltItemStyle Level0"
                    Case 1
                        e.Item.CssClass = "AltItemStyle Level1"
                    Case 2
                        e.Item.CssClass = "AltItemStyle Level2"
                End Select
            End If
        End If
    End Sub
End Class