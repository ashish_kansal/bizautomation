<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCusOppList.aspx.vb" Inherits="BACRMPortal.frmCusOppList" %>
<%--<%@ Register TagPrefix="menu1" TagName="webmenu" src="../common/topbar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
		<title>Opportunity</title>
		<script language="javascript">
		function goto(target,frame,div)
	    {
	             window.open(target,'','toolbar=no,titlebar=no,left=150, top=150,width=750,height=450,scrollbars=no,resizable=yes')  
	            
        }
		function fnSortByCharPurchase(varSortChar)
			{
				document.Form1.txtSortCharPurchase.value = varSortChar;
				if (typeof(document.Form1.txtCurrrentPagePurchase)!='undefined')
				{
					document.Form1.txtCurrrentPagePurchase.value=1;
				}
				document.Form1.btnGo.click();
			}
		function fnSortByCharSales(varSortChar)
			{
				document.Form1.txtSortCharSales.value = varSortChar;
				if (typeof(document.Form1.txtCurrrentPageSales)!='undefined')
				{
					document.Form1.txtCurrrentPageSales.value=1;
				}
				document.Form1.btnGo.click();
			}
	
			function DeleteRecord()
		{
			if(confirm('Are you sure, you want to delete the selected record?'))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		function DeleteMessage()
		{
			alert("You Are not Authorized to Delete the Selected Record !");
			return false;
		}
		</script>
	</HEAD>
	<body >
		
		<form id="Form1" method="post" runat="server">	
		<menu1:webmenu id="webmenu1" runat="server"></menu1:webmenu>
           <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
			
			<asp:updatepanel ID="UpdatePanelTab" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" ><ContentTemplate>
			<table cellspacing="0" cellpadding="0" width="100%" border="0">
				<tr>
				<asp:Button runat="server" id="btnGo" Text="" style="display:none"></asp:Button>
				
				
					<td valign="bottom">
					</td>
					<TD class="normal1" width="150" align="center">No of Records:
						<asp:label id="lblRecordsSales" runat="server"></asp:label>
						<asp:label id="lblRecordsPurchase" runat="server"></asp:label>
					</TD>
					
					<asp:Panel id="pnlSales" Runat="server">
						<TD id="hideSales" noWrap align="right" runat="server">
							<table>
								<TR>
									<TD>
										<asp:label id="lblNextSales" runat="server" cssclass="Text_bold">Next:</asp:label></TD>
									<TD class="normal1">
										<asp:linkbutton id="lnk2Sales" runat="server">2</asp:linkbutton></TD>
									<TD class="normal1">
										<asp:linkbutton id="lnk3Sales" runat="server">3</asp:linkbutton></TD>
									<TD class="normal1">
										<asp:linkbutton id="lnk4Sales" runat="server">4</asp:linkbutton></TD>
									<TD class="normal1">
										<asp:linkbutton id="lnk5Sales" runat="server">5</asp:linkbutton></TD>
									<TD>
										<asp:linkbutton id="lnkFirstSales" runat="server">
											<div class="LinkArrow"><<</div>
										</asp:linkbutton></TD>
									<TD>
										<asp:linkbutton id="lnkPreviousSales" runat="server">
											<div class="LinkArrow"><</div>
										</asp:linkbutton></TD>
									<TD class="normal1">
										<asp:label id="lblPageSales" runat="server">Page</asp:label></TD>
									<TD>
										<asp:textbox id="txtCurrrentPageSales" runat="server" MaxLength="5" AutoPostBack="True" Width="28px"
											CssClass="signup"></asp:textbox></TD>
									<TD class="normal1">
										<asp:label id="lblOfSales" runat="server">of</asp:label></TD>
									<TD class="normal1">
										<asp:label id="lblTotalSales" runat="server"></asp:label></TD>
									<TD>
										<asp:linkbutton id="lnkNextSales" runat="server" CssClass="LinkArrow">
											<div class="LinkArrow">></div>
										</asp:linkbutton></TD>
									<TD>
										<asp:linkbutton id="lnkLastSales" runat="server">
											<div class="LinkArrow">>></div>
										</asp:linkbutton></TD>
								</TR>
							</table>
						</TD>
					</asp:Panel>
					<asp:Panel id="pnlPurchase" Runat="server">
						<TD id="hidePurchase" noWrap align="right" runat="server">
							<table>
								<TR>
									<TD>
										<asp:label id="lblNextPurchase" runat="server" cssclass="Text_bold">Next:</asp:label></TD>
									<TD class="normal1">
										<asp:linkbutton id="lnk2Purchase" runat="server">2</asp:linkbutton></TD>
									<TD class="normal1">
										<asp:linkbutton id="lnk3Purchase" runat="server">3</asp:linkbutton></TD>
									<TD class="normal1">
										<asp:linkbutton id="lnk4Purchase" runat="server">4</asp:linkbutton></TD>
									<TD class="normal1">
										<asp:linkbutton id="lnk5Purchase" runat="server">5</asp:linkbutton></TD>
									<TD>
										<asp:linkbutton id="lnkFirstPurchase" runat="server">
											<div class="LinkArrow"><<</div>
										</asp:linkbutton></TD>
									<TD>
										<asp:linkbutton id="lnkPreviousPurchase" runat="server">
											<div class="LinkArrow"><</div>
										</asp:linkbutton></TD>
									<TD class="normal1">
										<asp:label id="lblPagePurchase" runat="server">Page</asp:label></TD>
									<TD>
										<asp:textbox id="txtCurrrentPagePurchase" runat="server" MaxLength="5" AutoPostBack="True" Width="28px"
											CssClass="signup"></asp:textbox></TD>
									<TD class="normal1">
										<asp:label id="lblOfPurchase" runat="server">of</asp:label></TD>
									<TD class="normal1">
										<asp:label id="lblTotalPurchase" runat="server"></asp:label></TD>
									<TD>
										<asp:linkbutton id="lnkNextPurchase" runat="server" CssClass="LinkArrow">
											<div class="LinkArrow">></div>
										</asp:linkbutton></TD>
									<TD>
										<asp:linkbutton id="lnkLastPurchase" runat="server">
											<div class="LinkArrow">>></div>
										</asp:linkbutton></TD>
								</TR>
							</table>
						</TD>
					</asp:Panel>
				</tr>
			</table>
		<igtab:ultrawebtab  AutoPostBack="true" ImageDirectory=""  id="uwOppTab" runat="server" ThreeDEffect="True" BorderStyle="Solid" Width="100%" BarHeight="0"  BorderWidth="0">
                        <DefaultTabStyle Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Arial" >
                        </DefaultTabStyle>
                        <RoundedImage LeftSideWidth="7" RightSideWidth="8" ShiftOfImages="0" SelectedImage="../images/ig_tab_winXPs3.gif" NormalImage="../images/ig_tab_winXP3.gif" HoverImage="../images/ig_tab_winXPs3.gif" FillStyle="LeftMergedWithCenter" ></RoundedImage>
                     <SelectedTabStyle Height="23px"  ForeColor="white">
                        </SelectedTabStyle>
                        <HoverTabStyle Height="23px" ForeColor="white"></HoverTabStyle>
                        <Tabs>
                            <igtab:Tab Text="&nbsp;&nbsp;Sales Opportunities&nbsp;&nbsp;" >
                                <ContentTemplate>
                              
					<table cellspacing="1" cellpadding="1" width="100%" border="0">
						<tr>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="a" href="javascript:fnSortByCharSales('a')">
									<div class="A2Z">A</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="b" href="javascript:fnSortByCharSales('b')">
									<div class="A2Z">B</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="c" href="javascript:fnSortByCharSales('c')">
									<div class="A2Z">C</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="d" href="javascript:fnSortByCharSales('d')">
									<div class="A2Z">D</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="e" href="javascript:fnSortByCharSales('e')">
									<div class="A2Z">E</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="f" href="javascript:fnSortByCharSales('f')">
									<div class="A2Z">F</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="g" href="javascript:fnSortByCharSales('g')">
									<div class="A2Z">G</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="h" href="javascript:fnSortByCharSales('h')">
									<div class="A2Z">H</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="I" href="javascript:fnSortByCharSales('i')">
									<div class="A2Z">I</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="j" href="javascript:fnSortByCharSales('j')">
									<div class="A2Z">J</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="k" href="javascript:fnSortByCharSales('k')">
									<div class="A2Z">K</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="l" href="javascript:fnSortByCharSales('l')">
									<div class="A2Z">L</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="m" href="javascript:fnSortByCharSales('m')">
									<div class="A2Z">M</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="n" href="javascript:fnSortByCharSales('n')">
									<div class="A2Z">N</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="o" href="javascript:fnSortByCharSales('o')">
									<div class="A2Z">O</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="p" href="javascript:fnSortByCharSales('p')">
									<div class="A2Z">P</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="q" href="javascript:fnSortByCharSales('q')">
									<div class="A2Z">Q</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="r" href="javascript:fnSortByCharSales('r')">
									<div class="A2Z">R</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="s" href="javascript:fnSortByCharSales('s')">
									<div class="A2Z">S</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="t" href="javascript:fnSortByCharSales('t')">
									<div class="A2Z">T</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="u" href="javascript:fnSortByCharSales('u')">
									<div class="A2Z">U</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="v" href="javascript:fnSortByCharSales('v')">
									<div class="A2Z">V</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="w" href="javascript:fnSortByCharSales('w')">
									<div class="A2Z">W</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="x" href="javascript:fnSortByCharSales('x')">
									<div class="A2Z">X</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="y" href="javascript:fnSortByCharSales('y')">
									<div class="A2Z">Y</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="z" href="javascript:fnSortByCharSales('z')">
									<div class="A2Z">Z</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="all" href="javascript:fnSortByCharSales('0')">
									<div class="A2Z">All</div>
								</A>
							</td>
						</tr>
					</table>
					<asp:table id="table1" cellpadding="0" cellspacing="0" BorderWidth="1" Runat="server" Width="100%"
						BorderColor="black" GridLines="None" Height="350" CssClass="aspTable">
						<asp:tableRow>
							<asp:tableCell VerticalAlign="Top">
							
								<asp:datagrid id="dgSales" AllowSorting="True" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
									BorderColor="white">
									<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
									<ItemStyle CssClass="is"></ItemStyle>
									<HeaderStyle CssClass="hs"></HeaderStyle>
									<Columns>
										<asp:BoundColumn Visible="False" DataField="numOppId"></asp:BoundColumn>
										<asp:BoundColumn Visible="False" DataField="numRecOwner" ></asp:BoundColumn>
										<asp:ButtonColumn DataTextField="Name" HeaderText="<font color=white>Name</font>" SortExpression="vcPOppName"
											CommandName="Name"></asp:ButtonColumn>
										<asp:ButtonColumn DataTextField="Company" SortExpression="vcCompanyName" HeaderText="<font color=white>Customer</font>"
											CommandName="Customer"></asp:ButtonColumn>
										<asp:ButtonColumn DataTextField="Contact" SortExpression="ADC.vcFirstName" HeaderText="<font color=white>Contact</font>"
											CommandName="Contact"></asp:ButtonColumn>
										<asp:BoundColumn DataField="STAGE" SortExpression="numOppId" HeaderText="<font color=white>Status</font>"></asp:BoundColumn>
										<asp:TemplateColumn HeaderText="<font color=white>Due Date</font>" SortExpression="intPEstimatedCloseDate">
											<ItemTemplate>
												<%#ReturnDateTime(DataBinder.Eval(Container.DataItem, "CloseDate"))%>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="<font color=white>Amount</font>" SortExpression="monPAmount">
											<ItemTemplate>
												<%# ReturnMoney(DataBinder.Eval(Container.DataItem, "monPAmount")) %>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:BoundColumn DataField="UserName" SortExpression="ADC1.vcFirstName" HeaderText="<font color=white>Record Owner</font>"></asp:BoundColumn>
										<asp:BoundColumn DataField="AssignedToBy" SortExpression="ADC1.vcFirstName" HeaderText="<font color=white>Assigned To/By</font>"></asp:BoundColumn>
										<asp:TemplateColumn>
									
											<ItemTemplate>
												<asp:Button ID="btnDeleteSales" Runat="server" CssClass="Delete" Text="X" CommandName="Delete"></asp:Button>
												<asp:LinkButton ID="lnkdeleteSales" Runat="server" Visible="false">
													<font color="#730000">*</font></asp:LinkButton>
											</ItemTemplate>
										</asp:TemplateColumn>
									</Columns>
								</asp:datagrid>
								
							</asp:tableCell>
						</asp:tableRow>
					</asp:table>
				  </ContentTemplate>
                             </igtab:Tab>
                              <igtab:Tab Text="&nbsp;&nbsp;Purchase Opportunities&nbsp;&nbsp;" >
                                <ContentTemplate>
                            
					<table cellspacing="1" cellpadding="1" width="100%" border="0">
						<tr>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="a1" href="javascript:fnSortByCharPurchase('a')">
									<div class="A2Z">A</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="A2" href="javascript:fnSortByCharPurchase('b')">
									<div class="A2Z">B</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="A3" href="javascript:fnSortByCharPurchase('c')">
									<div class="A2Z">C</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="A4" href="javascript:fnSortByCharPurchase('d')">
									<div class="A2Z">D</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="A5" href="javascript:fnSortByCharPurchase('e')">
									<div class="A2Z">E</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="A6" href="javascript:fnSortByCharPurchase('f')">
									<div class="A2Z">F</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="A7" href="javascript:fnSortByCharPurchase('g')">
									<div class="A2Z">G</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="A8" href="javascript:fnSortByCharPurchase('h')">
									<div class="A2Z">H</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="A9" href="javascript:fnSortByCharPurchase('i')">
									<div class="A2Z">I</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="A10" href="javascript:fnSortByCharPurchase('j')">
									<div class="A2Z">J</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="A11" href="javascript:fnSortByCharPurchase('k')">
									<div class="A2Z">K</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="A12" href="javascript:fnSortByCharPurchase('l')">
									<div class="A2Z">L</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="A13" href="javascript:fnSortByCharPurchase('m')">
									<div class="A2Z">M</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="A14" href="javascript:fnSortByCharPurchase('n')">
									<div class="A2Z">N</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="A15" href="javascript:fnSortByCharPurchase('o')">
									<div class="A2Z">O</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="A16" href="javascript:fnSortByCharPurchase('p')">
									<div class="A2Z">P</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="A17" href="javascript:fnSortByCharPurchase('q')">
									<div class="A2Z">Q</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="A18" href="javascript:fnSortByCharPurchase('r')">
									<div class="A2Z">R</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="A19" href="javascript:fnSortByCharPurchase('s')">
									<div class="A2Z">S</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="A20" href="javascript:fnSortByCharPurchase('t')">
									<div class="A2Z">T</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="A21" href="javascript:fnSortByCharPurchase('u')">
									<div class="A2Z">U</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="A22" href="javascript:fnSortByCharPurchase('v')">
									<div class="A2Z">V</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="A23" href="javascript:fnSortByCharPurchase('w')">
									<div class="A2Z">W</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="A24" href="javascript:fnSortByCharPurchase('x')">
									<div class="A2Z">X</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="A25" href="javascript:fnSortByCharPurchase('y')">
									<div class="A2Z">Y</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="A26" href="javascript:fnSortByCharPurchase('z')">
									<div class="A2Z">Z</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="a27" href="javascript:fnSortByCharPurchase('0')">
									<div class="A2Z">All</div>
								</A>
							</td>
						</tr>
					</table>
					<asp:table id="table3" cellpadding="0" cellspacing="0" BorderWidth="1" Runat="server" Width="100%"
						BorderColor="black" GridLines="None" Height="350" CssClass="aspTable">
						<asp:tableRow>
							<asp:tableCell VerticalAlign="Top">
							
								<asp:datagrid id="dgPurchase" AllowSorting="True" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
									BorderColor="white">
									<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
									<ItemStyle CssClass="is"></ItemStyle>
									<HeaderStyle CssClass="hs"></HeaderStyle>
									<Columns>
										<asp:BoundColumn Visible="False" DataField="numOppId"></asp:BoundColumn>
										<asp:BoundColumn Visible="False" DataField="numRecOwner" ></asp:BoundColumn>
										<asp:ButtonColumn DataTextField="Name" SortExpression="vcPOppName" HeaderText="<font color=white>Name</font>"
											CommandName="Name"></asp:ButtonColumn>
										<asp:ButtonColumn DataTextField="Company" SortExpression="vcCompanyName" HeaderText="<font color=white>Vendor</font>"
											CommandName="Customer"></asp:ButtonColumn>
										<asp:ButtonColumn DataTextField="Contact" SortExpression="vcFirstName" HeaderText="<font color=white>Contact</font>"
											CommandName="Contact"></asp:ButtonColumn>
										
										<asp:BoundColumn DataField="STAGE" SortExpression="numOppId" HeaderText="<font color=white>Status</font>"></asp:BoundColumn>
										<asp:TemplateColumn HeaderText="<font color=white>Due Date</font>" SortExpression="intPEstimatedCloseDate">
											<ItemTemplate>
												<%# ReturnDateTime(DataBinder.Eval(Container.DataItem, "CloseDate")) %>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:TemplateColumn HeaderText="<font color=white>Amount</font>" SortExpression="monPAmount">
											<ItemTemplate>
												<%# ReturnMoney(DataBinder.Eval(Container.DataItem, "monPAmount")) %>
											</ItemTemplate>
										</asp:TemplateColumn>
										<asp:BoundColumn DataField="UserName" SortExpression="ADC1.vcFirstName" HeaderText="<font color=white>Record Owner</font>"></asp:BoundColumn>
										<asp:BoundColumn DataField="AssignedToBy" SortExpression="ADC1.vcFirstName" HeaderText="<font color=white>Assigned To/By</font>"></asp:BoundColumn>
										<asp:TemplateColumn>
										
											<ItemTemplate>
												<asp:Button ID="btnDeletePurchase" Runat="server" CssClass="Delete" Text="X" CommandName="Delete"></asp:Button>
												<asp:LinkButton ID="lnkDeletePurchase" Runat="server" Visible="false">
													<font color="#730000">*</font></asp:LinkButton>
											</ItemTemplate>
										</asp:TemplateColumn>
									</Columns>
									<PagerStyle Visible="False" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
								</asp:datagrid>
								
							</asp:tableCell>
						</asp:tableRow>
					</asp:table>
			    </ContentTemplate>
                             </igtab:Tab>
                        </Tabs>
                    </igtab:ultrawebtab>
			<table width="100%">
				<tr>
					<td class="normal4" align="center">
						<asp:Literal ID="litMessage" Runat="server"></asp:Literal></td>
				</tr>
			</table>
			<asp:TextBox ID="txtTotalPageSales" Runat="server" style="DISPLAY:none"></asp:TextBox>
			<asp:TextBox ID="txtTotalRecordsSales" Runat="server" style="DISPLAY:none"></asp:TextBox>
			<asp:TextBox ID="txtSortCharSales" Runat="server" style="DISPLAY:none"></asp:TextBox>
			<asp:TextBox ID="txtTotalPagePurchase" Runat="server" style="DISPLAY:none"></asp:TextBox>
			<asp:TextBox ID="txtTotalRecordsPurchase" Runat="server" style="DISPLAY:none"></asp:TextBox>
			<asp:TextBox ID="txtSortCharPurchase" Runat="server" style="DISPLAY:none"></asp:TextBox>
		</ContentTemplate>
	</asp:updatepanel>
		
		</form>
	</body>
</HTML>

--%>