Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Partial Class frmItemPriceRecommd : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not IsPostBack Then
                Dim objItems As New CItems
                objItems.ItemCode = GetQueryStringVal( "Itemcode")
                objItems.NoofUnits = IIf(GetQueryStringVal( "Unit") = "", 0, GetQueryStringVal( "Unit"))
                objItems.OppId = GetQueryStringVal( "OppID")
                objItems.mode = True
                dgItemPricemgmt.DataSource = objItems.GetPriceManagementList1
                dgItemPricemgmt.DataBind()
            End If
            btnClose.Attributes.Add("onclick", "return Close();")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Function ReturnName(ByVal bintCreatedDate) As String
        Try
            Dim strCreateDate As String = ""
            If Not IsDBNull(bintCreatedDate) Then strCreateDate = FormattedDateFromDate(bintCreatedDate, Session("DateFormat"))
            Return strCreateDate
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub dgItemPricemgmt_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgItemPricemgmt.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim lbl As Label
                lbl = e.Item.FindControl("lblLstPrice")
                Dim btnAdd As Button
                btnAdd = e.Item.FindControl("btnAdd")
                If GetQueryStringVal( "Type") = "Edit" Then
                    btnAdd.Attributes.Add("onclick", "return AccessParent('" & String.Format("{0:#,###.00}", CDec(lbl.Text)) & "','2','" & GetQueryStringVal( "txtName").Trim & "')")
                Else : btnAdd.Attributes.Add("onclick", "return AccessParent('" & String.Format("{0:#,###.00}", CDec(lbl.Text)) & "','1','')")
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class

