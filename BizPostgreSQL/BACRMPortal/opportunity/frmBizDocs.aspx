<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmBizDocs.aspx.vb"
    Inherits="BACRMPortal.frmBizDocs" %>

<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <title>BizDocs</title>

    <script language="javascript">
        function Close() {
            window.close();
            return false;
        }
        function AddBizDocs(a) {
            var str;
            str = '';
            var stri;
            for (i = 1; i < document.getElementById('dgBizDocs').rows.length; i++) {
                if (i < 10) {
                    stri = '0' + (i + 1)
                }
                else {
                    stri = i + 1
                }
                if (document.all) {
                    str = str + document.getElementById('dgBizDocs_ctl' + (stri) + '_lblBizDocID').innerText + ',';
                } else {
                    str = str + document.getElementById('dgBizDocs_ctl' + (stri) + '_lblBizDocID').textContent + ',';
                }
            }

            window.location.href = "frmAddBizDocs.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=" + a + "&BizDocIDs=" + str;
            return false;
        }
        function OpenBizInvoice(a, b) {
            window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=680,height=700,scrollbars=yes,resizable=yes');
            return false;
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function reDirect(a) {
            //document.location.href=a;
        }
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <br />
    <table cellspacing="1" cellpadding="1" width="100%" border="0">
        <tr>
            <td>
            </td>
            <td align="right">
                <asp:Button ID="btnAdd" runat="server" Text="Add BizDoc" CssClass="button"></asp:Button>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Table ID="tblMile" runat="server" BorderColor="black" GridLines="None" Width="100%"
                    CellSpacing="0" CssClass="aspTable" CellPadding="0" BorderWidth="1" Height="200px">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:DataGrid ID="dgBizDocs" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
                                BorderColor="white">
                                <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                <ItemStyle CssClass="is"></ItemStyle>
                                <HeaderStyle CssClass="hs"></HeaderStyle>
                                <Columns>
                                    <asp:TemplateColumn HeaderText="BizDoc">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hplBizdcoc" runat="server" NavigateUrl="" CssClass="hyperlink"
                                                Text='<%# DataBinder.Eval(Container.DataItem, "BizDoc") %>'>
                                            </asp:HyperLink>
                                            <asp:Label ID="lblBizDocID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "numBizDocId") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Date Created">
                                        <ItemTemplate>
                                            <%# ReturnName(DataBinder.Eval(Container.DataItem, "CreatedDate")) %>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn HeaderText="Opportunty/Deal Name" DataField="vcPOppName"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="ID.#" DataField="vcBizDocID"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="P.O." DataField="vcRefOrderNo"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Amount" DataFormatString="{0:#,##0.00}" DataField="monPAmount">
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtBizDocID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "numOppBizDocsId") %>'>
                                            </asp:TextBox>
                                            <asp:Button ID="btnDelete" runat="server" CssClass="Delete" Text="X" CommandName="Delete">
                                            </asp:Button>
                                            <asp:LinkButton ID="lnkDelete" runat="server" Visible="false">
														<font color="#730000">*</font></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
