
''Modified By Anoop Jayaraj
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Partial Class frmNewOpprtunity
    Inherits System.Web.UI.Page

    Protected WithEvents ddlSalesProcess As System.Web.UI.WebControls.DropDownList


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Dim tblItem As New Datatable
    Dim drowItem As DataRow
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            Dim objCommon As New CCommon
            objCommon.sb_FillComboFromDBwithSel(ddlSource, 9, Session("DomainID"))
            objCommon.sb_FillComboFromDBwithSel(ddlCampaign, 24, Session("DomainID"))
            objCommon.sb_FillItemFromDB(ddlItem, Session("DomainId"))
            binddatagrid()
            FillContact(ddlContacts)
        End If
        lblOpptComments.Text = ""
        btnSave.Attributes.Add("onclick", "return Save()")
        btnAdd.Attributes.Add("onclick", "return Add()")
        txtUnits.Attributes.Add("onkeypress", "CheckNumber(2)")
        txtPrice.Attributes.Add("onkeypress", "CheckNumber(1)")
        btnClose.Attributes.Add("onclick", "return Close()")
    End Sub




    Public Function FillContact(ByVal ddlCombo As DropDownList)
        Dim fillCombo As New COpportunities
        With fillCombo
            .DivisionID = Session("DivId")
            ddlCombo.DataSource = fillCombo.ListContact().tables(0).DefaultView()
            ddlCombo.DataTextField = "vcFirstName"
            ddlCombo.DataValueField = "numcontactId"
            ddlCombo.DataBind()
        End With
        ddlCombo.Items.Insert(0, New ListItem("--Select One--", "0"))
    End Function

    Public Sub SalesProcess(ByVal ddlCombo As DropDownList)
        Dim fillCombo As New COpportunities
        With fillCombo
            ddlCombo.DataSource = fillCombo.ListSource().tables(0).DefaultView
            ddlCombo.DataTextField = "slp_name"
            ddlCombo.DataValueField = "slp_id"
            ddlCombo.DataBind()
        End With
    End Sub
    Function binddatagrid()
        tblItem.Columns.Add("OppItemCode")
        tblItem.Columns.Add("ItemCode")
        tblItem.Columns.Add("ItemName")
        tblItem.Columns.Add("Unit")
        tblItem.Columns.Add("Price")
        tblItem.Columns.Add("Amount")
        Session("tblItem") = tblItem
        dtgItem.DataSource = Session("tblItem")
        dtgItem.DataBind()
    End Function

    Function AddNewItem()
        tblItem = Session("tblItem")
        drowItem = tblItem.NewRow
        drowItem("OppItemCode") = 0
        drowItem("ItemCode") = ddlItem.SelectedItem.Value
        drowItem("ItemName") = ddlItem.SelectedItem.Text
        drowItem("Unit") = Replace(txtUnits.Text, ",", "")
        drowItem("Price") = Replace(txtPrice.Text, ",", "")
        drowItem("Amount") = CInt(Replace(txtUnits.Text, ",", "")) * CInt(Replace(txtPrice.Text, ",", ""))
        tblItem.Rows.Add(drowItem)
        dtgItem.DataSource = tblItem
        dtgItem.DataBind()
        Session("tblItem") = tblItem
    End Function

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If dtgItem.Items.Count < 1 Then
            lblOpptComments.Text = "Enter Item"
            lblOpptComments.Visible = True
            Exit Sub
        End If
        'SaveOpportunity()
        Opportunity()
    End Sub

    Private Overloads Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If IsNumeric(txtUnits.Text) = False Then
            lblOpptComments.Text = "Enter Unit numeric"
            Exit Sub
        End If
        If IsNumeric(txtPrice.Text) = False Then
            lblOpptComments.Text = "Enter Price numeric"
            Exit Sub
        End If
        dtgItem.GridLines = GridLines.Vertical
        AddNewItem()
    End Sub



    Sub Opportunity()
        Dim arrOutPut() As String
        Dim objOpportunity As New MOpportunity(Session("UserContactID"))
        objOpportunity.OpportunityId = 0

        objOpportunity.ContactID = ddlContacts.SelectedItem.Value
        objOpportunity.DivisionID = Session("DivId")
        objOpportunity.OpportunityName = Session("CompName") & "-" & Format(Now(), "MMMM")
        objOpportunity.UserCntID = Session("UserContactID")
        objOpportunity.EstimatedCloseDate = Now
        objOpportunity.CampaignID = ddlCampaign.SelectedItem.Value
        objOpportunity.PublicFlag = 0
        objOpportunity.Source = ddlSource.SelectedItem.Value
        objOpportunity.DomainID = Session("DomainId")
        objOpportunity.OppType = ddlOppType.SelectedItem.Value
        If ddlOppType.SelectedItem.Value = 1 Then
            Session("OppType") = 1
        Else
            Session("OppType") = 2
        End If
        Dim dsNew As New DataSet
        Dim TotalAmount As Decimal
        If Not Session("tblItem") Is Nothing Then
            Dim dttable As New Datatable
            dttable = Session("tblItem")
            If dttable.Rows.Count > 0 Then
                Dim j As Integer
                For j = 0 To dttable.Rows.Count - 1
                    TotalAmount = TotalAmount + dttable.Rows(j).Item("Amount")
                Next
                dttable.TableName = "Table"
                dsNew.tables.Add(dttable.Copy)
                objOpportunity.strItems = dsNew.GetXml
                dsNew.tables.Remove(dsNew.tables(0))
            End If
        End If
        arrOutPut = objOpportunity.Save
        Session("OppID") = arrOutPut(0)
        If ddlOppType.SelectedItem.Value <> 1 Then
            Try


                ' When a Purchase Opportunity is created by a �non� employee, & when a Purchase Order is created by anyone. 
                Dim objAlerts As New CAlerts
                Dim dtDetails As DataTable
                objAlerts.AlertDTLID = 21 'Alert DTL ID for sending alerts in opportunities
                objAlerts.DomainID = Session("DomainID")
                dtDetails = objAlerts.GetIndAlertDTL
                If dtDetails.Rows.Count > 0 Then
                    If dtDetails.Rows(0).Item("tintAlertOn") = 1 Then
                        Dim dtEmailTemplate As DataTable
                        Dim objDocuments As New DocumentList
                        objDocuments.GenDocID = dtDetails.Rows(0).Item("numEmailTemplate")
                        objDocuments.DomainID = Session("DomainID")
                        dtEmailTemplate = objDocuments.GetDocByGenDocID
                        If dtEmailTemplate.Rows.Count > 0 Then
                            Dim objSendMail As New clsSendEmail
                            Dim dtMergeFields As New DataTable
                            Dim drNew As DataRow
                            dtMergeFields.Columns.Add("OppID")
                            dtMergeFields.Columns.Add("DealAmount")
                            dtMergeFields.Columns.Add("Organization")
                            dtMergeFields.Columns.Add("ContactName")
                            drNew = dtMergeFields.NewRow
                            drNew("OppID") = arrOutPut(1)
                            drNew("DealAmount") = TotalAmount
                            drNew("ContactName") = ddlContacts.SelectedItem.Text
                            drNew("Organization") = Session("CompName")
                            dtMergeFields.Rows.Add(drNew)

                            Dim dtEmail As DataTable
                            objAlerts.AlertDTLID = 21
                            objAlerts.DomainID = Session("DomainId")
                            dtEmail = objAlerts.GetAlertEmails
                            Dim strCC As String = ""
                            Dim p As Integer
                            For p = 0 To dtEmail.Rows.Count - 1
                                strCC = strCC & dtEmail.Rows(p).Item("vcEmailID") & ","
                            Next
                            strCC = strCC.TrimEnd(",")
                            objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), "", ConfigurationManager.AppSettings("FromAddress"), strCC, dtMergeFields)
                        End If
                    End If
                End If
            Catch ex As Exception

            End Try
        End If
        Dim strScript As String = "<script language=JavaScript>"
        strScript += "window.opener.reDirect('../Opportunities/frmOpportunities.aspx'); self.close();"
        strScript += "</script>"
        If (Not Page.IsStartupScriptRegistered("clientScript")) Then
            Page.RegisterStartupScript("clientScript", strScript)
        End If
    End Sub

    Private Sub dtgItem_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dtgItem.DeleteCommand
        Try
            Dim RowNo As Integer
            Dim tblItem As DataTable = Session("tblItem")
            RowNo = e.Item.ItemIndex
            tblItem.Rows.RemoveAt(RowNo)
            Session("tblItem") = tblItem
            dtgItem.DataSource = tblItem
            dtgItem.DataBind()
        Catch ex As Exception

        End Try
    End Sub
End Class
