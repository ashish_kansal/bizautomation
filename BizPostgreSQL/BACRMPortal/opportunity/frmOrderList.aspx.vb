Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item

Partial Class frmOrderList : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Dim strColumn As String
    
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not IsPostBack Then
                Dim objCommon As New CCommon
                m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmOrderList.aspx", Session("UserContactID"), 15, 6)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../Common/frmAuthorization.aspx")
                txtCurrrentPage.Text = 1
                BindDatagrid()
                Dim objItems As New CItems
                Dim lngCredit As Long
                objItems.DivisionID = Session("DivId")
                lngCredit = objItems.GetCreditStatusofCompany
                Dim dtTable As DataTable
                dtTable = objItems.GetAmountDue
                lblBalDue.Text = String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("AmountDueSO"))
                lblRemCredit.Text = String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("RemainingCredit"))
                lblAmtPastDue.Text = String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("AmountPastDueSO"))
            End If
            If txtSortChar.Text <> "" Then
                ViewState("SortChar") = txtSortChar.Text
                ViewState("Column") = "BizDocName"
                Session("Asc") = 0
                BindDatagrid()
                txtSortChar.Text = ""
            End If
            'btnNew.Attributes.Add("onclick", "return goto('../Order/frmOrder.aspx','cntOpenItem1','divOrder');")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindDatagrid()
        Try
            Dim dtOpportunity As DataTable
            Dim objOpportunity As New COpportunities
            Dim SortChar As Char
            If ViewState("SortChar") <> "" Then
                SortChar = ViewState("SortChar")
            Else : SortChar = "0"
            End If
            With objOpportunity
                .UserCntID = Session("UserContactID")
                .DomainID = Session("DomainID")
                .SortCharacter = SortChar
                .DivisionID = Session("DivID")
                If txtCurrrentPage.Text.Trim <> "" Then
                    .CurrentPage = txtCurrrentPage.Text
                Else : .CurrentPage = 1
                End If
                .PageSize = Session("PagingRows")
                .TotalRecords = 0
                If ViewState("Column") <> "" Then
                    .columnName = ViewState("Column")
                Else : .columnName = "dtCreatedDate"
                End If
                If Session("Asc") = 1 Then
                    .columnSortOrder = "Asc"
                Else : .columnSortOrder = "Desc"
                End If
                .OppType = 1
                .bitflag = True ' open orders
            End With
            dtOpportunity = objOpportunity.GetInvoiceList
            If objOpportunity.TotalRecords = 0 Then
                hidenav.Visible = False
            Else
                hidenav.Visible = True
                lblRecordCount.Text = String.Format("{0:#,###}", objOpportunity.TotalRecords)
                Dim strTotalPage As String()
                Dim decTotalPage As Decimal
                decTotalPage = lblRecordCount.Text / Session("PagingRows")
                decTotalPage = Math.Round(decTotalPage, 2)
                strTotalPage = CStr(decTotalPage).Split(".")
                If (lblRecordCount.Text Mod Session("PagingRows")) = 0 Then
                    lblTotal.Text = strTotalPage(0)
                    txtTotalPage.Text = strTotalPage(0)
                Else
                    lblTotal.Text = strTotalPage(0) + 1
                    txtTotalPage.Text = strTotalPage(0) + 1
                End If
                txtTotalRecords.Text = lblRecordCount.Text
            End If
            dgOpportunity.DataSource = dtOpportunity
            dgOpportunity.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub lnkLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkLast.Click
        Try
            txtCurrrentPage.Text = txtTotalPage.Text
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkPrevious.Click
        Try
            If txtCurrrentPage.Text = 1 Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text - 1
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkFirst.Click
        Try
            txtCurrrentPage.Text = 1
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkPrevious.Click, lnkNext.Click
        Try
            If txtCurrrentPage.Text = txtTotalPage.Text Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 1
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk2.Click
        Try
            If txtCurrrentPage.Text + 1 = txtTotalPage.Text Or txtCurrrentPage.Text + 1 > txtTotalPage.Text Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 2
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk3.Click
        Try
            If txtCurrrentPage.Text + 2 = txtTotalPage.Text Or txtCurrrentPage.Text + 2 > txtTotalPage.Text Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 3
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk4.Click
        Try
            If txtCurrrentPage.Text + 3 = txtTotalPage.Text Or txtCurrrentPage.Text + 3 > txtTotalPage.Text Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 4
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk5.Click
        Try
            If txtCurrrentPage.Text + 4 = txtTotalPage.Text Or txtCurrrentPage.Text + 4 > txtTotalPage.Text Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 5
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub txtCurrrentPage_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCurrrentPage.TextChanged
        Try
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Function ReturnName(ByVal bintCreatedDate As Date) As String
        Try
            Dim strCreateDate As String
            strCreateDate = FormattedDateFromDate(bintCreatedDate, Session("DateFormat"))
            Return strCreateDate
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function ReturnDateTime(ByVal CloseDate) As String
        Try
            Dim strTargetResolveDate As String = ""
            Dim temp As String = ""
            If Not IsDBNull(CloseDate) Then
                strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))

                Dim timePart As String = CloseDate.ToShortTimeString.Substring(0, CloseDate.ToShortTimeString.Length - 1)
                ' remove gaps
                If timePart.Split(" ").Length >= 2 Then timePart = timePart.Split(" ").GetValue(0) + timePart.Split(" ").GetValue(1)

                ' check Today date components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                ' if both are same it is today
                If (CloseDate.Date = Now.Date And CloseDate.Month = Now.Month And CloseDate.Year = Now.Year) Then
                    strTargetResolveDate = "<font color=red><b>Today</b></font>"
                    Return strTargetResolveDate

                    ' check [ TodayDate + 1 ] Date.... components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                    ' if both are same it was Yesterday
                ElseIf (CloseDate.Date.AddDays(1).Date = Now.Date And CloseDate.AddDays(1).Month = Now.Month And CloseDate.AddDays(1).Year = Now.Year) Then
                    strTargetResolveDate = "<font color=purple><b>Yesterday</b></font>"
                    Return strTargetResolveDate

                    ' check TodayDate .... components [ Date , Month , Year ] with Parameter [ CloseDate + 1 ] Date....  components [ Date , Month , Year ] 
                    ' if both are same it will Tomorrow
                ElseIf (CloseDate.Date = Now.AddDays(1).Date And CloseDate.Month = Now.AddDays(1).Month And CloseDate.Year = Now.AddDays(1).Year) Then
                    temp = CloseDate.Hour.ToString + ":" + CloseDate.Minute.ToString
                    strTargetResolveDate = "<font color=orange><b>Tomorrow</b></font>"
                    Return strTargetResolveDate

                    ' display day name for next 4 days from now
                ElseIf Format(CloseDate, "yyyyMMdd") = Format(DateTime.Now.AddDays(2), "yyyyMMdd") Or Format(CloseDate, "yyyyMMdd") = Format(DateTime.Now.AddDays(3), "yyyyMMdd") Or Format(CloseDate, "yyyyMMdd") = Format(DateTime.Now.AddDays(4), "yyyyMMdd") Or Format(CloseDate, "yyyyMMdd") = Format(DateTime.Now.AddDays(5), "yyyyMMdd") Then
                    strTargetResolveDate = "<b>" & CloseDate.DayOfWeek.ToString & "</b>"
                    Return strTargetResolveDate
                Else
                    strTargetResolveDate = strTargetResolveDate
                    Return strTargetResolveDate
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function ReturnMoney(ByVal Money)
        Try
            Return Math.Round(Money, 2)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub dgOpportunity_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgOpportunity.SortCommand
        Try
            strColumn = e.SortExpression.ToString()
            If ViewState("Column") <> strColumn Then
                ViewState("Column") = strColumn
                Session("Asc") = 0
            Else
                If Session("Asc") = 0 Then
                    Session("Asc") = 1
                Else : Session("Asc") = 0
                End If
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgOpportunity_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgOpportunity.ItemCommand
        Try
            Dim OpporID As Long
            If Not e.CommandName = "Sort" Then OpporID = e.Item.Cells(0).Text()
            If e.CommandName = "Customer" Then
                Response.Redirect("../common/frmAccounts.aspx")
            ElseIf e.CommandName = "Contact" Then
                Dim objCommon As New CCommon
                objCommon.OppID = OpporID
                objCommon.charModule = "O"
                objCommon.GetCompanySpecificValues1()
                Session("ContactId") = objCommon.ContactID
                Response.Redirect("../common/frmContacts.aspx")
            ElseIf e.CommandName = "Name" Then
                Session("OppID") = OpporID
                Response.Redirect("../Opportunities/frmOpportunities.aspx?frm=Orderlist")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgOpportunity_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgOpportunity.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim lngGrandTotal, lngAmtPaid As Decimal
                lngGrandTotal = e.Item.Cells(7).Text
                lngAmtPaid = e.Item.Cells(8).Text
                e.Item.Cells(9).Text = String.Format("{0:#,##0.00}", lngGrandTotal - lngAmtPaid)
                'e.Item.Cells(2).Attributes.Add("onclick", "return OpenBizInvoice('" & e.Item.Cells(0).Text & "','" & e.Item.Cells(1).Text & "','" & ConfigurationManager.AppSettings("PortalURL").Replace("Login.aspx", "").Replace("http://", "https://") & "')")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

   

    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ibtnPayNow_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnPayNow.Click
        Try

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class
