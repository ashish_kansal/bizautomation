<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCreateSalesPurFromOpp.aspx.vb" Inherits="BACRMPortal.frmCreateSalesPurFromOpp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server" enableviewstate="true" >
    <title id="titCreateOpp" runat="server"></title>
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <script type="text/javascript"  language="javascript" >
        function Close()
        {
            window.close()
            return false;
        }
        function Save()
		{
		   

			if ((document.form1.ddlCompany.selectedIndex==-1 )||(document.form1.ddlCompany.value==0))
			{
				alert("Select Company")
				document.form1.txtCompName.focus();
				return false;
			}
			if ((document.form1.ddlContact.selectedIndex==-1 )||(document.form1.ddlContact.value==0 ))
			{
				alert("Select Contact")
				document.form1.ddlContact.focus();
				return false;
			}
			 for(var i=2; i<=dgItem.rows.length; i++)
			{
			    var str;
			    if (i<10)
			    {
			        str='0'+i;
			        
			    } 
			    else
			    {
			        str=i;
			    }
			    if (document.form1.all['dgItem_ctl'+str+'_chkSelect'].checked==true)
			    {
			        if (document.form1.all['dgItem_ctl'+str+'_txtPrice'].value=="")
			        {
			            		alert("Enter Price")
				                document.form1.all['dgItem_ctl'+str+'_txtPrice'].focus();
				                return false;
			        }
			    }
			}
			
		}
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <br />
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                 <table class="TabStyle">
		            <tr>
			            <td>&nbsp;&nbsp;&nbsp;<asp:Label ID="lblHeading" runat="server" ></asp:Label>&nbsp;&nbsp;&nbsp;
			            </td>
		            </tr>
	            </table>
            </td>
            <td align="right" >
            <asp:Button  ID="btnSave" runat="server" CssClass="button" Text="Save & Close" />
            <asp:Button  ID="btnClose" runat="server" CssClass="button" Text="Close" />
           </td>
        </tr>
    </table>
   
	<asp:table id="tblOppr" CellPadding="0" CellSpacing="0" BorderWidth="1" Runat="server" CssClass="aspTable"
				            Width="100%" BorderColor="black" GridLines="None">
				            <asp:TableRow>
					            <asp:TableCell VerticalAlign="Top" >
					                    <asp:datagrid id="dgItem" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False" BorderColor="white">
					                            <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
					                            <ItemStyle CssClass="is"></ItemStyle>
					                            <HeaderStyle CssClass="hs"></HeaderStyle>
						                            <Columns>
						                                <asp:TemplateColumn>
						                                    <ItemTemplate>
						                                        <asp:CheckBox ID="chkSelect" runat="server" /> 
						                                        <asp:Label ID="lblNewOppID" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "numoppIDNew") %>'  style="display:none"></asp:Label>
						                                        <asp:Label ID="lblOppItemID" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "numoppitemtCode") %>'  style="display:none"></asp:Label>
						                                        <asp:Label ID="lblItemID" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "numItemCode") %>'  style="display:none"></asp:Label>
						                                        <asp:Label ID="lblWareHouseItemID" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "numWarehouseItmsID") %>'  style="display:none"></asp:Label>
						                                        
						                                        <asp:Label ID="lblItemDesc" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "vcItemDesc") %>'  style="display:none"></asp:Label>
						                                    </ItemTemplate>
						                                </asp:TemplateColumn>
						                                <asp:BoundColumn  DataField="vcItemName"  HeaderText="Item">
						                                </asp:BoundColumn>
						                                <asp:BoundColumn  DataField="ItemType"  HeaderText="Type">
						                                </asp:BoundColumn>
						                                 <asp:BoundColumn  DataField="ItemType"  HeaderText="Type">
						                                </asp:BoundColumn>
						                                 <asp:BoundColumn  DataField="numUnitHour"  HeaderText="Unit/Hours">
						                                </asp:BoundColumn>
						                                 <asp:BoundColumn  DataField="vcPOppName"  HeaderText="Record Location">
						                                </asp:BoundColumn>
						                                  <asp:BoundColumn  DataField="vcCompanyName"  HeaderText="Customer">
						                                </asp:BoundColumn>
						                                 <asp:TemplateColumn HeaderText="Price">
						                                    <ItemTemplate>
						                                        <asp:TextBox ID="txtPrice" CssClass="signup" runat="Server"  Text='<%# DataBinder.Eval(Container.DataItem, "NewPrice") %>'></asp:TextBox>
						                                        <asp:HyperLink ID="hplSuggest" runat="server" Text="Suggest Price" CssClass="hyperlink" ></asp:HyperLink>   
						                                     </ItemTemplate>
						                                </asp:TemplateColumn>
						                            </Columns>
				                            </asp:datagrid>
				                            <br />
				                            <br />
					            <table align="center" >
                                    <tr>
                                        <td colspan="4">
                                    
                                        </td>
                                    </tr>
        	                            <tr>
				                            <td class="text" align="right" nowrap>Customer or Vendor<FONT color="#ff3333"> *</FONT></td>
				                            <td>
					                            <asp:textbox id="txtCompName" Runat="server" width="200" cssclass="signup"></asp:textbox>&nbsp;
				                            </td>
				                            <td align="center" >
				                                <asp:Button ID="btnGo" Runat="server" Text="Go" Width="25" CssClass="button"></asp:Button>&nbsp;
				                            </td>
				                            <td>
				                                <asp:dropdownlist id="ddlCompany" Runat="server" Width="200" AutoPostBack="True" CssClass="signup"></asp:dropdownlist>
				                            </td>
		                            </tr>
		                             <tr>
		                                <td class="text" align="right">
		                                    Contact
		                                </td>
		                                 <td colspan="3">
				                                <asp:dropdownlist id="ddlContact" Runat="server" Width="300" CssClass="signup"></asp:dropdownlist>
				                          </td>
		                            </tr>
		                            <tr>
		                                <td class="text" align="right">
		                                    Opportunity
		                                </td>
		                                 <td colspan="3">
				                                <asp:dropdownlist id="ddlOpportunity" Runat="server" Width="300" CssClass="signup"></asp:dropdownlist>
				                          </td>
		                            </tr>
		                            <tr>
                            		    
		                            </tr>
                                    <tr>
                                        <td valign="top" class="text" align="right">
                                        <br />
                                            Bill To Address
                                        </td>
                                        <td class="text" >
                                            <asp:RadioButton ID="radBillEmployer" AutoPostBack="true"  GroupName="rad1" runat="server" Text="Employer" /><br />
                                            <asp:RadioButton ID="radBillCustomer" AutoPostBack="true"  GroupName="rad1" runat="server" Text="Customer" /><br />
                                            <asp:RadioButton ID="radBillOther" AutoPostBack="true"  GroupName="rad1" runat="server" Text="Other Location" />
                                      
                                        </td>
                                        <td valign="top" class="text" align="right">
                                        <br />
                                            Ship To Address
                                        </td>
                                        <td class="text" >
                                            <asp:RadioButton ID="radShipEmployer" AutoPostBack="true"  GroupName="rad2" runat="server" Text="Employer" /><br />
                                            <asp:RadioButton ID="radShipCustomer" AutoPostBack="true"  GroupName="rad2" runat="server" Text="Customer" /><br />
                                            <asp:RadioButton ID="radShipOther" AutoPostBack="true"  GroupName="rad2" runat="server" Text="Other Location" />
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                              <table id="tblBillToAddress" runat="server" >
                                                <tr>
                                                    <td class="text" align="right">Street</td>
				                                    <td><asp:textbox id="txtStreet" tabIndex="10" runat="server" cssclass="signup" textMode=MultiLine  width="200"></asp:textbox></td>
				                                </tr>
				                                <tr>
				                                    <td class="text" align="right">City</td>
					                                <td><asp:textbox id="txtCity" tabIndex="11" runat="server" cssclass="signup" width="200"></asp:textbox></td>
                                                </tr>
                                                <tr>
                                                    <td class="text" align="right">State</td>
					                                <td>
					                                    <asp:DropDownList ID="ddlBillState" Runat="server" tabIndex="12" Width="208" CssClass="signup">
						                                </asp:DropDownList>
				                                    </td>
                                                </tr>
                                                 <tr>
                                                    <td class="text" align="right">Postal</td>
					                                <td><asp:textbox id="txtPostal" tabIndex="13" runat="server" cssclass="signup" width="200" MaxLength="8"></asp:textbox></td>
                                								
                                                </tr>
                                                 <tr>
                                                    <td class="text" align="right">Country</td>
					                                <td>
					                                    <asp:DropDownList ID="ddlBillCountry" tabIndex="14" AutoPostBack=True  Runat="server" Width="208" CssClass="signup">
						                                </asp:DropDownList>
					                                </td>
                                                </tr>
                                             </table> 
                                        </td>
                                        <td colspan="2">
                                            <table id="tblShipToAddress" runat="server" >
                                                    <tr>
                                                              <td class="text" align="right">Street</td>
								                                <td class="text"><asp:textbox id="txtShipStreet" tabIndex="15" runat="server" textMode=MultiLine  cssclass="signup" width="200"></asp:textbox></td>
                                							
                                                    </tr>
                                                     <tr>
                            	                                <td class="text" align="right">City</td>
								                                <td class="text"><asp:textbox id="txtShipCity" tabIndex="16" runat="server" cssclass="signup" width="200"></asp:textbox></td>
                                							
                                                    </tr>
                                                     <tr>
                                                            <td class="text" align="right">State</td>
								                                <td class="text"><asp:DropDownList ID="ddlShipState" tabIndex="17" Runat="server" Width="208" CssClass="signup">
								                                </asp:DropDownList></td>
                                                    </tr>
                                                     <tr>
                            	                                <td class="text" align="right">Postal</td>
								                                <td class="text"><asp:textbox id="txtShipPostal" tabIndex="18" runat="server" cssclass="signup" width="200" MaxLength="8"></asp:textbox></td>
                                							
                                                    </tr>
                                                     <tr>
                                                            <td class="text" align="right">Country</td>
								                                <td class="text">
								                                    <asp:DropDownList ID="ddlShipCountry" tabIndex="19" AutoPostBack=True  Runat="server" Width="208" CssClass="signup"></asp:DropDownList>
								                                </td>
                                                    </tr>
		                                        </table>
                                        </td>
                                    </tr>
                                   </table>
                                   <br />
					            </asp:TableCell>
					       </asp:TableRow>
	            </asp:table>
    
    </form>
</body>
</html>
