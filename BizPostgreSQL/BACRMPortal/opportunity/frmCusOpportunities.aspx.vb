'***************************************************************************************************************************
'     Author Name				 :  Anoop Jayaraj
'     Date Written				 :  18/2/2005
'***************************************************************************************************************************
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports system.Reflection
Imports BACRM.BusinessLogic.Item
Imports Infragistics.WebUI.UltraWebGrid
Imports Infragistics.WebUI.UltraWebTab
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Outlook

Partial Public Class frmCusOpportunities : Inherits BACRMPage

    Dim objContacts As CContacts
    Dim objOpportunity As MOpportunity
    Dim objCommon As New CCommon
    Dim objCus As CustomFields
    Dim objItems As CItems
    Dim dsTemp As DataSet
    Dim ds As DataSet
    Dim dtAssignto, dtET, dtActivity, dtEvent, dtTime, dtRemider, dtStartDate, dtActType As DataTable
    Dim dtOppAtributes As DataTable
    Dim dtOppOptAttributes As DataTable
    Dim strValues As String
    Dim SI As Integer = 0
    Dim SI1 As Integer = 0
    Dim SI2 As Integer = 0
    Dim frm As String = ""
    Dim frm1 As String = ""
    Dim frm2 As String = ""
    
    Dim myRow As DataRow
    Dim arrOutPut() As String = New String(2) {}
    Dim lngOppId As Long
    Dim lngDivId As Long
    Dim str As String = ""
    Dim dtStageStatus As DataTable
    Dim objOutLook As COutlook
    Dim objActionItem As ActionItem

End Class