Imports BACRM.BusinessLogic.TimeAndExpense
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contract

Public Class frmOppTime : Inherits BACRMPage

    Dim numCategoryHDRID As Long
    Dim numOppid As Long
    Dim numStageId As Long
    Dim DivId As Long

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddlBizDoc As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtRate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtHour As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlMin As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtDesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnSaveClose As System.Web.UI.WebControls.Button
    Protected WithEvents btnCancel As System.Web.UI.WebControls.Button
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            numCategoryHDRID = GetQueryStringVal( "numCategoryHDRID")
            numOppid = GetQueryStringVal( "OpID")
            numStageId = GetQueryStringVal( "OPPStageID")
            DivId = GetQueryStringVal( "DivId")
            If Not IsPostBack Then
                LoadContractsInfo(DivId)
                getdetails()
                If ddlContract.SelectedValue <> "0" Then
                    txtRate.Enabled = False
                Else : txtRate.Enabled = True
                End If
                ViewState("ContractId") = ddlContract.SelectedValue
            End If
            If GetQueryStringVal( "frm") <> "TE" Then btnCancel.Attributes.Add("onclick", "return Close();")
            btnSaveClose.Attributes.Add("onclick", "return Save();")
            hplRec.Attributes.Add("onclick", "return OpenRec(" & DivId & ")")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub getdetails()
        Try
            'Dim objOppTimeExpense As New OppTimeExpense(Session("UserContactID"))
            'Dim dtTimeDetails As DataTable
            'objOppTimeExpense.OppID = GetQueryStringVal( "Opid")
            'objOppTimeExpense.OppStageID = GetQueryStringVal( "OPPStageID")
            'objOppTimeExpense.CurrentUserId = Session("UsercontactId")
            'objOppTimeExpense.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            'dtTimeDetails = objOppTimeExpense.GetTimeDetails
            Dim objTimeExp As New TimeExpenseLeave
            Dim dtTimeDetails As DataTable

            objTimeExp.UserCntID = Session("UserContactId")
            objTimeExp.DomainID = Session("DomainID")
            objTimeExp.OppID = numOppid
            objTimeExp.StageId = numStageId
            objTimeExp.TEType = 1
            objTimeExp.CategoryID = 1
            objTimeExp.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            dtTimeDetails = objTimeExp.GetTimeAndExpDetails
            If dtTimeDetails.Rows.Count <> 0 Then
                txtCategoryHDRID.Text = dtTimeDetails.Rows(0).Item("numCategoryHDRID")
                txtRate.Text = String.Format("{0:#,##0.00}", dtTimeDetails.Rows(0).Item("monAmount"))
                txtDesc.Text = IIf(IsDBNull(dtTimeDetails.Rows(0).Item("txtDesc")), "", dtTimeDetails.Rows(0).Item("txtDesc"))
                radBill.Checked = IIf(dtTimeDetails.Rows(0).Item("numType") = 1, True, False)
                If radBill.Checked = False Then
                    radNonBill.Checked = True
                    pnlRate.Visible = False
                End If
                If Not IsDBNull(dtTimeDetails.Rows(0).Item("dtFromDate")) Then
                    calFrom.SelectedDate = dtTimeDetails.Rows(0).Item("dtFromDate")
                Else : calFrom.SelectedDate = Now
                End If

                If Not IsDBNull(dtTimeDetails.Rows(0).Item("dtToDate")) Then
                    calto.SelectedDate = dtTimeDetails.Rows(0).Item("dtToDate")
                Else : calto.SelectedDate = Now.AddMinutes(30)
                End If

                ddltime.SelectedItem.Selected = False
                If Not ddltime.Items.FindByText(Format(dtTimeDetails.Rows(0).Item("dtFromDate"), "h:mm")) Is Nothing Then
                    ddltime.Items.FindByText(Format(dtTimeDetails.Rows(0).Item("dtFromDate"), "h:mm")).Selected = True
                End If
                ddlEndTime.SelectedItem.Selected = False
                If Not ddlEndTime.Items.FindByText(Format(dtTimeDetails.Rows(0).Item("dtToDate"), "h:mm")) Is Nothing Then
                    ddlEndTime.Items.FindByText(Format(dtTimeDetails.Rows(0).Item("dtToDate"), "h:mm")).Selected = True
                End If
                If Format(dtTimeDetails.Rows(0).Item("dtFromDate"), "tt") = "AM" Then
                    chkAM.Checked = True
                Else : chkPM.Checked = True
                End If
                If Format(dtTimeDetails.Rows(0).Item("dtToDate"), "tt") = "AM" Then
                    chkEndAM.Checked = True
                Else : chkEndPM.Checked = True
                End If
                If Not dtTimeDetails.Rows(0).Item("numContractId") = 0 Then
                    ddlContract.Items.FindByValue(dtTimeDetails.Rows(0).Item("numContractId")).Selected = True
                    BindContractinfo()
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub radBill_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radBill.CheckedChanged
        Try
            If radBill.Checked = True Then
                pnlRate.Visible = True
            Else : pnlRate.Visible = False
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub radNonBill_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radNonBill.CheckedChanged
        Try
            If radBill.Checked = True Then
                pnlRate.Visible = True
            Else : pnlRate.Visible = False
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub saveDetails()
        Try
            Dim strStartTime, strEndTime, strStartDate, strEndDate As String
            strStartDate = calFrom.SelectedDate
            strEndDate = calto.SelectedDate
            strStartTime = strStartDate.Trim & " " & ddltime.SelectedItem.Text.Trim & ":00" & IIf(chkAM.Checked = True, " AM", " PM")
            strEndTime = strEndDate.Trim & " " & ddlEndTime.SelectedItem.Text.Trim & ":00" & IIf(chkEndAM.Checked = True, " AM", " PM")
            Dim strSplitStartTimeDate, strSplitEndTimeDate As Date
            strSplitStartTimeDate = CType(strStartTime, DateTime).AddMinutes(Session("ClientMachineUTCTimeOffset"))
            strSplitEndTimeDate = CType(strEndTime, DateTime).AddMinutes(Session("ClientMachineUTCTimeOffset"))

            If strSplitStartTimeDate > strSplitEndTimeDate Then
                litMessage.Text = "Start Time Cannot be Greater Than End Time"
                Exit Sub
            End If
            If radBill.Checked = True And ddlContract.SelectedValue <> 0 Then

                Dim hours As Decimal
                hours = DateDiff(DateInterval.Minute, strSplitStartTimeDate, strSplitEndTimeDate) * 0.0167
                If lblRemHours.Text = "-" Then
                    litMessage.Text = "Cannot Save as Contract Hours Not Sufficient"
                    Exit Sub
                Else
                    If hours > lblRemHours.Text Then
                        litMessage.Text = "Cannot Save as Contract Hours Not Sufficient"
                        Exit Sub
                    End If
                End If
            End If

            'Dim objOppTimeExpense As New OppTimeExpense
            'objOppTimeExpense.OppID = GetQueryStringVal( "Opid")
            'objOppTimeExpense.OppStageID = GetQueryStringVal( "OPPStageID")
            'objOppTimeExpense.Rate = txtRate.Text

            'objOppTimeExpense.Desc = txtDesc.Text
            'objOppTimeExpense.ItemID = IIf(txtItemID.Text = "", 0, txtItemID.Text)
            'objOppTimeExpense.Billable = radBill.Checked

            'If radBill.Checked Then
            '    objOppTimeExpense.ContractID = ddlContract.SelectedValue
            'Else
            '    objOppTimeExpense.ContractID = 0
            'End If
            'objOppTimeExpense.CurrentUserId = Session("UserContactId")
            'objOppTimeExpense.FromDate = strSplitStartTimeDate
            'objOppTimeExpense.ToDate = strSplitEndTimeDate
            'objOppTimeExpense.SaveTimeDetails()
            Dim objTimeExp As New TimeExpenseLeave
            objTimeExp.CategoryHDRID = txtCategoryHDRID.Text
            objTimeExp.DivisionID = DivId
            objTimeExp.CategoryID = 1
            objTimeExp.CategoryType = IIf(radBill.Checked, 1, 2)
            If radBill.Checked Then
                objTimeExp.ContractID = ddlContract.SelectedItem.Value
                objTimeExp.Amount = txtRate.Text.Trim
            Else
                objTimeExp.ContractID = 0
                objTimeExp.Amount = 0
            End If

            objTimeExp.FromDate = strSplitStartTimeDate
            objTimeExp.ToDate = strSplitEndTimeDate
            objTimeExp.ProID = 0
            objTimeExp.OppID = numOppid
            objTimeExp.StageId = numStageId
            objTimeExp.CaseID = 0
            objTimeExp.OppBizDocsId = 0
            objTimeExp.Desc = txtDesc.Text.Trim
            objTimeExp.UserCntID = Session("UserContactId")
            objTimeExp.DomainID = Session("DomainID")
            objTimeExp.TEType = 1
            objTimeExp.ManageTimeAndExpense()

            If GetQueryStringVal( "frm") = "TE" Then
                Response.Redirect("../TimeAndExpense/frmDaydetails.aspx?Date=" & GetQueryStringVal( "Date") & "&CntID=" & GetQueryStringVal( "CntID"))
            Else : Response.Write("<script>window.close();</script>")
            End If
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            saveDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btndelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelete.Click
        Try
            'Dim objOppTimeExpense As New OppTimeExpense
            'objOppTimeExpense.OppID = GetQueryStringVal( "Opid")
            'objOppTimeExpense.OppStageID = GetQueryStringVal( "OPPStageID")
            'objOppTimeExpense.CurrentUserId = Session("UsercontactId")
            'objOppTimeExpense.DeleteTimeDetails()
            Dim objTimeAndExp As New TimeExpenseLeave
            objTimeAndExp.CategoryHDRID = txtCategoryHDRID.Text
            objTimeAndExp.DomainID = Session("DomainId")
            objTimeAndExp.DeleteTimeExpLeave()
            If GetQueryStringVal( "frm") = "TE" Then
                Response.Redirect("../TimeAndExpense/frmDaydetails.aspx?Date=" & GetQueryStringVal( "Date") & "&CntID=" & GetQueryStringVal( "CntID"))
            Else : Response.Write("<script>window.close();</script>")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadContractsInfo(ByVal intDivId As Integer)
        Try
            Dim objContract As New CContracts
            Dim dtTable As DataTable
            objContract.DivisionId = intDivId
            objContract.UserCntId = Session("UserContactId")
            objContract.DomainId = Session("DomainId")
            dtTable = objContract.GetContractDdlList()
            ddlContract.DataSource = dtTable

            ddlContract.DataTextField = "vcContractName"
            ddlContract.DataValueField = "numcontractId"
            ddlContract.DataBind()
            ddlContract.Items.Insert(0, "--Select One--")
            ddlContract.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlContract_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlContract.SelectedIndexChanged
        Try
            BindContractinfo()
            If ddlContract.SelectedValue <> "0" Then
                txtRate.Enabled = False
            Else : txtRate.Enabled = True
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindContractinfo()
        Try
            Dim objContract As New CContracts
            Dim dtTable As DataTable
            objContract.ContractID = ddlContract.SelectedValue
            objContract.CategoryHDRID = txtCategoryHDRID.Text
            objContract.DomainId = Session("DomainId")
            dtTable = objContract.GetContractHrsAmount()
            If dtTable.Rows.Count > 0 Then
                lblRemHours.Text = IIf(dtTable.Rows(0).Item("bitHour"), String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("RemHours")), "-")
                txtRate.Text = dtTable.Rows(0).Item("decRate")
            Else
                lblRemHours.Text = ""
                txtRate.Text = ""
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            If GetQueryStringVal( "frm") = "TE" Then
                Response.Redirect("../TimeAndExpense/frmDaydetails.aspx?Date=" & GetQueryStringVal( "Date") & "&CntID=" & GetQueryStringVal( "CntID"))
            Else : Response.Write("<script>window.close();</script>")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class

