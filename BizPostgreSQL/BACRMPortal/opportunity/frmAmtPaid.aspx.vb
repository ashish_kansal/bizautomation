'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Accounting
Partial Class frmAmtPaid : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Dim objCommon As CCommon
    Dim lngOppBizDocID, lngOppID, lngContactId As Long
    Dim CurrentPageIndex As Integer
    Dim ResponseMessage As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            CurrentPageIndex = GetQueryStringVal( "Page")
            If Not IsPostBack Then
                If objCommon Is Nothing Then objCommon = New CCommon
                objCommon.sb_FillComboFromDBwithSel(ddlPayment, 31, Session("DomainID"))
                ddlPayment.Items.FindByValue("1").Selected = True
                BindCardType()
                BindDatagrid()
            End If
            btnCancel.Attributes.Add("onclick", "return WindowClose()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Dim TransactionCharge As Double
    Dim TransactionChargeAccountID As Long
    Dim TransactionChargeBareBy As Short = 0 ' default by employer
    Dim UndepositedFundAccountID As Long
    Dim lngCustomerARAccount As Long
    Dim lngJournalID As Long
    Dim AmountToPay As Decimal
    Private Sub btnSaveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            Dim objOppInvoice As New OppInvoice()
            Dim objOppBizDocs As New OppBizDocs
            If objCommon Is Nothing Then objCommon = New CCommon

            Dim dtTable As DataTable
            objCommon.DomainID = Session("DomainID")
            dtTable = objCommon.GetPaymentGatewayDetails
            If dtTable.Rows.Count = 0 Then
                litMessage.Text = "Please Configure Payment Gateway"
                Exit Sub
            Else
                Dim objAuthoritativeBizDocs As New AuthoritativeBizDocs
                'validate Card Type Mapping 
                'Get transaction charge for selected credit card type 
                Dim lShouldReturn As Boolean
                ValidateCardTypeMapping(TransactionCharge, TransactionChargeAccountID, lShouldReturn)
                If lShouldReturn Then
                    Exit Sub
                End If
                UndepositedFundAccountID = ChartOfAccounting.GetDefaultAccount("UF", Session("DomainID"))
                If Not UndepositedFundAccountID > 0 Then
                    litMessage.Text = "Please Map Default Undeposited Funds Account for Your Company from Administration->Global Settings->Accounting->Default Accounts."
                    Exit Sub
                End If
                lngCustomerARAccount = objOppBizDocs.ValidateCustomerAR_APAccounts("AR", Session("DomainID"), Session("DivID"))
                If lngCustomerARAccount = 0 Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set AR and AP Relationship from Administration->Global Settings->Accounting->Accounts for RelationShip To Save Amount' );", True)
                    Exit Sub
                End If

                'Charge Total amount to credit card
                'Update Amount paid for each invoice
                'Create journal entries for each amount paid
                Dim responseCode As String = ""
                Dim ReturnTransactionID As String = ""
                Dim objPaymentGateway As New PaymentGateway
                objPaymentGateway.DomainID = Session("DomainID")

                If objPaymentGateway.GatewayTransaction(IIf(IsNumeric(lblTotalAmount.Text), lblTotalAmount.Text, 0), txtCHName.Text, IIf(txtCCNo.Text.Trim().Contains("*"), Session("CCNO"), txtCCNo.Text.Trim()), txtCVV2.Text, ddlMonth.SelectedValue, ddlYear.SelectedValue, Session("UserContactID"), ResponseMessage, ReturnTransactionID, responseCode) Then
                    If dtTable.Rows(0)("bitSaveCreditCardInfo") = True Then
                        'Save Credit Card Details
                        Dim objEncryption As New QueryStringValues
                        With objOppInvoice
                            .ContactID = Session("UserContactID") 'dsBizDocDetail.Tables(0).Rows(0)("numContactId").ToString()
                            .CardHolder = objEncryption.Encrypt(txtCHName.Text.Trim())
                            .CardTypeID = ddlCardType.SelectedValue
                            .CreditCardNumber = objEncryption.Encrypt(IIf(txtCCNo.Text.Trim().Contains("*"), Session("CCNO"), txtCCNo.Text.Trim()))
                            .CVV2 = objEncryption.Encrypt(txtCVV2.Text.Trim())
                            .ValidMonth = ddlMonth.SelectedValue
                            .ValidYear = ddlYear.SelectedValue
                            .UserCntID = Session("UserContactID")
                            .AddCustomerCreditCardInfo()
                        End With
                    End If

                    For Each item As DataGridItem In dgOpportunity.Items
                        lngOppID = CCommon.ToLong(item.Cells(0).Text)
                        lngOppBizDocID = CCommon.ToLong(item.Cells(1).Text)
                        lngContactId = CCommon.ToLong(item.Cells(2).Text)
                        AmountToPay = CCommon.ToDecimal(CType(item.FindControl("txtAmountToPay"), TextBox).Text)

                        If AmountToPay > 0 Then
                            objOppInvoice.AmtPaid = AmountToPay
                            objOppInvoice.UserCntID = Session("UserContactID")
                            objOppInvoice.OppBizDocId = lngOppBizDocID
                            objOppInvoice.OppId = lngOppID
                            objOppInvoice.PaymentMethod = ddlPayment.SelectedItem.Value
                            objOppInvoice.BizDocsPaymentDetId = IIf(IsNothing(GetQueryStringVal("d")), 0, GetQueryStringVal("d"))
                            objOppInvoice.Reference = ResponseMessage
                            objOppInvoice.IntegratedToAcnt = False
                            objOppInvoice.Memo = ""
                            objOppInvoice.DomainID = Session("DomainId")
                            objOppInvoice.CardTypeID = ddlCardType.SelectedValue
                            objOppInvoice.bitDisable = True

                            Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                                If objOppInvoice.UpdateAmountPaid() = True Then
                                    'Create Journal Entries Here if payment through CreditCard And if its Sales Order
                                    lngJournalID = SaveDataToHeader(AmountToPay + (AmountToPay * TransactionCharge / 100), objOppInvoice.BizDocsPaymentDetId, lngOppBizDocID, lngOppID)
                                    SaveDataToGeneralJournalDetailsForCreditCard(objOppInvoice.BizDocsPaymentDetId)
                                    If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
                                    If CType(item.FindControl("txtComments"), TextBox).Text.Trim.Length > 0 Then
                                        objOppBizDocs.OppBizDocId = lngOppBizDocID
                                        objOppBizDocs.vcComments = CType(item.FindControl("txtComments"), TextBox).Text
                                        objOppBizDocs.SaveInvoiceDtl()
                                    End If
                                End If

                                objTransactionScope.Complete()
                            End Using
                        End If
                    Next

                Else
                    litMessage.Text = ResponseMessage
                    Exit Sub
                End If
                litMessage.Text = ResponseMessage
            End If

            'objOppInvoice.AmtPaid = 0 'lblTotalAmount.Text
            'objOppInvoice.UserCntID = Session("UserContactID")
            'objOppInvoice.OppBizDocId = GetQueryStringVal( "OppBizDocID")
            'objOppInvoice.UpdateAmountPaid()
            Response.Write("<script>opener.location.reload(true); self.close();</script>")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub BindCardType()
        Try
            Dim objCommon As New CCommon
            objCommon.DomainID = Session("DomainID")
            objCommon.tinyOrder = 0
            ddlCardType.DataTextField = "CardType"
            ddlCardType.DataValueField = "numListItemID"
            ddlCardType.DataSource = objCommon.GetCardTypes()
            ddlCardType.DataBind()
            ddlCardType.Items.Insert(0, "--Select One--")
            ddlCardType.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub BindDatagrid()
        Try
            Dim dtOpportunity As DataTable
            Dim objOpportunity As New COpportunities
            Dim SortChar As Char
            If ViewState("SortChar") <> "" Then
                SortChar = ViewState("SortChar")
            Else : SortChar = "0"
            End If
            With objOpportunity
                .UserCntID = Session("UserContactID")
                .DomainID = Session("DomainID")
                .SortCharacter = SortChar
                .DivisionID = Session("DivID")
                .CurrentPage = CurrentPageIndex
                .PageSize = Session("PagingRows")
                .TotalRecords = 0
                .columnName = "dtCreatedDate"
                .columnSortOrder = "Desc"
                .OppType = 1
                .bitflag = True ' open orders
            End With
            dtOpportunity = objOpportunity.GetInvoiceList
            If objOpportunity.TotalRecords = 0 Then
                litMessage.Text = "No Invoice(s) found"
            End If
            If dtOpportunity.Rows.Count > 0 Then
                dgOpportunity.DataSource = dtOpportunity
                dgOpportunity.DataBind()
            End If
            lblTotalAmount.Text = String.Format("{0:#,##0.00}", CCommon.ToDecimal(dtOpportunity.Compute("sum(BalanceDue)", "")))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub ValidateCardTypeMapping(ByRef TransactionCharge As Double, ByRef TransactionChargeAccountID As Long, ByRef shouldReturn As Boolean)
        shouldReturn = False
        If ddlCardType.SelectedValue > 0 Then
            Dim objCOA As New ChartOfAccounting
            Dim ds As DataSet
            With objCOA
                .TransChargeID = 0
                .CreditCardTypeId = ddlCardType.SelectedValue
                .DomainId = Session("DomainID")
                ds = .GetCOACreditCardCharge()
                If ds.Tables(0).Rows.Count > 0 Then
                    TransactionChargeBareBy = CCommon.ToInteger(ds.Tables(0).Rows(0).Item("tintBareBy"))
                    TransactionChargeAccountID = CCommon.ToLong(ds.Tables(0).Rows(0).Item("numAccountId"))
                    If TransactionChargeAccountID > 0 Then
                        TransactionCharge = CCommon.ToDouble(ds.Tables(0).Rows(0).Item("fltTransactionCharge"))
                    Else
                        litMessage.Text = "Please Set AP account for " & ddlCardType.SelectedItem.Text & " from Administration->Global Settings->Accounting->Accounts for Credit Card."
                        shouldReturn = True : Exit Sub
                    End If
                Else
                    litMessage.Text = "Please Set AP account for " & ddlCardType.SelectedItem.Text & " from Administration-Global Settings->Accounting->Accounts for Credit Card."
                    shouldReturn = True : Exit Sub
                End If
            End With
        Else
            litMessage.Text = "Please Select a Card Type"
            shouldReturn = True : Exit Sub
        End If
    End Sub
    Private Function SaveDataToHeader(ByVal p_Amount As Decimal, ByVal p_BizDocsPaymentDetId As Integer, ByVal p_OppBizDocId As Integer, ByVal p_OppId As Integer) As Integer
        Try
            Dim objJEHeader As New JournalEntryHeader
            With objJEHeader
                .JournalId = 0
                .RecurringId = 0
                .EntryDate = Now.UtcNow
                .Description = ""
                .Amount = p_Amount
                .CheckId = 0
                .CashCreditCardId = 0
                .ChartAcntId = 0
                .OppId = p_OppId
                .OppBizDocsId = p_OppBizDocId
                .DepositId = 0
                .BizDocsPaymentDetId = p_BizDocsPaymentDetId
                .IsOpeningBalance = 0
                .LastRecurringDate = Date.Now
                .NoTransactions = 0
                .CategoryHDRID = 0
                .ReturnID = 0
                .CheckHeaderID = 0
                .BillID = 0
                .BillPaymentID = 0
                .UserCntID = Session("UserContactID")
                .DomainID = Session("DomainID")
            End With
            Return objJEHeader.Save()
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Sub SaveDataToGeneralJournalDetailsForCreditCard(ByVal lngBizDocsPaymentDetailsId As Long)
        Dim ldecAmount As Decimal
        Try
            Dim objJEList As New JournalEntryCollection

            Dim objJE As New JournalEntryNew()

            If TransactionChargeBareBy = enmTransChargeBareBy.Employer Then
                'Credit: Credit Card Customer's AR account 299
                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = 0
                objJE.CreditAmt = AmountToPay
                objJE.ChartAcntId = lngCustomerARAccount
                objJE.Description = "Credit amount to Customer's AR account"
                objJE.CustomerId = Session("DivID")
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = ""
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 0
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = lngBizDocsPaymentDetailsId
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)


                'Debit: Credit card amount to Undeposited Fund 299
                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = AmountToPay
                objJE.CreditAmt = 0
                objJE.ChartAcntId = UndepositedFundAccountID
                objJE.Description = "Credit card amount to Undeposited Fund"
                objJE.CustomerId = Session("DivID")
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = ""
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 0
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = lngBizDocsPaymentDetailsId
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)

                'Debit: Transaction Charge 5.98
                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = (AmountToPay * TransactionCharge / 100)
                objJE.CreditAmt = 0
                objJE.ChartAcntId = TransactionChargeAccountID
                objJE.Description = "Transaction Charge"
                objJE.CustomerId = Session("DivID")
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = ""
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 0
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = lngBizDocsPaymentDetailsId
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)


                'Credit: Transaction Charge amount to Undeposited Fund 5.98
                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = 0
                objJE.CreditAmt = (AmountToPay * TransactionCharge / 100)
                objJE.ChartAcntId = UndepositedFundAccountID
                objJE.Description = "Transaction Charge to Undeposited Fund"
                objJE.CustomerId = Session("DivID")
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = ""
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 0
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = lngBizDocsPaymentDetailsId
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)

            ElseIf TransactionChargeBareBy = enmTransChargeBareBy.Customer Then

                ldecAmount = AmountToPay + (AmountToPay * TransactionCharge / 100)

                'Debit : Undeposit Fund (Amount*TransCharge/100)
                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = ldecAmount
                objJE.CreditAmt = 0
                objJE.ChartAcntId = UndepositedFundAccountID
                objJE.Description = "Credit card amount to Undeposited Fund"
                objJE.CustomerId = Session("DivID")
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = ""
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 0
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = lngBizDocsPaymentDetailsId
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)

                'Credit: Customer A/R With (Amount)
                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = 0
                objJE.CreditAmt = AmountToPay
                objJE.ChartAcntId = lngCustomerARAccount
                objJE.Description = "Credit Customer's AR account"
                objJE.CustomerId = Session("DivID")
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = ""
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 0
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = lngBizDocsPaymentDetailsId
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)

                'Credit: CreditCard's transaction charge to Mapped Transaction Account(Whatever %) 
                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = 0
                objJE.CreditAmt = (AmountToPay * TransactionCharge / 100)
                objJE.ChartAcntId = TransactionChargeAccountID
                objJE.Description = "Transaction Charge"
                objJE.CustomerId = Session("DivID")
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = ""
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 0
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = lngBizDocsPaymentDetailsId
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)

                'Debit: CreditCard's transaction charge
                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = (AmountToPay * TransactionCharge / 100)
                objJE.CreditAmt = 0
                objJE.ChartAcntId = TransactionChargeAccountID
                objJE.Description = "Transaction Charge"
                objJE.CustomerId = Session("DivID")
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = ""
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 0
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = lngBizDocsPaymentDetailsId
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)

                'Credit: Undeposited Fund 
                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = 0
                objJE.CreditAmt = (AmountToPay * TransactionCharge / 100)
                objJE.ChartAcntId = UndepositedFundAccountID
                objJE.Description = "Credit card amount to Undeposited Fund"
                objJE.CustomerId = Session("DivID")
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = ""
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 0
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = lngBizDocsPaymentDetailsId
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)
            End If

            objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalID, Session("DomainID"))

            'ds.Tables.Add(dt)
            'lstr = ds.GetXml()
            'objReceivePayment.JournalId = lngJournalID
            'objReceivePayment.Mode = 0
            'objReceivePayment.JournalDetails = lstr
            'objReceivePayment.DomainId = Session("DomainId")
            'objReceivePayment.SaveDataToJournalDetailsForReceivePayment()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class


