Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin

Partial Public Class frmCreateSalesPurFromOpp : Inherits System.Web.UI.Page

    Dim dsTemp As DataSet
    Dim lngOppID As Long

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                LoadItems()
                tblBillToAddress.Visible = False
                tblShipToAddress.Visible = False
                Dim objCommon As New CCommon
                objCommon.sb_FillComboFromDBwithSel(ddlBillCountry, 40, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlShipCountry, 40, Session("DomainID"))
                If GetQueryStringVal(Request.QueryString("enc"), "OppType") = 1 Then
                    titCreateOpp.Text = "Create Purchase Opportunity From Sales Deals / Order"
                    lblHeading.Text = "Create Purchase Opportunity From Sales Deals / Order"
                    radBillEmployer.Visible = True
                    radShipEmployer.Visible = True
                    radBillEmployer.Checked = True
                    radShipEmployer.Checked = True
                Else
                    radBillEmployer.Visible = False
                    radShipEmployer.Visible = False
                    radBillCustomer.Checked = True
                    radShipCustomer.Checked = True
                    titCreateOpp.Text = "Create Sales Opportunity From Purchase Deals / Order"
                    lblHeading.Text = "Create Sales Opportunity From Purchase Deals / Order"
                End If
                btnClose.Attributes.Add("onclick", "return Close()")
                btnSave.Attributes.Add("onclick", "return Save()")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadItems()
        Try
            Dim objOpp As New MOpportunity
            objOpp.OpportunityId = GetQueryStringVal(Request.QueryString("enc"), "OppID")
            dgItem.DataSource = objOpp.LoadItemsFromOppToCreateOpp
            dgItem.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgItem_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgItem.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim lbl As Label
                lbl = e.Item.FindControl("lblNewOppID")
                If lbl.Text > 0 Then
                    Dim chk As CheckBox
                    chk = e.Item.FindControl("chkSelect")
                    chk.Enabled = False
                End If
                Dim txtPrice As TextBox
                txtPrice = e.Item.FindControl("txtPrice")
                If txtPrice.Text <> "" Then txtPrice.Text = String.Format("{0:#,##0.00}", CType(txtPrice.Text, Decimal))
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            FillCustomer()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Public Function FillCustomer()
        Try
            Dim fillCombo As New COpportunities
            With fillCombo
                .DomainID = Session("DomainID")
                .CompFilter = Trim(txtCompName.Text) & "%"
                .UserCntID = Session("UserContactID")
                ddlCompany.DataSource = fillCombo.ListCustomer().Tables(0).DefaultView
                ddlCompany.DataTextField = "vcCompanyname"
                ddlCompany.DataValueField = "numDivisionID"
                ddlCompany.DataBind()
            End With
            ddlCompany.Items.Insert(0, New ListItem("--Select One--", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub ddlCompany_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            If ddlCompany.SelectedIndex > 0 Then
                Dim objProjectList As New ProjectsList
                objProjectList.DomainID = Session("DomainId")
                objProjectList.DivisionID = ddlCompany.SelectedValue
                objProjectList.bytemode = 1
                ddlOpportunity.DataSource = objProjectList.GetOpportunities
                ddlOpportunity.DataTextField = "vcPOppName"
                ddlOpportunity.DataValueField = "numOppId"
                ddlOpportunity.DataBind()
                ddlOpportunity.Items.Insert(0, "Create New Opportunity")
                ddlOpportunity.Items.FindByText("Create New Opportunity").Value = 0
                FillContact(ddlContact)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Public Function FillContact(ByVal ddlCombo As DropDownList)
        Try
            Dim fillCombo As New COpportunities
            With fillCombo
                .DivisionID = ddlCompany.SelectedItem.Value
                ddlCombo.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
                ddlCombo.DataTextField = "Name"
                ddlCombo.DataValueField = "numcontactId"
                ddlCombo.DataBind()
            End With
            ddlCombo.Items.Insert(0, New ListItem("---Select One---", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim dtTable As New DataTable
            dtTable.Columns.Add("numOppOldItemID")
            dtTable.Columns.Add("numOppNewItemID")
            dtTable.TableName = "Item"
            Dim ds As New DataSet
            Dim dr As DataRow
            Dim objOpportunity As New MOpportunity
            objOpportunity.OppType = IIf(GetQueryStringVal(Request.QueryString("enc"), "OppType") = 1, 2, 1)
            If ddlOpportunity.SelectedIndex = 0 Then
                objOpportunity.OpportunityId = 0
                objOpportunity.OpportunityName = ddlCompany.SelectedItem.Text & "-" & Format(Now(), "MMMM")
                objOpportunity.ContactID = ddlContact.SelectedValue
                objOpportunity.DivisionID = ddlCompany.SelectedValue
                objOpportunity.UserCntID = Session("UserContactID")
                objOpportunity.EstimatedCloseDate = Now
                objOpportunity.DomainID = Session("DomainId")
                objOpportunity.OppType = IIf(GetQueryStringVal(Request.QueryString("enc"), "OppType") = 1, 2, 1)
                objOpportunity.Active = 1
                'objOpportunity.strItems = dsTemp.GetXml
                lngOppID = objOpportunity.Save()(0)
            ElseIf ddlOpportunity.SelectedIndex > 0 Then
                lngOppID = ddlOpportunity.SelectedValue
            End If
            objOpportunity.OpportunityId = lngOppID
            Dim dg As DataGridItem
            Dim i As Integer
            For i = 0 To dgItem.Items.Count - 1
                dg = dgItem.Items(i)
                Dim chk As CheckBox
                chk = dg.FindControl("chkSelect")
                If chk.Checked = True Then
                    dr = dtTable.NewRow
                    dr("numOppOldItemID") = CType(dg.FindControl("lblOppItemID"), Label).Text
                    objOpportunity.ItemCode = CType(dg.FindControl("lblItemID"), Label).Text
                    objOpportunity.Units = dg.Cells(4).Text
                    objOpportunity.UnitPrice = CType(dg.FindControl("txtPrice"), TextBox).Text
                    objOpportunity.Desc = CType(dg.FindControl("lblItemDesc"), Label).Text
                    If CType(dg.FindControl("lblWareHouseItemID"), Label).Text.Trim <> "" Then
                        objOpportunity.WarehouseItemID = CType(dg.FindControl("lblWareHouseItemID"), Label).Text.Trim
                    End If
                    objOpportunity.ItemType = dg.Cells(3).Text
                    dr("numOppNewItemID") = objOpportunity.UpdateOpportunityItems()
                    dtTable.Rows.Add(dr)
                End If
            Next
            ds.Tables.Add(dtTable)
            If radBillEmployer.Checked = True Then
                objOpportunity.BillToType = 0
            ElseIf radBillCustomer.Checked = True Then
                objOpportunity.BillToType = 1
            Else
                objOpportunity.BillToType = 2
            End If
            If radShipEmployer.Checked = True Then
                objOpportunity.ShipToType = 0
            ElseIf radShipCustomer.Checked = True Then
                objOpportunity.ShipToType = 1
            Else : objOpportunity.ShipToType = 2
            End If
            objOpportunity.strItems = ds.GetXml
            objOpportunity.BillStreet = txtStreet.Text
            objOpportunity.BillCity = txtCity.Text
            objOpportunity.BillPostal = txtPostal.Text
            If ddlBillState.SelectedIndex > 0 Then objOpportunity.BillState = ddlBillState.SelectedValue
            objOpportunity.BillCountry = ddlBillCountry.SelectedValue
            objOpportunity.ShipStreet = txtShipStreet.Text
            objOpportunity.ShipCity = txtShipCity.Text
            objOpportunity.ShipPostal = txtShipPostal.Text
            If ddlShipState.SelectedIndex > 0 Then objOpportunity.ShipState = ddlShipState.SelectedValue
            objOpportunity.ShipCountry = ddlShipCountry.SelectedValue
            objOpportunity.UpdateOpportunityLinkingDtls()
            Dim strScript As String = "<script language=JavaScript>"
            strScript += "window.opener.reDirectPage('../opportunity/frmOpportunities.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&opId=" & lngOppID & "'); self.close();"
            strScript += "</script>"
            If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlBillCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBillCountry.SelectedIndexChanged
        Try
            FillState(ddlBillState, ddlBillCountry.SelectedItem.Value, Session("DomainID"))
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlShipCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShipCountry.SelectedIndexChanged
        Try
            FillState(ddlShipState, ddlShipCountry.SelectedItem.Value, Session("DomainID"))
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub radBillCustomer_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radBillCustomer.CheckedChanged
        Try
            tblBillToAddress.Visible = False
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub radBillEmployer_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radBillEmployer.CheckedChanged
        Try
            tblBillToAddress.Visible = False
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub radBillOther_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radBillOther.CheckedChanged
        Try
            tblBillToAddress.Visible = True
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub radShipCustomer_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radShipCustomer.CheckedChanged
        Try
            tblShipToAddress.Visible = False
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub radShipEmployer_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radShipEmployer.CheckedChanged
        Try
            tblShipToAddress.Visible = False
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub radShipOther_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radShipOther.CheckedChanged
        Try
            tblShipToAddress.Visible = True
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub FillState(ByVal ddl As DropDownList, ByVal lngCountry As Long, ByVal lngDomainID As Long)
        Try
            Dim dtTable As DataTable
            Dim objUserAccess As New UserAccess
            objUserAccess.DomainID = lngDomainID
            objUserAccess.Country = lngCountry
            dtTable = objUserAccess.SelState
            ddl.DataSource = dtTable
            ddl.DataTextField = "vcState"
            ddl.DataValueField = "numStateID"
            ddl.DataBind()
            ddl.Items.Insert(0, "--Select One--")
            ddl.Items.FindByText("--Select One--").Value = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class