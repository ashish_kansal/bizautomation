<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCusAddOpp.aspx.vb"
    Inherits="BACRMPortal.frmCusAddOpp" %>
<%--
<%@ Register Assembly="RadGrid.Net2" Namespace="Telerik.WebControls" TagPrefix="rad" %>
<%@ Register Assembly="RadComboBox.Net2" Namespace="Telerik.WebControls" TagPrefix="rad" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <title>Opportunity</title>

    <script language="javascript" type="text/javascript">
        function DropShip() {
            if (document.getElementById('chkDropShip').checked == true) {
                if (typeof (trWareHouse) != 'undefined') {
                    trWareHouse.style.display = 'none';
                }
                if (typeof (trAttributes) != 'undefined') {
                    trAttributes.style.display = 'none';
                }
            }
            else {
                if (typeof (trWareHouse) != 'undefined') {
                    trWareHouse.style.display = '';
                }
                if (typeof (trAttributes) != 'undefined') {
                    trAttributes.style.display = '';
                }
            }

        }
        function OptDropShip() {

            if (document.getElementById('chkOptDropShip').checked == true) {
                if (typeof (trOptAttributes) != 'undefined') {
                    trOptAttributes.style.display = 'none';
                }
                if (typeof (trOptWareHouse) != 'undefined') {
                    trOptWareHouse.style.display = 'none';
                }
            }
            else {
                if (typeof (trOptAttributes) != 'undefined') {
                    trOptAttributes.style.display = '';
                }
                if (typeof (trOptWareHouse) != 'undefined') {
                    trOptWareHouse.style.display = '';
                }
            }

        }
        function AddPrice(a, b, c) {
            document.getElementById(c).value = a;
        }
        function DeleteRows() {


            if (document.getElementById('ucItem') != null) {
                var grid = igtbl_getGridById('ucItem');
                var i;
                var RowsSelected = 0;
                for (i = grid.Rows.length - 1; i >= 0; i--) {
                    var row = grid.Rows.getRow(i);
                    if (row.getCellFromKey("Select").getValue() == 'true') {
                        row.remove();
                    }
                }


            }
            return false;
        }
        function CheckNumber(cint) {
            if (cint == 1) {
                if (!(window.event.keyCode > 47 && window.event.keyCode < 58 || window.event.keyCode == 44 || window.event.keyCode == 46)) {
                    window.event.keyCode = 0;
                }
            }
            if (cint == 2) {
                if (!(window.event.keyCode > 47 && window.event.keyCode < 58)) {
                    window.event.keyCode = 0;
                }
            }

        }
        function Save() {
            if ((document.Form1.ddlContacts.selectedIndex == -1) || (document.Form1.ddlContacts.value == 0)) {
                alert("Select Contact")
                document.Form1.ddlContacts.focus();
                return false;
            }
            if (document.Form1.ddlOppType.value == 0) {
                alert("Select Opportunity Type")
                document.Form1.ddlOppType.focus();
                return false;
            }
        }

        function Add(a) {

            if (radCmbtem.GetValue() == '') {
                alert("Select Item")
                return false;
            }
            if (document.Form1.txtHidValue.value == "False") {
                if (a != '') {
                    var ddlIDs = a.split(",");
                    for (i = 0; i < ddlIDs.length; i++) {
                        if (document.getElementById(ddlIDs[i]) != null) {
                            if (document.getElementById(ddlIDs[i].split("~")[0]).value == "0") {
                                alert("Select " + ddlIDs[i].split("~")[1])
                                document.getElementById(ddlIDs[i].split("~")[0]).focus();
                                return false;
                            }
                        }
                    }
                }

            }
            if (document.getElementById('chkDropShip').checked == false) {
                if (document.Form1.ddlWarehouse != null) {
                    if (document.Form1.ddlWarehouse.value == 0) {
                        alert("Select Warehouse")
                        document.Form1.ddlWarehouse.focus();
                        return false;
                    }
                }
            }

            //RadGrid1.MasterTableView.Rows[0].Control.cells(8).outerText

            if (document.getElementById('RadGrid1') != null) {
                var grid = RadGrid1;
                var i;
                var RowsSelected = 0;
                for (i = 0; i < RadGrid1.MasterTableView.Rows.length; i++) {
                    if (RadGrid1.MasterTableView.Rows[i].Control.cells(3).outerText == "") {

                    }
                    else {
                        if (document.Form1.ddlWarehouse != null) {
                            if (RadGrid1.MasterTableView.Rows[i].Control.cells(3).outerText == document.Form1.ddlWarehouse.value) {
                                alert("This Item is already added to opportunity. Please Edit the details")
                                return false;
                            }
                        }
                    }

                }


            }

            if (document.Form1.txtHidValue.value == "False" || document.Form1.txtHidValue.value == "" || document.Form1.ddlOppType.value == 2 || document.getElementById('chkDropShip').checked == true) {
                if (document.Form1.txtunits.value == "") {
                    alert("Enter Units")
                    document.Form1.txtunits.focus();
                    return false;
                }
            }
            else {


                if (document.getElementById('uwItemSel') != null) {
                    var grid = igtbl_getGridById('uwItemSel');
                    var i;
                    var j;
                    var rep = 'false';
                    var RowsSelected = 0;
                    for (i = 0; i < grid.Rows.length; i++) {
                        var row = grid.Rows.getRow(i);
                        row.setExpanded()

                        var ChildRows = row.getChildRows();

                        if (ChildRows != null) {
                            for (j = 0; j < ChildRows.length; j++) {
                                var ChildRow = ChildRows[j];
                                var child = igtbl_getRowById(ChildRow.id);
                                if (child.getCellFromKey("Select").getValue().toLowerCase() == 'true') {
                                    rep = 'true';
                                    RowsSelected = RowsSelected + 1;
                                }
                            }
                        }
                        else {
                            rep = 'true';
                        }

                    }

                    if (rep == 'false') {
                        alert('Select Serialized Items')
                        return false
                    }
                    else {
                        row.setExpanded(false)
                    }
                }

            }


            if (document.Form1.txtprice.value == "") {
                alert("Enter Price")
                document.Form1.txtprice.focus();
                return false;
            }
            if (document.Form1.ddlOppType.value == 0) {
                Save()
                return false;
            }


        }
        function AddOption(a) {
            if (document.Form1.ddlOppType.value == 1) {
                if (document.Form1.ddlOptItem.value == 0) {
                    alert("Select Option Item")
                    document.Form1.ddlOptItem.focus();
                    return false;
                }
                if (document.Form1.txtHidOptValue.value == "False") {
                    if (a != '') {
                        var ddlIDs = a.split(",");
                        for (i = 0; i < ddlIDs.length; i++) {
                            if (document.getElementById(ddlIDs[i].split("~")[0]).value == "0") {
                                alert("Select " + ddlIDs[i].split("~")[1])
                                document.getElementById(ddlIDs[i].split("~")[0]).focus();
                                return false;
                            }
                        }
                    }

                }
                if (document.getElementById('chkOptDropShip').checked == false) {
                    if (document.Form1.ddlOptWarehouse != null) {

                        if (document.Form1.ddlOptWarehouse.value == 0) {
                            alert("Select Warehouse")
                            document.Form1.ddlOptWarehouse.focus();
                            return false;
                        }
                    }
                }
                if (document.getElementById('RadGrid1') != null) {
                    var grid = RadGrid1;
                    var i;
                    var RowsSelected = 0;
                    for (i = 0; i < RadGrid1.MasterTableView.Rows.length; i++) {
                        if (RadGrid1.MasterTableView.Rows[i].Control.cells(3).outerText == "") {

                        }
                        else {
                            if (document.Form1.ddlOptWarehouse != null) {
                                if (RadGrid1.MasterTableView.Rows[i].Control.cells(3).outerText == document.Form1.ddlOptWarehouse.value) {
                                    alert("This Item is already added to opportunity. Please Edit the details")
                                    return false;
                                }
                            }
                        }

                    }


                }

                if (document.Form1.txtHidOptValue.value == "False" || document.Form1.txtHidOptValue.value == "") {
                    if (document.Form1.txtOptUnits.value == "") {
                        alert("Enter Units")
                        document.Form1.txtOptUnits.focus();
                        return false;
                    }
                }


                if (document.Form1.txtOptPrice.value == "") {
                    alert("Enter Price")
                    document.Form1.txtOptPrice.focus();
                    return false;
                }


            }
            if (document.Form1.ddlOppType.value == 0) {
                Save()
                return false;
            }


        }
        function openUnit(a) {
            a = document.getElementById(a).value
            window.open('../opportunity/frmUnitdtlsForItem.aspx?ItemCode=' + a, '', 'toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')
        }
        function openItem(a, b, c, d, e) {
            if (radCmbtem.GetValue() == "") {
                a = 0
            }
            else {
                a = radCmbtem.GetValue()
            }

            if (radCmbCompany.GetValue() == "") {
                c = 0
            }
            else {
                c = radCmbCompany.GetValue();
            }
            if (document.getElementById(d).value == 1) {
                b = 0
            }
            if ((document.getElementById(d).value == 1) && (document.getElementById('uwItemSel') != null)) {
                b = 0
                var grid = igtbl_getGridById('uwItemSel');
                var i;
                var j;
                for (i = 0; i < grid.Rows.length; i++) {
                    var row = grid.Rows.getRow(i);
                    var ChildRows = row.getChildRows();
                    if (ChildRows != null) {
                        for (j = 0; j < ChildRows.length; j++) {

                            var childRow = ChildRows[j];
                            //alert(childRow.getCell(0))
                            //var test =igtbl_getElementById(childRow.id)
                            //alert(test)

                            var Test = igtbl_getRowById(childRow.id)
                            if (Test.getCellFromKey("Select").getValue() == 'true') {
                                b = b + 1
                            }
                            //rows.getRow(0).getCell(0).setValue(check);

                        }

                    }
                }

            }
            else if (document.getElementById(d).value == 0) {
                if (document.getElementById(b).value == "") {
                    b = 0
                }
                else {
                    b = document.getElementById(b).value
                }
            }
            var CalPrice = 0;
            if (document.getElementById('hdKit').value == 'True' && (document.getElementById('uKit') != null)) {
                var grid = igtbl_getGridById('uKit');
                var i;
                for (i = 0; i < grid.Rows.length; i++) {
                    var row = grid.Rows.getRow(i);
                    var str = row.getCellFromKey("monListPrice").getValue();
                    //var str='asass/asass'
                    CalPrice = CalPrice + (parseFloat(str.split('/')[0]) * row.getCellFromKey("numQtyItemsReq").getValue())

                }
            }
            if (a != 0 && b != 0) {
                window.open('../opportunity/frmItemPriceRecommd.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ItemCode=' + a + '&Unit=' + b + '&DivID=' + c + '&OppType=' + e + '&CalPrice=' + CalPrice, '', 'toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')

            }
        }
        function openOptItem(a, b, c) {
            if (document.getElementById(a).selectedIndex < 1) {
                a = 0
            }
            else {
                a = document.getElementById(a).value
            }

            if (document.getElementById(c).selectedIndex < 1) {
                c = 0
            }
            else {
                c = document.getElementById(c).value
            }

            if (document.getElementById(b).value == "") {
                b = 0
            }
            else {
                b = document.getElementById(b).value
            }
            if (a != 0 && b != 0) {
                window.open('../opportunity/frmItemPriceRecommd.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ItemCode=' + a + '&Unit=' + b + '&DivID=' + c + '&OptItem=' + 1 + '&CalPrice=0', '', 'toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')

            }
        }
        function openSelVendor(a) {
            if (radCmbtem.GetValue() == '') {
                alert("Select Item");
                return false;
            }
            a = radCmbtem.GetValue()
            window.open('../opportunity/frmSelectVendor.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ItemCode=' + a, '', 'toolbar=no,titlebar=no,left=300,top=450,width=700,height=400,scrollbars=yes,resizable=yes')
            return false;
        }
        function ShowWindow(Page, q, att) {

            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;
            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                return false;

            }
        }
        function Focus() {
            document.Form1.txtCompName.focus();
        }
        function openPurOpp() {

            window.open('../opportunity/frmSelectPurOpp.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppID=0', '', 'toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function SelectVendor(ItemCode, DivID, Cost, ItemName, CompName, Unit) {
            document.Form1.txtunits.value = Unit;
            document.Form1.txtprice.value = Cost;
            document.Form1.hdDivID.value = DivID;
            document.Form1.btnLOadCo.click();
        }
        function GetSelectedItem(combobox) {
            combobox.ClientDataString = document.Form1.hdDivID.value + '~' + document.getElementById('ddlOppType').value;
        }
        function WinClose() {
            window.close()
            return false;
        }
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <br />
    <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server">
        <tr>
            <td valign="bottom">
                <table class="TabStyle">
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;New Opportunity&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td align="right">
                <asp:Button ID="btnSave" runat="server" Text="Save &amp; Proceed" CssClass="button">
                </asp:Button>
                <asp:Button ID="btnCancel" runat="server" Text="Close" CssClass="button"></asp:Button>
            </td>
        </tr>
    </table>
    <asp:Table ID="tblOppr" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        CssClass="aspTable" Width="100%" BorderColor="black" GridLines="None">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br />
                <table width="100%" border="0">
                    <tr>
                        <td rowspan="30" valign="top">
                            <img src="../images/Dart-32.gif" />
                        </td>
                        <td class="text" align="right">
                            Contact<font color="#ff3333"> *</font>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlContacts" runat="server" Width="200" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                        <td class="text" align="right">
                            Opportunity Source
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlSource" runat="server" CssClass="signup" Width="200">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="text" align="right">
                            Opportunity Type<font color="#ff3333"> *</font>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlOppType" runat="server" CssClass="signup" Width="200" AutoPostBack="true">
                                <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                <asp:ListItem Value="1">Sales Opportunity</asp:ListItem>
                                <asp:ListItem Value="2">Purchase Opportunity</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <asp:Panel ID="pnlSales" runat="server" Visible="false">
                            <td class="text" align="right">
                                Campaign
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlCampaign" runat="server" CssClass="signup" Width="200">
                                </asp:DropDownList>
                            </td>
                        </asp:Panel>
                        <asp:Panel ID="pnlPurchase" runat="server" Visible="false">
                            <td class="text" align="right">
                            </td>
                            <td>
                                <asp:Button ID="btnSelectVendor" CssClass="button" runat="server" Text="Select Vendor" />
                            </td>
                        </asp:Panel>
                    </tr>
                </table>
                <br />
                <table align="center">
                    <tr>
                        <td class="normal1" rowspan="111111115">
                            <asp:Image ID="imgItem" runat="server" BorderWidth="1" Width="100" BorderColor="black"
                                Height="100"></asp:Image><br>
                            <asp:HyperLink ID="hplImage" runat="server" CssClass="hyperlink" NavigateUrl="">View Full Image</asp:HyperLink>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td class="normal1" valign="top" align="right">
                                        Item
                                    </td>
                                    <td class="normal1" valign="top">
                                        <rad:RadComboBox ID="radCmbtem" OnClientItemsRequesting="GetSelectedItem" ExternalCallBackPage="frmOppLoadItems.aspx"
                                            Width="195px" DropDownWidth="500px" Skin="WindowsXP" runat="server" AutoPostBack="True"
                                            AllowCustomText="True" EnableLoadOnDemand="True">
                                            <HeaderTemplate>
                                                <table style="width: 500px; text-align: left">
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <td style="width: 125px;">
                                                            Item
                                                        </td>
                                                        <td style="width: 125px;">
                                                            List Price
                                                        </td>
                                                        <td style="width: 125px;">
                                                            SKU
                                                        </td>
                                                        <td style="width: 125px;">
                                                            Model ID
                                                        </td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <table style="width: 500px; text-align: left">
                                                    <tr>
                                                        <td style="display: none">
                                                            <%#DataBinder.Eval(Container.DataItem, "numItemCode")%>
                                                        </td>
                                                        <td style="width: 125px;">
                                                            <%#DataBinder.Eval(Container.DataItem, "vcItemName")%>
                                                        </td>
                                                        <td style="width: 125px;">
                                                            <%#DataBinder.Eval(Container.DataItem, "Price")%>
                                                        </td>
                                                        <td style="width: 125px;">
                                                            <%#DataBinder.Eval(Container.DataItem, "SKU")%>
                                                        </td>
                                                        <td style="width: 125px;">
                                                            <%# DataBinder.Eval(Container.DataItem, "Model ID") %>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </rad:RadComboBox>
                                    </td>
                                    <td class="normal1" align="right">
                                        List Price :
                                    </td>
                                    <td>
                                        <asp:Label ID="lblListPrice" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right">
                                        Type
                                    </td>
                                    <td class="normal1">
                                        <asp:DropDownList ID="ddltype" runat="server" CssClass="signup" Width="200" Enabled="False">
                                            <asp:ListItem Value="P">Product</asp:ListItem>
                                            <asp:ListItem Value="S">Service</asp:ListItem>
                                            <asp:ListItem Value="A">Accessory</asp:ListItem>
                                            <asp:ListItem Value="N">Non-Inventory Item</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                    </td>
                                    <td class="normal1">
                                        <asp:CheckBox ID="chkDropShip" Text="Drop Ship" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="white-space: nowrap">
                                        <asp:PlaceHolder ID="phItems" runat="server"></asp:PlaceHolder>
                                    </td>
                                </tr>
                                <tr id="trWareHouse" runat="server">
                                    <td class="normal1" align="right">
                                        Warehouse<font color="red">*</font>
                                    </td>
                                    <td class="normal1" colspan="3">
                                        <asp:DropDownList ID="ddlWarehouse" AutoPostBack="true" runat="server" CssClass="signup"
                                            Width="200">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblBudget" runat="server" Text="The budget balance for the item group this item belongs to is: "
                                            Visible="false"></asp:Label>
                                        &nbsp;&nbsp;
                                        <asp:Label ID="lblBudgetMonth" runat="server" Text="" Visible="false"></asp:Label>&nbsp;&nbsp;
                                        <asp:Label ID="lblBudgetYear" runat="server" Text="" Visible="false"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right">
                                        Description
                                    </td>
                                    <td class="normal1" colspan="3">
                                        <asp:TextBox ID="txtdesc" runat="server" TextMode="MultiLine" Width="500"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right">
                                        <asp:Label ID="tdUnits" runat="server">
                                            <asp:Label ID="lblUnits" runat="server">Units</asp:Label><font color="red">*</font></asp:Label>
                                    </td>
                                    <td class="normal1" nowrap colspan="3">
                                        <asp:TextBox ID="txtunits" runat="server" CssClass="signup" Width="90px"></asp:TextBox>
                                        <asp:HyperLink ID="hplPrice" runat="server" CssClass="hyperlink">Unit Price</asp:HyperLink><font
                                            color="red">*</font>
                                        <asp:TextBox ID="txtprice" runat="server" CssClass="signup" Width="90px" MaxLength="11"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:Button ID="btnAdd" runat="server" Width="60px" CssClass="button" Text="Add">
                                        </asp:Button>&nbsp;&nbsp;&nbsp;<asp:Button ID="btnEditCancel" runat="server" Visible="false"
                                            CssClass="button" Text="Cancel"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table id="tblKits" visible="false" runat="server" width="100%">
                    <tr>
                        <td class="text_bold">
                            Kit Items
                        </td>
                        <td>
                            <rad:RadComboBox ID="radItem" ExternalCallBackPage="../Items/frmLoadItems.aspx" Width="150"
                                DropDownWidth="150" Skin="WindowsXP" runat="server" EnableLoadOnDemand="True">
                            </rad:RadComboBox>
                            &nbsp;<asp:Button ID="btnAddKitItem" runat="server" Text="Add Child Items" CssClass="button" />
                        </td>
                        <td>
                            <asp:Button ID="btnRemoveKitItems" runat="server" Text="Remove Child Items" CssClass="button" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <igtbl:UltraWebGrid ID="uKit" Width="100%" DisplayLayout-AllowRowNumberingDefault="ByDataIsland"
                                runat="server" Browser="Xml" Height="100%">
                                <DisplayLayout AutoGenerateColumns="false" RowHeightDefault="18" AllowAddNewDefault="No"
                                    Version="3.00" SelectTypeRowDefault="Single" ViewType="Hierarchical" TableLayout="Auto"
                                    SelectTypeCellDefault="Extended" BorderCollapseDefault="Separate" AllowColSizingDefault="Free"
                                    Name="uKit" EnableClientSideRenumbering="true" SelectTypeColDefault="Extended"
                                    AllowUpdateDefault="Yes">
                                    <HeaderStyleDefault VerticalAlign="Middle" Font-Size="8pt" Font-Bold="true" Font-Names="Arial"
                                        BorderStyle="Solid" HorizontalAlign="Left" ForeColor="white" BackColor="#52658C">
                                        <Padding Left="2px" Right="2px"></Padding>
                                        <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
                                        </BorderDetails>
                                    </HeaderStyleDefault>
                                    <RowSelectorStyleDefault BackColor="White">
                                    </RowSelectorStyleDefault>
                                    <FrameStyle Width="100%" Cursor="Default" BorderWidth="0px" Font-Size="8pt" Font-Names="Arial"
                                        BorderStyle="Double">
                                    </FrameStyle>
                                    <FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
                                        <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
                                        </BorderDetails>
                                    </FooterStyleDefault>
                                    <EditCellStyleDefault BorderWidth="0px" BorderStyle="None">
                                    </EditCellStyleDefault>
                                    <SelectedRowStyleDefault ForeColor="White" BackColor="#666666">
                                    </SelectedRowStyleDefault>
                                    <RowStyleDefault BorderWidth="1px" Font-Size="8pt" Font-Names="Arial" BorderColor="Gray"
                                        BorderStyle="Solid" BackColor="White">
                                        <Padding Left="5px" Right="5px"></Padding>
                                        <BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
                                    </RowStyleDefault>
                                    <RowExpAreaStyleDefault BackColor="LightSteelBlue">
                                    </RowExpAreaStyleDefault>
                                </DisplayLayout>
                                <Bands>
                                    <igtbl:UltraGridBand AllowDelete="No" AllowAdd="Yes" BaseTableName="ChildItems">
                                        <Columns>
                                            <igtbl:UltraGridColumn Type="CheckBox" Key="Remove">
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Hidden="true" Width="100%" IsBound="true" BaseColumnName="numItemCode"
                                                Key="numItemCode">
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Hidden="true" Width="100%" IsBound="true" BaseColumnName="numOppChildItemID"
                                                Key="numOppChildItemID">
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn HeaderText="Child Item" AllowUpdate="No" IsBound="true" BaseColumnName="vcItemName"
                                                Key="vcItemName">
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn HeaderText="Description" AllowUpdate="No" IsBound="true" BaseColumnName="txtItemDesc"
                                                Key="txtItemDesc">
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn HeaderText="Type" AllowUpdate="No" IsBound="true" BaseColumnName="charItemType"
                                                Key="charItemType">
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn HeaderText="Unit Price" AllowUpdate="No" IsBound="true" BaseColumnName="UnitPrice"
                                                Key="monListPrice">
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn HeaderText="Quantity" AllowUpdate="Yes" IsBound="true" BaseColumnName="numQtyItemsReq"
                                                Key="numQtyItemsReq">
                                            </igtbl:UltraGridColumn>
                                        </Columns>
                                    </igtbl:UltraGridBand>
                                </Bands>
                            </igtbl:UltraWebGrid>
                        </td>
                    </tr>
                </table>
                <table width="100%" id="tblItemWareHouse" runat="server" visible="false">
                    <tr>
                        <td class="text_bold">
                            Inventory Details
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <igtbl:UltraWebGrid ID="uwItemSel" Width="100%" DisplayLayout-AllowRowNumberingDefault="ByDataIsland"
                                runat="server" Browser="Xml" Height="100%">
                                <DisplayLayout AutoGenerateColumns="true" RowHeightDefault="18" AllowAddNewDefault="Yes"
                                    Version="3.00" SelectTypeRowDefault="Single" ViewType="Hierarchical" TableLayout="Auto"
                                    SelectTypeCellDefault="Extended" BorderCollapseDefault="Separate" AllowColSizingDefault="Free"
                                    Name="uwItemSel" EnableClientSideRenumbering="true" SelectTypeColDefault="Extended"
                                    AllowUpdateDefault="Yes">
                                    <HeaderStyleDefault VerticalAlign="Middle" Font-Size="8pt" Font-Bold="true" Font-Names="Arial"
                                        BorderStyle="Solid" HorizontalAlign="Left" ForeColor="white" BackColor="#52658C">
                                        <Padding Left="2px" Right="2px"></Padding>
                                        <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
                                        </BorderDetails>
                                    </HeaderStyleDefault>
                                    <RowSelectorStyleDefault BackColor="White">
                                    </RowSelectorStyleDefault>
                                    <FrameStyle Width="100%" Cursor="Default" BorderWidth="0px" Font-Size="8pt" Font-Names="Arial"
                                        BorderStyle="Double">
                                    </FrameStyle>
                                    <FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
                                        <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
                                        </BorderDetails>
                                    </FooterStyleDefault>
                                    <EditCellStyleDefault BorderWidth="0px" BorderStyle="None">
                                    </EditCellStyleDefault>
                                    <SelectedRowStyleDefault ForeColor="White" BackColor="#666666">
                                    </SelectedRowStyleDefault>
                                    <RowStyleDefault BorderWidth="1px" Font-Size="8pt" Font-Names="Arial" BorderColor="Gray"
                                        BorderStyle="Solid" BackColor="White">
                                        <Padding Left="5px" Right="5px"></Padding>
                                        <BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
                                    </RowStyleDefault>
                                    <RowExpAreaStyleDefault BackColor="LightSteelBlue">
                                    </RowExpAreaStyleDefault>
                                </DisplayLayout>
                                <Bands>
                                    <igtbl:UltraGridBand AllowDelete="No" AllowAdd="Yes" AddButtonCaption="WareHouse"
                                        BaseTableName="WareHouse" Key="WareHouse">
                                        <Columns>
                                            <igtbl:UltraGridColumn Hidden="true" Width="100%" IsBound="true" BaseColumnName="numWareHouseItemID"
                                                Key="numWareHouseItemID">
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Hidden="true" IsBound="true" BaseColumnName="numWareHouseID"
                                                Key="numWareHouseID">
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn Hidden="true" IsBound="true" BaseColumnName="numItemCode"
                                                Key="numItemCode">
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn HeaderText="Warehouse" AllowUpdate="No" IsBound="true" BaseColumnName="vcWarehouse"
                                                Key="vcWarehouse">
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn HeaderText="On Hand" AllowUpdate="No" IsBound="true" BaseColumnName="OnHand"
                                                Key="OnHand">
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn HeaderText="On Order" AllowUpdate="No" IsBound="true" BaseColumnName="OnOrder"
                                                Key="OnOrder">
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn HeaderText="Reorder" AllowUpdate="No" IsBound="true" BaseColumnName="Reorder"
                                                Key="Reorder">
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn HeaderText="Allocation" AllowUpdate="No" IsBound="true" BaseColumnName="Allocation"
                                                Key="Allocation">
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn HeaderText="BackOrder" AllowUpdate="No" IsBound="true" BaseColumnName="BackOrder"
                                                Key="BackOrder">
                                            </igtbl:UltraGridColumn>
                                            <igtbl:UltraGridColumn HeaderText="Units" Hidden="true" AllowUpdate="Yes" Format="###,##0.00"
                                                Key="Units">
                                            </igtbl:UltraGridColumn>
                                        </Columns>
                                    </igtbl:UltraGridBand>
                                    <igtbl:UltraGridBand AllowDelete="No" AllowAdd="Yes" AllowUpdate="No" AddButtonCaption="Serialized Items"
                                        BaseTableName="SerializedItems" Key="SerializedItems">
                                        <Columns>
                                            <igtbl:UltraGridColumn AllowUpdate="Yes" Type="CheckBox" Key="Select">
                                            </igtbl:UltraGridColumn>
                                        </Columns>
                                    </igtbl:UltraGridBand>
                                </Bands>
                            </igtbl:UltraWebGrid>
                        </td>
                    </tr>
                </table>
                <br />
                <br />
                <table align="center" id="tblOptSelection" runat="server" visible="false">
                    <tr>
                        <td class="text_bold" colspan="2">
                            Choose Options And Accessories
                            <br />
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" rowspan="111111115">
                            <asp:Image ID="imgOptItem" runat="server" BorderWidth="1" Width="100" BorderColor="black"
                                Height="100"></asp:Image><br>
                            <asp:HyperLink ID="hplOptImage" runat="server" CssClass="hyperlink" NavigateUrl="">View Full Image</asp:HyperLink>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td class="normal1" valign="top" align="right">
                                        Option Item
                                    </td>
                                    <td class="normal1" valign="top">
                                        <asp:DropDownList ID="ddlOptItem" TabIndex="19" runat="server" CssClass="signup"
                                            Width="450" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right">
                                        List Price :
                                    </td>
                                    <td class="normal1">
                                        <asp:Label ID="lblOptPrice" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                    </td>
                                    <td class="normal1">
                                        <asp:CheckBox ID="chkOptDropShip" Text="Drop Ship" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" style="white-space: nowrap">
                                        <asp:PlaceHolder ID="phOptItem" runat="server"></asp:PlaceHolder>
                                    </td>
                                </tr>
                                <tr id="trOptWareHouse" runat="server">
                                    <td class="normal1" align="right">
                                        Warehouse<font color="red">*</font>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlOptWarehouse" AutoPostBack="true" runat="server" CssClass="signup"
                                            Width="180">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right">
                                        Description
                                    </td>
                                    <td class="normal1" colspan="3">
                                        <asp:TextBox ID="txtOptDesc" runat="server" TextMode="MultiLine" Width="500"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right">
                                        <asp:Label ID="lblOptUnits" runat="server">Units</asp:Label><font color="red">*</font>
                                    </td>
                                    <td class="normal1" nowrap colspan="3">
                                        <asp:TextBox ID="txtOptUnits" runat="server" CssClass="signup" Width="90px"></asp:TextBox>
                                        <asp:HyperLink ID="hplOptPrice" runat="server" CssClass="hyperlink">Unit Price</asp:HyperLink><font
                                            color="red">*</font>
                                        <asp:TextBox ID="txtOptPrice" runat="server" CssClass="signup" Width="90px" MaxLength="11"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:Button ID="btnOptAdd" runat="server" Width="60px" CssClass="button" Text="Add">
                                        </asp:Button>&nbsp;&nbsp;&nbsp;<asp:Button ID="Button2" runat="server" Visible="false"
                                            CssClass="button" Text="Cancel"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <igtbl:UltraWebGrid ID="uwOptionItem" Visible="false" Width="100%" DisplayLayout-AllowRowNumberingDefault="ByDataIsland"
                    runat="server" Browser="Xml" Height="100%">
                    <DisplayLayout AutoGenerateColumns="true" RowHeightDefault="18" AllowAddNewDefault="Yes"
                        Version="3.00" SelectTypeRowDefault="Single" ViewType="Hierarchical" TableLayout="Auto"
                        SelectTypeCellDefault="Extended" BorderCollapseDefault="Separate" AllowColSizingDefault="Free"
                        Name="uwItemSel" EnableClientSideRenumbering="true" SelectTypeColDefault="Extended"
                        AllowUpdateDefault="Yes">
                        <HeaderStyleDefault VerticalAlign="Middle" Font-Size="8pt" Font-Bold="true" Font-Names="Arial"
                            BorderStyle="Solid" HorizontalAlign="Left" ForeColor="white" BackColor="#52658C">
                            <Padding Left="2px" Right="2px"></Padding>
                            <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
                            </BorderDetails>
                        </HeaderStyleDefault>
                        <RowSelectorStyleDefault BackColor="White">
                        </RowSelectorStyleDefault>
                        <FrameStyle Width="100%" Cursor="Default" BorderWidth="0px" Font-Size="8pt" Font-Names="Arial"
                            BorderStyle="Double">
                        </FrameStyle>
                        <FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
                            <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
                            </BorderDetails>
                        </FooterStyleDefault>
                        <EditCellStyleDefault BorderWidth="0px" BorderStyle="None">
                        </EditCellStyleDefault>
                        <SelectedRowStyleDefault ForeColor="White" BackColor="#666666">
                        </SelectedRowStyleDefault>
                        <RowStyleDefault BorderWidth="1px" Font-Size="8pt" Font-Names="Arial" BorderColor="Gray"
                            BorderStyle="Solid" BackColor="White">
                            <Padding Left="5px" Right="5px"></Padding>
                            <BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
                        </RowStyleDefault>
                        <RowExpAreaStyleDefault BackColor="LightSteelBlue">
                        </RowExpAreaStyleDefault>
                    </DisplayLayout>
                    <Bands>
                        <igtbl:UltraGridBand AllowDelete="No" AllowAdd="Yes" AddButtonCaption="WareHouse"
                            BaseTableName="WareHouse" Key="WareHouse">
                            <Columns>
                                <igtbl:UltraGridColumn Hidden="true" Width="100%" IsBound="true" BaseColumnName="numWareHouseItemID"
                                    Key="numWareHouseItemID">
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn Hidden="true" IsBound="true" BaseColumnName="numWareHouseID"
                                    Key="numWareHouseID">
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn Hidden="true" IsBound="true" BaseColumnName="numItemCode"
                                    Key="numItemCode">
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn HeaderText="Warehouse" AllowUpdate="No" IsBound="true" BaseColumnName="vcWarehouse"
                                    Key="vcWarehouse">
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn HeaderText="On Hand" AllowUpdate="No" IsBound="true" BaseColumnName="OnHand"
                                    Key="OnHand">
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn HeaderText="On Order" AllowUpdate="No" IsBound="true" BaseColumnName="OnOrder"
                                    Key="OnOrder">
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn HeaderText="Reorder" AllowUpdate="No" IsBound="true" BaseColumnName="Reorder"
                                    Key="Reorder">
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn HeaderText="Allocation" AllowUpdate="No" IsBound="true" BaseColumnName="Allocation"
                                    Key="Allocation">
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn HeaderText="BackOrder" AllowUpdate="No" IsBound="true" BaseColumnName="BackOrder"
                                    Key="BackOrder">
                                </igtbl:UltraGridColumn>
                                <igtbl:UltraGridColumn HeaderText="Units" Hidden="true" AllowUpdate="Yes" Format="###,##0.00"
                                    Key="Units">
                                </igtbl:UltraGridColumn>
                            </Columns>
                        </igtbl:UltraGridBand>
                    </Bands>
                </igtbl:UltraWebGrid>
                <br />
                <table width="100%" id="tblProducts" runat="server" visible="false">
                    <tr>
                        <td class="text_bold">
                            Items Added To Opportunity
                        </td>
                        <td align="right">
                            <asp:Button ID="btnRemoveItem" runat="server" CssClass="button" Text="Remove Items" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <rad:RadGrid ID="RadGrid1" Skin="Windows" runat="server" Width="100%" AutoGenerateColumns="False"
                                GridLines="None" ShowFooter="false" AllowMultiRowSelection="true">
                                <MasterTableView DataMember="Item" DataKeyNames="numoppitemtCode" Width="100%">
                                    <Columns>
                                        <rad:GridClientSelectColumn UniqueName="Select">
                                        </rad:GridClientSelectColumn>
                                        <rad:GridBoundColumn SortExpression="numoppitemtCode" UniqueName="oppitemtCode" HeaderText=""
                                            HeaderButtonType="TextButton" DataField="numoppitemtCode" Display="false">
                                        </rad:GridBoundColumn>
                                        <rad:GridBoundColumn SortExpression="numWarehouseItmsID" HeaderText="" HeaderButtonType="TextButton"
                                            DataField="numWarehouseItmsID" Display="false">
                                        </rad:GridBoundColumn>
                                        <rad:GridBoundColumn SortExpression="numWareHouseID" HeaderText="" HeaderButtonType="TextButton"
                                            DataField="numWareHouseID" Display="false">
                                        </rad:GridBoundColumn>
                                        <rad:GridBoundColumn SortExpression="numWarehouseItmsID" HeaderText="" HeaderButtonType="TextButton"
                                            DataField="numWarehouseItmsID" Display="false">
                                        </rad:GridBoundColumn>
                                        <rad:GridBoundColumn SortExpression="numItemCode" HeaderText="" HeaderButtonType="TextButton"
                                            DataField="numItemCode" Display="false">
                                        </rad:GridBoundColumn>
                                        <rad:GridBoundColumn SortExpression="vcItemName" HeaderText="Item" HeaderButtonType="TextButton"
                                            DataField="vcItemName">
                                        </rad:GridBoundColumn>
                                        <rad:GridBoundColumn SortExpression="Warehouse" HeaderText="WareHouse" HeaderButtonType="TextButton"
                                            DataField="Warehouse">
                                        </rad:GridBoundColumn>
                                        <rad:GridBoundColumn SortExpression="ItemType" HeaderText="Type" HeaderButtonType="TextButton"
                                            DataField="ItemType">
                                        </rad:GridBoundColumn>
                                        <rad:GridBoundColumn SortExpression="vcItemDesc" HeaderText="Description" HeaderButtonType="TextButton"
                                            DataField="vcItemDesc">
                                        </rad:GridBoundColumn>
                                        <rad:GridBoundColumn SortExpression="Attributes" HeaderText="Attributes" HeaderButtonType="TextButton"
                                            DataField="Attributes">
                                        </rad:GridBoundColumn>
                                        <rad:GridBoundColumn SortExpression="DropShip" HeaderText="Drop Ship" HeaderButtonType="TextButton"
                                            DataField="DropShip">
                                        </rad:GridBoundColumn>
                                        <rad:GridBoundColumn SortExpression="numUnitHour" HeaderText="Units" HeaderButtonType="TextButton"
                                            DataField="numUnitHour">
                                        </rad:GridBoundColumn>
                                        <rad:GridBoundColumn SortExpression="monPrice" HeaderText="Unit Price" HeaderButtonType="TextButton"
                                            DataField="monPrice">
                                        </rad:GridBoundColumn>
                                        <rad:GridBoundColumn SortExpression="monTotAmount" HeaderText="Amount" HeaderButtonType="TextButton"
                                            DataField="monTotAmount">
                                        </rad:GridBoundColumn>
                                        <rad:GridTemplateColumn>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CommandName="EditSelect"></asp:LinkButton>
                                            </ItemTemplate>
                                        </rad:GridTemplateColumn>
                                    </Columns>
                                    <DetailTables>
                                        <rad:GridTableView DataMember="SerialNo" HierarchyLoadMode="Client" Width="100%">
                                            <ParentTableRelation>
                                                <rad:GridRelationFields DetailKeyField="numoppitemtCode" MasterKeyField="numoppitemtCode" />
                                            </ParentTableRelation>
                                            <Columns>
                                                <rad:GridBoundColumn SortExpression="numWarehouseItmsDTLID" HeaderText="" HeaderButtonType="TextButton"
                                                    DataField="numWarehouseItmsDTLID" Display="false">
                                                </rad:GridBoundColumn>
                                                <rad:GridBoundColumn SortExpression="numoppitemtCode" HeaderText="" HeaderButtonType="TextButton"
                                                    DataField="numoppitemtCode" Display="false">
                                                </rad:GridBoundColumn>
                                                <rad:GridBoundColumn SortExpression="vcSerialNo" HeaderText="Serial No" HeaderButtonType="TextButton"
                                                    DataField="vcSerialNo">
                                                </rad:GridBoundColumn>
                                                <rad:GridBoundColumn SortExpression="Comments" HeaderText="Comments" HeaderButtonType="TextButton"
                                                    DataField="Comments">
                                                </rad:GridBoundColumn>
                                                <rad:GridBoundColumn SortExpression="Attributes" HeaderText="Attributes" HeaderButtonType="TextButton"
                                                    DataField="Attributes">
                                                </rad:GridBoundColumn>
                                            </Columns>
                                        </rad:GridTableView>
                                        <rad:GridTableView DataMember="ChildItems" HierarchyLoadMode="Client" Width="100%">
                                            <ParentTableRelation>
                                                <rad:GridRelationFields DetailKeyField="numoppitemtCode" MasterKeyField="numoppitemtCode" />
                                            </ParentTableRelation>
                                            <Columns>
                                                <rad:GridBoundColumn SortExpression="vcItemName" HeaderText="Item" HeaderButtonType="TextButton"
                                                    DataField="vcItemName">
                                                </rad:GridBoundColumn>
                                                <rad:GridBoundColumn SortExpression="txtItemDesc" HeaderText="Description" HeaderButtonType="TextButton"
                                                    DataField="txtItemDesc">
                                                </rad:GridBoundColumn>
                                                <rad:GridBoundColumn SortExpression="charItemType" HeaderText="Type" HeaderButtonType="TextButton"
                                                    DataField="charItemType">
                                                </rad:GridBoundColumn>
                                                <rad:GridBoundColumn SortExpression="UnitPrice" HeaderText="Unit Price" HeaderButtonType="TextButton"
                                                    DataField="UnitPrice">
                                                </rad:GridBoundColumn>
                                                <rad:GridBoundColumn SortExpression="numQtyItemReq" HeaderText="Quantity" HeaderButtonType="TextButton"
                                                    DataField="numQtyItemsReq">
                                                </rad:GridBoundColumn>
                                            </Columns>
                                        </rad:GridTableView>
                                    </DetailTables>
                                </MasterTableView>
                                <ClientSettings>
                                    <Selecting AllowRowSelect="true" />
                                </ClientSettings>
                            </rad:RadGrid>
                        </td>
                    </tr>
                </table>
                <br />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:TextBox ID="txtHidValue" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtHidEditOppItem" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtHidOptValue" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSerialize" runat="server" Style="display: none"></asp:TextBox>
    <input id="hdKit" runat="server" type="hidden" />
    <input id="hdOptKit" runat="server" type="hidden" />
    <input id="hdDivID" runat="server" type="hidden" />
    <input id="Hidden1" runat="server" type="hidden" />
    <asp:Button ID="btnLOadCo" runat="server" Style="display: none" />&nbsp;
    <br />
    </form>
</body>
</html>
--%>