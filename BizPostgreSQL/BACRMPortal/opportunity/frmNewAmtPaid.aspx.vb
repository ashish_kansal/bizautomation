﻿'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Contract
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports System.Text.RegularExpressions
Imports System.Web.Script.Serialization
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Prospects


Public Class frmNewAmtPaid
    Inherits System.Web.UI.Page

    Dim objOppBizDocs As OppBizDocs
    Dim ResponseMessage As String
    Dim objCommon As New CCommon

    Dim lngDivID, lngDepositeID, lngTransactionHistoryID As Long
    Dim ReturnTransactionID As String = ""

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            lngDivID = Session("DivID")
            hdnOppType.Value = 1 'This page will be only used for Sales Invoice payment, not for purchase bizdocs
            If CCommon.ToLong(Session("DomainID")) = 0 Or CCommon.ToLong(Session("DivID")) = 0 Then
                Response.Write("<script>opener.location='../login.aspx'; self.close();</script>")
                Exit Sub
                'Response.Redirect("../login.aspx", False)
            End If

            If Not IsPostBack Then
                FillDepositTocombo()
                BindCardYear()
                FillContact(ddlContact)
                BindCardType()

                BindDatagrid()
                LoadSavedCreditCardInfo()
            End If

            btnSaveClose.Attributes.Add("onclick", "return Save(0)")
            txtCCNo.Attributes.Add("onkeypress", "CheckNumber(2,event)")
            txtCVV2.Attributes.Add("onkeypress", "CheckNumber(2,event)")
            txtSwipe.Attributes.Add("onchange", "swipedCreditCard()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub BindCardYear()
        Try
            For i As Integer = Date.Now.Year To Date.Now.Year + 10
                ddlYear.Items.Add(New ListItem(i.ToString("00"), i.ToString.Substring(2, 2)))
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadSavedCreditCardInfo()
        Try
            Dim objOppInvoice As New OppInvoice
            Dim dsCCInfo As DataSet
            objOppInvoice.DomainID = Session("DomainID")
            objOppInvoice.DivisionID = lngDivID
            'objOppInvoice.UserCntID =
            objOppInvoice.numCCInfoID = ddlCards.SelectedValue
            objOppInvoice.IsDefault = 1
            objOppInvoice.bitflag = 1
            dsCCInfo = objOppInvoice.GetCustomerCreditCardInfo()

            If dsCCInfo.Tables(0).Rows.Count > 0 Then
                Dim objEncryption As New QueryStringValues
                txtCHName.Text = objEncryption.Decrypt(dsCCInfo.Tables(0).Rows(0)("vcCardHolder").ToString())
                Dim strCCNo As String
                strCCNo = objEncryption.Decrypt(dsCCInfo.Tables(0).Rows(0)("vcCreditCardNo").ToString())
                Session("CCNO") = strCCNo
                If strCCNo.Length > 4 Then
                    strCCNo = "************" & strCCNo.Substring(strCCNo.Length - 4, 4)
                Else
                    strCCNo = ""
                End If
                txtCCNo.Text = strCCNo

                'txtCVV2.Text = objEncryption.Decrypt(dsCCInfo.Tables(0).Rows(0)("vcCVV2").ToString())
                ddlCardType.SelectedIndex = -1
                ddlMonth.SelectedIndex = -1
                ddlYear.SelectedIndex = -1
                If Not ddlCardType.Items.FindByValue(dsCCInfo.Tables(0).Rows(0)("numCardTypeID")) Is Nothing Then
                    ddlCardType.Items.FindByValue(dsCCInfo.Tables(0).Rows(0)("numCardTypeID").ToString()).Selected = True
                End If
                If ddlCardType.SelectedValue > 0 Then
                    ddlCardType_SelectedIndexChanged(New Object, Nothing)
                End If
                If Not ddlMonth.Items.FindByValue(CInt(dsCCInfo.Tables(0).Rows(0)("tintValidMonth")).ToString("00")) Is Nothing Then
                    ddlMonth.Items.FindByValue(CInt(dsCCInfo.Tables(0).Rows(0)("tintValidMonth")).ToString("00")).Selected = True
                End If
                If Not ddlYear.Items.FindByValue(dsCCInfo.Tables(0).Rows(0)("intValidYear").ToString()) Is Nothing Then
                    ddlYear.Items.FindByValue(dsCCInfo.Tables(0).Rows(0)("intValidYear").ToString()).Selected = True
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindCardType()
        Try
            objCommon.sb_FillComboFromDBwithSel(ddlCardType, 120, Session("DomainID"))

            Dim objOppInvoice As New OppInvoice
            Dim dsCCInfo As DataSet
            objOppInvoice.DomainID = Session("DomainID")
            objOppInvoice.DivisionID = lngDivID
            objOppInvoice.numCCInfoID = 0
            objOppInvoice.IsDefault = 1
            objOppInvoice.bitflag = 1
            dsCCInfo = objOppInvoice.GetCustomerCreditCardInfo()
            Dim strCard As String = ""
            Dim objEncryption As New QueryStringValues
            For i As Integer = 0 To dsCCInfo.Tables(0).Rows.Count - 1
                strCard = objEncryption.Decrypt(dsCCInfo.Tables(0).Rows(i)("vcCreditCardNo").ToString())
                If strCard.Length > 4 Then
                    dsCCInfo.Tables(0).Rows(i)("vcCreditCardNo") = "************" & strCard.Substring(strCard.Length - 4, 4)
                Else
                    dsCCInfo.Tables(0).Rows(i)("vcCreditCardNo") = "NA"
                End If
            Next
            ddlCards.DataTextField = "vcCreditCardNo"
            ddlCards.DataValueField = "numCCInfoID"
            ddlCards.DataSource = dsCCInfo
            ddlCards.DataBind()
            ddlCards.Items.Insert(0, "--Select One--")
            ddlCards.Items.FindByText("--Select One--").Value = "0"

            If CCommon.ToLong(hdnDefaultCreditCard.Value) > 0 Then
                ddlCards.ClearSelection()
                ddlCards.Items.FindByValue(CCommon.ToLong(hdnDefaultCreditCard.Value)).Selected = True

                LoadSavedCreditCardInfo()
            ElseIf dsCCInfo.Tables(0).Rows.Count > 0 Then
                ddlCards.SelectedIndex = 1
                LoadSavedCreditCardInfo()
            End If

            pnlCreditCardForm.Visible = True

            'Me.ClientScript.RegisterStartupScript(Me.GetType(), "ResizeWindow", "windowResize();", True) 'Bug id 2076

            btnSaveClose.Text = "Charge Credit Card"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindDatagrid()
        Try

            Dim dtBizDocs As DataTable
            Dim objMakeDeposit As New MakeDeposit
            objMakeDeposit.DomainID = Session("DomainID")
            objMakeDeposit.DivisionId = lngDivID
            objMakeDeposit.DepositId = 0
            objMakeDeposit.CurrencyID = Session("BaseCurrencyID")
            dtBizDocs = objMakeDeposit.GetBizDocsForReceivePayment().Tables(0)
            dgOpportunity.DataSource = dtBizDocs
            dgOpportunity.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlCards_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCards.SelectedIndexChanged
        Try
            LoadSavedCreditCardInfo()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlCardType_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlCardType.SelectedIndexChanged
        Try
            'validate Card Type Mapping 
            'Get transaction charge for selected credit card type
            If ddlCardType.SelectedValue > 0 Then
                'SetCreditCardTransValues()
                If TransactionCharge > 0 And TransactionChargeBareBy = enmTransChargeBareBy.Customer Then
                    TransactionCharge = (TransactionCharge * CCommon.ToDecimal(Replace(lblBalanceAmt.Text, ",", "")) / 100)

                    lblAmountToPay.Text = CCommon.ToDecimal(Replace(lblBalanceAmt.Text, ",", "")) + TransactionCharge
                    'txtAmount.Text = CCommon.ToDecimal(Replace(lblBalanceAmt.Text, ",", "")) + TransactionCharge
                    lblTransCharge.Text = CCommon.ToDecimal(TransactionCharge)
                    lblTransChargeLabel.Visible = True
                    lblTransCharge.Visible = True
                Else
                    'txtAmount.Text = CCommon.ToDecimal(Replace(lblBalanceAmt.Text, ",", ""))
                    lblTransCharge.Text = 0
                    lblTransChargeLabel.Visible = False
                    lblTransCharge.Visible = False
                End If
                'If TransactionChargeAccountID = 0 Then litMessage.Text = "Please Set AP account for " & ddlCardType.SelectedItem.Text & " from ""Administration->Domain Details->Accounting->Accounts for Credit Card""."
            Else
                'litMessage.Text = "Please Select a Card Type"
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Public Function FillContact(ByVal ddlCombo As DropDownList)
        Try
            Dim fillCombo As New COpportunities
            With fillCombo
                .DivisionID = lngDivID
                ddlCombo.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
                ddlCombo.DataTextField = "Name"
                ddlCombo.DataValueField = "numcontactId"
                ddlCombo.DataBind()
            End With
            ddlCombo.Items.Insert(0, New ListItem("---Select One---", "0"))
            If ddlCombo.Items.Count >= 2 Then
                ddlCombo.Items(1).Selected = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Sub FillDepositTocombo()
        'Try
        '    Dim dtChartAcntDetails As DataTable
        '    Dim objCOA As New ChartOfAccounting
        '    objCOA.DomainID = Session("DomainId")
        '    objCOA.AccountCode = "010101"
        '    dtChartAcntDetails = objCOA.GetParentCategory()

        '    'Exclude AR As per discussion with jeff. since it created AR aging summary report balance off.
        '    Dim drArray() As DataRow = dtChartAcntDetails.Select("vcAccountTypeCode <> '01010105'")
        '    If drArray.Length > 0 Then
        '        dtChartAcntDetails = drArray.CopyToDataTable()
        '    End If

        '    Dim item As ListItem
        '    For Each dr As DataRow In dtChartAcntDetails.Rows
        '        item = New ListItem()
        '        item.Text = dr("vcAccountName")
        '        item.Value = dr("numAccountID")
        '        item.Attributes("OptionGroup") = dr("vcAccountType")
        '        ddlDepositTo.Items.Add(item)
        '    Next
        '    ddlDepositTo.Items.Insert(0, New ListItem("--Select One --", "0"))
        'Catch ex As Exception
        '    Throw ex
        'End Try
    End Sub

    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:###0.00}", Money)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Dim TransactionCharge As Double
    Dim TransactionChargeAccountID As Long
    Dim TransactionChargeBareBy As Short = 0 ' default by employer
    Dim UndepositedFundAccountID As Long
    Dim lngJournalID As Long
    Dim lngCustomerARAccount As Long
    Dim lngEmpPayRollAccount As Long
    Dim lngDefaultAPAccount As Long

    Dim AmountToPay As Decimal
    Private Sub btnSaveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            SaveNew()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Function SaveNew()
        Try
            'Check if all default accounts are set before making journal entries
            If PerformValidations() Then
                'Process Payment When Payment method is Credit card
                If ProcessPayment() Then
                    Dim decAmount As Decimal = CCommon.ToDecimal(hdnAmountPaid.Value)
                    Dim dtInvoices As DataTable
                    dtInvoices = GetItems()

                    If decAmount <> 0 Then
                        Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                            'Create Single Deposite Entry against multiple invoice payments And Store in DB   
                            If SaveDeposite(dtInvoices) > 0 Then
                                'Create Respective journal entry and save
                                lngJournalID = SaveDataToHeader(decAmount, 0, 0)
                                SaveDataToGeneralJournalDetailsForCashAndCheCks(decAmount)

                                'Order Automation Execution  
                                For Each item As DataGridItem In dgOpportunity.Items
                                    If CType(item.FindControl("chk"), CheckBox).Checked Then
                                        Dim objRule As New OrderAutoRules
                                        objRule.GenerateAutoPO(CCommon.ToLong(CType(item.FindControl("hdnOppBizDocID"), HiddenField).Value))
                                    End If
                                Next
                            End If

                            objTransactionScope.Complete()
                        End Using
                    End If

                    Response.Write("<script>opener.location.reload(true); self.close();</script>")
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function PerformValidations() As Boolean
        Try
            'validate account mapping
            UndepositedFundAccountID = ChartOfAccounting.GetDefaultAccount("UF", Session("DomainID")) 'Undeposited Fund
            If Not UndepositedFundAccountID > 0 Then
                litMessage.Text = "Please contact Administrator for Default Undeposited Funds Account."
                Return False
            End If

            lngDefaultAPAccount = ChartOfAccounting.GetDefaultAccount("PL", Session("DomainID"))

            If Not lngDefaultAPAccount > 0 Then
                litMessage.Text = "Please contact Administrator for Default Payroll Liablity Account."
                Return False
            End If

            lngEmpPayRollAccount = ChartOfAccounting.GetDefaultAccount("EP", Session("DomainID"))

            If Not lngEmpPayRollAccount > 0 Then
                litMessage.Text = "Please contact Administrator for Default Employee Payroll Expense Account."
                Return False
            End If

            If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
            If dgOpportunity.Items.Count > 0 Then 'Multiple Invoice payment
                For Each item As DataGridItem In dgOpportunity.Items
                    lngCustomerARAccount = objOppBizDocs.ValidateCustomerAR_APAccounts("AR", Session("DomainID"), lngDivID)
                    If lngCustomerARAccount = 0 Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please contact Administrator for Set AR and AP Relationship' );", True)
                        Return False
                    End If
                Next
            Else 'single invoice paymentZ
                lngCustomerARAccount = objOppBizDocs.ValidateCustomerAR_APAccounts("AR", Session("DomainID"), lngDivID)
                If lngCustomerARAccount = 0 Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please contact Administrator for Set AR and AP Relationship' );", True)
                    Return False
                End If
            End If

            ' Validation of linked deposit transaction

            If CCommon.ToBool(hdnIsDepositedToAccount.Value) Then
                litMessage.Text = "This transaction has been deposited. If you want to change or delete it, you must edit the deposit it appears on and remove it first."
                Return False
            End If

            If CCommon.ToLong(ddlContact.SelectedValue) = 0 Then
                litMessage.Text = "Please select contact to proceed with credit card payment, its required to process credit card."
                Return False
            End If

            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function ProcessPayment() As Boolean
        Try
            Dim objOppInvoice As New OppInvoice

            'If ProcessCard = True Then
            Dim dtTable As DataTable
            objCommon.DomainID = Session("DomainID")
            dtTable = objCommon.GetPaymentGatewayDetails
            If dtTable.Rows.Count = 0 Then
                litMessage.Text = "Please Configure Payment Gateway"
                Return False
            Else
                Dim responseCode As String = ""
                Dim objPaymentGateway As New PaymentGateway
                objPaymentGateway.DomainID = Session("DomainID")

                If objPaymentGateway.GatewayTransaction(IIf(IsNumeric(hdnAmountPaid.Value), hdnAmountPaid.Value, 0), txtCHName.Text, IIf(txtCCNo.Text.Trim().Contains("*"), Session("CCNO"), txtCCNo.Text.Trim()), txtCVV2.Text, ddlMonth.SelectedValue, ddlYear.SelectedValue, CCommon.ToLong(ddlContact.SelectedValue), ResponseMessage, ReturnTransactionID, responseCode, CreditCardAuthOnly:=False, CreditCardTrack1:=hdnSwipeTrack1.Value) Then

                    'Insert Transaction details into TransactionHistory table
                    Dim objTransactionHistory As New TransactionHistory
                    objTransactionHistory.TransHistoryID = 0
                    objTransactionHistory.DivisionID = lngDivID
                    objTransactionHistory.ContactID = CCommon.ToLong(ddlContact.SelectedValue)
                    objTransactionHistory.OppID = 0
                    objTransactionHistory.OppBizDocsID = 0
                    objTransactionHistory.TransactionID = CCommon.ToString(ReturnTransactionID)
                    objTransactionHistory.TransactionStatus = 2
                    objTransactionHistory.PGResponse = ResponseMessage
                    objTransactionHistory.Type = 2 '1= bizcart & 2 =Internal
                    objTransactionHistory.CapturedAmt = CCommon.ToDecimal(hdnAmountPaid.Value)
                    objTransactionHistory.RefundAmt = 0
                    objTransactionHistory.CardHolder = objCommon.Encrypt(txtCHName.Text)
                    objTransactionHistory.CreditCardNo = objCommon.Encrypt(IIf(txtCCNo.Text.Trim().Contains("*"), Session("CCNO"), txtCCNo.Text.Trim()))
                    objTransactionHistory.Cvv2 = objCommon.Encrypt(txtCVV2.Text)
                    objTransactionHistory.ValidMonth = ddlMonth.SelectedValue
                    objTransactionHistory.ValidYear = ddlYear.SelectedValue
                    objTransactionHistory.SignatureFile = ""
                    objTransactionHistory.UserCntID = Session("UserContactID")
                    objTransactionHistory.DomainID = Session("DomainID")
                    objTransactionHistory.CardType = ddlCardType.SelectedValue
                    objTransactionHistory.ResponseCode = responseCode
                    lngTransactionHistoryID = objTransactionHistory.ManageTransactionHistory()



                    'If True Then
                    If dtTable.Rows(0)("bitSaveCreditCardInfo") = True And CCommon.ToLong(ddlContact.SelectedValue) > 0 Then
                        'Save Credit Card Details
                        Dim objEncryption As New QueryStringValues
                        With objOppInvoice
                            .ContactID = CCommon.ToLong(ddlContact.SelectedValue)
                            .CardHolder = objEncryption.Encrypt(txtCHName.Text.Trim())
                            .CardTypeID = ddlCardType.SelectedValue
                            .CreditCardNumber = objEncryption.Encrypt(IIf(txtCCNo.Text.Trim().Contains("*"), Session("CCNO"), txtCCNo.Text.Trim()))
                            .CVV2 = objEncryption.Encrypt(txtCVV2.Text.Trim())
                            .ValidMonth = ddlMonth.SelectedValue
                            .ValidYear = ddlYear.SelectedValue
                            .UserCntID = Session("UserContactID")
                            .AddCustomerCreditCardInfo()
                        End With
                    End If

                    litMessage.Text = ResponseMessage

                Else
                    litMessage.Text = ResponseMessage
                    Return False

                End If

            End If


            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function GetItems() As DataTable
        Try
            Dim dtMain As New DataTable
            Dim dtCredits As New DataTable
            Dim dtDeposit As New DataTable

            CCommon.AddColumnsToDataTable(dtMain, "numDepositeDetailID,numOppBizDocsID,numOppID,monAmountPaid")
            CCommon.AddColumnsToDataTable(dtDeposit, "numDepositeDetailID,numOppBizDocsID,numOppID,monAmountPaid")

            Dim dr As DataRow

            For Each item As DataGridItem In dgOpportunity.Items
                If CType(item.FindControl("chk"), CheckBox).Checked Then
                    dr = dtMain.NewRow
                    dr("numDepositeDetailID") = CCommon.ToLong(CType(item.FindControl("hdnDepositeDetailID"), HiddenField).Value)
                    dr("numOppBizDocsID") = CCommon.ToLong(CType(item.FindControl("hdnOppBizDocID"), HiddenField).Value)
                    dr("numOppID") = CCommon.ToLong(CType(item.FindControl("hdnOppID"), HiddenField).Value)
                    dr("monAmountPaid") = CCommon.ToDecimal(CType(item.FindControl("txtAmountToPay"), TextBox).Text)

                    dtMain.Rows.Add(dr)
                End If
            Next

            Dim decAmount As Decimal = CCommon.ToDecimal(hdnAmountPaid.Value)

            For Each drMain As DataRow In dtMain.Rows
                If decAmount > 0 Then
                    Dim monAmountPaid As Decimal = CCommon.ToDecimal(drMain("monAmountPaid"))

                    If monAmountPaid > 0 Then
                        dr = dtDeposit.NewRow

                        dr("numDepositeDetailID") = IIf(CCommon.ToLong(hdnDepositePage.Value) = 3, 0, drMain("numDepositeDetailID"))
                        dr("numOppBizDocsID") = drMain("numOppBizDocsID")
                        dr("numOppID") = drMain("numOppID")

                        If decAmount > monAmountPaid Then
                            dr("monAmountPaid") = monAmountPaid
                            decAmount = decAmount - monAmountPaid
                        Else
                            dr("monAmountPaid") = decAmount
                            decAmount = 0
                        End If

                        drMain("monAmountPaid") = monAmountPaid - dr("monAmountPaid")
                        dtDeposit.Rows.Add(dr)
                    End If
                Else
                    Exit For
                End If
            Next

            dtMain.AcceptChanges()

            Return dtDeposit

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    '' To Save Deposit details in DepositDetails table
    Private Function SaveDeposite(ByVal dtInvoices As DataTable) As Long
        Try
            Dim objMakeDeposit As New MakeDeposit
            With objMakeDeposit
                .DivisionId = lngDivID
                .Entry_Date = Date.Now.Date
                .TransactionHistoryID = lngTransactionHistoryID
                .Reference = ""
                .Memo = ""
                .PaymentMethod = 1 'Credit Card
                .DepositeToType = 2 'Group with other undeposited funds
                ''   .numAmount = Replace(lblDepositTotalAmount.Text, ",", "")
                .numAmount = Replace(hdnAmountPaid.Value, ",", "")
                .AccountId = UndepositedFundAccountID
                .RecurringId = 0
                .DepositId = 0
                .UserCntID = Session("UserContactID")
                .DomainID = Session("DomainId")

                Dim ds As New DataSet
                Dim str As String = ""

                If dtInvoices.Rows.Count > 0 Then
                    ds.Tables.Add(dtInvoices.Copy)
                    ds.Tables(0).TableName = "Item"
                    str = ds.GetXml()
                End If

                .StrItems = str
                .Mode = 0
                .DepositePage = 2
                .CurrencyID = Session("BaseCurrencyID")
                .ExchangeRate = 1
                lngDepositeID = .SaveDataToMakeDepositDetails()

            


                Return lngDepositeID
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function SaveDataToHeader(ByVal p_Amount As Decimal, ByVal p_OppBizDocId As Integer, ByVal p_OppId As Integer) As Integer
        Try
            Dim objJEHeader As New JournalEntryHeader
            Dim lngJournalID As Long
            With objJEHeader
                .JournalId = 0
                .RecurringId = 0
                .EntryDate = Now.Date
                .Description = "Payment received:"
                .Amount = p_Amount * 1
                .CheckId = 0
                .CashCreditCardId = 0
                .ChartAcntId = 0
                .OppId = p_OppId
                .OppBizDocsId = p_OppBizDocId
                .DepositId = lngDepositeID
                .BizDocsPaymentDetId = 0
                .IsOpeningBalance = 0
                .LastRecurringDate = Date.Now
                .NoTransactions = 0
                .CategoryHDRID = 0
                .ReturnID = 0
                .CheckHeaderID = 0
                .BillID = 0
                .BillPaymentID = 0
                .UserCntID = Session("UserContactID")
                .DomainID = Session("DomainID")
            End With
            lngJournalID = objJEHeader.Save()
            Return lngJournalID
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub SaveDataToGeneralJournalDetailsForCashAndCheCks(ByVal p_Amount As Decimal)
        Try

            Dim objJEList As New JournalEntryCollection
            Dim objJE As New JournalEntryNew()
            Dim FltExchangeRate As Double
            Dim TotalAmountAtExchangeRateOfOrder As Decimal
            Dim TotalAmountVariationDueToCurrencyExRate As Decimal = 0

            'Determine Exchange Rate
            FltExchangeRate = 1
            'i.e foreign currency payment
            If FltExchangeRate <> 1 Then
                TotalAmountAtExchangeRateOfOrder = 0
                For Each item As DataGridItem In dgOpportunity.Items
                    If CType(item.FindControl("chk"), CheckBox).Checked Then
                        TotalAmountAtExchangeRateOfOrder = TotalAmountAtExchangeRateOfOrder + CCommon.ToDecimal(CType(item.FindControl("txtAmountToPay"), TextBox).Text) * CCommon.ToDouble(CType(item.FindControl("hdnExchangeRateOfOrder"), HiddenField).Value)
                    End If
                Next
                If TotalAmountAtExchangeRateOfOrder > 0 Then
                    TotalAmountVariationDueToCurrencyExRate = TotalAmountAtExchangeRateOfOrder - (p_Amount * FltExchangeRate)
                    If Math.Abs(TotalAmountVariationDueToCurrencyExRate) < 0.0 Then
                        TotalAmountVariationDueToCurrencyExRate = 0
                    End If
                End If
            End If



            'Debit : Undeposit Fund (Amount)
            objJE = New JournalEntryNew()

            objJE.TransactionId = 0
            objJE.DebitAmt = p_Amount * FltExchangeRate 'total amount at current exchange rate
            objJE.CreditAmt = 0
            objJE.ChartAcntId = UndepositedFundAccountID
            objJE.Description = "Payment received to Undeposited Fund" 'IIf(radDepositeTo.Checked, "Amount Paid (" & p_Amount.ToString() & ")  To " & ddlDepositTo.SelectedItem.Text, "Amount Paid (" & p_Amount.ToString() & ")  To Undeposited Fund")
            objJE.CustomerId = lngDivID
            objJE.MainDeposit = 1
            objJE.MainCheck = 0
            objJE.MainCashCredit = 0
            objJE.OppitemtCode = 0
            objJE.BizDocItems = ""
            objJE.Reference = ""
            objJE.PaymentMethod = 1
            objJE.Reconcile = False
            objJE.CurrencyID = Session("BaseCurrencyID")
            objJE.FltExchangeRate = FltExchangeRate
            objJE.TaxItemID = 0
            objJE.BizDocsPaymentDetailsId = 0
            objJE.ContactID = 0
            objJE.ItemID = 0
            objJE.ProjectID = 0
            objJE.ClassID = 0
            objJE.CommissionID = 0
            objJE.ReconcileID = 0
            objJE.Cleared = 0
            objJE.ReferenceType = enmReferenceType.DepositHeader
            objJE.ReferenceID = lngDepositeID

            objJEList.Add(objJE)

            'For Each dr As DataRow In dtItems.Rows
            'Credit: Customer A/R With (Amount)
            objJE = New JournalEntryNew()

            objJE.TransactionId = 0
            objJE.DebitAmt = 0
            objJE.CreditAmt = IIf(TotalAmountAtExchangeRateOfOrder > 0, TotalAmountAtExchangeRateOfOrder, p_Amount * FltExchangeRate)
            objJE.ChartAcntId = lngCustomerARAccount
            objJE.Description = "Payment received-Credited to Customer A/R" '"Credit Customer's AR account"
            objJE.CustomerId = lngDivID
            objJE.MainDeposit = 0
            objJE.MainCheck = 0
            objJE.MainCashCredit = 0
            objJE.OppitemtCode = 0
            objJE.BizDocItems = ""
            objJE.Reference = ""
            objJE.PaymentMethod = 1
            objJE.Reconcile = False
            objJE.CurrencyID = Session("BaseCurrencyID")
            objJE.FltExchangeRate = FltExchangeRate
            objJE.TaxItemID = 0
            objJE.BizDocsPaymentDetailsId = 0
            objJE.ContactID = 0
            objJE.ItemID = 0
            objJE.ProjectID = 0
            objJE.ClassID = 0
            objJE.CommissionID = 0
            objJE.ReconcileID = 0
            objJE.Cleared = 0
            objJE.ReferenceType = enmReferenceType.DepositDetail
            objJE.ReferenceID = 0

            objJEList.Add(objJE)

            objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalID, Session("DomainID"))

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnGo_Click(sender As Object, e As EventArgs) Handles btnGo.Click
        Try
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class