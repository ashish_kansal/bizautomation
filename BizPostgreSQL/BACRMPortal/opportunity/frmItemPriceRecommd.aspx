<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmItemPriceRecommd.aspx.vb" Inherits="BACRMPortal.frmItemPriceRecommd" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
		<title>Price Recommdation</title>
		<script language="javascript">
		function Close()
		{
			window.close()
			return false;
		}
		function AccessParent(a,b,c)
		{
			window.opener.AddPrice(a,b,c)
			window.close();
			return false;
		}
		</script>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
			<table width="100%">
				<tr>
					<td align="right">
						<asp:Button ID="btnClose" Runat="server" Width="50" Text="Close" CssClass="button"></asp:Button>
					</td>
				</tr>
				<tr>
					<td class="text_bold">
						Item Price Recommendations &amp; Quotes from History
					</td>
				</tr>
				<tr>
					<td>
						<asp:datagrid id="dgItemPricemgmt" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
							BorderColor="white">
							<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							<ItemStyle CssClass="is"></ItemStyle>
							<HeaderStyle CssClass="hs"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Date Previously Quoted">
									<ItemTemplate>
										<%# ReturnName(DataBinder.Eval(Container.DataItem, "bintCreatedDate")) %>
									<asp:Label ID=lblLstPrice Runat=server Visible=false Text='<%# DataBinder.Eval(Container.DataItem, "ListPrice") %>'></asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn DataField="numUnitHour" HeaderText="Units"></asp:BoundColumn>
								<asp:BoundColumn DataField="OppStatus" HeaderText="Type of Quotation"></asp:BoundColumn>
								<asp:BoundColumn DataField="vcPOppName" HeaderText="Opportunity/Deal Name"></asp:BoundColumn>
								<asp:BoundColumn DataField="ListPrice" HeaderText="Price" DataFormatString="{0:#,##0.00}"></asp:BoundColumn>
								<asp:TemplateColumn>
									<ItemTemplate>
										<asp:Button ID="btnAdd" Runat="server" CssClass="button" Text="Add" Width="50"></asp:Button>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
