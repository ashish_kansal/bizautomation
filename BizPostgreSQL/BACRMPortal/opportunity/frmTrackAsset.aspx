<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmTrackAsset.aspx.vb" Inherits="BACRMPortal.frmTracksset" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
     <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <title>Track Customer Asset</title>
    <script>
        function Save()
        {
          
            if (document.form1.ddlcompany.value == 0)
            {
                alert ("Select The Company")
                return false
            }
            else
            {
                return true
            }
            
        }
        
    </script>
</head>
<body>
    <form id="form1" runat="server">
    
    <%--<table width="100%">
	    <tr>
	        <td align="left">
	            <table>
	                <tr>
	                    <td class="normal1" align="right">Customer</td>
	                    <td >
	                            <asp:textbox id="txtComp" Runat="server" cssclass="signup" width="125px"></asp:textbox>&nbsp;
	                            <asp:button id="imgCustGo" Runat="server" CssClass="button" Text="Go" Width="30"></asp:button>&nbsp;
	                            <asp:dropdownlist id="ddlcompany" tabIndex="15" Runat="server" CssClass="signup" Width="200" AutoPostBack="false"></asp:dropdownlist>
	                    </td>
	                </tr>
	            </table>
	        </td>
	        	
	        <td align="right">
	                <asp:Button runat="server" CssClass="button" ID="btnSave" Text="Save & Close" />
	                <asp:Button runat="server" CssClass="button" Text="Close"  OnClientClick="javascript:window.close()"/>
	        </td>     
	    </tr>
	</table>
	<br />

	
    <asp:table id="Table3" CellPadding="0" CellSpacing="0" BorderWidth="1" Runat="server" CssClass="aspTable"
	 Width="100%" BorderColor="black" GridLines="None">
	    <asp:TableRow>
	        <asp:TableCell VerticalAlign=Top >
	            <igtbl:ultrawebgrid id="uwItem" Width="100%" DisplayLayout-AllowRowNumberingDefault="ByDataIsland"  runat="server" Browser="Xml"   Height="100%">
				<DisplayLayout AutoGenerateColumns="false"  RowHeightDefault="18"  AllowAddNewDefault="Yes" Version="3.00" SelectTypeRowDefault="Single"
				ViewType="Hierarchical" TableLayout="Auto" SelectTypeCellDefault="Extended" BorderCollapseDefault="Separate" AllowColSizingDefault="Free" 
				Name="uwItem" EnableClientSideRenumbering="true" SelectTypeColDefault="Extended" AllowUpdateDefault="Yes">
				<HeaderStyleDefault VerticalAlign="Middle" Font-Size="8pt" Font-Bold="true"  Font-Names="Arial" BorderStyle="Solid" HorizontalAlign="Left" ForeColor="white" BackColor="#52658C">
                <Padding Left="2px" Right="2px"></Padding>
                <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
                </HeaderStyleDefault>
                <RowSelectorStyleDefault BackColor="White"></RowSelectorStyleDefault>
                 <FrameStyle Width="100%" Cursor="Default" BorderWidth="0px" Font-Size="8pt" Font-Names="Arial" BorderStyle="Double"></FrameStyle>
                <FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
                <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
                </FooterStyleDefault>
                <EditCellStyleDefault BorderWidth="0px" BorderStyle="None"></EditCellStyleDefault>
                <SelectedRowStyleDefault ForeColor="White" BackColor="#666666"></SelectedRowStyleDefault>
                <RowStyleDefault BorderWidth="1px" Font-Size="8pt" Font-Names="Arial" BorderColor="Gray" BorderStyle="Solid" BackColor="White">
                <Padding Left="5px" Right="5px"></Padding>
                <BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
                </RowStyleDefault>
                <RowExpAreaStyleDefault BackColor="LightSteelBlue"></RowExpAreaStyleDefault>
				</DisplayLayout>
					<Bands>
						<igtbl:UltraGridBand AllowDelete="No" AllowAdd="Yes"   >
								<Columns>
									<igtbl:UltraGridColumn AllowUpdate="Yes"  Type="CheckBox" Key="Select">
									</igtbl:UltraGridColumn>
									<igtbl:UltraGridColumn Hidden="true" IsBound="false" BaseColumnName="numItemCode" Key="numItemCode" >
									</igtbl:UltraGridColumn>
									<igtbl:UltraGridColumn Hidden="true" IsBound="false" BaseColumnName="type" Key="type" >
									</igtbl:UltraGridColumn> 
									<igtbl:UltraGridColumn Hidden="true" IsBound="false" BaseColumnName="numWareHouseItmsDtlId" Key="numWareHouseItmsDtlId" >
									</igtbl:UltraGridColumn>
									<igtbl:UltraGridColumn HeaderText="Item Name" Width="40%" AllowUpdate="No" IsBound="false" BaseColumnName="vcitemName" Key="vcitemName" >
									</igtbl:UltraGridColumn>
									<igtbl:UltraGridColumn HeaderText="Serial No" Width="40%" AllowUpdate="No" IsBound="false" BaseColumnName="vcserialno" Key="vcserialno" >
									</igtbl:UltraGridColumn>												                        
									<igtbl:UltraGridColumn HeaderText="Units"  Width="20%"  IsBound="false" Format="###,##0.00"  BaseColumnName="unit" Key="unit" >
									</igtbl:UltraGridColumn>
							</Columns>
						</igtbl:UltraGridBand>												                   
					</Bands>
                </igtbl:ultrawebgrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:table>--%>
    </form>
</body>
</html>
