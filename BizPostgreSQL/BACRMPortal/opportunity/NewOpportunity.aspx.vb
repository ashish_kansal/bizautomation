''Modified By Anoop Jayaraj
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item
Imports Infragistics.WebUI.UltraWebGrid

Public Class NewOpportunity : Inherits BACRMPage

    Dim drowItem As DataRow
    Dim ds As DataSet
    Dim dsTemp As DataSet
    Dim objCommon As CCommon
    Dim objItems As CItems

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    'Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
    '    Try
    '        If Not IsPostBack Then
    '            imgItem.Visible = False
    '            hplImage.Visible = False
    '            'createSet()
    '            Session("Help") = "Opportunity"

    '            objCommon = New CCommon
    '            
    '            m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "NewOpportunity.aspx", Session("UserContactID"), 10, 1)
    '            If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
    '                btnSave.Visible = False
    '                Exit Sub
    '            Else : btnSave.Visible = True
    '            End If
    '            objCommon.sb_FillComboFromDBwithSel(ddlSource, 9, Session("DomainID"))
    '            objCommon.sb_FillComboFromDBwithSel(ddlCampaign, 24, Session("DomainID"))
    '            'objCommon.sb_FillItemFromDB(ddlItems, Session("DomainId"))

    '            If GetQueryStringVal( "CntID") <> "" Or GetQueryStringVal( "CaseID") <> "" Or GetQueryStringVal( "DivID") <> "" Or GetQueryStringVal( "ProID") <> "" Or GetQueryStringVal( "OpID") <> "" Then
    '                If GetQueryStringVal( "CntID") <> "" Then
    '                    objCommon.ContactID = GetQueryStringVal( "CntID")
    '                    objCommon.charModule = "C"
    '                ElseIf GetQueryStringVal( "DivID") <> "" Then
    '                    objCommon.DivisionID = GetQueryStringVal( "DivID")
    '                    objCommon.charModule = "D"
    '                ElseIf GetQueryStringVal( "ProID") <> "" Then
    '                    objCommon.ProID = GetQueryStringVal( "ProID")
    '                    objCommon.charModule = "P"
    '                ElseIf GetQueryStringVal( "OpID") <> "" Then
    '                    objCommon.OppID = GetQueryStringVal( "OpID")
    '                    objCommon.charModule = "O"
    '                ElseIf GetQueryStringVal( "CaseID") <> "" Then
    '                    objCommon.CaseID = GetQueryStringVal( "CaseID")
    '                    objCommon.charModule = "S"
    '                End If
    '                objCommon.GetCompanySpecificValues1()
    '               Dim strCompany, strDivision As String
    '                strCompany = objCommon.GetCompanyName
    '                ddlCompany.Items.Add(strCompany)
    '                ddlCompany.Items.FindByText(strCompany).Value = objCommon.DivisionID
    '                FillContact(ddlContacts)
    '                If Not ddlContacts.Items.FindByValue(objCommon.ContactID) Is Nothing Then
    '                    ddlContacts.Items.FindByValue(objCommon.ContactID).Selected = True
    '                End If
    '            End If
    '        End If
    '        btnSave.Attributes.Add("onclick", "return Save()")
    '        btnClose.Attributes.Add("onclick", "return Close()")
    '        txtunits.Attributes.Add("onkeypress", "CheckNumber(2)")
    '        txtprice.Attributes.Add("onkeypress", "CheckNumber(1)")
    '        If ddlItems.SelectedIndex <> -1 Then
    '            Dim lngDiv As Long
    '            If ddlCompany.SelectedIndex = -1 Then
    '                lngDiv = 0
    '            Else : lngDiv = ddlCompany.SelectedItem.Value
    '            End If
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Public Function FillCustomer()
    '    Try

    '        If objCommon Is Nothing Then objCommon = New CCommon
    '        With objCommon
    '            .DomainID = Session("DomainID")
    '            .Filter = Trim(txtCompName.Text) & "%"
    '            .UserCntID = Session("UserContactID")
    '            ddlCompany.DataSource = objCommon.PopulateOrganization.Tables(0).DefaultView
    '            ddlCompany.DataTextField = "vcCompanyname"
    '            ddlCompany.DataValueField = "numDivisionID"
    '            ddlCompany.DataBind()
    '        End With
    '        ddlCompany.Items.Insert(0, New ListItem("---Select One---", "0"))
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    'Private Sub ddlCompany_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
    '    Try
    '        If ddlCompany.SelectedIndex > 0 Then
    '            ddlContacts.Enabled = True
    '            FillContact(ddlContacts)
    '            If ddlOppType.SelectedItem.Value = 2 Then
    '                Dim objCommon As New CCommon
    '                objCommon.sb_FillComboPurchaseItem(ddlItems, ddlCompany.SelectedValue, Session("DomainId"))
    '            End If
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Public Function FillContact(ByVal ddlCombo As DropDownList)
    '    Try
    '        Dim fillCombo As New COpportunities
    '        With fillCombo
    '            .DivisionID = ddlCompany.SelectedItem.Value
    '            ddlCombo.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
    '            ddlCombo.DataTextField = "Name"
    '            ddlCombo.DataValueField = "numcontactId"
    '            ddlCombo.DataBind()
    '        End With
    '        ddlCombo.Items.Insert(0, New ListItem("---Select One---", "0"))
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    'Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    '    Try
    '        Opportunity()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo.Click
    '    Try
    '        FillCustomer()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Sub Opportunity()
    '    Try
    '        Dim arrOutPut() As String
    '        Dim objOpportunity As New MOpportunity
    '        objOpportunity.OpportunityId = 0
    '        objOpportunity.ContactID = ddlContacts.SelectedItem.Value
    '        objOpportunity.DivisionID = ddlCompany.SelectedItem.Value
    '        objOpportunity.OpportunityName = ddlCompany.SelectedItem.Text & "-" & Format(Now(), "MMMM")
    '        objOpportunity.UserCntID = Session("UserContactID")
    '        objOpportunity.EstimatedCloseDate = Now
    '        objOpportunity.CampaignID = ddlCampaign.SelectedItem.Value
    '        objOpportunity.PublicFlag = 0
    '        objOpportunity.Source = ddlSource.SelectedItem.Value
    '        objOpportunity.DomainID = Session("DomainId")
    '        objOpportunity.OppType = ddlOppType.SelectedItem.Value
    '        ' objOpportunity.DropShipped = IIf(chkDropShipped.Checked = False, 0, 1)
    '        Dim TotalAmount As Decimal = 0
    '        If Not Session("Data") Is Nothing Then
    '            dsTemp = Session("Data")
    '            TotalAmount = CType(dsTemp.Tables(0).Compute("Sum(monTotAmount)", ""), Decimal)
    '            objOpportunity.strItems = dsTemp.GetXml
    '        End If
    '        arrOutPut = objOpportunity.Save()
    '        Session("NewPurChecked") = Nothing
    '        Try
    '            Dim objAlerts As New CAlerts
    '            If ddlOppType.SelectedItem.Value <> 1 Then
    '                ' Sending Mail When it is purchase Order

    '                Dim dtDetails As DataTable
    '                objAlerts.AlertDTLID = 22 'Alert DTL ID for sending alerts in opportunities
    '                objAlerts.DomainID = Session("DomainID")
    '                dtDetails = objAlerts.GetIndAlertDTL
    '                If dtDetails.Rows.Count > 0 Then
    '                    If dtDetails.Rows(0).Item("tintAlertOn") = 1 Then
    '                        Dim dtEmailTemplate As DataTable
    '                        Dim objDocuments As New DocumentList
    '                        objDocuments.GenDocID = dtDetails.Rows(0).Item("numEmailTemplate")
    '                        objDocuments.DomainID = Session("DomainID")
    '                        dtEmailTemplate = objDocuments.GetDocByGenDocID
    '                        If dtEmailTemplate.Rows.Count > 0 Then
    '                            Dim objSendMail As New Email
    '                            Dim dtMergeFields As New DataTable
    '                            Dim drNew As DataRow
    '                            dtMergeFields.Columns.Add("OppID")
    '                            dtMergeFields.Columns.Add("DealAmount")
    '                            dtMergeFields.Columns.Add("Organization")
    '                            dtMergeFields.Columns.Add("ContactName")
    '                            drNew = dtMergeFields.NewRow
    '                            drNew("OppID") = arrOutPut(1)
    '                            drNew("DealAmount") = TotalAmount
    '                            drNew("ContactName") = ddlContacts.SelectedItem.Text
    '                            drNew("Organization") = ddlCompany.SelectedItem.Text
    '                            dtMergeFields.Rows.Add(drNew)

    '                            Dim dtEmail As DataTable
    '                            objAlerts.AlertDTLID = 22
    '                            objAlerts.DomainID = Session("DomainId")
    '                            dtEmail = objAlerts.GetAlertEmails
    '                            Dim strCC As String = ""
    '                            Dim p As Integer
    '                            For p = 0 To dtEmail.Rows.Count - 1
    '                                strCC = strCC & dtEmail.Rows(p).Item("vcEmailID") & ";"
    '                            Next
    '                            strCC = strCC.TrimEnd(";")
    '                            objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), "", Session("UserEmail"), strCC, dtMergeFields)
    '                        End If
    '                    End If
    '                End If
    '            End If

    '            objAlerts.AlertDTLID = 19
    '            objAlerts.ContactID = ddlCompany.SelectedItem.Value ''pass CompanyID eventhough declared as contact
    '            If objAlerts.GetCountCompany > 0 Then
    '                ' Sending Mail for Key Organization Opportunity
    '                Dim dtDetails As DataTable
    '                objAlerts.AlertDTLID = 19 'Alert DTL ID for sending alerts in opportunities
    '                objAlerts.DomainID = Session("DomainID")
    '                dtDetails = objAlerts.GetIndAlertDTL
    '                If dtDetails.Rows.Count > 0 Then
    '                    If dtDetails.Rows(0).Item("tintAlertOn") = 1 Then
    '                        Dim dtEmailTemplate As DataTable
    '                        Dim objDocuments As New DocumentList
    '                        objDocuments.GenDocID = dtDetails.Rows(0).Item("numEmailTemplate")
    '                        objDocuments.DomainID = Session("DomainID")
    '                        dtEmailTemplate = objDocuments.GetDocByGenDocID
    '                        If dtEmailTemplate.Rows.Count > 0 Then
    '                            Dim objSendMail As New Email
    '                            Dim dtMergeFields As New DataTable
    '                            Dim drNew As DataRow
    '                            dtMergeFields.Columns.Add("OppID")
    '                            dtMergeFields.Columns.Add("DealAmount")
    '                            dtMergeFields.Columns.Add("Organization")
    '                            drNew = dtMergeFields.NewRow
    '                            drNew("OppID") = arrOutPut(1)
    '                            drNew("DealAmount") = TotalAmount
    '                            drNew("Organization") = ddlCompany.SelectedItem.Text

    '                            Dim dtEmail As DataTable
    '                            objAlerts.AlertDTLID = 19
    '                            objAlerts.DomainID = Session("DomainId")
    '                            dtEmail = objAlerts.GetAlertEmails
    '                            Dim strCC As String = ""
    '                            Dim p As Integer
    '                            For p = 0 To dtEmail.Rows.Count - 1
    '                                strCC = strCC & dtEmail.Rows(p).Item("vcEmailID") & ";"
    '                            Next
    '                            strCC = strCC.TrimEnd(";")

    '                            dtMergeFields.Rows.Add(drNew)
    '                            Dim objCommon As New CCommon
    '                            objCommon.byteMode = 1
    '                            objCommon.ContactID = Session("UserContactID")
    '                            objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), IIf(dtDetails.Rows(0).Item("tintCCManager") = 1, objCommon.GetManagerEmail, ""), Session("UserEmail"), strCC, dtMergeFields)
    '                        End If
    '                    End If
    '                End If
    '            End If
    '        Catch ex As Exception

    '        End Try
    '        Dim strScript As String = "<script language=JavaScript>"
    '        strScript += "window.opener.reDirectPage('../opportunity/frmOpportunities.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&opId=" & arrOutPut(0) & "'); self.close();"
    '        strScript += "</script>"
    '        If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub ddlOppType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOppType.SelectedIndexChanged
    '    Try
    '        If objCommon Is Nothing Then objCommon = New CCommon
    '        objCommon.sb_FillItemFromDB(ddlItems, Session("DomainId"))
    '        If ddlOppType.SelectedValue = 1 Then
    '            pnlSales.Visible = True
    '            pnlPurrchase.Visible = False
    '            If ddlItems.SelectedIndex <> -1 Then
    '                If ddlCompany.SelectedIndex <> -1 Then hplPrice.Attributes.Add("onclick", "return openItem(" & ddlItems.SelectedItem.Value & ",0," & ddlCompany.SelectedValue & ")")
    '                hplUnit.Attributes.Add("onclick", "return openUnit('" & ddlItems.SelectedItem.Value & "','" & txtunits.Text & "')")
    '            End If
    '            hplPrice.CssClass = "hyperlink"
    '            hplPrice.Text = "Price"
    '        ElseIf ddlOppType.SelectedValue = 2 Then
    '            pnlSales.Visible = False
    '            pnlPurrchase.Visible = True
    '            hplPrice.Text = "Purchase Price"
    '            If ddlItems.SelectedIndex <> -1 Then hplUnit.Attributes.Add("onclick", "return openUnit('" & ddlItems.SelectedItem.Value.Split(",")(0) & "','" & txtunits.Text & "')")
    '        End If
    '        Session("Data") = Nothing
    '        createSet()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub ddlItems_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlItems.SelectedIndexChanged
    '    Try
    '        btnAdd.Text = "Add"
    '        LoadItemDetails()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Sub LoadItemDetails()
    '    Try
    '        If ddlItems.SelectedIndex > 0 Then
    '            Dim uwCoumn As UltraGridColumn
    '            Dim dtTable As DataTable
    '            If objItems Is Nothing Then objItems = New CItems
    '            objItems.ItemCode = ddlItems.SelectedItem.Value
    '            dtTable = objItems.ItemDetails
    '            If dtTable.Rows.Count = 1 Then
    '                ddltype.SelectedItem.Selected = False
    '                ddltype.Items.FindByValue(dtTable.Rows(0).Item("charItemType")).Selected = True
    '                txtdesc.Text = dtTable.Rows(0).Item("txtItemDesc")
    '                If Not IsDBNull(dtTable.Rows(0).Item("vcPathForImage")) Then
    '                    If dtTable.Rows(0).Item("vcPathForImage") <> "" Then
    '                        imgItem.Visible = True
    '                        hplImage.Visible = True
    '                        imgItem.ImageUrl = Session("SiteType") & "//" & Request.ServerVariables("SERVER_NAME") & "/" & ConfigurationManager.AppSettings("PortalVirtualDirectoryName") & "/Documents/Docs/" & dtTable.Rows(0).Item("vcPathForImage")
    '                        hplImage.Attributes.Add("onclick", "return OpenImage(" & ddlItems.SelectedItem.Value & ")")
    '                    Else
    '                        imgItem.Visible = False
    '                        hplImage.Visible = False
    '                    End If
    '                Else
    '                    imgItem.Visible = False
    '                    hplImage.Visible = False
    '                End If
    '                If dtTable.Rows(0).Item("charItemType") = "P" Or dtTable.Rows(0).Item("charItemType") = "A" Then
    '                    tblItemWareHouse.Visible = True
    '                    txtunits.Visible = False
    '                    hplUnit.Visible = False
    '                    populateDataSet(objItems, ddlItems.SelectedItem.Value)
    '                    Dim i As Integer
    '                    If uwItemSel.Bands(0).Columns.Count > 10 Then
    '                        i = uwItemSel.Bands(0).Columns.Count
    '                        While i > 10
    '                            uwItemSel.Bands(0).Columns.RemoveAt(9)
    '                            i = uwItemSel.Bands(0).Columns.Count
    '                        End While
    '                    End If

    '                    If dtTable.Rows(0).Item("bitSerialized") = True And ddlOppType.SelectedValue = 1 Then
    '                        uwItemSel.Bands(0).Columns.FromKey("Units").Hidden = True
    '                        uwItemSel.Bands(1).Columns.FromKey("Select").Hidden = False
    '                    Else
    '                        uwItemSel.Bands(1).Columns.FromKey("Select").Hidden = True
    '                        uwItemSel.Bands(0).Columns.FromKey("Units").Hidden = False
    '                    End If
    '                    Dim dtAttributes As DataTable
    '                    dtAttributes = ds.Tables(2)
    '                    Dim drAttrRow As DataRow

    '                    If dtTable.Rows(0).Item("bitSerialized") = False Then
    '                        For Each drAttrRow In dtAttributes.Rows
    '                            uwCoumn = New UltraGridColumn
    '                            uwCoumn.HeaderText = drAttrRow("Fld_label")
    '                            uwCoumn.AllowUpdate = Infragistics.WebUI.UltraWebGrid.AllowUpdate.No
    '                            uwCoumn.BaseColumnName = drAttrRow("Fld_label")
    '                            uwCoumn.Key = drAttrRow("Fld_label")
    '                            uwCoumn.IsBound = True
    '                            uwItemSel.Bands(0).Columns.Add(uwCoumn)
    '                        Next
    '                        uwItemSel.Bands(0).Columns.FromKey("Units").Move(uwItemSel.Bands(0).Columns.Count - 1)
    '                    End If
    '                    uwItemSel.DataSource = ds
    '                    uwItemSel.DataBind()
    '                    uwItemSel.Bands(0).DataKeyField = "numWareHouseItemID"
    '                    uwItemSel.Bands(1).DataKeyField = "numWareHouseItmsDTLID"
    '                Else
    '                    txtunits.Visible = True
    '                    hplUnit.Visible = True
    '                    tblItemWareHouse.Visible = False
    '                End If
    '            End If
    '            objItems.strItemCodes = ddlItems.SelectedValue
    '            dtTable = objItems.GetOptAccWareHouses

    '            If dtTable.Rows.Count > 0 Then
    '                tblOptions.Visible = True
    '                Dim i As Integer
    '                If uwOption.Columns.Count > 11 Then
    '                    i = uwOption.Bands(0).Columns.Count
    '                    While i > 12
    '                        uwOption.Bands(0).Columns.RemoveAt(10)
    '                        i = uwOption.Bands(0).Columns.Count
    '                    End While
    '                End If
    '                If dtTable.Columns.Count > 1 Then

    '                    For i = 11 To dtTable.Columns.Count - 1
    '                        uwCoumn = New UltraGridColumn
    '                        uwCoumn.HeaderText = dtTable.Columns(i).ColumnName
    '                        uwCoumn.AllowUpdate = Infragistics.WebUI.UltraWebGrid.AllowUpdate.No
    '                        uwCoumn.BaseColumnName = dtTable.Columns(i).ColumnName
    '                        uwCoumn.Key = dtTable.Columns(i).ColumnName
    '                        uwCoumn.IsBound = True
    '                        uwCoumn.Width = Unit.Percentage(10)
    '                        uwOption.Bands(0).Columns.Add(uwCoumn)
    '                    Next
    '                    uwOption.Bands(0).Columns.FromKey("Price").Move(uwOption.Bands(0).Columns.Count - 1)
    '                    uwOption.Bands(0).Columns.FromKey("Units").Move(uwOption.Bands(0).Columns.Count - 1)
    '                End If

    '                uwOption.DataSource = dtTable
    '                uwOption.DataBind()
    '                uwOption.Visible = True
    '                If Not Session("Data") Is Nothing Then
    '                    Dim dtItem As DataTable
    '                    dsTemp = Session("Data")
    '                    dtItem = dsTemp.Tables(0)
    '                    Dim dv As DataView
    '                    dv = New DataView(dtItem)
    '                    dv.RowFilter = "ItemType = 'Options'"
    '                    Dim uwRow As UltraGridRow
    '                    For i = 0 To dv.Count - 1
    '                        For Each uwRow In uwOption.Rows
    '                            If uwRow.Cells.FromKey("numWareHouseItemID").Value = dv(i).Item("numWarehouseItmsID") Then
    '                                uwRow.Cells.FromKey("Units").Value = dv(i).Item("numUnitHour")
    '                                uwRow.Cells.FromKey("Price").Value = dv(i).Item("monPrice")
    '                            End If
    '                        Next
    '                    Next
    '                End If
    '            Else : tblOptions.Visible = False
    '            End If
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub populateDataSet(ByVal objItems As CItems, ByVal lngItemCode As Long)
    '    Try
    '        objItems.ItemCode = lngItemCode
    '        ds = objItems.GetItemWareHouses()
    '        If ddlOppType.SelectedValue = 2 Then
    '            ds.Tables(1).Rows.Clear()
    '            ds.AcceptChanges()
    '        End If
    '        If ds.Relations.Count < 1 Then
    '            ds.Tables(0).TableName = "WareHouse"
    '            ds.Tables(1).TableName = "SerializedItems"
    '            ds.Tables(0).PrimaryKey = New DataColumn() {ds.Tables(0).Columns("numWareHouseItemID")}
    '            ds.Tables(1).PrimaryKey = New DataColumn() {ds.Tables(1).Columns("numWareHouseItmsDTLID")}
    '            ds.Relations.Add("Customers", ds.Tables(0).Columns("numWareHouseItemID"), ds.Tables(1).Columns("numWareHouseItemID"))
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Sub createSet()
    '    Try
    '        dsTemp = New DataSet
    '        Dim dtItem As New DataTable
    '        Dim dtSerItem As New DataTable
    '        dtItem.Columns.Add("numoppitemtCode")
    '        dtItem.Columns.Add("numItemCode")
    '        dtItem.Columns.Add("numUnitHour")
    '        dtItem.Columns.Add("monPrice")
    '        dtItem.Columns.Add("monTotAmount", GetType(Decimal))
    '        dtItem.Columns.Add("numSourceID")
    '        dtItem.Columns.Add("vcItemDesc")
    '        dtItem.Columns.Add("numWarehouseID")
    '        dtItem.Columns.Add("vcItemName")
    '        dtItem.Columns.Add("Warehouse")
    '        dtItem.Columns.Add("numWarehouseItmsID")
    '        dtItem.Columns.Add("ItemType")

    '        dtSerItem.Columns.Add("numWarehouseItmsDTLID")
    '        dtSerItem.Columns.Add("numWItmsID")
    '        dtSerItem.Columns.Add("numoppitemtCode")
    '        dtSerItem.Columns.Add("vcSerialNo")
    '        dtItem.TableName = "Item"
    '        dtSerItem.TableName = "SerialNo"

    '        dsTemp.Tables.Add(dtItem)
    '        dsTemp.Tables.Add(dtSerItem)
    '        dtItem.PrimaryKey = New DataColumn() {dsTemp.Tables(0).Columns("numoppitemtCode")}
    '        'dsTemp.Relations.Add("Item", ds.Tables(0).Columns("numWarehouseID"), ds.Tables(1).Columns("numWarehouseID"))
    '        Session("Data") = dsTemp
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
    '    Try
    '        dsTemp = Session("Data")
    '        Dim dtItem As DataTable
    '        Dim dtSerItem As DataTable
    '        dtItem = dsTemp.Tables(0)
    '        dtSerItem = dsTemp.Tables(1)
    '        Dim dr As DataRow
    '        If btnAdd.Text = "Add" Then

    '            If ddltype.SelectedValue = "P" Or ddltype.SelectedValue = "A" Then
    '                Dim ugRow, ugRow1 As UltraGridRow
    '                If uwItemSel.Bands(0).Columns.FromKey("Units").Hidden = False Then

    '                    For Each ugRow In uwItemSel.Rows
    '                        If ugRow.Cells.FromKey("Units").Value > 0 Then
    '                            dr = dtItem.NewRow
    '                            dr("numoppitemtCode") = CType(IIf(IsDBNull(dtItem.Compute("MAX(numoppitemtCode)", "")), 0, dtItem.Compute("MAX(numoppitemtCode)", "")), Integer) + 1
    '                            dr("numItemCode") = ddlItems.SelectedValue
    '                            dr("numUnitHour") = ugRow.Cells.FromKey("Units").Value
    '                            dr("monPrice") = txtprice.Text
    '                            dr("monTotAmount") = IIf(IsDBNull(dr("numUnitHour")), 0, dr("numUnitHour")) * IIf(IsDBNull(dr("monPrice")), 0, dr("monPrice"))
    '                            dr("vcItemDesc") = txtdesc.Text
    '                            dr("numWarehouseID") = ugRow.Cells.FromKey("numWareHouseID").Value
    '                            dr("vcItemName") = ddlItems.SelectedItem.Text
    '                            dr("Warehouse") = ugRow.Cells.FromKey("vcWarehouse").Value
    '                            dr("numWarehouseItmsID") = ugRow.Cells.FromKey("numWareHouseItemID").Value
    '                            dr("ItemType") = ddltype.SelectedItem.Text
    '                            dtItem.Rows.Add(dr)
    '                        End If
    '                    Next
    '                Else
    '                    Dim iCount As Integer = 0
    '                    For Each ugRow In uwItemSel.Rows
    '                        If ugRow.HasChildRows = True Then
    '                            Dim dr1 As DataRow
    '                            For Each ugRow1 In ugRow.Rows
    '                                If ugRow1.Cells.FromKey("Select").Value = True Then
    '                                    iCount = iCount + 1
    '                                    If iCount = 1 Then
    '                                        dr = dtItem.NewRow
    '                                        dr("numoppitemtCode") = CType(IIf(IsDBNull(dtItem.Compute("MAX(numoppitemtCode)", "")), 0, dtItem.Compute("MAX(numoppitemtCode)", "")), Integer) + 1
    '                                        dr("numItemCode") = ddlItems.SelectedValue
    '                                        'dr("numUnitHour") = iCount
    '                                        dr("monPrice") = txtprice.Text
    '                                        'dr("monTotAmount") = IIf(IsDBNull(dr("numUnitHour")), 0, dr("numUnitHour")) * IIf(IsDBNull(dr("monPrice")), 0, dr("monPrice"))
    '                                        dr("vcItemDesc") = txtdesc.Text
    '                                        dr("numWarehouseID") = ugRow.Cells.FromKey("numWareHouseID").Value
    '                                        dr("Warehouse") = ugRow.Cells.FromKey("vcWarehouse").Value
    '                                        dr("vcItemName") = ddlItems.SelectedItem.Text
    '                                        dr("numWarehouseItmsID") = ugRow.Cells.FromKey("numWareHouseItemID").Value
    '                                        dtItem.Rows.Add(dr)
    '                                    End If
    '                                    dr1 = dtSerItem.NewRow
    '                                    dr1("numWarehouseItmsDTLID") = ugRow1.Cells.FromKey("numWareHouseItmsDTLID").Value
    '                                    dr1("numoppitemtCode") = dr("numoppitemtCode")
    '                                    dr1("numWItmsID") = dr("numWarehouseItmsID")
    '                                    dr1("vcSerialNo") = ugRow1.Cells.FromKey("vcSerialNo").Value
    '                                    dtSerItem.Rows.Add(dr1)
    '                                End If
    '                            Next
    '                            If iCount > 0 Then
    '                                dr("numUnitHour") = iCount
    '                                dr("monPrice") = txtprice.Text
    '                                dr("monTotAmount") = iCount * dr("monPrice")
    '                                dr("ItemType") = ddltype.SelectedItem.Text
    '                                dr.AcceptChanges()
    '                                dtItem.AcceptChanges()
    '                                iCount = 0
    '                            End If
    '                        End If
    '                    Next
    '                End If
    '            Else
    '                dr = dtItem.NewRow
    '                dr("numoppitemtCode") = CType(IIf(IsDBNull(dtItem.Compute("MAX(numoppitemtCode)", "")), 0, dtItem.Compute("MAX(numoppitemtCode)", "")), Integer) + 1
    '                dr("numItemCode") = ddlItems.SelectedValue
    '                dr("numUnitHour") = txtunits.Text
    '                dr("monPrice") = txtprice.Text
    '                dr("monTotAmount") = IIf(IsDBNull(dr("numUnitHour")), 0, dr("numUnitHour")) * IIf(IsDBNull(dr("monPrice")), 0, dr("monPrice"))
    '                dr("vcItemDesc") = txtdesc.Text
    '                dr("numWarehouseID") = DBNull.Value
    '                dr("numWarehouseItmsID") = DBNull.Value
    '                dr("vcItemName") = ddlItems.SelectedItem.Text
    '                dr("ItemType") = ddltype.SelectedItem.Text
    '                dtItem.Rows.Add(dr)
    '            End If
    '            dsTemp.AcceptChanges()

    '            Dim uwRow As Infragistics.WebUI.UltraWebGrid.UltraGridRow
    '            dtItem = dsTemp.Tables(0)
    '            Dim dv As DataView
    '            dv = New DataView(dtItem)
    '            'Dim i As Integer
    '            ' dv.RowFilter = "ItemType=Options"
    '            If tblOptions.Visible = True Then

    '                For Each uwRow In uwOption.Rows
    '                    dv.RowFilter = "ItemType = 'Options' and numWarehouseItmsID = " & uwRow.Cells.FromKey("numWareHouseItemID").Value
    '                    If uwRow.Cells.FromKey("Units").Value <> "" And uwRow.Cells.FromKey("Price").Value <> "" And dv.Count = 0 Then

    '                        dr = dtItem.NewRow
    '                        dr("numoppitemtCode") = CType(IIf(IsDBNull(dtItem.Compute("MAX(numoppitemtCode)", "")), 0, dtItem.Compute("MAX(numoppitemtCode)", "")), Integer) + 1
    '                        dr("numItemCode") = uwRow.Cells.FromKey("numItemCode").Value
    '                        dr("numUnitHour") = uwRow.Cells.FromKey("Units").Value
    '                        dr("monPrice") = uwRow.Cells.FromKey("Price").Value
    '                        dr("monTotAmount") = IIf(IsDBNull(dr("numUnitHour")), 0, dr("numUnitHour")) * IIf(IsDBNull(dr("monPrice")), 0, dr("monPrice"))
    '                        dr("Warehouse") = uwRow.Cells.FromKey("vcWarehouse").Value
    '                        dr("vcItemDesc") = ""
    '                        dr("numWarehouseID") = uwRow.Cells.FromKey("numWareHouseID").Value
    '                        dr("numWarehouseItmsID") = uwRow.Cells.FromKey("numWareHouseItemID").Value
    '                        dr("vcItemName") = uwRow.Cells.FromKey("vcItemName").Value
    '                        dr("ItemType") = "Options"
    '                        'dr("Op_Flag") = 1
    '                        dtItem.Rows.Add(dr)
    '                    ElseIf (uwRow.Cells.FromKey("Units").Value <> "" And uwRow.Cells.FromKey("Price").Value <> "" And dv.Count > 0) Then
    '                        'dr("numoppitemtCode") = CType(IIf(IsDBNull(dtItem.Compute("MAX(numoppitemtCode)", "")), 0, dtItem.Compute("MAX(numoppitemtCode)", "")), Integer) + 1
    '                        'dr("numItemCode") = uwRow.Cells.FromKey("numItemCode").Value
    '                        dr = dtItem.Rows.Find(dv(0).Item("numWarehouseItmsID"))
    '                        dr("numUnitHour") = uwRow.Cells.FromKey("Units").Value
    '                        dr("monPrice") = uwRow.Cells.FromKey("Price").Value
    '                        dr("monTotAmount") = IIf(IsDBNull(dr("numUnitHour")), 0, dr("numUnitHour")) * IIf(IsDBNull(dr("monPrice")), 0, dr("monPrice"))
    '                        dr("Warehouse") = uwRow.Cells.FromKey("vcWarehouse").Value
    '                        dr("vcItemDesc") = ""
    '                        dr("numWarehouseID") = uwRow.Cells.FromKey("numWareHouseID").Value
    '                        dr("numWarehouseItmsID") = uwRow.Cells.FromKey("numWareHouseItemID").Value
    '                        dr("vcItemName") = uwRow.Cells.FromKey("vcItemName").Value
    '                        dr("ItemType") = "Options"
    '                        'dr("Op_Flag") = 2
    '                    End If
    '                Next
    '                dv.RowFilter = ""
    '                dtItem.AcceptChanges()
    '                dsTemp.AcceptChanges()
    '                Session("Data") = dsTemp
    '                ucItem.DataSource = dsTemp
    '                ucItem.DataBind()
    '                tblOptions.Visible = False
    '            End If
    '        Else
    '            btnAdd.Text = "Add"
    '            Dim ugRow, ugRow1 As UltraGridRow

    '            For Each dr In dtItem.Rows
    '                If ddltype.SelectedValue = "P" Or ddltype.SelectedValue = "A" Then

    '                    If uwItemSel.Bands(0).Columns.FromKey("Units").Hidden = False Then
    '                        For Each ugRow In uwItemSel.Rows
    '                            If ugRow.Hidden = False Then
    '                                If ugRow.Cells.FromKey("numWareHouseItemID").Value = dr("numWarehouseItmsID") Then
    '                                    dr("numUnitHour") = ugRow.Cells.FromKey("Units").Value
    '                                    dr("monPrice") = txtprice.Text
    '                                    dr("monTotAmount") = IIf(IsDBNull(dr("numUnitHour")), 0, dr("numUnitHour")) * IIf(IsDBNull(dr("monPrice")), 0, dr("monPrice"))
    '                                End If
    '                            End If
    '                        Next
    '                    Else
    '                        For Each ugRow In uwItemSel.Rows
    '                            If ugRow.Hidden = False Then
    '                                If ugRow.Cells.FromKey("numWareHouseItemID").Value = dr("numWarehouseItmsID") Then
    '                                    Dim iCount As Integer = 0
    '                                    Dim dr1 As DataRow
    '                                    For Each ugRow1 In ugRow.Rows
    '                                        For Each dr1 In dtSerItem.Rows
    '                                            If ugRow1.Cells.FromKey("numWareHouseItmsDTLID").Value = dr1("numWareHouseItmsDTLID") Then
    '                                                dr1.Delete()
    '                                            End If
    '                                        Next
    '                                        dtSerItem.AcceptChanges()
    '                                    Next
    '                                    For Each ugRow1 In ugRow.Rows
    '                                        If ugRow1.Cells.FromKey("Select").Value = True Then
    '                                            dr1 = dtSerItem.NewRow
    '                                            dr1("numWarehouseItmsDTLID") = ugRow1.Cells.FromKey("numWareHouseItmsDTLID").Value
    '                                            dr1("numoppitemtCode") = dr("numoppitemtCode")
    '                                            dr1("numWItmsID") = dr("numWarehouseItmsID")
    '                                            dr1("vcSerialNo") = ugRow1.Cells.FromKey("vcSerialNo").Value
    '                                            dtSerItem.Rows.Add(dr1)
    '                                            iCount = iCount + 1
    '                                        End If
    '                                    Next
    '                                    dr("numUnitHour") = iCount
    '                                    dr("monPrice") = txtprice.Text
    '                                    dr("monTotAmount") = IIf(IsDBNull(dr("numUnitHour")), 0, dr("numUnitHour")) * IIf(IsDBNull(dr("monPrice")), 0, dr("monPrice"))
    '                                    If iCount = 0 Then
    '                                        dtItem.Rows.Remove(dr)
    '                                    End If
    '                                End If
    '                            End If
    '                        Next
    '                    End If
    '                Else
    '                    If dr("numItemCode") = ddlItems.SelectedValue Then
    '                        dr("numUnitHour") = txtunits.Text
    '                        dr("monPrice") = txtprice.Text
    '                        dr("monTotAmount") = IIf(IsDBNull(dr("numUnitHour")), 0, dr("numUnitHour")) * IIf(IsDBNull(dr("monPrice")), 0, dr("monPrice"))
    '                    End If
    '                End If
    '                dr.AcceptChanges()
    '            Next
    '            dsTemp.AcceptChanges()
    '        End If
    '        If dtSerItem.ParentRelations.Count = 0 Then dsTemp.Relations.Add("Item", dsTemp.Tables(0).Columns("numoppitemtCode"), dsTemp.Tables(1).Columns("numoppitemtCode"))
    '        Session("Data") = dsTemp
    '        tblProducts.Visible = True
    '        ucItem.DataSource = dsTemp
    '        ucItem.DataBind()
    '        tblItemWareHouse.Visible = False
    '        tblOptions.Visible = False
    '        txtunits.Text = ""
    '        txtprice.Text = ""
    '        ddlItems.SelectedIndex = 0
    '        txtdesc.Text = ""
    '        btnEditCancel.Visible = False
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub ucItem_ClickCellButton(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.CellEventArgs) Handles ucItem.ClickCellButton
    '    Try
    '        ddlItems.SelectedItem.Selected = False
    '        If Not ddlItems.Items.FindByValue(e.Cell.Tag.Split("~")(0)) Is Nothing Then ddlItems.Items.FindByValue(e.Cell.Tag.Split("~")(0)).Selected = True
    '        LoadItemDetails()
    '        dsTemp = Session("Data")
    '        Dim dtItem As DataTable
    '        Dim dtSerItem As DataTable
    '        dtItem = dsTemp.Tables(0)
    '        dtSerItem = dsTemp.Tables(1)
    '        Dim dv As DataView
    '        dv = New DataView(dtItem)
    '        dv.RowFilter = "numItemCode=" & ddlItems.SelectedValue
    '        Dim i, k As Integer
    '        For i = 0 To dv.Count - 1
    '            If ddltype.SelectedValue = "P" Or ddltype.SelectedValue = "A" Then
    '                Dim ugRow, ugRow1 As UltraGridRow
    '                If uwItemSel.Bands(0).Columns.FromKey("Units").Hidden = False Then
    '                    txtprice.Text = String.Format("{0:###,##0.00}", CDec(dv(i).Item("monPrice")))
    '                    For Each ugRow In uwItemSel.Rows
    '                        If ugRow.Cells.FromKey("numWareHouseItemID").Value = e.Cell.Tag.Split("~")(1) Then
    '                            ugRow.Cells.FromKey("Units").Value = dv(i).Item("numUnitHour")
    '                        Else : ugRow.Hidden = True
    '                        End If
    '                    Next
    '                Else
    '                    txtprice.Text = String.Format("{0:###,##0.00}", CDec(dv(i).Item("monPrice")))
    '                    For Each ugRow In uwItemSel.Rows
    '                        If ugRow.Cells.FromKey("numWareHouseItemID").Value = e.Cell.Tag.Split("~")(1) Then
    '                            If ugRow.HasChildRows = True Then
    '                                ugRow.Expanded = True
    '                            End If
    '                            For Each ugRow1 In ugRow.Rows
    '                                Dim dv1 As DataView
    '                                dv1 = New DataView(dtSerItem)
    '                                dv1.RowFilter = "numoppitemtCode=" & dv(i).Item("numoppitemtCode")
    '                                For k = 0 To dv1.Count - 1
    '                                    If ugRow1.Cells.FromKey("numWareHouseItmsDTLID").Value = dv1(k).Item("numWarehouseItmsDTLID") Then
    '                                        ugRow1.Cells.FromKey("Select").Value = True
    '                                    End If
    '                                Next
    '                            Next
    '                        Else : ugRow.Hidden = True
    '                        End If
    '                    Next
    '                End If
    '            Else
    '                txtunits.Text = String.Format("{0:###,##0.00}", CDec(dv(i).Item("numUnitHour")))
    '                txtprice.Text = String.Format("{0:###,##0.00}", CDec(dv(i).Item("monPrice")))
    '            End If
    '        Next
    '        btnAdd.Text = "Update"
    '        tblProducts.Visible = False
    '        tblOptions.Visible = False
    '        tblProducts.Visible = False
    '        btnEditCancel.Visible = True
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub ucItem_DeleteRowBatch(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles ucItem.DeleteRowBatch
    '    Try
    '        dsTemp = Session("Data")
    '        Dim table As DataTable = dsTemp.Tables(e.Row.Band.BaseTableName)
    '        If table.ParentRelations.Count > 0 Then
    '            Dim row As DataRow = table.Rows.Find(e.Row.Cells.FromKey("numWarehouseItmsDTLID").Value)
    '            table.Rows.Remove(row)
    '        Else
    '            Dim row As DataRow = table.Rows.Find(e.Row.Cells.FromKey("numoppitemtCode").Value)
    '            table.Rows.Remove(row)
    '        End If
    '        dsTemp.AcceptChanges()
    '        Session("Data") = dsTemp
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub ucItem_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles ucItem.InitializeRow
    '    Try
    '        If e.Row.HasParent = False Then
    '            e.Row.Cells.FromKey("Action").Tag = e.Row.Cells.FromKey("numItemCode").Value & "~" & e.Row.Cells.FromKey("numWareHouseItemID").Value
    '            e.Row.Cells.FromKey("Action").Value = "Edit"
    '            e.Row.Cells.FromKey("Action").Column.CellButtonStyle.CssClass = "button"
    '            e.Row.Cells.FromKey("Action").Column.Width = Unit.Pixel(50)
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub uwItemSel_InitializeLayout(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.LayoutEventArgs) Handles uwItemSel.InitializeLayout
    '    Try
    '        uwItemSel.Bands(1).Columns(1).Hidden = True
    '        uwItemSel.Bands(1).Columns(2).Hidden = True
    '        uwItemSel.Bands(1).Columns(3).HeaderText = "Serial No"
    '        uwItemSel.Bands(1).Columns(4).Hidden = True

    '        Dim dtAttributes As DataTable
    '        dtAttributes = ds.Tables(2)
    '        Dim dr As DataRow
    '        Dim shtBand As Short
    '        If uwItemSel.Bands(0).Columns.Count > 10 Then
    '            shtBand = 0
    '        Else : shtBand = 1
    '        End If
    '        For Each dr In dtAttributes.Rows
    '            If dr("fld_type") = "Drop Down List Box" Then
    '                uwItemSel.Bands(shtBand).Columns.FromKey(dr("Fld_label")).Type = ColumnType.DropDownList
    '                If objCommon Is Nothing Then objCommon = New CCommon
    '                Dim attVlist As New Infragistics.WebUI.UltraWebGrid.ValueList
    '                Dim dtValues As DataTable
    '                dtValues = objCommon.GetMasterListItems(dr("numlistid"), Session("DomainID"))
    '                attVlist.ValueListItems.Add("0", "-")
    '                Dim drValueList As DataRow
    '                For Each drValueList In dtValues.Rows
    '                    attVlist.ValueListItems.Add(CStr(drValueList("numListItemID")), drValueList("vcData"))
    '                Next
    '                uwItemSel.Bands(shtBand).Columns.FromKey(dr("Fld_label")).ValueList = attVlist
    '            ElseIf dr("fld_type") = "Check box" Then
    '                uwItemSel.Bands(shtBand).Columns.FromKey(dr("Fld_label")).Type = ColumnType.CheckBox
    '            End If
    '        Next
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub btnEditCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditCancel.Click
    '    Try
    '        tblProducts.Visible = True
    '        tblOptions.Visible = False
    '        tblItemWareHouse.Visible = False
    '        txtunits.Text = ""
    '        txtprice.Text = ""
    '        ddlItems.SelectedIndex = 0
    '        txtdesc.Text = ""
    '        btnEditCancel.Visible = False
    '        btnAdd.Text = "Add"
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

End Class



