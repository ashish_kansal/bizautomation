<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmOpportunities.aspx.vb"
    Inherits="BACRMPortal.frmOpportunities" %>

<%@ Register Assembly="Infragistics35.WebUI.UltraWebTab.v8.1, Version=8.1.20081.2046, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebTab" TagPrefix="igtab" %>
<%@ Register TagPrefix="menu1" TagName="Menu" Src="../common/topbar.ascx" %>
<%@ Register Assembly="Infragistics35.WebUI.UltraWebGrid.v8.1, Version=8.1.20081.2046, Culture=neutral, PublicKeyToken=7dd5c3163f2cd0cb"
    Namespace="Infragistics.WebUI.UltraWebGrid" TagPrefix="igtbl" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Reference Control="../include/calandar.ascx" %>
<%@ Register Assembly="RadComboBox.Net2" Namespace="Telerik.WebControls" TagPrefix="rad" %>
<%@ Register Assembly="RadGrid.Net2" Namespace="Telerik.WebControls" TagPrefix="rad" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <link href="../css/lists.css" type="text/css" rel="STYLESHEET" />
    <title>Opportunities</title>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">

    <script language="javascript" type="text/javascript">
        function DropShip() {
            if (document.all['uwOppTab__ctl3_chkDropShip'].checked == true) {
                if (typeof (uwOppTab__ctl3_trWareHouse) != 'undefined') {
                    uwOppTab__ctl3_trWareHouse.style.display = 'none';
                }
                if (typeof (uwOppTab__ctl3_trAttributes) != 'undefined') {
                    uwOppTab__ctl3_trAttributes.style.display = 'none';
                }
            }
            else {
                if (typeof (uwOppTab__ctl3_trWareHouse) != 'undefined') {
                    uwOppTab__ctl3_trWareHouse.style.display = '';
                }
                if (typeof (uwOppTab__ctl3_trAttributes) != 'undefined') {
                    uwOppTab__ctl3_trAttributes.style.display = '';
                }
            }

        }
        function OptDropShip() {

            if (document.all['uwOppTab__ctl3_chkOptDropShip'].checked == true) {
                if (typeof (uwOppTab__ctl3_trOptAttributes) != 'undefined') {
                    uwOppTab__ctl3_trOptAttributes.style.display = 'none';
                }
                if (typeof (uwOppTab__ctl3_trOptWareHouse) != 'undefined') {
                    trOptWareHouse.style.display = 'none';
                }
            }
            else {
                if (typeof (uwOppTab__ctl3_trOptAttributes) != 'undefined') {
                    uwOppTab__ctl3_trOptAttributes.style.display = '';
                }
                if (typeof (uwOppTab__ctl3_trOptWareHouse) != 'undefined') {
                    uwOppTab__ctl3_trOptWareHouse.style.display = '';
                }
            }

        }
        function DeleteRows() {


            if (typeof (igtbl_getGridById('uwOppTab__ctl3_ucItem')) != 'undefined') {
                var grid = igtbl_getGridById('uwOppTab__ctl3_ucItem');
                var i;
                var RowsSelected = 0;
                for (i = grid.Rows.length - 1; i >= 0; i--) {
                    var row = grid.Rows.getRow(i);
                    if (row.getCellFromKey("Select").getValue() == 'true') {
                        row.remove();
                    }
                }


            }
            return false;
        }
        function GetSelectedItem(combobox) {
            combobox.ClientDataString = document.Form1.txtDivId.value + '~' + document.Form1.all['ddlOppType'].value;
        }
        function reDirectPage(url) {
            window.location.href = url
        }
        function openTrackAsset(a, b) {
            window.open("../opportunity/frmTrackAsset.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&opId=" + a + "&DivId=" + b, '', 'toolbar=no,titlebar=no,left=300,top=200,width=800,height=500,scrollbars=yes,resizable=yes')
            return false;
        }
        function fn_GoToURL(varURL) {

            if ((varURL != '') && (varURL.substr(0, 7) == 'http://') && (varURL.length > 7)) {
                var LoWindow = window.open(varURL, "", "");
                LoWindow.focus();
            }
            return false;
        }
        function ShowlinkedProjects(a) {
            window.open("../opportunity/frmLinkedProjects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&opId=" + a, '', 'toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeletMsg() {
            var bln = confirm("You�re about to remove the Stage from this Process, all stage data will be deleted")
            if (bln == true) {
                return true;
            }
            else {
                return false;
            }
        }
        function OpenDocuments(a) {
            window.open("../Documents/frmSpecDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=O&yunWE=" + a, '', 'toolbar=no,titlebar=no,top=200,width=700,height=450,left=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function DealCompleted() {
            /*  if (document.Form1.uwOppTab__ctl0_calShip_txtDate.value=='')
            {
            alert("Enter Ship Date");
            return false;
            }*/
            //			alert("This Deal will now be removed from the 'Open Deals' section, and will reside only in the 'Closed Deals' section within the Organization the deal is for. Except for BizDocs or any Projects that depend on BizDocs - Modifications to Deal Details, Milestones & Stages, Associated Contacts, and Products / Services, will no longer be allowed.")
            if (confirm("This Deal will now be removed from the 'Open Deals' section, and will reside only in the 'Closed Deals' section within the Organization the deal is for. Except for BizDocs or any Projects that depend on BizDocs - Modifications to Deal Details, Milestones & Stages, Associated Contacts, and Products / Services, will no longer be allowed.")) {
                return true;
            }
            else {
                return false;
            }

        }
        function CannotShip() {

            alert("You can't ship at this time because you don't have enough quantity on hand to support your shipment. Your options are to modify your order, or replenish inventory (to check inventory click on the edit link within the line item, then the value in the 'Products/Services' column):")
            return false;
        }
        function AlertMsg() {
            if (document.Form1.uwOppTab__ctl0_calShip_txtDate.value == '') {
                alert("Enter Ship Date");
                return false;
            }
            if (confirm("Please note that after a 'Received' or 'Shipped' request is executed, except for the BizDocs and any Projects that depend on BizDocs - Additional changes will not be permitted on this Deal (i.e. it will be frozen). Do you wish to continue ?")) {
                return true;
            }
            else {
                return false;
            }
        }
        function OpenImage(a) {
            window.open('../opportunity/frmFullImage.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ItemCode=' + a, '', 'toolbar=no,titlebar=no,left=300, top=100,width=600,height=500,scrollbars=yes,resizable=yes');
            return false;
        }
        function Update(ddl, txt, txtPrice) {
            if (ddl.value == 0) {
                alert("Select Item")
                ddl.focus()
                return false;
            }
            if (txt.value == '') {
                alert("Enter Units")
                txt.focus()
                return false;
            }
            if (txtPrice.value == '') {
                alert("Enter Price")
                txtPrice.focus()
                return false;
            }
        }

        function AddPrice(a, b, c) {
            if (b == 1) {
                document.Form1.txtprice.value = a;
                return false;
            }
            else {
                document.all('uwOppTab__ctl3_' + c).value = a;
                return false;
            }
        }
        function openCompetition(a) {
            window.open("../opportunity/frmCompetition.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ItemCode=" + a, '', 'toolbar=no,titlebar=no,left=300,top=450,width=700,height=150,scrollbars=yes,resizable=yes')
            /*document.all['IframeComp'].src="../opportunity/frmCompetition.aspx?ItemCode="+a ;
            document.all['divCompetition'].style.visibility = "visible";*/
            return false;
        }

        function openUnit(a) {
            a = document.all[a].value
            window.open('../opportunity/frmUnitdtlsForItem.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ItemCode=' + a, '', 'toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')
        }
        function openItem(a, b, c, d, e) {
            if (uwOppTab__ctl3_radCmbtem.GetValue() == "") {
                a = 0
            }
            else {
                a = uwOppTab__ctl3_radCmbtem.GetValue();
            }
            if (document.all[d].value == 1) {
                b = 0
            }
            if ((document.all[d].value == 1) && (typeof (document.all['uwOppTabxxctl3xuwItemSel']) != 'undefined')) {
                b = 0
                var grid = igtbl_getGridById('uwOppTabxxctl3xuwItemSel');
                var i;
                var j;
                for (i = 0; i < grid.Rows.length; i++) {
                    var row = grid.Rows.getRow(i);
                    var ChildRows = row.getChildRows();
                    if (ChildRows != null) {
                        for (j = 0; j < ChildRows.length; j++) {

                            var childRow = ChildRows[j];
                            //alert(childRow.getCell(0))
                            //var test =igtbl_getElementById(childRow.id)
                            //alert(test)

                            var Test = igtbl_getRowById(childRow.id)
                            if (Test.getCellFromKey("Select").getValue() == 'true') {
                                b = b + 1
                            }
                            //rows.getRow(0).getCell(0).setValue(check);

                        }

                    }
                }

            }
            else if (document.all[d].value == 0) {
                if (document.all[b].value == "") {
                    b = 0
                }
                else {
                    b = document.all[b].value
                }
            }
            var CalPrice = 0;
            if (document.all['hdKit'].value == 'True' && (typeof (document.all['uwOppTab__ctl3_uKit']) != 'undefined')) {
                var grid = igtbl_getGridById('uwOppTab__ctl3_uKit');
                var i;
                for (i = 0; i < grid.Rows.length; i++) {
                    var row = grid.Rows.getRow(i);

                    var str = row.getCellFromKey("monListPrice").getValue();
                    //alert(parseFloat(str.split('/')[0]))
                    //alert(parseFloat(str.split('/')[0])*row.getCellFromKey("numQtyItemsReq").getValue())
                    //var str='asass/asass'
                    CalPrice = CalPrice + (parseFloat(str.split('/')[0]) * row.getCellFromKey("numQtyItemsReq").getValue())

                }
            }
            //alert(CalPrice)
            if (a != 0 && b != 0) {
                window.open('../opportunity/frmItemPriceRecommd.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ItemCode=' + a + '&Unit=' + b + '&DivID=' + c + '&Type=Edit' + '&OppType=' + e + '&CalPrice=' + CalPrice, '', 'toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')

            }
        }
        function openOptItem(a, b, c) {
            if (document.all[a].selectedIndex < 1) {
                a = 0
            }
            else {
                a = document.all[a].value
            }
            if (document.all[b].value == "") {
                b = 0
            }
            else {
                b = document.all[b].value
            }
            if (a != 0 && b != 0) {
                window.open('../opportunity/frmItemPriceRecommd.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ItemCode=' + a + '&Unit=' + b + '&DivID=' + c + '&OptItem=' + 1 + '&Type=Edit' + '&CalPrice=0', '', 'toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')

            }
        }
        function ShowWindow(Page, q, att) {

            if (att == 'show') {
                document.all[Page].style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.all[Page].style.visibility = "hidden";
                return false;

            }
        }
        function CheckNumber(cint) {
            if (cint == 1) {
                if (!(window.event.keyCode > 47 && window.event.keyCode < 58 || window.event.keyCode == 44 || window.event.keyCode == 46)) {
                    window.event.keyCode = 0;
                }
            }
            if (cint == 2) {
                if (!(window.event.keyCode > 47 && window.event.keyCode < 58)) {
                    window.event.keyCode = 0;
                }
            }

        }
        function Save(cint) {
            if (cint == 1) {
                if (document.Form1.ddlCompanyName.value == 0) {
                    alert("Select Customer");
                    document.Form1.uwOppTab.tabIndex = 0;
                    document.Form1.ddlCompanyName.focus();
                    return false;
                }
                if (document.Form1.ddlTaskContact.selectedIndex == 0) {
                    alert("Select Contact");
                    document.Form1.uwOppTab.tabIndex = 0;
                    document.Form1.ddlTaskContact.focus();
                    return false;
                }
            }

            if (document.Form1.uwOppTab__ctl0_calDue_txtDate.value == '') {
                alert("Enter Due Date");
                document.Form1.uwOppTab.tabIndex = 0;
                return false;
            }
            if (typeof (document.all['uwOppTab__ctl1_chkDClosed']) != 'undefined') {
                if (document.all['uwOppTab__ctl1_chkDClosed'].checked == true) {
                    if (document.Form1.all['uwOppTab__ctl1_ddlClReason'].value == 0) {
                        alert("Select Conclusion Analysis")
                        document.Form1.uwOppTab.tabIndex = 1;
                        document.Form1.all['uwOppTab__ctl1_ddlClReason'].focus()
                        return false;
                    }
                }
            }
            if (typeof (document.all['uwOppTab__ctl1_chkDlost']) != 'undefined') {
                if (document.all['uwOppTab__ctl1_chkDlost'].checked == true) {
                    if (document.Form1.all['uwOppTab__ctl1_ddlClReason'].value == 0) {
                        alert("Select Conclusion Analysis")
                        document.Form1.uwOppTab.tabIndex = 1;
                        document.Form1.all['uwOppTab__ctl1_ddlClReason'].focus()
                        return false;
                    }

                }
            }

        }
        function AddContact() {
            if (document.Form1.uwOppTab__ctl2_ddlcompany.value == 0) {
                alert("Select Customer");
                document.Form1.uwOppTab.tabIndex = 2;
                document.Form1.uwOppTab__ctl2_ddlcompany.focus();
                return false;
            }
            if (document.Form1.uwOppTab__ctl2_ddlAssocContactId.value == 0) {
                alert("Select Contact");
                document.Form1.uwOppTab.tabIndex = 2;
                document.Form1.uwOppTab__ctl2_ddlAssocContactId.focus();
                return false;
            }
            var str;
            for (i = 0; i < document.Form1.elements.length; i++) {
                if (i <= 9) {
                    str = '0' + (i + 1)
                }
                else {
                    str = i + 1
                }
                if (typeof (document.Form1.all['uwOppTab__ctl2_dgContact_ctl' + str + '_txtContactID']) != 'undefined') {
                    if (document.Form1.all['uwOppTab__ctl2_dgContact_ctl' + str + '_txtContactID'].value == document.Form1.uwOppTab__ctl2_ddlAssocContactId.value) {
                        alert("Associated contact is already added");
                        return false;
                    }
                }
            }

        }
        function Add(a) {



            if (uwOppTab__ctl3_radCmbtem.GetValue() == "") {
                alert("Select Item")
                return false;
            }
            if (document.Form1.txtHidValue.value == "False") {
                if (a != '') {
                    var ddlIDs = a.split(",");
                    for (i = 0; i < ddlIDs.length; i++) {
                        if (typeof (document.all['uwOppTab__ctl3_' + ddlIDs[i]]) != 'undefined') {
                            if (document.all['uwOppTab__ctl3_' + ddlIDs[i].split("~")[0]].value == "0") {
                                alert("Select " + ddlIDs[i].split("~")[1])
                                document.all['uwOppTab__ctl3_' + ddlIDs[i].split("~")[0]].focus();
                                return false;
                            }
                        }

                    }
                }

            }
            if (document.all['uwOppTab__ctl3_chkDropShip'].checked == false) {
                if (typeof (document.Form1.uwOppTab__ctl3_ddlWarehouse) != 'undefined') {
                    if (document.Form1.uwOppTab__ctl3_ddlWarehouse.value == 0) {
                        alert("Select Warehouse")
                        document.Form1.uwOppTab__ctl3_ddlWarehouse.focus();
                        return false;
                    }
                }
            }
            if (typeof (document.all['uwOppTab__ctl3_RadGrid1']) != 'undefined') {
                var grid = uwOppTab__ctl3_RadGrid1;
                var i;
                var RowsSelected = 0;
                for (i = 0; i < uwOppTab__ctl3_RadGrid1.MasterTableView.Rows.length; i++) {
                    if (uwOppTab__ctl3_RadGrid1.MasterTableView.Rows[i].Control.cells(3).outerText == "") {

                    }
                    else {
                        if (typeof (document.Form1.uwOppTab__ctl3_ddlWarehouse) != 'undefined') {
                            if (uwOppTab__ctl3_RadGrid1.MasterTableView.Rows[i].Control.cells(3).outerText == document.Form1.uwOppTab__ctl3_ddlWarehouse.value) {
                                alert("This Item is already added to opportunity. Please Edit the details")
                                return false;
                            }
                        }
                    }

                }


            }


            if (document.Form1.txtHidValue.value == "False" || document.Form1.txtHidValue.value == "" || document.Form1.ddlOppType.value == 2 || document.all['uwOppTab__ctl3_chkDropShip'].checked == true) {
                if (document.Form1.uwOppTab__ctl3_txtunits.value == "") {
                    alert("Enter Units")
                    document.Form1.uwOppTab__ctl3_txtunits.focus();
                    return false;
                }
            }
            else {


                if (document.all['uwOppTab__ctl3_uwItemSel'] != 'undefined') {
                    var grid = igtbl_getGridById('uwOppTab__ctl3_uwItemSel');
                    var i;
                    var j;
                    var rep = 'false';
                    var RowsSelected = 0;
                    for (i = 0; i < grid.Rows.length; i++) {
                        var row = grid.Rows.getRow(i);
                        row.setExpanded()

                        var ChildRows = row.getChildRows();

                        if (ChildRows != null) {
                            for (j = 0; j < ChildRows.length; j++) {
                                var ChildRow = ChildRows[j];
                                var child = igtbl_getRowById(ChildRow.id);
                                if (child.getCellFromKey("Select").getValue().toLowerCase() == 'true') {
                                    rep = 'true';
                                    RowsSelected = RowsSelected + 1;
                                }
                            }
                        }
                        else {
                            rep = 'true';
                        }

                    }

                    if (rep == 'false') {
                        alert('Select Serialized Items')
                        return false
                    }
                    else {
                        row.setExpanded(false)
                    }
                }

            }


            if (document.Form1.txtHidValue.value == "False" || document.Form1.txtHidValue.value == "") {
                if (document.Form1.uwOppTab__ctl3_txtunits.value == "") {
                    alert("Enter Units")
                    document.Form1.uwOppTab__ctl3_txtunits.focus();
                    return false;
                }
            }
            else {

                if (typeof (document.all['uwOppTabxxctl3xuwItemSel']) != 'undefined') {
                    var grid = igtbl_getGridById('uwOppTabxxctl3xuwItemSel');
                    var i;
                    var j;
                    var rep = 'false';
                    for (i = 0; i < grid.Rows.length; i++) {

                        var row = grid.Rows.getRow(i);
                        row.setExpanded()
                        var ChildRows = row.getChildRows();

                        if (ChildRows != null) {
                            for (j = 0; j < ChildRows.length; j++) {

                                var ChildRow = ChildRows[j];
                                var child = igtbl_getRowById(ChildRow.id);
                                if (child.getCellFromKey("Select").getValue().toLowerCase() == 'true') {
                                    rep = 'true';
                                }
                            }
                        }
                        else {
                            rep = 'true';
                        }
                    }

                    if (rep == 'false') {
                        alert('Select Serialized Items')
                        return false
                    }
                    else {
                        row.setExpanded(false)
                    }
                }
            }


            if (document.Form1.uwOppTab__ctl3_txtprice.value == "") {
                alert("Enter Price")
                document.Form1.uwOppTab__ctl3_txtprice.focus();
                return false;
            }
        }
        function AddOption(a) {
            if (document.Form1.uwOppTab__ctl3_ddlOptItem.value == 0) {
                alert("Select Option Item")
                document.Form1.uwOppTab__ctl3_ddlOptItem.focus();
                return false;
            }
            if (document.Form1.txtHidOptValue.value == "False") {
                if (a != '') {
                    var ddlIDs = a.split(",");
                    for (i = 0; i < ddlIDs.length; i++) {
                        if (document.all['uwOppTab__ctl3_' + ddlIDs[i].split("~")[0]].value == "0") {
                            alert("Select " + ddlIDs[i].split("~")[1])
                            document.all['uwOppTab__ctl3_' + ddlIDs[i].split("~")[0]].focus();
                            return false;
                        }
                    }
                }

            }
            if (typeof (document.Form1.uwOppTab__ctl3_ddlOptWarehouse) != 'undefined') {
                if (document.Form1.uwOppTab__ctl3_ddlOptWarehouse.value == 0) {
                    alert("Select Warehouse")
                    document.Form1.uwOppTab__ctl3_ddlOptWarehouse.focus();
                    return false;
                }
            }
            if (typeof (document.all['uwOppTab__ctl3_RadGrid1']) != 'undefined') {
                var grid = uwOppTab__ctl3_RadGrid1;
                var i;
                var RowsSelected = 0;
                for (i = 0; i < uwOppTab__ctl3_RadGrid1.MasterTableView.Rows.length; i++) {
                    if (uwOppTab__ctl3_RadGrid1.MasterTableView.Rows[i].Control.cells(3).outerText == "") {

                    }
                    else {
                        if (typeof (document.Form1.uwOppTab__ctl3_ddlOptWarehouse) != 'undefined') {
                            if (uwOppTab__ctl3_RadGrid1.MasterTableView.Rows[i].Control.cells(3).outerText == document.Form1.uwOppTab__ctl3_ddlOptWarehouse.value) {
                                alert("This Item is already added to opportunity. Please Edit the details")
                                return false;
                            }
                        }
                    }

                }


            }
            if (document.Form1.txtHidOptValue.value == "False" || document.Form1.txtHidOptValue.value == "") {
                if (document.Form1.uwOppTab__ctl3_txtOptUnits.value == "") {
                    alert("Enter Units")
                    document.Form1.uwOppTab__ctl3_txtOptUnits.focus();
                    return false;
                }
            }


            if (document.Form1.uwOppTab__ctl3_txtOptPrice.value == "") {
                alert("Enter Price")
                document.Form1.uwOppTab__ctl3_txtOptPrice.focus();
                return false;
            }

        }

        function deleteItem() {
            var bln;
            bln = window.confirm("Delete Seleted Row - Are You Sure ?")
            if (bln == true) {
                return true;
            }
            else {
                return false;
            }
        }

        function OpenBiz(a) {
            window.open('../opportunity/frmBizDocs.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a, '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes');
            return false;
        }


        function OpenDependency(a, b, c, d) {
            window.open('../opportunity/frmOppDependency.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OPPStageID=' + a + '&Opid=' + b + '&PerID=' + c + '&StgDtlId=' + d, '', 'toolbar=no,titlebar=no,left=500, top=300,width=500,height=200,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenExpense(a, b, c, d, e) {
            window.open('../opportunity/frmOppExpense.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OPPStageID=' + a + '&Opid=' + b + '&PerID=' + c + '&StgDtlId=' + d + '&DivId=' + e, '', 'toolbar=no,titlebar=no,left=500, top=300,width=650,height=300,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenTime(a, b, c, d, e) {
            window.open('../opportunity/frmOppTime.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OPPStageID=' + a + '&Opid=' + b + '&PerID=' + c + '&StgDtlId=' + d + '&DivId=' + e, '', 'toolbar=no,titlebar=no,left=500, top=300,width=650,height=300,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenSubStage(a, b, c, d) {
            window.open('../opportunity/frmOPPSubStagesaspx.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OPPStageID=' + a + '&Opid=' + b + '&PerID=' + c + '&StgDtlId=' + d, '', 'toolbar=no,titlebar=no,left=500, top=300,width=500,height=200,scrollbars=yes,resizable=yes');
            return false;
        }
        function CheckBoxCon(a, b, c) {
            if (parseInt(c) == 1) {
                document.all['uwOppTab__ctl1_chkStage~' + a + '~' + b].checked = true
            }
            else {
                document.all['uwOppTab__ctl1_chkStage~' + a + '~' + b].checked = false
            }
        }
        function ValidateCheckBox(cint) {
            if (cint == 1) {
                if (document.all['uwOppTab__ctl1_chkDClosed'].checked == true) {
                    if (document.all['uwOppTab__ctl1_chkDlost'].checked == true) {
                        alert("The Deal is already Lost !")
                        document.all['uwOppTab__ctl1_chkDClosed'].checked = false
                        return false;
                    }

                }
            }
            if (cint == 2) {
                if (document.all['uwOppTab__ctl1_chkDlost'].checked == true) {
                    if (document.all['uwOppTab__ctl1_chkDClosed'].checked == true) {
                        alert("The Deal is already Closed !")
                        document.all['uwOppTab__ctl1_chkDlost'].checked = false
                        return false;
                    }
                    document.Form1.all['uwOppTab__ctl0_chkActive'].checked = false;
                }
            }

        }
        function ShowWindow(Page, q, att) {
            if (att == 'show') {
                document.all[Page].style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.all[Page].style.visibility = "hidden";
                return false;

            }

        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function OpenCreateOpp(a, b) {
            window.open('../opportunity/frmCreateSalesPurFromOpp.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppID=' + a + '&OppType=' + b + '&Created=false', '', 'toolbar=no,titlebar=no,left=100,top=100,width=1000,height=700,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenConfSerItem(a) {
            window.open('../opportunity/frmAddSerializedItem.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppID=' + a, '', 'toolbar=no,titlebar=no,left=100,top=100,width=1000,height=700,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenTransfer(url) {
            window.open(url, '', "width=340,height=150,status=no,top=100,left=150");
            return false;
        }
        function beforeSelectEvent(owner, item, evt) {
            //alert(item.getIndex())
            if (item.getIndex() == 4) {
                var iframe = document.getElementById('uwOppTab__ctl4_IframeBiz')
                if (iframe.src == '') {
                    iframe.src = '../opportunity/frmBizDocs.aspx?OpID=' + document.getElementById('txtOppId').value
                }
                return false;
            }
        }
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <menu1:Menu ID="webmenu1" runat="server"></menu1:Menu>
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
        EnableViewState="true">
        <ContentTemplate>
            <table width="100%" align="center">
                <tr>
                    <td>
                        <table bordercolor="black" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td class="tr1" align="center">
                                    <b>Record Owner: </b>
                                    <asp:Label ID="lblRecordOwner" runat="server" ForeColor="Black"></asp:Label>
                                </td>
                                <td class="td1" width="1" height="18">
                                </td>
                                <td class="tr1" align="center">
                                    <b>Created By: </b>
                                    <asp:Label ID="lblCreatedBy" runat="server" ForeColor="Black"></asp:Label>
                                </td>
                                <td class="td1" width="1" height="18">
                                </td>
                                <td class="tr1" align="center">
                                    <b>Last Modified By: </b>
                                    <asp:Label ID="lblLastModifiedBy" runat="server" ForeColor="Black"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td class="normal1" align="center">
                                    <asp:Label ID="lblCustomerType" runat="server"></asp:Label>
                                    : <u>
                                        <asp:HyperLink ID="hplCustomer" runat="server" CssClass="hyperlink"></asp:HyperLink></u>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblDealCompletedDate" runat="server"
                                        CssClass="text"></asp:Label>
                                </td>
                                <td align="right">
                                    <asp:Button ID="btnTransfer" runat="server" Text="Transfer Ownership" CssClass="button"
                                        Width="120"></asp:Button>
                                    <asp:Button ID="btnTrackAsset" runat="server" CssClass="button" Text="Track As Customer Asset"
                                        Width="175" />
                                    <asp:Button ID="btnCreateOpp" runat="server" CssClass="button" Visible="false"></asp:Button>
                                    <asp:Button ID="btnConfSerItems" runat="server" CssClass="button" Visible="false"
                                        Text="Configure Serialized Item"></asp:Button>
                                    <asp:Button ID="btnReceivedOrShipped" runat="server" CssClass="button" Visible="false">
                                    </asp:Button>
                                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"></asp:Button>
                                    <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Save &amp; Close">
                                    </asp:Button>
                                    <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close"></asp:Button>
                                    <asp:Button ID="btnActdelete" runat="server" CssClass="Delete" Text="r"></asp:Button>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <igtab:UltraWebTab ID="uwOppTab" runat="server" ThreeDEffect="True" BorderStyle="Solid"
                Width="100%" BarHeight="0" BorderWidth="0">
                <DefaultTabStyle Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Arial">
                </DefaultTabStyle>
                <ClientSideEvents BeforeSelectedTabChange="beforeSelectEvent" />
                <RoundedImage LeftSideWidth="7" RightSideWidth="8" ShiftOfImages="0" SelectedImage="../images/ig_tab_winXPs3.gif"
                    NormalImage="../images/ig_tab_winXP3.gif" HoverImage="../images/ig_tab_winXPs3.gif"
                    FillStyle="LeftMergedWithCenter"></RoundedImage>
                <SelectedTabStyle Height="23px" ForeColor="white">
                </SelectedTabStyle>
                <HoverTabStyle Height="23px" ForeColor="white">
                </HoverTabStyle>
                <Tabs>
                    <igtab:Tab Text="Opportunity Details">
                        <ContentTemplate>
                            <asp:Table ID="tblOppr" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
                                CssClass="aspTable" Width="100%" BorderColor="black" GridLines="None">
                                <asp:TableRow>
                                    <asp:TableCell VerticalAlign="Top">
                                        <br />
                                        <table id="tblDetails" runat="server" width="100%" border="0">
                                            <tr>
                                                <td rowspan="30" valign="top">
                                                    <img src="../images/Dart-32.gif" />
                                                </td>
                                                <td class="normal1" align="right">
                                                    Name
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtName" runat="server" Width="300" CssClass="signup" MaxLength="100"></asp:TextBox>
                                                </td>
                                                <td class="normal1" align="right">
                                                    Assigned To
                                                </td>
                                                <td class="normal1">
                                                    <asp:DropDownList ID="ddlAssignedTo" CssClass="signup" runat="server" Width="180px">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="normal1" align="right">
                                                    Due Date<font color="red">*</font>
                                                </td>
                                                <td>
                                                    <BizCalendar:Calendar runat="server" ID="calDue" />
                                                </td>
                                                <td class="normal1" align="right">
                                                    Contact<font color="red">*</font>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlContact" runat="server" CssClass="signup" Width="180" Enabled="False">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="normal1" align="right">
                                                    Opportunity Source
                                                </td>
                                                <td class="normal1">
                                                    <asp:DropDownList ID="ddlSource" runat="server" Width="180" CssClass="signup">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="normal1" align="right">
                                                    <asp:Label ID="lblsalesorPurType" runat="server"> </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlSalesorPurType" runat="server" CssClass="signup" Width="180">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="normal1" align="right">
                                                    Ship Date
                                                </td>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <BizCalendar:Calendar ID="calShip" runat="server" />
                                                            </td>
                                                            <td>
                                                                &nbsp;<font class="normal1">Shipping Cost</font> &nbsp;
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtShipCost" TabIndex="9" runat="server" Width="90px" CssClass="signup"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td class="normal1" align="right">
                                                    Billing Terms
                                                </td>
                                                <td class="normal1">
                                                    <asp:CheckBox ID="chkBillinTerms" runat="server"></asp:CheckBox>&nbsp; Msg.
                                                    <asp:TextBox ID="txtSummary" runat="server" CssClass="signup"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                            </tr>
                                            <tr>
                                                <td class="text" align="right">
                                                    Ship Via
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlShipCompany" runat="server" Width="200" CssClass="signup">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="normal1" align="right">
                                                    Net
                                                </td>
                                                <td class="normal1">
                                                    <asp:TextBox ID="txtNetdays" runat="server" Width="40" CssClass="signup"></asp:TextBox>
                                                    days
                                                    <asp:RadioButton ID="radPlus" GroupName="rad" runat="server" Text="Plus"></asp:RadioButton>
                                                    <asp:RadioButton ID="radMinus" runat="server" GroupName="rad" Text="Minus"></asp:RadioButton>&nbsp;
                                                    <asp:TextBox ID="txtInterest" runat="server" Width="40" CssClass="signup"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text" align="right">
                                                    Ship Doc
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlShipDoc" runat="server" Width="200" CssClass="signup">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    <asp:HyperLink ID="hplDocuments" runat="server" CssClass="hyperlink">
                                                    </asp:HyperLink>
                                                    /
                                                    <asp:HyperLink ID="hplLnkProjects" runat="server" CssClass="hyperlink"></asp:HyperLink>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="normal1" align="right" rowspan="2">
                                                    Tracking URL
                                                </td>
                                                <td rowspan="2">
                                                    <asp:TextBox ID="txttrackingURL" runat="server" CssClass="signup" Width="300" TextMode="MultiLine"
                                                        Height="40" Text="http://"></asp:TextBox>&nbsp;
                                                    <asp:Button ID="btntrackGo" runat="server" CssClass="button" Text="Go" Width="25">
                                                    </asp:Button>
                                                    <br>
                                                </td>
                                                <td class="normal1" align="right">
                                                    Active
                                                </td>
                                                <td class="normal1">
                                                    <asp:CheckBox ID="chkActive" runat="server"></asp:CheckBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="normal1" align="right">
                                                    Calculated Amount :
                                                </td>
                                                <td class="normal1">
                                                    <asp:Label ID="lblAmount" runat="server"></asp:Label>&nbsp;(Includes Time & Expense)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="normal1" align="right" rowspan="3">
                                                    Comments
                                                </td>
                                                <td rowspan="3">
                                                    <asp:TextBox ID="txtOComments" TabIndex="11" Height="60px" runat="server" Width="300"
                                                        CssClass="signup" MaxLength="250" TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                                <td class="normal1" align="right">
                                                    Amount
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAmount" runat="server" CssClass="signup" Width="180"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" class="normal1" id="tdSales1" runat="server" visible="false">
                                                    Campaign
                                                </td>
                                                <td id="tdSales2" runat="server" visible="false">
                                                    <asp:DropDownList ID="ddlCampaign" runat="server" CssClass="signup" Width="180">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr id="trRecurringTemplate" runat="server">
                                                <td class="normal1" align="right">
                                                    Recurring Template
                                                </td>
                                                <td class="normal1">
                                                    <asp:DropDownList ID="ddlRecurringTemplate" runat="server" CssClass="signup" Width="180">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </igtab:Tab>
                    <igtab:Tab Text="Milestones & Stages">
                        <ContentTemplate>
                            <asp:Table ID="Table1" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
                                CssClass="aspTable" Width="100%" BorderColor="black" GridLines="None" Height="300">
                                <asp:TableRow>
                                    <asp:TableCell VerticalAlign="Top">
                                        <br>
                                        <table width="100%">
                                            <tr>
                                                <td class="normal1" align="right">
                                                    Conclusion Reason
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlClReason" runat="server" CssClass="signup" Width="180">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr id="trSalesProcess" runat="server">
                                                <td class="normal1" align="right">
                                                    Opportunity Process
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlProcessList" runat="server" CssClass="signup" Width="180"
                                                        AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Table ID="tblMilestone" runat="server" Width="100%" GridLines="none" BorderWidth="0"
                                            CellSpacing="0">
                                        </asp:Table>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </igtab:Tab>
                    <igtab:Tab Text="Associated Contacts">
                        <ContentTemplate>
                            <asp:Table ID="Table2" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
                                CssClass="aspTable" Width="100%" BorderColor="black" GridLines="None" Height="300">
                                <asp:TableRow>
                                    <asp:TableCell VerticalAlign="Top">
                                        <br>
                                        <table width="100%">
                                            <tr>
                                                <td class="normal1" align="right">
                                                    Customer
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtComp" runat="server" CssClass="signup" Width="125px"></asp:TextBox>&nbsp;
                                                    <asp:Button ID="imgCustGo" runat="server" CssClass="button" Text="Go" Width="30">
                                                    </asp:Button>&nbsp;
                                                    <asp:DropDownList ID="ddlcompany" TabIndex="15" runat="server" CssClass="signup"
                                                        Width="200" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="normal1" align="right">
                                                    Contact
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlAssocContactId" runat="server" CssClass="signup" Width="180">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="normal1" align="right">
                                                    Contact Role
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlContactRole" runat="server" CssClass="signup" Width="180">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="normal1" align="right">
                                                    Share Opportunity via Partner Point ?
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkShare" runat="server" />&nbsp;&nbsp;&nbsp;
                                                    <asp:Button ID="btnAddContact" runat="server" CssClass="button" Text="Add Contact">
                                                    </asp:Button>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                        <asp:DataGrid ID="dgContact" runat="server" CssClass="dg" Width="100%" BorderColor="white"
                                            AutoGenerateColumns="False">
                                            <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                            <ItemStyle CssClass="is"></ItemStyle>
                                            <HeaderStyle CssClass="hs"></HeaderStyle>
                                            <Columns>
                                                <asp:BoundColumn DataField="numContactId" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="bitPartner" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderText="Organization,Relationship" DataField="Company"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderText="First & Last Name" DataField="Name"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderText="Phone - Ext" DataField="Phone"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderText="Email" DataField="Email"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderText="Contact Role" DataField="ContactRole"></asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="Share Opportunity via Partner Point ?">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblShare" runat="server" CssClass="cell" Font-Size="Large"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn>
                                                    <HeaderTemplate>
                                                        <asp:Button ID="btnHdeleteCnt" runat="server" CssClass="Delete" Text="r"></asp:Button>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtContactID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "numContactId") %>'>
                                                        </asp:TextBox>
                                                        <asp:Button ID="btnDeleteCnt" runat="server" CssClass="Delete" Text="r" CommandName="Delete">
                                                        </asp:Button>
                                                        <asp:LinkButton ID="lnkDeleteCnt" runat="server" Visible="false">
																<font color="#730000">*</font></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </igtab:Tab>
                    <igtab:Tab Text="Product/Service">
                        <ContentTemplate>
                            <asp:Table ID="Table3" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
                                CssClass="aspTable" Width="100%" BorderColor="black" GridLines="None" Height="300">
                                <asp:TableRow>
                                    <asp:TableCell VerticalAlign="Top">
                                        <br>
                                        <table align="center">
                                            <tr>
                                                <td class="normal1" rowspan="111111115">
                                                    <asp:Image ID="imgItem" runat="server" BorderWidth="1" Width="100" BorderColor="black"
                                                        Height="100"></asp:Image><br>
                                                    <asp:HyperLink ID="hplImage" runat="server" CssClass="hyperlink" NavigateUrl="">View Full Image</asp:HyperLink>
                                                </td>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td class="normal1" valign="top" align="right">
                                                                Item
                                                            </td>
                                                            <td class="normal1" valign="top">
                                                                <rad:RadComboBox ID="radCmbtem" OnClientItemsRequesting="GetSelectedItem" ExternalCallBackPage="frmOppLoadItems.aspx"
                                                                    Width="195px" DropDownWidth="500px" Skin="WindowsXP" runat="server" AutoPostBack="True"
                                                                    AllowCustomText="True" EnableLoadOnDemand="True">
                                                                    <HeaderTemplate>
                                                                        <table style="width: 500px; text-align: left">
                                                                            <tr>
                                                                                <td>
                                                                                </td>
                                                                                <td style="width: 125px;">
                                                                                    Item
                                                                                </td>
                                                                                <td style="width: 125px;">
                                                                                    List Price
                                                                                </td>
                                                                                <td style="width: 125px;">
                                                                                    SKU
                                                                                </td>
                                                                                <td style="width: 125px;">
                                                                                    Model ID
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <table style="width: 500px; text-align: left">
                                                                            <tr>
                                                                                <td style="display: none">
                                                                                    <%#DataBinder.Eval(Container.DataItem, "numItemCode")%>
                                                                                </td>
                                                                                <td style="width: 125px;">
                                                                                    <%#DataBinder.Eval(Container.DataItem, "vcItemName")%>
                                                                                </td>
                                                                                <td style="width: 125px;">
                                                                                    <%#DataBinder.Eval(Container.DataItem, "Price")%>
                                                                                </td>
                                                                                <td style="width: 125px;">
                                                                                    <%#DataBinder.Eval(Container.DataItem, "SKU")%>
                                                                                </td>
                                                                                <td style="width: 125px;">
                                                                                    <%# DataBinder.Eval(Container.DataItem, "Model ID") %>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ItemTemplate>
                                                                </rad:RadComboBox>
                                                            </td>
                                                            <td class="normal1" align="right">
                                                                List Price :
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblListPrice" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="normal1" align="right">
                                                                Type
                                                            </td>
                                                            <td class="normal1">
                                                                <asp:DropDownList ID="ddltype" runat="server" CssClass="signup" Width="200" Enabled="False">
                                                                    <asp:ListItem Value="P">Product</asp:ListItem>
                                                                    <asp:ListItem Value="S">Service</asp:ListItem>
                                                                    <asp:ListItem Value="A">Accessory</asp:ListItem>
                                                                    <asp:ListItem Value="N">Non-Inventory Item</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                            <asp:Panel ID="pnlDropShip" runat="server">
                                                                <td>
                                                                </td>
                                                                <td class="normal1">
                                                                    <asp:CheckBox ID="chkDropShip" Text="Drop Ship" runat="server" />
                                                                </td>
                                                            </asp:Panel>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4" style="white-space: nowrap">
                                                                <asp:PlaceHolder ID="phItems" runat="server"></asp:PlaceHolder>
                                                            </td>
                                                        </tr>
                                                        <tr id="trWareHouse" runat="server">
                                                            <td class="normal1" align="right">
                                                                Warehouse<font color="red">*</font>
                                                            </td>
                                                            <td class="normal1" colspan="3">
                                                                <asp:DropDownList ID="ddlWarehouse" AutoPostBack="true" runat="server" CssClass="signup"
                                                                    Width="200">
                                                                </asp:DropDownList>
                                                                <asp:Label ID="lblBudget" runat="server" Text="The budget balance for the item group this item belongs to is: "
                                                                    Visible="false"></asp:Label>
                                                                &nbsp;&nbsp;
                                                                <asp:Label ID="lblBudgetMonth" runat="server" Text="" Visible="false"></asp:Label>&nbsp;&nbsp;
                                                                <asp:Label ID="lblBudgetYear" runat="server" Text="" Visible="false"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="normal1" align="right">
                                                                Description
                                                            </td>
                                                            <td class="normal1" colspan="3">
                                                                <asp:TextBox ID="txtdesc" runat="server" TextMode="MultiLine" Width="500"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="normal1" align="right">
                                                                <asp:Label ID="tdUnits" runat="server">
                                                                    <asp:Label ID="lblUnits" runat="server">Units</asp:Label><font color="red">*</font></asp:Label>
                                                            </td>
                                                            <td class="normal1" nowrap colspan="3">
                                                                <asp:TextBox ID="txtunits" runat="server" CssClass="signup" Width="90px"></asp:TextBox>
                                                                <asp:HyperLink ID="hplPrice" runat="server" CssClass="hyperlink">Unit Price</asp:HyperLink><font
                                                                    color="red">*</font>
                                                                <asp:TextBox ID="txtprice" runat="server" CssClass="signup" Width="90px" MaxLength="11"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <asp:Button ID="btnAdd" runat="server" Width="60px" CssClass="button" Text="Add">
                                                                </asp:Button>&nbsp;&nbsp;&nbsp;<asp:Button ID="btnEditCancel" runat="server" Visible="false"
                                                                    CssClass="button" Text="Cancel"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <table id="tblKits" visible="false" runat="server" width="100%">
                                            <tr>
                                                <td class="text_bold">
                                                    Kit Items
                                                </td>
                                                <td>
                                                    <rad:RadComboBox ID="radItem" ExternalCallBackPage="../Items/frmLoadItems.aspx" Width="150"
                                                        DropDownWidth="150" Skin="WindowsXP" runat="server" EnableLoadOnDemand="True">
                                                    </rad:RadComboBox>
                                                    &nbsp;<asp:Button ID="btnAddKitItem" runat="server" Text="Add Child Items" CssClass="button" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnRemoveKitItems" runat="server" Text="Remove Child Items" CssClass="button" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                    <igtbl:UltraWebGrid ID="uKit" Width="100%" DisplayLayout-AllowRowNumberingDefault="ByDataIsland"
                                                        runat="server" Browser="Xml" Height="100%">
                                                        <DisplayLayout AutoGenerateColumns="false" RowHeightDefault="18" AllowAddNewDefault="No"
                                                            Version="3.00" SelectTypeRowDefault="Single" ViewType="Hierarchical" TableLayout="Auto"
                                                            SelectTypeCellDefault="Extended" BorderCollapseDefault="Separate" AllowColSizingDefault="Free"
                                                            Name="uKit" EnableClientSideRenumbering="true" SelectTypeColDefault="Extended"
                                                            AllowUpdateDefault="Yes">
                                                            <HeaderStyleDefault VerticalAlign="Middle" Font-Size="8pt" Font-Bold="true" Font-Names="Arial"
                                                                BorderStyle="Solid" HorizontalAlign="Left" ForeColor="white" BackColor="#52658C">
                                                                <Padding Left="2px" Right="2px"></Padding>
                                                                <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
                                                                </BorderDetails>
                                                            </HeaderStyleDefault>
                                                            <RowSelectorStyleDefault BackColor="White">
                                                            </RowSelectorStyleDefault>
                                                            <FrameStyle Width="100%" Cursor="Default" BorderWidth="0px" Font-Size="8pt" Font-Names="Arial"
                                                                BorderStyle="Double">
                                                            </FrameStyle>
                                                            <FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
                                                                <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
                                                                </BorderDetails>
                                                            </FooterStyleDefault>
                                                            <EditCellStyleDefault BorderWidth="0px" BorderStyle="None">
                                                            </EditCellStyleDefault>
                                                            <SelectedRowStyleDefault ForeColor="White" BackColor="#666666">
                                                            </SelectedRowStyleDefault>
                                                            <RowStyleDefault BorderWidth="1px" Font-Size="8pt" Font-Names="Arial" BorderColor="Gray"
                                                                BorderStyle="Solid" BackColor="White">
                                                                <Padding Left="5px" Right="5px"></Padding>
                                                                <BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
                                                            </RowStyleDefault>
                                                            <RowExpAreaStyleDefault BackColor="LightSteelBlue">
                                                            </RowExpAreaStyleDefault>
                                                        </DisplayLayout>
                                                        <Bands>
                                                            <igtbl:UltraGridBand AllowDelete="No" AllowAdd="Yes" BaseTableName="ChildItems">
                                                                <Columns>
                                                                    <igtbl:UltraGridColumn Type="CheckBox" Key="Remove">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn Hidden="true" Width="100%" IsBound="true" BaseColumnName="numItemCode"
                                                                        Key="numItemCode">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn Hidden="true" Width="100%" IsBound="true" BaseColumnName="numOppChildItemID"
                                                                        Key="numOppChildItemID">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn HeaderText="Child Item" AllowUpdate="No" IsBound="true" BaseColumnName="vcItemName"
                                                                        Key="vcItemName">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn HeaderText="Description" AllowUpdate="No" IsBound="true" BaseColumnName="txtItemDesc"
                                                                        Key="txtItemDesc">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn HeaderText="Type" AllowUpdate="No" IsBound="true" BaseColumnName="charItemType"
                                                                        Key="charItemType">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn HeaderText="Unit Price" AllowUpdate="No" IsBound="true" BaseColumnName="UnitPrice"
                                                                        Key="monListPrice">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn HeaderText="Quantity" AllowUpdate="Yes" IsBound="true" BaseColumnName="numQtyItemsReq"
                                                                        Key="numQtyItemsReq">
                                                                    </igtbl:UltraGridColumn>
                                                                </Columns>
                                                            </igtbl:UltraGridBand>
                                                        </Bands>
                                                    </igtbl:UltraWebGrid>
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="100%" id="tblItemWareHouse" runat="server" visible="false">
                                            <tr>
                                                <td class="text_bold">
                                                    Inventory Details
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <igtbl:UltraWebGrid ID="uwItemSel" Width="100%" DisplayLayout-AllowRowNumberingDefault="ByDataIsland"
                                                        runat="server" Browser="Xml" Height="100%">
                                                        <DisplayLayout AutoGenerateColumns="true" RowHeightDefault="18" AllowAddNewDefault="Yes"
                                                            Version="3.00" SelectTypeRowDefault="Single" ViewType="Hierarchical" TableLayout="Auto"
                                                            SelectTypeCellDefault="Extended" BorderCollapseDefault="Separate" AllowColSizingDefault="Free"
                                                            Name="uwItemSel" EnableClientSideRenumbering="true" SelectTypeColDefault="Extended"
                                                            AllowUpdateDefault="Yes">
                                                            <HeaderStyleDefault VerticalAlign="Middle" Font-Size="8pt" Font-Bold="true" Font-Names="Arial"
                                                                BorderStyle="Solid" HorizontalAlign="Left" ForeColor="white" BackColor="#52658C">
                                                                <Padding Left="2px" Right="2px"></Padding>
                                                                <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
                                                                </BorderDetails>
                                                            </HeaderStyleDefault>
                                                            <RowSelectorStyleDefault BackColor="White">
                                                            </RowSelectorStyleDefault>
                                                            <FrameStyle Width="100%" Cursor="Default" BorderWidth="0px" Font-Size="8pt" Font-Names="Arial"
                                                                BorderStyle="Double">
                                                            </FrameStyle>
                                                            <FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
                                                                <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
                                                                </BorderDetails>
                                                            </FooterStyleDefault>
                                                            <EditCellStyleDefault BorderWidth="0px" BorderStyle="None">
                                                            </EditCellStyleDefault>
                                                            <SelectedRowStyleDefault ForeColor="White" BackColor="#666666">
                                                            </SelectedRowStyleDefault>
                                                            <RowStyleDefault BorderWidth="1px" Font-Size="8pt" Font-Names="Arial" BorderColor="Gray"
                                                                BorderStyle="Solid" BackColor="White">
                                                                <Padding Left="5px" Right="5px"></Padding>
                                                                <BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
                                                            </RowStyleDefault>
                                                            <RowExpAreaStyleDefault BackColor="LightSteelBlue">
                                                            </RowExpAreaStyleDefault>
                                                        </DisplayLayout>
                                                        <Bands>
                                                            <igtbl:UltraGridBand AllowDelete="No" AllowAdd="Yes" AddButtonCaption="WareHouse"
                                                                BaseTableName="WareHouse" Key="WareHouse">
                                                                <Columns>
                                                                    <igtbl:UltraGridColumn Hidden="true" Width="100%" IsBound="true" BaseColumnName="numWareHouseItemID"
                                                                        Key="numWareHouseItemID">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn Hidden="true" IsBound="true" BaseColumnName="numWareHouseID"
                                                                        Key="numWareHouseID">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn Hidden="true" IsBound="true" BaseColumnName="numItemCode"
                                                                        Key="numItemCode">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn HeaderText="Warehouse" AllowUpdate="No" IsBound="true" BaseColumnName="vcWarehouse"
                                                                        Key="vcWarehouse">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn HeaderText="On Hand" AllowUpdate="No" IsBound="true" BaseColumnName="OnHand"
                                                                        Key="OnHand">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn HeaderText="On Order" AllowUpdate="No" IsBound="true" BaseColumnName="OnOrder"
                                                                        Key="OnOrder">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn HeaderText="Reorder" AllowUpdate="No" IsBound="true" BaseColumnName="Reorder"
                                                                        Key="Reorder">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn HeaderText="Allocation" AllowUpdate="No" IsBound="true" BaseColumnName="Allocation"
                                                                        Key="Allocation">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn HeaderText="BackOrder" AllowUpdate="No" IsBound="true" BaseColumnName="BackOrder"
                                                                        Key="BackOrder">
                                                                    </igtbl:UltraGridColumn>
                                                                    <igtbl:UltraGridColumn HeaderText="Units" Hidden="true" AllowUpdate="Yes" Format="###,##0.00"
                                                                        Key="Units">
                                                                    </igtbl:UltraGridColumn>
                                                                </Columns>
                                                            </igtbl:UltraGridBand>
                                                            <igtbl:UltraGridBand AllowDelete="No" AllowAdd="Yes" AllowUpdate="No" AddButtonCaption="Serialized Items"
                                                                BaseTableName="SerializedItems" Key="SerializedItems">
                                                                <Columns>
                                                                    <igtbl:UltraGridColumn AllowUpdate="Yes" Type="CheckBox" Key="Select">
                                                                    </igtbl:UltraGridColumn>
                                                                </Columns>
                                                            </igtbl:UltraGridBand>
                                                        </Bands>
                                                    </igtbl:UltraWebGrid>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                        <br />
                                        <table align="center" id="tblOptSelection" runat="server" visible="false">
                                            <tr>
                                                <td class="text_bold" colspan="2">
                                                    Choose Options And Accessories
                                                    <br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="normal1" rowspan="111111115">
                                                    <asp:Image ID="imgOptItem" runat="server" BorderWidth="1" Width="100" BorderColor="black"
                                                        Height="100"></asp:Image><br>
                                                    <asp:HyperLink ID="hplOptImage" runat="server" CssClass="hyperlink" NavigateUrl="">View Full Image</asp:HyperLink>
                                                </td>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td class="normal1" valign="top" align="right">
                                                                Option Item
                                                            </td>
                                                            <td class="normal1" valign="top">
                                                                <asp:DropDownList ID="ddlOptItem" TabIndex="19" runat="server" CssClass="signup"
                                                                    Width="450" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="normal1" align="right">
                                                                List Price :
                                                            </td>
                                                            <td class="normal1">
                                                                <asp:Label ID="lblOptPrice" runat="server"></asp:Label>
                                                            </td>
                                                            <td>
                                                            </td>
                                                            <td class="normal1">
                                                                <asp:CheckBox ID="chkOptDropShip" Text="Drop Ship" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4" style="white-space: nowrap">
                                                                <asp:PlaceHolder ID="phOptItem" runat="server"></asp:PlaceHolder>
                                                            </td>
                                                        </tr>
                                                        <tr id="trOptWareHouse" runat="server">
                                                            <td class="normal1" align="right">
                                                                Warehouse<font color="red">*</font>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlOptWarehouse" AutoPostBack="true" runat="server" CssClass="signup"
                                                                    Width="180">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="normal1" align="right">
                                                                Description
                                                            </td>
                                                            <td class="normal1" colspan="3">
                                                                <asp:TextBox ID="txtOptDesc" runat="server" TextMode="MultiLine" Width="500"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="normal1" align="right">
                                                                <asp:Label ID="lblOptUnits" runat="server">Units</asp:Label><font color="red">*</font>
                                                            </td>
                                                            <td class="normal1" nowrap colspan="3">
                                                                <asp:TextBox ID="txtOptUnits" runat="server" CssClass="signup" Width="90px"></asp:TextBox>
                                                                <asp:HyperLink ID="hplOptPrice" runat="server" CssClass="hyperlink">Unit Price</asp:HyperLink><font
                                                                    color="red">*</font>
                                                                <asp:TextBox ID="txtOptPrice" runat="server" CssClass="signup" Width="90px" MaxLength="11"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <asp:Button ID="btnOptAdd" runat="server" Width="60px" CssClass="button" Text="Add">
                                                                </asp:Button>&nbsp;&nbsp;&nbsp;<asp:Button ID="Button2" runat="server" Visible="false"
                                                                    CssClass="button" Text="Cancel"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <igtbl:UltraWebGrid ID="uwOptionItem" Visible="false" Width="100%" DisplayLayout-AllowRowNumberingDefault="ByDataIsland"
                                            runat="server" Browser="Xml" Height="100%">
                                            <DisplayLayout AutoGenerateColumns="true" RowHeightDefault="18" AllowAddNewDefault="Yes"
                                                Version="3.00" SelectTypeRowDefault="Single" ViewType="Hierarchical" TableLayout="Auto"
                                                SelectTypeCellDefault="Extended" BorderCollapseDefault="Separate" AllowColSizingDefault="Free"
                                                Name="uwItemSel" EnableClientSideRenumbering="true" SelectTypeColDefault="Extended"
                                                AllowUpdateDefault="Yes">
                                                <HeaderStyleDefault VerticalAlign="Middle" Font-Size="8pt" Font-Bold="true" Font-Names="Arial"
                                                    BorderStyle="Solid" HorizontalAlign="Left" ForeColor="white" BackColor="#52658C">
                                                    <Padding Left="2px" Right="2px"></Padding>
                                                    <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
                                                    </BorderDetails>
                                                </HeaderStyleDefault>
                                                <RowSelectorStyleDefault BackColor="White">
                                                </RowSelectorStyleDefault>
                                                <FrameStyle Width="100%" Cursor="Default" BorderWidth="0px" Font-Size="8pt" Font-Names="Arial"
                                                    BorderStyle="Double">
                                                </FrameStyle>
                                                <FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
                                                    <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White">
                                                    </BorderDetails>
                                                </FooterStyleDefault>
                                                <EditCellStyleDefault BorderWidth="0px" BorderStyle="None">
                                                </EditCellStyleDefault>
                                                <SelectedRowStyleDefault ForeColor="White" BackColor="#666666">
                                                </SelectedRowStyleDefault>
                                                <RowStyleDefault BorderWidth="1px" Font-Size="8pt" Font-Names="Arial" BorderColor="Gray"
                                                    BorderStyle="Solid" BackColor="White">
                                                    <Padding Left="5px" Right="5px"></Padding>
                                                    <BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
                                                </RowStyleDefault>
                                                <RowExpAreaStyleDefault BackColor="LightSteelBlue">
                                                </RowExpAreaStyleDefault>
                                            </DisplayLayout>
                                            <Bands>
                                                <igtbl:UltraGridBand AllowDelete="No" AllowAdd="Yes" AddButtonCaption="WareHouse"
                                                    BaseTableName="WareHouse" Key="WareHouse">
                                                    <Columns>
                                                        <igtbl:UltraGridColumn Hidden="true" Width="100%" IsBound="true" BaseColumnName="numWareHouseItemID"
                                                            Key="numWareHouseItemID">
                                                        </igtbl:UltraGridColumn>
                                                        <igtbl:UltraGridColumn Hidden="true" IsBound="true" BaseColumnName="numWareHouseID"
                                                            Key="numWareHouseID">
                                                        </igtbl:UltraGridColumn>
                                                        <igtbl:UltraGridColumn Hidden="true" IsBound="true" BaseColumnName="numItemCode"
                                                            Key="numItemCode">
                                                        </igtbl:UltraGridColumn>
                                                        <igtbl:UltraGridColumn HeaderText="Warehouse" AllowUpdate="No" IsBound="true" BaseColumnName="vcWarehouse"
                                                            Key="vcWarehouse">
                                                        </igtbl:UltraGridColumn>
                                                        <igtbl:UltraGridColumn HeaderText="On Hand" AllowUpdate="No" IsBound="true" BaseColumnName="OnHand"
                                                            Key="OnHand">
                                                        </igtbl:UltraGridColumn>
                                                        <igtbl:UltraGridColumn HeaderText="On Order" AllowUpdate="No" IsBound="true" BaseColumnName="OnOrder"
                                                            Key="OnOrder">
                                                        </igtbl:UltraGridColumn>
                                                        <igtbl:UltraGridColumn HeaderText="Reorder" AllowUpdate="No" IsBound="true" BaseColumnName="Reorder"
                                                            Key="Reorder">
                                                        </igtbl:UltraGridColumn>
                                                        <igtbl:UltraGridColumn HeaderText="Allocation" AllowUpdate="No" IsBound="true" BaseColumnName="Allocation"
                                                            Key="Allocation">
                                                        </igtbl:UltraGridColumn>
                                                        <igtbl:UltraGridColumn HeaderText="BackOrder" AllowUpdate="No" IsBound="true" BaseColumnName="BackOrder"
                                                            Key="BackOrder">
                                                        </igtbl:UltraGridColumn>
                                                        <igtbl:UltraGridColumn HeaderText="Units" Hidden="true" AllowUpdate="Yes" Format="###,##0.00"
                                                            Key="Units">
                                                        </igtbl:UltraGridColumn>
                                                    </Columns>
                                                </igtbl:UltraGridBand>
                                            </Bands>
                                        </igtbl:UltraWebGrid>
                                        <br />
                                        <table width="100%" id="tblProducts" runat="server" visible="false">
                                            <tr>
                                                <td class="text_bold">
                                                    Items Added To Opportunity
                                                </td>
                                                <td align="right">
                                                    <asp:Button ID="btnRemoveItem" runat="server" CssClass="button" Text="Remove Items" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <rad:RadGrid ID="RadGrid1" Skin="Windows" runat="server" Width="100%" AutoGenerateColumns="False"
                                                        GridLines="None" ShowFooter="false" AllowMultiRowSelection="true">
                                                        <MasterTableView DataMember="Item" DataKeyNames="numoppitemtCode" Width="100%">
                                                            <Columns>
                                                                <rad:GridClientSelectColumn UniqueName="Select">
                                                                </rad:GridClientSelectColumn>
                                                                <rad:GridBoundColumn SortExpression="numoppitemtCode" HeaderText="" UniqueName="oppitemtCode"
                                                                    HeaderButtonType="TextButton" DataField="numoppitemtCode" Display="false">
                                                                </rad:GridBoundColumn>
                                                                <rad:GridBoundColumn SortExpression="numWarehouseItmsID" HeaderText="" HeaderButtonType="TextButton"
                                                                    DataField="numWarehouseItmsID" Display="false">
                                                                </rad:GridBoundColumn>
                                                                <rad:GridBoundColumn SortExpression="numWareHouseID" HeaderText="" HeaderButtonType="TextButton"
                                                                    DataField="numWareHouseID" Display="false">
                                                                </rad:GridBoundColumn>
                                                                <rad:GridBoundColumn SortExpression="numWarehouseItmsID" HeaderText="" HeaderButtonType="TextButton"
                                                                    DataField="numWarehouseItmsID" Display="false">
                                                                </rad:GridBoundColumn>
                                                                <rad:GridBoundColumn SortExpression="numItemCode" HeaderText="" HeaderButtonType="TextButton"
                                                                    DataField="numItemCode" Display="false">
                                                                </rad:GridBoundColumn>
                                                                <rad:GridBoundColumn SortExpression="vcItemName" HeaderText="Item" HeaderButtonType="TextButton"
                                                                    DataField="vcItemName">
                                                                </rad:GridBoundColumn>
                                                                <rad:GridBoundColumn SortExpression="Warehouse" HeaderText="WareHouse" HeaderButtonType="TextButton"
                                                                    DataField="Warehouse">
                                                                </rad:GridBoundColumn>
                                                                <rad:GridBoundColumn SortExpression="ItemType" HeaderText="Type" HeaderButtonType="TextButton"
                                                                    DataField="ItemType">
                                                                </rad:GridBoundColumn>
                                                                <rad:GridBoundColumn SortExpression="vcItemDesc" HeaderText="Description" HeaderButtonType="TextButton"
                                                                    DataField="vcItemDesc">
                                                                </rad:GridBoundColumn>
                                                                <rad:GridBoundColumn SortExpression="Attributes" HeaderText="Attributes" HeaderButtonType="TextButton"
                                                                    DataField="Attributes">
                                                                </rad:GridBoundColumn>
                                                                <rad:GridBoundColumn SortExpression="DropShip" HeaderText="Drop Ship" HeaderButtonType="TextButton"
                                                                    DataField="DropShip">
                                                                </rad:GridBoundColumn>
                                                                <rad:GridBoundColumn SortExpression="numUnitHour" HeaderText="Units" HeaderButtonType="TextButton"
                                                                    DataField="numUnitHour">
                                                                </rad:GridBoundColumn>
                                                                <rad:GridBoundColumn SortExpression="monPrice" HeaderText="Unit Price" HeaderButtonType="TextButton"
                                                                    DataField="monPrice">
                                                                </rad:GridBoundColumn>
                                                                <rad:GridBoundColumn SortExpression="monTotAmount" HeaderText="Amount" HeaderButtonType="TextButton"
                                                                    DataField="monTotAmount">
                                                                </rad:GridBoundColumn>
                                                                <rad:GridTemplateColumn>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CommandName="EditSelect"></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </rad:GridTemplateColumn>
                                                            </Columns>
                                                            <DetailTables>
                                                                <rad:GridTableView DataMember="SerialNo" HierarchyLoadMode="Client" Width="100%">
                                                                    <ParentTableRelation>
                                                                        <rad:GridRelationFields DetailKeyField="numoppitemtCode" MasterKeyField="numoppitemtCode" />
                                                                    </ParentTableRelation>
                                                                    <Columns>
                                                                        <rad:GridBoundColumn SortExpression="numWarehouseItmsDTLID" HeaderText="" HeaderButtonType="TextButton"
                                                                            DataField="numWarehouseItmsDTLID" Display="false">
                                                                        </rad:GridBoundColumn>
                                                                        <rad:GridBoundColumn SortExpression="numoppitemtCode" HeaderText="" HeaderButtonType="TextButton"
                                                                            DataField="numoppitemtCode" Display="false">
                                                                        </rad:GridBoundColumn>
                                                                        <rad:GridBoundColumn SortExpression="vcSerialNo" HeaderText="Serial No" HeaderButtonType="TextButton"
                                                                            DataField="vcSerialNo">
                                                                        </rad:GridBoundColumn>
                                                                        <rad:GridBoundColumn SortExpression="Comments" HeaderText="Comments" HeaderButtonType="TextButton"
                                                                            DataField="Comments">
                                                                        </rad:GridBoundColumn>
                                                                        <rad:GridBoundColumn SortExpression="Attributes" HeaderText="Attributes" HeaderButtonType="TextButton"
                                                                            DataField="Attributes">
                                                                        </rad:GridBoundColumn>
                                                                    </Columns>
                                                                </rad:GridTableView>
                                                                <rad:GridTableView DataMember="ChildItems" HierarchyLoadMode="Client" Width="100%">
                                                                    <ParentTableRelation>
                                                                        <rad:GridRelationFields DetailKeyField="numoppitemtCode" MasterKeyField="numoppitemtCode" />
                                                                    </ParentTableRelation>
                                                                    <Columns>
                                                                        <rad:GridBoundColumn SortExpression="vcItemName" HeaderText="Item" HeaderButtonType="TextButton"
                                                                            DataField="vcItemName">
                                                                        </rad:GridBoundColumn>
                                                                        <rad:GridBoundColumn SortExpression="txtItemDesc" HeaderText="Description" HeaderButtonType="TextButton"
                                                                            DataField="txtItemDesc">
                                                                        </rad:GridBoundColumn>
                                                                        <rad:GridBoundColumn SortExpression="charItemType" HeaderText="Type" HeaderButtonType="TextButton"
                                                                            DataField="charItemType">
                                                                        </rad:GridBoundColumn>
                                                                        <rad:GridBoundColumn SortExpression="UnitPrice" HeaderText="Unit Price" HeaderButtonType="TextButton"
                                                                            DataField="UnitPrice">
                                                                        </rad:GridBoundColumn>
                                                                        <rad:GridBoundColumn SortExpression="numQtyItemReq" HeaderText="Quantity" HeaderButtonType="TextButton"
                                                                            DataField="numQtyItemsReq">
                                                                        </rad:GridBoundColumn>
                                                                    </Columns>
                                                                </rad:GridTableView>
                                                            </DetailTables>
                                                        </MasterTableView>
                                                        <ClientSettings>
                                                            <Selecting AllowRowSelect="true" />
                                                        </ClientSettings>
                                                    </rad:RadGrid>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </igtab:Tab>
                    <igtab:Tab Text="BizDocs">
                        <ContentTemplate>
                            <asp:Table ID="table5" runat="server" BorderWidth="1" Width="100%" BackColor="white"
                                CellSpacing="0" CellPadding="0" BorderColor="black" GridLines="None" Height="300"
                                CssClass="aspTable">
                                <asp:TableRow>
                                    <asp:TableCell VerticalAlign="Top">
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <div id="divBizDocsDtl" runat="server">
                                                        <iframe id="IframeBiz" frameborder="0" width="100%" scrolling="auto" runat="server"
                                                            height="300"></iframe>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ContentTemplate>
                    </igtab:Tab>
                </Tabs>
            </igtab:UltraWebTab>
            <table width="100%">
                <tr>
                    <td class="normal4" align="center">
                        <asp:Literal ID="litMessage" runat="server"></asp:Literal>
                    </td>
                </tr>
            </table>
            <asp:TextBox ID="txtrecOwner" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtHidValue" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtHidEditOppItem" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtHidOptValue" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="ddlOppType" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtSerialize" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtTemplateId" Text="0" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtProcessId" Text="0" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox runat="server" ID="txtDivId" Style="display: none"></asp:TextBox>
            <asp:TextBox runat="server" ID="txtOppId" Style="display: none"></asp:TextBox>
            <input id="hdKit" runat="server" type="hidden" />
            <input id="hdOptKit" runat="server" type="hidden" />
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
