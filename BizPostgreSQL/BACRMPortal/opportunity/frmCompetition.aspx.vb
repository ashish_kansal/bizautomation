Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Partial Class frmCompetition : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Dim lngItemCode As Long
    Protected WithEvents a As System.Web.UI.HtmlControls.HtmlAnchor

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            lngItemCode = GetQueryStringVal( "ItemCode")
            If Not IsPostBack Then BindGrid()
            btnAdd.Attributes.Add("onclick", "return Add()")
            btnSave.Attributes.Add("onclick", "return Save()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindGrid()
        Try
            Dim objItems As New CItems
            objItems.DomainID = Session("DomainID")
            objItems.ItemCode = lngItemCode

            dgCompetitor.DataSource = objItems.GetCompetitors
            dgCompetitor.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try

            Dim objCommon As New CCommon
            With objCommon
                .DomainID = Session("DomainID")
                .UserCntID = Session("UserContactID")
                .Filter = Trim(txtSearch.Text) & "%"
                ddlCompany.DataSource = objCommon.PopulateOrganization.Tables(0).DefaultView
                ddlCompany.DataTextField = "vcCompanyname"
                ddlCompany.DataValueField = "numCompanyid"
                ddlCompany.DataBind()
                ddlCompany.Items.Insert(0, "--Select One--")
                ddlCompany.Items.FindByText("--Select One--").Value = 0
            End With
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlCompany_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            Dim objOpportunities As New COpportunities
            With objOpportunities
                .DomainID = Session("DomainID")
                .CompanyID = ddlCompany.SelectedItem.Value
                .CompFilter = ""
                'ddlDivision.DataSource = objOpportunities.ListCustomer().tables(0).DefaultView
                'ddlDivision.DataTextField = "vcDivisionName"
                'ddlDivision.DataValueField = "numdivisionId"
                'ddlDivision.DataBind()
                'ddlDivision.Items.Insert(0, "--Select One--")
                'ddlDivision.Items.FindByText("--Select One--").Value = 0
            End With
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            Dim objItems As New CItems
            objItems.DivisionID = ddlCompany.SelectedItem.Value
            objItems.DomainID = Session("DomainID")
            objItems.ItemCode = lngItemCode
            objItems.AddCompetitor()
            BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim dtCompetitorDetails As New DataTable
            Dim i As Integer
            Dim dr As DataRow
            dtCompetitorDetails.Columns.Add("CompetitorID")
            dtCompetitorDetails.Columns.Add("Price")
            dtCompetitorDetails.Columns.Add("Date")
            Dim str As String() = txtHidden.Text.Split("~")
            Dim strValues As String()
            Dim strEditedfldlst As String

            For i = 0 To str.Length - 2
                strValues = str(i).Split(",")
                dr = dtCompetitorDetails.NewRow
                dr("CompetitorID") = strValues(0)
                dr("Price") = strValues(1)
                If strValues(2) <> "" Then dr("Date") = DateFromFormattedDate(strValues(2), Session("DateFormat"))
                dtCompetitorDetails.Rows.Add(dr)
            Next

            dtCompetitorDetails.TableName = "table"
            Dim ds As New DataSet
            ds.Tables.Add(dtCompetitorDetails)
            strEditedfldlst = ds.GetXml()
            ds.Tables.Remove(dtCompetitorDetails)

            Dim objItems As New CItems
            objItems.strFieldList = strEditedfldlst
            objItems.ItemCode = lngItemCode
            objItems.UpdateCompetitor()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Function ReturnName(ByVal bintCreatedDate As Date) As String
        Try
            Dim strCreateDate As String
            strCreateDate = FormattedDateFromDate(bintCreatedDate, Session("DateFormat"))
            Return strCreateDate
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub dgCompetitor_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgCompetitor.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                Dim objItems As New CItems
                objItems.CompetitorID = e.Item.Cells(0).Text
                objItems.DomainID = Session("DomainID")
                objItems.ItemCode = lngItemCode
                If objItems.DeleteCompetitors = False Then
                    litMessage.Text = "Dependent Records Exists. Cannot Be deleted"
                Else : BindGrid()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgCompetitor_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgCompetitor.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As Button
                btnDelete = e.Item.FindControl("btnDelete")
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class

