<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmDealList.aspx.vb" Inherits="BACRMPortal.frmDealList" %>
<%@ Register TagPrefix="menu1" TagName="PartnerPoint" src="../Common/PartnerPoint.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
      <link rel="stylesheet" href="~/CSS/master.css" type="text/css" /> 
      <script language="javascript">
      function DeleteRecord()
		{   
		
		    var OppId='';
		      for(var i=2; i<=gvSearch.rows.length; i++)
			{
			    if (i<10)
		        {
		         str='0'+i   
		         }
		         else
		         {
		           str=i
		         }
		    
		        if  (document.all('gvSearch_ctl'+str+'_chk').checked==true)
		       {
		           
		            if (OppId == '')
		            {
		                OppId = document.all('gvSearch_ctl'+str+'_lbl1').innerHTML+'~'+document.all('gvSearch_ctl'+str+'_lbl2').innerHTML+'~'+document.all('gvSearch_ctl'+str+'_lbl3').innerHTML
		            }
		            else
		            {
		                 OppId =OppId+','+document.all('gvSearch_ctl'+str+'_lbl1').innerHTML+'~'+document.all('gvSearch_ctl'+str+'_lbl2').innerHTML+'~'+document.all('gvSearch_ctl'+str+'_lbl3').innerHTML
		            }
		       } 
			}
			  document.all('txtDelOppId').value = OppId
			  
			return true
		}
      function fnSortByCharDeals(varSortChar)
			{
				document.Form1.txtSortCharDeals.value = varSortChar;
				if (typeof(document.Form1.txtCurrrentPageDeals)!='undefined')
				{
					document.Form1.txtCurrrentPageDeals.value=1;
				}
				document.Form1.btnGo1.click();
			}
			 function SortColumn(a)
    {
       document.Form1.txtSortColumn.value=a;
       document.Form1.submit();
       return false;
    }
    function SelectAll(a)
    {
        var str;

        if (typeof(a)=='string')
        {
            a=document.all[a]
        }
        if (a.checked==true)
        {
             for(var i=1; i<=gvSearch.rows.length; i++)
			{
			    if (i<10)
		        {
		         str='0'+i   
		         }
		         else
		         {
		           str=i
		         }
		         document.all('gvSearch_ctl'+str+'_chk').checked=true;
			}
        }
        else if (a.checked==false)
        {
             for(var i=1; i<=gvSearch.rows.length; i++)
			{
			    if (i<10)
		        {
		         str='0'+i   
		         }
		         else
		         {
		           str=i
		         }
		         document.all('gvSearch_ctl'+str+'_chk').checked=false;
			}
        }
       
        //gvSearch_ctl02_chk
    }
    function OpenWindow(a,b,c)
    {
        var str;
        if (b==0)
        {   
            if (c==1)
            {
            str="../pagelayout/frmLeaddtl.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=deallist&DivID=" + a;
            }
            else{
            str="../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=deallist&DivID=" + a;
            }
        }
        else if (b==1)
        {
            if (c==1)
            {
            str="../pagelayout/frmProspectdtl.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=deallist&DivID=" + a;
            }
            else
            {
              str="../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=deallist&DivID=" + a;
            }
        }
         else if (b==2)
        {
            if (c==1)
            {
            str="../pagelayout/frmAccountdtl.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=deallist&klds+7kldf=fjk-las&DivId=" + a;
            }
            else
            {
              str="../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=deallist&klds+7kldf=fjk-las&DivId=" + a;
            }
        }
       
            document.location.href=str;
       
    }
		function OpenContact(a,b)
    {
      
        var str;
        if (b==1)
            {
            str="../pagelayout/frmContact.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=deallist&ujfda=tyuu&CntId=" + a;
            }
            else
            {
             str="../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=deallist&ft6ty=oiuy&CntId=" + a;
             }
        
            document.location.href=str;
      
    }
		function DeleteMessage()
		{
			alert("You Are not Authorized to Delete the Selected Record !");
			return false;
		}
		function SelWarOpenDeals(a)
		{
		    window.open("../opportunity/frmSelWhouseOpenDls.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Opid="+a)
		    return false;
		}
			function OpenSetting(a)
		{
		                   
		    window.open('../Opportunity/frmConfOppList.aspx?type='+a,'','toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
		    return false
		}
		function OpenOpp(a,b)
		{
		  
		 var str;
        if (b==1)
            {
            str="../pagelayout/frmOppurtunityDtl.aspx?frm=deallist&OpID="+a;
            }
            else
            {
             str="../opportunity/frmOpportunities.aspx?frm=deallist&OpID="+a;
             }    
              document.location.href=str; 
		}
		</script>
</head>
<body>
    <form id="Form1" runat="server">
<asp:ScriptManager runat="server" ID="sm" EnablePartialRendering="true"></asp:ScriptManager>
<menu1:PartnerPoint ID="menu" runat="server" />
  			<table width="100%">
			    <tr>
			        <td>
			        <asp:HyperLink Text="Settings" CssClass="hyperlink" runat="server" ID="hplSettings"></asp:HyperLink>
			        </td>
			        	
				
						<TD>
							<asp:dropdownlist Visible="false" id="ddlSortDeals" runat="server" cssclass="signup" AutoPostBack="True">
							
								<asp:ListItem Value="1">Sales Deals</asp:ListItem>
								<asp:ListItem Value="2">Purchase Deals</asp:ListItem>
								
							</asp:dropdownlist></TD>
						
				
			       <td align="right">
  	            <TABLE align="right">
				            <tr>
					            <TD class="normal1" width="150">No of Records:			
						            <asp:label id="lblRecordsDeals" runat="server"></asp:label>
					            </TD>
					            <TD class="normal1">Organization
					            </TD>
					            <TD><asp:textbox id="txtCustomer" runat="server" Width="55px" CssClass="signup"></asp:textbox></TD>
					            <TD class="normal1">First Name
					            </TD>
					            <TD><asp:textbox id="txtFirstName" runat="server" Width="55px" CssClass="signup"></asp:textbox></TD>
					            <TD class="normal1">Last Name</TD>
					            <TD><asp:textbox id="txtLastName" runat="server" Width="55px" CssClass="signup"></asp:textbox></TD>
					            <TD><asp:button id="btnGo" Width="25" CssClass="button" Runat="server" Text="Go"></asp:button></TD>
					             <TD><asp:button id="btnDelete" OnClientClick="return DeleteRecord()" CssClass="button" Runat="server" Text="Delete"></asp:button></TD>
				            </TR>
            			
			            </TABLE>
            </td></tr></table>


					<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td vAlign="bottom" width="170">
					<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;<asp:label id="lblDeal" Runat="server"></asp:label>&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
				<TD id="hideDeals" noWrap align="right" runat="server">
							<TABLE>
								<TR>
									<TD>
										<asp:label id="lblNextDeals" runat="server" cssclass="Text_bold">Next:</asp:label></TD>
									<TD class="normal1">
										<asp:linkbutton id="lnk2Deals" runat="server">2</asp:linkbutton></TD>
									<TD class="normal1">
										<asp:linkbutton id="lnk3Deals" runat="server">3</asp:linkbutton></TD>
									<TD class="normal1">
										<asp:linkbutton id="lnk4Deals" runat="server">4</asp:linkbutton></TD>
									<TD class="normal1">
										<asp:linkbutton id="lnk5Deals" runat="server">5</asp:linkbutton></TD>
									<TD>
										<asp:linkbutton id="lnkFirstDeals" runat="server">
											<div class="LinkArrow">9</div>
										</asp:linkbutton></TD>
									<TD>
										<asp:linkbutton id="lnkPreviousDeals" runat="server">
											<div class="LinkArrow">3</div>
										</asp:linkbutton></TD>
									<TD class="normal1">
										<asp:label id="lblPageDeals" runat="server">Page</asp:label></TD>
									<TD>
										<asp:textbox id="txtCurrrentPageDeals" runat="server" CssClass="signup" Width="28px" AutoPostBack="True"
											MaxLength="5" Text="1"></asp:textbox></TD>
									<TD class="normal1">
										<asp:label id="lblOfDeals" runat="server">of</asp:label></TD>
									<TD class="normal1">
										<asp:label id="lblTotalDeals" runat="server"></asp:label></TD>
									<TD>
										<asp:linkbutton id="lnkNextDeals" runat="server" CssClass="LinkArrow">
											<div class="LinkArrow">4</div>
										</asp:linkbutton></TD>
									<TD>
										<asp:linkbutton id="lnkLastDeals" runat="server">
											<div class="LinkArrow">:</div>
										</asp:linkbutton></TD>
								</TR>
							</TABLE>
						</TD>
					</tr>
					</table>
			<table cellSpacing="1" cellPadding="1" width="100%" border="0">
						<tr>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="a" href="javascript:fnSortByCharDeals('a')">
									<div class="A2Z">A</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="b" href="javascript:fnSortByCharDeals('b')">
									<div class="A2Z">B</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="c" href="javascript:fnSortByCharDeals('c')">
									<div class="A2Z">C</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="d" href="javascript:fnSortByCharDeals('d')">
									<div class="A2Z">D</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="e" href="javascript:fnSortByCharDeals('e')">
									<div class="A2Z">E</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="f" href="javascript:fnSortByCharDeals('f')">
									<div class="A2Z">F</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="g" href="javascript:fnSortByCharDeals('g')">
									<div class="A2Z">G</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="h" href="javascript:fnSortByCharDeals('h')">
									<div class="A2Z">H</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="I" href="javascript:fnSortByCharDeals('i')">
									<div class="A2Z">I</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="j" href="javascript:fnSortByCharDeals('j')">
									<div class="A2Z">J</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="k" href="javascript:fnSortByCharDeals('k')">
									<div class="A2Z">K</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="l" href="javascript:fnSortByCharDeals('l')">
									<div class="A2Z">L</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="m" href="javascript:fnSortByCharDeals('m')">
									<div class="A2Z">M</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="n" href="javascript:fnSortByCharDeals('n')">
									<div class="A2Z">N</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="o" href="javascript:fnSortByCharDeals('o')">
									<div class="A2Z">O</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="p" href="javascript:fnSortByCharDeals('p')">
									<div class="A2Z">P</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="q" href="javascript:fnSortByCharDeals('q')">
									<div class="A2Z">Q</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="r" href="javascript:fnSortByCharDeals('r')">
									<div class="A2Z">R</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="s" href="javascript:fnSortByCharDeals('s')">
									<div class="A2Z">S</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="t" href="javascript:fnSortByCharDeals('t')">
									<div class="A2Z">T</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="u" href="javascript:fnSortByCharDeals('u')">
									<div class="A2Z">U</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="v" href="javascript:fnSortByCharDeals('v')">
									<div class="A2Z">V</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="w" href="javascript:fnSortByCharDeals('w')">
									<div class="A2Z">W</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="x" href="javascript:fnSortByCharDeals('x')">
									<div class="A2Z">X</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="y" href="javascript:fnSortByCharDeals('y')">
									<div class="A2Z">Y</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="z" href="javascript:fnSortByCharDeals('z')">
									<div class="A2Z">Z</div>
								</A>
							</td>
							<td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
								bgColor="#52658c"><A id="all" href="javascript:fnSortByCharDeals('0')">
									<div class="A2Z">All</div>
								</A>
							</td>
						</tr>
					</table>
					
			<asp:table id="table3" Width="100%" Runat="server" Height="350" GridLines="None" BorderColor="black" CssClass="aspTable"
				BorderWidth="1" CellSpacing="0" CellPadding="0">
				<asp:tableRow>
					<asp:tableCell VerticalAlign="Top">
						    <asp:GridView ID="gvSearch" runat="server" EnableViewState="true"  AutoGenerateColumns="false"  CssClass="dg" Width="100%">
                                    <AlternatingRowStyle CssClass="ais"/>
                                    <RowStyle CssClass="is"/>
                                    <HeaderStyle CssClass="hs"/>
						                        <Columns>
						                   
						                        </Columns>		
                                </asp:GridView>
					</asp:tableCell>
				</asp:tableRow>
			</asp:table>
						<table width="100%">
				<tr>
					<td class="normal4" align="center">
						<asp:Literal ID="litMessage" Runat="server"></asp:Literal></td>
				</tr>
			</table>
			 <asp:TextBox ID="txtDelOppId" runat="server" style="display:none"></asp:TextBox>
			   <asp:TextBox ID="txtSortColumn" runat="server" style="display:none"></asp:TextBox>			
			<asp:TextBox ID="txtTotalPageDeals" Runat="server" style="DISPLAY:none"></asp:TextBox>
			<asp:TextBox ID="txtTotalRecordsDeals" Runat="server" style="DISPLAY:none"></asp:TextBox>
			<asp:TextBox ID="txtSortCharDeals" Runat="server" style="DISPLAY:none"></asp:TextBox>	
			<asp:button id="btnGo1" Width="25" Runat="server" style="DISPLAY:none" />
			
    </form>
</body>
</html>
