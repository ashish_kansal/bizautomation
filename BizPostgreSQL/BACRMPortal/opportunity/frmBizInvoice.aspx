<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmBizInvoice.aspx.vb"
    Inherits="BACRMPortal.frmBizInvoice" %>

<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <title>BizDocs</title>
    <script language="javascript" type="text/javascript">
        function PrintIt() {
            document.getElementById('tblButtons').style.display = 'none';
            window.print();
            return false;
        }
        function Submit() {
            if (document.Form1.txtCCNo.value == '') {
                alert("Enter Credit Card No")
                document.Form1.txtCCNo.focus();
                return false;
            }
            if (document.Form1.txtCHName.value == '') {
                alert("Enter Card Holder Name")
                document.Form1.txtCHName.focus();
                return false;
            }
            if (document.Form1.txtCVV2.value == '') {
                alert("Enter CVV2 or CVD")
                document.Form1.txtCVV2.focus();
                return false;
            }
            if (document.Form1.ddlCardType.value == 0) {
                alert("Select Card Type")
                document.Form1.ddlCardType.focus();
                return false;
            }
            if (document.Form1.txtAmount.value == '') {
                alert("Enter Amount")
                document.Form1.txtAmount.focus();
                return false;
            }
        }
        function MakeFinal() {
            var varBalance;
            if (document.all) {
                varBalance = lblBalance.innerText;
            } else {
                varBalance = lblBalance.textContent;
            }
            if (varBalance <= 0) {
                return true
            }
            else {
                alert("BizDoc can not be made final, until the balance due reads 0.00")
                return false
            }
        }
        function Close1(lngOppID) {
            //opener.location='dreamweaver.htm';self.close()">
            // window.open('../opportunity/frmQBStatusReport.aspx?BizDocID='+lngOppID,'','toolbar=no,titlebar=no,top=300,width=800,height=500,scrollbars=yes,resizable=yes')
            window.opener.reDirect('../pagelayout/frmOppurtunitydtl.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=opportunitylist&OpID=' + lngOppID);
            self.close();
            return false;
        }
        function openStatusReport(a) {
            window.open('../opportunity/frmQBStatusReport.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&BizDocID=' + a, '', 'toolbar=no,titlebar=no,top=300,width=800,height=500,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenPaymentDetails(a, b) {
            var BalanceAmt;
            BalanceAmt = 0;
            if (document.all) {
                BalanceAmt = window.document.getElementById('lblBalance').innerText;
            } else {
                BalanceAmt = window.document.getElementById('lblBalance').textContent;
            }
            BalanceAmt = BalanceAmt.replace(/,/g, "");
            window.open('../opportunity/frmPaymentDetails.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&a=' + a + '&b=' + b + '&c=' + BalanceAmt, '', 'toolbar=no,titlebar=no,top=300,width=800,height=380,scrollbars=yes,resizable=yes')
            return false;
        }

        ////			function OpenAmtPaid(a,b,c)
        ////			{
        ////				window.open('../opportunity/frmAmtPaid.aspx?OppBizDocID='+a+'&Amt='+b+'&OppID='+c,'','toolbar=no,titlebar=no,top=300,width=500,height=150,scrollbars=yes,resizable=yes');
        ////				return false;
        ////			}

        function OpenAmtPaid(a, b) {

            var BalanceAmt;
            BalanceAmt = 0;
            if (document.all) {
                BalanceAmt = window.document.getElementById('lblBalance').innerText;
            } else {
                BalanceAmt = window.document.getElementById('lblBalance').textContent;
            }
            BalanceAmt = BalanceAmt.replace(/,/g, "");
            // alert(BalanceAmt);
            //OppBizDocID===a
            //OppID==B
            //BalanceAmt==c
            window.open('../opportunity/frmAmtPaid.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&a=' + a + '&b=' + b + '&c=' + BalanceAmt, '', 'toolbar=no,titlebar=no,top=300,width=600,height=270,scrollbars=no,resizable=no');
            return false;
        }

        function OpenRecievedDate(OppID) {
            window.open('../opportunity/frmRecievedDate.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppID=' + OppID, '', 'toolbar=no,titlebar=no,top=300,width=400,height=100,scrollbars=yes,resizable=yes');
            return false;
        }
        function ShowWindow(Page, q, att) {

            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                return false;

            }
        }
        function OpenTerms(a, b) {
            window.open('frmTerms.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppBizDocID=' + a + '&Ter=' + b, '', 'toolbar=no,titlebar=no,top=300,width=400,height=100,scrollbars=yes,resizable=yes');
            return false;
        }

        function OpenLogoPage() {
            window.open('frmLogoUpload.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=BizDocs', '', 'toolbar=no,titlebar=no,top=300,width=400,height=100,scrollbars=yes,resizable=yes');
            return false;
        }
        function openClone() {
            window.open('frmCloneDetails.aspx', '', 'toolbar=no,titlebar=no,top=300,width=400,height=100,scrollbars=yes,resizable=yes');
            return false;
        }
        function Save() {
            if (document.Form1.txtDisc.value != '') {
                if (isNaN(parseFloat(document.Form1.txtDisc.value))) {
                    alert("Enter a Valid Discount Percentage!")
                    document.Form1.txtDisc.focus();
                    return false;
                }
            }

            if (parseFloat(document.Form1.txtDisc.value) > 100) {
                alert("Enter a Valid Discount Percentage!")
                document.Form1.txtDisc.focus();
                return false;
            }
        }

        -
            function CheckDisc(eventTarget, eventArgument) {

                if (document.Form1.txtDisc.value != '') {
                    if (isNaN(parseFloat(document.Form1.txtDisc.value))) {
                        alert("Enter a Valid Discount Percentage!")
                        document.Form1.txtDisc.focus();
                        return false;
                    }
                }


                if (parseFloat(document.Form1.txtDisc.value) > 100) {
                    alert("Enter a Valid Discount Percentage!")
                    document.Form1.txtDisc.focus();
                    return false;
                }
            }
        function Close() {
            window.close();
        }

        function CheckNumber(cint) {
            if (cint == 1) {
                if (!(window.event.keyCode > 47 && window.event.keyCode < 58 || window.event.keyCode == 44 || window.event.keyCode == 46)) {
                    window.event.keyCode = 0;
                }
            }
            if (cint == 2) {
                if (!(window.event.keyCode > 47 && window.event.keyCode < 58)) {
                    window.event.keyCode = 0;
                }
            }

        }
        function openApp(a, b, c) {
            if (document.all) {
                window.open('../Documents/frmDocApprovers.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&DocID=' + a + '&DocType=' + b + '&OpID=' + c + "&DocName=" + document.getElementById('lblBizDocIDValue').innerText, '', 'toolbar=no,titlebar=no,left=300,top=450,width=900,height=600,scrollbars=yes,resizable=yes')
            } else {
                window.open('../Documents/frmDocApprovers.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&DocID=' + a + '&DocType=' + b + '&OpID=' + c + "&DocName=" + document.getElementById('lblBizDocIDValue').textContent, '', 'toolbar=no,titlebar=no,left=300,top=450,width=900,height=600,scrollbars=yes,resizable=yes')
            }

            return false;
        }
        function SendEmail(a, b) {
            window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail=' + a + '&pqwRT=' + b, '', 'toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenAtch(a, b, c, d) {
            window.open("../opportunity/frmBizDocAttachments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&BizDocID=" + document.Form1.txtBizDoc.value + "&E=2&OpID=" + a + "&OppBizId=" + b + "&DomainID=" + c + "&ConID=" + c, "", "width=800,height=400,status=no,scrollbars=yes,left=155,top=160");
            return false;
        }
        function openeditAddress(a, b) {
            window.open('../opportunity/frmEditOppAddress.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&AddType=' + a + '&OppID=' + b, '', 'toolbar=no,titlebar=no,left=100,top=350,width=300,height=250,scrollbars=yes,resizable=yes')
            return false;
        }
        function Openfooter() {
            window.open('../opportunity/frmFooterUpload.aspx', '', 'toolbar=no,titlebar=no,top=300,width=400,height=100,scrollbars=yes,resizable=yes');
            return false;
        }
    </script>
    <style type="text/css">
        pre.WordWrap {
            height: auto !important;
            overflow: auto !important;
            overflow-x: auto !important; /* Use horizontal scroller if needed; for Firefox 2, not */
            white-space: pre-wrap !important;
            white-space: -moz-pre-wrap !important; /* Mozilla, since 1999 */
            word-wrap: break-word !important; /* Internet Explorer 5.5+ */
            font-family: Arial,Helvetica,sans-serif !important;
        }
    </style>
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <br>
        <table width="100%" border="0" cellpadding="0" id="tblApproval">
            <tr>
            </tr>
        </table>
        <%--<asp:Panel ID="pnlBizDoc" runat="server">
            <link rel="stylesheet" href="../CSS/master.css" type="text/css" />
            <asp:Table ID="tblOriginalBizDoc" BorderWidth="1" runat="server" Width="100%" BorderColor="black"
                GridLines="None">
                <asp:TableRow>
                    <asp:TableCell>
                        <style>
                            .title
                                {
                                    color: #696969; /*padding:10px;*/
                                    font: bold 30px Arial, Helvetica, sans-serif;
                                }

                                .RowHeader
                                {
                                    background-color: #dedede;
                                    color: #333;
                                    font-family: Arial;
                                    font-size: 8pt;
                                }

                                    .RowHeader .hyperlink
                                    {
                                        color: #333;
                                    }

                                .ItemHeader, .ItemStyle, .AltItemStyle
                                {
                                    border-width: 1px;
                                    padding: 8px;
                                    font: normal 12px/17px arial;
                                    border-style: solid;
                                    border-color: #666666;
                                    background-color: #dedede;
                                }

                                .ItemHeader
                                {
                                    border-width: 1px;
                                    padding: 8px;
                                    border-style: solid;
                                    border-color: #666666;
                                }

                                .ItemStyle td
                                {
                                    border-width: 1px;
                                    padding: 8px;
                                    border-style: solid;
                                    border-color: #666666;
                                    background-color: #ffffff;
                                }

                                .AltItemStyle
                                {
                                    background-color: White;
                                    border-color: black;
                                    padding: 8px;
                                }

                                    .AltItemStyle td
                                    {
                                        padding: 8px;
                                    }

                                .ItemHeader td
                                {
                                    border-width: 1px;
                                    padding: 8px;
                                    font-weight: bold;
                                    border-style: solid;
                                    border-color: #666666;
                                    background-color: #dedede;
                                }

                                    .ItemHeader td th
                                    {
                                        border-width: 1px;
                                        padding: 8px;
                                        border-style: solid;
                                        border-color: #666666;
                                        background-color: #dedede;
                                    }

                                #tblBizDocSumm
                                {
                                    font: normal 12px verdana;
                                    color: #333;
                                    border-width: 1px;
                                    border-color: #666666;
                                    border-collapse: collapse;
                                    background-color: #dedede;
                                }

                                    #tblBizDocSumm td
                                    {
                                        border-width: 1px;
                                        padding: 8px;
                                        border-style: solid;
                                        border-color: #666666;
                                    }

                                .WordWrapSerialNo
                                {
                                    width: 30%;
                                    word-break: break-all;
                                }
                        </style>
                        <table width="100%" border="0" cellpadding="0">
                            <tr>
                                <td valign="top" align="left">
                                    <asp:Image ID="imgLogo" runat="server"></asp:Image>
                                </td>
                                <td align="right" nowrap class="title">
                                    <asp:Label ID="lblOrganizationName" runat="server"></asp:Label>&nbsp;<asp:Label ID="lblOrganizationContactName"
                                        runat="server"></asp:Label>
                                </td>
                                <td align="right" nowrap class="title">
                                    <asp:Label ID="lblBizDoc" ForeColor="#c0c0c0" Font-Name="Arial Narrow" runat="server"
                                        Font-Size="25" Font-Bold="True"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                            </tr>
                        </table>
                        <table width="100%" border="0" cellpadding="0">
                            <tr>
                                <td align="right">
                                    <table width="100%">
                                        <tr>
                                            <td class="RowHeader">Discount
                                            </td>
                                            <td class="RowHeader">Billing Terms
                                            </td>
                                            <td class="RowHeader">
                                                <a id="hplDueDate" runat="server" class="hyperlink" href="#"><font color="white">Due
                                                Date</font></a>
                                            </td>
                                         
                                            <td class="RowHeader">
                                                <asp:Label runat="server" ID="lblBizDocIDLabel" Text="ID#"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="normal1">
                                                <asp:Label ID="lblDiscount" runat="server"></asp:Label>
                                            </td>
                                            <td class="normal1">
                                                <asp:Label ID="lblBillingTerms" runat="server"></asp:Label>
                                                <asp:Label ID="lblBillingTermsName" runat="server"></asp:Label>
                                            </td>
                                            <td class="normal1">
                                                <asp:Label ID="lblDuedate" runat="server"></asp:Label>
                                            </td>
                                            
                                            <td class="normal1">
                                                <asp:Label ID="lblBizDocIDValue" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="normal1">
                                <td colspan="5">
                                    <table height="100%" width="100%">
                                        <tr>
                                            <td class="RowHeader">
                                                <a id="hplBillto" runat="server" class="hyperlink"><font color="white">Bill To</font></a>
                                            </td>
                                            <td class="RowHeader">
                                                <a id="hplShipTo" runat="server" class="hyperlink"><font color="white">Ship To</font></a>
                                            </td>
                                            <td class="RowHeader">Status
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="normal1">
                                                <asp:Label ID="lblBillToAddressName" runat="server"></asp:Label>
                                                <asp:Label ID="lblBillToCompanyName" runat="server"></asp:Label>
                                                <asp:Label ID="lblBillTo" runat="server"></asp:Label>
                                            </td>
                                            <td class="normal1">
                                                <asp:Label ID="lblShipToAddressName" runat="server"></asp:Label>
                                                <asp:Label ID="lblShipToCompanyName" runat="server"></asp:Label>
                                                <asp:Label ID="lblShipTo" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <table height="100%" width="100%">
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="normal1">
                                                            <font color="#333399">
                                                                <asp:HyperLink class="normal1" ID="hplAmountPaid" runat="server" CssClass="hyperlink">Amount Paid:</asp:HyperLink></font>
                                                        </td>
                                                        <td class="normal1">
                                                            <asp:Label ID="lblAmountPaidCurrency" runat="server" Text=""></asp:Label>&nbsp;<asp:Label
                                                                ID="lblAmountPaid" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="normal1">Balance Due:
                                                        </td>
                                                        <td class="normal1">
                                                            <asp:Label ID="lblBalanceDueCurrency" runat="server" Text=""></asp:Label>&nbsp;<asp:Label
                                                                ID="lblBalance" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="normal1">
                                <td colspan="5">
                                    <table height="100%" width="100%">
                                        <tr>
                                            <td class="RowHeader">
                                                <asp:Label ID="lblPONo" runat="server"></asp:Label>&nbsp;#
                                            </td>
                                            <td class="RowHeader">Opportunity or Deal ID
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblNo" runat="server"></asp:Label>
                                            </td>
                                            <td class="normal1">
                                                <asp:HyperLink ID="hplOppID" runat="server" CssClass="hyperlink"> </asp:HyperLink>
                                              
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="normal1">
                                <td class="RowHeader" colspan="5">Comments
                                </td>
                            </tr>
                            <tr class="normal1">
                                <td colspan="5">
                                    <asp:Label ID="lblComments" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr class="normal1">
                                <td colspan="5">
                                    <asp:DataGrid ID="dgBizDocs" runat="server" ForeColor="" Font-Size="10px" Width="100%"
                                        BorderStyle="None" BorderColor="White" BackColor="White" GridLines="Vertical"
                                        HorizontalAlign="Center" AutoGenerateColumns="False">
                                        <AlternatingItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="AltItemStyle"></AlternatingItemStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="ItemStyle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="ItemHeader"></HeaderStyle>
                                        <Columns>
                                        </Columns>
                                    </asp:DataGrid>
                                </td>
                            </tr>
                            
                        </table>
                        <table width="100%">
                            <tr>
                                <td colspan="4">
                                   
                                </td>
                                <td align="right">
                                    <table id="tblBizDocSumm" runat="server" border="0">
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <asp:Image ID="imgFooter" runat="server" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
            <asp:Table ID="tblFormattedBizDoc" Visible="false" BorderWidth="1" runat="server"
                Width="100%" BorderColor="black" GridLines="None">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Literal ID="litBizDocTemplate" runat="server"></asp:Literal>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>--%>
        <asp:Panel ID="pnlBizDoc" runat="server">
            <link rel="stylesheet" href="../CSS/master.css" type="text/css" />
            <asp:Table ID="tblOriginalBizDoc" BorderWidth="1" runat="server" Width="100%" BorderColor="black"
                GridLines="None">
                <asp:TableRow>
                    <asp:TableCell>
                        <style type="text/css">
                            .title {
                                color: #696969; /*padding:10px;*/
                                font: bold 30px Arial, Helvetica, sans-serif;
                            }

                            .RowHeader {
                                background-color: #dedede;
                                color: #333;
                                font-family: Arial;
                                font-size: 8pt;
                            }

                                .RowHeader .hyperlink {
                                    color: #333;
                                }

                            .ItemHeader, .ItemStyle, .AltItemStyle {
                                border-width: 1px;
                                padding: 8px;
                                font: normal 12px/17px arial;
                                border-style: solid;
                                border-color: #666666;
                                background-color: #dedede;
                            }

                            .ItemHeader {
                                border-width: 1px;
                                padding: 8px;
                                border-style: solid;
                                border-color: #666666;
                            }

                            .ItemStyle td {
                                border-width: 1px;
                                padding: 8px;
                                border-style: solid;
                                border-color: #666666;
                                background-color: #ffffff;
                            }

                            .AltItemStyle {
                                background-color: White;
                                border-color: black;
                                padding: 8px;
                            }

                                .AltItemStyle td {
                                    padding: 8px;
                                }

                            .ItemHeader td {
                                border-width: 1px;
                                padding: 8px;
                                font-weight: bold;
                                border-style: solid;
                                border-color: #666666;
                                background-color: #dedede;
                            }

                                .ItemHeader td th {
                                    border-width: 1px;
                                    padding: 8px;
                                    border-style: solid;
                                    border-color: #666666;
                                    background-color: #dedede;
                                }

                            #tblBizDocSumm {
                                font: normal 12px verdana;
                                color: #333;
                                border-width: 1px;
                                border-color: #666666;
                                border-collapse: collapse;
                                background-color: #dedede;
                            }

                                #tblBizDocSumm td {
                                    border-width: 1px;
                                    padding: 8px;
                                    border-style: solid;
                                    border-color: #666666;
                                }

                            .WordWrapSerialNo {
                                width: 30%;
                                word-break: break-all;
                            }
                        </style>
                        <table width="100%" border="0" cellpadding="0">
                            <tr>
                                <td valign="top" align="left">
                                    <asp:Image ID="imgLogo" runat="server"></asp:Image>
                                </td>
                                <td align="right" nowrap>
                                    <asp:Label ID="lblOrganizationName" runat="server"></asp:Label>&nbsp;<asp:Label ID="lblOrganizationContactName"
                                        runat="server"></asp:Label>
                                </td>
                                <td align="right" nowrap class="title">
                                    <asp:Label ID="lblBizDoc" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </td>
                            </tr>
                        </table>
                        <table width="100%" border="0" cellpadding="0">
                            <tr>
                                <td align="right">
                                    <table width="100%">
                                        <tr>
                                            <td class="RowHeader">Discount
                                            </td>
                                            <td class="RowHeader">Billing Terms
                                            </td>
                                            <td class="RowHeader">
                                                <a id="hplDueDate" runat="server" class="hyperlink">Due Date</a>
                                            </td>
                                            <td class="RowHeader">Date Created
                                            </td>
                                            <td class="RowHeader">
                                                <asp:Label runat="server" ID="lblBizDocIDLabel" Text="ID#"></asp:Label>
                                                <%--<font color="white">Invoice#</font>--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="normal1">
                                                <asp:Label ID="lblDiscount" runat="server"></asp:Label>
                                            </td>
                                            <td class="normal1">
                                                <asp:Label ID="lblBillingTerms" runat="server"></asp:Label>
                                                <asp:Label ID="lblBillingTermsName" runat="server"></asp:Label>
                                            </td>
                                            <td class="normal1">
                                                <asp:Label ID="lblDuedate" runat="server"></asp:Label>
                                            </td>
                                            <td class="normal1">
                                                <asp:Label ID="lblDate" runat="server"></asp:Label>
                                            </td>
                                            <td class="normal1">
                                                <asp:Label ID="lblBizDocIDValue" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="normal1">
                                <td colspan="5">
                                    <table height="100%" width="100%">
                                        <tr>
                                            <td class="RowHeader">
                                                <a id="hplBillto" runat="server" class="hyperlink">Bill To</a>
                                            </td>
                                            <td class="RowHeader">
                                                <a id="hplShipTo" runat="server" class="hyperlink">Ship To</a>
                                            </td>
                                            <td class="RowHeader">Status
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="normal1">
                                                <asp:Label ID="lblBillToAddressName" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblBillToCompanyName" runat="server"></asp:Label>
                                                <asp:Label ID="lblBillTo" runat="server"></asp:Label>
                                            </td>
                                            <td class="normal1">
                                                <asp:Label ID="lblShipToAddressName" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblShipToCompanyName" runat="server"></asp:Label>
                                                <asp:Label ID="lblShipTo" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <table height="100%" width="100%">
                                                    <tr>
                                                        <td colspan="2" class="normal1">
                                                            <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="normal1">
                                                            <asp:HyperLink class="normal1" ID="hplAmountPaid" runat="server" CssClass="hyperlink"><font color="#333399">Amount Paid:</font></asp:HyperLink>
                                                        </td>
                                                        <td class="normal1">
                                                            <asp:Label ID="lblAmountPaidCurrency" runat="server" Text=""></asp:Label>&nbsp;<asp:Label
                                                                ID="lblAmountPaid" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="normal1">Balance Due:
                                                        </td>
                                                        <td class="normal1">
                                                            <asp:Label ID="lblBalanceDueCurrency" runat="server" Text=""></asp:Label>&nbsp;<asp:Label
                                                                ID="lblBalance" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>


                            <tr class="normal1">
                                <td colspan="5">
                                    <table runat="server" id="vendorAddress" height="100%" width="100%">
                                        <tr>
                                            <td class="RowHeader">

                                                <a id="hplCusVenBillto" runat="server" class="hyperlink">
                                                    <asp:Label ID="lblVendorBillAddHeader" runat="server"></asp:Label>

                                                </a>
                                            </td>
                                            <td class="RowHeader">
                                                <a id="hplCusVenShipTo" runat="server" class="hyperlink">

                                                    <asp:Label ID="lblVendorShipAddHeader" runat="server"></asp:Label>

                                                </a>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td class="normal1">
                                                <asp:Label ID="lblCusVenBillToAddressName" runat="server"></asp:Label>
                                                <asp:Label ID="lblCusVenBillToCompanyName" runat="server"></asp:Label>
                                            </td>
                                            <td class="normal1">
                                                <asp:Label ID="lblCusVenShipToAddressName" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lblCusVenShipToCompanyName" runat="server"></asp:Label>
                                            </td>

                                        </tr>
                                    </table>
                                </td>
                            </tr>


                            <tr class="normal1">
                                <td colspan="5">
                                    <table height="100%" width="100%">
                                        <tr>
                                            <td class="RowHeader">
                                                <asp:Label ID="lblPONo" runat="server"></asp:Label>&nbsp;#
                                            </td>
                                            <td class="RowHeader">Deal or Order ID
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblNo" runat="server"></asp:Label>
                                            </td>
                                            <td class="normal1">
                                                <asp:HyperLink ID="hplOppID" runat="server" CssClass="hyperlink"> </asp:HyperLink>
                                                <%--<asp:Label ID="lblOppID" runat="server"></asp:Label>--%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="RowHeader" colspan="5">Tracking Numbers
                                </td>
                            </tr>
                            <tr>
                                <td class="RowHeader" colspan="5">
                                    <%--<asp:Label ID="lblTrackingNumbers" runat="server"></asp:Label>--%>
                                    <asp:HyperLink ID="hplTrackingNumbers" runat="server" Target="_blank" />
                                </td>
                            </tr>
                            <tr>
                                <td class="RowHeader" colspan="5">Comments
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5" align="left" valign="top">
                                    <%--<div style="word-break:break-all;width:100%">--%>
                                    <asp:Label ID="lblComments" runat="server"></asp:Label>
                                    <%--</div>--%>
                                </td>
                            </tr>
                            <tr class="normal1">
                                <td colspan="5">
                                    <asp:DataGrid ID="dgBizDocs" runat="server" ForeColor="" Font-Size="10px" Width="100%"
                                        BorderStyle="None" BackColor="White" GridLines="Vertical" HorizontalAlign="Center"
                                        AutoGenerateColumns="False">
                                        <AlternatingItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="AltItemStyle"></AlternatingItemStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="ItemStyle"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="ItemHeader"></HeaderStyle>
                                        <Columns>
                                        </Columns>
                                    </asp:DataGrid>
                                </td>
                            </tr>
                            <%--<tr class="normal1">
                                <td colspan="5">
                                    <asp:Image ID="imgSignature" runat="server" Width="300px"></asp:Image>
                                </td>
                            </tr>--%>
                        </table>
                        <table width="100%">
                            <tr>
                                <td align="right">
                                    <table id="tblBizDocSumm" runat="server" border="0">
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <asp:Image ID="imgFooter" runat="server" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
            <asp:Table ID="tblFormattedBizDoc" Visible="false" BorderWidth="1" runat="server"
                Width="100%" BorderColor="black" GridLines="None">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Literal ID="litBizDocTemplate" runat="server"></asp:Literal>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
        <asp:Panel ID="pnlShow" runat="server" Visible="false">
            <table id="tblButtons" width="100%">
                <tr>
                    <td align="right" class="normal1" colspan="2">
                        <asp:Button ID="btnPrint" runat="server" Text="Print" Width="50" CssClass="button"></asp:Button>

                        <asp:Button ID="btnClose" runat="server" Text="Close" Width="50" CssClass="button"></asp:Button>
                    </td>
                </tr>
                <tr>
                    <td valign="top"></td>
                    <td align="right" class="normal1" valign="top">

                        <asp:HyperLink ID="hplPaymentDetails" Visible="false" runat="server" CssClass="hyperlink">Payment History</asp:HyperLink>
                        <br />
                        <br />

                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="pnlApprove" runat="server">
                            <table width="100%">
                                <tr>
                                    <td class="normal1" align="right">Comments&nbsp;&nbsp;
                                    </td>
                                    <td class="normal1" align="left">
                                        <asp:TextBox runat="server" ID="txtComment" CssClass="signup" Width="340" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal4" align="right">Document is Ready for Approval&nbsp;&nbsp;
                                    </td>
                                    <td class="normal4" align="left">
                                        <asp:Button ID="btnApprove" runat="server" CssClass="button" Text="Approve" />
                                        <asp:Button ID="btnDecline" runat="server" CssClass="button" Text="Decline" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <table id="tblCreditForm" runat="server" visible="false">
                            <tr>
                                <td class="text_bold" colspan="4">Pay By Credit Card
                                </td>
                            </tr>
                            <tr>
                                <td class="normal1" align="right">Credit Card No
                                </td>
                                <td>
                                    <asp:TextBox Width="200" ID="txtCCNo" CssClass="signup" runat="server"></asp:TextBox>
                                </td>
                                <td class="normal1" align="right">Card Holder Name
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCHName" Width="200" runat="server" CssClass="signup"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="normal1" align="right">CVV2 or CVD
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCVV2" CssClass="signup" runat="server"></asp:TextBox>
                                </td>
                                <td class="normal1" align="right">Valid Upto
                                </td>
                                <td>
                                    <asp:DropDownList CssClass="signup" ID="ddlMonth" runat="server">
                                        <asp:ListItem Value="01">01</asp:ListItem>
                                        <asp:ListItem Value="02">02</asp:ListItem>
                                        <asp:ListItem Value="03">03</asp:ListItem>
                                        <asp:ListItem Value="04">04</asp:ListItem>
                                        <asp:ListItem Value="05">05</asp:ListItem>
                                        <asp:ListItem Value="06">06</asp:ListItem>
                                        <asp:ListItem Value="07">07</asp:ListItem>
                                        <asp:ListItem Value="08">08</asp:ListItem>
                                        <asp:ListItem Value="09">09</asp:ListItem>
                                        <asp:ListItem Value="10">10</asp:ListItem>
                                        <asp:ListItem Value="11">11</asp:ListItem>
                                        <asp:ListItem Value="12">12</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:DropDownList CssClass="signup" ID="ddlYear" runat="server">
                                        <asp:ListItem Value="08">08</asp:ListItem>
                                        <asp:ListItem Value="09">09</asp:ListItem>
                                        <asp:ListItem Value="10">10</asp:ListItem>
                                        <asp:ListItem Value="11">11</asp:ListItem>
                                        <asp:ListItem Value="12">12</asp:ListItem>
                                        <asp:ListItem Value="13">13</asp:ListItem>
                                        <asp:ListItem Value="14">14</asp:ListItem>
                                        <asp:ListItem Value="15">15</asp:ListItem>
                                        <asp:ListItem Value="16">16</asp:ListItem>
                                        <asp:ListItem Value="17">17</asp:ListItem>
                                        <asp:ListItem Value="18">18</asp:ListItem>
                                        <asp:ListItem Value="19">19</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="normal1" align="right">Card Type:
                                </td>
                                <td>
                                    <asp:DropDownList runat="server" ID="ddlCardType" CssClass="signup">
                                    </asp:DropDownList>
                                </td>
                                <td class="normal1" align="right">Amount
                                </td>
                                <td>
                                    <asp:TextBox ID="txtAmount" runat="server" CssClass="signup"></asp:TextBox>
                                </td>
                            </tr>
                            <tr id="trCred" runat="server" visible="false">
                                <td class="normal1" align="right">Source Key
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSourceKey" runat="server" CssClass="signup"></asp:TextBox>
                                </td>
                                <td class="normal1" align="right">Pin
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSourcePin" runat="server" CssClass="signup"></asp:TextBox>
                                    <asp:TextBox ID="txtTest" runat="server" CssClass="signup"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center">
                                    <asp:Button ID="btnCharge" runat="server" Text="Submit" CssClass="button" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br />
        </asp:Panel>
        <table width="100%">
            <tr>
                <td class="normal4" align="center">
                    <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
                </td>
            </tr>
        </table>
        <asp:TextBox ID="txtHidden" Style="display: none" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtBizDoc" Style="display: none" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtConEmail" Style="display: none" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtOppOwner" Style="display: none" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtCompName" Style="display: none" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtConID" Style="display: none" runat="server"></asp:TextBox>
        <asp:TextBox ID="txtBizDocRecOwner" Style="display: none" runat="server"></asp:TextBox>
        <input id="hdSubTotal" runat="server" type="hidden" />
        <input id="hdShipAmt" runat="server" type="hidden" />
        <input id="hdTaxAmt" runat="server" type="hidden" />
        <input id="hdCRVTxtAmt" runat="server" type="hidden" />
        <input id="hdDisc" runat="server" type="hidden" />
        <input id="hdLateCharge" runat="server" type="hidden" />
        <input id="hdGrandTotal" runat="server" type="hidden" />
        <input id="hdnCreditAmount" runat="server" type="hidden" />
    </form>
</body>
</html>
