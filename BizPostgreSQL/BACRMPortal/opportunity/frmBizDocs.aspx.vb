'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Partial Class frmBizDocs
    Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents hplNew As System.Web.UI.WebControls.HyperLink
    
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            btnAdd.Attributes.Add("onclick", "return AddBizDocs('" & GetQueryStringVal( "OpID") & "');")
            If Not IsPostBack Then getDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub getDetails()
        Try
            Dim dtTable As DataTable
            Dim objBizDocs As New OppBizDocs
            objBizDocs.OppId = GetQueryStringVal( "OpID")
            objBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            objBizDocs.DomainID = Session("DomainID")
            objBizDocs.UserCntID = Session("UserContactID")
            Dim dsBizDocs As DataSet = objBizDocs.GetBizDocsByOOpId

            If Not dsBizDocs Is Nothing AndAlso dsBizDocs.Tables.Count > 1 Then
                dtTable = dsBizDocs.Tables(1)
            End If

            dgBizDocs.DataSource = dtTable
            dgBizDocs.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgBizDocs_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgBizDocs.ItemCommand
        Try
            Dim objOppBizDocs As New OppBizDocs
            Dim lintAuthorizativeBizDocsId As Integer
            Dim lobjJournalEntry As New JournalEntry
            Dim lintCount As Integer

            If e.CommandName = "Delete" Then
                Dim txtOppBizId As TextBox
                Dim lintJournalId As Integer
                Dim lblBizDocID As Label
                txtOppBizId = e.Item.FindControl("txtBizDocID")
                lblBizDocID = e.Item.FindControl("lblBizDocID")
                Dim objBizDocs As New OppBizDocs()

                objBizDocs.OppBizDocId = txtOppBizId.Text
                objBizDocs.OppId = GetQueryStringVal( "OpID")
                objBizDocs.DomainID = Session("DomainID")
                objBizDocs.UserCntID = Session("UserContactID")

                objOppBizDocs.OppId = GetQueryStringVal( "OpID")
                objOppBizDocs.UserCntID = Session("UserContactID")
                objOppBizDocs.DomainID = Session("DomainID")
                lintCount = objBizDocs.GetOpportunityBizDocsCount()
                objOppBizDocs.OppType = objOppBizDocs.GetOpportunityType()
                lintAuthorizativeBizDocsId = objOppBizDocs.GetAuthorizativeOpportuntiy()
                objOppBizDocs.OppBizDocId = txtOppBizId.Text
                lintJournalId = objOppBizDocs.GetJournalIdForAuthorizativeBizDocs
                If lintAuthorizativeBizDocsId = lblBizDocID.Text Then
                    If lintCount = 0 Then
                        Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                            objBizDocs.DeleteBizDoc()
                            ''To Delete Journal Entry
                            If lintJournalId <> 0 Then
                                lobjJournalEntry.JournalId = lintJournalId
                                lobjJournalEntry.DomainID = Session("DomainId")
                                lobjJournalEntry.DeleteJournalEntryDetails()
                            End If

                            objTransactionScope.Complete()
                        End Using
                        Response.Redirect("frmBizDocs.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=" & GetQueryStringVal("OpID"))
                    Else : litMessage.Text = "You cannot delete BizDocs for which amount is deposited"
                    End If
                Else
                    objBizDocs.DeleteBizDoc()
                    Response.Redirect("frmBizDocs.aspx?OpID=" & GetQueryStringVal( "OpID"))
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgBizDocs_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgBizDocs.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As Button
                Dim lnkDelete As LinkButton
                lnkDelete = e.Item.FindControl("lnkDelete")
                btnDelete = e.Item.FindControl("btnDelete")
                lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                Dim hpl As HyperLink
                hpl = e.Item.FindControl("hplBizdcoc")
                Dim txtOppBizId As TextBox
                txtOppBizId = e.Item.FindControl("txtBizDocID")
                hpl.Attributes.Add("onclick", "return OpenBizInvoice('" & GetQueryStringVal( "OpID") & "','" & txtOppBizId.Text & "')")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function ReturnName(ByVal SDate) As String
        Try
            Dim strDate As String = ""
            If Not IsDBNull(SDate) Then
                strDate = FormattedDateFromDate(SDate, Session("DateFormat"))
                If Format(SDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                    strDate = "<font color=red>" & strDate & "</font>"
                ElseIf Format(SDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, 1, Now()), "yyyyMMdd") Then
                    strDate = "<font color=orange>" & strDate & "</font>"
                End If
            End If
            Return strDate
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    

End Class



