<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmEmailTemplate.aspx.vb" Inherits="BACRMPortal.frmEmailTemplate" %>

<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
     <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
</head>
<body>
    <form id="form1" runat="server">
    
    <table cellSpacing="0" cellPadding="0" width="100%" >
        <tr>
            <td align="left">
                	<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;Prepare Email
									Template&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
            </td>
            <td align="right">
                <asp:button id="btnSaveClose" Runat="server" text="Save & Close" CssClass="button"></asp:button>						
                 <asp:button id="btnClose" Runat="server" text="Close" OnClientClick="javascript:self.close()" CssClass="button"></asp:button>
            </td>
        </tr>
    </table>
    <asp:Table runat="server" CssClass="aspTable" BorderColor="black" BorderWidth="1" CellPadding="0" CellSpacing="0" Width="100%">
        <asp:TableRow CssClass="normal1" Height="300" VerticalAlign="Top">
            <asp:TableCell >
              <table width="100%">
						
						    <tr>
						        <td width="10%" align="right">
						            Template Name
						        </td>
						        <td>
						            <asp:TextBox ID="txtTemplateName" Runat="server" CssClass="signup" Width="230"></asp:TextBox>
						            <asp:CheckBox ID='forMarketingDept' runat="server" Text="Marketing Dept" />
						        </td>
						    </tr>
						
							<tr>
								<td class="normal1" align="right">
									Subject
								</td>
								<td align="left" >
									<asp:TextBox ID="txtSubject" Runat="server" CssClass="signup" Width="500"></asp:TextBox>
									
								</td>
							</tr>
							<tr>
								<td class="normal1" align="right" valign="top">
									Message
								
				                <ig_spell:WebSpellChecker ID="oSpellChecker" runat="server"></ig_spell:WebSpellChecker>
				                </td>
								<td align="left" valign="top" width="300">
									
									 <ighedit:WebHtmlEditor ImageDirectory="../images/htmleditor/" Width="100%" Height="300" id="oEditHtml" SpellCheckerId="oSpellChecker" runat="server" FontFormattingList="Heading 1=<h1>&amp;Heading 2=<h2>&amp;Heading 3=<h3>&amp;Heading 4=<h4>&amp;Heading 5=<h5>&amp;Normal=<p>"
                SpecialCharacterList="&amp;#937;,&amp;#931;,&amp;#916;,&amp;#934;,&amp;#915;,&amp;#936;,&amp;#928;,&amp;#920;,&amp;#926;,&amp;#923;,&amp;#958;,&amp;#956;,&amp;#951;,&amp;#966;,&amp;#969;,&amp;#949;,&amp;#952;,&amp;#948;,&amp;#950;,&amp;#968;,&amp;#946;,&amp;#960;,&amp;#963;,&amp;szlig;,&amp;thorn;,&amp;THORN;,&amp;#402,&amp;#1041;,&amp;#1046;,&amp;#1044;,&amp;#1062;,&amp;#1064;,&amp;#1070;,&amp;#1071;,&amp;#1073;,&amp;#1078;,&amp;#1092;,&amp;#1096;,&amp;#1102;,&amp;#1103;,&amp;#12362;,&amp;#12354;,&amp;#32117;,&amp;#25152;,&amp;AElig;,&amp;Aring;,&amp;Ccedil;,&amp;ETH;,&amp;Ntilde;,&amp;Ouml;,&amp;aelig;,&amp;aring;,&amp;atilde;,&amp;auml;,&amp;ccedil;,&amp;ecirc;,&amp;eth;,&amp;euml;,&amp;ntilde;,&amp;cent;,&amp;pound;,&amp;curren;,&amp;yen;,&amp;#8470;,&amp;#153;,&amp;copy;,&amp;reg;,&amp;#151;,@,&amp;#149;,&amp;iexcl;,&amp;#14;,&amp;#18;,&amp;#24;,&amp;#26;,&amp;#27;,&amp;brvbar;,&amp;sect;,&amp;uml;,&amp;ordf;,&amp;not;,&amp;macr;,&amp;para;,&amp;deg;,&amp;plusmn;,&amp;laquo;,&amp;raquo;,&amp;middot;,&amp;cedil;,&amp;ordm;,&amp;sup1;,&amp;sup2;,&amp;sup3;,&amp;frac14;,&amp;frac12;,&amp;frac34;,&amp;iquest;,&amp;times;,&amp;divide;"
                FontNameList="Arial,Verdana,Tahoma,Courier New,Georgia" FontStyleList="Blue Underline=color:blue;text-decoration:underline;&amp;Red Bold=color:red;font-weight:bold;&amp;ALL CAPS=text-transform:uppercase;&amp;all lowercase=text-transform:lowercase;&amp;Reset="
                FontSizeList="1,2,3,4,5,6,7">
                <DialogStyle Font-Size="8pt" Font-Names="sans-serif" BorderWidth="1px"  ForeColor="Black" BorderStyle="Solid"
                    BorderColor="Black" BackColor="#ECE9D8"></DialogStyle>
                <Toolbar>
                    <ighedit:ToolbarImage Type="DoubleSeparator"></ighedit:ToolbarImage>
                    <ighedit:ToolbarButton Type="Bold"></ighedit:ToolbarButton>
                    <ighedit:ToolbarButton Type="Italic"></ighedit:ToolbarButton>
                    <ighedit:ToolbarButton Type="Underline"></ighedit:ToolbarButton>
                    <ighedit:ToolbarButton Type="Strikethrough"></ighedit:ToolbarButton>
                    <ighedit:ToolbarImage Type="Separator"></ighedit:ToolbarImage>
                    <ighedit:ToolbarButton Type="Subscript"></ighedit:ToolbarButton>
                    <ighedit:ToolbarButton Type="Superscript"></ighedit:ToolbarButton>
                    <ighedit:ToolbarImage Type="Separator"></ighedit:ToolbarImage>
                    <ighedit:ToolbarButton Type="Cut"></ighedit:ToolbarButton>
                    <ighedit:ToolbarButton Type="Copy"></ighedit:ToolbarButton>
                    <ighedit:ToolbarButton Type="Paste"></ighedit:ToolbarButton>
                    <ighedit:ToolbarImage Type="Separator"></ighedit:ToolbarImage>
                    <ighedit:ToolbarButton Type="Undo"></ighedit:ToolbarButton>
                    <ighedit:ToolbarButton Type="Redo"></ighedit:ToolbarButton>
                    <ighedit:ToolbarImage Type="Separator"></ighedit:ToolbarImage>
                    <ighedit:ToolbarButton Type="JustifyLeft"></ighedit:ToolbarButton>
                    <ighedit:ToolbarButton Type="JustifyCenter"></ighedit:ToolbarButton>
                    <ighedit:ToolbarButton Type="JustifyRight"></ighedit:ToolbarButton>
                    <ighedit:ToolbarButton Type="JustifyFull"></ighedit:ToolbarButton>
                    <ighedit:ToolbarImage Type="Separator"></ighedit:ToolbarImage>
                    <ighedit:ToolbarButton Type="Indent"></ighedit:ToolbarButton>
                    <ighedit:ToolbarButton Type="Outdent"></ighedit:ToolbarButton>
                    <ighedit:ToolbarImage Type="Separator"></ighedit:ToolbarImage>
                    <ighedit:ToolbarButton Type="UnorderedList"></ighedit:ToolbarButton>
                    <ighedit:ToolbarButton Type="OrderedList"></ighedit:ToolbarButton>
                    <ighedit:ToolbarImage Type="Separator"></ighedit:ToolbarImage>
                    <ighedit:ToolbarDialogButton Type="InsertRule">
                        <Dialog Strings="" InternalDialogType="InsertRule"></Dialog>
                    </ighedit:ToolbarDialogButton>
                    <ighedit:ToolbarImage Type="RowSeparator"></ighedit:ToolbarImage>
                    <ighedit:ToolbarImage Type="DoubleSeparator"></ighedit:ToolbarImage>
                    <ighedit:ToolbarDialogButton Type="FontColor">
                        <Dialog Strings=""></Dialog>
                    </ighedit:ToolbarDialogButton>
                    <ighedit:ToolbarDialogButton Type="FontHighlight">
                        <Dialog Strings=""></Dialog>
                    </ighedit:ToolbarDialogButton>
                    <ighedit:ToolbarDialogButton Type="SpecialCharacter">
                        <Dialog Strings="" InternalDialogType="SpecialCharacterPicker" Type="InternalWindow"></Dialog>
                    </ighedit:ToolbarDialogButton>
                    <ighedit:ToolbarMenuButton Type="InsertTable">
                        <Menu Width="80px">
                            <ighedit:HtmlBoxMenuItem Act="TableProperties">
                                <Dialog Strings="" InternalDialogType="InsertTable"></Dialog>
                            </ighedit:HtmlBoxMenuItem>
                            <ighedit:HtmlBoxMenuItem Act="InsertColumnRight"></ighedit:HtmlBoxMenuItem>
                            <ighedit:HtmlBoxMenuItem Act="InsertColumnLeft"></ighedit:HtmlBoxMenuItem>
                            <ighedit:HtmlBoxMenuItem Act="InsertRowAbove"></ighedit:HtmlBoxMenuItem>
                            <ighedit:HtmlBoxMenuItem Act="InsertRowBelow"></ighedit:HtmlBoxMenuItem>
                            <ighedit:HtmlBoxMenuItem Act="DeleteRow"></ighedit:HtmlBoxMenuItem>
                            <ighedit:HtmlBoxMenuItem Act="DeleteColumn"></ighedit:HtmlBoxMenuItem>
                            <ighedit:HtmlBoxMenuItem Act="IncreaseColspan"></ighedit:HtmlBoxMenuItem>
                            <ighedit:HtmlBoxMenuItem Act="DecreaseColspan"></ighedit:HtmlBoxMenuItem>
                            <ighedit:HtmlBoxMenuItem Act="IncreaseRowspan"></ighedit:HtmlBoxMenuItem>
                            <ighedit:HtmlBoxMenuItem Act="DecreaseRowspan"></ighedit:HtmlBoxMenuItem>
                            <ighedit:HtmlBoxMenuItem Act="CellProperties">
                                <Dialog Strings="" InternalDialogType="CellProperties"></Dialog>
                            </ighedit:HtmlBoxMenuItem>
                            <ighedit:HtmlBoxMenuItem Act="TableProperties">
                                <Dialog Strings="" InternalDialogType="ModifyTable"></Dialog>
                            </ighedit:HtmlBoxMenuItem>
                        </Menu>
                    </ighedit:ToolbarMenuButton>
                    <ighedit:ToolbarButton Type="ToggleBorders"></ighedit:ToolbarButton>
                    <ighedit:ToolbarImage Type="Separator"></ighedit:ToolbarImage>
                    <ighedit:ToolbarButton Type="InsertLink"></ighedit:ToolbarButton>
                    <ighedit:ToolbarButton Type="RemoveLink"></ighedit:ToolbarButton>
                    <ighedit:ToolbarImage Type="Separator"></ighedit:ToolbarImage>
                    <ighedit:ToolbarButton Type="Save" RaisePostback="True"></ighedit:ToolbarButton>
                    <ighedit:ToolbarUploadButton Type="Open">
                        <Upload Filter="*.htm,*.html,*.asp,*.aspx" Height="350px" Mode="File" Width="500px" Strings=""></Upload>
                    </ighedit:ToolbarUploadButton>
                    <ighedit:ToolbarButton Type="Preview"></ighedit:ToolbarButton>
                    <ighedit:ToolbarImage Type="Separator"></ighedit:ToolbarImage>
                    <ighedit:ToolbarDialogButton Type="FindReplace">
                        <Dialog Strings="" InternalDialogType="FindReplace"></Dialog>
                    </ighedit:ToolbarDialogButton>
                    <ighedit:ToolbarDialogButton Type="InsertBookmark">
                        <Dialog Strings="" InternalDialogType="InsertBookmark"></Dialog>
                    </ighedit:ToolbarDialogButton>
                    <ighedit:ToolbarUploadButton Type="InsertImage">
                        <Upload Height="420px" Width="500px" Strings=""></Upload>
                    </ighedit:ToolbarUploadButton>
                    <ighedit:ToolbarUploadButton Type="InsertFlash">
                        <Upload Filter="*.swf" Height="440px" Mode="Flash" Width="500px" Strings=""></Upload>
                    </ighedit:ToolbarUploadButton>
                    <ighedit:ToolbarUploadButton Type="InsertWindowsMedia">
                        <Upload Filter="*.asf,*.wma,*.wmv,*.wm,*.avi,*.mpg,*.mpeg,*.m1v,*.mp2,*.mp3,*.mpa,*.mpe,*.mpv2,*.m3u,*.mid,*.midi,*.rmi,*.aif,*.aifc,*.aiff,*.au,*.snd,*.wav,*.cda,*.ivf"
                            Height="400px" Mode="WindowsMedia" Width="500px" Strings=""></Upload>
                    </ighedit:ToolbarUploadButton>
                   
                    <ighedit:ToolbarButton Type="CleanWord"></ighedit:ToolbarButton>
                    <ighedit:ToolbarButton Type="WordCount"></ighedit:ToolbarButton>
                    <ighedit:ToolbarButton Type="PasteHtml"></ighedit:ToolbarButton>
                    <ighedit:ToolbarMenuButton Type="Zoom">
                        <Menu Width="180px">
                            <ighedit:HtmlBoxMenuItem Act="Zoom25"></ighedit:HtmlBoxMenuItem>
                            <ighedit:HtmlBoxMenuItem Act="Zoom50"></ighedit:HtmlBoxMenuItem>
                            <ighedit:HtmlBoxMenuItem Act="Zoom75"></ighedit:HtmlBoxMenuItem>
                            <ighedit:HtmlBoxMenuItem Act="Zoom100"></ighedit:HtmlBoxMenuItem>
                            <ighedit:HtmlBoxMenuItem Act="Zoom200"></ighedit:HtmlBoxMenuItem>
                            <ighedit:HtmlBoxMenuItem Act="Zoom300"></ighedit:HtmlBoxMenuItem>
                            <ighedit:HtmlBoxMenuItem Act="Zoom400"></ighedit:HtmlBoxMenuItem>
                            <ighedit:HtmlBoxMenuItem Act="Zoom500"></ighedit:HtmlBoxMenuItem>
                            <ighedit:HtmlBoxMenuItem Act="Zoom600"></ighedit:HtmlBoxMenuItem>
                        </Menu>
                    </ighedit:ToolbarMenuButton>
                    <ighedit:ToolbarButton Type="TogglePositioning"></ighedit:ToolbarButton>
                    <ighedit:ToolbarButton Type="BringForward"></ighedit:ToolbarButton>
                    <ighedit:ToolbarButton Type="SendBackward"></ighedit:ToolbarButton>
                    <ighedit:ToolbarImage Type="RowSeparator"></ighedit:ToolbarImage>
                    <ighedit:ToolbarImage Type="DoubleSeparator"></ighedit:ToolbarImage>
                    <ighedit:ToolbarDropDown Type="FontName"></ighedit:ToolbarDropDown>
                    <ighedit:ToolbarDropDown Type="FontSize"></ighedit:ToolbarDropDown>
                    <ighedit:ToolbarDropDown Type="FontFormatting"></ighedit:ToolbarDropDown>
                    <ighedit:ToolbarDropDown Type="FontStyle"></ighedit:ToolbarDropDown>
                    <ighedit:ToolbarDropDown Type="Insert"   >
                        <Items >
                            <ighedit:ToolbarDropDownItem  ></ighedit:ToolbarDropDownItem>
                            
                        </Items>
                    </ighedit:ToolbarDropDown>
                     
                    <ighedit:ToolbarButton Type="SpellCheck"></ighedit:ToolbarButton>
                </Toolbar>
                <RightClickMenu>
                    <ighedit:HtmlBoxMenuItem Act="Cut"></ighedit:HtmlBoxMenuItem>
                    <ighedit:HtmlBoxMenuItem Act="Copy"></ighedit:HtmlBoxMenuItem>
                    <ighedit:HtmlBoxMenuItem Act="Paste"></ighedit:HtmlBoxMenuItem>
                    <ighedit:HtmlBoxMenuItem Act="PasteHtml"></ighedit:HtmlBoxMenuItem>
                    <ighedit:HtmlBoxMenuItem Act="CellProperties">
                        <Dialog Strings="" InternalDialogType="CellProperties"></Dialog>
                    </ighedit:HtmlBoxMenuItem>
                    <ighedit:HtmlBoxMenuItem Act="TableProperties">
                        <Dialog Strings="" InternalDialogType="ModifyTable"></Dialog>
                    </ighedit:HtmlBoxMenuItem>
                    <ighedit:HtmlBoxMenuItem Act="InsertImage"></ighedit:HtmlBoxMenuItem>
                </RightClickMenu>
            </ighedit:WebHtmlEditor>
								</td>
							</tr>
						</table>
						<br>
            </asp:TableCell>
            
        </asp:TableRow>
    </asp:Table>
    </form>
</body>
</html>
--%>