Imports BACRM.BusinessLogic.Marketing
Imports Infragistics.WebUI.WebHtmlEditor
Imports System.IO
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Partial Public Class frmEmailTemplate : Inherits BACRMPage

    Dim AlertDTLID As Long = 0
    Dim objCamp As Campaign

    'Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '    Try
    '        oEditHtml.UploadedFilesDirectory = ConfigurationManager.AppSettings("StateAndCountryList")
    '        If Not GetQueryStringVal( "AlertDTLID") Is Nothing Then
    '            AlertDTLID = GetQueryStringVal( "AlertDTLID")
    '        End If
    '        If Not IsPostBack Then BindMergeFields()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Sub BindMergeFields()
    '    Try

    '        Dim dtTable As DataTable
    '        Dim objDocument As New DocumentList
    '        objDocument.ModuleID = 1
    '        objDocument.byteMode = 0
    '        dtTable = objDocument.GetMergeFieldsByModuleID()

    '        Dim ToolBarDropDown As New ToolbarDropDown
    '        ' oEditHtml.FindByKeyOrAction("Insert").Controls.Clear()
    '        ToolBarDropDown = oEditHtml.FindByKeyOrAction("Insert")
    '        ToolBarDropDown.Width = Unit.Pixel(150)
    '        'ToolBarDropDown.Items.Add(New ToolbarDropDownItem("First Name", "##vcFirstName##"))
    '        'ToolBarDropDown.Items.Add(New ToolbarDropDownItem("Last Name", "##vcLastName##"))
    '        'ToolBarDropDown.Items.Add(New ToolbarDropDownItem("Company Name", "##vcCompanyName##"))
    '        'ToolBarDropDown.Items.Add(New ToolbarDropDownItem("Shipping Address", "##Ship##"))
    '        Dim i As Integer = 0
    '        For i = 0 To dtTable.Rows.Count - 1
    '            ToolBarDropDown.Items.Add(New ToolbarDropDownItem(dtTable.Rows(i).Item(0), dtTable.Rows(i).Item(1)))

    '            'str(i, 0) = dSMergerFields.Tables(0).Rows(i).Item(0)
    '            'str(i, 1) = dSMergerFields.Tables(0).Rows(i).Item(1)
    '        Next
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
    '    Try
    '        Save()
    '        Dim strScript As String = "<script language=JavaScript>"
    '        strScript += "window.opener.reLoad(); self.close();"
    '        strScript += "</script>"
    '        If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Sub Save()
    '    Try
    '        If CCommon.ToLong(Session("DomainId")) = 0 Then
    '            Response.Redirect("../admin/authentication.aspx?mesg=AS")
    '        End If
    '        Dim objDocuments As New DocumentList
    '        With objDocuments
    '            .DomainID = Session("DomainId")
    '            .UserCntID = Session("UserContactID")
    '            .UrlType = ""
    '            .DocCategory = 369
    '            .FileType = ""
    '            .DocName = txtTemplateName.Text
    '            .Subject = txtSubject.Text
    '            .DocDesc = oEditHtml.Text
    '            If forMarketingDept.Checked Then
    '                .IsForMarketingDept = 1
    '            Else : .IsForMarketingDept = 0
    '            End If
    '            .DocumentType = 1 '1=generic,2=specific
    '            .SaveDocuments()
    '        End With
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

End Class