<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmHelpTree.aspx.vb" Inherits="BACRMPortal.frmHelpTree" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Help</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
</head>
<body bottommargin="0" leftmargin="0" topmargin="0" rightmargin="0">
    <form id="Form1" method="post" runat="server">
    <table height="100%" cellspacing="0" width="100%" border="0">
        <tr>
            <td bgcolor="#f1f1f1">
                <a href="http://www.bizautomation.com" style="font-size: 11px">Business Managament Software
                    from BizAutomation.com</a>
            </td>
            <td width="1" bgcolor="black">
            </td>
            <td valign="top" width="82%">
            </td>
        </tr>
        <tr>
            <td valign="top" width="18%" bgcolor="#f1f1f1">
                <asp:TreeView ID="Treeview1" runat="server" SelectedStyle="FONT-SIZE: 11px; COLOR: black; FONT-FAMILY: Arial;BACKGROUND-COLOR: white;BACKGROUND-COLOR: transparent"
                    HoverStyle="FONT-SIZE: 11px; COLOR: black; FONT-FAMILY: Arial;TEXT-DECORATION: underline;BACKGROUND-COLOR: #DFDFDF"
                    DefaultStyle="FONT-SIZE: 11px; COLOR: black; FONT-FAMILY: Arial" NAME="Treeview1">
                    <Nodes>
                        <asp:TreeNode NavigateUrl="frmHelptree.aspx?Page=Tickler" Text="Tickler"></asp:TreeNode>
                        <asp:TreeNode NavigateUrl="frmHelptree.aspx?Page=Organizations" Text="Organizations &amp; Relationships">
                        </asp:TreeNode>
                        <asp:TreeNode Text="Contacts" NavigateUrl="frmHelptree.aspx?Page=Contacts"></asp:TreeNode>
                        <asp:TreeNode Text="Opportunities &amp; Deals" NavigateUrl="frmHelptree.aspx?Page=Opportunities">
                        </asp:TreeNode>
                        <asp:TreeNode Text="Forecasting" NavigateUrl="frmHelptree.aspx?Page=Forecasting">
                        </asp:TreeNode>
                        <asp:TreeNode Text="Marketing">
                            <asp:TreeNode NavigateUrl="frmHelptree.aspx?Page=MarketingCampaigns" Text="Campaigns">
                            </asp:TreeNode>
                            <asp:TreeNode NavigateUrl="frmHelptree.aspx?Page=MarketingSurveys" Text="Surveys">
                            </asp:TreeNode>
                            <asp:TreeNode Text="Email Broadcasting" NavigateUrl="frmHelptree.aspx?Page=MarketingEmail">
                            </asp:TreeNode>
                        </asp:TreeNode>
                        <asp:TreeNode Text="Service &amp; Support" NavigateUrl="frmHelptree.aspx?Page=Service">
                        </asp:TreeNode>
                        <asp:TreeNode Text="Projects" NavigateUrl="frmHelptree.aspx?Page=Projects"></asp:TreeNode>
                        <asp:TreeNode Text="Documents" NavigateUrl="frmHelptree.aspx?Page=Documents"></asp:TreeNode>
                        <asp:TreeNode Text="BizDocs<i>(Invoices, P.O.s,etc..)</i>" NavigateUrl="frmHelptree.aspx?Page=BizDocs">
                        </asp:TreeNode>
                        <asp:TreeNode Text="Search &amp; Search Results" NavigateUrl="frmHelptree.aspx?Page=Search">
                        </asp:TreeNode>
                        <asp:TreeNode Text="Importing Records" NavigateUrl="frmHelptree.aspx?Page=Importing">
                        </asp:TreeNode>
                        <asp:TreeNode Text="Admin Panel">
                            <asp:TreeNode Text="Master List Administration" NavigateUrl="frmHelptree.aspx?Page=Master">
                            </asp:TreeNode>
                            <asp:TreeNode Text="BizForms Wizard" NavigateUrl="frmHelptree.aspx?Page=BizForms">
                            </asp:TreeNode>
                            <asp:TreeNode Text="Business Processes" NavigateUrl="frmHelptree.aspx?Page=Business">
                            </asp:TreeNode>
                            <asp:TreeNode Text="Auto Routing Rules" NavigateUrl="frmHelptree.aspx?Page=Routing">
                            </asp:TreeNode>
                            <asp:TreeNode Text="Item &amp; Price Book Management" NavigateUrl="frmHelptree.aspx?Page=Item">
                            </asp:TreeNode>
                            <asp:TreeNode Text="User Access Settings" NavigateUrl="frmHelptree.aspx?Page=User">
                            </asp:TreeNode>
                            <asp:TreeNode Text="Areas Of Interest<i>(AOIs)</i>" NavigateUrl="frmHelptree.aspx?Page=Areas">
                            </asp:TreeNode>
                            <asp:TreeNode Text="BPA &amp; Email Alerts" NavigateUrl="frmHelptree.aspx?Page=BPA">
                            </asp:TreeNode>
                            <asp:TreeNode Text="Accounting Integration Setup" NavigateUrl="frmHelptree.aspx?Page=Accounting">
                            </asp:TreeNode>
                            <asp:TreeNode Text="Custom Field Builder" NavigateUrl="frmHelptree.aspx?Page=Custom">
                            </asp:TreeNode>
                            <asp:TreeNode Text="Associations &amp; Data Cleansing" NavigateUrl="frmHelptree.aspx?Page=Associations">
                            </asp:TreeNode>
                        </asp:TreeNode>
                        <asp:TreeNode Text="BizPortal<i>(Self-Service Business Portal)</i>" NavigateUrl="frmHelptree.aspx?Page=BizPortal">
                        </asp:TreeNode>
                        <asp:TreeNode Text="Reports" NavigateUrl="frmHelptree.aspx?Page=Reports"></asp:TreeNode>
                        <asp:TreeNode Text="E-Commerce / Order Entry" NavigateUrl="frmHelptree.aspx?Page=E-Commerce">
                        </asp:TreeNode>
                        <asp:TreeNode Text="Financials<i>(Terms, Bill me options, etc..)</i>" NavigateUrl="frmHelptree.aspx?Page=Financials">
                        </asp:TreeNode>
                        <asp:TreeNode Text="System Configuration" NavigateUrl="frmHelptree.aspx?Page=System">
                        </asp:TreeNode>
                        <asp:TreeNode Text="Glossary" NavigateUrl="frmHelptree.aspx?Page=Glossary"></asp:TreeNode>
                    </Nodes>
                </asp:TreeView>
            </td>
            <td width="1" bgcolor="black" height="100%">
            </td>
            <td valign="top" width="82%" height="100%">
                <iframe id="ifHelp" src="about:blank" frameborder="no" width="100%" scrolling="auto"
                    height="100%" runat="server"></iframe>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
