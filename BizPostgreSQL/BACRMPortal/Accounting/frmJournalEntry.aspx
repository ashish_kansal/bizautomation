﻿<%--<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmJournalEntry.aspx.vb"
    Inherits="BACRMPortal.frmJournalEntry" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <title>Bank Register</title>
    <link rel="stylesheet" type="text/css" href="~/CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="~/CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script language="javascript" type="text/javascript">
        function CheckNumber(cint) {
            if (cint == 1) {
                if (!(window.event.keyCode > 47 && window.event.keyCode < 58 || window.event.keyCode == 44 || window.event.keyCode == 46)) {
                    window.event.keyCode = 0;
                }
            }
            if (cint == 2) {
                if (!(window.event.keyCode > 47 && window.event.keyCode < 58)) {
                    window.event.keyCode = 0;
                }
            }

        }

        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }

        function reDirect(a) {
            document.location.href = a;
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function OpenBizIncome(url) {
            window.open(url, "BizInvoice", 'toolbar=no,titlebar=no,left=200, top=300,width=750,height=550,scrollbars=no,resizable=yes');
            return false;
        }
        function OpenTimeExpense(url) {
            window.open(url, "TimeExoenses", 'toolbar=no,titlebar=no,left=200, top=300,width=650,height=400,scrollbars=no,resizable=yes');
            return false;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server" method="post">
    <br />
    <table id="Table1" height="2" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td valign="bottom">
                <table class="TabStyle">
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;
                            <asp:Label ID="lblJournalEntryTitle" runat="server" Text="Bank Register"></asp:Label>&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td class="normal1" width="30px">
                <asp:Label ID="JournalDate" runat="server" Text="Date: "></asp:Label>
            </td>
            <td class="normal1" align="left" width="30px">
                <BizCalendar:Calendar ID="calFrom" runat="server" />
            </td>
            <td align="right" width="85px">
                <asp:Button ID="btnGo" runat="server" Text="Go" CssClass="button"></asp:Button>
                <asp:Button ID="btnBack" CssClass="button" runat="server" Text="Back" Width="50">
                </asp:Button>
            </td>
        </tr>
    </table>
    <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Height="384px">
        <asp:TableRow ID="TableRow1" VerticalAlign="Top" runat="server">
            <asp:TableCell ID="TableCell1" runat="server">
                <table id="Table2" height="2" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td valign="bottom">
                            <asp:DataGrid ID="dgJournalEntry" runat="server" CssClass="dg" Width="100%" BorderColor="white"
                                AutoGenerateColumns="False">
                                <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                <ItemStyle CssClass="is"></ItemStyle>
                                <HeaderStyle CssClass="hs"></HeaderStyle>
                                <Columns>
                                    <asp:TemplateColumn ItemStyle-Wrap="False" ItemStyle-VerticalAlign="Bottom">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" Text="Edit" ID="lnkbtnEdt" CommandName="Edit"></asp:LinkButton>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:LinkButton ID="lnkbtnUpdt" runat="server" Text="Update" CommandName="Update"></asp:LinkButton>&nbsp;
                                            <asp:LinkButton runat="server" Text="Cancel" CommandName="Cancel" ID="lnkbtnCncl"></asp:LinkButton>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="JournalId" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="numTransactionId" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="CheckId" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="CashCreditCardId" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="numChartAcntId" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="numOppId" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="numOppBizDocsId" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="numDepositId" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="numBizDocsPaymentDetId" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="numCategoryHDRID" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="tintTEType" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="numCategory" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="numUserCntID" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="dtFromDate" Visible="false"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="<font color=white>Entry Date</font>">
                                        <ItemTemplate>
                                            <asp:Label ID="lblJDate" runat="server" Text='<%# ReturnDate(DataBinder.Eval(Container.DataItem, "EntryDate")) %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Label ID="lblEditDate" runat="server" Text='<%# ReturnDate(DataBinder.Eval(Container.DataItem, "EntryDate")) %>'></asp:Label>
                                            
                                            <BizCalendar:Calendar ID="txtJournalDateDisplay" runat="server" SelectedDate='<%# DataBinder.Eval(Container.DataItem, "EntryDate") %>' />
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Transaction Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTransactiontype" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.TransactionType") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Label ID="lblTransactiontype1" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.TransactionType") %>'></asp:Label>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCompany" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.CompanyName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:Label ID="lblJournalId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.JournalId") %>'></asp:Label>
                                            <asp:Label ID="lblTransactionId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.numTransactionId") %>'></asp:Label>
                                            <asp:Label ID="lblCheckId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.CheckId") %>'></asp:Label>
                                            <asp:Label ID="lblCashCreditCardId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.CashCreditCardId") %>'></asp:Label>
                                            <asp:Label ID="lblDivisionId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.numCustomerId") %>'></asp:Label>
                                            <asp:Label ID="lblChartAcntId" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.numChartAcntId") %>'></asp:Label>
                                            <asp:TextBox ID="txtCompanyName" Width="60px" runat="server" CssClass="signup"></asp:TextBox>
                                            <asp:Button ID="btnCompanyName" CommandName="Go" Text="Go" runat="server" CssClass="button" />
                                            <asp:DropDownList ID="ddlCompany" runat="server" CssClass="signup">
                                            </asp:DropDownList>
                                            <asp:Label ID="lblCompany" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.CompanyName") %>'></asp:Label>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Memo">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMemo" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Memo") %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtEDesc" TextMode="MultiLine" Width="300" runat="server" CssClass="signup"
                                                Text='<%# DataBinder.Eval(Container,"DataItem.Memo") %>'>
                                            </asp:TextBox>
                                            <asp:Label ID="lblEditMemo" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Memo") %>'></asp:Label>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Deposit">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDeposit" runat="server" Text='<%# ReturnMoney(DataBinder.Eval(Container.DataItem, "Deposit")) %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtDeposit" runat="server" CssClass="signup" Text='<%# ReturnMoney(DataBinder.Eval(Container.DataItem, "Deposit")) %>'></asp:TextBox>
                                            <asp:Label ID="lblDepositAmt" runat="server" Text='<%# ReturnMoney(DataBinder.Eval(Container.DataItem, "Deposit")) %>'></asp:Label>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Payment" ItemStyle-VerticalAlign="Bottom">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPayment" runat="server" Text='<%# ReturnMoney(DataBinder.Eval(Container,"DataItem.Payment")) %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtPayment" runat="server" CssClass="signup" Text='<%# ReturnMoney(DataBinder.Eval(Container,"DataItem.Payment")) %>'></asp:TextBox>
                                            <asp:Label ID="lblPaymentAmt" runat="server" Text='<%# ReturnMoney(DataBinder.Eval(Container.DataItem, "Payment")) %>'></asp:Label>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Balance">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBalance" runat="server" Text='<%# ReturnMoney(DataBinder.Eval(Container.DataItem, "numBalance")) %>'>
                                            </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" Text="Edit" ID="lnkbtnEditJournal" CommandName="EditJournalEntry"></asp:LinkButton>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <asp:Button ID="btnDeleteAction" runat="server" CssClass="Delete" Text="X" CommandName="Delete">
                                            </asp:Button>
                                            <asp:LinkButton ID="lnkDeleteAction" runat="server" Visible="false">
																	<font color="#730000">*</font></asp:LinkButton>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    </form>
</body>
</html>--%>
