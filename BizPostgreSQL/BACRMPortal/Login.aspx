<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Login.aspx.vb" Inherits="BACRMPortal.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    <title>Customer Portal - Total BizAutomation</title>
    <script src="JavaScript/jquery.min.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
        function Login() {
            if (document.Form1.txtEmaillAdd.value == "") {
                alert("Enter Email Address")
                document.Form1.txtEmaillAdd.focus()
                return false;
            }
            if (document.Form1.txtPassword.value == "") {
                alert("Enter Password")
                document.Form1.txtPassword.focus()
                return false;
            }
            document.Form1.txtOffset.value = new Date().getTimezoneOffset();
        }
        function Forgot() {
            if (document.Form1.txtEmaillAdd.value == "") {
                alert("Enter Email Address")
                document.Form1.txtEmaillAdd.focus()
                return false;
            }
        }

        $(document).ready(function () {
            $('#txtEmaillAdd').focus();
        });
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
        <table style="position: absolute; left: 40%; top: 40%;" border="0">
            <tr>
                <td class="normal4" colspan="2">
                    <asp:Literal ID="litMessage" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr style="font-weight:bold;font:14px">
                <td class="normal1" align="right">Email Address
                </td>
                <td align="left">
                    <asp:TextBox ID="txtEmaillAdd" CssClass="signup" runat="server" Width="250"></asp:TextBox>
                </td>
            </tr>
            <tr id="trPassword" runat="server" style="font-weight:bold;font:14px">
                <td class="normal1" align="right">Password
                </td>
                <td align="left">
                    <asp:TextBox ID="txtPassword" CssClass="signup" TextMode="Password" Width="250" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr runat="server" id="trDomain" visible="false" style="font-weight:bold;font:14px">
                <td class="normal1" align="right">Select Domain
                </td>
                <td align="left">
                    <asp:DropDownList runat="server" ID="ddlDomain">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr id="trLogin" runat="server">
                <td align="center" colspan="2">
                    <asp:Button ID="btnLogin" CssClass="button" runat="server" Text="Login" Width="80"></asp:Button>
                </td>
            </tr>
            <tr id="trForgotPwd" runat="server" visible="false">
                <td align="center" colspan="2">
                    <asp:Button ID="btnSendPwd" CssClass="button" runat="server" Text="Send Password" OnClientClick="return Forgot();"></asp:Button>
                    <asp:Button ID="btnBack" CssClass="button" runat="server" Text="Back"></asp:Button>
                </td>
            </tr>
            <tr id="trForgotPwdLink" runat="server">
                <td class="text_bold" align="center" colspan="2">
                    <asp:LinkButton ID="lnkForgot" runat="server"><font color="#52658C">Forgot 
											Password</font></asp:LinkButton>&nbsp;&nbsp;&nbsp;
                </td>
            </tr>
        </table>
        <asp:TextBox ID="txtOffset" runat="server" Style="display: none"></asp:TextBox>
        <asp:HiddenField ID="hdnWebStoreURL" runat="server" />
        <table style="vertical-align: bottom; position: absolute; left: 85%; top: 95%;">
            <tr>
                <td>
                    <i style="font-size: smaller;">Powered by</i>
                </td>
                <td>
                    <a href="http://www.bizautomation.com" target="_blank">
                        <img style="border: 0;" src="images/poweredBiz.gif"></a>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
