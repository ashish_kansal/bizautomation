<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmProjectAdd.aspx.vb" Inherits="BACRMPortal.frmProjectAdd"%>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" src="../include/calandar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1"    runat="server">		
        <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
        	<link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
		<title>Project</title>


		<script language="javascript" type="text/javascript" >
		function Save()
		{
			if ((document.frm.ddlCompany.selectedIndex==-1 )||(document.frm.ddlCompany.value==-0 ))
			{
				alert("Select Company")
				document.frm.txtCompName.focus();
				return false;
			}
			if (document.frm.ddlCusomer.value==0 )
			{
				alert("Select Contact")
				document.frm.ddlCusomer.focus();
				return false;
			}
		}
	function Focus()
					{
						document.frm.txtCompName.focus();
					}
	function Close()
		{
		    window.close()
		    return false;
		}
		</script>
	</head>
	<body onload="Focus()">
		<form id="frm" method="post" runat="server">
		<asp:ScriptManager ID="scr" EnablePartialRendering="true" runat="server"></asp:ScriptManager>
		
        <br />
			<table id="table1" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
				<tr>
					<td valign="bottom">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;New 
									Project&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td class="text_red">&nbsp;
						<asp:label id="lblOpptComments" runat="server" Visible="False" Font-Size="9px"></asp:label></td>
					<td align="right">
						<asp:Button ID="btnSave" Runat="server" Text="Save &amp; Close" CssClass="button"></asp:Button>
						<asp:button id="btnClose" Runat="server" CssClass="button" Text="Close" Width="50"></asp:button>
					</td>
				</tr>
			</table>

				<asp:table id="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" Runat="server" Width="100%" CssClass="aspTable"
				BorderColor="black" GridLines="None">
				<asp:tableRow>
					<asp:tableCell>
						<br>
						<table cellspacing="1" cellpadding="1" width="100%" border="0">
						<tr>
								<td valign="top"  rowspan="30">
                                                        <img src="../images/Compass-32.gif" />
										            </td>
					            <td class="text" align="right">Customer<FONT color="red">*</FONT></td>
								<td>
									<asp:textbox id="txtCompName" Runat="server" width="150" cssclass="signup"></asp:textbox>&nbsp;&nbsp;
									<asp:Button ID="btnGo" Runat="server" Text="Go" CssClass="button"></asp:Button>&nbsp;&nbsp;
									<asp:dropdownlist id="ddlCompany" Runat="server" Width="200" AutoPostBack="true" CssClass="signup"></asp:dropdownlist></td>
							</tr>
							<tr>
								<td class="text" align="right">Opportunity/Deal(Active)</td>
								<td>
								<asp:ListBox id="lstOpportunity" runat="server" CssClass="signup" Width="300" SelectionMode="Multiple"></asp:ListBox>
									</td>
							</tr>
							<tr>
								<td class="text" align="right">Internal Project Manager</td>
								<td><asp:dropdownlist id="ddlInternal" Runat="server" Width="200" CssClass="signup"></asp:dropdownlist></td>
							</tr>
							<tr>
								<td class="text" align="right">Customer Side Project Manager<FONT color="red">*</FONT>
								</td>
								<td><asp:dropdownlist id="ddlCusomer" Runat="server" Width="200" CssClass="signup"></asp:dropdownlist></td>
							</tr>
							<tr>
								<td class="normal1" align="right">Due Date</td>
								<td>
									<BizCalendar:Calendar ID="calDue" runat="server" />
								</td>
							</tr>
							<tr>
								<td class="normal1" align="right">Description</td>
								<td>
									<asp:TextBox ID="txtDescription" cssclass="signup" Runat="server" Width="500" TextMode=MultiLine  Height="50"></asp:TextBox></td>
							</tr>
						</table>
					    <br />
					</asp:tableCell>
				    </asp:tableRow>
				</asp:table>
		</form>
		</body>

</html>
