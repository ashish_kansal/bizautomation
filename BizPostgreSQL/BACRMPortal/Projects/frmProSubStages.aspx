<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmProSubStages.aspx.vb" Inherits="BACRMPortal.frmProSubStages"%>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
       <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
		<title>Sub Stages</title>
		<script language="javascript">
			function Close()
			{
				window.close()
			}
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table width="100%" align="center" class="aspTable">
				<tr >
					<td align="right" colSpan="2">
						<asp:Button ID="btnCancel" Runat="server" CssClass="button" Text="Close"></asp:Button>
						<br>
					</td>
				</tr>
				<tr>
				</tr>
				<tr >
					<td class=normal1 align=right >Choose Sub Stage&nbsp;
					</td>
					<td><asp:dropdownlist id="ddlSubStage" AutoPostBack="true" Width="150" CssClass="signup" Runat="server"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td colspan="2"><asp:table ID="tblSubStage" Runat="server" Width="100%"></asp:table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
