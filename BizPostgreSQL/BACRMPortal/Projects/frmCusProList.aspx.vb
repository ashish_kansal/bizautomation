'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Common
Imports System.Configuration

Partial Public Class frmCusProList : Inherits BACRMPage

    Dim strColumn As String
    

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim objCommon As New CCommon
            m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmProjectList.aspx", Session("UserContactID"), 15, 10)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../Common/frmAuthorization.aspx")
            If Not IsPostBack Then
                Session("Asc") = 1
                txtCurrrentPage.Text = 1
                BindDatagrid()
            End If
            If txtSortChar.Text <> "" Then
                ViewState("SortChar") = txtSortChar.Text
                ViewState("Column") = "Name"
                Session("Asc") = 0
                BindDatagrid()
                txtSortChar.Text = ""
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindDatagrid()
        Try
            Dim dtProjects As DataTable
            Dim objProjects As New Project
            Dim SortChar As Char
            If ViewState("SortChar") <> "" Then
                SortChar = ViewState("SortChar")
            Else : SortChar = "0"
            End If
            With objProjects
                .DomainID = Session("DomainID")
                .SortCharacter = SortChar
                .DivisionID = Session("DivId")
                .UserRightType = m_aryRightsForPage(RIGHTSTYPE.VIEW)
                .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                If txtCurrrentPage.Text.Trim <> "" Then
                    .CurrentPage = txtCurrrentPage.Text
                Else : .CurrentPage = 1
                End If
                .PageSize = Session("PagingRows")
                .TotalRecords = 0
                If ViewState("Column") <> "" Then
                    .columnName = ViewState("Column")
                Else : .columnName = "pro.bintcreateddate"
                End If
                If Session("Asc") = 1 Then
                    .columnSortOrder = "Desc"
                Else : .columnSortOrder = "Asc"
                End If
            End With
            'dtProjects = objProjects.GetProjectListPortal
            If objProjects.TotalRecords = 0 Then
                hidenav.Visible = False
                lblRecordCount.Text = 0
            Else
                hidenav.Visible = True
                lblRecordCount.Text = String.Format("{0:#,###}", objProjects.TotalRecords)
                Dim strTotalPage As String()
                Dim decTotalPage As Decimal
                decTotalPage = lblRecordCount.Text / Session("PagingRows")
                decTotalPage = Math.Round(decTotalPage, 2)
                strTotalPage = CStr(decTotalPage).Split(".")
                If (lblRecordCount.Text Mod Session("PagingRows")) = 0 Then
                    lblTotal.Text = strTotalPage(0)
                    txtTotalPage.Text = strTotalPage(0)
                Else
                    lblTotal.Text = strTotalPage(0) + 1
                    txtTotalPage.Text = strTotalPage(0) + 1
                End If
                txtTotalRecords.Text = lblRecordCount.Text
            End If
            dgProjects.DataSource = dtProjects
            dgProjects.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub lnkLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkLast.Click
        Try
            txtCurrrentPage.Text = txtTotalPage.Text
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkPrevious.Click
        Try
            If txtCurrrentPage.Text = 1 Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text - 1
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkFirst.Click
        Try
            txtCurrrentPage.Text = 1
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnkNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkPrevious.Click, lnkNext.Click
        Try
            If txtCurrrentPage.Text = txtTotalPage.Text Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 1
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk2.Click
        Try
            If txtCurrrentPage.Text + 1 = txtTotalPage.Text Or txtCurrrentPage.Text + 1 > txtTotalPage.Text Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 2
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk3.Click
        Try
            If txtCurrrentPage.Text + 2 = txtTotalPage.Text Or txtCurrrentPage.Text + 2 > txtTotalPage.Text Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 3
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk4.Click
        Try
            If txtCurrrentPage.Text + 3 = txtTotalPage.Text Or txtCurrrentPage.Text + 3 > txtTotalPage.Text Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 4
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub lnk5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk5.Click
        Try
            If txtCurrrentPage.Text + 4 = txtTotalPage.Text Or txtCurrrentPage.Text + 4 > txtTotalPage.Text Then
                Exit Sub
            Else : txtCurrrentPage.Text = txtCurrrentPage.Text + 5
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub txtCurrrentPage_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCurrrentPage.TextChanged
        Try
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgProjects_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgProjects.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As Button
                Dim lnkDelete As LinkButton
                lnkDelete = e.Item.FindControl("lnkDelete")
                btnDelete = e.Item.FindControl("btnDelete")
                If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                    btnDelete.Visible = False
                    lnkDelete.Visible = True
                    lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                Else : btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgProjects_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgProjects.SortCommand
        Try
            strColumn = e.SortExpression.ToString()
            If ViewState("Column") <> strColumn Then
                ViewState("Column") = strColumn
                Session("Asc") = 0
            Else
                If Session("Asc") = 0 Then
                    Session("Asc") = 1
                Else : Session("Asc") = 0
                End If
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgProjects_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgProjects.ItemCommand
        Dim lngProID, lngOppID As Long
        Try
            If Not e.CommandName = "Sort" Then
                lngProID = e.Item.Cells(0).Text
                lngOppID = CLng(e.Item.Cells(1).Text)
            End If
            If e.CommandName = "Name" Then
                Session("ProID") = lngProID
                If Session("EnableIntMedPage") = 1 Then
                    Response.Redirect("../pagelayout/frmCustProjectdtl.aspx?frm=ProjectList", False)
                Else : Response.Redirect("../projects/frmCusProDTL.aspx?frm=ProjectList", False)
                End If
            ElseIf e.CommandName = "Contact" Then
                Dim objCommon As New CCommon
                objCommon.ProID = lngProID
                objCommon.charModule = "P"
                objCommon.GetCompanySpecificValues1()
                Session("ContactId") = objCommon.ContactID
                If Session("EnableIntMedPage") = 1 Then
                    Response.Redirect("../pagelayout/frmCustContactdtl.aspx?frm=ProjectList", False)
                Else : Response.Redirect("../Contacts/frmCstContacts.aspx?frm=ProjectList", False)
                End If
            ElseIf e.CommandName = "Opportunity" Then
                Session("OppID") = lngOppID
                If Session("EnableIntMedPage") = 1 Then
                    Response.Redirect("../pagelayout/frmCustOppurtunitydtl.aspx?frm=ProjectList", False)
                Else : Response.Redirect("../Opportunities/frmCusOpportunities.aspx?frm=ProjectList", False)
                End If
            ElseIf e.CommandName = "Delete" Then
                Dim ObjProjects As New Project
                ObjProjects.ProjectId = lngProID
                ObjProjects.DomainID = Session("DomainID")
                If ObjProjects.DeleteProjects = False Then
                    litMessage.Text = "Dependent Records Exists.Cannot be deleted."
                Else
                    litMessage.Text = ""
                    BindDatagrid()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class
