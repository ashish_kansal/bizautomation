Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Common
Public Class frmProDependency
    Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents btnCancel As System.Web.UI.WebControls.Button
    Protected WithEvents dgDependency As System.Web.UI.WebControls.DataGrid
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
        Try
            If Not IsPostBack Then
                Getdetails()
                btnCancel.Attributes.Add("onclick", "return Close();")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub Getdetails()
        Try
            Dim objDependency As New ProjectDependency
            objDependency.ProID = GetQueryStringVal( "Proid")
            objDependency.ProStageId = GetQueryStringVal( "ProStageID")
            dgDependency.DataSource = objDependency.GetDependencyDtls
            dgDependency.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class

