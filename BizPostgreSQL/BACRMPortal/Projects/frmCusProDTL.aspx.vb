'***************************************************************************************************************************
'     Author Name				 :  Anoop Jayaraj
'     Date Written				 :  20/3/2005
'***************************************************************************************************************************
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Contract
Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Admin
Imports Infragistics.WebUI.UltraWebTab
Imports System.Reflection
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Outlook

Partial Public Class frmCusProDTL : Inherits BACRMPage

    'Dim SI As Integer = 0
    'Dim SI1 As Integer = 0
    'Dim SI2 As Integer = 0
    'Dim frm As String = ""
    'Dim frm1 As String = ""
    'Dim frm2 As String = ""
    'Dim dtAssignto, dtET, dtActivity, dtActType, dtEvent, dtTime, dtRemider, dtStartDate As DataTable
    'Dim myRow As DataRow
    'Dim arrOutPut() As String = New String(2) {}
    'Dim lngProID, lngDivId As Long
    '
    'Dim objContacts As CContacts
    'Dim objOpportunity As MOpportunity
    'Dim objProject As Project
    'Dim objCommon As New CCommon
    'Dim ObjCus As CustomFields
    'Dim dtStageStatus As DataTable
    'Dim objOutLook As COutlook
    'Dim objActionItem As ActionItem

    'Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '    Try
    '        If Not GetQueryStringVal( "SI") Is Nothing Then
    '            SI = GetQueryStringVal( "SI")
    '        End If
    '        If Not GetQueryStringVal( "SI1") Is Nothing Then
    '            SI1 = GetQueryStringVal( "SI1")
    '        Else : SI1 = 0
    '        End If
    '        If Not GetQueryStringVal( "SI2") Is Nothing Then
    '            SI2 = GetQueryStringVal( "SI2")
    '        Else : SI2 = 0
    '        End If
    '        If Not GetQueryStringVal( "frm") Is Nothing Then
    '            frm = ""
    '            frm = GetQueryStringVal( "frm")
    '        Else : frm = ""
    '        End If
    '        If Not GetQueryStringVal( "frm1") Is Nothing Then
    '            frm1 = ""
    '            frm1 = GetQueryStringVal( "frm1")
    '        Else : frm1 = ""
    '        End If
    '        If Not GetQueryStringVal( "SI2") Is Nothing Then
    '            frm2 = ""
    '            frm2 = GetQueryStringVal( "frm2")
    '        Else : frm2 = ""
    '        End If
    '        lngProID = Session("ProID")
    '        btnAddContact.Attributes.Add("onclick", "return AddContact()")
    '        m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmProjects.aspx", Session("UserContactID"), 15, 11)
    '        If Not IsPostBack Then

    '            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../Common/frmAuthorization.aspx")
    '            If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
    '                btnTSave.Visible = False
    '                btnTSaveAndCancel.Visible = False
    '            End If
    '            LoadSalesProcess()

    '            If GetQueryStringVal( "SelectedIndex") <> "" Then
    '                uwOppTab.SelectedTabIndex = GetQueryStringVal( "SelectedIndex")
    '            End If
    '            objCommon.sb_FillComboFromDBwithSel(ddlContactRole, 26, Session("DomainID"))
    '            LoadSavedInformation()
    '            FillContact()
    '            tblMenu.Visible = True

    '            If ViewState("MileCheck") <> 1 Then trSalesProcess.Visible = False

    '            btnTSave.Attributes.Add("onclick", "return Save(2)")
    '            btnTSaveAndCancel.Attributes.Add("onclick", "return Save(2)")
    '        End If
    '        If Not Session("ProProcessDetails") Is Nothing Then CreatMilestone()
    '        DisplayDynamicFlds()
    '        If Not IsPostBack Then
    '            If uwOppTab.Tabs.Count > SI Then uwOppTab.SelectedTabIndex = SI
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Sub LoadSavedInformation()
    '    Try
    '        Dim dtDetails As DataTable
    '        If objProject Is Nothing Then objProject = New Project
    '        objProject.ProjectId = lngProID
    '        objProject.DomainID = Session("DomainID")
    '        objProject.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
    '        dtDetails = objProject.ProjectDetail(Of DataTable)()
    '        hplCustomer.Text = dtDetails.Rows(0).Item("vcCompanyName")

    '        If Session("EnableIntMedPage") = 1 Then
    '            hplCustomer.NavigateUrl = "../pagelayout/frmCustAccountdtl.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=ProjectDetails&DivID=" & Session("DivID") & "&frm1=" & GetQueryStringVal( "frm")
    '        Else : hplCustomer.NavigateUrl = "../account/frmCusAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=ProjectDetails&DivID=" & Session("DivID") & "&frm1=" & GetQueryStringVal( "frm")
    '        End If
    '        txtDivId.Text = dtDetails.Rows(0).Item("numDivisionID")
    '        lngDivId = dtDetails.Rows(0).Item("numDivisionID")
    '        txtProName.Text = dtDetails.Rows(0).Item("vcprojectName")

    '        ViewState("ContractID") = dtDetails.Rows(0).Item("numcontractId")
    '        txtName.Text = dtDetails.Rows(0).Item("vcProjectName")
    '        lblLastModifiedBy.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("vcModifiedby")), "", dtDetails.Rows(0).Item("vcModifiedby"))
    '        lblCreatedBy.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("vcCreatedBy")), "", dtDetails.Rows(0).Item("vcCreatedBy"))
    '        lblRecordOwner.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("vcRecOwner")), "", dtDetails.Rows(0).Item("vcRecOwner"))
    '        txtOComments.Text = dtDetails.Rows(0).Item("txtComments")
    '        LoadContacts(dtDetails.Rows(0).Item("numDivisionId"))

    '        'loading the Opportunities Dropdown
    '        Dim dtOpportunity As DataTable
    '        Dim dtSelOpportunity As DataTable
    '        objProject.ProjectId = lngProID
    '        objProject.DomainID = Session("DomainId")
    '        objProject.DivisionID = dtDetails.Rows(0).Item("numDivisionId")
    '        objProject.bytemode = 1
    '        dtOpportunity = objProject.GetOpportunities

    '        lstOpportunity.DataSource = dtOpportunity
    '        lstOpportunity.DataTextField = "vcPOppName"
    '        lstOpportunity.DataValueField = "numOppId"
    '        lstOpportunity.DataBind()

    '        dtSelOpportunity = objProject.GetProOpportunities

    '        Dim i As Integer = 0
    '        For i = 0 To dtSelOpportunity.Rows.Count - 1
    '            If Not lstOpportunity.Items.FindByValue(dtSelOpportunity.Rows(i).Item("numOppid")) Is Nothing Then
    '                lstOpportunity.Items.FindByValue(dtSelOpportunity.Rows(i).Item("numOppid")).Selected = True
    '            End If
    '        Next

    '        If Not IsDBNull(dtDetails.Rows(0).Item("intDueDate")) Then
    '            calDue.SelectedDate = dtDetails.Rows(0).Item("intDueDate")
    '        End If

    '        If Not IsDBNull(dtDetails.Rows(0).Item("numCustPrjMgr")) Then
    '            If Not ddlCustPrjMgr.Items.FindByValue(dtDetails.Rows(0).Item("numCustPrjMgr")) Is Nothing Then
    '                ddlCustPrjMgr.Items.FindByValue(dtDetails.Rows(0).Item("numCustPrjMgr")).Selected = True
    '            End If
    '        End If

    '        If Not IsDBNull(dtDetails.Rows(0).Item("numintPrjMgr")) Then
    '            If Not ddlIntPrgMgr.Items.FindByValue(dtDetails.Rows(0).Item("numintPrjMgr")) Is Nothing Then
    '                ddlIntPrgMgr.Items.FindByValue(dtDetails.Rows(0).Item("numintPrjMgr")).Selected = True
    '            End If
    '        End If

    '        objCommon.sb_FillConEmpFromDBSel(ddlAssignedTo, Session("DomainID"), 1, dtDetails.Rows(0).Item("numAssignedTo"))
    '        If Not IsDBNull(dtDetails.Rows(0).Item("numAssignedTo")) Then
    '            If Not ddlAssignedTo.Items.FindByValue(dtDetails.Rows(0).Item("numAssignedTo")) Is Nothing Then
    '                ddlAssignedTo.Items.FindByValue(dtDetails.Rows(0).Item("numAssignedTo")).Selected = True
    '            End If
    '        End If

    '        dgContact.DataSource = objProject.AssCntsByProId
    '        dgContact.DataBind()

    '        Dim dtMilestone As DataTable
    '        objProject.DomainID = Session("DomainId")
    '        objProject.ContactID = Session("UserContactID")
    '        dtMilestone = objProject.SalesProcessDtlByProId
    '        Session("ProProcessDetails") = dtMilestone
    '        If dtMilestone.Rows.Count = 0 Then ViewState("MileCheck") = 1
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Sub LoadContacts(ByVal intDivision As Integer)
    '    Try
    '        Dim fillCombo As New COpportunities
    '        With fillCombo
    '            .DivisionID = intDivision
    '            ddlCustPrjMgr.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
    '            ddlCustPrjMgr.DataTextField = "Name"
    '            ddlCustPrjMgr.DataValueField = "numcontactId"
    '            ddlCustPrjMgr.DataBind()
    '        End With
    '        ddlCustPrjMgr.Items.Insert(0, New ListItem("---Select One---", "0"))

    '        objCommon.DivisionID = intDivision
    '        objCommon.DomainID = Session("DomainId")
    '        objCommon.ContactType = 92
    '        ddlIntPrgMgr.DataSource = objCommon.AssignTo
    '        ddlIntPrgMgr.DataTextField = "vcName"
    '        ddlIntPrgMgr.DataValueField = "ContactID"
    '        ddlIntPrgMgr.DataBind()
    '        ddlIntPrgMgr.Items.Insert(0, "--Select One--")
    '        ddlIntPrgMgr.Items.FindByText("--Select One--").Value = 0
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    ' ''On selectindex changed of Items in Milestones and stages
    'Private Sub ddlProcessList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProcessList.SelectedIndexChanged
    '    Try
    '        Dim dtProjectProcess As DataTable
    '        If objProject Is Nothing Then objProject = New Project
    '        objProject.ProjectProcessId = CInt(ddlProcessList.SelectedItem.Value)
    '        objProject.ContactID = Session("UserContactID")
    '        objProject.DomainID = Session("DomainId")
    '        dtProjectProcess = objProject.SalesProcessDtlByProcessId
    '        If dtProjectProcess.Rows.Count > 0 Then
    '            '  dtProjectProcess.Rows(dtProjectProcess.Rows.Count - 1).Delete()
    '            ' dtProjectProcess.AcceptChanges()
    '            dtProjectProcess.Rows(dtProjectProcess.Rows.Count - 1).Item("vcStageDetail") = "Project Completed "
    '        End If
    '        Session("ProProcessDetails") = dtProjectProcess
    '        ViewState("MileCheck") = 1
    '        If ddlProcessList.SelectedIndex > 0 Then ViewState("ReloadMilestone") = 1
    '        txtProcessId.Text = ddlProcessList.SelectedItem.Value
    '        CreatMilestone()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Sub LoadMileStoneDropDowns()
    '    Try
    '        If objCommon Is Nothing Then objCommon = New CCommon

    '        objCommon.intOption = 19
    '        dtEvent = objCommon.GetDefaultValues

    '        objCommon.intOption = 18
    '        dtRemider = objCommon.GetDefaultValues

    '        objCommon.intOption = 17
    '        dtTime = objCommon.GetDefaultValues

    '        objCommon.intOption = 16
    '        dtStartDate = objCommon.GetDefaultValues

    '        dtActivity = objCommon.GetMasterListItems(73, Session("DomainId"))
    '        dtActType = objCommon.GetMasterListItems(73, Session("DomainId"))
    '        Dim objCampaign As New Campaign
    '        objCampaign.DomainID = Session("DomainID")
    '        objCampaign.UserCntID = Session("UserContactID")
    '        dtET = objCampaign.GetUserEmailTemplates()
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Sub CreatMilestone()
    '    Try
    '        Dim dtProjectProcess As DataTable
    '        dtProjectProcess = Session("ProProcessDetails")
    '        tblMilestone.Rows.Clear()
    '        If dtProjectProcess.Rows.Count <> 0 Then
    '            LoadStageStatus()
    '            LoadAssignTo()
    '            LoadMileStoneDropDowns()
    '            Dim i As Integer
    '            Dim tblCell As TableCell
    '            Dim tblrow As TableRow
    '            Dim chkDlost As CheckBox
    '            Dim chkDClosed As CheckBox
    '            Dim btnAdd As New Button
    '            Dim btnDelete As New Button
    '            Dim InitialPercentage As Integer = dtProjectProcess.Rows(0).Item(0)
    '            If dtProjectProcess.Rows(0).Item(0) <> 100 And dtProjectProcess.Rows(0).Item(0) <> 0 Then
    '                If dtProjectProcess.Rows.Count <> 2 Then
    '                    If Not dtProjectProcess.Rows(0).Item("Op_Flag") = 1 Then
    '                        tblrow = New TableRow
    '                        tblCell = New TableCell
    '                        tblCell.Text = "<font color=white>&nbsp;&nbsp;&nbsp;&nbsp;   Milestone - " & InitialPercentage & "%&nbsp;&nbsp;" & dtProjectProcess.Rows(0).Item("vcStagePercentageDtl") & "</font>"
    '                        tblCell.Height = Unit.Pixel(20)
    '                        tblCell.CssClass = "text_bold"
    '                        tblCell.ColumnSpan = 8
    '                        tblCell.BackColor = System.Drawing.Color.FromName("#52658C")
    '                        tblrow.Controls.Add(tblCell)
    '                        tblMilestone.Controls.Add(tblrow)
    '                        txtTemplateId.Text = dtProjectProcess.Rows(0).Item("numTemplateId")

    '                        tblrow = New TableRow
    '                        tblCell = New TableCell
    '                        tblCell.ColumnSpan = 8
    '                        btnAdd = New Button
    '                        btnDelete = New Button
    '                        btnAdd.ID = "btnAdd~" & InitialPercentage
    '                        btnDelete.ID = "btnDelete~" & InitialPercentage
    '                        btnAdd.Text = "Add"
    '                        btnDelete.Text = "Delete"
    '                        btnAdd.Width = Unit.Pixel(50)
    '                        btnDelete.Width = Unit.Pixel(50)
    '                        btnAdd.CssClass = "button"
    '                        btnDelete.CssClass = "button"
    '                        btnDelete.Attributes.Add("onclick", "return DeletMsg()")
    '                        AddHandler btnAdd.Click, AddressOf btnAddClick
    '                        AddHandler btnDelete.Click, AddressOf btnDeleteClick
    '                        tblCell.HorizontalAlign = HorizontalAlign.Right
    '                        Dim lblSB As New Label
    '                        lblSB.ID = "lblSB~" & InitialPercentage
    '                        lblSB.Text = "&nbsp;"
    '                        tblCell.Controls.Add(btnAdd)
    '                        tblCell.Controls.Add(lblSB)
    '                        tblCell.Controls.Add(btnDelete)
    '                        tblrow.Controls.Add(tblCell)
    '                        tblMilestone.Controls.Add(tblrow)
    '                        ' End If
    '                    End If
    '                End If
    '            End If
    '            ViewState("CheckColor") = 0
    '            For i = 0 To dtProjectProcess.Rows.Count - 1
    '                If dtProjectProcess.Rows(i).Item(0) <> 100 And dtProjectProcess.Rows(i).Item(0) <> 0 Then
    '                    If InitialPercentage = dtProjectProcess.Rows(i).Item(0) Then
    '                        If dtProjectProcess.Rows(i).Item("Op_Flag") <> 1 Then
    '                            createStages(dtProjectProcess.Rows(i))
    '                            InitialPercentage = dtProjectProcess.Rows(i).Item(0)
    '                        End If
    '                    Else
    '                        If dtProjectProcess.Rows(i).Item("Op_Flag") <> 1 Then
    '                            InitialPercentage = dtProjectProcess.Rows(i).Item(0)
    '                            tblrow = New TableRow
    '                            tblCell = New TableCell
    '                            tblCell.Text = "<font color=white>&nbsp;&nbsp;&nbsp;&nbsp;   Milestone - " & InitialPercentage & "%&nbsp;&nbsp;" & dtProjectProcess.Rows(i).Item("vcStagePercentageDtl") & "</font>"
    '                            tblCell.Height = Unit.Pixel(20)
    '                            tblCell.CssClass = "text_bold"
    '                            tblCell.ColumnSpan = 8
    '                            tblCell.BackColor = System.Drawing.Color.FromName("#52658C")
    '                            tblrow.Controls.Add(tblCell)
    '                            tblMilestone.Controls.Add(tblrow)
    '                            txtTemplateId.Text = dtProjectProcess.Rows(0).Item("numTemplateId")

    '                            '  If Trim(GetQueryStringVal("opId")) = "" Or Viewstate("MileCheck") = 1 Then  
    '                            tblrow = New TableRow
    '                            tblCell = New TableCell
    '                            tblCell.ColumnSpan = 8
    '                            btnAdd = New Button
    '                            btnDelete = New Button
    '                            btnAdd.ID = "btnAdd~" & InitialPercentage
    '                            btnDelete.ID = "btnDelete~" & InitialPercentage
    '                            btnAdd.Text = "Add"
    '                            btnDelete.Text = "Delete"
    '                            btnAdd.Width = Unit.Pixel(50)
    '                            btnDelete.Width = Unit.Pixel(50)
    '                            btnAdd.CssClass = "button"
    '                            btnDelete.CssClass = "button"
    '                            btnDelete.Attributes.Add("onclick", "return DeletMsg()")
    '                            AddHandler btnAdd.Click, AddressOf btnAddClick
    '                            AddHandler btnDelete.Click, AddressOf btnDeleteClick
    '                            tblCell.HorizontalAlign = HorizontalAlign.Right
    '                            Dim lblSBs As New Label
    '                            lblSBs.ID = "lblSBs" & InitialPercentage
    '                            lblSBs.Text = "&nbsp;"
    '                            tblCell.Controls.Add(btnAdd)
    '                            tblCell.Controls.Add(lblSBs)
    '                            tblCell.Controls.Add(btnDelete)
    '                            tblrow.Controls.Add(tblCell)
    '                            tblMilestone.Controls.Add(tblrow)
    '                            'End If
    '                            ViewState("CheckColor") = 0
    '                            createStages(dtProjectProcess.Rows(i))
    '                        End If
    '                    End If
    '                End If
    '            Next

    '            ''Project Conclusion
    '            tblrow = New TableRow
    '            Dim txtDComments As TextBox
    '            Dim lblComm As Label
    '            tblCell = New TableCell
    '            tblCell.Text = "<font color=white>&nbsp;&nbsp;&nbsp;&nbsp; Conclusion</font>"
    '            tblCell.Height = Unit.Pixel(20)
    '            tblCell.CssClass = "text_bold"
    '            tblCell.ColumnSpan = 8
    '            tblCell.BackColor = System.Drawing.Color.FromName("#52658C")
    '            tblrow.Controls.Add(tblCell)
    '            tblMilestone.Controls.Add(tblrow)

    '            '' Project Completion Details
    '            tblrow = New TableRow
    '            tblCell = New TableCell
    '            tblCell.CssClass = "normal1"
    '            tblCell.ColumnSpan = 8
    '            chkDClosed = New CheckBox
    '            chkDClosed.ID = "chkDClosed"
    '            chkDClosed.Text = "Project Completed &nbsp;"
    '            If (dtProjectProcess.Rows(dtProjectProcess.Rows.Count - 1).Item("bitStageCompleted")) = True Then
    '                chkDClosed.Checked = True
    '            Else : chkDClosed.Checked = False
    '            End If
    '            lblComm = New Label
    '            lblComm.Text = "Comments &nbsp; "
    '            txtDComments = New TextBox
    '            txtDComments.CssClass = "signup"
    '            txtDComments.ID = "txtDCComm"
    '            txtDComments.Text = dtProjectProcess.Rows(dtProjectProcess.Rows.Count - 1).Item("vcComments")
    '            txtDComments.Width = Unit.Pixel(600)
    '            tblCell.Controls.Add(chkDClosed)
    '            tblCell.CssClass = "normal1"
    '            tblCell.Controls.Add(lblComm)
    '            tblCell.Controls.Add(txtDComments)
    '            tblrow.Controls.Add(tblCell)
    '            tblMilestone.Controls.Add(tblrow)
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Sub createStages(ByVal dr As DataRow)
    '    Try
    '        Dim tblCell As TableCell
    '        Dim tblrow As TableRow
    '        Dim lbl As Label
    '        Dim chkStage As CheckBox
    '        Dim txtStage As TextBox
    '        Dim lblStageCM As Label
    '        Dim lblInform As Label
    '        Dim txtComm As TextBox
    '        Dim ddlStatus As DropDownList
    '        Dim ddlAgginTo As DropDownList
    '        Dim chkAlert As CheckBox
    '        Dim txtChecKStage As TextBox
    '        Dim hpkLink As HyperLink
    '        Dim txtDueDate As TextBox
    '        Dim ddlEvent As DropDownList
    '        Dim ddlReminder As DropDownList
    '        Dim ddlEmailTemp As DropDownList
    '        Dim ddlActivity As DropDownList
    '        Dim ddlStartDate As DropDownList
    '        Dim ddlStartTime As DropDownList
    '        Dim ddlStartTimePeriod As DropDownList
    '        Dim ddlEndTime As DropDownList
    '        Dim ddlEndTimePeriod As DropDownList
    '        Dim txtBody As TextBox

    '        '''First row
    '        tblrow = New TableRow
    '        If ViewState("CheckColor") = 1 Then tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
    '        tblCell = New TableCell
    '        tblCell.CssClass = "normal1"
    '        tblCell.Text = "Stage Name"
    '        tblCell.HorizontalAlign = HorizontalAlign.Right
    '        tblrow.Controls.Add(tblCell)

    '        tblCell = New TableCell
    '        txtStage = New TextBox
    '        txtStage.ID = "txtStage~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
    '        txtStage.Text = dr.Item("vcstageDetail")
    '        txtStage.CssClass = "signup"
    '        txtStage.Width = Unit.Pixel(250)
    '        tblCell.ColumnSpan = 3
    '        tblCell.CssClass = "normal1"
    '        tblCell.Controls.Add(txtStage)
    '        tblrow.Controls.Add(tblCell)

    '        tblCell = New TableCell
    '        tblCell.CssClass = "normal1"
    '        tblCell.HorizontalAlign = HorizontalAlign.Right
    '        lblStageCM = New Label
    '        lblStageCM.Text = "&nbsp;&nbsp;Stage Status&nbsp;&nbsp;"
    '        lblStageCM.ID = "lblStatus" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
    '        tblCell.Controls.Add(lblStageCM)
    '        tblrow.Controls.Add(tblCell)

    '        tblCell = New TableCell
    '        tblCell.CssClass = "normal1"
    '        ddlStatus = New DropDownList
    '        ddlStatus.DataSource = dtStageStatus
    '        ddlStatus.DataTextField = "vcData"
    '        ddlStatus.DataValueField = "numListItemID"
    '        ddlStatus.DataBind()
    '        ddlStatus.Items.Insert(0, "--Select One--")
    '        ddlStatus.Items.FindByText("--Select One--").Value = 0

    '        If Not IsDBNull(dr.Item("numStage")) Then
    '            If Not ddlStatus.Items.FindByValue(dr.Item("numStage")) Is Nothing Then
    '                ddlStatus.Items.FindByValue(dr.Item("numStage")).Selected = True
    '            End If
    '        End If
    '        ddlStatus.CssClass = "signup"
    '        ddlStatus.ID = "ddlStatus~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
    '        tblCell.Controls.Add(ddlStatus)
    '        tblrow.Controls.Add(tblCell)

    '        tblCell = New TableCell
    '        lblStageCM = New Label
    '        lblStageCM.Text = "Last Modified By:  "
    '        lblStageCM.ID = "lblStageModifiedBy" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
    '        lblInform = New Label
    '        If Not IsDBNull(dr.Item("numModifiedBy")) Then
    '            lblInform.Text = IIf(dr.Item("numModifiedBy") = 0, "", dr.Item("numModifiedByName"))
    '        End If
    '        lblInform.ID = "lblInformBy" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
    '        tblCell.CssClass = "normal1"
    '        tblCell.Controls.Add(lblStageCM)
    '        tblCell.Controls.Add(lblInform)
    '        tblrow.Controls.Add(tblCell)

    '        tblCell = New TableCell
    '        lblStageCM = New Label
    '        lblStageCM.Text = "Last Modified Date:  "
    '        lblStageCM.ID = "lblStageModified" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
    '        lblInform = New Label
    '        lblInform.Text = FormattedDateFromDate(DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), dr.Item("bintModifiedDate")), Session("DateFormat"))
    '        lblInform.ID = "lblInform" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
    '        tblCell.CssClass = "normal1"
    '        tblCell.Controls.Add(lblStageCM)
    '        tblCell.Controls.Add(lblInform)
    '        tblrow.Controls.Add(tblCell)
    '        tblMilestone.Controls.Add(tblrow)

    '        'Second Row
    '        tblrow = New TableRow
    '        If ViewState("CheckColor") = 1 Then tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")

    '        tblCell = New TableCell
    '        tblCell.CssClass = "normal1"
    '        tblCell.HorizontalAlign = HorizontalAlign.Right
    '        lbl = New Label
    '        lbl.Text = "Comments &nbsp;"
    '        tblCell.Controls.Add(lbl)
    '        tblrow.Controls.Add(tblCell)

    '        tblCell = New TableCell
    '        tblCell.CssClass = "normal1"
    '        tblCell.ColumnSpan = 5
    '        lbl = New Label
    '        txtComm = New TextBox
    '        txtComm.ID = "txtComm~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
    '        txtComm.Width = Unit.Pixel(750)
    '        txtComm.CssClass = "signup"
    '        txtComm.Text = dr.Item("vcComments")

    '        tblCell.Controls.Add(txtComm)
    '        tblCell.CssClass = "normal1"
    '        tblrow.Controls.Add(tblCell)

    '        tblCell = New TableCell
    '        tblCell.CssClass = "normal1"
    '        lbl = New Label
    '        lbl.Text = "This stage comprises "
    '        tblCell.Controls.Add(lbl)
    '        tblCell.ColumnSpan = 2
    '        Dim txtStagePer As New TextBox
    '        txtStagePer.ID = "txtStagePer~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
    '        txtStagePer.Width = Unit.Pixel(30)
    '        txtStagePer.CssClass = "signup"
    '        txtStagePer.Text = dr.Item("tintPercentage")
    '        tblCell.Controls.Add(txtStagePer)
    '        lbl = New Label
    '        lbl.Text = "  % of completion"
    '        tblCell.Controls.Add(lbl)
    '        tblrow.Controls.Add(tblCell)
    '        tblMilestone.Controls.Add(tblrow)

    '        ''Third Row
    '        If ViewState("MileCheck") = 0 Then
    '            tblrow = New TableRow
    '            If ViewState("CheckColor") = 1 Then tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
    '            tblCell = New TableCell
    '            tblCell.ColumnSpan = 2
    '            tblCell.HorizontalAlign = HorizontalAlign.Center
    '            tblCell.CssClass = "normal1"
    '            hpkLink = New HyperLink
    '            hpkLink.ID = "hpkLinkTime" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")

    '            hpkLink.Attributes.Add("onclick", "return OpenTime(" & dr.Item("ProStageID") & "," & lngProID & "," & dr.Item("numStagePercentage") & "," & dr.Item("numstagedetailsID") & "," & txtDivId.Text & ");")

    '            hpkLink.Text = "<font class='hyperlink'>Time</font>"
    '            hpkLink.CssClass = "hyperlink"
    '            lbl = New Label
    '            lbl.ID = "lblTime" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
    '            If Not IsDBNull(dr.Item("Time")) Then
    '                lbl.Text = "<font color=red>&nbsp;" & dr.Item("Time") & "</font>"
    '            End If

    '            tblCell.Controls.Add(hpkLink)
    '            tblCell.Controls.Add(lbl)
    '            tblrow.Controls.Add(tblCell)

    '            tblCell = New TableCell
    '            tblCell.ColumnSpan = 2
    '            tblCell.HorizontalAlign = HorizontalAlign.Center
    '            hpkLink = New HyperLink
    '            hpkLink.ID = "hpkLinkExpense" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
    '            'If Not dr.Item("numOppStageID") Is Nothing Then
    '            hpkLink.Attributes.Add("onclick", "return OpenExpense(" & dr.Item("ProStageID") & "," & lngProID & "," & dr.Item("numStagePercentage") & "," & dr.Item("numstagedetailsID") & "," & txtDivId.Text & ");")

    '            hpkLink.Text = "<font class='hyperlink'>Expense</font>"
    '            hpkLink.CssClass = "hyperlink"
    '            tblCell.CssClass = "normal1"
    '            lbl = New Label
    '            lbl.ID = "lblExp" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
    '            If Not IsDBNull(dr.Item("Expense")) Then
    '                lbl.Text = "<font color=red>&nbsp;" & String.Format("{0:#,##0.00}", CDec(dr.Item("Expense"))) & "</font>"
    '            End If
    '            tblCell.Controls.Add(hpkLink)
    '            tblCell.Controls.Add(lbl)
    '            tblrow.Controls.Add(tblCell)

    '            tblCell = New TableCell
    '            tblCell.ColumnSpan = 2
    '            tblCell.HorizontalAlign = HorizontalAlign.Center
    '            tblCell.CssClass = "normal1"
    '            hpkLink = New HyperLink
    '            hpkLink.ID = "hpkLinkDependency" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")

    '            hpkLink.Attributes.Add("onclick", "return OpenDependency(" & dr.Item("ProStageID") & "," & lngProID & "," & dr.Item("numStagePercentage") & "," & dr.Item("numstagedetailsID") & ");")
    '            'End If
    '            hpkLink.Text = "<font class='hyperlink'>Dependency</font>"
    '            hpkLink.CssClass = "hyperlink"
    '            lbl = New Label
    '            lbl.ID = "lblDep" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
    '            If Not IsDBNull(dr.Item("Depend")) Then
    '                If dr.Item("Depend") = "1" Then
    '                    lbl.Text = "<font color='red' face='Wingdings 2'>&nbsp;" & 8 & "</font>"
    '                End If
    '            End If
    '            tblCell.Controls.Add(hpkLink)
    '            tblCell.Controls.Add(lbl)
    '            tblrow.Controls.Add(tblCell)

    '            tblCell = New TableCell
    '            tblCell.ColumnSpan = 2
    '            tblCell.HorizontalAlign = HorizontalAlign.Center
    '            tblCell.CssClass = "normal1"
    '            lbl = New Label

    '            hpkLink = New HyperLink
    '            hpkLink.ID = "hpkLinkSubStage" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")

    '            hpkLink.Text = "<font  class='hyperlink'>Sub Stages</font>"
    '            hpkLink.CssClass = "hyperlink"

    '            hpkLink.Attributes.Add("onclick", "return OpenSubStage(" & dr.Item("ProStageID") & "," & lngProID & "," & dr.Item("numStagePercentage") & "," & dr.Item("numstagedetailsID") & ");")
    '            'End If
    '            lbl.ID = "lblStg" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
    '            If Not IsDBNull(dr.Item("SubStg")) Then
    '                If dr.Item("SubStg") = "1" Then
    '                    lbl.Text = "<font color='red' face='Wingdings 2'>&nbsp;" & 8 & "</font>"
    '                End If
    '            End If

    '            tblCell.Controls.Add(hpkLink)
    '            tblCell.Controls.Add(lbl)
    '            tblrow.Controls.Add(tblCell)
    '            tblMilestone.Controls.Add(tblrow)
    '        End If

    '        ''''Fourth Row
    '        tblrow = New TableRow
    '        If ViewState("CheckColor") = 1 Then
    '            tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
    '        End If
    '        tblCell = New TableCell
    '        tblCell.HorizontalAlign = HorizontalAlign.Right
    '        tblCell.CssClass = "normal1"
    '        lbl = New Label
    '        lbl.Text = "&nbsp;Assign To"
    '        tblCell.Controls.Add(lbl)
    '        tblrow.Controls.Add(tblCell)

    '        tblCell = New TableCell
    '        tblCell.CssClass = "normal1"
    '        ddlAgginTo = New DropDownList
    '        ddlAgginTo.Width = Unit.Pixel(150)
    '        ddlAgginTo.ID = "ddlAgginTo~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
    '        ddlAgginTo.CssClass = "signup"
    '        ddlAgginTo.DataSource = dtAssignto
    '        ddlAgginTo.DataTextField = "vcName"
    '        ddlAgginTo.DataValueField = "ContactID"
    '        ddlAgginTo.DataBind()
    '        ddlAgginTo.Items.Insert(0, "--Select One--")
    '        ddlAgginTo.Items.FindByText("--Select One--").Value = 0
    '        If Not IsDBNull(dr.Item("numAssignTo")) Then
    '            If Not ddlAgginTo.Items.FindByValue(dr.Item("numAssignTo")) Is Nothing Then
    '                ddlAgginTo.Items.FindByValue(dr.Item("numAssignTo")).Selected = True
    '            End If
    '        End If
    '        tblCell.Controls.Add(ddlAgginTo)
    '        tblrow.Controls.Add(tblCell)

    '        tblCell = New TableCell
    '        tblCell.CssClass = "normal1"
    '        lbl = New Label
    '        lbl.Text = "Alert"
    '        tblCell.HorizontalAlign = HorizontalAlign.Right
    '        tblCell.Controls.Add(lbl)
    '        tblrow.Controls.Add(tblCell)

    '        tblCell = New TableCell
    '        tblCell.CssClass = "normal1"
    '        chkAlert = New CheckBox
    '        chkAlert.ID = "chkAlert~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
    '        If dr.Item("bitAlert") = 1 Then
    '            chkAlert.Checked = True
    '        Else : chkAlert.Checked = False
    '        End If

    '        tblCell.Controls.Add(chkAlert)
    '        tblrow.Cells.Add(tblCell)

    '        tblCell = New TableCell
    '        tblCell.CssClass = "normal1"
    '        tblCell.HorizontalAlign = HorizontalAlign.Right
    '        lbl = New Label
    '        lbl.Text = "Due Date &nbsp;"
    '        tblCell.Controls.Add(lbl)
    '        tblrow.Cells.Add(tblCell)

    '        tblCell = New TableCell
    '        Dim bizCalendar As UserControl = LoadControl("../include/calandar.ascx")
    '        bizCalendar.ID = "cal" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
    '        tblCell.Controls.Add(bizCalendar)
    '        tblrow.Cells.Add(tblCell)
    '        tblCell = New TableCell
    '        tblCell.CssClass = "normal1"
    '        lbl = New Label
    '        lbl.Text = "Stage Completed : "
    '        tblCell.Controls.Add(lbl)
    '        lbl = New Label
    '        If Not IsDBNull(dr.Item("bintStageComDate")) Then
    '            If dr.Item("bitStageCompleted") = True Then
    '                lbl.Text = FormattedDateFromDate(DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), dr.Item("bintStageComDate")), Session("DateFormat"))
    '            End If
    '        End If
    '        tblCell.Controls.Add(lbl)
    '        tblrow.Controls.Add(tblCell)

    '        tblCell = New TableCell
    '        tblCell.CssClass = "normal1"
    '        lbl = New Label
    '        chkStage = New CheckBox
    '        txtChecKStage = New TextBox
    '        txtChecKStage.ID = "txtChecKStage~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
    '        txtChecKStage.Attributes.Add("style", "display:none")
    '        If dr.Item("bitStageCompleted") = False Then
    '            txtChecKStage.Text = 0
    '            lbl.Text = "Stage Closed"
    '        Else
    '            txtChecKStage.Text = 1
    '            lbl.Text = "<font color='#CC0000'><b>Stage Closed</b></font>"
    '            chkStage.Checked = True
    '        End If

    '        chkStage.ID = "chkStage~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
    '        tblCell.Controls.Add(lbl)
    '        tblCell.Controls.Add(txtChecKStage)
    '        tblCell.Controls.Add(chkStage)
    '        tblrow.Controls.Add(tblCell)
    '        tblMilestone.Controls.Add(tblrow)

    '        ''Fifth Row
    '        tblrow = New TableRow
    '        If ViewState("CheckColor") = 1 Then tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
    '        tblCell = New TableCell
    '        tblCell.CssClass = "normal1"
    '        tblCell.HorizontalAlign = HorizontalAlign.Right
    '        lbl = New Label
    '        lbl.Text = "&nbsp;Event "
    '        tblCell.Controls.Add(lbl)
    '        tblrow.Controls.Add(tblCell)

    '        tblCell = New TableCell
    '        ddlEvent = New DropDownList
    '        ddlEvent.Enabled = False
    '        ddlEvent.ID = "ddlEvent~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
    '        ddlEvent.CssClass = "signup"
    '        ddlEvent.DataSource = dtEvent
    '        ddlEvent.DataTextField = "vcOptionName"
    '        ddlEvent.DataValueField = "vcSQLValue"
    '        ddlEvent.DataBind()

    '        If Not IsDBNull(dr.Item("numEvent")) Then
    '            If Not ddlEvent.Items.FindByValue(dr.Item("numEvent")) Is Nothing Then
    '                ddlEvent.Items.FindByValue(dr.Item("numEvent")).Selected = True
    '            End If
    '        End If
    '        tblCell.Controls.Add(ddlEvent)
    '        tblrow.Controls.Add(tblCell)

    '        tblCell = New TableCell
    '        tblCell.CssClass = "normal1"
    '        tblCell.HorizontalAlign = HorizontalAlign.Right
    '        lbl = New Label
    '        lbl.Text = "&nbsp;Reminder "
    '        tblCell.Controls.Add(lbl)
    '        tblrow.Controls.Add(tblCell)

    '        tblCell = New TableCell
    '        ddlReminder = New DropDownList
    '        ddlReminder.ID = "ddlReminder~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
    '        ddlReminder.CssClass = "signup"
    '        ddlReminder.DataSource = dtRemider
    '        ddlReminder.DataTextField = "vcOptionName"
    '        ddlReminder.DataValueField = "vcSQLValue"
    '        ddlReminder.DataBind()
    '        If Not IsDBNull(dr.Item("numReminder")) Then
    '            If Not ddlReminder.Items.FindByValue(dr.Item("numReminder")) Is Nothing Then
    '                ddlReminder.Items.FindByValue(dr.Item("numReminder")).Selected = True
    '            End If
    '        End If
    '        tblCell.Controls.Add(ddlReminder)
    '        tblrow.Controls.Add(tblCell)

    '        tblCell = New TableCell
    '        tblCell.CssClass = "normal1"
    '        tblCell.HorizontalAlign = HorizontalAlign.Right
    '        lbl = New Label
    '        lbl.Text = "&nbsp;Email Template "
    '        tblCell.Controls.Add(lbl)
    '        tblrow.Controls.Add(tblCell)

    '        tblCell = New TableCell
    '        ddlEmailTemp = New DropDownList
    '        ddlEmailTemp.ID = "ddlEmailTemp~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
    '        ddlEmailTemp.CssClass = "signup"
    '        ddlEmailTemp.DataSource = dtET
    '        ddlEmailTemp.DataTextField = "VcDocName"
    '        ddlEmailTemp.DataValueField = "numGenericDocID"
    '        ddlEmailTemp.DataBind()
    '        ddlEmailTemp.Items.Insert(0, "--Select One--")
    '        ddlEmailTemp.Items.FindByText("--Select One--").Value = "0"
    '        If Not IsDBNull(dr.Item("numET")) Then
    '            If Not ddlEmailTemp.Items.FindByValue(dr.Item("numET")) Is Nothing Then
    '                ddlEmailTemp.Items.FindByValue(dr.Item("numET")).Selected = True
    '            End If
    '        End If
    '        tblCell.Controls.Add(ddlEmailTemp)
    '        tblrow.Controls.Add(tblCell)

    '        tblCell = New TableCell
    '        tblCell.ColumnSpan = 2
    '        tblCell.CssClass = "normal1"
    '        lbl = New Label
    '        lbl.Text = "&nbsp;Activity"
    '        ddlActivity = New DropDownList
    '        ddlActivity.ID = "ddlActivity~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
    '        ddlActivity.CssClass = "signup"
    '        ddlActivity.DataSource = dtActivity
    '        ddlActivity.DataTextField = "vcData"
    '        ddlActivity.DataValueField = "numListItemID"
    '        ddlActivity.DataBind()
    '        ddlActivity.Items.Insert(0, "--Select One--")
    '        ddlActivity.Items.FindByText("--Select One--").Value = "0"
    '        If Not IsDBNull(dr.Item("numActivity")) Then
    '            If Not ddlActivity.Items.FindByValue(dr.Item("numActivity")) Is Nothing Then
    '                ddlActivity.Items.FindByValue(dr.Item("numActivity")).Selected = True
    '            End If
    '        End If
    '        tblCell.Controls.Add(lbl)
    '        tblCell.Controls.Add(ddlActivity)
    '        tblrow.Controls.Add(tblCell)
    '        tblMilestone.Controls.Add(tblrow)

    '        ''Sixth Row
    '        tblrow = New TableRow
    '        If ViewState("CheckColor") = 1 Then
    '            tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
    '        End If
    '        tblCell = New TableCell
    '        tblCell.CssClass = "normal1"
    '        tblCell.HorizontalAlign = HorizontalAlign.Right
    '        lbl = New Label
    '        lbl.Text = "&nbsp;Start Date "
    '        tblCell.Controls.Add(lbl)
    '        tblrow.Controls.Add(tblCell)

    '        tblCell = New TableCell
    '        ddlStartDate = New DropDownList
    '        ddlStartDate.Width = Unit.Pixel(200)
    '        ddlStartDate.ID = "ddlStartDate~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
    '        ddlStartDate.CssClass = "signup"
    '        ddlStartDate.DataSource = dtStartDate
    '        ddlStartDate.DataTextField = "vcOptionName"
    '        ddlStartDate.DataValueField = "vcSQLValue"
    '        ddlStartDate.DataBind()
    '        If Not IsDBNull(dr.Item("numStartDate")) Then
    '            If Not ddlStartDate.Items.FindByValue(dr.Item("numStartDate")) Is Nothing Then
    '                ddlStartDate.Items.FindByValue(dr.Item("numStartDate")).Selected = True
    '            End If
    '        End If
    '        tblCell.Controls.Add(ddlStartDate)
    '        tblrow.Controls.Add(tblCell)

    '        tblCell = New TableCell
    '        tblCell.CssClass = "normal1"
    '        tblCell.HorizontalAlign = HorizontalAlign.Right
    '        lbl = New Label
    '        lbl.Text = "&nbsp;Start Time "
    '        tblCell.Controls.Add(lbl)
    '        tblrow.Cells.Add(tblCell)

    '        tblCell = New TableCell
    '        ddlStartTime = New DropDownList
    '        ddlStartTime.ID = "ddlStartTime~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
    '        ddlStartTime.CssClass = "signup"
    '        ddlStartTime.DataSource = dtTime
    '        ddlStartTime.DataTextField = "vcOptionName"
    '        ddlStartTime.DataValueField = "vcSQLValue"
    '        ddlStartTime.DataBind()
    '        If Not IsDBNull(dr.Item("numStartTime")) Then
    '            If Not ddlStartTime.Items.FindByValue(dr.Item("numStartTime")) Is Nothing Then
    '                ddlStartTime.Items.FindByValue(dr.Item("numStartTime")).Selected = True
    '            End If
    '        End If

    '        ddlStartTimePeriod = New DropDownList
    '        ddlStartTimePeriod.ID = "ddlStartTimePeriod~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
    '        ddlStartTimePeriod.CssClass = "signup"
    '        ddlStartTimePeriod.Items.Add(New ListItem("AM", "0"))
    '        ddlStartTimePeriod.Items.Add(New ListItem("PM", "1"))
    '        If Not IsDBNull(dr.Item("numStartTimePeriod")) Then
    '            If Not ddlStartTimePeriod.Items.FindByValue(dr.Item("numStartTimePeriod")) Is Nothing Then
    '                ddlStartTimePeriod.Items.FindByValue(dr.Item("numStartTimePeriod")).Selected = True
    '            End If
    '        End If

    '        tblCell.Controls.Add(ddlStartTime)

    '        lbl = New Label
    '        lbl.Text = "&nbsp;"
    '        tblCell.Controls.Add(lbl)

    '        tblCell.Controls.Add(ddlStartTimePeriod)
    '        tblrow.Controls.Add(tblCell)

    '        tblCell = New TableCell
    '        tblCell.CssClass = "normal1"
    '        tblCell.HorizontalAlign = HorizontalAlign.Right
    '        lbl = New Label
    '        lbl.Text = "&nbsp;End Time "
    '        tblCell.Controls.Add(lbl)
    '        tblrow.Controls.Add(tblCell)

    '        tblCell = New TableCell
    '        ddlEndTime = New DropDownList
    '        ddlEndTime.ID = "ddlEndTime~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
    '        ddlEndTime.CssClass = "signup"
    '        ddlEndTime.DataSource = dtTime
    '        ddlEndTime.DataTextField = "vcOptionName"
    '        ddlEndTime.DataValueField = "vcSQLValue"
    '        ddlEndTime.DataBind()
    '        If Not IsDBNull(dr.Item("numEndTime")) Then
    '            If Not ddlEndTime.Items.FindByValue(dr.Item("numEndTime")) Is Nothing Then
    '                ddlEndTime.Items.FindByValue(dr.Item("numEndTime")).Selected = True
    '            End If
    '        End If
    '        ddlEndTimePeriod = New DropDownList
    '        ddlEndTimePeriod.ID = "ddlEndTimePeriod~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
    '        ddlEndTimePeriod.CssClass = "signup"
    '        ddlEndTimePeriod.Items.Add(New ListItem("AM", "0"))
    '        ddlEndTimePeriod.Items.Add(New ListItem("PM", "1"))
    '        If Not IsDBNull(dr.Item("numEndTimePeriod")) Then
    '            If Not ddlEndTimePeriod.Items.FindByValue(dr.Item("numEndTimePeriod")) Is Nothing Then
    '                ddlEndTimePeriod.Items.FindByValue(dr.Item("numEndTimePeriod")).Selected = True
    '            End If
    '        End If

    '        tblCell.Controls.Add(ddlEndTime)
    '        lbl = New Label
    '        lbl.Text = "&nbsp;"
    '        tblCell.Controls.Add(lbl)

    '        tblCell.Controls.Add(ddlEndTimePeriod)
    '        tblrow.Controls.Add(tblCell)

    '        tblCell = New TableCell
    '        tblCell.ColumnSpan = 2
    '        tblCell.CssClass = "normal1"
    '        lbl = New Label
    '        lbl.Text = "&nbsp;Action Item Type"
    '        Dim ddlActType As New DropDownList
    '        ddlActType.ID = "ddlActType~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
    '        ddlActType.CssClass = "signup"
    '        ddlActType.DataSource = dtActType
    '        ddlActType.DataTextField = "vcData"
    '        ddlActType.DataValueField = "numListItemID"
    '        ddlActType.DataBind()
    '        ddlActType.Items.Insert(0, "--Select One--")
    '        ddlActType.Items.FindByText("--Select One--").Value = "0"
    '        If Not IsDBNull(dr.Item("numType")) Then
    '            If Not ddlActType.Items.FindByValue(dr.Item("numType")) Is Nothing Then
    '                ddlActType.Items.FindByValue(dr.Item("numType")).Selected = True
    '            End If
    '        End If
    '        tblCell.Controls.Add(lbl)
    '        tblCell.Controls.Add(ddlActType)
    '        tblrow.Controls.Add(tblCell)
    '        tblMilestone.Rows.Add(tblrow)

    '        ''Seventh Row
    '        tblrow = New TableRow
    '        If ViewState("CheckColor") = 1 Then
    '            tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
    '        End If
    '        tblCell = New TableCell
    '        tblCell.HorizontalAlign = HorizontalAlign.Right
    '        tblCell.CssClass = "normal1"
    '        lbl = New Label
    '        lbl.Text = "&nbsp;Comments 2 "
    '        tblCell.Controls.Add(lbl)
    '        tblrow.Controls.Add(tblCell)

    '        tblCell = New TableCell
    '        tblCell.ColumnSpan = 7
    '        txtBody = New TextBox
    '        txtBody.CssClass = "signup"
    '        txtBody.Width = Unit.Pixel(750)
    '        txtBody.ID = "txtBody~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")

    '        If Not IsDBNull(dr.Item("txtCom")) Then txtBody.Text = dr.Item("txtCom")
    '        tblCell.Controls.Add(txtBody)
    '        tblrow.Controls.Add(tblCell)
    '        tblMilestone.Rows.Add(tblrow)

    '        ''Eigth Row
    '        tblrow = New TableRow
    '        If ViewState("CheckColor") = 1 Then
    '            tblrow.BackColor = System.Drawing.Color.FromName("#C6D3E7")
    '        End If
    '        tblCell = New TableCell
    '        tblCell.HorizontalAlign = HorizontalAlign.Right
    '        tblCell.CssClass = "normal1"
    '        Dim chkChgStatus As New CheckBox
    '        chkChgStatus.CssClass = "signup"
    '        chkChgStatus.ID = "chkChgStatus~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
    '        chkChgStatus.Checked = dr.Item("bitChgStatus")

    '        tblCell.Controls.Add(chkChgStatus)
    '        tblrow.Controls.Add(tblCell)

    '        tblCell = New TableCell
    '        tblCell.ColumnSpan = 7
    '        lbl = New Label
    '        lbl.CssClass = "normal1"
    '        lbl.Text = "When done change stage status to &nbsp;&nbsp;"

    '        Dim ddlChgStatus As New DropDownList
    '        ddlChgStatus.CssClass = "signup"
    '        ddlChgStatus.ID = "ddlChgStatus~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
    '        ddlChgStatus.DataSource = dtStageStatus
    '        ddlChgStatus.DataTextField = "vcData"
    '        ddlChgStatus.DataValueField = "numListItemID"
    '        ddlChgStatus.DataBind()
    '        ddlChgStatus.Items.Insert(0, "--Select One--")
    '        ddlChgStatus.Items.FindByText("--Select One--").Value = 0

    '        If Not IsDBNull(dr.Item("numChgStatus")) Then
    '            If Not ddlChgStatus.Items.FindByValue(dr.Item("numChgStatus")) Is Nothing Then
    '                ddlChgStatus.Items.FindByValue(dr.Item("numChgStatus")).Selected = True
    '            End If
    '        End If
    '        tblCell.Controls.Add(lbl)
    '        tblCell.Controls.Add(ddlChgStatus)


    '        Dim chkClose As New CheckBox
    '        chkClose.Text = "When done close stage"
    '        chkClose.CssClass = "signup"
    '        chkClose.ID = "chkClose~" & dr.Item("numStagePercentage") & "~" & dr.Item("numstagedetailsID")
    '        chkClose.Checked = dr.Item("bitClose")

    '        tblCell.Controls.Add(chkClose)
    '        tblrow.Controls.Add(tblCell)
    '        tblMilestone.Rows.Add(tblrow)

    '        If Not IsDBNull(dr.Item("bintDueDate")) Then
    '            ' bizCalendar. = FormattedDateFromDate(dr.Item("bintDueDate"), Session("DateFormat"))
    '            Dim _myControlType As Type = bizCalendar.GetType()
    '            Dim _myUC_DueDate As PropertyInfo = _myControlType.GetProperty("SelectedDate")
    '            _myUC_DueDate.SetValue(bizCalendar, CStr(dr.Item("bintDueDate")), Nothing)
    '        End If

    '        If ViewState("CheckColor") = 1 Then
    '            ViewState("CheckColor") = 0
    '        Else : ViewState("CheckColor") = 1
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Sub LoadSalesProcess()
    '    Try
    '        Dim dtProjectProcess As DataTable
    '        If objOpportunity Is Nothing Then objOpportunity = New MOpportunity
    '        objOpportunity.ProType = 1
    '        objOpportunity.DomainID = Session("DomainID")
    '        dtProjectProcess = objOpportunity.BusinessProcess
    '        BindSaleProcess(dtProjectProcess)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Sub BindSaleProcess(ByVal dtTable As DataTable)
    '    Try
    '        ddlProcessList.DataSource = dtTable
    '        ddlProcessList.DataTextField = "slp_name"
    '        ddlProcessList.DataValueField = "slp_id"
    '        ddlProcessList.DataBind()
    '        ddlProcessList.Items.Insert(0, "--Select One--")
    '        ddlProcessList.Items.FindByText("--Select One--").Value = 0
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Sub LoadAssignTo()
    '    Try
    '        objCommon.DomainID = Session("DomainId")
    '        objCommon.ContactType = 92
    '        dtAssignto = objCommon.AssignTo
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Sub LoadStageStatus()
    '    Try
    '        dtStageStatus = objCommon.GetMasterListItems(42, Session("DomainID"))
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub dgContact_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgContact.ItemDataBound
    '    Try
    '        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
    '            Dim btnDelete As Button
    '            Dim lnkDelete As LinkButton
    '            lnkDelete = e.Item.FindControl("lnkDeleteCnt")
    '            btnDelete = e.Item.FindControl("btnDeleteCnt")
    '            If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
    '                btnDelete.Visible = False
    '                lnkDelete.Visible = True
    '                lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
    '            Else : btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
    '            End If
    '            If e.Item.Cells(1).Text = 1 Then CType(e.Item.FindControl("lblShare"), Label).Text = "a"
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub dgContact_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgContact.ItemCommand
    '    Try
    '        If e.CommandName = "Delete" Then
    '            If objProject Is Nothing Then objProject = New Project
    '            objProject.bytemode = 1
    '            objProject.ProjectId = lngProID
    '            objProject.DomainID = Session("DomainID")
    '            objProject.ContactID = e.Item.Cells(0).Text
    '            dgContact.DataSource = objProject.AddProContacts
    '            dgContact.DataBind()
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Sub btnAddClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If objProject Is Nothing Then objProject = New Project
    '        Dim ctrlId As String = sender.id
    '        Dim strSplit As String()
    '        strSplit = ctrlId.Split("~")
    '        Dim dtSalesDetails As DataTable
    '        dtSalesDetails = Session("ProProcessDetails")

    '        myRow = dtSalesDetails.NewRow
    '        myRow("numStagePercentage") = strSplit(1)
    '        myRow("numstagedetailsID") = CType(dtSalesDetails.Compute("MAX(numstagedetailsID)", ""), Integer) + 1
    '        myRow("vcstageDetail") = ""
    '        myRow("ProStageID") = 0
    '        myRow("numDomainId") = Session("DomainId")
    '        myRow("numCreatedBy") = Session("UserContactID")
    '        myRow("bintCreatedDate") = Date.UtcNow
    '        myRow("numModifiedBy") = Session("UserContactID")
    '        myRow("bintModifiedDate") = Date.UtcNow
    '        myRow("bintDueDate") = Date.UtcNow
    '        myRow("bintStageComDate") = System.DBNull.Value
    '        myRow("vcComments") = ""
    '        myRow("numAssignTo") = 0
    '        myRow("bitAlert") = 0
    '        myRow("bitStageCompleted") = 0
    '        myRow("Op_Flag") = 0
    '        myRow("numModifiedByName") = ""
    '        myRow("numStage") = 0
    '        dtSalesDetails.Rows.Add(myRow)

    '        Dim ds As New DataSet
    '        dtSalesDetails.TableName = "Table"
    '        ds.Tables.Add(dtSalesDetails.Copy)
    '        objProject.strMilestone = ds.GetXml
    '        ds.Tables.Remove(ds.Tables(0))
    '        objProject.ProjectId = lngProID

    '        If dtSalesDetails.Rows.Count = 0 Then
    '            objProject.bytemode = 0
    '        Else : objProject.bytemode = 1
    '        End If
    '        If ViewState("byteMode") = 1 Then
    '            objProject.bytemode = 0
    '            ViewState("byteMode") = Nothing
    '        End If
    '        objProject.UserCntID = Session("UserContactID")
    '        'objProject.SaveMilestone()
    '        dtSalesDetails = objProject.SalesProcessDtlByProId
    '        Session("ProProcessDetails") = dtSalesDetails
    '        CreatMilestone()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Sub btnDeleteClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim ctrlId As String = sender.id
    '        Dim strSplit As String()
    '        Dim i As Integer
    '        strSplit = ctrlId.Split("~")
    '        Dim dtSalesDetails As DataTable
    '        dtSalesDetails = Session("ProProcessDetails")
    '        For i = 0 To dtSalesDetails.Rows.Count - 1
    '            If strSplit(1) = dtSalesDetails.Rows(i).Item("numStagePercentage") And dtSalesDetails.Rows(i).Item("Op_Flag") = 0 Then
    '                Dim chk As CheckBox
    '                chk = uwOppTab.FindControl("chkStage~" & strSplit(1) & "~" & dtSalesDetails.Rows(i).Item("numstagedetailsID"))
    '                If chk.Checked = False Then
    '                Else : dtSalesDetails.Rows(i).Item("Op_Flag") = 1
    '                End If
    '            End If
    '        Next
    '        Session("ProProcessDetails") = dtSalesDetails
    '        CreatMilestone()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Function SaveProjects() As Integer
    '    Try
    '        If objProject Is Nothing Then objProject = New Project
    '        CreateTableMilestone()
    '        objProject.ProjectId = lngProID
    '        objProject.ProComments = txtOComments.Text
    '        If calDue.SelectedDate <> "" Then objProject.DueDate = calDue.SelectedDate
    '        objProject.ProjectName = txtName.Text
    '        objProject.OpportunityId = 0
    '        objProject.IntPrjMgr = ddlIntPrgMgr.SelectedItem.Value
    '        objProject.CusPrjMgr = ddlCustPrjMgr.SelectedItem.Value
    '        objProject.UserCntID = Session("UserContactID")
    '        objProject.AssignedTo = ddlAssignedTo.SelectedValue
    '        objProject.DomainID = Session("DomainId")
    '        objProject.ContractID = ViewState("ContractID")
    '        Dim dsNew As New DataSet
    '        If Not Session("ProProcessDetails") Is Nothing Then
    '            Dim dtTable As DataTable
    '            dtTable = Session("ProProcessDetails")
    '            If dtTable.Rows.Count > 0 Then
    '                dtTable.TableName = "Table"
    '                dsNew.Tables.Add(dtTable.Copy)
    '                objProject.strMilestone = dsNew.GetXml
    '                dsNew.Tables.Remove(dsNew.Tables(0))
    '            End If
    '        End If

    '        arrOutPut = objProject.Save()
    '        If arrOutPut(0) = 0 Then
    '            Return 1
    '            Exit Function
    '        End If
    '        objProject.ProjectId = arrOutPut(0)

    '        Dim i As Integer = 0
    '        Dim strOppSelected As String = ""
    '        For i = 0 To lstOpportunity.Items.Count - 1
    '            If lstOpportunity.Items(i).Selected = True Then
    '                strOppSelected = strOppSelected & lstOpportunity.Items(i).Value.ToString & ","
    '            End If
    '        Next

    '        objProject.strOppSel = strOppSelected
    '        objProject.SaveProjectOpportunities()

    '        If ViewState("MileCheck") = 1 Then
    '            objProject.bytemode = 0
    '        Else : objProject.bytemode = 1
    '        End If
    '        'objProject.SaveMilestone()
    '        Return 0
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    'Sub CreateTableMilestone()
    '    Try
    '        Dim dtProjectProcess As DataTable
    '        Dim i, intModified As Integer
    '        If Not Session("ProProcessDetails") Is Nothing Then
    '            dtProjectProcess = Session("ProProcessDetails")
    '            If dtProjectProcess.Rows.Count <> 0 Then
    '                For i = 0 To dtProjectProcess.Rows.Count - 1
    '                    If dtProjectProcess.Rows(i).Item("numStagePercentage") <> 100 Then
    '                        If dtProjectProcess.Rows(i).Item("numStagePercentage") <> 0 Then
    '                            If Not dtProjectProcess.Rows(i).Item("Op_Flag") = 1 Then
    '                                Dim txtchkStage As TextBox
    '                                Dim chkStage As CheckBox
    '                                Dim ddlStatus As DropDownList
    '                                ddlStatus = uwOppTab.FindControl("ddlStatus~" & dtProjectProcess.Rows(i).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i).Item("numstagedetailsID"))
    '                                txtchkStage = uwOppTab.FindControl("txtChecKStage~" & dtProjectProcess.Rows(i).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i).Item("numstagedetailsID"))
    '                                Dim txtStageDetails As TextBox
    '                                txtStageDetails = uwOppTab.FindControl("txtStage~" & dtProjectProcess.Rows(i).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i).Item("numstagedetailsID"))
    '                                If dtProjectProcess.Rows(i).Item("vcstageDetail") <> txtStageDetails.Text Then
    '                                    intModified = 1
    '                                End If
    '                                If dtProjectProcess.Rows(i).Item("numStage") <> ddlStatus.SelectedItem.Value Then
    '                                    intModified = 1
    '                                End If
    '                                dtProjectProcess.Rows(i).Item("numStage") = ddlStatus.SelectedItem.Value
    '                                dtProjectProcess.Rows(i).Item("vcstageDetail") = txtStageDetails.Text
    '                                chkStage = uwOppTab.FindControl("chkStage~" & dtProjectProcess.Rows(i).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i).Item("numstagedetailsID"))
    '                                dtProjectProcess.Rows(i).Item("numEvent") = CType(uwOppTab.FindControl("ddlEvent~" & dtProjectProcess.Rows(i).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i).Item("numstagedetailsID")), DropDownList).SelectedValue
    '                                dtProjectProcess.Rows(i).Item("numReminder") = CType(uwOppTab.FindControl("ddlReminder~" & dtProjectProcess.Rows(i).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i).Item("numstagedetailsID")), DropDownList).SelectedValue
    '                                dtProjectProcess.Rows(i).Item("numET") = CType(uwOppTab.FindControl("ddlEmailTemp~" & dtProjectProcess.Rows(i).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i).Item("numstagedetailsID")), DropDownList).SelectedValue
    '                                dtProjectProcess.Rows(i).Item("numActivity") = CType(uwOppTab.FindControl("ddlActivity~" & dtProjectProcess.Rows(i).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i).Item("numstagedetailsID")), DropDownList).SelectedValue
    '                                dtProjectProcess.Rows(i).Item("numStartDate") = CType(uwOppTab.FindControl("ddlStartDate~" & dtProjectProcess.Rows(i).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i).Item("numstagedetailsID")), DropDownList).SelectedValue
    '                                dtProjectProcess.Rows(i).Item("numStartTime") = CType(uwOppTab.FindControl("ddlStartTime~" & dtProjectProcess.Rows(i).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i).Item("numstagedetailsID")), DropDownList).SelectedValue
    '                                dtProjectProcess.Rows(i).Item("numStartTimePeriod") = CType(uwOppTab.FindControl("ddlStartTimePeriod~" & dtProjectProcess.Rows(i).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i).Item("numstagedetailsID")), DropDownList).SelectedValue
    '                                dtProjectProcess.Rows(i).Item("numEndTime") = CType(uwOppTab.FindControl("ddlEndTime~" & dtProjectProcess.Rows(i).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i).Item("numstagedetailsID")), DropDownList).SelectedValue
    '                                dtProjectProcess.Rows(i).Item("numEndTimePeriod") = CType(uwOppTab.FindControl("ddlEndTimePeriod~" & dtProjectProcess.Rows(i).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i).Item("numstagedetailsID")), DropDownList).SelectedValue
    '                                dtProjectProcess.Rows(i).Item("txtCom") = CType(uwOppTab.FindControl("txtBody~" & dtProjectProcess.Rows(i).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i).Item("numstagedetailsID")), TextBox).Text
    '                                dtProjectProcess.Rows(i).Item("numChgStatus") = CType(uwOppTab.FindControl("ddlChgStatus~" & dtProjectProcess.Rows(i).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i).Item("numstagedetailsID")), DropDownList).SelectedValue
    '                                dtProjectProcess.Rows(i).Item("bitChgStatus") = CType(uwOppTab.FindControl("chkChgStatus~" & dtProjectProcess.Rows(i).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i).Item("numstagedetailsID")), CheckBox).Checked
    '                                dtProjectProcess.Rows(i).Item("bitClose") = CType(uwOppTab.FindControl("ChkClose~" & dtProjectProcess.Rows(i).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i).Item("numstagedetailsID")), CheckBox).Checked
    '                                dtProjectProcess.Rows(i).Item("numType") = CType(uwOppTab.FindControl("ddlActType~" & dtProjectProcess.Rows(i).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i).Item("numstagedetailsID")), DropDownList).SelectedValue

    '                                If txtchkStage.Text = 0 And chkStage.Checked = True Then
    '                                    dtProjectProcess.Rows(i).Item("bitStageCompleted") = True
    '                                    dtProjectProcess.Rows(i).Item("bintStageComDate") = Date.UtcNow

    '                                    ' Sending Mail to Internal prjoject's Manager and external project manager on Completeing a Stage
    '                                    Try
    '                                        Dim dtDetails As DataTable
    '                                        Dim objAlerts As New CAlerts

    '                                        objAlerts.AlertID = 9
    '                                        dtDetails = objAlerts.GetAlertDetails
    '                                        Dim strTo As String = ""
    '                                        If dtDetails.Rows(4).Item("tintAlertOn") = 1 Then
    '                                            objCommon.ContactID = ddlIntPrgMgr.SelectedItem.Value
    '                                            If ddlIntPrgMgr.SelectedItem.Value > 0 Then
    '                                                strTo = objCommon.GetContactsEmail
    '                                            End If
    '                                        End If
    '                                        Dim dtEmailTemplate As DataTable
    '                                        Dim objDocuments As New DocumentList
    '                                        objDocuments.GenDocID = dtDetails.Rows(4).Item("numEmailTemplate")
    '                                        objDocuments.DomainID = Session("DomainID")
    '                                        dtEmailTemplate = objDocuments.GetDocByGenDocID
    '                                        If dtEmailTemplate.Rows.Count > 0 Then
    '                                            Dim objSendMail As New Email
    '                                            Dim dtMergeFields As New DataTable
    '                                            Dim drNew As DataRow
    '                                            dtMergeFields.Columns.Add("OppID")
    '                                            dtMergeFields.Columns.Add("Organization")
    '                                            dtMergeFields.Columns.Add("Stage")
    '                                            drNew = dtMergeFields.NewRow
    '                                            drNew("OppID") = txtName.Text
    '                                            drNew("Organization") = hplCustomer.Text
    '                                            drNew("Stage") = txtStageDetails.Text

    '                                            If strTo <> "" Then
    '                                                If dtDetails.Rows(5).Item("tintAlertOn") = 1 Then
    '                                                    objCommon.ContactID = ddlCustPrjMgr.SelectedItem.Value
    '                                                    If ddlCustPrjMgr.SelectedItem.Value > 0 Then
    '                                                        strTo = strTo & "," & objCommon.GetContactsEmail
    '                                                    Else : strTo = objCommon.GetContactsEmail
    '                                                    End If
    '                                                End If
    '                                            Else
    '                                                objCommon.ContactID = ddlCustPrjMgr.SelectedItem.Value
    '                                                strTo = objCommon.GetContactsEmail
    '                                            End If

    '                                            Dim dtEmail As DataTable
    '                                            objAlerts.AlertDTLID = 12
    '                                            objAlerts.DomainID = Session("DomainId")
    '                                            dtEmail = objAlerts.GetAlertEmails
    '                                            objCommon.byteMode = 0
    '                                            objCommon.ContactID = ddlIntPrgMgr.SelectedItem.Value
    '                                            Dim strCC As String = ""
    '                                            Dim p As Integer
    '                                            For p = 0 To dtEmail.Rows.Count - 1
    '                                                strCC = strCC & dtEmail.Rows(p).Item("vcEmailID") & ","
    '                                            Next
    '                                            strCC = strCC.TrimEnd(",")
    '                                            If dtDetails.Rows(2).Item("tintCCManager") = 1 Then
    '                                                Dim strCCAssignee As String = objCommon.GetManagerEmail
    '                                                If strCCAssignee <> "" Then
    '                                                    strCC = strCC & "," & strCCAssignee
    '                                                End If
    '                                            End If
    '                                            dtMergeFields.Rows.Add(drNew)
    '                                            objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), strCC, Session("UserEmail"), strTo, dtMergeFields)
    '                                        End If
    '                                    Catch ex As Exception

    '                                    End Try
    '                                ElseIf txtchkStage.Text = 1 And chkStage.Checked = False Then
    '                                    dtProjectProcess.Rows(i).Item("bitStageCompleted") = False
    '                                    dtProjectProcess.Rows(i).Item("bintStageComDate") = System.DBNull.Value
    '                                End If
    '                                Dim txtStageComments As TextBox
    '                                txtStageComments = uwOppTab.FindControl("txtComm~" & dtProjectProcess.Rows(i).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i).Item("numstagedetailsID"))
    '                                If dtProjectProcess.Rows(i).Item("vcComments") <> txtStageComments.Text Then
    '                                    intModified = 1
    '                                End If
    '                                dtProjectProcess.Rows(i).Item("vcComments") = txtStageComments.Text
    '                                Dim chkAlert As CheckBox
    '                                chkAlert = uwOppTab.FindControl("chkAlert~" & dtProjectProcess.Rows(i).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i).Item("numstagedetailsID"))
    '                                If chkAlert.Checked = True Then
    '                                    dtProjectProcess.Rows(i).Item("bitAlert") = 1
    '                                End If
    '                                Dim BizCalendar As UserControl
    '                                BizCalendar = uwOppTab.FindControl("cal" & dtProjectProcess.Rows(i).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i).Item("numstagedetailsID"))

    '                                Dim strDueDate As String
    '                                Dim _myControlType As Type = BizCalendar.GetType()
    '                                Dim _myUC_DueDate As PropertyInfo = _myControlType.GetProperty("SelectedDate")
    '                                strDueDate = _myUC_DueDate.GetValue(BizCalendar, Nothing)

    '                                If Not strDueDate = "" Then
    '                                    dtProjectProcess.Rows(i).Item("bintDueDate") = strDueDate
    '                                    If dtProjectProcess.Rows(i).Item("bintDueDate") <> strDueDate Then
    '                                        intModified = 1
    '                                    End If
    '                                Else
    '                                    If IsDBNull(dtProjectProcess.Rows(i).Item("bintDueDate")) Then
    '                                        intModified = 1
    '                                    End If
    '                                End If

    '                                Dim ddlAssignTo As DropDownList
    '                                ddlAssignTo = uwOppTab.FindControl("ddlAgginTo~" & dtProjectProcess.Rows(i).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i).Item("numstagedetailsID"))
    '                                If dtProjectProcess.Rows(i).Item("numAssignTo") <> ddlAssignTo.SelectedItem.Value Then
    '                                    intModified = 1
    '                                End If
    '                                dtProjectProcess.Rows(i).Item("numAssignTo") = ddlAssignTo.SelectedItem.Value
    '                                Dim txtStagePer As TextBox
    '                                txtStagePer = uwOppTab.FindControl("txtStagePer~" & dtProjectProcess.Rows(i).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i).Item("numstagedetailsID"))
    '                                dtProjectProcess.Rows(i).Item("tintPercentage") = IIf(txtStagePer.Text = "", 0, txtStagePer.Text)
    '                                'Sending Mail to the next Stage assignee
    '                                If txtchkStage.Text = 0 And chkStage.Checked = True Then
    '                                    Try
    '                                        intModified = 1
    '                                        Dim objAlerts As New CAlerts
    '                                        Dim dtDetails As DataTable
    '                                        objAlerts.AlertDTLID = 2 'Alert DTL ID for sending alerts in opportunities
    '                                        objAlerts.DomainID = Session("DomainID")
    '                                        dtDetails = objAlerts.GetIndAlertDTL
    '                                        If dtDetails.Rows.Count > 0 Then
    '                                            If dtDetails.Rows(0).Item("tintAlertOn") = 1 Then
    '                                                Dim dtEmailTemplate As DataTable
    '                                                Dim objDocuments As New DocumentList
    '                                                objDocuments.GenDocID = dtDetails.Rows(0).Item("numEmailTemplate")
    '                                                objDocuments.DomainID = Session("DomainID")
    '                                                dtEmailTemplate = objDocuments.GetDocByGenDocID
    '                                                If dtEmailTemplate.Rows.Count > 0 Then
    '                                                    Dim objSendMail As New Email
    '                                                    Dim dtMergeFields As New DataTable
    '                                                    Dim drNew As DataRow
    '                                                    dtMergeFields.Columns.Add("Assignee")
    '                                                    dtMergeFields.Columns.Add("Stage")
    '                                                    dtMergeFields.Columns.Add("DueDate")
    '                                                    dtMergeFields.Columns.Add("OppID")
    '                                                    drNew = dtMergeFields.NewRow
    '                                                    drNew("Assignee") = ddlAssignTo.SelectedItem.Text
    '                                                    drNew("Stage") = txtStageDetails.Text
    '                                                    drNew("DueDate") = FormattedDateFromDate(strDueDate, Session("DateFormat"))
    '                                                    drNew("OppID") = txtName.Text
    '                                                    dtMergeFields.Rows.Add(drNew)
    '                                                    If dtProjectProcess.Rows(i + 1).Item("numStagePercentage") <> 100 Then
    '                                                        If dtProjectProcess.Rows(i + 1).Item("numStagePercentage") <> 0 Then
    '                                                            If Not dtProjectProcess.Rows(i + 1).Item("Op_Flag") = 1 Then
    '                                                                Dim ddlNAssignTo As DropDownList
    '                                                                ddlNAssignTo = uwOppTab.FindControl("ddlAgginTo~" & dtProjectProcess.Rows(i + 1).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i + 1).Item("numstagedetailsID"))

    '                                                                objCommon.byteMode = 1
    '                                                                objCommon.ContactID = ddlNAssignTo.SelectedItem.Value
    '                                                                objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), IIf(dtDetails.Rows(0).Item("tintCCManager") = 1, objCommon.GetManagerEmail, ""), Session("UserEmail"), objCommon.GetContactsEmail, dtMergeFields)
    '                                                            End If
    '                                                        End If
    '                                                    End If
    '                                                End If
    '                                            End If
    '                                        End If
    '                                    Catch ex As Exception
    '                                    End Try
    '                                End If
    '                                If txtProcessId.Text <> "0" And txtTemplateId.Text <> "0" Then
    '                                    Try
    '                                        intModified = 1
    '                                        Dim objAlerts As New CAlerts
    '                                        Dim dtDetails As DataTable
    '                                        objAlerts.AlertDTLID = 2 'Alert DTL ID for sending alerts in opportunities
    '                                        objAlerts.DomainID = Session("DomainID")
    '                                        dtDetails = objAlerts.GetIndAlertDTL
    '                                        If dtDetails.Rows.Count > 0 Then
    '                                            If dtDetails.Rows(0).Item("tintAlertOn") = 1 Then
    '                                                Dim dtEmailTemplate As DataTable
    '                                                Dim objDocuments As New DocumentList
    '                                                objDocuments.GenDocID = CInt(txtTemplateId.Text)
    '                                                objDocuments.DomainID = Session("DomainID")
    '                                                dtEmailTemplate = objDocuments.GetDocByGenDocID
    '                                                If dtEmailTemplate.Rows.Count > 0 Then
    '                                                    Dim objSendMail As New Email
    '                                                    Dim dtMergeFields As New DataTable
    '                                                    Dim drNew As DataRow
    '                                                    dtMergeFields.Columns.Add("Assignee")
    '                                                    dtMergeFields.Columns.Add("Stage")
    '                                                    dtMergeFields.Columns.Add("DueDate")
    '                                                    dtMergeFields.Columns.Add("OppID")
    '                                                    drNew = dtMergeFields.NewRow
    '                                                    drNew("Assignee") = ddlAssignTo.SelectedItem.Text
    '                                                    drNew("Stage") = txtStageDetails.Text
    '                                                    drNew("DueDate") = FormattedDateFromDate(strDueDate, Session("DateFormat"))
    '                                                    drNew("OppID") = txtName.Text
    '                                                    dtMergeFields.Rows.Add(drNew)
    '                                                    If dtProjectProcess.Rows(i + 1).Item("numStagePercentage") <> 100 Then
    '                                                        If dtProjectProcess.Rows(i + 1).Item("numStagePercentage") <> 0 Then
    '                                                            If Not dtProjectProcess.Rows(i + 1).Item("Op_Flag") = 1 Then
    '                                                                Dim ddlNAssignTo As DropDownList
    '                                                                ddlNAssignTo = uwOppTab.FindControl("ddlAgginTo~" & dtProjectProcess.Rows(i + 1).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i + 1).Item("numstagedetailsID"))

    '                                                                objCommon.byteMode = 1
    '                                                                objCommon.ContactID = ddlNAssignTo.SelectedItem.Value
    '                                                                objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), IIf(dtDetails.Rows(0).Item("tintCCManager") = 1, objCommon.GetManagerEmail, ""), Session("UserEmail"), objCommon.GetContactsEmail, dtMergeFields)
    '                                                            End If
    '                                                        End If
    '                                                    End If
    '                                                End If
    '                                            End If
    '                                        End If
    '                                    Catch ex As Exception
    '                                    End Try
    '                                End If
    '                                If intModified = 1 Then
    '                                    dtProjectProcess.Rows(i).Item("numModifiedBy") = Session("UserContactID")
    '                                    dtProjectProcess.Rows(i).Item("bintModifiedDate") = Date.UtcNow
    '                                End If
    '                                intModified = 0
    '                                If chkAlert.Checked = True And ddlAssignTo.SelectedIndex > 0 Then
    '                                    Try
    '                                        Dim objAlerts As New CAlerts
    '                                        Dim dtDetails As DataTable
    '                                        objAlerts.AlertDTLID = 1 'Alert DTL ID for sending alerts in opportunities
    '                                        objAlerts.DomainID = Session("DomainID")
    '                                        dtDetails = objAlerts.GetIndAlertDTL
    '                                        If dtDetails.Rows.Count > 0 Then
    '                                            If dtDetails.Rows(0).Item("tintAlertOn") = 1 Then
    '                                                Dim dtEmailTemplate As DataTable
    '                                                Dim objDocuments As New DocumentList
    '                                                objDocuments.GenDocID = dtDetails.Rows(0).Item("numEmailTemplate")
    '                                                objDocuments.DomainID = Session("DomainID")
    '                                                dtEmailTemplate = objDocuments.GetDocByGenDocID
    '                                                If dtEmailTemplate.Rows.Count > 0 Then
    '                                                    Dim objSendMail As New Email
    '                                                    Dim dtMergeFields As New DataTable
    '                                                    Dim drNew As DataRow
    '                                                    dtMergeFields.Columns.Add("Assignee")
    '                                                    dtMergeFields.Columns.Add("Stage")
    '                                                    dtMergeFields.Columns.Add("DueDate")
    '                                                    dtMergeFields.Columns.Add("OppID")
    '                                                    drNew = dtMergeFields.NewRow
    '                                                    drNew("Assignee") = ddlAssignTo.SelectedItem.Text
    '                                                    drNew("Stage") = txtStageDetails.Text
    '                                                    drNew("DueDate") = FormattedDateFromDate(strDueDate, Session("DateFormat"))
    '                                                    drNew("OppID") = txtName.Text
    '                                                    dtMergeFields.Rows.Add(drNew)

    '                                                    objCommon.byteMode = 1
    '                                                    objCommon.ContactID = ddlAssignTo.SelectedItem.Value
    '                                                    objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), IIf(dtDetails.Rows(0).Item("tintCCManager") = 1, objCommon.GetManagerEmail, ""), Session("UserEmail"), objCommon.GetContactsEmail, dtMergeFields)
    '                                                End If
    '                                            End If
    '                                        End If
    '                                    Catch ex As Exception

    '                                    End Try
    '                                End If
    '                            End If
    '                        End If
    '                    End If
    '                Next
    '                txtProcessId.Text = "0"
    '                Dim txtDealClosedComments As TextBox
    '                Dim chkDC As CheckBox
    '                txtDealClosedComments = uwOppTab.FindControl("txtDCComm")
    '                chkDC = uwOppTab.FindControl("chkDClosed")
    '                If chkDC.Checked = True Then
    '                    dtProjectProcess.Rows(dtProjectProcess.Rows.Count - 1).Item("bitStageCompleted") = 1

    '                    ' Sending Mail to Internal prjoject's Manager and external project manager
    '                    Try
    '                        Dim dtDetails As DataTable
    '                        Dim objAlerts As New CAlerts

    '                        objAlerts.AlertID = 9
    '                        dtDetails = objAlerts.GetAlertDetails
    '                        Dim strTo As String = ""
    '                        If dtDetails.Rows(2).Item("tintAlertOn") = 1 Then
    '                            objCommon.ContactID = ddlIntPrgMgr.SelectedItem.Value
    '                            If ddlIntPrgMgr.SelectedItem.Value > 0 Then
    '                                strTo = objCommon.GetContactsEmail
    '                            End If
    '                        End If
    '                        Dim dtEmailTemplate As DataTable
    '                        Dim objDocuments As New DocumentList
    '                        objDocuments.GenDocID = dtDetails.Rows(2).Item("numEmailTemplate")
    '                        objDocuments.DomainID = Session("DomainID")
    '                        dtEmailTemplate = objDocuments.GetDocByGenDocID
    '                        If dtEmailTemplate.Rows.Count > 0 Then
    '                            Dim objSendMail As New Email
    '                            Dim dtMergeFields As New DataTable
    '                            Dim drNew As DataRow
    '                            dtMergeFields.Columns.Add("OppID")
    '                            dtMergeFields.Columns.Add("Organization")
    '                            drNew = dtMergeFields.NewRow
    '                            drNew("OppID") = txtName.Text
    '                            drNew("Organization") = hplCustomer.Text

    '                            If strTo <> "" Then
    '                                If dtDetails.Rows(3).Item("tintAlertOn") = 1 Then
    '                                    objCommon.ContactID = ddlCustPrjMgr.SelectedItem.Value
    '                                    If ddlCustPrjMgr.SelectedItem.Value > 0 Then
    '                                        strTo = strTo & "," & objCommon.GetContactsEmail
    '                                    Else : strTo = objCommon.GetContactsEmail
    '                                    End If
    '                                End If
    '                            Else
    '                                objCommon.ContactID = ddlCustPrjMgr.SelectedItem.Value
    '                                strTo = objCommon.GetContactsEmail
    '                            End If
    '                            Dim dtEmail As DataTable
    '                            objAlerts.AlertDTLID = 14
    '                            objAlerts.DomainID = Session("DomainId")
    '                            dtEmail = objAlerts.GetAlertEmails
    '                            objCommon.byteMode = 0
    '                            objCommon.ContactID = ddlIntPrgMgr.SelectedItem.Value
    '                            Dim strCC As String = ""
    '                            Dim p As Integer
    '                            For p = 0 To dtEmail.Rows.Count - 1
    '                                strCC = strCC & dtEmail.Rows(p).Item("vcEmailID") & ","
    '                            Next
    '                            strCC = strCC.TrimEnd(",")
    '                            If dtDetails.Rows(2).Item("tintCCManager") = 1 Then
    '                                Dim strCCAssignee As String = objCommon.GetManagerEmail
    '                                If strCCAssignee <> "" Then
    '                                    strCC = strCC & "," & strCCAssignee
    '                                End If
    '                            End If
    '                            dtMergeFields.Rows.Add(drNew)
    '                            objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), strCC, Session("UserEmail"), strTo, dtMergeFields)
    '                        End If
    '                    Catch ex As Exception

    '                    End Try
    '                Else : dtProjectProcess.Rows(dtProjectProcess.Rows.Count - 1).Item("bitStageCompleted") = 0
    '                End If
    '                dtProjectProcess.Rows(dtProjectProcess.Rows.Count - 1).Item("vcComments") = txtDealClosedComments.Text
    '                Session("ProProcessDetails") = dtProjectProcess
    '            End If
    '        End If
    '        MileStoneItemsCreate()
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Sub MileStoneItemsCreate()
    '    Try
    '        If Not Session("ProProcessDetails") Is Nothing Then
    '            Dim dtProjectProcess As DataTable = Session("ProProcessDetails")
    '            Dim i As Integer = 0
    '            Dim bitProjectCompleted As Boolean

    '            If dtProjectProcess.Rows(dtProjectProcess.Rows.Count - 1).Item("numStagePercentage") = 100 Then
    '                bitProjectCompleted = dtProjectProcess.Rows(dtProjectProcess.Rows.Count - 1).Item("bitStageCompleted")
    '            End If

    '            For i = 0 To dtProjectProcess.Rows.Count - 1
    '                'If Deal Is lost Delete All the Action Items and Calendar Items
    '                If Not bitProjectCompleted Then
    '                    'Must Have an assigneTo and Start Date Set
    '                    'Stage Should not be 100 or 0 (Deal completed or Deal Lost)
    '                    If bitProjectCompleted Then
    '                        If dtProjectProcess.Rows(i).Item("bitStageCompleted") <> True Then
    '                            dtProjectProcess.Rows(i).Item("bitStageCompleted") = True
    '                            Dim utcdate As DateTime = DateTime.Now.AddMinutes(Session("ClientMachineUTCTimeOffset"))
    '                            dtProjectProcess.Rows(i).Item("bintStageComDate") = utcdate
    '                        End If
    '                    End If
    '                    If dtProjectProcess.Rows(i).Item("numStagePercentage") <> 100 _
    '                                                And dtProjectProcess.Rows(i).Item("numStagePercentage") <> 0 _
    '                                                And dtProjectProcess.Rows(i).Item("numAssignTo") <> 0 _
    '                                                And dtProjectProcess.Rows(i).Item("numStartDate") <> 0 Then

    '                        Dim startDate As String
    '                        Dim startTime As String
    '                        Dim endTime As String
    '                        Dim bitAddUpdate As Boolean = True

    '                        '0 is deal closed ,for First Stage If Start date is selected as previous stage closedate Exit 
    '                        If i = 1 And dtProjectProcess.Rows(i).Item("numStartDate") >= 3 Then
    '                            bitAddUpdate = False
    '                        End If

    '                        If dtProjectProcess.Rows(i).Item("numStartDate") = 1 Then
    '                            startDate = CType(dtProjectProcess.Rows(i).Item("bintDueDate"), DateTime).Date
    '                        ElseIf dtProjectProcess.Rows(i).Item("numStartDate") = 2 Then
    '                            startDate = CType(dtProjectProcess.Rows(i).Item("bintCreatedDate"), DateTime).Date
    '                        Else
    '                            ' If Previous Stage Is Not Closed Exit
    '                            If Not dtProjectProcess.Rows(i - 1).Item("bitStageCompleted") Then
    '                                bitAddUpdate = False
    '                            End If
    '                            startDate = CType(DateAdd(DateInterval.Day, dtProjectProcess.Rows(i).Item("numStartDate") - 3, _
    '                                        dtProjectProcess.Rows(i - 1).Item("bintStageComDate")), DateTime).Date
    '                        End If
    '                        Dim strStartTimeText As String = CType(uwOppTab.FindControl("ddlStartTime~" & dtProjectProcess.Rows(i).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i).Item("numstagedetailsID")), DropDownList).SelectedItem.Text.Trim()
    '                        Dim strEndTimeText As String = CType(uwOppTab.FindControl("ddlEndTime~" & dtProjectProcess.Rows(i).Item("numStagePercentage") & "~" & dtProjectProcess.Rows(i).Item("numstagedetailsID")), DropDownList).SelectedItem.Text.Trim()
    '                        startTime = startDate.Trim & " " & strStartTimeText & ":00" & IIf(dtProjectProcess.Rows(i).Item("numStartTimePeriod") = 0, " AM", " PM")
    '                        endTime = startDate.Trim & " " & strEndTimeText & ":00" & IIf(dtProjectProcess.Rows(i).Item("numEndTimePeriod") = 0, " AM", " PM")

    '                        Dim numcommId As Long = dtProjectProcess.Rows(i).Item("numCommActId")

    '                        Select Case CInt(dtProjectProcess.Rows(i).Item("numEvent"))                    'Type Of Event

    '                            Case 2
    '                                'If no Action Item type is selected Exit
    '                                If dtProjectProcess.Rows(i).Item("numType") = 0 Then
    '                                    bitAddUpdate = False
    '                                End If
    '                                If bitAddUpdate = True Then

    '                                    If objActionItem Is Nothing Then
    '                                        objActionItem = New ActionItem
    '                                    End If
    '                                    objCommon.ContactID = ddlCustPrjMgr.SelectedValue
    '                                    objCommon.charModule = "C"
    '                                    objCommon.GetCompanySpecificValues1()

    '                                    With objActionItem
    '                                        .CommID = numcommId
    '                                        .Task = dtProjectProcess.Rows(i).Item("numType")
    '                                        .ContactID = ddlCustPrjMgr.SelectedValue
    '                                        .DivisionID = objCommon.DivisionID
    '                                        .Details = dtProjectProcess.Rows(i).Item("txtCom")
    '                                        .AssignedTo = dtProjectProcess.Rows(i).Item("numAssignTo")
    '                                        .UserCntID = ddlCustPrjMgr.SelectedValue
    '                                        .DomainID = Session("DomainID")
    '                                        If dtProjectProcess.Rows(i).Item("numType") = 973 Or bitProjectCompleted = True Then
    '                                            .BitClosed = 1
    '                                        Else : .BitClosed = 0
    '                                        End If
    '                                        If dtProjectProcess.Rows(i).Item("numType") = 973 Or dtProjectProcess.Rows(i).Item("numType") = 974 Then
    '                                            .StartTime = startDate
    '                                            .EndTime = DateAdd(DateInterval.Day, 1, .StartTime)
    '                                        Else
    '                                            .StartTime = startTime
    '                                            .EndTime = endTime
    '                                        End If
    '                                        .Activity = dtProjectProcess.Rows(i).Item("numActivity")
    '                                        .Status = 0
    '                                        .Snooze = 0
    '                                        .SnoozeStatus = 0
    '                                        .Remainder = dtProjectProcess.Rows(i).Item("numReminder")
    '                                        .RemainderStatus = 0
    '                                        .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
    '                                        .bitOutlook = 0
    '                                        .SendEmailTemplate = 0
    '                                        .EmailTemplate = 0
    '                                        .Hours = 0
    '                                        .Alert = 0
    '                                        .ActivityId = 0
    '                                        .CaseID = 0
    '                                    End With

    '                                    numcommId = objActionItem.SaveCommunicationinfo()
    '                                    dtProjectProcess.Rows(i).Item("numCommActId") = numcommId
    '                                End If
    '                            Case 3

    '                                If objOutLook Is Nothing Then
    '                                    objOutLook = New COutlook
    '                                End If
    '                                objOutLook.UserCntID = dtProjectProcess.Rows(i).Item("numAssignTo")
    '                                objOutLook.DomainId = Session("DomainId")
    '                                Dim dtResource As DataTable
    '                                dtResource = objOutLook.GetResourceId
    '                                If dtResource.Rows.Count <= 0 Then
    '                                    bitAddUpdate = False
    '                                End If
    '                                If bitAddUpdate = True Then
    '                                    If bitProjectCompleted = False Then
    '                                        Dim strSplitStartTimeDate As Date                                               'To store the Date and Time for the start of the Action Item
    '                                        Dim strSplitEndTimeDate As Date         'To store the Date and Time for the end of the Action Item
    '                                        strSplitStartTimeDate = CType(startTime, DateTime).AddMinutes(Session("ClientMachineUTCTimeOffset"))        'Convert the Start Date and Time to UT
    '                                        strSplitEndTimeDate = CType(endTime, DateTime).AddMinutes(Session("ClientMachineUTCTimeOffset"))           'Convert the End Date and Time to UTC


    '                                        objOutLook.AllDayEvent = False
    '                                        objOutLook.ActivityDescription = dtProjectProcess.Rows(i).Item("txtCom")
    '                                        objOutLook.Location = ""
    '                                        objOutLook.Subject = dtProjectProcess.Rows(i).Item("vcComments")
    '                                        objOutLook.Duration = DateDiff(DateInterval.Second, strSplitStartTimeDate, strSplitEndTimeDate)
    '                                        objOutLook.StartDateTimeUtc = strSplitStartTimeDate
    '                                        objOutLook.EnableReminder = True
    '                                        objOutLook.ReminderInterval = 900
    '                                        objOutLook.ShowTimeAs = 3
    '                                        objOutLook.Importance = 0
    '                                        objOutLook.RecurrenceKey = -999
    '                                        objOutLook.ResourceName = dtResource.Rows(0).Item("ResourceId")

    '                                        If numcommId = 0 Then
    '                                            numcommId = objOutLook.AddActivity()
    '                                            dtProjectProcess.Rows(i).Item("numCommActId") = numcommId
    '                                        Else
    '                                            objOutLook.ActivityID = numcommId
    '                                            objOutLook.UpdateActivity()
    '                                        End If
    '                                    Else
    '                                        If numcommId <> 0 Then
    '                                            objOutLook.ActivityID = numcommId
    '                                            objOutLook.RemoveActivity()
    '                                            dtProjectProcess.Rows(i).Item("numCommActId") = 0
    '                                        End If
    '                                    End If
    '                                End If
    '                        End Select
    '                    End If
    '                Else
    '                    If dtProjectProcess.Rows(i).Item("numEvent") = 2 And dtProjectProcess.Rows(i).Item("numCommActId") <> 0 Then
    '                        If objActionItem Is Nothing Then
    '                            objActionItem = New ActionItem
    '                        End If
    '                        objActionItem.CommID = dtProjectProcess.Rows(i).Item("numCommActId")
    '                        objActionItem.DeleteActionItem()
    '                        dtProjectProcess.Rows(i).Item("numCommActId") = 0
    '                    End If
    '                    If dtProjectProcess.Rows(i).Item("numEvent") = 3 And dtProjectProcess.Rows(i).Item("numCommActId") <> 0 Then
    '                        If objOutLook Is Nothing Then
    '                            objOutLook = New COutlook
    '                        End If
    '                        objOutLook.ActivityID = dtProjectProcess.Rows(i).Item("numCommActId")
    '                        objOutLook.RemoveActivity()
    '                        dtProjectProcess.Rows(i).Item("numCommActId") = 0
    '                    End If
    '                End If
    '            Next
    '            Session("ProProcessDetails") = dtProjectProcess
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub btnTSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTSave.Click
    '    Try
    '        If SaveProjects() = 1 Then
    '            litMessage.Text = " No of Incidents Not Sufficient"
    '            Exit Sub
    '        End If
    '        SaveCusField()
    '        Session("ProProcessDetails") = objProject.SalesProcessDtlByProId
    '        If ViewState("ReloadMilestone") = 1 Then ViewState("MileCheck") = 0
    '        CreatMilestone()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub btnTSaveAndCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTSaveAndCancel.Click
    '    Try
    '        If SaveProjects() = 1 Then
    '            litMessage.Text = " No of Incidents Not Sufficient"
    '            Exit Sub
    '        End If
    '        SaveCusField()
    '        PageRedirect()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub btnTCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTCancel.Click
    '    Try
    '        PageRedirect()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Function FillCustomer(ByVal ddlCombo As DropDownList, ByVal strName As String)
    '    Try

    '        If objCommon Is Nothing Then objCommon = New CCommon
    '        With objCommon
    '            .DomainID = Session("DomainID")
    '            .UserCntID = Session("UserContactID")
    '            .Filter = Trim(strName) & "%"
    '            ddlCombo.DataSource = objCommon.PopulateOrganization.Tables(0).DefaultView
    '            ddlCombo.DataTextField = "vcCompanyname"
    '            ddlCombo.DataValueField = "numDivisionID"
    '            ddlCombo.DataBind()
    '        End With
    '        ddlCombo.Items.Insert(0, New ListItem("--Select One--", "0"))
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    'Private Sub btnAddContact_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddContact.Click
    '    Try
    '        If objProject Is Nothing Then objProject = New Project
    '        objProject.bytemode = 0
    '        objProject.ProjectId = lngProID
    '        objProject.ContactID = CInt(ddlAssocContactId.SelectedItem.Value)
    '        objProject.DomainID = Session("DomainID")
    '        objProject.ContactRole = ddlContactRole.SelectedValue
    '        objProject.bitPartner = chkShare.Checked
    '        dgContact.DataSource = objProject.AddProContacts
    '        dgContact.DataBind()
    '        ddlContactRole.SelectedIndex = 0
    '        chkShare.Checked = False
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Sub PageRedirect()
    '    Try
    '        If GetQueryStringVal( "frm") = "ProjectList" Then
    '            Response.Redirect("../projects/frmCusProList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
    '        ElseIf GetQueryStringVal( "frm") = "Projectdtl" Then
    '            Response.Redirect("../pagelayout/frmCustProjectdtl.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Public Function FillContact()
    '    Try
    '        Dim objOpport As New COpportunities
    '        With objOpport
    '            .DivisionID = Session("DivID")
    '        End With
    '        With ddlAssocContactId
    '            .DataSource = objOpport.ListContact().Tables(0).DefaultView()
    '            .DataTextField = "Name"
    '            .DataValueField = "numcontactId"
    '            .DataBind()
    '            .ClearSelection()
    '        End With
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    'Sub DisplayDynamicFlds()
    '    Try
    '        'm_aryRightsForCusFlds = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmContacts.aspx", Session("userID"), 11, 9)
    '        'If m_aryRightsForCusFlds(RIGHTSTYPE.VIEW) = 0 Then
    '        '    Exit Sub
    '        'End If
    '        Dim strDate As String
    '        Dim bizCalendar As UserControl
    '        Dim _myUC_DueDate As PropertyInfo
    '        Dim PreviousRowID As Integer = 0
    '        Dim objRow As HtmlTableRow
    '        Dim objCell As HtmlTableCell
    '        Dim i, k, j As Integer
    '        Dim dtTable As DataTable
    '        'Dim count As Integer = tsVert.Items.Count
    '        Dim count As Integer = uwOppTab.Tabs.Count
    '        ' Tabstrip4.Items.Clear()
    '        ObjCus = New CustomFields
    '        ObjCus.locId = 11
    '        ObjCus.DomainID = Session("DomainID")
    '        ObjCus.RelId = 0
    '        ObjCus.RecordId = lngProID
    '        dtTable = ObjCus.GetCustFlds.Tables(0)
    '        Session("CusFields") = dtTable

    '        If uwOppTab.Tabs.Count > 3 Then
    '            Dim iItemcount As Integer
    '            iItemcount = uwOppTab.Tabs.Count
    '            While uwOppTab.Tabs.Count > 3
    '                uwOppTab.Tabs.RemoveAt(iItemcount - 1)
    '                iItemcount = iItemcount - 1
    '            End While
    '        End If

    '        If dtTable.Rows.Count > 0 Then
    '            'Main Detail Section
    '            k = 0
    '            objRow = New HtmlTableRow
    '            For i = 0 To dtTable.Rows.Count - 1
    '                If dtTable.Rows(i).Item("TabId") = 0 Then
    '                    If k = 2 Then
    '                        k = 0
    '                        tblMain.Rows.Add(objRow)
    '                        objRow = New HtmlTableRow
    '                    End If

    '                    objCell = New HtmlTableCell
    '                    objCell.Align = "Right"
    '                    objCell.Attributes.Add("class", "normal1")
    '                    If dtTable.Rows(i).Item("fld_type") <> "Link" Then
    '                        objCell.InnerText = dtTable.Rows(i).Item("fld_label")
    '                    Else : objCell.InnerText = ""
    '                    End If
    '                    objRow.Cells.Add(objCell)
    '                    If dtTable.Rows(i).Item("fld_type") = "Text Box" Then
    '                        objCell = New HtmlTableCell
    '                        CreateTexBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))
    '                    ElseIf dtTable.Rows(i).Item("fld_type") = "Drop Down List Box" Then
    '                        objCell = New HtmlTableCell
    '                        CreateDropdown(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CInt(dtTable.Rows(i).Item("Value")), dtTable.Rows(i).Item("numlistid"))
    '                    ElseIf dtTable.Rows(i).Item("fld_type") = "Check box" Then
    '                        objCell = New HtmlTableCell
    '                        CreateChkBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CInt(dtTable.Rows(i).Item("Value")))
    '                    ElseIf dtTable.Rows(i).Item("fld_type") = "Text Area" Then
    '                        objCell = New HtmlTableCell
    '                        CreateTextArea(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))
    '                    ElseIf dtTable.Rows(i).Item("fld_type") = "Date Field" Then
    '                        PreviousRowID = i
    '                        objCell = New HtmlTableCell
    '                        bizCalendar = LoadControl("../include/calandar.ascx")
    '                        bizCalendar.ID = "cal" & dtTable.Rows(i).Item("fld_id")
    '                        objCell.Controls.Add(bizCalendar)
    '                        objRow.Cells.Add(objCell)

    '                    ElseIf dtTable.Rows(i).Item("fld_type") = "Link" Then
    '                        objCell = New HtmlTableCell
    '                        CreateLink(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("vcURL"), lngProID, dtTable.Rows(i).Item("fld_label"))
    '                    End If
    '                    k = k + 1
    '                End If
    '            Next
    '            tblMain.Rows.Add(objRow)

    '            'CustomField Section
    '            Dim Tab As Infragistics.WebUI.UltraWebTab.Tab
    '            'Dim Tab As Tab
    '            '                    Dim ContentPane As Infragistics.WebUI.UltraWebTab.con
    '            Dim aspTable As HtmlTable
    '            Dim Table As Table
    '            Dim tblcell As TableCell
    '            Dim tblRow As TableRow
    '            k = 0
    '            ViewState("TabId") = dtTable.Rows(0).Item("TabId")
    '            ViewState("Check") = 0
    '            ViewState("FirstTabCreated") = 0
    '            ' Tabstrip3.Items.Clear()
    '            For i = 0 To dtTable.Rows.Count - 1
    '                If dtTable.Rows(i).Item("TabId") <> 0 Then
    '                    If ViewState("TabId") <> dtTable.Rows(i).Item("TabId") Or ViewState("FirstTabCreated") = 0 Then
    '                        If ViewState("Check") <> 0 Then
    '                            aspTable.Rows.Add(objRow)
    '                            tblcell.Controls.Add(aspTable)
    '                            tblRow.Cells.Add(tblcell)
    '                            Table.Rows.Add(tblRow)
    '                            Tab.ContentPane.Controls.Add(Table)
    '                            '  pageView.Controls.Add(Table)
    '                            'uwOppTab.Tabs.Add(Tab)
    '                            '  mpages.Controls.Add(pageView)
    '                        End If
    '                        k = 0
    '                        ViewState("FirstTabCreated") = 1
    '                        ViewState("Check") = 1
    '                        '   If Not IsPostBack Then
    '                        ViewState("TabId") = dtTable.Rows(i).Item("TabId")
    '                        Tab = New Tab
    '                        Tab.Text = "&nbsp;&nbsp;" & dtTable.Rows(i).Item("tabname") & "&nbsp;&nbsp;"

    '                        uwOppTab.Tabs.Add(Tab)
    '                        'End If
    '                        'pageView = New PageView
    '                        aspTable = New HtmlTable
    '                        Table = New Table
    '                        Table.Width = Unit.Percentage(100)
    '                        Table.BorderColor = System.Drawing.Color.FromName("black")
    '                        Table.GridLines = GridLines.None
    '                        Table.BorderWidth = Unit.Pixel(1)
    '                        Table.Height = Unit.Pixel(300)
    '                        Table.CssClass = "aspTable"
    '                        tblcell = New TableCell
    '                        tblRow = New TableRow
    '                        tblcell.VerticalAlign = VerticalAlign.Top
    '                        aspTable.Width = "100%"
    '                        objRow = New HtmlTableRow
    '                        objCell = New HtmlTableCell
    '                        objCell.InnerHtml = "<br>"
    '                        objRow.Cells.Add(objCell)
    '                        aspTable.Rows.Add(objRow)
    '                        objRow = New HtmlTableRow
    '                    End If

    '                    'pageView.Controls.Add("")
    '                    If k = 3 Then
    '                        k = 0
    '                        aspTable.Rows.Add(objRow)

    '                        objRow = New HtmlTableRow
    '                    End If
    '                    objCell = New HtmlTableCell
    '                    objCell.Align = "right"
    '                    objCell.Attributes.Add("class", "normal1")
    '                    If dtTable.Rows(i).Item("fld_type") <> "Link" Then
    '                        objCell.InnerText = dtTable.Rows(i).Item("fld_label")
    '                    End If
    '                    objRow.Cells.Add(objCell)
    '                    If dtTable.Rows(i).Item("fld_type") = "Text Box" Then
    '                        objCell = New HtmlTableCell
    '                        CreateTexBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))
    '                    ElseIf dtTable.Rows(i).Item("fld_type") = "Drop Down List Box" Then
    '                        objCell = New HtmlTableCell
    '                        CreateDropdown(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CInt(dtTable.Rows(i).Item("Value")), dtTable.Rows(i).Item("numlistid"))
    '                    ElseIf dtTable.Rows(i).Item("fld_type") = "Check box" Then
    '                        objCell = New HtmlTableCell
    '                        CreateChkBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CInt(dtTable.Rows(i).Item("Value")))
    '                    ElseIf dtTable.Rows(i).Item("fld_type") = "Text Area" Then
    '                        objCell = New HtmlTableCell
    '                        CreateTextArea(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))
    '                    ElseIf dtTable.Rows(i).Item("fld_type") = "Date Field" Then
    '                        PreviousRowID = i
    '                        objCell = New HtmlTableCell
    '                        bizCalendar = LoadControl("../include/calandar.ascx")
    '                        bizCalendar.ID = "cal" & dtTable.Rows(i).Item("fld_id")
    '                        objCell.Controls.Add(bizCalendar)
    '                        objRow.Cells.Add(objCell)
    '                    ElseIf dtTable.Rows(i).Item("fld_type") = "Link" Then
    '                        objCell = New HtmlTableCell
    '                        CreateLink(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("vcURL"), lngProID, dtTable.Rows(i).Item("fld_label"))
    '                    ElseIf dtTable.Rows(i).Item("fld_type") = "Frame" Then
    '                        objCell = New HtmlTableCell
    '                        Dim strFrame As String
    '                        Dim URL As String
    '                        URL = dtTable.Rows(i).Item("vcURL")
    '                        URL = URL.Replace("RecordID", lngProID)
    '                        strFrame = "<iframe src ='" & URL & "' width='100%' frameborder='0' height= '800px'></iframe>"
    '                        objCell.Controls.Add(New LiteralControl(strFrame))
    '                        objRow.Cells.Add(objCell)
    '                    End If
    '                    k = k + 1
    '                End If
    '            Next
    '            If ViewState("Check") = 1 Then
    '                aspTable.Rows.Add(objRow)
    '                tblcell.Controls.Add(aspTable)
    '                tblRow.Cells.Add(tblcell)
    '                Table.Rows.Add(tblRow)
    '                Tab.ContentPane.Controls.Add(Table)
    '            End If
    '        End If

    '        Dim dvCusFields As DataView
    '        dvCusFields = dtTable.DefaultView
    '        dvCusFields.RowFilter = "fld_type='Date Field'"
    '        Dim iViewCount As Integer
    '        For iViewCount = 0 To dvCusFields.Count - 1
    '            If Not IsDBNull(dvCusFields(iViewCount).Item("Value")) Then
    '                bizCalendar = uwOppTab.FindControl("cal" & dvCusFields(iViewCount).Item("fld_id"))
    '                If Not bizCalendar Is Nothing Then
    '                    Dim _myControlType As Type = bizCalendar.GetType()
    '                    _myUC_DueDate = _myControlType.GetProperty("SelectedDate")
    '                    strDate = dvCusFields(iViewCount).Item("Value")
    '                    If strDate = "0" Then strDate = ""
    '                    If strDate <> "" Then
    '                        'strDate = DateFromFormattedDate(strDate, Session("DateFormat"))
    '                        _myUC_DueDate.SetValue(bizCalendar, strDate, Nothing)
    '                    End If
    '                End If
    '            End If
    '        Next
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Sub SaveCusField()
    '    Try
    '        Dim dtTable As DataTable
    '        Dim i As Integer
    '        If Not Session("CusFields") Is Nothing Then
    '            dtTable = Session("CusFields")
    '            For i = 0 To dtTable.Rows.Count - 1
    '                If dtTable.Rows(i).Item("fld_type") = "Text Box" Then
    '                    Dim txt As TextBox
    '                    txt = uwOppTab.FindControl(dtTable.Rows(i).Item("fld_id"))
    '                    dtTable.Rows(i).Item("Value") = txt.Text
    '                ElseIf dtTable.Rows(i).Item("fld_type") = "Drop Down List Box" Then
    '                    Dim ddl As DropDownList
    '                    ddl = uwOppTab.FindControl(dtTable.Rows(i).Item("fld_id"))
    '                    dtTable.Rows(i).Item("Value") = CStr(ddl.SelectedItem.Value)
    '                ElseIf dtTable.Rows(i).Item("fld_type") = "Check box" Then
    '                    Dim chk As CheckBox
    '                    chk = uwOppTab.FindControl(dtTable.Rows(i).Item("fld_id"))
    '                    If chk.Checked = True Then
    '                        dtTable.Rows(i).Item("Value") = "1"
    '                    Else : dtTable.Rows(i).Item("Value") = "0"
    '                    End If
    '                ElseIf dtTable.Rows(i).Item("fld_type") = "Text Area" Then
    '                    Dim txt As TextBox
    '                    txt = uwOppTab.FindControl(dtTable.Rows(i).Item("fld_id"))
    '                    dtTable.Rows(i).Item("Value") = txt.Text
    '                ElseIf dtTable.Rows(i).Item("fld_type") = "Date Field" Then
    '                    Dim BizCalendar As UserControl
    '                    BizCalendar = uwOppTab.FindControl("cal" & dtTable.Rows(i).Item("fld_id"))
    '                    Dim strDueDate As String
    '                    Dim _myControlType As Type = BizCalendar.GetType()
    '                    Dim _myUC_DueDate As PropertyInfo = _myControlType.GetProperty("SelectedDate")
    '                    strDueDate = _myUC_DueDate.GetValue(BizCalendar, Nothing)
    '                    If strDueDate <> "" Then
    '                        dtTable.Rows(i).Item("Value") = strDueDate
    '                    Else : dtTable.Rows(i).Item("Value") = ""
    '                    End If
    '                End If
    '            Next

    '            Dim ds As New DataSet
    '            Dim strdetails As String
    '            dtTable.TableName = "Table"
    '            ds.Tables.Add(dtTable.Copy)
    '            strdetails = ds.GetXml
    '            ds.Tables.Remove(ds.Tables(0))
    '            ObjCus.strDetails = strdetails
    '            ObjCus.locId = 11
    '            ObjCus.RecordId = lngProID
    '            ObjCus.SaveCustomFldsByRecId()
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

End Class