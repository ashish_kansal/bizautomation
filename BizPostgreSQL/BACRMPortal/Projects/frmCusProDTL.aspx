<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCusProDTL.aspx.vb"
    Inherits="BACRMPortal.frmCusProDTL" %>

<<%--%@ Register TagPrefix="menu1" TagName="Menu" Src="../common/topbar.ascx" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
    	<link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <title>Project</title>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">

    <script language="JavaScript" src="../include/DATE-PICKER.JS" type="text/javascript"></script>

    <script language="javascript">
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeletMsg() {
            var bln = confirm("You're about to remove the Stage from this Process, all stage data will be deleted")
            if (bln == true) {
                return true;
            }
            else {
                return false;
            }
        }
        function OpenDocuments(a) {
            window.open("../Documents/frmSpecDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=P&yunWE=" + a, '', 'toolbar=no,titlebar=no,top=200,width=700,height=450,left=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function CheckNumber() {
            if (!(window.event.keyCode > 47 && window.event.keyCode < 58)) {
                window.event.keyCode = 0;
            }
        }

        function Save(cint) {

            if (cint == 1) {
                if (document.Form1.uwOppTab__ctl0_ddlIntPrgMgr.selectedIndex == 0) {
                    alert("Select Internal Project Manager");
                    tsVert.selectedIndex = 0;
                    document.Form1.ddlTaskContact.focus();
                    return false;
                }
            }
            if (document.Form1.uwOppTab__ctl0_ddlCustPrjMgr.value == 0) {
                alert("Select Customer Project Manager");
                tsVert.selectedIndex = 0;
                document.Form1.ddlCustPrjMgr.focus();
                return false;
            }

        }
        function AddContact() {
            if (document.Form1.uwOppTab__ctl2_ddlAssocContactId.value == 0) {
                alert("Select Contact");
                document.Form1.uwOppTab.tabIndex = 2;
                document.Form1.uwOppTab__ctl2_ddlAssocContactId.focus();
                return false;
            }
            var str;
            for (i = 0; i < document.Form1.elements.length; i++) {
                if (i <= 9) {
                    str = '0' + (i + 1)
                }
                else {
                    str = i + 1
                }
                if (document.getElementById('uwOppTab__ctl2_dgContact_ctl' + str + '_txtContactID') != null) 
                {
                    if (document.getElementById('uwOppTab__ctl2_dgContact_ctl' + str + '_txtContactID').value == document.Form1.uwOppTab__ctl2_ddlAssocContactId.value) {
                        alert("Associated contact is already added");
                        return false;
                    }
                }
            }

        }

        function deleteItem() {
            var bln;
            bln = window.confirm("Delete Seleted Row - Are You Sure ?")
            if (bln == true) {
                return true;
            }
            else {
                return false;
            }
        }


        function OpenTemplate(a, b) {
            window.open('../common/templates.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&pageid=' + a + '&id=' + b, '', 'toolbar=no,titlebar=no,left=500, top=300,width=350,height=200,scrollbars=no,resizable=yes');
            return false;
        }
        function OpenDependency(a, b, c, d) {
            window.open('../projects/frmProDependency.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ProStageID=' + a + '&Proid=' + b + '&PerID=' + c + '&StgDtlId=' + d, '', 'toolbar=no,titlebar=no,left=500, top=300,width=500,height=200,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenExpense(a, b, c, d, e) {
            window.open('../projects/frmProExpense.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ProStageID=' + a + '&Proid=' + b + '&PerID=' + c + '&StgDtlId=' + d + '&DivId=' + e, '', 'toolbar=no,titlebar=no,left=500, top=300,width=650,height=300,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenTime(a, b, c, d, e) {
            window.open('../projects/frmProTime.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ProStageID=' + a + '&Proid=' + b + '&PerID=' + c + '&StgDtlId=' + d + '&DivId=' + e, '', 'toolbar=no,titlebar=no,left=500, top=300,width=650,height=300,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenSubStage(a, b, c, d) {
            window.open('../projects/frmprosubstages.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ProStageID=' + a + '&Proid=' + b + '&PerID=' + c + '&StgDtlId=' + d, '', 'toolbar=no,titlebar=no,left=500, top=300,width=500,height=200,scrollbars=yes,resizable=yes');
            return false;
        }
        function CheckBoxCon(a, b, c) {
            if (parseInt(c) == 1) {
                document.getElementById('uwOppTab__ctl1_chkStage~' + a + '~' + b).checked = true
            }
            else {
                document.getElementById('uwOppTab__ctl1_chkStage~' + a + '~' + b).checked = false
            }
        }
        function ValidateCheckBox(cint) {
            if (cint == 1) {
                if (document.getElementById('chkDClosed').checked == true) {
                    if (document.getElementById('chkDlost').checked == true) {
                        alert("The Deal is already Lost !")
                        document.getElementById('chkDClosed').checked = false
                        return false;
                    }
                }
            }
            if (cint == 2) {
                if (document.getElementById('chkDlost').checked == true) {
                    if (document.getElementById('chkDClosed').checked == true) {
                        alert("The Deal is already Closed !")
                        document.getElementById('chkDlost').checked = false
                        return false;
                    }
                }
            }
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function ShowWindow1(Page, q, att) {
            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                //window.location.reload(true);
                return false;

            }

        }
        function OpenTransfer(url) {
            window.open(url, '', "width=340,height=150,status=no,top=100,left=150");
            return false;
        }
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <menu1:Menu ID="webmenu1" runat="server"></menu1:Menu>
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
        EnableViewState="true">
        <ContentTemplate>
            <table width="100%" align="center">
                <tr>
                    <td>
                        <table id="tblMenu" bordercolor="black" cellspacing="0" cellpadding="0" width="100%"
                            border="0" runat="server">
                            <tr>
                                <td class="tr1" align="center">
                                    <b>Record Owner: </b>
                                    <asp:Label ID="lblRecordOwner" runat="server" ForeColor="Black"></asp:Label>
                                </td>
                                <td class="td1" width="1" height="18">
                                </td>
                                <td class="tr1" align="center">
                                    <b>Created By: </b>
                                    <asp:Label ID="lblCreatedBy" runat="server" ForeColor="Black"></asp:Label>
                                </td>
                                <td class="td1" width="1" height="18">
                                </td>
                                <td class="tr1" align="center">
                                    <b>Last Modified By: </b>
                                    <asp:Label ID="lblLastModifiedBy" runat="server" ForeColor="Black"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td class="normal1" align="left">
                                    Organization : <u>
                                        <asp:HyperLink ID="hplCustomer" runat="server" CssClass="hyperlink"></asp:HyperLink>
                                </td>
                                <td align="right">
                                    <asp:Button ID="btnTSave" runat="server" CssClass="button" Text="Save"></asp:Button>
                                    <asp:Button ID="btnTSaveAndCancel" runat="server" CssClass="button" Text="Save &amp; Close">
                                    </asp:Button>
                                    <asp:Button ID="btnTCancel" runat="server" CssClass="button" Text="Close"></asp:Button>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <igtab:UltraWebTab ImageDirectory="" ID="uwOppTab" runat="server" ThreeDEffect="True"
                            BorderStyle="Solid" Width="100%" BarHeight="0" BorderWidth="0">
                            <DefaultTabStyle Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Arial">
                            </DefaultTabStyle>
                            <RoundedImage LeftSideWidth="7" RightSideWidth="8" ShiftOfImages="0" SelectedImage="../images/ig_tab_winXPs3.gif"
                                NormalImage="../images/ig_tab_winXP3.gif" HoverImage="../images/ig_tab_winXPs3.gif"
                                FillStyle="LeftMergedWithCenter"></RoundedImage>
                            <SelectedTabStyle Height="23px" ForeColor="white">
                            </SelectedTabStyle>
                            <HoverTabStyle Height="23px" ForeColor="white">
                            </HoverTabStyle>
                            <Tabs>
                                <igtab:Tab Text="&nbsp;&nbsp;Project Details&nbsp;&nbsp;">
                                    <ContentTemplate>
                                        <asp:Table ID="tblOppr" BorderWidth="1" CellPadding="0" CellSpacing="0" runat="server"
                                            Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Height="300">
                                            <asp:TableRow>
                                                <asp:TableCell VerticalAlign="Top">
                                                    <br>
                                                    <table id="tblMain" width="100%" runat="server">
                                                        <tr>
                                                            <td valign="top" rowspan="30">
                                                                <img src="../images/Compass-32.gif" />
                                                            </td>
                                                            <td class="normal1" align="right">
                                                                Project Name
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtName" TabIndex="6" runat="server" Width="300" CssClass="signup"
                                                                    MaxLength="100"></asp:TextBox>
                                                            </td>
                                                            <td class="normal1" align="right">
                                                                Assigned to
                                                            </td>
                                                            <td class="normal1">
                                                                <asp:DropDownList ID="ddlAssignedTo" runat="server" CssClass="signup" Width="180">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="normal1" align="right">
                                                                Opportunity Or Deal
                                                            </td>
                                                            <td>
                                                                <asp:ListBox ID="lstOpportunity" runat="server" CssClass="signup" Width="300" SelectionMode="Multiple">
                                                                </asp:ListBox>
                                                            </td>
                                                            <td class="normal1" align="right">
                                                                Internal Project Manager
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlIntPrgMgr" runat="server" CssClass="signup" Width="200">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="normal1" align="right" rowspan="2">
                                                                Comments
                                                            </td>
                                                            <td rowspan="2">
                                                                <asp:TextBox ID="txtOComments" TabIndex="11" Height="44px" runat="server" Width="300"
                                                                    CssClass="signup" MaxLength="250" TextMode="MultiLine"></asp:TextBox>
                                                            </td>
                                                            <td class="normal1" align="right">
                                                                Customer Side Project Manager
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlCustPrjMgr" runat="server" CssClass="signup" Width="200">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="normal1" align="right">
                                                                Due Date
                                                            </td>
                                                            <td class="normal1">
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <BizCalendar:Calendar ID="calDue" runat="server" />
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </ContentTemplate>
                                </igtab:Tab>
                                <igtab:Tab Text="&nbsp;&nbsp;Milestones And Stages&nbsp;&nbsp;">
                                    <ContentTemplate>
                                        <asp:Table ID="tblMile" BorderWidth="1" runat="server" Width="100%" CellPadding="0"
                                            CellSpacing="0" CssClass="aspTable" BorderColor="black" GridLines="None" Height="300">
                                            <asp:TableRow>
                                                <asp:TableCell VerticalAlign="Top">
                                                    <br>
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr id="trSalesProcess" runat="server">
                                                            <td class="normal1" align="right">
                                                                Project Process
                                                            </td>
                                                            <td colspan="4">
                                                                &nbsp;
                                                                <asp:DropDownList ID="ddlProcessList" runat="server" Width="130px" AutoPostBack="True"
                                                                    CssClass="signup">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <br>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="5">
                                                                <table bordercolor="black" width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Table ID="tblMilestone" runat="server" Width="100%" CellPadding="0" CellSpacing="0"
                                                                                GridLines="None">
                                                                            </asp:Table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </ContentTemplate>
                                </igtab:Tab>
                                <igtab:Tab Text="&nbsp;&nbsp;Associated Contacts&nbsp;&nbsp;">
                                    <ContentTemplate>
                                        <asp:Table ID="tblCont" BorderWidth="1" runat="server" CellPadding="0" CellSpacing="0"
                                            Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Height="300">
                                            <asp:TableRow>
                                                <asp:TableCell VerticalAlign="Top">
                                                    <br>
                                                    <table width="100%">
                                                        <tr>
                                                            <td class="normal1" align="right">
                                                                Contact
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlAssocContactId" runat="server" CssClass="signup" Width="180">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="normal1" align="right">
                                                                Contact Role
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlContactRole" runat="server" CssClass="signup" Width="180">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="normal1" align="right">
                                                                Share Opportunity via Partner Point ?
                                                            </td>
                                                            <td>
                                                                <asp:CheckBox ID="chkShare" runat="server" />&nbsp;&nbsp;&nbsp;
                                                                <asp:Button ID="btnAddContact" runat="server" CssClass="button" Text="Add Contact">
                                                                </asp:Button>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br />
                                                    <asp:DataGrid ID="dgContact" runat="server" CssClass="dg" Width="100%" BorderColor="white"
                                                        AutoGenerateColumns="False">
                                                        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                                        <ItemStyle CssClass="is"></ItemStyle>
                                                        <HeaderStyle CssClass="hs"></HeaderStyle>
                                                        <Columns>
                                                            <asp:BoundColumn DataField="numContactId" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="bitPartner" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn HeaderText="Organization,Relationship" DataField="Company"></asp:BoundColumn>
                                                            <asp:BoundColumn HeaderText="First & Last Name" DataField="Name"></asp:BoundColumn>
                                                            <asp:BoundColumn HeaderText="Phone - Ext" DataField="Phone"></asp:BoundColumn>
                                                            <asp:BoundColumn HeaderText="Email" DataField="Email"></asp:BoundColumn>
                                                            <asp:BoundColumn HeaderText="Contact Role" DataField="ContactRole"></asp:BoundColumn>
                                                            <asp:TemplateColumn HeaderText="Share Project via Partner Point ?">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" CssClass="cell1" ID="lblShare" Font-Size="Large"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn>
                                                                <HeaderTemplate>
                                                                    <asp:Button ID="btnHdeleteCnt" runat="server" CssClass="Delete" Text="X"></asp:Button>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtContactID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "numContactId") %>'>
                                                                    </asp:TextBox>
                                                                    <asp:Button ID="btnDeleteCnt" runat="server" CssClass="Delete" Text="X" CommandName="Delete">
                                                                    </asp:Button>
                                                                    <asp:LinkButton ID="lnkDeleteCnt" runat="server" Visible="false">
																<font color="#730000">*</font></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                    <br>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </ContentTemplate>
                                </igtab:Tab>
                            </Tabs>
                        </igtab:UltraWebTab>
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td class="normal4" align="center">
                        <asp:Literal ID="litMessage" runat="server"></asp:Literal>
                    </td>
                    <asp:TextBox ID="txtTemplateId" Text="0" runat="server" Style="display: none"></asp:TextBox>
                    <asp:TextBox ID="txtProcessId" Text="0" runat="server" Style="display: none"></asp:TextBox>
                    <asp:TextBox runat="server" ID="txtDivId" Style="display: none"></asp:TextBox>
                    <asp:TextBox ID="txtProName" Style="display: none" runat="server"></asp:TextBox>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
--%>