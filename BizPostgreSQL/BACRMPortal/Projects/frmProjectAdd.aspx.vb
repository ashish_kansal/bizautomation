Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Outlook

Public Class frmProjectAdd : Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblOpptComments As System.Web.UI.WebControls.Label
    Protected WithEvents btnSave As System.Web.UI.WebControls.Button
    Protected WithEvents txtCompName As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnGo As System.Web.UI.WebControls.Button
    Protected WithEvents ddlCompany As System.Web.UI.WebControls.DropDownList
    Protected WithEvents Table1 As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents tblCustomer As System.Web.UI.HtmlControls.HtmlTable
    ' Protected WithEvents ddlOpportunity As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlInternal As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlCusomer As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtDueDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lstOpportunity As System.Web.UI.WebControls.ListBox

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


    Dim objCommon As New CCommon

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not IsPostBack Then
                Session("Help") = "Project"
                If GetQueryStringVal( "uihTR") <> "" Or GetQueryStringVal( "rtyWR") <> "" Or GetQueryStringVal( "tyrCV") <> "" Or GetQueryStringVal( "pluYR") <> "" Or GetQueryStringVal( "fghTY") <> "" Then

                    
                    m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmProjectAdd.aspx", Session("UserContactID"), 30, 1)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AS")
                    If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                        btnSave.Visible = False
                        Exit Sub
                    Else : btnSave.Visible = True
                    End If
                    If GetQueryStringVal( "uihTR") <> "" Then
                        objCommon.ContactID = GetQueryStringVal( "uihTR")
                        objCommon.charModule = "C"
                    ElseIf GetQueryStringVal( "rtyWR") <> "" Then
                        objCommon.DivisionID = GetQueryStringVal( "rtyWR")
                        objCommon.charModule = "D"
                    ElseIf GetQueryStringVal( "tyrCV") <> "" Then
                        objCommon.ProID = GetQueryStringVal( "tyrCV")
                        objCommon.charModule = "P"
                    ElseIf GetQueryStringVal( "pluYR") <> "" Then
                        objCommon.OppID = GetQueryStringVal( "pluYR")
                        objCommon.charModule = "O"
                    ElseIf GetQueryStringVal( "fghTY") <> "" Then
                        objCommon.CaseID = GetQueryStringVal( "fghTY")
                        objCommon.charModule = "S"
                    End If
                    objCommon.GetCompanySpecificValues1()
                   Dim strCompany, strDivision As String
                    strCompany = objCommon.GetCompanyName
                    ddlCompany.Items.Add(strCompany)
                    ddlCompany.Items.FindByText(strCompany).Value = objCommon.DivisionID
                    LoadContacts()
                    'If ddlCompany.SelectedItem.Value > 0 Then
                    '    LoadOpportunity()
                    'End If
                    If GetQueryStringVal( "uihTR") <> "" Then
                        If Not ddlCusomer.Items.FindByValue(objCommon.ContactID) Is Nothing Then
                            ddlCusomer.Items.FindByValue(objCommon.ContactID).Selected = True
                        End If
                    End If
                End If
                If GetQueryStringVal( "frm") = "outlook" Then
                    Dim objOutlook As New COutlook
                    Dim dttable As DataTable
                    objOutlook.numEmailHstrID = GetQueryStringVal( "EmailHstrId")
                    objOutlook.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    dttable = objOutlook.getMail()
                    If dttable.Rows.Count > 0 Then txtDescription.Text = dttable.Rows(0).Item("vcBodyText")
                End If
            End If
            btnSave.Attributes.Add("onclick", "return Save()")
            btnClose.Attributes.Add("onclick", "return Close()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Public Function FillCustomer()
        Try

            If objCommon Is Nothing Then objCommon = New CCommon
            With objCommon
                .DomainID = Session("DomainID")
                .Filter = Trim(txtCompName.Text) & "%"
                .UserCntID = Session("UserContactID")
                ddlCompany.DataSource = objCommon.PopulateOrganization.Tables(0).DefaultView
                ddlCompany.DataTextField = "vcCompanyname"
                ddlCompany.DataValueField = "numDivisionID"
                ddlCompany.DataBind()
            End With
            ddlCompany.Items.Insert(0, New ListItem("---Select One---", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            FillCustomer()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim arrOutPut As String()
            Dim objProject As New Project
            objProject.ProjectId = 0
            objProject.DivisionID = ddlCompany.SelectedItem.Value
            objProject.ProComments = ""
            If calDue.SelectedDate <> "" Then objProject.DueDate = calDue.SelectedDate
            objProject.ProjectName = ddlCompany.SelectedItem.Text & "-" & Format(Now(), "MMMM")
            objProject.UserCntID = Session("UserContactID")
            objProject.OpportunityId = 0
            objProject.IntPrjMgr = ddlInternal.SelectedItem.Value
            objProject.CusPrjMgr = ddlCusomer.SelectedItem.Value
            objProject.DomainID = Session("DomainId")
            objProject.ProComments = txtDescription.Text
            arrOutPut = objProject.Save()
            'objProject.ProjectId = arrOutPut(0)
            'Dim i As Integer = 0
            'Dim strOppSelected As String = ""
            'For i = 0 To lstOpportunity.Items.Count - 1
            '    If lstOpportunity.Items(i).Selected = True Then
            '        strOppSelected = strOppSelected & lstOpportunity.Items(i).Value.ToString & ","
            '    End If
            'Next
            'objProject.strOppSel = strOppSelected
            'objProject.SaveProjectOpportunities()
            Dim strScript As String = "<script language=JavaScript>"
            strScript += "window.opener.reDirectPage('../projects/frmProjects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=ProjectList&ProId=" & arrOutPut(0) & "'); self.close();"
            strScript += "</script>"
            If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

 

    Private Sub ddlCompany_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            LoadContacts()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadContacts()
        Try
            Dim fillCombo As New COpportunities
            With fillCombo
                .DivisionID = ddlCompany.SelectedItem.Value
                ddlCusomer.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
                ddlCusomer.DataTextField = "Name"
                ddlCusomer.DataValueField = "numcontactId"
                ddlCusomer.DataBind()
            End With
            ddlCusomer.Items.Insert(0, New ListItem("---Select One---", "0"))
            'objCommon.DivisionID = ddlCompany.SelectedItem.Value
            'objCommon.DomainID = Session("DomainId")
            'objCommon.ContactType = 92
            objCommon.FillAssignToBasedOnPreference(ddlInternal, CCommon.ToLong(ddlCompany.SelectedValue))
            'ddlInternal.DataSource = objCommon.AssignTo
            'ddlInternal.DataTextField = "vcName"
            'ddlInternal.DataValueField = "ContactID"
            'ddlInternal.DataBind()
            'ddlInternal.Items.Insert(0, "--Select One--")
            'ddlInternal.Items.FindByText("--Select One--").Value = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class

