Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Contract
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common

Partial Public Class frmAddContract : Inherits BACRMPage

    Dim ContractId As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            txtInci.Attributes.Add("onkeypress", "CheckNumber(1)")
            txtHrs.Attributes.Add("onkeypress", "CheckNumber(1)")
            btnSaveClose.Attributes.Add("onclick", "return Save()")
            btnClose.Attributes.Add("onclick", "Close()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Public Function FillCustomer()
        Try

            Dim objCommon As New CCommon
            With objCommon
                .DomainID = Session("DomainID")
                .Filter = Trim(txtCompName.Text) & "%"
                .UserCntID = Session("UserContactID")
                ddlCompany.DataSource = objCommon.PopulateOrganization.Tables(0).DefaultView()
                ddlCompany.DataTextField = "vcCompanyname"
                ddlCompany.DataValueField = "numDivisionID"
                ddlCompany.DataBind()
            End With
            ddlCompany.Items.Insert(0, New ListItem("---Select One---", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            FillCustomer()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        saveContract()
        fncPageRedirect()
    End Sub

    Private Sub fncPageRedirect()
        Try
            Dim strScript As String = "<script language=JavaScript>"
            strScript += "window.opener.reDirectPage('../ContractManagement/frmContract.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=ContractList&contractId=" & ContractId & "'); self.close();"
            strScript += "</script>"
            If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub saveContract()
        Try
            Dim objContract As New CContracts
            With objContract
                .DivisionId = ddlCompany.SelectedItem.Value
                .ContractName = txtContractName.Text

                .DomainId = Session("DomainID")
                .UserCntId = Session("UserContactID")
                If chkAmount.Checked = True Then
                    .Amount = txtAmount.Text
                    .bitAmount = True
                Else : .bitAmount = False
                End If
                If (chkDays.Checked = True) Then
                    .bitDays = True
                    If calStartDate.SelectedDate <> "" Then objContract.StartDate = calStartDate.SelectedDate
                    If calExpDate.SelectedDate <> "" Then objContract.ExpDate = calExpDate.SelectedDate
                Else : .bitDays = False
                End If
                If (chkIncidents.Checked = True) Then
                    .Incidents = txtInci.Text
                    .bitInci = True
                Else : .bitInci = False
                End If
                If (chkHrs.Checked = True) Then
                    .Hours = txtHrs.Text
                    .bitHrs = True
                Else : .bitHrs = False
                End If

                .Notes = txtNotes.Text
                .EmailDays = 0
                .EmailIncidents = 0
                .EmailHours = 0
            End With
            ContractId = objContract.SaveContracts
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub chkIncidents_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIncidents.CheckedChanged
        Try
            If chkIncidents.Checked Then
                txtInci.Enabled = True
            Else : txtInci.Enabled = False
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub chkHrs_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkHrs.CheckedChanged
        Try
            If chkHrs.Checked Then
                txtHrs.Enabled = True
            Else : txtHrs.Enabled = False
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class