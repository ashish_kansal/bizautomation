<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmContract.aspx.vb" Inherits="BACRMPortal.frmContract" %>
<<%--%@ Register TagPrefix="BizCalendar" TagName="Calendar" src="../include/calandar.ascx" %>
<%@ Register TagPrefix="menu1" TagName="webmenu" src="../common/topbar.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
        <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
        	<link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <title>New Contract</title>

	<script language="javascript" type="text/javascript" >
	function CheckSchedule()
	    
	{
	
	   if (document.getElementById('uwOppTab$_ctl0$ddlEmailTemp').value == 0)
	   {
	            alert("Select EmailTemplate")
				document.form1.uwOppTab$_ctl0$ddlEmailTemp.focus();
				return false;
	   }
	   if (document.getElementById('uwOppTab$_ctl0$ddlContacts').value == 0)
	   {
	            alert("Select Contact")
				document.form1.uwOppTab$_ctl0$ddlEmailTemp.focus();
				return false;
	   }	
	   if (document.getElementById('uwOppTab__ctl0_chkEmailDays').checked == false && document.getElementById('uwOppTab__ctl0_chkEmailHrs').checked == false && document.getElementById('uwOppTab__ctl0_chkEmailInci').checked == false) 
	   {
        	    alert('Select Conditions')
	    		return false;
	   }
	   if (document.getElementById('uwOppTab__ctl0_chkEmailDays').checked == true && document.getElementById('uwOppTab$_ctl0$txtEmailDays').value == '0')
	   {
	            alert('Enter Days before Contract Expiration Date')
	            document.getElementById('uwOppTab$_ctl0$txtEmailDays').focus();
	    		return false;
	   }
	   if (document.getElementById('uwOppTab__ctl0_chkEmailInci').checked == true && document.getElementById('uwOppTab$_ctl0$txtEmailInci').value == '0')
	   {
	            alert('Enter support incidents left')
	            document.getElementById('uwOppTab$_ctl0$txtEmailInci').focus();
	    		return false;
	   }
	   if (document.getElementById('uwOppTab__ctl0_chkEmailHrs').checked == true && document.getElementById('uwOppTab$_ctl0$txtEmailHrs').value == '0')
	   {
	            alert('Enter hours left')
	            document.getElementById('uwOppTab$_ctl0$txtEmailHrs').focus();
	    		return false;
	   }
	}
	function reLoad()
		{
		    document.form1.btnClick.click();
		}
	function OpenET(a)
		{
		    window.open('../Marketing/frmEmailTemplate.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&AlertDTLID='+a,'','toolbar=no,titlebar=no,top=100,left=100,width=850,height=500,scrollbars=yes,resizable=yes')
		}
	function openProjects(a)
	{
	    window.open("../ContractManagement/frmContractsProCases.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ContractID="+a+"&dg=Projects",'','toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')
	    return false
	}
	function openCases(a)
	{
	      window.open("../ContractManagement/frmContractsProCases.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ContractID="+a+"&dg=Cases",'','toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')
	    return false
	}
	function Save()
	{
	  if ((document.form1.uwOppTab$_ctl0$ddlCompany.value=="") || (document.form1.uwOppTab$_ctl0$ddlCompany.value==0))
			{
				alert("Select Company")
				document.form1.uwOppTab$_ctl0$txtCompName.focus();
				return false;
			}
			if (document.form1.uwOppTab$_ctl0$txtContractName.value=="")
			{
			    //alert("Date val=" & document.form1.uwOppTab$_ctl0$calStartDate.value)
				alert("Enter Contract Name")
				document.form1.uwOppTab$_ctl0$txtContractName.focus();
				return false;
			}
			
			if(document.form1.uwOppTab$_ctl0$chkAmount.checked==true)
			{
			    if (document.form1.uwOppTab$_ctl0$txtAmount.value=="")
			    {
				    alert("Enter Amount")
				    document.form1.uwOppTab$_ctl0$txtAmount.focus();
				    return false;
			    }
			}
			
			
			if (document.form1.uwOppTab$_ctl0$chkDays.checked==true)
			{
			    if(document.form1.uwOppTab__ctl0_calStartDate_txtDate.value=="")
			    {
			        alert("Select the Start Date")
				   document.form1.uwOppTab__ctl0_calStartDate_txtDate.focus();
				    return false;
			    }
			    if(document.form1.uwOppTab__ctl0_calExpDate_txtDate.value=="")
			    {
			        alert("Select the Expiration Date")
				    document.form1.uwOppTab__ctl0_calExpDate_txtDate.focus();
				    return false;
			    }
			    
			}
			if(document.form1.uwOppTab$_ctl0$chkIncidents.checked==true)
			{
			    if (document.form1.uwOppTab$_ctl0$txtInci.value=="")
			    {
				    alert("Enter No. of Incidents")
				    document.form1.uwOppTab$_ctl0$txtInci.focus();
				    return false;
			    }
			}
			if(document.form1.uwOppTab$_ctl0$chkHrs.checked==true)
			{
			    if (document.form1.uwOppTab$_ctl0$txtHrs.value=="")
			    {
				    alert("Enter No. of Hours")
				    document.form1.uwOppTab$_ctl0$txtHrs.focus();
				    return false;
			    }
			    if (document.form1.uwOppTab__ctl0_txtRateHr.value=="" || document.form1.uwOppTab__ctl0_txtRateHr.value=="0" ||document.form1.uwOppTab__ctl0_txtRateHr.value=="0.00")
			    {
				    alert("Enter Rate/Hour")
				    document.form1.uwOppTab__ctl0_txtRateHr.focus();
				    return false;
			    }
			}
			
	}
	
    
   function Close()
    {
        window.close()
        return false;
    }
    
  function CheckNumber(cint)
	{
		if (cint==1)
		{
			if (!(window.event.keyCode > 47 && window.event.keyCode < 58 || window.event.keyCode==44 || window.event.keyCode==46))
			{
				window.event.keyCode=0;
			}
		}
		if (cint==2)
		{
			if (!(window.event.keyCode > 47 && window.event.keyCode < 58))
			{
				window.event.keyCode=0;
			}
		}
		
	}
	</script>

</head>
<body>
    <form id="form1" runat="server">
  <menu1:webmenu id="webmenu1" runat="server"></menu1:webmenu>
            <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
            </asp:ScriptManager>
    <asp:Button ID="btnClick" runat="server" style="display:none" />
   <table width="100%">
       <tr>
            <td>
                   <table width="100%" cellSpacing="0" cellPadding="0" border="0">
	                    <tr align="left">			
			                <td align="right" >
			                    <asp:button id="btnSave" Runat="server" CssClass="button" Text="Save" Width="50"></asp:button>
				                <asp:Button ID="btnSaveClose" Runat="server" Text="Save &amp; Close" CssClass="button" Width="150"></asp:Button>
				                <asp:button id="btnCancel" Runat="server" CssClass="button" Text="Cancel" Width="70"></asp:button>
				                <asp:button id="btnDel" Visible="false" Runat="server"  CssClass="Delete" Text="X" Width="20"></asp:button>
			                </td>
		                </tr>
	                </table>
            </td>
       </tr>
       <tr>
            <td>
            
            
 
	<igtab:ultrawebtab   ImageDirectory=""  id="uwOppTab" runat="server" ThreeDEffect="True" BorderStyle="Solid" Width="100%" BarHeight="0"  BorderWidth="0">
             <DefaultTabStyle Height="23px" Font-Bold="true" Font-Size="11px" Font-Names="Arial" >
                        </DefaultTabStyle>
                        <RoundedImage LeftSideWidth="7" RightSideWidth="8" ShiftOfImages="0" SelectedImage="../images/ig_tab_winXPs3.gif" NormalImage="../images/ig_tab_winXP3.gif" HoverImage="../images/ig_tab_winXPs3.gif" FillStyle="LeftMergedWithCenter" ></RoundedImage>
                     <SelectedTabStyle Height="23px"  ForeColor="white">
                        </SelectedTabStyle>
                        <HoverTabStyle Height="23px" ForeColor="white">
                        </HoverTabStyle>
           <Tabs>
           <igtab:Tab Text="Contract" >
           <ContentTemplate>
                    <asp:updatepanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" 
                    UpdateMode="Conditional" EnableViewState="true" >
                       <ContentTemplate>
			            	<asp:table id="tblContacts" Runat="server" BorderWidth="1" Width="100%" GridLines="None" BorderColor="black" CssClass="aspTable"
									Height="300">
									<asp:TableRow>
										<asp:TableCell VerticalAlign="Top">
                					
					                    <table width="100%" class="normal1">
						                    <tr>
						                        <td colspan="4">
						                        Account<font color="red">*</font>
						                        <asp:textbox id="txtCompName" Runat="server" cssclass="signup"/> &nbsp;
						                        <asp:Button ID="btnGo" Runat="server" Text="Go" CssClass="button"></asp:Button>&nbsp;
						                        <asp:DropDownList ID="ddlCompany" Runat="server" AutoPostBack="True" CssClass="signup" Width="150"></asp:DropDownList>
						                        </td>
					                        </tr>
							                <tr>
								                <td>Contract Name<font color="red">*</font><asp:TextBox ID="txtContractName" runat="server" 
								                Width="250" CssClass="signup"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								                 <asp:HyperLink ID="hplProjects"  CssClass="hyperlink" runat="server" Text="Linked Items"></asp:HyperLink></td>								
							                </tr>
							                <tr><td class="normal7">Commitments</td></tr>
							                <tr>
							                        <td>
							                           <table width="100%" class="normal1">
					                                    <tr>
    					                                  <td width="10%">
    					                                    <asp:CheckBox ID="chkAmount" runat="server" cssclass="normal7" Text="Amount" />
    					                                  </td>
    					                                  <td>
    					                                     <asp:TextBox ID="txtAmount" runat="server" CssClass="signup" Text=""></asp:TextBox> &nbsp; &nbsp;
    					                                     <asp:Label ID="lblBalance" runat ="server" CssClass="signup" Text="Balance of Deferred Income:"  Visible ="false"  ></asp:Label> &nbsp;
    					                                     <asp:Label ID="lblBalanceAmt" runat ="server" CssClass="signup" Visible ="false" ></asp:Label>
    					                                  </td>
    					                                  <td  align="right" class="normal7">Remaining Amount :</td>
    					                                  <td width="20%"><asp:Label runat="server" ID="lblRemAmount" Text="0" CssClass="normal1"></asp:Label></td>
					                                    </tr>
					                                    <tr>
    					                                    <td width="10%">
    					                                   <asp:CheckBox ID="chkDays" cssclass="normal7" runat="server" Text="Days" />
    					                                    </td>
    					                                    <td>
    					                                    <table> <tr><td align="left">Starting Date</td><td align="left">
						                                              <BizCalendar:Calendar id="calStartDate" runat="server" />
						                                         </td>   
						                                        <td>Expiration Date </td><td align="left">
						                                               <BizCalendar:Calendar id="calExpDate" runat="server" />
						                                               </td></tr></table>
    					                                    </td>
    					                                   <td  align="right" class="normal7">Remaining Days :</td>
    					                                  <td><asp:Label runat="server" ID="lblRemDays" CssClass="normal1"></asp:Label></td>
					                                    </tr>
					                                    <tr>
    					                                    <td width="10%">
    					                                   <asp:CheckBox ID="chkIncidents" runat="server" cssclass="normal7" Text="Incidents (Support)" />
    					                                    </td>
    					                                    <td>
    					                                     <asp:TextBox ID="txtInci" CssClass="signup" runat="server" ></asp:TextBox>
    					                                    </td>
    					                                    <td align="right" class="normal7">Remaining Incidents :</td>
    					                                  <td><asp:Label runat="server" ID="lblRemInci" CssClass="normal1"></asp:Label></td>
					                                    </tr>
					                                    <tr>
    					                                    <td width="10%">
    					                                   <asp:CheckBox ID="chkHrs" CssClass="normal7" runat="server" Text="Hours" />
    					                                    </td>
    					                                    <td>
    					                                     <table> 
    					                                        <tr>
    						                                        <td><asp:TextBox ID="txtHrs" CssClass="signup" runat="server" ></asp:TextBox></td>
    						                                        <td>&nbsp;Rate/Hour</td>
    						                                        <td><asp:TextBox ID="txtRateHr" Width="50" Text="0" CssClass="signup" runat="server" ></asp:TextBox></td>
    						                            
						                                            
						                                        </tr>
						                                     </table>
    					                                    </td>
    					                                    <td align="right" class="normal7">Remaining Hours :</td>
    					                                  <td><asp:Label runat="server" ID="lblRemHrs" CssClass="normal1"></asp:Label></td>
					                                    </tr>
					                                     <tr>
    					                                    <td width="10%">
    					                                       &nbsp;Notes:
    					                                    </td>
    					                                    <td>
    					                                    <asp:TextBox CssClass="signup" ID="txtNotes" runat="server" TextMode="MultiLine" Width="300"></asp:TextBox>
    					                                    </td>
    					                                    <td align="right" class="normal7" >
    					                                        <asp:HyperLink runat="server" ID="hptCreateTemplate"  CssClass="hyperlink" Text="Create Email Template"></asp:HyperLink>
    					                                    </td>
    					                                  
					                                    </tr>
					                                 
					                                </table>
					                        </td>
						                    </tr>
						                    <tr>
                						    <td>
						                        <table class="normal1">
						                        <tr>
						                            <td>Email the following Template</td>
						                            <td><asp:DropDownList ID="ddlEmailTemp"  CssClass="signup"  runat="server" Width="150"></asp:DropDownList></td>
						                            <td>to the Contract Record Owner</td>	
						                            <td colspan="2" align="right"><asp:CheckBox ID="chkEmailDays" runat="server" /></td>
						                            <td><asp:TextBox ID="txtEmailDays"  CssClass="signup"  runat="server" Width="20"/> Days before Contract Expiration Date</td>					        
						                         </tr>
	     					                     <tr>
						                            <td>and to the following Contacts:</td>
						                            <td><asp:DropDownList ID="ddlContacts" CssClass="signup" runat="server" Width="150"></asp:DropDownList></td>
						                            <td><asp:Button ID="btnAdd" cssclass="button" runat="server" Text="Add" /></td>
						                            <td colspan="2" align="right"><asp:CheckBox ID="chkEmailInci" runat="server" /></td>
						                            <td>When there are <asp:TextBox ID="txtEmailInci"  CssClass="signup"  runat="server" Width="20"/> support incidents left
						                             </td>
						                             <tr>
						                                <td colspan="5" align="right">
						                                    <asp:CheckBox ID="chkEmailHrs" runat="server" /></td>
						                                <td>When there are <asp:TextBox ID="txtEmailHrs"  CssClass="signup"  runat="server" Width="20"/> hours left.
						                                </td>
						                             </tr>
                						                                     
						                        
						                     </table>
						                         </td> 
						                         </tr>
						                 </table>							 
					                </asp:TableCell>
				                </asp:TableRow>
				                <asp:TableRow >
				                    <asp:TableCell>
				                                   <asp:DataGrid ID="dgEmployee" Width="100%" runat="server" AutoGenerateColumns="false" >
				                                      <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							                                            <ItemStyle CssClass="is"></ItemStyle>
							                                            <HeaderStyle CssClass="hs"></HeaderStyle>
							                                            <Columns>
							                                             <asp:BoundColumn DataField="numContactId" Visible="False"></asp:BoundColumn>
							                                             <asp:BoundColumn DataField="Name" HeaderText = "Name"></asp:BoundColumn>
							                    	                            <asp:TemplateColumn HeaderText="Email">
														                            <ItemTemplate>
															                            <asp:HyperLink ID="hplEmail" Runat="server" CssClass="hyperlink" Text='<%# DataBinder.Eval(Container.DataItem, "vcEmail") %>' Target="_blank" >
															                            </asp:HyperLink>
														                            </ItemTemplate>
													                            </asp:TemplateColumn>
													                            <asp:TemplateColumn HeaderText = ""> 														
														                            <ItemTemplate>
															                            <asp:Button ID="btnDelete" Runat="server" CssClass="Delete" Text="X" CommandName="Delete"></asp:Button>															
														                            </ItemTemplate>
													                            </asp:TemplateColumn>
							                                            </Columns>
					                                </asp:DataGrid>
				                    </asp:TableCell>
				                </asp:TableRow>
			                </asp:table>
			                </ContentTemplate>
			                <Triggers>
			                <asp:PostBackTrigger  ControlID="ddlCompany"/>
			                </Triggers>
			                </asp:updatepanel>
                    </ContentTemplate>
                    </igtab:Tab>
                    <igtab:Tab Text="Assets" >
                        <ContentTemplate>
                         <asp:updatepanel ID="updatepanel1" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" >
                        <ContentTemplate>
                        <asp:table id="Table1" Runat="server" BorderWidth="1" CellPadding="0" CellSpacing="0" Width="100%" GridLines="None" BorderColor="black" CssClass="aspTable"
									Height="300">
									<asp:TableRow>
										<asp:TableCell  VerticalAlign="Top">
									             <igtbl:ultrawebgrid id="uwItem" Width="100%" DisplayLayout-AllowRowNumberingDefault="ByDataIsland"  runat="server" Browser="Xml"   Height="100%">
				                                    <DisplayLayout AutoGenerateColumns="false"  RowHeightDefault="18"  AllowAddNewDefault="Yes" Version="3.00" SelectTypeRowDefault="Single"
				                                    ViewType="Hierarchical" TableLayout="Auto" SelectTypeCellDefault="Extended" BorderCollapseDefault="Separate" AllowColSizingDefault="Free" 
				                                    Name="uwItem" EnableClientSideRenumbering="true" SelectTypeColDefault="Extended" AllowUpdateDefault="Yes">
				                                    <HeaderStyleDefault VerticalAlign="Middle" Font-Size="8pt" Font-Bold="true"  Font-Names="Arial" BorderStyle="Solid" HorizontalAlign="Left" ForeColor="white" BackColor="#52658C">
                                                    <Padding Left="2px" Right="2px"></Padding>
                                                    <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
                                                    </HeaderStyleDefault>
                                                    <RowSelectorStyleDefault BackColor="White"></RowSelectorStyleDefault>
                                                     <FrameStyle Width="100%" Cursor="Default" BorderWidth="0px" Font-Size="8pt" Font-Names="Arial" BorderStyle="Double"></FrameStyle>
                                                    <FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
                                                    <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
                                                    </FooterStyleDefault>
                                                    <EditCellStyleDefault BorderWidth="0px" BorderStyle="None"></EditCellStyleDefault>
                                                    <SelectedRowStyleDefault ForeColor="White" BackColor="#666666"></SelectedRowStyleDefault>
                                                    <RowStyleDefault BorderWidth="1px" Font-Size="8pt" Font-Names="Arial" BorderColor="Gray" BorderStyle="Solid" BackColor="White">
                                                    <Padding Left="5px" Right="5px"></Padding>
                                                    <BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
                                                    </RowStyleDefault>
                                                    <RowExpAreaStyleDefault BackColor="LightSteelBlue"></RowExpAreaStyleDefault>
				                                    </DisplayLayout>
					                                    <Bands>
						                                    <igtbl:UltraGridBand AllowDelete="No" AllowAdd="Yes"   >
								                                    <Columns>
									                                  
									                                    <igtbl:UltraGridColumn Hidden="true" IsBound="false" BaseColumnName="numAItemCode" Key="numAItemCode" >
									                                    </igtbl:UltraGridColumn>									                                    
									                                    <igtbl:UltraGridColumn HeaderText="Item Name" Width="41%" AllowUpdate="No" IsBound="false" BaseColumnName="vcitemName" Key="vcitemName" >
									                                    </igtbl:UltraGridColumn>
									                                    <igtbl:UltraGridColumn HeaderText="Serial No" Width="35%" AllowUpdate="No" IsBound="false" BaseColumnName="vcserialno" Key="vcserialno" >
									                                    </igtbl:UltraGridColumn>												                        
									                                    <igtbl:UltraGridColumn HeaderText="Units" Width="20%"  AllowUpdate="No" IsBound="false" Format="###,##0.00"  BaseColumnName="unit" Key="unit" >
									                                    </igtbl:UltraGridColumn>
									                                       <igtbl:UltraGridColumn  Width="4%"  CellButtonDisplay="Always" Type="Button"  Key="Action" >
									                                    </igtbl:UltraGridColumn>
							                                    </Columns>
						                                    </igtbl:UltraGridBand>												                   
					                                    </Bands>
                                                    </igtbl:ultrawebgrid>
									        </asp:TableCell>
								        </asp:TableRow>
								    </asp:table>
								    </ContentTemplate>
								   </asp:updatepanel>
							  </ContentTemplate>
            </igtab:Tab>
            </Tabs>
            </igtab:ultrawebtab>
            </td>
       </tr>
   </table>
    </form>
</body>
</html>
--%>