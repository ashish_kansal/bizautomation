Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Contract
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Marketing

Partial Public Class frmContract : Inherits BACRMPage

    'Dim lngCntrId As Integer
    '
    'Dim objCommon As CCommon
    'Dim objContract As CContracts

    'Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '    Try
    '        If GetQueryStringVal( "contractId") <> "" Then
    '            lngCntrId = GetQueryStringVal( "contractId")
    '            btnSave.Attributes.Add("onclick", "return Save()")
    '            btnSaveClose.Attributes.Add("onclick", "return Save()")
    '            txtAmount.Attributes.Add("onKeyPress", "CheckNumber(2)")
    '            txtHrs.Attributes.Add("onKeyPress", "CheckNumber(2)")
    '            txtInci.Attributes.Add("onKeyPress", "CheckNumber(2)")
    '            txtEmailDays.Attributes.Add("onKeyPress", "CheckNumber(2)")
    '            txtEmailInci.Attributes.Add("onKeyPress", "CheckNumber(2)")
    '            txtEmailHrs.Attributes.Add("onKeyPress", "CheckNumber(2)")
    '            txtRateHr.Attributes.Add("onKeyPress", "CheckNumber(2)")
    '            hplProjects.Attributes.Add("onclick", "return openProjects('" & lngCntrId & "')")

    '        End If
    '        btnAdd.Attributes.Add("onclick", "return CheckSchedule()")
    '        hptCreateTemplate.Attributes.Add("onclick", "return OpenET('" & "29" & "')")
    '        If Not IsPostBack Then
    '            'To Set Permission
    '            objCommon = New CCommon
    '            m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmContract.aspx", Session("UserContactID"), 34, 2)
    '            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
    '                Response.Redirect("../admin/authentication.aspx?mesg=AC")
    '            ElseIf m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
    '                btnSave.Visible = False
    '                btnSaveClose.Visible = False
    '            ElseIf m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
    '                btnDel.Visible = False
    '            End If
    '            FillCustomer()
    '            GetAssignedToDdl()
    '            LoadTemplates()
    '            BindData()
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Sub BindData()
    '    Try
    '        If objContract Is Nothing Then objContract = New CContracts

    '        Dim dtTable As DataTable

    '        objContract.ContractID = lngCntrId
    '        objContract.UserCntId = Session("UserContactID")
    '        objContract.DomainId = Session("DomainID")
    '        dtTable = objContract.GetContractDtl()

    '        If dtTable.Rows.Count > 0 Then
    '            If IsDBNull(dtTable.Rows(0).Item("numDivisionId")) Then
    '                ddlCompany.Items.FindByValue("0").Selected = True
    '            Else
    '                If Not ddlCompany.Items.FindByValue(dtTable.Rows(0).Item("numDivisionId")) Is Nothing Then
    '                    ddlCompany.Items.FindByValue(dtTable.Rows(0).Item("numDivisionId")).Selected = True
    '                    LoadAssets()
    '                End If
    '            End If

    '            txtContractName.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcContractName")), "", dtTable.Rows(0).Item("vcContractName"))
    '            txtNotes.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcNotes")), "", dtTable.Rows(0).Item("vcNotes"))

    '            If Not IsDBNull(dtTable.Rows(0).Item("bitAmount")) Then
    '                'txtAmount.Text = dtTable.Rows(0).Item("numAmount")
    '                chkAmount.Checked = dtTable.Rows(0).Item("bitAmount")
    '                If Not IsDBNull(dtTable.Rows(0).Item("numAmount")) And chkAmount.Checked = True Then
    '                    txtAmount.Text = dtTable.Rows(0).Item("numAmount")
    '                End If
    '                lblRemAmount.Text = IIf(chkAmount.Checked = True, String.Format("{0:#,##0.00}", CDec(dtTable.Rows(0).Item("RemAmount"))), "")
    '            Else
    '                'lblRemAmount.Text = ""
    '                txtAmount.Text = 0
    '                chkAmount.Checked = True
    '            End If
    '            If Not IsDBNull(dtTable.Rows(0).Item("bitHour")) Then
    '                'txtAmount.Text = dtTable.Rows(0).Item("numAmount")
    '                chkHrs.Checked = dtTable.Rows(0).Item("bitHour")
    '                If Not IsDBNull(dtTable.Rows(0).Item("numhours")) And chkHrs.Checked = True Then
    '                    txtHrs.Text = dtTable.Rows(0).Item("numhours")
    '                    txtRateHr.Text = dtTable.Rows(0).Item("decrate")
    '                End If
    '                lblRemHrs.Text = IIf(chkHrs.Checked = True, String.Format("{0:#,##0.00}", CDec(dtTable.Rows(0).Item("RemHours"))), "")
    '            Else
    '                'lblRemHrs.Text = ""
    '                txtHrs.Text = 0
    '                chkHrs.Checked = True
    '            End If
    '            If Not IsDBNull(dtTable.Rows(0).Item("BalanceAmt")) Then
    '                If dtTable.Rows(0).Item("BalanceAmt") > 0 Then
    '                    lblBalance.Visible = True
    '                    lblBalanceAmt.Visible = True
    '                    lblBalanceAmt.Text = String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("BalanceAmt"))
    '                Else
    '                    lblBalance.Visible = False
    '                    lblBalanceAmt.Visible = False
    '                End If
    '            End If

    '            If Not IsDBNull(dtTable.Rows(0).Item("bitdays")) Then
    '                'txtAmount.Text = dtTable.Rows(0).Item("numAmount")
    '                chkDays.Checked = dtTable.Rows(0).Item("bitdays")
    '                If dtTable.Rows(0).Item("Days") <> 0 And chkDays.Checked = True Then

    '                    If IsDBNull(dtTable.Rows(0).Item("bintStartDate")) Then
    '                        calStartDate.SelectedDate = Now()
    '                    Else : calStartDate.SelectedDate = DateAdd(DateInterval.Day, 0, CDate(dtTable.Rows(0).Item("bintStartDate")))
    '                    End If

    '                    If IsDBNull(dtTable.Rows(0).Item("bintExpDate")) Then
    '                        calExpDate.SelectedDate = DateAdd(DateInterval.Day, 30, Now())
    '                    Else : calExpDate.SelectedDate = DateAdd(DateInterval.Day, 0, CDate(dtTable.Rows(0).Item("bintExpDate")))
    '                    End If
    '                End If
    '                lblRemDays.Text = IIf(chkDays.Checked = True, String.Format("{0:#,##0.00}", CDec(dtTable.Rows(0).Item("Days"))), "")
    '            Else : chkDays.Checked = True
    '            End If

    '            If Not IsDBNull(dtTable.Rows(0).Item("bitincidents")) Then
    '                'txtAmount.Text = dtTable.Rows(0).Item("numAmount")
    '                chkIncidents.Checked = dtTable.Rows(0).Item("bitincidents")
    '                If Not IsDBNull(dtTable.Rows(0).Item("numIncidents")) And chkIncidents.Checked = True Then
    '                    txtInci.Text = dtTable.Rows(0).Item("numIncidents")
    '                End If
    '                lblRemInci.Text = IIf(chkIncidents.Checked = True, String.Format("{0:#,##0.00}", CDec(dtTable.Rows(0).Item("Incidents"))), "")
    '            Else
    '                'lblRemInci.Text = ""
    '                txtInci.Text = 0
    '                chkIncidents.Checked = True
    '            End If
    '            'lblRemAmount.Text = CDec(IIf(dtTable.Rows(0).Item("RemAmount") = "-", "-", String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("RemAmount"))))
    '            'lblRemAmount.Text = String.Format("{0:#,##0.00}", CDec(IIf(IsDBNull(dtTable.Rows(0).Item("RemAmount")), "0", dtTable.Rows(0).Item("RemAmount"))))
    '            'lblRemHrs.Text = String.Format("{0:#,##0.00}", CDec(IIf(IsDBNull(dtTable.Rows(0).Item("RemHours")), "", dtTable.Rows(0).Item("RemHours"))))
    '            'lblRemInci.Text = dtTable.Rows(0).Item("Incidents")
    '            'lblRemDays.Text = dtTable.Rows(0).Item("Days")
    '            'lblRemInci.TemplateControl()
    '        End If
    '        If dtTable.Rows(0).Item("bitEincidents") Then
    '            chkEmailInci.Checked = True
    '        Else : chkEmailInci.Checked = False
    '        End If
    '        If dtTable.Rows(0).Item("bitEDays") Then
    '            chkEmailDays.Checked = True
    '        Else : chkEmailDays.Checked = False
    '        End If
    '        If dtTable.Rows(0).Item("bitEHour") Then
    '            chkEmailHrs.Checked = True
    '        Else : chkEmailHrs.Checked = False
    '        End If
    '        txtEmailDays.Text = dtTable.Rows(0).Item("numEmailDays")
    '        txtEmailInci.Text = dtTable.Rows(0).Item("numEmailIncidents")
    '        txtEmailHrs.Text = dtTable.Rows(0).Item("numEmailHours")
    '        If Not ddlEmailTemp.Items.FindByValue(dtTable.Rows(0).Item("numTemplateId")) Is Nothing Then
    '            ddlEmailTemp.SelectedItem.Selected = False
    '            ddlEmailTemp.Items.FindByValue(dtTable.Rows(0).Item("numTemplateId")).Selected = True
    '        End If
    '        BindContactGrid()
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Sub BindContactGrid()
    '    Try
    '        If objContract Is Nothing Then objContract = New CContracts
    '        objContract.UserCntId = Session("UserContactId")
    '        objContract.DomainId = Session("DomainId")
    '        objContract.ContractID = lngCntrId
    '        dgEmployee.DataSource = objContract.GetContactSelected()
    '        dgEmployee.DataBind()
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Sub LoadAssets()
    '    Try
    '        Dim objItems As New CItems
    '        Dim dtItems As DataTable
    '        objItems.DivisionID = ddlCompany.SelectedValue
    '        objItems.DomainID = Session("DomainId")
    '        dtItems = objItems.getCompanyAssets()
    '        uwItem.DataSource = dtItems
    '        uwItem.DataBind()
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Sub LoadTemplates()
    '    Try
    '        Dim objCampaign As New Campaign
    '        Dim dtTable As DataTable
    '        objCampaign.UserCntID = Session("UserContactID")
    '        objCampaign.DomainID = Session("DomainID")
    '        dtTable = objCampaign.GetUserEmailTemplates
    '        ddlEmailTemp.DataSource = dtTable
    '        ddlEmailTemp.DataTextField = "VcDocName"
    '        ddlEmailTemp.DataValueField = "numGenericDocID"
    '        ddlEmailTemp.DataBind()
    '        ddlEmailTemp.Items.Insert(0, "--Select One--")
    '        ddlEmailTemp.Items.FindByText("--Select One--").Value = 0
    '    Catch ex As Exception
    '        Throw ex
    '    End Try

    'End Sub

    'Sub GetAssignedToDdl()
    '    Try
    '        If objCommon Is Nothing Then objCommon = New CCommon

    '        ddlContacts.DataSource = objCommon.ConEmpList(Session("DomainId"), False, 0)
    '        ddlContacts.DataTextField = "vcUserName"
    '        ddlContacts.DataValueField = "numContactID"
    '        ddlContacts.DataBind()
    '        ddlContacts.Items.Insert(0, "--Select One--")
    '        ddlContacts.Items.FindByText("--Select One--").Value = 0
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Public Function FillCustomer()
    '    Try

    '        If objCommon Is Nothing Then objCommon = New CCommon
    '        With objCommon
    '            .DomainID = Session("DomainID")
    '            .Filter = Trim(txtCompName.Text) & "%"
    '            .UserCntID = Session("UserContactID")
    '            ddlCompany.DataSource = objCommon.PopulateOrganization.Tables(0).DefaultView
    '            ddlCompany.DataTextField = "vcCompanyname"
    '            ddlCompany.DataValueField = "numDivisionID"
    '            ddlCompany.DataBind()
    '        End With
    '        ddlCompany.Items.Insert(0, New ListItem("---Select One---", "0"))
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function

    'Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
    '    Try
    '        Save()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
    '    Try
    '        Save()
    '        PageRedirect()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Sub Save()
    '    Try
    '        If objContract Is Nothing Then objContract = New CContracts
    '        With objContract
    '            .ContractID = lngCntrId
    '            .DivisionId = ddlCompany.SelectedItem.Value
    '            .ContractName = txtContractName.Text

    '            .DomainId = Session("DomainID")
    '            .UserCntId = Session("UserContactID")
    '            If chkAmount.Checked = True Then
    '                .Amount = IIf(txtAmount.Text = "", 0, CInt(txtAmount.Text))
    '                .bitAmount = True
    '            Else
    '                .Amount = 0
    '                .bitAmount = False
    '            End If
    '            If (chkDays.Checked = True) Then
    '                .bitDays = True
    '                If calStartDate.SelectedDate <> "" Then objContract.StartDate = calStartDate.SelectedDate
    '                If calExpDate.SelectedDate <> "" Then objContract.ExpDate = calExpDate.SelectedDate
    '            Else : .bitDays = False
    '            End If
    '            If (chkIncidents.Checked = True) Then
    '                .Incidents = IIf(txtInci.Text = "", 0, CInt(txtInci.Text))
    '                .bitInci = True
    '            Else
    '                .Incidents = 0
    '                .bitInci = False
    '            End If
    '            If (chkHrs.Checked = True) Then
    '                .Hours = IIf(txtHrs.Text = "", 0, CInt(txtHrs.Text))
    '                .rate = IIf(txtRateHr.Text = "", 0, txtRateHr.Text)
    '                .bitHrs = True
    '            Else
    '                .Hours = 0
    '                .rate = 0
    '                .bitHrs = False
    '            End If
    '            .Notes = txtNotes.Text
    '            If (chkEmailDays.Checked = True) Then .EmailDays = txtEmailDays.Text
    '            If (chkEmailInci.Checked = True) Then .EmailIncidents = txtEmailInci.Text
    '            If (chkEmailHrs.Checked = True) Then .EmailHours = txtEmailHrs.Text
    '            .bitEDays = chkEmailDays.Checked
    '            .bitEHrs = chkEmailHrs.Checked
    '            .bitEInci = chkEmailInci.Checked
    '            .TemplateId = ddlEmailTemp.SelectedValue
    '        End With
    '        objContract.ModifyContracts()
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Sub PageRedirect()
    '    Try
    '        Response.Redirect("../ContractManagement/frmContractList.aspx")
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub btnDel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDel.Click
    '    Try
    '        If objContract Is Nothing Then objContract = New CContracts
    '        objContract.ContractID = lngCntrId
    '        objContract.DomainId = Session("DomainId")
    '        objContract.DeleteContract()
    '        PageRedirect()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    '    Try
    '        PageRedirect()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
    '    Try
    '        LoadAssets()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub uwItem_ClickCellButton(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.CellEventArgs) Handles uwItem.ClickCellButton
    '    Try
    '        Dim numAItemCode As Integer = CInt(e.Cell.Tag)
    '        Dim objItem As New CItems
    '        objItem.DivisionID = ddlCompany.SelectedValue
    '        objItem.DomainID = Session("DomainId")
    '        objItem.ItemCode = numAItemCode
    '        objItem.DeleteItemFromCmpAsset()
    '        LoadAssets()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub uwItem_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwItem.InitializeRow
    '    Try
    '        If e.Row.HasParent = False Then
    '            e.Row.Cells.FromKey("Action").Tag = e.Row.Cells.FromKey("numAItemCode").Value
    '            e.Row.Cells.FromKey("Action").Value = "r"
    '            e.Row.Cells.FromKey("Action").Column.CellButtonStyle.CssClass = "Delete"
    '            e.Row.Cells.FromKey("Action").Column.Width = Unit.Pixel(20)
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
    '    Try
    '        FillCustomer()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
    '    Try
    '        Save()
    '        If objContract Is Nothing Then objContract = New CContracts
    '        objContract.ContactID = ddlContacts.SelectedValue
    '        objContract.DomainId = Session("DomainId")
    '        objContract.ContractID = lngCntrId
    '        objContract.SaveContractsContacts()
    '        BindContactGrid()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub dgEmployee_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgEmployee.ItemCommand
    '    Try
    '        If e.CommandName = "Delete" Then
    '            If e.Item.Cells(0).Text <> 0 Then
    '                If objContract Is Nothing Then objContract = New CContracts
    '                objContract.ContactID = e.Item.Cells(0).Text
    '                objContract.DomainId = Session("DomainId")
    '                objContract.ContractID = lngCntrId
    '                objContract.DeleteContacts()
    '                BindContactGrid()
    '            End If
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub btnClick_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClick.Click
    '    Try
    '        LoadTemplates()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

End Class