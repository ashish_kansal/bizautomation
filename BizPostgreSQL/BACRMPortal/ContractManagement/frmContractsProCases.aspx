<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmContractsProCases.aspx.vb" Inherits="BACRMPortal.frmContractsProCases" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
            <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
</head>
<body>
    <form id="form1" runat="server">
    <table width="100%">
       
        <tr>
             <td align="left" vAlign="bottom">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;Contracts Linked Items&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
		    </td>
            <td align="right" vAlign="bottom">
                <asp:Button ID="Button1" runat="server" Text="Close" OnClientClick="javascript:self.close()" CssClass="button" />        
            </td>
        </tr>
    </table>
    
    
           
    	        <asp:table id="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" 
			        Runat="server" Width="100%"  BorderColor="black" GridLines="None">
				        <asp:TableRow>
					        <asp:TableCell>
					            <asp:DataGrid runat="server" id="dgItems" Width="100%" AutoGenerateColumns="false" >
        					      <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
							        <ItemStyle CssClass="is"></ItemStyle>
							        <HeaderStyle CssClass="hs"></HeaderStyle>
							         <Columns>
							         <asp:BoundColumn DataField="chrFrom" HeaderText="From"></asp:BoundColumn>							        						     
							         <asp:BoundColumn DataField="Category" HeaderText="Category"></asp:BoundColumn>
							         <asp:BoundColumn DataField="Detail" HeaderText="Detail"></asp:BoundColumn>
							         </Columns>
					            </asp:DataGrid>
					        
					        </asp:TableCell>
		                </asp:TableRow>
		        </asp:table>
	
    </form>
</body>
</html>
