Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Contract
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Marketing

Partial Public Class frmCustContract : Inherits BACRMPage

    Dim lngCntrId As Integer
    
    Dim objCommon As CCommon
    Dim objContract As CContracts

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngCntrId = GetQueryStringVal( "contractId")
            If Not IsPostBack Then
                'To Set Permission
                objCommon = New CCommon
                m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmCustContract.aspx", Session("UserContactID"), 15, 15)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../Common/frmAuthorization.aspx")
                GetAssignedToDdl()
                LoadTemplates()
                BindData()
                If GetQueryStringVal( "contractId") <> "" Then
                    txtAmount.Attributes.Add("onKeyPress", "CheckNumber(2)")
                    txtHrs.Attributes.Add("onKeyPress", "CheckNumber(2)")
                    txtInci.Attributes.Add("onKeyPress", "CheckNumber(2)")
                    txtEmailDays.Attributes.Add("onKeyPress", "CheckNumber(2)")
                    txtEmailInci.Attributes.Add("onKeyPress", "CheckNumber(2)")
                    txtEmailHrs.Attributes.Add("onKeyPress", "CheckNumber(2)")
                    txtRateHr.Attributes.Add("onKeyPress", "CheckNumber(2)")
                    hplProjects.Attributes.Add("onclick", "return openProjects('" & lngCntrId & "')")

                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindData()
        Try
            If objContract Is Nothing Then objContract = New CContracts

            Dim dtTable As DataTable

            objContract.ContractID = lngCntrId
            objContract.UserCntId = Session("UserContactID")
            objContract.DomainId = Session("DomainID")
            dtTable = objContract.GetContractDtl()

            If dtTable.Rows.Count > 0 Then
                lblcompany.Text = IIf(IsDBNull(dtTable.Rows(0).Item("CompanyName")), "", dtTable.Rows(0).Item("CompanyName"))

                txtContractName.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcContractName")), "", dtTable.Rows(0).Item("vcContractName"))
                txtNotes.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcNotes")), "", dtTable.Rows(0).Item("vcNotes"))

                If Not IsDBNull(dtTable.Rows(0).Item("bitAmount")) Then
                    'txtAmount.Text = dtTable.Rows(0).Item("numAmount")
                    chkAmount.Checked = dtTable.Rows(0).Item("bitAmount")
                    If Not IsDBNull(dtTable.Rows(0).Item("numAmount")) And chkAmount.Checked = True Then
                        txtAmount.Text = dtTable.Rows(0).Item("numAmount")
                    End If
                    lblRemAmount.Text = IIf(chkAmount.Checked = True, String.Format("{0:#,##0.00}", CDec(dtTable.Rows(0).Item("RemAmount"))), "")
                Else
                    'lblRemAmount.Text = ""
                    txtAmount.Text = 0
                    chkAmount.Checked = True
                End If
                If Not IsDBNull(dtTable.Rows(0).Item("bitHour")) Then
                    'txtAmount.Text = dtTable.Rows(0).Item("numAmount")
                    chkHrs.Checked = dtTable.Rows(0).Item("bitHour")
                    If Not IsDBNull(dtTable.Rows(0).Item("numhours")) And chkHrs.Checked = True Then
                        txtHrs.Text = dtTable.Rows(0).Item("numhours")
                        txtRateHr.Text = dtTable.Rows(0).Item("decrate")
                    End If
                    lblRemHrs.Text = IIf(chkHrs.Checked = True, String.Format("{0:#,##0.00}", CDec(dtTable.Rows(0).Item("RemHours"))), "")
                Else
                    'lblRemHrs.Text = ""
                    txtHrs.Text = 0
                    chkHrs.Checked = True
                End If
                If Not IsDBNull(dtTable.Rows(0).Item("BalanceAmt")) Then
                    If dtTable.Rows(0).Item("BalanceAmt") > 0 Then
                        lblBalance.Visible = True
                        lblBalanceAmt.Visible = True
                        lblBalanceAmt.Text = String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("BalanceAmt"))
                    Else
                        lblBalance.Visible = False
                        lblBalanceAmt.Visible = False
                    End If
                End If

                If Not IsDBNull(dtTable.Rows(0).Item("bitdays")) Then
                    'txtAmount.Text = dtTable.Rows(0).Item("numAmount")
                    chkDays.Checked = dtTable.Rows(0).Item("bitdays")
                    If dtTable.Rows(0).Item("Days") <> 0 And chkDays.Checked = True Then

                        If IsDBNull(dtTable.Rows(0).Item("bintStartDate")) Then
                            calStartDate.SelectedDate = Now()
                        Else : calStartDate.SelectedDate = DateAdd(DateInterval.Day, 0, CDate(dtTable.Rows(0).Item("bintStartDate")))
                        End If

                        If IsDBNull(dtTable.Rows(0).Item("bintExpDate")) Then
                            calExpDate.SelectedDate = DateAdd(DateInterval.Day, 30, Now())
                        Else : calExpDate.SelectedDate = DateAdd(DateInterval.Day, 0, CDate(dtTable.Rows(0).Item("bintExpDate")))
                        End If
                    End If
                    lblRemDays.Text = IIf(chkDays.Checked = True, String.Format("{0:#,##0.00}", CDec(dtTable.Rows(0).Item("Days"))), "")
                Else
                    'lblRemDays.Text = ""
                    chkDays.Checked = True
                End If

                If Not IsDBNull(dtTable.Rows(0).Item("bitincidents")) Then
                    'txtAmount.Text = dtTable.Rows(0).Item("numAmount")
                    chkIncidents.Checked = dtTable.Rows(0).Item("bitincidents")
                    If Not IsDBNull(dtTable.Rows(0).Item("numIncidents")) And chkIncidents.Checked = True Then
                        txtInci.Text = dtTable.Rows(0).Item("numIncidents")
                    End If
                    lblRemInci.Text = IIf(chkIncidents.Checked = True, String.Format("{0:#,##0.00}", CDec(dtTable.Rows(0).Item("Incidents"))), "")
                Else
                    'lblRemInci.Text = ""
                    txtInci.Text = 0
                    chkIncidents.Checked = True
                End If

            End If
            If dtTable.Rows(0).Item("bitEincidents") Then
                chkEmailInci.Checked = True
            Else : chkEmailInci.Checked = False
            End If
            If dtTable.Rows(0).Item("bitEDays") Then
                chkEmailDays.Checked = True
            Else : chkEmailDays.Checked = False
            End If
            If dtTable.Rows(0).Item("bitEHour") Then
                chkEmailHrs.Checked = True
            Else : chkEmailHrs.Checked = False
            End If
            txtEmailDays.Text = dtTable.Rows(0).Item("numEmailDays")
            txtEmailInci.Text = dtTable.Rows(0).Item("numEmailIncidents")
            txtEmailHrs.Text = dtTable.Rows(0).Item("numEmailHours")
            If Not ddlEmailTemp.Items.FindByValue(dtTable.Rows(0).Item("numTemplateId")) Is Nothing Then
                ddlEmailTemp.SelectedItem.Selected = False
                ddlEmailTemp.Items.FindByValue(dtTable.Rows(0).Item("numTemplateId")).Selected = True
            End If
            BindContactGrid()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindContactGrid()
        Try
            If objContract Is Nothing Then objContract = New CContracts
            objContract.UserCntId = Session("UserContactId")
            objContract.DomainId = Session("DomainId")
            objContract.ContractID = lngCntrId
            dgEmployee.DataSource = objContract.GetContactSelected()
            dgEmployee.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Sub LoadAssets()
    '    Try
    '        Dim objItems As New CItems
    '        Dim dtItems As DataTable
    '        objItems.DivisionID = Session("DivId")
    '        objItems.DomainID = Session("DomainId")
    '        dtItems = objItems.getCompanyAssets()
    '        uwItem.DataSource = dtItems
    '        uwItem.DataBind()
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Sub LoadTemplates()
        Try
            Dim objCampaign As New Campaign
            Dim dtTable As DataTable
            objCampaign.UserCntID = Session("UserContactID")
            objCampaign.DomainID = Session("DomainID")
            dtTable = objCampaign.GetUserEmailTemplates
            ddlEmailTemp.DataSource = dtTable
            ddlEmailTemp.DataTextField = "VcDocName"
            ddlEmailTemp.DataValueField = "numGenericDocID"
            ddlEmailTemp.DataBind()
            ddlEmailTemp.Items.Insert(0, "--Select One--")
            ddlEmailTemp.Items.FindByText("--Select One--").Value = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub GetAssignedToDdl()
        Try
            If objCommon Is Nothing Then objCommon = New CCommon
            ddlContacts.DataSource = objCommon.ConEmpList(Session("DomainId"), False, 0)
            ddlContacts.DataTextField = "vcUserName"
            ddlContacts.DataValueField = "numContactID"
            ddlContacts.DataBind()
            ddlContacts.Items.Insert(0, "--Select One--")
            ddlContacts.Items.FindByText("--Select One--").Value = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub PageRedirect()
        Try
            Response.Redirect("../ContractManagement/frmcustContractList.aspx")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            PageRedirect()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    'Private Sub uwItem_ClickCellButton(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.CellEventArgs) Handles uwItem.ClickCellButton
    '    Try
    '        Dim numAItemCode As Integer = CInt(e.Cell.Tag)
    '        Dim objItem As New CItems
    '        objItem.DivisionID = Session("DivId")
    '        objItem.DomainID = Session("DomainId")
    '        objItem.ItemCode = numAItemCode
    '        objItem.DeleteItemFromCmpAsset()
    '        'LoadAssets()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Private Sub uwItem_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwItem.InitializeRow
    '    Try
    '        If e.Row.HasParent = False Then
    '            e.Row.Cells.FromKey("Action").Tag = e.Row.Cells.FromKey("numAItemCode").Value
    '            e.Row.Cells.FromKey("Action").Value = "r"
    '            e.Row.Cells.FromKey("Action").Column.CellButtonStyle.CssClass = "Delete"
    '            e.Row.Cells.FromKey("Action").Column.Width = Unit.Pixel(20)
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Private Sub dgEmployee_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgEmployee.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                If e.Item.Cells(0).Text <> 0 Then
                    If objContract Is Nothing Then objContract = New CContracts
                    objContract.ContactID = e.Item.Cells(0).Text
                    objContract.DomainId = Session("DomainId")
                    objContract.ContractID = lngCntrId
                    objContract.DeleteContacts()
                    BindContactGrid()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnClick_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClick.Click
        Try
            LoadTemplates()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class