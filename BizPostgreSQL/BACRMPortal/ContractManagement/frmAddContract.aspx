<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAddContract.aspx.vb" Inherits="BACRMPortal.frmAddContract" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" src="../include/calandar.ascx" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
        <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
        	<link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <title>New Contract</title>
    

	<script language="javascript" type="text/javascript" >
	function Close()
	{
	window.close();
	}
	function Save()
	{
	  if ((document.form1.ddlCompany.value=="") || (document.form1.ddlCompany.value==0))
			{
				alert("Select Company")
				document.form1.txtCompName.focus();
				return false;
			}
			if (document.form1.txtContractName.value=="")
			{
			    
				alert("Enter Contract Name")
				document.form1.txtContractName.focus();
				return false;
			}
			
			if(document.form1.chkAmount.checked==true)
			{
			    if (document.form1.txtAmount.value=="")
			    {
				    alert("Enter Amount")
				    document.form1.txtAmount.focus();
				    return false;
			    }
			}
			
			
			if (document.form1.chkDays.checked==true)
			{
			    if(document.form1.calStartDate_txtDate.value=="")
			    {
			        alert("Select the Start Date")
				   document.form1.calStartDate_txtDate.focus();
				    return false;
			    }
			    if(document.form1.calExpDate_txtDate.value=="")
			    {
			        alert("Select the Expiration Date")
				    document.form1.calExpDate_txtDate.focus();
				    return false;
			    }
			    
			}
			if(document.form1.chkIncidents.checked==true)
			{
			    if (document.form1.txtInci.value=="")
			    {
				    alert("Enter No. of Incidents")
				    document.form1.txtInci.focus();
				    return false;
			    }
			}
			if(document.form1.chkHrs.checked==true)
			{
			    if (document.form1.txtHrs.value=="")
			    {
				    alert("Enter No. of Hours")
				    document.form1.txtHrs.focus();
				    return false;
			    }
			}
			
	}
  
  function CheckNumber(cint)
	{
		if (cint==1)
		{
			if (!(window.event.keyCode > 47 && window.event.keyCode < 58 || window.event.keyCode==44 || window.event.keyCode==46))
			{
				window.event.keyCode=0;
			}
		}
		if (cint==2)
		{
			if (!(window.event.keyCode > 47 && window.event.keyCode < 58))
			{
				window.event.keyCode=0;
			}
		}
		
	}
	</script>

</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <table width="100%" border="0">
	    <tr>
			<td valign="bottom">
			<table class="TabStyle">
    			<tr><td>New Contract</td></tr>
			</table>
			</td>
			<td align="right" >
			    
				<asp:Button ID="btnSaveClose" Runat="server" Text="Save &amp; Close" CssClass="button" Width="100"></asp:Button>
				<asp:button id="btnClose" Runat="server" CssClass="button" Text="Close" Width="50"></asp:button>				
			</td>
		</tr>
	</table>
    <asp:updatepanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" 
    UpdateMode="Conditional" EnableViewState="true" >
       <ContentTemplate>
			<asp:table id="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" 
			Runat="server" Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None">
				<asp:TableRow>
					<asp:TableCell>
					    <table width="100%" class="normal1">
						    <tr>
						        <td colspan="4" width="20%">
						        Account<font color="red">*</font>
						        <asp:textbox id="txtCompName" Runat="server" cssclass="signup"/> &nbsp;
						        <asp:Button ID="btnGo" Runat="server" Text="Go" CssClass="button"></asp:Button>&nbsp;
						        <asp:DropDownList ID="ddlCompany" Runat="server" AutoPostBack="True" CssClass="signup" Width="175"></asp:DropDownList>
						        </td>
					        </tr>
							<tr>
								<td>Contract Name<font color="red">*</font><asp:TextBox ID="txtContractName" runat="server" 
								Width="250" CssClass="signup"></asp:TextBox></td>								
							</tr>
							<tr><td>Commitments</td></tr>
						    <tr>
							        <td>
							           <table width="100%" class="normal1">
					                    <tr>
    					                  <td width="20%">
    					                    <asp:CheckBox ID="chkAmount" runat="server" Text="Amount" />
    					                  </td>
    					                  <td>
    					                     <asp:TextBox ID="txtAmount" runat="server" CssClass="signup" Text=""></asp:TextBox>
    					                  </td>
					                    </tr>
					                    <tr>
    					                    <td width="15%">
    					                   <asp:CheckBox ID="chkDays" runat="server" Text="Days" />
    					                    </td>
    					                    <td>
    					                    <table> <tr><td align="left">Starting Date</td><td align="left">
						                              <BizCalendar:Calendar id="calStartDate" runat="server" />
						                         </td>   
						                        <td>Expiration Date </td><td align="left">
						                               <BizCalendar:Calendar id="calExpDate" runat="server" />
						                               </td></tr></table>
    					                    </td>
					                    </tr>
					                    <tr>
    					                    <td width="15%">
    					                   <asp:CheckBox ID="chkIncidents" runat="server" Text="Incidents (Support)" />
    					                    </td>
    					                    <td>
    					                     <asp:TextBox ID="txtInci" CssClass="signup" runat="server" ></asp:TextBox>
    					                    </td>
					                    </tr>
					                    <tr>
    					                    <td width="15%">
    					                   <asp:CheckBox ID="chkHrs" runat="server" Text="Hours" />
    					                    </td>
    					                    <td>
    					                     <table> 
    					                        <tr>
    						                        <td><asp:TextBox ID="txtHrs" CssClass="signup" runat="server" ></asp:TextBox></td>
						                           
						                        </tr>
						                     </table>
    					                    </td>
					                    </tr>
					                     <tr>
    					                    <td width="15%">
    					                       &nbsp;Notes:
    					                    </td>
    					                    <td>
    					                    <asp:TextBox CssClass="signup" ID="txtNotes" runat="server" TextMode="MultiLine" Width="300"></asp:TextBox>
    					                    </td>
					                    </tr>
					                </table>
					        </td>
						    </tr>
						    <tr>			        			        
						        

						 </table>							 
					</asp:TableCell>
				</asp:TableRow>
			</asp:table>
			</ContentTemplate>
			</asp:updatepanel>

    </form>
</body>
</html>
