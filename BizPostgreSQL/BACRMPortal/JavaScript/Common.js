﻿function OpenWebsite(varURL) {
    var url = '';

    if (document.getElementById(varURL) != null) {
        url = document.getElementById(varURL).value;
    }

    if ((url != '') && (url.substr(0, 7) == 'http://') && (url.length > 7)) {
        var LoWindow = window.open(url, "", "");
        LoWindow.focus();
    }
    return false;
}


function OpenEmail(txtMailAddr, a, b) {
    var Email = document.getElementById(txtMailAddr)

    if (document.getElementById(txtMailAddr) != null) {
        if (a == 1) {

            window.open('mailto:' + Email.value);
        }
        else if (a == 2) {
            window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&LsEmail=' + Email.value + '&ContID=' + b, 'mail', 'toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes');
        }

    }
    else {
        if (a == 1) {

            window.open('mailto:' + txtMailAddr);
        }
        else if (a == 2) {
            window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&LsEmail=' + txtMailAddr + '&ContID=' + b, 'mail', 'toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes');
        }
    }
    return false;
}


//Address reload
function FillAddress() {
    window.location.reload(true);
    return false;
}

function formatCurrency(num) {
    num = num.toString().replace(/\$|\,/g, '');
    if (isNaN(num))
        num = "0";
    sign = (num == (num = Math.abs(num)));
    num = Math.floor(num * 100 + 0.50000000001);
    cents = num % 100;
    num = Math.floor(num / 100).toString();
    if (cents < 10)
        cents = "0" + cents;
    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
        num = num.substring(0, num.length - (4 * i + 3)) + ',' +
    num.substring(num.length - (4 * i + 3));
    return (((sign) ? '' : '-') + num + '.' + cents);
}

function currencyToDecimal(num) {
    num = num.toString().replace(/\$|\,/g, '');
    if (isNaN(num))
        num = "0";

    return num;
}

// http://javascript.about.com
var th = ['', 'thousand', 'million', 'billion', 'trillion'];

var dg = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
var tn = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];
var tw = ['twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];

function toWords(s) {
    s = s.toString();
    s = s.replace(/[\, ]/g, '');
    if (s != parseFloat(s)) return 'not a number';
    var x = s.indexOf('.');
    if (x == -1) x = s.length;
    if (x > 15) return 'too big';
    var n = s.split('');
    var str = '';
    var sk = 0;
    for (var i = 0; i < x; i++) {
        if ((x - i) % 3 == 2) {
            if (n[i] == '1') {
                str += tn[Number(n[i + 1])] + ' ';
                i++;
                sk = 1;
            } else if (n[i] != 0) {
                str += tw[n[i] - 2] + ' ';
                sk = 1;
            }
        } else if (n[i] != 0) {
            str += dg[n[i]] + ' ';
            if ((x - i) % 3 == 0) str += 'hundred ';
            sk = 1;
        }
        if ((x - i) % 3 == 1) {
            if (sk) str += th[(x - i - 1) / 3] + ' ';
            sk = 0;
        }
    }
    if (x != s.length) {
        var y = s.length;
        str += ' and ';
        for (var i = x + 1; i < y; i++) str += n[i];

        str += ' / 100';
    }

    str = str.replace(/\s+/g, ' ');
    return pad(100, str.charAt(0).toUpperCase() + str.slice(1) + ' ', '*')
}

function pad(l, o, s) {
    while (o.length < l) {
        o = o + s;
    }
    return o;
}

function CheckIsReconciled() {
    if (document.getElementById('hdnIsReconciled').value == 1) {
        var str;
        str = 'The transaction you are editing has been reconciled. Saving your changes could put you out of balance the next time you try to reconcile. Are you sure you want to modify it?'
        if (confirm(str)) {
            return true;
        }
        else {
            return false;
        }
    }

    return true;
}