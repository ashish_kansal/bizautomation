/*
Purpose:	Closes the present window
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function CloseThisWin()
{
	this.close();
}
/*
Purpose:	The processing required before the form is saved
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function PreSaveProcess(frmForm)
{
	var sTeamlist = '0';
	var sXMLString = '<Teams>'+'\n';
	//Create xml for available fields
	for(var i=0; i<frmForm.lstTeamSelected.options.length; i++)
	{
		sXMLString += '<Team>'+'\n';
		sXMLString += '<numTeamId>' + frmForm.lstTeamSelected.options[i].value + '</numTeamId>'+'\n';
		sXMLString += '</Team>'+'\n';
		sTeamlist += ',' + frmForm.lstTeamSelected.options[i].value;
	}
	sXMLString += '</Teams>'+'\n';
	frmForm.hdXMLString.value = sXMLString;
	sTeamlist = sTeamlist.substring(2,sTeamlist.length);
	opener.document.getElementById("hdTeams").value = sTeamlist;
}
/*
Purpose:	To validate the form data
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) False/ True
*/
function validateFormData(frmForm)
{
	if(frmForm.lstTeamSelected.options.length > 0)
	{
		PreSaveProcess(frmForm);
		return true;
	}
	alert('Please select the teams for filtering search.');
	return false;
}
/*
Purpose:	To move the options upwards from one position in the listbox 
			to another position in the same listbox
Created By: Debasish Tapan Nag
Parameter:	1) tbox: The target list box
Return		1) False
*/
function MoveUp(tbox)
{		
	for(var i=1; i<tbox.options.length; i++)
	{
		if(tbox.options[i].selected && tbox.options[i].value != "") 
		{		
			var SelectedText,SelectedValue,PrevSelectedValue;
			SelectedValue = tbox.options[i].value;
			SelectedText = tbox.options[i].text;
			PrevSelectedValue = tbox.options[i-1].value;
			tbox.options[i].value=tbox.options[i-1].value;
			tbox.options[i].text=tbox.options[i-1].text;
			tbox.options[i-1].value=SelectedValue;
			tbox.options[i-1].text=SelectedText;
			tbox.options[i-1].selected=true;
		}
	}
	return false;
}
/*
Purpose:	To move the options downwards from one position in the listbox 
			to another position in the same listbox
Created By: Debasish Tapan Nag
Parameter:	1) tbox: The target list box
Return		1) False
*/
function MoveDown(tbox)
{
	for(var i=0; i<tbox.options.length-1; i++)
	{
		if(tbox.options[i].selected && tbox.options[i].value != "") 
		{
			var SelectedText,SelectedValue,NextSelectedValue;
			SelectedValue = tbox.options[i].value;
			SelectedText = tbox.options[i].text;
			NextSelectedValue = tbox.options[i+1].value;
			tbox.options[i].value=tbox.options[i+1].value;
			tbox.options[i].text=tbox.options[i+1].text;
			tbox.options[i+1].value=SelectedValue;
			tbox.options[i+1].text=SelectedText;
		}
	}
	return false;
}

sortitems = 0;  // 0-False , 1-True
/*
Purpose:	To move the options from one listbox to another
Created By: Debasish Tapan Nag
Parameter:	1) fbox:The from list box
			2) tbox: The target list box
Return		1) False
*/
function move(fbox,tbox)
{
	fBoxSelectedIndex = -1;
	for(var i=0; i<fbox.options.length; i++)
	{
		if(fbox.options[i].selected && fbox.options[i].value != "") 
		{
			/// to check for duplicates 
			for (var j=0;j<tbox.options.length;j++)
			{
				if (tbox.options[j].value == fbox.options[i].value)	
				{
					alert("Item is already selected");
					return false;
				}
			}
			fBoxSelectedIndex = fbox.options[i].value;
			var no = new Option();
			no.value = fbox.options[i].value;
			no.text = fbox.options[i].text;
			tbox.options[tbox.options.length] = no;
			fbox.options[i].value = "";
			fbox.options[i].text = "";
		}
	}
	BumpUp(fbox);
	if (sortitems) SortD(tbox);
	return false;
	
}
/*
Purpose:	To remove the options from one listbox to another
Created By: Debasish Tapan Nag
Parameter:	1) fbox:The from list box
			2) tbox: The target list box
			3) aoiflag: aoi flag
Return		1) False
*/
function remove(fbox,tbox)
{
	fBoxSelectedIndex = -1;
	for(var i=0; i<fbox.options.length; i++)
	{
		if(fbox.options[i].selected && fbox.options[i].value != "") 
		{
			fBoxSelectedIndex = fbox.options[i].value;
			/// to check for duplicates 
			for (var j=0;j<tbox.options.length;j++)
			{
				if (tbox.options[j].value == fbox.options[i].value)	
				{
					fbox.options[i].value = "";
					fbox.options[i].text = "";
					BumpUp(fbox);
					if (sortitems) SortD(tbox);
					return false;
					
					
					//alert("Item is already selected");
					//return false;
				}
			}
			var no = new Option();
			no.value = fbox.options[i].value;
			if(fbox.options[i].text.substring(0,4) != '(*) ')
				no.text = fbox.options[i].text;
			else
				no.text = fbox.options[i].text.substring(4,fbox.options[i].text.length);
			tbox.options[tbox.options.length] = no;
			fbox.options[i].value = "";
			fbox.options[i].text = "";		
		}
	}
	BumpUp(fbox);
	if (sortitems) SortD(tbox);
	return false;
}
/*
Purpose:	To shift the option upwards after one is removed from the middle
Created By: Debasish Tapan Nag
Parameter:	1) box:The list box under consideration
Return		1) None
*/
function BumpUp(box) 
{
	for(var i=0; i<box.options.length; i++) 
	{
		if(box.options[i].value == "")  
		{
			for(var j=i; j<box.options.length-1; j++)  
			{
				box.options[j].value = box.options[j+1].value;
				box.options[j].text = box.options[j+1].text;
			}
			var ln = i;
			break;
		}
	}
	if(ln < box.options.length) 
	{
		box.options.length -= 1;
		BumpUp(box);
	}
}
/*
Purpose:	To sort the options in the drop down
Created By: Debasish Tapan Nag
Parameter:	1) box:The list box under consideration
Return		1) None
*/
function SortD(box) 
{
	var temp_opts = new Array();
	var temp = new Object();
	for(var i=0; i<box.options.length; i++)  
	{
		temp_opts[i] = box.options[i];
	}
	for(var x=0; x<temp_opts.length-1; x++)  
	{
		for(var y=(x+1); y<temp_opts.length; y++)  
		{
			if(temp_opts[x].text > temp_opts[y].text) 
			{
				temp = temp_opts[x].text;
				temp_opts[x].text = temp_opts[y].text;
				temp_opts[y].text = temp;
				temp = temp_opts[x].value;
				temp_opts[x].value = temp_opts[y].value;
				temp_opts[y].value = temp;
			}
		}
	}
	for(var i=0; i<box.options.length; i++)  
	{
		box.options[i].value = temp_opts[i].value;
		box.options[i].text = temp_opts[i].text;
	}
}