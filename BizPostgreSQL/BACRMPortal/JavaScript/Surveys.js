////////////////////////////////////////////////////////////////////////////
///////////////////////////SURVEY LIST MANAGEMENT///////////////////////////
////////////////////////////////////////////////////////////////////////////
/*
Purpose:	Edits the requested Survey
Created By: Debasish Tapan Nag
Parameter:	1) numSurId: The Survey being requested
Return		1) None
*/
function EditSurvey(numSurId) {
    //document.location.href = 'frmEditSurveyRedirection.aspx?numSurveyId=' + numSurId;
    //frames['IfrOpenSurvey'].document.location.href = 'frmEditSurveyRedirection.aspx?numSurveyId=' + numSurId;
    document.getElementById('IfrOpenSurvey').src = 'frmEditSurveyRedirection.aspx?numSurveyId=' + numSurId;
    //document.frames['IfrOpenSurvey'].location.href = 'frmEditSurveyRedirection.aspx?numSurveyId=' + numSurId;

}
/*
Purpose:	Edits the requested Survey questions
d By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function ShowSurveyQuestions() {
    var numSurId = document.getElementById('hdSurId').value;
    document.getElementById('ifSurveyQuestions').src = 'frmCampaignSurveyQuestions.aspx?numSurId=' + numSurId;
}

function ShowSurveyCreateRecord() {
    var numSurId = document.getElementById('hdSurId').value;
    var cbCreateRecord = document.getElementById('cbCreateRecord')
    if (cbCreateRecord.checked)
        window.open('frmCampaignSurveyCreateRecord.aspx?numSurId=' + numSurId + '', 'SurveyCreateRecord', 'toolbar=no,titlebar=no,left=100, top=100,width=700,height=600,scrollbars=yes,resizable=yes');
    return false;
}
/*
Purpose:	If Skip Registration Is YES; uncheck & disable Post Registration
Created By: Debasish Tapan Nag
Parameter:	1) thisCb: The Skip Registration Checkbox Status
Return		1) None
*/
function ReassurePostRegistration(thisCb) {
    if (thisCb.checked) {
        document.getElementById('cbPostRegistration').checked = 0;
        document.getElementById('cbPostRegistration').disabled = 1;

        document.getElementById('cbNewRelationship').checked = 0;
        document.getElementById('cbNewRelationship').disabled = 1;

        document.getElementById('cbRedirect').checked = 0;
        document.getElementById('cbRedirect').disabled = 1;

        document.getElementById('cbEmail').checked = 0;
        document.getElementById('cbEmail').disabled = 1;

        document.getElementById('cbRecordOwner').checked = 0;
        document.getElementById('cbRecordOwner').disabled = 1;

        document.getElementById('ddlRelationShip').disabled = 1;
        document.getElementById('ddlCRMType').disabled = 1;
        document.getElementById('ddlGroup').disabled = 1;
        document.getElementById('ddlRecordOwner').disabled = 1;
        document.getElementById('ddlProfile').disabled = 1;

        document.getElementById('txtSurveyRedirect').disabled = 1;
        document.getElementById('ddlEmailTemplate1').disabled = 1;
        document.getElementById('ddlEmailTemplate2').disabled = 1;

        //        document.getElementById('txtLandingPageYes').disabled = 1;
        //        document.getElementById('txtLandingPageNo').disabled = 1;

        document.getElementById('cbLandingPage').checked = 0;
        document.getElementById('cbLandingPage').disabled = 1;

        document.getElementById('cbCreateRecord').checked = 0;
        document.getElementById('cbCreateRecord').disabled = 1;

    } else {
        document.getElementById('cbPostRegistration').disabled = 0;
        document.getElementById('cbNewRelationship').disabled = 0;
        document.getElementById('cbRedirect').disabled = 0;
        document.getElementById('cbEmail').disabled = 0;
        document.getElementById('cbRecordOwner').disabled = 0;

        document.getElementById('ddlRelationShip').disabled = 0;

        document.getElementById('ddlCRMType').disabled = 0;
        document.getElementById('ddlGroup').disabled = 0;
        document.getElementById('ddlRecordOwner').disabled = 0;
        document.getElementById('ddlProfile').disabled = 0;
        
        document.getElementById('txtSurveyRedirect').disabled = 0;
        document.getElementById('ddlEmailTemplate1').disabled = 0;
        document.getElementById('ddlEmailTemplate2').disabled = 0;

        document.getElementById('cbLandingPage').disabled = 0;
        document.getElementById('cbCreateRecord').disabled = 0;
        //        document.getElementById('txtLandingPageYes').disabled = 0;
        //        document.getElementById('txtLandingPageNo').disabled = 0;

    }
}

function CreateRecord() {
    if (document.getElementById('cbNewRelationship').checked) {
        document.getElementById('cbCreateRecord').checked = 0;
        document.getElementById('cbCreateRecord').disabled = 1;

        document.getElementById('cbNewRelationship').disabled = 0;

        document.getElementById('ddlRelationShip').disabled = 0;
        document.getElementById('ddlCRMType').disabled = 0;
        document.getElementById('ddlGroup').disabled = 0;
        document.getElementById('ddlRecordOwner').disabled = 0;
    }
    else if (document.getElementById('cbCreateRecord').checked) {
        document.getElementById('cbNewRelationship').checked = 0;
        document.getElementById('cbNewRelationship').disabled = 1;

        document.getElementById('cbCreateRecord').disabled = 0;

        document.getElementById('ddlRelationShip').disabled = 1;
        document.getElementById('ddlCRMType').disabled = 1;
        document.getElementById('ddlGroup').disabled = 1;
        document.getElementById('ddlRecordOwner').disabled = 1;
    }
    else {
        document.getElementById('cbCreateRecord').disabled = 0;
        document.getElementById('cbNewRelationship').disabled = 0;

        document.getElementById('ddlRelationShip').disabled = 0;
        document.getElementById('ddlCRMType').disabled = 0;
        document.getElementById('ddlGroup').disabled = 0;
        document.getElementById('ddlRecordOwner').disabled = 0;
    }
}

function LandingPage(thisCb) {
    if (thisCb.checked) {

        document.getElementById('txtLandingPageYes').disabled = 0;
        document.getElementById('txtLandingPageNo').disabled = 0;

    } else {
        document.getElementById('txtLandingPageYes').disabled = 1;
        document.getElementById('txtLandingPageNo').disabled = 1;

    }
}
/*
Purpose:	Calls to save the survey along with the Questions and Answers and the Rules
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function RequestSurveySave(sActionRequested) {
    Page_ClientValidate(); 						//explcitly call for validation
    if (Page_IsValid) {
        GetFrameDoc('ifSurveyQuestions').getElementById('hdAction').value = sActionRequested;
        GetFrameDoc('ifSurveyQuestions').frmCampaignSurveyQuestions.btnSaveQuestion.click();
    }
    return false;
}
/*
Purpose:	Calls to display the Q & A Averages Report
Created By: Debasish Tapan Nag
Parameter:	1) numSurId: The Survey id whose Report is reqeusted
Return		1) None
*/
function EditSurveyQAAgerages(numSurId) {
    document.location.href = 'frmCampaignSurveyReportQAAvg.aspx?numSurId=' + numSurId;
}

function SurveyCss(Type) {
    document.location.href = 'frmSurveyCssList.aspx?Type=' + Type;
}
/*
Purpose:	Calls to display the Q & A Averages Report
Created By: Debasish Tapan Nag
Parameter:	1) numSurId: The Survey id whose Report is reqeusted
Return		1) None
*/
function EditSurveyRespondents(numSurId, bitSkipRegistration) {
    if (bitSkipRegistration == 0) {
        document.location.href = 'frmCampaignSurveyRespondents.aspx?numSurId=' + numSurId;
    } else {
        alert("Registration was skipped for this Survey so the respondents are not registered.");
    }

}
/*
Purpose:	Re-confirm user action to delete respondent
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function DeleteRespondentRecord() {
    return confirm('This response will be deleted.');
}
////////////////////////////////////////////////////////////////////////////
///////////////////////////SURVEY EDIT MANAGEMENT///////////////////////////
////////////////////////////////////////////////////////////////////////////
/*
Purpose:	Requests to clone the survey
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function RequestSurveyClone() {
    document.getElementById('txtSurveyName').value += ' (Clone)';
    return true;
}
/*
Purpose:	Requests to open the Survey in a new window
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function RequestToViewSurveyQuestionnaire(vcScreenPath, boolOnlyRegistration) {
    numSurId = document.getElementById('hdSurId').value;
    window.open(vcScreenPath + '&numSurId=' + numSurId + '&boolOnlyRegistration=' + boolOnlyRegistration)
    return false;
}
/*
Purpose:	Requests to open the Survey in a new window
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function RequestToViewSurveyQuestionnaireWithPostReg(vcScreenPath) {
    window.open(vcScreenPath + '&numRespondantID=0&bitPostRegistration=1')
    return false;
}
/*
Purpose:	Requests to open the Survey in a new window
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function RequestToViewSurveyRegistrationScreen(vcScreenPath, boolOnlyRegistration) {
    numSurId = document.getElementById('hdSurId').value;
    window.open(vcScreenPath + '&numSurId=' + numSurId + '&boolOnlyRegistration=' + boolOnlyRegistration)
    return false;
}
////////////////////////////////////////////////////////////////////////////
///////////////////////////SURVEY QUESTION MANAGEMENT///////////////////////
////////////////////////////////////////////////////////////////////////////
/*
Purpose:	Checks the maximum number of charcters that is entered in the textbox
Created By: Debasish Tapan Nag
Parameter:	1) sTextControl: the text box name
2) iLength: the max length allowed
Return		1) None
*/
function CheckTextLength(sTextControl, iLength) {
    var maxlength = new Number(iLength) // Change number to your max length.
    if (sTextControl.value.length > maxlength) {
        sTextControl.value = sTextControl.value.substring(0, maxlength);
        alert("Please restrict the question to " + iLength + " chars");
    }
}

/*
Purpose:	Opens the Survey Questionnaire in a new popup window
Created By: Debasish Tapan Nag
Parameter:	1) numSurId: the survey id
Return		1) None
*/
function openSurveyQuestionnaire(numSurId) {
}

/*
Purpose:	Opens the Survey answers in a new popup window
Created By: Debasish Tapan Nag
Parameter:	1) numQuestionId: the question id
Return		1) None
*/
function AddEditAnswer(numQuestionId, vcAction) {
    numSurId = document.getElementById('hdSurId').value;
    if (vcAction == 'Edit') {
        hnadw = window.open('frmCampaignSurveyAnswers.aspx?numQuestionId=' + numQuestionId + '&numSurId=' + numSurId, 'SurveyAnswers', 'toolbar=no,titlebar=no,left=100, top=100,width=700,height=600,scrollbars=yes,resizable=yes')
        hnadw.focus();
    } else {
        document.getElementById('hdAction').value = 'SaveB4AddingAnswers';
        document.getElementById('btnSaveQuestion').click();
    }
    return false;
}

/*
Purpose:	Initiates deletion of a question
Created By: Debasish Tapan Nag
Parameter:	1) numQuestionId: the question id
Return		1) None
*/
function DeleteQuestion(numQuestionId, vcAction) {
    document.getElementById('hdQIDDelete').value = numQuestionId;
    document.getElementById('txtQuestion' + numQuestionId).value = '$$@@$$RequestDeletion$$@@$$';
    document.getElementById('tRow' + numQuestionId).style.display = 'none';
    if (!document.getElementById('txtQuestion' + parseInt(numQuestionId + 1))) {
        document.getElementById('hdQuesNewRow').value = 'False';
    }
    document.getElementById('btnDeleteQuestion').click();
    return false;
}
/*
Purpose:	Generates a click event on the corresponding button in the iframe
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) false
*/
function RequestSurveyQuestionnaireSave() {
    GetFrameDoc('ifSurveyQuestions').form1.btnSaveQuestion.click();
    return false;
}
/*
Purpose:	Generates a click event on the corresponding button in the iframe
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) false
*/
function RequestSurveyQuestionnaireNew() {
    GetFrameDoc('ifSurveyQuestions').getElementById('btnAddNewQuestion').click();
    return false;
}
/*
Purpose:	Generates a click event on the corresponding button in the iframe
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) false
*/
function AuroSaveQuestionsBeforeAddingAnswers() {
    document.getElementById('hdAction').value = 'Save';
    SetActionOfPage();
}
/*
Purpose:	Generates a click event on the corresponding button in the iframe
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) false
*/
function SetActionOfPage() {
    if (document.getElementById('hdAction').value != 'SaveB4AddingAnswers' && document.getElementById('hdAction').value != 'SaveB4SavingSurvey' && document.getElementById('hdAction').value != 'SaveB4SavingAndClosingSurvey')
        document.getElementById('hdAction').value = "Save";
}
/*
Purpose:	Reverse initiates the saveing of the entire survey by a click on a hidden save button on the parent frame
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) false
*/
function InitiateSavingSurvey() {
    parent.document.form1.btnSaveSurveyActualButHidden.click();
    return false;
}

/*
Purpose:	Reverse initiates the saveing of the entire survey (before closing the survey) by a click 
on a hidden save button on the parent frame
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) false
*/
function InitiateSavingSurveyBeforeClose() {
    parent.document.form1.btnSaveAndCloseActualButHidden.click();
    return false;
}
////////////////////////////////////////////////////////////////////////////
/////////////////////////////SURVEY ANSWER MANAGEMENT///////////////////////
////////////////////////////////////////////////////////////////////////////
/*
Purpose:	Closes the present window
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function CloseAnswerWin() {
    this.close();
    return false;
}
/*
Purpose:	Sets the flag which indicates a save
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function SaveAnswers() {
    if (document.getElementById('hdAction').value != 'SaveB4AddingAnswers')
        document.getElementById('hdAction').value = "Save";
}

/*
Purpose:	Edits the Rule Screen
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function AddEditAnswerRule(numAnsID, vcAction, numMatrix) {

    if (vcAction == 'Edit') {
        numSurId = document.getElementById('hdSurId').value;
        numQuestionId = document.getElementById('hdQId').value;
        document.location.href = 'frmCampaignSurveyWorflowRuleEditor.aspx?numAnsID=' + numAnsID + '&numQuestionId=' + numQuestionId + '&numSurId=' + numSurId + '&numMatrix=' + numMatrix;
    } else {
        document.getElementById('hdAction').value = 'SaveB4AddingAnswers';
        document.getElementById('hdAnsId').value = numAnsID;
        document.getElementById('btnSaveAnswer').click();
    }
    return false;
}

/*
Purpose:	To move the options upwards from one position in the listbox 
to another position in the same listbox
Created By: Debasish Tapan Nag
Parameter:	1) tbox: The target list box
Return		1) False
*/
function MoveUp(tbox) {
    for (var i = 1; i < tbox.options.length; i++) {
        if (tbox.options[i].selected && tbox.options[i].value != "") {
            var SelectedText, SelectedValue, PrevSelectedValue;
            SelectedValue = tbox.options[i].value;
            SelectedText = tbox.options[i].text;
            PrevSelectedValue = tbox.options[i - 1].value;
            tbox.options[i].value = tbox.options[i - 1].value;
            tbox.options[i].text = tbox.options[i - 1].text;
            tbox.options[i - 1].value = SelectedValue;
            tbox.options[i - 1].text = SelectedText;
            tbox.options[i - 1].selected = true;
        }
    }
    return false;
}
/*
Purpose:	To move the options downwards from one position in the listbox 
to another position in the same listbox
Created By: Debasish Tapan Nag
Parameter:	1) tbox: The target list box
Return		1) False
*/
function MoveDown(tbox) {
    for (var i = 0; i < tbox.options.length - 1; i++) {
        if (tbox.options[i].selected && tbox.options[i].value != "") {
            var SelectedText, SelectedValue, NextSelectedValue;
            SelectedValue = tbox.options[i].value;
            SelectedText = tbox.options[i].text;
            NextSelectedValue = tbox.options[i + 1].value;
            tbox.options[i].value = tbox.options[i + 1].value;
            tbox.options[i].text = tbox.options[i + 1].text;
            tbox.options[i + 1].value = SelectedValue;
            tbox.options[i + 1].text = SelectedText;
        }
    }
    return false;
}

sortitems = 0;  // 0-False , 1-True
/*
Purpose:	To move the options from one listbox to another
Created By: Debasish Tapan Nag
Parameter:	1) fbox:The from list box
2) tbox: The target list box
Return		1) False
*/
function move(fbox, tbox) {
    fBoxSelectedIndex = -1;
    for (var i = 0; i < fbox.options.length; i++) {
        if (fbox.options[i].selected && fbox.options[i].value != "") {
            /// to check for duplicates 
            for (var j = 0; j < tbox.options.length; j++) {
                if (tbox.options[j].value == fbox.options[i].value) {
                    alert("Item is already selected");
                    return false;
                }
            }
            fBoxSelectedIndex = fbox.options[i].value;
            var no = new Option();
            no.value = fbox.options[i].value;
            no.text = fbox.options[i].text;
            tbox.options[tbox.options.length] = no;
            fbox.options[i].value = "";
            fbox.options[i].text = "";
        }
    }
    BumpUp(fbox);
    if (sortitems) SortD(tbox);
    return false;

}
/*
Purpose:	To remove the options from one listbox to another
Created By: Debasish Tapan Nag
Parameter:	1) fbox:The from list box
2) tbox: The target list box
3) aoiflag: aoi flag
Return		1) False
*/
function remove1(fbox, tbox) {
    fBoxSelectedIndex = -1;
    for (var i = 0; i < fbox.options.length; i++) {
        if (fbox.options[i].selected && fbox.options[i].value != "") {
            fBoxSelectedIndex = fbox.options[i].value;
            /// to check for duplicates 
            for (var j = 0; j < tbox.options.length; j++) {
                if (tbox.options[j].value == fbox.options[i].value) {
                    fbox.options[i].value = "";
                    fbox.options[i].text = "";
                    BumpUp(fbox);
                    if (sortitems) SortD(tbox);
                    return false;


                    //alert("Item is already selected");
                    //return false;
                }
            }
            var no = new Option();
            no.value = fbox.options[i].value;
            if (fbox.options[i].text.substring(0, 4) != '(*) ')
                no.text = fbox.options[i].text;
            else
                no.text = fbox.options[i].text.substring(4, fbox.options[i].text.length);
            tbox.options[tbox.options.length] = no;
            fbox.options[i].value = "";
            fbox.options[i].text = "";
        }
    }
    BumpUp(fbox);
    if (sortitems) SortD(tbox);
    return false;
}
/*
Purpose:	To shift the option upwards after one is removed from the middle
Created By: Debasish Tapan Nag
Parameter:	1) box:The list box under consideration
Return		1) None
*/
function BumpUp(box) {
    for (var i = 0; i < box.options.length; i++) {
        if (box.options[i].value == "") {
            for (var j = i; j < box.options.length - 1; j++) {
                box.options[j].value = box.options[j + 1].value;
                box.options[j].text = box.options[j + 1].text;
            }
            var ln = i;
            break;
        }
    }
    if (ln < box.options.length) {
        box.options.length -= 1;
        BumpUp(box);
    }
}
/*
Purpose:	To sort the options in the drop down
Created By: Debasish Tapan Nag
Parameter:	1) box:The list box under consideration
Return		1) None
*/
function SortD(box) {
    var temp_opts = new Array();
    var temp = new Object();
    for (var i = 0; i < box.options.length; i++) {
        temp_opts[i] = box.options[i];
    }
    for (var x = 0; x < temp_opts.length - 1; x++) {
        for (var y = (x + 1); y < temp_opts.length; y++) {
            if (temp_opts[x].text > temp_opts[y].text) {
                temp = temp_opts[x].text;
                temp_opts[x].text = temp_opts[y].text;
                temp_opts[y].text = temp;
                temp = temp_opts[x].value;
                temp_opts[x].value = temp_opts[y].value;
                temp_opts[y].value = temp;
            }
        }
    }
    for (var i = 0; i < box.options.length; i++) {
        box.options[i].value = temp_opts[i].value;
        box.options[i].text = temp_opts[i].text;
    }
}
/*
Purpose:	To transfer the AOIs from the drop down to the edit boxes
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function makeAOITransfer() {
    var fbox = document.getElementById('ddlSelectedAOIs');
    var txtBoxIndex = 1;
    if (fbox.options.length == document.getElementById('ddlNumberOfResponse').value) {
        for (var i = 0; i < fbox.options.length; i++) {
            if (fbox.options[i].value != "") {
                document.getElementById('txtAnswer' + txtBoxIndex).value = fbox.options[i].text;
            }
            txtBoxIndex += 1;
        }
        document.getElementById('divAOISelector').style.visibility = 'hidden';
        document.getElementById('btnAOIs').disabled = false;
    } else {
        alert('You must select ' + document.getElementById('ddlNumberOfResponse').value + ' responses.')
    }
}
/*
Purpose:	displays the div and asslows sellection of AOIs
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function AllowSelectionOfAOIs() {
    document.getElementById('divAOISelector').style.visibility = 'visible';
    document.getElementById('btnAOIs').disabled = true;
    return false;
}
/*
Purpose:	Hide the interface for selection of AOIs
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function cancelAOI() {
    document.getElementById('divAOISelector').style.visibility = 'hidden';
    document.getElementById('btnAOIs').disabled = false;
}
////////////////////////////////////////////////////////////////////////////
/////////////////////////SURVEY ANSWER WORKFLOW MANAGEMENT//////////////////
////////////////////////////////////////////////////////////////////////////
/*
Purpose:	Redirects to the Answer Window
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function CloseWorkFlowRulesWin() {
    numQuestionId = document.getElementById('hdQId').value;
    numSurId = document.getElementById('hdSurId').value;
    document.location.href = 'frmCampaignSurveyAnswers.aspx?numQuestionId=' + numQuestionId + '&numSurId=' + numSurId;
    return false;
}

/*
Purpose:	Redirects to the Answer Window
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function ValidateWorkFlowRulesEditor() {
    if (document.getElementById('cbActivationRuleOne').checked) {
        if (trim(document.getElementById('txtAskQuestions').value) == '' || trim(document.getElementById('txtLinkedSurveyID').value) == '') {
            alert('Workflow Rule one has not been correctly set.')
            return false;
        }
        if (!IsNumeric(document.getElementById('txtLinkedSurveyID').value)) {
            alert('Workflow Rule one has not been correctly set.')
            return false;
        }
        if (document.getElementById('txtLinkedSurveyID').value == document.getElementById('hdSurID').value) {
            alert('Cannot have circular reference to the same survey. Please re-enter the Survey ID.');
            document.getElementById('txtLinkedSurveyID').focus();
            document.getElementById('txtLinkedSurveyID').select();
            return false;
        }
    }
    if (document.getElementById('cbActivationRuleTwo').checked) {
        if (trim(document.getElementById('txtSkipQuestions').value) == '') {
            alert('Workflow Rule two has not been correctly set.')
            return false;
        }
    }
    if (document.getElementById('cbActivationRuleSix').checked) {
        if (trim(document.getElementById('txtOpenURL').value) == '') {
            alert('Workflow Rule Six has not been correctly set.')
            return false;
        }
    }
    if (document.getElementById('cbActivationRuleSeven').checked) {
        if (trim(document.getElementById('txtAddSurveyRating').value) == '') {
            alert('Workflow Rule Seven has not been correctly set.')
            return false;
        } else if (!IsNumeric(document.getElementById('txtAddSurveyRating').value)) {
            alert('Workflow Rule Seven has not been correctly set.')
            return false;
        }
    }
    var sEventOrder = '0';
    var ddlTrailer;
    var arrContiniousOrderOfEvents = new Array(0, 0, 0, 0, 0);
    for (var iEventOrder = 1; iEventOrder <= 7; iEventOrder++) {
        if (iEventOrder == 1)
            ddlTrailer = 'One';
        else if (iEventOrder == 2)
            ddlTrailer = 'Two';
        else if (iEventOrder == 3)
            ddlTrailer = 'Three';
        else if (iEventOrder == 4)
            ddlTrailer = 'Four';
        else if (iEventOrder == 5)
            ddlTrailer = 'Five';
        else if (iEventOrder == 6)
            ddlTrailer = 'Six';
        else if (iEventOrder == 7)
            ddlTrailer = 'Seven';
        if (document.getElementById('cbActivationRule' + ddlTrailer).checked) {
            if (sEventOrder.indexOf(',' + document.getElementById('ddlOrderOfEvents' + ddlTrailer).options[document.getElementById('ddlOrderOfEvents' + ddlTrailer).selectedIndex].value) != -1) {
                alert('Order of events for Rule ' + iEventOrder + ' is repeating. Please correct.')
                return false;
            }
            sEventOrder += ',' + document.getElementById('ddlOrderOfEvents' + ddlTrailer).options[document.getElementById('ddlOrderOfEvents' + ddlTrailer).selectedIndex].value;
            arrContiniousOrderOfEvents[document.getElementById('ddlOrderOfEvents' + ddlTrailer).options[document.getElementById('ddlOrderOfEvents' + ddlTrailer).selectedIndex].value - 1] = document.getElementById('ddlOrderOfEvents' + ddlTrailer).options[document.getElementById('ddlOrderOfEvents' + ddlTrailer).selectedIndex].value;
        }
    }
    var boolContinuityConditionStart = false;
    for (var iContiniousOrderOfEvents = arrContiniousOrderOfEvents.length - 1; iContiniousOrderOfEvents >= 0; iContiniousOrderOfEvents--) {
        if (arrContiniousOrderOfEvents[iContiniousOrderOfEvents] != 0 && !boolContinuityConditionStart)
            boolContinuityConditionStart = true;

        if (boolContinuityConditionStart)
            if (arrContiniousOrderOfEvents[iContiniousOrderOfEvents] == 0) {
                alert('Order of events for execution of the workflow rules is incorrect.');
                return false;
            }
    }
    return true;
}
/*
Purpose:	Highlight a Rule Row
Created By: Debasish Tapan Nag
Parameter:	1) trName: Name of the TR to be highlighted
Return		1) None
*/
function HighLightRule(thisElement, trName) {
    if (thisElement.checked)
        document.getElementById(trName).className = 'tr1';
    else
        document.getElementById(trName).className = 'normal1';
}
/*
Purpose:	Applies Rules to the interface
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function ConfirmRuleApplicability() {
    if (opener.parent.document.getElementById('cbSinglePageSurvey').checked) {
        document.getElementById('cbActivationRuleOne').checked = false;
        document.getElementById('cbActivationRuleTwo').checked = false;
        document.getElementById('cbActivationRuleOne').disabled = true;
        document.getElementById('cbActivationRuleTwo').disabled = true;
    }
    //Rule #7 cannot be checked unless Rule#3 is checked
    document.getElementById('cbActivationRuleSeven').disabled = !document.getElementById('cbActivationRuleThree').checked;
    if (document.getElementById('cbActivationRuleThree').disabled || !document.getElementById('cbActivationRuleThree').checked)
        document.getElementById('cbActivationRuleSeven').checked = false;

    for (var iEventOrder = 1; iEventOrder <= 5; iEventOrder++) {
        if (iEventOrder == 1)
            ddlTrailer = 'One';
        else if (iEventOrder == 2)
            ddlTrailer = 'Two';
        else if (iEventOrder == 3)
            ddlTrailer = 'Three';
        else if (iEventOrder == 4)
            ddlTrailer = 'Four';
        else if (iEventOrder == 5)
            ddlTrailer = 'Five';
        else if (iEventOrder == 6)
            ddlTrailer = 'Six';
        else if (iEventOrder == 7)
            ddlTrailer = 'Seven';
        HighLightRule(document.getElementById('cbActivationRule' + ddlTrailer), 'trActivationRule' + ddlTrailer);
    }
}
////////////////////////////////////////////////////////////////////////////
//////////////////////////////SURVEY Q & A AVERAGES ////////////////////////
////////////////////////////////////////////////////////////////////////////



/*
Purpose:	Opens the Q & A Averages in a new screen
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function ExportToExcel(numSurId, sGraphType) {
    var sURL;
    var iWinHeight;
    var iWinWidth;
    sURL = 'frmCampaignSurveyReportQAAvgExcel.aspx?ExportAction=True&PrintAction=False&numSurId=' + numSurId + '&sGraphType=' + sGraphType;
    iWinHeight = 600;
    iWinWidth = 750;
    window.open(sURL, '', 'toolbar=no,titlebar=no,left=10, top=30,width=' + iWinWidth + ',height=' + iWinHeight + ',scrollbars=no,resizable=yes')
    return false;
}
/*
Purpose:	Opens the interface to start printing
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function GoForPrint() {
    var sURL;
    var iWinHeight;
    var iWinWidth;
    sURL = 'frmCampaignSurveyReportQAAvgExcel.aspx?ExportAction=False&PrintAction=True';
    iWinHeight = 10;
    iWinWidth = 10;
    window.open(sURL, '', 'toolbar=no,titlebar=no,left=10, top=30,width=' + iWinWidth + ',height=' + iWinHeight + ',scrollbars=no,resizable=yes')
    return false;
}
/*
Purpose:	Initiate Print
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function InitiatePrint() {
    window.print();
    window.blur();
    opener.focus();
    return false;
}
/*
Purpose:	Lead to Organization Details Page
Created By: Debasish Tapan Nag
Parameter:	1) numContactId: The Contact Id
Return		1) None
*/
function GoOrgDetails(numContactId) {
    //    frames['IfrOpenOrgContact'].document.getElementById('hdRedirectionEntity').value = 'Organization';
    //    frames['IfrOpenOrgContact'].document.getElementById('hdContactId').value = numContactId;
    //    frames['IfrOpenOrgContact'].document.forms['frmOrgContactRedirect'].submit();

    window.open("../Marketing/frmOrgContactRedirect.aspx?RE=Organization&CI=" + numContactId, '', 'toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes');
    //return false;
}
/*
Purpose:	Lead to Contact Details Page
Created By: Debasish Tapan Nag
Parameter:	1) numContactId: The Contact Id
Return		1) None
*/
function GoContactDetails(numContactId) {
    //    frames['IfrOpenOrgContact'].document.getElementById('hdRedirectionEntity').value = 'Contact';
    //    frames['IfrOpenOrgContact'].document.getElementById('hdContactId').value = numContactId;
    //    frames['IfrOpenOrgContact'].document.forms['frmOrgContactRedirect'].submit();

    window.open("../Marketing/frmOrgContactRedirect.aspx?RE=Contact&CI=" + numContactId, '', 'toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes');
    //return false;
}
function move(fbox, tbox, colno, aoiflag) {
    fBoxSelectedIndex = -1;
    for (var i = 0; i < fbox.options.length; i++) {
        if (fbox.options[i].selected && fbox.options[i].value != "") {
            /// to check for duplicates 
            for (var j = 0; j < tbox.options.length; j++) {
                if (tbox.options[j].value == fbox.options[i].value) {
                    alert("Item is already selected");
                    return false;
                }
            }
            fBoxSelectedIndex = fbox.options[i].value;
            var no = new Option();
            no.value = fbox.options[i].value;
            no.text = fbox.options[i].text;
            tbox.options[tbox.options.length] = no;
            fbox.options[i].value = "";
            fbox.options[i].text = "";

            arrFieldConfig[arrFieldConfig.length] = fbox.options[i].value;
        }
    }
    BumpUp(fbox, aoiflag);
    return false;
}
/*
Purpose:	To shift the option upwards after one is removed from the middle
Created By: Debasish Tapan Nag
Parameter:	1) box:The list box under consideration
Return		1) None
*/
function BumpUp(box, aoiflag) {
    for (var i = 0; i < box.options.length; i++) {
        if (box.options[i].value == "") {
            for (var j = i; j < box.options.length - 1; j++) {
                PrevSelectedValue = box.options[j].value;
                SelectedValue = box.options[j + 1].value;
                box.options[j].value = box.options[j + 1].value;
                box.options[j].text = box.options[j + 1].text;

            }
            var ln = i;
            break;
        }
    }
    if (ln < box.options.length) {
        box.options.length -= 1;
        BumpUp(box, aoiflag);
        if (sortitems) SortD(tbox);
        return false;
    }
}
/*
Purpose:	To remove the options from one listbox to another
Created By: Debasish Tapan Nag
Parameter:	1) fbox:The from list box
2) tbox: The target list box
3) aoiflag: aoi flag
Return		1) False
*/
function remove1(fbox, tbox, aoiflag) {
    var fboxSelectedElementName;
    fBoxSelectedIndex = -1;
    for (var i = 0; i < fbox.options.length; i++) {
        if (fbox.options[i].selected && fbox.options[i].value != "") {
            fBoxSelectedIndex = fbox.options[i].value;
            //fboxSelectedElementName = getFieldName(fBoxSelectedIndex, aoiflag)
            //if(!boolReferencesAreClear(fboxSelectedElementName))
            //{			
            //calls function to swap the array
            //swapColumnPosition(fBoxSelectedIndex, 0, 0, aoiflag);

            /// to check for duplicates 
            for (var j = 0; j < tbox.options.length; j++) {
                if (tbox.options[j].value == fbox.options[i].value) {
                    fbox.options[i].value = "";
                    fbox.options[i].text = "";
                    BumpUp(fbox, aoiflag);
                    if (sortitems) SortD(tbox);
                    return false;


                    //alert("Item is already selected");
                    //return false;
                }
            }
            var no = new Option();
            no.value = fbox.options[i].value;
            if (fbox.options[i].text.substring(0, 4) != '(*) ')
                no.text = fbox.options[i].text;
            else
                no.text = fbox.options[i].text.substring(4, fbox.options[i].text.length);
            tbox.options[tbox.options.length] = no;
            fbox.options[i].value = "";
            fbox.options[i].text = "";
            //}else{
            //alert('The selected field is referenced in Auto Rules and cannot be removed unless the Rule is changed.');
            //return false;
            //}	
        }
    }
    BumpUp(fbox, aoiflag);
    if (sortitems) SortD(fbox);
    return false;
}
/*
Purpose:	To sort the options in the drop down
Created By: Debasish Tapan Nag
Parameter:	1) box:The list box under consideration
Return		1) None
*/
function SortD(box) {
    var temp_opts = new Array();
    var temp = new Object();
    for (var i = 0; i < box.options.length; i++) {
        temp_opts[i] = box.options[i];
    }
    for (var x = 0; x < temp_opts.length - 1; x++) {
        for (var y = (x + 1); y < temp_opts.length; y++) {
            if (temp_opts[x].text > temp_opts[y].text) {
                temp = temp_opts[x].text;
                temp_opts[x].text = temp_opts[y].text;
                temp_opts[y].text = temp;
                temp = temp_opts[x].value;
                temp_opts[x].value = temp_opts[y].value;
                temp_opts[y].value = temp;
            }
        }
    }
    for (var i = 0; i < box.options.length; i++) {
        box.options[i].value = temp_opts[i].value;
        box.options[i].text = temp_opts[i].text;
    }
}
function CreateIdList(tbox) {


    var arrTemp = new Array();
    for (var j = 0; j < tbox.options.length; j++) {
        if (tbox.options[j].value != "") {
            arrTemp[j] = tbox.options[j].value

            //alert("Item is already selected");
            //return false;
        }
    }
    document.getElementById('hdXMLString').value = arrTemp.join(',');


}
function getFieldName(fBoxSelectedValue, aoiFlag) {
    for (var iIndex = 0; iIndex < arrFieldConfig.length; iIndex++) {
        if (arrFieldConfig[iIndex].numFormFieldId == fBoxSelectedValue && arrFieldConfig[iIndex].boolAOIField == aoiFlag) {
            if (aoiFlag == 1)
                return arrFieldConfig[iIndex].vcDbColumnName + arrFieldConfig[iIndex].numFormFieldId.substring(0, arrFieldConfig[iIndex].numFormFieldId.length - 1);
            else
                return arrFieldConfig[iIndex].vcDbColumnName;
        }
    }
}
//function EnableLink(link) {
//    debugger;
//    document.getElementById(link).Enabled = true;
//document.
//}
function OpenWindow(path) {
    var hwnd = window.open(path);
    if (hwnd == null) {
        alert('A popup may have been blocked');
    }
}

function GetFrameDoc(FrameName) {
    var frm = document.getElementById(FrameName);
    var doc = frm.contentDocument;
    if (doc == undefined || doc == null)
        doc = frm.contentWindow.document;
    return doc;
}