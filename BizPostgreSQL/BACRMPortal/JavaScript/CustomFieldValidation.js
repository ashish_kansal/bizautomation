﻿function $SearchElement(name, tag, elm) {
    var tag = tag || "*";
    var elm = elm || document;
    var elements = (tag == "*" && elm.all) ? elm.all : elm.getElementsByTagName(tag);
    var returnElements = [];
    var current;
    var length = elements.length;
    for (var i = 0; i < length; i++) {
        current = elements[i];

        if (current.id.indexOf(name) >= 0) {
            returnElements.push(current);
        }
    }
    return returnElements[0];
};

var emailfilter = /^\w+[\+\.\w-]*@([\w-]+\.)*\w+[\w-]*\.([a-z]{2,4}|\d+)$/i
function CheckMail(ClientID, FieldName, FieldMessage) {
    var element = $SearchElement(ClientID)
    var returnval = emailfilter.test(trim(element.value))
    if (returnval == false) {
        if (FieldMessage.length == 0)
            alert("Please enter a valid " + FieldName + " .")
        else
            alert(FieldMessage);
        element.select()
    }
    return returnval
}

function RequiredField(ClientID, FieldName, FieldMessage) {
    var element = $SearchElement(ClientID)
    if (element != null)
        if (trim(element.value).length == 0) {
            if (FieldMessage.length == 0)
                alert(FieldName + ' is required!');
            else
                alert(FieldMessage);
            element.focus();
            return false;
        }
}
function RequiredFieldDropDown(ClientID, FieldName, FieldMessage) {
    var element = $SearchElement(ClientID)
    if (element != null)
        if (element.value == 0) {
            if (FieldMessage.length == 0)
                alert(FieldName + ' is required!');
            else
                alert(FieldMessage);
            element.focus();
            return false;
        }
}
function IsNumeric(ClientID, FieldName, FieldMessage) {
    var element = $SearchElement(ClientID)
    var x;
    if (element != null) {
        x = trim(element.value);
    }
    var anum = /(^\d+$)|(^\d+\.\d+$)/
    if (anum.test(x))
        testresult = true
    else {
        if (FieldMessage.length == 0)
            alert("Please input a valid number for " + FieldName + "!")
        else
            alert(FieldMessage);
        element.focus();
        testresult = false
    }
    return (testresult)
}

function IsAlphaNumeric(ClientID, FieldName, FieldMessage) {
    var element = $SearchElement(ClientID)
    var numaric;
    if (element != null) {
        numaric = trim(element.value);
    }

    for (var j = 0; j < numaric.length; j++) {
        var alphaa = numaric.charAt(j);
        var hh = alphaa.charCodeAt(0);
        if ((hh > 47 && hh < 58) || (hh > 64 && hh < 91) || (hh > 96 && hh < 123)) {
        }
        else {
            if (FieldMessage.length == 0)
                alert("Please enter valid AlphaNumeric value for " + FieldName + " ");
            else
                alert(FieldMessage);
            element.focus();
            return false;
        }
    }
    return true;
}
function RangeValidator(ClientID, FieldName, MinValue, MaxValue, FieldMessage) {

    var element = $SearchElement(ClientID)
    var numaric;
    if (element != null) {
        numaric = parseInt(trim(element.value));
    }
    if (trim(element.value) == '') {
        return true;
    }
    if (isNaN(numaric)) {
        if (IsNumeric(ClientID, FieldName, FieldMessage) == false)
            return false;
    }
    if (!isNaN(numaric)) {
        //validate if numeric first
        MinValue = parseInt(MinValue);
        MaxValue = parseInt(MaxValue);
        if (!(numaric <= MaxValue && numaric >= MinValue)) {
            if (FieldMessage.length == 0)
                alert('Value of ' + FieldName + ' must be between ' + MinValue + ' and ' + MaxValue);
            else
                alert(FieldMessage);
            element.focus();
            return false;
        }
    }
}

function trim(str, chars) {
    return ltrim(rtrim(str, chars), chars);
}

function ltrim(str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}

function rtrim(str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}