﻿$(document).ready(function () {
    var partnercode = getUrlVars()["partnercode"];
    var partnercontact = getUrlVars()["partnercontact"];
    $("#hfPartenrSource").val(partnercode);
    $("#hfPartenerContact").val(partnercontact);
    if ($("#hfEnableLinkedin").val() == 1) {
        $("#btnLinkedinLogin").css('display', 'block');
    } else {
        $("#btnLinkedinLogin").css('display', 'none');
    }
})
// Read a page's GET URL variables and return them as an associative array.
function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
function onLinkedInLoad() {
    IN.UI.Authorize().place();
    IN.Event.on(IN, "auth", onLinkedInAuth);
    return false;
}
function onLinkedInAuth() {
    IN.API.Profile("me")
    .fields("id", "firstName", "lastName", "industry", "location:(name)", "picture-url", "headline", "summary", "num-connections", "public-profile-url", "distance", "positions", "email-address", "educations", "date-of-birth")
    .result(displayProfiles)
    .error(displayProfilesErrors);
}

function displayProfiles(profiles) {
    member = profiles.values[0];
    //alert(JSON.stringify(member));
    //alert(member.id);

    //            document.getElementById("lblName").innerHTML = member.firstName + " " + member.lastName + "<br/> Location is " + member.location.name;
    $("#vcFirstName_51R").val(member.firstName);
    $("#vcLastName_52R").val(member.lastName);
    $("#vcEmail_53R").val(member.emailAddress);
    $("#hfLinkedinUrl").val(member.publicProfileUrl);
    $("#hfLinkedinId").val(member.id);
}
function displayProfilesErrors(error) {
    profilesDiv = document.getElementById("profiles");
    profilesDiv.innerHTML = error.message;
    console.log(error);
}