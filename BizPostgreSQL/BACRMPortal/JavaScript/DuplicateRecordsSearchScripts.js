/*
Purpose:	Toggles the display of the Search Settings
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function DisplaySearchSettings()
{
	if(document.getElementById('rbSearchFor_0').checked)
	{
		document.getElementById('trDuplicateContactsSettings').style.display='inline';
		document.getElementById('trDuplicateOrgSettings').style.display='none';
	}else{
		document.getElementById('trDuplicateContactsSettings').style.display='none';
		document.getElementById('trDuplicateOrgSettings').style.display='inline';
	}
	if(document.getElementById('tblDuplicateSearchResults'))
	{
		document.getElementById('tblDuplicateSearchResults').style.display='none';
		document.getElementById('dgDuplicateSearchResults').style.display='none';
	}
}
/*
Purpose:	If checked it checks all Leads/ Accouts/ Prospects and includes all Relationships
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function SearchRecordsIn()
{
	if(document.getElementById('cbCheckAll').checked)
	{
		document.getElementById('cbSearchInLeads').checked = true;
		document.getElementById('cbSearchInProspects').checked = true;
		document.getElementById('cbSearchInAccounts').checked = true;
		document.getElementById('ddlRelationship').selectedIndex = 0;
	}else{
		document.getElementById('cbSearchInLeads').checked = false;
		document.getElementById('cbSearchInProspects').checked = false;
		document.getElementById('cbSearchInAccounts').checked = false;
	}
}
/*
Purpose:	If inchecked then remove the selection of Check All
Created By: Debasish Tapan Nag
Parameter:	1) thisObject: The object which triggered this event
Return		1) None
*/
function RemoveSearchAllSelection(thisObject)
{
	if(thisObject.type == 'checkbox')
	{
		if(!thisObject.checked)
			document.getElementById('cbCheckAll').checked = false;
	}else{
		if(!thisObject.selectedIndex == 0)
			document.getElementById('cbCheckAll').checked = false;
	}
}
/*
Purpose:	Validates the search filter criteria
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function ValidateSearchCriteria(frmObject)
{	
	if (Page_IsValid)
	{
		return true;;
	}
	return false;
}
/*
Purpose:	Provides client side script to the custom validatior
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function validateSearchCriteria4DateRange(source, args)
{
	cbCreatedOnFilteration = document.getElementById('cbCreatedOn');
	txtFromDateDisplay = document.getElementById('txtFromDateDisplay');
	txtToDateDisplay = document.getElementById('txtToDateDisplay');
	if(cbCreatedOnFilteration.checked && (txtFromDateDisplay.value == '' && txtToDateDisplay.value == ''))
	{
		args.IsValid = false;
		return;
	}
	args.IsValid = true;
}
/*
Purpose:	Provides client side script to the custom validatior for mandatory selection
			of either Leads or Prospects or Accounts
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function validateSearchCriteria4CRMType(source, args)
{
	cbSearchInLeads  = document.getElementById('cbSearchInLeads');
	cbSearchInProspects  = document.getElementById('cbSearchInProspects');
	cbSearchInAccounts  = document.getElementById('cbSearchInAccounts');
	if(!cbSearchInLeads.checked && !cbSearchInProspects.checked && !cbSearchInAccounts.checked)
	{
		args.IsValid = false;
		return;
	}
	args.IsValid = true;
}
/*
Purpose:	Provides client side script to the custom validatior for mandatory entry of 
			First and Last Names when the checkbox is marked as checked
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function validateFirstAndLastName(source, args)
{
	cbCntIgnoreNames  = document.getElementById('cbCntIgnoreNames');
	rbSearchDupContacts  = document.getElementById('rbSearchFor_0');
	if(rbSearchDupContacts.checked && cbCntIgnoreNames.checked)
	{
		txtCntFirstName = document.getElementById('txtCntFirstName');
		txtCntLastName = document.getElementById('txtCntLastName');
		if(txtCntFirstName.value == '' && txtCntFirstName.value == '')
		{
			args.IsValid = false;
			return;
		}
	}
	args.IsValid = true;
}
/*
Purpose:	To Toggle the selection of the checkboxes
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
var boolDivCBSelected = false;
function ToggleDivContCBSelection()
{
	boolDivCBSelected = !boolDivCBSelected;
	var sDivIds = new Array();
	var vcRecordsOnPage;
	vcRecordsOnPage = document.getElementById('txtRecordsOnPage').value;
	sDivIds = vcRecordsOnPage.split(',');
	for(var iCheckBoxIndex=0;iCheckBoxIndex<sDivIds.length;iCheckBoxIndex++)
	{
		document.getElementById('cbSR' + sDivIds[iCheckBoxIndex]).checked = boolDivCBSelected;
	}
}
/*
Purpose:	Opens the Association Window in a new popup
Created By: Debasish Tapan Nag
Parameter:	1) numDivisionId: The Division Id
Return		1) None
*/
function OpenAssociationToWindow()
{
	var vcDivisionIds;
	vcDivisionIds = '';
	var vcRecordsOnPage;
	vcRecordsOnPage = document.getElementById('txtRecordsOnPage').value;
	var sDivIds = new Array();
	sDivIds = vcRecordsOnPage.split(',');
	for(var iCheckBoxIndex=0;iCheckBoxIndex<sDivIds.length;iCheckBoxIndex++)
	{
		if(document.getElementById('cbSR' + sDivIds[iCheckBoxIndex]).checked)
		{
			vcDivisionIds += sDivIds[iCheckBoxIndex] + ',';
		}
	}
	vcDivisionIds = vcDivisionIds.substring(0,vcDivisionIds.length-1);
	if (vcDivisionIds != '')
		window.open("frmComAssociationTo.aspx?boolAssociateDiv=true&numDivisionId="+vcDivisionIds,'','toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
	else
		alert('Please select the records which you want to associate with another parent organizations.');
	return false;
}

function OpenMergeCopyWindow()
{

	var vcContactIds;
	vcContactIds = '';
	var vcRecordsOnPage;
	vcRecordsOnPage = document.getElementById('txtRecordsOnPage').value;
	var sContactIds = new Array();
	sContactIds = vcRecordsOnPage.split(',');
	for(var iCheckBoxIndex=0;iCheckBoxIndex<sContactIds.length;iCheckBoxIndex++)
	{
		if(document.getElementById('cbSR' + sContactIds[iCheckBoxIndex]).checked)
		{
			vcContactIds += sContactIds[iCheckBoxIndex] + ',';
		}
	}
	vcContactIds = vcContactIds.substring(0,vcContactIds.length-1);
	if (vcContactIds != '')
		window.open("frmComAssociationTo.aspx?boolAssociateCont=true&numContactId="+vcContactIds,'','toolbar=no,titlebar=no,top=300,width=750,height=50,scrollbars=yes,resizable=yes')
	else
		alert('Please select the records which you want to merge with/ copy to another parent organizations.');
	return false;
}

/*
Purpose:	Returns to the Admin Panel when the Back button is clicked
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function ReturnToAdminPanel()
{
	document.location.href='frmAdminSection.aspx';
	return false;
}
/*
Purpose:	When the checkbox of Dates is unchecked, this blanks out the dates
Created By: Debasish Tapan Nag
Parameter:	1) thisObject: The checkbox Object
Return		1) None
*/
function BlankOutDatesOnUncheck(thisObject)
{
	if(!thisObject.checked)
	{
		document.getElementById('txtFromDateDisplay').value='';
		document.getElementById('txtToDateDisplay').value='';
	}
}
/*
Purpose:	Lead to Organization Details Page
Created By: Debasish Tapan Nag
Parameter:	1) numContactId: The Contact Id
Return		1) None
*/
function GoOrgDetails(numDivisionId)
{
	frames['IfrOpenOrgContact'].document.getElementById('hdRedirectionEntity').value='OrganizationFromDiv';
	frames['IfrOpenOrgContact'].document.getElementById('hdDivisionId').value = numDivisionId;
	frames['IfrOpenOrgContact'].document.forms['frmOrgContactRedirect'].submit();
}
/*
Purpose:	Lead to Contact Details Page
Created By: Debasish Tapan Nag
Parameter:	1) numContactId: The Contact Id
Return		1) None
*/
function GoContactDetails(numContactId)
{
	frames['IfrOpenOrgContact'].document.getElementById('hdRedirectionEntity').value='Contact';
	frames['IfrOpenOrgContact'].document.getElementById('hdContactId').value = numContactId;
	frames['IfrOpenOrgContact'].document.forms['frmOrgContactRedirect'].submit();
}