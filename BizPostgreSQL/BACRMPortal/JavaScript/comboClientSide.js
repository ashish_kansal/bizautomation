﻿function initComboExtensions() {
    //extend radCombobox class
    Telerik.Web.UI.RadComboBox.prototype.onDataBound = null;
    Telerik.Web.UI.RadComboBox.prototype.nrOfComboItems = 0;
    Telerik.Web.UI.RadComboBox.prototype.lastRequestFilter = "";
}

function OnClientDropDownOpening(sender, eventArgs) {
    if (trim(sender.get_text()).length == 0)
    { eventArgs.set_cancel(false); return; }

    //    for (var i = 0; i < sender.get_text().length; i++) {
    //        document.Form1.txtdesc.value = document.Form1.txtdesc.value + ' New1:' + String.charCodeAt(sender.get_text().substring(i, i + 1)); ;
    //    }
    var comboText = sender.get_text();
    if (sender.get_text().toString.length <= SearchCharLength)
    { return; }
    if (comboText != sender.lastRequestFilter)
        sender.requestItems(comboText);
}
function OnClientItemsRequesting(sender, eventArgs) {
    if (trim(sender.get_text()).length == 0)
    { eventArgs.set_cancel(false); return; }

    //    for (var i = 0; i < sender.get_text().length; i++) {
    //        document.Form1.txtdesc.value = document.Form1.txtdesc.value + ' New:' + String.charCodeAt(sender.get_text().substring(i, i + 1)); ;
    //    }

    //get the context object
    var context = eventArgs.get_context();
    context["SearchOrderCustomerHistory"] = document.getElementById('hdnSearchOrderCustomerHistory').value
    //set a value to the context object
    context["Division"] = document.getElementById('hdnCmbDivisionID').value;
    context["OppType"] = document.getElementById('hdnCmbOppType').value;
    sender.lastRequestFilter = sender.get_text();

    if (sender.get_text().length < SearchCharLength)
    { eventArgs.set_cancel(true); return; }

}

function trim(str, chars) {
    return ltrim(rtrim(str, chars), chars);
}

function ltrim(str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}

function rtrim(str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}

function OnClientItemsRequested(sender, eventArgs) {

    var ComboClientID;
    ComboClientID = sender._uniqueId.replace("$", "_");
    if ($find(ComboClientID) == null) {//When inside tab then uniqueid will return uwOppTab$_ctl3$radCmbItem
        var mySplitResult = sender._uniqueId.split("$");
        ComboClientID = mySplitResult[mySplitResult.length - 1]
    }
    //console.log(ComboClientID);
    var cbbox = $find(ComboClientID);
    var comboText = sender.get_text();
    comboText = comboText.toString();
    //    if (comboText.length == 0) {
    //        return;
    //    }
    if (cbbox.onDataBound != null) {
        var ulList = $(ComboClientID + "_DropDown").getElementsByClassName("rcbList")[0];
        var liElements = ulList.childElements();
        var items = sender.get_items();

        var startPos = 0;

        if (liElements[0] == undefined) {
            cbbox.nrOfComboItems = 0;
            cbbox.lastRequestFilter = "";
            return;
        }
        //if there is no <span> in the first <li> then it's a new list
        if (liElements[0].childElements().length == 0)
            startPos = 0;
        else
            startPos = cbbox.nrOfComboItems - 1;

        var dataItem;
        var dropDownLine;
        for (var index = startPos; index < liElements.length; ++index) {
            dataItem = items.getItem(index);
            if (dataItem != null) {
                dropDownLine = liElements[index];
                cbbox.onDataBound(dataItem, dropDownLine);
            }

            if (liElements.length == 1 && document.getElementById('hdnPOS').value == "1") {
                var allDetailItems = $find(ComboClientID).get_items();
                var itemAtIndex = allDetailItems.getItem(0);  
                itemAtIndex.select();
            }
        }
        cbbox.nrOfComboItems = liElements.length;
        cbbox.lastRequestFilter = "";
    }
}