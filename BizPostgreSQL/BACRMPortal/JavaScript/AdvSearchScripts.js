////////////////////////////////////////////////////////////////////////////
////////////////////ADVANCE SEARCH COLUMNS SELECTION////////////////////////
////////////////////////////////////////////////////////////////////////////
function Tickler()
{
    window.location.href = "../common/frmticklerdisplay.aspx";
}
/*
Purpose:	Opens the Edit Search Layout Screen in a new popup window
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function OpenSelSearchResultLayout()
{
	if(confirm('Are you sure, you want to change the search layout?'))
	{
		window.open('frmAdvSearchColumnCustomization.aspx','','toolbar=no,titlebar=no,left=200, top=200,width=500,height=300,scrollbars=no,resizable=yes')
	}
	return false;
}
/*
Purpose:	Redirects to the Admin Main Screen
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function Back()
{
	document.location.href='adminlinks.aspx';
}
/*
Purpose:	Closes the present window
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function CloseThisWin()
{
	if(opener.document.getElementById('hdDataGridFieldsChangedFlag').value=='true')
	{
		opener.document.getElementById('btnReInitializeScreen').click();
	}
	this.close();
}

/*
Purpose:	encodes the string
Created By: Debasish Tapan Nag
Parameter:	1) fBoxString: The source string
Return		1) The htmlencoded string
*/
function encodeMyHtml(fBoxString)
{
     encodedHtml = unescape(fBoxString);
     //encodedHtml = encodedHtml.replace(/&/g,"\&amp;");
     //encodedHtml = encodedHtml.replace(/\>/g,"&gt;");
     //encodedHtml = encodedHtml.replace(/\</g,"&lt;");
     encodedHtml = encodedHtml.replace(/'/g,"\''");
     //encodedHtml = encodedHtml.replace(/"/g,"\&quot;");
     return encodedHtml;
}
/*
Purpose:	The processing required before the form is saved
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function PreSaveSearchCustomFields(frmForm)
{
	var sXMLString = '<FormFields>'+'\n';
	//Create xml for available fields
	for(var i=0; i<frmForm.lstSelectedfld.options.length; i++)
	{
		sXMLString += '<FormField>'+'\n';
		sXMLString += '<vcDbColumnName>' + frmForm.lstSelectedfld.options[i].value + '</vcDbColumnName>'+'\n';
		sXMLString += '<vcFormFieldName>' + frmForm.lstSelectedfld.options[i].text + '</vcFormFieldName>'+'\n';
		sXMLString += '</FormField>'+'\n';
	}
	sXMLString += '</FormFields>'+'\n';
	frmForm.hdXMLString.value = sXMLString;
}
/*
Purpose:	To validate the form data
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) False/ True
*/
function validateSelectedSearchFields(frmForm)
{
	if(frmForm.lstSelectedfld.options.length > 0)
	{
		PreSaveSearchCustomFields(frmForm);		
		frmForm.hdSave.value = 'True';
		if((opener.document.getElementById('hdAuthenticationGroupID').value == frmForm.ddlGroup.options[frmForm.ddlGroup.selectedIndex].value) && (frmForm.ddlView.options[frmForm.ddlView.selectedIndex].value == opener.document.getElementById('ddlView').value))
		{
			opener.document.getElementById('hdDataGridFieldsChangedFlag').value='true';
		}
		return true;
	}
	alert('Please select the columns for search result of the selected group.');
	return false;
}
/*
Purpose:	To validate the form data
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) False/ True
*/
function checkGroupSelection(frmForm)
{
	if(frmForm.ddlGroup.selectedIndex == 0)
	{
		alert('Please select the user group prior to selecting the columns.');
		return false;
	}
	return true;
}
/*
Purpose:	To move the options upwards from one position in the listbox 
			to another position in the same listbox
Created By: Debasish Tapan Nag
Parameter:	1) tbox: The target list box
Return		1) False
*/
function MoveUp(tbox)
{		
	for(var i=1; i<tbox.options.length; i++)
	{
		if(tbox.options[i].selected && tbox.options[i].value != "") 
		{		
			var SelectedText,SelectedValue,PrevSelectedValue;
			SelectedValue = tbox.options[i].value;
			SelectedText = tbox.options[i].text;
			PrevSelectedValue = tbox.options[i-1].value;
			tbox.options[i].value=tbox.options[i-1].value;
			tbox.options[i].text=tbox.options[i-1].text;
			tbox.options[i-1].value=SelectedValue;
			tbox.options[i-1].text=SelectedText;
			tbox.options[i-1].selected=true;
		}
	}
	return false;
}
/*
Purpose:	To move the options downwards from one position in the listbox 
			to another position in the same listbox
Created By: Debasish Tapan Nag
Parameter:	1) tbox: The target list box
Return		1) False
*/
function MoveDown(tbox)
{
	for(var i=0; i<tbox.options.length-1; i++)
	{
		if(tbox.options[i].selected && tbox.options[i].value != "") 
		{
			var SelectedText,SelectedValue,NextSelectedValue;
			SelectedValue = tbox.options[i].value;
			SelectedText = tbox.options[i].text;
			NextSelectedValue = tbox.options[i+1].value;
			tbox.options[i].value=tbox.options[i+1].value;
			tbox.options[i].text=tbox.options[i+1].text;
			tbox.options[i+1].value=SelectedValue;
			tbox.options[i+1].text=SelectedText;
		}
	}
	return false;
}
/*
Purpose:	Checks the number of fields selected and then disables/ enables the buttons
Created By: Debasish Tapan Nag
Parameter:	1) tbox: The target list box
Return		1) False
*/
function checkMaxFields(tbox)
{
	if(tbox.options.length >= 10)
		document.getElementById('btnAdd').disabled=true;
	else document.getElementById('btnAdd').disabled=false;
}

sortitems = 0;  // 0-False , 1-True
/*
Purpose:	To move the options from one listbox to another
Created By: Debasish Tapan Nag
Parameter:	1) fbox:The from list box
			2) tbox: The target list box
Return		1) False
*/
function move(fbox,tbox)
{
	fBoxSelectedIndex = -1;
	for(var i=0; i<fbox.options.length; i++)
	{
		if(fbox.options[i].selected && fbox.options[i].value != "") 
		{
			/// to check for duplicates 
			for (var j=0;j<tbox.options.length;j++)
			{
				if (tbox.options[j].value == fbox.options[i].value)	
				{
					alert("Item is already selected");
					return false;
				}
			}
			fBoxSelectedIndex = fbox.options[i].value;
			var no = new Option();
			no.value = fbox.options[i].value;
			no.text = fbox.options[i].text;
			tbox.options[tbox.options.length] = no;
			fbox.options[i].value = "";
			fbox.options[i].text = "";
		}
	}
	BumpUp(fbox);
	checkMaxFields(tbox);	//Calls to limit the number of fields selected in a view
	if (sortitems) SortD(tbox);
	return false;
	
}
/*
Purpose:	To remove the options from one listbox to another
Created By: Debasish Tapan Nag
Parameter:	1) fbox:The from list box
			2) tbox: The target list box
			3) aoiflag: aoi flag
Return		1) False
*/
function remove(fbox,tbox)
{
	fBoxSelectedIndex = -1;
	for(var i=0; i<fbox.options.length; i++)
	{
		if(fbox.options[i].selected && fbox.options[i].value != "") 
		{
			fBoxSelectedIndex = fbox.options[i].value;
			/// to check for duplicates 
			for (var j=0;j<tbox.options.length;j++)
			{
				if (tbox.options[j].value == fbox.options[i].value)	
				{
					fbox.options[i].value = "";
					fbox.options[i].text = "";
					BumpUp(fbox);
					if (sortitems) SortD(tbox);
					return false;
					
					
					//alert("Item is already selected");
					//return false;
				}
			}
			var no = new Option();
			no.value = fbox.options[i].value;
			if(fbox.options[i].text.substring(0,4) != '(*) ')
				no.text = fbox.options[i].text;
			else
				no.text = fbox.options[i].text.substring(4,fbox.options[i].text.length);
			tbox.options[tbox.options.length] = no;
			fbox.options[i].value = "";
			fbox.options[i].text = "";		
		}
	}
	BumpUp(fbox);
	checkMaxFields(fbox);	//Calls to limit the number of fields selected in a view
	if (sortitems) SortD(tbox);
	return false;
}
/*
Purpose:	To shift the option upwards after one is removed from the middle
Created By: Debasish Tapan Nag
Parameter:	1) box:The list box under consideration
Return		1) None
*/
function BumpUp(box) 
{
	for(var i=0; i<box.options.length; i++) 
	{
		if(box.options[i].value == "")  
		{
			for(var j=i; j<box.options.length-1; j++)  
			{
				box.options[j].value = box.options[j+1].value;
				box.options[j].text = box.options[j+1].text;
			}
			var ln = i;
			break;
		}
	}
	if(ln < box.options.length) 
	{
		box.options.length -= 1;
		BumpUp(box);
	}
}
/*
Purpose:	To sort the options in the drop down
Created By: Debasish Tapan Nag
Parameter:	1) box:The list box under consideration
Return		1) None
*/
function SortD(box) 
{
	var temp_opts = new Array();
	var temp = new Object();
	for(var i=0; i<box.options.length; i++)  
	{
		temp_opts[i] = box.options[i];
	}
	for(var x=0; x<temp_opts.length-1; x++)  
	{
		for(var y=(x+1); y<temp_opts.length; y++)  
		{
			if(temp_opts[x].text > temp_opts[y].text) 
			{
				temp = temp_opts[x].text;
				temp_opts[x].text = temp_opts[y].text;
				temp_opts[y].text = temp;
				temp = temp_opts[x].value;
				temp_opts[x].value = temp_opts[y].value;
				temp_opts[y].value = temp;
			}
		}
	}
	for(var i=0; i<box.options.length; i++)  
	{
		box.options[i].value = temp_opts[i].value;
		box.options[i].text = temp_opts[i].text;
	}
}
////////////////////////////////////////////////////////////////////////////
////////////////ADVANCE SEARCH FILTER CONDITIONS SELECTION//////////////////
////////////////////////////////////////////////////////////////////////////
/*
Purpose:	Opens the Commerce Marketing selection screen in a new indow
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function OpenCommerceMarketing()
{
	hndwAccount = window.open('frmAccountAdvSearch.aspx?frmScreen=frmGenericFormAdvSearch.aspx','CommerceMarketing','toolbar=no,titlebar=no,left=200, top=300,width=500,height=140,scrollbars=no,resizable=yes')
	hndwAccount.focus();
}
/*
Purpose:	Opens the list of teams in a new window
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function OpenSelTeam(a)
{
	window.open("../Forecasting/frmSelectTeams.aspx?Type="+a,'','toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
	return false;
}
/*
Purpose:	Opens the list of territories in a new window
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function OpenTerritory(a)
{
	window.open("../Reports/frmSelectTerritories.aspx?Type="+a,'','toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
	return false;
}
/*
Purpose:	Uncheck the checkbox specified
Created By: Debasish Tapan Nag
Parameter:	1) uncheckElementName: The checkbox name to be unchecked
Return		1) None
*/
function UncheckOtherDateFilteration(uncheckElementName)
{
	document.getElementById(uncheckElementName).checked = false;
}
/*
Purpose:	Class for storing the form data
Created By: Debasish Tapan Nag
Parameter:	1) vcDbColumnName: The table columns where the data entered in these fields will be entered
			2) vcFieldName: The form field text
			3) vcDbColumnValue: The form field value
			4) numRowNum: The row number in which this control will appear
			5) numColumnNum: The column number in which this control will appear
			6) vcAssociatedControlType: The control type (EditBox/ SelectBox/ CheckBox etc)
			7) boolAOIField: Indicates if the field is AOI or not
Return		1) Nothing
*/
function classFormFieldConfig(vcDbColumnName,vcFieldName,vcDbColumnValue,vcDbColumnValueText,numRowNum,numColumnNum,vcAssociatedControlType,boolAOIField)
{
	this.vcDbColumnName = vcDbColumnName;
	this.vcFieldName = vcFieldName;
	this.vcDbColumnValue = vcDbColumnValue;
	this.vcDbColumnValueText = vcDbColumnValue;
	this.numRowNum = numRowNum;
	this.numColumnNum = numColumnNum;
	this.vcAssociatedControlType = vcAssociatedControlType;
	this.boolAOIField = boolAOIField;
}
/*
Purpose:	Class for storing the form config parameters for Custom Areas
Created By: Debasish Tapan Nag
Parameter:	1) numLocId: The Location Id for which this config settings are
			3) vcFormFieldName: The form field text
Return		1) Nothing
*/
function classCustomAreas(numLocId,vcFormFieldName)
{
	this.numLocId = numLocId;
	this.vcFormFieldName = vcFormFieldName;
}
/*
Purpose:	Provides client side script to the custom validatior for Team and Territory
Created By: Debasish Tapan Nag
Parameter:	1) source: Source of the event
			2) args: Arguments for the event
Return		1) None
*/
function validateCustomSearchCriteria4TerritoryTeam(source, args)
{
	if(document.getElementById('rdlReportType_1').checked && document.getElementById('hdTeams').value=='')
	{
		args.IsValid = false;
		return;
	}
	if(document.getElementById('rdlReportType_2').checked && document.getElementById('hdTerritories').value=='')
	{
		args.IsValid = false;
		return;
	}
	args.IsValid = true;
}
/*
Purpose:	Provides client side script to the custom validatior
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function validateCustomSearchCriteria4DateRange(source, args)
{
	cbCreatedOnFilteration = document.getElementById('cbCreatedOnFilteration');
	cbModifiedOnFilteration = document.getElementById('cbModifiedOnFilteration');
	txtFromDateDisplay = document.getElementById('txtFromDateDisplay');
	txtToDateDisplay = document.getElementById('txtToDateDisplay');
	if((cbCreatedOnFilteration.checked || cbModifiedOnFilteration.checked) && (txtFromDateDisplay.value == '' || txtToDateDisplay.value == ''))
	{
		args.IsValid = false;
		return;
	}
	args.IsValid = true;
}
/*
Purpose:	Provides client side script to the custom validatior for mandatory selection
			of either Leads or Prospects or Accounts
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function validateCustomSearchCriteria4CRMType(source, args)
{
	ddlLeads  = document.getElementById('ddlLead');
	cbSearchInProspects  = document.getElementById('cbSearchInProspects');
	cbSearchInAccounts  = document.getElementById('cbSearchInAccounts');
	if(ddlLeads.selectedIndex==0 && !cbSearchInProspects.checked && !cbSearchInAccounts.checked)
	{
		args.IsValid = false;
		return;
	}
	args.IsValid = true;
}
/*
Purpose:	Provides client side script to the custom validatior for mandatory entry
			of Custom Field Search once a Custom Field Area CheckBox is selected
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function validateCustomFieldAreas(source, args)
{
	if(document.getElementById('txtCustomKeyword'))
	{
		var boolCustomFieldAreaChecked = false;
		var sCustomFieldGroupIdList = '';
		for(var iIndex=0;iIndex<arrCustomAreasFieldConfig.length;iIndex++)
		{
			if(document.getElementById('Cfld_' + iIndex).checked)
			{
				boolCustomFieldAreaChecked = true;
				sCustomFieldGroupIdList = sCustomFieldGroupIdList + ',' + arrCustomAreasFieldConfig[iIndex].numLocId;
			}
		}
		
		if((boolCustomFieldAreaChecked && document.getElementById('txtCustomKeyword').value=='') || (!boolCustomFieldAreaChecked && document.getElementById('txtCustomKeyword').value!=''))
		{
			args.IsValid = false;
			return;
		}else{
			document.getElementById('hdCustomFieldGroupIds').value = sCustomFieldGroupIdList.substring(1,sCustomFieldGroupIdList.length);
		}
	}
	args.IsValid = true;
}
/*
Purpose:	The processing required before the form is saved
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function PreSaveSearchCriteria(frmForm)
{
	if (frmForm.txtCustomKeyword!=null)
	{
	    frmForm.hdCustField.value = frmForm.txtCustomKeyword.value;
	}
	
	//Search "within" or "search string starts with"
	var contextOfSearch;
	if(frmForm.rbListSearchContext[1].checked)	
		contextOfSearch = 'Starting';
	else
		contextOfSearch = 'Within';
		
	//"My Web Leads" or "Teams" or "Territories"
	var sXMLString = '';
	if(frmForm.ddlLead.options[frmForm.ddlLead.selectedIndex].value=="0")
	{	sXMLString += '(' + '\n';}

	if(frmForm.rdlReportType_0.checked)
	{	sXMLString += ' (numContRecOwner = ' + frmForm.hdUserId.value + ' OR numDivRecOwner = ' + frmForm.hdUserId.value +  ')\n';}	//Contact and Org owner Record are separated (CR by Carl on 2nd Jan 2006)
	else if(frmForm.rdlReportType_1.checked)
	{	sXMLString += ' ';}	//Contact and Org owner Record are separated (CR by Carl on 2nd Jan 2006)
	//else if(frmForm.rdlReportType[1].checked)	//Teams are handled differently (CR by Carl on 2nd Jan 2006)/ on the server side
	//{	sXMLString += ' numTeam IN (' + frmForm.hdTeams.value + ')' + '\n';}
	else if(frmForm.rdlReportType_2.checked)
	{	sXMLString += ' (numTerritoryId IN (0,' + frmForm.hdTerritories.value + ')  AND ((bitPublicFlag = 0) OR (bitPublicFlag=1 and numDivRecOwner = ' + frmForm.hdUserId.value + '))' + '\n';}	//All Org with no territories are included (CR by Carl on 2nd Jan 2006)

	if(frmForm.ddlLead.options[frmForm.ddlLead.selectedIndex].value=="PublicLeads")	//Taking care of Leadbox drop down
	{	sXMLString += ' AND (((tintCRMType = 0) AND (numGroupId = 2))' + '\n';}	//"bitPublicFlag = 0 AND " removed on the 7th of Mar 2006
	else if(frmForm.ddlLead.options[frmForm.ddlLead.selectedIndex].value=="PrivateLeads")
	{	sXMLString += ' AND (((tintCRMType = 0) AND (numGroupId = 5))' + '\n';}	//"bitPublicFlag = 1 AND " removed on the 7th of Mar 2006
	else if(frmForm.ddlLead.options[frmForm.ddlLead.selectedIndex].value=="MyWebLeads")
	{	sXMLString += ' AND (((tintCRMType = 0) AND (numGroupId = 1))' + '\n';}	//"bitLeadBoxFlg = 1 AND" removed on the 7th of Mar 2006
	else if(frmForm.ddlLead.options[frmForm.ddlLead.selectedIndex].value=="All")
	{	sXMLString += ' AND (((tintCRMType = 0) AND ((numGroupId = 2) OR (numGroupId = 5) OR (numGroupId = 1))) ' + '\n';}	//"bitLeadBoxFlg = 0 AND " "(bitPublicFlag = 0) OR (bitPublicFlag = 1)" removed on the 7th of Mar 2006
	//"Prospects" or "Accounts"
	var sFlagLeadProspectAccounts=-1;
	if(frmForm.cbSearchInProspects.checked)					
		sFlagLeadProspectAccounts += ',1';													//Prospects
	if(frmForm.cbSearchInAccounts.checked)					
		sFlagLeadProspectAccounts += ',2';													//Accounts
	if(sFlagLeadProspectAccounts != -1)
	{
		sFlagLeadProspectAccounts = sFlagLeadProspectAccounts.substring(3,sFlagLeadProspectAccounts.length);
		sXMLString += ' OR tintCRMType IN (' + sFlagLeadProspectAccounts + ')';
	}
	sXMLString += ') ';
	if(frmForm.rdlReportType_2.checked)
	  sXMLString += ') ';
	
	//"Created Date Range"
	if(frmForm.cbCreatedOnFilteration.checked)												//accounts for creted date range
	{
		sXMLString += ' AND bintCreatedDate >= \'' + fDateFormat(frmForm.txtFromDateDisplay.value) + '\' AND bintCreatedDate <= \'' + fDateFormat(frmForm.txtToDateDisplay.value) + '\'' + '\n';
	}else if(frmForm.cbModifiedOnFilteration.checked)
	{
		sXMLString += ' AND (bintModifiedDate IS NOT NULL) AND (bintModifiedDate >= \'' + fDateFormat(frmForm.txtFromDateDisplay.value) + '\' AND bintModifiedDate <= \'' + fDateFormat(frmForm.txtToDateDisplay.value) + '\')' + '\n';
	}
	
	//"Relation Ship" etc
	if(frmForm.ddlRelationship.options[frmForm.ddlRelationship.selectedIndex].value!="0")	//Taking care of Relationship drop down
		sXMLString += ' AND numCompanyType = ' + frmForm.ddlRelationship.options[frmForm.ddlRelationship.selectedIndex].value + '\n';
	
	
	var vcDbColumnValue;
	var vcAOIList='';
	var vcDbColumnValueTo;
	var vcAvailableColList = document.getElementById('hdSearchableColList').value;
	var boolIsPostalCodePresent;		//Since Postal code is a range box (CR by Carl on the 28th of Sept 2005)
	//Create a where condition with available fields and their data
	for(iIndex=0;iIndex<arrFieldConfig.length;iIndex++)
	{
		boolIsPostalCodePresent = false;
		if(arrFieldConfig[iIndex].vcDbColumnName.substring(0,13) == 'vcInterested_' || vcAvailableColList.toLowerCase().indexOf(arrFieldConfig[iIndex].vcDbColumnName.toLowerCase()) > -1)
		{
			if(arrFieldConfig[iIndex].vcAssociatedControlType == 'EditBox' || arrFieldConfig[iIndex].vcAssociatedControlType == 'TextArea' || arrFieldConfig[iIndex].vcAssociatedControlType == 'TextBox')
			{
				vcDbColumnValue = encodeMyHtml(document.getElementById(arrFieldConfig[iIndex].vcDbColumnName).value);
				if(arrFieldConfig[iIndex].vcDbColumnName == 'intPostalCode')
					boolIsPostalCodePresent = true;//Postal Code is there in the search screen
			}
			else if(arrFieldConfig[iIndex].vcAssociatedControlType == 'SelectBox')
			{
				vcDbColumnValue = eval('frmForm.'+arrFieldConfig[iIndex].vcDbColumnName+'.options[frmForm.'+arrFieldConfig[iIndex].vcDbColumnName+'.selectedIndex].value');
			}
			else if(arrFieldConfig[iIndex].vcAssociatedControlType == 'RadioBox' || arrFieldConfig[iIndex].vcAssociatedControlType == 'CheckBox')
			{
				vcDbColumnValue = document.getElementById(arrFieldConfig[iIndex].vcDbColumnName).checked ? 1 : 0;
				if(arrFieldConfig[iIndex].vcDbColumnName.substring(0,13) == 'vcInterested_' && vcDbColumnValue != '')
				{
					if(document.getElementById(arrFieldConfig[iIndex].vcDbColumnName).checked)
					vcAOIList += ' OR vcInterested Like (' + '\'%' + arrFieldConfig[iIndex].vcFieldName + '%\'' + ')';
				}
			}
			if(arrFieldConfig[iIndex].vcDbColumnName.substring(0,13) != 'vcInterested_')
				if(vcDbColumnValue != '')
					if(contextOfSearch == 'Within')
					{
						if(boolIsPostalCodePresent)
						{
							vcDbColumnValueTo = document.getElementById(arrFieldConfig[iIndex].vcDbColumnName + 'To').value;
							if(vcDbColumnValueTo!='')
								sXMLString += ' AND (IsNumeric(intPostalCode) = 1 AND LTRIM(Convert(NVarchar(250),' + arrFieldConfig[iIndex].vcDbColumnName + ')) >= \'' + vcDbColumnValue + '\' AND Convert(NVarchar(250),LTRIM(' + arrFieldConfig[iIndex].vcDbColumnName + ')) <= \'' + vcDbColumnValueTo + '\')';
							else
								sXMLString += ' AND (IsNumeric(intPostalCode) = 1 AND LTRIM(Convert(NVarchar(250),' + arrFieldConfig[iIndex].vcDbColumnName + ')) >= \'' + vcDbColumnValue + '\')';
						}else{
							sXMLString += ' AND ' + (arrFieldConfig[iIndex].vcDbColumnName.substring(0,12) == 'vcInterested'? 'vcInterested' : 'LTRIM(Convert(NVarchar(250),' + arrFieldConfig[iIndex].vcDbColumnName + '))' + ((arrFieldConfig[iIndex].vcAssociatedControlType == 'EditBox' || arrFieldConfig[iIndex].vcAssociatedControlType == 'TextArea' || arrFieldConfig[iIndex].vcAssociatedControlType == 'TextBox') ? ' LIKE ': ' = ') + '\'' + ((arrFieldConfig[iIndex].vcAssociatedControlType == 'EditBox' || arrFieldConfig[iIndex].vcAssociatedControlType == 'TextArea') ? '%' + vcDbColumnValue + '%' : vcDbColumnValue) + '\'') +'\n';
						}
					}else{
						if(boolIsPostalCodePresent)
						{
							vcDbColumnValueTo = document.getElementById(arrFieldConfig[iIndex].vcDbColumnName + 'To').value;
							if(vcDbColumnValueTo!='')
								sXMLString += ' AND (IsNumeric(intPostalCode) = 1 AND LTRIM(Convert(NVarchar(250),' + arrFieldConfig[iIndex].vcDbColumnName + ')) >= \'' + vcDbColumnValue + '\' AND Convert(NVarchar(250),LTRIM(' + arrFieldConfig[iIndex].vcDbColumnName + ')) <= \'' + vcDbColumnValueTo + '\')';
							else
								sXMLString += ' AND (IsNumeric(intPostalCode) = 1 AND LTRIM(Convert(NVarchar(250),' + arrFieldConfig[iIndex].vcDbColumnName + ')) >= \'' + vcDbColumnValue + '\')';
						}else{
							sXMLString += ' AND ' + (arrFieldConfig[iIndex].vcDbColumnName.substring(0,12) == 'vcInterested'? 'vcInterested' : 'LTRIM(Convert(NVarchar(250),' + arrFieldConfig[iIndex].vcDbColumnName + '))' + ((arrFieldConfig[iIndex].vcAssociatedControlType == 'EditBox' || arrFieldConfig[iIndex].vcAssociatedControlType == 'TextArea' || arrFieldConfig[iIndex].vcAssociatedControlType == 'TextBox') ? ' LIKE ': ' = ') + '\'' + ((arrFieldConfig[iIndex].vcAssociatedControlType == 'EditBox' || arrFieldConfig[iIndex].vcAssociatedControlType == 'TextArea') ? vcDbColumnValue + '%' : vcDbColumnValue) + '\'') +'\n';
						}
					}
		}
	}
	if(vcAOIList != '')
	{
		sXMLString += ' AND (' + vcAOIList.substring(4, vcAOIList.length) + ')';
	}
	frmForm.hdXMLString.value = sXMLString;
	return true;
}
/*
Purpose:	Validates the search filter criteria
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function ValidateSearchCriteria(frmObject)
{	
	Page_ClientValidate();							//explcitly call for validation
	if (Page_IsValid)
	{
		return PreSaveSearchCriteria(frmObject);
	}
	return false;
}
/*
Purpose:	Returns the Date as a contcatenated String
Created By: Debasish Tapan Nag
Parameter:	1) vDateValue: The Date which is passed
			2) dateFormat: The format in which the date is passed
Return		1) The string of requested date
*/
function fDateFormat(vDateValue) {
	return vDateValue
}

////////////////////////////////////////////////////////////////////////////
////////////////////////////////ADVANCE SEARCH RESULT///////////////////////
////////////////////////////////////////////////////////////////////////////
/*
Purpose:	To set the filter within column values
Created By: Debasish Tapan Nag
Parameter:	1) frmElement: The source of data
			2) toElement: The Destination of data
Return		1) None
*/
function makeDataTransfer(varSortColName,frmElement,toElement,divName)
{
	if(! IsEmpty(frmElement.value))
	{
		document.getElementById(divName).style.visibility='hidden';
		if(frmElement.value=='ALL')
			frmElement.value = '%';
		toElement.value = frmElement.value;
		frmElement.value='';
		for(var iFieldIndex=0;iFieldIndex < document.frmSearchResultDisplay.ddlSortColumn.options.length;iFieldIndex++)
		{
			if(varSortColName.replace(/\+/g," ") == document.frmSearchResultDisplay.ddlSortColumn.options[iFieldIndex].text)
				varSortColName = document.frmSearchResultDisplay.ddlSortColumn.options[iFieldIndex].value;
		}
		document.getElementById('txtFilterColumn').value = varSortColName;
		document.getElementById('txtCurrentPage').value = 1;
		document.frmSearchResultDisplay.btnRefresh.click();
	}else{
		alert('Please enter the filter within text.');
		frmElement.focus();
	}
}
/*
Purpose:	Hide the div element
Created By: Debasish Tapan Nag
Parameter:	1) frmElement: The source of data
			2) toElement: The Destination of data
Return		1) None
*/
function HideDivElement(divName)
{
	document.getElementById(divName).style.visibility='hidden';
}
/*
Purpose:	Hide the div element
Created By: Debasish Tapan Nag
Parameter:	1) frmElement: The source of data
			2) toElement: The Destination of data
Return		1) None
*/
function ShowAdvancedWin(divName)
{
    document.getElementById(divName).style.visibility='visible';
    return false;
}
function FilterWithinRecords(labelName, divName)
{
	document.getElementById('spColName').innerHTML=labelName.replace(/\+/g," ");
	document.getElementById(divName).style.visibility='visible';
	document.getElementById('txtColValue').focus();
}
/*
Purpose:	Sets the sort by attribute when the character rules is clicked
Created By: Debasish Tapan Nag
Parameter:	1) frmElement: The source of data
			2) toElement: The Destination of data
Return		1) None
*/
function fnSortByChar(varSortChar)
{
	if(varSortChar == '0')
		varSortChar = '%';
	if(document.frmSearchResultDisplay.ddlSortColumn.options.length > 0)
	{
		document.frmSearchResultDisplay.txtSortChar.value = varSortChar;
		var varSortColName;
		varSortColName = document.frmSearchResultDisplay.ddlSortColumn.options[document.frmSearchResultDisplay.ddlSortColumn.selectedIndex].value;
		document.frmSearchResultDisplay.txtFilterColumn.value = varSortColName;
		document.frmSearchResultDisplay.txtCurrentPage.value = 1;
		document.frmSearchResultDisplay.btnRefresh.click();
	}
}
/*
Purpose:	Checks all the checkboxes
Created By: Debasish Tapan Nag
Parameter:	1) status: Checked/ Unchecked
Return		1) None
*/
function CheckAll(status)
{
	if(document.getElementById('cbSelectAllRecords'))
		if (document.getElementById('cbSelectAllRecords').checked)
			status = true;
	
	var elementName;
	for(var iFieldIndex=0; iFieldIndex < document.frmSearchResultDisplay.elements.length; iFieldIndex++)
	{
		elementName =  document.frmSearchResultDisplay.elements[iFieldIndex].name;
		if(elementName.substring(0,4) == 'cbSR')
			document.frmSearchResultDisplay.elements[iFieldIndex].checked = status;
	}
	if(!status)
		if(window.cbCheckAll)
			document.getElementById('cbCheckAll').checked = false;
}
/*
Purpose:	Sets the sort the column by its value according to specified order
Created By: Debasish Tapan Nag
Parameter:	1) varSortColName: The Column Name and Sort Order
Return		1) None
*/
function SetSortColumn(varSortColName,varSortOrder)
{
	for(var iFieldIndex=0;iFieldIndex < document.frmSearchResultDisplay.ddlSortColumn.options.length;iFieldIndex++)
	{
		if(varSortColName.replace(/\+/g," ") == document.frmSearchResultDisplay.ddlSortColumn.options[iFieldIndex].text)
			varSortColName = document.frmSearchResultDisplay.ddlSortColumn.options[iFieldIndex].value;
	}
	document.frmSearchResultDisplay.txtSortColumn.value = varSortColName;
	document.frmSearchResultDisplay.txtSortColumnOrder.value = varSortOrder;
	document.frmSearchResultDisplay.btnRefresh.click();
}
/*
Purpose:	restores the status of the screen
Created By: Debasish Tapan Nag
Parameter:	1) none
Return		1) None
*/
function ProcessPostLoadStatus()
{
	if(document.getElementById('cbSelectAllRecords'))
	CheckAll(document.getElementById('cbSelectAllRecords').checked);
}
/*
Purpose:	Checks if the Record owner is selected
Created By: Debasish Tapan Nag
Parameter:	1) frmObject:The Form Object
Return		1) true/ false
*/
function CheckIfRecordOwnerSelected(frmObject)
{
	if(frmObject.ddlRecordOwner.selectedIndex > 0)
	{
		var sCompIdList = 0;
		var boolAtleastOneSelected = false;
		if(frmObject.cbSelectAllRecords.checked || frmObject.cbCheckAll.checked)
		{
			sCompIdList = frmObject.txtAllRecordCompIds.value;
			boolAtleastOneSelected = true;
		}
		else
		{
			for(var iFieldIndex=0; iFieldIndex < frmObject.elements.length; iFieldIndex++)
			{
				elementName =  frmObject.elements[iFieldIndex].name;
				if(elementName.substring(0,4) == 'cbSR' && frmObject.elements[iFieldIndex].checked)
				{
					boolAtleastOneSelected = true;
					sCompIdList += ',' + frmObject.elements[iFieldIndex].value;
				}
			}
			if(boolAtleastOneSelected)
			sCompIdList = sCompIdList.substring(2,sCompIdList.length)
		}
		if(!boolAtleastOneSelected)
		{
			alert('Select the records before trying to transfer ownership.');
			return false;
		} else 
		{
				if(confirm('Are you sure, you want to change the owners for the selected records?'))
				{
					frmObject.txtCheckedRecordCompIds.value = sCompIdList;
					return true;
				}return false;
		}	
	}
	alert('Please select the new owner for the selected records.');
	return false;
}

/*
Purpose:	Checks if a Follow-Up Status name is selected from the dropdown
Created By: Debasish Tapan Nag
Parameter:	1) frmObject:The Form Object
Return		1) true/ false
*/
function CheckIfFollowUpStatusSelected(frmObject)
{
	if(frmObject.ddlFollowUpStatus.selectedIndex > 0)
	{
		var sCompIdList = 0;
		var boolAtleastOneSelected = false;
		if(frmObject.cbSelectAllRecords.checked || frmObject.cbCheckAll.checked)
		{
			sCompIdList = frmObject.txtAllRecordCompIds.value;
			boolAtleastOneSelected = true;
		}
		else
		{
			for(var iFieldIndex=0; iFieldIndex < frmObject.elements.length; iFieldIndex++)
			{
				elementName =  frmObject.elements[iFieldIndex].name;
				if(elementName.substring(0,4) == 'cbSR' && frmObject.elements[iFieldIndex].checked)
				{
					boolAtleastOneSelected = true;
					sCompIdList += ',' + frmObject.elements[iFieldIndex].value;
				}
			}
			if(boolAtleastOneSelected)
			sCompIdList = sCompIdList.substring(2,sCompIdList.length)
		}
		if(!boolAtleastOneSelected)
		{
			alert('Select the records before trying to change the Follow-Up Status.');
			return false;
		} else 
		{
				if(confirm('Are you sure, you want to change the Follow-Up Status for the selected records?'))
				{
					frmObject.txtCheckedRecordCompIds.value = sCompIdList;
					return true;
				}return false;
		}
	}
	alert('Please select the Follow-Up Status for the selected records.');
	return false;
}

/*
Purpose:	Checks if the Record is selected for Mass Delete
Created By: Debasish Tapan Nag
Parameter:	1) frmObject:The Form Object
Return		1) true/ false
*/
function CheckIfRecordsSelectedForMassDelete(frmObject,a)
{
	var sCompIdList = 0;
	var boolAtleastOneSelected = false;
	if(frmObject.cbSelectAllRecords.checked)
	{
		sCompIdList = frmObject.txtAllRecordCompIds.value;
		boolAtleastOneSelected = true;
	}
	else
	{
		for(var iFieldIndex=0; iFieldIndex < frmObject.elements.length; iFieldIndex++)
		{
			elementName =  frmObject.elements[iFieldIndex].name;
			if(elementName.substring(0,4) == 'cbSR' && frmObject.elements[iFieldIndex].checked)
			{
				boolAtleastOneSelected = true;
				sCompIdList += ',' + frmObject.elements[iFieldIndex].value;
			}
		}
		if(boolAtleastOneSelected)
		sCompIdList = sCompIdList.substring(2,sCompIdList.length)
	}
	
		if(!boolAtleastOneSelected)
		{
			alert('Select the records before trying to delete.');
			return false;
		} else {
			frmObject.txtCheckedRecordCompIds.value = sCompIdList;
			if (a==0)
			{
				if(frmObject.cbSelectAllRecords.checked)
				return confirm('IMPORTANT !\nYou are about to delete ALL the records across ALL the pages you have selected. This means that ALL references that cascade from these records will be ENTIRELY Deleted ! Are you sure you want to do this ?');
				else if(frmObject.cbCheckAll.checked)
				return confirm('IMPORTANT !\nYou are about to delete ALL the records on this page. This means that ALL references that cascade from these records will be ENTIRELY Deleted ! Are you sure you want to do this ?');
				else
				return confirm('IMPORTANT !\nYou are about to delete some the records on this page. This means that ALL references that cascade from these records will be ENTIRELY Deleted ! Are you sure you want to do this ?');
			}
		
	}
	
}
/*
Purpose:	Triggered when the Show All checkbox is clicked from the "Filter Within" columns
Created By: Debasish Tapan Nag
Parameter:	1) thisChecked:The Status of the CheckBox
Return		1) None
*/
function setShowallColParam(thisChecked)
{
	document.getElementById('txtColValue').value=thisChecked?'ALL':'';
	document.getElementById('txtColValue').readOnly=thisChecked;
}
/*
Purpose:	Lead to Organization Details Page
Created By: Debasish Tapan Nag
Parameter:	1) numContactId: The Contact Id
Return		1) None
*/
function GoOrgDetails(numDivisionId)
{
	frames['IfrOpenOrgContact'].document.getElementById('hdRedirectionEntity').value='OrganizationFromDiv';
	frames['IfrOpenOrgContact'].document.getElementById('hdDivisionId').value = numDivisionId;
	if(!document.getElementById('cbOpenInSeparateWindow').checked)
	frames['IfrOpenOrgContact'].document.getElementById('hdRedirectionNewWindow').value = 0;
	frames['IfrOpenOrgContact'].document.forms['frmOrgContactRedirect'].submit();
}
/*
Purpose:	Lead to Contact Details Page
Created By: Debasish Tapan Nag
Parameter:	1) numContactId: The Contact Id
Return		1) None
*/
function GoContactDetails(numContactId)
{
	frames['IfrOpenOrgContact'].document.getElementById('hdRedirectionEntity').value='Contact';
	frames['IfrOpenOrgContact'].document.getElementById('hdContactId').value = numContactId;
	if(!document.getElementById('cbOpenInSeparateWindow').checked)
	frames['IfrOpenOrgContact'].document.getElementById('hdRedirectionNewWindow').value = 0;
	frames['IfrOpenOrgContact'].document.forms['frmOrgContactRedirect'].submit();
}
/*
Purpose:	The function to set the shipping address same as bitting address
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function MakeAddressShippingSameAsBilling(thisObject)
{
	if(thisObject.checked)
	{
		var arrBillFields = new Array('numBillCountry','vcBillStreet','numBillState','vcBillPostCode','vcBillCity');
		var arrShipFields = new Array('numShipCountry','vcShipStreet','numShipState','vcShipPostCode','vcShipCity');
		for(var iIndex = 0;iIndex < arrBillFields.length;iIndex++)
		{
			try{
				objElement = document.getElementById(arrBillFields[iIndex]);
				if(objElement.getAttribute('type').toLowerCase() == 'text')
				{
					document.getElementById(arrShipFields[iIndex]).value = document.getElementById(arrBillFields[iIndex]).value;
				}
				else if(objElement.getAttribute('type').toLowerCase() == 'select-one')
				{
					document.getElementById(arrShipFields[iIndex]).selectedIndex = document.getElementById(arrBillFields[iIndex]).selectedIndex;
					if(arrBillFields[iIndex] == 'numBillCountry')
					filterStates4Country(document.getElementById(arrShipFields[iIndex]));
				}
			}catch(e){}
		}
	}
}
/*
Purpose:	The function ensures that the selection of Leads Checkbox is consistant
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function MakeLeadsCheckBoxSelectionConsistant(cbElement)
{
	if(!cbElement.checked)
	{
		document.getElementById('ddlLead').selectedIndex = 0;
	}else
	{
		document.getElementById('ddlLead').selectedIndex = 1;	
	}
}
/*
Purpose:	The function ensures that the selection of Leads Checkbox is consistant
Created By: Debasish Tapan Nag
Parameter:	1) ddlElement: Leads drop down object
Return		1) None
*/
function MakeLeadsOptionSelectionConsistant()
{
	if(document.getElementById('ddlLead').selectedIndex == 0)
	{
		document.getElementById('cbSearchInLeads').checked = 0;
	}else{
		document.getElementById('cbSearchInLeads').checked = 1;
	}
}
////////////////////////////////////////////////////////////////////////////
///////////////////////ADVANCE SEARCH MASS UPDATE///////////////////////////
////////////////////////////////////////////////////////////////////////////
/*
Purpose:	The function opens popup window for the specifying updated value
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function OpenPopupForMassUpdate(frmObject)
{
	var sCompIdList = 0;
	var boolAtleastOneSelected = false;
	if(frmObject.cbSelectAllRecords.checked || frmObject.cbCheckAll.checked)
	{
		sCompIdList = frmObject.txtAllRecordCompIds.value;
		boolAtleastOneSelected = true;
	}
	else
	{
		for(var iFieldIndex=0; iFieldIndex < frmObject.elements.length; iFieldIndex++)
		{
			elementName =  frmObject.elements[iFieldIndex].name;
			if(elementName.substring(0,4) == 'cbSR' && frmObject.elements[iFieldIndex].checked)
			{
				boolAtleastOneSelected = true;
				sCompIdList += ',' + frmObject.elements[iFieldIndex].value;
			}
		}
		if(boolAtleastOneSelected)
		sCompIdList = sCompIdList.substring(2,sCompIdList.length)
	}
	if(!boolAtleastOneSelected)
	{
		alert('Select the records before trying to mass update.');
		return false;
	} else 
	{
		frmObject.txtCheckedRecordCompIds.value = sCompIdList;
	}	


	hnadw = window.open('frmAdvSearchMassUpdater.aspx','AdvSearchMassUpdater','toolbar=no,titlebar=no,left=200, top=200,width=300,height=85,scrollbars=yes,resizable=yes')
	hnadw.focus();
	return false;
}
////////////////////////////////////////////////////////////////////////////
///////////////////////ADVANCE SEARCH SURVEY RATING/////////////////////////
////////////////////////////////////////////////////////////////////////////
/*
Purpose:	The function to set the shipping address same as bitting address
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function OpenPopupForSurveyRating()
{
	hnadwSR = window.open('frmAdvSearchSurveyRatingFilter.aspx','AdvSearchSurveyRating','toolbar=no,titlebar=no,left=200, top=200,width=250,height=70,scrollbars=yes,resizable=yes')
	hnadwSR.focus();
	return false;
}
////////////////////////////////////////////////////////////////////////////
///////////////////////ADVANCE SEARCH COMMERCE MARKETING/////////////////////////
////////////////////////////////////////////////////////////////////////////
/*
Purpose:	The function initiates search for Accounts
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function InitiateSearch()
{
    var sControlsNames = 'cbOptionOne,cbOptionTwo,rbOptionThree';
    var sRanValidationControlsNames = 'ranValOptionOne,ranValOptionTwo,ranValOptionThree';
    var sReqValidationControlsNames = 'reqValOptionOne,reqValOptionTwo,reqValOptionThree';
    var sControlNameArray = new Array();
    sControlNameArray = sControlsNames.split(',');
    var sRanValidationControlsNameArray = new Array();
    sRanValidationControlsNameArray = sRanValidationControlsNames.split(',');
    var sReqValidationControlsNameArray = new Array();
    sReqValidationControlsNameArray = sReqValidationControlsNames.split(',');
    var boolSelectionValid=false;
    var Page_ValidatorObj = new Array();
    for(var iIndex = 0; iIndex < sControlNameArray.length;iIndex++)
    {
        if(document.getElementById(sControlNameArray[iIndex]).checked)
        {
            Page_ValidatorObj[Page_ValidatorObj.length] = document.getElementById(sRanValidationControlsNameArray[iIndex]);
            Page_ValidatorObj[Page_ValidatorObj.length] = document.getElementById(sReqValidationControlsNameArray[iIndex]);
            if(iIndex==1)
            {
                Page_ValidatorObj[Page_ValidatorObj.length] = document.getElementById('reqValDealAmt');
            }
            if(iIndex==2)
            {
                var sItemList='';
                var objSelectedItems = document.getElementById('lstSelectedfld');
                for(var itemIndex=0;itemIndex<objSelectedItems.length;itemIndex++)
                {
                    sItemList = sItemList + ',' + objSelectedItems.options[itemIndex].value;
                }
                sItemList = sItemList.substring(1,sItemList.length)
                if(sItemList == '')
                {
                    ShowItemList();
                    alert('Please select one or more items.');
                    return false;
                }
                document.getElementById('hdItemList').value = sItemList;
            }
            boolSelectionValid = true;
        }
        if(!boolSelectionValid && iIndex == sControlNameArray.length-1)
        {
            alert('Please select from one of the available options.');
            return false;
        }
    }
    Page_Validators = Page_ValidatorObj;
	Page_ClientValidate();							//explcitly call for validation
    return true;
}
/*
Purpose:	Closes the present window
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function CloseWin()
{
	this.close();
	return false;
}
/*
Purpose:	Unchecks the other checkboxes
Created By: Debasish Tapan Nag
Parameter:	1) sControlsNames: The control names which has to be unchecked
Return		1) None
*/
function UncheckOtherFilteration(sControlsNames)
{
    var sControlNameArray = new Array();
    sControlNameArray = sControlsNames.split(',');
    for(var iIndex = 0; iIndex < sControlNameArray.length;iIndex++)
    {
        document.getElementById(sControlNameArray[iIndex]).checked = 0;
    }
}
/*
Purpose:	ClientCallback for item list
Created By: Debasish Tapan Nag
Parameter:	1) None
Return		1) None
*/
function  ShowItemList()
{
    if(document.getElementById('trItemList').style.display=='none')
    {
        
        document.getElementById('trItemList').style.display='inline';
    }else{
        
        document.getElementById('trItemList').style.display='none';
    }
}