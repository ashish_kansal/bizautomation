﻿
function InitializeValidation() {
    var validator = $("#form1").bind("invalid-form.validate", function () { }).validate(
    {
        errorElement: "em",
        errorLabelContainer: "#messagebox",
        wrapper: "li"
    });

    // INTEGER VALUE TEXTBOX
    $('body').delegate('.required_integer','keypress',function (e, text) {
        var keynum;
        var keychar;
        var regEx;
        var allowedKeyNums = [44, 46, 8, 37, 39, 16, 9, 110, 35, 36, 46]; // Backspace, Tab, End, Home, (Delete & period)

        var k;
        k = (e.which) ? e.which : event.keyCode;
        keynum = k;

        if (k == 8 && e.which) // Netscape/Firefox/Opera
            return true;
        keychar = String.fromCharCode(keynum);
        regEx = /^\d+$/;   // Undesirable characters

        // Test for keynum values that collide with undesirable characters
        if ($.inArray(keynum, allowedKeyNums) > -1)
            return regEx.test(keychar);

        regEx = /^\d+$/;
        return regEx.test(keychar);
    });

        // DECIMAL VALUE TEXTBOX 
    $('body').delegate('.required_decimal','blur',function () {
        //alert($(".required_decimal").val());
        var origText;
        origText = $(this).val();
        if (origText.length > 0) {
            if (origText.charAt(origText.length - 1) == ".") {
                origText = origText + "00";
                $(this).val(origText);
                //$(this).focus();
            }
        }
    });

    $('body').delegate('.required_decimal','keypress',function (e, text) {
        var keynum;
        var keychar;
        var regEx;
        var allowedKeyNums = [44, 8, 37, 39, 16, 9, 110, 190, 35, 36, 46]; // Backspace, Tab, End, Home, (Delete & period)

        var k;
        k = (e.which) ? e.which : event.keyCode;
        keynum = k;
        if (k == 8 && e.which || k == 46) // Netscape/Firefox/Opera
            return true;

        keychar = String.fromCharCode(keynum);
        regEx = /^\d\..+$/;  // Undesirable characters

        // Test for keynum values that collide with undesirable characters
        if ($.inArray(keynum, allowedKeyNums) > -1)
            return regEx.test(keychar);

        regEx = /^\d+$/
        return regEx.test(keychar);
    });

    // DECIMAL VALUE TEXTBOX
    $('body').delegate('.required_money','keypress',function (e, text) {
        var keynum;
        var keychar;
        var regEx;
        var allowedKeyNums = [44, 8, 37, 39, 16, 9, 110, 190, 35, 36, 46, 45]; // Backspace, Tab, End, Home, (Delete & period)

        var k;
        k = (e.which) ? e.which : event.keyCode;
        keynum = k;
        if (k == 8 && e.which || k == 46) // Netscape/Firefox/Opera
            return true;

        keychar = String.fromCharCode(keynum);
        regEx = /^\d\..+$/;  // Undesirable characters
        //regEx = /^(-)?\d+(\.\d{1,4})?$/;

        // Test for keynum values that collide with undesirable characters
        if ($.inArray(keynum, allowedKeyNums) > -1)
            return keychar;

        regEx = /^\d+$/
        return regEx.test(keychar);
    });

    //$('body').delegate('.required_money', 'keyup', function (e, text) {
    //    var val = $(this).val();
    //    var regex = /[^(-)?\d+(\.\d{1,4})?$]/g;
    //    if (val.match(regex)) {
    //        $(this).css("background", "red");
    //        val = val.replace(regex, "");
    //        $(this).val(val);
    //    }
    //});
}

// To Set msg as per id,class,message,fadeOutTime defined.
function UpdateMsgStatus(id, className, message, fadeOutTime) {
    $(id).text(message);
    $(id).addClass(className);
    if (fadeOutTime > 0) {
        $(id).fadeIn().delay(fadeOutTime).fadeOut();
    }
}


