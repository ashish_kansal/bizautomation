﻿function BindInlineEdit() {
    $(".editable_select").editable("../include/SaveData.ashx", {
        loadurl: "../include/JSONSelect.ashx",
        type: "select",
        onsubmit: function (settings, td) {
            var input = $(td).find('SELECT');

            var iIndex = FindArrayIndex(td);

            if (iIndex > -1) {
                if (arrFieldConfig[iIndex].bitIsRequired == 'True') { $(td).attr('requiredfielddropdown', arrFieldConfig[iIndex].vcFieldMessage); }
            }

            var original = input.val();
            if ($(td).attr('requiredfielddropdown') != null) {
                if (RequiredFieldDropDown_Inline(original, $(td).attr('requiredfielddropdown'))) {
                    return true;
                } else {
                    input.css('background-color', '#c00').css('color', '#fff');
                    return false;
                }
            }
        } 
    });
    $(".editable_CheckBox").editable("../include/SaveData.ashx", {
        type: "CheckBox",
      
    });
    $(".editable_textarea").editable("../include/SaveData.ashx", {
        type: 'textarea',
        submitdata: { _method: "put" },
        select: true,
        rows: "5",
        cols: "30",
        onsubmit: function (settings, td) {
            var input = $(td).find('textarea');

            var original = input.val();
            var iIndex = FindArrayIndex(td);

            if (iIndex > -1) {
                if (arrFieldConfig[iIndex].bitIsRequired == 'True') { $(td).attr('requiredfield', arrFieldConfig[iIndex].vcFieldMessage); }
            }

            if ($(td).attr('requiredfield') != null) {
                if (RequiredField_Inline(original, $(td).attr('requiredfield'))) {
                    return true;
                } else {
                    input.css('background-color', '#c00').css('color', '#fff');
                    return false;
                }
            }
        }
    });

    $(".editable_Amount").editable("../include/SaveData.ashx", {
        width: "130px",
        type: "Amount",
        onsubmit: function (settings, td) {
            var input = $(td).find('input');
            input.val($.trim(input.val()));

        if(validateInlineEdit(td)){
                return true; }
            
            return false;
        }
    });

    

    $(".editable_Number").editable("../include/SaveData.ashx", {
        width: "130px",
        type: "Number",
        onsubmit: function (settings, td) {
            var input = $(td).find('input');
            input.val($.trim(input.val()));

           if(validateInlineEdit(td)){
                return true; }
            
            return false;
        }
    });

    $(".click").editable("../include/SaveData.ashx", {
        width: "130px",
        onsubmit: function (settings, td) {
            var input = $(td).find('input');
            input.val($.trim(input.val()));

          if(validateInlineEdit(td)){
                return true; }
            
            return false;
        }
    });

    $(".editable_DateField").editable("../include/SaveData.ashx", {
        type: 'DateField',
        width: "100px"
    });
}


function validateInlineEdit(td)
    {
        var input = $(td).find('input');
            var original = input.val();

            var iIndex = FindArrayIndex(td);
            if (iIndex > -1) {

                if (arrFieldConfig[iIndex].bitIsRequired == 'True') { $(td).attr('requiredfield', arrFieldConfig[iIndex].vcFieldMessage); }

                if (arrFieldConfig[iIndex].bitIsNumeric == 'True') {  $(td).attr('isnumeric', arrFieldConfig[iIndex].vcFieldMessage); }

                if (arrFieldConfig[iIndex].bitIsAlphaNumeric == 'True') { $(td).attr('isalphanumeric', arrFieldConfig[iIndex].vcFieldMessage); }

                if (arrFieldConfig[iIndex].bitIsEmail == 'True') { $(td).attr('checkmail', arrFieldConfig[iIndex].vcFieldMessage); }

                if (arrFieldConfig[iIndex].bitIsLengthValidation == 'True') {
                    $(td).attr('rangevalidator', arrFieldConfig[iIndex].vcFieldMessage);
                    $(td).attr('MinValue', arrFieldConfig[iIndex].intMinLength);
                    $(td).attr('MaxValue', arrFieldConfig[iIndex].intMaxLength);
                }
            }

            if ($(td).attr('requiredfield') != null) {
                if (RequiredField_Inline(original, $(td).attr('requiredfield'))) {

                } else {
                    input.css('background-color', '#c00').css('color', '#fff');
                    return false;
                }
            }

            if ($(td).attr('checkmail') != null) {
                if (CheckMail_Inline(original, $(td).attr('checkmail'))) {

                } else {
                    input.css('background-color', '#c00').css('color', '#fff');
                    return false;
                }
            }

            if ($(td).attr('isnumeric') != null) {
                if (IsNumeric_Inline(original, $(td).attr('isnumeric'))) {

                } else {
                    input.css('background-color', '#c00').css('color', '#fff');
                    return false;
                }
            }

            if ($(td).attr('isalphanumeric') != null) {
                if (IsAlphaNumeric_Inline(original, $(td).attr('isalphanumeric'))) {

                } else {
                    input.css('background-color', '#c00').css('color', '#fff');
                    return false;
                }
            }

            if ($(td).attr('rangevalidator') != null) {
                if (RangeValidator_Inline(original, $(td).attr('MinValue'), $(td).attr('MaxValue'), $(td).attr('rangevalidator'))) {

                } else {
                    input.css('background-color', '#c00').css('color', '#fff');
                    return false;
                }
            }

            if ($(td).attr('requiredfielddropdown') != null) {
                if (RequiredFieldDropDown_Inline(original, $(td).attr('requiredfielddropdown'))) {

                } else {
                    input.css('background-color', '#c00').css('color', '#fff');
                    return false;
                }
            }

            return true;
    }

function FindArrayIndex(td) {
    var currentId = $(td).attr('id');
    var substr = currentId.split('~');
    var FormFieldId = substr[1];
    var bitCustomField = substr[2];

    for (var iIndex = 0; iIndex < arrFieldConfig.length; iIndex++) {
        if (arrFieldConfig[iIndex].numFormFieldId == FormFieldId && arrFieldConfig[iIndex].bitCustomField == bitCustomField) {
            return iIndex;
        }
    }

    return -1;
}

function RequiredField_Inline(value,errMsg) {
    if (trim(value).length == 0) { alert(errMsg); return false; }
    return true;
}

function RequiredFieldDropDown_Inline(value, errMsg) {
    if (value == "0") { alert(errMsg);  return false; }
    return true;
}

function CheckMail_Inline(value, errMsg) {
    if (value == null || !value.toString().match(/^\w+[\+\.\w-]*@([\w-]+\.)*\w+[\w-]*\.([a-z]{2,4}|\d+)$/i)) { alert(errMsg); return false; }
    return true;
}

function IsNumeric_Inline(value, errMsg) {
    if (value == null || !value.toString().match(/(^\d+$)|(^\d+\.\d+$)/)) { alert(errMsg); return false; }
    return true;
}

function IsAlphaNumeric_Inline(value, errMsg) {
    for (var j = 0; j < value.length; j++) {
        var alphaa = value.charAt(j);
        var hh = alphaa.charCodeAt(0);
        if ((hh > 47 && hh < 58) || (hh > 64 && hh < 91) || (hh > 96 && hh < 123)) {
        }
        else {
            alert(errMsg); return false;
        }
    }
    return true;
}

function RangeValidator_Inline(value, MinValue, MaxValue, errMsg) {

    var numaric;
    numaric = parseInt(trim(value));

    if (trim(value) == '') {
        return true;
    }
    if (isNaN(numaric)) {
        if (IsNumeric_Inline(value) == false) {
            alert(errMsg);
            return false;
        }
    }
    if (!isNaN(numaric)) {
        //validate if numeric first
        MinValue = parseInt(MinValue);
        MaxValue = parseInt(MaxValue);
        if (!(numaric <= MaxValue && numaric >= MinValue)) {
            alert(errMsg);
            return false;
        }
    }
    return true;
}

function trim(str, chars) {
    return ltrim(rtrim(str, chars), chars);
}

function ltrim(str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}

function rtrim(str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}


function InlineEditValidation(numFormFieldId, bitCustomField, bitIsRequired, bitIsNumeric, bitIsAlphaNumeric, bitIsEmail, bitIsLengthValidation, intMaxLength, intMinLength, vcFieldMessage) {
    this.numFormFieldId = numFormFieldId;
    this.bitCustomField = bitCustomField;
    this.bitIsRequired = bitIsRequired;
    this.bitIsNumeric = bitIsNumeric;
    this.bitIsAlphaNumeric = bitIsAlphaNumeric;
    this.bitIsEmail = bitIsEmail;
    this.bitIsLengthValidation = bitIsLengthValidation;
    this.intMaxLength = intMaxLength;
    this.intMinLength = intMinLength;
    this.vcFieldMessage = vcFieldMessage;
}


function CheckValidNumber(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            if (cint == 1) {
                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
        }



    function validateDate(CalDate, format) {
        if (!isDate(document.getElementById(CalDate).value, format)) {
            alert("Enter Valid Date");
        }
    }

