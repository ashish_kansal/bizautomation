﻿
$(document).ready(function () {
    $('.BizDocStatus').change(function () {
        var BizDocStatusval = $(this).val();
        //console.log(BizDocStatusval);

        var OppID = $(this).parent().parent().find("[id*='txtOppID']").val();
        //console.log(OppID);

        var BizDocID = $(this).parent().parent().find("[id*='txtBizDocID']").val();
        //console.log(BizDocID);

        var BizDocTypeID = $(this).parent().parent().find("[id*='lblBizDocID']").text();
        //console.log(BizDocTypeID);

        var oldBizDocStatus = $(this).parent().parent().find("[id*='txtBizDocStatus']").val();

        //var iRuleID = FindSFRule(BizDocTypeID, BizDocStatusval);
        var iRuleID = 0;
        //console.log(iRuleID);

        //if (iRuleID > 0) {
        //    var message = '';
        //    if (iRuleID == 1) {
        //        message = 'This will create invoice for items within selected fulfillment order(s).';
        //    }
        //    else if (iRuleID == 2) {
        //        message = 'This will create packing slip for items within selected fulfillment order(s).';
        //    }
        //    else if (iRuleID == 3) {
        //        message = 'This will clear inventory from allocation for items within this fulfillment order.';
        //    }
        //    else if (iRuleID == 4) {
        //        message = 'This will put the inventory back on allocation (which also impacts the cost of sale).';
        //    }
        //    else if (iRuleID == 5) {
        //        message = 'This will Remove the Fulfillment Order from the Sales Fulfillment queue.';
        //    }

        //    var yes = "<a href='#' onclick=\"return UpdateBizDocStatus(\'" + $(this).attr("id") + "\',\'" + OppID + "\',\'" + BizDocID + "\',\'" + BizDocStatusval + "\',\'" + oldBizDocStatus + "\',\'" + iRuleID + "\')\">Yes</a>";
        //    var no = "<a href='#' onclick=\"return CancelBizDocStatus(\'" + $(this).attr("id") + "\',\'" + OppID + "\',\'" + BizDocID + "\',\'" + BizDocStatusval + "\',\'" + oldBizDocStatus + "\',\'" + iRuleID + "\');\">No</a>";

        //    message = message + " Are you sure you want to proceed ? " + yes + "&nbsp;&nbsp;" + no;

        //    $('#lblmsg').html(message);

        //    $('#lblmsg').removeClass('errorInfo');
        //    $('#lblmsg').removeClass('successInfo');
        //    $('#lblmsg').removeClass('info');

        //    $('#lblmsg').addClass('info');
        //    $('#lblmsg').fadeIn();

        //    $(".dg tr").not(".hs,.fs").each(function () {
        //        ddlBizDocStatus = $(this).find("[id*='ddlBizDocStatus']");
        //        $(ddlBizDocStatus).attr("disabled", true);
        //    });
        //}
        //else {
        UpdateBizDocStatus($(this).attr("id"), OppID, BizDocID, BizDocStatusval, oldBizDocStatus, iRuleID);
        //}
    });
});

function UpdateBizDocStatus(ddlBizDocStatus, OppID, BizDocID, BizDocStatusval, oldBizDocStatus, iRuleID) {
    $.ajax({
        type: "POST",
        url: "../common/Common.asmx/UpdateBizDocStatus",
        data: "{lngOppID:'" + OppID + "',lngOppBizDocsId:'" + BizDocID + "',lngBizDocStatus:'" + BizDocStatusval + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            // Insert the returned HTML into the <div>.
            UpdateMsgStatus(msg.d);

            $(".dg tr").not(".hs,.fs").each(function () {
                $(this).find("[id*='ddlBizDocStatus']").attr("disabled", false)
            });

            if ((msg.d.toLowerCase().indexOf("error") >= 0)) {
                CancelBizDocStatus(ddlBizDocStatus, OppID, BizDocID, BizDocStatusval, oldBizDocStatus);
            }
            else {
                //if (iRuleID == 1 || iRuleID == 2)
                //    __doPostBack('btnReloadGrid', '');
                $('#' + ddlBizDocStatus).parent().parent().find("[id*='txtBizDocStatus']").val(BizDocStatusval);
            }
        }
    });
}

function CancelBizDocStatus(ddlBizDocStatus, OppID, BizDocID, BizDocStatusval, oldBizDocStatus, iRuleID) {
    $(".dg tr").not(".hs,.fs").each(function () {
        $(this).find("[id*='ddlBizDocStatus']").attr("disabled", false);
    });

    $('#' + ddlBizDocStatus).val(oldBizDocStatus);

    $('#lblmsg').fadeOut();
}

function FindSFRule(BizDocTypeID, BizDocStatusval) {
    if (BizDocStatusval > 0) {
        for (var iIndex = 0; iIndex < arrFulfillmentRulesFieldConfig.length; iIndex++) {
            if (arrFulfillmentRulesFieldConfig[iIndex].numBizDocTypeID == BizDocTypeID && arrFulfillmentRulesFieldConfig[iIndex].numBizDocStatus == BizDocStatusval) {
                return arrFulfillmentRulesFieldConfig[iIndex].numRuleID;
            }
        }
    }

    return -1;
}

function UpdateMsgStatus(message) {
    $('#lblmsg').text(message);

    $('#lblmsg').removeClass('errorInfo');
    $('#lblmsg').removeClass('successInfo');
    $('#lblmsg').removeClass('info');

    if (message.toLowerCase().indexOf("error") >= 0)
        $('#lblmsg').addClass('errorInfo');
    else
        $('#lblmsg').addClass('successInfo');

    $('#lblmsg').fadeIn().delay(5000).fadeOut();
}

function FulfillmentRules(numRuleID, numBizDocTypeID, numBizDocStatus) {
    this.numRuleID = numRuleID;
    this.numBizDocTypeID = numBizDocTypeID;
    this.numBizDocStatus = numBizDocStatus;
}