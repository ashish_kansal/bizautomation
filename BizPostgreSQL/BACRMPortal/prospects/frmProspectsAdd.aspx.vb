Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Common

Public Class frmProspectsAdd : Inherits BACRMPage

    Dim strAdd As String = ""
    Dim lngDivID As Integer

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            chkSameAddr.Attributes.Add("onclick", "fn_SameAddress();")
            lngDivID = GetQueryStringVal( "rtyWR")
            If lngDivID = 0 Then lngDivID = Session("DivId")
            If Not IsPostBack Then
                Session("Help") = "Organization"
                Dim objCommon As New CCommon
                objCommon.sb_FillComboFromDBwithSel(ddlBillCountry, 40, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlShipCountry, 40, Session("DomainID"))
                LoadAddress()
            End If
            btnCancel.Attributes.Add("onclick", "return Close();")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadAddress()
        Try
            Dim dtTable As DataTable
            Dim objContacts As New CContacts
            objContacts.DivisionID = lngDivID
            objContacts.DomainID = Session("DomainID")
            dtTable = objContacts.GetBillOrgorContAdd
            txtStreet.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcBillstreet")), "", dtTable.Rows(0).Item("vcBillstreet"))
            txtCity.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcBillCity")), "", dtTable.Rows(0).Item("vcBillCity"))
            If Not IsDBNull(dtTable.Rows(0).Item("vcBillCountry")) Then
                If Not ddlBillCountry.Items.FindByValue(dtTable.Rows(0).Item("vcBillCountry")) Is Nothing Then
                    ddlBillCountry.Items.FindByValue(dtTable.Rows(0).Item("vcBillCountry")).Selected = True
                End If
            End If
            If ddlBillCountry.SelectedIndex > 0 Then
                FillState(ddlBillState, ddlBillCountry.SelectedItem.Value, Session("DomainID"))
                If Not IsDBNull(dtTable.Rows(0).Item("vcBilState")) Then
                    If Not ddlBillState.Items.FindByValue(dtTable.Rows(0).Item("vcBilState")) Is Nothing Then
                        ddlBillState.Items.FindByValue(dtTable.Rows(0).Item("vcBilState")).Selected = True
                    End If
                End If
            End If
            txtPostal.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcBillPostCode")), "", dtTable.Rows(0).Item("vcBillPostCode"))
            If Not IsDBNull(dtTable.Rows(0).Item("bitSameAddr")) Then
                If dtTable.Rows(0).Item("bitSameAddr") = 0 Then
                    chkSameAddr.Checked = False
                Else : chkSameAddr.Checked = True
                End If
            End If
            txtShipStreet.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcShipStreet")), "", dtTable.Rows(0).Item("vcShipStreet"))
            txtShipCity.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcShipCity")), "", dtTable.Rows(0).Item("vcShipCity"))
            If Not IsDBNull(dtTable.Rows(0).Item("vcShipCountry")) Then
                If Not ddlShipCountry.Items.FindByValue(dtTable.Rows(0).Item("vcShipCountry")) Is Nothing Then
                    ddlShipCountry.Items.FindByValue(dtTable.Rows(0).Item("vcShipCountry")).Selected = True
                End If
            End If
            If ddlShipCountry.SelectedIndex > 0 Then
                FillState(ddlShipState, ddlShipCountry.SelectedItem.Value, Session("DomainID"))
                If Not IsDBNull(dtTable.Rows(0).Item("vcShipState")) Then
                    If Not ddlShipState.Items.FindByValue(dtTable.Rows(0).Item("vcShipState")) Is Nothing Then
                        ddlShipState.Items.FindByValue(dtTable.Rows(0).Item("vcShipState")).Selected = True
                    End If
                End If
            End If
            txtShipPostal.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcShipPostCode")), "", dtTable.Rows(0).Item("vcShipPostCode"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            save()
            Dim strScript As String = "<script language=JavaScript>window.opener.FillAddress('" & strAdd & "');</script>"
            If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            save()
            Dim strScript As String = "<script language=JavaScript>window.opener.FillAddress('" & strAdd & "');self.close()</script>"
            If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub save()
        Try
            Dim objContacts As New CContacts
            objContacts.BillStreet = txtStreet.Text
            objContacts.BillCity = txtCity.Text
            objContacts.BillCountry = ddlBillCountry.SelectedItem.Value
            objContacts.BillPostal = txtPostal.Text
            objContacts.BillState = ddlBillState.SelectedItem.Value
            objContacts.BillingAddress = IIf(chkSameAddr.Checked = True, 1, 0)
            objContacts.ShipStreet = txtShipStreet.Text
            objContacts.ShipState = ddlShipState.SelectedItem.Value
            objContacts.ShipPostal = txtShipPostal.Text
            objContacts.ShipCountry = ddlShipCountry.SelectedItem.Value
            objContacts.ShipCity = txtShipCity.Text
            objContacts.DivisionID = lngDivID
            objContacts.UpdateCompanyAddress()

            If txtStreet.Text <> "" Then strAdd = txtStreet.Text
            If txtCity.Text <> "" Then
                If strAdd = "" Then
                    strAdd = txtCity.Text
                Else : strAdd = strAdd & "," & txtCity.Text
                End If
            End If
            If ddlBillState.SelectedValue <> 0 Then
                If strAdd = "" Then
                    strAdd = ddlBillState.SelectedItem.Text
                Else : strAdd = strAdd & "," & ddlBillState.SelectedItem.Text
                End If
            End If
            If txtPostal.Text <> "" Then
                If strAdd = "" Then
                    strAdd = txtPostal.Text
                Else : strAdd = strAdd & "," & txtPostal.Text
                End If
            End If
            If ddlBillCountry.SelectedValue <> 0 Then
                If strAdd = "" Then
                    strAdd = ddlBillCountry.SelectedItem.Text
                Else : strAdd = strAdd & "," & ddlBillCountry.SelectedItem.Text
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlShipCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShipCountry.SelectedIndexChanged
        Try
            FillState(ddlShipState, ddlShipCountry.SelectedItem.Value, Session("DomainID"))
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlBillCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBillCountry.SelectedIndexChanged
        Try
            FillState(ddlBillState, ddlBillCountry.SelectedItem.Value, Session("DomainID"))
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class

