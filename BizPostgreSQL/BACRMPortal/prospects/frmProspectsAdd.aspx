<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmProspectsAdd.aspx.vb" Inherits="BACRMPortal.frmProspectsAdd"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1"    runat="server">		
        <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />

		<title>Address</title>

		<script language="javascript">
			function CheckNumber()
					{
							if (!(window.event.keyCode > 47 && window.event.keyCode < 58))
							{
								window.event.keyCode=0;
							}
					}
	function Close()
	{
	    window.close()
	    return false;
	}
	
	
		function fn_SameAddress()
		{
			if(document.Form1.chkSameAddr.checked==true)
			{
				document.Form1.txtShipPostal.value=document.Form1.txtPostal.value;
				document.Form1.txtShipStreet.value=document.Form1.txtStreet.value;
				document.Form1.txtShipCity.value=document.Form1.txtCity.value;
				Remove(document.Form1.ddlShipState)
				for(var i=0; i<document.Form1.ddlBillState.options.length; i++)
				{
				  
				    var no = new Option();
					no.value = document.Form1.ddlBillState.options[i].value
					no.text = document.Form1.ddlBillState.options[i].text
				    document.Form1.ddlShipState.options[i]=no;
				 
				} 
				document.Form1.ddlShipState.selectedIndex=document.Form1.ddlBillState.selectedIndex
				document.Form1.ddlShipCountry.selectedIndex=document.Form1.ddlBillCountry.selectedIndex
			}
			else
			{
				document.Form1.txtShipPostal.value='';
				document.Form1.txtShipStreet.value='';
				document.Form1.txtShipCity.value='';
				document.Form1.ddlShipCountry.selectedIndex=0
				document.Form1.ddlShipState.selectedIndex=0
			}
		}
		function Remove(box) 
			{
			for(var i=0; i<box.options.length; i++) 
			{
			
				for(var j=i; j<box.options.length-1; j++)  
				{
				box.options[j].value = box.options[j+1].value;
				box.options[j].text = box.options[j+1].text;
				}
				var ln = i;
				break;
			}
			if(ln < box.options.length) 
			{
			box.options.length -= 1;
			Remove(box);
			}
			}
		</script>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
		<table width="100%" border="0">
							<tr>
								<td align="right" >
									<asp:Button ID="btnSave" Runat="server" CssClass="button" text="Save"></asp:Button>
									<asp:Button ID="btnSaveClose" Runat="server" CssClass="button" Width="80" text="Save &amp; Close"></asp:Button>
									<asp:Button ID="btnCancel" Runat="server" CssClass="button" Width="50" text="Close"></asp:Button>
								</td>
							</tr>
		</table> 
		<asp:table id="Table5" Width="100%" Runat="server" BorderWidth="1" GridLines="None" CssClass="aspTable"
				BorderColor="black">
				<asp:TableRow>
					<asp:TableCell VerticalAlign="Top">
					<table width="100%" border="0">
							<tr>
							<tr>
								<td class="text_bold" colSpan="2">&nbsp;Billing Address</td>
								<td class="text" colSpan="2"><b>Shipping Address</b><asp:checkbox id="chkSameAddr" text="Same as Billing" Runat="server"></asp:checkbox>
									</td>
								<td vAlign="top" align="right"> </td>
							</tr>
							<tr>
								<td class="text" align="right">Street</td>
								<td><asp:textbox id="txtStreet" tabIndex="10" runat="server" cssclass="signup" textMode=MultiLine  width="200"></asp:textbox></td>
								<td class="text" align="right">Street</td>
								<td class="text"><asp:textbox id="txtShipStreet" tabIndex="15" runat="server" textMode=MultiLine  cssclass="signup" width="200"></asp:textbox></td>
							</tr>
							<tr>
								<td class="text" align="right">City</td>
								<td><asp:textbox id="txtCity" tabIndex="11" runat="server" cssclass="signup" width="200"></asp:textbox></td>
								<td class="text" align="right">City</td>
								<td class="text"><asp:textbox id="txtShipCity" tabIndex="16" runat="server" cssclass="signup" width="200"></asp:textbox></td>
							</tr>
							<tr>
								<td class="text" align="right">State</td>
								<td><asp:DropDownList ID="ddlBillState" Runat="server" tabIndex="12" Width="200" CssClass="signup">
								<asp:ListItem Value=0>--Select One--</asp:ListItem>
								</asp:DropDownList></td>
								<td class="text" align="right">State</td>
								<td class="text"><asp:DropDownList ID="ddlShipState" tabIndex="17" Runat="server" Width="200" CssClass="signup">
								<asp:ListItem Value=0>--Select One--</asp:ListItem>
								</asp:DropDownList></td>
							</tr>
							<tr>
								<td class="text" align="right">Postal</td>
								<td><asp:textbox id="txtPostal" tabIndex="13" runat="server" cssclass="signup" width="200"></asp:textbox></td>
								<td class="text" align="right">Postal</td>
								<td class="text"><asp:textbox id="txtShipPostal" tabIndex="18" runat="server" cssclass="signup" width="200"></asp:textbox></td>
							</tr>
							<tr>
								<td class="text" align="right">Country</td>
								<td><asp:DropDownList ID="ddlBillCountry" tabIndex="14" AutoPostBack=True  Runat="server" Width="200" CssClass="signup">
								<asp:ListItem Value=0>--Select One--</asp:ListItem>
								</asp:DropDownList></td>
								<td class="text" align="right">Country</td>
								<td class="text"><asp:DropDownList ID="ddlShipCountry" tabIndex="19" AutoPostBack=True  Runat="server" Width="200" CssClass="signup">
								<asp:ListItem Value=0>--Select One--</asp:ListItem>
								</asp:DropDownList></td>
							</tr>
						</table>
					
					</asp:TableCell>
			</asp:TableRow>
		</asp:table>
		
		</form>
	</body>
</html>
