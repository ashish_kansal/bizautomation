Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts

Partial Public Class frmConfCompanyList : Inherits BACRMPage

    Dim objCommon As New CCommon
    Dim objContact As CContacts
    Dim numformId As Int16 = 11

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GetQueryStringVal( "FormId") = "12" Then
                numformId = 12
            ElseIf GetQueryStringVal( "FormId") = "13" Then
                numformId = 13
            End If
            btnSave.Attributes.Add("onclick", "Save()")
            If Not IsPostBack Then
                If numformId = 11 Then
                    lblName.Text = "Organization Layout"
                    objCommon.sb_FillComboFromDBwithSel(ddlRelationship, 5, Session("DomainID"))
                    objCommon.sb_FillComboFromDBwithSel(ddlFollow, 30, Session("DomainID"))
                    ddlRelationship.Items.RemoveAt(1)
                    ddlRelationship.Items.Insert(1, "Leads")
                    ddlRelationship.Items.FindByText("Leads").Value = "1"
                    ddlRelationship.Items.Insert(2, "Accounts")
                    ddlRelationship.Items.FindByText("Accounts").Value = "2"
                    ddlRelationship.Items.Insert(3, "Prospects")
                    ddlRelationship.Items.FindByText("Prospects").Value = "3"
                    If GetQueryStringVal( "RelId") <> "" Then
                        If Not ddlRelationship.Items.FindByValue(GetQueryStringVal( "RelId")) Is Nothing Then
                            ddlRelationship.SelectedItem.Selected = False
                            ddlRelationship.Items.FindByValue(GetQueryStringVal( "RelId")).Selected = True
                        End If
                    End If
                    pnlRelation.Visible = True
                    pnlCase.Visible = False
                    pnlPro.Visible = False
                ElseIf numformId = 12 Then
                    lblName.Text = "Cases Layout"
                    pnlRelation.Visible = False
                    pnlCase.Visible = True
                    pnlPro.Visible = False
                    ddlRelationship.Visible = False
                ElseIf numformId = 13 Then
                    lblName.Text = "Projects Layout"
                    pnlRelation.Visible = False
                    pnlCase.Visible = False
                    ddlRelationship.Visible = False
                    pnlPro.Visible = True
                End If
                BindLists()
                ManageFilters()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindLists()
        Try
            Dim ds As DataSet
            objContact = New CContacts
            objContact.DomainID = Session("DomainId")
            objContact.FormId = numformId
            objContact.UserCntID = Session("UserContactId")
            If numformId = 12 Or numformId = 13 Then
                objContact.ContactType = 0
            Else : objContact.ContactType = ddlRelationship.SelectedValue
            End If

            ds = objContact.GetColumnConfiguration()
            lstAvailablefld.DataSource = ds.Tables(0)
            lstAvailablefld.DataTextField = "vcFormFieldName"
            lstAvailablefld.DataValueField = "numFormFieldID"
            lstAvailablefld.DataBind()
            lstSelectedfld.DataSource = ds.Tables(1)
            lstSelectedfld.DataValueField = "numFormFieldID"
            lstSelectedfld.DataTextField = "vcFormFieldName"
            lstSelectedfld.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlRelationship_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRelationship.SelectedIndexChanged
        Try
            ManageFilters()
            BindLists()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub ManageFilters()
        Try
            If numformId = 11 Then
                Select Case ddlRelationship.SelectedValue
                    Case 0
                        spnLead.Visible = False
                        ddlLeadType.Visible = False
                        ddlAccountFilter.Visible = False
                        ddlCompanyFilter.Visible = False
                        ddlProspectFilter.Visible = False
                        ddlFollow.Visible = False
                    Case 1
                        spnLead.Visible = True
                        ddlLeadType.Visible = True
                        ddlFollow.Visible = True
                        ddlAccountFilter.Visible = False
                        ddlCompanyFilter.Visible = False
                        ddlProspectFilter.Visible = False
                    Case 2
                        spnLead.Visible = False
                        ddlLeadType.Visible = False
                        ddlFollow.Visible = False
                        ddlAccountFilter.Visible = True
                        ddlCompanyFilter.Visible = False
                        ddlProspectFilter.Visible = False
                    Case 3
                        spnLead.Visible = False
                        ddlLeadType.Visible = False
                        ddlFollow.Visible = False
                        ddlAccountFilter.Visible = False
                        ddlCompanyFilter.Visible = False
                        ddlProspectFilter.Visible = True
                    Case Else
                        spnLead.Visible = False
                        ddlLeadType.Visible = False
                        ddlFollow.Visible = False
                        ddlAccountFilter.Visible = False
                        ddlCompanyFilter.Visible = True
                        ddlProspectFilter.Visible = False
                End Select
                pnlPro.Visible = False
            ElseIf numformId = 12 Then
                pnlCase.Visible = True
                pnlPro.Visible = False
            ElseIf numformId = 13 Then
                pnlCase.Visible = False
                pnlPro.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If objContact Is Nothing Then objContact = New CContacts

            Dim dsNew As New DataSet
            Dim dtTable As New DataTable
            dtTable.Columns.Add("numFormFieldID")
            dtTable.Columns.Add("bitCustom")
            dtTable.Columns.Add("tintOrder")

            Dim i As Integer
            Dim dr As DataRow
            Dim str As String()
            str = hdnCol.Value.Split(",")
            For i = 0 To str.Length - 2
                dr = dtTable.NewRow
                dr("numFormFieldID") = str(i).Split("~")(0)
                dr("bitCustom") = str(i).Split("~")(1)
                dr("tintOrder") = i
                dtTable.Rows.Add(dr)
            Next

            dtTable.TableName = "Table"
            dsNew.Tables.Add(dtTable.Copy)
            If numformId = 12 Or numformId = 13 Then
                objContact.ContactType = 0
            Else : objContact.ContactType = ddlRelationship.SelectedValue
            End If

            objContact.DomainID = Session("DomainId")
            objContact.UserCntID = Session("UserContactId")
            objContact.FormId = numformId
            objContact.strXml = dsNew.GetXml
            objContact.SaveContactColumnConfiguration()
            dsNew.Tables.Remove(dsNew.Tables(0))

            If numformId = 11 Then
                If radY.Checked = True Then
                    objContact.bitDefault = True
                Else : objContact.bitDefault = False
                End If
                Select Case ddlRelationship.SelectedValue
                    Case 0 : objContact.FilterID = 0
                    Case 1
                        objContact.FilterID = ddlFollow.SelectedValue
                        objContact.GroupID = ddlLeadType.SelectedValue
                    Case 3 : objContact.FilterID = ddlProspectFilter.SelectedValue
                    Case 2 : objContact.FilterID = ddlAccountFilter.SelectedValue
                    Case Else
                        objContact.FilterID = ddlCompanyFilter.SelectedValue
                End Select
                objContact.SaveDefaultFilter()
            ElseIf numformId = 12 Then
                objContact.bitDefault = False
                objContact.FilterID = ddlCase.SelectedValue
                objContact.SaveDefaultFilter()
            ElseIf numformId = 13 Then
                objContact.bitDefault = False
                objContact.FilterID = ddlPro.SelectedValue
                objContact.SaveDefaultFilter()
            End If
            BindLists()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class
