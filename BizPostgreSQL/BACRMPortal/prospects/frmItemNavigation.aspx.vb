Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.admin
Imports Infragistics.WebUI.UltraWebNavigator
Imports BACRM.BusinessLogic.Item

Partial Public Class frmItemNavigation : Inherits BACRMPage

    'Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '    Try
    '        Dim dtTab As DataTable
    '        Dim intModuleIID As Integer
    '        intModuleIID = GetQueryStringVal( "ModID")

    '        If intModuleIID = 2 Then
    '            UltraWebTree1.RootNodeImageUrl = Nothing
    '            UltraWebTree1.LeafNodeImageUrl = Nothing
    '            UltraWebTree1.Font.Bold = False
    '        End If
    '        UltraWebTree1.DefaultImage = Nothing
    '        UltraWebTree1.DefaultSelectedImage = Nothing
    '        Dim objCommon As New CCommon
    '        objCommon.ModuleID = intModuleIID
    '        objCommon.DomainID = Session("DomainId")
    '        Dim dtTable As DataTable

    '        If intModuleIID = 2 Then
    '            dtTable = objCommon.GetPageNavigationRelDTLs
    '        Else : dtTable = objCommon.GetPageNavigationDTLs
    '        End If

    '        Dim treeNodeA As Node
    '        If intModuleIID = 2 Then dtTab = Session("DefaultTab")
    '        For Each dr As DataRow In dtTable.Rows
    '            treeNodeA = New Node
    '            treeNodeA.TargetFrame = "rightFrame"
    '            treeNodeA.TargetUrl = dr("vcNavURL")
    '            treeNodeA.Tag = dr("numPageNavID").ToString
    '            If intModuleIID = 2 Then

    '                If dr("numParentID").ToString = "0" Then
    '                    'treeNodeA.ImageUrl = "../images/tf_gold_dot.gif"
    '                    treeNodeA.Styles.Font.Bold = True
    '                ElseIf dr("numParentID") = "6" Then
    '                    treeNodeA.ImageUrl = "../images/tf_note.gif"
    '                    treeNodeA.Styles.Font.Bold = True
    '                Else
    '                    treeNodeA.Styles.Font.Bold = False
    '                    treeNodeA.Styles.Font.Italic = True
    '                End If
    '            Else
    '                If dr("numParentID").ToString = "0" Then
    '                    treeNodeA.ImageUrl = "../images/clearpixel.gif"
    '                    treeNodeA.SelectedImageUrl = "../images/clearpixel.gif"
    '                    treeNodeA.Styles.Font.Bold = True
    '                Else
    '                    If Not IsDBNull(dr("vcImageURL")) Then
    '                        treeNodeA.ImageUrl = dr("vcImageURL")
    '                        treeNodeA.SelectedImageUrl = dr("vcImageURL")
    '                        treeNodeA.Styles.Font.Bold = True
    '                    Else
    '                        treeNodeA.Styles.Font.Bold = False
    '                        treeNodeA.Styles.Font.Italic = True
    '                    End If
    '                End If
    '            End If

    '            If intModuleIID = 2 Then
    '                Select Case dr("vcPageNavName")
    '                    Case "Leads" : treeNodeA.Text = dtTab.Rows(0).Item("vcLead").ToString & "s"
    '                    Case "My Leads" : treeNodeA.Text = "My " & dtTab.Rows(0).Item("vcLead").ToString & "s"
    '                    Case "WebLeads" : treeNodeA.Text = "Web" & dtTab.Rows(0).Item("vcLead").ToString & "s"
    '                    Case "Public Leads" : treeNodeA.Text = "Public" & dtTab.Rows(0).Item("vcLead").ToString & "s"
    '                    Case "Contacts" : treeNodeA.Text = dtTab.Rows(0).Item("vcContact").ToString & "s"
    '                    Case "Prospects" : treeNodeA.Text = dtTab.Rows(0).Item("vcProspect").ToString & "s"
    '                    Case "Accounts" : treeNodeA.Text = dtTab.Rows(0).Item("vcAccount").ToString & "s"
    '                    Case Else
    '                        treeNodeA.Text = dr("vcPageNavName")
    '                End Select
    '            Else : treeNodeA.Text = dr("vcPageNavName")
    '            End If
    '            If dr("numParentID") = 0 Then
    '                UltraWebTree1.Nodes.Add(treeNodeA)
    '                UltraWebTree1.ExpandAll()
    '            Else : UltraWebTree1.Find(dr("numParentID").ToString).Nodes.Add(treeNodeA)
    '            End If
    '        Next
    '        If intModuleIID = 2 Then
    '            'UltraWebTree1.ExpandAll()
    '            Dim objTab As New Tabs
    '            Dim dtTabDataMenu As DataTable
    '            objTab.ListId = 5
    '            objTab.DomainID = Session("DomainID")
    '            objTab.mode = True
    '            dtTabDataMenu = objTab.GetTabMenuItem()
    '            For Each dr As DataRow In dtTabDataMenu.Rows
    '                '    treeNodeA = New Node
    '                '    treeNodeA.Text = dr("vcData")
    '                '    treeNodeA.TargetFrame = "rightFrame"
    '                '    treeNodeA.TargetUrl = "../prospects/frmCompanyList.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&RelId=" & dr("numListItemID")
    '                '    UltraWebTree1.Find("6").Nodes.Add(treeNodeA)
    '                treeNodeA = New Node
    '                treeNodeA.TargetFrame = "rightFrame"
    '                treeNodeA.Text = dr("vcData")
    '                treeNodeA.TargetUrl = dr("vcNavURL")
    '                treeNodeA.Tag = dr("numListItemID").ToString
    '                If dr("numListItemID") <> 0 Then
    '                    treeNodeA.ImageUrl = "../images/tf_note.gif"
    '                    treeNodeA.Styles.Font.Bold = True
    '                Else
    '                    treeNodeA.Styles.Font.Italic = True
    '                    treeNodeA.Styles.Font.Bold = False
    '                End If
    '                UltraWebTree1.Find(dr("parentid").ToString).Nodes.Add(treeNodeA)
    '            Next
    '            UltraWebTree1.ExpandAll()
    '            UltraWebTree1.Find("7").Expanded = True
    '        ElseIf intModuleIID = 6 Then
    '            Dim objItems As New CItems
    '            Dim dtItemGroups As DataTable
    '            objItems.DomainID = Session("DomainID")
    '            dtItemGroups = objItems.GetItemGroups.Tables(0)
    '            For Each dr As DataRow In dtItemGroups.Rows
    '                treeNodeA = New Node
    '                treeNodeA.TargetFrame = "rightFrame"
    '                treeNodeA.Text = dr("vcItemGroup")
    '                treeNodeA.TargetUrl = "../Items/frmItemList.aspx?Page=All Items&ItemGroup=" & dr("numItemGroupID")
    '                treeNodeA.Tag = dr("numItemGroupID").ToString
    '                UltraWebTree1.Find("76").Nodes.Add(treeNodeA)
    '            Next
    '        End If
    '        If intModuleIID = 9 Or intModuleIID = 8 Or intModuleIID = 3 Then UltraWebTree1.ExpandAll()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

End Class

