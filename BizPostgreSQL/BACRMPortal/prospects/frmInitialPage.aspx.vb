Imports BACRM.BusinessLogic.Account
Imports BACRM.BusinessLogic.Common

Partial Public Class frmInitialPage
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Dim objAccount As New CAccounts
                Dim dtTable As DataTable
                objAccount.UserCntID = Session("UserContactId")
                objAccount.DomainID = Session("DomainId")
                If GetQueryStringVal( "frm") = "opp" Then
                    objAccount.FormId = 14
                Else : objAccount.FormId = 0
                End If

                dtTable = objAccount.GetInitialPageDetails()
                If dtTable.Rows.Count = 1 Then
                    Dim FormId As Short = dtTable.Rows(0).Item("numFormId")
                    Dim Relationid As Long = dtTable.Rows(0).Item("numRelationId")
                    Dim filterId As Long = dtTable.Rows(0).Item("numFilterId")
                    Dim strRedirect As String = ""
                    If FormId = 10 Then
                        strRedirect = "../contact/frmContactList.aspx?ContactType=" & Relationid & "&srt=" & filterId
                    ElseIf FormId = 11 Then
                        If Relationid = 1 Then
                            strRedirect = "../Leads/frmLeadList.aspx?FilterId=" & filterId & "&GrpId=" & dtTable.Rows(0).Item("tintGroupId")
                        ElseIf Relationid = 3 Then
                            strRedirect = "../prospects/frmProspectList.aspx?FilterId=" & filterId
                        ElseIf Relationid = 2 Then
                            strRedirect = "../Account/frmAccountList.aspx?FilterId=" & filterId
                        Else : strRedirect = "../prospects/frmCompanyList.aspx?RelId=" & Relationid & "&FilterId=" & filterId
                        End If
                    ElseIf FormId = 14 Then
                        If Relationid = 1 Then
                            strRedirect = "../Opportunity/frmOpportunityList.aspx?SI=0"
                        ElseIf Relationid = 2 Then
                            strRedirect = "../Opportunity/frmOpportunityList.aspx?SI=1"
                        End If
                    End If
                    If strRedirect <> "" Then Response.Redirect(strRedirect)
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class

