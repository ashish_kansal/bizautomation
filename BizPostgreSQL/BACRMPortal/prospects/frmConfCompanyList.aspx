<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmConfCompanyList.aspx.vb" Inherits="BACRMPortal.frmConfCompanyList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
        <link href="~/css/MASTER.CSS" rel="stylesheet" type="text/css" runat="server" id="MasterCSS" />
		<title>Contacts Column Customization</title>
		<script language="javascript" src="../javascript/AdvSearchScripts.js"></script>
		<script language="javascript" type="text/javascript" >
		function Save()
		{
		    	var str='';
	            for(var i=0; i<document.frmCompany.lstSelectedfld.options.length; i++)
			            {
				            var SelectedValue;
				            SelectedValue = document.frmCompany.lstSelectedfld.options[i].value;
				            str=str+ SelectedValue +','
			            }
			            document.frmCompany.hdnCol.value=str;
			           // alert( document.frmCompany.hdnCol.value)
		}
		
		</script>
	</HEAD>
	<body >
		<form id="frmCompany" method="post" runat="server" style="LEFT: 10px; TOP: 10px; POSITION: relative;">
			<table cellSpacing="0" cellPadding="0" width="520">
				<tr>
					<td style="WIDTH: 211px" vAlign="bottom">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;<asp:Label runat="server" ID="lblName" ></asp:Label>
									&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
					<td class="normal1"></td>
					<td > <asp:DropDownList ID="ddlRelationship" runat="server" AutoPostBack="true" CssClass="signup" ></asp:DropDownList></td>
					<td align="right" height="23">
					<asp:Button runat="server" ID="btnSave" Text="Save" CssClass="button" Width="50" />
					<input class="button" id="btnClose" style="width:50" onclick="javascript:window.close()" type="button" value="Close">&nbsp;&nbsp;&nbsp;
					</td>
				</tr>
			</table>
			<asp:table id="tblAdvSearchFieldCustomization" Runat="server" Width="520" GridLines="None" CssClass="aspTable"
				BorderColor="black" BorderWidth="1">
				<asp:TableRow>
					<asp:TableCell>
						<table cellpadding="0" cellspacing="0" width="100%">
							
							<tr>
								<td align="center" class="normal1" valign="top">
									Available Fields<br>
									&nbsp;&nbsp;
									<asp:ListBox ID="lstAvailablefld" Runat="server" Width="150" Height="200" CssClass="signup" EnableViewState="False"></asp:ListBox>
								</td>
								<td align="center" class="normal1" valign="middle">
									<input type="button" id="btnAdd" Class="button" Value="Add >" onclick="javascript:move(document.frmCompany.lstAvailablefld,document.frmCompany.lstSelectedfld)">
									<br>
									<br>
									<input type="button" id="btnRemove" Class="button" Value="< Remove" onclick="javascript:remove(document.frmCompany.lstSelectedfld,document.frmCompany.lstAvailablefld);"></td>
								<td align="center" class="normal1">
									Selected Fields/ Choose Order<br>
									<asp:ListBox ID="lstSelectedfld" Runat="server" Width="150" Height="200" CssClass="signup" EnableViewState="False"></asp:ListBox>
								</td>
								<td align="center" class="normal1" valign="middle">
									<br>
									<br>
									<br>
									<br>
									<br>
									<input type="button" id="btnMoveupOne" Class="button" style="FONT-FAMILY: Webdings" value="5"
										onclick="javascript:MoveUp(document.frmCompany.lstSelectedfld);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<br>
									<br>
									<input type="button" id="btnMoveDownOne" Class="button" style="FONT-FAMILY: Webdings" value="6"
										onclick="javascript:MoveDown(document.frmCompany.lstSelectedfld);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<br>
									<br>
									<br>
									<br>
									<br>
									(Max 10)
								</td>
							</tr>
						</table>
					</asp:TableCell>
				</asp:TableRow>
				<asp:TableRow>
				    <asp:TableCell>
				   <asp:Panel runat="server" ID="pnlRelation">
				               <table width="100%" >
					                    <tr>
					                            <td colspan="2" class="normal7">
					                                    When you click selected relationship, the list will default based on your settings here:    
					                            </td>
					                    </tr>
					                    <tr>
					                            <td class="normal1">
                    					            Filter :
					                            </td>
					                           <td align="left">
                    					              <asp:dropdownlist id="ddlCompanyFilter" runat="server" cssclass="signup" >
							                             	<asp:ListItem Value="3" Selected="True">My Organization</asp:ListItem>
							                                <asp:ListItem Value="4">All Organization</asp:ListItem>
							                                <asp:ListItem Value="1">Active Organization</asp:ListItem>
							                                <asp:ListItem Value="2">Inactive Organization</asp:ListItem>
							                                <asp:ListItem Value="5">Rating</asp:ListItem>
							                                <asp:ListItem Value="6">Added in Last 7 days</asp:ListItem>
							                                <asp:ListItem Value="7">Last 20 Added by me</asp:ListItem>
							                                <asp:ListItem Value="8">Last 20 Modified by me</asp:ListItem>
							                                <asp:ListItem Value="9">Favorites</asp:ListItem>
						                                </asp:dropdownlist>
						                                
						                                <asp:dropdownlist id="ddlProspectFilter" runat="server"  cssclass="signup">
							                                <asp:ListItem Value="3" Selected="True">My Prospects</asp:ListItem>
							                                <asp:ListItem Value="4">All Prospects</asp:ListItem>
							                                <asp:ListItem Value="1">Active Prospects</asp:ListItem>
							                                <asp:ListItem Value="2">Inactive Prospects</asp:ListItem>
							                                <asp:ListItem Value="5">Rating</asp:ListItem>
							                                <asp:ListItem Value="6">Added in Last 7 days</asp:ListItem>
							                                <asp:ListItem Value="7">Last 20 Added by me</asp:ListItem>
							                                <asp:ListItem Value="8">Last 20 Modified by me</asp:ListItem>
							                                <asp:ListItem Value="9">Favorites</asp:ListItem>
						                                </asp:dropdownlist>
						                                    
						                                <asp:dropdownlist id="ddlAccountFilter" runat="server" cssclass="signup" >
							                                <asp:ListItem Value="3" Selected="true">My Accounts</asp:ListItem>
							                                <asp:ListItem Value="4">All Accounts</asp:ListItem>
							                                <asp:ListItem Value="1">Active Accounts</asp:ListItem>
							                                <asp:ListItem Value="2">Inactive Accounts</asp:ListItem>
							                                <asp:ListItem Value="5">Rating</asp:ListItem>
							                                <asp:ListItem Value="6">Added in Last 7 days</asp:ListItem>
							                                <asp:ListItem Value="7">Last 20 Added by me</asp:ListItem>
							                                <asp:ListItem Value="8">Last 20 Modified by me</asp:ListItem>
							                                <asp:ListItem Value="9">Favorites</asp:ListItem>
						                                </asp:dropdownlist>
						                                <asp:dropdownlist id="ddlFollow" runat="server" CssClass="signup" Width="130px" ></asp:dropdownlist>
						                                <span id="spnLead" runat="server" class="normal1">&nbsp;&nbsp;&nbsp;Group</span>
						                                <asp:dropdownlist id="ddlLeadType" runat="server" CssClass="signup" Width="130px" >
						                                        <asp:ListItem Text="My Leads" Value="5"></asp:ListItem>
						                                        <asp:ListItem Text="Web Leads" Value="1"></asp:ListItem>
						                                         <asp:ListItem Text="Public Leads" Value="2"></asp:ListItem>
						                                </asp:dropdownlist>
						                                
					                            </td> 
					                    </tr>
					                   <tr >
					                             <td colspan="2" class="normal7">
					                                    When you click on the Relationships tab, is this the list you want to see ? 
					                                    <asp:RadioButton ID="radY" GroupName="Group" runat="server" Text="Yes" />
					                                    <asp:RadioButton ID="radN" GroupName="Group" runat="server" Checked="true" Text="No" />
					                            </td>
					                   </tr> 
					        </table>
					    </asp:Panel>
				    <asp:Panel runat="server" ID="pnlCase">
				               <table width="100%" >
					                    <tr>
					                            <td colspan="2" class="normal7">
					                                    When you click selected case, the list will default based on your settings here:    
					                            </td>
					                    </tr>
					                    <tr>
					                            <td class="normal1">
                    					            Filter :
					                            </td>
					                           <td align="left">
                    					                  <asp:dropdownlist id="ddlCase" runat="server" cssclass="signup" >
				                                                     <asp:ListItem Value="0">My Cases</asp:ListItem>
								                                    <asp:ListItem Value="1">All Cases</asp:ListItem>
								                                    <asp:ListItem Value="2">Added in Last 7 Days</asp:ListItem>
								                                    <asp:ListItem Value="3">Last 20 Added by me</asp:ListItem>
								                                    <asp:ListItem Value="4">Last 20 Modified by me</asp:ListItem>
						                                </asp:dropdownlist>
						                       </td>
						                       
						                   </tr>
						                   
						                  </table>
				    </asp:Panel> 
				    <asp:Panel runat="server" ID="pnlPro">
				               <table width="100%" >
					                    <tr>
					                            <td colspan="2" class="normal7">
					                                    When you click selected case, the list will default based on your settings here:    
					                            </td>
					                    </tr>
					                    <tr>
					                            <td class="normal1">
                    					            Filter :
					                            </td>
					                           <td align="left">
                    					                 <asp:dropdownlist id="ddlPro" runat="server" cssclass="signup">
							                                    <asp:ListItem Value="1">My Projects</asp:ListItem>
							                                    <asp:ListItem Value="3">All Projects</asp:ListItem>
							                                    <asp:ListItem Value="2">My Subordinates</asp:ListItem>
							                                    <asp:ListItem Value="4">Added in Last 7 days</asp:ListItem>
							                                    <asp:ListItem Value="5">Last 20 Added by me</asp:ListItem>
							                                    <asp:ListItem Value="6">Last 20 Modified by me</asp:ListItem>
						                                    </asp:dropdownlist>
						                       </td>
						                       
						                   </tr>
						                   
						                  </table>
				    </asp:Panel> 
				    
				    </asp:TableCell>
				</asp:TableRow>
			</asp:table><input id="hdnCol" type="hidden" name="hdXMLString" runat="server" value="" />
			<input id="hdSave" type="hidden" name="hdSave" runat="server" value="False" />
		</form>
		</body>
</html>
