Imports BACRM.BusinessLogic.Common
Partial Public Class frmHelpTree
    Inherits BACRMPage

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If GetQueryStringVal( "Page") = "Tickler" Then
                ifHelp.Attributes.Add("src", "Help/Tickler.htm")
                Treeview1.Nodes.Item(0).Expanded = True
            ElseIf GetQueryStringVal( "Page") = "Organizations" Then
                ifHelp.Attributes.Add("src", "Help/Organizations.htm")
            ElseIf GetQueryStringVal( "Page") = "Contacts" Then
                ifHelp.Attributes.Add("src", "Help/Contacts.htm")
            ElseIf GetQueryStringVal( "Page") = "Opportunities" Then
                ifHelp.Attributes.Add("src", "Help/Opportunities.htm")
            ElseIf GetQueryStringVal( "Page") = "Forecasting" Then
                ifHelp.Attributes.Add("src", "Help/Forecasting.htm")
            ElseIf GetQueryStringVal( "Page") = "MarketingCampaigns" Then
                Treeview1.Nodes.Item(5).Expanded = True
                ifHelp.Attributes.Add("src", "Help/Marketing/Campaigns.htm")
            ElseIf GetQueryStringVal( "Page") = "MarketingSurveys" Then
                Treeview1.Nodes.Item(5).Expanded = True
                ifHelp.Attributes.Add("src", "Help/Marketing/Surveys.htm")
            ElseIf GetQueryStringVal( "Page") = "MarketingEmail" Then
                Treeview1.Nodes.Item(5).Expanded = True
                ifHelp.Attributes.Add("src", "Help/Marketing/EmailBroadcasting.htm")
            ElseIf GetQueryStringVal( "Page") = "Service" Then
                ifHelp.Attributes.Add("src", "Help/Support.htm")
            ElseIf GetQueryStringVal( "Page") = "Projects" Then
                ifHelp.Attributes.Add("src", "Help/Projects.htm")
            ElseIf GetQueryStringVal( "Page") = "Documents" Then
                ifHelp.Attributes.Add("src", "Help/Documents.htm")
            ElseIf GetQueryStringVal( "Page") = "BizDocs" Then
                ifHelp.Attributes.Add("src", "Help/BizDocs.htm")
            ElseIf GetQueryStringVal( "Page") = "Search" Then
                ifHelp.Attributes.Add("src", "Help/AdvancedSearch.htm")
            ElseIf GetQueryStringVal( "Page") = "Importing" Then
                ifHelp.Attributes.Add("src", "Help/Import.htm")
            ElseIf GetQueryStringVal( "Page") = "Master" Then
                ifHelp.Attributes.Add("src", "Help/Admin/MasterList.htm")
                Treeview1.Nodes.Item(12).Expanded = True
            ElseIf GetQueryStringVal( "Page") = "BizForms" Then
                ifHelp.Attributes.Add("src", "Help/Admin/BizForms.htm")
                Treeview1.Nodes.Item(12).Expanded = True
            ElseIf GetQueryStringVal( "Page") = "Business" Then
                ifHelp.Attributes.Add("src", "Help/Admin/BusinessProcesses.htm")
                Treeview1.Nodes.Item(12).Expanded = True
            ElseIf GetQueryStringVal( "Page") = "Routing" Then
                ifHelp.Attributes.Add("src", "Help/Admin/AutoRoutingRules.htm")
                Treeview1.Nodes.Item(12).Expanded = True
            ElseIf GetQueryStringVal( "Page") = "Item" Then
                ifHelp.Attributes.Add("src", "Help/Admin/Item&PriceBookMgt.htm")
                Treeview1.Nodes.Item(12).Expanded = True
            ElseIf GetQueryStringVal( "Page") = "User" Then
                ifHelp.Attributes.Add("src", "Help/Admin/UserAccessMgt.htm")
                Treeview1.Nodes.Item(12).Expanded = True
            ElseIf GetQueryStringVal( "Page") = "Areas" Then
                ifHelp.Attributes.Add("src", "Help/Admin/AreasOfInterest.htm")
                Treeview1.Nodes.Item(12).Expanded = True
            ElseIf GetQueryStringVal( "Page") = "BPA" Then
                ifHelp.Attributes.Add("src", "Help/Admin/BPA&Alerts.htm")
                Treeview1.Nodes.Item(12).Expanded = True
            ElseIf GetQueryStringVal( "Page") = "Accounting" Then
                ifHelp.Attributes.Add("src", "Help/Admin/AccountingIntegration.htm")
                Treeview1.Nodes.Item(12).Expanded = True
            ElseIf GetQueryStringVal( "Page") = "Custom" Then
                ifHelp.Attributes.Add("src", "Help/Admin/CustomFieldBuilder.htm")
                Treeview1.Nodes.Item(12).Expanded = True
            ElseIf GetQueryStringVal( "Page") = "Associations" Then
                ifHelp.Attributes.Add("src", "Help/Associations&DataCleansing.htm")
                Treeview1.Nodes.Item(12).Expanded = True
            ElseIf GetQueryStringVal( "Page") = "BizPortal" Then
                ifHelp.Attributes.Add("src", "Help/BizPortal.htm")
            ElseIf GetQueryStringVal( "Page") = "Reports" Then
                ifHelp.Attributes.Add("src", "Help/Reports.htm")
            ElseIf GetQueryStringVal( "Page") = "E-Commerce" Then
                ifHelp.Attributes.Add("src", "Help/Ecommerce.htm")
            ElseIf GetQueryStringVal( "Page") = "Financials" Then
                ifHelp.Attributes.Add("src", "Help/Financials.htm")
            ElseIf GetQueryStringVal( "Page") = "System" Then
                ifHelp.Attributes.Add("src", "help/Admin/SystemConfiguration.htm")
            ElseIf GetQueryStringVal( "Page") = "Glossary" Then
                ifHelp.Attributes.Add("src", "Help/Glossary.htm")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class