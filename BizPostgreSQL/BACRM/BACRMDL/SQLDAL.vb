' The SqlDAL class is intended to encapsulate high performance, scalable best practices for 
' common uses of SqlClient.
' 
' VERSION	DESCRIPTION
'   1.0	     
' 
' ===============================================================================
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Data
Imports System.Reflection
Imports System.Collections.Generic
Imports Npgsql

Namespace BACRMAPI.DataAccessLayer

    Public NotInheritable Class SqlDAL
        Protected mobjSqlConnection As NpgsqlConnection
        Protected mobjSqlTransaction As NpgsqlTransaction


        Public Shared NoTimeOut As Boolean = False




#Region "private methods and constructors"

        ' Since this class provides only static methods, make the default constructor private to prevent 
        ' instances from being created with "new SqlHelper()".
        Private Sub New()
        End Sub ' New

        ' This method is used to attach array of SqlParameters to a NpgsqlCommand.
        ' This method will assign a value of DbNull to any parameter with a direction of
        ' InputOutput and a value of null.  
        ' This behavior will prevent default values from being used, but
        ' this will be the less common case than an intended pure output parameter (derived as InputOutput)
        ' where the user provided no input value.
        ' Parameters:
        ' -command - The command to which the parameters will be added
        ' -commandParameters - an array of SqlParameters to be added to command
        Private Shared Sub AttachParameters(ByVal command As NpgsqlCommand, ByVal commandParameters() As Npgsql.NpgsqlParameter)
            If (command Is Nothing) Then Throw New ArgumentNullException("command")
            If (Not commandParameters Is Nothing) Then
                Dim postgresCommandText As String = "SELECT " & command.CommandText & "("

                Dim p As Npgsql.NpgsqlParameter
                For Each p In commandParameters
                    If p.NpgsqlDbType.ToString().ToLower() = "char" AndAlso Not p.Value Is DBNull.Value AndAlso Convert.ToString(p.Value) = vbNullChar Then
                        p.Value = DBNull.Value
                    End If
                    If (Not p Is Nothing) Then
                        ' Check for derived output value with no value assigned
                        If (p.Direction = ParameterDirection.InputOutput OrElse p.Direction = ParameterDirection.Input) AndAlso p.Value Is Nothing Then
                            p.Value = DBNull.Value
                        End If
                        p.NpgsqlValue = p.Value
                        command.Parameters.Add(p)

                        postgresCommandText = postgresCommandText & If(postgresCommandText.EndsWith("("), "", ",") & If(p.NpgsqlDbType.ToString().ToLower() = "text" Or p.NpgsqlDbType.ToString().ToLower() = "nvarchar" Or p.NpgsqlDbType.ToString().ToLower() = "varchar", "'", "") & p.Value & If(p.NpgsqlDbType.ToString().ToLower() = "text" Or p.NpgsqlDbType.ToString().ToLower() = "nvarchar" Or p.NpgsqlDbType.ToString().ToLower() = "varchar", "'", "") & "::" & p.NpgsqlDbType.ToString()
                    End If
                Next p

                postgresCommandText = postgresCommandText & ");"


                If command.CommandText <> "USP_ManageException" AndAlso Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings("IsLogProcedures")) Then
                    Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                    arParms(0) = New Npgsql.NpgsqlParameter("@vcMessage", NpgsqlTypes.NpgsqlDbType.Varchar)
                    arParms(0).Value = postgresCommandText

                    arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                    arParms(1).Value = 0

                    arParms(2) = New Npgsql.NpgsqlParameter("@numUserContactID", NpgsqlTypes.NpgsqlDbType.Bigint)
                    arParms(2).Value = 0

                    arParms(3) = New Npgsql.NpgsqlParameter("@vcURL", NpgsqlTypes.NpgsqlDbType.Varchar, 1000)
                    arParms(3).Value = ""

                    arParms(4) = New Npgsql.NpgsqlParameter("@vcReferrer", NpgsqlTypes.NpgsqlDbType.Varchar, 1000)
                    arParms(4).Value = ""

                    arParms(5) = New Npgsql.NpgsqlParameter("@vcBrowser", NpgsqlTypes.NpgsqlDbType.Varchar, 100)
                    arParms(5).Value = ""

                    arParms(6) = New Npgsql.NpgsqlParameter("@vcBrowserAgent", NpgsqlTypes.NpgsqlDbType.Varchar, 500)
                    arParms(6).Value = ""


                    ExecuteNonQuery(command.Connection, "USP_ManageException", arParms)
                End If
            End If
        End Sub ' AttachParameters

        ' This method assigns dataRow column values to an array of SqlParameters.
        ' Parameters:
        ' -commandParameters: Array of SqlParameters to be assigned values
        ' -dataRow: the dataRow used to hold the stored procedure' s parameter values
        Private Overloads Shared Sub AssignParameterValues(ByVal commandParameters() As Npgsql.NpgsqlParameter, ByVal dataRow As DataRow)

            If commandParameters Is Nothing OrElse dataRow Is Nothing Then
                ' Do nothing if we get no data    
                Exit Sub
            End If

            ' Set the parameters values
            Dim commandParameter As Npgsql.NpgsqlParameter
            Dim i As Integer
            For Each commandParameter In commandParameters
                ' Check the parameter name
                If (commandParameter.ParameterName Is Nothing OrElse commandParameter.ParameterName.Length <= 1) Then
                    Throw New Exception(String.Format("Please provide a valid parameter name on the parameter #{0}, the ParameterName property has the following value: ' {1}' .", i, commandParameter.ParameterName))
                End If
                If dataRow.Table.Columns.IndexOf(commandParameter.ParameterName.Substring(1)) <> -1 Then
                    commandParameter.Value = dataRow(commandParameter.ParameterName.Substring(1))
                End If
                i = i + 1
            Next
        End Sub

        ' This method assigns an array of values to an array of SqlParameters.
        ' Parameters:
        ' -commandParameters - array of SqlParameters to be assigned values
        ' -array of objects holding the values to be assigned
        Private Overloads Shared Sub AssignParameterValues(ByVal commandParameters() As Npgsql.NpgsqlParameter, ByVal parameterValues() As Object)

            Dim i As Integer
            Dim j As Integer

            If (commandParameters Is Nothing) AndAlso (parameterValues Is Nothing) Then
                ' Do nothing if we get no data
                Return
            End If

            ' We must have the same number of values as we pave parameters to put them in
            If commandParameters.Length <> parameterValues.Length Then
                Throw New ArgumentException("Parameter count does not match Parameter Value count.")
            End If

            ' Value array
            j = commandParameters.Length - 1
            For i = 0 To j
                ' If the current array value derives from IDbDataParameter, then assign its Value property
                If TypeOf parameterValues(i) Is IDbDataParameter Then
                    Dim paramInstance As IDbDataParameter = CType(parameterValues(i), IDbDataParameter)
                    If (paramInstance.Value Is Nothing) Then
                        commandParameters(i).Value = DBNull.Value
                    Else
                        commandParameters(i).Value = paramInstance.Value
                    End If
                ElseIf (parameterValues(i) Is Nothing) Then
                    commandParameters(i).Value = DBNull.Value
                Else
                    commandParameters(i).Value = parameterValues(i)
                End If
            Next
        End Sub ' AssignParameterValues

        ' This method opens (if necessary) and assigns a connection, transaction, command type and parameters 
        ' to the provided command.
        ' Parameters:
        ' -command - the NpgsqlCommand to be prepared
        ' -connection - a valid NpgsqlConnection, on which to execute this command
        ' -transaction - a valid NpgsqlTransaction, or ' null' 
        ' -commandType - the CommandType (stored procedure, text, etc.)
        ' -commandText - the stored procedure name or T-SQL command
        ' -commandParameters - an array of SqlParameters to be associated with the command or ' null' if no parameters are required
        Private Shared Sub PrepareCommand(ByVal command As NpgsqlCommand,
                                          ByVal connection As NpgsqlConnection,
                                          ByVal transaction As NpgsqlTransaction,
                                          ByVal commandType As CommandType,
                                          ByVal commandText As String,
                                          ByVal commandParameters() As Npgsql.NpgsqlParameter, ByRef mustCloseConnection As Boolean)

            If (command Is Nothing) Then Throw New ArgumentNullException("command")
            If (commandText Is Nothing OrElse commandText.Length = 0) Then Throw New ArgumentNullException("commandText")

            ' If the provided connection is not open, we will open it
            If connection.State <> ConnectionState.Open Then
                connection.Open()
                mustCloseConnection = True
            Else
                mustCloseConnection = False
            End If

            ' Associate the connection with the command
            command.Connection = connection

            ' Set the command text (stored procedure name or SQL statement)
            command.CommandText = commandText

            ' If we were provided a transaction, assign it.
            If Not (transaction Is Nothing) Then
                If transaction.Connection Is Nothing Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
                command.Transaction = transaction
            End If

            ' Set the command type
            command.CommandType = commandType

            ' Attach the command parameters if they are provided
            If Not (commandParameters Is Nothing) Then
                AttachParameters(command, commandParameters)
            End If

            Return
        End Sub ' PrepareCommand

#End Region

#Region "ExecuteNonQuery"


        ''' <summary>
        ''' 
        ''' </summary>
        ''' <typeparam name="T"></typeparam>
        ''' <param name="connString"></param>
        ''' <param name="spName"></param>
        ''' <param name="paramObj"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function ExecMethod(Of T)(ByVal connString As String, ByVal spName As String, ByVal paramObj() As Object) As Object
            Try
                Dim pInfo As PropertyInfo() = GetType(T).GetProperties
                If pInfo(0).ReflectedType.Name = "DataTable" Then
                    Return SqlDAL.ExecuteDatable(connString, spName, paramObj)
                ElseIf pInfo(0).ReflectedType.Name = "DataSet" Then
                    Return SqlDAL.ExecuteDataset(connString, spName, paramObj)
                ElseIf pInfo(0).ReflectedType.Name = "IDataReader" Then
                    Return SqlDAL.ExecuteReader(connString, spName, paramObj)
                End If
                Return ""
            Catch ex As Exception
                Throw
            End Try
        End Function


        '***************************************************************************************************************************
        '     Function / Subroutine  Name: ExecuteNonQuery
        '     Purpose					 : Execute a NpgsqlCommand (that returns no resultset and takes no parameters) against the database specified in 
        '                                  the connection string. 
        '     Example                    :  
        '                                   Dim result As Integer =  ExecuteNonQuery(connString, "CustomerOrders")
        '     Parameters                 :
        '                                   connectionString - a valid connection string for a NpgsqlConnection
        '                                   commandType - the CommandType (stored procedure, text, etc.)
        '                                   commandText - the stored procedure name or T-SQL command
        '                                    Returns: An int representing the number of rows affected by the command
        '     Inputs					 : No input Parmeter.                          
        '                                 
        '     Outputs					 : 
        '						
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 11/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteNonQuery(ByVal connectionString As String,
                                                       ByVal commandType As CommandType,
                                                       ByVal commandText As String) As Integer
            ' Pass through the call providing null for the set of SqlParameters
            Return ExecuteNonQuery(connectionString, commandType, commandText, CType(Nothing, Npgsql.NpgsqlParameter()))
        End Function ' ExecuteNonQuery
        '***************************************************************************************************************************
        '     Function / Subroutine  Name: ExecuteNonQuery
        '     Purpose					 :  Execute a NpgsqlCommand (that returns no resultset) against the database specified in the connection string 
        '                                   using the provided parameters. 
        '     Example                    :  
        '                                   Dim result As Integer = ExecuteNonQuery(connString, "CustomerOrders", new Npgsql.NpgsqlParameter("@CustID", 30))
        '     Parameters                 :
        '                                   connectionString - a valid connection string for a NpgsqlConnection
        '                                   commandType - the CommandType (stored procedure, text, etc.)
        '                                   commandText - the stored procedure name or T-SQL command
        '                                   commandParameters - an array of SqlParamters used to execute the command
        '                                   Returns: An int representing the number of rows affected by the command
        '     Inputs					 :  SqlParmeters.                          
        '                                 
        '     Outputs					 :  Returns: An int representing the number of rows affected by the command
        '						
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 11/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteNonQuery(ByVal connectionString As String,
                                                 ByVal commandType As CommandType,
                                                 ByVal commandText As String,
                                                 ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter) As Integer
            If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
            ' Create & open a NpgsqlConnection, and dispose of it after we are done
            Dim connection As NpgsqlConnection
            Try
                connection = New NpgsqlConnection(connectionString)
                If connection.State <> ConnectionState.Open Then
                    connection.Open()
                End If

                ' Call the overload that takes a connection in place of the connection string
                Return ExecuteNonQuery(connection, commandType, commandText, commandParameters)
            Catch ex As Exception
                Throw
            Finally
                If Not connection Is Nothing Then
                    connection.Dispose()
                End If
            End Try
        End Function ' ExecuteNonQuery
        '***************************************************************************************************************************
        '     Function / Subroutine  Name: ExecuteNonQuery
        '     Purpose					 :  Execute a stored procedure via a NpgsqlCommand (that returns no resultset) against the database specified in 
        '                                   the connection string using the provided parameter values.  This method will discover the parameters for the 
        '                                   stored procedure, and assign the values based on parameter order.
        '                                   This method provides no access to output parameters or the stored procedure' s return value parameter.

        '     Example                    :  
        '                                   Dim result As Integer = ExecuteNonQuery(connString, "CustomerOrders", 24, 36)
        '     Parameters                 :
        '                                    connectionString - a valid connection string for a NpgsqlConnection
        '                                    spName - the name of the stored procedure
        '                                    parameterValues - an array of objects to be assigned as the input values of the stored procedure
        '                                   Returns: An int representing the number of rows affected by the command
        '                                 
        '     Outputs					 : Returns: An int representing the number of rows affected by the command
        '                                 
        '						
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 11/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteNonQuery(ByVal connectionString As String,
                                                 ByVal spName As String,
                                                 ByVal ParamArray parameterValues() As Object) As Integer

            If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

            Dim commandParameters As Npgsql.NpgsqlParameter()

            ' If we receive parameter values, we need to figure out where they go
            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)

                commandParameters = SqlDALParameterCache.GetSpParameterSet(connectionString, spName)

                ' Assign the provided values to these parameters based on parameter order
                AssignParameterValues(commandParameters, parameterValues)

                ' Call the overload that takes an array of SqlParameters
                Return ExecuteNonQuery(connectionString, CommandType.StoredProcedure, spName, commandParameters)
                ' Otherwise we can just call the SP without params
            Else
                Return ExecuteNonQuery(connectionString, CommandType.StoredProcedure, spName)
            End If
        End Function ' ExecuteNonQuery

        Public Overloads Shared Function ExecuteNonQuery(ByVal connectionString As String,
                                                         ByVal spName As String,
                                                         ByRef parameterValues() As Object,
                                                         ByVal isOutPutNeeded As Boolean) As Integer

            If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

            Dim commandParameters As Npgsql.NpgsqlParameter() = Nothing
            Dim intResult As Integer = 0

            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                commandParameters = SqlDALParameterCache.GetSpParameterSet(connectionString, spName)
                AssignParameterValues(commandParameters, parameterValues)
                intResult = ExecuteNonQuery(connectionString, CommandType.StoredProcedure, spName, commandParameters)
            Else
                intResult = ExecuteNonQuery(connectionString, CommandType.StoredProcedure, spName)
            End If

            If isOutPutNeeded = True Then
                parameterValues = commandParameters
            End If

            Return intResult
        End Function

        '***************************************************************************************************************************
        '     Function / Subroutine  Name: ExecuteNonQuery
        '     Purpose					 : Execute a NpgsqlCommand (that returns no resultset and takes no parameters) against the provided NpgsqlConnection. 
        ' 
        '     Example                    :  
        '                                  Dim result As Integer = ExecuteNonQuery(conn, "PublishOrders")
        '     Parameters                 :
        '                                  connection - a valid NpgsqlConnection
        '                                  commandType - the CommandType (stored procedure, text, etc.)
        '                                  commandText - the stored procedure name or T-SQL command 
        '                                 
        '     Outputs					 : An int representing the number of rows affected by the command
        '						
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 11/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteNonQuery(ByVal connection As NpgsqlConnection,
                                                         ByVal commandType As CommandType,
                                                         ByVal commandText As String) As Integer
            ' Pass through the call providing null for the set of SqlParameters
            Return ExecuteNonQuery(connection, commandType, commandText, CType(Nothing, Npgsql.NpgsqlParameter()))

        End Function ' ExecuteNonQuery
        '***************************************************************************************************************************
        '     Function / Subroutine  Name: ExecuteNonQuery
        '     Purpose					 : Execute a NpgsqlCommand (that returns no resultset) against the specified NpgsqlConnection 
        '                                  using the provided parameters.
        '     Example                    :  
        '                                   Dim result As Integer = ExecuteNonQuery(conn, "PublishOrders", New Npgsql.NpgsqlParameter("@prodid", 24))
        '     Parameters                :
        '                                   connection - a valid NpgsqlConnection 
        '                                   commandType - the CommandType (stored procedure, text, etc.)
        '                                   commandText - the stored procedure name or T-SQL command 
        '                                   commandParameters - an array of SqlParamters used to execute the command 
        '                                 
        '     Outputs					 :  Returns: An int representing the number of rows affected by the command 

        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 11/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteNonQuery(ByVal connection As NpgsqlConnection,
                                                 ByVal commandType As CommandType,
                                                 ByVal commandText As String,
                                                 ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter) As Integer

            If (connection Is Nothing) Then Throw New ArgumentNullException("connection")

            ' Create a command and prepare it for execution
            Dim cmd As New NpgsqlCommand
            Dim retval As Integer
            Dim mustCloseConnection As Boolean = False


            If NoTimeOut = True Then
                cmd.CommandTimeout = 0
            End If

            PrepareCommand(cmd, connection, CType(Nothing, NpgsqlTransaction), commandType, commandText, commandParameters, mustCloseConnection)


            ' Finally, execute the command
            Try
                retval = cmd.ExecuteNonQuery()
            Catch ex As Exception
                Dim postgresCommandText As String = "SELECT " & cmd.CommandText & "("

                Dim p As Npgsql.NpgsqlParameter
                For Each p In commandParameters
                    If p.NpgsqlDbType.ToString().ToLower() = "char" AndAlso Not p.Value Is DBNull.Value AndAlso Convert.ToString(p.Value) = vbNullChar Then
                        p.Value = DBNull.Value
                    End If
                    If (Not p Is Nothing) Then
                        ' Check for derived output value with no value assigned
                        If (p.Direction = ParameterDirection.InputOutput OrElse p.Direction = ParameterDirection.Input) AndAlso p.Value Is Nothing Then
                            p.Value = DBNull.Value
                        End If
                        p.NpgsqlValue = p.Value

                        postgresCommandText = postgresCommandText & If(postgresCommandText.EndsWith("("), "", ",") & If(p.NpgsqlDbType.ToString().ToLower() = "text" Or p.NpgsqlDbType.ToString().ToLower() = "nvarchar" Or p.NpgsqlDbType.ToString().ToLower() = "varchar", "'", "") & p.Value & If(p.NpgsqlDbType.ToString().ToLower() = "text" Or p.NpgsqlDbType.ToString().ToLower() = "nvarchar" Or p.NpgsqlDbType.ToString().ToLower() = "varchar", "'", "") & "::" & p.NpgsqlDbType.ToString()
                    End If
                Next p

                postgresCommandText = postgresCommandText & ");"

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                If cmd.CommandText <> "USP_ManageException" Then
                    Try
                        arParms(0) = New Npgsql.NpgsqlParameter("@vcMessage", NpgsqlTypes.NpgsqlDbType.Varchar)
                        arParms(0).Value = postgresCommandText

                        arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                        arParms(1).Value = 0

                        arParms(2) = New Npgsql.NpgsqlParameter("@numUserContactID", NpgsqlTypes.NpgsqlDbType.Bigint)
                        arParms(2).Value = 0

                        arParms(3) = New Npgsql.NpgsqlParameter("@vcURL", NpgsqlTypes.NpgsqlDbType.Varchar, 1000)
                        arParms(3).Value = ""

                        arParms(4) = New Npgsql.NpgsqlParameter("@vcReferrer", NpgsqlTypes.NpgsqlDbType.Varchar, 1000)
                        arParms(4).Value = ""

                        arParms(5) = New Npgsql.NpgsqlParameter("@vcBrowser", NpgsqlTypes.NpgsqlDbType.Varchar, 100)
                        arParms(5).Value = ""

                        arParms(6) = New Npgsql.NpgsqlParameter("@vcBrowserAgent", NpgsqlTypes.NpgsqlDbType.Varchar, 500)
                        arParms(6).Value = ""


                        ExecuteNonQuery(connection, "USP_ManageException", arParms)
                    Catch exInner As Exception
                        'DO NOT RAISE ERROR
                    End Try
                End If

                Throw
            End Try

            ' Detach the SqlParameters from the command object, so they can be used again
            cmd.Parameters.Clear()

            If (mustCloseConnection) Then connection.Close()

            Return retval
        End Function ' ExecuteNonQuery

        '***************************************************************************************************************************
        '     Function / Subroutine  Name: ExecuteNonQuery
        '     Purpose					 : Execute a stored procedure via a NpgsqlCommand (that returns no resultset) against the specified NpgsqlConnection 
        '                                  using the provided parameter values.  This method will discover the parameters for the 
        '                                  stored procedure, and assign the values based on parameter order.
        '                                  This method provides no access to output parameters or the stored procedure' s return value parameter.

        '     Example                    :  
        '                                  Dim result As integer = ExecuteNonQuery(conn, "PublishOrders", 24, 36)
        '     Parameters                 :
        '                                  connection - a valid NpgsqlConnection
        '                                  spName - the name of the stored procedure 
        '                                  parameterValues - an array of objects to be assigned as the input values of the stored procedure 
        '                                 
        '     Outputs					 :  Returns: An int representing the number of rows affected by the command 

        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 11/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteNonQuery(ByVal connection As NpgsqlConnection,
                                                         ByVal spName As String,
                                                         ByVal ParamArray parameterValues() As Object) As Integer
            If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
            Dim commandParameters As Npgsql.NpgsqlParameter()

            ' If we receive parameter values, we need to figure out where they go
            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                commandParameters = SqlDALParameterCache.GetSpParameterSet(connection, spName)

                ' Assign the provided values to these parameters based on parameter order
                AssignParameterValues(commandParameters, parameterValues)

                ' Call the overload that takes an array of SqlParameters
                Return ExecuteNonQuery(connection, CommandType.StoredProcedure, spName, commandParameters)
            Else ' Otherwise we can just call the SP without params
                Return ExecuteNonQuery(connection, CommandType.StoredProcedure, spName)
            End If

        End Function ' ExecuteNonQuery
        '***************************************************************************************************************************
        '     Function / Subroutine  Name: ExecuteNonQuery
        '     Purpose					 : Execute a NpgsqlCommand (that returns no resultset and takes no parameters) against the provided NpgsqlTransaction.

        '     Example                    :  
        '                                  Dim result As Integer = ExecuteNonQuery(trans, "PublishOrders")
        '     Parameters                 :
        '                                  transaction - a valid NpgsqlTransaction associated with the connection 
        '                                  commandType - the CommandType (stored procedure, text, etc.) 
        '                                  commandText - the stored procedure name or T-SQL command 
        '                                 
        '     Outputs					 :  Returns: An int representing the number of rows affected by the command 

        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 11/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteNonQuery(ByVal transaction As NpgsqlTransaction,
                                                 ByVal commandType As CommandType,
                                                 ByVal commandText As String) As Integer
            ' Pass through the call providing null for the set of SqlParameters
            Return ExecuteNonQuery(transaction, commandType, commandText, CType(Nothing, Npgsql.NpgsqlParameter()))
        End Function ' ExecuteNonQuery
        '***************************************************************************************************************************
        '     Function / Subroutine  Name: ExecuteNonQuery
        '     Purpose					 : Execute a NpgsqlCommand (that returns no resultset) against the specified NpgsqlTransaction
        '                                  using the provided parameters.

        '     Example                    :  
        '                                  Dim result As Integer = ExecuteNonQuery(trans, "GetOrders", new Npgsql.NpgsqlParameter("@prodid", 24))
        '     Parameters                 :
        '                                   transaction - a valid NpgsqlTransaction 
        '                                   commandType - the CommandType (stored procedure, text, etc.) 
        '                                   commandText - the stored procedure name or T-SQL command 
        '                                   commandParameters - an array of SqlParamters used to execute the command 
        '                                 
        '     Outputs					 :  Returns: An int representing the number of rows affected by the command 

        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 11/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteNonQuery(ByVal transaction As NpgsqlTransaction,
                                                 ByVal commandType As CommandType,
                                                 ByVal commandText As String,
                                                 ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter) As Integer

            If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
            If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")

            ' Create a command and prepare it for execution
            Dim cmd As New NpgsqlCommand
            Dim retval As Integer
            Dim mustCloseConnection As Boolean = False

            PrepareCommand(cmd, transaction.Connection, transaction, commandType, commandText, commandParameters, mustCloseConnection)

            ' Finally, execute the command
            Try
                retval = cmd.ExecuteNonQuery()
            Catch ex As Exception
                Dim postgresCommandText As String = "SELECT " & cmd.CommandText & "("

                Dim p As Npgsql.NpgsqlParameter
                For Each p In commandParameters
                    If p.NpgsqlDbType.ToString().ToLower() = "char" AndAlso Not p.Value Is DBNull.Value AndAlso Convert.ToString(p.Value) = vbNullChar Then
                        p.Value = DBNull.Value
                    End If
                    If (Not p Is Nothing) Then
                        ' Check for derived output value with no value assigned
                        If (p.Direction = ParameterDirection.InputOutput OrElse p.Direction = ParameterDirection.Input) AndAlso p.Value Is Nothing Then
                            p.Value = DBNull.Value
                        End If
                        p.NpgsqlValue = p.Value

                        postgresCommandText = postgresCommandText & If(postgresCommandText.EndsWith("("), "", ",") & If(p.NpgsqlDbType.ToString().ToLower() = "text" Or p.NpgsqlDbType.ToString().ToLower() = "nvarchar" Or p.NpgsqlDbType.ToString().ToLower() = "varchar", "'", "") & p.Value & If(p.NpgsqlDbType.ToString().ToLower() = "text" Or p.NpgsqlDbType.ToString().ToLower() = "nvarchar" Or p.NpgsqlDbType.ToString().ToLower() = "varchar", "'", "") & "::" & p.NpgsqlDbType.ToString()
                    End If
                Next p

                postgresCommandText = postgresCommandText & ");"

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                If cmd.CommandText <> "USP_ManageException" Then
                    Try
                        arParms(0) = New Npgsql.NpgsqlParameter("@vcMessage", NpgsqlTypes.NpgsqlDbType.Varchar)
                        arParms(0).Value = postgresCommandText

                        arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                        arParms(1).Value = 0

                        arParms(2) = New Npgsql.NpgsqlParameter("@numUserContactID", NpgsqlTypes.NpgsqlDbType.Bigint)
                        arParms(2).Value = 0

                        arParms(3) = New Npgsql.NpgsqlParameter("@vcURL", NpgsqlTypes.NpgsqlDbType.Varchar, 1000)
                        arParms(3).Value = ""

                        arParms(4) = New Npgsql.NpgsqlParameter("@vcReferrer", NpgsqlTypes.NpgsqlDbType.Varchar, 1000)
                        arParms(4).Value = ""

                        arParms(5) = New Npgsql.NpgsqlParameter("@vcBrowser", NpgsqlTypes.NpgsqlDbType.Varchar, 100)
                        arParms(5).Value = ""

                        arParms(6) = New Npgsql.NpgsqlParameter("@vcBrowserAgent", NpgsqlTypes.NpgsqlDbType.Varchar, 500)
                        arParms(6).Value = ""


                        ExecuteNonQuery(transaction.Connection, "USP_ManageException", arParms)
                    Catch exInner As Exception
                        'DO NOT RAISE ERROR
                    End Try
                End If

                Throw
            End Try

            ' Detach the SqlParameters from the command object, so they can be used again
            cmd.Parameters.Clear()

            Return retval
        End Function ' ExecuteNonQuery

        '***************************************************************************************************************************
        '     Function / Subroutine  Name: ExecuteNonQuery
        '     Purpose					 :  Execute a stored procedure via a NpgsqlCommand (that returns no resultset) against the specified NpgsqlTransaction 
        '                                   using the provided parameter values.  This method will discover the parameters for the 
        '                                   stored procedure, and assign the values based on parameter order.
        '                                   This method provides no access to output parameters or the stored procedure' s return value parameter.

        '     Example                    :  
        '                                  Dim result As Integer = SqlDAL.ExecuteNonQuery(trans, "PublishOrders", 24, 36)
        '     Parameters                 :
        '                                   transaction - a valid NpgsqlTransaction 
        '                                   spName - the name of the stored procedure 
        '                                   parameterValues - an array of objects to be assigned as the input values of the stored procedure 
        '                                 
        '     Outputs					 :  Returns: An int representing the number of rows affected by the command 

        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 11/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteNonQuery(ByVal transaction As NpgsqlTransaction,
                                                 ByVal spName As String,
                                                 ByVal ParamArray parameterValues() As Object) As Integer
            If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
            If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

            Dim commandParameters As Npgsql.NpgsqlParameter()

            ' If we receive parameter values, we need to figure out where they go
            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                commandParameters = SqlDALParameterCache.GetSpParameterSet(transaction.Connection, spName)

                ' Assign the provided values to these parameters based on parameter order
                AssignParameterValues(commandParameters, parameterValues)

                ' Call the overload that takes an array of SqlParameters
                Return ExecuteNonQuery(transaction, CommandType.StoredProcedure, spName, commandParameters)
            Else ' Otherwise we can just call the SP without params
                Return ExecuteNonQuery(transaction, CommandType.StoredProcedure, spName)
            End If
        End Function ' ExecuteNonQuery

#End Region


        Public Overloads Shared Function ExecuteDataTable1(ByVal connectionString As String,
                                             ByVal spName As String,
                                             ByVal ParamArray parameterValues() As Object) As DataTable

            If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
            Dim commandParameters As Npgsql.NpgsqlParameter()
            Dim dataAdatpter As NpgsqlDataAdapter
            Dim pobjDataSet As New DataSet
            ' If we receive parameter values, we need to figure out where they go
            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                commandParameters = SqlDALParameterCache.GetSpParameterSet(connectionString, spName)

                ' Assign the provided values to these parameters based on parameter order
                AssignParameterValues(commandParameters, parameterValues)

                dataAdatpter.Fill(pobjDataSet)
                ' Call the overload that takes an array of SqlParameters
                Return pobjDataSet.Tables(0)
            End If
        End Function





#Region "ExecuteDataTable"
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="connectionString"></param>
        ''' <param name="spName"></param>
        ''' <param name="parameterValues"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Overloads Shared Function ExecuteDatable(ByVal connectionString As String,
                                                ByVal spName As String,
                                                ByVal ParamArray parameterValues() As Object) As DataTable

            If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
            Dim commandParameters As Npgsql.NpgsqlParameter()

            ' If we receive parameter values, we need to figure out where they go
            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                commandParameters = SqlDALParameterCache.GetSpParameterSet(connectionString, spName)

                ' Assign the provided values to these parameters based on parameter order
                AssignParameterValues(commandParameters, parameterValues)

                Dim ds As DataSet = ExecuteDataset(connectionString, CommandType.StoredProcedure, spName, commandParameters)

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                    Return ds.Tables(0)
                Else
                    Return Nothing
                End If
            Else ' Otherwise we can just call the SP without params
                Return ExecuteDataset(connectionString, CommandType.StoredProcedure, spName).Tables(0)
            End If
        End Function ' ExecuteDataset
#End Region





#Region "ExecuteDataset"

        '***************************************************************************************************************************
        '     Function / Subroutine  Name: ExecuteDataset
        '     Purpose					 :  Execute a NpgsqlCommand (that returns a resultset and takes no parameters) against the database specified in 
        '                                   the connection string.
        '     Example                    :  
        '                                   Dim ds As DataSet = SqlDAL.ExecuteDataset("", "GetOrders")
        '     Parameters                 :
        '                                   connectionString - a valid connection string for a NpgsqlConnection
        '                                   commandType - the CommandType (stored procedure, text, etc.)
        '                                   commandText - the stored procedure name or T-SQL command
        '                                 
        '     Outputs					 : Returns: A dataset containing the resultset generated by the command

        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 11/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteDataset(ByVal connectionString As String,
                                                ByVal commandType As CommandType,
                                                ByVal commandText As String) As DataSet
            ' Pass through the call providing null for the set of SqlParameters
            Return ExecuteDataset(connectionString, commandType, commandText, CType(Nothing, Npgsql.NpgsqlParameter()))
        End Function ' ExecuteDataset
        '***************************************************************************************************************************
        '     Function / Subroutine  Name: ExecuteDataset
        '     Purpose					 :  Execute a NpgsqlCommand (that returns a resultset) against the database specified in the connection string 
        '                                   using the provided parameters.

        '     Example                    :  
        '                                   Dim ds As Dataset = ExecuteDataset(connString, "GetOrders", new Npgsql.NpgsqlParameter("@prodid", 24))
        '     Parameters                 :
        '                                   connectionString - a valid connection string for a NpgsqlConnection
        '                                   commandType - the CommandType (stored procedure, text, etc.)
        '                                   commandText - the stored procedure name or T-SQL command
        '                                   commandParameters - an array of SqlParamters used to execute the command
        '                                 
        '     Outputs					 : Returns: A dataset containing the resultset generated by the command

        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 11/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteDataset(ByVal connectionString As String,
                                                ByVal commandType As CommandType,
                                                ByVal commandText As String,
                                                ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter) As DataSet

            If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")

            ' Create & open a NpgsqlConnection, and dispose of it after we are done
            Dim connection As NpgsqlConnection
            Try
                connection = New NpgsqlConnection(connectionString)
                If connection.State <> ConnectionState.Open Then
                    connection.Open()
                End If

                ' Call the overload that takes a connection in place of the connection string
                Return ExecuteDataset(connection, commandType, commandText, commandParameters)
            Catch ex As Exception
                Throw
            Finally
                If Not connection Is Nothing Then
                    connection.Dispose()
                End If
            End Try
        End Function ' ExecuteDataset
        '***************************************************************************************************************************
        '     Function / Subroutine  Name: ExecuteDataset
        '     Purpose					 : Execute a stored procedure via a NpgsqlCommand (that returns a resultset) against the database specified in 
        '                                  the connection string using the provided parameter values.  This method will discover the parameters for the 
        '                                  stored procedure, and assign the values based on parameter order.
        '                                  This method provides no access to output parameters or the stored procedure' s return value parameter.
        '     Example                    :  
        '                                  Dim ds As Dataset= ExecuteDataset(connString, "GetOrders", 24, 36)
        '     Parameters                 :
        '                                  connectionString - a valid connection string for a NpgsqlConnection
        '                                  spName - the name of the stored procedure
        '                                  parameterValues - an array of objects to be assigned as the input values of the stored procedure
        '                                 
        '     Outputs					 : Returns: A dataset containing the resultset generated by the command

        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 11/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteDataset(ByVal connectionString As String,
                                                ByVal spName As String,
                                                ByVal ParamArray parameterValues() As Object) As DataSet

            If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
            Dim commandParameters As Npgsql.NpgsqlParameter()

            ' If we receive parameter values, we need to figure out where they go
            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                commandParameters = SqlDALParameterCache.GetSpParameterSet(connectionString, spName)

                ' Assign the provided values to these parameters based on parameter order
                AssignParameterValues(commandParameters, parameterValues)
                parameterValues = commandParameters

                ' Call the overload that takes an array of SqlParameters
                Return ExecuteDataset(connectionString, CommandType.StoredProcedure, spName, commandParameters)
            Else ' Otherwise we can just call the SP without params
                Return ExecuteDataset(connectionString, CommandType.StoredProcedure, spName)
            End If
        End Function ' ExecuteDataset

        Public Overloads Shared Function ExecuteDatasetWithOutPut(ByVal connectionString As String,
                                                        ByVal spName As String,
                                                        ByRef parameterValues() As Object,
                                                        ByVal isOutPutNeeded As Boolean) As DataSet

            If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
            Dim commandParameters As Npgsql.NpgsqlParameter()
            Dim dsResult As New DataSet

            ' If we receive parameter values, we need to figure out where they go
            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                commandParameters = SqlDALParameterCache.GetSpParameterSet(connectionString, spName)

                ' Assign the provided values to these parameters based on parameter order
                AssignParameterValues(commandParameters, parameterValues)

                ' Call the overload that takes an array of SqlParameters
                dsResult = ExecuteDataset(connectionString, CommandType.StoredProcedure, spName, commandParameters)
                If isOutPutNeeded = True Then
                    parameterValues = commandParameters
                End If

            Else ' Otherwise we can just call the SP without params
                dsResult = ExecuteDataset(connectionString, CommandType.StoredProcedure, spName)
            End If

            Return dsResult

        End Function ' ExecuteDataset


        '***************************************************************************************************************************
        '     Function / Subroutine  Name: ExecuteDataset
        '     Purpose					 : Execute a NpgsqlCommand (that returns a resultset and takes no parameters) against the provided NpgsqlConnection. 

        '     Example                    :  
        '                                   Dim ds As Dataset = ExecuteDataset(conn, "GetOrders")
        '     Parameters                 :
        '                                   connection - a valid NpgsqlConnection
        '                                   commandType - the CommandType (stored procedure, text, etc.)
        '                                   commandText - the stored procedure name or T-SQL command
        '                                 
        '     Outputs					 : Returns: A dataset containing the resultset generated by the command

        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 12/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteDataset(ByVal connection As NpgsqlConnection,
                                                        ByVal commandType As CommandType,
                                                        ByVal commandText As String) As DataSet

            ' Pass through the call providing null for the set of SqlParameters
            Return ExecuteDataset(connection, commandType, commandText, CType(Nothing, Npgsql.NpgsqlParameter()))
        End Function ' ExecuteDataset
        '***************************************************************************************************************************
        '     Function / Subroutine  Name: ExecuteDataset
        '     Purpose					 :  Execute a NpgsqlCommand (that returns a resultset) against the specified NpgsqlConnection 
        '                                   using the provided parameters.
        '     Example                    :  
        '                                    Dim ds As Dataset = ExecuteDataset(conn, "GetOrders", new Npgsql.NpgsqlParameter("@prodid", 24))
        '     Parameters                 :
        '                                    connection - a valid NpgsqlConnection
        '                                    commandType - the CommandType (stored procedure, text, etc.)
        '                                    commandText - the stored procedure name or T-SQL command
        '                                    commandParameters - an array of SqlParamters used to execute the command
        '                                 
        '     Outputs					 : Returns: A dataset containing the resultset generated by the command

        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 11/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteDataset(ByVal connection As NpgsqlConnection,
                                                        ByVal commandType As CommandType,
                                                        ByVal commandText As String,
                                                        ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter) As DataSet
            If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
            ' Create a command and prepare it for execution
            Dim cmd As New NpgsqlCommand
            Dim ds As New DataSet
            Dim mustCloseConnection As Boolean = False

            PrepareCommand(cmd, connection, CType(Nothing, NpgsqlTransaction), commandType, commandText, commandParameters, mustCloseConnection)

            Try
                Using objTransaction As NpgsqlTransaction = connection.BeginTransaction()
                    ExecuteCommand(connection, cmd, ds, commandParameters)

                    objTransaction.Commit()
                End Using
            Catch ex As Exception When TypeOf ex Is System.InvalidOperationException AndAlso ex.Message = "A transaction is already in progress; nested/concurrent transactions aren't supported."
                ExecuteCommand(connection, cmd, ds, commandParameters)
            Catch ex As Exception
                Dim postgresCommandText As String = "SELECT " & cmd.CommandText & "("

                Dim p As Npgsql.NpgsqlParameter
                For Each p In commandParameters
                    If p.NpgsqlDbType.ToString().ToLower() = "char" AndAlso Not p.Value Is DBNull.Value AndAlso Convert.ToString(p.Value) = vbNullChar Then
                        p.Value = DBNull.Value
                    End If
                    If (Not p Is Nothing) Then
                        ' Check for derived output value with no value assigned
                        If (p.Direction = ParameterDirection.InputOutput OrElse p.Direction = ParameterDirection.Input) AndAlso p.Value Is Nothing Then
                            p.Value = DBNull.Value
                        End If
                        p.NpgsqlValue = p.Value

                        postgresCommandText = postgresCommandText & If(postgresCommandText.EndsWith("("), "", ",") & If(p.NpgsqlDbType.ToString().ToLower() = "text" Or p.NpgsqlDbType.ToString().ToLower() = "nvarchar" Or p.NpgsqlDbType.ToString().ToLower() = "varchar", "'", "") & p.Value & If(p.NpgsqlDbType.ToString().ToLower() = "text" Or p.NpgsqlDbType.ToString().ToLower() = "nvarchar" Or p.NpgsqlDbType.ToString().ToLower() = "varchar", "'", "") & "::" & p.NpgsqlDbType.ToString()
                    End If
                Next p

                postgresCommandText = postgresCommandText & ");"

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                If cmd.CommandText <> "USP_ManageException" Then
                    Try
                        arParms(0) = New Npgsql.NpgsqlParameter("@vcMessage", NpgsqlTypes.NpgsqlDbType.Varchar)
                        arParms(0).Value = postgresCommandText

                        arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                        arParms(1).Value = 0

                        arParms(2) = New Npgsql.NpgsqlParameter("@numUserContactID", NpgsqlTypes.NpgsqlDbType.Bigint)
                        arParms(2).Value = 0

                        arParms(3) = New Npgsql.NpgsqlParameter("@vcURL", NpgsqlTypes.NpgsqlDbType.Varchar, 1000)
                        arParms(3).Value = ""

                        arParms(4) = New Npgsql.NpgsqlParameter("@vcReferrer", NpgsqlTypes.NpgsqlDbType.Varchar, 1000)
                        arParms(4).Value = ""

                        arParms(5) = New Npgsql.NpgsqlParameter("@vcBrowser", NpgsqlTypes.NpgsqlDbType.Varchar, 100)
                        arParms(5).Value = ""

                        arParms(6) = New Npgsql.NpgsqlParameter("@vcBrowserAgent", NpgsqlTypes.NpgsqlDbType.Varchar, 500)
                        arParms(6).Value = ""


                        ExecuteNonQuery(connection, "USP_ManageException", arParms)
                    Catch exInner As Exception
                        'DO NOT RAISE ERROR
                    End Try
                End If

                Throw
            Finally
                connection.Close()
            End Try

            ' Return the dataset
            Return ds
        End Function ' ExecuteDataset

        Public Overloads Shared Function ExecuteDatasetCodeLevelTransaction(ByVal connection As NpgsqlConnection,
                                                        ByVal commandType As CommandType,
                                                        ByVal commandText As String,
                                                        ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter) As DataSet
            If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
            ' Create a command and prepare it for execution
            Dim cmd As New NpgsqlCommand
            Dim ds As New DataSet
            Dim dataAdatpter As NpgsqlDataAdapter
            Dim mustCloseConnection As Boolean = False

            PrepareCommand(cmd, connection, CType(Nothing, NpgsqlTransaction), commandType, commandText, commandParameters, mustCloseConnection)

            Dim i As Integer = 0

            Try
                cmd.ExecuteNonQuery()

                For Each parm As NpgsqlParameter In commandParameters
                    If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                        If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                            Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                            dataAdatpter = New NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                            ds.Tables.Add("Table" & i)
                            dataAdatpter.Fill(ds.Tables(i))
                            i += 1
                        End If
                    End If
                Next

                cmd.Parameters.Clear()
            Catch ex As Exception
                Dim postgresCommandText As String = "SELECT " & cmd.CommandText & "("

                Dim p As Npgsql.NpgsqlParameter
                For Each p In commandParameters
                    If p.NpgsqlDbType.ToString().ToLower() = "char" AndAlso Not p.Value Is DBNull.Value AndAlso Convert.ToString(p.Value) = vbNullChar Then
                        p.Value = DBNull.Value
                    End If
                    If (Not p Is Nothing) Then
                        ' Check for derived output value with no value assigned
                        If (p.Direction = ParameterDirection.InputOutput OrElse p.Direction = ParameterDirection.Input) AndAlso p.Value Is Nothing Then
                            p.Value = DBNull.Value
                        End If
                        p.NpgsqlValue = p.Value

                        postgresCommandText = postgresCommandText & If(postgresCommandText.EndsWith("("), "", ",") & If(p.NpgsqlDbType.ToString().ToLower() = "text" Or p.NpgsqlDbType.ToString().ToLower() = "nvarchar" Or p.NpgsqlDbType.ToString().ToLower() = "varchar", "'", "") & p.Value & If(p.NpgsqlDbType.ToString().ToLower() = "text" Or p.NpgsqlDbType.ToString().ToLower() = "nvarchar" Or p.NpgsqlDbType.ToString().ToLower() = "varchar", "'", "") & "::" & p.NpgsqlDbType.ToString()
                    End If
                Next p

                postgresCommandText = postgresCommandText & ");"

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                If cmd.CommandText <> "USP_ManageException" Then
                    Try
                        arParms(0) = New Npgsql.NpgsqlParameter("@vcMessage", NpgsqlTypes.NpgsqlDbType.Varchar)
                        arParms(0).Value = postgresCommandText

                        arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                        arParms(1).Value = 0

                        arParms(2) = New Npgsql.NpgsqlParameter("@numUserContactID", NpgsqlTypes.NpgsqlDbType.Bigint)
                        arParms(2).Value = 0

                        arParms(3) = New Npgsql.NpgsqlParameter("@vcURL", NpgsqlTypes.NpgsqlDbType.Varchar, 1000)
                        arParms(3).Value = ""

                        arParms(4) = New Npgsql.NpgsqlParameter("@vcReferrer", NpgsqlTypes.NpgsqlDbType.Varchar, 1000)
                        arParms(4).Value = ""

                        arParms(5) = New Npgsql.NpgsqlParameter("@vcBrowser", NpgsqlTypes.NpgsqlDbType.Varchar, 100)
                        arParms(5).Value = ""

                        arParms(6) = New Npgsql.NpgsqlParameter("@vcBrowserAgent", NpgsqlTypes.NpgsqlDbType.Varchar, 500)
                        arParms(6).Value = ""


                        ExecuteNonQuery(connection, "USP_ManageException", arParms)
                    Catch exInner As Exception
                        'DO NOT RAISE ERROR
                    End Try
                End If

                Throw
            Finally
                connection.Close()
            End Try

            ' Return the dataset
            Return ds
        End Function ' ExecuteDataset

        '***************************************************************************************************************************
        '     Function / Subroutine  Name: ExecuteDataset
        '     Purpose					 :  Execute a stored procedure via a NpgsqlCommand (that returns a resultset) against the specified NpgsqlConnection 
        '                                   using the provided parameter values.  This method will discover the parameters for the 
        '                                   stored procedure, and assign the values based on parameter order.
        '                                   This method provides no access to output parameters or the stored procedure' s return value parameter.

        '     Example                    :  
        '                                    Dim ds As Dataset = ExecuteDataset(conn, "GetOrders", 24, 36)
        '     Parameters                 :
        '                                    connection - a valid NpgsqlConnection
        '                                    spName - the name of the stored procedure
        '                                    parameterValues - an array of objects to be assigned as the input values of the stored procedure
        '                                 
        '     Outputs					 : Returns: A dataset containing the resultset generated by the command

        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 12/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteDataset(ByVal connection As NpgsqlConnection,
                                                        ByVal spName As String,
                                                        ByVal ParamArray parameterValues() As Object) As DataSet

            If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

            Dim commandParameters As Npgsql.NpgsqlParameter()

            ' If we receive parameter values, we need to figure out where they go
            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                commandParameters = SqlDALParameterCache.GetSpParameterSet(connection, spName)

                ' Assign the provided values to these parameters based on parameter order
                AssignParameterValues(commandParameters, parameterValues)

                ' Call the overload that takes an array of SqlParameters
                Return ExecuteDataset(connection, CommandType.StoredProcedure, spName, commandParameters)
            Else ' Otherwise we can just call the SP without params
                Return ExecuteDataset(connection, CommandType.StoredProcedure, spName)
            End If

        End Function ' ExecuteDataset


        Public Overloads Shared Function ExecuteDatasetCpdeLevelTransaction(ByVal connection As NpgsqlConnection,
                                                        ByVal spName As String,
                                                        ByVal ParamArray parameterValues() As Object) As DataSet

            If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

            Dim commandParameters As Npgsql.NpgsqlParameter()

            ' If we receive parameter values, we need to figure out where they go
            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                commandParameters = SqlDALParameterCache.GetSpParameterSet(connection, spName)

                ' Assign the provided values to these parameters based on parameter order
                AssignParameterValues(commandParameters, parameterValues)

                ' Call the overload that takes an array of SqlParameters
                Return ExecuteDataset(connection, CommandType.StoredProcedure, spName, commandParameters)
            Else ' Otherwise we can just call the SP without params
                Return ExecuteDataset(connection, CommandType.StoredProcedure, spName)
            End If

        End Function ' ExecuteDataset

        '***************************************************************************************************************************
        '     Function / Subroutine  Name: ExecuteReader
        '     Purpose					 :  Execute a NpgsqlCommand (that returns a resultset and takes no parameters) against the provided NpgsqlTransaction. 


        '     Example                    :  
        '                                   Dim ds As Dataset = ExecuteDataset(trans, "GetOrders")

        '     Parameters                 :
        '                                   transaction - a valid NpgsqlTransaction
        '                                   commandType - the CommandType (stored procedure, text, etc.)
        '                                   commandText - the stored procedure name or T-SQL command
        '     Outputs					 :  returns: A NpgsqlDataReader containing the resultset generated by the command 

        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 14/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteDataset(ByVal transaction As NpgsqlTransaction,
                                                        ByVal commandType As CommandType,
                                                        ByVal commandText As String) As DataSet
            ' Pass through the call providing null for the set of SqlParameters
            Return ExecuteDataset(transaction, commandType, commandText, CType(Nothing, Npgsql.NpgsqlParameter()))
        End Function ' ExecuteDataset
        '***************************************************************************************************************************
        '     Function / Subroutine  Name: ExecuteDataset
        '     Purpose					 : Execute a NpgsqlCommand (that returns a resultset) against the specified NpgsqlTransaction
        '                                  using the provided parameters.

        '     Example                    :  
        '                                   Dim ds As Dataset = ExecuteDataset(trans, "GetOrders", new Npgsql.NpgsqlParameter("@prodid", 24))
        '     Parameters                 :
        '                                   transaction - a valid NpgsqlTransaction 
        '                                   commandType - the CommandType (stored procedure, text, etc.)
        '                                   commandText - the stored procedure name or T-SQL command
        '                                   commandParameters - an array of SqlParamters used to execute the command
        '                                 
        '     Outputs					 : Returns: A dataset containing the resultset generated by the command

        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 12/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteDataset(ByVal transaction As NpgsqlTransaction,
                                                ByVal commandType As CommandType,
                                                ByVal commandText As String,
                                                ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter) As DataSet
            If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
            If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")

            ' Create a command and prepare it for execution
            Dim cmd As New NpgsqlCommand
            Dim ds As New DataSet
            Dim dataAdatpter As NpgsqlDataAdapter
            Dim mustCloseConnection As Boolean = False

            PrepareCommand(cmd, transaction.Connection, transaction, commandType, commandText, commandParameters, mustCloseConnection)

            Try
                Using objTransaction As NpgsqlTransaction = transaction.Connection.BeginTransaction()
                    ExecuteCommand(transaction.Connection, cmd, ds, commandParameters)

                    objTransaction.Commit()
                End Using
            Catch ex As Exception When TypeOf ex Is System.InvalidOperationException AndAlso ex.Message = "A transaction is already in progress; nested/concurrent transactions aren't supported."
                ExecuteCommand(transaction.Connection, cmd, ds, commandParameters)
            Catch ex As Exception
                Dim postgresCommandText As String = "SELECT " & cmd.CommandText & "("

                Dim p As Npgsql.NpgsqlParameter
                For Each p In commandParameters
                    If p.NpgsqlDbType.ToString().ToLower() = "char" AndAlso Not p.Value Is DBNull.Value AndAlso Convert.ToString(p.Value) = vbNullChar Then
                        p.Value = DBNull.Value
                    End If
                    If (Not p Is Nothing) Then
                        ' Check for derived output value with no value assigned
                        If (p.Direction = ParameterDirection.InputOutput OrElse p.Direction = ParameterDirection.Input) AndAlso p.Value Is Nothing Then
                            p.Value = DBNull.Value
                        End If
                        p.NpgsqlValue = p.Value

                        postgresCommandText = postgresCommandText & If(postgresCommandText.EndsWith("("), "", ",") & If(p.NpgsqlDbType.ToString().ToLower() = "text" Or p.NpgsqlDbType.ToString().ToLower() = "nvarchar" Or p.NpgsqlDbType.ToString().ToLower() = "varchar", "'", "") & p.Value & If(p.NpgsqlDbType.ToString().ToLower() = "text" Or p.NpgsqlDbType.ToString().ToLower() = "nvarchar" Or p.NpgsqlDbType.ToString().ToLower() = "varchar", "'", "") & "::" & p.NpgsqlDbType.ToString()
                    End If
                Next p

                postgresCommandText = postgresCommandText & ");"

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                If cmd.CommandText <> "USP_ManageException" Then
                    Try
                        arParms(0) = New Npgsql.NpgsqlParameter("@vcMessage", NpgsqlTypes.NpgsqlDbType.Varchar)
                        arParms(0).Value = postgresCommandText

                        arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                        arParms(1).Value = 0

                        arParms(2) = New Npgsql.NpgsqlParameter("@numUserContactID", NpgsqlTypes.NpgsqlDbType.Bigint)
                        arParms(2).Value = 0

                        arParms(3) = New Npgsql.NpgsqlParameter("@vcURL", NpgsqlTypes.NpgsqlDbType.Varchar, 1000)
                        arParms(3).Value = ""

                        arParms(4) = New Npgsql.NpgsqlParameter("@vcReferrer", NpgsqlTypes.NpgsqlDbType.Varchar, 1000)
                        arParms(4).Value = ""

                        arParms(5) = New Npgsql.NpgsqlParameter("@vcBrowser", NpgsqlTypes.NpgsqlDbType.Varchar, 100)
                        arParms(5).Value = ""

                        arParms(6) = New Npgsql.NpgsqlParameter("@vcBrowserAgent", NpgsqlTypes.NpgsqlDbType.Varchar, 500)
                        arParms(6).Value = ""


                        ExecuteNonQuery(transaction.Connection, "USP_ManageException", arParms)
                    Catch exInner As Exception
                        'DO NOT RAISE ERROR
                    End Try
                End If

                Throw
            Finally
                transaction.Connection.Close()
            End Try

            ' Return the dataset
            Return ds

        End Function ' ExecuteDataset

        '***************************************************************************************************************************
        '     Function / Subroutine  Name: ExecuteDataset
        '     Purpose					 : Execute a stored procedure via a NpgsqlCommand (that returns a resultset) against the specified
        '                                  NpgsqlTransaction using the provided parameter values.  This method will discover the parameters for the 
        '                                  stored procedure, and assign the values based on parameter order.
        '                                  This method provides no access to output parameters or the stored procedure' s return value parameter.

        '     Example                    :  
        '                                   Dim ds As Dataset = ExecuteDataset(trans, "GetOrders", 24, 36)
        '     Parameters                 :
        '                                  transaction - a valid NpgsqlTransaction 
        '                                  spName - the name of the stored procedure
        '                                  parameterValues - an array of objects to be assigned as the input values of the stored procedure
        '                                 
        '     Outputs					 : Returns: A dataset containing the resultset generated by the command

        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 12/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteDataset(ByVal transaction As NpgsqlTransaction,
                                                ByVal spName As String,
                                                ByVal ParamArray parameterValues() As Object) As DataSet

            If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
            If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

            Dim commandParameters As Npgsql.NpgsqlParameter()

            ' If we receive parameter values, we need to figure out where they go
            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                commandParameters = SqlDALParameterCache.GetSpParameterSet(transaction.Connection, spName)

                ' Assign the provided values to these parameters based on parameter order
                AssignParameterValues(commandParameters, parameterValues)

                ' Call the overload that takes an array of SqlParameters
                Return ExecuteDataset(transaction, CommandType.StoredProcedure, spName, commandParameters)
            Else ' Otherwise we can just call the SP without params
                Return ExecuteDataset(transaction, CommandType.StoredProcedure, spName)
            End If
        End Function ' ExecuteDataset

#End Region

#Region "ExecuteReader"
        ' this enum is used to indicate whether the connection was provided by the caller, or created by SqlDAL, so that
        ' we can set the appropriate CommandBehavior when calling ExecuteReader()
        Private Enum SqlConnectionOwnership
            ' Connection is owned and managed by SqlDAL
            Internal
            ' Connection is owned and managed by the caller
            [External]
        End Enum ' SqlConnectionOwnership
        '***************************************************************************************************************************
        '     Function / Subroutine  Name: ExecuteReader
        '     Purpose					 :  Create and prepare a NpgsqlCommand, and call ExecuteReader with the appropriate CommandBehavior.
        '                                   If we created and opened the connection, we want the connection to be closed when the DataReader is closed.
        '                                   If the caller provided the connection, we want to leave it to them to manage.

        '     Example                    :  
        '                                   Dim ds As Dataset = ExecuteDataset(trans, "GetOrders", 24, 36)
        '     Parameters                 :
        '                                   connection - a valid NpgsqlConnection, on which to execute this command 
        '                                   transaction - a valid NpgsqlTransaction, or ' null' 
        '                                   commandType - the CommandType (stored procedure, text, etc.) 
        '                                   commandText - the stored procedure name or T-SQL command 
        '                                   commandParameters - an array of SqlParameters to be associated with the command or ' null' if no parameters are required 
        '                                   connectionOwnership - indicates whether the connection parameter was provided by the caller, or created by SqlDAL 
        '     Outputs					 : Returns: NpgsqlDataReader containing the results of the command

        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 12/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Private Overloads Shared Function ExecuteReader(ByVal connection As NpgsqlConnection,
                                                ByVal transaction As NpgsqlTransaction,
                                                ByVal commandType As CommandType,
                                                ByVal commandText As String,
                                                ByVal commandParameters() As Npgsql.NpgsqlParameter,
                                                ByVal connectionOwnership As SqlConnectionOwnership) As NpgsqlDataReader

            If (connection Is Nothing) Then Throw New ArgumentNullException("connection")

            Dim mustCloseConnection As Boolean = False
            ' Create a command and prepare it for execution
            Dim cmd As New NpgsqlCommand
            Try
                ' Create a reader
                Dim dataReader As NpgsqlDataReader

                PrepareCommand(cmd, connection, transaction, commandType, commandText, commandParameters, mustCloseConnection)

                ' Call ExecuteReader with the appropriate CommandBehavior
                If connectionOwnership = SqlConnectionOwnership.External Then
                    dataReader = cmd.ExecuteReader()
                Else
                    dataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                End If

                ' Detach the SqlParameters from the command object, so they can be used again
                Dim canClear As Boolean = True
                Dim commandParameter As Npgsql.NpgsqlParameter
                For Each commandParameter In cmd.Parameters
                    If commandParameter.Direction <> ParameterDirection.Input Then
                        canClear = False
                    End If
                Next

                If (canClear) Then cmd.Parameters.Clear()

                Return dataReader
            Catch
                If (mustCloseConnection) Then connection.Close()
                Throw
            End Try
        End Function ' ExecuteReader

        '***************************************************************************************************************************
        '     Function / Subroutine  Name: ExecuteReader
        '     Purpose					 :  Execute a NpgsqlCommand (that returns a resultset and takes no parameters) against the database specified in 
        '                                   the connection string. 

        '     Example                    :  
        '                                   Dim dr As NpgsqlDataReader = ExecuteReader(connString, "GetOrders")
        '     Parameters                 :
        '                                   connectionString - a valid connection string for a NpgsqlConnection 
        '                                   commandType - the CommandType (stored procedure, text, etc.) 
        '                                   commandText - the stored procedure name or T-SQL command 
        '     Outputs					 : Returns: NpgsqlDataReader containing the results of the command

        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 12/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '    -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '    -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteReader(ByVal connectionString As String,
                                               ByVal commandType As CommandType,
                                               ByVal commandText As String) As NpgsqlDataReader
            ' Pass through the call providing null for the set of SqlParameters
            Return ExecuteReader(connectionString, commandType, commandText, CType(Nothing, Npgsql.NpgsqlParameter()))
        End Function ' ExecuteReader
        '***************************************************************************************************************************
        '     Function / Subroutine  Name: ExecuteReader
        '     Purpose					 :  Execute a NpgsqlCommand (that returns a resultset) against the database specified in the connection string 
        '                                   using the provided parameters.


        '     Example                    :  
        '                                   Dim dr As NpgsqlDataReader = ExecuteReader(connString, "GetOrders", new Npgsql.NpgsqlParameter("@prodid", 24))

        '     Parameters                 :
        '                                   connectionString - a valid connection string for a NpgsqlConnection 
        '                                   commandType - the CommandType (stored procedure, text, etc.) 
        '                                   commandText - the stored procedure name or T-SQL command 
        '                                   commandParameters - an array of SqlParamters used to execute the command 
        '     Outputs					 :  Returns: NpgsqlDataReader containing the results of the command

        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 13/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteReader(ByVal connectionString As String,
                                                       ByVal commandType As CommandType,
                                                       ByVal commandText As String,
                                                       ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter) As NpgsqlDataReader
            If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")

            ' Create & open a NpgsqlConnection
            Dim connection As NpgsqlConnection
            Try
                connection = New NpgsqlConnection(connectionString)
                If connection.State <> ConnectionState.Open Then
                    connection.Open()
                End If
                ' Call the private overload that takes an internally owned connection in place of the connection string
                Return ExecuteReader(connection, CType(Nothing, NpgsqlTransaction), commandType, commandText, commandParameters, SqlConnectionOwnership.Internal)
            Catch
                ' If we fail to return the SqlDatReader, we need to close the connection ourselves
                If Not connection Is Nothing Then
                    connection.Dispose()
                End If
                Throw
            End Try
        End Function ' ExecuteReader
        '***************************************************************************************************************************
        '     Function / Subroutine  Name: ExecuteReader
        '     Purpose					 : Execute a stored procedure via a NpgsqlCommand (that returns a resultset) against the database specified in 
        '                                  the connection string using the provided parameter values.  This method will discover the parameters for the 
        '                                  stored procedure, and assign the values based on parameter order.
        '                                  This method provides no access to output parameters or the stored procedure' s return value parameter.


        '     Example                    :  
        '                                   Dim dr As NpgsqlDataReader = ExecuteReader(connString, "GetOrders", 24, 36)

        '     Parameters                 :
        '                                   connectionString - a valid connection string for a NpgsqlConnection 
        '                                   spName - the name of the stored procedure 
        '                                   parameterValues - an array of objects to be assigned as the input values of the stored procedure 
        '     Outputs					 :  returns: A NpgsqlDataReader containing the resultset generated by the command 

        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 14/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteReader(ByVal connectionString As String,
                                               ByVal spName As String,
                                               ByVal ParamArray parameterValues() As Object) As NpgsqlDataReader
            If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

            Dim commandParameters As Npgsql.NpgsqlParameter()

            ' If we receive parameter values, we need to figure out where they go
            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                commandParameters = SqlDALParameterCache.GetSpParameterSet(connectionString, spName)

                ' Assign the provided values to these parameters based on parameter order
                AssignParameterValues(commandParameters, parameterValues)

                ' Call the overload that takes an array of SqlParameters
                Return ExecuteReader(connectionString, CommandType.StoredProcedure, spName, commandParameters)
                ' Otherwise we can just call the SP without params
            Else
                Return ExecuteReader(connectionString, CommandType.StoredProcedure, spName)
            End If
        End Function ' ExecuteReader
        '***************************************************************************************************************************
        '     Function / Subroutine  Name: ExecuteReader
        '     Purpose					 :  Execute a NpgsqlCommand (that returns a resultset and takes no parameters) against the provided NpgsqlConnection. 


        '     Example                    :  
        '                                   Dim dr As NpgsqlDataReader = ExecuteReader(conn, "GetOrders")

        '     Parameters                 :
        '                                   connection - a valid NpgsqlConnection 
        '                                   commandType - the CommandType (stored procedure, text, etc.) 
        '                                   commandText - the stored procedure name or T-SQL command 
        '     Outputs					 :  returns: A NpgsqlDataReader containing the resultset generated by the command 

        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 14/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteReader(ByVal connection As NpgsqlConnection,
                                                       ByVal commandType As CommandType,
                                                       ByVal commandText As String) As NpgsqlDataReader

            Return ExecuteReader(connection, commandType, commandText, CType(Nothing, Npgsql.NpgsqlParameter()))

        End Function ' ExecuteReader
        '***************************************************************************************************************************
        '     Function / Subroutine  Name: ExecuteReader
        '     Purpose					 :  Execute a NpgsqlCommand (that returns a resultset) against the specified NpgsqlConnection 
        '                                   using the provided parameters.

        '     Example                    :  
        '                                   Dim dr As NpgsqlDataReader = ExecuteReader(conn, "GetOrders", new Npgsql.NpgsqlParameter("@prodid", 24))

        '     Parameters                 :
        '                                  connection - a valid NpgsqlConnection 
        '                                  commandType - the CommandType (stored procedure, text, etc.) 
        '                                  commandText - the stored procedure name or T-SQL command 
        '                                  commandParameters - an array of SqlParamters used to execute the command 
        '     Outputs					 :  returns: A NpgsqlDataReader containing the resultset generated by the command 

        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 15/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteReader(ByVal connection As NpgsqlConnection,
                                               ByVal commandType As CommandType,
                                               ByVal commandText As String,
                                               ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter) As NpgsqlDataReader
            ' Pass through the call to private overload using a null transaction value
            Return ExecuteReader(connection, CType(Nothing, NpgsqlTransaction), commandType, commandText, commandParameters, SqlConnectionOwnership.External)

        End Function ' ExecuteReader
        '***************************************************************************************************************************
        '     Function / Subroutine  Name: ExecuteReader
        '     Purpose					 :  Execute a stored procedure via a NpgsqlCommand (that returns a resultset) against the specified NpgsqlConnection 
        '                                   using the provided parameter values.  This method will discover the parameters for the 
        '                                   stored procedure, and assign the values based on parameter order.
        '                                   This method provides no access to output parameters or the stored procedure' s return value parameter.

        '     Example                    :  
        '                                   Dim dr As NpgsqlDataReader = ExecuteReader(conn, "GetOrders", 24, 36)

        '     Parameters                 :
        '                                   connection - a valid NpgsqlConnection 
        '                                   SpName - the name of the stored procedure 
        '                                   parameterValues - an array of objects to be assigned as the input values of the stored procedure 
        '     Outputs					 :  returns: A NpgsqlDataReader containing the resultset generated by the command 

        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 16/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteReader(ByVal connection As NpgsqlConnection,
                                               ByVal spName As String,
                                               ByVal ParamArray parameterValues() As Object) As NpgsqlDataReader
            If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

            Dim commandParameters As Npgsql.NpgsqlParameter()
            ' If we receive parameter values, we need to figure out where they go
            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                commandParameters = SqlDALParameterCache.GetSpParameterSet(connection, spName)

                AssignParameterValues(commandParameters, parameterValues)

                Return ExecuteReader(connection, CommandType.StoredProcedure, spName, commandParameters)
            Else ' Otherwise we can just call the SP without params
                Return ExecuteReader(connection, CommandType.StoredProcedure, spName)
            End If

        End Function ' ExecuteReader
        '***************************************************************************************************************************
        '     Function / Subroutine  Name: ExecuteReader
        '     Purpose					 :   Execute a NpgsqlCommand (that returns a resultset and takes no parameters) against the provided NpgsqlTransaction.
        '
        '     Example                    :  
        '                                    Dim dr As NpgsqlDataReader = ExecuteReader(trans, "GetOrders")

        '     Parameters                 :
        '                                    transaction - a valid NpgsqlTransaction  
        '                                    commandType - the CommandType (stored procedure, text, etc.) 
        '                                    commandText - the stored procedure name or T-SQL command 
        '     Outputs					 :  returns: A NpgsqlDataReader containing the resultset generated by the command 

        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 16/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteReader(ByVal transaction As NpgsqlTransaction,
                                                       ByVal commandType As CommandType,
                                                       ByVal commandText As String) As NpgsqlDataReader
            ' Pass through the call providing null for the set of SqlParameters
            Return ExecuteReader(transaction, commandType, commandText, CType(Nothing, Npgsql.NpgsqlParameter()))
        End Function ' ExecuteReader
        '***************************************************************************************************************************
        '     Function / Subroutine  Name: ExecuteReader
        '     Purpose					 :  Execute a NpgsqlCommand (that returns a resultset) against the specified NpgsqlTransaction
        '                                   using the provided parameters.

        '     Example                    :  
        '                                  Dim dr As NpgsqlDataReader = ExecuteReader(trans, "GetOrders", new Npgsql.NpgsqlParameter("@prodid", 24))

        '     Parameters                 :
        '                                   transaction - a valid NpgsqlTransaction 
        '                                   commandType - the CommandType (stored procedure, text, etc.)
        '                                   commandText - the stored procedure name or T-SQL command 
        '                                   commandParameters - an array of SqlParamters used to execute the command 
        '     Outputs					 :  returns: A NpgsqlDataReader containing the resultset generated by the command 

        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 16/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteReader(ByVal transaction As NpgsqlTransaction,
                                               ByVal commandType As CommandType,
                                               ByVal commandText As String,
                                               ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter) As NpgsqlDataReader
            If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
            If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
            ' Pass through to private overload, indicating that the connection is owned by the caller
            Return ExecuteReader(transaction.Connection, transaction, commandType, commandText, commandParameters, SqlConnectionOwnership.External)
        End Function ' ExecuteReader
        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  ExecuteReader
        '     Purpose					 :  Execute a stored procedure via a NpgsqlCommand (that returns a resultset) against the specified NpgsqlTransaction 
        '                                   using the provided parameter values.  This method will discover the parameters for the 
        '                                   stored procedure, and assign the values based on parameter order.
        '                                   This method provides no access to output parameters or the stored procedure' s return value parameter.
        '     Example                    :  
        '                                    Dim dr As NpgsqlDataReader = ExecuteReader(trans, "GetOrders", 24, 36)

        '     Parameters                 :
        '                                    transaction - a valid NpgsqlTransaction 
        '                                    spName - the name of the stored procedure 
        '                                    parameterValues - an array of objects to be assigned as the input values of the stored procedure
        '     Outputs					 :  returns: A NpgsqlDataReader containing the resultset generated by the command 

        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 16/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteReader(ByVal transaction As NpgsqlTransaction,
                                               ByVal spName As String,
                                               ByVal ParamArray parameterValues() As Object) As NpgsqlDataReader
            If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
            If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

            Dim commandParameters As Npgsql.NpgsqlParameter()

            ' If we receive parameter values, we need to figure out where they go
            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                commandParameters = SqlDALParameterCache.GetSpParameterSet(transaction.Connection, spName)

                AssignParameterValues(commandParameters, parameterValues)

                Return ExecuteReader(transaction, CommandType.StoredProcedure, spName, commandParameters)
            Else ' Otherwise we can just call the SP without params
                Return ExecuteReader(transaction, CommandType.StoredProcedure, spName)
            End If
        End Function ' ExecuteReader

#End Region

#Region "ExecuteScalar"
        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  ExecuteScalar
        '     Purpose					 :  Execute a NpgsqlCommand (that returns a 1x1 resultset and takes no parameters) against the database specified in 
        '                                   the connection string. 
        '     Example                    :  
        '                                  Dim orderCount As Integer = CInt(ExecuteScalar(connString, "GetOrderCount"))

        '     Parameters                 :
        '                                   connectionString - a valid connection string for a NpgsqlConnection 
        '                                   commandType - the CommandType (stored procedure, text, etc.) 
        '                                   commandText - the stored procedure name or T-SQL command 
        '     OutPut                     : An object containing the value in the 1x1 resultset generated by the command
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 18/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteScalar(ByVal connectionString As String,
                                                       ByVal commandType As CommandType,
                                                       ByVal commandText As String) As Object
            ' Pass through the call providing null for the set of SqlParameters
            Return ExecuteScalar(connectionString, commandType, commandText, CType(Nothing, Npgsql.NpgsqlParameter()))
        End Function ' ExecuteScalar
        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  ExecuteScalar
        '     Purpose					 : Execute a NpgsqlCommand (that returns a 1x1 resultset) against the database specified in the connection string 
        '                                  using the provided parameters.

        '     Example                    :  
        '                                   Dim orderCount As Integer = Cint(ExecuteScalar(connString, "GetOrderCount", new Npgsql.NpgsqlParameter("@prodid", 24)))

        '     Parameters                 :
        '                                   connectionString - a valid connection string for a NpgsqlConnection 
        '                                   commandType - the CommandType (stored procedure, text, etc.) 
        '                                   commandText - the stored procedure name or T-SQL command 
        '                                   commandParameters - an array of SqlParamters used to execute the command 
        '     OutPut                     : An object containing the value in the 1x1 resultset generated by the command
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 18/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteScalar(ByVal connectionString As String,
                                               ByVal commandType As CommandType,
                                               ByVal commandText As String,
                                               ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter) As Object
            If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
            ' Create & open a NpgsqlConnection, and dispose of it after we are done.
            Dim connection As NpgsqlConnection
            Try
                connection = New NpgsqlConnection(connectionString)
                If connection.State <> ConnectionState.Open Then
                    connection.Open()
                End If

                ' Call the overload that takes a connection in place of the connection string
                Return ExecuteScalar(connection, commandType, commandText, commandParameters)
            Catch ex As Exception
                Throw
            Finally
                If Not connection Is Nothing Then
                    connection.Dispose()
                End If
            End Try
        End Function ' ExecuteScalar
        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  ExecuteScalar
        '     Purpose					 : Execute a stored procedure via a NpgsqlCommand (that returns a 1x1 resultset) against the database specified in 
        '                                  the connection string using the provided parameter values.  This method will discover the parameters for the 
        '                                  stored procedure, and assign the values based on parameter order.
        '                                  This method provides no access to output parameters or the stored procedure' s return value parameter.


        '     Example                    :  
        '                                  Dim orderCount As Integer = CInt(ExecuteScalar(connString, "GetOrderCount", 24, 36))
        '     Parameters                 :
        '                                  connectionString - a valid connection string for a NpgsqlConnection 
        '                                  spName - the name of the stored procedure 
        '                                  parameterValues - an array of objects to be assigned as the input values of the stored procedure 
        '     OutPut                     : An object containing the value in the 1x1 resultset generated by the command
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 18/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteScalar(ByVal connectionString As String,
                                                       ByVal spName As String,
                                                       ByVal ParamArray parameterValues() As Object) As Object
            If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

            Dim commandParameters As Npgsql.NpgsqlParameter()

            ' If we receive parameter values, we need to figure out where they go
            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                commandParameters = SqlDALParameterCache.GetSpParameterSet(connectionString, spName)

                ' Assign the provided values to these parameters based on parameter order
                AssignParameterValues(commandParameters, parameterValues)

                ' Call the overload that takes an array of SqlParameters
                Return ExecuteScalar(connectionString, CommandType.StoredProcedure, spName, commandParameters)
                ' Otherwise we can just call the SP without params
            Else
                Return ExecuteScalar(connectionString, CommandType.StoredProcedure, spName)
            End If
        End Function ' ExecuteScalar

        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  ExecuteScalar
        '     Purpose					 : Execute a NpgsqlCommand (that returns a 1x1 resultset and takes no parameters) against the provided NpgsqlConnection. 


        '     Example                    :  
        '                                  Dim orderCount As Integer = CInt(ExecuteScalar(conn, "GetOrderCount"))
        '     Parameters                 :
        '                                  connection - a valid NpgsqlConnection 
        '                                  commandType - the CommandType (stored procedure, text, etc.) 
        '                                  commandText - the stored procedure name or T-SQL command 
        '     OutPut                     : An object containing the value in the 1x1 resultset generated by the command
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 18/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        Public Overloads Shared Function ExecuteScalar(ByVal connection As NpgsqlConnection,
                                               ByVal commandType As CommandType,
                                               ByVal commandText As String) As Object
            ' Pass through the call providing null for the set of SqlParameters
            Return ExecuteScalar(connection, commandType, commandText, CType(Nothing, Npgsql.NpgsqlParameter()))
        End Function ' ExecuteScalar
        '***********************************************************************************************************************************************
        '     Function / Subroutine  Name:  ExecuteScalar
        '     Purpose					 : Execute a NpgsqlCommand (that returns a 1x1 resultset) against the specified NpgsqlConnection 


        '     Example                    :  
        '                                  Dim orderCount As Integer = CInt(ExecuteScalar(conn, "GetOrderCount", new Npgsql.NpgsqlParameter("@prodid", 24)))
        '     Parameters                 :
        '                                 connection - a valid NpgsqlConnection 
        '                                 commandType - the CommandType (stored procedure, text, etc.) 
        '                                 commandText - the stored procedure name or T-SQL command 
        '                                 commandParameters - an array of SqlParamters used to execute the command 
        '     OutPut                     : An object containing the value in the 1x1 resultset generated by the command
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 18/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '************************************************************************************************************************************************
        Public Overloads Shared Function ExecuteScalar(ByVal connection As NpgsqlConnection,
                                                       ByVal commandType As CommandType,
                                                       ByVal commandText As String,
                                                       ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter) As Object

            If (connection Is Nothing) Then Throw New ArgumentNullException("connection")

            ' Create a command and prepare it for execution
            Dim cmd As New NpgsqlCommand
            Dim retval As Object
            Dim mustCloseConnection As Boolean = False

            PrepareCommand(cmd, connection, CType(Nothing, NpgsqlTransaction), commandType, commandText, commandParameters, mustCloseConnection)

            Dim ds As New DataSet

            Try
                Using objTransaction As NpgsqlTransaction = connection.BeginTransaction()
                    ExecuteCommand(connection, cmd, ds, commandParameters)

                    objTransaction.Commit()
                End Using
            Catch ex As Exception When TypeOf ex Is System.InvalidOperationException AndAlso ex.Message = "A transaction is already in progress; nested/concurrent transactions aren't supported."
                ExecuteCommand(connection, cmd, ds, commandParameters)
            Catch ex As Exception
                Dim postgresCommandText As String = "SELECT " & cmd.CommandText & "("

                Dim p As Npgsql.NpgsqlParameter
                For Each p In commandParameters
                    If p.NpgsqlDbType.ToString().ToLower() = "char" AndAlso Not p.Value Is DBNull.Value AndAlso Convert.ToString(p.Value) = vbNullChar Then
                        p.Value = DBNull.Value
                    End If
                    If (Not p Is Nothing) Then
                        ' Check for derived output value with no value assigned
                        If (p.Direction = ParameterDirection.InputOutput OrElse p.Direction = ParameterDirection.Input) AndAlso p.Value Is Nothing Then
                            p.Value = DBNull.Value
                        End If
                        p.NpgsqlValue = p.Value

                        postgresCommandText = postgresCommandText & If(postgresCommandText.EndsWith("("), "", ",") & If(p.NpgsqlDbType.ToString().ToLower() = "text" Or p.NpgsqlDbType.ToString().ToLower() = "nvarchar" Or p.NpgsqlDbType.ToString().ToLower() = "varchar", "'", "") & p.Value & If(p.NpgsqlDbType.ToString().ToLower() = "text" Or p.NpgsqlDbType.ToString().ToLower() = "nvarchar" Or p.NpgsqlDbType.ToString().ToLower() = "varchar", "'", "") & "::" & p.NpgsqlDbType.ToString()
                    End If
                Next p

                postgresCommandText = postgresCommandText & ");"

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                If cmd.CommandText <> "USP_ManageException" Then
                    Try
                        arParms(0) = New Npgsql.NpgsqlParameter("@vcMessage", NpgsqlTypes.NpgsqlDbType.Varchar)
                        arParms(0).Value = postgresCommandText

                        arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                        arParms(1).Value = 0

                        arParms(2) = New Npgsql.NpgsqlParameter("@numUserContactID", NpgsqlTypes.NpgsqlDbType.Bigint)
                        arParms(2).Value = 0

                        arParms(3) = New Npgsql.NpgsqlParameter("@vcURL", NpgsqlTypes.NpgsqlDbType.Varchar, 1000)
                        arParms(3).Value = ""

                        arParms(4) = New Npgsql.NpgsqlParameter("@vcReferrer", NpgsqlTypes.NpgsqlDbType.Varchar, 1000)
                        arParms(4).Value = ""

                        arParms(5) = New Npgsql.NpgsqlParameter("@vcBrowser", NpgsqlTypes.NpgsqlDbType.Varchar, 100)
                        arParms(5).Value = ""

                        arParms(6) = New Npgsql.NpgsqlParameter("@vcBrowserAgent", NpgsqlTypes.NpgsqlDbType.Varchar, 500)
                        arParms(6).Value = ""


                        ExecuteNonQuery(connection, "USP_ManageException", arParms)
                    Catch exInner As Exception
                        'DO NOT RAISE ERROR
                    End Try
                End If

                Throw
            End Try

            ' Detach the SqlParameters from the command object, so they can be used again
            cmd.Parameters.Clear()

            If (mustCloseConnection) Then connection.Close()

            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                retval = ds.Tables(0).Rows(0)(0)
            End If

            Return retval

        End Function ' ExecuteScalar

        '***********************************************************************************************************************************************
        '     Function / Subroutine  Name:  ExecuteScalar
        '     Purpose					 : Execute a stored procedure via a NpgsqlCommand (that returns a 1x1 resultset) against the specified NpgsqlConnection 
        '                                  using the provided parameter values.  This method will discover the parameters for the 
        '                                  stored procedure, and assign the values based on parameter order.
        '                                  This method provides no access to output parameters or the stored procedure' s return value parameter.


        '     Example                    :  
        '                                  Dim orderCount As Integer = CInt(ExecuteScalar(conn, "GetOrderCount", 24, 36))
        '     Parameters                 :
        '                                  connection - a valid NpgsqlConnection 
        '                                  spName - the name of the stored procedure 
        '                                  parameterValues - an array of objects to be assigned as the input values of the stored procedure 
        '     OutPut                     : An object containing the value in the 1x1 resultset generated by the command
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 18/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '************************************************************************************************************************************************
        Public Overloads Shared Function ExecuteScalar(ByVal connection As NpgsqlConnection,
                                               ByVal spName As String,
                                               ByVal ParamArray parameterValues() As Object) As Object
            If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

            Dim commandParameters As Npgsql.NpgsqlParameter()

            ' If we receive parameter values, we need to figure out where they go
            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                commandParameters = SqlDALParameterCache.GetSpParameterSet(connection, spName)

                ' Assign the provided values to these parameters based on parameter order
                AssignParameterValues(commandParameters, parameterValues)

                ' Call the overload that takes an array of SqlParameters
                Return ExecuteScalar(connection, CommandType.StoredProcedure, spName, commandParameters)
            Else ' Otherwise we can just call the SP without params
                Return ExecuteScalar(connection, CommandType.StoredProcedure, spName)
            End If

        End Function ' ExecuteScalar

        ' Execute a NpgsqlCommand (that returns a 1x1 resultset and takes no parameters) against the provided NpgsqlTransaction.
        ' e.g.:  
        ' Dim orderCount As Integer  = CInt(ExecuteScalar(trans, "GetOrderCount"))
        ' Parameters:
        ' -transaction - a valid NpgsqlTransaction 
        ' -commandType - the CommandType (stored procedure, text, etc.) 
        ' -commandText - the stored procedure name or T-SQL command 
        ' Returns: An object containing the value in the 1x1 resultset generated by the command 
        Public Overloads Shared Function ExecuteScalar(ByVal transaction As NpgsqlTransaction,
                                                       ByVal commandType As CommandType,
                                                       ByVal commandText As String) As Object
            ' Pass through the call providing null for the set of SqlParameters
            Return ExecuteScalar(transaction, commandType, commandText, CType(Nothing, Npgsql.NpgsqlParameter()))
        End Function ' ExecuteScalar

        ' Execute a NpgsqlCommand (that returns a 1x1 resultset) against the specified NpgsqlTransaction
        ' using the provided parameters.
        ' e.g.:  
        ' Dim orderCount As Integer = CInt(ExecuteScalar(trans, "GetOrderCount", new Npgsql.NpgsqlParameter("@prodid", 24)))
        ' Parameters:
        ' -transaction - a valid NpgsqlTransaction  
        ' -commandType - the CommandType (stored procedure, text, etc.) 
        ' -commandText - the stored procedure name or T-SQL command 
        ' -commandParameters - an array of SqlParamters used to execute the command 
        ' Returns: An object containing the value in the 1x1 resultset generated by the command 
        Public Overloads Shared Function ExecuteScalar(ByVal transaction As NpgsqlTransaction,
                                                       ByVal commandType As CommandType,
                                                       ByVal commandText As String,
                                                       ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter) As Object
            If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
            If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")

            ' Create a command and prepare it for execution
            Dim cmd As New NpgsqlCommand
            Dim retval As Object
            Dim mustCloseConnection As Boolean = False

            PrepareCommand(cmd, transaction.Connection, transaction, commandType, commandText, commandParameters, mustCloseConnection)

            Dim ds As New DataSet

            Try
                Using objTransaction As NpgsqlTransaction = transaction.Connection.BeginTransaction()
                    ExecuteCommand(transaction.Connection, cmd, ds, commandParameters)

                    objTransaction.Commit()
                End Using
            Catch ex As Exception When TypeOf ex Is System.InvalidOperationException AndAlso ex.Message = "A transaction is already in progress; nested/concurrent transactions aren't supported."
                ExecuteCommand(transaction.Connection, cmd, ds, commandParameters)
            Catch ex As Exception
                Dim postgresCommandText As String = "SELECT " & cmd.CommandText & "("

                Dim p As Npgsql.NpgsqlParameter
                For Each p In commandParameters
                    If p.NpgsqlDbType.ToString().ToLower() = "char" AndAlso Not p.Value Is DBNull.Value AndAlso Convert.ToString(p.Value) = vbNullChar Then
                        p.Value = DBNull.Value
                    End If
                    If (Not p Is Nothing) Then
                        ' Check for derived output value with no value assigned
                        If (p.Direction = ParameterDirection.InputOutput OrElse p.Direction = ParameterDirection.Input) AndAlso p.Value Is Nothing Then
                            p.Value = DBNull.Value
                        End If
                        p.NpgsqlValue = p.Value

                        postgresCommandText = postgresCommandText & If(postgresCommandText.EndsWith("("), "", ",") & If(p.NpgsqlDbType.ToString().ToLower() = "text" Or p.NpgsqlDbType.ToString().ToLower() = "nvarchar" Or p.NpgsqlDbType.ToString().ToLower() = "varchar", "'", "") & p.Value & If(p.NpgsqlDbType.ToString().ToLower() = "text" Or p.NpgsqlDbType.ToString().ToLower() = "nvarchar" Or p.NpgsqlDbType.ToString().ToLower() = "varchar", "'", "") & "::" & p.NpgsqlDbType.ToString()
                    End If
                Next p

                postgresCommandText = postgresCommandText & ");"

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                If cmd.CommandText <> "USP_ManageException" Then
                    Try
                        arParms(0) = New Npgsql.NpgsqlParameter("@vcMessage", NpgsqlTypes.NpgsqlDbType.Varchar)
                        arParms(0).Value = postgresCommandText

                        arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                        arParms(1).Value = 0

                        arParms(2) = New Npgsql.NpgsqlParameter("@numUserContactID", NpgsqlTypes.NpgsqlDbType.Bigint)
                        arParms(2).Value = 0

                        arParms(3) = New Npgsql.NpgsqlParameter("@vcURL", NpgsqlTypes.NpgsqlDbType.Varchar, 1000)
                        arParms(3).Value = ""

                        arParms(4) = New Npgsql.NpgsqlParameter("@vcReferrer", NpgsqlTypes.NpgsqlDbType.Varchar, 1000)
                        arParms(4).Value = ""

                        arParms(5) = New Npgsql.NpgsqlParameter("@vcBrowser", NpgsqlTypes.NpgsqlDbType.Varchar, 100)
                        arParms(5).Value = ""

                        arParms(6) = New Npgsql.NpgsqlParameter("@vcBrowserAgent", NpgsqlTypes.NpgsqlDbType.Varchar, 500)
                        arParms(6).Value = ""


                        ExecuteNonQuery(transaction.Connection, "USP_ManageException", arParms)
                    Catch exInner As Exception
                        'DO NOT RAISE ERROR
                    End Try
                End If

                Throw
            End Try

            ' Detach the SqlParameters from the command object, so they can be used again
            cmd.Parameters.Clear()

            If (mustCloseConnection) Then transaction.Connection.Close()

            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                retval = ds.Tables(0).Rows(0)(0)
            End If

            Return retval
        End Function ' ExecuteScalar
        '***********************************************************************************************************************************************
        '     Function / Subroutine  Name:  ExecuteScalar
        '     Purpose					 :  Execute a stored procedure via a NpgsqlCommand (that returns a 1x1 resultset) against the specified NpgsqlTransaction 
        '                                   using the provided parameter values.  This method will discover the parameters for the 
        '                                   stored procedure, and assign the values based on parameter order.
        '                                   This method provides no access to output parameters or the stored procedure' s return value parameter.
        '

        '     Example                    :  
        '                                    Dim orderCount As Integer = CInt(ExecuteScalar(trans, "GetOrderCount", 24, 36))
        '     Parameters                 :
        '                                    transaction - a valid NpgsqlTransaction 
        '                                    spName - the name of the stored procedure 
        '                                    parameterValues - an array of objects to be assigned as the input values of the stored procedure 
        '     OutPut                     : An object containing the value in the 1x1 resultset generated by the command
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 18/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '************************************************************************************************************************************************
        Public Overloads Shared Function ExecuteScalar(ByVal transaction As NpgsqlTransaction,
                                               ByVal spName As String,
                                               ByVal ParamArray parameterValues() As Object) As Object
            If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
            If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

            Dim commandParameters As Npgsql.NpgsqlParameter()
            ' If we receive parameter values, we need to figure out where they go
            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                commandParameters = SqlDALParameterCache.GetSpParameterSet(transaction.Connection, spName)

                ' Assign the provided values to these parameters based on parameter order
                AssignParameterValues(commandParameters, parameterValues)

                ' Call the overload that takes an array of SqlParameters
                Return ExecuteScalar(transaction, CommandType.StoredProcedure, spName, commandParameters)
            Else ' Otherwise we can just call the SP without params
                Return ExecuteScalar(transaction, CommandType.StoredProcedure, spName)
            End If
        End Function ' ExecuteScalar

#End Region

#Region "FillDataset"
        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  FillDataset
        '     Purpose					 :  Execute a NpgsqlCommand (that returns a resultset and takes no parameters) against the database specified in 
        '                                   the connection string. 
        '
        '     Example                    :  
        '                                    FillDataset (connString, "GetOrders", ds, new String() {"orders"})
        '     Parameters                 :
        '                                    connectionString: A valid connection string for a NpgsqlConnection
        '                                    commandType: the CommandType (stored procedure, text, etc.)
        '                                    commandText: the stored procedure name or T-SQL command
        '                                    dataSet: A dataset wich will contain the resultset generated by the command
        '                                    tableNames: this array will be used to create table mappings allowing the DataTables to be referenced
        '                                     by a user defined name (probably the actual table name)
        '     OutPut                     :  
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 18/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Sub FillDataset(ByVal connectionString As String, ByVal commandType As CommandType, ByVal commandText As String, ByVal dataSet As DataSet, ByVal tableNames() As String)

            If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
            If (dataSet Is Nothing) Then Throw New ArgumentNullException("dataSet")

            ' Create & open a NpgsqlConnection, and dispose of it after we are done
            Dim connection As NpgsqlConnection
            Try
                connection = New NpgsqlConnection(connectionString)
                If connection.State <> ConnectionState.Open Then
                    connection.Open()
                End If

                ' Call the overload that takes a connection in place of the connection string
                FillDataset(connection, commandType, commandText, dataSet, tableNames)
            Catch ex As Exception
                Throw
            Finally
                If Not connection Is Nothing Then
                    connection.Dispose()
                End If
            End Try
        End Sub
        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  FillDataset
        '     Purpose					 :  Execute a NpgsqlCommand (that returns a resultset) against the database specified in the connection string 
        '                                   using the provided parameters.
        '     Example                    :  
        '                                    FillDataset (connString, "GetOrders", ds, new String() = {"orders"}, new Npgsql.NpgsqlParameter("@prodid", 24))
        '     Parameters                 :
        '                                   connectionString: A valid connection string for a NpgsqlConnection
        '                                   commandType: the CommandType (stored procedure, text, etc.)
        '                                   commandText: the stored procedure name or T-SQL command
        '                                   dataSet: A dataset wich will contain the resultset generated by the command
        '                                   tableNames: this array will be used to create table mappings allowing the DataTables to be referenced
        '                                   by a user defined name (probably the actual table name)
        '                                   commandParameters: An array of SqlParamters used to execute the command 
        '     OutPut                     :  
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 18/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Sub FillDataset(ByVal connectionString As String, ByVal commandType As CommandType, ByVal commandText As String, ByVal dataSet As DataSet,
    ByVal tableNames() As String, ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter)

            If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
            If (dataSet Is Nothing) Then Throw New ArgumentNullException("dataSet")

            ' Create & open a NpgsqlConnection, and dispose of it after we are done
            Dim connection As NpgsqlConnection
            Try
                connection = New NpgsqlConnection(connectionString)
                If connection.State <> ConnectionState.Open Then
                    connection.Open()
                End If

                ' Call the overload that takes a connection in place of the connection string
                FillDataset(connection, commandType, commandText, dataSet, tableNames, commandParameters)
            Catch ex As Exception
                Throw
            Finally
                If Not connection Is Nothing Then
                    connection.Dispose()
                End If
            End Try
        End Sub
        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  FillDataset
        '     Purpose					 : Execute a stored procedure via a NpgsqlCommand (that returns a resultset) against the database specified in 
        '                                  the connection string using the provided parameter values.  This method will query the database to discover the parameters for the 
        '                                  stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.
        '                                  This method provides no access to output parameters or the stored procedure' s return value parameter.
        ' 
        '     Example                    :  
        '                                   FillDataset (connString, "GetOrders", ds, new String() {"orders"}, 24)
        '     Parameters                 :
        '                                  -connectionString: A valid connection string for a NpgsqlConnection
        '                                  -spName: the name of the stored procedure
        '                                  -dataSet: A dataset wich will contain the resultset generated by the command
        '                                  -tableNames: this array will be used to create table mappings allowing the DataTables to be referenced
        '                                   by a user defined name (probably the actual table name)
        '                                  -parameterValues: An array of objects to be assigned As the input values of the stored procedure
        '     OutPut                     :  
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 18/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Sub FillDataset(ByVal connectionString As String, ByVal spName As String,
    ByVal dataSet As DataSet, ByVal tableNames As String(), ByVal ParamArray parameterValues() As Object)

            If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
            If (dataSet Is Nothing) Then Throw New ArgumentNullException("dataSet")

            ' Create & open a NpgsqlConnection, and dispose of it after we are done
            Dim connection As NpgsqlConnection
            Try
                connection = New NpgsqlConnection(connectionString)
                If connection.State <> ConnectionState.Open Then
                    connection.Open()
                End If
                ' Call the overload that takes a connection in place of the connection string
                FillDataset(connection, spName, dataSet, tableNames, parameterValues)
            Catch ex As Exception
                Throw
            Finally
                If Not connection Is Nothing Then
                    connection.Dispose()
                End If
            End Try
        End Sub
        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  FillDataset
        '     Purpose					 :  Execute a NpgsqlCommand (that returns a resultset and takes no parameters) against the provided NpgsqlConnection. 

        '     Example                    :  
        '                                    FillDataset (conn, "GetOrders", ds, new String() {"orders"})
        '     Parameters                 :
        '                                  -connection: A valid NpgsqlConnection
        '                                  -commandType: the CommandType (stored procedure, text, etc.)
        '                                  -commandText: the stored procedure name or T-SQL command
        '                                  -dataSet: A dataset wich will contain the resultset generated by the command
        '                                  -tableNames: this array will be used to create table mappings allowing the DataTables to be referenced
        '                                  by a user defined name (probably the actual table name)
        '                                  -commandParameters: An array of SqlParamters used to execute the command
        '     OutPut                     :  
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 18/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Sub FillDataset(ByVal connection As NpgsqlConnection, ByVal commandType As CommandType,
            ByVal commandText As String, ByVal dataSet As DataSet, ByVal tableNames As String())

            FillDataset(connection, commandType, commandText, dataSet, tableNames, Nothing)

        End Sub
        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  FillDataset
        '     Purpose					 :  Execute a NpgsqlCommand (that returns a resultset) against the specified NpgsqlConnection 
        '                                   using the provided parameters.

        '     Example                    :  
        '                                   FillDataset (conn, "GetOrders", ds, new String() {"orders"}, new Npgsql.NpgsqlParameter("@prodid", 24))
        '     Parameters                 :
        '                                   -connection: A valid NpgsqlConnection
        '                                   -commandType: the CommandType (stored procedure, text, etc.)
        '                                   -commandText: the stored procedure name or T-SQL command
        '                                   -dataSet: A dataset wich will contain the resultset generated by the command
        '                                   -tableNames: this array will be used to create table mappings allowing the DataTables to be referenced
        '                                    by a user defined name (probably the actual table name)
        '                                   -commandParameters: An array of SqlParamters used to execute the command
        '     OutPut                     :  
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 18/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Sub FillDataset(ByVal connection As NpgsqlConnection, ByVal commandType As CommandType,
        ByVal commandText As String, ByVal dataSet As DataSet, ByVal tableNames As String(),
            ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter)

            FillDataset(connection, Nothing, commandType, commandText, dataSet, tableNames, commandParameters)

        End Sub
        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  FillDataset
        '     Purpose					 :  Execute a stored procedure via a NpgsqlCommand (that returns a resultset) against the specified NpgsqlConnection 
        '                                   using the provided parameter values.  This method will query the database to discover the parameters for the 
        '                                   stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.
        '                                   This method provides no access to output parameters or the stored procedure' s return value parameter.
        ' 
        '     Example                    :  
        '                                   FillDataset (conn, "GetOrders", ds, new string() {"orders"}, 24, 36)
        '     Parameters                 :
        '                                  -connection: A valid NpgsqlConnection
        '                                  -spName: the name of the stored procedure
        '                                  -dataSet: A dataset wich will contain the resultset generated by the command
        '                                  -tableNames: this array will be used to create table mappings allowing the DataTables to be referenced
        '                                    by a user defined name (probably the actual table name)
        '                                  -parameterValues: An array of objects to be assigned as the input values of the stored procedure
        '     OutPut                     :  
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 18/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Sub FillDataset(ByVal connection As NpgsqlConnection, ByVal spName As String, ByVal dataSet As DataSet,
    ByVal tableNames() As String, ByVal ParamArray parameterValues() As Object)

            If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
            If (dataSet Is Nothing) Then Throw New ArgumentNullException("dataSet")
            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

            ' If we receive parameter values, we need to figure out where they go
            If Not parameterValues Is Nothing AndAlso parameterValues.Length > 0 Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                Dim commandParameters() As Npgsql.NpgsqlParameter = SqlDALParameterCache.GetSpParameterSet(connection, spName)

                ' Assign the provided values to these parameters based on parameter order
                AssignParameterValues(commandParameters, parameterValues)

                ' Call the overload that takes an array of SqlParameters
                FillDataset(connection, CommandType.StoredProcedure, spName, dataSet, tableNames, commandParameters)
            Else ' Otherwise we can just call the SP without params
                FillDataset(connection, CommandType.StoredProcedure, spName, dataSet, tableNames)
            End If

        End Sub
        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  FillDataset
        '     Purpose					 :  Execute a NpgsqlCommand (that returns a resultset and takes no parameters) against the provided NpgsqlTransaction. 

        '     Example                    :  
        '                                   FillDataset (trans, "GetOrders", ds, new string() {"orders"})
        '     Parameters                 :
        '                                   -transaction: A valid NpgsqlTransaction
        '                                   -commandType: the CommandType (stored procedure, text, etc.)
        '                                   -commandText: the stored procedure name or T-SQL command
        '                                   -dataSet: A dataset wich will contain the resultset generated by the command
        '                                   -tableNames: this array will be used to create table mappings allowing the DataTables to be referenced
        '                                    by a user defined name (probably the actual table name)
        '     OutPut                     :  
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 18/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Sub FillDataset(ByVal transaction As NpgsqlTransaction, ByVal commandType As CommandType,
    ByVal commandText As String, ByVal dataSet As DataSet, ByVal tableNames() As String)

            FillDataset(transaction, commandType, commandText, dataSet, tableNames, Nothing)
        End Sub
        '***************************************************************************************************************************
        '     Function / Subroutine  Name: FillDataset
        '     Purpose					 :  Execute a NpgsqlCommand (that returns a resultset) against the specified NpgsqlTransaction
        '                                   using the provided parameters.
        '
        '     Example                    :  
        '                                   FillDataset(trans, "GetOrders", ds, new string() {"orders"}, new Npgsql.NpgsqlParameter("@prodid", 24))
        '     Parameters                 :
        '                                  transaction: A valid NpgsqlTransaction
        '                                  commandType: the CommandType (stored procedure, text, etc.)
        '                                  commandText: the stored procedure name or T-SQL command
        '                                  dataSet: A dataset wich will contain the resultset generated by the command
        '                                  tableNames: this array will be used to create table mappings allowing the DataTables to be referenced
        '                                  by a user defined name (probably the actual table name)
        '                                  commandParameters: An array of SqlParamters used to execute the command
        '     OutPut                     :  
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 18/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Sub FillDataset(ByVal transaction As NpgsqlTransaction, ByVal commandType As CommandType,
    ByVal commandText As String, ByVal dataSet As DataSet, ByVal tableNames() As String,
    ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter)

            If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
            If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
            FillDataset(transaction.Connection, transaction, commandType, commandText, dataSet, tableNames, commandParameters)

        End Sub
        '***************************************************************************************************************************
        '     Function / Subroutine  Name: FillDataset
        '     Purpose					 : Execute a stored procedure via a NpgsqlCommand (that returns a resultset) against the specified 
        '                                  NpgsqlTransaction using the provided parameter values.  This method will query the database to discover the parameters for the 
        '                                  stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.
        '                                  This method provides no access to output parameters or the stored procedure' s return value parameter.

        '     Example                    :  
        '                                  FillDataset(trans, "GetOrders", ds, new String(){"orders"}, 24, 36) 
        '     Parameters                 :
        '                                  transaction: A valid NpgsqlTransaction
        '                                  spName: the name of the stored procedure
        '                                  dataSet: A dataset wich will contain the resultset generated by the command
        '                                  tableNames: this array will be used to create table mappings allowing the DataTables to be referenced
        '                                    by a user defined name (probably the actual table name)
        '                                  parameterValues: An array of objects to be assigned as the input values of the stored procedure
        '     OutPut                     :  
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 18/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Sub FillDataset(ByVal transaction As NpgsqlTransaction, ByVal spName As String,
            ByVal dataSet As DataSet, ByVal tableNames() As String, ByVal ParamArray parameterValues() As Object)

            If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
            If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
            If (dataSet Is Nothing) Then Throw New ArgumentNullException("dataSet")
            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

            ' If we receive parameter values, we need to figure out where they go
            If Not parameterValues Is Nothing AndAlso parameterValues.Length > 0 Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                Dim commandParameters() As Npgsql.NpgsqlParameter = SqlDALParameterCache.GetSpParameterSet(transaction.Connection, spName)

                ' Assign the provided values to these parameters based on parameter order
                AssignParameterValues(commandParameters, parameterValues)

                ' Call the overload that takes an array of SqlParameters
                FillDataset(transaction, CommandType.StoredProcedure, spName, dataSet, tableNames, commandParameters)
            Else ' Otherwise we can just call the SP without params
                FillDataset(transaction, CommandType.StoredProcedure, spName, dataSet, tableNames)
            End If
        End Sub
        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  FillDataset
        '     Purpose					 : Private helper method that execute a NpgsqlCommand (that returns a resultset) against the specified NpgsqlTransaction and NpgsqlConnection
        '                                  using the provided parameters.

        '     Example                    :  
        '                                   FillDataset(conn, trans, "GetOrders", ds, new String() {"orders"}, new Npgsql.NpgsqlParameter("@prodid", 24))
        '     Parameters                 :
        '                                  -connection: A valid NpgsqlConnection
        '                                  -transaction: A valid NpgsqlTransaction
        '                                  -commandType: the CommandType (stored procedure, text, etc.)
        '                                  -commandText: the stored procedure name or T-SQL command
        '                                  -dataSet: A dataset wich will contain the resultset generated by the command
        '                                  -tableNames: this array will be used to create table mappings allowing the DataTables to be referenced
        '                                  by a user defined name (probably the actual table name)
        '                                  -commandParameters: An array of SqlParamters used to execute the command
        '     OutPut                     :  
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 18/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Private Overloads Shared Sub FillDataset(ByVal connection As NpgsqlConnection, ByVal transaction As NpgsqlTransaction, ByVal commandType As CommandType,
            ByVal commandText As String, ByVal dataSet As DataSet, ByVal tableNames() As String,
            ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter)

            If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
            If (dataSet Is Nothing) Then Throw New ArgumentNullException("dataSet")

            ' Create a command and prepare it for execution
            Dim command As New NpgsqlCommand

            Dim mustCloseConnection As Boolean = False
            PrepareCommand(command, connection, transaction, commandType, commandText, commandParameters, mustCloseConnection)

            ' Create the DataAdapter & DataSet
            Dim dataAdapter As NpgsqlDataAdapter = New NpgsqlDataAdapter(command)

            Try
                ' Add the table mappings specified by the user
                If Not tableNames Is Nothing AndAlso tableNames.Length > 0 Then

                    Dim tableName As String = "Table"
                    Dim index As Integer

                    For index = 0 To tableNames.Length - 1
                        If (tableNames(index) Is Nothing OrElse tableNames(index).Length = 0) Then Throw New ArgumentException("The tableNames parameter must contain a list of tables, a value was provided as null or empty string.", "tableNames")
                        dataAdapter.TableMappings.Add(tableName, tableNames(index))
                        tableName = tableName & (index + 1).ToString()
                    Next
                End If

                ' Fill the DataSet using default values for DataTable names, etc
                dataAdapter.Fill(dataSet)

                ' Detach the SqlParameters from the command object, so they can be used again
                command.Parameters.Clear()
            Catch ex As Exception
                Throw
            Finally
                If (Not dataAdapter Is Nothing) Then dataAdapter.Dispose()
            End Try

            If (mustCloseConnection) Then connection.Close()

        End Sub
#End Region

#Region "UpdateDataset"
        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  UpdateDataset
        '     Purpose					 :  Executes the respective command for each inserted, updated, or deleted row in the DataSet.
        '     Example                    :  
        '                                    UpdateDataset(conn, insertCommand, deleteCommand, updateCommand, dataSet, "Order")
        '     Parameters                 :
        '                                  -insertCommand: A valid transact-SQL statement or stored procedure to insert new records into the data source
        '                                  -deleteCommand: A valid transact-SQL statement or stored procedure to delete records from the data source
        '                                  -updateCommand: A valid transact-SQL statement or stored procedure used to update records in the data source
        '                                  -dataSet: the DataSet used to update the data source
        '                                  -tableName: the DataTable used to update the data source
        '     OutPut                     :  
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 18/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Sub UpdateDataset(ByVal insertCommand As NpgsqlCommand, ByVal deleteCommand As NpgsqlCommand, ByVal updateCommand As NpgsqlCommand, ByVal dataSet As DataSet, ByVal tableName As String)

            If (insertCommand Is Nothing) Then Throw New ArgumentNullException("insertCommand")
            If (deleteCommand Is Nothing) Then Throw New ArgumentNullException("deleteCommand")
            If (updateCommand Is Nothing) Then Throw New ArgumentNullException("updateCommand")
            If (dataSet Is Nothing) Then Throw New ArgumentNullException("dataSet")
            If (tableName Is Nothing OrElse tableName.Length = 0) Then Throw New ArgumentNullException("tableName")

            ' Create a NpgsqlDataAdapter, and dispose of it after we are done
            Dim dataAdapter As New NpgsqlDataAdapter
            Try
                ' Set the data adapter commands
                dataAdapter.UpdateCommand = updateCommand
                dataAdapter.InsertCommand = insertCommand
                dataAdapter.DeleteCommand = deleteCommand

                ' Update the dataset changes in the data source
                dataAdapter.Update(dataSet, tableName)

                ' Commit all the changes made to the DataSet
                dataSet.AcceptChanges()
            Catch ex As Exception
                Throw
            Finally
                If (Not dataAdapter Is Nothing) Then dataAdapter.Dispose()
            End Try
        End Sub
#End Region

#Region "CreateCommand"
        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  CreateCommand
        '     Purpose					 :  Simplify the creation of a Sql command object by allowing
        '                                   a stored procedure and optional parameters to be provided
        '     Example                    :  
        '                                   Dim command As NpgsqlCommand = CreateCommand(conn, "AddCustomer", "CustomerID", "CustomerName")
        '     Parameters                 :
        '                                   -connection: A valid NpgsqlConnection object
        '                                   -spName: the name of the stored procedure
        '                                   -sourceColumns: An array of string to be assigned as the source columns of the stored procedure parameters
        '     OutPut                     :  a valid NpgsqlCommand object
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 18/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function CreateCommand(ByVal connection As NpgsqlConnection, ByVal spName As String, ByVal ParamArray sourceColumns() As String) As NpgsqlCommand

            If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
            ' Create a NpgsqlCommand
            Dim cmd As New NpgsqlCommand(spName, connection)
            cmd.CommandType = CommandType.StoredProcedure

            ' If we receive parameter values, we need to figure out where they go
            If Not sourceColumns Is Nothing AndAlso sourceColumns.Length > 0 Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                Dim commandParameters() As Npgsql.NpgsqlParameter = SqlDALParameterCache.GetSpParameterSet(connection, spName)

                ' Assign the provided source columns to these parameters based on parameter order
                Dim index As Integer
                For index = 0 To sourceColumns.Length - 1
                    commandParameters(index).SourceColumn = sourceColumns(index)
                Next

                ' Attach the discovered parameters to the NpgsqlCommand object
                AttachParameters(cmd, commandParameters)
            End If

            CreateCommand = cmd
        End Function
#End Region

#Region "ExecuteNonQueryTypedParams"
        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  ExecuteNonQueryTypedParams
        '     Purpose					 : Execute a stored procedure via a NpgsqlCommand (that returns no resultset) against the database specified in 
        '                                  the connection string using the dataRow column values as the stored procedure' s parameters values.
        '                                  This method will query the database to discover the parameters for the 
        '                                  stored procedure (the first time each stored procedure is called), and assign the values based on row values.
        '     Example                    :  

        '     Parameters                 :
        '                                  -connectionString: A valid connection string for a NpgsqlConnection
        '                                  -spName: the name of the stored procedure
        '                                  -dataRow: The dataRow used to hold the stored procedure' s parameter values
        '     OutPut                     : an int representing the number of rows affected by the command
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 18/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteNonQueryTypedParams(ByVal connectionString As String, ByVal spName As String, ByVal dataRow As DataRow) As Integer
            If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

            ' If the row has values, the store procedure parameters must be initialized
            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                Dim commandParameters() As Npgsql.NpgsqlParameter = SqlDALParameterCache.GetSpParameterSet(connectionString, spName)

                ' Set the parameters values
                AssignParameterValues(commandParameters, dataRow)

                ExecuteNonQueryTypedParams = SqlDAL.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, spName, commandParameters)
            Else
                ExecuteNonQueryTypedParams = SqlDAL.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, spName)
            End If
        End Function
        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  ExecuteNonQueryTypedParams
        '     Purpose					 :  Execute a stored procedure via a NpgsqlCommand (that returns no resultset) against the specified NpgsqlConnection 
        '                                   using the dataRow column values as the stored procedure' s parameters values.  
        '                                   This method will query the database to discover the parameters for the 
        '                                   stored procedure (the first time each stored procedure is called), and assign the values based on row values.
        '     Example                    :  
        '                                  
        '     Parameters                 :
        '                                   -connection:a valid NpgsqlConnection object
        '                                   -spName: the name of the stored procedure
        '                                   -dataRow:The dataRow used to hold the stored procedure' s parameter values.
        '     OutPut                     : an int representing the number of rows affected by the command
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 18/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteNonQueryTypedParams(ByVal connection As NpgsqlConnection, ByVal spName As String, ByVal dataRow As DataRow) As Integer
            If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

            ' If the row has values, the store procedure parameters must be initialized
            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                Dim commandParameters() As Npgsql.NpgsqlParameter = SqlDALParameterCache.GetSpParameterSet(connection, spName)

                ' Set the parameters values
                AssignParameterValues(commandParameters, dataRow)

                ExecuteNonQueryTypedParams = SqlDAL.ExecuteNonQuery(connection, CommandType.StoredProcedure, spName, commandParameters)
            Else
                ExecuteNonQueryTypedParams = SqlDAL.ExecuteNonQuery(connection, CommandType.StoredProcedure, spName)
            End If
        End Function
        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  ExecuteNonQueryTypedParams
        '     Purpose					 : Execute a stored procedure via a NpgsqlCommand (that returns no resultset) against the specified
        '                                  NpgsqlTransaction using the dataRow column values as the stored procedure' s parameters values.
        '                                  This method will query the database to discover the parameters for the 
        '                                  stored procedure (the first time each stored procedure is called), and assign the values based on row values.

        '     Example                    :  
        '                                  
        '     Parameters                 :
        '                                  transaction:a valid NpgsqlTransaction object
        '                                  spName:the name of the stored procedure
        '                                  dataRow:The dataRow used to hold the stored procedure' s parameter values.
        '     OutPut                     : an int representing the number of rows affected by the command
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 18/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteNonQueryTypedParams(ByVal transaction As NpgsqlTransaction, ByVal spName As String, ByVal dataRow As DataRow) As Integer

            If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
            If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

            ' If the row has values, the store procedure parameters must be initialized
            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                Dim commandParameters() As Npgsql.NpgsqlParameter = SqlDALParameterCache.GetSpParameterSet(transaction.Connection, spName)

                ' Set the parameters values
                AssignParameterValues(commandParameters, dataRow)

                ExecuteNonQueryTypedParams = SqlDAL.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spName, commandParameters)
            Else

                ExecuteNonQueryTypedParams = SqlDAL.ExecuteNonQuery(transaction, CommandType.StoredProcedure, spName)
            End If
        End Function
#End Region

#Region "ExecuteDatasetTypedParams"
        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  ExecuteDatasetTypedParams
        '     Purpose					 :  Execute a stored procedure via a NpgsqlCommand (that returns a resultset) against the database specified in 
        '                                   the connection string using the dataRow column values as the stored procedure' s parameters values.
        '                                   This method will query the database to discover the parameters for the 
        '                                   stored procedure (the first time each stored procedure is called), and assign the values based on row values.

        '     Example                    :  
        '                                  
        '     Parameters                 :
        '                                  -connectionString: A valid connection string for a NpgsqlConnection
        '                                  -spName: the name of the stored procedure
        '                                  -dataRow: The dataRow used to hold the stored procedure' s parameter values.
        '     OutPut                     : a dataset containing the resultset generated by the command
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 18/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteDatasetTypedParams(ByVal connectionString As String, ByVal spName As String, ByVal dataRow As DataRow) As DataSet
            If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

            ' If the row has values, the store procedure parameters must be initialized
            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                Dim commandParameters() As Npgsql.NpgsqlParameter = SqlDALParameterCache.GetSpParameterSet(connectionString, spName)

                ' Set the parameters values
                AssignParameterValues(commandParameters, dataRow)

                ExecuteDatasetTypedParams = SqlDAL.ExecuteDataset(connectionString, CommandType.StoredProcedure, spName, commandParameters)
            Else

                ExecuteDatasetTypedParams = SqlDAL.ExecuteDataset(connectionString, CommandType.StoredProcedure, spName)
            End If
        End Function
        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  ExecuteDatasetTypedParams
        '     Purpose					 :  Execute a stored procedure via a NpgsqlCommand (that returns a resultset) against the specified NpgsqlConnection 
        '                                   using the dataRow column values as the store procedure' s parameters values.
        '                                   This method will query the database to discover the parameters for the 
        '                                   stored procedure (the first time each stored procedure is called), and assign the values based on row values.
        ' 
        '     Example                    :  
        '                                  
        '     Parameters                 :
        '                                    connection: A valid NpgsqlConnection object
        '                                    spName: the name of the stored procedure
        '                                    dataRow: The dataRow used to hold the stored procedure' s parameter values.
        '     OutPut                     : a dataset containing the resultset generated by the command
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 18/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteDatasetTypedParams(ByVal connection As NpgsqlConnection, ByVal spName As String, ByVal dataRow As DataRow) As DataSet

            If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

            ' If the row has values, the store procedure parameters must be initialized
            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                Dim commandParameters() As Npgsql.NpgsqlParameter = SqlDALParameterCache.GetSpParameterSet(connection, spName)

                ' Set the parameters values
                AssignParameterValues(commandParameters, dataRow)

                ExecuteDatasetTypedParams = SqlDAL.ExecuteDataset(connection, CommandType.StoredProcedure, spName, commandParameters)
            Else

                ExecuteDatasetTypedParams = SqlDAL.ExecuteDataset(connection, CommandType.StoredProcedure, spName)
            End If
        End Function
        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  ExecuteDatasetTypedParams
        '     Purpose					 :  Execute a stored procedure via a NpgsqlCommand (that returns a resultset) against the specified NpgsqlTransaction 
        '                                   using the dataRow column values as the stored procedure' s parameters values.
        '                                   This method will query the database to discover the parameters for the 
        '                                   stored procedure (the first time each stored procedure is called), and assign the values based on row values.

        '     Example                    :  
        '                                  
        '     Parameters                 :
        '                                   -transaction: A valid NpgsqlTransaction object
        '                                   -spName: the name of the stored procedure
        '                                   -dataRow: The dataRow used to hold the stored procedure' s parameter values.
        '     OutPut                     : a dataset containing the resultset generated by the command
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 18/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteDatasetTypedParams(ByVal transaction As NpgsqlTransaction, ByVal spName As String, ByVal dataRow As DataRow) As DataSet
            If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
            If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

            ' If the row has values, the store procedure parameters must be initialized
            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                Dim commandParameters() As Npgsql.NpgsqlParameter = SqlDALParameterCache.GetSpParameterSet(transaction.Connection, spName)

                ' Set the parameters values
                AssignParameterValues(commandParameters, dataRow)

                ExecuteDatasetTypedParams = SqlDAL.ExecuteDataset(transaction, CommandType.StoredProcedure, spName, commandParameters)
            Else

                ExecuteDatasetTypedParams = SqlDAL.ExecuteDataset(transaction, CommandType.StoredProcedure, spName)
            End If
        End Function
#End Region

#Region "ExecuteReaderTypedParams"
        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  ExecuteReaderTypedParams
        '     Purpose					 :  Execute a stored procedure via a NpgsqlCommand (that returns a resultset) against the database specified in 
        '                                   the connection string using the dataRow column values as the stored procedure' s parameters values.
        '                                   This method will query the database to discover the parameters for the 
        '                                   stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.

        '     Example                    :  
        '                                  
        '     Parameters                 :
        '                                 -connectionString: A valid connection string for a NpgsqlConnection
        '                                 -spName: the name of the stored procedure
        '                                 -dataRow: The dataRow used to hold the stored procedure' s parameter values.
        '     OutPut                     :  a NpgsqlDataReader containing the resultset generated by the command
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 18/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteReaderTypedParams(ByVal connectionString As String, ByVal spName As String, ByVal dataRow As DataRow) As NpgsqlDataReader
            If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

            ' If the row has values, the store procedure parameters must be initialized
            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                Dim commandParameters() As Npgsql.NpgsqlParameter = SqlDALParameterCache.GetSpParameterSet(connectionString, spName)

                ' Set the parameters values
                AssignParameterValues(commandParameters, dataRow)

                ExecuteReaderTypedParams = SqlDAL.ExecuteReader(connectionString, CommandType.StoredProcedure, spName, commandParameters)
            Else
                ExecuteReaderTypedParams = SqlDAL.ExecuteReader(connectionString, CommandType.StoredProcedure, spName)
            End If
        End Function
        '***************************************************************************************************************************
        '     Function / Subroutine  Name: ExecuteReaderTypedParams
        '     Purpose					 : Execute a stored procedure via a NpgsqlCommand (that returns a resultset) against the specified NpgsqlConnection 
        '                                  using the dataRow column values as the stored procedure' s parameters values.
        '                                  This method will query the database to discover the parameters for the 
        '                                  stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.

        '     Example                    :  
        '                                  
        '     Parameters                 :
        '                                  -connection: A valid NpgsqlConnection object
        '                                  -spName: The name of the stored procedure
        '                                  -dataRow: The dataRow used to hold the stored procedure' s parameter values.
        '     OutPut                     :  a NpgsqlDataReader containing the resultset generated by the command
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 18/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteReaderTypedParams(ByVal connection As NpgsqlConnection, ByVal spName As String, ByVal dataRow As DataRow) As NpgsqlDataReader
            If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

            ' If the row has values, the store procedure parameters must be initialized
            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                Dim commandParameters() As Npgsql.NpgsqlParameter = SqlDALParameterCache.GetSpParameterSet(connection, spName)

                ' Set the parameters values
                AssignParameterValues(commandParameters, dataRow)

                ExecuteReaderTypedParams = SqlDAL.ExecuteReader(connection, CommandType.StoredProcedure, spName, commandParameters)
            Else
                ExecuteReaderTypedParams = SqlDAL.ExecuteReader(connection, CommandType.StoredProcedure, spName)
            End If
        End Function
        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  ExecuteReaderTypedParams
        '     Purpose					 :  Execute a stored procedure via a NpgsqlCommand (that returns a resultset) against the specified NpgsqlTransaction 
        '                                   using the dataRow column values as the stored procedure' s parameters values.
        '                                   This method will query the database to discover the parameters for the 
        '                                   stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.

        '     Example                    :  
        '                                  
        '     Parameters                 :
        '                                   -transaction: A valid NpgsqlTransaction object
        '                                   -spName" The name of the stored procedure
        '                                   -dataRow: The dataRow used to hold the stored procedure' s parameter values.
        '     OutPut                     :  a NpgsqlDataReader containing the resultset generated by the command
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 18/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteReaderTypedParams(ByVal transaction As NpgsqlTransaction, ByVal spName As String, ByVal dataRow As DataRow) As NpgsqlDataReader
            If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
            If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

            ' If the row has values, the store procedure parameters must be initialized
            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                Dim commandParameters() As Npgsql.NpgsqlParameter = SqlDALParameterCache.GetSpParameterSet(transaction.Connection, spName)

                ' Set the parameters values
                AssignParameterValues(commandParameters, dataRow)

                ExecuteReaderTypedParams = SqlDAL.ExecuteReader(transaction, CommandType.StoredProcedure, spName, commandParameters)
            Else
                ExecuteReaderTypedParams = SqlDAL.ExecuteReader(transaction, CommandType.StoredProcedure, spName)
            End If
        End Function
#End Region

#Region "ExecuteScalarTypedParams"
        '***************************************************************************************************************************
        '     Function / Subroutine  Name: ExecuteScalarTypedParams
        '     Purpose					 : Execute a stored procedure via a NpgsqlCommand (that returns a 1x1 resultset) against the database specified in 
        '                                  the connection string using the dataRow column values as the stored procedure' s parameters values.
        '                                  This method will query the database to discover the parameters for the 
        '                                  stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.

        '     Example                    :  
        '                                  
        '     Parameters                 :
        '                                  connectionString: A valid connection string for a NpgsqlConnection
        '                                  spName: The name of the stored procedure
        '                                  dataRow: The dataRow used to hold the stored procedure' s parameter values.
        '     OutPut                     : An object containing the value in the 1x1 resultset generated by the command
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 18/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteScalarTypedParams(ByVal connectionString As String, ByVal spName As String, ByVal dataRow As DataRow) As Object
            If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
            ' If the row has values, the store procedure parameters must be initialized
            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                Dim commandParameters() As Npgsql.NpgsqlParameter = SqlDALParameterCache.GetSpParameterSet(connectionString, spName)

                ' Set the parameters values
                AssignParameterValues(commandParameters, dataRow)

                ExecuteScalarTypedParams = SqlDAL.ExecuteScalar(connectionString, CommandType.StoredProcedure, spName, commandParameters)
            Else
                ExecuteScalarTypedParams = SqlDAL.ExecuteScalar(connectionString, CommandType.StoredProcedure, spName)
            End If
        End Function
        '***************************************************************************************************************************
        '     Function / Subroutine  Name: ExecuteScalarTypedParams
        '     Purpose					 : Execute a stored procedure via a NpgsqlCommand (that returns a 1x1 resultset) against the specified NpgsqlConnection 
        '                                  using the dataRow column values as the stored procedure' s parameters values.
        '                                  This method will query the database to discover the parameters for the 
        '                                  stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.

        '     Example                    :  
        '                                  
        '     Parameters                 :
        '                                 -connection: A valid NpgsqlConnection object
        '                                 -spName: the name of the stored procedure
        '                                 -dataRow: The dataRow used to hold the stored procedure' s parameter values.
        '     OutPut                     : An object containing the value in the 1x1 resultset generated by the command
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 18/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteScalarTypedParams(ByVal connection As NpgsqlConnection, ByVal spName As String, ByVal dataRow As DataRow) As Object
            If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

            ' If the row has values, the store procedure parameters must be initialized
            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                Dim commandParameters() As Npgsql.NpgsqlParameter = SqlDALParameterCache.GetSpParameterSet(connection, spName)

                ' Set the parameters values
                AssignParameterValues(commandParameters, dataRow)

                ExecuteScalarTypedParams = SqlDAL.ExecuteScalar(connection, CommandType.StoredProcedure, spName, commandParameters)
            Else
                ExecuteScalarTypedParams = SqlDAL.ExecuteScalar(connection, CommandType.StoredProcedure, spName)
            End If
        End Function
        '***************************************************************************************************************************
        '     Function / Subroutine  Name:ExecuteScalarTypedParams
        '     Purpose					 :Execute a stored procedure via a NpgsqlCommand (that returns a 1x1 resultset) against the specified NpgsqlTransaction
        '                                 using the dataRow column values as the stored procedure' s parameters values.
        '                                 This method will query the database to discover the parameters for the 
        '                                 stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.

        '     Example                    :  
        '                                  
        '     Parameters                 :
        '                                  transaction: A valid NpgsqlTransaction object
        '                                  spName: the name of the stored procedure
        '                                  dataRow: The dataRow used to hold the stored procedure' s parameter values.
        '     OutPut                     : An object containing the value in the 1x1 resultset generated by the command
        '     Author Name				 : Ajeet Singh
        '     Date Written				 : 18/10/2004
        '     Cross References 			 : List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Overloads Shared Function ExecuteScalarTypedParams(ByVal transaction As NpgsqlTransaction, ByVal spName As String, ByVal dataRow As DataRow) As Object
            If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
            If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

            ' If the row has values, the store procedure parameters must be initialized
            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                Dim commandParameters() As Npgsql.NpgsqlParameter = SqlDALParameterCache.GetSpParameterSet(transaction.Connection, spName)

                ' Set the parameters values
                AssignParameterValues(commandParameters, dataRow)

                ExecuteScalarTypedParams = SqlDAL.ExecuteScalar(transaction, CommandType.StoredProcedure, spName, commandParameters)
            Else
                ExecuteScalarTypedParams = SqlDAL.ExecuteScalar(transaction, CommandType.StoredProcedure, spName)
            End If
        End Function
#End Region

#Region "ADD PARAMETER"
        Public Shared Function Add_Parameter(ByVal paramName As String,
                                             ByVal paramValue As Object,
                                             ByVal paramType As Object,
                                             Optional ByVal paramSize As Integer = 0,
                                             Optional ByVal paramDirectionName As ParameterDirection = ParameterDirection.Input) As Npgsql.NpgsqlParameter
            Dim sqlParam As New Npgsql.NpgsqlParameter

            sqlParam.ParameterName = paramName
            sqlParam.Value = paramValue
            sqlParam.NpgsqlDbType = paramType
            sqlParam.Size = paramSize
            sqlParam.Direction = paramDirectionName

            Return sqlParam
        End Function
#End Region

#Region "GET OUTPUT PARAM"

        Public Shared Function GetOutPutParam(ByVal sqlParams() As Object, ByVal strParamName As String) As Object
            Dim objResult As Object = Nothing
            Dim iEnum As IEnumerator = sqlParams.GetEnumerator
            While iEnum.MoveNext
                If iEnum.Current.ToString = strParamName Then
                    objResult = DirectCast(iEnum.Current, Npgsql.NpgsqlParameter).Value
                    Exit While
                End If
            End While

            Return objResult
        End Function

#End Region

        Private Shared Sub ExecuteCommand(ByVal connection As NpgsqlConnection, ByVal cmd As NpgsqlCommand, ByRef ds As DataSet, ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter)
            Try
                Dim i As Int32 = 0
                Dim dataAdatpter As NpgsqlDataAdapter

                cmd.ExecuteNonQuery()

                For Each parm As NpgsqlParameter In commandParameters
                    If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                        If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                            Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                            dataAdatpter = New NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                            ds.Tables.Add("Table" & i)
                            dataAdatpter.Fill(ds.Tables(i))
                            i += 1
                        End If
                    End If
                Next
            Catch ex As Exception
                Throw
            End Try
        End Sub

    End Class ' SqlDAL

    ' SqlDALParameterCache provides functions to leverage a static cache of procedure parameters, and the
    ' ability to discover parameters for stored procedures at run-time.
    Public NotInheritable Class SqlDALParameterCache

#Region "private methods, variables, and constructors"


        ' Since this class provides only static methods, make the default constructor private to prevent 
        ' instances from being created with "new SqlHelperParameterCache()".
        Private Sub New()
        End Sub ' New 

        Private Shared paramCache As Hashtable = Hashtable.Synchronized(New Hashtable)

        ' resolve at run time the appropriate set of SqlParameters for a stored procedure
        ' Parameters:
        ' - connectionString - a valid connection string for a NpgsqlConnection
        ' - spName - the name of the stored procedure
        ' - includeReturnValueParameter - whether or not to include their return value parameter>
        ' Returns: Npgsql.NpgsqlParameter()
        Private Shared Function DiscoverSpParameterSet(ByVal connection As NpgsqlConnection,
                                                           ByVal spName As String,
                                                           ByVal includeReturnValueParameter As Boolean,
                                                           ByVal ParamArray parameterValues() As Object) As Npgsql.NpgsqlParameter()

            If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
            Dim cmd As New NpgsqlCommand(spName, connection)
            cmd.CommandType = CommandType.StoredProcedure
            Dim discoveredParameters() As Npgsql.NpgsqlParameter
            connection.Open()
            NpgsqlCommandBuilder.DeriveParameters(cmd)
            connection.Close()
            'Commented because not used in NpgSQL
            'If Not includeReturnValueParameter Then
            '    cmd.Parameters.RemoveAt(0)
            'End If

            discoveredParameters = New Npgsql.NpgsqlParameter(cmd.Parameters.Count - 1) {}
            cmd.Parameters.CopyTo(discoveredParameters, 0)

            ' Init the parameters with a DBNull value
            Dim discoveredParameter As Npgsql.NpgsqlParameter
            For Each discoveredParameter In discoveredParameters
                discoveredParameter.Value = DBNull.Value
            Next

            Return discoveredParameters

        End Function ' DiscoverSpParameterSet

        ' Deep copy of cached Npgsql.NpgsqlParameter array
        Private Shared Function CloneParameters(ByVal originalParameters() As Npgsql.NpgsqlParameter) As Npgsql.NpgsqlParameter()

            Dim i As Integer
            Dim j As Integer = originalParameters.Length - 1
            Dim clonedParameters(j) As Npgsql.NpgsqlParameter

            For i = 0 To j
                clonedParameters(i) = CType(CType(originalParameters(i), ICloneable).Clone, Npgsql.NpgsqlParameter)
            Next

            Return clonedParameters
        End Function ' CloneParameters


#End Region

#Region "caching functions"

        ' add parameter array to the cache
        ' Parameters
        ' -connectionString - a valid connection string for a NpgsqlConnection 
        ' -commandText - the stored procedure name or T-SQL command 
        ' -commandParameters - an array of SqlParamters to be cached 
        Public Shared Sub CacheParameterSet(ByVal connectionString As String,
                                            ByVal commandText As String,
                                            ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter)
            If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
            If (commandText Is Nothing OrElse commandText.Length = 0) Then Throw New ArgumentNullException("commandText")

            Dim hashKey As String = connectionString + ":" + commandText

            paramCache(hashKey) = commandParameters
        End Sub ' CacheParameterSet

        ' retrieve a parameter array from the cache
        ' Parameters:
        ' -connectionString - a valid connection string for a NpgsqlConnection 
        ' -commandText - the stored procedure name or T-SQL command 
        ' Returns: An array of SqlParamters 
        Public Shared Function GetCachedParameterSet(ByVal connectionString As String, ByVal commandText As String) As Npgsql.NpgsqlParameter()
            If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
            If (commandText Is Nothing OrElse commandText.Length = 0) Then Throw New ArgumentNullException("commandText")

            Dim hashKey As String = connectionString + ":" + commandText
            Dim cachedParameters As Npgsql.NpgsqlParameter() = CType(paramCache(hashKey), Npgsql.NpgsqlParameter())

            If cachedParameters Is Nothing Then
                Return Nothing
            Else
                Return CloneParameters(cachedParameters)
            End If
        End Function ' GetCachedParameterSet

#End Region

#Region "Parameter Discovery Functions"
        ' Retrieves the set of SqlParameters appropriate for the stored procedure.
        ' This method will query the database for this information, and then store it in a cache for future requests.
        ' Parameters:
        ' -connectionString - a valid connection string for a NpgsqlConnection 
        ' -spName - the name of the stored procedure 
        ' Returns: An array of SqlParameters
        Public Overloads Shared Function GetSpParameterSet(ByVal connectionString As String, ByVal spName As String) As Npgsql.NpgsqlParameter()
            Return GetSpParameterSet(connectionString, spName, False)
        End Function ' GetSpParameterSet 

        ' Retrieves the set of SqlParameters appropriate for the stored procedure.
        ' This method will query the database for this information, and then store it in a cache for future requests.
        ' Parameters:
        ' -connectionString - a valid connection string for a NpgsqlConnection
        ' -spName - the name of the stored procedure 
        ' -includeReturnValueParameter - a bool value indicating whether the return value parameter should be included in the results 
        ' Returns: An array of SqlParameters 
        Public Overloads Shared Function GetSpParameterSet(ByVal connectionString As String,
                                                           ByVal spName As String,
                                                           ByVal includeReturnValueParameter As Boolean) As Npgsql.NpgsqlParameter()
            If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
            Dim connection As NpgsqlConnection
            Try
                connection = New NpgsqlConnection(connectionString)
                GetSpParameterSet = GetSpParameterSetInternal(connection, spName, includeReturnValueParameter)
            Catch ex As Exception
                Throw
            Finally
                If Not connection Is Nothing Then
                    connection.Dispose()
                End If
            End Try
        End Function ' GetSpParameterSet

        ' Retrieves the set of SqlParameters appropriate for the stored procedure.
        ' This method will query the database for this information, and then store it in a cache for future requests.
        ' Parameters:
        ' -connection - a valid NpgsqlConnection object
        ' -spName - the name of the stored procedure 
        ' -includeReturnValueParameter - a bool value indicating whether the return value parameter should be included in the results 
        ' Returns: An array of SqlParameters 
        Public Overloads Shared Function GetSpParameterSet(ByVal connection As NpgsqlConnection,
                                                           ByVal spName As String) As Npgsql.NpgsqlParameter()

            GetSpParameterSet = GetSpParameterSet(connection, spName, False)
        End Function ' GetSpParameterSet

        ' Retrieves the set of SqlParameters appropriate for the stored procedure.
        ' This method will query the database for this information, and then store it in a cache for future requests.
        ' Parameters:
        ' -connection - a valid NpgsqlConnection object
        ' -spName - the name of the stored procedure 
        ' -includeReturnValueParameter - a bool value indicating whether the return value parameter should be included in the results 
        ' Returns: An array of SqlParameters 
        Public Overloads Shared Function GetSpParameterSet(ByVal connection As NpgsqlConnection,
                                                           ByVal spName As String,
                                                           ByVal includeReturnValueParameter As Boolean) As Npgsql.NpgsqlParameter()
            If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
            Dim clonedConnection As NpgsqlConnection
            Try
                clonedConnection = CType((CType(connection, ICloneable).Clone), NpgsqlConnection)
                GetSpParameterSet = GetSpParameterSetInternal(clonedConnection, spName, includeReturnValueParameter)
            Catch ex As Exception
                Throw
            Finally
                If Not clonedConnection Is Nothing Then
                    clonedConnection.Dispose()
                End If
            End Try
        End Function ' GetSpParameterSet

        ' Retrieves the set of SqlParameters appropriate for the stored procedure.
        ' This method will query the database for this information, and then store it in a cache for future requests.
        ' Parameters:
        ' -connection - a valid NpgsqlConnection object
        ' -spName - the name of the stored procedure 
        ' -includeReturnValueParameter - a bool value indicating whether the return value parameter should be included in the results 
        ' Returns: An array of SqlParameters 
        Private Overloads Shared Function GetSpParameterSetInternal(ByVal connection As NpgsqlConnection,
                                                        ByVal spName As String,
                                                        ByVal includeReturnValueParameter As Boolean) As Npgsql.NpgsqlParameter()

            If (connection Is Nothing) Then Throw New ArgumentNullException("connection")

            Dim cachedParameters() As Npgsql.NpgsqlParameter
            Dim hashKey As String

            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

            hashKey = connection.ConnectionString + ":" + spName + IIf(includeReturnValueParameter = True, ":include ReturnValue Parameter", "").ToString

            cachedParameters = CType(paramCache(hashKey), Npgsql.NpgsqlParameter())

            If (cachedParameters Is Nothing) Then
                Dim spParameters() As Npgsql.NpgsqlParameter = DiscoverSpParameterSet(connection, spName, includeReturnValueParameter)
                paramCache(hashKey) = spParameters
                cachedParameters = spParameters

            End If

            Return CloneParameters(cachedParameters)

        End Function ' GetSpParameterSet
#End Region

    End Class ' SqlDALParameterCache

End Namespace