Imports System.Data.SqlClient
Imports System.ComponentModel
Imports Npgsql

' Pending Tasks
'   Connection Pooling (Refer DAAG.PDF)

'Catch ex As SqlException
'    lobjSqlCommand.Cancel()
Namespace BACRMAPI.DataAccessLayer

    Public NotInheritable Class DALObject : Implements System.IDisposable

#Region " Private Variables "

        Protected mobjSqlConnection As NpgSqlConnection
        Protected mobjSqlTransaction As NpgSqlTransaction
#End Region

#Region " New(ConnectionString), Dispose(), Open(), Close() "
        Public Sub New( _
            ByVal pstrConnectionString As String)
            mobjSqlConnection = New NpgsqlConnection(pstrConnectionString)
            OpenConnection()
        End Sub
        Public Sub Dispose() Implements IDisposable.Dispose
            If Not IsNothing(mobjSqlTransaction) Then
                mobjSqlTransaction.Dispose()
            End If
            CloseConnection()
            mobjSqlConnection.Dispose()
            GC.SuppressFinalize(Me)
        End Sub

        Public Sub OpenConnection()
            If mobjSqlConnection.State <> ConnectionState.Open Then
                mobjSqlConnection.Open()
            End If
        End Sub

        Public Sub CloseConnection()
            If mobjSqlConnection.State <> ConnectionState.Closed Then
                mobjSqlConnection.Close()
            End If
        End Sub


#End Region

#Region " Transactions "

        Public Overloads Sub BeginTransaction()
            mobjSqlTransaction = mobjSqlConnection.BeginTransaction()
        End Sub
        Public Overloads Sub BeginTransaction( _
            ByVal pstrTransactionName As String)
            mobjSqlTransaction = mobjSqlConnection.BeginTransaction(pstrTransactionName)
        End Sub
        Public Sub SaveTransaction( _
            ByVal pstrSavePointName As String)
            mobjSqlTransaction.Save(pstrSavePointName)
        End Sub
        Public Sub CommitTransaction()
            mobjSqlTransaction.Commit()
        End Sub
        Public Overloads Sub RollbackTransaction()
            mobjSqlTransaction.Rollback()
        End Sub
        Public Overloads Sub RollbackTransaction( _
            ByVal pstrTransactionName As String)
            mobjSqlTransaction.Rollback(pstrTransactionName)
        End Sub

#End Region

#Region " ExecuteNonQueryStorProc w/wo Parameters (Returns Integer)"

        Public Overloads Function ExecuteNonQueryStorProc( _
            ByVal pstrStorProcName As String, _
            ByVal pParameters As IDataParameter(), _
            ByVal pblnTransactionRequired As Boolean) _
            As Integer

            Dim lobjSqlCommand As NpgsqlCommand

            Try
                lobjSqlCommand =
                  BuildCommandObject(pstrStorProcName, pParameters, pblnTransactionRequired)

                Return lobjSqlCommand.ExecuteNonQuery()

            Finally
                lobjSqlCommand.Dispose()
                lobjSqlCommand = Nothing

            End Try

        End Function

        Public Overloads Function ExecuteNonQueryStorProc(
             ByVal pstrStorProcName As String,
             ByVal pblnTransactionRequired As Boolean) _
             As Integer

            Dim lobjSqlCommand As NpgsqlCommand

            Try
                lobjSqlCommand =
                  BuildCommandObject(pstrStorProcName, pblnTransactionRequired)

                Return lobjSqlCommand.ExecuteNonQuery()

            Finally
                lobjSqlCommand.Dispose()
                lobjSqlCommand = Nothing

            End Try

        End Function

#End Region


#Region " ExecuteScalarStorProc w/wo Parameters (Returns String)"

        Public Overloads Function ExecuteScalarStorProc(
            ByVal pstrStorProcName As String,
            ByVal pParameters As IDataParameter(),
            ByVal pblnTransactionRequired As Boolean) _
            As String

            Dim lobjSqlCommand As NpgsqlCommand

            Try
                lobjSqlCommand =
                  BuildCommandObject(pstrStorProcName, pParameters, pblnTransactionRequired)

                Return lobjSqlCommand.ExecuteScalar.ToString()

            Finally
                lobjSqlCommand.Dispose()
                lobjSqlCommand = Nothing

            End Try

        End Function

        Public Overloads Function ExecuteScalarStorProc(
             ByVal pstrStorProcName As String,
             ByVal pblnTransactionRequired As Boolean) _
             As String

            Dim lobjSqlCommand As NpgsqlCommand

            Try
                lobjSqlCommand =
                  BuildCommandObject(pstrStorProcName, pblnTransactionRequired)

                Return lobjSqlCommand.ExecuteScalar.ToString()

            Finally
                lobjSqlCommand.Dispose()
                lobjSqlCommand = Nothing

            End Try

        End Function

#End Region


#Region " ExecuteReaderStorProc w/wo Parameters (Returns DataReader)"

        Public Overloads Function ExecuteReaderStorProc(
            ByVal pstrStorProcName As String,
            ByVal pParameters As IDataParameter(),
            ByVal pblnTransactionRequired As Boolean) _
            As NpgsqlDataReader

            Dim lobjSqlCommand As NpgsqlCommand

            Try
                lobjSqlCommand =
                  BuildCommandObject(pstrStorProcName, pParameters, pblnTransactionRequired)

                Return lobjSqlCommand.ExecuteReader(CommandBehavior.CloseConnection)

            Finally
                lobjSqlCommand.Dispose()
                lobjSqlCommand = Nothing

            End Try

        End Function

        Public Overloads Function ExecuteReaderStorProc(
            ByVal pstrStorProcName As String,
            ByVal pblnTransactionRequired As Boolean) _
            As NpgsqlDataReader

            Dim lobjSqlCommand As NpgsqlCommand

            Try
                lobjSqlCommand =
                  BuildCommandObject(pstrStorProcName, pblnTransactionRequired)

                Return lobjSqlCommand.ExecuteReader(CommandBehavior.CloseConnection)

            Finally
                lobjSqlCommand.Dispose()
                lobjSqlCommand = Nothing

            End Try

        End Function

#End Region


#Region " ExecuteDataViewStorProc w/wo Parameters (Returns DataView)"

        Public Overloads Function ExecuteDataViewStorProc(
          ByVal pstrStorProcName As String,
          ByVal pParameters As IDataParameter(),
          ByVal pblnTransactionRequired As Boolean) _
          As DataView

            Dim lobjSqlDataAdapter As NpgsqlDataAdapter
            Dim pobjDataSet As New DataSet

            Try
                lobjSqlDataAdapter = New NpgsqlDataAdapter
                lobjSqlDataAdapter.SelectCommand =
                    BuildCommandObject(pstrStorProcName, pParameters, pblnTransactionRequired)
                lobjSqlDataAdapter.Fill(pobjDataSet)

                Return pobjDataSet.Tables(0).DefaultView

            Finally
                lobjSqlDataAdapter.Dispose()
                lobjSqlDataAdapter = Nothing

            End Try

        End Function

        Public Overloads Function ExecuteDataViewStorProc(
          ByVal pstrStorProcName As String,
          ByVal pblnTransactionRequired As Boolean) _
          As DataView

            Dim lobjSqlDataAdapter As NpgsqlDataAdapter
            Dim pobjDataSet As New DataSet

            Try
                lobjSqlDataAdapter = New NpgsqlDataAdapter
                lobjSqlDataAdapter.SelectCommand =
                    BuildCommandObject(pstrStorProcName, pblnTransactionRequired)
                lobjSqlDataAdapter.Fill(pobjDataSet)

                Return pobjDataSet.Tables(0).DefaultView

            Finally
                lobjSqlDataAdapter.Dispose()
                lobjSqlDataAdapter = Nothing

            End Try

        End Function

#End Region


#Region " ExecuteDataViewStorProc w/wo Parameters (Returns Datatable)"

        Public Overloads Function ExecuteDataTableStorProc(
          ByVal pstrStorProcName As String,
          ByVal pParameters As IDataParameter(),
          ByVal pblnTransactionRequired As Boolean) _
          As DataTable

            Dim lobjSqlDataAdapter As NpgsqlDataAdapter
            Dim pobjDataSet As New DataSet

            Try
                lobjSqlDataAdapter = New NpgsqlDataAdapter
                lobjSqlDataAdapter.SelectCommand =
                    BuildCommandObject(pstrStorProcName, pParameters, pblnTransactionRequired)
                lobjSqlDataAdapter.Fill(pobjDataSet)

                Return pobjDataSet.Tables(0)

            Finally
                lobjSqlDataAdapter.Dispose()
                lobjSqlDataAdapter = Nothing

            End Try

        End Function

        Public Overloads Function ExecuteDataTableStorProc(
          ByVal pstrStorProcName As String,
          ByVal pblnTransactionRequired As Boolean) _
          As DataTable

            Dim lobjSqlDataAdapter As NpgsqlDataAdapter
            Dim pobjDataSet As New DataSet

            Try
                lobjSqlDataAdapter = New NpgsqlDataAdapter
                lobjSqlDataAdapter.SelectCommand =
                    BuildCommandObject(pstrStorProcName, pblnTransactionRequired)
                lobjSqlDataAdapter.Fill(pobjDataSet)

                Return pobjDataSet.Tables(0)

            Finally
                lobjSqlDataAdapter.Dispose()
                lobjSqlDataAdapter = Nothing

            End Try

        End Function

#End Region


#Region " PVT BuildCommandObject w/wo Parameters (Returns CommandObject)"

        Private Overloads Function BuildCommandObject(
          ByVal pstrStorProcName As String,
          ByVal pParameters As IDataParameter(),
          ByVal pblnTransactionRequired As Boolean) _
          As NpgsqlCommand

            Dim lobjSqlCommand As NpgsqlCommand
            Dim lobjSqlParameter As Npgsql.NpgsqlParameter

            Try
                lobjSqlCommand = New NpgsqlCommand(pstrStorProcName, mobjSqlConnection)
                lobjSqlCommand.CommandType = CommandType.StoredProcedure

                'Check whether Transaction is required or not
                If pblnTransactionRequired = True Then
                    lobjSqlCommand.Transaction = mobjSqlTransaction
                End If

                For Each lobjSqlParameter In pParameters
                    lobjSqlCommand.Parameters.Add(lobjSqlParameter)
                Next

                Return lobjSqlCommand

            Finally
                lobjSqlCommand.Dispose()
                lobjSqlCommand = Nothing

            End Try

        End Function

        Private Overloads Function BuildCommandObject(
          ByVal pstrStorProcName As String,
          ByVal pblnTransactionRequired As Boolean) _
          As NpgsqlCommand

            Dim lobjSqlCommand As NpgsqlCommand

            Try
                lobjSqlCommand = New NpgsqlCommand(pstrStorProcName, mobjSqlConnection)
                lobjSqlCommand.CommandType = CommandType.StoredProcedure

                'Check whether Transaction is required or not
                If pblnTransactionRequired = True Then
                    lobjSqlCommand.Transaction = mobjSqlTransaction
                End If

                Return lobjSqlCommand

            Finally
                lobjSqlCommand.Dispose()
                lobjSqlCommand = Nothing

            End Try
        End Function

#End Region


#Region " ExecuteDataViewStorProc w/wo Parameters (Returns Boolean)"

        Public Overloads Function ExecuteDataBooleanStorProc(
          ByVal pstrStorProcName As String,
          ByVal pParameters As IDataParameter(),
          ByVal pblnTransactionRequired As Boolean) _
          As Boolean

            Dim lobjSqlDataAdapter As NpgsqlDataAdapter
            Dim pobjDataSet As New DataSet

            Try
                lobjSqlDataAdapter = New NpgsqlDataAdapter
                lobjSqlDataAdapter.SelectCommand =
                    BuildCommandObject(pstrStorProcName, pParameters, pblnTransactionRequired)
                lobjSqlDataAdapter.Fill(pobjDataSet)
                Return ExecuteDataBooleanStorProc = True
            Finally
                lobjSqlDataAdapter.Dispose()
                lobjSqlDataAdapter = Nothing
            End Try
        End Function

        Public Overloads Function ExecuteDataBooleanStorProc(
          ByVal pstrStorProcName As String,
          ByVal pblnTransactionRequired As Boolean) _
          As Boolean

            Dim lobjSqlDataAdapter As NpgsqlDataAdapter
            Dim pobjDataSet As New DataSet

            Try
                lobjSqlDataAdapter = New NpgsqlDataAdapter
                lobjSqlDataAdapter.SelectCommand = _
                    BuildCommandObject(pstrStorProcName, pblnTransactionRequired)
                lobjSqlDataAdapter.Fill(pobjDataSet)

                Return True

            Finally
                lobjSqlDataAdapter.Dispose()
                lobjSqlDataAdapter = Nothing

            End Try

        End Function

#End Region

    End Class
End Namespace

