' CSqlHelper.vb
' CHANGE CONTROL:
'	ORIGINAL AUTHOR: Microsoft Application block for Data Access.

'	Revision 				DATE				VERSION	CHANGES	KEYSTRING:
'**********************************************************************************
'The CSqlHelper class is almost a copy of Microsoft Application Block's 
' CSqlHelper class except for Connection String Modification.
' The CSqlHelper class is intended to encapsulate high performance, scalable 
' best practices for common uses of SqlClient.
' ===============================================================================

Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports System.Configuration
Imports System.Text
Imports System.Xml
Imports Npgsql

Namespace BACRMAPI.DataAccessLayer
    Public NotInheritable Class CSqlHelper

#Region "private utility methods & constructors"
        Private Shared _connString As String
        Private Const _connectionKey As String = "ConnectionString"


        '**********************************************************************************
        ' Name       : _getConnectionString
        ' Type       :  Sub
        ' Scope      : Private
        ' Returns    : None
        ' Parameters : None
        ' Description: This function get the Connection String spcified in the Web Config File 
        '**********************************************************************************
        Private Shared Function _getConnectionString() As String
            _connString = ConfigurationManager.AppSettings(_connectionKey)
        End Function
        '**********************************************************************************
        ' Name       : New
        ' Type       : Sub
        ' Scope      : Private
        ' Returns    : None
        ' Parameters : None
        ' Description: Since this class provides only static methods, make the default constructor  
        '               private to prevent instances from being created with "new CSqlHelper()
        '**********************************************************************************
        Private Sub New()
        End Sub

        '**********************************************************************************
        ' Name       : AttachParameters
        ' Type       : Shared Sub
        ' Scope      : Private
        ' Returns    : None
        ' Parameters : ByVal command As NpgSqlCommand, ByVal commandParameters() As Npgsql.NpgsqlParameter
        ' Description: This method is used to attach array of SqlParameters to a NpgSqlCommand. 
        '               This method will assign a value of DbNull to any parameter with a direction of
        '               InputOutput and a value of null.
        '**********************************************************************************
        Private Shared Sub AttachParameters(ByVal command As NpgsqlCommand, ByVal commandParameters() As Npgsql.NpgsqlParameter)
            If (command Is Nothing) Then Throw New ArgumentNullException("command")
            If (Not commandParameters Is Nothing) Then
                Dim p As Npgsql.NpgsqlParameter
                For Each p In commandParameters
                    If (Not p Is Nothing) Then
                        ' Check for derived output value with no value assigned
                        If (p.Direction = ParameterDirection.InputOutput OrElse p.Direction = ParameterDirection.Input) AndAlso p.Value Is Nothing Then
                            p.Value = DBNull.Value
                        End If
                        command.Parameters.Add(p)
                    End If
                Next p
            End If
        End Sub

        '**********************************************************************************
        ' Name       : AssignParameterValues
        ' Type       : Shared Sub
        ' Scope      : Private
        ' Returns    : None
        ' Parameters : ByVal commandParameters() As Npgsql.NpgsqlParameter, ByVal dataRow As DataRow.
        ' Description: This method assigns dataRow column values to an array of SqlParameters.
        '**********************************************************************************
        Private Overloads Shared Sub AssignParameterValues(ByVal commandParameters() As Npgsql.NpgsqlParameter, ByVal dataRow As DataRow)
            If commandParameters Is Nothing OrElse dataRow Is Nothing Then
                ' Do nothing if we get no data    
                Exit Sub
            End If

            ' Set the parameters values
            Dim commandParameter As Npgsql.NpgsqlParameter
            Dim i As Integer
            For Each commandParameter In commandParameters
                ' Check the parameter name
                If (commandParameter.ParameterName Is Nothing OrElse commandParameter.ParameterName.Length <= 1) Then
                    Throw New Exception(String.Format("Please provide a valid parameter name on the parameter #{0}, the ParameterName property has the following value: ' {1}' .", i, commandParameter.ParameterName))
                End If
                If dataRow.Table.Columns.IndexOf(commandParameter.ParameterName.Substring(1)) <> -1 Then
                    commandParameter.Value = dataRow(commandParameter.ParameterName.Substring(1))
                End If
                i = i + 1
            Next
        End Sub
        '**********************************************************************************
        ' Name       : AssignParameterValues
        ' Type       : Overloaded Shared Sub
        ' Scope      : Private
        ' Returns    : None
        ' Parameters : ByVal commandParameters() As Npgsql.NpgsqlParameter, ByVal parameterValues() As Object.
        ' Description: This method assigns an array of values to an array of SqlParameters.
        '**********************************************************************************
        Private Overloads Shared Sub AssignParameterValues(ByVal commandParameters() As Npgsql.NpgsqlParameter, ByVal parameterValues() As Object)

            Dim i As Integer
            Dim j As Integer

            If (commandParameters Is Nothing) AndAlso (parameterValues Is Nothing) Then
                ' Do nothing if we get no data
                Return
            End If

            ' We must have the same number of values as we pave parameters to put them in
            If commandParameters.Length <> parameterValues.Length Then
                Throw New ArgumentException("Parameter count does not match Parameter Value count.")
            End If

            ' Value array
            j = commandParameters.Length - 1
            For i = 0 To j
                ' If the current array value derives from IDbDataParameter, then assign its Value property
                If TypeOf parameterValues(i) Is IDbDataParameter Then
                    Dim paramInstance As IDbDataParameter = CType(parameterValues(i), IDbDataParameter)
                    If (paramInstance.Value Is Nothing) Then
                        commandParameters(i).Value = DBNull.Value
                    Else
                        commandParameters(i).Value = paramInstance.Value
                    End If
                ElseIf (parameterValues(i) Is Nothing) Then
                    commandParameters(i).Value = DBNull.Value
                Else
                    commandParameters(i).Value = parameterValues(i)
                End If
            Next
        End Sub ' AssignParameterValues

        '**********************************************************************************
        ' Name       : ReAssignParameterValues
        ' Type       : Overloaded Shared Sub
        ' Scope      : Private
        ' Returns    : None
        ' Parameters : ByVal commandParameters() As Npgsql.NpgsqlParameter, ByVal parameterValues() As Npgsql.NpgsqlParameter
        ' Description: This method Reassigns an array of values to an array of SqlParameters.
        '**********************************************************************************
        Private Overloads Shared Sub ReAssignParameterValues(ByVal commandParameters() As Npgsql.NpgsqlParameter, ByVal parameterValues() As Npgsql.NpgsqlParameter)

            Dim i As Integer
            Dim j As Integer

            If (commandParameters Is Nothing) AndAlso (parameterValues Is Nothing) Then
                ' Do nothing if we get no data
                Return
            End If

            ' We must have the same number of values as we pave parameters to put them in
            If commandParameters.Length <> parameterValues.Length Then
                Throw New ArgumentException("Parameter count does not match Parameter Value count.")
            End If

            Dim intCnt As Int32 = 0
            For intCnt = 0 To commandParameters.Length - 1
                parameterValues(intCnt).Value = commandParameters(intCnt).Value
            Next
        End Sub


        '**********************************************************************************
        ' Name       : PrepareCommand
        ' Type       : Shared Sub
        ' Scope      : Private
        ' Returns    : None
        ' Parameters : ByVal command As NpgSqlCommand, ByVal connection As NpgsqlConnection, 
        '                                ByVal transaction As NpgSqlTransaction, ByVal commandType As CommandType, _
        '                                ByVal commandText As String, ByVal commandParameters() As Npgsql.NpgsqlParameter, ByRef mustCloseConnection As Boolean
        ' Description: This method opens (if necessary) and assigns a connection, transaction,  
        '               command type and parameters to the provided command.
        '**********************************************************************************
        Private Shared Sub PrepareCommand(ByVal command As NpgsqlCommand,
                                             ByVal connection As NpgsqlConnection,
                                             ByVal transaction As NpgsqlTransaction,
                                             ByVal commandType As CommandType,
                                             ByVal commandText As String,
                                             ByVal commandParameters() As Npgsql.NpgsqlParameter, ByRef mustCloseConnection As Boolean)

            Try
                If (command Is Nothing) Then Throw New ArgumentNullException("command")
                If (commandText Is Nothing OrElse commandText.Length = 0) Then Throw New ArgumentNullException("commandText")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            End Try

            ' If the provided connection is not open, we will open it
            If connection.State <> ConnectionState.Open Then
                connection.Open()
                mustCloseConnection = True
            Else
                mustCloseConnection = False
            End If

            ' Associate the connection with the command
            command.Connection = connection

            ' Set the command text (stored procedure name or SQL statement)
            command.CommandText = commandText

            ' If we were provided a transaction, assign it.
            If Not (transaction Is Nothing) Then
                Try
                    If transaction.Connection Is Nothing Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
                Catch exArg As ArgumentException
                    Throw New Exception("The transaction was rollbacked or commited, please provide an open transaction.")
                End Try

                command.Transaction = transaction
            End If

            ' Set the command type
            command.CommandType = commandType

            ' Attach the command parameters if they are provided
            If Not (commandParameters Is Nothing) Then
                AttachParameters(command, commandParameters)
            End If
            Return
        End Sub

#End Region

#Region "ExecuteNonQuery"

        '**********************************************************************************
        ' Name       : ExecuteNonQuery
        ' Type       : Function
        ' Scope      : Public
        ' Returns    : Integer (number of rows affected by the command)
        ' Parameters : ByVal commandType As CommandType, ByVal commandText As String
        ' Description: Execute a NpgSqlCommand (that returns no resultset and takes no parameters) 
        '               against the database specified in  the connection string.
        '**********************************************************************************
        Public Overloads Shared Function ExecuteNonQuery(ByVal commandType As CommandType,
                                                    ByVal commandText As String) As Integer
            ' Pass through the call providing null for the set of SqlParameters
            Return ExecuteNonQuery(commandType, commandText, CType(Nothing, Npgsql.NpgsqlParameter()))
        End Function

        '**********************************************************************************
        ' Name       : ExecuteNonQuery
        ' Type       : Function
        ' Scope      : Public
        ' Returns    : Integer (number of rows affected by the command)
        ' Parameters : ByVal commandType As CommandType, 
        '              ByVal commandText As String, ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter
        ' Description: Execute a NpgSqlCommand (that returns no resultset) 
        '              against the database specified in the connection string using the provided parameters.
        '**********************************************************************************
        Public Overloads Shared Function ExecuteNonQuery(ByVal commandType As CommandType,
                                                        ByVal commandText As String,
                                                        ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter) As Integer
            Try
                _getConnectionString()
                If (_connString Is Nothing OrElse _connString.Length = 0) Then
                    Throw New ArgumentNullException("connectionString")
                End If
            Catch exArgNull As ArgumentNullException
                Throw New Exception("ConnectionString to database should not be null")
            End Try

            ' Create & open a NpgsqlConnection, and dispose of it after we are done
            Dim connection As NpgsqlConnection
            Try
                connection = New NpgsqlConnection(_connString)
                connection.Open()

                ' Call the overload that takes a connection in place of the connection string
                Return ExecuteNonQuery(connection, commandType, commandText, commandParameters)
            Catch exSQL As SqlException
                Throw New Exception(exSQL.Message)
            Catch ex As Exception
                'Throw ex
                Throw New Exception(ex.Message)
            Finally
                If Not connection Is Nothing Then connection.Dispose()
            End Try
        End Function

        '**********************************************************************************
        ' Name       : ExecuteNonQuery
        ' Type       : Function
        ' Scope      : Public
        ' Returns    : Integer (number of rows affected by the command)
        ' Parameters : ByVal spName As String, ByVal ParamArray parameterValues() As Object
        ' Description: Execute a stored procedure via a NpgSqlCommand (that returns no resultset) against the database specified in 
        '           the connection string using the provided parameter values.  This method will discover the parameters for the 
        '           stored procedure, and assign the values based on parameter order.
        '**********************************************************************************
        Public Overloads Shared Function ExecuteNonQuery(ByVal spName As String,
                                                        ByVal ParamArray parameterValues() As Object) As Integer


            If (spName Is Nothing OrElse spName.Length = 0) Then
                Throw New ArgumentNullException("spName")
            End If

            Dim commandParameters As Npgsql.NpgsqlParameter()

            ' If we receive parameter values, we need to figure out where they go
            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)

                commandParameters = SqlHelperParameterCache.GetSpParameterSet(spName)

                ' Assign the provided values to these parameters based on parameter order
                AssignParameterValues(commandParameters, parameterValues)

                ' Call the overload that takes an array of SqlParameters
                Return ExecuteNonQuery(CommandType.StoredProcedure, spName, commandParameters)
                ' Otherwise we can just call the SP without params
            Else
                Return ExecuteNonQuery(CommandType.StoredProcedure, spName)
            End If
        End Function

        '**********************************************************************************
        ' Name       : ExecuteNonQuery
        ' Type       : Overloaded Function
        ' Scope      : Public
        ' Returns    : Integer (number of rows affected by the command)
        ' Parameters : ByVal connection As NpgsqlConnection, ByVal commandType As CommandType, ByVal commandText As String
        ' Description: Execute a NpgSqlCommand (that returns no resultset and takes no parameters) 
        '               against the provided NpgsqlConnection.
        '**********************************************************************************
        Public Overloads Shared Function ExecuteNonQuery(ByVal connection As NpgsqlConnection,
                                                        ByVal commandType As CommandType,
                                                        ByVal commandText As String) As Integer
            ' Pass through the call providing null for the set of SqlParameters
            Return ExecuteNonQuery(connection, commandType, commandText, CType(Nothing, Npgsql.NpgsqlParameter()))

        End Function

        '**********************************************************************************
        ' Name       : ExecuteNonQuery
        ' Type       : Overloaded Function
        ' Scope      : Public
        ' Returns    : Integer (number of rows affected by the command)
        ' Parameters : ByVal connection As NpgsqlConnection, ByVal commandType As CommandType, 
        '               ByVal commandText As String, ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter
        ' Description: Execute a NpgSqlCommand (that returns no resultset) against the specified NpgsqlConnection
        '**********************************************************************************
        Public Overloads Shared Function ExecuteNonQuery(ByVal connection As NpgsqlConnection,
                                                        ByVal commandType As CommandType,
                                                        ByVal commandText As String,
                                                        ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter) As Integer

            Try
                If connection Is Nothing Then
                    Throw New ArgumentNullException("connection")
                End If
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgsqlConnection Parameter should not be null")
            End Try

            ' Create a command and prepare it for execution
            Dim cmd As New NpgsqlCommand
            Dim retval As Integer
            Dim mustCloseConnection As Boolean = False

            PrepareCommand(cmd, connection, CType(Nothing, NpgsqlTransaction), commandType, commandText, commandParameters, mustCloseConnection)

            ' Finally, execute the command
            retval = cmd.ExecuteNonQuery()

            ' Detach the SqlParameters from the command object, so they can be used again
            cmd.Parameters.Clear()

            If (mustCloseConnection) Then connection.Close()

            Return retval
        End Function

        '**********************************************************************************
        ' Name       : ExecuteNonQuery
        ' Type       : Overloaded Function
        ' Scope      : Public
        ' Returns    : Integer (number of rows affected by the command)
        ' Parameters : ByVal connection As NpgsqlConnection, ByVal spName As String, ByVal ParamArray parameterValues() As Object
        ' Description: Execute a stored procedure via a NpgSqlCommand (that returns no resultset) 
        '               against the specified NpgsqlConnection.
        '**********************************************************************************
        Public Overloads Shared Function ExecuteNonQuery(ByVal connection As NpgsqlConnection,
                                                        ByVal spName As String,
                                                        ByVal ParamArray parameterValues() As Object) As Integer
            Try
                If connection Is Nothing Then
                    Throw New ArgumentNullException("connection")
                End If
                If spName Is Nothing Or spName.Length = 0 Then
                    Throw New ArgumentNullException("spName")
                End If
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgsqlConnection Parameter should not be null")
            End Try

            Dim commandParameters As Npgsql.NpgsqlParameter()

            ' If we receive parameter values, we need to figure out where they go
            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                commandParameters = SqlHelperParameterCache.GetSpParameterSet(connection, spName)

                ' Assign the provided values to these parameters based on parameter order
                AssignParameterValues(commandParameters, parameterValues)

                ' Call the overload that takes an array of SqlParameters
                Return ExecuteNonQuery(connection, spName, commandParameters)
            Else ' Otherwise we can just call the SP without params
                Return ExecuteNonQuery(connection, spName)
            End If
        End Function
        '**********************************************************************************
        ' Name       : ExecuteNonQuery
        ' Type       : Overloaded Shared Function
        ' Scope      : Public
        ' Returns    : Integer (number of rows affected by the command)
        ' Parameters : ByVal transaction As NpgSqlTransaction, ByVal commandType As CommandType, ByVal commandText As String
        ' Description: Execute a NpgSqlCommand (that returns no resultset and takes no parameters)
        '                against the provided NpgSqlTransaction.
        '**********************************************************************************
        Public Overloads Shared Function ExecuteNonQuery(ByVal transaction As NpgsqlTransaction,
                                                        ByVal commandType As CommandType,
                                                        ByVal commandText As String) As Integer
            ' Pass through the call providing null for the set of SqlParameters
            Return ExecuteNonQuery(transaction, commandType, commandText, CType(Nothing, Npgsql.NpgsqlParameter()))
        End Function

        '**********************************************************************************
        ' Name       : ExecuteNonQuery
        ' Type       : Overloaded Shared Function
        ' Scope      : Public
        ' Returns    : Integer (number of rows affected by the command)
        ' Parameters : ByVal transaction As NpgSqlTransaction, ByVal commandType As CommandType, _
        '               ByVal commandText As String, ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter
        ' Description: Execute a NpgSqlCommand (that returns no resultset) against the specified NpgSqlTransaction
        '**********************************************************************************
        Public Overloads Shared Function ExecuteNonQuery(ByVal transaction As NpgsqlTransaction,
                                                        ByVal commandType As CommandType,
                                                        ByVal commandText As String,
                                                        ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter) As Integer

            Try
                If transaction Is Nothing Then Throw New ArgumentNullException("transaction")
                If Not (transaction Is Nothing) And transaction.Connection Is Nothing Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgsqlConnection Parameter should not be null")
            Catch exArg As ArgumentException
                Throw New Exception("NpgsqlConnection Parameter should not be null")
            End Try

            ' Create a command and prepare it for execution
            Dim cmd As New NpgsqlCommand
            Dim retval As Integer = 0
            Dim mustCloseConnection As Boolean = False

            PrepareCommand(cmd, transaction.Connection, transaction, commandType, commandText, commandParameters, mustCloseConnection)

            Try
                ' Finally, execute the command
                retval = cmd.ExecuteNonQuery()
            Catch exSQL As SqlException
                Throw New Exception(exSQL.Message)

            End Try

            ' Detach the SqlParameters from the command object, so they can be used again
            cmd.Parameters.Clear()

            Return retval
        End Function

        '**********************************************************************************
        ' Name       : ExecuteNonQuery
        ' Type       : Overloaded Shared Function
        ' Scope      : Public
        ' Returns    : Integer (number of rows affected by the command)
        ' Parameters : ByVal transaction As NpgSqlTransaction, ByVal spName As String, _
        '               ByVal ParamArray parameterValues() As Object
        ' Description: Execute a stored procedure via a NpgSqlCommand (that returns no resultset) 
        '               against the specified NpgSqlTransaction 
        '**********************************************************************************
        Public Overloads Shared Function ExecuteNonQuery(ByVal transaction As NpgsqlTransaction,
                                                        ByVal spName As String,
                                                        ByVal ParamArray parameterValues() As Object) As Integer
            Try
                If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
                If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
                If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            Catch exArg As ArgumentException
                Throw New Exception("The transaction already closed.  Start a new transaction.")
            End Try

            Dim commandParameters As Npgsql.NpgsqlParameter()

            ' If we receive parameter values, we need to figure out where they go
            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                commandParameters = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName)

                ' Assign the provided values to these parameters based on parameter order
                AssignParameterValues(commandParameters, parameterValues)

                ' Call the overload that takes an array of SqlParameters
                Return ExecuteNonQuery(transaction, spName, commandParameters)
            Else ' Otherwise we can just call the SP without params
                Return ExecuteNonQuery(transaction, spName)
            End If
        End Function

#End Region

        '**********************************************************************************
        ' Name       : GetConnectionString
        ' Type       : Function
        ' Scope      : Private
        ' Returns    : Object
        ' Parameters : None
        ' Description: Function to get the connection string
        '**********************************************************************************
        Public Shared Function GetConnectionString() As String
            _getConnectionString()
            Return _connString
        End Function
#Region "ExecuteDataset"

        '**********************************************************************************
        ' Name       : ExecuteDataset
        ' Type       : Overloaded Shared Function
        ' Scope      : Public
        ' Returns    : Dataset (resultset generated by the command)
        ' Parameters : ByVal commandType As CommandType, 
        '               ByVal commandText As String
        ' Description: Execute a NpgSqlCommand (that returns a resultset and takes no parameters)   
        '               against the database specified in  the connection string. 
        '**********************************************************************************
        Public Overloads Shared Function ExecuteDataset(ByVal commandType As CommandType,
           ByVal commandText As String) As DataSet
            ' Pass through the call providing null for the set of SqlParameters
            Return ExecuteDataset(commandType, commandText, CType(Nothing, Npgsql.NpgsqlParameter()))
        End Function

        '**********************************************************************************
        ' Name       : ExecuteDataset
        ' Type       : Overloaded Shared Function
        ' Scope      : Public
        ' Returns    : Dataset (resultset generated by the command)
        ' Parameters : ByVal commandType As CommandType, 
        '                ByVal commandText As String, ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter
        ' Description: Execute a NpgSqlCommand (that returns a resultset and takes no parameters)   
        '               against the database specified in  the connection string. 
        '**********************************************************************************
        Public Overloads Shared Function ExecuteDataset(ByVal commandType As CommandType,
           ByVal commandText As String,
           ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter) As DataSet

            Try
                _getConnectionString()
                If (_connString Is Nothing OrElse _connString.Length = 0) Then
                    Throw New ArgumentNullException("connectionString")
                End If
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            End Try

            ' Create & open a NpgsqlConnection, and dispose of it after we are done
            Dim connection As NpgsqlConnection
            Try
                connection = New NpgsqlConnection(_connString)
                connection.Open()

                ' Call the overload that takes a connection in place of the connection string
                Return ExecuteDataset(connection, commandType, commandText, commandParameters)
            Catch exSQL As SqlException
                Throw New Exception(exSQL.Message)
            Catch ex As Exception
                Throw New Exception(ex.Message)
            Finally
                If Not connection Is Nothing Then connection.Dispose()
            End Try
        End Function

        '**********************************************************************************
        ' Name       : ExecuteDataset
        ' Type       : Overloaded Shared Function
        ' Scope      : Public
        ' Returns    : Dataset (resultset generated by the command)
        ' Parameters : ByVal connectionString As String, ByVal spName As String, 
        '              ByVal ParamArray parameterValues() As Object
        ' Description: Execute a stored procedure via a NpgSqlCommand (that returns a resultset) 
        '               against the database specified in the connection string using the provided parameter values
        '               This method will discover the parameters for the stored procedure, and assign the values based on parameter order.
        '**********************************************************************************
        Public Overloads Shared Function ExecuteDataset(ByVal spName As String,
           ByVal ParamArray parameterValues() As Object) As DataSet

            Try
                If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            End Try

            Dim commandParameters As Npgsql.NpgsqlParameter()
            Dim dstRes As DataSet

            ' If we receive parameter values, we need to figure out where they go
            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                commandParameters = SqlHelperParameterCache.GetSpParameterSet(spName)

                ' Assign the provided values to these parameters based on parameter order
                AssignParameterValues(commandParameters, parameterValues)

                ' Call the overload that takes an array of SqlParameters
                dstRes = ExecuteDataset(CommandType.StoredProcedure, spName, commandParameters)
                ReAssignParameterValues(commandParameters, CType(parameterValues, Npgsql.NpgsqlParameter()))
            Else    ' Otherwise we can just call the SP without params
                dstRes = ExecuteDataset(CommandType.StoredProcedure, spName)
            End If
            Return dstRes
        End Function

        '**********************************************************************************
        ' Name       : ExecuteDataset
        ' Type       : Overloaded Shared Function
        ' Scope      : Public
        ' Returns    : Dataset (resultset generated by the command)
        ' Parameters : ByVal connection As NpgsqlConnection, ByVal commandType As CommandType, 
        '               ByVal commandText As String
        ' Description: Execute a NpgSqlCommand (that returns a resultset and takes no parameters) 
        '               against the provided NpgsqlConnection.
        '**********************************************************************************
        Public Overloads Shared Function ExecuteDataset(ByVal connection As NpgsqlConnection,
           ByVal commandType As CommandType,
           ByVal commandText As String) As DataSet

            ' Pass through the call providing null for the set of SqlParameters
            Return ExecuteDataset(connection, commandType, commandText, CType(Nothing, Npgsql.NpgsqlParameter()))
        End Function

        '**********************************************************************************
        ' Name       : ExecuteDataset
        ' Type       : Overloaded Shared Function
        ' Scope      : Public
        ' Returns    : Dataset (resultset generated by the command)
        ' Parameters : ByVal connection As NpgsqlConnection, ByVal commandType As CommandType, 
        '              ByVal commandText As String, ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter
        ' Description: Execute a NpgSqlCommand (that returns a resultset) against the specified NpgsqlConnection
        '               using the provided parameters.
        '**********************************************************************************
        Public Overloads Shared Function ExecuteDataset(ByVal connection As NpgsqlConnection,
           ByVal commandType As CommandType,
           ByVal commandText As String,
           ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter) As DataSet
            Try
                If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            End Try

            ' Create a command and prepare it for execution
            Dim cmd As New NpgsqlCommand
            Dim ds As New DataSet
            Dim dataAdatpter As NpgsqlDataAdapter
            Dim mustCloseConnection As Boolean = False

            PrepareCommand(cmd, connection, CType(Nothing, NpgsqlTransaction), commandType, commandText, commandParameters, mustCloseConnection)

            Try
                ' Create the DataAdapter & DataSet
                dataAdatpter = New NpgsqlDataAdapter(cmd)

                ' Fill the DataSet using default values for DataTable names, etc
                dataAdatpter.Fill(ds)

                ' Detach the SqlParameters from the command object, so they can be used again
                cmd.Parameters.Clear()
            Catch exSQL As SqlException
                Throw New Exception(exSQL.Message)
            Catch ex As Exception
                Throw New Exception(ex.Message)
            Finally
                If (Not dataAdatpter Is Nothing) Then dataAdatpter.Dispose()
            End Try
            If (mustCloseConnection) Then connection.Close()

            ' Return the dataset
            Return ds
        End Function

        '**********************************************************************************
        ' Name       : ExecuteDataset
        ' Type       : Overloaded Shared Function
        ' Scope      : Public
        ' Returns    : Dataset (resultset generated by the command)
        ' Parameters : ByVal connection As NpgsqlConnection, ByVal spName As String, 
        '                    ByVal ParamArray parameterValues() As Object
        ' Description: Execute a stored procedure via a NpgSqlCommand (that returns a resultset) against the specified NpgsqlConnection 
        '              using the provided parameter values.  This method will discover the parameters for the 
        '              stored procedure, and assign the values based on parameter order.
        '**********************************************************************************
        Public Overloads Shared Function ExecuteDataset(ByVal connection As NpgsqlConnection,
   ByVal spName As String,
   ByVal ParamArray parameterValues() As Object) As DataSet

            Try
                If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
                If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            End Try

            Dim commandParameters As Npgsql.NpgsqlParameter()

            ' If we receive parameter values, we need to figure out where they go
            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                commandParameters = SqlHelperParameterCache.GetSpParameterSet(connection, spName)

                ' Assign the provided values to these parameters based on parameter order
                AssignParameterValues(commandParameters, parameterValues)

                ' Call the overload that takes an array of SqlParameters
                Return ExecuteDataset(connection, spName, commandParameters)
            Else    ' Otherwise we can just call the SP without params
                Return ExecuteDataset(connection, spName)
            End If

        End Function
        '**********************************************************************************
        ' Name       : ExecuteDataset
        ' Type       : Overloaded Shared Function
        ' Scope      : Public
        ' Returns    : Dataset (resultset generated by the command)
        ' Parameters : ByVal transaction As NpgSqlTransaction, ByVal commandType As CommandType, _
        '               ByVal commandText As String
        ' Description: Execute a NpgSqlCommand (that returns a resultset and takes no parameters) 
        '                   against the provided NpgSqlTransaction.
        '**********************************************************************************
        Public Overloads Shared Function ExecuteDataset(ByVal transaction As NpgsqlTransaction,
           ByVal commandType As CommandType,
           ByVal commandText As String) As DataSet
            ' Pass through the call providing null for the set of SqlParameters
            Return ExecuteDataset(transaction, commandType, commandText, CType(Nothing, Npgsql.NpgsqlParameter()))
        End Function

        '**********************************************************************************
        ' Name       : ExecuteDataset
        ' Type       : Overloaded Shared Function
        ' Scope      : Public
        ' Returns    : Dataset (resultset generated by the command)
        ' Parameters : ByVal transaction As NpgSqlTransaction, ByVal commandType As CommandType, _
        '               ByVal commandText As String, ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter
        ' Description: Execute a NpgSqlCommand (that returns a resultset) against the specified NpgSqlTransaction
        '               using the provided parameters.
        '**********************************************************************************
        Public Overloads Shared Function ExecuteDataset(ByVal transaction As NpgsqlTransaction,
           ByVal commandType As CommandType,
           ByVal commandText As String,
           ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter) As DataSet
            Try
                If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
                If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            Catch exArg As ArgumentException
                Throw New Exception("The transaction already closed.  Start a new transaction.")
            End Try

            ' Create a command and prepare it for execution
            Dim cmd As New NpgsqlCommand
            Dim ds As New DataSet
            Dim dataAdatpter As NpgsqlDataAdapter
            Dim mustCloseConnection As Boolean = False

            PrepareCommand(cmd, transaction.Connection, transaction, commandType, commandText, commandParameters, mustCloseConnection)

            Try
                ' Create the DataAdapter & DataSet
                dataAdatpter = New NpgsqlDataAdapter(cmd)

                ' Fill the DataSet using default values for DataTable names, etc
                dataAdatpter.Fill(ds)

                ' Detach the SqlParameters from the command object, so they can be used again
                cmd.Parameters.Clear()
            Catch exSQL As SqlException
                Throw New Exception(exSQL.Message)
            Finally
                If (Not dataAdatpter Is Nothing) Then dataAdatpter.Dispose()
            End Try

            ' Return the dataset
            Return ds

        End Function

        '**********************************************************************************
        ' Name       : ExecuteDataset
        ' Type       : Overloaded Shared Function
        ' Scope      : Public
        ' Returns    : Dataset (resultset generated by the command)
        ' Parameters : ByVal transaction As NpgSqlTransaction, ByVal spName As String, _
        '               ByVal ParamArray parameterValues() As Object
        ' Description: Execute a stored procedure via a NpgSqlCommand (that returns a resultset) 
        '               against the specified NpgSqlTransaction using the provided parameter values.  
        '               This method will discover the parameters for the stored procedure, and assign the values based on parameter order.
        '**********************************************************************************
        Public Overloads Shared Function ExecuteDataset(ByVal transaction As NpgsqlTransaction,
           ByVal spName As String,
           ByVal ParamArray parameterValues() As Object) As DataSet

            Try
                If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
                If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
                If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            Catch exArg As ArgumentException
                Throw New Exception("The transaction already closed.  Start a new transaction.")
            End Try

            Dim commandParameters As Npgsql.NpgsqlParameter()

            ' If we receive parameter values, we need to figure out where they go
            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                commandParameters = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName)

                ' Assign the provided values to these parameters based on parameter order
                AssignParameterValues(commandParameters, parameterValues)

                ' Call the overload that takes an array of SqlParameters
                Return ExecuteDataset(transaction, spName, commandParameters)
            Else    ' Otherwise we can just call the SP without params
                Return ExecuteDataset(transaction, spName)
            End If
        End Function

#End Region

#Region "ExecuteReader"
        ' this enum is used to indicate whether the connection was provided by the caller, or created by CSqlHelper, so that
        ' we can set the appropriate CommandBehavior when calling ExecuteReader()
        Private Enum SqlConnectionOwnership
            ' Connection is owned and managed by SqlHelper
            Internal
            ' Connection is owned and managed by the caller
            [External]
        End Enum

        '**********************************************************************************
        ' Name       : ExecuteReader
        ' Type       : Overloaded Shared Function
        ' Scope      : Public
        ' Returns    : NpgSqlDataReader (containing the results of the command)
        ' Parameters : ByVal connection As NpgsqlConnection, ByVal transaction As NpgSqlTransaction,
        '               ByVal commandType As CommandType, ByVal commandText As String,
        '               ByVal commandParameters() As Npgsql.NpgsqlParameter, ByVal connectionOwnership As SqlConnectionOwnership
        ' Description: Create and prepare a NpgSqlCommand, and call ExecuteReader with the appropriate CommandBehavior.  
        '**********************************************************************************
        Private Overloads Shared Function ExecuteReader(ByVal connection As NpgsqlConnection,
                    ByVal transaction As NpgsqlTransaction,
                    ByVal commandType As CommandType,
                    ByVal commandText As String,
                    ByVal commandParameters() As NpgsqlParameter,
                    ByVal connectionOwnership As SqlConnectionOwnership) As NpgsqlDataReader

            Try
                If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            End Try

            Dim mustCloseConnection As Boolean = False
            ' Create a command and prepare it for execution
            Dim cmd As New NpgsqlCommand
            Try
                ' Create a reader
                Dim dataReader As NpgsqlDataReader

                PrepareCommand(cmd, connection, transaction, commandType, commandText, commandParameters, mustCloseConnection)

                ' Call ExecuteReader with the appropriate CommandBehavior
                If connectionOwnership = SqlConnectionOwnership.External Then
                    dataReader = cmd.ExecuteReader()
                Else
                    dataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                End If

                ' Detach the SqlParameters from the command object, so they can be used again
                Dim canClear As Boolean = True
                Dim commandParameter As Npgsql.NpgsqlParameter
                For Each commandParameter In cmd.Parameters
                    If commandParameter.Direction <> ParameterDirection.Input Then
                        canClear = False
                    End If
                Next

                If (canClear) Then cmd.Parameters.Clear()

                Return dataReader
            Catch
                If (mustCloseConnection) Then connection.Close()
                Throw
            End Try
        End Function

        '**********************************************************************************
        ' Name       : ExecuteReader
        ' Type       : Overloaded Shared Function
        ' Scope      : Public
        ' Returns    : NpgSqlDataReader (containing the results of the command)
        ' Parameters : ByVal commandType As CommandType, ByVal commandText As String
        ' Description: Execute a NpgSqlCommand (that returns a resultset and takes no parameters) 
        '               against the database specified in the connection string.
        '**********************************************************************************
        Public Overloads Shared Function ExecuteReader(ByVal commandType As CommandType,
                   ByVal commandText As String) As NpgsqlDataReader
            ' Pass through the call providing null for the set of SqlParameters
            Return ExecuteReader(commandType, commandText, CType(Nothing, Npgsql.NpgsqlParameter()))
        End Function

        '**********************************************************************************
        ' Name       : ExecuteReader
        ' Type       : Overloaded Shared Function
        ' Scope      : Public
        ' Returns    : NpgSqlDataReader (containing the results of the command)
        ' Parameters : ByVal commandType As CommandType, _
        '               ByVal commandText As String, ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter
        ' Description: Execute a NpgSqlCommand (that returns a resultset) 
        '               against the database specified in the connection string using the provided parameters.
        '**********************************************************************************
        Public Overloads Shared Function ExecuteReader(ByVal commandType As CommandType,
                   ByVal commandText As String,
                   ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter) As NpgsqlDataReader
            Try
                _getConnectionString()
                If (_connString Is Nothing OrElse _connString.Length = 0) Then
                    Throw New ArgumentNullException("connectionString")
                End If
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            End Try

            ' Create & open a NpgsqlConnection
            Dim connection As NpgsqlConnection
            Try
                connection = New NpgsqlConnection(_connString)
                connection.Open()
                ' Call the private overload that takes an internally owned connection in place of the connection string
                Return ExecuteReader(connection, CType(Nothing, NpgsqlTransaction), commandType, commandText, commandParameters, SqlConnectionOwnership.Internal)
            Catch
                ' If we fail to return the SqlDatReader, we need to close the connection ourselves
                If Not connection Is Nothing Then connection.Dispose()
                Throw
            End Try
        End Function

        '**********************************************************************************
        ' Name       : ExecuteReader
        ' Type       : Overloaded Shared Function
        ' Scope      : Public
        ' Returns    : NpgSqlDataReader (containing the results of the command)
        ' Parameters : ByVal spName As String, ByVal ParamArray parameterValues() As Object
        ' Description: Execute a stored procedure via a NpgSqlCommand (that returns a resultset) 
        '               against the database specified in the connection string using the provided 
        '               parameter values.  This method will discover the parameters for the 
        '               stored procedure, and assign the values based on parameter order.
        '**********************************************************************************
        Public Overloads Shared Function ExecuteReader(ByVal spName As String,
                   ByVal ParamArray parameterValues() As Object) As NpgsqlDataReader

            Dim commandParameters As Npgsql.NpgsqlParameter()

            ' If we receive parameter values, we need to figure out where they go
            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                commandParameters = SqlHelperParameterCache.GetSpParameterSet(spName)

                ' Assign the provided values to these parameters based on parameter order
                AssignParameterValues(commandParameters, parameterValues)

                ' Call the overload that takes an array of SqlParameters
                Return ExecuteReader(CommandType.StoredProcedure, spName, commandParameters)
                ' Otherwise we can just call the SP without params
            Else
                Return ExecuteReader(CommandType.StoredProcedure, spName)
            End If
        End Function

        '**********************************************************************************
        ' Name       : ExecuteReader
        ' Type       : Overloaded Shared Function
        ' Scope      : Public
        ' Returns    : NpgSqlDataReader (containing the results of the command)
        ' Parameters : ByVal connection As NpgsqlConnection, ByVal commandType As CommandType, 
        '               ByVal commandText As String
        ' Description: Execute a NpgSqlCommand (that returns a resultset and takes no parameters) \
        '               against the provided NpgsqlConnection. 
        '**********************************************************************************
        Public Overloads Shared Function ExecuteReader(ByVal connection As NpgsqlConnection,
                 ByVal commandType As CommandType,
                 ByVal commandText As String) As NpgsqlDataReader

            Return ExecuteReader(connection, commandType, commandText, CType(Nothing, Npgsql.NpgsqlParameter()))

        End Function

        '**********************************************************************************
        ' Name       : ExecuteReader
        ' Type       : Overloaded Shared Function
        ' Scope      : Public
        ' Returns    : NpgSqlDataReader (containing the results of the command)
        ' Parameters : ByVal connection As NpgsqlConnection, ByVal commandType As CommandType, 
        '              ByVal commandText As String, ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter
        ' Description: Execute a NpgSqlCommand (that returns a resultset) 
        '               against the specified NpgsqlConnection using the provided parameters.
        '**********************************************************************************
        Public Overloads Shared Function ExecuteReader(ByVal connection As NpgsqlConnection,
                     ByVal commandType As CommandType,
                     ByVal commandText As String,
                     ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter) As NpgsqlDataReader
            ' Pass through the call to private overload using a null transaction value
            Return ExecuteReader(connection, CType(Nothing, NpgsqlTransaction), commandType, commandText, commandParameters, SqlConnectionOwnership.External)

        End Function

        '**********************************************************************************
        ' Name       : ExecuteReader
        ' Type       : Overloaded Shared Function
        ' Scope      : Public
        ' Returns    : NpgSqlDataReader (containing the results of the command)
        ' Parameters : ByVal connection As NpgsqlConnection, ByVal spName As String,
        '              ByVal ParamArray parameterValues() As Object
        ' Description: Execute a stored procedure via a NpgSqlCommand (that returns a resultset) 
        '               against the specified NpgsqlConnection using the provided parameter values.  
        '               This method will discover the parameters for the 
        '               stored procedure, and assign the values based on parameter order.
        '**********************************************************************************
        Public Overloads Shared Function ExecuteReader(ByVal connection As NpgsqlConnection,
                      ByVal spName As String,
                      ByVal ParamArray parameterValues() As Object) As NpgsqlDataReader
            Try
                If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
                If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            End Try

            Dim commandParameters As Npgsql.NpgsqlParameter()
            ' If we receive parameter values, we need to figure out where they go
            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                commandParameters = SqlHelperParameterCache.GetSpParameterSet(connection, spName)

                AssignParameterValues(commandParameters, parameterValues)

                Return ExecuteReader(connection, spName, commandParameters)
            Else    ' Otherwise we can just call the SP without params
                Return ExecuteReader(connection, spName)
            End If

        End Function

        '**********************************************************************************
        ' Name       : ExecuteReader
        ' Type       : Overloaded Shared Function
        ' Scope      : Public
        ' Returns    : NpgSqlDataReader (containing the results of the command)
        ' Parameters : ByVal transaction As NpgSqlTransaction, ByVal commandType As CommandType, 
        '              ByVal commandText As String
        ' Description: Execute a NpgSqlCommand (that returns a resultset and takes no parameters) 
        '              against the provided NpgSqlTransaction.
        '**********************************************************************************
        Public Overloads Shared Function ExecuteReader(ByVal transaction As NpgsqlTransaction,
                   ByVal commandType As CommandType,
                   ByVal commandText As String) As NpgsqlDataReader
            ' Pass through the call providing null for the set of SqlParameters
            Return ExecuteReader(transaction, commandType, commandText, CType(Nothing, Npgsql.NpgsqlParameter()))
        End Function
        '**********************************************************************************
        ' Name       : ExecuteReader
        ' Type       : Overloaded Shared Function
        ' Scope      : Public
        ' Returns    : NpgSqlDataReader (containing the results of the command)
        ' Parameters : ByVal transaction As NpgSqlTransaction, ByVal commandType As CommandType,
        '               ByVal commandText As String, ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter
        ' Description: Execute a NpgSqlCommand (that returns a resultset) 
        '               against the specified NpgSqlTransaction using the provided parameters.
        '**********************************************************************************
        Public Overloads Shared Function ExecuteReader(ByVal transaction As NpgsqlTransaction,
                   ByVal commandType As CommandType,
                   ByVal commandText As String,
                   ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter) As NpgsqlDataReader
            Try
                If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
                If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            Catch exArg As ArgumentException
                Throw New Exception("The transaction already closed.  Start a new transaction.")
            End Try

            ' Pass through to private overload, indicating that the connection is owned by the caller
            Return ExecuteReader(transaction.Connection, transaction, commandType, commandText, commandParameters, SqlConnectionOwnership.External)
        End Function

        '**********************************************************************************
        ' Name       : ExecuteReader
        ' Type       : Overloaded Shared Function
        ' Scope      : Public
        ' Returns    : NpgSqlDataReader (containing the results of the command)
        ' Parameters : ByVal transaction As NpgSqlTransaction, ByVal spName As String, 
        '              ByVal ParamArray parameterValues() As Object
        ' Description: Execute a stored procedure via a NpgSqlCommand (that returns a resultset) 
        '               against the specified NpgSqlTransaction using the provided parameter values.  
        '               This method will discover the parameters for the stored procedure, 
        '               and assign the values based on parameter order.
        '**********************************************************************************
        Public Overloads Shared Function ExecuteReader(ByVal transaction As NpgsqlTransaction,
                     ByVal spName As String,
                     ByVal ParamArray parameterValues() As Object) As NpgsqlDataReader
            Try
                If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
                If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
                If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            Catch exArg As ArgumentException
                Throw New Exception("The transaction already closed.  Start a new transaction.")
            End Try

            Dim commandParameters As Npgsql.NpgsqlParameter()

            ' If we receive parameter values, we need to figure out where they go
            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                commandParameters = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName)

                AssignParameterValues(commandParameters, parameterValues)

                Return ExecuteReader(transaction, spName, commandParameters)
            Else    ' Otherwise we can just call the SP without params
                Return ExecuteReader(transaction, spName)
            End If
        End Function

#End Region

#Region "ExecuteScalar"

        '**********************************************************************************
        ' Name         : ExecuteScalar
        ' Type         : Function
        ' Scope        : Public
        ' Returns      : Object	
        ' Parameters   : ByVal commandType As CommandType,ByVal commandText As String
        ' Description  : Execute a NpgSqlCommand (that returns a 1x1 resultset and takes no parameters) against the database specified in 
        '               the connection string. 
        '**********************************************************************************
        Public Overloads Shared Function ExecuteScalar(ByVal commandType As CommandType,
          ByVal commandText As String) As Object
            ' Pass through the call providing null for the set of SqlParameters
            Return ExecuteScalar(commandType, commandText, CType(Nothing, Npgsql.NpgsqlParameter()))
        End Function

        '**********************************************************************************
        ' Name         : ExecuteScalar
        ' Type         : Function
        ' Scope        : Public
        ' Returns      : Npgsql.NpgsqlParameter)
        ' Parameters   : ByVal commandType As CommandType,ByVal commandText As String,ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter
        ' Description  : Execute a NpgSqlCommand (that returns a 1x1 resultset) against the database specified in the connection string 
        '               using the provided parameters.
        '**********************************************************************************
        Public Overloads Shared Function ExecuteScalar(ByVal commandType As CommandType,
          ByVal commandText As String,
          ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter) As Object
            Try
                _getConnectionString()
                If (_connString Is Nothing OrElse _connString.Length = 0) Then
                    Throw New ArgumentNullException("connectionString")
                End If
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            End Try

            ' Create & open a NpgsqlConnection, and dispose of it after we are done.
            Dim connection As NpgsqlConnection
            Try
                connection = New NpgsqlConnection(_connString)
                connection.Open()

                ' Call the overload that takes a connection in place of the connection string
                Return ExecuteScalar(connection, commandType, commandText, commandParameters)
            Catch exSQL As SqlException
                Throw New Exception(exSQL.Message)
            Finally
                If Not connection Is Nothing Then connection.Dispose()
            End Try
        End Function

        '**********************************************************************************
        ' Name         : ExecuteScalar
        ' Type         : Function
        ' Scope        : Public
        ' Returns      : Object)
        ' Parameters   : ByVal spName As String,ByVal ParamArray parameterValues() As Object
        ' Description  : Execute a stored procedure via a NpgSqlCommand (that returns a 1x1 resultset) against the database specified in 
        '               the connection string using the provided parameter values.  This method will discover the parameters for the 
        '               stored procedure, and assign the values based on parameter order.
        '               This method provides no access to output parameters or the stored procedure' s return value parameter.
        '**********************************************************************************
        Public Overloads Shared Function ExecuteScalar(ByVal spName As String,
        ByVal ParamArray parameterValues() As Object) As Object
            Try
                If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            End Try

            Dim commandParameters As Npgsql.NpgsqlParameter()

            ' If we receive parameter values, we need to figure out where they go
            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                commandParameters = SqlHelperParameterCache.GetSpParameterSet(spName)

                ' Assign the provided values to these parameters based on parameter order
                AssignParameterValues(commandParameters, parameterValues)

                ' Call the overload that takes an array of SqlParameters
                Return ExecuteScalar(CommandType.StoredProcedure, spName, commandParameters)
                ' Otherwise we can just call the SP without params
            Else
                Return ExecuteScalar(CommandType.StoredProcedure, spName)
            End If
        End Function

        '**********************************************************************************
        ' Name         : ExecuteScalar
        ' Type         : Function
        ' Scope        : Public
        ' Returns      : Object	
        ' Parameters   : ByVal connection As NpgsqlConnection,ByVal commandType As CommandType,
        '                 ByVal commandText As String
        ' Description  : Execute a NpgSqlCommand (that returns a 1x1 resultset and takes no parameters) against the provided NpgsqlConnection. 
        '**********************************************************************************
        Public Overloads Shared Function ExecuteScalar(ByVal connection As NpgsqlConnection,
        ByVal commandType As CommandType,
         ByVal commandText As String) As Object
            ' Pass through the call providing null for the set of SqlParameters
            Return ExecuteScalar(connection, commandType, commandText, CType(Nothing, Npgsql.NpgsqlParameter()))
        End Function


        '**********************************************************************************
        ' Name         : ExecuteScalar
        ' Type         : Function
        ' Scope        : Public
        ' Returns      : Npgsql.NpgsqlParameter)
        ' Parameters   : ByVal connection As NpgsqlConnection,ByVal commandType As CommandType,
        '                ByVal commandText As String,ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter
        ' Description  : Execute a NpgSqlCommand (that returns a 1x1 resultset) against the specified NpgsqlConnection 
        '               using the provided parameters.
        '**********************************************************************************
        Public Overloads Shared Function ExecuteScalar(ByVal connection As NpgsqlConnection,
        ByVal commandType As CommandType,
        ByVal commandText As String,
          ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter) As Object

            Try
                If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            End Try

            ' Create a command and prepare it for execution
            Dim cmd As New NpgsqlCommand
            Dim retval As Object
            Dim mustCloseConnection As Boolean = False

            PrepareCommand(cmd, connection, CType(Nothing, NpgsqlTransaction), commandType, commandText, commandParameters, mustCloseConnection)

            ' Execute the command & return the results
            retval = cmd.ExecuteScalar()

            ' Detach the SqlParameters from the command object, so they can be used again
            cmd.Parameters.Clear()

            If (mustCloseConnection) Then connection.Close()

            Return retval

        End Function


        '**********************************************************************************
        ' Name         : ExecuteScalar
        ' Type         : Function
        ' Scope        : Public
        ' Returns      : Object)
        ' Parameters   : ByVal connection As NpgsqlConnection,ByVal spName As String,
        '                  ByVal ParamArray parameterValues() As Object
        ' Description  : Execute a stored procedure via a NpgSqlCommand (that returns a 1x1 resultset) against the specified NpgsqlConnection 
        '               using the provided parameter values.  This method will discover the parameters for the 
        '               stored procedure, and assign the values based on parameter order.
        '               This method provides no access to output parameters or the stored procedure' s return value parameter.
        '**********************************************************************************
        Public Overloads Shared Function ExecuteScalar(ByVal connection As NpgsqlConnection,
          ByVal spName As String,
          ByVal ParamArray parameterValues() As Object) As Object
            Try
                If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
                If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            End Try

            Dim commandParameters As Npgsql.NpgsqlParameter()

            ' If we receive parameter values, we need to figure out where they go
            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                commandParameters = SqlHelperParameterCache.GetSpParameterSet(connection, spName)

                ' Assign the provided values to these parameters based on parameter order
                AssignParameterValues(commandParameters, parameterValues)

                ' Call the overload that takes an array of SqlParameters
                Return ExecuteScalar(connection, spName, commandParameters)
            Else    ' Otherwise we can just call the SP without params
                Return ExecuteScalar(connection, spName)
            End If

        End Function

        '**********************************************************************************
        ' Name         : ExecuteScalar
        ' Type         : Function
        ' Scope        : Public
        ' Returns      : Object	
        ' Parameters   : ByVal transaction As NpgSqlTransaction,ByVal commandType As CommandType,
        '                  ByVal commandText As String
        ' Description  : Execute a NpgSqlCommand (that returns a 1x1 resultset and takes no parameters) against the provided NpgSqlTransaction.
        '**********************************************************************************
        Public Overloads Shared Function ExecuteScalar(ByVal transaction As NpgsqlTransaction,
          ByVal commandType As CommandType,
          ByVal commandText As String) As Object
            ' Pass through the call providing null for the set of SqlParameters
            Return ExecuteScalar(transaction, commandType, commandText, CType(Nothing, Npgsql.NpgsqlParameter()))
        End Function

        '**********************************************************************************
        ' Name         : ExecuteScalar
        ' Type         : Function
        ' Scope        : Public
        ' Returns      : Npgsql.NpgsqlParameter)
        ' Parameters   : ByVal transaction As NpgSqlTransaction,ByVal commandType As CommandType,
        '                  ByVal commandText As String,ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter
        ' Description  : Execute a NpgSqlCommand (that returns a 1x1 resultset) against the specified NpgSqlTransaction
        '               using the provided parameters.
        '**********************************************************************************
        Public Overloads Shared Function ExecuteScalar(ByVal transaction As NpgsqlTransaction,
          ByVal commandType As CommandType,
          ByVal commandText As String,
          ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter) As Object
            Try
                If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
                If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            Catch exArg As ArgumentException
                Throw New Exception("The transaction already closed.  Start a new transaction.")
            End Try

            ' Create a command and prepare it for execution
            Dim cmd As New NpgsqlCommand
            Dim retval As Object
            Dim mustCloseConnection As Boolean = False

            PrepareCommand(cmd, transaction.Connection, transaction, commandType, commandText, commandParameters, mustCloseConnection)

            ' Execute the command & return the results
            retval = cmd.ExecuteScalar()

            ' Detach the SqlParameters from the command object, so they can be used again
            cmd.Parameters.Clear()

            Return retval
        End Function


        '**********************************************************************************
        ' Name         : ExecuteScalar
        ' Type         : Function
        ' Scope        : Public
        ' Returns      : Object)
        ' Parameters   : ByVal transaction As NpgSqlTransaction,ByVal spName As String,
        '                  ByVal ParamArray parameterValues() As Object
        ' Description  : Execute a stored procedure via a NpgSqlCommand (that returns a 1x1 resultset) against the specified NpgSqlTransaction 
        '               using the provided parameter values.  This method will discover the parameters for the 
        '               stored procedure, and assign the values based on parameter order.
        '               This method provides no access to output parameters or the stored procedure' s return value parameter.
        '**********************************************************************************
        Public Overloads Shared Function ExecuteScalar(ByVal transaction As NpgsqlTransaction,
          ByVal spName As String,
          ByVal ParamArray parameterValues() As Object) As Object
            Try
                If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
                If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
                If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            Catch exArg As ArgumentException
                Throw New Exception("The transaction already closed.  Start a new transaction.")
            End Try

            Dim commandParameters As Npgsql.NpgsqlParameter()
            ' If we receive parameter values, we need to figure out where they go
            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                commandParameters = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName)

                ' Assign the provided values to these parameters based on parameter order
                AssignParameterValues(commandParameters, parameterValues)

                ' Call the overload that takes an array of SqlParameters
                Return ExecuteScalar(transaction, spName, commandParameters)
            Else    ' Otherwise we can just call the SP without params
                Return ExecuteScalar(transaction, spName)
            End If
        End Function

#End Region

#Region "FillDataset"
        '**********************************************************************************
        ' Name         : FillDataset
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal commandType As CommandType,ByVal commandText As String
        '                ByVal dataSet As DataSet,ByVal tableNames() As String
        ' Description  : Execute a NpgSqlCommand (that returns a resultset and takes no parameters) against the database specified in 
        '                   the connection string. 
        '**********************************************************************************
        Public Overloads Shared Sub FillDataset(ByVal commandType As CommandType, ByVal commandText As String, ByVal dataSet As DataSet, ByVal tableNames() As String)

            Try
                _getConnectionString()
                If (_connString Is Nothing OrElse _connString.Length = 0) Then
                    Throw New ArgumentNullException("connectionString")
                End If
                If (dataSet Is Nothing) Then Throw New ArgumentNullException("dataSet")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            End Try

            ' Create & open a NpgsqlConnection, and dispose of it after we are done
            Dim connection As NpgsqlConnection
            Try
                connection = New NpgsqlConnection(_connString)

                connection.Open()

                ' Call the overload that takes a connection in place of the connection string
                FillDataset(connection, commandType, commandText, dataSet, tableNames)
            Finally
                If Not connection Is Nothing Then connection.Dispose()
            End Try
        End Sub

        '**********************************************************************************
        ' Name         : FillDataset
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal commandType As CommandType,ByVal commandText As String
        '                ByVal dataSet As DataSet,ByVal tableNames() As String
        ' Description  : Execute a NpgSqlCommand (that returns a resultset) against the database specified in the connection string 
        '               using the provided parameters.
        '**********************************************************************************
        Public Overloads Shared Sub FillDataset(ByVal commandType As CommandType, ByVal commandText As String, ByVal dataSet As DataSet,
        ByVal tableNames() As String, ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter)

            Try
                _getConnectionString()
                If (_connString Is Nothing OrElse _connString.Length = 0) Then
                    Throw New ArgumentNullException("connectionString")
                End If
                If (dataSet Is Nothing) Then Throw New ArgumentNullException("dataSet")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            End Try

            ' Create & open a NpgsqlConnection, and dispose of it after we are done
            Dim connection As NpgsqlConnection
            Try
                connection = New NpgsqlConnection(_connString)

                connection.Open()

                ' Call the overload that takes a connection in place of the connection string
                FillDataset(connection, commandType, commandText, dataSet, tableNames, commandParameters)
            Finally
                If Not connection Is Nothing Then connection.Dispose()
            End Try
        End Sub


        '**********************************************************************************
        ' Name         : FillDataset
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal spName As String,ByVal dataSet As DataSet
        '                ByVal tableNames As String() As String
        ' Description  : Execute a stored procedure via a NpgSqlCommand (that returns a resultset) against the database specified in 
        '                   the connection string using the provided parameter values.  This method will query the database to discover the parameters for the 
        '               stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.
        '               This method provides no access to output parameters or the stored procedure' s return value parameter.
        '**********************************************************************************
        Public Overloads Shared Sub FillDataset(ByVal spName As String,
          ByVal dataSet As DataSet, ByVal tableNames As String(), ByVal ParamArray parameterValues() As Object)

            Try
                _getConnectionString()
                If (_connString Is Nothing OrElse _connString.Length = 0) Then
                    Throw New ArgumentNullException("connectionString")
                End If
                If (dataSet Is Nothing) Then Throw New ArgumentNullException("dataSet")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            End Try

            ' Create & open a NpgsqlConnection, and dispose of it after we are done
            Dim connection As NpgsqlConnection
            Try
                connection = New NpgsqlConnection(_connString)

                connection.Open()

                ' Call the overload that takes a connection in place of the connection string
                FillDataset(connection, spName, dataSet, tableNames, parameterValues)
            Finally
                If Not connection Is Nothing Then connection.Dispose()
            End Try
        End Sub


        '**********************************************************************************
        ' Name         : FillDataset		
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal connection As NpgsqlConnection,ByVal commandType As CommandType,
        '                yVal commandText As String,ByVal dataSet As DataSet,
        '                ByVal tableNames As String() As String
        ' Description  : Execute a NpgSqlCommand (that returns a resultset and takes no parameters) against the provided NpgsqlConnection. 
        '**********************************************************************************
        Public Overloads Shared Sub FillDataset(ByVal connection As NpgsqlConnection, ByVal commandType As CommandType,
          ByVal commandText As String, ByVal dataSet As DataSet, ByVal tableNames As String())

            FillDataset(connection, commandType, commandText, dataSet, tableNames, Nothing)

        End Sub

        '**********************************************************************************
        ' Name         : FillDataset
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal connection As NpgsqlConnection,ByVal commandType As CommandType,
        '                yVal commandText As String,ByVal dataSet As DataSet,
        '                ByVal tableNames As String() As String
        ' Description  : Execute a NpgSqlCommand (that returns a resultset) against the specified NpgsqlConnection 
        '**********************************************************************************
        Public Overloads Shared Sub FillDataset(ByVal connection As NpgsqlConnection, ByVal commandType As CommandType,
          ByVal commandText As String, ByVal dataSet As DataSet, ByVal tableNames As String(),
         ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter)

            FillDataset(connection, Nothing, commandType, commandText, dataSet, tableNames, commandParameters)

        End Sub


        '**********************************************************************************
        ' Name         : FillDataset
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal connection As NpgsqlConnection,ByVal spName As String,
        '                ByVal dataSet As DataSet,ByVal tableNames() As String
        ' Description  : Execute a stored procedure via a NpgSqlCommand (that returns a resultset) against the specified NpgsqlConnection 
        '               using the provided parameter values.  This method will query the database to discover the parameters for the 
        '               stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.
        '               This method provides no access to output parameters or the stored procedure' s return value parameter.
        '**********************************************************************************
        Public Overloads Shared Sub FillDataset(ByVal connection As NpgsqlConnection, ByVal spName As String, ByVal dataSet As DataSet,
          ByVal tableNames() As String, ByVal ParamArray parameterValues() As Object)

            Try
                If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
                If (dataSet Is Nothing) Then Throw New ArgumentNullException("dataSet")
                If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            End Try

            ' If we receive parameter values, we need to figure out where they go
            If Not parameterValues Is Nothing AndAlso parameterValues.Length > 0 Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                Dim commandParameters() As Npgsql.NpgsqlParameter = SqlHelperParameterCache.GetSpParameterSet(connection, spName)

                ' Assign the provided values to these parameters based on parameter order
                AssignParameterValues(commandParameters, parameterValues)

                ' Call the overload that takes an array of SqlParameters
                FillDataset(connection, spName, dataSet, tableNames, commandParameters)
            Else    ' Otherwise we can just call the SP without params
                FillDataset(connection, spName, dataSet, tableNames)
            End If

        End Sub

        '**********************************************************************************
        ' Name         : FillDataset
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal transaction As NpgSqlTransaction,ByVal commandType As CommandType,
        '                yVal commandText As String,ByVal dataSet As DataSet,
        '                ByVal tableNames() As String
        ' Description  : Execute a NpgSqlCommand (that returns a resultset and takes no parameters) against the provided NpgSqlTransaction. 
        '**********************************************************************************
        Public Overloads Shared Sub FillDataset(ByVal transaction As NpgsqlTransaction, ByVal commandType As CommandType,
          ByVal commandText As String, ByVal dataSet As DataSet, ByVal tableNames() As String)

            FillDataset(transaction, commandType, commandText, dataSet, tableNames, Nothing)
        End Sub


        '**********************************************************************************
        ' Name         : FillDataset
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal transaction As NpgSqlTransaction,ByVal commandType As CommandType,
        '                yVal commandText As String,ByVal dataSet As DataSet,
        '                ByVal tableNames() As String
        ' Description  : Execute a NpgSqlCommand (that returns a resultset) against the specified NpgSqlTransaction
        '               using the provided parameters.
        '**********************************************************************************
        Public Overloads Shared Sub FillDataset(ByVal transaction As NpgsqlTransaction, ByVal commandType As CommandType,
          ByVal commandText As String, ByVal dataSet As DataSet, ByVal tableNames() As String,
         ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter)

            Try
                If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
                If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            Catch exArg As ArgumentException
                Throw New Exception("The transaction already closed.  Start a new transaction.")
            End Try


            FillDataset(transaction.Connection, transaction, commandType, commandText, dataSet, tableNames, commandParameters)

        End Sub

        '**********************************************************************************
        ' Name         : FillDataset
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal transaction As NpgSqlTransaction,ByVal spName As String,
        '                yVal dataSet As DataSet,ByVal tableNames() As String
        ' Description  : Execute a stored procedure via a NpgSqlCommand (that returns a resultset) against the specified 
        '               NpgSqlTransaction using the provided parameter values.  This method will query the database to discover the parameters for the 
        '               stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.
        '               This method provides no access to output parameters or the stored procedure' s return value parameter.
        '**********************************************************************************
        Public Overloads Shared Sub FillDataset(ByVal transaction As NpgsqlTransaction, ByVal spName As String,
          ByVal dataSet As DataSet, ByVal tableNames() As String, ByVal ParamArray parameterValues() As Object)

            Try
                If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
                If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
                If (dataSet Is Nothing) Then Throw New ArgumentNullException("dataSet")
                If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            Catch exArg As ArgumentException
                Throw New Exception("The transaction already closed.  Start a new transaction.")
            End Try

            ' If we receive parameter values, we need to figure out where they go
            If Not parameterValues Is Nothing AndAlso parameterValues.Length > 0 Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                Dim commandParameters() As Npgsql.NpgsqlParameter = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName)

                ' Assign the provided values to these parameters based on parameter order
                AssignParameterValues(commandParameters, parameterValues)

                ' Call the overload that takes an array of SqlParameters
                FillDataset(transaction, spName, dataSet, tableNames, commandParameters)
            Else    ' Otherwise we can just call the SP without params
                FillDataset(transaction, spName, dataSet, tableNames)
            End If
        End Sub


        '**********************************************************************************
        ' Name         : FillDataset
        ' Type         : Sub
        ' Scope        : Private
        ' Returns      : N/A
        ' Parameters   : ByVal connection As NpgsqlConnection,ByVal transaction As NpgSqlTransaction,
        '                ByVal commandType As CommandType,ByVal commandText As String,
        '                ByVal dataSet As DataSet,ByVal tableNames() As String,
        '               ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter
        ' Description  : Private helper method that execute a NpgSqlCommand (that returns a resultset) against the specified NpgSqlTransaction and NpgsqlConnection
        '               using the provided parameters.
        '**********************************************************************************
        Private Overloads Shared Sub FillDataset(ByVal connection As NpgsqlConnection, ByVal transaction As NpgsqlTransaction, ByVal commandType As CommandType,
          ByVal commandText As String, ByVal dataSet As DataSet, ByVal tableNames() As String,
         ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter)

            Try
                If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
                If (dataSet Is Nothing) Then Throw New ArgumentNullException("dataSet")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            End Try

            ' Create a command and prepare it for execution
            Dim command As New NpgsqlCommand

            Dim mustCloseConnection As Boolean = False
            PrepareCommand(command, connection, transaction, commandType, commandText, commandParameters, mustCloseConnection)

            ' Create the DataAdapter & DataSet
            Dim dataAdapter As NpgsqlDataAdapter = New NpgsqlDataAdapter(command)

            Try
                ' Add the table mappings specified by the user
                If Not tableNames Is Nothing AndAlso tableNames.Length > 0 Then

                    Dim tableName As String = "Table"
                    Dim index As Integer

                    For index = 0 To tableNames.Length - 1
                        If (tableNames(index) Is Nothing OrElse tableNames(index).Length = 0) Then Throw New ArgumentException("The tableNames parameter must contain a list of tables, a value was provided as null or empty string.", "tableNames")
                        dataAdapter.TableMappings.Add(tableName, tableNames(index))
                        tableName = tableName & (index + 1).ToString()
                    Next
                End If

                ' Fill the DataSet using default values for DataTable names, etc
                dataAdapter.Fill(dataSet)

                ' Detach the SqlParameters from the command object, so they can be used again
                command.Parameters.Clear()
            Finally
                If (Not dataAdapter Is Nothing) Then dataAdapter.Dispose()
            End Try

            If (mustCloseConnection) Then connection.Close()

        End Sub
#End Region

#Region "UpdateDataset"
        '**********************************************************************************
        ' Name         : UpdateDataset
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal insertCommand As NpgSqlCommand,ByVal deleteCommand As NpgSqlCommand,
        '                ByVal updateCommand As NpgSqlCommand,ByVal dataSet As DataSet,
        '                ByVal tableName As String
        ' Description  : Executes the respective command for each inserted, updated, or deleted row in the DataSet.
        '**********************************************************************************
        Public Overloads Shared Sub UpdateDataset(ByVal insertCommand As NpgsqlCommand, ByVal deleteCommand As NpgsqlCommand, ByVal updateCommand As NpgsqlCommand, ByVal dataSet As DataSet, ByVal tableName As String)

            Try
                If (insertCommand Is Nothing) Then Throw New ArgumentNullException("insertCommand")
                If (deleteCommand Is Nothing) Then Throw New ArgumentNullException("deleteCommand")
                If (updateCommand Is Nothing) Then Throw New ArgumentNullException("updateCommand")
                If (dataSet Is Nothing) Then Throw New ArgumentNullException("dataSet")
                If (tableName Is Nothing OrElse tableName.Length = 0) Then Throw New ArgumentNullException("tableName")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            End Try

            ' Create a NpgSqlDataAdapter, and dispose of it after we are done
            Dim dataAdapter As New NpgsqlDataAdapter
            Try
                ' Set the data adapter commands
                dataAdapter.UpdateCommand = updateCommand
                dataAdapter.InsertCommand = insertCommand
                dataAdapter.DeleteCommand = deleteCommand

                Try
                    ' Update the dataset changes in the data source
                    dataAdapter.Update(dataSet, tableName)
                Catch exDB As DBConcurrencyException
                    Throw New Exception("An attempt to execute an INSERT, UPDATE, or DELETE statement resulted in zero records affected.")
                Catch exSystem As SystemException
                    Throw New Exception("A source table could not be found.")
                End Try

                ' Commit all the changes made to the DataSet
                dataSet.AcceptChanges()
            Finally
                If (Not dataAdapter Is Nothing) Then dataAdapter.Dispose()
            End Try
        End Sub
#End Region

#Region "CreateCommand"

        Public Overloads Shared Function CreateCommand(ByVal connection As NpgsqlConnection, ByVal spName As String, ByVal ParamArray sourceColumns() As String) As NpgsqlCommand

            Try
                If connection Is Nothing Then Throw New ArgumentNullException("connection")
                If spName Is Nothing Or spName.Length = 0 Then Throw New ArgumentNullException("spName")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            End Try

            ' Create a NpgSqlCommand
            Dim cmd As New NpgsqlCommand(spName, connection)
            cmd.CommandType = CommandType.StoredProcedure

            ' If we receive parameter values, we need to figure out where they go
            If Not sourceColumns Is Nothing AndAlso sourceColumns.Length > 0 Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                Dim commandParameters() As Npgsql.NpgsqlParameter = SqlHelperParameterCache.GetSpParameterSet(connection, spName)

                ' Assign the provided source columns to these parameters based on parameter order
                Dim index As Integer
                For index = 0 To sourceColumns.Length - 1
                    commandParameters(index).SourceColumn = sourceColumns(index)
                Next

                ' Attach the discovered parameters to the NpgSqlCommand object
                AttachParameters(cmd, commandParameters)
            End If

            CreateCommand = cmd
        End Function
#End Region

#Region "ExecuteNonQueryTypedParams"
        '**********************************************************************************
        ' Name         : ExecuteNonQueryTypedParams
        ' Type         : Function
        ' Scope        : Public
        ' Returns      : Integer
        ' Parameters   : ByVal spName As String,ByVal dataRow As DataRow
        ' Description  : Execute a stored procedure via a NpgSqlCommand (that returns no resultset) against the database specified in 
        '               the connection string using the dataRow column values as the stored procedure' s parameters values.
        '               This method will query the database to discover the parameters for the 
        '               stored procedure (the first time each stored procedure is called), and assign the values based on row values.
        '**********************************************************************************
        Public Overloads Shared Function ExecuteNonQueryTypedParams(ByVal spName As String, ByVal dataRow As DataRow) As Integer

            Try
                If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            End Try

            ' If the row has values, the store procedure parameters must be initialized
            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                Dim commandParameters() As Npgsql.NpgsqlParameter = SqlHelperParameterCache.GetSpParameterSet(spName)

                ' Set the parameters values
                AssignParameterValues(commandParameters, dataRow)

                ExecuteNonQueryTypedParams = CSqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, spName, commandParameters)
            Else
                ExecuteNonQueryTypedParams = CSqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, spName)
            End If
        End Function


        '**********************************************************************************
        ' Name         : ExecuteNonQueryTypedParams
        ' Type         : Function
        ' Scope        : Public
        ' Returns      : Integer
        ' Parameters   : ByVal connection As NpgsqlConnection,ByVal spName As String,
        '                ByVal dataRow As DataRow
        ' Description  : Execute a stored procedure via a NpgSqlCommand (that returns no resultset) against the specified NpgsqlConnection 
        '               using the dataRow column values as the stored procedure' s parameters values.  
        '               This method will query the database to discover the parameters for the 
        '               stored procedure (the first time each stored procedure is called), and assign the values based on row values.
        '**********************************************************************************
        Public Overloads Shared Function ExecuteNonQueryTypedParams(ByVal connection As NpgsqlConnection, ByVal spName As String, ByVal dataRow As DataRow) As Integer

            Try
                If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
                If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            End Try

            ' If the row has values, the store procedure parameters must be initialized
            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                Dim commandParameters() As Npgsql.NpgsqlParameter = SqlHelperParameterCache.GetSpParameterSet(connection, spName)

                ' Set the parameters values
                AssignParameterValues(commandParameters, dataRow)

                ExecuteNonQueryTypedParams = CSqlHelper.ExecuteNonQuery(connection, spName, commandParameters)
            Else
                ExecuteNonQueryTypedParams = CSqlHelper.ExecuteNonQuery(connection, spName)
            End If
        End Function


        '**********************************************************************************
        ' Name         : ExecuteNonQueryTypedParams
        ' Type         : Function
        ' Scope        : Public
        ' Returns      : Integer
        ' Parameters   : ByVal transaction As NpgSqlTransaction,ByVal spName As String,
        '                ByVal dataRow As DataRow
        ' Description  : Execute a stored procedure via a NpgSqlCommand (that returns no resultset) against the specified
        '               NpgSqlTransaction using the dataRow column values as the stored procedure' s parameters values.
        '               This method will query the database to discover the parameters for the 
        '               stored procedure (the first time each stored procedure is called), and assign the values based on row values.
        '**********************************************************************************
        Public Overloads Shared Function ExecuteNonQueryTypedParams(ByVal transaction As NpgsqlTransaction, ByVal spName As String, ByVal dataRow As DataRow) As Integer

            Try
                If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
                If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
                If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            Catch exArg As ArgumentException
                Throw New Exception("The transaction already closed.  Start a new transaction.")
            End Try

            ' If the row has values, the store procedure parameters must be initialized
            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                Dim commandParameters() As Npgsql.NpgsqlParameter = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName)

                ' Set the parameters values
                AssignParameterValues(commandParameters, dataRow)

                ExecuteNonQueryTypedParams = CSqlHelper.ExecuteNonQuery(transaction, spName, commandParameters)
            Else

                ExecuteNonQueryTypedParams = CSqlHelper.ExecuteNonQuery(transaction, spName)
            End If
        End Function
#End Region

#Region "ExecuteDatasetTypedParams"
        ' Execute a stored procedure via a NpgSqlCommand (that returns a resultset) against the database specified in 
        ' the connection string using the dataRow column values as the stored procedure' s parameters values.
        ' This method will query the database to discover the parameters for the 
        ' stored procedure (the first time each stored procedure is called), and assign the values based on row values.
        ' Parameters:
        ' -connectionString: A valid connection string for a NpgsqlConnection
        ' -spName: the name of the stored procedure
        ' -dataRow: The dataRow used to hold the stored procedure' s parameter values.
        ' Returns:
        ' a dataset containing the resultset generated by the command
        Public Overloads Shared Function ExecuteDatasetTypedParams(ByVal spName As String, ByVal dataRow As DataRow) As DataSet

            Try
                If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            End Try

            ' If the row has values, the store procedure parameters must be initialized
            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                Dim commandParameters() As Npgsql.NpgsqlParameter = SqlHelperParameterCache.GetSpParameterSet(spName)

                ' Set the parameters values
                AssignParameterValues(commandParameters, dataRow)

                ExecuteDatasetTypedParams = CSqlHelper.ExecuteDataset(CommandType.StoredProcedure, spName, commandParameters)
            Else

                ExecuteDatasetTypedParams = CSqlHelper.ExecuteDataset(CommandType.StoredProcedure, spName)
            End If
        End Function

        ' Execute a stored procedure via a NpgSqlCommand (that returns a resultset) against the specified NpgsqlConnection 
        ' using the dataRow column values as the store procedure' s parameters values.
        ' This method will query the database to discover the parameters for the 
        ' stored procedure (the first time each stored procedure is called), and assign the values based on row values.
        ' Parameters:
        ' -connection: A valid NpgsqlConnection object
        ' -spName: the name of the stored procedure
        ' -dataRow: The dataRow used to hold the stored procedure' s parameter values.
        ' Returns:
        ' a dataset containing the resultset generated by the command
        Public Overloads Shared Function ExecuteDatasetTypedParams(ByVal connection As NpgsqlConnection, ByVal spName As String, ByVal dataRow As DataRow) As DataSet

            Try
                If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
                If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            End Try

            ' If the row has values, the store procedure parameters must be initialized
            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                Dim commandParameters() As Npgsql.NpgsqlParameter = SqlHelperParameterCache.GetSpParameterSet(connection, spName)

                ' Set the parameters values
                AssignParameterValues(commandParameters, dataRow)

                ExecuteDatasetTypedParams = CSqlHelper.ExecuteDataset(connection, spName, commandParameters)
            Else

                ExecuteDatasetTypedParams = CSqlHelper.ExecuteDataset(connection, spName)
            End If
        End Function

        ' Execute a stored procedure via a NpgSqlCommand (that returns a resultset) against the specified NpgSqlTransaction 
        ' using the dataRow column values as the stored procedure' s parameters values.
        ' This method will query the database to discover the parameters for the 
        ' stored procedure (the first time each stored procedure is called), and assign the values based on row values.
        ' Parameters:
        ' -transaction: A valid NpgSqlTransaction object
        ' -spName: the name of the stored procedure
        ' -dataRow: The dataRow used to hold the stored procedure' s parameter values.
        ' Returns:
        ' a dataset containing the resultset generated by the command
        Public Overloads Shared Function ExecuteDatasetTypedParams(ByVal transaction As NpgsqlTransaction, ByVal spName As String, ByVal dataRow As DataRow) As DataSet

            Try
                If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
                If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
                If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            Catch exArg As ArgumentException
                Throw New Exception("The transaction already closed.  Start a new transaction.")
            End Try

            ' If the row has values, the store procedure parameters must be initialized
            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                Dim commandParameters() As Npgsql.NpgsqlParameter = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName)

                ' Set the parameters values
                AssignParameterValues(commandParameters, dataRow)

                ExecuteDatasetTypedParams = CSqlHelper.ExecuteDataset(transaction, spName, commandParameters)
            Else

                ExecuteDatasetTypedParams = CSqlHelper.ExecuteDataset(transaction, spName)
            End If
        End Function
#End Region

#Region "ExecuteReaderTypedParams"
        ' Execute a stored procedure via a NpgSqlCommand (that returns a resultset) against the database specified in 
        ' the connection string using the dataRow column values as the stored procedure' s parameters values.
        ' This method will query the database to discover the parameters for the 
        ' stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.
        ' Parameters:
        ' -connectionString: A valid connection string for a NpgsqlConnection
        ' -spName: the name of the stored procedure
        ' -dataRow: The dataRow used to hold the stored procedure' s parameter values.
        ' Returns:
        ' a NpgSqlDataReader containing the resultset generated by the command
        Public Overloads Shared Function ExecuteReaderTypedParams(ByVal spName As String, ByVal dataRow As DataRow) As NpgsqlDataReader


            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")


            ' If the row has values, the store procedure parameters must be initialized
            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                Dim commandParameters() As Npgsql.NpgsqlParameter = SqlHelperParameterCache.GetSpParameterSet(spName)

                ' Set the parameters values
                AssignParameterValues(commandParameters, dataRow)

                ExecuteReaderTypedParams = CSqlHelper.ExecuteReader(CommandType.StoredProcedure, spName, commandParameters)
            Else
                ExecuteReaderTypedParams = CSqlHelper.ExecuteReader(CommandType.StoredProcedure, spName)
            End If
        End Function

        ' Execute a stored procedure via a NpgSqlCommand (that returns a resultset) against the specified NpgsqlConnection 
        ' using the dataRow column values as the stored procedure' s parameters values.
        ' This method will query the database to discover the parameters for the 
        ' stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.
        ' Parameters:
        ' -connection: A valid NpgsqlConnection object
        ' -spName: The name of the stored procedure
        ' -dataRow: The dataRow used to hold the stored procedure' s parameter values.
        ' Returns:
        ' a NpgSqlDataReader containing the resultset generated by the command
        Public Overloads Shared Function ExecuteReaderTypedParams(ByVal connection As NpgsqlConnection, ByVal spName As String, ByVal dataRow As DataRow) As NpgsqlDataReader

            Try
                If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
                If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            End Try

            ' If the row has values, the store procedure parameters must be initialized
            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                Dim commandParameters() As Npgsql.NpgsqlParameter = SqlHelperParameterCache.GetSpParameterSet(connection, spName)

                ' Set the parameters values
                AssignParameterValues(commandParameters, dataRow)

                ExecuteReaderTypedParams = CSqlHelper.ExecuteReader(connection, spName, commandParameters)
            Else
                ExecuteReaderTypedParams = CSqlHelper.ExecuteReader(connection, spName)
            End If
        End Function

        ' Execute a stored procedure via a NpgSqlCommand (that returns a resultset) against the specified NpgSqlTransaction 
        ' using the dataRow column values as the stored procedure' s parameters values.
        ' This method will query the database to discover the parameters for the 
        ' stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.
        ' Parameters:
        ' -transaction: A valid NpgSqlTransaction object
        ' -spName" The name of the stored procedure
        ' -dataRow: The dataRow used to hold the stored procedure' s parameter values.
        ' Returns:
        ' a NpgSqlDataReader containing the resultset generated by the command
        Public Overloads Shared Function ExecuteReaderTypedParams(ByVal transaction As NpgsqlTransaction, ByVal spName As String, ByVal dataRow As DataRow) As NpgsqlDataReader

            Try
                If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
                If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
                If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            Catch exArg As ArgumentException
                Throw New Exception("The transaction already closed.  Start a new transaction.")
            End Try

            ' If the row has values, the store procedure parameters must be initialized
            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                Dim commandParameters() As Npgsql.NpgsqlParameter = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName)

                ' Set the parameters values
                AssignParameterValues(commandParameters, dataRow)

                ExecuteReaderTypedParams = CSqlHelper.ExecuteReader(transaction, spName, commandParameters)
            Else
                ExecuteReaderTypedParams = CSqlHelper.ExecuteReader(transaction, spName)
            End If
        End Function
#End Region

#Region "ExecuteScalarTypedParams"
        ' Execute a stored procedure via a NpgSqlCommand (that returns a 1x1 resultset) against the database specified in 
        ' the connection string using the dataRow column values as the stored procedure' s parameters values.
        ' This method will query the database to discover the parameters for the 
        ' stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.
        ' Parameters:
        ' -connectionString: A valid connection string for a NpgsqlConnection
        ' -spName: The name of the stored procedure
        ' -dataRow: The dataRow used to hold the stored procedure' s parameter values.
        ' Returns:
        ' An object containing the value in the 1x1 resultset generated by the command</returns>
        Public Overloads Shared Function ExecuteScalarTypedParams(ByVal spName As String, ByVal dataRow As DataRow) As Object


            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

            ' If the row has values, the store procedure parameters must be initialized
            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                Dim commandParameters() As Npgsql.NpgsqlParameter = SqlHelperParameterCache.GetSpParameterSet(spName)

                ' Set the parameters values
                AssignParameterValues(commandParameters, dataRow)

                ExecuteScalarTypedParams = CSqlHelper.ExecuteScalar(CommandType.StoredProcedure, spName, commandParameters)
            Else
                ExecuteScalarTypedParams = CSqlHelper.ExecuteScalar(CommandType.StoredProcedure, spName)
            End If
        End Function

        ' Execute a stored procedure via a NpgSqlCommand (that returns a 1x1 resultset) against the specified NpgsqlConnection 
        ' using the dataRow column values as the stored procedure' s parameters values.
        ' This method will query the database to discover the parameters for the 
        ' stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.
        ' Parameters:
        ' -connection: A valid NpgsqlConnection object
        ' -spName: the name of the stored procedure
        ' -dataRow: The dataRow used to hold the stored procedure' s parameter values.
        ' Returns: 
        ' an object containing the value in the 1x1 resultset generated by the command</returns>
        Public Overloads Shared Function ExecuteScalarTypedParams(ByVal connection As NpgsqlConnection, ByVal spName As String, ByVal dataRow As DataRow) As Object

            Try
                If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
                If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            End Try

            ' If the row has values, the store procedure parameters must be initialized
            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                Dim commandParameters() As Npgsql.NpgsqlParameter = SqlHelperParameterCache.GetSpParameterSet(connection, spName)

                ' Set the parameters values
                AssignParameterValues(commandParameters, dataRow)

                ExecuteScalarTypedParams = CSqlHelper.ExecuteScalar(connection, spName, commandParameters)
            Else
                ExecuteScalarTypedParams = CSqlHelper.ExecuteScalar(connection, spName)
            End If
        End Function

        ' Execute a stored procedure via a NpgSqlCommand (that returns a 1x1 resultset) against the specified NpgSqlTransaction
        ' using the dataRow column values as the stored procedure' s parameters values.
        ' This method will query the database to discover the parameters for the 
        ' stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.
        ' Parameters:
        ' -transaction: A valid NpgSqlTransaction object
        ' -spName: the name of the stored procedure
        ' -dataRow: The dataRow used to hold the stored procedure' s parameter values.
        ' Returns: 
        ' an object containing the value in the 1x1 resultset generated by the command</returns>
        Public Overloads Shared Function ExecuteScalarTypedParams(ByVal transaction As NpgsqlTransaction, ByVal spName As String, ByVal dataRow As DataRow) As Object

            Try
                If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
                If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
                If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            Catch exArg As ArgumentException
                Throw New Exception("The transaction already closed.  Start a new transaction.")
            End Try

            ' If the row has values, the store procedure parameters must be initialized
            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                Dim commandParameters() As Npgsql.NpgsqlParameter = SqlHelperParameterCache.GetSpParameterSet(transaction.Connection, spName)

                ' Set the parameters values
                AssignParameterValues(commandParameters, dataRow)

                ExecuteScalarTypedParams = CSqlHelper.ExecuteScalar(transaction, spName, commandParameters)
            Else
                ExecuteScalarTypedParams = CSqlHelper.ExecuteScalar(transaction, spName)
            End If
        End Function
#End Region


    End Class ' CSqlHelper

    ' SqlHelperParameterCache provides functions to leverage a static cache of procedure parameters, and the
    ' ability to discover parameters for stored procedures at run-time.
    Public NotInheritable Class SqlHelperParameterCache

#Region "private methods, variables, and constructors"


        ' Since this class provides only static methods, make the default constructor private to prevent 
        ' instances from being created with "new SqlHelperParameterCache()".
        Private Sub New()
        End Sub ' New 

        Private Shared paramCache As Hashtable = Hashtable.Synchronized(New Hashtable)

        ' resolve at run time the appropriate set of SqlParameters for a stored procedure
        ' Parameters:
        ' - connectionString - a valid connection string for a NpgsqlConnection
        ' - spName - the name of the stored procedure
        ' - includeReturnValueParameter - whether or not to include their return value parameter>
        ' Returns: Npgsql.NpgsqlParameter()
        Private Shared Function DiscoverSpParameterSet(ByVal connection As NpgsqlConnection,
                                                        ByVal spName As String,
                                                        ByVal includeReturnValueParameter As Boolean,
                                                        ByVal ParamArray parameterValues() As Object) As Npgsql.NpgsqlParameter()

            Try
                If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
                If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            End Try

            Dim cmd As New NpgsqlCommand(spName, connection)
            cmd.CommandType = CommandType.StoredProcedure
            Dim discoveredParameters() As Npgsql.NpgsqlParameter

            connection.Open()

            Try
                NpgsqlCommandBuilder.DeriveParameters(cmd)
            Catch ex As DataException
                Dim strMessage As String = ex.Message
                connection.Close()
                Throw
            Catch ex As SqlException
                Dim strMessage As String = ex.Message
                connection.Close()
                Throw
            Catch ex As Exception
                Dim strMessage As String = ex.Message
                connection.Close()
                Throw
            End Try

            connection.Close()

            If Not includeReturnValueParameter Then
                cmd.Parameters.RemoveAt(0)
            End If

            discoveredParameters = New Npgsql.NpgsqlParameter(cmd.Parameters.Count - 1) {}
            cmd.Parameters.CopyTo(discoveredParameters, 0)

            ' Init the parameters with a DBNull value
            Dim discoveredParameter As Npgsql.NpgsqlParameter
            For Each discoveredParameter In discoveredParameters
                discoveredParameter.Value = DBNull.Value
            Next

            Return discoveredParameters

        End Function ' DiscoverSpParameterSet

        ' Deep copy of cached Npgsql.NpgsqlParameter array
        Private Shared Function CloneParameters(ByVal originalParameters() As Npgsql.NpgsqlParameter) As Npgsql.NpgsqlParameter()

            Dim i As Integer
            Dim j As Integer = originalParameters.Length - 1
            Dim clonedParameters(j) As Npgsql.NpgsqlParameter

            For i = 0 To j
                clonedParameters(i) = CType(CType(originalParameters(i), ICloneable).Clone, Npgsql.NpgsqlParameter)
            Next

            Return clonedParameters
        End Function ' CloneParameters

#End Region

#Region "caching functions"

        ' add parameter array to the cache
        ' Parameters
        ' -connectionString - a valid connection string for a NpgsqlConnection 
        ' -commandText - the stored procedure name or T-SQL command 
        ' -commandParameters - an array of SqlParamters to be cached 
        Public Shared Sub CacheParameterSet(ByVal commandText As String,
                                            ByVal ParamArray commandParameters() As Npgsql.NpgsqlParameter)
            Dim connectionString As String
            Try
                connectionString = CSqlHelper.GetConnectionString
                If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
                If (commandText Is Nothing OrElse commandText.Length = 0) Then Throw New ArgumentNullException("commandText")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")

            End Try

            Dim hashKey As String = connectionString + ":" + commandText

            paramCache(hashKey) = commandParameters
        End Sub ' CacheParameterSet

        ' retrieve a parameter array from the cache
        ' Parameters:
        ' -connectionString - a valid connection string for a NpgsqlConnection 
        ' -commandText - the stored procedure name or T-SQL command 
        ' Returns: An array of SqlParamters 
        Public Shared Function GetCachedParameterSet(ByVal commandText As String) As Npgsql.NpgsqlParameter()
            Dim connectionString As String
            Try
                connectionString = CSqlHelper.GetConnectionString()
                If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
                If (commandText Is Nothing OrElse commandText.Length = 0) Then Throw New ArgumentNullException("commandText")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            End Try

            Dim hashKey As String = connectionString + ":" + commandText
            Dim cachedParameters As Npgsql.NpgsqlParameter() = CType(paramCache(hashKey), Npgsql.NpgsqlParameter())

            If cachedParameters Is Nothing Then
                Return Nothing
            Else
                Return CloneParameters(cachedParameters)
            End If
        End Function ' GetCachedParameterSet

#End Region

#Region "Parameter Discovery Functions"
        ' Retrieves the set of SqlParameters appropriate for the stored procedure.
        ' This method will query the database for this information, and then store it in a cache for future requests.
        ' Parameters:
        ' -connectionString - a valid connection string for a NpgsqlConnection 
        ' -spName - the name of the stored procedure 
        ' Returns: An array of SqlParameters
        Public Overloads Shared Function GetSpParameterSet(ByVal spName As String) As Npgsql.NpgsqlParameter()
            Return GetSpParameterSet(spName, False)
        End Function ' GetSpParameterSet 

        ' Retrieves the set of SqlParameters appropriate for the stored procedure.
        ' This method will query the database for this information, and then store it in a cache for future requests.
        ' Parameters:
        ' -connectionString - a valid connection string for a NpgsqlConnection
        ' -spName - the name of the stored procedure 
        ' -includeReturnValueParameter - a bool value indicating whether the return value parameter should be included in the results 
        ' Returns: An array of SqlParameters 
        Public Overloads Shared Function GetSpParameterSet(ByVal spName As String,
                                                        ByVal includeReturnValueParameter As Boolean) As Npgsql.NpgsqlParameter()
            Dim connectionString As String
            Try
                connectionString = CSqlHelper.GetConnectionString()
                If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
                If spName Is Nothing Or spName.Length = 0 Then Throw New ArgumentNullException("spName")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            End Try


            Dim connection As NpgsqlConnection
            Try
                connection = New NpgsqlConnection(connectionString)
                GetSpParameterSet = GetSpParameterSetInternal(connection, spName, includeReturnValueParameter)
            Finally
                If Not connection Is Nothing Then connection.Dispose()
            End Try
        End Function ' GetSpParameterSet

        ' Retrieves the set of SqlParameters appropriate for the stored procedure.
        ' This method will query the database for this information, and then store it in a cache for future requests.
        ' Parameters:
        ' -connection - a valid NpgsqlConnection object
        ' -spName - the name of the stored procedure 
        ' -includeReturnValueParameter - a bool value indicating whether the return value parameter should be included in the results 
        ' Returns: An array of SqlParameters 
        Public Overloads Shared Function GetSpParameterSet(ByVal connection As NpgsqlConnection,
                                                        ByVal spName As String) As Npgsql.NpgsqlParameter()

            GetSpParameterSet = GetSpParameterSet(connection, spName, False)
        End Function ' GetSpParameterSet

        ' Retrieves the set of SqlParameters appropriate for the stored procedure.
        ' This method will query the database for this information, and then store it in a cache for future requests.
        ' Parameters:
        ' -connection - a valid NpgsqlConnection object
        ' -spName - the name of the stored procedure 
        ' -includeReturnValueParameter - a bool value indicating whether the return value parameter should be included in the results 
        ' Returns: An array of SqlParameters 
        Public Overloads Shared Function GetSpParameterSet(ByVal connection As NpgsqlConnection,
                                                        ByVal spName As String,
                                                        ByVal includeReturnValueParameter As Boolean) As Npgsql.NpgsqlParameter()
            Try
                If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            End Try

            Dim clonedConnection As NpgsqlConnection
            Try
                clonedConnection = CType((CType(connection, ICloneable).Clone), NpgsqlConnection)
                GetSpParameterSet = GetSpParameterSetInternal(clonedConnection, spName, includeReturnValueParameter)
            Finally
                If Not clonedConnection Is Nothing Then clonedConnection.Dispose()
            End Try
        End Function ' GetSpParameterSet

        ' Retrieves the set of SqlParameters appropriate for the stored procedure.
        ' This method will query the database for this information, and then store it in a cache for future requests.
        ' Parameters:
        ' -connection - a valid NpgsqlConnection object
        ' -spName - the name of the stored procedure 
        ' -includeReturnValueParameter - a bool value indicating whether the return value parameter should be included in the results 
        ' Returns: An array of SqlParameters 
        Private Overloads Shared Function GetSpParameterSetInternal(ByVal connection As NpgsqlConnection,
                                                        ByVal spName As String,
                                                        ByVal includeReturnValueParameter As Boolean) As Npgsql.NpgsqlParameter()

            Try
                If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
            Catch exArgNull As ArgumentNullException
                Throw New Exception("NpgSqlCommand Parameter should not be null")
            End Try

            Dim cachedParameters() As Npgsql.NpgsqlParameter
            Dim hashKey As String

            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

            hashKey = connection.ConnectionString + ":" + spName + IIf(includeReturnValueParameter = True, ":include ReturnValue Parameter", "").ToString

            cachedParameters = CType(paramCache(hashKey), Npgsql.NpgsqlParameter())

            If (cachedParameters Is Nothing) Then
                Dim spParameters() As Npgsql.NpgsqlParameter = DiscoverSpParameterSet(connection, spName, includeReturnValueParameter)
                paramCache(hashKey) = spParameters
                cachedParameters = spParameters

            End If

            Return CloneParameters(cachedParameters)

        End Function ' GetSpParameterSet
#End Region

    End Class ' SqlHelperParameterCache 

End Namespace

