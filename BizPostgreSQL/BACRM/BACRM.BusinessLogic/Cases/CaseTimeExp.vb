

Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Namespace BACRM.BusinessLogic.Case
    Public Class CaseTimeExp
        Inherits BACRM.BusinessLogic.CBusinessBase
        Private _CommID As Long
        Private _CaseID As Long
        Private _CaseTimeID As Long
        Private _CaseExpID As Long
        Private _Rate As Decimal
        Private _Amount As Long
        Private _Desc As String
        Private _Billable As Boolean = True
        Private _ContractID As Long = 0
        Private _FromDate As Date = New Date(1753, 1, 1)
        Private _ToDate As Date = New Date(1753, 1, 1)
        Private _ContactID As Long = 0
        Private _OppID As Long = 0
        Private _ClientTimeZoneOffset As Integer = 0
        Public Property ClientTimeZoneOffset() As Integer
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal Value As Integer)
                _ClientTimeZoneOffset = Value
            End Set
        End Property
        Public Property OppID() As Long
            Get
                Return _OppID
            End Get
            Set(ByVal Value As Long)
                _OppID = Value
            End Set
        End Property
        Public Property CommID() As Long
            Get
                Return _CommID
            End Get
            Set(ByVal Value As Long)
                _CommID = Value
            End Set
        End Property
        Public Property ContactID() As Long
            Get
                Return _ContactID
            End Get
            Set(ByVal Value As Long)
                _ContactID = Value
            End Set
        End Property
        Public Property ContractID() As Long
            Get
                Return _ContractID
            End Get
            Set(ByVal Value As Long)
                _ContractID = Value
            End Set
        End Property
        Public Property Billable() As Boolean
            Get
                Return _Billable
            End Get
            Set(ByVal Value As Boolean)
                _Billable = Value
            End Set
        End Property

        Public Property Desc() As String
            Get
                Return _Desc
            End Get
            Set(ByVal Value As String)
                _Desc = Value
            End Set
        End Property

        Public Property Amount() As Long
            Get
                Return _Amount
            End Get
            Set(ByVal Value As Long)
                _Amount = Value
            End Set
        End Property



        Public Property Rate() As Decimal
            Get
                Return _Rate
            End Get
            Set(ByVal Value As Decimal)
                _Rate = Value
            End Set
        End Property

        Public Property CaseTimeID() As Long
            Get
                Return _CaseTimeID
            End Get
            Set(ByVal Value As Long)
                _CaseTimeID = Value
            End Set
        End Property
        Public Property CaseExpID() As Long
            Get
                Return _CaseExpID
            End Get
            Set(ByVal Value As Long)
                _CaseExpID = Value
            End Set
        End Property

        Public Property CaseID() As Long
            Get
                Return _CaseID
            End Get
            Set(ByVal Value As Long)
                _CaseID = Value
            End Set
        End Property

        Public Property ToDate() As Date
            Get
                Return _ToDate
            End Get
            Set(ByVal Value As Date)
                _ToDate = Value
            End Set
        End Property

        Public Property FromDate() As Date
            Get
                Return _FromDate
            End Get
            Set(ByVal Value As Date)
                _FromDate = Value
            End Set
        End Property
        Public Function GetTimeDetails() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As SqlParameter = New SqlParameter(12) {}


                arParms(0) = New SqlParameter("@byteMode", SqlDbType.TinyInt)
                arParms(0).Value = 1

                arParms(1) = New SqlParameter("@CaseID", SqlDbType.BigInt)
                arParms(1).Value = _CaseID

                arParms(2) = New SqlParameter("@CaseTimeID", SqlDbType.BigInt)
                arParms(2).Value = _CaseTimeID

                arParms(3) = New SqlParameter("@numRate", SqlDbType.BigInt)
                arParms(3).Value = _Rate

                arParms(4) = New SqlParameter("@vcDesc", SqlDbType.VarChar, 1000)
                arParms(4).Value = _Desc

                arParms(5) = New SqlParameter("@bitBillable", SqlDbType.Bit)
                arParms(5).Value = _Billable

                arParms(6) = New SqlParameter("@numContractID", SqlDbType.BigInt, 9)
                arParms(6).Value = _ContractID

                arParms(7) = New SqlParameter("@dtFromDate", SqlDbType.DateTime)
                arParms(7).Value = _FromDate

                arParms(8) = New SqlParameter("@dtToDate", SqlDbType.DateTime)
                arParms(8).Value = _ToDate


                arParms(9) = New SqlParameter("@CommID", SqlDbType.DateTime)
                arParms(9).Value = _CommID

                arParms(10) = New SqlParameter("@numUserCntId", SqlDbType.DateTime)
                arParms(10).Value = _ContactID

                arParms(11) = New SqlParameter("@numOppId", SqlDbType.BigInt, 9)
                arParms(11).Value = _OppID

                arParms(12) = New SqlParameter("@ClientTimeZoneOffset", SqlDbType.Int)
                arParms(12).Value = _ClientTimeZoneOffset
                ds = SqlDAL.ExecuteDataset(connString, "usp_caseTime", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function SaveTimeDetails() As Long
            Try
                Dim arParms() As SqlParameter = New SqlParameter(12) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim CaseTimeID As Integer


                arParms(0) = New SqlParameter("@byteMode", SqlDbType.TinyInt)
                arParms(0).Value = 0

                arParms(1) = New SqlParameter("@CaseID", SqlDbType.BigInt)
                arParms(1).Value = _CaseID

                arParms(2) = New SqlParameter("@CaseTimeID", SqlDbType.BigInt)
                '' arParms(2).Direction = ParameterDirection.InputOutput
                arParms(2).Value = _CaseTimeID

                arParms(3) = New SqlParameter("@numRate", SqlDbType.BigInt)
                arParms(3).Value = _Rate

                arParms(4) = New SqlParameter("@vcDesc", SqlDbType.VarChar, 1000)
                arParms(4).Value = _Desc

                arParms(5) = New SqlParameter("@bitBillable", SqlDbType.Bit)
                arParms(5).Value = _Billable

                arParms(6) = New SqlParameter("@numContractID", SqlDbType.BigInt, 9)
                arParms(6).Value = _ContractID

                arParms(7) = New SqlParameter("@dtFromDate", SqlDbType.DateTime)
                arParms(7).Value = _FromDate

                arParms(8) = New SqlParameter("@dtToDate", SqlDbType.DateTime)
                arParms(8).Value = _ToDate


                arParms(9) = New SqlParameter("@CommID", SqlDbType.DateTime)
                arParms(9).Value = _CommID

                arParms(10) = New SqlParameter("@numUserCntId", SqlDbType.DateTime)
                arParms(10).Value = _ContactID

                arParms(11) = New SqlParameter("@numOppId", SqlDbType.BigInt, 9)
                arParms(11).Value = _OppID

                arParms(12) = New SqlParameter("@ClientTimeZoneOffset", SqlDbType.Int)
                arParms(12).Value = _ClientTimeZoneOffset

                CaseTimeID = CInt(SqlDAL.ExecuteScalar(connString, "usp_CaseTime", arParms))

                ''_CaseTimeID = arParms(2).Value
                Return CaseTimeID
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function DeleteTimeDetails() As Boolean
            Try
                Dim arParms() As SqlParameter = New SqlParameter(12) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString



                arParms(0) = New SqlParameter("@byteMode", SqlDbType.TinyInt)
                arParms(0).Value = 2

                arParms(1) = New SqlParameter("@CaseID", SqlDbType.BigInt)
                arParms(1).Value = _CaseID

                arParms(2) = New SqlParameter("@CaseTimeID", SqlDbType.BigInt)
                arParms(2).Value = _CaseTimeID

                arParms(3) = New SqlParameter("@numRate", SqlDbType.BigInt)
                arParms(3).Value = _Rate

                arParms(4) = New SqlParameter("@vcDesc", SqlDbType.VarChar, 1000)
                arParms(4).Value = _Desc

                arParms(5) = New SqlParameter("@bitBillable", SqlDbType.Bit)
                arParms(5).Value = _Billable

                arParms(6) = New SqlParameter("@numContractID", SqlDbType.BigInt, 9)
                arParms(6).Value = _ContractID

                arParms(7) = New SqlParameter("@dtFromDate", SqlDbType.DateTime)
                arParms(7).Value = _FromDate

                arParms(8) = New SqlParameter("@dtToDate", SqlDbType.DateTime)
                arParms(8).Value = _ToDate

                arParms(9) = New SqlParameter("@CommID", SqlDbType.DateTime)
                arParms(9).Value = _CommID

                arParms(10) = New SqlParameter("@numUserCntId", SqlDbType.DateTime)
                arParms(10).Value = _ContactID

                arParms(11) = New SqlParameter("@numOppId", SqlDbType.BigInt, 9)
                arParms(11).Value = 0
                arParms(12) = New SqlParameter("@ClientTimeZoneOffset", SqlDbType.Int)
                arParms(12).Value = _ClientTimeZoneOffset
                SqlDAL.ExecuteNonQuery(connString, "usp_CaseTime", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
#Region "Expense"
        Public Function GetExpenseDetails() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As SqlParameter = New SqlParameter(9) {}


                arParms(0) = New SqlParameter("@byteMode", SqlDbType.TinyInt)
                arParms(0).Value = 1


                arParms(1) = New SqlParameter("@numCaseID", SqlDbType.BigInt)
                arParms(1).Value = _CaseID

                arParms(2) = New SqlParameter("@numCaseExpID", SqlDbType.BigInt)
                arParms(2).Value = _CaseExpID

                arParms(3) = New SqlParameter("@monAmount", SqlDbType.BigInt)
                arParms(3).Value = _Amount

                arParms(4) = New SqlParameter("@vcDesc", SqlDbType.VarChar, 1000)
                arParms(4).Value = _Desc

                arParms(5) = New SqlParameter("@bitBillable", SqlDbType.Bit)
                arParms(5).Value = _Billable

                arParms(6) = New SqlParameter("@numContractID", SqlDbType.BigInt, 9)
                arParms(6).Value = _ContractID

                arParms(7) = New SqlParameter("@CommID", SqlDbType.DateTime)
                arParms(7).Value = _CommID


                arParms(8) = New SqlParameter("@numUserCntId", SqlDbType.DateTime)
                arParms(8).Value = _ContactID
                arParms(9) = New SqlParameter("@numOppId", SqlDbType.BigInt, 9)
                arParms(9).Value = 0
                ds = SqlDAL.ExecuteDataset(connString, "usp_CaseExpense", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function SaveExpenseDetails() As Integer
            Try
                Dim arParms() As SqlParameter = New SqlParameter(9) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim lngCaseExpId As Integer = 0
                arParms(0) = New SqlParameter("@byteMode", SqlDbType.TinyInt)
                arParms(0).Value = 0


                arParms(1) = New SqlParameter("@numCaseId", SqlDbType.BigInt)
                arParms(1).Value = _CaseID

                arParms(2) = New SqlParameter("@numCaseExpID", SqlDbType.BigInt)
                arParms(2).Value = _CaseExpID

                arParms(3) = New SqlParameter("@monAmount", SqlDbType.BigInt)
                arParms(3).Value = _Amount

                arParms(4) = New SqlParameter("@vcDesc", SqlDbType.VarChar, 1000)
                arParms(4).Value = _Desc

                arParms(5) = New SqlParameter("@bitBillable", SqlDbType.Bit)
                arParms(5).Value = _Billable

                arParms(6) = New SqlParameter("@numContractID", SqlDbType.BigInt, 9)
                arParms(6).Value = _ContractID

                arParms(7) = New SqlParameter("@CommID", SqlDbType.DateTime)
                arParms(7).Value = _CommID

                arParms(8) = New SqlParameter("@numUserCntId", SqlDbType.DateTime)
                arParms(8).Value = _ContactID

                arParms(9) = New SqlParameter("@numOppId", SqlDbType.BigInt, 9)
                arParms(9).Value = _OppID
                lngCaseExpId = SqlDAL.ExecuteScalar(connString, "usp_CaseExpense", arParms)
                Return lngCaseExpId
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function DeleteExpenseDetails() As Boolean
            Try
                Dim arParms() As SqlParameter = New SqlParameter(9) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New SqlParameter("@byteMode", SqlDbType.TinyInt)
                arParms(0).Value = 2

                arParms(1) = New SqlParameter("@numCaseId", SqlDbType.BigInt)
                arParms(1).Value = _CaseID

                arParms(2) = New SqlParameter("@numCaseExpID", SqlDbType.BigInt)
                arParms(2).Value = _CaseExpID

                arParms(3) = New SqlParameter("@monAmount", SqlDbType.BigInt)
                arParms(3).Value = _Amount

                arParms(4) = New SqlParameter("@vcDesc", SqlDbType.VarChar, 1000)
                arParms(4).Value = _Desc

                arParms(5) = New SqlParameter("@bitBillable", SqlDbType.Bit)
                arParms(5).Value = _Billable

                arParms(6) = New SqlParameter("@numContractID", SqlDbType.BigInt, 9)
                arParms(6).Value = _ContractID

                arParms(7) = New SqlParameter("@CommID", SqlDbType.DateTime)
                arParms(7).Value = _CommID

                arParms(8) = New SqlParameter("@numUserCntId", SqlDbType.DateTime)
                arParms(8).Value = _ContactID

                arParms(9) = New SqlParameter("@numOppId", SqlDbType.BigInt, 9)
                arParms(9).Value = 0


                arParms(10) = New SqlParameter("@numOppId", SqlDbType.BigInt, 9)
                arParms(10).Value = 0

                SqlDAL.ExecuteNonQuery(connString, "usp_CaseExpense", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
#End Region
        Public Function GetCaseOpp() As DataTable
            Try
                Dim arParms() As SqlParameter = New SqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New SqlParameter("@byteMode", SqlDbType.TinyInt)
                arParms(0).Value = 1

                arParms(1) = New SqlParameter("@numProId", SqlDbType.BigInt)
                arParms(1).Value = 0

                arParms(2) = New SqlParameter("@numCaseId", SqlDbType.BigInt)
                arParms(2).Value = _CaseID

                ds = SqlDAL.ExecuteDataset(connString, "usp_ProCaseOppLinked", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace
