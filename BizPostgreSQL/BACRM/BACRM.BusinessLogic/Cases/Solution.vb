'Created  By Anoop Jayaraj
Option Explicit On 
Option Strict On
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Namespace BACRM.BusinessLogic.Case

Public Class Solution
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _Category As Long
        Private _SolName As String
        Private _SolDesc As String
        Private _KeyWord As String
        Private _SolID As Long
        Private _CaseID As Long
        Private _SortCharacter As Char
        Private _columnSortOrder As String
        Private _columnName As String
        Private _TotalRecords As Integer
        Private _Link As String
        'Private DomainId As Long
        'Private UserCntID As Long

        'Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal Value As Long)
        '        UserCntID = Value
        '    End Set
        'End Property

        Private _Mode As Boolean
        Public Property Mode() As Boolean
            Get
                Return _Mode
            End Get
            Set(ByVal value As Boolean)
                _Mode = value
            End Set
        End Property

        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property

        Public Property Link() As String
            Get
                Return _Link
            End Get
            Set(ByVal Value As String)
                _Link = Value
            End Set
        End Property

        Public Property TotalRecords() As Integer
            Get
                Return _TotalRecords
            End Get
            Set(ByVal Value As Integer)
                _TotalRecords = Value
            End Set
        End Property

        Public Property columnName() As String
            Get
                Return _columnName
            End Get
            Set(ByVal Value As String)
                _columnName = Value
            End Set
        End Property

        Public Property columnSortOrder() As String
            Get
                Return _columnSortOrder
            End Get
            Set(ByVal Value As String)
                _columnSortOrder = Value
            End Set
        End Property

        Public Property SortCharacter() As Char
            Get
                Return _SortCharacter
            End Get
            Set(ByVal Value As Char)
                _SortCharacter = Value
            End Set
        End Property

        Public Property CaseID() As Long
            Get
                Return _CaseID
            End Get
            Set(ByVal Value As Long)
                _CaseID = Value
            End Set
        End Property

        Public Property SolID() As Long
            Get
                Return _SolID
            End Get
            Set(ByVal Value As Long)
                _SolID = Value
            End Set
        End Property

        Public Property KeyWord() As String
            Get
                Return _KeyWord
            End Get
            Set(ByVal Value As String)
                _KeyWord = Value
            End Set
        End Property

        Public Property SolDesc() As String
            Get
                Return _SolDesc
            End Get
            Set(ByVal Value As String)
                _SolDesc = Value
            End Set
        End Property

        Public Property SolName() As String
            Get
                Return _SolName
            End Get
            Set(ByVal Value As String)
                _SolName = Value
            End Set
        End Property

        Public Property Category() As Long
            Get
                Return _Category
            End Get
            Set(ByVal Value As Long)
                _Category = Value
            End Set
        End Property

        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop Jayaraj
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop Jayaraj
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region

        Public Function SaveSolution() As Long
            Try

                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numCategory", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _Category

                arParms(1) = New Npgsql.NpgsqlParameter("@vcSolnTitle", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(1).Value = _SolName

                arParms(2) = New Npgsql.NpgsqlParameter("@txtSolution", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(2).Value = _SolDesc

                arParms(3) = New Npgsql.NpgsqlParameter("@numSolID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Direction = ParameterDirection.InputOutput
                arParms(3).Value = _SolID

                arParms(4) = New Npgsql.NpgsqlParameter("@vcLink", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(4).Value = _Link

                arParms(5) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = DomainId

                arParms(6) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = UserCntID

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_SolManage", objParam, True)
                Return Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(3).Value)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveCaseSolutionShortMessage() As Long
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numCaseId", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(1).Value = CaseID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcSolutionShortMessage", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(2).Value = SolName

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = UserCntID

                SqlDAL.ExecuteNonQuery(connString, "USP_SaveCaseSolutionShortMessage", arParms)
                Return 1
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetKnowledgeBases() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numCategory", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _Category


                arParms(1) = New Npgsql.NpgsqlParameter("@KeyWord", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(1).Value = Replace(_KeyWord, "'", "''")

                arParms(2) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(2).Value = _SortCharacter

                arParms(3) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(3).Value = _columnName

                arParms(4) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(4).Value = _columnSortOrder

                arParms(5) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Direction = ParameterDirection.InputOutput
                arParms(5).Value = _TotalRecords

                arParms(6) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(6).Value = DomainID

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetSolution", arParms)

                If Not ds Is Nothing AndAlso ds.Tables.Count() > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    _TotalRecords = ds.Tables(0).Rows.Count
                Else
                    _TotalRecords = 0
                End If


                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetSolutionForEdit() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSolnID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _SolID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetSolutionForEdit", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function LinkSolToCases() As Boolean
            Try

                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numCaseID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _CaseID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSolnID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _SolID

                arParms(2) = New Npgsql.NpgsqlParameter("@bitMode", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(2).Value = _Mode

                SqlDAL.ExecuteNonQuery(connString, "USP_Solution_Manage", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetSolutionForCases() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numCaseId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _CaseID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetSoltionForCases", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteSolution() As Boolean
            Try

                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSolutionID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _SolID


                SqlDAL.ExecuteDataset(connString, "USP_DeleteSolution", arParms)
                Return True
            Catch ex As Exception
                Return False
                Throw ex
            End Try
        End Function

    End Class

End Namespace
