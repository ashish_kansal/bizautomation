﻿'Created Anoop Jayaraj
Option Explicit On
Option Strict On


Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports Npgsql

Namespace BACRM.BusinessLogic.Case
    Public Class CaseIP
        Inherits CCases


        Private _Email As String
        Private _Phone As String
        Private _Name As String
        Private _AssignToName As String
        Private _PriorityName As String
        Private _CaseTypeName As String
        Private _ReasonName As String
        Private _OrginName As String
        Private _DocCount As Integer

        Private _SoultionCount As Long
        Public Property SolutionCount() As Long
            Get
                Return _SoultionCount
            End Get
            Set(ByVal Value As Long)
                _SoultionCount = Value
            End Set
        End Property

        Private _SoultionName As String
        Public Property SoultionName() As String
            Get
                Return _SoultionName
            End Get
            Set(ByVal Value As String)
                _SoultionName = Value
            End Set
        End Property
        Public Property DocCount() As Integer
            Get
                Return _DocCount
            End Get
            Set(ByVal Value As Integer)
                _DocCount = Value
            End Set
        End Property

        Public Property OrginName() As String
            Get
                Return _OrginName
            End Get
            Set(ByVal Value As String)
                _OrginName = Value
            End Set
        End Property


        Public Property ReasonName() As String
            Get
                Return _ReasonName
            End Get
            Set(ByVal Value As String)
                _ReasonName = Value
            End Set
        End Property


        Public Property CaseTypeName() As String
            Get
                Return _CaseTypeName
            End Get
            Set(ByVal Value As String)
                _CaseTypeName = Value
            End Set
        End Property


        Public Property PriorityName() As String
            Get
                Return _PriorityName
            End Get
            Set(ByVal Value As String)
                _PriorityName = Value
            End Set
        End Property


        Public Property AssignToName() As String
            Get
                Return _AssignToName
            End Get
            Set(ByVal Value As String)
                _AssignToName = Value
            End Set
        End Property


        Public Property Name() As String
            Get
                Return _Name
            End Get
            Set(ByVal Value As String)
                _Name = Value
            End Set
        End Property


        Public Property Phone() As String
            Get
                Return _Phone
            End Get
            Set(ByVal Value As String)
                _Phone = Value
            End Set
        End Property


        Public Property Email() As String
            Get
                Return _Email
            End Get
            Set(ByVal Value As String)
                _Email = Value
            End Set
        End Property
        'Obsolete Property remove it
        Private _LinkedItemCount As Integer
        Public Property LinkedItemCount() As Integer
            Get
                Return _LinkedItemCount
            End Get
            Set(ByVal value As Integer)
                _LinkedItemCount = value
            End Set
        End Property

        Private _CaseStatusName As String
        Public Property CaseStatusName() As String
            Get
                Return _CaseStatusName
            End Get
            Set(ByVal value As String)
                _CaseStatusName = value
            End Set
        End Property

        Private _ContractIDName As String
        Public Property ContractIDName() As String
            Get
                Return _ContractIDName
            End Get
            Set(ByVal value As String)
                _ContractIDName = value
            End Set
        End Property

        Private _ShareRecordWith As String
        Public Property ShareRecordWith() As String
            Get
                Return _ShareRecordWith
            End Get
            Set(ByVal Value As String)
                _ShareRecordWith = Value
            End Set
        End Property

        Private _ContactIDName As String
        Public Property ContactIDName() As String
            Get
                Return _ContactIDName
            End Get
            Set(ByVal value As String)
                _ContactIDName = value
            End Set
        End Property

        Public Property vcCompactContactDetails As String

        Public Function GetCaseDetails() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numCaseId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = Me.CaseID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = Me.DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = Me.ClientTimeZoneOffset

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_CaseDetailsdtlPL", arParms)

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables.Count > 0 Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        If Not IsDBNull(dr("textSubject")) Then
                            Me.Subject = CType(dr("textSubject"), String)
                        End If
                        If Not IsDBNull(dr("intTargetResolveDate")) Then
                            Me.TargetResolveDate = CType(dr("intTargetResolveDate"), Date)
                        End If
                        If Not IsDBNull(dr("numAssignedTo")) Then
                            Me.AssignTo = CType(dr("numAssignedTo"), Long)
                        End If
                        If Not IsDBNull(dr("vcEmail")) Then
                            Me.Email = CType(dr("vcEmail"), String)
                        End If
                        If Not IsDBNull(dr("Phone")) Then
                            Me.Phone = CType(dr("Phone"), String)
                        End If
                        If Not IsDBNull(dr("Name")) Then
                            Me.Name = CType(dr("Name"), String)
                        End If
                        If Not IsDBNull(dr("numPriority")) Then
                            Me.Priority = CType(dr("numPriority"), Long)
                        End If
                        If Not IsDBNull(dr("numType")) Then
                            Me.CaseType = CType(dr("numType"), Long)
                        End If
                        If Not IsDBNull(dr("numReason")) Then
                            Me.Reason = CType(dr("numReason"), Long)
                        End If
                        If Not IsDBNull(dr("numOrigin")) Then
                            Me.Orgin = CType(dr("numOrigin"), Long)
                        End If
                        If Not IsDBNull(dr("textInternalComments")) Then
                            Me.InternalComments = CType(dr("textInternalComments"), String)
                        End If
                        If Not IsDBNull(dr("textDesc")) Then
                            Me.Desc = CType(dr("textDesc"), String)
                        End If
                        If Not IsDBNull(dr("numAssignedToName")) Then
                            Me.AssignToName = CType(dr("numAssignedToName"), String)
                        End If
                        If Not IsDBNull(dr("numPriorityName")) Then
                            Me.PriorityName = CType(dr("numPriorityName"), String)
                        End If
                        If Not IsDBNull(dr("numTypeName")) Then
                            Me.CaseTypeName = CType(dr("numTypeName"), String)
                        End If
                        If Not IsDBNull(dr("numReasonName")) Then
                            Me.ReasonName = CType(dr("numReasonName"), String)
                        End If
                        If Not IsDBNull(dr("numOriginName")) Then
                            Me.OrginName = CType(dr("numOriginName"), String)
                        End If
                        If Not IsDBNull(dr("LinkedItemCount")) Then
                            Me.LinkedItemCount = CType(dr("LinkedItemCount"), Integer)
                        End If
                        If Not IsDBNull(dr("numStatus")) Then
                            Me.CaseStatus = CType(dr("numStatus"), Long)
                        End If
                        If Not IsDBNull(dr("numStatusName")) Then
                            Me.CaseStatusName = CType(dr("numStatusName"), String)
                        End If
                        If Not IsDBNull(dr("numContractID")) Then
                            Me.ContractID = CType(dr("numContractID"), Long)
                        End If
                        If Not IsDBNull(dr("ContractName")) Then
                            Me.ContractIDName = CType(dr("ContractName"), String)
                        End If
                        If Not IsDBNull(dr("numShareWith")) Then
                            Me.ShareRecordWith = CType(dr("numShareWith"), String)
                        End If
                        If Not IsDBNull(dr("DocumentCount")) Then
                            Me.DocCount = CType(dr("DocumentCount"), Integer)
                        End If
                        If Not IsDBNull(dr("numContactID")) Then
                            Me.ContactID = CType(dr("numContactID"), Long)
                        End If
                        If Not IsDBNull(dr("numContactIdName")) Then
                            Me.ContactIDName = CType(dr("numContactIdName"), String)
                        End If
                        If Not IsDBNull(dr("SolutionCount")) Then
                            Me.SolutionCount = CType(dr("SolutionCount"), Long)
                        End If
                        If Not IsDBNull(dr("SoultionName")) Then
                            Me.SoultionName = CType(dr("SoultionName"), String)
                        End If
                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetDueCaseList() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = ClientTimeZoneOffset

                arParms(2) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = ContactID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_CaseDueListForWeek", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

    End Class
End Namespace

