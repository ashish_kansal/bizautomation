
' Modified BY Anoop Jayaraj


Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Case

    '**********************************************************************************
    ' Module Name  : None
    ' Module Type  : CAccount
    ' 
    ' Description  : This is the business logic classe for Accounts Module
    '**********************************************************************************
    Public Class CCases
        Inherits BACRM.BusinessLogic.CBusinessBase


        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Goyal 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Goyal 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region

        Private _SortOrder As Short
        'Private UserCntID As Long = 0
        Private _CompanyName As String = String.Empty
        Private _FirstName As String = String.Empty
        Private _LastName As String = String.Empty
        Private _CustName As String = String.Empty
        ''Private DomainId As Long = 0
        Private _UserRightType As Integer = 0
        Private _CaseID As Long = 0
        Private _SortChar As String = String.Empty
        Private _SortChar1 As String = String.Empty
        Private _TerritoryID As Long = 0
        Private _ComanyID As Long = 0
        Private _CRMType As Long = 0
        Private _DivisionID As Long = 0
        Private _OppID As Long = 0
        Private _ContactID As Long = 0
        Private _CaseNo As String
        Private _Subject As String
        Private _Desc As String
        Private _SupportKeyType As Short
        Private _TargetResolveDate As Date = New Date(1753, 1, 1)
        Private _CaseStatus As Long
        Private _Priority As Long
        Private _InternalComments As String
        Private _Reason As Long
        Private _Orgin As Long
        Private _CaseType As Long
        Private _AssignTo As String
        Private _CurrentPage As Integer
        Private _PageSize As Integer
        Private _TotalRecords As Integer
        Private _SortCharacter As Char
        Private _columnSortOrder As String
        Private _columnName As String
        Private _KeyWord As String
        Private _FromDate As Date
        Private _ToDate As Date
        Private _byteMode As Short
        Private _Comments As String
        Private _bitPartner As Boolean
        Private _BitTask As Short
        Private _ClientTimeZoneOffset As Int32
        Private _strOppSel As String
        Private _ContractID As Long = 0
        Private _ProCaseNo As String
        Private _ContactRole As Long
        Private _Email As String
        Private _bitSubscribedEmailAlert As Boolean
        Public Property bitSubscribedEmailAlert() As Boolean
            Get
                Return _bitSubscribedEmailAlert
            End Get
            Set(ByVal Value As Boolean)
                _bitSubscribedEmailAlert = Value
            End Set
        End Property

        Public Property ContactRole() As Long
            Get
                Return _ContactRole
            End Get
            Set(ByVal Value As Long)
                _ContactRole = Value
            End Set
        End Property

        Private _OppItemID As Long
        Public Property OppItemID() As Long
            Get
                Return _OppItemID
            End Get
            Set(ByVal value As Long)
                _OppItemID = value
            End Set
        End Property


        Public Property strOppSel() As String
            Get
                Return _strOppSel
            End Get
            Set(ByVal Value As String)
                _strOppSel = Value
            End Set
        End Property

        Public Property Email() As String
            Get
                Return _Email
            End Get
            Set(ByVal Value As String)
                _Email = Value
            End Set
        End Property
        Public Property ClientTimeZoneOffset() As Int32
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal Value As Int32)
                _ClientTimeZoneOffset = Value
            End Set
        End Property
        Public Property BitTask() As Short
            Get
                Return _BitTask
            End Get
            Set(ByVal Value As Short)
                _BitTask = Value
            End Set
        End Property

        Public Property bitPartner() As Boolean
            Get
                Return _bitPartner
            End Get
            Set(ByVal Value As Boolean)
                _bitPartner = Value
            End Set
        End Property








        Public Property Comments() As String
            Get
                Return _Comments
            End Get
            Set(ByVal Value As String)
                _Comments = Value
            End Set
        End Property




        Public Property byteMode() As Short
            Get
                Return _byteMode
            End Get
            Set(ByVal Value As Short)
                _byteMode = Value
            End Set
        End Property


        Public Property ToDate() As Date
            Get
                Return _ToDate
            End Get
            Set(ByVal Value As Date)
                _ToDate = Value
            End Set
        End Property


        Public Property FromDate() As Date
            Get
                Return _FromDate
            End Get
            Set(ByVal Value As Date)
                _FromDate = Value
            End Set
        End Property


        Public Property KeyWord() As String
            Get
                Return _KeyWord
            End Get
            Set(ByVal Value As String)
                _KeyWord = Value
            End Set
        End Property




        Public Property columnName() As String
            Get
                Return _columnName
            End Get
            Set(ByVal Value As String)
                _columnName = Value
            End Set
        End Property

        Public Property columnSortOrder() As String
            Get
                Return _columnSortOrder
            End Get
            Set(ByVal Value As String)
                _columnSortOrder = Value
            End Set
        End Property

        Public Property SortCharacter() As Char
            Get
                Return _SortCharacter
            End Get
            Set(ByVal Value As Char)
                _SortCharacter = Value
            End Set
        End Property

        Public Property TotalRecords() As Integer
            Get
                Return _TotalRecords
            End Get
            Set(ByVal Value As Integer)
                _TotalRecords = Value
            End Set
        End Property

        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal Value As Integer)
                _PageSize = Value
            End Set
        End Property

        Public Property CurrentPage() As Integer
            Get
                Return _CurrentPage
            End Get
            Set(ByVal Value As Integer)
                _CurrentPage = Value
            End Set
        End Property


        Public Property AssignTo() As Long
            Get
                Return _AssignTo
            End Get
            Set(ByVal Value As Long)
                _AssignTo = Value
            End Set
        End Property

        Public Property CaseType() As Long
            Get
                Return _CaseType
            End Get
            Set(ByVal Value As Long)
                _CaseType = Value
            End Set
        End Property

        Public Property Orgin() As Long
            Get
                Return _Orgin
            End Get
            Set(ByVal Value As Long)
                _Orgin = Value
            End Set
        End Property

        Public Property Reason() As Long
            Get
                Return _Reason
            End Get
            Set(ByVal Value As Long)
                _Reason = Value
            End Set
        End Property

        Public Property InternalComments() As String
            Get
                Return _InternalComments
            End Get
            Set(ByVal Value As String)
                _InternalComments = Value
            End Set
        End Property

        Public Property Priority() As Long
            Get
                Return _Priority
            End Get
            Set(ByVal Value As Long)
                _Priority = Value
            End Set
        End Property

        Public Property CaseStatus() As Long
            Get
                Return _CaseStatus
            End Get
            Set(ByVal Value As Long)
                _CaseStatus = Value
            End Set
        End Property

        Private _sMode As Short
        Public Property sMode() As Short
            Get
                Return _sMode
            End Get
            Set(ByVal value As Short)
                _sMode = value
            End Set
        End Property


        Public Property TargetResolveDate() As Date
            Get
                Return _TargetResolveDate
            End Get
            Set(ByVal Value As Date)
                _TargetResolveDate = Value
            End Set
        End Property



        Public Property SupportKeyType() As Short
            Get
                Return _SupportKeyType
            End Get
            Set(ByVal Value As Short)
                _SupportKeyType = Value
            End Set
        End Property



        Public Property Desc() As String
            Get
                Return _Desc
            End Get
            Set(ByVal Value As String)
                _Desc = Value
            End Set
        End Property



        Public Property Subject() As String
            Get
                Return _Subject
            End Get
            Set(ByVal Value As String)
                _Subject = Value
            End Set
        End Property


        Public Property ProCaseNo() As String
            Get
                Return _ProCaseNo
            End Get
            Set(ByVal Value As String)
                _ProCaseNo = Value
            End Set
        End Property
        Public Property CaseNo() As String
            Get
                Return _CaseNo
            End Get
            Set(ByVal Value As String)
                _CaseNo = Value
            End Set
        End Property

        Public Property ContractID() As Long
            Get
                Return _ContractID
            End Get
            Set(ByVal Value As Long)
                _ContractID = Value
            End Set
        End Property

        Public Property ContactID() As Long
            Get
                Return _ContactID
            End Get
            Set(ByVal Value As Long)
                _ContactID = Value
            End Set
        End Property



        Public Property OppID() As Long
            Get
                Return _OppID
            End Get
            Set(ByVal Value As Long)
                _OppID = Value
            End Set
        End Property



        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property


        Public Property CRMType() As Long
            Get
                Return _CRMType
            End Get
            Set(ByVal Value As Long)
                _CRMType = Value
            End Set
        End Property


        Public Property ComanyID() As Long
            Get
                Return _ComanyID
            End Get
            Set(ByVal Value As Long)
                _ComanyID = Value
            End Set
        End Property


        Public Property TerritoryID() As Long
            Get
                Return _TerritoryID
            End Get
            Set(ByVal Value As Long)
                _TerritoryID = Value
            End Set
        End Property


        Public Property SortChar() As String
            Get
                Return _SortChar
            End Get
            Set(ByVal Value As String)
                _SortChar = Value
            End Set
        End Property
        Public Property SortChar1() As String
            Get
                Return _SortChar1
            End Get
            Set(ByVal Value As String)
                _SortChar1 = Value
            End Set
        End Property
        Public Property CaseID() As Long
            Get
                Return _CaseID
            End Get
            Set(ByVal Value As Long)
                _CaseID = Value
            End Set
        End Property
        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property
        Public Property UserRightType() As Integer
            Get
                Return _UserRightType
            End Get
            Set(ByVal Value As Integer)
                _UserRightType = Value
            End Set
        End Property

        Public Property CompanyName() As String
            Get
                Return _CompanyName
            End Get
            Set(ByVal Value As String)
                _CompanyName = Value
            End Set
        End Property
        Public Property FirstName() As String
            Get
                Return _FirstName
            End Get
            Set(ByVal Value As String)
                _FirstName = Value
            End Set
        End Property
        Public Property LastName() As String
            Get
                Return _LastName
            End Get
            Set(ByVal Value As String)
                _LastName = Value
            End Set
        End Property
        Public Property CustName() As String
            Get
                Return _CustName
            End Get
            Set(ByVal Value As String)
                _CustName = Value
            End Set
        End Property

        Public Property SortOrder() As Short
            Get
                Return _SortOrder
            End Get
            Set(ByVal Value As Short)
                _SortOrder = Value
            End Set
        End Property

        Private _RegularSearchCriteria As String
        Public Property RegularSearchCriteria() As String
            Get
                Return _RegularSearchCriteria
            End Get
            Set(ByVal Value As String)
                _RegularSearchCriteria = Value
            End Set
        End Property

        Private _CustomSearchCriteria As String
        Public Property CustomSearchCriteria() As String
            Get
                Return _CustomSearchCriteria
            End Get
            Set(ByVal Value As String)
                _CustomSearchCriteria = Value
            End Set
        End Property
        Private _SearchText As String
        Public Property SearchText() As String
            Get
                Return _SearchText
            End Get
            Set(ByVal Value As String)
                _SearchText = Value
            End Set
        End Property
        'Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal Value As Long)
        '        UserCntID = Value
        '    End Set
        'End Property



        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  RemoveRecrodCase()
        '     Purpose					 :  To delete data from database 
        '     Example                    :  
        '     Parameters                 :  @ItemCode                     
        '                                 
        '     Outputs					 :  It return true  if deleted otherwise return false
        '						
        '     Author Name				 :  Ajeet Singh
        '     Date Written				 :  1/11/2004
        '     Cross References 			 :  List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Function DeleteCase() As Boolean
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Dim Custint As Integer
            Try
                ' Set up parameters 
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                ' @ItemCount Input Parameter 
                arParms(0) = New Npgsql.NpgsqlParameter("@numCaseID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _CaseID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                Custint = SqlDAL.ExecuteNonQuery(connString, "usp_DeleteCase ", arParms)

                Return True
            Catch ex As Exception
                Return False
                Throw ex
            Finally

            End Try

        End Function


        Public Function GetDealSupportKey() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_CaseDealSupportKey", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function SaveCases() As Long
            Try

                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(17) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numCaseId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _CaseID

                'arParms(1) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                'arParms(1).Value = _OppID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _DivisionID

                arParms(2) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _ContactID

                arParms(3) = New Npgsql.NpgsqlParameter("@vcCaseNumber", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(3).Value = _CaseNo


                arParms(4) = New Npgsql.NpgsqlParameter("@textSubject", NpgsqlTypes.NpgsqlDbType.VarChar, 2000)
                arParms(4).Value = _Subject

                arParms(5) = New Npgsql.NpgsqlParameter("@textDesc", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(5).Value = _Desc

                arParms(6) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(6).Value = UserCntID

                arParms(7) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(7).Value = DomainId

                arParms(8) = New Npgsql.NpgsqlParameter("@tintSupportKeyType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(8).Value = _SupportKeyType

                arParms(9) = New Npgsql.NpgsqlParameter("@intTargetResolveDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(9).Value = _TargetResolveDate

                arParms(10) = New Npgsql.NpgsqlParameter("@numStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(10).Value = _CaseStatus

                arParms(11) = New Npgsql.NpgsqlParameter("@numPriority", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(11).Value = _Priority

                arParms(12) = New Npgsql.NpgsqlParameter("@textInternalComments", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(12).Value = _InternalComments

                arParms(13) = New Npgsql.NpgsqlParameter("@numReason", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(13).Value = _Reason

                arParms(14) = New Npgsql.NpgsqlParameter("@numOrigin", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(14).Value = _Orgin

                arParms(15) = New Npgsql.NpgsqlParameter("@numType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(15).Value = _CaseType

                arParms(16) = New Npgsql.NpgsqlParameter("@numAssignedTo", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(16).Value = Convert.ToInt64(_AssignTo)

                arParms(17) = New Npgsql.NpgsqlParameter("@numContractId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(17).Value = _ContractID

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_CaseManage", objParam, True)
                _CaseID = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

                Return _CaseID
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function SaveCasesFromWeb() As Long
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(18) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numCaseId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _CaseID

                'arParms(1) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                'arParms(1).Value = _OppID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _DivisionID

                arParms(2) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _ContactID

                arParms(3) = New Npgsql.NpgsqlParameter("@vcCaseNumber", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(3).Value = _CaseNo


                arParms(4) = New Npgsql.NpgsqlParameter("@textSubject", NpgsqlTypes.NpgsqlDbType.VarChar, 2000)
                arParms(4).Value = _Subject

                arParms(5) = New Npgsql.NpgsqlParameter("@textDesc", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(5).Value = _Desc

                arParms(6) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(6).Value = UserCntID

                arParms(7) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(7).Value = DomainID

                arParms(8) = New Npgsql.NpgsqlParameter("@tintSupportKeyType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(8).Value = _SupportKeyType

                arParms(9) = New Npgsql.NpgsqlParameter("@intTargetResolveDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(9).Value = _TargetResolveDate

                arParms(10) = New Npgsql.NpgsqlParameter("@numStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(10).Value = _CaseStatus

                arParms(11) = New Npgsql.NpgsqlParameter("@numPriority", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(11).Value = _Priority

                arParms(12) = New Npgsql.NpgsqlParameter("@textInternalComments", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(12).Value = _InternalComments

                arParms(13) = New Npgsql.NpgsqlParameter("@numReason", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(13).Value = _Reason

                arParms(14) = New Npgsql.NpgsqlParameter("@numOrigin", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(14).Value = _Orgin

                arParms(15) = New Npgsql.NpgsqlParameter("@numType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(15).Value = _CaseType

                arParms(16) = New Npgsql.NpgsqlParameter("@numAssignedTo", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(16).Value = _AssignTo

                arParms(17) = New Npgsql.NpgsqlParameter("@numContractId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(17).Value = _ContractID

                arParms(18) = New Npgsql.NpgsqlParameter("@vcEmail", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(18).Value = _Email

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_CaseManageFromWeb", objParam, True)
                _CaseID = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

                Return _CaseID
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetMaxCaseID() As Long
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return CInt(SqlDAL.ExecuteScalar(GetConnection.GetConnectionString, "USP_GetCaseId", sqlParams.ToArray()))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCaseDTL() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numCaseId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _CaseID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _ClientTimeZoneOffset

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_CaseDetails", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetOpenCases() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetOpenCases", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCaseList() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(18) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintSortOrder", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _SortOrder


                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = DomainId

                arParms(3) = New Npgsql.NpgsqlParameter("@tintUserRightType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _UserRightType

                arParms(4) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(4).Value = _SortCharacter

                arParms(5) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _CurrentPage

                arParms(6) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = _PageSize

                arParms(7) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(7).Direction = ParameterDirection.InputOutput
                arParms(7).Value = _TotalRecords

                arParms(8) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(8).Value = _columnName

                arParms(9) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(9).Value = _columnSortOrder

                arParms(10) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(10).Value = _DivisionID

                arParms(11) = New Npgsql.NpgsqlParameter("@bitPartner", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(11).Value = _bitPartner

                arParms(12) = New Npgsql.NpgsqlParameter("@vcRegularSearchCriteria", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(12).Value = _RegularSearchCriteria

                arParms(13) = New Npgsql.NpgsqlParameter("@vcCustomSearchCriteria", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(13).Value = _CustomSearchCriteria

                arParms(14) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(14).Value = _ClientTimeZoneOffset

                arParms(15) = New Npgsql.NpgsqlParameter("@numStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(15).Value = _CaseStatus

                arParms(16) = New Npgsql.NpgsqlParameter("@SearchText", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(16).Value = _SearchText

                arParms(17) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(17).Value = Nothing
                arParms(17).Direction = ParameterDirection.InputOutput

                arParms(18) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(18).Value = Nothing
                arParms(18).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()

                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetCaseList1", objParam, True)
                _TotalRecords = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(7).Value)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCaseListForPortal() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(20) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintSortOrder", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _SortOrder


                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@tintUserRightType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _UserRightType

                arParms(4) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(4).Value = _SortCharacter

                arParms(5) = New Npgsql.NpgsqlParameter("@CustName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(5).Value = IIf(_CustName = "", "", Replace(_CustName, "'", "''"))

                arParms(6) = New Npgsql.NpgsqlParameter("@FirstName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(6).Value = IIf(_FirstName = "", "", Replace(_FirstName, "'", "''"))

                arParms(7) = New Npgsql.NpgsqlParameter("@LastName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(7).Value = IIf(_LastName = "", "", Replace(_LastName, "'", "''"))

                arParms(8) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(8).Value = _CurrentPage

                arParms(9) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(9).Value = _PageSize

                arParms(10) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(10).Direction = ParameterDirection.InputOutput
                arParms(10).Value = _TotalRecords

                arParms(11) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(11).Value = _columnName

                arParms(12) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(12).Value = _columnSortOrder

                arParms(13) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(13).Value = _DivisionID

                arParms(14) = New Npgsql.NpgsqlParameter("@bitPartner", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(14).Value = _bitPartner

                arParms(15) = New Npgsql.NpgsqlParameter("@vcRegularSearchCriteria", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(15).Value = _RegularSearchCriteria

                arParms(16) = New Npgsql.NpgsqlParameter("@vcCustomSearchCriteria", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(16).Value = _CustomSearchCriteria

                arParms(17) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(17).Value = _ClientTimeZoneOffset

                arParms(18) = New Npgsql.NpgsqlParameter("@numStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(18).Value = _CaseStatus

                arParms(19) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(19).Value = _sMode

                arParms(20) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(20).Value = Nothing
                arParms(20).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                Dim dtTable As DataTable

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetCaseListForPortal", arParms)

                dtTable = ds.Tables(0)
                If dtTable.Rows.Count > 0 AndAlso _sMode = 0 Then
                    _TotalRecords = CInt(dtTable.Rows(0).Item("TotalRowCount"))
                End If

                ds.AcceptChanges()
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCaseListPortal() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(16) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintSortOrder", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _SortOrder


                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = DomainId

                arParms(3) = New Npgsql.NpgsqlParameter("@tintUserRightType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _UserRightType

                arParms(4) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(4).Value = _SortCharacter

                arParms(5) = New Npgsql.NpgsqlParameter("@CustName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(5).Value = IIf(_CustName = "", "", Replace(_CustName, "'", "''"))

                arParms(6) = New Npgsql.NpgsqlParameter("@FirstName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(6).Value = IIf(_FirstName = "", "", Replace(_FirstName, "'", "''"))

                arParms(7) = New Npgsql.NpgsqlParameter("@LastName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(7).Value = IIf(_LastName = "", "", Replace(_LastName, "'", "''"))

                arParms(8) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(8).Value = _CurrentPage

                arParms(9) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(9).Value = _PageSize

                arParms(10) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(10).Direction = ParameterDirection.InputOutput
                arParms(10).Value = _TotalRecords

                arParms(11) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(11).Value = _columnName

                arParms(12) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(12).Value = _columnSortOrder

                arParms(13) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(13).Value = _DivisionID

                arParms(14) = New Npgsql.NpgsqlParameter("@bitPartner", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(14).Value = _bitPartner

                arParms(15) = New Npgsql.NpgsqlParameter("@numStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(15).Value = _CaseStatus

                arParms(16) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(16).Value = Nothing
                arParms(16).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetCaseList", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(10).Value)

                Dim dtTable As DataTable
                dtTable = ds.Tables(0)

                Return dtTable
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function AssCntsByCaseID() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numCaseId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CaseID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_AssCntCase", arParms).Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetOpportunities() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = _bytemode

                arParms(1) = New Npgsql.NpgsqlParameter("@domainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId


                arParms(2) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _DivisionID

                arParms(3) = New Npgsql.NpgsqlParameter("@numProjectId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = 0

                arParms(4) = New Npgsql.NpgsqlParameter("@numCaseId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _CaseID

                arParms(5) = New Npgsql.NpgsqlParameter("@numStageID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(5).Value = 0

                arParms(6) = New Npgsql.NpgsqlParameter("@tintOppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(6).Value = 0

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_ProjectOpp", arParms)

                Return ds.Tables(0)
            Catch ex As Exception

                Throw ex
            End Try
        End Function
        Public Function GetCaseOpportunities() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numcaseId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CaseID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@vcOppName", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(2).Value = _strOppSel

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "Usp_GetCaseOpportunities", arParms)

                Return ds.Tables(0)
            Catch ex As Exception

                Throw ex
            End Try
        End Function

        Sub SaveCaseOpportunities()
            Try
                Dim getconnection As New GetConnection

                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numCaseId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CaseID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomianID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@strOppid", NpgsqlTypes.NpgsqlDbType.VarChar, 4000)
                arParms(2).Value = _strOppSel

                arParms(3) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _byteMode

                SqlDAL.ExecuteNonQuery(connString, "Usp_SaveCaseOpportunities", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function AddCaseContacts() As DataTable

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numCaseId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CaseID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ContactID

                arParms(3) = New Npgsql.NpgsqlParameter("@numRole", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _ContactRole

                arParms(4) = New Npgsql.NpgsqlParameter("@bitPartner", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(4).Value = _bitPartner

                arParms(5) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _byteMode

                arParms(6) = New Npgsql.NpgsqlParameter("@bitSubscribedEmailAlert", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(6).Value = _bitSubscribedEmailAlert

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                Return (SqlDAL.ExecuteDataset(connString, "USP_ManageCaseAssociatedContact", arParms).Tables(0))

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        'Private __sMode As Short
        'Public Property sMode() As Short
        '    Get
        '        Return __sMode
        '    End Get
        '    Set(ByVal value As Short)
        '        __sMode = value
        '    End Set
        'End Property

        Public Sub UpdateCaseStatus()
            Try
                'Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                SqlDAL.ExecuteScalar(connString, "Usp_UpdateCaseStatus")

            Catch ex As Exception
                Throw ex
            End Try

        End Sub


        Public Function GetEmailToCaseCheck(ByVal FromEmail As String) As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcFromEmail", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(1).Value = FromEmail

                arParms(2) = New Npgsql.NpgsqlParameter("@vcCaseNo", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(2).Value = _CaseNo

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetEmailToCaseCheck", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function AssignCaseRecOwner(ByVal RecOwner As Long) As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numRecOwner", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = RecOwner

                arParms(1) = New Npgsql.NpgsqlParameter("@numAssignedTo", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _AssignTo

                arParms(2) = New Npgsql.NpgsqlParameter("@numCaseId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Value = _CaseID

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_AssignCaseRecOwner", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetFieldValue(ByVal fieldName As String) As Object
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numCaseId", CaseID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcFieldName", fieldName, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteScalar(connString, "USP_Cases_GetFieldValue", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub ChangeOrganization(ByVal newDivisionID As Long, ByVal newContactID As Long)
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numCaseId", CaseID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numNewDivisionID", newDivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numNewContactID", newContactID, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_Cases_ChangeOrganization", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub
    End Class
End Namespace

