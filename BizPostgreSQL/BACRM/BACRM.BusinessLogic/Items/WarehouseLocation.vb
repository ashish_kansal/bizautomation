﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.Item
    Public Class WarehouseLocation
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"

        Public Property WLocationID As Long
        Public Property WarehouseID As Long
        Public Property Aisle As String
        Public Property Rack As String
        Public Property Shelf As String
        Public Property Bin As String
        Public Property Location As String
        Public Property IsDefault As Boolean
        Public Property IsSystem As Boolean
        Public Property IsGlobal As Boolean

        Public Property SearchText As String
        Public Property OffsetRecords As Integer
        Public Property PageSize As Integer

#End Region

#Region "Public Methods"

        Public Function SearchLocationByLocationName() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWarehouseID", WarehouseID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcSearchText", SearchText, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@intOffset", OffsetRecords, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPageSize", PageSize, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_WarehouseLocation_SearchByLocationName", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function


#End Region

    End Class
End Namespace

