﻿
Option Explicit On
Option Strict On
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Item
    Public Class CommissionRule
        Inherits BACRM.BusinessLogic.CBusinessBase


        Private _RuleID As Long
        Private _RuleName As String
        Private _RuleType As Short
        Private _byteMode As Short
        Private _RuleAppType As Short
        Private _RuleValue As Long
        Private _RuleDTLID As Long
        Private _DivisionID As Long
        Private _ItemID As Long

        Private _BasedOn As Short
        Public Property BasedOn() As Short
            Get
                Return _BasedOn
            End Get
            Set(ByVal value As Short)
                _BasedOn = value
            End Set
        End Property
        Private _AppliesTo As Short
        Public Property AppliesTo() As Short
            Get
                Return _AppliesTo
            End Get
            Set(ByVal value As Short)
                _AppliesTo = value
            End Set
        End Property
        Private _Duration As Short
        Public Property Duration() As Short
            Get
                Return _Duration
            End Get
            Set(ByVal value As Short)
                _Duration = value
            End Set
        End Property
        Private _CommissionType As Short
        Public Property CommissionType() As Short
            Get
                Return _CommissionType
            End Get
            Set(ByVal value As Short)
                _CommissionType = value
            End Set
        End Property

        Private _strItems As String
        Public Property strItems() As String
            Get
                Return _strItems
            End Get
            Set(ByVal value As String)
                _strItems = value
            End Set
        End Property

        Public Property ItemID() As Long
            Get
                Return _ItemID
            End Get
            Set(ByVal Value As Long)
                _ItemID = Value
            End Set
        End Property

        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property

        Public Property RuleDTLID() As Long
            Get
                Return _RuleDTLID
            End Get
            Set(ByVal Value As Long)
                _RuleDTLID = Value
            End Set
        End Property

        Public Property RuleValue() As Long
            Get
                Return _RuleValue
            End Get
            Set(ByVal Value As Long)
                _RuleValue = Value
            End Set
        End Property

        Public Property RuleAppType() As Short
            Get
                Return _RuleAppType
            End Get
            Set(ByVal Value As Short)
                _RuleAppType = Value
            End Set
        End Property


        Public Property byteMode() As Short
            Get
                Return _byteMode
            End Get
            Set(ByVal Value As Short)
                _byteMode = Value
            End Set
        End Property

        Public Property RuleType() As Short
            Get
                Return _RuleType
            End Get
            Set(ByVal Value As Short)
                _RuleType = Value
            End Set
        End Property


        Public Property RuleName() As String
            Get
                Return _RuleName
            End Get
            Set(ByVal Value As String)
                _RuleName = Value
            End Set
        End Property

        Public Property RuleID() As Long
            Get
                Return _RuleID
            End Get
            Set(ByVal Value As Long)
                _RuleID = Value
            End Set
        End Property


        Private _AssignTo As Short
        Public Property AssignTo() As Short
            Get
                Return _AssignTo
            End Get
            Set(ByVal value As Short)
                _AssignTo = value
            End Set
        End Property

        Private _Owner As Short
        Public Property Owner() As Short
            Get
                Return _Owner
            End Get
            Set(ByVal value As Short)
                _Owner = value
            End Set
        End Property

        Private _numComReports As Long
        Public Property numComReports() As Long
            Get
                Return _numComReports
            End Get
            Set(ByVal value As Long)
                _numComReports = value
            End Set
        End Property

        Private _bitCommContact As Boolean
        Public Property bitCommContact() As Boolean
            Get
                Return _bitCommContact
            End Get
            Set(ByVal value As Boolean)
                _bitCommContact = value
            End Set
        End Property

        Private _tintComOrgType As Short
        Public Property tintComOrgType() As Short
            Get
                Return _tintComOrgType
            End Get
            Set(ByVal value As Short)
                _tintComOrgType = value
            End Set
        End Property

        Private _numProfile As Long
        Public Property Profile() As Long
            Get
                Return _numProfile
            End Get
            Set(ByVal value As Long)
                _numProfile = value
            End Set
        End Property
        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop  Jayaraj 	DATE:30-March-05
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop  Jayaraj 	DATE:30-March-05
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region


        Public Function ManageCommissionRule() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(10) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numComRuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _RuleID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcCommissionName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(1).Value = _RuleName

                arParms(2) = New Npgsql.NpgsqlParameter("@tintComAppliesTo", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _AppliesTo

                arParms(3) = New Npgsql.NpgsqlParameter("@tintComBasedOn", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _BasedOn

                arParms(4) = New Npgsql.NpgsqlParameter("@tinComDuration", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _Duration

                arParms(5) = New Npgsql.NpgsqlParameter("@tintComType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _CommissionType

                arParms(6) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = DomainID

                arParms(7) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(7).Value = _strItems

                arParms(8) = New Npgsql.NpgsqlParameter("@tintAssignTo", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(8).Value = _AssignTo

                arParms(9) = New Npgsql.NpgsqlParameter("@tintComOrgType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(9).Value = _tintComOrgType

                arParms(10) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(10).Value = Nothing
                arParms(10).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_ManageCommissionRule", arParms))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCommissionRule() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numComRuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _RuleID

                arParms(1) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _byteMode

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetCommissionRule", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function DeleteCommissionRule() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numComRuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _RuleID

                SqlDAL.ExecuteNonQuery(connString, "usp_DeleteCommissionRule", arParms)

                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function GetCommissionRuleItems() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numRuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _RuleID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintRuleAppType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _RuleAppType

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetCommissionRuleItems", arParms)

                Return ds
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function ManageCommissionRuleItems() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numComRuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _RuleID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintAppRuleType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = RuleAppType

                arParms(2) = New Npgsql.NpgsqlParameter("@numValue", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _RuleValue

                arParms(3) = New Npgsql.NpgsqlParameter("@numComRuleItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _RuleDTLID

                arParms(4) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _byteMode

                arParms(5) = New Npgsql.NpgsqlParameter("@numProfile", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _numProfile

                SqlDAL.ExecuteScalar(connString, "USP_ManageCommissionRuleItems", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function GetCommissionTable() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numComRuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _RuleID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetCommissionTable", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetCommissionRuleContacts() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numComRuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _RuleID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetCommissionRuleContacts", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function ManageCommissionReports() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ItemID

                arParms(1) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntId

                arParms(2) = New Npgsql.NpgsqlParameter("@tintAssignTo", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _AssignTo

                arParms(3) = New Npgsql.NpgsqlParameter("@bitCommContact", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Value = _bitCommContact

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = DomainID
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageCommissionReports", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCommissionReports() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@bitCommContact", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(2).Value = _bitCommContact

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetCommissionReports", arParms)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Function DeleteCommissionReports() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numComReports", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _numComReports

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteCommissionReports", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace
