﻿Imports BACRM.BusinessLogic.Common
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.Data

Public Class ItemUOMConversion
    Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"
    Public Property numItemUOMConvID As Long
    Public Property numItemCode As Long
    Public Property numSourceUOM As Long
    Public Property SourceUOM As String
    Public Property numTargetUOM As Long
    Public Property TargetUOM As String
    Public Property numTargetUnit As Double
    Public Property numCreatedBy As Long
    Public Property CreatedBy As String
    Public Property numModifiedBy As Long
    Public Property ModifiedBy As String
    Public Property CreatedDate As DateTime
    Public Property ModifiedDate As DateTime
#End Region

#Region "Private Methods"
    Public Sub Save()
        Dim getconnection As New GetConnection
        Dim connString As String = getconnection.GetConnectionString

        Try
            Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

            With sqlParams
                .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                .Add(SqlDAL.Add_Parameter("@numItemCode", numItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                .Add(SqlDAL.Add_Parameter("@numSourceUOM", numSourceUOM, NpgsqlTypes.NpgsqlDbType.BigInt))
                .Add(SqlDAL.Add_Parameter("@numTargetUOM", numTargetUOM, NpgsqlTypes.NpgsqlDbType.BigInt))
                .Add(SqlDAL.Add_Parameter("@numTargetUnit", numTargetUnit, NpgsqlTypes.NpgsqlDbType.Numeric))
            End With

            SqlDAL.ExecuteNonQuery(connString, "USP_ItemUOMConversion_Save", sqlParams.ToArray())
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub Delete()
        Dim getconnection As New GetConnection
        Dim connString As String = getconnection.GetConnectionString

        Try
            Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

            With sqlParams
                .Add(SqlDAL.Add_Parameter("@numItemUOMConvID", numItemUOMConvID, NpgsqlTypes.NpgsqlDbType.BigInt))
            End With

            SqlDAL.ExecuteNonQuery(connString, "USP_ItemUOMConversion_Delete", sqlParams.ToArray())
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Function GetUOMConversionByfromtoconversion(ByVal Action As String, ByVal DDomainID As Long, ByVal fromUom As Long, ByVal toUom As Long, ByVal DItemCode As Long) As DataTable
        Dim getconnection As New GetConnection
        Dim connString As String = getconnection.GetConnectionString
        Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

        With sqlParams
            .Add(SqlDAL.Add_Parameter("@chrAction", Action, NpgsqlTypes.NpgsqlDbType.Char))
            .Add(SqlDAL.Add_Parameter("@numDomainID", DDomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
            .Add(SqlDAL.Add_Parameter("@fromUOM", fromUom, NpgsqlTypes.NpgsqlDbType.BigInt))
            .Add(SqlDAL.Add_Parameter("@toUOM", toUom, NpgsqlTypes.NpgsqlDbType.BigInt))
            .Add(SqlDAL.Add_Parameter("@numItemCode", DItemCode, NpgsqlTypes.NpgsqlDbType.Bigint))
            .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
        End With

        Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_GetUOMConversionByfromtoconversion", sqlParams.ToArray())
        Dim dt As New DataTable()
        dt = ds.Tables(0)
        Return dt
    End Function

    Public Function GetAllByItem() As Object
        Dim itemBaseUOM As Long = 0
        Dim listItemUOMConversion As New List(Of ItemUOMConversion)
        Dim getconnection As New GetConnection
        Dim connString As String = getconnection.GetConnectionString

        Try
            Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

            With sqlParams
                .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                .Add(SqlDAL.Add_Parameter("@numItemCode", numItemCode, NpgsqlTypes.NpgsqlDbType.Bigint))
                .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
            End With

            Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_ItemUOMConversion_GetAllByItem", sqlParams.ToArray())

            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    itemBaseUOM = CCommon.ToLong(ds.Tables(0).Rows(0)(0))
                End If

                If ds.Tables.Count > 1 AndAlso ds.Tables(1).Rows.Count > 0 Then
                    Dim objItemUOMConversion As ItemUOMConversion

                    For Each dr As DataRow In ds.Tables(1).Rows
                        objItemUOMConversion = New ItemUOMConversion

                        objItemUOMConversion.numItemUOMConvID = IIf(dr("numItemUOMConvID") Is DBNull.Value, 0, Convert.ToInt64(dr("numItemUOMConvID")))
                        objItemUOMConversion.DomainID = IIf(dr("numDomainID") Is DBNull.Value, 0, Convert.ToInt64(dr("numDomainID")))
                        objItemUOMConversion.numItemCode = IIf(dr("numItemCode") Is DBNull.Value, 0, Convert.ToInt64(dr("numItemCode")))
                        objItemUOMConversion.numSourceUOM = IIf(dr("numSourceUOM") Is DBNull.Value, 0, Convert.ToInt64(dr("numSourceUOM")))
                        objItemUOMConversion.SourceUOM = IIf(dr("SourceUOM") Is DBNull.Value, String.Empty, Convert.ToString(dr("SourceUOM")))
                        objItemUOMConversion.numTargetUOM = IIf(dr("numTargetUOM") Is DBNull.Value, 0, Convert.ToInt64(dr("numTargetUOM")))
                        objItemUOMConversion.TargetUOM = IIf(dr("TargetUOM") Is DBNull.Value, String.Empty, Convert.ToString(dr("TargetUOM")))
                        objItemUOMConversion.numTargetUnit = IIf(dr("numTargetUnit") Is DBNull.Value, 0.0, Convert.ToDouble(dr("numTargetUnit")))

                        listItemUOMConversion.Add(objItemUOMConversion)
                    Next
                End If

            End If

            Return New With {Key .BaseUOM = itemBaseUOM, Key .ListItemUOMConversion = listItemUOMConversion}
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function CheckIFItemUOMConversionExists() As Boolean
        Dim getconnection As New GetConnection
        Dim connString As String = getconnection.GetConnectionString

        Try
            Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

            With sqlParams
                .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                .Add(SqlDAL.Add_Parameter("@numItemCode", numItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                .Add(SqlDAL.Add_Parameter("@numSourceUnit", numSourceUOM, NpgsqlTypes.NpgsqlDbType.Bigint))
                .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
            End With

            Return CCommon.ToBool(SqlDAL.ExecuteScalar(connString, "USP_ItemUOMConversion_CheckConversionExists", sqlParams.ToArray()))
        Catch ex As Exception
            Throw
        End Try
    End Function
#End Region

End Class
