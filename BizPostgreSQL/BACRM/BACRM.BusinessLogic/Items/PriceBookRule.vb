'Created Anoop Jayaraj
Option Explicit On
Option Strict On
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Item
    Public Class PriceBookRule
        Inherits BACRM.BusinessLogic.CBusinessBase


        Private _RuleID As Long
        Private _RuleName As String
        Private _RuleType As Short
        Private _DedPercentage As Double
        Private _DiscountAmount As Decimal
        Private _boolVolDisc As Boolean
        Private _QntyofItems As Integer
        Private _MaxDedPerAmt As Decimal
        Private _boolTillDate As Boolean
        Private _dtTillDate As Date
        Private _byteMode As Short
        'Private DomainId As Long
        Private _RuleAppType As Short
        Private _RuleValue As Long
        Private _Profile As Long
        Private _RuleDTLID As Long
        Private _DivisionID As Long
        Private _ItemID As Long
        Private _boolSort As Boolean

        Private _WarehouseID As Long
        Public Property WarehouseID() As Long
            Get
                Return _WarehouseID
            End Get
            Set(ByVal value As Long)
                _WarehouseID = value
            End Set
        End Property

        Private _RuleDescription As String
        Public Property RuleDescription() As String
            Get
                Return _RuleDescription
            End Get
            Set(ByVal value As String)
                _RuleDescription = value
            End Set
        End Property

        Private _PricingMethod As Short
        Public Property PricingMethod() As Short
            Get
                Return _PricingMethod
            End Get
            Set(ByVal value As Short)
                _PricingMethod = value
            End Set
        End Property

        Private _DiscountType As Short
        Public Property DiscountType() As Short
            Get
                Return _DiscountType
            End Get
            Set(ByVal value As Short)
                _DiscountType = value
            End Set
        End Property


        Private _Step2 As Short
        Public Property Step2() As Short
            Get
                Return _Step2
            End Get
            Set(ByVal value As Short)
                _Step2 = value
            End Set
        End Property

        Private _Step3 As Short
        Public Property Step3() As Short
            Get
                Return _Step3
            End Get
            Set(ByVal value As Short)
                _Step3 = value
            End Set
        End Property


        Private _strItems As String
        Public Property strItems() As String
            Get
                Return _strItems
            End Get
            Set(ByVal value As String)
                _strItems = value
            End Set
        End Property

        Public Property boolSort() As Boolean
            Get
                Return _boolSort
            End Get
            Set(ByVal Value As Boolean)
                _boolSort = Value
            End Set
        End Property


        Public Property ItemID() As Long
            Get
                Return _ItemID
            End Get
            Set(ByVal Value As Long)
                _ItemID = Value
            End Set
        End Property


        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property


        Public Property RuleDTLID() As Long
            Get
                Return _RuleDTLID
            End Get
            Set(ByVal Value As Long)
                _RuleDTLID = Value
            End Set
        End Property


        Public Property Profile() As Long
            Get
                Return _Profile
            End Get
            Set(ByVal Value As Long)
                _Profile = Value
            End Set
        End Property


        Public Property RuleValue() As Long
            Get
                Return _RuleValue
            End Get
            Set(ByVal Value As Long)
                _RuleValue = Value
            End Set
        End Property


        Public Property RuleAppType() As Short
            Get
                Return _RuleAppType
            End Get
            Set(ByVal Value As Short)
                _RuleAppType = Value
            End Set
        End Property


        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property


        Public Property byteMode() As Short
            Get
                Return _byteMode
            End Get
            Set(ByVal Value As Short)
                _byteMode = Value
            End Set
        End Property


        Public Property dtTillDate() As Date
            Get
                Return _dtTillDate
            End Get
            Set(ByVal Value As Date)
                _dtTillDate = Value
            End Set
        End Property


        Public Property boolTillDate() As Boolean
            Get
                Return _boolTillDate
            End Get
            Set(ByVal Value As Boolean)
                _boolTillDate = Value
            End Set
        End Property


        Public Property MaxDedPerAmt() As Decimal
            Get
                Return _MaxDedPerAmt
            End Get
            Set(ByVal Value As Decimal)
                _MaxDedPerAmt = Value
            End Set
        End Property


        Public Property QntyofItems() As Integer
            Get
                Return _QntyofItems
            End Get
            Set(ByVal Value As Integer)
                _QntyofItems = Value
            End Set
        End Property


        'Public Property boolVolDisc() As Boolean
        '    Get
        '        Return _boolVolDisc
        '    End Get
        '    Set(ByVal Value As Boolean)
        '        _boolVolDisc = Value
        '    End Set
        'End Property


        Public Property DiscountAmount() As Decimal
            Get
                Return _DiscountAmount
            End Get
            Set(ByVal Value As Decimal)
                _DiscountAmount = Value
            End Set
        End Property


        'Public Property DedPercentage() As Double
        '    Get
        '        Return _DedPercentage
        '    End Get
        '    Set(ByVal Value As Double)
        '        _DedPercentage = Value
        '    End Set
        'End Property


        Public Property RuleType() As Short
            Get
                Return _RuleType
            End Get
            Set(ByVal Value As Short)
                _RuleType = Value
            End Set
        End Property


        Public Property RuleName() As String
            Get
                Return _RuleName
            End Get
            Set(ByVal Value As String)
                _RuleName = Value
            End Set
        End Property

        Public Property RuleID() As Long
            Get
                Return _RuleID
            End Get
            Set(ByVal Value As Long)
                _RuleID = Value
            End Set
        End Property

        Private _RuleFor As Short
        Public Property RuleFor() As Short
            Get
                Return _RuleFor
            End Get
            Set(ByVal Value As Short)
                _RuleFor = Value
            End Set
        End Property

        Public Property WarehouseItemID As Long
        Public Property KitSelectedChild As String
        Public Property CurrencyID As Long
        Public Property SiteID As Long
        Public Property IsRoundTo As Boolean
        Public Property RoundTo As Short

        Public Property VendorCostType As Short
        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop  Jayaraj 	DATE:30-March-05
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop  Jayaraj 	DATE:30-March-05
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region


        Public Function ManagePriceBookRule() As Long
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numPricRuleID", _RuleID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@vcRuleName", _RuleName, NpgsqlTypes.NpgsqlDbType.Varchar, 100))
                    .Add(SqlDAL.Add_Parameter("@vcRuleDescription", _RuleDescription, NpgsqlTypes.NpgsqlDbType.Varchar, 100))
                    .Add(SqlDAL.Add_Parameter("@tintRuleType", _RuleType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@decflatAmount", _DiscountAmount, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@intQntyItems", _QntyofItems, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@decMaxDedPerAmt", _MaxDedPerAmt, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@tintPricingMethod", _PricingMethod, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@tintDiscountType", _DiscountType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@tintStep2", _Step2, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@tintStep3", _Step3, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@tintRuleFor", _RuleFor, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@bitRoundTo", IsRoundTo, NpgsqlTypes.NpgsqlDbType.Boolean))
                    .Add(SqlDAL.Add_Parameter("@tintRoundTo", RoundTo, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@tintVendorCostType", VendorCostType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_ManagePriceBookRules", sqlParams.ToArray()))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetPriceBookRule() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numPricRuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _RuleID

                arParms(1) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _byteMode

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainId

                arParms(3) = New Npgsql.NpgsqlParameter("@bitSort", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Value = _boolSort

                arParms(4) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _ItemID

                arParms(5) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _DivisionID

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetPriceBookRule", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function DelPriceBookRule() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numPricRuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _RuleID

                SqlDAL.ExecuteNonQuery(connString, "USP_DelPriceBookRule", arParms)

                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function GetPriceBookRuleDTL() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numRuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _RuleID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintRuleAppType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _RuleAppType

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetDataForPricebookRule", arParms)

                Return ds
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function ManagePriceBookRuleDTL() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numPricRuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _RuleID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintAppRuleType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = RuleAppType

                arParms(2) = New Npgsql.NpgsqlParameter("@numValue", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _RuleValue

                arParms(3) = New Npgsql.NpgsqlParameter("@numProfile", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _Profile

                arParms(4) = New Npgsql.NpgsqlParameter("@numPriceRuleDTLID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _RuleDTLID

                arParms(5) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _byteMode

                SqlDAL.ExecuteScalar(connString, "USP_ManagePriceBookRulesDTL", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function



        Public Function GetPriceBasedonPriceBook() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(10) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ItemID

                arParms(1) = New Npgsql.NpgsqlParameter("@intQuantity", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _QntyofItems

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainId

                arParms(3) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _DivisionID

                arParms(4) = New Npgsql.NpgsqlParameter("@numWarehouseID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _WarehouseID

                arParms(5) = New Npgsql.NpgsqlParameter("@numWarehouseItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = WarehouseItemID

                arParms(6) = New Npgsql.NpgsqlParameter("@vcSelectedKitChildItems", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(6).Value = KitSelectedChild

                arParms(7) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(7).Value = SiteId

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                arParms(9) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(9).Value = Nothing
                arParms(9).Direction = ParameterDirection.InputOutput

                arParms(10) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(10).Value = Nothing
                arParms(10).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetPriceOfItem", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Sub ManagePriceTable()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numPriceRuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _RuleID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ItemID

                arParms(2) = New Npgsql.NpgsqlParameter("@numCurrencyID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = CurrencyID

                arParms(3) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(3).Value = _strItems

                SqlDAL.ExecuteScalar(connString, "USP_ManagePricingTable", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub ManagePriceNamesTable()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numCreatedBy", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(2).Value = _strItems

                SqlDAL.ExecuteScalar(connString, "USP_ManagePricingNamesTable", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function GetPriceTable() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numPriceRuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _RuleID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ItemID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@numCurrencyID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = CurrencyID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetPriceTable", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetPriceNamesTable() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetPriceNamesTable", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

    End Class
End Namespace