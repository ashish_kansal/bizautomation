﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.Item
    Public Class ItemWarehouse
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"

        Public Property ItemCode As Long
        Public Property WarehouseItemID As Long
        Public Property WarehouseID As Long
        Public Property InternalLocationID As Long

#End Region


#Region "Public Methods"

        Public Sub ChangeInternalLocation()
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numItemCode", ItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWarehouseItemID", WarehouseItemID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWarehouseID", WarehouseID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numLocationID", InternalLocationID, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_WarehouseItems_ChangeInternalLocation", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub ClearInternalLocation()
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWarehouseItemID", WarehouseItemID, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_WarehouseItems_ClearInternalLocation", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetWarehouses() As DataTable
            Try
                Try
                    Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                    Dim getconnection As New GetConnection
                    Dim connString As String = getconnection.GetConnectionString

                    With sqlParams
                        .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                        .Add(SqlDAL.Add_Parameter("@numItemCode", ItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                        .Add(SqlDAL.Add_Parameter("@numWarehouseID", WarehouseID, NpgsqlTypes.NpgsqlDbType.BigInt))
                        .Add(SqlDAL.Add_Parameter("@numWLocationID", InternalLocationID, NpgsqlTypes.NpgsqlDbType.Bigint))
                        .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    End With

                    Return SqlDAL.ExecuteDatable(connString, "USP_WarehouseItems_GetByItem", sqlParams.ToArray())
                Catch ex As Exception
                    Throw
                End Try
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub BulkRemove()
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_WarehouseItems_BulkRemove", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub
#End Region

    End Class
End Namespace