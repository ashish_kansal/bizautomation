﻿Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRM.BusinessLogic.Common

Namespace BACRM.BusinessLogic.Item

    Public Class AssembledItem
        Inherits BACRM.BusinessLogic.CBusinessBase
#Region "Enum"
        Public Enum AssemblyType As Short
            Build = 1
            WorkOrder = 2
        End Enum
#End Region

#Region "Public Properties"
        Public Property ID As Long
        Public Property ItemCode As Long
        Public Property WarehouseItemID As Long
        Public Property AssembledQty As Long
        Public Property ClientTimeZoneOffset As Integer
        Public Property Type As AssemblyType
#End Region

#Region "Public Methods"
        Public Function Save() As Long
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = ItemCode

                arParms(3) = New Npgsql.NpgsqlParameter("@numWarehouseItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = WarehouseItemID

                arParms(4) = New Npgsql.NpgsqlParameter("@numQty", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = AssembledQty

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Return CCommon.ToLong(SqlDAL.ExecuteScalar(connString, "USP_AssembledItem_Save", arParms))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub Delete()
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@ID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = ID

                arParms(1) = New Npgsql.NpgsqlParameter("@numQty", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = AssembledQty

                SqlDAL.ExecuteNonQuery(connString, "USP_AssembledItem_Delete", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetByID() As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@ID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = ID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_AssembledItem_GetByID", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetByItemCode() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = ItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = ClientTimeZoneOffset

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_AssembledItem_GetByItemCode", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub DisassembleItem()
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@ID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = ID

                arParms(3) = New Npgsql.NpgsqlParameter("@numQty", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = AssembledQty

                arParms(4) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = Convert.ToInt16(Type)

                SqlDAL.ExecuteNonQuery(connString, "USP_AssembledItem_Disassemble", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub
#End Region

    End Class

End Namespace