﻿Namespace BACRM.BusinessLogic.Item

    Public Class ESItemWarehouseDetails
#Region "Public Properties"

        Private _numItemID As Decimal
        Public Property numItemID() As Decimal
            Get
                Return _numItemID
            End Get
            Set(ByVal value As Decimal)
                _numItemID = value
            End Set
        End Property

        Private _vcWarehouse As String
        Public Property vcWarehouse() As String
            Get
                Return _vcWarehouse
            End Get
            Set(ByVal value As String)
                _vcWarehouse = value
            End Set
        End Property

        Private _numWarehouseItemID As Decimal
        Public Property numWarehouseItemID() As Decimal
            Get
                Return _numWarehouseItemID
            End Get
            Set(ByVal value As Decimal)
                _numWarehouseItemID = value
            End Set
        End Property

        Private _numWareHouseID As Decimal
        Public Property numWareHouseID() As Decimal
            Get
                Return _numWareHouseID
            End Get
            Set(ByVal value As Decimal)
                _numWareHouseID = value
            End Set
        End Property

        Private _numOnHand As Double
        Public Property numOnHand() As Double
            Get
                Return _numOnHand
            End Get
            Set(ByVal value As Double)
                _numOnHand = value
            End Set
        End Property

        Private _numAllocation As Double
        Public Property numAllocation() As Double
            Get
                Return _numAllocation
            End Get
            Set(ByVal value As Double)
                _numAllocation = value
            End Set
        End Property
#End Region

    End Class

    Public Class ESItemCategoryDetails
#Region "Public Properties"

        Private _numItemID As Decimal
        Public Property numItemID() As Decimal
            Get
                Return _numItemID
            End Get
            Set(ByVal value As Decimal)
                _numItemID = value
            End Set
        End Property

        Private _numCategoryID As Decimal
        Public Property numCategoryID() As Decimal
            Get
                Return _numCategoryID
            End Get
            Set(ByVal value As Decimal)
                _numCategoryID = value
            End Set
        End Property

        Private _vcCategoryName As String
        Public Property vcCategoryName() As String
            Get
                Return _vcCategoryName
            End Get
            Set(ByVal value As String)
                _vcCategoryName = value
            End Set
        End Property

        Private _CategoryDesc As String
        Public Property CategoryDesc() As String
            Get
                Return _CategoryDesc
            End Get
            Set(ByVal value As String)
                _CategoryDesc = value
            End Set
        End Property
#End Region

    End Class
End Namespace