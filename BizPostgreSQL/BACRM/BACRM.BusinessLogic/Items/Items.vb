'**********************************************************************************
' <CItems.vb>
' 
' 	CHANGE CONTROL:
'	
'	AUTHOR: Goyal 	DATE:28-Feb-05 	VERSION	CHANGES	KEYSTRING:
'**********************************************************************************


Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Runtime.Serialization
Imports System.Web
Imports BACRM.BusinessLogic.Common
Imports System.Collections.Generic
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Opportunities

Namespace BACRM.BusinessLogic.Item

    '**********************************************************************************
    ' Module Name  : None
    ' Module Type  : CCommon
    ' 
    ' Description  : This is the business logic classe for Common Stuff
    '**********************************************************************************
    <DataContract()>
    Public Class CItems
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Constructor"
        '**********************************************************************************
        ' Name         : New
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal intUserId As Int32               
        ' Description  : The constructor                
        ' Notes        : N/A                
        ' Created By   : Goyal 	DATE:28-Feb-05
        '**********************************************************************************
        'Public Sub New(ByVal intUserId As Integer)
        '    'Constructor
        '    MyBase.New(intUserId)
        'End Sub

        '**********************************************************************************
        ' Name         : New
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal intUserId As Int32              
        ' Description  : The constructor                
        ' Notes        : N/A                
        ' Created By   : Goyal 	DATE:28-Feb-05
        '**********************************************************************************
        'Public Sub New()
        '    'Constructor
        '    MyBase.New()
        'End Sub
#End Region

        Private _ItemCode As Integer = 0
        Private _CustomerPartNo As String = String.Empty
        Private _ItemName As String = String.Empty
        Private _ItemDesc As String = String.Empty
        Private _ItemType As String = String.Empty
        Private _CurrentPage As Integer = 0
        Private _PageSize As Integer = 0
        Private _TotalRecords As Integer = 0
        Private _TotalPages As Long = 1
        Private _ListPrice As Double
        Private _ItemClassification As Long
        Private _Taxable As Boolean
        Private _SKU As String
        Private _KitParent As Boolean
        Private _Container As Boolean
        Private _NoofItemsReqForKit As Long
        Private _DateEntered As Date = New Date(1753, 1, 1)
        Private _OnHand As Double
        Private _OnOrder As Double
        Private _ReOrder As Double
        Private _BackOrder As Double
        Private _KitID As Long
        Private _SortCharacter As Char
        Private _columnSortOrder As String
        Private _columnName As String
        Private _KeyWord As String
        Private _DivisionID As Long
        Private _ManufacturerID As Long
        Private _VendorID As Long
        Private _strFieldList As String
        Private _NoofUnits As Decimal
        Private _CompetitorID As Long
        Private _OnAllocation As Double
        Private _PathForImage As String
        Private _PathForTImage As String
        Private _IsImage As Boolean
        Private _OppId As Long
        Private _ContactID As Long
        Private _OppName As String
        Private _OppItemCode As Long
        Private _ReceivedDate As Long
        Private _byteMode As Short
        Private _Tax As Decimal
        Private _Amount As Decimal
        Private _RelID As Long
        Private _ProfileID As Long
        Private _DiscProfileID As Long
        Private _Disc As Decimal
        Private _Applicable As Boolean
        Private _CategoryID As Long
        Private _CategoryName As String
        Private _SubCategoryID As Long
        Private _ParentCategoryID As Long
        Private _Checked As Short
        Private _str As String
        Private _wStreet As String
        Private _wCity As String
        Private _wPinccode As String
        Private _WarehouseID As Long
        Private _wState As Long
        Private _wCountry As Long
        Private _Priority As Long
        Private _extType As Short
        Private _ItemGroupID As Long
        Private _ItemGroupName As String
        Private _bitSerialized As Boolean
        Private _ModelID As String
        Private _WareHouseItemID As Long
        Private _strItemCodes As String
        Private _COGSChartAcntId As Integer
        Private _AssetChartAcntId As Integer
        Private _IncomeChartAcntId As Integer
        Private _Warehouse As String
        Private _AverageCost As Decimal
        Private _LabourCost As Decimal
        Private _Country As Long = 0
        Private _State As Long = 0
        Private _mode As Boolean = False
        Private _WareHouseDetailID As Long
        Private _FromPinCode As Integer = 0
        Private _ToPinCode As Integer = 0
        Private _Weight As Double = 0
        Private _Height As Double = 0
        Private _Length As Double = 0
        Private _Width As Double = 0
        Private _FreeShipping As Boolean
        Private _AllowBackOrder As Boolean
        Private _ShippingCost As Decimal
        Private _ClientZoneOffsetTime As Integer
        Private _UnitofMeasure As String
        Private _strChildItems As String
        Private _ShowDeptItem As Boolean
        Private _ShowDeptItemDesc As Boolean
        Private _CalPriceBasedOnIndItems As Boolean
        Private _Assembly As Boolean
        Private _OppBizDocID As Long
        Private _ShippingCMPID As Long
        Private _strItemTaxTypes As String
        Private _Source As String
        Private _CampaignID As Integer
        Private _BarCodeID As String
        Private _DepartmentID As Integer
        Private _Appreciation As Short
        Private _Deppreciation As Short
        Private _AppreValue As Double
        Private _DepreValue As Double
        Private _PurchaseDate As String
        Private _WarrentyTill As String
        Private _WLocation As String
        Private _WListPrice As Double
        Private _WOnHand As Double
        Private _Manufacturer As String
        Private _WSKU As String
        Private _WBarCode As String
        Private _tintMode As Integer
        Private _ReferenceID As Double
        Private _RefType As Integer
        Private _Description As String
        Private _txtShortDesc As String
        'Added by Sachin Sadhu||30Dec2013
        'To search Order date wise
        Private _StartDate As Date


        Private _EndDate As Date
        'Private _StartDate As Nullable(Of Date) = Nothing
        'Private _EndDate As Nullable(Of Date) = Nothing
        Private _bitPrimary As Boolean
        Private _numContainer As Long
        Private _numNoItemIntoContainer As Long
        Private _bitCombineAssemblies As Boolean
        Private _numMapToDropdownFld As Long
        Private _numProfileItem As Long
        Private _numListid As Long
        Private _numSortOrder As Long

        Private _numUserCntID As Long
        Private _vcCookieId As String
        Private _vcPartenerContact As String
        Private _numShipmentMethod As Long
        Private _numShowOnWebFieldId As Long
        Private _bitMultiSelect As Boolean
        Private _vcAsile As String
        Private _vcRack As String
        Private _vcShelf As String
        Private _vcBin As String
        Private _bitApplyFilter As Boolean

        Public Property bitApplyFilter() As Boolean
            Get
                Return _bitApplyFilter
            End Get
            Set(value As Boolean)
                _bitApplyFilter = value
            End Set
        End Property

        Public Property vcAsile() As String
            Get
                Return _vcAsile
            End Get
            Set(value As String)
                _vcAsile = value
            End Set
        End Property

        Public Property vcShelf() As String
            Get
                Return _vcShelf
            End Get
            Set(value As String)
                _vcShelf = value
            End Set
        End Property

        Public Property vcRack() As String
            Get
                Return _vcRack
            End Get
            Set(value As String)
                _vcRack = value
            End Set
        End Property

        Public Property vcBin() As String
            Get
                Return _vcBin
            End Get
            Set(value As String)
                _vcBin = value
            End Set
        End Property

        Public Property numShowOnWebFieldId() As Long
            Get
                Return _numShowOnWebFieldId
            End Get
            Set(value As Long)
                _numShowOnWebFieldId = value
            End Set
        End Property


        Public Property numShipmentMethod() As Long
            Get
                Return _numShipmentMethod
            End Get
            Set(value As Long)
                _numShipmentMethod = value
            End Set
        End Property

        Public Property vcPartenerContact() As String
            Get
                Return _vcPartenerContact
            End Get
            Set(value As String)
                _vcPartenerContact = value
            End Set
        End Property
        Public Property txtShortDesc() As String
            Get
                Return _txtShortDesc
            End Get
            Set(value As String)
                _txtShortDesc = value
            End Set
        End Property
        Public Property vcCookieId() As String
            Get
                Return _vcCookieId
            End Get
            Set(value As String)
                _vcCookieId = value
            End Set
        End Property
        Public Property bitCombineAssemblies() As Boolean
            Get
                Return _bitCombineAssemblies
            End Get
            Set(value As Boolean)
                _bitCombineAssemblies = value
            End Set
        End Property

        Public Property numSortOrder() As Long
            Get
                Return _numSortOrder
            End Get
            Set(value As Long)
                _numSortOrder = value
            End Set
        End Property

        Public Property numMapToDropdownFld() As Long
            Get
                Return _numMapToDropdownFld
            End Get
            Set(value As Long)
                _numMapToDropdownFld = value
            End Set
        End Property

        Public Property numProfileItem() As Long
            Get
                Return _numProfileItem
            End Get
            Set(value As Long)
                _numProfileItem = value
            End Set
        End Property

        Public Property numListid() As Long
            Get
                Return _numListid
            End Get
            Set(value As Long)
                _numListid = value
            End Set
        End Property

        Public Property numContainer() As Long
            Get
                Return _numContainer
            End Get
            Set(value As Long)
                _numContainer = value
            End Set
        End Property

        Public Property numNoItemIntoContainer() As Long
            Get
                Return _numNoItemIntoContainer
            End Get
            Set(value As Long)
                _numNoItemIntoContainer = value
            End Set
        End Property

        Public Property bitPrimary() As Boolean
            Get
                Return _bitPrimary
            End Get
            Set(value As Boolean)
                _bitPrimary = value
            End Set
        End Property
        Public Property Description() As String
            Get
                Return _Description
            End Get
            Set(ByVal value As String)
                _Description = value
            End Set
        End Property
        Public Property RefType() As Integer
            Get
                Return _RefType
            End Get
            Set(ByVal value As Integer)
                _RefType = value
            End Set
        End Property
        Public Property ReferenceID() As Long
            Get
                Return _ReferenceID
            End Get
            Set(ByVal value As Long)
                _ReferenceID = value
            End Set
        End Property
        Public Property tintMode() As Integer
            Get
                Return _tintMode
            End Get
            Set(ByVal value As Integer)
                _tintMode = value
            End Set
        End Property
        Private _ItemDetailID As Long
        Public Property ItemDetailID() As Long
            Get
                Return _ItemDetailID
            End Get
            Set(ByVal value As Long)
                _ItemDetailID = value
            End Set
        End Property

        Private _IsPrimaryVendor As Boolean
        Public Property IsPrimaryVendor() As Boolean
            Get
                Return _IsPrimaryVendor
            End Get
            Set(ByVal value As Boolean)
                _IsPrimaryVendor = value
            End Set
        End Property

        Public Property WSKU() As String
            Get
                Return _WSKU
            End Get
            Set(ByVal value As String)
                _WSKU = value
            End Set
        End Property

        Public Property bitContainer() As Boolean
            Get
                Return _Container
            End Get
            Set(value As Boolean)
                _Container = value
            End Set
        End Property

        Public Property WBarCode() As String
            Get
                Return _WBarCode
            End Get
            Set(ByVal value As String)
                _WBarCode = value
            End Set
        End Property
        Public Property Manufacturer() As String
            Get
                Return _Manufacturer
            End Get
            Set(ByVal value As String)
                _Manufacturer = value
            End Set
        End Property
        Private _BizDocID As Long
        Public Property BizDocID() As Long
            Get
                Return _BizDocID
            End Get
            Set(ByVal value As Long)
                _BizDocID = value
            End Set
        End Property
        Public Property WLocation() As String
            Get
                Return _WLocation
            End Get
            Set(ByVal value As String)
                _WLocation = value
            End Set
        End Property

        Public Property WListPrice() As Double
            Get
                Return _WListPrice
            End Get
            Set(ByVal value As Double)
                _WListPrice = value
            End Set
        End Property

        Public Property WOnHand() As Double
            Get
                Return _WOnHand
            End Get
            Set(ByVal value As Double)
                _WOnHand = value
            End Set
        End Property

        Public Property WarrentyTill() As String
            Get
                Return _WarrentyTill
            End Get
            Set(ByVal value As String)
                _WarrentyTill = value
            End Set
        End Property
        Public Property PurchaseDate() As String
            Get
                Return _PurchaseDate
            End Get
            Set(ByVal value As String)
                _PurchaseDate = value
            End Set
        End Property
        Public Property DepreValue() As Double
            Get
                Return _DepreValue
            End Get
            Set(ByVal value As Double)
                _DepreValue = value
            End Set
        End Property
        Public Property AppreValue() As Double
            Get
                Return _AppreValue
            End Get
            Set(ByVal value As Double)
                _AppreValue = value
            End Set
        End Property
        Public Property Deppreciation() As Short
            Get
                Return _Deppreciation
            End Get
            Set(ByVal value As Short)
                _Deppreciation = value
            End Set
        End Property
        Public Property Appreciation() As Short
            Get
                Return _Appreciation
            End Get
            Set(ByVal value As Short)
                _Appreciation = value
            End Set
        End Property
        Public Property DepartmentID() As Integer
            Get
                Return _DepartmentID
            End Get
            Set(ByVal value As Integer)
                _DepartmentID = value
            End Set
        End Property


        Public Property BarCodeID() As String
            Get
                Return _BarCodeID
            End Get
            Set(ByVal value As String)
                _BarCodeID = value
            End Set
        End Property

        Public Property CampaignID() As Integer
            Get
                Return _CampaignID
            End Get
            Set(ByVal value As Integer)
                _CampaignID = value
            End Set
        End Property
        Private _SiteID As Long
        Public Property SiteID() As Long
            Get
                Return _SiteID
            End Get
            Set(ByVal value As Long)
                _SiteID = value
            End Set
        End Property
        Private _OppStatus As Integer
        Public Property OppStatus() As Integer
            Get
                Return _OppStatus
            End Get
            Set(ByVal value As Integer)
                _OppStatus = value
            End Set
        End Property
        Private _tintSource As Integer
        Public Property tintSource() As Integer
            Get
                Return _tintSource
            End Get
            Set(ByVal value As Integer)
                _tintSource = value
            End Set
        End Property
        Private _SourceType As Integer
        Public Property SourceType() As Integer
            Get
                Return _SourceType
            End Get
            Set(ByVal value As Integer)
                _SourceType = value
            End Set
        End Property

        Private _SortBy As Short
        Public Property SortBy() As Short
            Get
                Return _SortBy
            End Get
            Set(ByVal value As Short)
                _SortBy = value
            End Set
        End Property

        Private _SortByString As String
        Public Property SortByString() As String
            Get
                Return _SortByString
            End Get
            Set(ByVal value As String)
                _SortByString = value
            End Set
        End Property

        Private _SearchText As String
        Public Property SearchText() As String
            Get
                Return _SearchText
            End Get
            Set(ByVal value As String)
                _SearchText = value
            End Set
        End Property
        Private _AdvancedSearchText As String
        Public Property AdvancedSearchText() As String
            Get
                Return _AdvancedSearchText
            End Get
            Set(ByVal value As String)
                _AdvancedSearchText = value
            End Set
        End Property
        Public Property Source() As String
            Get
                Return _Source
            End Get
            Set(ByVal value As String)
                _Source = value
            End Set
        End Property


        Public Property strItemTaxTypes() As String
            Get
                Return _strItemTaxTypes
            End Get
            Set(ByVal Value As String)
                _strItemTaxTypes = Value
            End Set
        End Property

        Public Property ShippingCMPID() As Long
            Get
                Return _ShippingCMPID
            End Get
            Set(ByVal Value As Long)
                _ShippingCMPID = Value
            End Set
        End Property

        Public Property OppBizDocID() As Long
            Get
                Return _OppBizDocID
            End Get
            Set(ByVal Value As Long)
                _OppBizDocID = Value
            End Set
        End Property

        Public Property Assembly() As Boolean
            Get
                Return _Assembly
            End Get
            Set(ByVal Value As Boolean)
                _Assembly = Value
            End Set
        End Property

        Public Property CalPriceBasedOnIndItems() As Boolean
            Get
                Return _CalPriceBasedOnIndItems
            End Get
            Set(ByVal Value As Boolean)
                _CalPriceBasedOnIndItems = Value
            End Set
        End Property

        Public Property ShowDeptItemDesc() As Boolean
            Get
                Return _ShowDeptItemDesc
            End Get
            Set(ByVal Value As Boolean)
                _ShowDeptItemDesc = Value
            End Set
        End Property

        Public Property ShowDeptItem() As Boolean
            Get
                Return _ShowDeptItem
            End Get
            Set(ByVal Value As Boolean)
                _ShowDeptItem = Value
            End Set
        End Property

        Public Property strChildItems() As String
            Get
                Return _strChildItems
            End Get
            Set(ByVal Value As String)
                _strChildItems = Value
            End Set
        End Property

        Public Property UnitofMeasure() As String
            Get
                Return _UnitofMeasure
            End Get
            Set(ByVal Value As String)
                _UnitofMeasure = Value
            End Set
        End Property

        Public Property ClientZoneOffsetTime() As Integer
            Get
                Return _ClientZoneOffsetTime
            End Get
            Set(ByVal Value As Integer)
                _ClientZoneOffsetTime = Value
            End Set
        End Property

        Public Property ShippingCost() As Decimal
            Get
                Return _ShippingCost
            End Get
            Set(ByVal Value As Decimal)
                _ShippingCost = Value
            End Set
        End Property


        Public Property AllowBackOrder() As Boolean
            Get
                Return _AllowBackOrder
            End Get
            Set(ByVal Value As Boolean)
                _AllowBackOrder = Value
            End Set
        End Property

        Public Property FreeShipping() As Boolean
            Get
                Return _FreeShipping
            End Get
            Set(ByVal Value As Boolean)
                _FreeShipping = Value
            End Set
        End Property

        Public Property Width() As Double
            Get
                Return _Width
            End Get
            Set(ByVal Value As Double)
                _Width = Value
            End Set
        End Property

        Public Property Length() As Double
            Get
                Return _Length
            End Get
            Set(ByVal Value As Double)
                _Length = Value
            End Set
        End Property

        Public Property Height() As Double
            Get
                Return _Height
            End Get
            Set(ByVal Value As Double)
                _Height = Value
            End Set
        End Property

        Public Property Weight() As Double
            Get
                Return _Weight
            End Get
            Set(ByVal Value As Double)
                _Weight = Value
            End Set
        End Property

        Public Property FromPinCode() As Integer
            Get
                Return _FromPinCode
            End Get
            Set(ByVal Value As Integer)
                _FromPinCode = Value
            End Set
        End Property

        Public Property ToPinCode() As Integer
            Get
                Return _ToPinCode
            End Get
            Set(ByVal Value As Integer)
                _ToPinCode = Value
            End Set
        End Property

        Public Property mode() As Boolean
            Get
                Return _mode
            End Get
            Set(ByVal Value As Boolean)
                _mode = Value
            End Set
        End Property

        Public Property Country() As Long
            Get
                Return _Country
            End Get
            Set(ByVal Value As Long)
                _Country = Value
            End Set
        End Property

        Public Property State() As Long
            Get
                Return _State
            End Get
            Set(ByVal Value As Long)
                _State = Value
            End Set
        End Property

        Public Property WareHouseDetailID() As Long
            Get
                Return _WareHouseDetailID
            End Get
            Set(ByVal Value As Long)
                _WareHouseDetailID = Value
            End Set
        End Property

        Public Property LabourCost() As Decimal
            Get
                Return _LabourCost
            End Get
            Set(ByVal Value As Decimal)
                _LabourCost = Value
            End Set
        End Property

        Public Property Warehouse() As String
            Get
                Return _Warehouse
            End Get
            Set(ByVal Value As String)
                _Warehouse = Value
            End Set
        End Property



        Public Property strItemCodes() As String
            Get
                Return _strItemCodes
            End Get
            Set(ByVal Value As String)
                _strItemCodes = Value
            End Set
        End Property



        Public Property WareHouseItemID() As Long
            Get
                Return _WareHouseItemID
            End Get
            Set(ByVal Value As Long)
                _WareHouseItemID = Value
            End Set
        End Property



        Public Property ModelID() As String
            Get
                Return _ModelID
            End Get
            Set(ByVal Value As String)
                _ModelID = Value
            End Set
        End Property

        Public Property bitSerialized() As Boolean
            Get
                Return _bitSerialized
            End Get
            Set(ByVal Value As Boolean)
                _bitSerialized = Value
            End Set
        End Property



        Public Property ItemGroupName() As String
            Get
                Return _ItemGroupName
            End Get
            Set(ByVal Value As String)
                _ItemGroupName = Value
            End Set
        End Property


        Public Property ItemGroupID() As Long
            Get
                Return _ItemGroupID
            End Get
            Set(ByVal Value As Long)
                _ItemGroupID = Value
            End Set
        End Property



        Public Property extType() As Short
            Get
                Return _extType
            End Get
            Set(ByVal Value As Short)
                _extType = Value
            End Set
        End Property



        Public Property Priority() As Long
            Get
                Return _Priority
            End Get
            Set(ByVal Value As Long)
                _Priority = Value
            End Set
        End Property



        Public Property wCountry() As Long
            Get
                Return _wCountry
            End Get
            Set(ByVal Value As Long)
                _wCountry = Value
            End Set
        End Property



        Public Property wState() As Long
            Get
                Return _wState
            End Get
            Set(ByVal Value As Long)
                _wState = Value
            End Set
        End Property



        Public Property WarehouseID() As Long
            Get
                Return _WarehouseID
            End Get
            Set(ByVal Value As Long)
                _WarehouseID = Value
            End Set
        End Property

        Public Property wPinccode() As String
            Get
                Return _wPinccode
            End Get
            Set(ByVal Value As String)
                _wPinccode = Value
            End Set
        End Property
        Public Property wCity() As String
            Get
                Return _wCity
            End Get
            Set(ByVal Value As String)
                _wCity = Value
            End Set
        End Property

        Public Property wStreet() As String
            Get
                Return _wStreet
            End Get
            Set(ByVal Value As String)
                _wStreet = Value
            End Set
        End Property

        Public Property str() As String
            Get
                Return _str
            End Get
            Set(ByVal Value As String)
                _str = Value
            End Set
        End Property

        Public Property Checked() As Short
            Get
                Return _Checked
            End Get
            Set(ByVal Value As Short)
                _Checked = Value
            End Set
        End Property


        Public Property SubCategoryID() As Long
            Get
                Return _CategoryName
            End Get
            Set(ByVal Value As Long)
                _SubCategoryID = Value
            End Set
        End Property
        Public Property ParentCategoryID() As Long
            Get
                Return _ParentCategoryID
            End Get
            Set(ByVal Value As Long)
                _ParentCategoryID = Value
            End Set
        End Property

        Public Property CategoryName() As String
            Get
                Return _CategoryName
            End Get
            Set(ByVal Value As String)
                _CategoryName = Value
            End Set
        End Property


        Public Property CategoryID() As Long
            Get
                Return _CategoryID
            End Get
            Set(ByVal Value As Long)
                _CategoryID = Value
            End Set
        End Property


        Public Property Applicable() As Boolean
            Get
                Return _Applicable
            End Get
            Set(ByVal Value As Boolean)
                _Applicable = Value
            End Set
        End Property


        Public Property Disc() As Decimal
            Get
                Return _Disc
            End Get
            Set(ByVal Value As Decimal)
                _Disc = Value
            End Set
        End Property


        Public Property DiscProfileID() As Long
            Get
                Return _DiscProfileID
            End Get
            Set(ByVal Value As Long)
                _DiscProfileID = Value
            End Set
        End Property


        Public Property ProfileID() As Long
            Get
                Return _ProfileID
            End Get
            Set(ByVal Value As Long)
                _ProfileID = Value
            End Set
        End Property

        Public Property RelID() As Long
            Get
                Return _RelID
            End Get
            Set(ByVal Value As Long)
                _RelID = Value
            End Set
        End Property


        Public Property Amount() As Decimal
            Get
                Return _Amount
            End Get
            Set(ByVal Value As Decimal)
                _Amount = Value
            End Set
        End Property


        Public Property Tax() As Decimal
            Get
                Return _Tax
            End Get
            Set(ByVal Value As Decimal)
                _Tax = Value
            End Set
        End Property


        Public Property byteMode() As Short
            Get
                Return _byteMode
            End Get
            Set(ByVal Value As Short)
                _byteMode = Value
            End Set
        End Property


        Public Property ReceivedDate() As Long
            Get
                Return _ReceivedDate
            End Get
            Set(ByVal Value As Long)
                _ReceivedDate = Value
            End Set
        End Property


        Public Property OppItemCode() As Long
            Get
                Return _OppItemCode
            End Get
            Set(ByVal Value As Long)
                _OppItemCode = Value
            End Set
        End Property


        Public Property OppName() As String
            Get
                Return _OppName
            End Get
            Set(ByVal Value As String)
                _OppName = Value
            End Set
        End Property


        Public Property ContactID() As Long
            Get
                Return _ContactID
            End Get
            Set(ByVal Value As Long)
                _ContactID = Value
            End Set
        End Property


        Public Property OppId() As Long
            Get
                Return _OppId
            End Get
            Set(ByVal Value As Long)
                _OppId = Value
            End Set
        End Property


        Public Property PathForImage() As String
            Get
                Return _PathForImage
            End Get
            Set(ByVal Value As String)
                _PathForImage = Value
            End Set
        End Property

        Public Property PathForTImage() As String
            Get
                Return _PathForTImage
            End Get
            Set(ByVal Value As String)
                _PathForTImage = Value
            End Set
        End Property
        Public Property IsImage() As Boolean
            Get
                Return _IsImage
            End Get
            Set(ByVal Value As Boolean)
                _IsImage = Value
            End Set
        End Property

        Public Property OnAllocation() As Double
            Get
                Return _OnAllocation
            End Get
            Set(ByVal Value As Double)
                _OnAllocation = Value
            End Set
        End Property


        Public Property CompetitorID() As Long
            Get
                Return _CompetitorID
            End Get
            Set(ByVal Value As Long)
                _CompetitorID = Value
            End Set
        End Property


        Public Property NoofUnits() As Decimal
            Get
                Return _NoofUnits
            End Get
            Set(ByVal Value As Decimal)
                _NoofUnits = Value
            End Set
        End Property


        Public Property strFieldList() As String
            Get
                Return _strFieldList
            End Get
            Set(ByVal Value As String)
                _strFieldList = Value
            End Set
        End Property


        Public Property VendorID() As Long
            Get
                Return _VendorID
            End Get
            Set(ByVal Value As Long)
                _VendorID = Value
            End Set
        End Property


        Public Property ManufacturerID() As Long
            Get
                Return _ManufacturerID
            End Get
            Set(ByVal Value As Long)
                _ManufacturerID = Value
            End Set
        End Property
        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property


        Public Property KeyWord() As String
            Get
                Return _KeyWord
            End Get
            Set(ByVal Value As String)
                _KeyWord = Value
            End Set
        End Property




        Public Property columnName() As String
            Get
                Return _columnName
            End Get
            Set(ByVal Value As String)
                _columnName = Value
            End Set
        End Property

        Public Property columnSortOrder() As String
            Get
                Return _columnSortOrder
            End Get
            Set(ByVal Value As String)
                _columnSortOrder = Value
            End Set
        End Property

        Public Property SortCharacter() As Char
            Get
                Return _SortCharacter
            End Get
            Set(ByVal Value As Char)
                _SortCharacter = Value
            End Set
        End Property

        Public Property KitID() As Long
            Get
                Return _KitID
            End Get
            Set(ByVal Value As Long)
                _KitID = Value
            End Set
        End Property

        Public Property BackOrder() As Double
            Get
                Return _BackOrder
            End Get
            Set(ByVal Value As Double)
                _BackOrder = Value
            End Set
        End Property

        Public Property ReOrder() As Double
            Get
                Return _ReOrder
            End Get
            Set(ByVal Value As Double)
                _ReOrder = Value
            End Set
        End Property

        Public Property OnOrder() As Double
            Get
                Return _OnOrder
            End Get
            Set(ByVal Value As Double)
                _OnOrder = Value
            End Set
        End Property

        Public Property OnHand() As Double
            Get
                Return _OnHand
            End Get
            Set(ByVal Value As Double)
                _OnHand = Value
            End Set
        End Property



        Public Property DateEntered() As Date
            Get
                Return _DateEntered
            End Get
            Set(ByVal Value As Date)
                _DateEntered = Value
            End Set
        End Property

        Public Property NoofItemsReqForKit() As Long
            Get
                Return _NoofItemsReqForKit
            End Get
            Set(ByVal Value As Long)
                _NoofItemsReqForKit = Value
            End Set
        End Property

        Public Property KitParent() As Boolean
            Get
                Return _KitParent
            End Get
            Set(ByVal Value As Boolean)
                _KitParent = Value
            End Set
        End Property



        Public Property SKU() As String
            Get
                Return _SKU
            End Get
            Set(ByVal Value As String)
                _SKU = Value
            End Set
        End Property

        Public Property Taxable() As Boolean
            Get
                Return _Taxable
            End Get
            Set(ByVal Value As Boolean)
                _Taxable = Value
            End Set
        End Property

        Public Property ItemClassification() As Long
            Get
                Return _ItemClassification
            End Get
            Set(ByVal Value As Long)
                _ItemClassification = Value
            End Set
        End Property

        Public Property ListPrice() As Double
            Get
                Return _ListPrice
            End Get
            Set(ByVal Value As Double)
                _ListPrice = Value
            End Set
        End Property

        Public Property TotalPages() As Long
            Get
                Return _TotalPages
            End Get
            Set(ByVal Value As Long)
                _TotalPages = Value
            End Set
        End Property

        Public Property CurrentPage() As Integer
            Get
                Return _CurrentPage
            End Get
            Set(ByVal Value As Integer)
                _CurrentPage = Value
            End Set
        End Property

        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal Value As Integer)
                _PageSize = Value
            End Set
        End Property

        Public Property TotalRecords() As Integer
            Get
                Return _TotalRecords
            End Get
            Set(ByVal Value As Integer)
                _TotalRecords = Value
            End Set
        End Property

        Public Property ItemCode() As Integer
            Get
                Return _ItemCode
            End Get
            Set(ByVal Value As Integer)
                _ItemCode = Value
            End Set
        End Property

        Public Property CustomerPartNo() As String
            Get
                Return _CustomerPartNo
            End Get
            Set(ByVal Value As String)
                _CustomerPartNo = Value
            End Set
        End Property

        Public Property ItemName() As String
            Get
                Return _ItemName
            End Get
            Set(ByVal Value As String)
                _ItemName = Value
            End Set
        End Property

        Public Property ItemDesc() As String
            Get
                Return _ItemDesc
            End Get
            Set(ByVal Value As String)
                _ItemDesc = Value
            End Set
        End Property

        Public Property ItemType() As String
            Get
                Return _ItemType
            End Get
            Set(ByVal Value As String)
                _ItemType = Value
            End Set
        End Property


        Public Property COGSChartAcntId() As Integer
            Get
                Return _COGSChartAcntId
            End Get
            Set(ByVal Value As Integer)
                _COGSChartAcntId = Value
            End Set
        End Property

        Public Property AssetChartAcntId() As Integer
            Get
                Return _AssetChartAcntId
            End Get
            Set(ByVal Value As Integer)
                _AssetChartAcntId = Value
            End Set
        End Property

        Public Property IncomeChartAcntId() As Integer
            Get
                Return _IncomeChartAcntId
            End Get
            Set(ByVal Value As Integer)
                _IncomeChartAcntId = Value
            End Set
        End Property

        Public Property IsExpenseItem As Boolean
        Public Property ExpenseChartAcntId As Long

        Public Property AverageCost() As Decimal
            Get
                Return _AverageCost
            End Get
            Set(ByVal value As Decimal)
                _AverageCost = value
            End Set
        End Property


        Private _bitTimeContractFromSalesOrder As Boolean

        Public Property bitTimeContractFromSalesOrder() As Boolean
            Get
                Return _bitTimeContractFromSalesOrder
            End Get
            Set(ByVal value As Boolean)
                _bitTimeContractFromSalesOrder = value
            End Set
        End Property

        Private _BusinessProcessId As Long

        Public Property BusinessProcessId() As Long
            Get
                Return _BusinessProcessId
            End Get
            Set(ByVal value As Long)
                _BusinessProcessId = value
            End Set
        End Property

        Private _Filter As String

        Public Property Filter() As String
            Get
                Return _Filter
            End Get
            Set(ByVal Value As String)
                _Filter = Value
            End Set
        End Property
        Private _type As Integer

        Public Property type() As Integer
            Get
                Return _type
            End Get
            Set(ByVal Value As Integer)
                _type = Value
            End Set
        End Property

        Private _ApiName As String
        Public Property ApiName() As String
            Get
                Return _ApiName
            End Get
            Set(ByVal value As String)
                _ApiName = value
            End Set
        End Property
        Private _WebApiId As Integer
        Public Property WebApiId() As Integer
            Get
                Return _WebApiId
            End Get
            Set(ByVal value As Integer)
                _WebApiId = value
            End Set
        End Property
        Private _ApiItemID As String
        Public Property ApiItemID() As String
            Get
                Return _ApiItemID
            End Get
            Set(ByVal value As String)
                _ApiItemID = value
            End Set
        End Property
        Private _ListOfAPI As String
        Public Property ListOfAPI() As String
            Get
                Return _ListOfAPI
            End Get
            Set(ByVal value As String)
                _ListOfAPI = value
            End Set
        End Property
        Private _ListOfCategories As String
        Public Property ListOfCategories() As String
            Get
                Return _ListOfCategories
            End Get
            Set(ByVal value As String)
                _ListOfCategories = value
            End Set
        End Property
        Private _DefaultSelect As Integer
        Public Property DefaultSelect() As Integer
            Get
                Return _DefaultSelect
            End Get
            Set(ByVal value As Integer)
                _DefaultSelect = value
            End Set
        End Property
        Private _numBaseUnit As Long

        Public Property BaseUnit() As Long
            Get
                Return _numBaseUnit
            End Get
            Set(ByVal Value As Long)
                _numBaseUnit = Value
            End Set
        End Property
        Private _numPurchaseUnit As Long

        Public Property PurchaseUnit() As Long
            Get
                Return _numPurchaseUnit
            End Get
            Set(ByVal Value As Long)
                _numPurchaseUnit = Value
            End Set
        End Property
        Private _numSaleUnit As Long

        Public Property SaleUnit() As Long
            Get
                Return _numSaleUnit
            End Get
            Set(ByVal Value As Long)
                _numSaleUnit = Value
            End Set
        End Property
        Private _bitLotNo As Boolean

        Public Property bitLotNo() As Boolean
            Get
                Return _bitLotNo
            End Get
            Set(ByVal Value As Boolean)
                _bitLotNo = Value
            End Set
        End Property

        Private _ClientTimeZoneOffset As Integer
        Public Property ClientTimeZoneOffset() As Integer
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal Value As Integer)
                _ClientTimeZoneOffset = Value
            End Set
        End Property

        Private _numWOId As Long

        Public Property numWOId() As Long
            Get
                Return _numWOId
            End Get
            Set(ByVal Value As Long)
                _numWOId = Value
            End Set
        End Property

        Private _numWODetailID As Long
        Public Property numWODetailID() As Long
            Get
                Return _numWODetailID
            End Get
            Set(ByVal value As Long)
                _numWODetailID = value
            End Set
        End Property

        Private _numWOStatus As Long

        Public Property numWOStatus() As Long
            Get
                Return _numWOStatus
            End Get
            Set(ByVal Value As Long)
                _numWOStatus = Value
            End Set
        End Property

        Private _CurrencyID As Long

        Public Property CurrencyID() As Long
            Get
                Return _CurrencyID
            End Get
            Set(ByVal value As Long)
                _CurrencyID = value
            End Set
        End Property

        Private _ChildItemID As Long

        Public Property ChildItemID() As Long
            Get
                Return _ChildItemID
            End Get
            Set(ByVal value As Long)
                _ChildItemID = value
            End Set
        End Property

        Private _ShipAddressId As Long

        Public Property ShipAddressId() As Long
            Get
                Return _ShipAddressId
            End Get
            Set(ByVal value As Long)
                _ShipAddressId = value
            End Set
        End Property

        Private _BillAddressId As Long

        Public Property BillAddressId() As Long
            Get
                Return _BillAddressId
            End Get
            Set(ByVal value As Long)
                _BillAddressId = value
            End Set
        End Property

        Private _Instruction As String

        Public Property Instruction() As String
            Get
                Return _Instruction
            End Get
            Set(ByVal Value As String)
                _Instruction = Value
            End Set
        End Property

        Private _CompliationDate As Date

        Public Property CompliationDate() As Date
            Get
                Return _CompliationDate
            End Get
            Set(ByVal Value As Date)
                _CompliationDate = Value
            End Set
        End Property

        Private _AssignTo As Long
        Public Property AssignTo() As Long
            Get
                Return _AssignTo
            End Get
            Set(ByVal Value As Long)
                _AssignTo = Value
            End Set
        End Property

        Private _SalesOrderId As Long
        Public Property SalesOrderId() As Long
            Get
                Return _SalesOrderId
            End Get
            Set(ByVal Value As Long)
                _SalesOrderId = Value
            End Set
        End Property


        Private _BuildProcessId As Long
        Public Property BuildProcessId() As Long
            Get
                Return _BuildProcessId
            End Get
            Set(ByVal Value As Long)
                _BuildProcessId = Value
            End Set
        End Property

        Private _ProjectFinishDate As Date
        Public Property ProjectFinishDate() As Date
            Get
                Return _ProjectFinishDate
            End Get
            Set(ByVal Value As Date)
                _ProjectFinishDate = Value
            End Set
        End Property


        Private _vcWOId As String

        Public Property vcWOId() As String
            Get
                Return _vcWOId
            End Get
            Set(ByVal Value As String)
                _vcWOId = Value
            End Set
        End Property

        Private _DiscountType As Short
        Public Property DiscountType() As Short
            Get
                Return _DiscountType
            End Get
            Set(ByVal value As Short)
                _DiscountType = value
            End Set
        End Property

        Private _Discount As Decimal
        Public Property Discount() As Decimal
            Get
                Return _Discount
            End Get
            Set(ByVal value As Decimal)
                _Discount = value
            End Set
        End Property

        Private _ProID As Long
        Public Property ProID() As Long
            Get
                Return _ProID
            End Get
            Set(ByVal Value As Long)
                _ProID = Value
            End Set
        End Property

        Private _intOrder As Integer
        Public Property intOrder() As Integer
            Get
                Return _intOrder
            End Get
            Set(ByVal Value As Integer)
                _intOrder = Value
            End Set
        End Property

        Private _CategoryDescription As String
        Public Property CategoryDescription() As String
            Get
                Return _CategoryDescription
            End Get
            Set(ByVal Value As String)
                _CategoryDescription = Value
            End Set
        End Property
        Private _bitArchieve As Boolean
        Public Property bitArchieve As Boolean
            Get
                Return _bitArchieve
            End Get
            Set(ByVal value As Boolean)
                _bitArchieve = value
            End Set
        End Property

        Private _bitArchieveItemSetting As Boolean
        Public Property ArchieveItemSetting As Boolean
            Get
                Return _bitArchieveItemSetting
            End Get
            Set(ByVal value As Boolean)
                _bitArchieveItemSetting = value
            End Set
        End Property

        Private _ParentItemCode As Double
        Public Property ParentItemCode As Double
            Get
                Return _ParentItemCode
            End Get
            Set(ByVal value As Double)
                _ParentItemCode = value
            End Set
        End Property
        Private _Relationship As String
        Public Property Relationship As String
            Get
                Return _Relationship
            End Get
            Set(ByVal value As String)
                _Relationship = value
            End Set
        End Property
        Private _bitPreUpSell As Boolean
        Public Property bitPreUpSell() As Boolean
            Get
                Return _bitPreUpSell
            End Get
            Set(ByVal value As Boolean)
                _bitPreUpSell = value
            End Set
        End Property
        Private _bitPostUpSell As Boolean
        Public Property bitPostUpSell() As Boolean
            Get
                Return _bitPostUpSell
            End Get
            Set(ByVal value As Boolean)
                _bitPostUpSell = value
            End Set
        End Property
        'Added by Neelam Kapila || 11/01/2017 - Added property to save Required field in Promotion Offer table
        Private _bitRequired As Boolean
        Public Property bitRequired() As Boolean
            Get
                Return _bitRequired
            End Get
            Set(ByVal value As Boolean)
                _bitRequired = value
            End Set
        End Property
        Private _strPostUpSellDesc As String
        Public Property strPostUpSellDesc() As String
            Get
                Return _strPostUpSellDesc
            End Get
            Set(ByVal value As String)
                _strPostUpSellDesc = value
            End Set
        End Property

        Private _PaymentMethodId As Long
        Public Property PaymentMethod() As Long
            Get
                Return _PaymentMethodId
            End Get
            Set(ByVal value As Long)
                _PaymentMethodId = value
            End Set
        End Property
        Private _numReportId As Long
        Public Property numReportId As Long
            Get
                Return _numReportId
            End Get
            Set(ByVal value As Long)
                _numReportId = value
            End Set
        End Property
        Private _numSiteId As Long
        Public Property numSiteId As Long
            Get
                Return _numSiteId
            End Get
            Set(ByVal value As Long)
                _numSiteId = value
            End Set
        End Property

        Private _ItemClass As Long
        Public Property ItemClass() As Long
            Get
                Return _ItemClass
            End Get
            Set(ByVal Value As Long)
                _ItemClass = Value
            End Set
        End Property
        Private _StandardProductIDType As Short
        Public Property StandardProductIDType() As Short
            Get
                Return _StandardProductIDType
            End Get
            Set(ByVal value As Short)
                _StandardProductIDType = value
            End Set
        End Property
        Private _ShippingClass As Long
        Public Property ShippingClass() As Long
            Get
                Return _ShippingClass
            End Get
            Set(ByVal value As Long)
                _ShippingClass = value
            End Set
        End Property

        Private _numItemImageId As Integer
        Public Property numItemImageId As Integer
            Get
                Return _numItemImageId

            End Get
            Set(ByVal value As Integer)
                _numItemImageId = value
            End Set
        End Property
        Private _bitDefault As Boolean
        Public Property bitDefault As Boolean
            Get
                Return _bitDefault

            End Get
            Set(ByVal value As Boolean)
                _bitDefault = value
            End Set
        End Property
        Private _DisplayOrder As Integer
        Public Property DisplayOrder As Integer
            Get
                Return _DisplayOrder

            End Get
            Set(ByVal value As Integer)
                _DisplayOrder = value
            End Set
        End Property

        Private _SerialNo As String
        Public Property SerialNo As String
            Get
                Return _SerialNo

            End Get
            Set(ByVal value As String)
                _SerialNo = value
            End Set
        End Property
        Private _Comments As String
        Public Property Comments As String
            Get
                Return _Comments

            End Get
            Set(ByVal value As String)
                _Comments = value
            End Set
        End Property
        Private _Quantity As Long
        Public Property Quantity As Long
            Get
                Return _Quantity

            End Get
            Set(ByVal value As Long)
                _Quantity = value
            End Set
        End Property
        Private _Price As String
        Public Property Price As String
            Get
                Return _Price

            End Get
            Set(ByVal value As String)
                _Price = value
            End Set
        End Property
        Private _FilterQuery As String
        Public Property FilterQuery As String
            Get
                Return _FilterQuery

            End Get
            Set(ByVal value As String)
                _FilterQuery = value
            End Set
        End Property

        Private _FilterRegularCondition As String
        Public Property FilterRegularCondition As String
            Get
                Return _FilterRegularCondition

            End Get
            Set(ByVal value As String)
                _FilterRegularCondition = value
            End Set
        End Property

        Private _FilterCustomCondition As String
        Public Property FilterCustomCondition As String
            Get
                Return _FilterCustomCondition

            End Get
            Set(ByVal value As String)
                _FilterCustomCondition = value
            End Set
        End Property

        Private _FilterCustomFields As String
        Public Property FilterCustomFields() As String
            Get
                Return _FilterCustomFields
            End Get
            Set(ByVal value As String)
                _FilterCustomFields = value
            End Set
        End Property


        Private _WareHouseItmsDTLID As Long
        Public Property WareHouseItmsDTLID() As Long
            Get
                Return _WareHouseItmsDTLID
            End Get
            Set(ByVal Value As Long)
                _WareHouseItmsDTLID = Value
            End Set
        End Property


        Private _strPartNo As String
        Public Property PartNo() As String
            Get
                Return _strPartNo
            End Get
            Set(ByVal value As String)
                _strPartNo = value
            End Set
        End Property

        Private _dblMonCost As Double
        Public Property monCost() As Double
            Get
                Return _dblMonCost
            End Get
            Set(ByVal value As Double)
                _dblMonCost = value
            End Set
        End Property

        Private _intMinQty As Integer
        Public Property minQty() As Integer
            Get
                Return _intMinQty
            End Get
            Set(ByVal value As Integer)
                _intMinQty = value
            End Set
        End Property

        Private _intVendorTcode As Long
        Public Property VendorTcode() As Long
            Get
                Return _intVendorTcode
            End Get
            Set(ByVal value As Long)
                _intVendorTcode = value
            End Set
        End Property

        Private _ProcedureCallFlag As Short
        Public Property ProcedureCallFlag() As Short
            Get
                Return _ProcedureCallFlag
            End Get
            Set(ByVal value As Short)
                _ProcedureCallFlag = value
            End Set
        End Property
        Private _ShipCost As Decimal
        Public Property ShipCost() As Decimal
            Get
                Return _ShipCost
            End Get
            Set(ByVal value As Decimal)
                _ShipCost = value
            End Set
        End Property

        Private _AllowDropShip As Boolean
        Public Property AllowDropShip() As Boolean
            Get
                Return _AllowDropShip
            End Get
            Set(ByVal Value As Boolean)
                _AllowDropShip = Value
            End Set
        End Property
        'added by hitesh
        'Private _AutomateReorderPoint As Boolean
        'Public Property AutomateReorderPoint() As Boolean
        '    Get
        '        Return _AutomateReorderPoint
        '    End Get
        '    Set(ByVal Value As Boolean)
        '        _AutomateReorderPoint = Value
        '    End Set
        'End Property

        Private _dtAdjustmentDate As DateTime
        Public Property dtAdjustmentDate() As DateTime
            Get
                Return _dtAdjustmentDate
            End Get
            Set(ByVal value As DateTime)
                _dtAdjustmentDate = value
            End Set
        End Property

        Private _strPriceLevelDetail As String
        Public Property PriceLevelDetail() As String
            Get
                Return _strPriceLevelDetail
            End Get
            Set(ByVal value As String)
                _strPriceLevelDetail = value
            End Set
        End Property

        Private _sPriceRuleType As Short
        Public Property PriceRuleType() As Short
            Get
                Return _sPriceRuleType
            End Get
            Set(ByVal value As Short)
                _sPriceRuleType = value
            End Set
        End Property

        Private _sPriceDiscountType As Short
        Public Property PriceDiscountType() As Short
            Get
                Return _sPriceDiscountType
            End Get
            Set(ByVal value As Short)
                _sPriceDiscountType = value
            End Set
        End Property



        Private _WarehouseLocationID As Long
        Public Property WarehouseLocationID() As Long
            Get
                Return _WarehouseLocationID
            End Get
            Set(ByVal value As Long)
                _WarehouseLocationID = value
            End Set
        End Property
        Public Property EndDate() As Date
            Get
                Return _EndDate
            End Get
            Set(ByVal Value As Date)
                _EndDate = Value
            End Set
        End Property

        Public Property StartDate() As Date
            Get
                Return _StartDate
            End Get
            Set(ByVal Value As Date)
                _StartDate = Value
            End Set
        End Property

        Private _bitAsset As Boolean
        Public Property bitAsset As Boolean
            Get
                Return _bitAsset
            End Get
            Set(ByVal value As Boolean)
                _bitAsset = value
            End Set
        End Property
        Private _bitRental As Boolean
        Public Property bitRental As Boolean
            Get
                Return _bitRental
            End Get
            Set(ByVal value As Boolean)
                _bitRental = value
            End Set
        End Property
        Private _numMaxWOQty As Integer
        Public Property numMaxWOQty As Integer
            Get
                Return _numMaxWOQty
            End Get
            Set(ByVal value As Integer)
                _numMaxWOQty = value
            End Set
        End Property
        Private _bitOrderDelete As Boolean
        Public Property bitOrderDelete As Boolean
            Get
                Return _bitOrderDelete
            End Get
            Set(ByVal value As Boolean)
                _bitOrderDelete = value
            End Set
        End Property

        Private _numPartner As Long
        Public Property numPartner As Long
            Get
                Return _numPartner
            End Get
            Set(ByVal value As Long)
                _numPartner = value
            End Set
        End Property
        'Added by Neelam Kapila || 11/02/2017 - Added property to check if related item is pre-checkout only
        Private _IsPreCheckout As Boolean
        Public Property IsPreCheckout() As Boolean
            Get
                Return _IsPreCheckout
            End Get
            Set(ByVal Value As Boolean)
                _IsPreCheckout = Value
            End Set
        End Property

        Public Property bitMultiSelect() As Boolean
            Get
                Return _bitMultiSelect
            End Get
            Set(ByVal Value As Boolean)
                _bitMultiSelect = Value
            End Set
        End Property

        Public Property bitHasKitAsChild As Boolean
        Public Property vcKitChildItems As String
        Public Property SelectedUOMID As Long
        Public Property SelectedUOM As String
        Public Property SelectedQuantity As Long
        Public Property CreatedAfter As DateTime?
        Public Property ModifiedAfter As DateTime?
        Public Property StockTransferToWarehouseItemID As Long
        Public Property CategoryProfileID As Long
        Public Property Attributes As String
        Public Property AttributeIDs As String
        Public Property IsVirtualInventory As Boolean
        Public Property AssembledItemID As Long
        Public Property QtyRequiredForSingleBuild As Double
        Public Property FuturePassword As String
        Public Property CategoryNameURL As String
        Public Property ShippingService As Long
        Public Property IsCreateGlobalLocation As Boolean
        Public Property RegularSearchCriteria As String
        Public Property CustomSearchCriteria As String
        Public Property IsMatrix As Boolean
        Public Property ItemAttributes As String

        Public Property PriceLevel1 As Decimal
        Public Property PriceLevel2 As Decimal
        Public Property PriceLevel3 As Decimal
        Public Property PriceLevel4 As Decimal
        Public Property PriceLevel5 As Decimal
        Public Property PriceLevel6 As Decimal
        Public Property PriceLevel7 As Decimal
        Public Property PriceLevel8 As Decimal
        Public Property PriceLevel9 As Decimal
        Public Property PriceLevel10 As Decimal
        Public Property PriceLevel11 As Decimal
        Public Property PriceLevel12 As Decimal
        Public Property PriceLevel13 As Decimal
        Public Property PriceLevel14 As Decimal
        Public Property PriceLevel15 As Decimal
        Public Property PriceLevel16 As Decimal
        Public Property PriceLevel17 As Decimal
        Public Property PriceLevel18 As Decimal
        Public Property PriceLevel19 As Decimal
        Public Property PriceLevel20 As Decimal
        Public Property PriceLevel1Name As String
        Public Property PriceLevel2Name As String
        Public Property PriceLevel3Name As String
        Public Property PriceLevel4Name As String
        Public Property PriceLevel5Name As String
        Public Property PriceLevel6Name As String
        Public Property PriceLevel7Name As String
        Public Property PriceLevel8Name As String
        Public Property PriceLevel9Name As String
        Public Property PriceLevel10Name As String
        Public Property PriceLevel11Name As String
        Public Property PriceLevel12Name As String
        Public Property PriceLevel13Name As String
        Public Property PriceLevel14Name As String
        Public Property PriceLevel15Name As String
        Public Property PriceLevel16Name As String
        Public Property PriceLevel17Name As String
        Public Property PriceLevel18Name As String
        Public Property PriceLevel19Name As String
        Public Property PriceLevel20Name As String
        Public Property PriceLevel1QtyFrom As Integer
        Public Property PriceLevel1QtyTo As Integer
        Public Property PriceLevel2QtyFrom As Integer
        Public Property PriceLevel2QtyTo As Integer
        Public Property PriceLevel3QtyFrom As Integer
        Public Property PriceLevel3QtyTo As Integer
        Public Property PriceLevel4QtyFrom As Integer
        Public Property PriceLevel4QtyTo As Integer
        Public Property PriceLevel5QtyFrom As Integer
        Public Property PriceLevel5QtyTo As Integer
        Public Property PriceLevel6QtyFrom As Integer
        Public Property PriceLevel6QtyTo As Integer
        Public Property PriceLevel7QtyFrom As Integer
        Public Property PriceLevel7QtyTo As Integer
        Public Property PriceLevel8QtyFrom As Integer
        Public Property PriceLevel8QtyTo As Integer
        Public Property PriceLevel9QtyFrom As Integer
        Public Property PriceLevel9QtyTo As Integer
        Public Property PriceLevel10QtyFrom As Integer
        Public Property PriceLevel10QtyTo As Integer
        Public Property PriceLevel11QtyFrom As Integer
        Public Property PriceLevel11QtyTo As Integer
        Public Property PriceLevel12QtyFrom As Integer
        Public Property PriceLevel12QtyTo As Integer
        Public Property PriceLevel13QtyFrom As Integer
        Public Property PriceLevel13QtyTo As Integer
        Public Property PriceLevel14QtyFrom As Integer
        Public Property PriceLevel14QtyTo As Integer
        Public Property PriceLevel15QtyFrom As Integer
        Public Property PriceLevel15QtyTo As Integer
        Public Property PriceLevel16QtyFrom As Integer
        Public Property PriceLevel16QtyTo As Integer
        Public Property PriceLevel17QtyFrom As Integer
        Public Property PriceLevel17QtyTo As Integer
        Public Property PriceLevel18QtyFrom As Integer
        Public Property PriceLevel18QtyTo As Integer
        Public Property PriceLevel19QtyFrom As Integer
        Public Property PriceLevel19QtyTo As Integer
        Public Property PriceLevel20QtyFrom As Integer
        Public Property PriceLevel20QtyTo As Integer
        Public Property vcNotes As String
        Public Property numManufacturer As Long
        Public Property PaymentGatewayID As Long
        Public Property CustomerPONumber As String
        Public Property CategoryMetaTitle As String
        Public Property CategoryMetaKeywords As String
        Public Property CategoryMetaDescription As String
        Public Property boolBillingTerms As Boolean
        Public Property BillingDays As Integer
        Public Property boolInterestType As Boolean
        Public Property Interest As Decimal
        Public Property DashboardReminderType As Short
        Public Property PrintNodeAPIKey As String
        Public Property PrintNodePrinterID As String
        Public Property vcASIN As String
        Public Property KitAssemblyPriceBasedOn As Short
        Public Property ReorderQty As Double
        Public Property ReorderPoint As Double
        Public Property IsSOWorkOrder As Boolean
        Public Property IsKitSingleSelect As Boolean
        Public Property KitSelectedChild As String 'Comma seperated
        Public Property OrderPromotionID As Long
        Public Property OrderPromotionDiscountID As Long
        Public Property UserRightType As Long
        Public Property ASIN As String
        Public Property IsAutomateReorderPoint As Boolean
        Public Property MatrixGroups As String
        Public Property AssetDepreciationPeriod As Double
        Public Property AssetResidual As Double
        Public Property AssetDepreciationMethod As Short

        Public Property IsPreventOrphanedParents As Boolean
        Public Property SearchType As String
        Public Property FilterFromDate As Date?
        Public Property FilterToDate As Date?
        Public Property FilterPOppName As String
        Public Property FilterModifiedBy As String
        Public Property FilterItemIDs As String
        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  RemoveRecrod()
        '     Purpose					 :  To delete data from database 
        '     Example                    :  
        '     Parameters                 :  @ItemCode                     
        '                                 
        '     Outputs					 :  It return true  if deleted otherwise return false
        '						
        '     Author Name				 :  Ajeet Singh
        '     Date Written				 :  18/10/2004
        '     Cross References 			 :  List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifier�s Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Function DeleteItems() As Boolean
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Try
                ' Set up parameters 
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                ' @ItemCount Input Parameter 
                arParms(0) = New Npgsql.NpgsqlParameter("@ItemCode", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomianID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteItem", arParms)

                Return True
            Catch ex As Exception
                'Return False
                Throw ex
            Finally

            End Try

        End Function
        Public Function DeleteKitItems() As Boolean
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Try
                ' Set up parameters 
                Dim arParms As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter("@numItemDetailID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms.Value = _ItemDetailID

                ' @ItemCount Input Parameter 
                'arParms(0) = New Npgsql.NpgsqlParameter("@numItemDetailID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                'arParms(0).Value = _ItemDetailID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteKitItem", arParms)

                Return True
            Catch ex As Exception
                Return False
                Throw ex
            Finally

            End Try

        End Function
        Public Function ItemDetails() As DataTable
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Dim ds As DataSet
            Try
                ' Set up parameters 
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                ' @ItemCount Input Parameter 
                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _ClientZoneOffsetTime

                arParms(2) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DivisionID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_ItemDetails", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            Finally

            End Try

        End Function
        Public Function ItemTax() As DataTable
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Dim ds As DataSet
            Try
                ' Set up parameters 
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                ' @ItemCount Input Parameter 
                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_ItemTaxTypes", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            Finally

            End Try

        End Function
        Public Function GetInventoryAudit() As DataTable
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Dim ds As DataSet
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numWareHouseItemID", _WareHouseItemID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numReferenceID", _ReferenceID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@tintRefType", _RefType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@vcDescription", _Description, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@tintMode", _tintMode, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@CurrentPage", _CurrentPage, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@PageSize", _PageSize, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@TotRecs", _TotalRecords, NpgsqlTypes.NpgsqlDbType.Integer, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@numModifiedBy", UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", _ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@v_dtRecordDate", Nothing, NpgsqlTypes.NpgsqlDbType.Timestamp))
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@v_dtFilterFromDate", FilterFromDate, NpgsqlTypes.NpgsqlDbType.Timestamp))
                    .Add(SqlDAL.Add_Parameter("@v_dtFilterToDate", FilterToDate, NpgsqlTypes.NpgsqlDbType.Timestamp))
                    .Add(SqlDAL.Add_Parameter("@v_vcFilterPOppName", FilterPOppName, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@v_vcFilterModifiedBy", FilterModifiedBy, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@v_vcFilterItemIDs", FilterItemIDs, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object = sqlParams.ToArray()
                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_ManageWareHouseItems_Tracking", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(7).Value)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            Finally

            End Try

        End Function
        Public Function GetAuditReport() As DataTable
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Dim ds As DataSet
            Try
                ' Set up parameters 
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                ' @ItemCount Input Parameter 
                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetInventoryAuditReport", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            Finally
            End Try
        End Function

        Public Function ManageItemTaxTypes() As Boolean
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Try
                ' Set up parameters 
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                ' @ItemCount Input Parameter 
                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@strItemDetails", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(1).Value = _strItemTaxTypes

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageItemTaxTypes", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            Finally

            End Try

        End Function




        Public Function AvailabilityItemStock() As DataSet
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Try
                ' Set up parameters 
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                ' @ItemCount Input Parameter 
                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_ItemsAvailablityOpenPO", arParms)


            Catch ex As Exception
                Throw ex
            End Try

        End Function


        Public Function ItemDetailsForEcomm() As DataSet
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Try
                ' Set up parameters 
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                ' @ItemCount Input Parameter 
                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@numWareHouseID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _WarehouseID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@numSiteId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _SiteID

                arParms(4) = New Npgsql.NpgsqlParameter("@vcCookieId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(4).Value = vcCookieId

                arParms(5) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(5).Value = UserCntID

                arParms(6) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = DivisionID

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_ItemDetailsForEcomm", arParms)


            Catch ex As Exception
                Throw ex
            End Try

        End Function


        Public Function ItemDetailsForEcommAutoSuggest() As DataSet
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Try
                ' Set up parameters 
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                ' @ItemCount Input Parameter 
                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSiteId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = SiteID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcSubChar", NpgsqlTypes.NpgsqlDbType.VarChar, 300)
                arParms(2).Value = strCondition

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_AutoSuggestItemList", arParms)


            Catch ex As Exception
                Throw ex
            End Try

        End Function


        Public Function ViewItemListForContainer() As DataSet
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Try
                ' Set up parameters 
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_ItemListForContainer", arParms)


            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Function ManageItemsAndKits() As String
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(72) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@vcItemName", NpgsqlTypes.NpgsqlDbType.VarChar, 300)
                arParms(1).Value = _ItemName

                arParms(2) = New Npgsql.NpgsqlParameter("@txtItemDesc", NpgsqlTypes.NpgsqlDbType.VarChar, 2000)
                arParms(2).Value = _ItemDesc

                arParms(3) = New Npgsql.NpgsqlParameter("@charItemType", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(3).Value = _ItemType

                arParms(4) = New Npgsql.NpgsqlParameter("@monListPrice", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(4).Value = _ListPrice

                arParms(5) = New Npgsql.NpgsqlParameter("@numItemClassification", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _ItemClassification

                arParms(6) = New Npgsql.NpgsqlParameter("@bitTaxable", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(6).Value = _Taxable

                arParms(7) = New Npgsql.NpgsqlParameter("@vcSKU", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(7).Value = _SKU

                arParms(8) = New Npgsql.NpgsqlParameter("@bitKitParent", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(8).Value = _KitParent

                arParms(9) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = DomainID

                arParms(10) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(10).Value = UserCntID

                arParms(11) = New Npgsql.NpgsqlParameter("@bitSerialized", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(11).Value = _bitSerialized

                arParms(12) = New Npgsql.NpgsqlParameter("@numVendorID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(12).Value = _VendorID

                arParms(13) = New Npgsql.NpgsqlParameter("@strFieldList", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(13).Value = _strFieldList

                arParms(14) = New Npgsql.NpgsqlParameter("@vcModelID", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(14).Value = _ModelID

                arParms(15) = New Npgsql.NpgsqlParameter("@numItemGroup", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(15).Value = _ItemGroupID

                arParms(16) = New Npgsql.NpgsqlParameter("@numCOGSChartAcntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(16).Value = _COGSChartAcntId

                arParms(17) = New Npgsql.NpgsqlParameter("@numAssetChartAcntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(17).Value = _AssetChartAcntId

                arParms(18) = New Npgsql.NpgsqlParameter("@numIncomeChartAcntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(18).Value = _IncomeChartAcntId

                arParms(19) = New Npgsql.NpgsqlParameter("@monAverageCost", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(19).Value = _AverageCost

                arParms(20) = New Npgsql.NpgsqlParameter("@monLabourCost", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(20).Value = _LabourCost

                arParms(21) = New Npgsql.NpgsqlParameter("@fltWeight", NpgsqlTypes.NpgsqlDbType.Double)
                arParms(21).Value = _Weight

                arParms(22) = New Npgsql.NpgsqlParameter("@fltHeight", NpgsqlTypes.NpgsqlDbType.Double)
                arParms(22).Value = _Height

                arParms(23) = New Npgsql.NpgsqlParameter("@fltLength", NpgsqlTypes.NpgsqlDbType.Double)
                arParms(23).Value = _Length

                arParms(24) = New Npgsql.NpgsqlParameter("@fltWidth", NpgsqlTypes.NpgsqlDbType.Double)
                arParms(24).Value = _Width

                arParms(25) = New Npgsql.NpgsqlParameter("@bitFreeshipping", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(25).Value = _FreeShipping

                arParms(26) = New Npgsql.NpgsqlParameter("@bitAllowBackOrder", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(26).Value = _AllowBackOrder

                arParms(27) = New Npgsql.NpgsqlParameter("@UnitofMeasure", NpgsqlTypes.NpgsqlDbType.VarChar, 30)
                arParms(27).Value = _UnitofMeasure

                arParms(28) = New Npgsql.NpgsqlParameter("@strChildItems", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(28).Value = _strChildItems

                arParms(29) = New Npgsql.NpgsqlParameter("@bitShowDeptItem", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(29).Value = _ShowDeptItem

                arParms(30) = New Npgsql.NpgsqlParameter("@bitShowDeptItemDesc", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(30).Value = _ShowDeptItemDesc

                arParms(31) = New Npgsql.NpgsqlParameter("@bitCalAmtBasedonDepItems", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(31).Value = _CalPriceBasedOnIndItems

                arParms(32) = New Npgsql.NpgsqlParameter("@bitAssembly", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(32).Value = _Assembly

                arParms(33) = New Npgsql.NpgsqlParameter("@intWebApiId", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(33).Value = _WebApiId

                arParms(34) = New Npgsql.NpgsqlParameter("@vcApiItemId", NpgsqlTypes.NpgsqlDbType.VarChar, 20)
                arParms(34).Value = _ApiItemID

                arParms(35) = New Npgsql.NpgsqlParameter("@numBarCodeID", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(35).Value = _BarCodeID

                arParms(36) = New Npgsql.NpgsqlParameter("@vcManufacturer", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(36).Value = _Manufacturer

                arParms(37) = New Npgsql.NpgsqlParameter("@numBaseUnit", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(37).Value = _numBaseUnit

                arParms(38) = New Npgsql.NpgsqlParameter("@numPurchaseUnit", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(38).Value = _numPurchaseUnit

                arParms(39) = New Npgsql.NpgsqlParameter("@numSaleUnit", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(39).Value = _numSaleUnit

                arParms(40) = New Npgsql.NpgsqlParameter("@bitLotNo", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(40).Value = _bitLotNo

                arParms(41) = New Npgsql.NpgsqlParameter("@IsArchieve", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(41).Value = bitArchieve

                arParms(42) = New Npgsql.NpgsqlParameter("@numItemClass", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(42).Value = _ItemClass

                arParms(43) = New Npgsql.NpgsqlParameter("@tintStandardProductIDType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(43).Value = _StandardProductIDType

                arParms(44) = New Npgsql.NpgsqlParameter("@vcExportToAPI", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(44).Value = _ListOfAPI

                arParms(45) = New Npgsql.NpgsqlParameter("@numShipClass", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(45).Value = _ShippingClass

                arParms(46) = New Npgsql.NpgsqlParameter("@ProcedureCallFlag", NpgsqlTypes.NpgsqlDbType.SmallInt)
                arParms(46).Value = _ProcedureCallFlag

                arParms(47) = New Npgsql.NpgsqlParameter("@vcCategories", NpgsqlTypes.NpgsqlDbType.VarChar, 2000)
                arParms(47).Value = _ListOfCategories

                arParms(48) = New Npgsql.NpgsqlParameter("@bitAllowDropShip", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(48).Value = _AllowDropShip

                arParms(49) = New Npgsql.NpgsqlParameter("@bitArchiveItem", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(49).Value = _bitArchieveItemSetting

                arParms(50) = New Npgsql.NpgsqlParameter("@bitAsset", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(50).Value = _bitAsset

                arParms(51) = New Npgsql.NpgsqlParameter("@bitRental", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(51).Value = _bitRental

                arParms(52) = New Npgsql.NpgsqlParameter("@numCategoryProfileID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(52).Value = CategoryProfileID

                arParms(53) = New Npgsql.NpgsqlParameter("@bitVirtualInventory", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(53).Value = IsVirtualInventory

                arParms(54) = New Npgsql.NpgsqlParameter("@bitContainer", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(54).Value = bitContainer

                arParms(55) = New Npgsql.NpgsqlParameter("@numContainer", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(55).Value = numContainer

                arParms(56) = New Npgsql.NpgsqlParameter("@numNoItemIntoContainer", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(56).Value = numNoItemIntoContainer

                arParms(57) = New Npgsql.NpgsqlParameter("@bitMatrix", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(57).Value = IsMatrix

                arParms(58) = New Npgsql.NpgsqlParameter("@vcItemAttributes", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(58).Value = ItemAttributes

                arParms(59) = New Npgsql.NpgsqlParameter("@numManufacturer", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(59).Value = numManufacturer

                arParms(60) = New Npgsql.NpgsqlParameter("@vcASIN", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(60).Value = vcASIN

                arParms(61) = New Npgsql.NpgsqlParameter("@tintKitAssemblyPriceBasedOn", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(61).Value = KitAssemblyPriceBasedOn

                arParms(62) = New Npgsql.NpgsqlParameter("@fltReorderQty", NpgsqlTypes.NpgsqlDbType.Double)
                arParms(62).Value = ReorderQty

                arParms(63) = New Npgsql.NpgsqlParameter("@bitSOWorkOrder", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(63).Value = IsSOWorkOrder

                arParms(64) = New Npgsql.NpgsqlParameter("@bitKitSingleSelect", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(64).Value = IsKitSingleSelect

                arParms(65) = New Npgsql.NpgsqlParameter("@bitExpenseItem", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(65).Value = IsExpenseItem

                arParms(66) = New Npgsql.NpgsqlParameter("@numExpenseChartAcntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(66).Value = ExpenseChartAcntId

                arParms(67) = New Npgsql.NpgsqlParameter("@numBusinessProcessId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(67).Value = BusinessProcessId

                arParms(68) = New Npgsql.NpgsqlParameter("@bitTimeContractFromSalesOrder", NpgsqlTypes.NpgsqlDbType.Boolean)
                arParms(68).Value = _bitTimeContractFromSalesOrder

                arParms(69) = New Npgsql.NpgsqlParameter("@numDepreciationPeriod", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(69).Value = AssetDepreciationPeriod

                arParms(70) = New Npgsql.NpgsqlParameter("@monResidualValue", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(70).Value = AssetResidual

                arParms(71) = New Npgsql.NpgsqlParameter("@tintDepreciationMethod", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(71).Value = AssetDepreciationMethod

                arParms(72) = New Npgsql.NpgsqlParameter("@bitPreventOrphanedParents", NpgsqlTypes.NpgsqlDbType.Boolean)
                arParms(72).Value = IsPreventOrphanedParents

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageItemsAndKits", objParam, True)
                _ItemCode = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

                Return ""
            Catch ex As Exception
                Select Case ex.Message
                    Case "CHILD_WAREHOUSE"
                        Return "Deleted Warehouse is being used by Opportunity, It can not be deleted."
                    Case Else
                        Throw ex
                End Select
            End Try
        End Function
        'Public Function ManageItemsAndKitsForOppNAcc() As Boolean
        '    Try
        '        Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
        '        Dim getconnection As New GetConnection
        '        Dim connString As String = getconnection.GetConnectionString

        '        arParms(0) = New Npgsql.NpgsqlParameter("@strChildItems", NpgsqlTypes.NpgsqlDbType.Text)
        '        arParms(0).Value = _strChildItems

        '        SqlDAL.ExecuteNonQuery(connString, "USP_ManageItemsAndKitsForOppNAcc", arParms)

        '        Return True
        '    Catch ex As Exception
        '        Select Case ex.Message
        '            Case "CHILD_WAREHOUSE"
        '                Return "Deleted Warehouse is being used by Opportunity, It can not be deleted."
        '            Case Else
        '                Throw ex
        '        End Select
        '    End Try
        'End Function



        Public Function GetItemsOrKits() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@strItem", _str, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@type", _type, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@bitAssembly", Assembly, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@numItemCode", ItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numPageIndex", CurrentPage, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@numPageSize", PageSize, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@TotalCount", TotalRecords, NpgsqlTypes.NpgsqlDbType.Bigint, 18, ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@p_SearchType", SearchType, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object = sqlParams.ToArray()

                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetItemsAndKits", objParam, True)

                Me.TotalRecords = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(7).Value)

                Return ds
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetItemsAccountwise(ByVal imode As Integer) As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(9) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DivisionID
                ' 8460405285
                arParms(2) = New Npgsql.NpgsqlParameter("@keyword", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(2).Value = KeyWord

                arParms(3) = New Npgsql.NpgsqlParameter("@startDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                If _StartDate = "#12:00:00 AM#" Then
                    arParms(3).Value = DBNull.Value
                Else

                    arParms(3).Value = _StartDate
                End If
                arParms(4) = New Npgsql.NpgsqlParameter("@endDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                If _EndDate = "#12:00:00 AM#" Then
                    arParms(4).Value = DBNull.Value
                Else
                    arParms(4).Value = _EndDate
                End If



                arParms(5) = New Npgsql.NpgsqlParameter("@imode", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = imode

                arParms(6) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = _CurrentPage

                arParms(7) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(7).Value = _PageSize

                arParms(8) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(8).Direction = ParameterDirection.InputOutput
                arParms(8).Value = _TotalRecords

                arParms(9) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(9).Value = Nothing
                arParms(9).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_OpportunityItems_OrderwiseItems", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(8).Value)

                Return ds
            Catch ex As Exception

                Throw ex
            End Try
        End Function




        Public Function GetItemOrKitList() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(33) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@ItemClassification", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _ItemClassification

                arParms(1) = New Npgsql.NpgsqlParameter("@IsKit", NpgsqlTypes.NpgsqlDbType.Boolean)
                arParms(1).Value = _KitParent

                arParms(2) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(2).Value = _SortCharacter

                arParms(3) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _CurrentPage

                arParms(4) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _PageSize

                arParms(5) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Direction = ParameterDirection.InputOutput
                arParms(5).Value = _TotalRecords

                arParms(6) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(6).Value = _columnName

                arParms(7) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(7).Value = _columnSortOrder

                arParms(8) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = DomainID

                arParms(9) = New Npgsql.NpgsqlParameter("@ItemType", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(9).Value = _ItemType

                arParms(10) = New Npgsql.NpgsqlParameter("@bitSerialized", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(10).Value = _bitSerialized

                arParms(11) = New Npgsql.NpgsqlParameter("@numItemGroup", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(11).Value = _ItemGroupID

                arParms(12) = New Npgsql.NpgsqlParameter("@bitAssembly", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(12).Value = _Assembly

                arParms(13) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(13).Value = UserCntID

                arParms(14) = New Npgsql.NpgsqlParameter("@Where", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(14).Value = _strCondition

                arParms(15) = New Npgsql.NpgsqlParameter("@IsArchive", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(15).Value = _bitArchieve

                arParms(16) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(16).Value = _byteMode

                arParms(17) = New Npgsql.NpgsqlParameter("@bitAsset", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(17).Value = _bitAsset

                arParms(18) = New Npgsql.NpgsqlParameter("@bitRental", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(18).Value = _bitRental

                arParms(19) = New Npgsql.NpgsqlParameter("@bitContainer", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(19).Value = bitContainer

                arParms(20) = New Npgsql.NpgsqlParameter("@SearchText", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(20).Value = SearchText

                arParms(21) = New Npgsql.NpgsqlParameter("@vcRegularSearchCriteria", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(21).Value = RegularSearchCriteria

                arParms(22) = New Npgsql.NpgsqlParameter("@vcCustomSearchCriteria", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(22).Value = CustomSearchCriteria

                arParms(23) = New Npgsql.NpgsqlParameter("@tintDashboardReminderType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(23).Value = DashboardReminderType

                arParms(24) = New Npgsql.NpgsqlParameter("@bitMultiSelect", NpgsqlTypes.NpgsqlDbType.Boolean)
                arParms(24).Value = _bitMultiSelect

                arParms(25) = New Npgsql.NpgsqlParameter("@numFilteredWarehouseID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(25).Value = WarehouseID

                arParms(26) = New Npgsql.NpgsqlParameter("@vcAsile", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(26).Value = vcAsile

                arParms(27) = New Npgsql.NpgsqlParameter("@vcRack", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(27).Value = vcRack

                arParms(28) = New Npgsql.NpgsqlParameter("@vcShelf", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(28).Value = vcShelf

                arParms(29) = New Npgsql.NpgsqlParameter("@vcBin", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(29).Value = vcBin

                arParms(30) = New Npgsql.NpgsqlParameter("@bitApplyFilter", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(30).Value = bitApplyFilter

                arParms(31) = New Npgsql.NpgsqlParameter("@tintUserRightType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(31).Value = UserRightType

                arParms(32) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(32).Value = Nothing
                arParms(32).Direction = ParameterDirection.InputOutput

                arParms(33) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(33).Value = Nothing
                arParms(33).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms

                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_ItemList1", objParam, True)

                _TotalRecords = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(5).Value)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetItemExport() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numWarehouseID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _WarehouseID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_ItemList1_export", arParms)



                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetServiceItem() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@ItemType", NpgsqlTypes.NpgsqlDbType.Char, 10)
                arParms(1).Value = _ItemType

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_ServiceItemList", arParms)


                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private _strCondition As String
        Public Property strCondition() As String
            Get
                Return _strCondition
            End Get
            Set(ByVal value As String)
                _strCondition = value
            End Set
        End Property

        Public Function GetItemsForAPI() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@bitMode", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(2).Value = _mode

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "usp_GetItemsForAPI", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetChildItems() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numKitId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetChildItems", arParms)
                Return ds.Tables(0)
            Catch ex As Exception

                Throw ex
            End Try
        End Function


        Public Function UpdateInventory() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@byteMode", _byteMode, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@numWareHouseItemID", _WareHouseItemID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@Units", _NoofUnits, NpgsqlTypes.NpgsqlDbType.Numeric))

                    .Add(SqlDAL.Add_Parameter("@numWOId", numWOId, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@numOppId", OppId, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@numAssembledItemID", AssembledItemID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@fltQtyRequiredForSingleBuild", QtyRequiredForSingleBuild, NpgsqlTypes.NpgsqlDbType.Numeric))

                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdatingInventory", sqlParams.ToArray())
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function AddVendor() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDivisionId", _DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numItemCode", _ItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitIsPrimaryVendor", _IsPrimaryVendor, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@vcPartNo", PartNo, NpgsqlTypes.NpgsqlDbType.VarChar, 100))
                    .Add(SqlDAL.Add_Parameter("@intMinQty", minQty, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@monCost", monCost, NpgsqlTypes.NpgsqlDbType.Numeric))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_AddVendor", sqlParams.ToArray())

                Return True
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetVendors() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetVendors", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteVendors() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numVendorID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _VendorID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteVendor", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function UpdateVendor() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@strFieldList", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(0).Value = _strFieldList

                arParms(1) = New Npgsql.NpgsqlParameter("@Itemcode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ItemCode

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateVendors", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetPriceManagementList1() As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(14) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@units", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(1).Value = _NoofUnits

                arParms(2) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _OppId

                arParms(3) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _DivisionID

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = DomainID

                arParms(5) = New Npgsql.NpgsqlParameter("@tintOppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _byteMode

                arParms(6) = New Npgsql.NpgsqlParameter("@CalPrice", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(6).Value = _Amount

                arParms(7) = New Npgsql.NpgsqlParameter("@numWareHouseItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _WareHouseItemID

                arParms(8) = New Npgsql.NpgsqlParameter("@bitMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(8).Value = If(_mode = True, 1, 0)

                arParms(9) = New Npgsql.NpgsqlParameter("@vcSelectedKitChildItems", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(9).Value = vcKitChildItems

                arParms(10) = New Npgsql.NpgsqlParameter("@numOppItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(10).Value = OppItemCode

                arParms(11) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(11).Value = SiteID

                arParms(12) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(12).Value = Nothing
                arParms(12).Direction = ParameterDirection.InputOutput

                arParms(13) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(13).Value = Nothing
                arParms(13).Direction = ParameterDirection.InputOutput

                arParms(14) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(14).Value = Nothing
                arParms(14).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_ItemPricingRecomm", arParms)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCompetitors() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetCompetitor", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function AddCompetitor() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ItemCode

                SqlDAL.ExecuteNonQuery(connString, "USP_AddCompetitor", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteCompetitors() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numCompetitorID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CompetitorID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ItemCode

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteCompetitor", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function UpdateCompetitor() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@strFieldList", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(0).Value = _strFieldList

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ItemCode


                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateCompetitor", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function UploadItemImage() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@vcPathForImage", NpgsqlTypes.NpgsqlDbType.VarChar, 300)
                arParms(1).Value = _PathForImage

                arParms(2) = New Npgsql.NpgsqlParameter("@vcPathForTImage", NpgsqlTypes.NpgsqlDbType.VarChar, 300)
                arParms(2).Value = _PathForTImage

                arParms(3) = New Npgsql.NpgsqlParameter("@numOppItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _OppItemCode

                arParms(4) = New Npgsql.NpgsqlParameter("@numCatergoryId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _CategoryID

                SqlDAL.ExecuteNonQuery(connString, "USP_UploadItemImage", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function CreateOrderForECommerce() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(31) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numDivID", NpgsqlTypes.NpgsqlDbType.BigInt, 18)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt, 18)
                arParms(1).Direction = ParameterDirection.InputOutput
                arParms(1).Value = _OppId

                arParms(2) = New Npgsql.NpgsqlParameter("@vcPOppName", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(2).Direction = ParameterDirection.InputOutput
                arParms(2).Value = _OppName

                arParms(3) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt, 18)
                arParms(3).Value = _ContactID

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 18)
                arParms(4).Value = DomainID

                arParms(5) = New Npgsql.NpgsqlParameter("@vcSource", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(5).Value = IIf(_Source = Nothing, DBNull.Value, _Source)

                arParms(6) = New Npgsql.NpgsqlParameter("@numCampainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = IIf(_CampaignID = Nothing, 0, _CampaignID)

                arParms(7) = New Npgsql.NpgsqlParameter("@numCurrencyID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _CurrencyID

                arParms(8) = New Npgsql.NpgsqlParameter("@numBillAddressId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = _BillAddressId

                arParms(9) = New Npgsql.NpgsqlParameter("@numShipAddressId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = _ShipAddressId

                arParms(10) = New Npgsql.NpgsqlParameter("@bitDiscountType", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(10).Value = If(_DiscountType = 1, True, False)

                arParms(11) = New Npgsql.NpgsqlParameter("@fltDiscount", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(11).Value = _Discount

                arParms(12) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(12).Value = _ProID

                arParms(13) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(13).Value = _SiteID

                arParms(14) = New Npgsql.NpgsqlParameter("@tintOppStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(14).Value = _OppStatus

                arParms(15) = New Npgsql.NpgsqlParameter("@monShipCost", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(15).Value = _ShipCost

                arParms(16) = New Npgsql.NpgsqlParameter("@tintSource", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(16).Value = _tintSource

                arParms(17) = New Npgsql.NpgsqlParameter("@tintSourceType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(17).Value = _SourceType

                arParms(18) = New Npgsql.NpgsqlParameter("@numPaymentMethodId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(18).Value = _PaymentMethodId

                arParms(19) = New Npgsql.NpgsqlParameter("@txtComments", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(19).Value = _Comments

                arParms(20) = New Npgsql.NpgsqlParameter("@numPartner", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(20).Value = _numPartner

                arParms(21) = New Npgsql.NpgsqlParameter("@txtFuturePassword", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(21).Value = FuturePassword

                arParms(22) = New Npgsql.NpgsqlParameter("@intUsedShippingCompany", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(22).Value = numShipmentMethod

                arParms(23) = New Npgsql.NpgsqlParameter("@vcPartenerContact", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(23).Value = vcPartenerContact

                arParms(24) = New Npgsql.NpgsqlParameter("@numShipmentMethod", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(24).Value = ShippingService

                arParms(25) = New Npgsql.NpgsqlParameter("@vcOppRefOrderNo", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(25).Value = CustomerPONumber

                arParms(26) = New Npgsql.NpgsqlParameter("@bitBillingTerms", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(26).Value = boolBillingTerms

                arParms(27) = New Npgsql.NpgsqlParameter("@intBillingDays", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(27).Value = BillingDays

                arParms(28) = New Npgsql.NpgsqlParameter("@bitInterestType", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(28).Value = boolInterestType

                arParms(29) = New Npgsql.NpgsqlParameter("@fltInterest", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(29).Value = Interest

                arParms(30) = New Npgsql.NpgsqlParameter("@numOrderPromotionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(30).Value = OrderPromotionID

                arParms(31) = New Npgsql.NpgsqlParameter("@numOrderPromotionDiscountID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(31).Value = OrderPromotionDiscountID

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ShoppingCart", objParam, True)
                _OppId = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(1).Value)
                _OppName = Convert.ToString(DirectCast(objParam, Npgsql.NpgsqlParameter())(2).Value)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteOppItem() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@OppItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OppItemCode

                SqlDAL.ExecuteNonQuery(connString, "USP_OppDeleteItem", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCreditStatusofCompany() As Integer
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return CInt(SqlDAL.ExecuteScalar(connString, "USP_GetCreditStatusOfCompany", arParms))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetAmountDue() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetDueAmount", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetOpenOrderDetails() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetOpenOrderDetails", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function UpdateDealStatus1() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OppId

                arParms(1) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _byteMode

                arParms(2) = New Npgsql.NpgsqlParameter("@Amount", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(2).Value = _Amount

                arParms(3) = New Npgsql.NpgsqlParameter("@ShippingCost", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(3).Value = _ShippingCost

                arParms(4) = New Npgsql.NpgsqlParameter("@numUserContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = UserCntID

                arParms(5) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = DomainID

                arParms(6) = New Npgsql.NpgsqlParameter("@BizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _BizDocID

                arParms(7) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _SiteID

                arParms(8) = New Npgsql.NpgsqlParameter("@numPaymentMethodId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = _PaymentMethodId

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateDealStatus", arParms)
                Return True

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetKitParentsForChildItem() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numChildItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetKitParentsForChildItems", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageKitParentsForChildItem() As Boolean
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numItemDetailID", ItemDetailID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numItemKitID", _KitID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numChildItemID", _ItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numQtyItemsReq", _NoofItemsReqForKit, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@byteMode", _byteMode, NpgsqlTypes.NpgsqlDbType.Smallint))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_ManagekitParentsForChildItems", sqlParams.ToArray())

                Return True
            Catch ex As Exception
                Throw
            End Try
        End Function



        Public Function GetItemDiscProfile() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _byteMode

                arParms(2) = New Npgsql.NpgsqlParameter("@numRelID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _RelID

                arParms(3) = New Npgsql.NpgsqlParameter("@numProID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _ProfileID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_ItemDiscProfile", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageItemDiscProfile() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = _byteMode

                arParms(1) = New Npgsql.NpgsqlParameter("@numDiscProfileID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DiscProfileID

                arParms(2) = New Npgsql.NpgsqlParameter("@decDisc", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(2).Value = _Disc

                arParms(3) = New Npgsql.NpgsqlParameter("@bitApplicable", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Value = _Applicable

                arParms(4) = New Npgsql.NpgsqlParameter("@numItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _ItemCode

                arParms(5) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _DivisionID

                arParms(6) = New Npgsql.NpgsqlParameter("@numRelID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _RelID

                arParms(7) = New Npgsql.NpgsqlParameter("@numProID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _ProfileID


                SqlDAL.ExecuteNonQuery(connString, "USP_ManageItemDiscProfile", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function DeleteItemDiscProfile() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numDiscProfileID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DiscProfileID


                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteDiscProfile", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function



        Public Function GetItemPriceafterdiscount() As Decimal
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@units", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _NoofUnits

                arParms(2) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _DivisionID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return CDec(SqlDAL.ExecuteScalar(connString, "USP_GetItemPrice", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SeleDelCategory() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(12) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numCatergoryId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CategoryID

                arParms(1) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _byteMode

                arParms(2) = New Npgsql.NpgsqlParameter("@str", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(2).Value = _str

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = DomainID

                arParms(4) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = _SiteID

                arParms(5) = New Npgsql.NpgsqlParameter("@numCurrentPage", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(5).Value = _CurrentPage

                arParms(6) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = _PageSize

                arParms(7) = New Npgsql.NpgsqlParameter("@numTotalPage", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(7).Value = _TotalPages

                arParms(8) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(8).Value = _ItemCode

                arParms(9) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(9).Value = _SortCharacter

                arParms(10) = New Npgsql.NpgsqlParameter("@numCategoryProfileID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(10).Value = CategoryProfileID

                arParms(11) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(11).Value = Nothing
                arParms(11).Direction = ParameterDirection.InputOutput

                arParms(12) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(12).Value = Nothing
                arParms(12).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetCategory", arParms)

                'If _byteMode = 5 Or _byteMode = 11 Or _byteMode = 12 Then TotalPages = ds.Tables(1).Rows(0).Item(0)

                Dim dtTable As DataTable
                If ds.Tables.Count = 2 Then
                    If _byteMode = 5 Or _byteMode = 11 Or _byteMode = 12 Then
                        TotalPages = ds.Tables(1).Rows(0).Item(0)
                    ElseIf _byteMode = 18 Then
                        dtTable = ds.Tables(1)

                        If dtTable.Rows.Count > 0 Then
                            _TotalRecords = CInt(dtTable.Rows(0).Item("TotalRowCount"))
                        End If
                        dtTable.Columns.Remove("TotalRowCount")
                        ds.AcceptChanges()
                    End If
                    Return ds.Tables(0)
                End If

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCategoriesAndItems() As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numCategoryID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _CategoryID

                arParms(2) = New Npgsql.NpgsqlParameter("@numWareHouseID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _WarehouseID

                arParms(3) = New Npgsql.NpgsqlParameter("@str", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(3).Value = _str

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetCategoriesAndItems", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetInventoryItemStatus() As String
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numWareHouseID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = IIf(_WarehouseID > 0, _WarehouseID, DBNull.Value)

                arParms(2) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ItemCode

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "usp_GetInventoryItemStatus", arParms)

                If ds.Tables(0).Rows.Count > 0 Then
                    Return ds.Tables(0).Compute("SUM(OnHand)", "").ToString()
                Else
                    Return ""
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageCategory() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(12) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numCatergoryId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CategoryID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcCatgoryName", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(1).Value = _CategoryName

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@vcDescription", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = _CategoryDescription

                arParms(4) = New Npgsql.NpgsqlParameter("@intDisplayOrder", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _DisplayOrder

                arParms(5) = New Npgsql.NpgsqlParameter("@vcPathForCategoryImage", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(5).Value = _PathForImage

                arParms(6) = New Npgsql.NpgsqlParameter("@numDepCategoryID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _ParentCategoryID

                arParms(7) = New Npgsql.NpgsqlParameter("@numCategoryProfileID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = CategoryProfileID

                arParms(8) = New Npgsql.NpgsqlParameter("@vcCatgoryNameURL", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(8).Value = CategoryNameURL

                arParms(9) = New Npgsql.NpgsqlParameter("@vcMetaTitle", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(9).Value = CategoryMetaTitle

                arParms(10) = New Npgsql.NpgsqlParameter("@vcMetaKeywords", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(10).Value = CategoryMetaKeywords

                arParms(11) = New Npgsql.NpgsqlParameter("@vcMetaDescription", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(11).Value = CategoryMetaDescription

                arParms(12) = New Npgsql.NpgsqlParameter("@vcMatrixGroups", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(12).Value = MatrixGroups

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageCategory", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageSubCategoriesAndItems() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numCategoryID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CategoryID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSubCategory", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _SubCategoryID

                arParms(2) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ItemCode

                arParms(3) = New Npgsql.NpgsqlParameter("@tintChecked", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _Checked

                arParms(4) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _byteMode


                SqlDAL.ExecuteNonQuery(connString, "USP_ManageItemCategories", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageItemExtendedDesc() As String
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _byteMode

                arParms(2) = New Npgsql.NpgsqlParameter("@txtDesc", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(2).Value = _str

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@txtShortDesc", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = txtShortDesc

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Return CStr(SqlDAL.ExecuteScalar(connString, "USP_ShoppingcartHDRDtls", arParms))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ViewItemExtendedDesc() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _byteMode

                arParms(2) = New Npgsql.NpgsqlParameter("@txtDesc", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(2).Value = _str

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@txtShortDesc", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(4).Value = txtShortDesc

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_ShoppingcartHDRDtls", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function



        Public Function FindItem() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@str", NpgsqlTypes.NpgsqlDbType.VarChar, 20)
                arParms(0).Value = _str

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return (SqlDAL.ExecuteDataset(connString, "usp_FindItem", arParms).Tables(0))

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetWarehouseFromDivID() As Long
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _byteMode

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return Common.CCommon.ToLong(SqlDAL.ExecuteScalar(connString, "USP_FindWareHouse", arParms))


            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetDiscountItems() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@str", NpgsqlTypes.NpgsqlDbType.VarChar, 20)
                arParms(1).Value = _str

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return (SqlDAL.ExecuteDataset(connString, "USP_GetDiscountItem", arParms).Tables(0))

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetUnArchivedServiceItems() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@str", NpgsqlTypes.NpgsqlDbType.VarChar, 20)
                arParms(1).Value = _str

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return (SqlDAL.ExecuteDataset(connString, "USP_GetUnArchivedServiceItems", arParms).Tables(0))

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function ManageWarehouse() As Long
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = _byteMode

                arParms(1) = New Npgsql.NpgsqlParameter("@numWareHouseID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _WarehouseID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcWarehouse", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(2).Value = _Warehouse

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = DomainID

                arParms(4) = New Npgsql.NpgsqlParameter("@numAddressID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = ShipAddressId

                arParms(5) = New Npgsql.NpgsqlParameter("@vcPrintNodeAPIKey", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(5).Value = PrintNodeAPIKey

                arParms(6) = New Npgsql.NpgsqlParameter("@vcPrintNodePrinterID", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(6).Value = PrintNodePrinterID

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_ManageWarehouse", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetWareHouses() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numWareHouseID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _WarehouseID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return (SqlDAL.ExecuteDataset(connString, "USP_GetWarehouses", arParms).Tables(0))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetItemGroups() As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemGroupID", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _ItemGroupID

                arParms(2) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _ItemCode

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetItemGroups", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetProfileItems() As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _ItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_CheckProfileItem", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageItemGroups() As Long
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@strOpt", NpgsqlTypes.NpgsqlDbType.VarChar, 4000)
                arParms(1).Value = _strFieldList

                arParms(2) = New Npgsql.NpgsqlParameter("@strAttr", NpgsqlTypes.NpgsqlDbType.VarChar, 4000)
                arParms(2).Value = _str

                arParms(3) = New Npgsql.NpgsqlParameter("@numItemGroupID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _ItemGroupID

                arParms(4) = New Npgsql.NpgsqlParameter("@vcName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(4).Value = _ItemGroupName

                arParms(5) = New Npgsql.NpgsqlParameter("@bitCombineAssemblies", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(5).Value = _bitCombineAssemblies

                arParms(6) = New Npgsql.NpgsqlParameter("@numMapToDropdownFld", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _numMapToDropdownFld

                arParms(7) = New Npgsql.NpgsqlParameter("@numProfileItem", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _numProfileItem

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_ManageItemGroups", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageItemsGroupsDTL_OPPNACC() As Long
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(10) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numItemGroupID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ItemGroupID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppAccAttrID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@numQtyItemsReq", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _NoofItemsReqForKit

                arParms(3) = New Npgsql.NpgsqlParameter("@numDefaultSelect", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _DefaultSelect

                arParms(4) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _byteMode

                arParms(5) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = DomainID

                arParms(6) = New Npgsql.NpgsqlParameter("@vcName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(6).Value = _ItemGroupName

                arParms(7) = New Npgsql.NpgsqlParameter("@bitCombineAssemblies", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(7).Value = _bitCombineAssemblies

                arParms(8) = New Npgsql.NpgsqlParameter("@numMapToDropdownFld", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = _numMapToDropdownFld

                arParms(9) = New Npgsql.NpgsqlParameter("@numProfileItem", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = _numProfileItem

                arParms(10) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(10).Value = Nothing
                arParms(10).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_ManageItemsGroupsDTL_OPPNACC", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteItemGroups() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numItemGroupID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ItemGroupID



                Return SqlDAL.ExecuteNonQuery(connString, "USP_DeleteItemGroups", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetItemWareHouses() As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _byteMode

                arParms(2) = New Npgsql.NpgsqlParameter("@numWarehouseItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = WareHouseItemID

                arParms(3) = New Npgsql.NpgsqlParameter("@numWarehouseID", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _WarehouseID

                arParms(4) = New Npgsql.NpgsqlParameter("@numWLocationID", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _WarehouseLocationID

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetWareHouseItems", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetItemAttributesForAPI() As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@WebApiId", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _WebApiId

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "Usp_GetItemAttributesForAPI", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetAssetSerial() As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "usp_GetAssetSerial", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        <Obsolete("This is old one not required. we are using Function AddUpdateWareHouseForItems ")>
        Public Function AddWareHouseForitems() As Long
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(9) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@numWareHouseID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _WarehouseID

                arParms(2) = New Npgsql.NpgsqlParameter("@numWareHouseItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _WareHouseItemID

                arParms(3) = New Npgsql.NpgsqlParameter("@vcLocation", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = _WLocation

                arParms(4) = New Npgsql.NpgsqlParameter("@monWListPrice", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(4).Value = _WListPrice

                arParms(5) = New Npgsql.NpgsqlParameter("@numOnHand", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(5).Value = _WOnHand

                arParms(6) = New Npgsql.NpgsqlParameter("@vcWHSKU", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(6).Value = _WSKU

                arParms(7) = New Npgsql.NpgsqlParameter("@vcBarCode", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(7).Value = _WBarCode

                arParms(8) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = DomainID

                arParms(9) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(9).Value = Nothing
                arParms(9).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_AddWareHouseForItems", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetOptAccWareHouses() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetWareHouseOptionsAcc", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetOppItemAttributes() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetItemAttributes", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetOppItemAttributesForEcomm() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@numWarehouseID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _WarehouseID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetItemAttributesEcomm", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetAssetInvetory() As DataTable
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Dim ds As DataSet
            Try
                ' Set up parameters 
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                ' @ItemCount Input Parameter 
                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetAssetItem", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            Finally

            End Try

        End Function

        Public Sub SaveAssetItem()
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(11) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numDivId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.VarChar, 8000)
                arParms(1).Value = _strItemCodes

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _type

                arParms(4) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _ItemCode

                arParms(5) = New Npgsql.NpgsqlParameter("@numDeptId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _DepartmentID

                arParms(6) = New Npgsql.NpgsqlParameter("@btApp", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(6).Value = _Appreciation

                arParms(7) = New Npgsql.NpgsqlParameter("@btDep", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(7).Value = _Deppreciation

                arParms(8) = New Npgsql.NpgsqlParameter("@numAppValue", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(8).Value = _AppreValue

                arParms(9) = New Npgsql.NpgsqlParameter("@numDepValue", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(9).Value = _DepreValue

                arParms(10) = New Npgsql.NpgsqlParameter("@dtPurchaseDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(10).Value = _PurchaseDate

                arParms(11) = New Npgsql.NpgsqlParameter("@dtWarrentyDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(11).Value = _WarrentyTill

                SqlDAL.ExecuteNonQuery(connString, "usp_SaveAssets", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Function getCompanyAssetsSerial() As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(11) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _CurrentPage

                arParms(3) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Direction = ParameterDirection.InputOutput
                arParms(3).Value = _TotalRecords

                arParms(4) = New Npgsql.NpgsqlParameter("@numItemClassification", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _ItemClassification

                If _KeyWord Is Nothing Then _KeyWord = ""
                arParms(5) = New Npgsql.NpgsqlParameter("@vcKeyWord", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(5).Value = _KeyWord

                If _columnName Is Nothing Then _columnName = ""
                arParms(6) = New Npgsql.NpgsqlParameter("@vcSortColumn", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(6).Value = _columnName

                If _columnSortOrder Is Nothing Then _columnSortOrder = ""
                arParms(7) = New Npgsql.NpgsqlParameter("@vcSort", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(7).Value = _columnSortOrder

                arParms(8) = New Npgsql.NpgsqlParameter("@vcSortChar", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(8).Value = _SortCharacter

                arParms(9) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(9).Value = _PageSize

                arParms(10) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(10).Value = Nothing
                arParms(10).Direction = ParameterDirection.InputOutput

                arParms(11) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(11).Value = Nothing
                arParms(11).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "usp_getCompanyAssets", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(3).Value)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Sub DeleteItemFromCmpAsset()
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numDivId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numAItemCode", NpgsqlTypes.NpgsqlDbType.VarChar, 8000)
                arParms(1).Value = _ItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID


                SqlDAL.ExecuteNonQuery(connString, "usp_DeleteItemCmpAsset", arParms)
            Catch ex As Exception

            End Try
        End Sub

        Public Function AddWarehouseDetails() As Integer
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numWareHouseDetailID", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(0).Value = _WareHouseDetailID

                arParms(1) = New Npgsql.NpgsqlParameter("@numCountry", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _Country

                arParms(2) = New Npgsql.NpgsqlParameter("@numState", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _State

                arParms(3) = New Npgsql.NpgsqlParameter("@numWarehouseID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _WarehouseID

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = DomainID

                arParms(5) = New Npgsql.NpgsqlParameter("@bitMode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _mode

                arParms(6) = New Npgsql.NpgsqlParameter("@numFromPin", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = _FromPinCode

                arParms(7) = New Npgsql.NpgsqlParameter("@numToPin", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(7).Value = _ToPinCode

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                Return CType(SqlDAL.ExecuteScalar(connString, "usp_ManageWarehouseDetails", arParms), Integer)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetWareHousesDetails() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As New DataSet
                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetWarehouseDetails", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function getItemList() As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@varFilter", NpgsqlTypes.NpgsqlDbType.VarChar, 30)
                arParms(1).Value = _Filter

                arParms(2) = New Npgsql.NpgsqlParameter("@type", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _type

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_getItemList", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GateWayDtls() As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSiteId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = numSiteId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetGatewatDTls", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetOrganizationLocation() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DivisionID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetCompanyAllAddress", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetWarehouseID() As Long
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numWareHouseItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _WareHouseItemID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_GetWareHouseID", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageGateWayDtls() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID


                arParms(1) = New Npgsql.NpgsqlParameter("@str", NpgsqlTypes.NpgsqlDbType.Xml)
                arParms(1).Value = _str

                arParms(2) = New Npgsql.NpgsqlParameter("@numSiteId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _numSiteId

                arParms(3) = New Npgsql.NpgsqlParameter("@numPaymentGatewayID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = PaymentGatewayID

                Return SqlDAL.ExecuteNonQuery(connString, "USP_ManageGatewayDTLs", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ShippingDtls() As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numListItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ShippingCMPID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetShippingDTLs", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageShipDTLs() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numLIstItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ShippingCMPID


                arParms(2) = New Npgsql.NpgsqlParameter("@str", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(2).Value = _str

                Return SqlDAL.ExecuteNonQuery(connString, "USP_ManageShipFields", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private _FilterCustomWhere As String
        Public Property FilterCustomWhere() As String
            Get
                Return _FilterCustomWhere
            End Get
            Set(ByVal value As String)
                _FilterCustomWhere = value
            End Set
        End Property

        Private _FilterItemAttributes As String
        Public Property FilterItemAttributes() As String
            Get
                Return _FilterItemAttributes
            End Get
            Set(ByVal value As String)
                _FilterItemAttributes = value
            End Set
        End Property

        Public Function GetManufactuerersForECommerce() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numRelationshipID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _DivisionID

                arParms(2) = New Npgsql.NpgsqlParameter("@numShowOnWebField", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _numShowOnWebFieldId

                arParms(3) = New Npgsql.NpgsqlParameter("@numRecordID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _ManufacturerID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet

                ds = SqlDAL.ExecuteDataset(connString, "USP_ManufacturerList", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetItemsForECommerce() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(17) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _SiteID

                arParms(1) = New Npgsql.NpgsqlParameter("@numCategoryID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _CategoryID

                arParms(2) = New Npgsql.NpgsqlParameter("@numWareHouseID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _WarehouseID

                arParms(3) = New Npgsql.NpgsqlParameter("@SortBy", NpgsqlTypes.NpgsqlDbType.VarChar, 8000)
                arParms(3).Value = _SortByString

                arParms(4) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _CurrentPage

                arParms(5) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _PageSize

                arParms(6) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Direction = ParameterDirection.InputOutput
                arParms(6).Value = _TotalRecords

                arParms(7) = New Npgsql.NpgsqlParameter("@SearchText", NpgsqlTypes.NpgsqlDbType.VarChar, 8000)
                arParms(7).Value = _SearchText

                arParms(8) = New Npgsql.NpgsqlParameter("@FilterQuery", NpgsqlTypes.NpgsqlDbType.VarChar, 8000)
                arParms(8).Value = _FilterQuery

                arParms(9) = New Npgsql.NpgsqlParameter("@FilterRegularCondition", NpgsqlTypes.NpgsqlDbType.VarChar, 8000)
                arParms(9).Value = _FilterRegularCondition

                arParms(10) = New Npgsql.NpgsqlParameter("@FilterCustomFields", NpgsqlTypes.NpgsqlDbType.VarChar, 8000)
                arParms(10).Value = _FilterCustomFields

                arParms(11) = New Npgsql.NpgsqlParameter("@FilterCustomWhere", NpgsqlTypes.NpgsqlDbType.VarChar, 8000)
                arParms(11).Value = _FilterCustomWhere

                arParms(12) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(12).Value = DivisionID

                arParms(13) = New Npgsql.NpgsqlParameter("@numManufacturerID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(13).Value = ManufacturerID

                arParms(14) = New Npgsql.NpgsqlParameter("@FilterItemAttributes", NpgsqlTypes.NpgsqlDbType.VarChar, 8000)
                arParms(14).Value = _FilterItemAttributes

                arParms(15) = New Npgsql.NpgsqlParameter("@numESItemCodes", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(15).Value = strItemCodes

                arParms(16) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(16).Value = Nothing
                arParms(16).Direction = ParameterDirection.InputOutput

                arParms(17) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(17).Value = Nothing
                arParms(17).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms

                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_ItemsForECommerce", objParam, True)

                _TotalRecords = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(6).Value)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetItemAttributesEcommerce() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numWareHouseID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = WarehouseID

                arParms(2) = New Npgsql.NpgsqlParameter("@FilterQuery", NpgsqlTypes.NpgsqlDbType.VarChar, 8000)
                arParms(2).Value = _FilterQuery

                arParms(3) = New Npgsql.NpgsqlParameter("@numCategoryID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _CategoryID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetItemAttributesEcommerce", arParms)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetFilterAttributes() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numCategoryID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _CategoryID

                arParms(2) = New Npgsql.NpgsqlParameter("@FilterQuery", NpgsqlTypes.NpgsqlDbType.VarChar, 8000)
                arParms(2).Value = _FilterQuery

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetProductFilters", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageSiteCategory() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numCatergoryId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _SiteID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcCatgoryName", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _str

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageSiteCategories", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCategoryID() As Long
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@vcCategoryName", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _CategoryName

                arParms(2) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _SiteID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return Convert.ToInt64(SqlDAL.ExecuteScalar(connString, "USP_GetCategoryIDForBreadCrumb", arParms))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ValidateItemAccount(ByVal lngItemCode As Long, ByVal lngDomainID As Long, ByVal OppType As Short) As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = lngItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = lngDomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintOppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = OppType

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                If Common.CCommon.ToInteger(SqlDAL.ExecuteScalar(connString, "USP_ValidateItemAccounts", arParms)) = 0 Then
                    Return False
                Else
                    Return True
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageItemSerialNo() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@strFieldList", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _strFieldList

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = UserCntID

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageItemSerialNo", arParms)
                Return True
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ManageAssetSerial() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@strFieldList", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _strFieldList

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageAssetSerial", arParms)
                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Function DeleteCompanyAssetSerial() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numAssetItemDTLID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _WareHouseDetailID

                SqlDAL.ExecuteNonQuery(connString, "USP_DelCompanyAssetSerial", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteWarehouseDetailItem() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@WareHouseDetailID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _WareHouseDetailID

                arParms(3) = New Npgsql.NpgsqlParameter("@WareHouseItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _WareHouseItemID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                If Common.CCommon.ToInteger(SqlDAL.ExecuteScalar(connString, "USP_DeleteWarehouseItem", arParms)) = 0 Then
                    Return False
                Else
                    Return True
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function CartParentCategory() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _SiteID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return Convert.ToBoolean(SqlDAL.ExecuteScalar(connString, "USP_GetCartParentCategory", arParms))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetChildItemsForKitsAss() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numKitId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@numWarehouseItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = WareHouseItemID

                arParms(2) = New Npgsql.NpgsqlParameter("@bitApplyFilter", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(2).Value = bitApplyFilter

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetChildItemsForKitsAss", arParms)
                Return ds.Tables(0)
            Catch ex As Exception

                Throw ex
            End Try
        End Function

        Public Function SaveCategoryDisplayOrder() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(0).Value = _str

                Return SqlDAL.ExecuteNonQuery(connString, "USP_SaveCategoryDisplayOrder", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageWorkOrder() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(12) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@numWareHouseItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _WareHouseItemID

                arParms(2) = New Npgsql.NpgsqlParameter("@Units", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _NoofUnits

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = DomainID

                arParms(4) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = UserCntID

                arParms(5) = New Npgsql.NpgsqlParameter("@vcInstruction", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(5).Value = _Instruction

                arParms(6) = New Npgsql.NpgsqlParameter("@bintCompliationDate", NpgsqlTypes.NpgsqlDbType.Date)
                arParms(6).Value = _CompliationDate

                arParms(7) = New Npgsql.NpgsqlParameter("@numAssignedTo", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _AssignTo

                arParms(8) = New Npgsql.NpgsqlParameter("@numSalesOrderId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = _SalesOrderId

                arParms(9) = New Npgsql.NpgsqlParameter("@numBuildProcessId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = _BusinessProcessId

                arParms(10) = New Npgsql.NpgsqlParameter("@dtmProjectFinishDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                If _ProjectFinishDate = "#12:00:00 AM#" Then
                    arParms(10).Value = DBNull.Value
                Else

                    arParms(10).Value = _ProjectFinishDate
                End If

                arParms(11) = New Npgsql.NpgsqlParameter("@dtmEndDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                If _EndDate = "#12:00:00 AM#" Then
                    arParms(11).Value = DBNull.Value
                Else

                    arParms(11).Value = _EndDate
                End If

                arParms(12) = New Npgsql.NpgsqlParameter("@dtmStartDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                If _StartDate = "#12:00:00 AM#" Then
                    arParms(12).Value = DBNull.Value
                Else

                    arParms(12).Value = _StartDate
                End If

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageWorkOrder", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetWorkOrder() As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(12) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _ClientTimeZoneOffset

                arParms(2) = New Npgsql.NpgsqlParameter("@numWOStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _numWOStatus

                arParms(3) = New Npgsql.NpgsqlParameter("@SortExpression", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(3).Value = _columnName

                arParms(4) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _CurrentPage

                arParms(5) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _PageSize

                arParms(6) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = UserCntID

                arParms(7) = New Npgsql.NpgsqlParameter("@vcWOId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(7).Value = _vcWOId

                arParms(8) = New Npgsql.NpgsqlParameter("@tintFilter", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(8).Value = Convert.ToInt16(Filter)

                arParms(9) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = OppId

                arParms(10) = New Npgsql.NpgsqlParameter("@numOppItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(10).Value = OppItemCode

                arParms(11) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(11).Value = Nothing
                arParms(11).Direction = ParameterDirection.InputOutput

                arParms(12) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(12).Value = Nothing
                arParms(12).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetWorkOrder", arParms)
                Return ds
            Catch ex As Exception

                Throw ex
            End Try
        End Function

        Public Function ManageWorkOrderStatus() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numWOId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _numWOId

                arParms(1) = New Npgsql.NpgsqlParameter("@numWOStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _numWOStatus

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = UserCntID

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageWorkOrderStatus", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteWorkOrder() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numWOId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _numWOId

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@bitOrderDelete", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Value = bitOrderDelete

                arParms(4) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = 1 'Order/Item

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteWorkOrder", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function InsertAssemblyItems() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numItemKitID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@numChildItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ChildItemID

                arParms(2) = New Npgsql.NpgsqlParameter("@numQtyItemsReq", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(2).Value = _NoofUnits

                arParms(3) = New Npgsql.NpgsqlParameter("@numUOMId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _numBaseUnit

                arParms(4) = New Npgsql.NpgsqlParameter("@numWareHouseId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _WarehouseID

                arParms(5) = New Npgsql.NpgsqlParameter("@vcItemDesc", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(5).Value = _ItemDesc

                arParms(6) = New Npgsql.NpgsqlParameter("@sintOrder", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = _intOrder

                arParms(7) = New Npgsql.NpgsqlParameter("@numItemDetailID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Direction = ParameterDirection.InputOutput
                arParms(7).Value = _ItemDetailID

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_InsertAssemblyItems", objParam, True)
                _ItemDetailID = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(7).Value)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetWareHouseItemID(ByVal vcText As String) As Long
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numItemKitID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ChildItemID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcText", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(1).Value = vcText

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_GetWareHouseItemID", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetVendorsWareHouse() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@numVendorid", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _VendorID

                arParms(3) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _byteMode

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetVendorsWareHouse", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetVendorShipmentMethod() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numVendorid", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _VendorID

                arParms(2) = New Npgsql.NpgsqlParameter("@numAddressID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ShipAddressId

                arParms(3) = New Npgsql.NpgsqlParameter("@numWarehouseID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = WarehouseID

                arParms(4) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _byteMode

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetVendorShipmentMethod", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageVendorShipmentMethod() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numVendorid", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _VendorID

                arParms(2) = New Npgsql.NpgsqlParameter("@numAddressID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ShipAddressId

                arParms(3) = New Npgsql.NpgsqlParameter("@numWarehouseID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = WarehouseID

                arParms(4) = New Npgsql.NpgsqlParameter("@str", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(4).Value = _str

                arParms(5) = New Npgsql.NpgsqlParameter("@bitPrimary", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(5).Value = _bitPrimary


                SqlDAL.ExecuteNonQuery(connString, "USP_ManageVendorShipmentMethod", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Sub AdjustInventory(ByVal ModifiedQuantity As Decimal, ByVal Mode As Short)
            Try
                Dim OriginalQuantity As Decimal
                Dim lngItemCode As Long

                Dim lngWIPAccount As Long ' Work in progress account
                Dim ds As DataSet
                Dim dt As DataTable
                Dim objItem As New CItems

                DomainID = HttpContext.Current.Session("DomainID")
                _ClientTimeZoneOffset = HttpContext.Current.Session("ClientMachineUTCTimeOffset")
                _numWOStatus = 0
                _PageSize = HttpContext.Current.Session("PagingRows")
                _vcWOId = _numWOId
                _CurrentPage = 1
                _columnName = "numWOId"


                ds = GetWorkOrder()
                dt = ds.Tables(0)

                If Mode = 0 Then 'from Inventory adjustment screens 
                    Dim dr() As DataRow = dt.Select("numWODetailID=" & _numWODetailID)
                    If dr.Length > 0 Then
                        OriginalQuantity = dr(0)("numQtyItemsReq")
                        lngItemCode = dr(0)("numItemCode")
                    End If

                ElseIf Mode = 1 Then ' from workorder screen
                    Dim dr() As DataRow = dt.Select("numParentWOID='0'")
                    If dr.Length > 0 Then
                        OriginalQuantity = dr(0)("numQtyItemsReq")
                        lngItemCode = dr(0)("numItemCode")
                    End If
                End If


                objItem.ItemCode = lngItemCode
                objItem.ClientZoneOffsetTime = HttpContext.Current.Session("ClientMachineUTCTimeOffset")
                dt = objItem.ItemDetails
                If dt.Rows.Count > 0 Then
                    _AssetChartAcntId = CCommon.ToLong(dt.Rows(0)("numAssetChartAcntId"))
                    _AverageCost = CCommon.ToDecimal(dt.Rows(0)("monAverageCost"))
                End If


                lngWIPAccount = ChartOfAccounting.GetDefaultAccount("WP", HttpContext.Current.Session("DomainID"))

                Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                    If Mode = 0 Then 'from Inventory adjustment screens 
                        If _AssetChartAcntId > 0 And lngWIPAccount > 0 And OriginalQuantity <> ModifiedQuantity Then
                            Dim journalID As Long = SaveJournalHeader(Math.Abs(ModifiedQuantity - OriginalQuantity))
                            SaveDataToGeneralJournalDetails(journalID, _AssetChartAcntId, lngWIPAccount, ModifiedQuantity - OriginalQuantity, _AverageCost, Mode, objItem.ItemCode)
                        End If
                    ElseIf Mode = 1 Then
                        If _AssetChartAcntId > 0 And lngWIPAccount > 0 Then
                            Dim journalID As Long = SaveJournalHeader(Math.Abs(OriginalQuantity))
                            SaveDataToGeneralJournalDetails(journalID, _AssetChartAcntId, lngWIPAccount, OriginalQuantity, _AverageCost, Mode, objItem.ItemCode)
                        End If
                    End If

                    objTransactionScope.Complete()
                End Using

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function SaveJournalHeader(ByVal Quantity As Decimal) As Long
            Try
                Dim objJEHeader As New JournalEntryHeader
                Dim lngJournalId As Long = 0
                With objJEHeader
                    .JournalId = 0
                    .RecurringId = 0
                    .EntryDate = CDate(Date.Now.ToShortDateString() & " 12:00:00") 'bug id 1028
                    .Description = "Stock Adjustment through Workorder"
                    .Amount = CCommon.ToDecimal(_AverageCost * Quantity)
                    .CheckId = 0
                    .CashCreditCardId = 0
                    .ChartAcntId = 0
                    .OppId = 0
                    .OppBizDocsId = 0
                    .DepositId = 0
                    .BizDocsPaymentDetId = 0
                    .IsOpeningBalance = 0
                    .LastRecurringDate = Date.Now
                    .NoTransactions = 0
                    .CategoryHDRID = 0
                    .ReturnID = 0
                    .CheckHeaderID = 0
                    .BillID = 0
                    .BillPaymentID = 0
                    .UserCntID = HttpContext.Current.Session("UserContactID")
                    .DomainID = HttpContext.Current.Session("DomainId")
                End With
                lngJournalId = objJEHeader.Save()
                Return lngJournalId
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Sub SaveDataToGeneralJournalDetails(ByVal lintJournalId As Integer, ByVal lngAssetAccont As Long, ByVal lngWIPAccount As Long, ByVal Quantity As Decimal, ByVal monAverageCost As Decimal, ByVal Mode As Short, Optional ItemID As Long = 0)
            Dim lstr As String
            Dim ds As New DataSet
            Dim objJournalEntry As New Accounting.JournalEntry

            Try
                Dim decDebitAmtAsset As Decimal
                Dim decCreditAmtAsset As Decimal
                Dim decDebitAmtWIP As Decimal
                Dim decCreditAmtWIP As Decimal

                If Mode = 0 Then
                    If Quantity > 0 Then
                        decDebitAmtAsset = 0
                        decCreditAmtAsset = Math.Abs(monAverageCost * Quantity)
                        decDebitAmtWIP = Math.Abs(monAverageCost * Quantity)
                        decCreditAmtWIP = 0
                    ElseIf Quantity < 0 Then
                        decDebitAmtAsset = Math.Abs(monAverageCost * Quantity)
                        decCreditAmtAsset = 0
                        decDebitAmtWIP = 0
                        decCreditAmtWIP = Math.Abs(monAverageCost * Quantity)
                    End If
                ElseIf Mode = 1 Then
                    decDebitAmtAsset = Math.Abs(monAverageCost * Quantity)
                    decCreditAmtAsset = 0
                    decDebitAmtWIP = 0
                    decCreditAmtWIP = Math.Abs(monAverageCost * Quantity)
                End If

                Dim objJEList As New JournalEntryCollection

                Dim objJE As New JournalEntryNew()

                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = decDebitAmtAsset
                objJE.CreditAmt = decCreditAmtAsset
                objJE.ChartAcntId = lngAssetAccont 'asset account
                objJE.Description = ""
                objJE.CustomerId = 0
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = ""
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 0
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = ItemID
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)

                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = decDebitAmtWIP
                objJE.CreditAmt = decCreditAmtWIP
                objJE.ChartAcntId = lngWIPAccount 'expense account
                objJE.Description = ""
                objJE.CustomerId = 0
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = ""
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 0
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)

                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lintJournalId, HttpContext.Current.Session("DomainId"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function ValidateeWorkOrderInventory() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numWOId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _numWOId

                arParms(1) = New Npgsql.NpgsqlParameter("@numWOStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _numWOStatus

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_ValidateeWorkOrderInventory", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function ManageSimilarItem() As String
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numParentItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ParentItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ItemCode

                arParms(3) = New Npgsql.NpgsqlParameter("@vcRelationship", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = _Relationship

                arParms(4) = New Npgsql.NpgsqlParameter("@bitPreUpSell", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(4).Value = _bitPreUpSell

                arParms(5) = New Npgsql.NpgsqlParameter("@bitPostUpSell", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(5).Value = _bitPostUpSell

                'Added by Neelam Kapila || 11/01/2017 - Modified procedure for related item display
                arParms(6) = New Npgsql.NpgsqlParameter("@bitRequired", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(6).Value = _bitRequired

                arParms(7) = New Npgsql.NpgsqlParameter("@vcUpSellDesc", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(7).Value = _strPostUpSellDesc

                arParms(8) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(8).Value = byteMode

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageSimilarItem", objParam, True)
                _ItemCode = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

                Return ""
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteSimilarItems() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numParentItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ParentItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ItemCode

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteSimilarItem", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetSimilarItem() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numParentItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ParentItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _byteMode

                arParms(3) = New Npgsql.NpgsqlParameter("@vcCookieId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = vcCookieId

                arParms(4) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = UserCntID

                arParms(5) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = SiteID

                arParms(6) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = OppId

                arParms(7) = New Npgsql.NpgsqlParameter("@numWarehouseID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = WarehouseID

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetSimilarItem", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Added by Neelam Kapila || 11/01/2017 - Added procedure to fetch Required related item 
        Public Function GetSimilarRequiredItem() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numParentItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ParentItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@numWarehouseID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = WarehouseID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetSimilarRequiredItem", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Added by Neelam Kapila || 11/01/2017 - Added procedure to fetch promotions for related item 
        Public Function GetPromotionsForSimilarItems() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = ItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@numWarehouseItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = WareHouseItemID

                arParms(3) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = SiteID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetPromotionsForSimilarItems", arParms)
                If ds.Tables.Count > 0 Then
                    Return ds.Tables(0)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Added by Neelam Kapila || 02/01/2018 - Added procedure to fetch SubTotal for Order Based Promotion 
        Public Function GetPromotionOfferForIncentiveSaleItems(monTotAmtBefDiscount As Decimal, strNumItemCode As String) As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@monTotAmtBefDiscount", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(1).Value = monTotAmtBefDiscount

                arParms(2) = New Npgsql.NpgsqlParameter("@itemIdSearch", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = strNumItemCode

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetPromotionOfferForIncentiveSaleItems", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Added by Neelam Kapila || 02/02/2018 - Added procedure to fetch Order Based Promotion Id for Cart Items
        '/* Public Function GetPromotionDetailForCartItems(ItemCodes As String) As DataTable
        '     Try
        '         Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
        '         Dim getconnection As New GetConnection
        '         Dim connString As String = GetConnection.GetConnectionString
        '         Dim ds As DataSet

        '         arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
        '         arParms(0).Value = DomainID

        '         arParms(1) = New Npgsql.NpgsqlParameter("@itemIdSearch", NpgsqlTypes.NpgsqlDbType.VarChar)
        '         arParms(1).Value = ItemCodes

        '         arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
        '         arParms(2).Value = Nothing
        '         arParms(2).Direction = ParameterDirection.InputOutput

        '         ds = SqlDAL.ExecuteDataset(connString, "USP_GetPromotionDTLForCartItems", arParms)
        '         Return ds.Tables(0)
        '     Catch ex As Exception
        '         Throw ex
        '     End Try
        ' End Function*/

        Public Function GetFeaturedItems() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numNewDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numReportId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _numReportId

                arParms(2) = New Npgsql.NpgsqlParameter("@numSiteId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _numSiteId

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_FeaturedItem", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetItemMaintenanceRepair() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _ClientZoneOffsetTime

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetItemMaintenanceRepair", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetItemForUpdateWizard() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetItemForUpdate", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetImportWareHouseItems() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemGroupID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ItemGroupID

                arParms(2) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _byteMode

                arParms(3) = New Npgsql.NpgsqlParameter("@bitSerialized", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Value = _bitSerialized

                arParms(4) = New Npgsql.NpgsqlParameter("@bitLotNo", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(4).Value = _bitLotNo

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetImportWareHouseItems", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function AddUpdateWareHouseForItems() As Long
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(18) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@numWareHouseID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _WarehouseID

                arParms(2) = New Npgsql.NpgsqlParameter("@numWareHouseItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Direction = ParameterDirection.InputOutput
                arParms(2).Value = _WareHouseItemID

                'Dim objParam As New Npgsql.NpgsqlParameter
                'objParam.DbType = NpgsqlTypes.NpgsqlDbType.BigInt
                'objParam.Direction = ParameterDirection.InputOutput
                'objParam.ParameterName = "@numWareHouseItemID"
                'objParam.Value = _WareHouseItemID
                'arParms(2) = objParam

                arParms(3) = New Npgsql.NpgsqlParameter("@vcLocation", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = _WLocation

                arParms(4) = New Npgsql.NpgsqlParameter("@monWListPrice", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(4).Value = _WListPrice

                arParms(5) = New Npgsql.NpgsqlParameter("@numOnHand", NpgsqlTypes.NpgsqlDbType.Double)
                arParms(5).Value = _OnHand

                arParms(6) = New Npgsql.NpgsqlParameter("@numReOrder", NpgsqlTypes.NpgsqlDbType.Double)
                arParms(6).Value = _ReOrder

                arParms(7) = New Npgsql.NpgsqlParameter("@vcWHSKU", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(7).Value = _WSKU

                arParms(8) = New Npgsql.NpgsqlParameter("@vcBarCode", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(8).Value = _WBarCode

                arParms(9) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = DomainID

                arParms(10) = New Npgsql.NpgsqlParameter("@strFieldList", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(10).Value = _strFieldList

                arParms(11) = New Npgsql.NpgsqlParameter("@vcSerialNo", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(11).Value = _SerialNo

                arParms(12) = New Npgsql.NpgsqlParameter("@vcComments", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(12).Value = _Comments

                arParms(13) = New Npgsql.NpgsqlParameter("@numQty", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(13).Value = _Quantity

                arParms(14) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(14).Value = _byteMode

                arParms(15) = New Npgsql.NpgsqlParameter("@numWareHouseItmsDTLID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(15).Value = _WareHouseItmsDTLID

                arParms(16) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(16).Value = UserCntID

                arParms(17) = New Npgsql.NpgsqlParameter("@dtAdjustmentDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(17).Value = DateTime.UtcNow

                arParms(18) = New Npgsql.NpgsqlParameter("@numWLocationID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(18).Value = _WarehouseLocationID

                Dim intResult As Long = 0
                Dim objParam() As Object
                objParam = arParms.ToArray()
                intResult = CLng(SqlDAL.ExecuteNonQuery(connString, "USP_AddUpdateWareHouseForItems", objParam, True))
                _WareHouseItemID = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(2).Value)

                Return intResult
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetItemBarcodeList(ByVal boolAllItems As Boolean, ByVal BarcodeBasedOn As Short) As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@strItemIdList", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _str

                arParms(2) = New Npgsql.NpgsqlParameter("@bitAllItems", NpgsqlTypes.NpgsqlDbType.Boolean)
                arParms(2).Value = boolAllItems

                arParms(3) = New Npgsql.NpgsqlParameter("@tinyBarcodeBasedOn", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = BarcodeBasedOn

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetItemBarcodeList", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetItemFromUPCSKU() As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcUPCSKU", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(1).Value = _SKU

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetItemFromUPCSKU", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ImageItemDetails() As DataTable
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString

            Dim ds As DataSet

            Try
                ' Set up parameters 
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 18)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt, 18)
                arParms(1).Value = _ItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GET_ItemImages", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            Finally

            End Try

        End Function

        Public Function AddUpdateImageItem() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numItemImageId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _numItemImageId

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@vcPathForImage", NpgsqlTypes.NpgsqlDbType.VarChar, 300)
                arParms(3).Value = _PathForImage

                arParms(4) = New Npgsql.NpgsqlParameter("@vcPathForTImage", NpgsqlTypes.NpgsqlDbType.VarChar, 300)
                arParms(4).Value = _PathForTImage

                arParms(5) = New Npgsql.NpgsqlParameter("@bitDefault", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(5).Value = _bitDefault

                arParms(6) = New Npgsql.NpgsqlParameter("@intDisplayOrder", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = _DisplayOrder

                arParms(7) = New Npgsql.NpgsqlParameter("@bitIsImage", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(7).Value = _IsImage

                Dim intResult As Long = 0
                Dim objParam() As Object
                objParam = arParms.ToArray()
                intResult = CLng(SqlDAL.ExecuteNonQuery(connString, "USP_ManageItemImages", objParam, True))
                _numItemImageId = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteItemImages(ByVal strNumImageIds As String) As Boolean
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Try
                ' Set up parameters 
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                ' @ItemCount Input Parameter 
                arParms(0) = New Npgsql.NpgsqlParameter("@numItemImageIds", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(0).Value = strNumImageIds

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomianID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _ItemCode



                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteItemImages", arParms)

                Return True
            Catch ex As Exception
                Return False
                Throw ex
            Finally

            End Try

        End Function

        Public Function GetItemOrderDependencies(Optional ByVal tintOppType As Short = 0, Optional ByVal tintShipped As Short = 0) As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _ClientZoneOffsetTime

                arParms(3) = New Npgsql.NpgsqlParameter("@tintOppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = tintOppType

                arParms(4) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _byteMode

                arParms(5) = New Npgsql.NpgsqlParameter("@numWareHouseId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = WarehouseID

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetItemOrderDependencies", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageItemVendor() As Integer
            Dim intResult As Integer = Nothing
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numVendorTcode", _intVendorTcode, NpgsqlTypes.NpgsqlDbType.BigInt, 18, ParameterDirection.InputOutput))

                    .Add(SqlDAL.Add_Parameter("@numItemKitID", _ItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numVendorID", _VendorID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@vcPartNo", _strPartNo, NpgsqlTypes.NpgsqlDbType.VarChar))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@monCost", _dblMonCost, NpgsqlTypes.NpgsqlDbType.Numeric))

                    .Add(SqlDAL.Add_Parameter("@intMinQty", _intMinQty, NpgsqlTypes.NpgsqlDbType.Integer))

                    .Add(SqlDAL.Add_Parameter("@bitIsPrimaryVendor", _IsPrimaryVendor, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@intMode", _mode, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@vcNotes", vcNotes, NpgsqlTypes.NpgsqlDbType.VarChar))

                End With

                Dim objParam() As Object = sqlParams.ToArray()
                intResult = CCommon.ToInteger(SqlDAL.ExecuteNonQuery(connString, "USP_MANAGE_ITEM_VENDOR", objParam, True))

                If _mode = 0 And intResult > 0 Then
                    _intVendorTcode = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)
                End If

            Catch ex As Exception
                Throw ex
            End Try
            Return intResult
        End Function

        Public Function GetItemVendorForUpdate() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object = sqlParams.ToArray()
                Return SqlDAL.ExecuteDataset(connString, "USP_GET_ITEM_VENDOR_FOR_UPDATE", objParam).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private _intMasterID As Integer
        Public Property MasterID() As Integer
            Get
                Return _intMasterID
            End Get
            Set(ByVal value As Integer)
                _intMasterID = value
            End Set
        End Property

        'USP_GetWareHouseIDByName
        Public Function GetWareHouseIDByName() As DataTable
            Dim dtResult As New DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@vcWareHouseName", _Warehouse, NpgsqlTypes.NpgsqlDbType.VarChar, 100))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetWareHouseIDByName", sqlParams.ToArray())
                If ds IsNot Nothing Then
                    dtResult = ds.Tables(0)
                End If

            Catch ex As Exception
                Throw ex
                dtResult = Nothing
            End Try
            Return dtResult
        End Function

        Public Function GetDataForImport() As DataTable
            Dim dtResult As New DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numMasterID", _intMasterID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetDetailForImport", sqlParams.ToArray())
                If ds IsNot Nothing Then
                    dtResult = ds.Tables(0)
                End If

            Catch ex As Exception
                Throw ex
                dtResult = Nothing
            End Try
            Return dtResult
        End Function

        Public Function GetItemsForInventoryAdjustment() As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(14) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numWarehouseID", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _WarehouseID

                arParms(2) = New Npgsql.NpgsqlParameter("@numItemGroupID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ItemGroupID

                arParms(3) = New Npgsql.NpgsqlParameter("@KeyWord", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(3).Value = _KeyWord

                arParms(4) = New Npgsql.NpgsqlParameter("@numCurrentPage", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = _CurrentPage

                arParms(5) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _PageSize

                arParms(6) = New Npgsql.NpgsqlParameter("@numTotalPage", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(6).Value = _TotalPages

                arParms(7) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(7).Value = _SortCharacter

                arParms(8) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = UserCntID

                arParms(9) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = ItemCode

                arParms(10) = New Npgsql.NpgsqlParameter("@numWarehouseItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(10).Value = WareHouseItemID

                arParms(11) = New Npgsql.NpgsqlParameter("@numWLocationID", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(11).Value = _WarehouseLocationID

                arParms(12) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(12).Value = Nothing
                arParms(12).Direction = ParameterDirection.InputOutput

                arParms(13) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(13).Value = Nothing
                arParms(13).Direction = ParameterDirection.InputOutput

                arParms(14) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(14).Value = Nothing
                arParms(14).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetItemsForInventoryAdjustment", arParms)

                Dim dtTable As DataTable
                If ds.Tables.Count = 3 Then

                    dtTable = ds.Tables(1)

                    If dtTable.Rows.Count > 0 Then
                        _TotalRecords = CInt(dtTable.Rows(0).Item("TotalRowCount"))
                    End If
                    dtTable.Columns.Remove("TotalRowCount")
                    ds.AcceptChanges()
                End If
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function UpdateInventoryAdjustments() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = ItemCode

                arParms(3) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = _str

                arParms(4) = New Npgsql.NpgsqlParameter("@dtAdjustmentDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(4).Value = _dtAdjustmentDate

                arParms(5) = New Npgsql.NpgsqlParameter("@fltReorderQty", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(5).Value = ReorderQty

                arParms(6) = New Npgsql.NpgsqlParameter("@fltReorderPoint", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(6).Value = ReorderPoint

                arParms(7) = New Npgsql.NpgsqlParameter("@bitAutomateReorderPoint", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(7).Value = IsAutomateReorderPoint

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateInventoryAdjustments", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ValidateSerializLot_Name() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@numWarehouseItmsID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _WareHouseItemID

                arParms(2) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.VarChar, 3000)
                arParms(2).Value = _str

                arParms(3) = New Npgsql.NpgsqlParameter("@bitSerialized", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Value = _bitSerialized

                arParms(4) = New Npgsql.NpgsqlParameter("@bitLotNo", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(4).Value = _bitLotNo

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_ValidateSerializLot_Name", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Function MakeItemQtyAdjustmentJournal(lngItemID As Long, strItemName As String, intQtyAdjusted As Double, decAverageCost As Decimal, lngAssetAccountID As Long, lngUserContactID As Long, lngDomainID As Long, Optional boolUseOpeningBalanceEqityAccount As Boolean = True, Optional dtAdjustmentDate As Date = Nothing)
            Try

                Dim decDebitAmtAsset As Decimal
                Dim decCreditAmtAsset As Decimal
                Dim decDebitAmtExpense As Decimal
                Dim decCreditAmtExpense As Decimal

                If intQtyAdjusted < 0 Then
                    decDebitAmtExpense = Math.Abs(intQtyAdjusted * decAverageCost)
                    decCreditAmtExpense = 0
                    decDebitAmtAsset = 0
                    decCreditAmtAsset = Math.Abs(intQtyAdjusted * decAverageCost)
                ElseIf intQtyAdjusted > 0 Then
                    decDebitAmtExpense = 0
                    decCreditAmtExpense = Math.Abs(intQtyAdjusted * decAverageCost)
                    decDebitAmtAsset = Math.Abs(intQtyAdjusted * decAverageCost)
                    decCreditAmtAsset = 0
                End If

                Dim objJEHeader As New JournalEntryHeader
                Dim lngJournalId As Long = 0
                With objJEHeader
                    .JournalId = 0
                    .RecurringId = 0
                    If boolUseOpeningBalanceEqityAccount Then
                        .EntryDate = CDate(Date.UtcNow.ToShortDateString & " 12:00:00")
                    Else
                        .EntryDate = CDate(dtAdjustmentDate.ToShortDateString & " 12:00:00")
                    End If

                    If intQtyAdjusted > 0 Then
                        .Description = "Stock Added: " & Math.Abs(intQtyAdjusted).ToString & " units:" & decAverageCost.ToString()
                    Else
                        .Description = "Stock Removed: " & Math.Abs(intQtyAdjusted).ToString & " units:" & decAverageCost.ToString()
                    End If
                    .Amount = Math.Abs(decAverageCost * intQtyAdjusted)
                    .CheckId = 0
                    .CashCreditCardId = 0
                    .ChartAcntId = 0
                    .OppId = 0
                    .OppBizDocsId = 0
                    .DepositId = 0
                    .BizDocsPaymentDetId = 0
                    .IsOpeningBalance = 0
                    .LastRecurringDate = Date.Now
                    .NoTransactions = 0
                    .CategoryHDRID = 0
                    .ReturnID = 0
                    .CheckHeaderID = 0
                    .BillID = 0
                    .BillPaymentID = 0
                    .UserCntID = lngUserContactID
                    .DomainID = lngDomainID
                End With
                lngJournalId = objJEHeader.Save()

                Dim objItem As New CItems
                Dim objJEList As New JournalEntryCollection
                Dim objJE As New JournalEntryNew()

                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = decDebitAmtExpense
                objJE.CreditAmt = decCreditAmtExpense
                If boolUseOpeningBalanceEqityAccount Then
                    objJE.ChartAcntId = ChartOfAccounting.GetDefaultAccount("OE", lngDomainID) 'Opening Balance Equity
                Else
                    objJE.ChartAcntId = ChartOfAccounting.GetDefaultAccount("IA", lngDomainID) 'Inventory Adjustment account
                End If
                objJE.Description = CCommon.Truncate(strItemName, 50)
                objJE.CustomerId = 0
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                If boolUseOpeningBalanceEqityAccount Then
                    objJE.BizDocItems = "OE1" ' using OE1 for Item Opening balance, OE for COA opening balance
                Else
                    objJE.BizDocItems = "IA1" ' using IA1, since IA is used for CoGS in Auth bizdoc
                End If
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 0
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)


                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = decDebitAmtAsset
                objJE.CreditAmt = decCreditAmtAsset
                objJE.ChartAcntId = lngAssetAccountID 'asset account
                objJE.Description = CCommon.Truncate(strItemName, 50)
                objJE.CustomerId = 0
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                If boolUseOpeningBalanceEqityAccount Then
                    objJE.BizDocItems = "OE1" ' using OE1 for Item Opening balance, OE for COA opening balance
                Else
                    objJE.BizDocItems = "IA1" ' using IA1, since IA is used for CoGS in Auth bizdoc
                End If
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 0
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = lngItemID 'added by chintan to track adjustment
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)

                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, lngDomainID)

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Function MakeAssemblyItemQtyAdjustmentJournal(lngItemID As Long, strItemName As String, intQtyAdjusted As Integer, decAverageCost As Decimal, lngAssetAccountID As Long, lngUserContactID As Long, lngDomainID As Long, numWarehouseItemID As Long)
            Try
                Dim objItems As New CItems
                objItems.ItemCode = lngItemID
                objItems.WareHouseItemID = numWarehouseItemID
                Dim dtChildItems As DataTable = objItems.GetChildItemsForKitsAss
                '3.	Each assembly item will only do calculations upto 1st child level. i.e. A Computer �s (assembly)  maximum possible finished goods count will only be determined by its immediate child like motherboard, RAM ,CPU etc. but not below that level(i.e chips,graphic card, circuit board etc)
                Dim drArray As DataRow() = dtChildItems.Select("numItemKitID = 0 AND ItemType='P'")

                Dim decAmount As Decimal
                For Each dr As DataRow In drArray
                    decAmount += CCommon.ToDecimal(dr("monAverageCostTotalForQtyRequired"))
                Next

                Dim objJEHeader As New JournalEntryHeader
                Dim lngJournalId As Long = 0
                With objJEHeader
                    .JournalId = 0
                    .RecurringId = 0
                    .EntryDate = CDate(Date.UtcNow.ToShortDateString & " 12:00:00")
                    .Description = "Assembly Created: " & Math.Abs(intQtyAdjusted).ToString & " units:" & String.Format("{0:###0.0000}", decAmount)
                    .Amount = decAmount
                    .CheckId = 0
                    .CashCreditCardId = 0
                    .ChartAcntId = 0
                    .OppId = 0
                    .OppBizDocsId = 0
                    .DepositId = 0
                    .BizDocsPaymentDetId = 0
                    .IsOpeningBalance = 0
                    .LastRecurringDate = Date.Now
                    .NoTransactions = 0
                    .CategoryHDRID = 0
                    .ReturnID = 0
                    .CheckHeaderID = 0
                    .BillID = 0
                    .BillPaymentID = 0
                    .UserCntID = lngUserContactID
                    .DomainID = lngDomainID
                End With
                lngJournalId = objJEHeader.Save()




                Dim objItem As New CItems
                Dim objJEList As New JournalEntryCollection
                Dim objJE As New JournalEntryNew()

                For Each dr As DataRow In drArray
                    objJE = New JournalEntryNew()
                    objJE.TransactionId = 0
                    objJE.DebitAmt = 0
                    objJE.CreditAmt = Math.Abs(CCommon.ToDecimal(dr("numConQty")) * CCommon.ToDecimal(dr("monAverageCost")))
                    objJE.ChartAcntId = CCommon.ToLong(dr("numAssetChartAcntId")) 'Inventory Adjustment account
                    objJE.Description = CCommon.Truncate(CCommon.ToString(dr("vcItemName")), 50)
                    objJE.CustomerId = 0
                    objJE.MainDeposit = 0
                    objJE.MainCheck = 0
                    objJE.MainCashCredit = 0
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = "IA1" ' using IA1, since IA is used for CoGS in Auth bizdoc
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = 0
                    objJE.FltExchangeRate = 0
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = CCommon.ToInteger(dr("numItemCode"))
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = 0
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)

                Next

                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = decAmount
                objJE.CreditAmt = 0
                objJE.ChartAcntId = lngAssetAccountID 'asset account
                objJE.Description = CCommon.Truncate(strItemName, 50)
                objJE.CustomerId = 0
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = "IA1" ' using IA1, since IA is used for CoGS in Auth bizdoc
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 0
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = lngItemID 'added by chintan to track adjustment
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)

                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, lngDomainID)

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Function ReverseAssemblyItemQtyAdjustmentJournal(ByVal numAssembledItemID As Long, lngItemID As Long, intQtyAdjusted As Integer, decAverageCost As Decimal, lngUserContactID As Long, lngDomainID As Long)
            Try
                Dim objAssembledItem As New AssembledItem
                objAssembledItem.DomainID = lngDomainID
                objAssembledItem.ID = numAssembledItemID
                Dim ds As DataSet = objAssembledItem.GetByID

                Dim strItemName As String = CCommon.ToString(ds.Tables(0).Rows(0)("vcItemName"))
                Dim lngAssetAccountID As Long = CCommon.ToLong(ds.Tables(0).Rows(0)("numAssetChartAcntId"))

                Dim decAmount As Decimal = CCommon.ToDecimal(decAverageCost * intQtyAdjusted)

                Dim objJEHeader As New JournalEntryHeader
                Dim lngJournalId As Long = 0
                With objJEHeader
                    .JournalId = 0
                    .RecurringId = 0
                    .EntryDate = CDate(Date.UtcNow.ToShortDateString & " 12:00:00")
                    .Description = "Assembly Disassembled: " & Math.Abs(intQtyAdjusted).ToString & " units:" & String.Format("{0:###0.0000}", decAmount)
                    .Amount = decAmount
                    .CheckId = 0
                    .CashCreditCardId = 0
                    .ChartAcntId = 0
                    .OppId = 0
                    .OppBizDocsId = 0
                    .DepositId = 0
                    .BizDocsPaymentDetId = 0
                    .IsOpeningBalance = 0
                    .LastRecurringDate = Date.Now
                    .NoTransactions = 0
                    .CategoryHDRID = 0
                    .ReturnID = 0
                    .CheckHeaderID = 0
                    .BillID = 0
                    .BillPaymentID = 0
                    .UserCntID = lngUserContactID
                    .DomainID = lngDomainID
                End With
                lngJournalId = objJEHeader.Save()




                Dim objItem As New CItems
                Dim objJEList As New JournalEntryCollection
                Dim objJE As New JournalEntryNew()

                For Each dr As DataRow In ds.Tables(1).Rows
                    objJE = New JournalEntryNew()
                    objJE.TransactionId = 0
                    objJE.CreditAmt = 0
                    objJE.DebitAmt = Math.Abs(CCommon.ToDecimal(intQtyAdjusted * dr("fltQtyRequiredForSingleBuild")) * CCommon.ToDecimal(dr("monAverageCost")))
                    objJE.ChartAcntId = CCommon.ToLong(dr("numAssetChartAcntId")) 'Inventory Adjustment account
                    objJE.Description = CCommon.Truncate(CCommon.ToString(dr("vcItemName")), 50)
                    objJE.CustomerId = 0
                    objJE.MainDeposit = 0
                    objJE.MainCheck = 0
                    objJE.MainCashCredit = 0
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = "IA1" ' using IA1, since IA is used for CoGS in Auth bizdoc
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = 0
                    objJE.FltExchangeRate = 0
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = CCommon.ToInteger(dr("numItemCode"))
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = 0
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)

                Next

                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.CreditAmt = decAmount
                objJE.DebitAmt = 0
                objJE.ChartAcntId = lngAssetAccountID 'asset account
                objJE.Description = CCommon.Truncate(strItemName, 50)
                objJE.CustomerId = 0
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = "IA1" ' using IA1, since IA is used for CoGS in Auth bizdoc
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 0
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = lngItemID 'added by chintan to track adjustment
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)

                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, lngDomainID)

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Function MakeAssemblyWorkOrderJournal(lngItemID As Long, strItemName As String, intQtyAdjusted As Integer, decAverageCost As Decimal, lngAssetAccountID As Long, lngUserContactID As Long, lngDomainID As Long, lngWarehouseItemID As Long)
            Try

                Dim objItems As New CItems
                objItems.ItemCode = lngItemID
                objItems.WareHouseItemID = lngWarehouseItemID
                Dim dtChildItems As DataTable = objItems.GetChildItemsForKitsAss

                Dim drArray As DataRow() = dtChildItems.Select("numItemKitID = 0 AND ItemType='P'")


                Dim decAmount As Decimal
                For Each dr As DataRow In drArray
                    decAmount += CCommon.ToDecimal(dr("monAverageCostTotalForQtyRequired"))
                Next


                Dim objJEHeader As New JournalEntryHeader
                Dim lngJournalId As Long = 0
                With objJEHeader
                    .JournalId = 0
                    .RecurringId = 0

                    .EntryDate = CDate(Date.UtcNow.ToShortDateString & " 12:00:00")

                    .Description = "Work Order Created: " & Math.Abs(intQtyAdjusted).ToString & " units:" & String.Format(CCommon.GetDataFormatString(), decAmount)
                    .Amount = decAmount
                    .CheckId = 0
                    .CashCreditCardId = 0
                    .ChartAcntId = 0
                    .OppId = 0
                    .OppBizDocsId = 0
                    .DepositId = 0
                    .BizDocsPaymentDetId = 0
                    .IsOpeningBalance = 0
                    .LastRecurringDate = Date.Now
                    .NoTransactions = 0
                    .CategoryHDRID = 0
                    .ReturnID = 0
                    .CheckHeaderID = 0
                    .BillID = 0
                    .BillPaymentID = 0
                    .UserCntID = lngUserContactID
                    .DomainID = lngDomainID
                End With
                lngJournalId = objJEHeader.Save()




                Dim objItem As New CItems
                Dim objJEList As New JournalEntryCollection
                Dim objJE As New JournalEntryNew()


                For Each dr As DataRow In drArray
                    objJE = New JournalEntryNew()
                    objJE.TransactionId = 0
                    objJE.DebitAmt = 0
                    objJE.CreditAmt = Math.Abs(CCommon.ToDouble(dr("numConQty")) * CCommon.ToDecimal(dr("monAverageCost")))
                    objJE.ChartAcntId = CCommon.ToLong(dr("numAssetChartAcntId")) 'Inventory Adjustment account
                    objJE.Description = CCommon.Truncate(CCommon.ToString(dr("vcItemName")), 50)
                    objJE.CustomerId = 0
                    objJE.MainDeposit = 0
                    objJE.MainCheck = 0
                    objJE.MainCashCredit = 0
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = "IA1" ' using IA1, since IA is used for CoGS in Auth bizdoc
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = 0
                    objJE.FltExchangeRate = 0
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = CCommon.ToInteger(dr("numItemCode"))
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = 0
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)

                Next



                Dim lngWorkInProgressAccountID As Long = ChartOfAccounting.GetDefaultAccount("WP", lngDomainID)

                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = decAmount
                objJE.CreditAmt = 0
                objJE.ChartAcntId = lngWorkInProgressAccountID
                objJE.Description = "Work In Progress A/C"
                objJE.CustomerId = 0
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = "WP" ' Word In Progress
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 0
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = lngItemID 'added by chintan to track adjustment
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)

                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, lngDomainID)

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Function ReverseAssemblyWorkOrderJournal(ByVal numWOID As Long, intQtyAdjusted As Integer, lngUserContactID As Long, lngDomainID As Long)
            Try
                Dim objWorkOrder As New WorkOrder
                objWorkOrder.DomainID = lngDomainID
                objWorkOrder.WorkOrderID = numWOID
                Dim ds As DataSet = objWorkOrder.GetWorkOrderDetailByID()

                Dim lngItemCode As Long = CCommon.ToDecimal(ds.Tables(0).Rows(0)("numItemCode"))
                Dim decAmount As Decimal = CCommon.ToDecimal(ds.Tables(0).Rows(0)("monAverageCost") * intQtyAdjusted)

                Dim objJEHeader As New JournalEntryHeader
                Dim lngJournalId As Long = 0
                With objJEHeader
                    .JournalId = 0
                    .RecurringId = 0

                    .EntryDate = CDate(Date.UtcNow.ToShortDateString & " 12:00:00")

                    .Description = "Work Order Disassembled: " & Math.Abs(intQtyAdjusted).ToString & " units:" & String.Format("{0:###0.0000}", decAmount)
                    .Amount = decAmount
                    .CheckId = 0
                    .CashCreditCardId = 0
                    .ChartAcntId = 0
                    .OppId = 0
                    .OppBizDocsId = 0
                    .DepositId = 0
                    .BizDocsPaymentDetId = 0
                    .IsOpeningBalance = 0
                    .LastRecurringDate = Date.Now
                    .NoTransactions = 0
                    .CategoryHDRID = 0
                    .ReturnID = 0
                    .CheckHeaderID = 0
                    .BillID = 0
                    .BillPaymentID = 0
                    .UserCntID = lngUserContactID
                    .DomainID = lngDomainID
                End With
                lngJournalId = objJEHeader.Save()


                Dim objItem As New CItems
                Dim objJEList As New JournalEntryCollection
                Dim objJE As New JournalEntryNew()

                For Each dr As DataRow In ds.Tables(1).Rows
                    objJE = New JournalEntryNew()
                    objJE.TransactionId = 0
                    objJE.CreditAmt = 0
                    objJE.DebitAmt = Math.Abs(CCommon.ToDecimal(dr("RequiredQty") * intQtyAdjusted) * CCommon.ToDecimal(dr("monAverageCost")))
                    objJE.ChartAcntId = CCommon.ToLong(dr("numAssetChartAcntId")) 'Inventory Adjustment account
                    objJE.Description = CCommon.Truncate(CCommon.ToString(dr("vcItemName")), 50)
                    objJE.CustomerId = 0
                    objJE.MainDeposit = 0
                    objJE.MainCheck = 0
                    objJE.MainCashCredit = 0
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = "IA1" ' using IA1, since IA is used for CoGS in Auth bizdoc
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = 0
                    objJE.FltExchangeRate = 0
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = CCommon.ToInteger(dr("numItemCode"))
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = 0
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)
                Next

                Dim lngWorkInProgressAccountID As Long = ChartOfAccounting.GetDefaultAccount("WP", lngDomainID)

                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.CreditAmt = decAmount
                objJE.DebitAmt = 0
                objJE.ChartAcntId = lngWorkInProgressAccountID
                objJE.Description = "Work In Progress A/C"
                objJE.CustomerId = 0
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = "WP" ' Word In Progress
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 0
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = lngItemCode 'added by chintan to track adjustment
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)

                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, lngDomainID)

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Function GetPriceLevelItemPrice() As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numWareHouseItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _WareHouseItemID

                arParms(3) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _DivisionID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetPriceLevelItemPrice", arParms)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetPriceLevelWithWarehouseItemPrice() As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numWareHouseID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _WarehouseID

                arParms(3) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _DivisionID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetPriceLevelWithWarehouseItemPrice", arParms)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetWarehouseLocation() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numWarehouseID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _WarehouseID

                arParms(2) = New Npgsql.NpgsqlParameter("@numWLocationID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _WarehouseLocationID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetWarehouseLocation", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetWarehouseLocationPaging() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(12) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numWarehouseID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _WarehouseID

                arParms(2) = New Npgsql.NpgsqlParameter("@numWLocationID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _WarehouseLocationID

                arParms(3) = New Npgsql.NpgsqlParameter("@vcSortChar", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = SortByString

                arParms(4) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = CurrentPage

                arParms(5) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = PageSize

                arParms(6) = New Npgsql.NpgsqlParameter("@vcAisle", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(6).Value = vcAsile

                arParms(7) = New Npgsql.NpgsqlParameter("@vcRack", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(7).Value = vcRack

                arParms(8) = New Npgsql.NpgsqlParameter("@vcShelf", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(8).Value = vcShelf

                arParms(9) = New Npgsql.NpgsqlParameter("@vcBin", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(9).Value = vcBin

                arParms(10) = New Npgsql.NpgsqlParameter("@vcLocation", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(10).Value = WLocation

                arParms(11) = New Npgsql.NpgsqlParameter("@intTotalRecordCount", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(11).Value = _TotalRecords
                arParms(11).Direction = ParameterDirection.InputOutput

                arParms(12) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(12).Value = Nothing
                arParms(12).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetWarehouseLocationPaging", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(11).Value)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function UpdateWareHouseLocation() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numWLocationID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _WarehouseLocationID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcAisle", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = vcAsile

                arParms(3) = New Npgsql.NpgsqlParameter("@vcRack", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = vcRack

                arParms(4) = New Npgsql.NpgsqlParameter("@vcShelf", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(4).Value = vcShelf

                arParms(5) = New Npgsql.NpgsqlParameter("@vcBin", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(5).Value = vcBin

                arParms(6) = New Npgsql.NpgsqlParameter("@vcLocation", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(6).Value = WLocation

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateWarehouseLocation", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteWarehouseLocation() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numWLocationID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _WarehouseLocationID


                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteWarehouseLocation", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageWarehouseLocation() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As New DataSet
                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numWarehouseID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _WarehouseID

                arParms(2) = New Npgsql.NpgsqlParameter("@numWLocationID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _WarehouseLocationID

                arParms(3) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(3).Value = _str

                arParms(4) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _byteMode

                Return SqlDAL.ExecuteNonQuery(connString, "USP_ManageWarehouseLocation", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private _strItemIDs As String
        Public Property strItemIDs() As String
            Get
                Return _strItemIDs
            End Get
            Set(ByVal value As String)
                _strItemIDs = value
            End Set
        End Property

        Public Function GetItemOnHandAvgCost() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@strItemIDs", _strItemIDs, NpgsqlTypes.NpgsqlDbType.VarChar))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))

                End With

                ds = SqlDAL.ExecuteDataset(connString, "Usp_GetItemOnHandAveCost", sqlParams.ToArray())

                If ds IsNot Nothing AndAlso ds.Tables.Count > 0 Then
                    Return ds.Tables(0)
                Else
                    Return Nothing
                End If


            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetItemDetails() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Try
                    Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                    arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                    arParms(0).Value = _ItemCode

                    arParms(1) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                    arParms(1).Value = _ClientZoneOffsetTime

                    arParms(2) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                    arParms(2).Value = DivisionID

                    arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                    arParms(3).Value = Nothing
                    arParms(3).Direction = ParameterDirection.InputOutput

                    Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_ItemDetails", arParms)

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        For Each dr As DataRow In ds.Tables(0).Rows
                            If Not IsDBNull(dr("vcItemName")) Then
                                Me.ItemName = CType(dr("vcItemName"), String)
                            End If

                            If Not IsDBNull(dr("txtItemDesc")) Then
                                Me.ItemDesc = CType(dr("txtItemDesc"), String)
                            End If

                            If Not IsDBNull(dr("charItemType")) Then
                                Me.ItemType = CType(dr("charItemType"), String)

                                If Me.ItemType = "Inventory Item" Then
                                    Me.ItemType = "P"
                                ElseIf Me.ItemType = "Service" Then
                                    Me.ItemType = "S"
                                ElseIf Me.ItemType = "Non-Inventory Item" Then
                                    Me.ItemType = "N"
                                End If

                            End If

                            If Not IsDBNull(dr("monListPrice")) Then
                                Me.ListPrice = CType(dr("monListPrice"), Double)
                            End If

                            If Not IsDBNull(dr("numItemClassification")) Then
                                Me.ItemClassification = CType(dr("numItemClassification"), Long)
                            End If

                            If Not IsDBNull(dr("vcModelID")) Then
                                Me.ModelID = CType(dr("vcModelID"), String)
                            End If

                            If Not IsDBNull(dr("bitKitParent")) Then
                                Me.KitParent = CType(dr("bitKitParent"), Boolean)
                            End If

                            If Not IsDBNull(dr("bitSerialized")) Then
                                Me.bitSerialized = CType(dr("bitSerialized"), Boolean)
                            End If

                            If Not IsDBNull(dr("bitLotNo")) Then
                                Me.bitLotNo = CType(dr("bitLotNo"), Boolean)
                            End If

                            If Not IsDBNull(dr("IsArchieve")) Then
                                Me.bitArchieve = CType(dr("IsArchieve"), Boolean)
                            End If

                            If Not IsDBNull(dr("vcManufacturer")) Then
                                Me.Manufacturer = CType(dr("vcManufacturer"), String)
                            End If

                            If Not IsDBNull(dr("numBarCodeId")) Then
                                Me.BarCodeID = CType(dr("numBarCodeId"), String)
                            End If

                            If Not IsDBNull(dr("bitTaxable")) Then
                                Me.Taxable = CType(dr("bitTaxable"), Boolean)
                            End If

                            If Not IsDBNull(dr("vcSKU")) Then
                                Me.SKU = CType(dr("vcSKU"), String)
                            End If

                            If Not IsDBNull(dr("numItemGroup")) Then
                                Me.ItemGroupID = CType(dr("numItemGroup"), Long)
                            End If

                            If Not IsDBNull(dr("monAverageCost")) Then
                                Me.AverageCost = CType(dr("monAverageCost"), Decimal)
                            End If

                            If Not IsDBNull(dr("monCampaignLabourCost")) Then
                                Me.LabourCost = CType(dr("monCampaignLabourCost"), Decimal)
                            End If

                            If Not IsDBNull(dr("numBaseUnit")) Then
                                Me.BaseUnit = CType(dr("numBaseUnit"), Long)
                            End If

                            If Not IsDBNull(dr("numPurchaseUnit")) Then
                                Me.PurchaseUnit = CType(dr("numPurchaseUnit"), Long)
                            End If

                            If Not IsDBNull(dr("numSaleUnit")) Then
                                Me.SaleUnit = CType(dr("numSaleUnit"), Long)
                            End If

                            If Not IsDBNull(dr("numCOGsChartAcntId")) Then
                                Me.COGSChartAcntId = CType(dr("numCOGsChartAcntId"), Long)
                            End If

                            If Not IsDBNull(dr("numAssetChartAcntId")) Then
                                Me.AssetChartAcntId = CType(dr("numAssetChartAcntId"), Long)
                            End If

                            If Not IsDBNull(dr("numIncomeChartAcntId")) Then
                                Me.IncomeChartAcntId = CType(dr("numIncomeChartAcntId"), Long)
                            End If

                            If Not IsDBNull(dr("fltWeight")) Then
                                Me.Weight = CType(dr("fltWeight"), Double)
                            End If

                            If Not IsDBNull(dr("fltHeight")) Then
                                Me.Height = CType(dr("fltHeight"), Double)
                            End If

                            If Not IsDBNull(dr("fltLength")) Then
                                Me.Length = CType(dr("fltLength"), Double)
                            End If

                            If Not IsDBNull(dr("fltWidth")) Then
                                Me.Width = CType(dr("fltWidth"), Double)
                            End If

                            If Not IsDBNull(dr("bitFreeShipping")) Then
                                Me.FreeShipping = CType(dr("bitFreeShipping"), Boolean)
                            End If

                            If Not IsDBNull(dr("bitAllowBackOrder")) Then
                                Me.AllowBackOrder = CType(dr("bitAllowBackOrder"), Boolean)
                            End If

                            If Not IsDBNull(dr("bitShowDeptItem")) Then
                                Me.ShowDeptItem = CType(dr("bitShowDeptItem"), Boolean)
                            End If

                            If Not IsDBNull(dr("bitShowDeptItemDesc")) Then
                                Me.ShowDeptItemDesc = CType(dr("bitShowDeptItemDesc"), Boolean)
                            End If

                            If Not IsDBNull(dr("bitCalAmtBasedonDepItems")) Then
                                Me.CalPriceBasedOnIndItems = CType(dr("bitCalAmtBasedonDepItems"), Boolean)
                            End If

                            If Not IsDBNull(dr("bitAssembly")) Then
                                Me.Assembly = CType(dr("bitAssembly"), Boolean)
                            End If

                            If Not IsDBNull(dr("numItemClass")) Then
                                Me.ItemClass = CType(dr("numItemClass"), Long)
                            End If

                            If Not IsDBNull(dr("tintStandardProductIDType")) Then
                                Me.StandardProductIDType = CType(dr("tintStandardProductIDType"), Short)
                            End If

                            If Not IsDBNull(dr("vcExportToAPI")) Then
                                Me.ListOfAPI = CType(dr("vcExportToAPI"), String)
                            End If

                            If Not IsDBNull(dr("vcItemCategories")) Then
                                Me.ListOfCategories = CType(dr("vcItemCategories"), String)
                            End If

                            If Not IsDBNull(dr("numShipClass")) Then
                                Me.ShippingClass = CType(dr("numShipClass"), Long)
                            End If

                            If Not IsDBNull(dr("bitAllowDropShip")) Then
                                Me.AllowDropShip = CType(dr("bitAllowDropShip"), Boolean)
                            End If

                            'Added by sandeep because following fields are not added yet
                            'start
                            If Not IsDBNull(dr("numOnHand")) Then
                                Me.OnHand = CType(dr("numOnHand"), Double)
                            End If

                            If Not IsDBNull(dr("numOnOrder")) Then
                                Me.OnOrder = CType(dr("numOnOrder"), Double)
                            End If

                            If Not IsDBNull(dr("numReorder")) Then
                                Me.ReOrder = CType(dr("numReorder"), Double)
                            End If

                            If Not IsDBNull(dr("numAllocation")) Then
                                Me.OnAllocation = CType(dr("numAllocation"), Double)
                            End If

                            If Not IsDBNull(dr("numBackOrder")) Then
                                Me.BackOrder = CType(dr("numBackOrder"), Double)
                            End If

                            If Not IsDBNull(dr("numVendorID")) Then
                                Me.VendorID = CType(dr("numVendorID"), Long)
                            End If

                            If Not IsDBNull(dr("monCost")) Then
                                Me.monCost = CType(dr("monCost"), Double)
                            End If

                            If Not IsDBNull(dr("bitMatrix")) Then
                                Me.IsMatrix = CType(dr("bitMatrix"), Double)
                            End If
                        Next
                    End If
                Catch ex As Exception
                    Throw ex
                End Try
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetItemAndWarehouseDetails() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Try
                    Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                    arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                    arParms(0).Value = _ItemCode

                    arParms(1) = New Npgsql.NpgsqlParameter("@numWarehouseItemID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                    arParms(1).Value = _WareHouseItemID

                    arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                    arParms(2).Value = _ClientZoneOffsetTime

                    arParms(3) = New Npgsql.NpgsqlParameter("@vcSelectedKitChildItems", NpgsqlTypes.NpgsqlDbType.VarChar)
                    arParms(3).Value = vcKitChildItems

                    arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                    arParms(4).Value = Nothing
                    arParms(4).Direction = ParameterDirection.InputOutput

                    Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_Item_GetDetails", arParms)

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        For Each dr As DataRow In ds.Tables(0).Rows
                            If Not IsDBNull(dr("vcItemName")) Then
                                Me.ItemName = CType(dr("vcItemName"), String)
                            End If

                            If Not IsDBNull(dr("txtItemDesc")) Then
                                Me.ItemDesc = CType(dr("txtItemDesc"), String)
                            End If

                            If Not IsDBNull(dr("charItemType")) Then
                                Me.ItemType = CType(dr("charItemType"), String)

                                If Me.ItemType = "Inventory Item" Then
                                    Me.ItemType = "P"
                                ElseIf Me.ItemType = "Service" Then
                                    Me.ItemType = "S"
                                ElseIf Me.ItemType = "Non-Inventory Item" Then
                                    Me.ItemType = "N"
                                End If

                            End If

                            If Not IsDBNull(dr("monListPrice")) Then
                                Me.ListPrice = CType(dr("monListPrice"), Double)
                            End If

                            If Not IsDBNull(dr("numItemClassification")) Then
                                Me.ItemClassification = CType(dr("numItemClassification"), Long)
                            End If

                            If Not IsDBNull(dr("vcModelID")) Then
                                Me.ModelID = CType(dr("vcModelID"), String)
                            End If

                            If Not IsDBNull(dr("bitKitParent")) Then
                                Me.KitParent = CType(dr("bitKitParent"), Boolean)
                            End If

                            If Not IsDBNull(dr("bitSerialized")) Then
                                Me.bitSerialized = CType(dr("bitSerialized"), Boolean)
                            End If

                            If Not IsDBNull(dr("bitLotNo")) Then
                                Me.bitLotNo = CType(dr("bitLotNo"), Boolean)
                            End If

                            If Not IsDBNull(dr("IsArchieve")) Then
                                Me.bitArchieve = CType(dr("IsArchieve"), Boolean)
                            End If

                            If Not IsDBNull(dr("vcManufacturer")) Then
                                Me.Manufacturer = CType(dr("vcManufacturer"), String)
                            End If

                            If Not IsDBNull(dr("numBarCodeId")) Then
                                Me.BarCodeID = CType(dr("numBarCodeId"), String)
                            End If

                            If Not IsDBNull(dr("bitTaxable")) Then
                                Me.Taxable = CType(dr("bitTaxable"), Boolean)
                            End If

                            If Not IsDBNull(dr("vcSKU")) Then
                                Me.SKU = CType(dr("vcSKU"), String)
                            End If

                            If Not IsDBNull(dr("numItemGroup")) Then
                                Me.ItemGroupID = CType(dr("numItemGroup"), Long)
                            End If

                            If Not IsDBNull(dr("monAverageCost")) Then
                                Me.AverageCost = CType(dr("monAverageCost"), Decimal)
                            End If

                            If Not IsDBNull(dr("monCampaignLabourCost")) Then
                                Me.LabourCost = CType(dr("monCampaignLabourCost"), Decimal)
                            End If

                            If Not IsDBNull(dr("numBaseUnit")) Then
                                Me.BaseUnit = CType(dr("numBaseUnit"), Long)
                            End If

                            If Not IsDBNull(dr("numPurchaseUnit")) Then
                                Me.PurchaseUnit = CType(dr("numPurchaseUnit"), Long)
                            End If

                            If Not IsDBNull(dr("numSaleUnit")) Then
                                Me.SaleUnit = CType(dr("numSaleUnit"), Long)
                            End If

                            If Not IsDBNull(dr("numCOGsChartAcntId")) Then
                                Me.COGSChartAcntId = CType(dr("numCOGsChartAcntId"), Long)
                            End If

                            If Not IsDBNull(dr("numAssetChartAcntId")) Then
                                Me.AssetChartAcntId = CType(dr("numAssetChartAcntId"), Long)
                            End If

                            If Not IsDBNull(dr("numIncomeChartAcntId")) Then
                                Me.IncomeChartAcntId = CType(dr("numIncomeChartAcntId"), Long)
                            End If

                            If Not IsDBNull(dr("fltWeight")) Then
                                Me.Weight = CType(dr("fltWeight"), Double)
                            End If

                            If Not IsDBNull(dr("fltHeight")) Then
                                Me.Height = CType(dr("fltHeight"), Double)
                            End If

                            If Not IsDBNull(dr("fltLength")) Then
                                Me.Length = CType(dr("fltLength"), Double)
                            End If

                            If Not IsDBNull(dr("fltWidth")) Then
                                Me.Width = CType(dr("fltWidth"), Double)
                            End If

                            If Not IsDBNull(dr("bitFreeShipping")) Then
                                Me.FreeShipping = CType(dr("bitFreeShipping"), Boolean)
                            End If

                            If Not IsDBNull(dr("bitAllowBackOrder")) Then
                                Me.AllowBackOrder = CType(dr("bitAllowBackOrder"), Boolean)
                            End If

                            If Not IsDBNull(dr("bitShowDeptItem")) Then
                                Me.ShowDeptItem = CType(dr("bitShowDeptItem"), Boolean)
                            End If

                            If Not IsDBNull(dr("bitShowDeptItemDesc")) Then
                                Me.ShowDeptItemDesc = CType(dr("bitShowDeptItemDesc"), Boolean)
                            End If

                            If Not IsDBNull(dr("bitCalAmtBasedonDepItems")) Then
                                Me.CalPriceBasedOnIndItems = CType(dr("bitCalAmtBasedonDepItems"), Boolean)
                            End If

                            If Not IsDBNull(dr("bitAssembly")) Then
                                Me.Assembly = CType(dr("bitAssembly"), Boolean)
                            End If

                            If Not IsDBNull(dr("numItemClass")) Then
                                Me.ItemClass = CType(dr("numItemClass"), Long)
                            End If

                            If Not IsDBNull(dr("tintStandardProductIDType")) Then
                                Me.StandardProductIDType = CType(dr("tintStandardProductIDType"), Short)
                            End If

                            If Not IsDBNull(dr("vcExportToAPI")) Then
                                Me.ListOfAPI = CType(dr("vcExportToAPI"), String)
                            End If

                            If Not IsDBNull(dr("vcItemCategories")) Then
                                Me.ListOfCategories = CType(dr("vcItemCategories"), String)
                            End If

                            If Not IsDBNull(dr("numShipClass")) Then
                                Me.ShippingClass = CType(dr("numShipClass"), Long)
                            End If

                            If Not IsDBNull(dr("bitAllowDropShip")) Then
                                Me.AllowDropShip = CType(dr("bitAllowDropShip"), Boolean)
                            End If

                            'Added by sandeep because following fields are not added yet
                            'start
                            If Not IsDBNull(dr("numOnHand")) Then
                                Me.OnHand = CType(dr("numOnHand"), Double)
                            End If

                            If Not IsDBNull(dr("numOnOrder")) Then
                                Me.OnOrder = CType(dr("numOnOrder"), Double)
                            End If

                            If Not IsDBNull(dr("numReorder")) Then
                                Me.ReOrder = CType(dr("numReorder"), Double)
                            End If

                            If Not IsDBNull(dr("numAllocation")) Then
                                Me.OnAllocation = CType(dr("numAllocation"), Double)
                            End If

                            If Not IsDBNull(dr("numBackOrder")) Then
                                Me.BackOrder = CType(dr("numBackOrder"), Double)
                            End If

                            If Not IsDBNull(dr("numVendorID")) Then
                                Me.VendorID = CType(dr("numVendorID"), Long)
                            End If

                            If Not IsDBNull(dr("numWOQty")) Then
                                Me.numMaxWOQty = CType(dr("numWOQty"), Integer)
                            End If
                            If Not IsDBNull(dr("numContainer")) Then
                                Me.numContainer = CType(dr("numContainer"), Integer)
                            End If
                            If Not IsDBNull(dr("numNoItemIntoContainer")) Then
                                Me.numNoItemIntoContainer = CType(dr("numNoItemIntoContainer"), Integer)
                            End If
                            If Not IsDBNull(dr("numWareHouseID")) Then
                                Me.WarehouseID = CType(dr("numWareHouseID"), Integer)
                            End If
                            If Not IsDBNull(dr("monVendorCost")) Then
                                Me.monCost = CType(dr("monVendorCost"), Double)
                            End If
                            If Not IsDBNull(dr("vcWareHouse")) Then
                                Me.Warehouse = CType(dr("vcWareHouse"), String)
                            End If
                        Next
                    End If
                Catch ex As Exception
                    Throw ex
                Finally

                End Try

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetWarehouseItemDetails() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Try
                    Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                    arParms(0) = New Npgsql.NpgsqlParameter("@numWareHouseItemID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                    arParms(0).Value = _WareHouseItemID

                    arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                    arParms(1).Value = DomainID

                    arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                    arParms(2).Value = Nothing
                    arParms(2).Direction = ParameterDirection.InputOutput

                    Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_GetWarehouseItemDetails", arParms)

                    If ds Is Nothing Or ds.Tables.Count = 0 Or ds.Tables(0).Rows.Count = 0 Then
                        Me.WareHouseItemID = 0
                    Else
                        For Each dr As DataRow In ds.Tables(0).Rows
                            If Not IsDBNull(dr("numItemID")) Then
                                Me.ItemCode = CType(dr("numItemID"), Long)
                            End If

                            If Not IsDBNull(dr("numWareHouseItemID")) Then
                                Me.WareHouseItemID = CType(dr("numWareHouseItemID"), Long)
                            End If

                            If Not IsDBNull(dr("numWareHouseID")) Then
                                Me.WarehouseID = CType(dr("numWareHouseID"), Long)
                            End If

                            If Not IsDBNull(dr("numWLocationID")) Then
                                Me.WarehouseLocationID = CType(dr("numWLocationID"), Long)
                            End If

                            If Not IsDBNull(dr("monWListPrice")) Then
                                Me.WListPrice = CType(dr("monWListPrice"), Double)
                            End If

                            If Not IsDBNull(dr("numOnHand")) Then
                                Me.OnHand = CType(dr("numOnHand"), Double)
                            End If

                            If Not IsDBNull(dr("numReorder")) Then
                                Me.ReOrder = CType(dr("numReorder"), Double)
                            End If

                            If Not IsDBNull(dr("vcWHSKU")) Then
                                Me.WSKU = CType(dr("vcWHSKU"), String)
                            End If

                            If Not IsDBNull(dr("vcBarCode")) Then
                                Me.WBarCode = CType(dr("vcBarCode"), String)
                            End If
                        Next
                    End If



                Catch ex As Exception
                    Throw ex
                Finally

                End Try

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetItemImageDetails() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Try
                    Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                    arParms(0) = New Npgsql.NpgsqlParameter("@numItemImageId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                    arParms(0).Value = _numItemImageId

                    arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                    arParms(1).Value = DomainID

                    arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                    arParms(2).Value = Nothing
                    arParms(2).Direction = ParameterDirection.InputOutput

                    Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_GetItemImageDetails", arParms)

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        For Each dr As DataRow In ds.Tables(0).Rows
                            If Not IsDBNull(dr("numItemImageId")) Then
                                Me.numItemImageId = CType(dr("numItemImageId"), Long)
                            End If

                            If Not IsDBNull(dr("numItemCode")) Then
                                Me.ItemCode = CType(dr("numItemCode"), Long)
                            End If

                            If Not IsDBNull(dr("vcPathForImage")) Then
                                Me.PathForImage = CType(dr("vcPathForImage"), String)
                            End If

                            If Not IsDBNull(dr("vcPathForTImage")) Then
                                Me.PathForTImage = CType(dr("vcPathForTImage"), String)
                            End If

                            If Not IsDBNull(dr("bitDefault")) Then
                                Me.bitDefault = CType(dr("bitDefault"), Boolean)
                            End If

                            If Not IsDBNull(dr("intDisplayOrder")) Then
                                Me.DisplayOrder = CType(dr("intDisplayOrder"), Integer)
                            End If

                            If Not IsDBNull(dr("bitIsImage")) Then
                                Me.IsImage = CType(dr("bitIsImage"), Boolean)
                            End If
                        Next
                    Else
                        Me.numItemImageId = 0
                    End If
                Catch ex As Exception
                    Throw ex
                Finally

                End Try

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetItemVendorDetails() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Try
                    Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                    arParms(0) = New Npgsql.NpgsqlParameter("@numVendorID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                    arParms(0).Value = _VendorID

                    arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                    arParms(1).Value = DomainID

                    arParms(2) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                    arParms(2).Value = ItemCode

                    arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                    arParms(3).Value = Nothing
                    arParms(3).Direction = ParameterDirection.InputOutput

                    Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_GetItemVendorDetails", arParms)

                    If ds Is Nothing Or ds.Tables.Count = 0 Or ds.Tables(0).Rows.Count = 0 Then
                        Me.VendorTcode = 0
                    Else
                        For Each dr As DataRow In ds.Tables(0).Rows
                            If Not IsDBNull(dr("numVendorTcode")) Then
                                Me.VendorTcode = CType(dr("numVendorTcode"), Long)
                            End If

                            If Not IsDBNull(dr("numVendorID")) Then
                                Me.VendorID = CType(dr("numVendorID"), Long)
                            End If

                            If Not IsDBNull(dr("monCost")) Then
                                Me.monCost = CType(dr("monCost"), Double)
                            End If

                            If Not IsDBNull(dr("vcPartNo")) Then
                                Me.PartNo = CType(dr("vcPartNo"), String)
                            End If

                            If Not IsDBNull(dr("numItemCode")) Then
                                Me.ItemCode = CType(dr("numItemCode"), Long)
                            End If

                            If Not IsDBNull(dr("intMinQty")) Then
                                Me.minQty = CType(dr("intMinQty"), Long)
                            End If
                        Next
                    End If
                Catch ex As Exception
                    Throw ex
                Finally

                End Try

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetKitSubItemDetails() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Try
                    Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                    arParms(0) = New Npgsql.NpgsqlParameter("@numVendorID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                    arParms(0).Value = _VendorID

                    arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                    arParms(1).Value = DomainID

                    arParms(2) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.Bigint)
                    arParms(2).Value = ItemCode

                    arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                    arParms(3).Value = Nothing
                    arParms(3).Direction = ParameterDirection.InputOutput

                    Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_GetItemVendorDetails", arParms)

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        For Each dr As DataRow In ds.Tables(0).Rows
                            If Not IsDBNull(dr("numItemDetailID")) Then
                                Me.ItemDetailID = CType(dr("numItemDetailID"), Long)
                            End If

                            If Not IsDBNull(dr("numItemKitID")) Then
                                Me.ItemCode = CType(dr("numItemKitID"), Long)
                            End If

                            If Not IsDBNull(dr("numChildItemID")) Then
                                Me.ChildItemID = CType(dr("numChildItemID"), Long)
                            End If

                            If Not IsDBNull(dr("numQtyItemsReq")) Then
                                Me.Quantity = CType(dr("numQtyItemsReq"), Long)
                            End If

                            If Not IsDBNull(dr("numWareHouseItemId")) Then
                                Me.WareHouseItemID = CType(dr("numWareHouseItemId"), Long)
                            End If

                            If Not IsDBNull(dr("numUOMId")) Then
                                Me.BaseUnit = CType(dr("numUOMId"), Long)
                            End If

                            If Not IsDBNull(dr("vcItemDesc")) Then
                                Me.ItemDesc = CType(dr("vcItemDesc"), String)
                            End If

                            If Not IsDBNull(dr("sintOrder")) Then
                                Me.intOrder = CType(dr("sintOrder"), Boolean)
                            End If
                        Next
                    End If
                Catch ex As Exception
                    Throw ex
                Finally

                End Try

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function CreateCloneItem() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numItemCode", _ItemCode, NpgsqlTypes.NpgsqlDbType.BigInt, 18, ParameterDirection.InputOutput))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                End With

                Dim objParam() As Object = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "Usp_CreateCloneItem", objParam, True)
                _ItemCode = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

                Return _ItemCode
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetItemBarcodeDisplayFields(ByVal boolAllItems As Boolean) As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@strItemIdList", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _str

                arParms(2) = New Npgsql.NpgsqlParameter("@bitAllItems", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(2).Value = boolAllItems

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_ItemDisplayFieldsForBarcode", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private _FormID As Long
        Public Property FormID() As Long
            Get
                Return _FormID
            End Get
            Set(ByVal value As Long)
                _FormID = value
            End Set
        End Property

        Public Function GetCartFilters() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numFormID", NpgsqlTypes.NpgsqlDbType.BigInt, 18)
                arParms(0).Value = _FormID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt, 18)
                arParms(1).Value = _SiteID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 18)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetCartItemFilters", arParms)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetWareHouseItemsBySKU() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@numWarehouseID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _WarehouseID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcSKU", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(2).Value = _WSKU

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = DomainID

                arParms(4) = New Npgsql.NpgsqlParameter("@numWareHouseItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _WareHouseItemID

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetWarehouseItemsBySKU", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Start - Added by sandeep for BizAPI item search
        Public Function GetItemsBizAPI(ByRef totalRecords As Integer) As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numPageIndex", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = CurrentPage

                arParms(2) = New Npgsql.NpgsqlParameter("@numPageSize", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = PageSize

                arParms(3) = New Npgsql.NpgsqlParameter("@TotalRecords", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Direction = ParameterDirection.Output

                arParms(4) = New Npgsql.NpgsqlParameter("@dtCreatedAfter", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(4).Value = CreatedAfter

                arParms(5) = New Npgsql.NpgsqlParameter("@dtModifiedAfter", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(5).Value = ModifiedAfter

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_Item_GetAll_BIZAPI", objParam, True)
                totalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(3).Value)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SearchItemsBizAPI(ByRef totalRecords As Integer) As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(9) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SearchText", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(1).Value = SearchText

                arParms(2) = New Npgsql.NpgsqlParameter("@numPageIndex", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = CurrentPage

                arParms(3) = New Npgsql.NpgsqlParameter("@numPageSize", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = PageSize

                arParms(4) = New Npgsql.NpgsqlParameter("@TotalRecords", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Direction = ParameterDirection.Output
                arParms(4).Value = 0

                arParms(5) = New Npgsql.NpgsqlParameter("@dtCreatedAfter", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(5).Value = CreatedAfter

                arParms(6) = New Npgsql.NpgsqlParameter("@dtModifiedAfter", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(6).Value = ModifiedAfter

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                arParms(9) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(9).Value = Nothing
                arParms(9).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_Item_Search_BIZAPI", objParam, True)
                totalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(4).Value)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ValidateItemDomain() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numItemCode", ItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_ValidateItemDomain", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function
        'End
        'Author : Sachin Sadhu ||Dated:08/09/2014
        'Description:Bind Assets/Rentals in Organization 
        Public Function GetOrganizationAssets() As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(11) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _CurrentPage

                arParms(3) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Direction = ParameterDirection.InputOutput
                arParms(3).Value = _TotalRecords

                arParms(4) = New Npgsql.NpgsqlParameter("@numItemClassification", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _ItemClassification

                If _KeyWord Is Nothing Then _KeyWord = ""
                arParms(5) = New Npgsql.NpgsqlParameter("@vcKeyWord", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(5).Value = _KeyWord

                If _columnName Is Nothing Then _columnName = ""
                arParms(6) = New Npgsql.NpgsqlParameter("@vcSortColumn", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(6).Value = _columnName

                If _columnSortOrder Is Nothing Then _columnSortOrder = ""
                arParms(7) = New Npgsql.NpgsqlParameter("@vcSort", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(7).Value = _columnSortOrder

                arParms(8) = New Npgsql.NpgsqlParameter("@vcSortChar", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(8).Value = _SortCharacter

                arParms(9) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(9).Value = _PageSize

                arParms(10) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(10).Value = Nothing
                arParms(10).Direction = ParameterDirection.InputOutput

                arParms(11) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(11).Value = Nothing
                arParms(11).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "usp_GetOrganizationAssets", objParam, True)
                TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(3).Value)

                Return ds

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        'Added By :Sachin Sadhu||Date:10thSept2014
        'Purpose  : Update Rental Warehouse Inventory
        Public Function UpdateInventoryRentalAsset(ByVal Mode As Integer) As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@itemcode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@onAllocation", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(2).Value = _OnAllocation

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomain", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = DomainID

                arParms(4) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = OppId


                arParms(5) = New Npgsql.NpgsqlParameter("@Mode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = Mode

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateInvenotry_RentalAsset", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'end of code

        Public Function GetItemChildMembershipDetails() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = ItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_Item_GetChildMembership", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ValidateNewSerialLotNo() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numWarehouseID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = WarehouseID

                arParms(2) = New Npgsql.NpgsqlParameter("@numWarehouseLocationID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = WareHouseItemID

                arParms(3) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = ItemCode

                arParms(4) = New Npgsql.NpgsqlParameter("@bitSerial", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(4).Value = bitSerialized

                arParms(5) = New Npgsql.NpgsqlParameter("@vcSerialLot", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(5).Value = SerialNo

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                Return CCommon.ToBool(SqlDAL.ExecuteScalar(connString, "USP_WareHouseItmsDTL_CheckForDuplicate", arParms))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function IsDuplicateLocationAttributes() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numWarehouseID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = WarehouseID

                arParms(2) = New Npgsql.NpgsqlParameter("@numWLocationID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = WarehouseLocationID

                arParms(3) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = ItemCode

                arParms(4) = New Npgsql.NpgsqlParameter("@strFieldList", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(4).Value = strFieldList

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Return Convert.ToBoolean(SqlDAL.ExecuteScalar(connString, "USP_WareHouseItems_CheckForDuplicateAttr", arParms))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function IsDuplicateWarehouseLocation() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numWarehouseID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = WarehouseID

                arParms(2) = New Npgsql.NpgsqlParameter("@numWLocationID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = WarehouseLocationID

                arParms(3) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = ItemCode

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return Convert.ToBoolean(SqlDAL.ExecuteScalar(connString, "USP_WareHouseItems_ValidateLocation", arParms))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function SaveWarehouseItems() As Long
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(14) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numWareHouseItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Direction = ParameterDirection.InputOutput
                arParms(2).Value = _WareHouseItemID

                arParms(3) = New Npgsql.NpgsqlParameter("@numWareHouseID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _WarehouseID

                arParms(4) = New Npgsql.NpgsqlParameter("@numWLocationID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _WarehouseLocationID

                arParms(5) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _ItemCode

                arParms(6) = New Npgsql.NpgsqlParameter("@vcLocation", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(6).Value = WLocation

                arParms(7) = New Npgsql.NpgsqlParameter("@monWListPrice", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(7).Value = WListPrice

                arParms(8) = New Npgsql.NpgsqlParameter("@numOnHand", NpgsqlTypes.NpgsqlDbType.Double)
                arParms(8).Value = OnHand

                arParms(9) = New Npgsql.NpgsqlParameter("@numReorder", NpgsqlTypes.NpgsqlDbType.Double)
                arParms(9).Value = ReOrder

                arParms(10) = New Npgsql.NpgsqlParameter("@vcWHSKU", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(10).Value = WSKU

                arParms(11) = New Npgsql.NpgsqlParameter("@vcBarCode", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(11).Value = WBarCode

                arParms(12) = New Npgsql.NpgsqlParameter("@strFieldList", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(12).Value = strFieldList

                arParms(13) = New Npgsql.NpgsqlParameter("@vcSerialLotNo", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(13).Value = SerialNo

                arParms(14) = New Npgsql.NpgsqlParameter("@bitCreateGlobalLocation", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(14).Value = IsCreateGlobalLocation

                Dim intResult As Long
                Dim objParam() As Object
                objParam = arParms.ToArray()
                intResult = CLng(SqlDAL.ExecuteNonQuery(connString, "USP_WarehouseItems_Save", objParam, True))
                _WareHouseItemID = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(2).Value)

                Return intResult
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetChildKitsOfKit() As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = ItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_ItemDetails_GetChildKitsOfKit", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetKitChildItems() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numKitItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = ItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = ChildItemID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_ItemDetails_GetKitChildItems", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub DisassembleItem()
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = ItemCode

                arParms(3) = New Npgsql.NpgsqlParameter("@numWarehouseItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = WareHouseItemID

                arParms(4) = New Npgsql.NpgsqlParameter("@numQty", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = NoofUnits

                SqlDAL.ExecuteNonQuery(connString, "USP_Item_Disassemble", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub InventoryTransfer()
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numFromWarehouseItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = WareHouseItemID

                arParms(3) = New Npgsql.NpgsqlParameter("@numToWarehouseItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = StockTransferToWarehouseItemID

                arParms(4) = New Npgsql.NpgsqlParameter("@numQty", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = NoofUnits

                arParms(5) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(5).Value = SerialNo

                SqlDAL.ExecuteNonQuery(connString, "USP_WarehouseItems_InventoryTransfer", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub
        Public Sub MassStockTransfer()
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = strItemCodes

                SqlDAL.ExecuteNonQuery(connString, "USP_Item_MassStockTransfer", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub


        Public Function GetMassStockTransferHistory() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = ClientTimeZoneOffset

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_MassStockTransfer_GetLast20Transfer", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function GetItemWarehouseLocations() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numWareHouseID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _WarehouseID

                arParms(2) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ItemCode

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_WarehouseItems_GetByItemAndExternalLocation", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetItemVendors() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ClientZoneOffsetTime

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_Item_GetVendors", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetItemVendorPriceHistory() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@numVendorID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _VendorID

                arParms(3) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _ClientZoneOffsetTime

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_Item_GetVendorPriceHistory", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetWillCallWarehouseDetail() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numWarehouseID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = WarehouseID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_Warehouses_GetWillCallWarehouse", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function


        Public Function GetItemAttributes() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = ItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@numItemGroup", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = ItemGroupID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_Item_GetAttributes", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetMatrixItemFromAttributes() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = ItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@vcAttributeIDs", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = AttributeIDs

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_Item_GetFromAttributeIds", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub ImportItemsPriceLevel()
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numItemCode", ItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintRuleType", PriceRuleType, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@tintDiscountType", PriceDiscountType, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@monPriceLevel1", PriceLevel1, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@monPriceLevel2", PriceLevel2, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@monPriceLevel3", PriceLevel3, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@monPriceLevel4", PriceLevel4, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@monPriceLevel5", PriceLevel5, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@monPriceLevel6", PriceLevel6, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@monPriceLevel7", PriceLevel7, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@monPriceLevel8", PriceLevel8, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@monPriceLevel9", PriceLevel9, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@monPriceLevel10", PriceLevel10, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@monPriceLevel11", PriceLevel11, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@monPriceLevel12", PriceLevel12, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@monPriceLevel13", PriceLevel13, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@monPriceLevel14", PriceLevel14, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@monPriceLevel15", PriceLevel15, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@monPriceLevel16", PriceLevel16, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@monPriceLevel17", PriceLevel17, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@monPriceLevel18", PriceLevel18, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@monPriceLevel19", PriceLevel19, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@monPriceLevel20", PriceLevel20, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel1FromQty", PriceLevel1QtyFrom, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel1ToQty", PriceLevel1QtyTo, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel2FromQty", PriceLevel2QtyFrom, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel2ToQty", PriceLevel2QtyTo, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel3FromQty", PriceLevel3QtyFrom, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel3ToQty", PriceLevel3QtyTo, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel4FromQty", PriceLevel4QtyFrom, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel4ToQty", PriceLevel4QtyTo, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel5FromQty", PriceLevel5QtyTo, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel5ToQty", PriceLevel5QtyTo, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel6FromQty", PriceLevel6QtyFrom, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel6ToQty", PriceLevel6QtyTo, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel7FromQty", PriceLevel7QtyFrom, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel7ToQty", PriceLevel7QtyTo, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel8FromQty", PriceLevel8QtyFrom, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel8ToQty", PriceLevel8QtyTo, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel9FromQty", PriceLevel9QtyFrom, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel9ToQty", PriceLevel9QtyTo, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel10FromQty", PriceLevel10QtyFrom, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel10ToQty", PriceLevel10QtyTo, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel11FromQty", PriceLevel11QtyFrom, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel11ToQty", PriceLevel11QtyTo, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel12FromQty", PriceLevel12QtyFrom, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel12ToQty", PriceLevel12QtyTo, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel13FromQty", PriceLevel13QtyFrom, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel13ToQty", PriceLevel13QtyTo, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel14FromQty", PriceLevel14QtyFrom, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel14ToQty", PriceLevel14QtyTo, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel15FromQty", PriceLevel15QtyFrom, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel15ToQty", PriceLevel15QtyTo, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel16FromQty", PriceLevel16QtyFrom, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel16ToQty", PriceLevel16QtyTo, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel17FromQty", PriceLevel17QtyFrom, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel17ToQty", PriceLevel17QtyTo, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel18FromQty", PriceLevel18QtyFrom, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel18ToQty", PriceLevel18QtyTo, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel19FromQty", PriceLevel19QtyFrom, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel19ToQty", PriceLevel19QtyTo, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel20FromQty", PriceLevel20QtyFrom, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intPriceLevel20ToQty", PriceLevel20QtyTo, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@vcPriceLevel1Name", PriceLevel1Name, NpgsqlTypes.NpgsqlDbType.VarChar, 300))
                    .Add(SqlDAL.Add_Parameter("@vcPriceLevel2Name", PriceLevel2Name, NpgsqlTypes.NpgsqlDbType.VarChar, 300))
                    .Add(SqlDAL.Add_Parameter("@vcPriceLevel3Name", PriceLevel3Name, NpgsqlTypes.NpgsqlDbType.VarChar, 300))
                    .Add(SqlDAL.Add_Parameter("@vcPriceLevel4Name", PriceLevel4Name, NpgsqlTypes.NpgsqlDbType.VarChar, 300))
                    .Add(SqlDAL.Add_Parameter("@vcPriceLevel5Name", PriceLevel5Name, NpgsqlTypes.NpgsqlDbType.VarChar, 300))
                    .Add(SqlDAL.Add_Parameter("@vcPriceLevel6Name", PriceLevel6Name, NpgsqlTypes.NpgsqlDbType.VarChar, 300))
                    .Add(SqlDAL.Add_Parameter("@vcPriceLevel7Name", PriceLevel7Name, NpgsqlTypes.NpgsqlDbType.VarChar, 300))
                    .Add(SqlDAL.Add_Parameter("@vcPriceLevel8Name", PriceLevel8Name, NpgsqlTypes.NpgsqlDbType.VarChar, 300))
                    .Add(SqlDAL.Add_Parameter("@vcPriceLevel9Name", PriceLevel9Name, NpgsqlTypes.NpgsqlDbType.VarChar, 300))
                    .Add(SqlDAL.Add_Parameter("@vcPriceLevel10Name", PriceLevel10Name, NpgsqlTypes.NpgsqlDbType.VarChar, 300))
                    .Add(SqlDAL.Add_Parameter("@vcPriceLevel11Name", PriceLevel11Name, NpgsqlTypes.NpgsqlDbType.VarChar, 300))
                    .Add(SqlDAL.Add_Parameter("@vcPriceLevel12Name", PriceLevel12Name, NpgsqlTypes.NpgsqlDbType.VarChar, 300))
                    .Add(SqlDAL.Add_Parameter("@vcPriceLevel13Name", PriceLevel13Name, NpgsqlTypes.NpgsqlDbType.VarChar, 300))
                    .Add(SqlDAL.Add_Parameter("@vcPriceLevel14Name", PriceLevel14Name, NpgsqlTypes.NpgsqlDbType.VarChar, 300))
                    .Add(SqlDAL.Add_Parameter("@vcPriceLevel15Name", PriceLevel15Name, NpgsqlTypes.NpgsqlDbType.VarChar, 300))
                    .Add(SqlDAL.Add_Parameter("@vcPriceLevel16Name", PriceLevel16Name, NpgsqlTypes.NpgsqlDbType.VarChar, 300))
                    .Add(SqlDAL.Add_Parameter("@vcPriceLevel17Name", PriceLevel17Name, NpgsqlTypes.NpgsqlDbType.VarChar, 300))
                    .Add(SqlDAL.Add_Parameter("@vcPriceLevel18Name", PriceLevel18Name, NpgsqlTypes.NpgsqlDbType.VarChar, 300))
                    .Add(SqlDAL.Add_Parameter("@vcPriceLevel19Name", PriceLevel19Name, NpgsqlTypes.NpgsqlDbType.VarChar, 300))
                    .Add(SqlDAL.Add_Parameter("@vcPriceLevel20Name", PriceLevel20Name, NpgsqlTypes.NpgsqlDbType.VarChar, 300))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_Item_ImportPriceLevel", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetItemWarehouseBasedOnAddress() As DataTable
            Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString

            With sqlParams
                .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                .Add(SqlDAL.Add_Parameter("@numItemCode", ItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                .Add(SqlDAL.Add_Parameter("@numAddressID", ShipAddressId, NpgsqlTypes.NpgsqlDbType.BigInt))
                .Add(SqlDAL.Add_Parameter("@numWarehouseItemID", WareHouseItemID, NpgsqlTypes.NpgsqlDbType.Bigint))
                .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
            End With

            Return SqlDAL.ExecuteDatable(connString, "USP_Item_GetWarehouseBasedonAddress", sqlParams.ToArray())
        End Function

        Public Sub GetItemDetailForImport()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = ItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_Import_GetItemDetails", arParms)

                If ds Is Nothing Or ds.Tables.Count = 0 Or ds.Tables(0).Rows.Count = 0 Then
                    Me.ItemCode = 0
                Else
                    For Each dr As DataRow In ds.Tables(0).Rows
                        If Not IsDBNull(dr("numShipClass")) Then
                            Me.ShippingClass = CType(dr("numShipClass"), Long)
                        End If

                        If Not IsDBNull(dr("vcItemName")) Then
                            Me.ItemName = CType(dr("vcItemName"), String)
                        End If

                        If Not IsDBNull(dr("monListPrice")) Then
                            Me.ListPrice = CType(dr("monListPrice"), Double)
                        End If

                        If Not IsDBNull(dr("txtItemDesc")) Then
                            Me.ItemDesc = CType(dr("txtItemDesc"), String)
                        End If

                        If Not IsDBNull(dr("vcModelID")) Then
                            Me.ModelID = CType(dr("vcModelID"), String)
                        End If

                        If Not IsDBNull(dr("vcManufacturer")) Then
                            Me.Manufacturer = CType(dr("vcManufacturer"), String)
                        End If

                        If Not IsDBNull(dr("numBarCodeId")) Then
                            Me.BarCodeID = CType(dr("numBarCodeId"), String)
                        End If

                        If Not IsDBNull(dr("fltWidth")) Then
                            Me.Width = CType(dr("fltWidth"), Double)
                        End If

                        If Not IsDBNull(dr("fltHeight")) Then
                            Me.Height = CType(dr("fltHeight"), Double)
                        End If

                        If Not IsDBNull(dr("fltWeight")) Then
                            Me.Weight = CType(dr("fltWeight"), Double)
                        End If

                        If Not IsDBNull(dr("fltLength")) Then
                            Me.Length = CType(dr("fltLength"), Double)
                        End If

                        If Not IsDBNull(dr("monAverageCost")) Then
                            Me.AverageCost = CType(dr("monAverageCost"), Decimal)
                        End If

                        If Not IsDBNull(dr("numItemCode")) Then
                            Me.ItemCode = CType(dr("numItemCode"), Long)
                        End If

                        If Not IsDBNull(dr("bitSerialized")) Then
                            Me.bitSerialized = CType(dr("bitSerialized"), Boolean)
                        End If

                        If Not IsDBNull(dr("bitKitParent")) Then
                            Me.KitParent = CType(dr("bitKitParent"), Boolean)
                        End If

                        If Not IsDBNull(dr("bitAssembly")) Then
                            Me.Assembly = CType(dr("bitAssembly"), Boolean)
                        End If

                        If Not IsDBNull(dr("IsArchieve")) Then
                            Me.bitArchieve = CType(dr("IsArchieve"), Boolean)
                        End If

                        If Not IsDBNull(dr("bitLotNo")) Then
                            Me.bitLotNo = CType(dr("bitLotNo"), Boolean)
                        End If

                        If Not IsDBNull(dr("bitAllowBackOrder")) Then
                            Me.AllowBackOrder = CType(dr("bitAllowBackOrder"), Boolean)
                        End If

                        If Not IsDBNull(dr("numIncomeChartAcntId")) Then
                            Me.IncomeChartAcntId = CType(dr("numIncomeChartAcntId"), Long)
                        End If

                        If Not IsDBNull(dr("numAssetChartAcntId")) Then
                            Me.AssetChartAcntId = CType(dr("numAssetChartAcntId"), Long)
                        End If

                        If Not IsDBNull(dr("numCOGsChartAcntId")) Then
                            Me.COGSChartAcntId = CType(dr("numCOGsChartAcntId"), Long)
                        End If

                        If Not IsDBNull(dr("numItemClassification")) Then
                            Me.ItemClassification = CType(dr("numItemClassification"), Long)
                        End If

                        If Not IsDBNull(dr("numItemGroup")) Then
                            Me.ItemGroupID = CType(dr("numItemGroup"), Long)
                        End If

                        If Not IsDBNull(dr("numPurchaseUnit")) Then
                            Me.PurchaseUnit = CType(dr("numPurchaseUnit"), Long)
                        End If

                        If Not IsDBNull(dr("numSaleUnit")) Then
                            Me.SaleUnit = CType(dr("numSaleUnit"), Long)
                        End If

                        If Not IsDBNull(dr("numItemClass")) Then
                            Me.ItemClass = CType(dr("numItemClass"), Long)
                        End If

                        If Not IsDBNull(dr("numBaseUnit")) Then
                            Me.BaseUnit = CType(dr("numBaseUnit"), Long)
                        End If

                        If Not IsDBNull(dr("vcSKU")) Then
                            Me.SKU = CType(dr("vcSKU"), String)
                        End If

                        If Not IsDBNull(dr("charItemType")) Then
                            Me.ItemType = CType(dr("charItemType"), String)
                        End If

                        If Not IsDBNull(dr("bitTaxable")) Then
                            Me.Taxable = CType(dr("bitTaxable"), Boolean)
                        End If

                        If Not IsDBNull(dr("txtDesc")) Then
                            Me.str = CType(dr("txtDesc"), String)
                        End If

                        If Not IsDBNull(dr("vcExportToAPI")) Then
                            Me.ListOfAPI = CType(dr("vcExportToAPI"), String)
                        End If

                        If Not IsDBNull(dr("bitAllowDropShip")) Then
                            Me.AllowDropShip = CType(dr("bitAllowDropShip"), Boolean)
                        End If

                        If Not IsDBNull(dr("bitMatrix")) Then
                            Me.IsMatrix = CType(dr("bitMatrix"), Boolean)
                        End If

                        If Not IsDBNull(dr("numReOrder")) Then
                            Me.ReOrder = CType(dr("numReOrder"), Double)
                        End If
                    Next
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub SaveItemForImport()
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numItemCode", ItemCode, NpgsqlTypes.NpgsqlDbType.BigInt, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@numShipClass", ShippingClass, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcItemName", ItemName, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@monListPrice", ListPrice, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@txtItemDesc", ItemDesc, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcModelID", ModelID, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcManufacturer", Manufacturer, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numBarCodeId", BarCodeID, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@fltWidth", Width, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@fltHeight", Height, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@fltWeight", Weight, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@fltLength", Length, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@monAverageCost", AverageCost, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@bitSerialized", bitSerialized, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitKitParent", KitParent, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitAssembly", Assembly, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@IsArchieve", bitArchieve, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitLotNo", bitLotNo, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitAllowBackOrder", AllowBackOrder, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@numIncomeChartAcntId", IncomeChartAcntId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numAssetChartAcntId", AssetChartAcntId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numCOGsChartAcntId", COGSChartAcntId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numItemClassification", ItemClassification, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numItemGroup", ItemGroupID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numPurchaseUnit", PurchaseUnit, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numSaleUnit", SaleUnit, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numItemClass", ItemClass, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numBaseUnit", BaseUnit, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcSKU", SKU, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@charItemType", ItemType, NpgsqlTypes.NpgsqlDbType.Char))
                    .Add(SqlDAL.Add_Parameter("@bitTaxable", Taxable, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@txtDesc", str, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcExportToAPI", ListOfAPI, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@bitAllowDropShip", AllowDropShip, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitMatrix", IsMatrix, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@vcItemAttributes", ItemAttributes, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcCategories", ListOfCategories, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@txtShortDesc", txtShortDesc, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numReOrder", ReOrder, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@bitAutomateReorderPoint", IsAutomateReorderPoint, NpgsqlTypes.NpgsqlDbType.Bit))
                End With


                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_Import_SaveItemDetails", objParam, True)
                Me.ItemCode = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(2).Value)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function UpdateReorder() As Long
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ItemCode

                arParms(3) = New Npgsql.NpgsqlParameter("@numWarehouseItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = WareHouseItemID

                arParms(4) = New Npgsql.NpgsqlParameter("@numReorder", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(4).Value = ReOrder

                Dim intResult As Long = 0
                Dim objParam() As Object
                objParam = arParms.ToArray()
                intResult = CLng(SqlDAL.ExecuteNonQuery(connString, "USP_WarehouseItems_UpdateReorder", objParam, True))
                _ItemCode = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(2).Value)

                Return intResult
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function SaveAttributesForMatrixItems() As Long
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numWareHouseItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Direction = ParameterDirection.InputOutput
                arParms(2).Value = _WareHouseItemID

                arParms(3) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _ItemCode

                arParms(4) = New Npgsql.NpgsqlParameter("@monWListPrice", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(4).Value = WListPrice

                arParms(5) = New Npgsql.NpgsqlParameter("@vcWHSKU", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(5).Value = WSKU

                arParms(6) = New Npgsql.NpgsqlParameter("@vcBarCode", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(6).Value = WBarCode

                arParms(7) = New Npgsql.NpgsqlParameter("@strFieldList", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(7).Value = strFieldList

                Dim intResult As Long = 0
                Dim objParam() As Object
                objParam = arParms.ToArray()
                intResult = CLng(SqlDAL.ExecuteNonQuery(connString, "USP_SaveAttributesForMatrixItems", objParam, True))
                _ItemCode = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(2).Value)

                Return intResult
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetMultiSelectItems(ByVal DomainID As Long, ByVal strItemCodes As String, ByVal oppType As Short) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(1).Value = strItemCodes

                arParms(2) = New Npgsql.NpgsqlParameter("@tintOppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = oppType

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetMultiSelectItems", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetAttributesForSelectedItemGroup() As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numListID", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _numListid

                arParms(2) = New Npgsql.NpgsqlParameter("@numItemGroupID", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _ItemGroupID

                arParms(3) = New Npgsql.NpgsqlParameter("@numWareHouseID", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _WarehouseID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetAttributesForSelectedItemGroup", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetItemDetailForStockTransfer(ByVal fromWarehouseItemID As Long, ByVal toWarehouseItemID As Long) As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numItemCode", ItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numFromWarehouseItemID", fromWarehouseItemID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numToWarehouseItemID", toWarehouseItemID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_Item_GetDetailForStockTransfer", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function SearchItemByFieldAndText(ByVal numDomainID As Long, ByVal numDivisionID As Long, ByVal numFieldID As Long, ByVal vcSearchText As String) As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", numDomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDivisionID", numDivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numFieldID", numFieldID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcSearchText", vcSearchText, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_Item_SearchByFieldAndText", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetOneWarehouseByItemExternalInternalLocation() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numItemCode", ItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWareHouseID", WarehouseID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWarehouseLocationID", WarehouseLocationID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_WarehouseItems_GetByItemAndExternalInternalLocation", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetInventoryValuation() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@dtDate", DateEntered, NpgsqlTypes.NpgsqlDbType.Date))
                    .Add(SqlDAL.Add_Parameter("@tintCurrentPage", CurrentPage, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_GetInventoryValuationReport", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetKitAssemblyCalculatedPrice() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDivisionID", DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numItemCode", ItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numQty", Quantity, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@tintKitAssemblyPriceBasedOn", KitAssemblyPriceBasedOn, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_Item_GetKitAssemblyCalculatedPrice", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub ManageSiteCategoriesVisibility(ByVal isVisible As Boolean)
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numCategoryProfileID", CategoryProfileID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numCategoryID", CategoryID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitShow", isVisible, NpgsqlTypes.NpgsqlDbType.Bit))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_SiteCategories_ShowHide", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function CheckBackOrderPermission(ByVal UserAuthGroupId As Long, ByVal qunatity As Long, ByVal numWarehouseitemid As Long, ByVal kitChildItem As String) As Integer
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@UserAuthGroupId", UserAuthGroupId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numitemcode", ItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numQuantity", Convert.ToDouble(qunatity), NpgsqlTypes.NpgsqlDbType.Double))
                    .Add(SqlDAL.Add_Parameter("@numWareHouseItemid", numWarehouseitemid, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@kitChildItem", kitChildItem, NpgsqlTypes.NpgsqlDbType.Varchar, 1000))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteScalar(connString, "USP_GetBackOrderPermission", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetDependedKitChilds() As DataSet
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numMainKitItemCode", ItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numChildKitItemCode", ChildItemID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numChildKitSelectedItem", KitSelectedChild, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDataset(connString, "USP_ItemDetails_GetDependedChildKitItems", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function IsValidKitSelection() As Boolean
            Try
                Try
                    Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                    Dim getconnection As New GetConnection
                    Dim connString As String = GetConnection.GetConnectionString

                    With sqlParams
                        .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                        .Add(SqlDAL.Add_Parameter("@numItemCode", ItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                        .Add(SqlDAL.Add_Parameter("@vcKitChildItems", vcKitChildItems, NpgsqlTypes.NpgsqlDbType.Varchar))
                        .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    End With

                    Return CCommon.ToBool(SqlDAL.ExecuteScalar(connString, "USP_ItemDetails_ValidateKitSelection", sqlParams.ToArray()))
                Catch ex As Exception
                    Throw
                End Try
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetItemCalculatedPrice() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numSiteID", SiteID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDivisionID", DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numItemCode", ItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numQty", Quantity, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@vcChildKitSelectedItem", KitSelectedChild, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_Item_GetCalculatedPrice", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetKits() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_Item_GetKits", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetChildKits() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numItemCode", ItemCode, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_Item_GetChildKits", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ValidateKitChildsOrderImport(ByVal domainID As Long, ByVal itemCode As Long, ByVal kitChilds As String) As String
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", domainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numItemCode", itemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcKitChilds", kitChilds, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteScalar(connString, "USP_OpportunityItems_ValidateKitChildsOrderImport", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetKitChildsFirstLevel() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@strItem", _str, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numItemCode", ItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numPageIndex", CurrentPage, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@numPageSize", PageSize, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@TotalCount", TotalRecords, NpgsqlTypes.NpgsqlDbType.Bigint, 18, ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object = sqlParams.ToArray()

                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetKitChildsFirstLevel", objParam, True)

                Me.TotalRecords = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(5).Value)

                Return ds
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetSelectedChildItemDetails(ByVal kitChilds As String, ByVal isEditClick As Boolean) As DataSet
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numItemCode", ItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numChildItemCode", ChildItemID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcKitChilds", kitChilds, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@bitEdit", isEditClick, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDataset(connString, "USP_Item_SelectedChildItemDetails", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetPriceBasedOnPricingMethod(ByVal orderedQty As Double, ByVal kitChilds As String, ByVal currencyID As Long) As Double
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDivisionID", DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numItemCode", ItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWarehouseItemID", WareHouseItemID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numQty", orderedQty, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@vcChildKits", kitChilds, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numCurrencyID", currencyID, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                Return CCommon.ToDouble(SqlDAL.ExecuteScalar(connString, "USP_Item_GetPriceBasedOnPricingMethod", sqlParams.ToArray()))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetEcommerceWarehouseAvailability(ByVal warehouseAvailability As Short, ByVal isDisplayQtyAvailable As Boolean) As String
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numItemCode", ItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintWarehouseAvailability", warehouseAvailability, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@bitDisplayQtyAvailable", isDisplayQtyAvailable, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return CCommon.ToString(SqlDAL.ExecuteScalar(connString, "USP_Item_GetEcommerceWarehouseAvailability", sqlParams.ToArray()))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetAssemblyDetail() As DataSet
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numItemCode", ItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWarehouseItemID", WareHouseItemID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDataset(connString, "USP_Item_GetAssemblyDetail", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetAssemblyMfgTimeCost() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numItemCode", ItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWarehouseID", WarehouseID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numQtyToBuild", Quantity, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@numBuildProcessID", BuildProcessId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@dtProjectedStartDate", StartDate, NpgsqlTypes.NpgsqlDbType.Timestamp))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_Item_GetAssemblyMfgTimeCost", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetWareHousesForPick() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numItemCode", ItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWarehouseID", WarehouseID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWarehouseItemID", WareHouseItemID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcWarehouseLocation", SearchText, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numPageIndex", CurrentPage, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@numPageSize", PageSize, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim dt As DataTable = SqlDAL.ExecuteDatable(connString, "USP_Item_GetWareHousesForPick", sqlParams.ToArray())

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    Me.TotalRecords = CCommon.ToLong(dt.Rows(0)("numTotalRecords"))
                Else
                    Me.TotalRecords = 0
                End If

                Return dt
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetItemsForECommerceDynamicValues(ByVal IsAvaiablityTokenCheck As Boolean, ByVal dtItem_UOMConversion As DataTable) As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                Dim dsInsert As New DataSet
                dsInsert.Tables.Add(dtItem_UOMConversion)

                arParms(0) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _SiteID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DivisionID

                arParms(2) = New Npgsql.NpgsqlParameter("@IsAvaiablityTokenCheck", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(2).Value = IsAvaiablityTokenCheck

                arParms(3) = New Npgsql.NpgsqlParameter("@numWareHouseID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(3).Value = WarehouseID

                arParms(4) = New Npgsql.NpgsqlParameter("@UTV_Item_UOMConversion", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(4).Value = dsInsert.GetXml()

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet

                ds = SqlDAL.ExecuteDataset(connString, "USP_Item_ElasticSearchBizCart_GetDynamicValues", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Sub UpdateItemVendorCost(ByVal selectedRecords As String)
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcSelectedRecords", selectedRecords, NpgsqlTypes.NpgsqlDbType.VarChar))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_Item_UpdateItemVendorCost", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetMatchedPO() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcItems", strItemCodes, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_Item_GetMatchedPO", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetPriceLevelEcommHtml() As String
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numSiteID", SiteID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numItemCode", ItemCode, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return CCommon.ToString(SqlDAL.ExecuteScalar(connString, "USP_Item_GetPriceLevelEcommHtml", sqlParams.ToArray()))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function IsKitWithChildKits() As Boolean
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numItemCode", ItemCode, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return CCommon.ToBool(SqlDAL.ExecuteScalar(connString, "USP_Item_IsKitWithChildKits", sqlParams.ToArray()))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetEcommerceWarehouseInTransit() As String
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numItemCode", ItemCode, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return CCommon.ToString(SqlDAL.ExecuteScalar(connString, "USP_Item_GetEcommerceWarehouseInTransit", sqlParams.ToArray()))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function CreateNewItemFromKitConfiguration() As Long
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@p_numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@p_numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@p_numItemCode", ItemCode, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@p_vcCurrentKitConfiguration", KitSelectedChild, NpgsqlTypes.NpgsqlDbType.Text))
                    .Add(SqlDAL.Add_Parameter("@p_bitAssembly", Assembly, NpgsqlTypes.NpgsqlDbType.Boolean))
                    .Add(SqlDAL.Add_Parameter("@p_bitKitParent", KitParent, NpgsqlTypes.NpgsqlDbType.Boolean))
                    .Add(SqlDAL.Add_Parameter("@p_numNewItemCode", Nothing, NpgsqlTypes.NpgsqlDbType.Numeric, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object = sqlParams.ToArray()

                SqlDAL.ExecuteDatasetWithOutPut(connString, "usp_item_createnewitemfromkitconfiguration", objParam, True)

                Return CCommon.ToLong(DirectCast(objParam, Npgsql.NpgsqlParameter())(6).Value)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

End Namespace