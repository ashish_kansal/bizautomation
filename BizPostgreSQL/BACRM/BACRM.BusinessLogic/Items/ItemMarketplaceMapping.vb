﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic

Public Class ItemMarketplaceMapping
    Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"

    Public Property ItemCode As Long
    Public Property MarketplaceID As Long
    Public Property MarketplaceUniqueID As String

#End Region

#Region "Public Methods"

    Public Sub Save()
        Try
            Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString

            With sqlParams
                .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                .Add(SqlDAL.Add_Parameter("@numItemCode", ItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                .Add(SqlDAL.Add_Parameter("@numMarketplaceID", MarketplaceID, NpgsqlTypes.NpgsqlDbType.BigInt))
                .Add(SqlDAL.Add_Parameter("@vcMarketplaceUniqueID", MarketplaceUniqueID, NpgsqlTypes.NpgsqlDbType.VarChar))
            End With

            SqlDAL.ExecuteNonQuery(connString, "USP_ItemMarketplaceMapping_Save", sqlParams.ToArray())
        Catch ex As Exception
            Throw
        End Try
    End Sub

#End Region


End Class
