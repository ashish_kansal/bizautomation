﻿Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Drawing.Text
Imports System.IO
Imports System.Text.RegularExpressions

''http://www.codeproject.com/Articles/3888/C-Barcode-Generator-WebService
Namespace BACRM.BusinessLogic.Item
    ''' <summary>
    ''' Summary description for Code39.
    ''' </summary>
    Public Class Code39
        Inherits BACRM.BusinessLogic.CBusinessBase
        Private Const _itemSepHeight As Integer = 3

        Private _titleSize As SizeF = SizeF.Empty
        Private _barCodeSize As SizeF = SizeF.Empty
        Private _codeStringSize As SizeF = SizeF.Empty

#Region "Barcode Title"

        Private _titleString As String = Nothing
        Private _titleFont As Font = Nothing

        Public Property Title() As String
            Get
                Return _titleString
            End Get
            Set(value As String)
                _titleString = value
            End Set
        End Property

        Public Property TitleFont() As Font
            Get
                Return _titleFont
            End Get
            Set(value As Font)
                _titleFont = value
            End Set
        End Property
#End Region

#Region "Barcode code string"

        Private _showCodeString As Boolean = False
        Private _codeStringFont As Font = Nothing

        Public Property ShowCodeString() As Boolean
            Get
                Return _showCodeString
            End Get
            Set(value As Boolean)
                _showCodeString = value
            End Set
        End Property

        Public Property CodeStringFont() As Font
            Get
                Return _codeStringFont
            End Get
            Set(value As Font)
                _codeStringFont = value
            End Set
        End Property
#End Region

#Region "Barcode Font"

        Private _c39Font As Font = Nothing
        Private _c39FontSize As Single = 12
        Private _c39FontFileName As String = Nothing
        Private _c39FontFamilyName As String = Nothing

        Public Property FontFileName() As String
            Get
                Return _c39FontFileName
            End Get
            Set(value As String)
                _c39FontFileName = value
            End Set
        End Property

        Public Property FontFamilyName() As String
            Get
                Return _c39FontFamilyName
            End Get
            Set(value As String)
                _c39FontFamilyName = value
            End Set
        End Property

        Public Property FontSize() As Single
            Get
                Return _c39FontSize
            End Get
            Set(value As Single)
                _c39FontSize = value
            End Set
        End Property

        Private ReadOnly Property Code39Font() As Font
            Get
                If _c39Font Is Nothing Then
                    ' Load the barcode font			
                    Dim pfc As New PrivateFontCollection()
                    pfc.AddFontFile(_c39FontFileName)
                    Dim family As New FontFamily(_c39FontFamilyName, pfc)
                    _c39Font = New Font(family, _c39FontSize)
                End If
                Return _c39Font
            End Get
        End Property

#End Region

        Public Sub New()
            _titleFont = New Font("Arial", 10)
            _codeStringFont = New Font("Arial", 10)
        End Sub

#Region "Barcode Generation"

        Public Function GenerateBarcode(barCode As String) As Bitmap

            Dim bcodeWidth As Integer = 0
            Dim bcodeHeight As Integer = 0

            ' Get the image container...
            Dim bcodeBitmap As Bitmap = CreateImageContainer(barCode, bcodeWidth, bcodeHeight)
            Dim objGraphics As Graphics = Graphics.FromImage(bcodeBitmap)

            ' Fill the background			
            objGraphics.FillRectangle(New SolidBrush(Color.White), New Rectangle(0, 0, bcodeWidth, bcodeHeight))

            Dim vpos As Integer = 0

            ' Draw the title string
            If _titleString IsNot Nothing Then
                objGraphics.DrawString(_titleString, _titleFont, New SolidBrush(Color.Black), XCentered(CInt(Math.Truncate(_titleSize.Width)), bcodeWidth), vpos)
                vpos += (CInt(Math.Truncate(_titleSize.Height)) + _itemSepHeight)
            End If
            ' Draw the barcode
            objGraphics.DrawString(barCode, Code39Font, New SolidBrush(Color.Black), XCentered(CInt(Math.Truncate(_barCodeSize.Width)), bcodeWidth), vpos)

            ' Draw the barcode string
            If _showCodeString Then
                vpos += CInt(Math.Truncate(_barCodeSize.Height))
                objGraphics.DrawString(barCode, _codeStringFont, New SolidBrush(Color.Black), XCentered(CInt(Math.Truncate(_codeStringSize.Width)), bcodeWidth), vpos)
            End If

            ' return the image...									
            Return bcodeBitmap
        End Function

        Private Function CreateImageContainer(barCode As String, ByRef bcodeWidth As Integer, ByRef bcodeHeight As Integer) As Bitmap

            Dim objGraphics As Graphics

            ' Create a temporary bitmap...
            Dim tmpBitmap As New Bitmap(1, 1, PixelFormat.Format32bppArgb)
            objGraphics = Graphics.FromImage(tmpBitmap)

            ' calculate size of the barcode items...
            If _titleString IsNot Nothing Then
                _titleSize = objGraphics.MeasureString(_titleString, _titleFont)
                bcodeWidth = CInt(Math.Truncate(_titleSize.Width))
                bcodeHeight = CInt(Math.Truncate(_titleSize.Height)) + _itemSepHeight
            End If

            _barCodeSize = objGraphics.MeasureString(barCode, Code39Font)
            bcodeWidth = Max(bcodeWidth, CInt(Math.Truncate(_barCodeSize.Width)))
            bcodeHeight += CInt(Math.Truncate(_barCodeSize.Height))

            If _showCodeString Then
                _codeStringSize = objGraphics.MeasureString(barCode, _codeStringFont)
                bcodeWidth = Max(bcodeWidth, CInt(Math.Truncate(_codeStringSize.Width)))
                bcodeHeight += (_itemSepHeight + CInt(Math.Truncate(_codeStringSize.Height)))
            End If

            ' dispose temporary objects...
            objGraphics.Dispose()
            tmpBitmap.Dispose()

            Return (New Bitmap(bcodeWidth, bcodeHeight, PixelFormat.Format32bppArgb))
        End Function

#End Region


#Region "Auxiliary Methods"

        Private Function Max(v1 As Integer, v2 As Integer) As Integer
            Return (If(v1 > v2, v1, v2))
        End Function

        Private Function XCentered(localWidth As Integer, globalWidth As Integer) As Integer
            Return ((globalWidth - localWidth) \ 2)
        End Function

#End Region

    End Class
End Namespace
