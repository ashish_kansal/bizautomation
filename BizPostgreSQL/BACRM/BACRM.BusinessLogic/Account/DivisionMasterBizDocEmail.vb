﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Account

    Public Class DivisionMasterBizDocEmail
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"

        Public Property OppBizDocID As Long
        Public Property DivisionID As Long
        Public Property BizDocID As Long

#End Region

#Region "Public Methods"

        Public Function GetByOppBizDocID() As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppBizDocID", OppBizDocID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_DivisionMasterBizDocEmail_GetByOppBizDocID", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

#End Region


    End Class

End Namespace