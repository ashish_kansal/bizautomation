'**********************************************************************************
' <CAccount.vb>
' 
' 	CHANGE CONTROL:
'	
'	AUTHOR: Goyal 	DATE:28-Feb-05 	VERSION	CHANGES	KEYSTRING:
' Modified By anoop jayaraj
'**********************************************************************************

Option Explicit On 
Option Strict On

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Account

    '**********************************************************************************
    ' Module Name  : None
    ' Module Type  : CAccount
    ' 
    ' Description  : This is the business logic classe for Accounts Module
    '**********************************************************************************
    Public Class CAccounts
        Inherits BACRM.BusinessLogic.CBusinessBase


#Region "Constructor"
        '**********************************************************************************
        ' Name         : New
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal intUserId As Int32               
        ' Description  : The constructor                
        ' Notes        : N/A                
        ' Created By   : Goyal 	DATE:28-Feb-05
        '**********************************************************************************
        'Public Sub New(ByVal intUserId As Integer)
        '    'Constructor
        '    MyBase.New(intUserId)
        'End Sub

        '**********************************************************************************
        ' Name         : New
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal intUserId As Int32              
        ' Description  : The constructor                
        ' Notes        : N/A                
        ' Created By   : Goyal 	DATE:28-Feb-05
        '**********************************************************************************
        Public Sub New()
            'Constructor
            MyBase.New()
        End Sub
#End Region

        'define private vairable
        ''Private DomainId As Long = 0
        Private _ContactType As Integer = 0
        Private _ContactID As Long
        'Private UserCntID As Long = 0
        Private _FirstName As String = String.Empty
        Private _LastName As String = String.Empty
        Private _Name As String = String.Empty
        Private _CompanyName As String = String.Empty
        Private _CompanyID As Long
        Private _DivisionID As Long
        Private _UserRights As Integer = 0
        Private _ContactNO As String = String.Empty
        Private _ShowAll As Integer = 0
        Private _CommunID As Long
        Private _UserRightType As Long = 0
        Private _SortOrder As Integer
        Private _SortChar As String = String.Empty
        Private _RelType As Long = 0
        Private _CustName As String = String.Empty
        Private _SortChar1 As String = String.Empty
        Private _TerritoryID As Long = 0
        Private _ContactExt As String = String.Empty
        Private _Email As String = String.Empty
        Private _CRMType As Short
        Private _supportKey As Long = 0
        Private _DomainName As String
        Private _CurrentPage As Integer
        Private _PageSize As Integer
        Private _TotalRecords As Integer
        Private _SortCharacter As Char
        Private _columnSortOrder As String
        Private _columnName As String
        Private _OppType As Short
        Private _Profile As Long
        Private _bitPartner As Boolean
        Private _FormId As Short
        Public Property FormId() As Short
            Get
                Return _FormId
            End Get
            Set(ByVal Value As Short)
                _FormId = Value
            End Set
        End Property
        Private _ClientTimeZoneOffset As Integer
        Public Property ClientTimeZoneOffset() As Integer
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal Value As Integer)
                _ClientTimeZoneOffset = Value
            End Set
        End Property

        Private _FollowUpStatus As Long
        Public Property FollowUpStatus() As Long
            Get
                Return _FollowUpStatus
            End Get
            Set(ByVal value As Long)
                _FollowUpStatus = value
            End Set
        End Property

        Public Property bitPartner() As Boolean
            Get
                Return _bitPartner
            End Get
            Set(ByVal Value As Boolean)
                _bitPartner = Value
            End Set
        End Property

        Public Property Profile() As Long
            Get
                Return _Profile
            End Get
            Set(ByVal Value As Long)
                _Profile = Value
            End Set
        End Property

        Public Property OppType() As Short
            Get
                Return _OppType
            End Get
            Set(ByVal Value As Short)
                _OppType = Value
            End Set
        End Property

        Public Property columnName() As String
            Get
                Return _columnName
            End Get
            Set(ByVal Value As String)
                _columnName = Value
            End Set
        End Property

        Public Property columnSortOrder() As String
            Get
                Return _columnSortOrder
            End Get
            Set(ByVal Value As String)
                _columnSortOrder = Value
            End Set
        End Property

        Public Property SortCharacter() As Char
            Get
                Return _SortCharacter
            End Get
            Set(ByVal Value As Char)
                _SortCharacter = Value
            End Set
        End Property

        Public Property TotalRecords() As Integer
            Get
                Return _TotalRecords
            End Get
            Set(ByVal Value As Integer)
                _TotalRecords = Value
            End Set
        End Property

        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal Value As Integer)
                _PageSize = Value
            End Set
        End Property

        Public Property CurrentPage() As Integer
            Get
                Return _CurrentPage
            End Get
            Set(ByVal Value As Integer)
                _CurrentPage = Value
            End Set
        End Property

        Public Property DomainName() As String
            Get
                Return _DomainName
            End Get
            Set(ByVal Value As String)
                _DomainName = Value
            End Set
        End Property
        Public Property supportKey() As Long
            Get
                Return _supportKey
            End Get
            Set(ByVal Value As Long)
                _supportKey = Value
            End Set
        End Property
        Public Property CRMType() As Short
            Get
                Return _CRMType
            End Get
            Set(ByVal Value As Short)
                _CRMType = Value
            End Set
        End Property



        Public Property Email() As String
            Get
                Return _Email
            End Get
            Set(ByVal Value As String)
                _Email = Value
            End Set
        End Property
        Public Property ContactExt() As String
            Get
                Return _ContactExt
            End Get
            Set(ByVal Value As String)
                _ContactExt = Value
            End Set
        End Property
        Public Property TerritoryID() As Long
            Get
                Return _TerritoryID
            End Get
            Set(ByVal Value As Long)
                _TerritoryID = Value
            End Set
        End Property
        Public Property CustName() As String
            Get
                Return _CustName
            End Get
            Set(ByVal Value As String)
                _CustName = Value
            End Set
        End Property
        Public Property UserRightType() As Long
            Get
                Return _UserRightType
            End Get
            Set(ByVal Value As Long)
                _UserRightType = Value
            End Set
        End Property
        Public Property SortOrder() As Integer
            Get
                Return _SortOrder
            End Get
            Set(ByVal Value As Integer)
                _SortOrder = Value
            End Set
        End Property
        Public Property SortChar() As String
            Get
                Return _SortChar
            End Get
            Set(ByVal Value As String)
                _SortChar = Value
            End Set
        End Property
        Public Property RelType() As Long
            Get
                Return _RelType
            End Get
            Set(ByVal Value As Long)
                _RelType = Value
            End Set
        End Property

        ''Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property
        Public Property ContactType() As Integer
            Get
                Return _ContactType
            End Get
            Set(ByVal Value As Integer)
                _ContactType = Value
            End Set
        End Property
        Public Property ContactID() As Long
            Get
                Return _ContactID
            End Get
            Set(ByVal Value As Long)
                _ContactID = Value
            End Set
        End Property
        'Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal Value As Long)
        '        UserCntID = Value
        '    End Set
        'End Property
        Public Property FirstName() As String
            Get
                Return _FirstName
            End Get
            Set(ByVal Value As String)
                _FirstName = Value
            End Set
        End Property
        Public Property LastName() As String
            Get
                Return _LastName
            End Get
            Set(ByVal Value As String)
                _LastName = Value
            End Set
        End Property
        Public Property Name() As String
            Get
                Return _Name
            End Get
            Set(ByVal Value As String)
                _Name = Value
            End Set
        End Property
        Public Property CompanyName() As String
            Get
                Return _CompanyName
            End Get
            Set(ByVal Value As String)
                _CompanyName = Value
            End Set
        End Property
        Public Property CompanyID() As Long
            Get
                Return _CompanyID
            End Get
            Set(ByVal Value As Long)
                _CompanyID = Value
            End Set
        End Property
        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property
        Public Property UserRights() As Integer
            Get
                Return _UserRights
            End Get
            Set(ByVal Value As Integer)
                _UserRights = Value
            End Set
        End Property
        Public Property ContactNo() As String
            Get
                Return _ContactNO
            End Get
            Set(ByVal Value As String)
                _ContactNO = Value
            End Set
        End Property
        Public Property CommunID() As Long
            Get
                Return _CommunID
            End Get
            Set(ByVal Value As Long)
                _CommunID = Value
            End Set
        End Property

        Public Property SortChar1() As String
            Get
                Return _SortChar1
            End Get
            Set(ByVal Value As String)
                _SortChar1 = Value
            End Set
        End Property

        Private _ActiveInActive As Boolean
        Public Property ActiveInActive() As Boolean
            Get
                Return _ActiveInActive
            End Get
            Set(ByVal Value As Boolean)
                _ActiveInActive = Value
            End Set
        End Property

        Private _RegularSearchCriteria As String
        Public Property RegularSearchCriteria() As String
            Get
                Return _RegularSearchCriteria
            End Get
            Set(ByVal Value As String)
                _RegularSearchCriteria = Value
            End Set
        End Property

        Private _CustomSearchCriteria As String
        Public Property CustomSearchCriteria() As String
            Get
                Return _CustomSearchCriteria
            End Get
            Set(ByVal Value As String)
                _CustomSearchCriteria = Value
            End Set
        End Property
        Private _SearchTxt As String
        Public Property SearchTxt() As String
            Get
                Return _SearchTxt
            End Get
            Set(ByVal Value As String)
                _SearchTxt = Value
            End Set
        End Property
        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  RemoveRecrod()
        '     Purpose					 :  To delete data from database 
        '     Example                    :  
        '     Parameters                 :  @ItemCode                     
        '                                 
        '     Outputs					 :  It return true  if deleted otherwise return false
        '						
        '     Author Name				 :  Ajeet Singh
        '     Date Written				 :  18/10/2004
        '     Cross References 			 :  List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Function DeleteOrg() As String
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString

            Try
                ' Set up parameters 
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim lintcount As Long
                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(1).Value = 0

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                lintcount = CInt(SqlDAL.ExecuteScalar(connString, "USP_GetAuthoritativeBizDocsCount", arParms))
                If lintcount > 0 Then Throw New Exception("AUTH_BIZDOC")
                SqlDAL.ExecuteScalar(connString, "usp_DeleteAccounts", arParms)
                Return ""
            Catch ex As Exception
                Select Case ex.Message
                    Case "CHILD_VENDOR"
                        Return "Selected record is associated as vendor in inventory management, Can not be deleted."

                    Case "CHILD_PROJECT"
                        Return "Selected record is associated with project(s), Can not be deleted."

                    Case "CHILD_CASE"
                        Return "Selected record is associated with case(s), Can not be deleted."

                    Case "CHILD_OPP"
                        Return "Selected record is associated with Opportunities/Orders, Can not be deleted."

                    Case "CHILD_ASSET"
                        Return "Selected record is associated with Asset Items, Can not be deleted."

                    Case "AUTH_BIZDOC"
                        Return "Selected record is associated with Authoritative BizDocs, Can not be deleted."

                    Case "BILL"
                        Return "Paid/Unpaid Bills are associated with selected record, Can not be deleted."

                    Case "DEPOSIT_EXISTS"
                        Return "Deposits are associated with selected record, can not be deleted."

                    Case "RETURN_EXISTS"
                        Return "Returns are associated with selected record, can not be deleted."

                    Case "STANDALONE_BILL_EXISTS"
                        Return "Stand Alone Bills are associated with selected record, can not be deleted."

                    Case "BILL_PAYMENT_EXISTS"
                        Return "Bill Payments are associated with selected record, can not be deleted."

                    Case "CHECK_EXISTS"
                        Return "Checks are associated with selected record, can not be deleted."

                    Case Else
                        Throw ex
                End Select
            End Try
        End Function

        'Public Function GenerateSupportKey() As Boolean
        '    Dim getconnection As New getconnection
        '    Dim connString As String = getconnection.GetConnectionString
        '    Try

        '        Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}


        '        arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
        '        arParms(0).Value = _DivisionID

        '        _supportKey = CInt(SqlDAL.ExecuteScalar(connString, "Usp_GenerateSupportKey", arParms))
        '        Return True
        '    Catch ex As Exception
        '        Return False
        '        Throw ex
        '    Finally

        '    End Try

        'End Function

        Public Function GetOpenCases() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "usp_GetCaseDetailsForAccount", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetCaseHstr() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "usp_GetCaseDetailsForAccounthstr", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function CheckDealsWon() As Integer
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return CInt(SqlDAL.ExecuteScalar(connString, "USP_CheckOpportunity", arParms))


            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function




        Public Function GetAccounts() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(19) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@CRMType", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(0).Value = _CRMType

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintUserRightType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _UserRightType

                arParms(3) = New Npgsql.NpgsqlParameter("@tintSortOrder", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _SortOrder

                arParms(4) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(4).Value = _SortCharacter

                arParms(5) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(5).Value = DomainID

                arParms(6) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = _CurrentPage

                arParms(7) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(7).Value = _PageSize

                arParms(8) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(8).Direction = ParameterDirection.InputOutput
                arParms(8).Value = _TotalRecords

                arParms(9) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.Varchar, 50)
                arParms(9).Value = _columnName

                arParms(10) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.Varchar, 10)
                arParms(10).Value = _columnSortOrder

                arParms(11) = New Npgsql.NpgsqlParameter("@numProfile", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(11).Value = _Profile

                arParms(12) = New Npgsql.NpgsqlParameter("@bitPartner", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(12).Value = _bitPartner

                arParms(13) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(13).Value = _ClientTimeZoneOffset

                arParms(14) = New Npgsql.NpgsqlParameter("@bitActiveInActive", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(14).Value = _ActiveInActive

                arParms(15) = New Npgsql.NpgsqlParameter("@vcRegularSearchCriteria", NpgsqlTypes.NpgsqlDbType.Varchar, 1000)
                arParms(15).Value = _RegularSearchCriteria

                arParms(16) = New Npgsql.NpgsqlParameter("@vcCustomSearchCriteria", NpgsqlTypes.NpgsqlDbType.Varchar, 1000)
                arParms(16).Value = _CustomSearchCriteria

                arParms(17) = New Npgsql.NpgsqlParameter("@SearchText", NpgsqlTypes.NpgsqlDbType.Varchar, 1000)
                arParms(17).Value = _SearchTxt

                arParms(18) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(18).Value = Nothing
                arParms(18).Direction = ParameterDirection.InputOutput

                arParms(19) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(19).Value = Nothing
                arParms(19).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object
                objParam = arParms.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_AccountList1", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(8).Value)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetProspects() As DataSet
            Try


                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(19) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@CRMType", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(0).Value = _CRMType

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintUserRightType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _UserRightType

                arParms(3) = New Npgsql.NpgsqlParameter("@tintSortOrder", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _SortOrder

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(4).Value = DomainID

                arParms(5) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(5).Value = _SortCharacter

                'arParms(6) = New Npgsql.NpgsqlParameter("@FirstName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                'arParms(6).Value = IIf(_FirstName = "", "", Replace(_FirstName, "'", "''"))

                'arParms(7) = New Npgsql.NpgsqlParameter("@LastName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                'arParms(7).Value = IIf(_LastName = "", "", Replace(_LastName, "'", "''"))

                'arParms(8) = New Npgsql.NpgsqlParameter("@CustName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)     
                'arParms(8).Value = IIf(_CustName = "", "", Replace(_CustName, "'", "''"))

                arParms(6) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = _CurrentPage

                arParms(7) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(7).Value = _PageSize

                arParms(8) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.Varchar, 50)
                arParms(8).Value = _columnName

                arParms(9) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.Varchar, 10)
                arParms(9).Value = _columnSortOrder

                arParms(10) = New Npgsql.NpgsqlParameter("@numProfile", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(10).Value = _Profile

                arParms(11) = New Npgsql.NpgsqlParameter("@bitPartner", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(11).Value = _bitPartner

                arParms(12) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(12).Value = _ClientTimeZoneOffset

                arParms(13) = New Npgsql.NpgsqlParameter("@bitActiveInActive", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(13).Value = _ActiveInActive

                arParms(14) = New Npgsql.NpgsqlParameter("@vcRegularSearchCriteria", NpgsqlTypes.NpgsqlDbType.Varchar, 1000)
                arParms(14).Value = _RegularSearchCriteria

                arParms(15) = New Npgsql.NpgsqlParameter("@vcCustomSearchCriteria", NpgsqlTypes.NpgsqlDbType.Varchar, 1000)
                arParms(15).Value = _CustomSearchCriteria

                arParms(16) = New Npgsql.NpgsqlParameter("@SearchText", NpgsqlTypes.NpgsqlDbType.Varchar, 1000)
                arParms(16).Value = _SearchTxt

                arParms(17) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(17).Direction = ParameterDirection.InputOutput
                arParms(17).Value = _TotalRecords

                arParms(18) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(18).Value = Nothing
                arParms(18).Direction = ParameterDirection.InputOutput

                arParms(19) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(19).Value = Nothing
                arParms(19).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "usp_GetProspectsList1", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(17).Value)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetProspectsPortal() As DataTable
            Try


                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(16) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@CRMType", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(0).Value = _CRMType

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintUserRightType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _UserRightType

                arParms(3) = New Npgsql.NpgsqlParameter("@tintSortOrder", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _SortOrder

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(4).Value = DomainID

                arParms(5) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(5).Value = _SortCharacter

                arParms(6) = New Npgsql.NpgsqlParameter("@FirstName", NpgsqlTypes.NpgsqlDbType.Varchar, 100)
                arParms(6).Value = IIf(_FirstName = "", "", Replace(_FirstName, "'", "''"))

                arParms(7) = New Npgsql.NpgsqlParameter("@LastName", NpgsqlTypes.NpgsqlDbType.Varchar, 100)
                arParms(7).Value = IIf(_LastName = "", "", Replace(_LastName, "'", "''"))

                arParms(8) = New Npgsql.NpgsqlParameter("@CustName", NpgsqlTypes.NpgsqlDbType.Varchar, 100)
                arParms(8).Value = IIf(_CustName = "", "", Replace(_CustName, "'", "''"))

                arParms(9) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(9).Value = _CurrentPage

                arParms(10) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(10).Value = _PageSize

                arParms(11) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(11).Direction = ParameterDirection.InputOutput
                arParms(11).Value = _TotalRecords


                arParms(12) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.Varchar, 50)
                arParms(12).Value = _columnName

                arParms(13) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.Varchar, 10)
                arParms(13).Value = _columnSortOrder

                arParms(14) = New Npgsql.NpgsqlParameter("@numProfile", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(14).Value = _Profile

                arParms(15) = New Npgsql.NpgsqlParameter("@bitPartner", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(15).Value = _bitPartner

                arParms(16) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(16).Value = Nothing
                arParms(16).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "usp_GetProspectsList", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(11).Value)

                Dim dtTable As DataTable
                dtTable = ds.Tables(0)

                Return dtTable
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function GetCompanyList() As DataSet
            Try


                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(20) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numRelationType", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(0).Value = _RelType

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintUserRightType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _UserRightType

                arParms(3) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(3).Value = _SortCharacter

                arParms(4) = New Npgsql.NpgsqlParameter("@FirstName", NpgsqlTypes.NpgsqlDbType.Varchar, 100)
                arParms(4).Value = IIf(_FirstName = "", "", Replace(_FirstName, "'", "''"))

                arParms(5) = New Npgsql.NpgsqlParameter("@LastName", NpgsqlTypes.NpgsqlDbType.Varchar, 100)
                arParms(5).Value = IIf(_LastName = "", "", Replace(_LastName, "'", "''"))

                arParms(6) = New Npgsql.NpgsqlParameter("@CustName", NpgsqlTypes.NpgsqlDbType.Varchar, 100)
                arParms(6).Value = IIf(_CustName = "", "", Replace(_CustName, "'", "''"))

                arParms(7) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(7).Value = _CurrentPage

                arParms(8) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(8).Value = _PageSize

                arParms(9) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.Varchar, 50)
                arParms(9).Value = _columnName

                arParms(10) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.Varchar, 10)
                arParms(10).Value = _columnSortOrder

                arParms(11) = New Npgsql.NpgsqlParameter("@tintSortOrder", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(11).Value = _SortOrder

                arParms(12) = New Npgsql.NpgsqlParameter("@numProfile", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(12).Value = _Profile

                arParms(13) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(13).Value = DomainID

                arParms(14) = New Npgsql.NpgsqlParameter("@tintCRMType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(14).Value = _CRMType

                arParms(15) = New Npgsql.NpgsqlParameter("@bitPartner", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(15).Value = _bitPartner

                arParms(16) = New Npgsql.NpgsqlParameter("@numFormID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(16).Value = _FormId

                arParms(17) = New Npgsql.NpgsqlParameter("@vcRegularSearchCriteria", NpgsqlTypes.NpgsqlDbType.Varchar, 1000)
                arParms(17).Value = _RegularSearchCriteria

                arParms(18) = New Npgsql.NpgsqlParameter("@vcCustomSearchCriteria", NpgsqlTypes.NpgsqlDbType.Varchar, 1000)
                arParms(18).Value = _CustomSearchCriteria

                arParms(19) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(19).Value = Nothing
                arParms(19).Direction = ParameterDirection.InputOutput

                arParms(20) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(20).Value = Nothing
                arParms(20).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet

                Dim dtTable As DataTable

                ds = SqlDAL.ExecuteDataset(connString, "USP_CompanyList1", arParms)
                dtTable = ds.Tables(0)
                If dtTable.Rows.Count > 0 Then
                    _TotalRecords = CInt(dtTable.Rows(0).Item("TotalRowCount"))
                End If
                dtTable.Columns.Remove("TotalRowCount")
                ds.AcceptChanges()
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetAccountPerformance() As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numdivisionid", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_AccountsPerformance", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetCompanyPrimaryContact() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "usp_GetCompanyPrimaryContactDTl", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Function GetInitialPageDetails() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numFormID", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _FormId

                arParms(3) = New Npgsql.NpgsqlParameter("@numTabId", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(3).Value = 0

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "usp_GetInitialPageDetails", arParms)
                Return ds.Tables(0)

            Catch ex As Exception

                Throw ex
            End Try
        End Function
        Public Function UpdateFollowUpStatus() As Boolean
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = GetConnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@strAdvSerView", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _ContactID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _FollowUpStatus

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateFollowUpStatus", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetDivisionCreditBalanceHistory() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _DivisionID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintOppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _OppType

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetDivisionCreditBalanceHistory", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetDefaultSettingValue(ByVal vcDbColumnName As String) As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDivisionID", DivisionID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@vcDbColumnName", vcDbColumnName, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_DivisionMaster_GetDefaultSettingByFieldName", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function OrderImportGetCustomer(ByVal vcCompanyName As String, ByVal numMatchField As Long, ByVal vcMatchValue As String) As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@vcCompanyName", vcCompanyName, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@numMatchField", numMatchField, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@vcMatchValue", vcMatchValue, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_DivisionMaster_GetByCompanyNameAndMatchField", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetVendorDetail(ByVal itemCode As Long, ByVal attrValues As String) As DataSet
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numDivisionID", DivisionID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numItemCode", itemCode, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@vcAttrValues", attrValues, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDataset(connString, "USP_Vendor_GetDetail", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

    End Class
End Namespace

