''' -----------------------------------------------------------------------------
''' Project	 : BACRM.BusinessLogic
''' Class	 : Survey
''' 
''' -----------------------------------------------------------------------------
''' <summary>
'''     This class contains several methods and functions, which is used to 
'''     Managing the Surveys.
''' </summary>
''' <history>
''' 	[Debasish Tapan Nag]	09/13/2005	Created
''' </history>
''' -----------------------------------------------------------------------------
Option Explicit On
Imports System.Data.SqlClient
Imports System.IO
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic

Namespace BACRM.BusinessLogic.Survey
    Public Class SurveyAdministration
        Inherits BACRM.BusinessLogic.CBusinessBase
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Survey Id.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Survey Id which is being edited/ deleted
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/13/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _numSurId As Long

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Manages the value of the Survey Id 
        ''' </summary>
        ''' <remarks>
        '''     Manages the Survey Id
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/13/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property SurveyId() As Long
            Get
                Return _numSurId
            End Get
            Set(ByVal Value As Long)
                _numSurId = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Survey Name.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Survey Name which is being edited/ added
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/17/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _vcSurName As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Manages the value of the Survey Name 
        ''' </summary>
        ''' <remarks>
        '''     Manages the Survey Name
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/17/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property SurveyName() As String
            Get
                Return _vcSurName
            End Get
            Set(ByVal Value As String)
                _vcSurName = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Redirection URL.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Redirection URL which is being edited/ added
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/17/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _vcSurRedirectionURL As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Manages the value of the Redirection URL
        ''' </summary>
        ''' <remarks>
        '''     Manages the Redirection URL
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/17/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property RedirectionURL() As String
            Get
                Return _vcSurRedirectionURL
            End Get
            Set(ByVal Value As String)
                _vcSurRedirectionURL = Value
            End Set
        End Property
        Private _vcSurveyRedirectURL As String
        Public Property SurveyRedirectURL() As String
            Get
                Return _vcSurveyRedirectURL
            End Get
            Set(ByVal Value As String)
                _vcSurveyRedirectURL = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Skip Registration flag.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Skip Registration flag which is being edited/ added
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/17/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _SkipRegistration As Byte
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Manages the value of the Skip Registration flag
        ''' </summary>
        ''' <remarks>
        '''     Manages the Skip Registration flag
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/17/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property SkipRegistration() As Byte
            Get
                Return _SkipRegistration
            End Get
            Set(ByVal Value As Byte)
                _SkipRegistration = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Random Answers flag.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Random Answers flag which is being edited/ added
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/17/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _RandomAnswers As Byte
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Manages the value of the Random Answers flag
        ''' </summary>
        ''' <remarks>
        '''     Manages the Random Answers flag
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/17/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property RandomAnswers() As Byte
            Get
                Return _RandomAnswers
            End Get
            Set(ByVal Value As Byte)
                _RandomAnswers = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Single Page Survey flag.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Single Page Survey flag which is being edited/ added
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/17/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _SinglePageSurvey As Byte
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Manages the value of the Single Page Survey flag
        ''' </summary>
        ''' <remarks>
        '''     Manages the Single Page Survey flag
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/17/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property SinglePageSurvey() As Byte
            Get
                Return _SinglePageSurvey
            End Get
            Set(ByVal Value As Byte)
                _SinglePageSurvey = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the if the registration is required after the survey is over or not.
        ''' </summary>
        ''' <remarks>
        '''     This holds the flag which indicates that registration is required after the survey
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	10/02/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _PostSurveyRegistration As Byte
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Manages the value of the flag
        ''' </summary>
        ''' <remarks>
        '''     Manages the Registration position Survey flag
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	10/02/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property PostSurveyRegistration() As Byte
            Get
                Return _PostSurveyRegistration
            End Get
            Set(ByVal Value As Byte)
                _PostSurveyRegistration = Value
            End Set
        End Property

        Private _RelationshipProfile As Byte
        Public Property RelationshipProfile() As Byte
            Get
                Return _RelationshipProfile
            End Get
            Set(ByVal Value As Byte)
                _RelationshipProfile = Value
            End Set
        End Property

        Private _SurveyRedirect As Byte
        Public Property SurveyRedirect() As Byte
            Get
                Return _SurveyRedirect
            End Get
            Set(ByVal Value As Byte)
                _SurveyRedirect = Value
            End Set
        End Property

        Private _EmailTemplateContact As Byte
        Public Property EmailTemplateContact() As Byte
            Get
                Return _EmailTemplateContact
            End Get
            Set(ByVal Value As Byte)
                _EmailTemplateContact = Value
            End Set
        End Property

        Private _EmailTemplateRecordOwner As Byte
        Public Property EmailTemplateRecordOwner() As Byte
            Get
                Return _EmailTemplateRecordOwner
            End Get
            Set(ByVal Value As Byte)
                _EmailTemplateRecordOwner = Value
            End Set
        End Property
        Private _LandingPage As Byte
        Public Property LandingPage() As Byte
            Get
                Return _LandingPage
            End Get
            Set(ByVal Value As Byte)
                _LandingPage = Value
            End Set
        End Property

        Private _vcLandingPageYes As String
        Public Property LandingPageYes() As String
            Get
                Return _vcLandingPageYes
            End Get
            Set(ByVal Value As String)
                _vcLandingPageYes = Value
            End Set
        End Property
        Private _vcLandingPageNo As String
        Public Property LandingPageNo() As String
            Get
                Return _vcLandingPageNo
            End Get
            Set(ByVal Value As String)
                _vcLandingPageNo = Value
            End Set
        End Property
        Private _CreateRecord As Byte
        Public Property CreateRecord() As Byte
            Get
                Return _CreateRecord
            End Get
            Set(ByVal Value As Byte)
                _CreateRecord = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Question Id Id.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Question Id which is being edited/ deleted
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/15/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _numQId As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Manages the value of the Question Id 
        ''' </summary>
        ''' <remarks>
        '''     Manages the Question Id
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/15/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property QuestionId() As Long
            Get
                Return _numQId
            End Get
            Set(ByVal Value As Long)
                _numQId = Value
            End Set
        End Property

        Private _numMatrixID As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Manages the value of the Question Id 
        ''' </summary>
        ''' <remarks>
        '''     Manages the Question Id
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/15/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property MatrixID() As Long
            Get
                Return _numMatrixID
            End Get
            Set(ByVal Value As Long)
                _numMatrixID = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Answer Id Id.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Answer Id which is being edited/ deleted
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/15/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _numAnsId As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Manages the value of the Answer Id 
        ''' </summary>
        ''' <remarks>
        '''     Manages the Answer Id
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/15/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property AnswerId() As Long
            Get
                Return _numAnsId
            End Get
            Set(ByVal Value As Long)
                _numAnsId = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the User Id.
        ''' </summary>
        ''' <remarks>
        '''     This holds the User Id of the current User
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/13/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _numUserId As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Manages the value of the user Id 
        ''' </summary>
        ''' <remarks>
        '''     Manages the User Id
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/13/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property UserId() As Long
            Get
                Return _numUserId
            End Get
            Set(ByVal Value As Long)
                _numUserId = Value
            End Set
        End Property

        Private _numRelationShipId As Long
        Public Property RelationShipId() As Long
            Get
                Return _numRelationShipId
            End Get
            Set(ByVal Value As Long)
                _numRelationShipId = Value
            End Set
        End Property
        Private _numCRMType As Integer
        Public Property CRMType() As Integer
            Get
                Return _numCRMType
            End Get
            Set(ByVal Value As Integer)
                _numCRMType = Value
            End Set
        End Property

        Private _numGrpId As Long
        Public Property GrpId() As Long
            Get
                Return _numGrpId
            End Get
            Set(ByVal Value As Long)
                _numGrpId = Value
            End Set
        End Property
        Private _numRecOwner As Long
        Public Property RecOwner() As Long
            Get
                Return _numRecOwner
            End Get
            Set(ByVal Value As Long)
                _numRecOwner = Value
            End Set
        End Property
        Private _numEmailTemplate1Id As Long
        Public Property EmailTemplate1Id() As Long
            Get
                Return _numEmailTemplate1Id
            End Get
            Set(ByVal Value As Long)
                _numEmailTemplate1Id = Value
            End Set
        End Property
        Private _numEmailTemplate2Id As Long
        Public Property EmailTemplate2Id() As Long
            Get
                Return _numEmailTemplate2Id
            End Get
            Set(ByVal Value As Long)
                _numEmailTemplate2Id = Value
            End Set
        End Property
        Private _numProfileId As Long
        Public Property ProfileId() As Long
            Get
                Return _numProfileId
            End Get
            Set(ByVal Value As Long)
                _numProfileId = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Contact Id.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Contact Id of the current User
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	10/02/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _numContactId As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Manages the value of the Contact Id 
        ''' </summary>
        ''' <remarks>
        '''     Manages the Contact Id
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	10/02/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property ContactId() As Long
            Get
                Return _numContactId
            End Get
            Set(ByVal Value As Long)
                _numContactId = Value
            End Set
        End Property

        Private _tIntPageType As Integer
        Public Property PageType() As Int16
            Get
                Return _tIntPageType
            End Get
            Set(ByVal Value As Int16)
                _tIntPageType = Value
            End Set
        End Property

        Private _strText As String = String.Empty
        Public Property strText() As String
            Get
                Return _strText
            End Get
            Set(ByVal value As String)
                _strText = value
            End Set
        End Property

        Private _StrItems As String = String.Empty
        Public Property StrItems() As String
            Get
                Return _StrItems
            End Get
            Set(ByVal value As String)
                _StrItems = value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the xml file path to the saved survey information file.
        ''' </summary>
        ''' <remarks>
        '''     This holds the filepath.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/14/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _XMLFilePath As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the XML File Path which stores the survey information temporarily.
        ''' </summary>
        ''' <value>Returns the XMLFilePath as String.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the XMLFilePath.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/14/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property XMLFilePath() As String
            Get
                Return _XMLFilePath
            End Get
            Set(ByVal Value As String)
                _XMLFilePath = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the dataset.
        ''' </summary>
        ''' <remarks>
        '''     This holds the dataset of survey information.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/14/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _SurveyInfo As DataSet
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the dataset of survey information temporarily.
        ''' </summary>
        ''' <value>Returns the complete Dataset.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the Dataset.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/14/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property SurveyInfo() As DataSet
            Get
                Return _SurveyInfo
            End Get
            Set(ByVal Value As DataSet)
                _SurveyInfo = Value
            End Set
        End Property
        Public Class SurveyExecution
            Public Shared objSurveyExecution As SurveyExecution                 'Create an object and return the object
            Private Shared blnFlag As Boolean                                   'Shared Flag to check for singleton

            ''' -----------------------------------------------------------------------------
            ''' <summary>
            '''     Create a new method so the object cannot be created outside of the class.
            ''' </summary>
            ''' <remarks>
            ''' </remarks>
            ''' <history>
            ''' 	[Debasish Tapan Nag]	09/21/2005	Created
            ''' </history>
            ''' -----------------------------------------------------------------------------
            Private Sub New()

            End Sub

            'this function will check to see if the object has been called before
            'if is hasn;t then we'll create an instance, if it has then the existing
            'object in memory will be returned.
            Shared Function SurveyExecution() As SurveyExecution
                If Not blnFlag Then
                    objSurveyExecution = New SurveyExecution
                    blnFlag = True
                    Return objSurveyExecution
                Else
                    Return objSurveyExecution
                End If
            End Function
            ''' -----------------------------------------------------------------------------
            ''' <summary>
            '''     Represents the Current RowIndex of the Question DataRow.
            ''' </summary>
            ''' <remarks>
            '''     This holds the Current RowIndex of the Question DataRow.
            ''' </remarks>
            ''' <history>
            ''' 	[Debasish Tapan Nag]	09/21/2005	Created
            ''' </history>
            ''' -----------------------------------------------------------------------------
            Private _numCurrentQuestionDataRow As Integer
            ''' -----------------------------------------------------------------------------
            ''' <summary>
            '''     This Sets and Gets the Current RowIndex of the Question DataRow.
            ''' </summary>
            ''' <value>Returns the Current RowIndex of the Question DataRow as Integer.</value>
            ''' <remarks>
            '''     This property is used to Set and Get the Current RowIndex of the Question DataRow.
            ''' </remarks>
            ''' <history>
            ''' 	[Debasish Tapan Nag]	09/21/2005	Created
            ''' </history>
            ''' -----------------------------------------------------------------------------
            Public Property CurrentQuestionDataRow() As Integer
                Get
                    Return _numCurrentQuestionDataRow
                End Get
                Set(ByVal Value As Integer)
                    _numCurrentQuestionDataRow = Value
                End Set
            End Property
            ''' -----------------------------------------------------------------------------
            ''' <summary>
            '''     Represents the Number Of Questions.
            ''' </summary>
            ''' <remarks>
            '''     This holds the Number Of Questions.
            ''' </remarks>
            ''' <history>
            ''' 	[Debasish Tapan Nag]	09/21/2005	Created
            ''' </history>
            ''' -----------------------------------------------------------------------------
            Private _numQuestionTable As Integer
            ''' -----------------------------------------------------------------------------
            ''' <summary>
            '''     This Sets and Gets the Number Of Questions.
            ''' </summary>
            ''' <value>Returns the Number Of Questions as Integer.</value>
            ''' <remarks>
            '''     This property is used to Set and Get the Number Of Questions.
            ''' </remarks>
            ''' <history>
            ''' 	[Debasish Tapan Nag]	09/21/2005	Created
            ''' </history>
            ''' -----------------------------------------------------------------------------
            Public Property NumberOfQuestions() As Integer
                Get
                    Return _numQuestionTable
                End Get
                Set(ByVal Value As Integer)
                    _numQuestionTable = Value
                End Set
            End Property
            ''' -----------------------------------------------------------------------------
            ''' <summary>
            '''     Represents the Respondent ID.
            ''' </summary>
            ''' <remarks>
            '''     This holds the Respondent ID.
            ''' </remarks>
            ''' <history>
            ''' 	[Debasish Tapan Nag]	09/23/2005	Created
            ''' </history>
            ''' -----------------------------------------------------------------------------
            Private _numRespondentID As Integer
            ''' -----------------------------------------------------------------------------
            ''' <summary>
            '''     This Sets and Gets the Respondent ID.
            ''' </summary>
            ''' <value>Returns the Respondent ID.</value>
            ''' <remarks>
            '''     This property is used to Set and Get the Respondent ID.
            ''' </remarks>
            ''' <history>
            ''' 	[Debasish Tapan Nag]	09/23/2005	Created
            ''' </history>
            ''' -----------------------------------------------------------------------------
            Public Property RespondentID() As Integer
                Get
                    Return _numRespondentID
                End Get
                Set(ByVal Value As Integer)
                    _numRespondentID = Value
                End Set
            End Property
            ''' -----------------------------------------------------------------------------
            ''' <summary>
            '''     Represents the Survey Rating.
            ''' </summary>
            ''' <remarks>
            '''     This holds the Survey Rating.
            ''' </remarks>
            ''' <history>
            ''' 	[Debasish Tapan Nag]	09/23/2005	Created
            ''' </history>
            ''' -----------------------------------------------------------------------------
            Private _numSurveyRating As Integer
            ''' -----------------------------------------------------------------------------
            ''' <summary>
            '''     This Sets and Gets the Survey Rating.
            ''' </summary>
            ''' <value>Returns the Survey Rating.</value>
            ''' <remarks>
            '''     This property is used to Set and Get the SurveyRating.
            ''' </remarks>
            ''' <history>
            ''' 	[Debasish Tapan Nag]	09/23/2005	Created
            ''' </history>
            ''' -----------------------------------------------------------------------------
            Public Property SurveyRating() As Integer
                Get
                    Return _numSurveyRating
                End Get
                Set(ByVal Value As Integer)
                    _numSurveyRating = Value
                End Set
            End Property
            ''' -----------------------------------------------------------------------------
            ''' <summary>
            '''     Represents the Group ID.
            ''' </summary>
            ''' <remarks>
            '''     This holds the Group ID.
            ''' </remarks>
            ''' <history>
            ''' 	[Debasish Tapan Nag]	09/23/2005	Created
            ''' </history>
            ''' -----------------------------------------------------------------------------
            Private _numGroupID As Integer
            ''' -----------------------------------------------------------------------------
            ''' <summary>
            '''     This Sets and Gets the Group ID.
            ''' </summary>
            ''' <value>Returns the Group ID.</value>
            ''' <remarks>
            '''     This property is used to Set and Get the Group ID.
            ''' </remarks>
            ''' <history>
            ''' 	[Debasish Tapan Nag]	09/23/2005	Created
            ''' </history>
            ''' -----------------------------------------------------------------------------
            Public Property GroupID() As Integer
                Get
                    Return _numGroupID
                End Get
                Set(ByVal Value As Integer)
                    _numGroupID = Value
                End Set
            End Property
            ''' -----------------------------------------------------------------------------
            ''' <summary>
            '''     Represents the Record Owner ID.
            ''' </summary>
            ''' <remarks>
            '''     This holds the Record Owner ID.
            ''' </remarks>
            ''' <history>
            ''' 	[Debasish Tapan Nag]	09/23/2005	Created
            ''' </history>
            ''' -----------------------------------------------------------------------------
            Private _numRecOwnerID As Integer
            ''' -----------------------------------------------------------------------------
            ''' <summary>
            '''     This Sets and Gets the Record Owner ID.
            ''' </summary>
            ''' <value>Returns the Record Owner ID.</value>
            ''' <remarks>
            '''     This property is used to Set and Get the Record Owner ID.
            ''' </remarks>
            ''' <history>
            ''' 	[Debasish Tapan Nag]	09/23/2005	Created
            ''' </history>
            ''' -----------------------------------------------------------------------------
            Public Property RecOwnerID() As Integer
                Get
                    Return _numRecOwnerID
                End Get
                Set(ByVal Value As Integer)
                    _numRecOwnerID = Value
                End Set
            End Property
            ''' -----------------------------------------------------------------------------
            ''' <summary>
            '''     Represents the RelationShip ID.
            ''' </summary>
            ''' <remarks>
            '''     This holds the RelationShip ID.
            ''' </remarks>
            ''' <history>
            ''' 	[Debasish Tapan Nag]	09/23/2005	Created
            ''' </history>
            ''' -----------------------------------------------------------------------------
            Private _numCompanyTypeID As Integer
            ''' -----------------------------------------------------------------------------
            ''' <summary>
            '''     This Sets and Gets the Record RelationShip ID.
            ''' </summary>
            ''' <value>Returns the RelationShip ID.</value>
            ''' <remarks>
            '''     This property is used to Set and Get the RelationShip ID.
            ''' </remarks>
            ''' <history>
            ''' 	[Debasish Tapan Nag]	09/23/2005	Created
            ''' </history>
            ''' -----------------------------------------------------------------------------
            Public Property RelationShipID() As Integer
                Get
                    Return _numCompanyTypeID
                End Get
                Set(ByVal Value As Integer)
                    _numCompanyTypeID = Value
                End Set
            End Property
            ''' -----------------------------------------------------------------------------
            ''' <summary>
            '''     Represents the CRM Type.
            ''' </summary>
            ''' <remarks>
            '''     This holds the CRM Type.
            ''' </remarks>
            ''' <history>
            ''' 	[Debasish Tapan Nag]	09/23/2005	Created
            ''' </history>
            ''' -----------------------------------------------------------------------------
            Private _tIntCrmType As Integer
            ''' -----------------------------------------------------------------------------
            ''' <summary>
            '''     This Sets and Gets the CRM Type.
            ''' </summary>
            ''' <value>Returns the CRM Type.</value>
            ''' <remarks>
            '''     This property is used to Set and Get the CRM Type.
            ''' </remarks>
            ''' <history>
            ''' 	[Debasish Tapan Nag]	09/23/2005	Created
            ''' </history>
            ''' -----------------------------------------------------------------------------
            Public Property CRMType() As Int16
                Get
                    Return _tIntCrmType
                End Get
                Set(ByVal Value As Int16)
                    _tIntCrmType = Value
                End Set
            End Property

        End Class


        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the survey list from master database.
        ''' </summary>
        ''' <remarks>
        '''     This function calls the stored procedure which returns the list of survey
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function getSurveyList() As DataTable
            Try
                Dim ds As DataSet                                                                   'declare a dataset
                Dim getconnection As New GetConnection                                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString                        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetSurveyList", arParms)                'execute the stored procedure
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the survey's master data.
        ''' </summary>
        ''' <remarks>
        '''     This function calls the stored procedure which returns the master data related to the survey
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/13/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function getSurveyMasterData() As DataTable
            Try
                Dim ds As DataSet                                                                   'declare a dataset
                Dim getconnection As New GetConnection                                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString                        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numSurID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = SurveyId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetSurveyMasterInfo", arParms)  'execute the stored procedure
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to save the survey information temporarily
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/14/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function SaveSurveyInformationTemp()
            Try
                If Directory.Exists(XMLFilePath) = False Then                                   'If Folder Does not exists create New Folder.
                    Directory.CreateDirectory(XMLFilePath)
                End If
                Dim xmlOut As New System.IO.FileStream(XMLFilePath & "\" & "TempSurveyInformation_" & DomainId & "_" & SurveyId & "_" & UserId & ".xml", System.IO.FileMode.Create) 'Create A file system object
                Dim myXmlWriter As New System.Xml.XmlTextWriter(xmlOut, System.Text.Encoding.Unicode)   'create a xml writer
                SurveyInfo.WriteXml(myXmlWriter)                                                    'Write teh xml from teh dataser
                myXmlWriter.Close()                                                                     'Close the writer
            Catch ex As Exception
                Throw ex
                'Appropriate permission not given on the folder
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method adds the Survey Master Inforamtion to the dataset
        ''' </summary>
        ''' <remarks>
        '''     Sets the Survey Master Information as a DataSet
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/17/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Sub AddSurveyMasterInformationTable()
            Dim dtSurveyMasterInformationTable As New DataTable
            dtSurveyMasterInformationTable.Columns.Add(New DataColumn("numSurID", System.Type.GetType("System.String"))) 'Add a column to store the Survey Id
            dtSurveyMasterInformationTable.Columns.Add(New DataColumn("vcSurName", System.Type.GetType("System.String"))) 'Add a column to store the Survey Name
            dtSurveyMasterInformationTable.Columns.Add(New DataColumn("vcRedirectURL", System.Type.GetType("System.String"))) 'Add a column to store the Survey Redirection URL
            dtSurveyMasterInformationTable.Columns.Add(New DataColumn("bitSkipRegistration", System.Type.GetType("System.String"))) 'Add a column to store the flag if a survey registration is to be skipped
            dtSurveyMasterInformationTable.Columns.Add(New DataColumn("bitRandomAnswers", System.Type.GetType("System.String"))) 'Add a column to store the Flag to indicate that the answers are to be arranged randomly
            dtSurveyMasterInformationTable.Columns.Add(New DataColumn("bitSinglePageSurvey", System.Type.GetType("System.String"))) 'Add a column to store the Flag to indicate that the survey has to be conducted on a single page
            dtSurveyMasterInformationTable.Columns.Add(New DataColumn("bitPostRegistration", System.Type.GetType("System.String"))) 'Add a column to store the Flag to indicate that the survey has post registration

            dtSurveyMasterInformationTable.Columns.Add(New DataColumn("bitRelationshipProfile", System.Type.GetType("System.String")))
            dtSurveyMasterInformationTable.Columns.Add(New DataColumn("bitSurveyRedirect", System.Type.GetType("System.String")))
            dtSurveyMasterInformationTable.Columns.Add(New DataColumn("bitEmailTemplateContact", System.Type.GetType("System.String")))
            dtSurveyMasterInformationTable.Columns.Add(New DataColumn("bitEmailTemplateRecordOwner", System.Type.GetType("System.String")))
            dtSurveyMasterInformationTable.Columns.Add(New DataColumn("bitLandingPage", System.Type.GetType("System.String")))
            dtSurveyMasterInformationTable.Columns.Add(New DataColumn("vcLandingPageYes", System.Type.GetType("System.String")))
            dtSurveyMasterInformationTable.Columns.Add(New DataColumn("vcLandingPageNo", System.Type.GetType("System.String")))
            dtSurveyMasterInformationTable.Columns.Add(New DataColumn("bitCreateRecord", System.Type.GetType("System.String")))

            dtSurveyMasterInformationTable.Columns.Add(New DataColumn("numRelationShipId", System.Type.GetType("System.String")))

            dtSurveyMasterInformationTable.Columns.Add(New DataColumn("tIntCRMType", System.Type.GetType("System.String")))
            dtSurveyMasterInformationTable.Columns.Add(New DataColumn("numGrpId", System.Type.GetType("System.String")))
            dtSurveyMasterInformationTable.Columns.Add(New DataColumn("numRecOwner", System.Type.GetType("System.String")))

            dtSurveyMasterInformationTable.Columns.Add(New DataColumn("numProfileId", System.Type.GetType("System.String")))
            dtSurveyMasterInformationTable.Columns.Add(New DataColumn("numEmailTemplate1Id", System.Type.GetType("System.String")))
            dtSurveyMasterInformationTable.Columns.Add(New DataColumn("numEmailTemplate2Id", System.Type.GetType("System.String")))
            dtSurveyMasterInformationTable.Columns.Add(New DataColumn("vcSurveyRedirect", System.Type.GetType("System.String"))) 'Add a column to store the Survey Redirection URL

            dtSurveyMasterInformationTable.TableName = "SurveyMaster"                                   'Set the name of the Table

            Dim dtSurveyMasterInformationTableRow As DataRow                                            'Declare a DataTable row
            dtSurveyMasterInformationTableRow = dtSurveyMasterInformationTable.NewRow                   'Make provision of a new row
            dtSurveyMasterInformationTableRow.Item("numSurID") = SurveyId                               'Set the Survey Id
            dtSurveyMasterInformationTableRow.Item("vcSurName") = SurveyName                            'Set the Survey Name
            dtSurveyMasterInformationTableRow.Item("vcRedirectURL") = RedirectionURL                    'Set the Survey Redirection URL
            dtSurveyMasterInformationTableRow.Item("bitSkipRegistration") = SkipRegistration            'Set the Survey Skip Registration Flag
            dtSurveyMasterInformationTableRow.Item("bitRandomAnswers") = RandomAnswers                  'Set the Survey Random Answers Flag
            dtSurveyMasterInformationTableRow.Item("bitSinglePageSurvey") = SinglePageSurvey            'Set the Survey Single Page Flag
            dtSurveyMasterInformationTableRow.Item("bitPostRegistration") = PostSurveyRegistration     'Set the Survey Single Page Flag

            dtSurveyMasterInformationTableRow.Item("bitRelationshipProfile") = RelationshipProfile
            dtSurveyMasterInformationTableRow.Item("bitSurveyRedirect") = SurveyRedirect
            dtSurveyMasterInformationTableRow.Item("bitEmailTemplateContact") = EmailTemplateContact
            dtSurveyMasterInformationTableRow.Item("bitEmailTemplateRecordOwner") = EmailTemplateRecordOwner
            dtSurveyMasterInformationTableRow.Item("bitLandingPage") = LandingPage
            dtSurveyMasterInformationTableRow.Item("bitCreateRecord") = CreateRecord

            dtSurveyMasterInformationTableRow.Item("numRelationShipId") = RelationShipId
            dtSurveyMasterInformationTableRow.Item("numProfileId") = ProfileId
            dtSurveyMasterInformationTableRow.Item("tIntCRMType") = CRMType
            dtSurveyMasterInformationTableRow.Item("numGrpId") = GrpId
            dtSurveyMasterInformationTableRow.Item("numRecOwner") = RecOwner

            dtSurveyMasterInformationTableRow.Item("numEmailTemplate1Id") = EmailTemplate1Id
            dtSurveyMasterInformationTableRow.Item("numEmailTemplate2Id") = EmailTemplate2Id

            dtSurveyMasterInformationTableRow.Item("vcLandingPageYes") = LandingPageYes
            dtSurveyMasterInformationTableRow.Item("vcLandingPageNo") = LandingPageNo
            dtSurveyMasterInformationTableRow.Item("vcSurveyRedirect") = SurveyRedirectURL

            dtSurveyMasterInformationTable.Rows.Add(dtSurveyMasterInformationTableRow)                  'Add the row to teh same datatable

            SurveyInfo.Tables.Add(dtSurveyMasterInformationTable)                                       'Add the table to the same dataset in which all the ionformation related to teh survey is stored
        End Sub
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to get the survey information temporarily
        ''' </summary>
        ''' <remarks>
        '''     Returns the Survey Information as a DataSet
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/14/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function GetSurveyInformationTemp() As DataSet
            Try
                Dim dsSurveyInformation As New DataSet                                                  'Create an instance of a dataset
                If SurveyId > 0 And File.Exists(XMLFilePath & "\" & "TempSurveyInformation_" & DomainId & "_" & SurveyId & "_" & UserId & ".xml") = False Then
                    dsSurveyInformation = getSurveyInformation()                                        'Get the Survey Information from the database
                    dsSurveyInformation.Tables(0).TableName = "SurveyQuestionMaster"                    'Set the name of the table
                    dsSurveyInformation.Tables(1).TableName = "SurveyAnsMaster"                         'Set the name of the table
                    dsSurveyInformation.Tables(2).TableName = "SurveyWorkflowRules"                     'Set the name of the table
                    dsSurveyInformation.Tables(3).TableName = "SurveyMatrixMaster"                     'Set the name of the table
                    dsSurveyInformation.Tables(4).TableName = "SurveyCreateRecord"                     'Set the name of the table

                    If Not dsSurveyInformation.Tables(2).Columns.Contains("numLinkedSurID") Then
                        dsSurveyInformation.Tables(2).Columns.Add(New DataColumn("numLinkedSurID", System.Type.GetType("System.String"))) 'Add a column to store the Linked Question IDs
                    End If
                    If Not dsSurveyInformation.Tables(2).Columns.Contains("numResponseRating") Then
                        dsSurveyInformation.Tables(2).Columns.Add(New DataColumn("numResponseRating", System.Type.GetType("System.String"))) 'Add a column to store the Rating given to this rule
                    End If
                    If Not dsSurveyInformation.Tables(2).Columns.Contains("numGrpId") Then
                        dsSurveyInformation.Tables(2).Columns.Add(New DataColumn("numGrpId", System.Type.GetType("System.String"))) 'Add a column to store the Group
                    End If
                    If Not dsSurveyInformation.Tables(2).Columns.Contains("numCompanyType") Then
                        dsSurveyInformation.Tables(2).Columns.Add(New DataColumn("numCompanyType", System.Type.GetType("System.String"))) 'Add a column to store the RelationShip
                    End If
                    If Not dsSurveyInformation.Tables(2).Columns.Contains("tIntCRMType") Then
                        dsSurveyInformation.Tables(2).Columns.Add(New DataColumn("tIntCRMType", System.Type.GetType("System.String"))) 'Add a column to store the CRM Type
                    End If
                    If Not dsSurveyInformation.Tables(2).Columns.Contains("numRecOwner") Then
                        dsSurveyInformation.Tables(2).Columns.Add(New DataColumn("numRecOwner", System.Type.GetType("System.String"))) 'Add a column to store the Record Owner
                    End If

                    If Not dsSurveyInformation.Tables(2).Columns.Contains("tIntRuleFourRadio") Then
                        dsSurveyInformation.Tables(2).Columns.Add(New DataColumn("tIntRuleFourRadio", System.Type.GetType("System.String"))) 'Add a column to store the Record Owner
                    End If
                    If Not dsSurveyInformation.Tables(2).Columns.Contains("tIntCreateNewID") Then
                        dsSurveyInformation.Tables(2).Columns.Add(New DataColumn("tIntCreateNewID", System.Type.GetType("System.String"))) 'Add a column to store the Record Owner
                    End If
                    If Not dsSurveyInformation.Tables(2).Columns.Contains("vcRuleFourSelectFields") Then
                        dsSurveyInformation.Tables(2).Columns.Add(New DataColumn("vcRuleFourSelectFields", System.Type.GetType("System.String"))) 'Add a column to store the Record Owner
                    End If
                    If Not dsSurveyInformation.Tables(2).Columns.Contains("boolPopulateSalesOpportunity") Then
                        dsSurveyInformation.Tables(2).Columns.Add(New DataColumn("boolPopulateSalesOpportunity", System.Type.GetType("System.String"))) 'Add a column to store the Record Owner
                    End If
                    If Not dsSurveyInformation.Tables(2).Columns.Contains("vcRuleFiveSelectFields") Then
                        dsSurveyInformation.Tables(2).Columns.Add(New DataColumn("vcRuleFiveSelectFields", System.Type.GetType("System.String"))) 'Add a column to store the Record Owner
                    End If
                    If Not dsSurveyInformation.Tables(2).Columns.Contains("vcItem") Then
                        dsSurveyInformation.Tables(2).Columns.Add(New DataColumn("vcItem", System.Type.GetType("System.String"))) 'Add a column to store the Record Owner
                    End If
                    If Not dsSurveyInformation.Tables(2).Columns.Contains("numEventOrder") Then
                        dsSurveyInformation.Tables(2).Columns.Add(New DataColumn("numEventOrder", System.Type.GetType("System.String")))
                    End If
                    SurveyInfo = dsSurveyInformation                                                    'Set the survey information in class variable
                    SaveSurveyInformationTemp()                                                         'Call to save the existing data temporarily

                    dsSurveyInformation.Tables(0).TableName = "SurveyQuestionMaster"                    'Set the name of the table
                    dsSurveyInformation.Tables(1).TableName = "SurveyAnsMaster"                         'Set the name of the table
                    dsSurveyInformation.Tables(2).TableName = "SurveyWorkflowRules"                     'Set the name of the table
                    dsSurveyInformation.Tables(3).TableName = "SurveyMatrixMaster"                     'Set the name of the table
                    dsSurveyInformation.Tables(4).TableName = "SurveyCreateRecord"                     'Set the name of the table
                Else
                    If File.Exists(XMLFilePath & "\" & "TempSurveyInformation_" & DomainId & "_" & SurveyId & "_" & UserId & ".xml") = False Then 'If file not exists exit
                        Dim dtTable As New DataTable                                                    'create a new datatable
                        dtTable.Columns.Add(New DataColumn("numSurID", System.Type.GetType("System.String"))) 'Add a column to store the Survey Id
                        dtTable.Columns.Add(New DataColumn("numQID", System.Type.GetType("System.String"))) 'Add a column to store the Question Id
                        dtTable.Columns.Add(New DataColumn("vcQuestion", System.Type.GetType("System.String"))) 'Add a column to store the Question
                        dtTable.Columns.Add(New DataColumn("tIntAnsType", System.Type.GetType("System.String"))) 'Add a column to store the Answer Type
                        dtTable.Columns.Add(New DataColumn("intNumOfAns", System.Type.GetType("System.String"))) 'Add a column to store the Nos of answers
                        dtTable.Columns.Add(New DataColumn("boolDeleted", System.Type.GetType("System.String"))) 'Add a column to store the answer type
                        dtTable.Columns.Add(New DataColumn("boolMatrixColumn", System.Type.GetType("System.String"))) 'Add a column to store the answer type
                        dtTable.Columns.Add(New DataColumn("intNumOfMatrixColumn", System.Type.GetType("System.String"))) 'Add a column to store the answer type

                        dsSurveyInformation.Tables.Add(dtTable)

                        dsSurveyInformation.Tables.Add(GetEmptySurveyAnsInformationTemp())              'Add the Answer table
                        dsSurveyInformation.Tables.Add(GetEmptySurveyWorkFlowInformationTemp())              'Add the WorkFlow Rules table
                        dsSurveyInformation.Tables.Add(GetEmptySurveyMatrixColumnInformationTemp())              'Add the WorkFlow Rules table
                        dsSurveyInformation.Tables.Add(GetEmptySurveyCreateRecordInformationTemp())              'Add the WorkFlow Rules table

                        dsSurveyInformation.Tables(0).TableName = "SurveyQuestionMaster"                    'Set the name of the table
                        dsSurveyInformation.Tables(1).TableName = "SurveyAnsMaster"                         'Set the name of the table
                        dsSurveyInformation.Tables(2).TableName = "SurveyWorkflowRules"                     'Set the name of the table
                        dsSurveyInformation.Tables(3).TableName = "SurveyMatrixMaster"                     'Set the name of the table
                        dsSurveyInformation.Tables(4).TableName = "SurveyCreateRecord"                     'Set the name of the table

                    ElseIf File.Exists(XMLFilePath & "\" & "TempSurveyInformation_" & DomainId & "_" & SurveyId & "_" & UserId & ".xml") = True Then
                        Try
                            dsSurveyInformation.ReadXml(XMLFilePath & "\" & "TempSurveyInformation_" & DomainId & "_" & SurveyId & "_" & UserId & ".xml")                                    'Read DataSet into XML
                            'Blank tables are not stored in the dataset so this workaround
                            If dsSurveyInformation.Tables.Count = 1 Then
                                dsSurveyInformation.Tables.Add(GetEmptySurveyAnsInformationTemp())              'Add the Answer table
                                dsSurveyInformation.Tables.Add(GetEmptySurveyWorkFlowInformationTemp())         'Add the WorkFlow Rules table
                                dsSurveyInformation.Tables.Add(GetEmptySurveyMatrixColumnInformationTemp())         'Add the WorkFlow Rules table
                                dsSurveyInformation.Tables.Add(GetEmptySurveyCreateRecordInformationTemp())              'Add the WorkFlow Rules table

                                dsSurveyInformation.Tables(2).TableName = "SurveyWorkflowRules"                     'Set the name of the table
                                dsSurveyInformation.Tables(3).TableName = "SurveyMatrixMaster"                     'Set the name of the table
                                dsSurveyInformation.Tables(4).TableName = "SurveyCreateRecord"                     'Set the name of the table
                                dsSurveyInformation.Tables(1).TableName = "SurveyAnsMaster"                         'Set the name of the table
                            Else
                                If dsSurveyInformation.Tables("SurveyQuestionMaster") Is Nothing Then

                                    Dim dtTable As New DataTable                                                    'create a new datatable
                                    dtTable.Columns.Add(New DataColumn("numSurID", System.Type.GetType("System.String"))) 'Add a column to store the Survey Id
                                    dtTable.Columns.Add(New DataColumn("numQID", System.Type.GetType("System.String"))) 'Add a column to store the Question Id
                                    dtTable.Columns.Add(New DataColumn("vcQuestion", System.Type.GetType("System.String"))) 'Add a column to store the Question
                                    dtTable.Columns.Add(New DataColumn("tIntAnsType", System.Type.GetType("System.String"))) 'Add a column to store the Answer Type
                                    dtTable.Columns.Add(New DataColumn("intNumOfAns", System.Type.GetType("System.String"))) 'Add a column to store the Nos of answers
                                    dtTable.Columns.Add(New DataColumn("boolDeleted", System.Type.GetType("System.String"))) 'Add a column to store the answer type
                                    dtTable.Columns.Add(New DataColumn("boolMatrixColumn", System.Type.GetType("System.String"))) 'Add a column to store the answer type
                                    dtTable.Columns.Add(New DataColumn("intNumOfMatrixColumn", System.Type.GetType("System.String"))) 'Add a column to store the answer type

                                    dsSurveyInformation.Tables.Add(dtTable)

                                    dsSurveyInformation.Tables(dsSurveyInformation.Tables.Count - 1).TableName = "SurveyQuestionMaster"                     'Set the name of the table
                                End If

                                If dsSurveyInformation.Tables("SurveyAnsMaster") Is Nothing Then
                                    dsSurveyInformation.Tables.Add(GetEmptySurveyAnsInformationTemp())         'Add the WorkFlow Rules table
                                    dsSurveyInformation.Tables(dsSurveyInformation.Tables.Count - 1).TableName = "SurveyAnsMaster"                     'Set the name of the table
                                End If

                                If dsSurveyInformation.Tables("SurveyWorkflowRules") Is Nothing Then
                                    dsSurveyInformation.Tables.Add(GetEmptySurveyWorkFlowInformationTemp())         'Add the WorkFlow Rules table
                                    dsSurveyInformation.Tables(dsSurveyInformation.Tables.Count - 1).TableName = "SurveyWorkflowRules"                     'Set the name of the table
                                End If

                                If dsSurveyInformation.Tables("SurveyMatrixMaster") Is Nothing Then
                                    dsSurveyInformation.Tables.Add(GetEmptySurveyMatrixColumnInformationTemp())         'Add the WorkFlow Rules table
                                    dsSurveyInformation.Tables(dsSurveyInformation.Tables.Count - 1).TableName = "SurveyMatrixMaster"                     'Set the name of the table
                                End If

                                If dsSurveyInformation.Tables("SurveyCreateRecord") Is Nothing Then
                                    dsSurveyInformation.Tables.Add(GetEmptySurveyCreateRecordInformationTemp())         'Add the WorkFlow Rules table
                                    dsSurveyInformation.Tables(dsSurveyInformation.Tables.Count - 1).TableName = "SurveyCreateRecord"                     'Set the name of the table
                                End If
                                'ElseIf dsSurveyInformation.Tables.Count = 2 Then
                                '    dsSurveyInformation.Tables.Add(GetEmptySurveyWorkFlowInformationTemp())         'Add the WorkFlow Rules table
                                '    dsSurveyInformation.Tables.Add(GetEmptySurveyMatrixColumnInformationTemp())         'Add the WorkFlow Rules table
                                '    dsSurveyInformation.Tables.Add(GetEmptySurveyCreateRecordInformationTemp())              'Add the WorkFlow Rules table

                                '    dsSurveyInformation.Tables(2).TableName = "SurveyWorkflowRules"                     'Set the name of the table
                                '    dsSurveyInformation.Tables(3).TableName = "SurveyMatrixMaster"                     'Set the name of the table
                                '    dsSurveyInformation.Tables(4).TableName = "SurveyCreateRecord"                     'Set the name of the table
                                'ElseIf dsSurveyInformation.Tables.Count = 3 Then
                                '    If dsSurveyInformation.Tables("SurveyMatrixMaster") IsNot Nothing Then
                                '        dsSurveyInformation.Tables.Add(GetEmptySurveyWorkFlowInformationTemp())         'Add the WorkFlow Rules table
                                '        dsSurveyInformation.Tables(3).TableName = "SurveyWorkflowRules"                     'Set the name of the table
                                '    Else
                                '        dsSurveyInformation.Tables.Add(GetEmptySurveyMatrixColumnInformationTemp())         'Add the WorkFlow Rules table
                                '        dsSurveyInformation.Tables(3).TableName = "SurveyMatrixMaster"                     'Set the name of the table
                                '    End If
                                '    dsSurveyInformation.Tables.Add(GetEmptySurveyCreateRecordInformationTemp())              'Add the WorkFlow Rules table
                                '    dsSurveyInformation.Tables(4).TableName = "SurveyCreateRecord"                     'Set the name of the table
                                'ElseIf dsSurveyInformation.Tables.Count = 4 Then
                                '    dsSurveyInformation.Tables.Add(GetEmptySurveyCreateRecordInformationTemp())              'Add the WorkFlow Rules table
                                '    dsSurveyInformation.Tables(4).TableName = "SurveyCreateRecord"                     'Set the name of the table
                            End If

                            dsSurveyInformation.Tables(0).TableName = "SurveyQuestionMaster"                    'Set the name of the table
                            'dsSurveyInformation.Tables(1).TableName = "SurveyAnsMaster"                         'Set the name of the table

                            If Not dsSurveyInformation.Tables(0).Columns.Contains("boolMatrixColumn") Then
                                dsSurveyInformation.Tables(0).Columns.Add(New DataColumn("boolMatrixColumn", System.Type.GetType("System.String"))) 'Add a column to store the Linked Question IDs
                            End If
                            If Not dsSurveyInformation.Tables(0).Columns.Contains("intNumOfMatrixColumn") Then
                                dsSurveyInformation.Tables(0).Columns.Add(New DataColumn("intNumOfMatrixColumn", System.Type.GetType("System.String"))) 'Add a column to store the Linked Question IDs
                            End If

                            If Not dsSurveyInformation.Tables("SurveyWorkflowRules").Columns.Contains("vcQuestionList") Then
                                dsSurveyInformation.Tables("SurveyWorkflowRules").Columns.Add(New DataColumn("vcQuestionList", System.Type.GetType("System.String"))) 'Add a column to store the Linked Question IDs
                            End If
                            If Not dsSurveyInformation.Tables("SurveyWorkflowRules").Columns.Contains("numLinkedSurID") Then
                                dsSurveyInformation.Tables("SurveyWorkflowRules").Columns.Add(New DataColumn("numLinkedSurID", System.Type.GetType("System.String"))) 'Add a column to store the Linked Question IDs
                            End If
                            If Not dsSurveyInformation.Tables("SurveyWorkflowRules").Columns.Contains("numResponseRating") Then
                                dsSurveyInformation.Tables("SurveyWorkflowRules").Columns.Add(New DataColumn("numResponseRating", System.Type.GetType("System.String"))) 'Add a column to store the Rating given to this rule
                            End If
                            If Not dsSurveyInformation.Tables("SurveyWorkflowRules").Columns.Contains("vcPopUpURL") Then
                                dsSurveyInformation.Tables("SurveyWorkflowRules").Columns.Add(New DataColumn("vcPopUpURL", System.Type.GetType("System.String"))) 'Add a column to store the Group
                            End If
                            If Not dsSurveyInformation.Tables("SurveyWorkflowRules").Columns.Contains("numGrpId") Then
                                dsSurveyInformation.Tables("SurveyWorkflowRules").Columns.Add(New DataColumn("numGrpId", System.Type.GetType("System.String"))) 'Add a column to store the Group
                            End If
                            If Not dsSurveyInformation.Tables("SurveyWorkflowRules").Columns.Contains("numCompanyType") Then
                                dsSurveyInformation.Tables("SurveyWorkflowRules").Columns.Add(New DataColumn("numCompanyType", System.Type.GetType("System.String"))) 'Add a column to store the RelationShip
                            End If
                            If Not dsSurveyInformation.Tables("SurveyWorkflowRules").Columns.Contains("tIntCRMType") Then
                                dsSurveyInformation.Tables("SurveyWorkflowRules").Columns.Add(New DataColumn("tIntCRMType", System.Type.GetType("System.String"))) 'Add a column to store the CRM Type
                            End If
                            If Not dsSurveyInformation.Tables("SurveyWorkflowRules").Columns.Contains("numRecOwner") Then
                                dsSurveyInformation.Tables("SurveyWorkflowRules").Columns.Add(New DataColumn("numRecOwner", System.Type.GetType("System.String"))) 'Add a column to store the Record Owner
                            End If
                            If Not dsSurveyInformation.Tables("SurveyWorkflowRules").Columns.Contains("numMatrixID") Then
                                dsSurveyInformation.Tables("SurveyWorkflowRules").Columns.Add(New DataColumn("numMatrixID", System.Type.GetType("System.String"))) 'Add a column to store the Record Owner
                            End If


                            If Not dsSurveyInformation.Tables("SurveyWorkflowRules").Columns.Contains("tIntRuleFourRadio") Then
                                dsSurveyInformation.Tables("SurveyWorkflowRules").Columns.Add(New DataColumn("tIntRuleFourRadio", System.Type.GetType("System.String"))) 'Add a column to store the Record Owner
                            End If
                            If Not dsSurveyInformation.Tables("SurveyWorkflowRules").Columns.Contains("tIntCreateNewID") Then
                                dsSurveyInformation.Tables("SurveyWorkflowRules").Columns.Add(New DataColumn("tIntCreateNewID", System.Type.GetType("System.String"))) 'Add a column to store the Record Owner
                            End If
                            If Not dsSurveyInformation.Tables("SurveyWorkflowRules").Columns.Contains("vcRuleFourSelectFields") Then
                                dsSurveyInformation.Tables("SurveyWorkflowRules").Columns.Add(New DataColumn("vcRuleFourSelectFields", System.Type.GetType("System.String"))) 'Add a column to store the Record Owner
                            End If
                            If Not dsSurveyInformation.Tables("SurveyWorkflowRules").Columns.Contains("boolPopulateSalesOpportunity") Then
                                dsSurveyInformation.Tables("SurveyWorkflowRules").Columns.Add(New DataColumn("boolPopulateSalesOpportunity", System.Type.GetType("System.String"))) 'Add a column to store the Record Owner
                            End If
                            If Not dsSurveyInformation.Tables("SurveyWorkflowRules").Columns.Contains("vcRuleFiveSelectFields") Then
                                dsSurveyInformation.Tables("SurveyWorkflowRules").Columns.Add(New DataColumn("vcRuleFiveSelectFields", System.Type.GetType("System.String"))) 'Add a column to store the Record Owner
                            End If
                            If Not dsSurveyInformation.Tables("SurveyWorkflowRules").Columns.Contains("vcItem") Then
                                dsSurveyInformation.Tables("SurveyWorkflowRules").Columns.Add(New DataColumn("vcItem", System.Type.GetType("System.String"))) 'Add a column to store the Record Owner
                            End If
                            If Not dsSurveyInformation.Tables("SurveyWorkflowRules").Columns.Contains("numEventOrder") Then
                                dsSurveyInformation.Tables("SurveyWorkflowRules").Columns.Add(New DataColumn("numEventOrder", System.Type.GetType("System.String")))
                            End If

                            If Not dsSurveyInformation.Tables("SurveyCreateRecord").Columns.Contains("bitDefault") Then
                                dsSurveyInformation.Tables("SurveyCreateRecord").Columns.Add(New DataColumn("bitDefault", System.Type.GetType("System.String")))
                            End If
                        Catch ex As Exception                                                           'Error while reading file
                            File.Delete(XMLFilePath & "\" & "TempSurveyInformation_" & DomainId & "_" & SurveyId & "_" & UserId & ".xml")                                                    'file is corrupted so delete it
                            GetSurveyInformationTemp()                                                  'Call self
                        End Try
                    End If

                End If

                Return dsSurveyInformation
            Catch ex As Exception
                Throw ex
                'Appropriate permission not given on the folder
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to get the survey information temporarily (return blank ans table)
        ''' </summary>
        ''' <remarks>
        '''     Returns the Survey Answer Information as a DataTable
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/16/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function GetEmptySurveyAnsInformationTemp() As DataTable
            Dim dtAnswerTable As New DataTable                                              'create a new datatable
            dtAnswerTable.Columns.Add(New DataColumn("numSurID", System.Type.GetType("System.String"))) 'Add a column to store the Survey Id
            dtAnswerTable.Columns.Add(New DataColumn("numQID", System.Type.GetType("System.String"))) 'Add a column to store the Question Id
            dtAnswerTable.Columns.Add(New DataColumn("numAnsID", System.Type.GetType("System.String"))) 'Add a column to store the Answer Id
            dtAnswerTable.Columns.Add(New DataColumn("vcAnsLabel", System.Type.GetType("System.String"))) 'Add a column to store the Answer
            dtAnswerTable.Columns.Add(New DataColumn("boolSurveyRuleAttached", System.Type.GetType("System.String"))) 'Add a column to store the flag if a survey is associated
            dtAnswerTable.Columns.Add(New DataColumn("boolDeleted", System.Type.GetType("System.String"))) 'Add a column to store the Answer deletion flag
            'dtAnswerTable.Columns.Add(New DataColumn("boolMatrixColumn", System.Type.GetType("System.String"))) 'Add a column to store the Answer deletion flag
            Return dtAnswerTable                                                            'Return Datatable 
        End Function

        Public Function GetEmptySurveyMatrixColumnInformationTemp() As DataTable
            Dim dtAnswerTable As New DataTable                                              'create a new datatable
            dtAnswerTable.Columns.Add(New DataColumn("numSurID", System.Type.GetType("System.String"))) 'Add a column to store the Survey Id
            dtAnswerTable.Columns.Add(New DataColumn("numQID", System.Type.GetType("System.String"))) 'Add a column to store the Question Id
            dtAnswerTable.Columns.Add(New DataColumn("numMatrixID", System.Type.GetType("System.String"))) 'Add a column to store the Answer Id
            dtAnswerTable.Columns.Add(New DataColumn("vcAnsLabel", System.Type.GetType("System.String"))) 'Add a column to store the Answer
            dtAnswerTable.Columns.Add(New DataColumn("boolSurveyRuleAttached", System.Type.GetType("System.String"))) 'Add a column to store the flag if a survey is associated
            dtAnswerTable.Columns.Add(New DataColumn("boolDeleted", System.Type.GetType("System.String"))) 'Add a column to store the Answer deletion flag
            Return dtAnswerTable                                                            'Return Datatable 
        End Function
        Public Function GetEmptySurveyCreateRecordInformationTemp() As DataTable
            Dim dtAnswerTable As New DataTable                                              'create a new datatable
            dtAnswerTable.Columns.Add(New DataColumn("numSurID", System.Type.GetType("System.String"))) 'Add a column to store the Survey Id
            dtAnswerTable.Columns.Add(New DataColumn("numCreateRecordId", System.Type.GetType("System.String"))) 'Add a column to store the Survey Id
            dtAnswerTable.Columns.Add(New DataColumn("numRatingMin", System.Type.GetType("System.String"))) 'Add a column to store the Rating Min
            dtAnswerTable.Columns.Add(New DataColumn("numRatingMax", System.Type.GetType("System.String"))) 'Add a column to store the Rating Max
            dtAnswerTable.Columns.Add(New DataColumn("numRelationShipId", System.Type.GetType("System.String"))) 'Add a column to store the RelationShip Id
            dtAnswerTable.Columns.Add(New DataColumn("numGrpId", System.Type.GetType("System.String"))) 'Add a column to store the Group Id
            dtAnswerTable.Columns.Add(New DataColumn("numRecOwner", System.Type.GetType("System.String"))) 'Add a column to store the Record Owner
            dtAnswerTable.Columns.Add(New DataColumn("numProfileId", System.Type.GetType("System.String"))) 'Add a column to store the Profile
            dtAnswerTable.Columns.Add(New DataColumn("tIntCRMType", System.Type.GetType("System.String"))) 'Add a column to store the Profile
            dtAnswerTable.Columns.Add(New DataColumn("numFollowUpStatus", System.Type.GetType("System.String"))) 'Add a column to store the Profile
            dtAnswerTable.Columns.Add(New DataColumn("bitDefault", System.Type.GetType("System.String")))

            Return dtAnswerTable                                                            'Return Datatable 
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to get the survey workflow rules information temporarily (return blank rules table)
        ''' </summary>
        ''' <remarks>
        '''     Returns the Survey Rule Information as a DataTable
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/16/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function GetEmptySurveyWorkFlowInformationTemp() As DataTable
            Dim dtWorkFlowRuleTable As New DataTable                                                            'create a new datatable
            dtWorkFlowRuleTable.Columns.Add(New DataColumn("numRuleID", System.Type.GetType("System.String"))) 'Add a column to store the WorkFlow Rule Id
            dtWorkFlowRuleTable.Columns.Add(New DataColumn("numSurID", System.Type.GetType("System.String"))) 'Add a column to store the Survey Id for the Rule
            dtWorkFlowRuleTable.Columns.Add(New DataColumn("numAnsID", System.Type.GetType("System.String"))) 'Add a column to store the Ans Id for the Rule
            dtWorkFlowRuleTable.Columns.Add(New DataColumn("numMatrixID", System.Type.GetType("System.String"))) 'Add a column to store the Matrix Column Id for the Rule

            dtWorkFlowRuleTable.Columns.Add(New DataColumn("numQID", System.Type.GetType("System.String"))) 'Add a column to store the Question Id for the Rule
            dtWorkFlowRuleTable.Columns.Add(New DataColumn("boolActivation", System.Type.GetType("System.String"))) 'Add a column to store the Activation Flag
            dtWorkFlowRuleTable.Columns.Add(New DataColumn("numEventOrder", System.Type.GetType("System.String"))) 'Add a column to Order of the execution of the Rule
            dtWorkFlowRuleTable.Columns.Add(New DataColumn("vcQuestionList", System.Type.GetType("System.String"))) 'Add a column to store the list of Questions
            dtWorkFlowRuleTable.Columns.Add(New DataColumn("numLinkedSurID", System.Type.GetType("System.String"))) 'Add a column to store the Survey Id for the Rule
            dtWorkFlowRuleTable.Columns.Add(New DataColumn("numResponseRating", System.Type.GetType("System.String"))) 'Add a column to store the Rating given to this rule
            dtWorkFlowRuleTable.Columns.Add(New DataColumn("vcPopUpURL", System.Type.GetType("System.String"))) 'Add a column to store the URL to be popped up when this rule is true
            dtWorkFlowRuleTable.Columns.Add(New DataColumn("numGrpId", System.Type.GetType("System.String"))) 'Add a column to store the Group with which the Database entry is created
            dtWorkFlowRuleTable.Columns.Add(New DataColumn("numCompanyType", System.Type.GetType("System.String"))) 'Add a column to store the Relation Type with which the Database entry is created
            dtWorkFlowRuleTable.Columns.Add(New DataColumn("tIntCRMType", System.Type.GetType("System.String"))) 'Add a column to store the CRM Type with which the Database entry is created
            dtWorkFlowRuleTable.Columns.Add(New DataColumn("numRecOwner", System.Type.GetType("System.String"))) 'Add a column to store the owner of the Database entry
            ' Added By : Pratik
            dtWorkFlowRuleTable.Columns.Add(New DataColumn("tIntRuleFourRadio", System.Type.GetType("System.String"))) 'Add a column to store the radio selected in rule 4
            dtWorkFlowRuleTable.Columns.Add(New DataColumn("tIntCreateNewID", System.Type.GetType("System.String"))) 'Create new combo selected field ID    
            dtWorkFlowRuleTable.Columns.Add(New DataColumn("vcRuleFourSelectFields", System.Type.GetType("System.String"))) 'Rule four selected fields
            dtWorkFlowRuleTable.Columns.Add(New DataColumn("boolPopulateSalesOpportunity", System.Type.GetType("System.String"))) 'populate sales opportunity check box value
            dtWorkFlowRuleTable.Columns.Add(New DataColumn("vcRuleFiveSelectFields", System.Type.GetType("System.String"))) 'Rule 5 select fields 
            dtWorkFlowRuleTable.Columns.Add(New DataColumn("vcItem", System.Type.GetType("System.String"))) 'Rule 5 item Select fields 
            Return dtWorkFlowRuleTable                                                                          'Return DataTable
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to delete the questions from the Sruvey
        ''' </summary>
        ''' <remarks>
        '''     Returns the Survey Information as a DataSet
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/14/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Sub DeleteSurveyQuestionTemp(ByVal numQID As String)
            Dim numSurveyQuestionMasterRowIndex As Integer                      'Declare a index for rows
            Dim dtSurveyQuestionMaster As DataTable = SurveyInfo.Tables("SurveyQuestionMaster") 'Create an object of Datatable and populating it
            For numSurveyQuestionMasterRowIndex = 0 To dtSurveyQuestionMaster.Rows.Count - 1 'Loop through the rows
                If numQID = dtSurveyQuestionMaster.Rows(numSurveyQuestionMasterRowIndex).Item("numQID") Then
                    DeleteSurveyAnswerTemp(dtSurveyQuestionMaster.Rows(numSurveyQuestionMasterRowIndex).Item("numQID")) 'Call to delete the answers for the question
                    dtSurveyQuestionMaster.Rows(numSurveyQuestionMasterRowIndex).Delete() 'Delete the row (set flag)
                    Exit For
                End If
            Next
        End Sub
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to delete the answers from the Sruvey
        ''' </summary>
        ''' <remarks>
        '''     Returns the Survey Information as a DataSet
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/14/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Sub DeleteSurveyAnswerTemp(ByVal numQID As Integer, Optional ByVal numAnsID As Integer = 0)
            Dim numSurveyAnswerMasterRowIndex As Integer                      'Declare a index for rows
            Dim dtSurveyAnswerMaster As DataTable = SurveyInfo.Tables("SurveyAnsMaster") 'Create an object of Datatable and populating it
            While numSurveyAnswerMasterRowIndex < dtSurveyAnswerMaster.Rows.Count
                If dtSurveyAnswerMaster.Rows(numSurveyAnswerMasterRowIndex).Item("numQID") = numQID Then
                    DeleteSurveyWorkFlowTemp(numQID, numAnsID)            'Request deletion of rules
                    If dtSurveyAnswerMaster.Rows(numSurveyAnswerMasterRowIndex).Item("numAnsID") = numAnsID Then
                        dtSurveyAnswerMaster.Rows(numSurveyAnswerMasterRowIndex).Delete() 'Delete the answers
                        numSurveyAnswerMasterRowIndex -= 1
                    ElseIf numAnsID = 0 Then
                        dtSurveyAnswerMaster.Rows(numSurveyAnswerMasterRowIndex).Delete() 'Delete the answers
                        numSurveyAnswerMasterRowIndex -= 1
                    End If
                End If
                numSurveyAnswerMasterRowIndex += 1
            End While
        End Sub
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to delete the work flows from the Sruvey
        ''' </summary>
        ''' <remarks>
        '''     Returns the Survey Information as a DataSet
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	05/20/2006	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Sub DeleteSurveyWorkFlowTemp(ByVal numQID As Integer, Optional ByVal numAnsID As Integer = 0)
            Dim dtSurveyWorkFlowMaster As DataTable
            dtSurveyWorkFlowMaster = SurveyInfo.Tables("SurveyWorkflowRules")                       'Get the workFlow table
            Dim intWorkFlowRowId As Integer                                                         'Declare a variable to loop across rows in Work Flow
            While intWorkFlowRowId < dtSurveyWorkFlowMaster.Rows.Count
                If dtSurveyWorkFlowMaster.Rows(intWorkFlowRowId).Item("numQID") = numQID Then
                    If dtSurveyWorkFlowMaster.Rows(intWorkFlowRowId).Item("numAnsID") = numAnsID Then
                        dtSurveyWorkFlowMaster.Rows(intWorkFlowRowId).Delete()                      'Delete the rules
                        intWorkFlowRowId -= 1                                                       'decrement the counter of work flow indicator
                    ElseIf numAnsID = 0 Then
                        dtSurveyWorkFlowMaster.Rows(intWorkFlowRowId).Delete()                      'Delete the rules
                        intWorkFlowRowId -= 1                                                       'decrement the counter of work flow indicator
                    End If
                End If
                intWorkFlowRowId += 1                                                               'increment the counter of work flow indicator
            End While
        End Sub
        Public Sub DeleteSurveyWorkFlowTempMatrix(ByVal numQID As Integer, Optional ByVal numAnsID As Integer = 0)
            Dim dtSurveyWorkFlowMaster As DataTable
            dtSurveyWorkFlowMaster = SurveyInfo.Tables("SurveyWorkflowRules")                       'Get the workFlow table
            Dim intWorkFlowRowId As Integer                                                         'Declare a variable to loop across rows in Work Flow
            While intWorkFlowRowId < dtSurveyWorkFlowMaster.Rows.Count
                If dtSurveyWorkFlowMaster.Rows(intWorkFlowRowId).Item("numQID") = numQID Then
                    If dtSurveyWorkFlowMaster.Rows(intWorkFlowRowId).Item("numMatrixID") = numAnsID Then
                        dtSurveyWorkFlowMaster.Rows(intWorkFlowRowId).Delete()                      'Delete the rules
                        intWorkFlowRowId -= 1                                                       'decrement the counter of work flow indicator
                    ElseIf numAnsID = 0 Then
                        dtSurveyWorkFlowMaster.Rows(intWorkFlowRowId).Delete()                      'Delete the rules
                        intWorkFlowRowId -= 1                                                       'decrement the counter of work flow indicator
                    End If
                End If
                intWorkFlowRowId += 1                                                               'increment the counter of work flow indicator
            End While
        End Sub
        Public Sub DeleteSurveyCreateRecord(ByVal numCreateRecordId As Integer)
            Dim dtSurveyWorkFlowMaster As DataTable
            dtSurveyWorkFlowMaster = SurveyInfo.Tables("SurveyCreateRecord")                       'Get the workFlow table
            Dim intWorkFlowRowId As Integer                                                         'Declare a variable to loop across rows in Work Flow
            While intWorkFlowRowId < dtSurveyWorkFlowMaster.Rows.Count
                If dtSurveyWorkFlowMaster.Rows(intWorkFlowRowId).Item("numCreateRecordId") = numCreateRecordId Then
                    dtSurveyWorkFlowMaster.Rows(intWorkFlowRowId).Delete()                      'Delete the rules
                    intWorkFlowRowId -= 1                                                       'decrement the counter of work flow indicator
                End If
                intWorkFlowRowId += 1                                                               'increment the counter of work flow indicator
            End While
        End Sub
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to delete the XML file containing the temporary survey information
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/14/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Sub DeleteTempSurveyInformation()
            Try
                If File.Exists(XMLFilePath & "\" & "TempSurveyInformation_" & DomainId & "_" & SurveyId & "_" & UserId & ".xml") Then                                                        'Check if the file exists
                    File.Delete(XMLFilePath & "\" & "TempSurveyInformation_" & DomainId & "_" & SurveyId & "_" & UserId & ".xml")                                                            'Delete the file
                End If
            Catch ex As Exception
                Throw ex
                'Appropriate permission not given on the folder
            End Try
        End Sub
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to retrieve the response details to a particular question
        ''' </summary>
        ''' <remarks>
        '''     Returns the Survey Response Type Information as a DataTable
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/15/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function GetResponseTypeDetailsForSurveyQuestion() As DataTable
            Dim dtSurveyQuestion As DataTable                                                               'Declare the datatable
            dtSurveyQuestion = SurveyInfo.Tables("SurveyQuestionMaster")                                    'Get the DataTable Data
            Dim drSurveyQuestions() As DataRow                                                              'Declare a DataRow
            drSurveyQuestions = dtSurveyQuestion.Select("numQID IN ('" & QuestionId & "')")                           'Set the filter
            Dim dtSurveyQuestionsClone As DataTable                                                         'Declare the datatable
            dtSurveyQuestionsClone = dtSurveyQuestion.Clone()                                               'Clone the DataTable

            Dim drSurveyQuestion As DataRow                                                                 'Declare a DataRow
            For Each drSurveyQuestion In drSurveyQuestions                                                  'loop through the rows that match the criteria
                dtSurveyQuestionsClone.ImportRow(drSurveyQuestion)                                          'import selected rows
            Next
            Return dtSurveyQuestionsClone
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to get teh Survey Information from teh database.
        ''' </summary>
        ''' <returns>Returns the value as DataSet.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the complete
        '''     information about a survey from the database.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function getSurveyInformation() As DataSet
            Try
                Dim ds As DataSet                                                   'declare a dataset
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSurID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = SurveyId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur4", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur5", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetSurveyInformation", arParms) 'execute and store to dataset

                Return ds                                                           'return datatable
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the values for AOI from the database.
        ''' </summary>
        ''' <returns>Returns the value as DataTable.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the value
        '''     of AOI's.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/16/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function getAOIList() As DataTable
            Try
                Dim ds As DataSet                                                   'declare a dataset
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetAOIs", arParms)      'execute and store to dataset

                Return ds.Tables(0)                                                 'return datatable
            Catch ex As Exception
                Throw ex
            End Try

        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to retrieve the answers to a particular question
        ''' </summary>
        ''' <remarks>
        '''     Returns the Survey Answer Information as a DataTable
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/15/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function GetAnswersForSurveyQuestion() As DataTable
            Dim dtAnswersForSurveyQuestion As DataTable = New DataTable                                     'Declare the datatable
            dtAnswersForSurveyQuestion = SurveyInfo.Tables("SurveyAnsMaster")                               'Get the DataTable's data
            Dim drAnswersForSurveyQuestion() As DataRow                                                     'Declare a DataRow
            drAnswersForSurveyQuestion = dtAnswersForSurveyQuestion.Select("(numSurId = '" & SurveyId & "' and numQID = '" & QuestionId & "' and boolDeleted = '0')", "numQID Asc, numAnsId Asc") 'Set the filter
            Dim dtAnswersForSurveyQuestionClone As DataTable = New DataTable                                'Declare the datatable
            dtAnswersForSurveyQuestionClone = dtAnswersForSurveyQuestion.Clone()                            'Clone the DataTable

            Dim drAnswers As DataRow                                                                        'Declare a DataRow
            Dim numAnsId As Integer                                                                         'Declare a variable to store the answer id
            For Each drAnswers In drAnswersForSurveyQuestion                                                'loop through the rows that match the criteria
                If numAnsId <> drAnswers.Item("numAnsId") Then                                              'since there is possibility of duplicates if the same question is forked to twice in the same survey
                    dtAnswersForSurveyQuestionClone.ImportRow(drAnswers)                                    'import selected rows
                End If
                numAnsId = drAnswers.Item("numAnsId")                                                       'Capture the latest AnsId
            Next
            Return dtAnswersForSurveyQuestionClone
        End Function

        Public Function GetMatrixForSurveyQuestion() As DataTable
            Dim dtAnswersForSurveyQuestion As DataTable = New DataTable                                     'Declare the datatable
            dtAnswersForSurveyQuestion = SurveyInfo.Tables("SurveyMatrixMaster")                               'Get the DataTable's data
            Dim drAnswersForSurveyQuestion() As DataRow                                                     'Declare a DataRow
            drAnswersForSurveyQuestion = dtAnswersForSurveyQuestion.Select("(numSurId = '" & SurveyId & "' and numQID = '" & QuestionId & "' and boolDeleted = '0')", "numQID Asc, numMatrixID Asc") 'Set the filter
            Dim dtAnswersForSurveyQuestionClone As DataTable = New DataTable                                'Declare the datatable
            dtAnswersForSurveyQuestionClone = dtAnswersForSurveyQuestion.Clone()                            'Clone the DataTable

            Dim drAnswers As DataRow                                                                        'Declare a DataRow
            Dim numAnsId As Integer                                                                         'Declare a variable to store the answer id
            For Each drAnswers In drAnswersForSurveyQuestion                                                'loop through the rows that match the criteria
                If numAnsId <> drAnswers.Item("numMatrixID") Then                                              'since there is possibility of duplicates if the same question is forked to twice in the same survey
                    dtAnswersForSurveyQuestionClone.ImportRow(drAnswers)                                    'import selected rows
                End If
                numAnsId = drAnswers.Item("numMatrixID")                                                       'Capture the latest AnsId
            Next
            Return dtAnswersForSurveyQuestionClone
        End Function

        Public Function GetCreateRecordForSurvey() As DataTable
            Dim dtCreateRecord As DataTable = New DataTable                                     'Declare the datatable
            dtCreateRecord = SurveyInfo.Tables("SurveyCreateRecord")                               'Get the DataTable's data

            Return dtCreateRecord
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to retrieve the Work FLow Rules for a particular anmswer
        ''' </summary>
        ''' <remarks>
        '''     Returns the Survey Work Flow Rules Information as a DataTable
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/16/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        ''' 

        Public Function GetSurveyWorkflowRulesDetailsForSurvey() As DataTable
            Dim dtSurveyAnswerWorkFlowRules As DataTable = New DataTable                                    'Declare the datatable
            dtSurveyAnswerWorkFlowRules = SurveyInfo.Tables("SurveyWorkflowRules")                          'Get the DataTable Data
            Dim drSurveyAnswerWorkFlowRules() As DataRow                                                    'Declare a DataRow
            drSurveyAnswerWorkFlowRules = dtSurveyAnswerWorkFlowRules.Select("(numSurID = '" & SurveyId & "'and boolActivation = 1)", "numEventOrder Asc") 'Set the filter
            Dim dtSurveyAnswerWorkFlowRulesClone As DataTable = New DataTable                               'Declare the datatable
            dtSurveyAnswerWorkFlowRulesClone = dtSurveyAnswerWorkFlowRules.Clone()                          'Clone the DataTable

            Dim drSurveyAnswerWorkFlowRule As DataRow                                                       'Declare a DataRow
            For Each drSurveyAnswerWorkFlowRule In drSurveyAnswerWorkFlowRules                              'loop through the rows that match the criteria
                dtSurveyAnswerWorkFlowRulesClone.ImportRow(drSurveyAnswerWorkFlowRule)                      'import selected rows
            Next
            Return dtSurveyAnswerWorkFlowRulesClone
        End Function
        Public Function GetSurveyWorkflowRulesDetailsForSurveyAnswer() As DataTable
            Dim dtSurveyAnswerWorkFlowRules As DataTable = New DataTable                                    'Declare the datatable
            dtSurveyAnswerWorkFlowRules = SurveyInfo.Tables("SurveyWorkflowRules")                          'Get the DataTable Data
            Dim drSurveyAnswerWorkFlowRules() As DataRow                                                    'Declare a DataRow
            drSurveyAnswerWorkFlowRules = dtSurveyAnswerWorkFlowRules.Select("(numSurID = '" & SurveyId & "' and numAnsID = '" & AnswerId & "' and numQID = '" & QuestionId & "' and numMatrixID = '" & MatrixID & "' and boolActivation = 1)", "numEventOrder Asc") 'Set the filter
            Dim dtSurveyAnswerWorkFlowRulesClone As DataTable = New DataTable                               'Declare the datatable
            dtSurveyAnswerWorkFlowRulesClone = dtSurveyAnswerWorkFlowRules.Clone()                          'Clone the DataTable

            Dim drSurveyAnswerWorkFlowRule As DataRow                                                       'Declare a DataRow
            For Each drSurveyAnswerWorkFlowRule In drSurveyAnswerWorkFlowRules                              'loop through the rows that match the criteria
                dtSurveyAnswerWorkFlowRulesClone.ImportRow(drSurveyAnswerWorkFlowRule)                      'import selected rows
            Next
            Return dtSurveyAnswerWorkFlowRulesClone
        End Function

        Public Function GetSurveyWorkflowRulesDetailsForSurveyMatrix() As DataTable
            Dim dtSurveyAnswerWorkFlowRules As DataTable = New DataTable                                    'Declare the datatable
            dtSurveyAnswerWorkFlowRules = SurveyInfo.Tables("SurveyWorkflowRules")                          'Get the DataTable Data
            Dim drSurveyAnswerWorkFlowRules() As DataRow                                                    'Declare a DataRow
            drSurveyAnswerWorkFlowRules = dtSurveyAnswerWorkFlowRules.Select("(numSurID = '" & SurveyId & "' and numMatrixID = '" & AnswerId & "' and numQID = '" & QuestionId & "' and boolActivation = 1)", "numEventOrder Asc") 'Set the filter
            Dim dtSurveyAnswerWorkFlowRulesClone As DataTable = New DataTable                               'Declare the datatable
            dtSurveyAnswerWorkFlowRulesClone = dtSurveyAnswerWorkFlowRules.Clone()                          'Clone the DataTable

            Dim drSurveyAnswerWorkFlowRule As DataRow                                                       'Declare a DataRow
            For Each drSurveyAnswerWorkFlowRule In drSurveyAnswerWorkFlowRules                              'loop through the rows that match the criteria
                dtSurveyAnswerWorkFlowRulesClone.ImportRow(drSurveyAnswerWorkFlowRule)                      'import selected rows
            Next
            Return dtSurveyAnswerWorkFlowRulesClone
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to retrieve the Work FLow Rules for a particular anmswer
        ''' </summary>
        ''' <remarks>
        '''     Returns the Survey Work Flow Rules Information as a DataTable
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/16/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function CheckOccuranceOfRule3() As Boolean
            Dim dtSurveyWorkFlowRules3 As DataTable = New DataTable                                    'Declare the datatable
            dtSurveyWorkFlowRules3 = SurveyInfo.Tables("SurveyWorkflowRules")                          'Get the DataTable Data
            Dim drSurveyAnswerWorkFlowRules3() As DataRow                                              'Declare a DataRow
            drSurveyAnswerWorkFlowRules3 = dtSurveyWorkFlowRules3.Select("(numRuleID = '3' AND boolActivation = 1)") 'Set the filter
            If drSurveyAnswerWorkFlowRules3.Length > 0 Then                                            'No occurance of Rule 3
                Dim drSurveyAnswerWorkFlowRules As DataRow
                For Each drSurveyAnswerWorkFlowRules In drSurveyAnswerWorkFlowRules3
                    If drSurveyAnswerWorkFlowRules.Item("numAnsId") = AnswerId And drSurveyAnswerWorkFlowRules.Item("numQId") = QuestionId Then
                        Return False
                        Exit Function
                    End If
                Next
                Return True
            Else
                Return False
            End If
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to save the survey information in the database
        ''' </summary>
        ''' <remarks>
        '''     Saves the Survey Information to the database and Returns the ID of the Survey
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/17/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function SaveSurveyInformationInDatabase() As Integer
            Dim sSurveyInformation As New System.Text.StringBuilder                                         'Declare a string Builder
            sSurveyInformation.Append(SurveyInfo.GetXml)                                                    'Retrieve the Survey Information
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSurID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = SurveyId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = UserId

                arParms(3) = New Npgsql.NpgsqlParameter("@vcSurveyInformation", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(3).Value = sSurveyInformation.ToString()

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return CInt(SqlDAL.ExecuteScalar(connString, "usp_InsertEditSurvey", arParms))  'execute and store to dataset

            Catch ex As Exception
                Return 0
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to Delete teh Survey from the database
        ''' </summary>
        ''' <remarks>
        '''     Deletes the Survey Information from the database
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/20/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Sub DeleteSurvey()
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSurID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = SurveyId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId

                SqlDAL.ExecuteNonQuery(connString, "usp_DeleteSurvey", arParms)  'execute and store to dataset

            Catch ex As Exception
            End Try
        End Sub
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to get teh Survey Information from teh database.
        ''' </summary>
        ''' <returns>Returns the value as DataSet.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the complete
        '''     information about a survey from the database.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function getSurveyInformation4Execution() As DataSet
            Try
                Dim ds As DataSet                                                   'declare a dataset
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSurID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = SurveyId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur4", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur5", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetSurveyInformation4StandardExecution", arParms) 'execute and store to dataset

                Return ds                                                           'return datatable
            Catch ex As Exception
                Throw ex
            End Try

        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to get the included Survey Information from the database.
        ''' </summary>
        ''' <returns>Returns the value as DataSet.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the selected Questions and answer
        '''     information about a requested survey from the database.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/21/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function GetForkedSurveyQuestionAnswer(ByVal numSurID As Integer, ByVal vcQuestionIDs As String) As DataSet
            Try
                Dim ds As DataSet                                                   'declare a dataset
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSurID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = numSurID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcQuestionIDs", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(1).Value = vcQuestionIDs

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput


                ds = SqlDAL.ExecuteDataset(connString, "usp_GetSurveyInformation4ForkedExecution", arParms) 'execute and store to dataset

                Return ds                                                           'return datatable
            Catch ex As Exception
                Throw ex
            End Try

        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to save the survey Respondents information in the database
        ''' </summary>
        ''' <remarks>
        '''     Saves the Survey Respondents Information to the database and Returns the ID of the Respondent
        ''' </remarks>
        ''' <param name="sSurveyRespondent">The string containing the Survey Respondent's information</param>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/23/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function SaveSurveyRespondentInformationInDatabase(ByVal sSurveyRespondent As String) As Integer
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSurID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = SurveyId

                arParms(1) = New Npgsql.NpgsqlParameter("@vcSurveyRespondentInformation", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = sSurveyRespondent

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteScalar(connString, "usp_SaveSurveyRespondent", arParms)  'Execute and store to dataset

            Catch ex As Exception
                Return 0
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to save the survey Respondents information in the database
        ''' </summary>
        ''' <remarks>
        '''     Saves the Survey Respondents Information to the database and Returns the ID of the Respondent
        ''' </remarks>
        ''' <param name="">The string containing the Survey Respondent's information</param>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	11/16/2006	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function SaveSurveyRespondentInformationInDatabase() As Integer
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSurID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = SurveyId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId

                Return SqlDAL.ExecuteScalar(connString, "usp_SaveEmptySurveyRespondent", arParms)  'Execute and store to dataset

            Catch ex As Exception
                Return 0
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to save the survey Respondents information in the database
        ''' </summary>
        ''' <remarks>
        '''     Saves the Survey Respondents Information to the database and Returns the ID of the Respondent
        ''' </remarks>
        ''' <param name="sSurveyRespondent">The string containing the Survey Respondent's information</param>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/23/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function UpdateSurveyRespondentInformationInDatabase(ByVal sSurveyRespondent As String, ByVal numRespondantID As Integer) As Integer
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSurID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = SurveyId

                arParms(1) = New Npgsql.NpgsqlParameter("@vcSurveyRespondentInformation", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = sSurveyRespondent

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@numRespondantID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = numRespondantID

                Return SqlDAL.ExecuteScalar(connString, "usp_UpdateSurveyRespondent", arParms)  'Execute and store to dataset

            Catch ex As Exception
                Return 0
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to get the survey Respondents information in the database
        ''' </summary>
        ''' <remarks>
        '''     Retrieves the Survey Respondents Information to the database and Returns the Info in a DataTable
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/23/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function getSurveyRespondentInformation() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSurID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = SurveyId

                arParms(1) = New Npgsql.NpgsqlParameter("@numRespondantID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = SurveyExecution.SurveyExecution().RespondentID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetSurveyRespondentForBusinessCreation", arParms) 'Execute and store to dataset
                Return ds.Tables(0)
            Catch ex As Exception

            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to save the survey Responses in the database
        ''' </summary>
        ''' <remarks>
        '''     Saves the Survey Response to the database 
        ''' </remarks>
        ''' <param name="dsSurveyRespondent">The DataSet containing the Survey Respondent's information</param>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/23/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function SaveSurveyResponseInformationInDatabase(ByVal dsSurveyResponse As DataSet, ByVal numSurveyRespondentContactId As Integer) As Integer
            Dim sSurveyResponseInformation As New System.Text.StringBuilder         'Declare a string Builder
            sSurveyResponseInformation.Append(dsSurveyResponse.GetXml)              'Retrieve the Survey Information
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSurID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = SurveyId

                arParms(1) = New Npgsql.NpgsqlParameter("@numRespondantID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = SurveyExecution.SurveyExecution.RespondentID

                arParms(2) = New Npgsql.NpgsqlParameter("@numSurRating", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = SurveyExecution.SurveyExecution.SurveyRating

                arParms(3) = New Npgsql.NpgsqlParameter("@numSurveyRespondentContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = numSurveyRespondentContactId

                arParms(4) = New Npgsql.NpgsqlParameter("@vcSurveyResponseInformation", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(4).Value = sSurveyResponseInformation.ToString()

                arParms(5) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = DomainId

                Return SqlDAL.ExecuteNonQuery(connString, "usp_InsertSurveyResponse", arParms)  'Execute and store to dataset

            Catch ex As Exception
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to checks if the user is preregistered, if yes then it registers for survey automatically
        ''' </summary>
        ''' <remarks>
        '''     Returns the Id of the respondent if the contact already exists, else it return zero
        ''' </remarks>
        ''' <param name="sEmail">The Email of  the Survey Respondent</param>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/29/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function CheckRegisteredUserAndUpdateRespondent(ByVal sEmail As String, ByVal numRespondantID As Integer) As Integer
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSurID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = SurveyId

                arParms(1) = New Npgsql.NpgsqlParameter("@vcEmail", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(1).Value = sEmail

                arParms(2) = New Npgsql.NpgsqlParameter("@numRespondantID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = numRespondantID

                Return SqlDAL.ExecuteScalar(connString, "usp_CheckExistingRespondentAndUpdateInfo", arParms)  'Execute and store to dataset
            Catch ex As Exception
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to checks if the user is preregistered, if yes then it registers for survey automatically
        ''' </summary>
        ''' <remarks>
        '''     Returns the Id of the respondent if the contact already exists, else it return zero
        ''' </remarks>
        ''' <param name="sEmail">The Email of  the Survey Respondent</param>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/29/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function CheckRegisteredUserAndSaveInformationInDatabase(ByVal sEmail As String) As Integer
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSurID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = SurveyId

                arParms(1) = New Npgsql.NpgsqlParameter("@vcEmail", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(1).Value = sEmail

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return CInt(SqlDAL.ExecuteScalar(connString, "usp_CheckExistingRespondentAndPreRegister", arParms))  'Execute and store to dataset
            Catch ex As Exception
            End Try
        End Function

        Public Function CheckRegisteredUserAndSaveInformationInDatabase_Direct() As Integer
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSurID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = SurveyId

                arParms(1) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = ContactId

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return CInt(SqlDAL.ExecuteScalar(connString, "usp_CheckExistingRespondentAndPreRegister_Direct", arParms))  'Execute and store to dataset
            Catch ex As Exception
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to get the respondents for a selected survey.
        ''' </summary>
        ''' <returns>Returns the value as DataSet.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the list of respondents for a survey
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/29/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function getSurveyRespondentsList() As DataSet
            Try
                Dim ds As DataSet                                                   'declare a dataset
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSurID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = SurveyId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetSurveyRespondants", arParms) 'execute and store to dataset

                Return ds                                                           'return datatable
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the survey list from master database.
        ''' </summary>
        ''' <remarks>
        '''     This function calls the stored procedure which returns the list of survey for the contact
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	10/02/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function getSurveyHistoryList() As DataTable
            Try
                Dim ds As DataSet                                                                   'declare a dataset
                Dim getconnection As New GetConnection                                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString                        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = ContactId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetSurveyHistoryList", arParms)         'execute the stored procedure
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to get the contacts responsese for a selected survey
        ''' </summary>
        ''' <remarks>
        '''     This function calls the stored procedure which returns the list of responses for the contact
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	10/02/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function getSurveyContactsResponses() As DataTable
            Try
                Dim ds As DataSet                                                                   'declare a dataset
                Dim getconnection As New GetConnection                                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString                        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSurID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = SurveyId

                arParms(1) = New Npgsql.NpgsqlParameter("@numRespondentId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = SurveyExecution.SurveyExecution.RespondentID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetSurveyContactRespondentsChoices", arParms)         'execute the stored procedure
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to Delete the Contacts Survey Responses from the database
        ''' </summary>
        ''' <remarks>
        '''     Deletes the Survey Information related to the contact from the database
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	10/02/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Sub DeleteSurveyHistoryForContact()
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSurID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = SurveyId

                arParms(1) = New Npgsql.NpgsqlParameter("@numRespondentID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = SurveyExecution.SurveyExecution.RespondentID

                SqlDAL.ExecuteNonQuery(connString, "usp_DeleteSurveyHistoryForContact", arParms)  'execute amd delete teh survey response

            Catch ex As Exception
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to get the answers and the response for a survey
        ''' </summary>
        ''' <remarks>
        '''     Gets the answers for a question and also gets the number of responses for each answers
        ''' </remarks>
        ''' <param name="numSurId">The current Survey ID</param>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	10/02/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function GetAnswersAndResponseForSurveyQuestion(ByVal numSurId As Integer) As DataTable
            Try
                Dim ds As DataSet                                                   'declare a dataset
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numParentSurID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = SurveyId

                arParms(1) = New Npgsql.NpgsqlParameter("@numSurID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = numSurId

                arParms(2) = New Npgsql.NpgsqlParameter("@numQID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = QuestionId

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetSurveyAnswersAndNosOfResponses", arParms)  'execute amd delete teh survey response
                Return ds.Tables(0)
            Catch ex As Exception
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to get the respondents DivisionId, CRMType and Company Id
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <param name="numContactId">The current contact id</param>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	01/06/2006	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function GetContactInfoForURLRedirection(ByVal numEntityId As Integer, ByVal vcEntityType As String) As DataTable
            Try
                Dim ds As DataSet                                                   'declare a dataset
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numEntityId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = numEntityId

                arParms(1) = New Npgsql.NpgsqlParameter("@vcEntityType", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(1).Value = vcEntityType

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetContactInfoForURLRedirection", arParms)  'execute amd delete teh survey response
                Return ds.Tables(0)
            Catch ex As Exception
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to get the name of the user who has checked out the survey
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	03/09/2006	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function getSurveyCheckedOutByUser()
            Dim getconnection As New GetConnection                              'declare a connection
            Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

            arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(0).Value = DomainId

            arParms(1) = New Npgsql.NpgsqlParameter("@numUserID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(1).Value = UserId

            arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
            arParms(2).Value = Nothing
            arParms(2).Direction = ParameterDirection.InputOutput

            Return SqlDAL.ExecuteScalar(connString, "usp_GetUserNameFromContacts", arParms)  'execute amd return the user name
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to get the survey List of a Domain For DropDown
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Gangadhar]	
        ''' </history>
        '''  -----------------------------------------------------------------------------
        ''' 
        Public Function GetSurveyListddl() As DataTable
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim ds As DataSet
                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput


                ds = SqlDAL.ExecuteDataset(connString, "usp_GetSurveyListDdl", arParms)  'execute amd return the user name
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetSurveyQuestions() As DataTable
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@SurveyId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = SurveyId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetSurveyQuestion", arParms)  'execute amd return the user name
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetSurveyAnswers() As DataTable
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@SurveyId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = SurveyId

                arParms(2) = New Npgsql.NpgsqlParameter("@QuestionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = QuestionId

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetSurveyAnswers", arParms)  'execute amd return the user name
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Added By Pratik
        Public Function GetSelectFields(ByVal intIsLeadsId As Integer) As DataTable
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numIsLeadsField", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = intIsLeadsId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput


                ds = SqlDAL.ExecuteDataset(connString, "usp_SurveySelectFields", arParms)  'execute amd return the user name
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageSurveyTemplate() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSurId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _numSurId

                arParms(2) = New Npgsql.NpgsqlParameter("@numPageType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _tIntPageType

                arParms(3) = New Npgsql.NpgsqlParameter("@txtTemplate", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(3).Value = _strText

                arParms(4) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(4).Value = _StrItems

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageSurveyTemplate", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetSurveyTemplate() As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSurId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _numSurId

                arParms(2) = New Npgsql.NpgsqlParameter("@numPageType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _tIntPageType

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetSurveyTemplate", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function getSurveyRating() As DataTable
            Try
                Dim ds As DataSet                                                                   'declare a dataset
                Dim getconnection As New GetConnection                                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString                        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numSurID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = SurveyId

                arParms(2) = New Npgsql.NpgsqlParameter("@numSurRating", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = SurveyExecution.SurveyExecution.SurveyRating

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetSurveyRating", arParms)  'execute the stored procedure
                Return ds.Tables(0)
            Catch ex As Exception
            End Try
        End Function
        Public Function getDefaultSurveyRating() As DataTable
            Try
                Dim ds As DataSet                                                                   'declare a dataset
                Dim getconnection As New GetConnection                                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString                        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numSurID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = SurveyId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetDefaultSurveyRating", arParms)  'execute the stored procedure
                Return ds.Tables(0)
            Catch ex As Exception
            End Try
        End Function
    End Class
End Namespace

