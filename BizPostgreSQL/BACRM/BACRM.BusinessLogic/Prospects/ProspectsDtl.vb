
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Prospects
    Public Class CProspectsDtl
        Inherits BACRM.BusinessLogic.CBusinessBase


        Private _DivisionID As Long = 0
        ''Private DomainId As Long = 0
        Private _ClientTimeZoneOffset As Int32

        Public Property ClientTimeZoneOffset() As Int32
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal Value As Int32)
                _ClientTimeZoneOffset = Value
            End Set
        End Property
        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property
        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property

    End Class

End Namespace