'**********************************************************************************
' <CProspects.vb>
' 
' 	CHANGE CONTROL:
'	
'	AUTHOR: Goyal 	DATE:28-Feb-05 	VERSION	CHANGES	KEYSTRING:
'**********************************************************************************


Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Prospects

    '**********************************************************************************
    ' Module Name  : None
    ' Module Type  : CCommon
    ' 
    ' Description  : This is the business logic classe for Common Stuff
    '**********************************************************************************
    Public Class CProspects
        Inherits BACRM.BusinessLogic.CBusinessBase


        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Goyal 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Goyal 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region

        Private _CRMType As Integer = 0
        'Private UserCntID As Long = 0
        Private _UserRightType As Long = 0
        Private _SortOrder As Long = 0
        Private _LastDate As Long = 0
        Private _SortChar As String = String.Empty
        Private _RelType As Long = 0
        Private _DivisionID As Long = 0
        Private _CompanyID As Long = 0
        Private _FirstName As String = String.Empty
        Private _LastName As String = String.Empty
        Private _CustName As String = String.Empty
        Private _ByteMode As Short
        Private _RatingID As Long
        Private _TerId As Long
        Private _ComInd As Long
        Private _Credit As Long
        Private _SICCode As String
        Private _Comments As String
        Private _WebSite As String
        Private _WebLabel1 As String
        Private _WebLink1 As String
        Private _WebLabel2 As String
        Private _WebLink2 As String
        Private _WebLabel3 As String
        Private _WebLink3 As String
        Private _WebLabel4 As String
        Private _WebLink4 As String
        Private _RelatedDoc As String
        Private _AnnualRev As Long
        Private _NoofEmp As Long
        Private _InfoSource As Long
        Private _Profile As Long
        Private _CreatedBy As Long
        Private _ModifiedBy As Long
        Private _PublicFlag As Short
        'Private DomainId As Long
        Private _ContactId As Long
        Private _ProStatus As Short
        Private _OppStatus As Short
        Private _OppType As Short
        Private _CurrentPage As Integer
        Private _PageSize As Integer
        Private _TotalRecords As Integer
        Private _FromDate As Date = New Date(1753, 1, 1)
        Private _ToDate As Date = New Date(1753, 1, 1)
        Private _BitTask As Short
        Private _KeyWord As String
        Private _AssociationID As Long
        Private _AssociationFrom As Long
        Private _AssociationType As Long
        Private _bitDeleted As Short
        Private _bitAssociatedTo As Boolean
        Private _bitSharePortal As Boolean
        Private _strCompanyTaxTypes As String
        Private _HeaderLink As Integer
        Public Property HeaderLink() As Integer
            Get
                Return _HeaderLink
            End Get
            Set(ByVal Value As Integer)
                _HeaderLink = Value
            End Set
        End Property
        Public Property strCompanyTaxTypes() As String
            Get
                Return _strCompanyTaxTypes
            End Get
            Set(ByVal Value As String)
                _strCompanyTaxTypes = Value
            End Set
        End Property

        Private _AssociationTypeForLabel As Long
        Public Property AssociationTypeForLabel() As Long
            Get
                Return _AssociationTypeForLabel
            End Get
            Set(ByVal value As Long)
                _AssociationTypeForLabel = value
            End Set
        End Property

        Public Property bitSharePortal() As Boolean
            Get
                Return _bitSharePortal
            End Get
            Set(ByVal Value As Boolean)
                _bitSharePortal = Value
            End Set
        End Property

        Public Property bitAssociatedTo() As Boolean
            Get
                Return _bitAssociatedTo
            End Get
            Set(ByVal Value As Boolean)
                _bitAssociatedTo = Value
            End Set
        End Property

        Public Property bitDeleted() As Short
            Get
                Return _bitDeleted
            End Get
            Set(ByVal Value As Short)
                _bitDeleted = Value
            End Set
        End Property

        Public Property AssociationType() As Long
            Get
                Return _AssociationType
            End Get
            Set(ByVal Value As Long)
                _AssociationType = Value
            End Set
        End Property

        Public Property AssociationFrom() As Long
            Get
                Return _AssociationFrom
            End Get
            Set(ByVal Value As Long)
                _AssociationFrom = Value
            End Set
        End Property

        Public Property AssociationID() As Long
            Get
                Return _AssociationID
            End Get
            Set(ByVal Value As Long)
                _AssociationID = Value
            End Set
        End Property

        Public Property KeyWord() As String
            Get
                Return _ToDate
            End Get
            Set(ByVal Value As String)
                _KeyWord = Value
            End Set
        End Property

        Public Property BitTask() As Short
            Get
                Return _BitTask
            End Get
            Set(ByVal Value As Short)
                _BitTask = Value
            End Set
        End Property

        Public Property ToDate() As Date
            Get
                Return _ToDate
            End Get
            Set(ByVal Value As Date)
                _ToDate = Value
            End Set
        End Property

        Public Property FromDate() As Date
            Get
                Return _FromDate
            End Get
            Set(ByVal Value As Date)
                _FromDate = Value
            End Set
        End Property

        Public Property TotalRecords() As Integer
            Get
                Return _TotalRecords
            End Get
            Set(ByVal Value As Integer)
                _TotalRecords = Value
            End Set
        End Property

        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal Value As Integer)
                _PageSize = Value
            End Set
        End Property

        Public Property CurrentPage() As Integer
            Get
                Return _CurrentPage
            End Get
            Set(ByVal Value As Integer)
                _CurrentPage = Value
            End Set
        End Property


        Public Property OppType() As Short
            Get
                Return _OppType
            End Get
            Set(ByVal Value As Short)
                _OppType = Value
            End Set
        End Property


        Public Property OppStatus() As Short
            Get
                Return _OppStatus
            End Get
            Set(ByVal Value As Short)
                _OppStatus = Value
            End Set
        End Property


        Public Property ProStatus() As Short
            Get
                Return _ProStatus
            End Get
            Set(ByVal Value As Short)
                _ProStatus = Value
            End Set
        End Property


        Public Property ContactId() As Long
            Get
                Return _ContactId
            End Get
            Set(ByVal Value As Long)
                _ContactId = Value
            End Set
        End Property


        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property


        Public Property PublicFlag() As Short
            Get
                Return _PublicFlag
            End Get
            Set(ByVal Value As Short)
                _PublicFlag = Value
            End Set
        End Property


        Public Property ModifiedBy() As Long
            Get
                Return _ModifiedBy
            End Get
            Set(ByVal Value As Long)
                _ModifiedBy = Value
            End Set
        End Property


        Public Property CreatedBy() As Long
            Get
                Return _CreatedBy
            End Get
            Set(ByVal Value As Long)
                _CreatedBy = Value
            End Set
        End Property


        Public Property Profile() As Long
            Get
                Return _Profile
            End Get
            Set(ByVal Value As Long)
                _Profile = Value
            End Set
        End Property


        Public Property InfoSource() As Long
            Get
                Return _InfoSource
            End Get
            Set(ByVal Value As Long)
                _InfoSource = Value
            End Set
        End Property


        Public Property NoofEmp() As Long
            Get
                Return _NoofEmp
            End Get
            Set(ByVal Value As Long)
                _NoofEmp = Value
            End Set
        End Property


        Public Property AnnualRev() As Long
            Get
                Return _AnnualRev
            End Get
            Set(ByVal Value As Long)
                _AnnualRev = Value
            End Set
        End Property


        Public Property RelatedDoc() As String
            Get
                Return _RelatedDoc
            End Get
            Set(ByVal Value As String)
                _RelatedDoc = Value
            End Set
        End Property


        Public Property WebLink4() As String
            Get
                Return _WebLink4
            End Get
            Set(ByVal Value As String)
                _WebLink4 = Value
            End Set
        End Property


        Public Property WebLabel4() As String
            Get
                Return _WebLabel4
            End Get
            Set(ByVal Value As String)
                _WebLabel4 = Value
            End Set
        End Property


        Public Property WebLink3() As String
            Get
                Return _WebLink3
            End Get
            Set(ByVal Value As String)
                _WebLink3 = Value
            End Set
        End Property


        Public Property WebLabel3() As String
            Get
                Return _WebLabel3
            End Get
            Set(ByVal Value As String)
                _WebLabel3 = Value
            End Set
        End Property


        Public Property WebLink2() As String
            Get
                Return _WebLink2
            End Get
            Set(ByVal Value As String)
                _WebLink2 = Value
            End Set
        End Property


        Public Property WebLabel2() As String
            Get
                Return _WebLabel2
            End Get
            Set(ByVal Value As String)
                _WebLabel2 = Value
            End Set
        End Property


        Public Property WebLink1() As String
            Get
                Return _WebLink1
            End Get
            Set(ByVal Value As String)
                _WebLink1 = Value
            End Set
        End Property


        Public Property WebLabel1() As String
            Get
                Return _WebLabel1
            End Get
            Set(ByVal Value As String)
                _WebLabel1 = Value
            End Set
        End Property


        Public Property WebSite() As String
            Get
                Return _WebSite
            End Get
            Set(ByVal Value As String)
                _WebSite = Value
            End Set
        End Property


        Public Property Comments() As String
            Get
                Return _Comments
            End Get
            Set(ByVal Value As String)
                _Comments = Value
            End Set
        End Property


        Public Property SICCode() As String
            Get
                Return _SICCode
            End Get
            Set(ByVal Value As String)
                _SICCode = Value
            End Set
        End Property


        Public Property Credit() As Long
            Get
                Return _Credit
            End Get
            Set(ByVal Value As Long)
                _Credit = Value
            End Set
        End Property



        Public Property ComInd() As Long
            Get
                Return _ComInd
            End Get
            Set(ByVal Value As Long)
                _ComInd = Value
            End Set
        End Property


        Public Property TerId() As Long
            Get
                Return _TerId
            End Get
            Set(ByVal Value As Long)
                _TerId = Value
            End Set
        End Property


        Public Property RatingID() As Long
            Get
                Return _RatingID
            End Get
            Set(ByVal Value As Long)
                _RatingID = Value
            End Set
        End Property


        Public Property ByteMode() As Short
            Get
                Return _ByteMode
            End Get
            Set(ByVal Value As Short)
                _ByteMode = Value
            End Set
        End Property


        Public Property CustName() As String
            Get
                Return _CustName
            End Get
            Set(ByVal Value As String)
                _CustName = Value
            End Set
        End Property
        Public Property FirstName() As String
            Get
                Return _FirstName
            End Get
            Set(ByVal Value As String)
                _FirstName = Value
            End Set
        End Property
        Public Property LastName() As String
            Get
                Return _LastName
            End Get
            Set(ByVal Value As String)
                _LastName = Value
            End Set
        End Property

        Public Property CompanyID() As Long
            Get
                Return _CompanyID
            End Get
            Set(ByVal Value As Long)
                _CompanyID = Value
            End Set
        End Property
        Public Property CRMType() As Integer
            Get
                Return _CRMType
            End Get
            Set(ByVal Value As Integer)
                _CRMType = Value
            End Set
        End Property
        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property

        'Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal Value As Long)
        '        UserCntID = Value
        '    End Set
        'End Property

        Public Property UserRightType() As Long
            Get
                Return _UserRightType
            End Get
            Set(ByVal Value As Long)
                _UserRightType = Value
            End Set
        End Property
        Public Property SortOrder() As Long
            Get
                Return _SortOrder
            End Get
            Set(ByVal Value As Long)
                _SortOrder = Value
            End Set
        End Property
        Public Property LastDate() As Long
            Get
                Return _LastDate
            End Get
            Set(ByVal Value As Long)
                _LastDate = Value
            End Set
        End Property
        Public Property SortChar() As Char
            Get
                Return _SortChar
            End Get
            Set(ByVal Value As Char)
                _SortChar = Value
            End Set
        End Property
        Public Property RelType() As Long
            Get
                Return _RelType
            End Get
            Set(ByVal Value As Long)
                _RelType = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Local (Client) Time Zone Offset from GMT .
        ''' </summary>
        ''' <remarks>
        '''     This holds the Client Time Offset which is being edited/ deleted
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/08/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _ClientTimeZoneOffset As Int32
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Manages the Private Property representing the Local (Client) Time Zone Offset from GMT .
        ''' </summary>
        ''' <remarks>
        '''     
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/08/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property ClientTimeZoneOffset() As Int32
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal Value As Int32)
                _ClientTimeZoneOffset = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Stores the flag to indicate that the target organization is the Parent Organization.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Parent Organization Flag.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/30/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _boolParentOrg As Boolean
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Gets/ Sets the Parent Organization Flag.
        ''' </summary>
        ''' <value>Returns the Parent Organization Flag.</value>
        ''' <remarks>
        '''     This property is used to Get/ Set the Parent Organization Flag. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	11/03/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property BoolParentOrg() As Boolean
            Get
                Return _boolParentOrg
            End Get
            Set(ByVal Value As Boolean)
                _boolParentOrg = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Stores the flag to indicate that the target organization is the ChildOrganization.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Child Organization Flag.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/02/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _boolChildOrg As Boolean
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Gets/ Sets the Child Organization Flag.
        ''' </summary>
        ''' <value>Returns the Child Organization Flag.</value>
        ''' <remarks>
        '''     This property is used to Get/ Set the Child Organization Flag. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/02/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property BoolChildOrg() As Boolean
            Get
                Return _boolChildOrg
            End Get
            Set(ByVal Value As Boolean)
                _boolChildOrg = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the list of Divisions which are being associated with another parent organization/ division.
        ''' </summary>
        ''' <remarks>
        '''     This holds the list of Division Ids.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/30/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _sDivisionIdList As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Gets/ Sets the list of Division Ids.
        ''' </summary>
        ''' <value>Returns the list of Division Ids as a string.</value>
        ''' <remarks>
        '''     This property is used to Get the list of Division ids. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/30/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property DivisionIdList() As String
            Get
                Return _sDivisionIdList
            End Get
            Set(ByVal Value As String)
                _sDivisionIdList = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the list of Contact which are being merged with/ copied to another parent organization/ division.
        ''' </summary>
        ''' <remarks>
        '''     This holds the list of Contact Ids.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	01/16/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _sContactIdList As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Gets/ Sets the list of Contact Ids.
        ''' </summary>
        ''' <value>Returns the list of Contact Ids as a string.</value>
        ''' <remarks>
        '''     This property is used to Get the list of Contact ids. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	01/16/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property ContactIdList() As String
            Get
                Return _sContactIdList
            End Get
            Set(ByVal Value As String)
                _sContactIdList = Value
            End Set
        End Property

        Private _ProjectStatus As Long
        Public Property ProjectStatus() As Long
            Get
                Return _ProjectStatus
            End Get
            Set(ByVal Value As Long)
                _ProjectStatus = Value
            End Set
        End Property

        Private _UserGroupID As Long
        Public Property UserGroupID() As Long
            Get
                Return _UserGroupID
            End Get
            Set(ByVal Value As Long)
                _UserGroupID = Value
            End Set
        End Property

        Private _strTransactionType As String
        Public Property strTransactionType() As String
            Get
                Return _strTransactionType
            End Get
            Set(ByVal Value As String)
                _strTransactionType = Value
            End Set
        End Property

        Public Function fn_GetCalenderName(ByVal CommunicationID As Integer) As String
            '================================================================================
            ' Purpose: This function is used for getting the user names from the user master
            '
            ' History
            ' Ver   Date        Ref     Author              Reason
            ' 1     17/02/2003          Madhan Kumar.V         Created
            '
            ' Non Compliance (any deviation from standards)
            '   No deviations from the standards.
            '================================================================================
            'Create a Command Object to Select the Columns
            Dim cnn As New Npgsql.NpgSqlConnection(GetConnection.GetConnectionString)
            If cnn.State = ConnectionState.Closed Then cnn.Open()
            Dim CmdGroup As Npgsql.NpgsqlCommand = New Npgsql.NpgsqlCommand("SELECT vcCalendarName FROM Communication WHERE numCommId=" & CommunicationID, cnn)
            'Create a Reader object from the Command
            Dim drdrUser As Npgsql.NpgsqlDataReader = CmdGroup.ExecuteReader()

            Try
                If drdrUser.Read() Then
                    If Not IsDBNull(drdrUser("vcCalendarName")) Then
                        Return CStr(drdrUser("vcCalendarName"))
                    Else
                        Return ""
                    End If

                End If
            Catch Exc As Exception
                Throw New Exception("Error while dumping users", Exc)
            Finally
                drdrUser.Close()
                CmdGroup.Dispose()
                cnn.Close()
            End Try
        End Function



        Public Function GetContactInfo1() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = UserCntID

                arParms(3) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _ClientTimeZoneOffset

                arParms(4) = New Npgsql.NpgsqlParameter("@numUserGroupID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = UserGroupID

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_ProsContacts", arParms)
                Return ds

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetOppDetailsForOrg(Optional ByVal intQuarter As Integer = 0, Optional ByVal intYear As Integer = 0) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(12) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@bytemode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = _ByteMode

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DivisionID

                arParms(2) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ContactId

                arParms(3) = New Npgsql.NpgsqlParameter("@tintOppStatus", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _OppStatus

                arParms(4) = New Npgsql.NpgsqlParameter("@tintOppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _OppType

                arParms(5) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = DomainID

                arParms(6) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = _CurrentPage

                arParms(7) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(7).Value = _PageSize

                arParms(8) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(8).Direction = ParameterDirection.InputOutput
                arParms(8).Value = _TotalRecords

                arParms(9) = New Npgsql.NpgsqlParameter("@intQuarter", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(9).Value = intQuarter

                arParms(10) = New Npgsql.NpgsqlParameter("@intYear", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(10).Value = intYear

                arParms(11) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(11).Value = Nothing
                arParms(11).Direction = ParameterDirection.InputOutput

                arParms(12) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(12).Value = Nothing
                arParms(12).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "usp_ProsOppDetails", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(8).Value)

                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetActionItems() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(11) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@bytemode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = _ByteMode

                arParms(1) = New Npgsql.NpgsqlParameter("@numdivisionid", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DivisionID

                arParms(2) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ContactId

                arParms(3) = New Npgsql.NpgsqlParameter("@bitTask", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _BitTask

                arParms(4) = New Npgsql.NpgsqlParameter("@FromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(4).Value = _FromDate

                arParms(5) = New Npgsql.NpgsqlParameter("@ToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(5).Value = _ToDate

                arParms(6) = New Npgsql.NpgsqlParameter("@KeyWord", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(6).Value = _KeyWord

                arParms(7) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(7).Value = _CurrentPage

                arParms(8) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(8).Value = _PageSize

                arParms(9) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer) 'Param added by Deb (Since Date and Time must be displayed w.r.t. the client machine)
                arParms(9).Value = ClientTimeZoneOffset

                arParms(10) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(10).Direction = ParameterDirection.InputOutput
                arParms(10).Value = _TotalRecords

                arParms(11) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(11).Value = Nothing
                arParms(11).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_ProsActItems", arParms)
                _TotalRecords = arParms(10).Value
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetProjectsForOrg() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numProjectStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ProjectStatus

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _ClientTimeZoneOffset

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_ProsProject", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetCompanyInfoForEdit() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _ClientTimeZoneOffset

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "usp_GetCompanyInfo", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetCompanyPurchaseIncentives() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "usp_GetPurchaseIncentives", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function ManageCompanyPurchaseIncentives() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@strXML", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = strCompanyTaxTypes

                SqlDAL.ExecuteDataset(connString, "usp_ManagePurchaseIncentives", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function GetCompanyTaxTypes() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_CompanyTaxTypes", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function



        Public Function ManageCompanyTaxTypes() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@strCompTaxTypes", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(1).Value = _strCompanyTaxTypes

                SqlDAL.ExecuteDataset(connString, "USP_ManageComapnyTaxTypes", arParms)

                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        'Public Function DeleteContactById() As Boolean
        '    Try

        '        Dim getconnection As New getconnection
        '        Dim connString As String = getconnection.GetConnectionString
        '        Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

        '        arParms(0) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(0).Value = _ContactId

        '        If CInt(SqlDAL.ExecuteScalar(connString, "usp_DeleteContact", arParms)) = 1 Then
        '            Return True
        '        Else
        '            Return False
        '        End If
        '    Catch ex As Exception
        '        'Return False
        '        Throw ex
        '    End Try
        'End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is called to insert new association information, if it does not exists
        ''' </summary>
        ''' <remarks>
        '''     Deb Note: Removed the paraneter numContactId from the input parameters to this SP, and added 
        '''     a parameter to indicate if the Target Company is a Parent Org or not
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/30/2005	Modified
        ''' </history>
        '''-----------------------------------------------------------------------------
        Public Function ManageAssociation() As String
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(14) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numAssociationID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _AssociationID

                arParms(1) = New Npgsql.NpgsqlParameter("@numAssociatedFromDivID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _AssociationFrom

                arParms(2) = New Npgsql.NpgsqlParameter("@numCompID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _CompanyID

                arParms(3) = New Npgsql.NpgsqlParameter("@numDivID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _DivisionID

                arParms(4) = New Npgsql.NpgsqlParameter("@bitParentOrg", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(4).Value = BoolParentOrg

                arParms(5) = New Npgsql.NpgsqlParameter("@bitChildOrg", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(5).Value = BoolChildOrg

                arParms(6) = New Npgsql.NpgsqlParameter("@numTypeID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _AssociationType

                arParms(7) = New Npgsql.NpgsqlParameter("@bitDeleted", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(7).Value = (_bitDeleted = 1)

                arParms(8) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = UserCntID

                arParms(9) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = DomainID

                arParms(10) = New Npgsql.NpgsqlParameter("@bitShareportal", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(10).Value = _bitSharePortal

                arParms(11) = New Npgsql.NpgsqlParameter("@bitTypeLabel", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(11).Value = (_HeaderLink = 1)

                arParms(12) = New Npgsql.NpgsqlParameter("@numAssoTypeLabel", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(12).Value = _AssociationTypeForLabel

                arParms(13) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(13).Value = _ContactId

                arParms(14) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(14).Value = Nothing
                arParms(14).Direction = ParameterDirection.InputOutput

                Return CStr(SqlDAL.ExecuteScalar(connString, "usp_ManageAssociations", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteAssociation() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numAssociationID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _AssociationID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID


                SqlDAL.ExecuteNonQuery(connString, "usp_DeleteAssociations", arParms)

                Return True

            Catch ex As Exception
                Return False
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is called to get the company with which the currect company is associated
        ''' </summary>
        ''' <remarks>
        '''     Deb Note: Removed the paraneter numContactId from the input parameters to this SP
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/30/2005	Modified
        ''' </history>
        '''-----------------------------------------------------------------------------
        Public Function GetAssociationTo() As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim ds As DataSet
                arParms(0) = New Npgsql.NpgsqlParameter("@numDivID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@bitGetAssociatedTo", NpgsqlTypes.NpgsqlDbType.Boolean)
                arParms(1).Value = _bitAssociatedTo

                arParms(2) = New Npgsql.NpgsqlParameter("@numAssociationId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _AssociationID

                arParms(3) = New Npgsql.NpgsqlParameter("@numDominId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = DomainID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetAssociations", arParms)

                Return ds

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is called to Merge/ Copy the contacts from one parent organization to another
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	01/16/2006	Created
        ''' </history>
        '''-----------------------------------------------------------------------------
        Public Function CopyOrMergeContacts(ByVal vcAction As Char)
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                arParms(0) = New Npgsql.NpgsqlParameter("@vcContactIDs", NpgsqlTypes.NpgsqlDbType.VarChar, 400)
                arParms(0).Value = ContactIdList

                arParms(1) = New Npgsql.NpgsqlParameter("@numCompanyId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = CompanyID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DivisionID

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@vcAction", NpgsqlTypes.NpgsqlDbType.Char)
                arParms(4).Value = vcAction

                SqlDAL.ExecuteNonQuery(connString, "usp_MergeOrCopyContacts", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is called to Merge/ Copy the contacts from one parent organization to another
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	01/16/2006	Created
        ''' </history>
        '''-----------------------------------------------------------------------------
        Public Function SyncDivAndCompOfMergedContacts()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DivisionID

                SqlDAL.ExecuteNonQuery(connString, "usp_DeleteDivisionAndCompany", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to get the respondents DivisionId, CRMType and Company Id
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <param name="numContactId">The current contact id</param>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	05/28/2006	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function GetContactInfoForURLRedirection(ByVal numContactId As Integer) As DataTable
            Try
                Dim ds As DataSet                                                   'declare a dataset
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numEntityId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = numContactId

                arParms(1) = New Npgsql.NpgsqlParameter("@vcEntityType", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(1).Value = "Cont"

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetContactInfoForURLRedirection", arParms)  'execute amd delete teh survey response
                Return ds.Tables(0)
            Catch ex As Exception
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is called to get the list of contacts for the selected division
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	05/26/2006	Created
        ''' </history>
        '''-----------------------------------------------------------------------------
        Public Function GetContactsForTheDivision() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim ds As DataSet
                arParms(0) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = 0

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DivisionID

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetContactWithoutPrimaryCnt", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetTransactionDetails() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}
                Dim ds As DataSet
                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DivisionID

                arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _ClientTimeZoneOffset

                arParms(3) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _CurrentPage

                arParms(4) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _PageSize

                arParms(5) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Direction = ParameterDirection.Output
                arParms(5).Value = _TotalRecords

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_CompanyTransactionDetails", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(5).Value)

                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetDivisionBillPaymentDetails() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DivisionID

                arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _ClientTimeZoneOffset

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetDivisionBillPaymentDetails", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetOrganizationTransaction() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(10) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _CurrentPage

                arParms(3) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _PageSize

                arParms(4) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Direction = ParameterDirection.InputOutput
                arParms(4).Value = _TotalRecords

                arParms(5) = New Npgsql.NpgsqlParameter("@vcTransactionType", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(5).Value = _strTransactionType

                arParms(6) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(6).Value = _FromDate

                arParms(7) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(7).Value = _ToDate

                arParms(8) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(8).Value = _ClientTimeZoneOffset

                arParms(9) = New Npgsql.NpgsqlParameter("@SortCreatedDate", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(9).Value = _SortOrder

                arParms(10) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(10).Value = Nothing
                arParms(10).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                Dim objParam() As Object = arParms.ToArray()
                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetOrganizationTransaction", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(4).Value)

                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace

