﻿Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRM.BusinessLogic.Common
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.ItemShipping
    Public Class ShippingReport
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"
        Public Property ShippingReportID As Long
        Public Property OppID As Long
        Public Property OppBizDocId As Long
        Public Property ShipVia As Long
        Public Property ShipService As Long
        Public Property IsDelivered As Boolean
        Public Property TrackingDetail As String
#End Region

#Region "Public Methods"

        Public Function Save() As Long
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = OppID

                arParms(3) = New Npgsql.NpgsqlParameter("@numOppBizDocId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = OppBizDocId

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return CCommon.ToLong(SqlDAL.ExecuteScalar(connString, "USP_ShippingReport_Save", arParms))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function SaveMassSalesFilfillment(ByVal vcBoxes As String, ByVal vcBoxItems As String, ByVal vcShippingDetail As String) As Long
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", OppID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppBizDocId", OppBizDocId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numShippingCompany", ShipVia, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numShippingService", ShipService, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcBoxes", vcBoxes, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcBoxItems", vcBoxItems, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcShippingDetail", vcShippingDetail, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return CCommon.ToLong(SqlDAL.ExecuteScalar(connString, "USP_ShippingReport_SaveMassSalesFulfillment", sqlParams.ToArray()))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub UpdateShippingReportMassSalesFilfillment(ByVal vcShippingDetail As String)
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", OppID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numShippingReportID", ShippingReportID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcShippingDetail", vcShippingDetail, NpgsqlTypes.NpgsqlDbType.VarChar))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_ShippingReport_UpdateMassSalesFulfillment", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetForTrackingDetailUpdate() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(0).Value = Nothing
                arParms(0).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_ShippingReport_GetForTrackingDetailUpdate", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function UpdateTrackingDetail() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numShippingReportID", ShippingReportID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitDelivered", IsDelivered, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@vcTrackingDetail", TrackingDetail, NpgsqlTypes.NpgsqlDbType.VarChar))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_ShippingReport_UpdateTrackingDetail", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function
#End Region

    End Class
End Namespace