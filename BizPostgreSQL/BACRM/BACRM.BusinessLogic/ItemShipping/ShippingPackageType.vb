﻿Imports System.Collections.Generic
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.ItemShipping
    Public Class ShippingPackageType
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"

        Public Property Id As String
        Public Property ShippingCompanyID As Long
        Public Property NsoftwarePackageID As Long
        Public Property vcPackageName As String
        Public Property IsEndicia As Boolean

#End Region

#Region "Public Methods"

        Public Function GetShippingPackageTypeByShippingCompany() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numShippingCompanyID", ShippingCompanyID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitEndicia", IsEndicia, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_ShippingPackageType_GetByShippingCompany", sqlParams.ToArray())

            Catch ex As Exception
                Throw
            End Try
        End Function

#End Region

    End Class
End Namespace
