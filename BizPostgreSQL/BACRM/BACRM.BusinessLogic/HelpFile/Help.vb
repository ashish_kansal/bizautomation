﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Net
Imports System.IO
Imports System.Web.UI
Imports System.Web
Imports System.Configuration
Imports BACRM.BusinessLogic.Item

Imports System.Text
'Imports System.IO
Imports System.Web.Mail
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Leads
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports BACRM.BusinessLogic.Common

Namespace BACRM.BusinessLogic.Help


    Public Class Help
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _helpID As Long
        Public Property HelpID() As Long
            Get
                Return _helpID
            End Get
            Set(ByVal value As Long)
                _helpID = value
            End Set
        End Property

        Private _helpCategory As Long
        Public Property HelpCategory() As Long
            Get
                Return _helpCategory
            End Get
            Set(ByVal value As Long)
                _helpCategory = value
            End Set
        End Property
        Private _helpHeader As String
        Public Property HelpHeader() As String
            Get
                Return _helpHeader
            End Get
            Set(ByVal value As String)
                _helpHeader = value
            End Set
        End Property
        Private _helpMetatag As String
        Public Property HelpMetatag() As String
            Get
                Return _helpMetatag
            End Get
            Set(ByVal value As String)
                _helpMetatag = value
            End Set
        End Property
        Private _helpDescription As String
        Public Property HelpDescription() As String
            Get
                Return _helpDescription
            End Get
            Set(ByVal value As String)
                _helpDescription = value
            End Set
        End Property
        Private _helpPageUrl As String
        Public Property HelpPageUrl() As String
            Get
                Return _helpPageUrl
            End Get
            Set(ByVal value As String)
                _helpPageUrl = value
            End Set
        End Property

        Private _helpshortDescription As String
        Public Property HelpShortDescription() As String
            Get
                Return _helpshortDescription
            End Get
            Set(ByVal value As String)
                _helpshortDescription = value
            End Set
        End Property


        Private _HelpLinkingPageURL As String
        Public Property HelpLinkingPageURL() As String
            Get
                Return _HelpLinkingPageURL
            End Get
            Set(ByVal value As String)
                _HelpLinkingPageURL = value
            End Set
        End Property

        Private _CategoryID As Long
        Public Property CategoryID() As Long
            Get
                Return _CategoryID
            End Get
            Set(ByVal value As Long)
                _CategoryID = value
            End Set
        End Property

        Private _CategoryName As String
        Public Property CategoryName() As String
            Get
                Return _CategoryName
            End Get
            Set(ByVal value As String)
                _CategoryName = value
            End Set
        End Property

        Private _LinkPage As Boolean
        Public Property LinkPage() As Boolean
            Get
                Return _LinkPage
            End Get
            Set(ByVal value As Boolean)
                _LinkPage = value
            End Set
        End Property

        Private _ParentCateID As Long
        Public Property ParentCatID() As Long
            Get
                Return _ParentCateID
            End Get
            Set(ByVal value As Long)
                _ParentCateID = value
            End Set
        End Property

        Private _level As Short
        Public Property Level() As Short
            Get
                Return _level
            End Get
            Set(ByVal value As Short)
                _level = value
            End Set
        End Property

        Private _StrItems As String
        Public Property StrItems() As String
            Get
                Return _StrItems
            End Get
            Set(ByVal value As String)
                _StrItems = value
            End Set
        End Property
        Private _numAccessId As Long
        Public Property numAccessId As Long
            Get
                Return _numAccessId
            End Get
            Set(ByVal value As Long)
                _numAccessId = value
            End Set
        End Property

        Public Function GethelpDataByID(ByVal iHelpID As Integer) As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@helpID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = iHelpID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "Usp_GetHelpByID", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GethelpDataByCategory() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@helpcategory", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CategoryID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "Usp_GetHelpByCategory", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GethelpDataByPageURl(ByVal strpageurl As String) As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@pageUrl", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(0).Value = strpageurl

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "Usp_GetHelpByPageUrl", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function SaveHelpDisplayOrder() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(0).Value = _StrItems

                Return SqlDAL.ExecuteNonQuery(connString, "USP_HelpDisplayOrder", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetHelpCategory(ByVal strSearch As String, ByVal intSearchType As Int32, Optional ByVal intHelpId As Integer = 0) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@Search", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(0).Value = strSearch

                arParms(1) = New Npgsql.NpgsqlParameter("@intSearchType", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = intSearchType

                arParms(2) = New Npgsql.NpgsqlParameter("@numHelpId", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = intHelpId

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_HELPCATEGORY", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GethelpDataBysearch(ByVal strsearchdata As String) As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@strsearch", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(0).Value = strsearchdata

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_SearchHelp", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function InsertHelpData() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numhelpID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Direction = ParameterDirection.Output
                arParms(0).Value = _helpID

                arParms(1) = New Npgsql.NpgsqlParameter("@helpcategory", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _helpCategory

                arParms(2) = New Npgsql.NpgsqlParameter("@helpheader", NpgsqlTypes.NpgsqlDbType.VarChar, 400)
                arParms(2).Value = _helpHeader

                arParms(3) = New Npgsql.NpgsqlParameter("@helpMetatag", NpgsqlTypes.NpgsqlDbType.VarChar, 400)
                arParms(3).Value = _helpMetatag

                arParms(4) = New Npgsql.NpgsqlParameter("@helpDescription", NpgsqlTypes.NpgsqlDbType.VarChar, -1)
                arParms(4).Value = _helpDescription

                arParms(5) = New Npgsql.NpgsqlParameter("@helpPageUrl", NpgsqlTypes.NpgsqlDbType.VarChar, 400)
                arParms(5).Value = _helpPageUrl

                arParms(6) = New Npgsql.NpgsqlParameter("@helpshortdesc", NpgsqlTypes.NpgsqlDbType.VarChar, 400)
                arParms(6).Value = _helpshortDescription

                arParms(7) = New Npgsql.NpgsqlParameter("@helpLinkingPageURL", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(7).Value = _HelpLinkingPageURL

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_AddHelp", objParam, True)

                _helpID = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)
                If _helpID <= 0 Then
                    Return False
                End If
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function UpdateHelpData() As Boolean

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numhelpID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _helpID

                arParms(1) = New Npgsql.NpgsqlParameter("@helpcategory", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _helpCategory

                arParms(2) = New Npgsql.NpgsqlParameter("@helpMetatag", NpgsqlTypes.NpgsqlDbType.VarChar, 400)
                arParms(2).Value = _helpMetatag

                arParms(3) = New Npgsql.NpgsqlParameter("@helpheader", NpgsqlTypes.NpgsqlDbType.Varchar, 400)
                arParms(3).Value = _helpHeader

                arParms(4) = New Npgsql.NpgsqlParameter("@helpDescription", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(4).Value = _helpDescription

                arParms(5) = New Npgsql.NpgsqlParameter("@helpshortdesc", NpgsqlTypes.NpgsqlDbType.VarChar, 400)
                arParms(5).Value = _helpshortDescription

                arParms(6) = New Npgsql.NpgsqlParameter("@helpLinkingPageURL", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(6).Value = _HelpLinkingPageURL

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateHelp", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function DeleteHelpData() As Boolean

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numhelpID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _helpID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteHelp", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageHelpCategory() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numHelpCategoryID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _CategoryID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcCatecoryName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(1).Value = _CategoryName

                arParms(2) = New Npgsql.NpgsqlParameter("@bitLinkPage", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(2).Value = _LinkPage

                arParms(3) = New Npgsql.NpgsqlParameter("@numPageID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _helpID

                arParms(4) = New Npgsql.NpgsqlParameter("@numParentCatID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _ParentCateID

                arParms(5) = New Npgsql.NpgsqlParameter("@tintLevel", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _level

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageHelpCategories", objParam, True)

                _CategoryID = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)
                If _CategoryID <= 0 Then
                    Return False
                End If
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private _byteMode As Short
        Public Property byteMode() As Short
            Get
                Return _byteMode
            End Get
            Set(ByVal value As Short)
                _byteMode = value
            End Set
        End Property

        Public Function GethelpCategories() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numHelpCategoryID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CategoryID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _byteMode

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetHelpCategories", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function DeleteHelpCategory() As Boolean

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numHelpCategoryID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CategoryID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteHelpCategories", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetHelpLogin() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numAppAccessId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _numAccessId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_CheckHelpLogin", arParms)
                If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 AndAlso CCommon.ToBool(ds.Tables(0).Rows(0)(0)) = True Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace