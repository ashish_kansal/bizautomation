'**********************************************************************************
' <CBusinessBase.vb>
' 
' 	CHANGE CONTROL:
'	
'	AUTHOR: Goyal 	DATE:28-Feb-05 	VERSION	CHANGES	KEYSTRING:
'**********************************************************************************

Option Explicit On
Option Strict On


Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Runtime.Serialization
Imports Npgsql

Namespace BACRM.BusinessLogic

    '**********************************************************************************
    ' Module Name  : None
    ' Module Type  : CBusinessBase
    ' 
    ' Description  : This is the base class for the business logic classes in the BIZ applicaiton
    '**********************************************************************************
    Public Class CBusinessBase
        Private __DomainId As Long
        Public Property DomainID() As Long
            Get
                Return __DomainId
            End Get
            Set(ByVal value As Long)
                __DomainId = value
            End Set
        End Property



        Private __UserCntID As Long
        Public Property UserCntID() As Long
            Get
                Return __UserCntID
            End Get
            Set(ByVal value As Long)
                __UserCntID = value
            End Set
        End Property

        Private _Status As String
        Public Property Status() As String
            Get
                Return _Status
            End Get
            Set(ByVal value As String)
                _Status = value
            End Set
        End Property

        Private _GradeIds As String
        Public Property GradeIds() As String
            Get
                Return _GradeIds
            End Get
            Set(ByVal value As String)
                _GradeIds = value
            End Set
        End Property

        Private _CreatedBy As Int64
        Public Property CreatedBy() As Int64
            Get
                Return _CreatedBy
            End Get
            Set(ByVal value As Int64)
                _CreatedBy = value
            End Set
        End Property

        Private _vcAssignedIds As String
        Public Property vcAssignedIds() As String
            Get
                Return _vcAssignedIds
            End Get
            Set(ByVal value As String)
                _vcAssignedIds = value
            End Set
        End Property

        Private _numStageDetailsGradeId As Int64
        Public Property numStageDetailsGradeId() As Int64
            Get
                Return _numStageDetailsGradeId
            End Get
            Set(ByVal value As Int64)
                _numStageDetailsGradeId = value
            End Set
        End Property

        '        Protected _intUserId As Long
        '        Protected _dtTimeStamp As DateTime
        '        Protected _strConnString As String

        '#Region "Constructors"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Protected
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32
        '        '               
        '        ' Description  : The constructor
        '        '                
        '        ' Notes        : N/A
        '        '                
        '        ' Created By   : Goyal 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Protected Sub New(ByVal intUserId As Int32)
        '            _intUserId = intUserId
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Protected
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32, ByVal strConnString As String
        '        '               
        '        ' Description  : The constructor
        '        '                
        '        ' Notes        : N/A
        '        '                
        '        ' Created By   : Goyal 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Protected Sub New(ByVal intUserId As Int32, ByVal strConnString As String)
        '            _strConnString = strConnString
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Protected
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal strConnString As String
        '        '               
        '        ' Description  : The constructor
        '        '                
        '        ' Notes        : N/A
        '        '                
        '        ' Created By   : Goyal 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Protected Sub New(ByVal strConnString As String)
        '            _strConnString = strConnString
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Protected
        '        ' Returns      : N/A
        '        ' Parameters   : 
        '        '               
        '        ' Description  : The constructor
        '        '                
        '        ' Notes        : N/A
        '        '                
        '        ' Created By   : Goyal 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Protected Sub New()

        '        End Sub
        '#End Region

        '#Region "Properties"

        '        'Property to set and the retrun value of the user id
        '        <DataMember()> _
        '        Public Property CurrentUserId() As Long
        '            Get
        '                Return _intUserId
        '            End Get
        '            Set(ByVal Value As Long)
        '                _intUserId = Value
        '            End Set
        '        End Property

        '        'Property to set and the retrun value of the user id
        '        Public ReadOnly Property ConnectionString() As String
        '            Get
        '                If _strConnString = "" Then
        '                    Return ConfigurationSettings.AppSettings("ConnectionString")
        '                Else
        '                    Return _strConnString
        '                End If

        '            End Get
        '            'Set(ByVal Value As String)
        '            '    _strConnString = Value
        '            'End Set
        '        End Property

        '        'Property to set and the retrun value of the time stamp
        '        <DataMember()> _
        '        Public Property TimeStamp() As String
        '            Get
        '                Return ServerCurrentDate.ToString("MM/dd/yyyy HH:mm:ss.fff")
        '            End Get
        '            Set(ByVal Value As String)
        '                ServerCurrentDate = Convert.ToDateTime(Value)
        '            End Set
        '        End Property

        '        'Property to set and the retrun value of the Server current date
        '        <DataMember()> _
        '        Public Property ServerCurrentDate() As DateTime
        '            Get
        '                'Return the datetime
        '                Return _dtTimeStamp
        '            End Get
        '            Set(ByVal Value As DateTime)
        '                If (Value <> #12:00:00 AM#) Then
        '                    'Set the value only if some valid value is provided
        '                    _dtTimeStamp = Value
        '                End If
        '            End Set
        '        End Property
        '#End Region

        <CLSCompliant(False)>
        Public Function BeginTransaction() As NpgsqlTransaction
            Try
                Dim getconnection As New BACRMBUSSLOGIC.BussinessLogic.GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim conn As New NpgsqlConnection(connString)
                conn.Open()
                Dim objTransaction As NpgsqlTransaction = conn.BeginTransaction()
                Return objTransaction
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Sub CommitTransaction(objTransaction As NpgsqlTransaction)
            Try
                If Not objTransaction Is Nothing Then
                    objTransaction.Commit()
                    If Not objTransaction.Connection Is Nothing Then
                        If objTransaction.Connection.State = ConnectionState.Open Then
                            objTransaction.Connection.Close()
                        End If
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Sub RollbackTransaction(objTransaction As NpgsqlTransaction)
            Try
                If Not objTransaction Is Nothing Then
                    objTransaction.Rollback()
                    If Not objTransaction.Connection Is Nothing Then
                        If objTransaction.Connection.State = ConnectionState.Open Then
                            objTransaction.Connection.Close()
                        End If
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
    End Class

End Namespace

