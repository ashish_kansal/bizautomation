'**********************************************************************************
' <CLeads.vb>
' 
' 	CHANGE CONTROL:
'	
'	AUTHOR: Goyal 	DATE:28-Feb-05 	VERSION	CHANGES	KEYSTRING:
'**********************************************************************************


Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Xml.Serialization
Imports System.Runtime.Serialization
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.Leads

    '**********************************************************************************
    ' Module Name  : None
    ' Module Type  : CCommon
    ' 
    ' Description  : This is the business logic classe for Common Stuff
    '**********************************************************************************
    <DataContract()> _
    Public Class CLeads
        Inherits BACRM.BusinessLogic.CBusinessBase


        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Goyal 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Goyal 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region

        Private _GroupID As Long = 0
        Private _UserRightType As Long = 0
        Private _DivisionID As Long = 0
        Private _CompanyID As Long = 0
        Private _ContactID As Long = 0
        Private _FirstName As String
        Private _LastName As String
        Private _CustName As String
        ''Private DomainId As Long = 0
        Private _DivisionName As String
        Private _CompanyName As String
        Private _ContactPhone As String
        Private _Email As String
        Private _Street As String
        Private _State As Long
        Private _City As String
        Private _Country As Long
        Private _GroupName As String
        Private _Position As Long
        Private _WebSite As String
        Private _Notes As String
        Private _AnnualRevenue As Long
        Private _NumOfEmp As Long
        Private _InfoSource As Long
        Private _Profile As Long = 0
        Private _PostalCode As String
        Private _SameAddr As Integer = 0
        Private _TerritoryID As Long = 0
        Private _PublicFlag As Boolean
        Private _CRMType As Integer = 0
        Private _LeadBoxFlg As Integer = 0
        Private _StatusID As Long = 0
        Private _CampaignID As Long = 0
        Private _CompanyType As Long = 0
        Private _CompanyRating As Long = 0
        Private _CompanyIndustry As Long = 0
        Private _CompanyCredit As Long = 0
        Private _Comments As String
        Private _WebLabel1 As String
        Private _WebLink1 As String
        Private _WebLabel2 As String
        Private _WebLink2 As String
        Private _WebLabel3 As String
        Private _WebLink3 As String
        Private _WebLabel4 As String
        Private _WebLink4 As String
        Private _ContactType As Long = 0
        Private _Department As Long
        Private _Category As Long
        Private _GivenName As String
        Private _PhoneExt As String
        Private _Cell As String
        Private _HomePhone As String
        Private _fax As String
        Private _AsstFirstName As String
        Private _AsstLastName As String
        Private _AsstPhone As String
        Private _AsstExt As String
        Private _AsstEmail As String
        Private _Gender As String
        Private _DOB As Date = "1/1/1753"
        Private _Type As String
        Private _ContactLoc As String
        Private _PStreet As String
        Private _PCity As String
        Private _PPostalCode As String
        Private _PState As Long
        Private _PCountry As Long
        Private _ContactLocation1 As String
        Private _Street1 As String
        Private _City1 As String
        Private _PostalCode1 As String
        Private _State1 As Long
        Private _Country1 As Long
        Private _ContactLocation2 As String
        Private _Street2 As String
        Private _City2 As String
        Private _PostalCode2 As String
        Private _State2 As Long
        Private _Country2 As Long
        Private _ContactLocation3 As String
        Private _Street3 As String
        Private _City3 As String
        Private _PostalCode3 As String
        Private _State3 As Long
        Private _Country3 As Long
        Private _ContactLocation4 As String
        Private _Street4 As String
        Private _City4 As String
        Private _PostalCode4 As String
        Private _State4 As Long
        Private _Country4 As Long
        Private _ManagerID As Long = 0
        Private _SortChar As String
        Private _SortChar1 As String
        Private _SStreet As String
        Private _SCity As String
        Private _SPostalCode As String
        Private _SState As Long
        Private _SCountry As Long
        Private _ShowAll As Integer = 0
        Private _FollowUpStatus As Long
        Private _CurrentPage As Integer
        Private _PageSize As Integer
        Private _TotalRecords As Integer
        Private _SortCharacter As Char
        Private _columnSortOrder As String
        Private _columnName As String
        Private _FollowUpHstrID As Long
        Private _Interest As Decimal
        Private _InterestType As Short
        Private _BillingTerms As Short
        Private _BillingDays As Long
        Private _RepType As Short
        Private _Condition As Char
        Private _startDate As Date
        Private _endDate As Date
        Private _Task As Long
        Private _byteMode As Short
        Private _ComPhone As String
        Private _ComFax As String
        Private _Title As String
        Private _AltEmail As String
        Private _strUserIDs As String
        Private _TeamType As Short
        Private _ListType As Short
        Private _TeamMemID As Long
        Private _TrackingID As Long
        'Private UserCntID As Long
        Private _AssignedTo As Long
        Private _Team As Long
        Private _EmpStatus As Long = 658  'Active'
        Private _bitPartner As Boolean
        Private _NoTax As Boolean
        Private _UpdateDefaultTax As Boolean = True

        'Add By Prasanta on 06-01-2016
        Private _LinkedinId As String
        Private _LinkedinURL As String

        Private _PartnerSource As String
        Private _PartnerContact As String

        Private _PartenerContact As Long
        Private _PartenerSource As Long
        Public Property PartenerContact() As Long
            Get
                Return _PartenerContact
            End Get
            Set(ByVal Value As Long)
                _PartenerContact = Value
            End Set
        End Property

        Public Property PartenerSource() As Long
            Get
                Return _PartenerSource
            End Get
            Set(ByVal value As Long)
                _PartenerSource = value
            End Set
        End Property
        Public Property PartnerSource() As String
            Get
                Return _PartnerSource
            End Get
            Set(ByVal value As String)
                _PartnerSource = value
            End Set
        End Property
        Public Property PartnerContact() As String
            Get
                Return _PartnerContact
            End Get
            Set(ByVal value As String)
                _PartnerContact = value
            End Set
        End Property
        Private _strNonBizCompanyID As String
        Public Property NonBizCompanyID() As String
            Get
                Return _strNonBizCompanyID
            End Get
            Set(ByVal value As String)
                _strNonBizCompanyID = value
            End Set
        End Property

        Private _strNonBizContactID As String
        Public Property NonBizContactID() As String
            Get
                Return _strNonBizContactID
            End Get
            Set(ByVal value As String)
                _strNonBizContactID = value
            End Set
        End Property


        Private _IsSelectiveUpdate As Boolean
        Public Property IsSelectiveUpdate() As Boolean
            Get
                Return _IsSelectiveUpdate
            End Get
            Set(ByVal value As Boolean)
                _IsSelectiveUpdate = value
            End Set
        End Property


        Public Property UpdateDefaultTax() As Boolean
            Get
                Return _UpdateDefaultTax
            End Get
            Set(ByVal Value As Boolean)
                _UpdateDefaultTax = Value
            End Set
        End Property

        Private _WhereCondition As String
        Public Property WhereCondition() As String
            Get
                Return _WhereCondition
            End Get
            Set(ByVal value As String)
                _WhereCondition = value
            End Set
        End Property

        Private _FormId As Long
        Public Property FormId() As Long
            Get
                Return _FormId
            End Get
            Set(ByVal value As Long)
                _FormId = value
            End Set
        End Property
        Private _DripCampaign As Long
        Public Property DripCampaign() As Long
            Get
                Return _DripCampaign
            End Get
            Set(ByVal value As Long)
                _DripCampaign = value
            End Set
        End Property

        Private _ContactECampaignID As Long
        Public Property ContactECampaignID() As Long
            Get
                Return _ContactECampaignID
            End Get
            Set(ByVal value As Long)
                _ContactECampaignID = value
            End Set
        End Property

        <DataMember()> _
        Public Property NoTax() As Boolean
            Get
                Return _NoTax
            End Get
            Set(ByVal Value As Boolean)
                _NoTax = Value
            End Set
        End Property
        <DataMember()> _
        Public Property bitPartner() As Boolean
            Get
                Return _bitPartner
            End Get
            Set(ByVal Value As Boolean)
                _bitPartner = Value
            End Set
        End Property
        <DataMember()> _
        Public Property EmpStatus() As Long
            Get
                Return _EmpStatus
            End Get
            Set(ByVal Value As Long)
                _EmpStatus = Value
            End Set
        End Property
        <DataMember()> _
        Public Property Team() As Long
            Get
                Return _Team
            End Get
            Set(ByVal Value As Long)
                _Team = Value
            End Set
        End Property
        <DataMember()> _
        Public Property AssignedTo() As Long
            Get
                Return _AssignedTo
            End Get
            Set(ByVal Value As Long)
                _AssignedTo = Value
            End Set
        End Property
        <DataMember()> _
        Public Property LinkedinId() As String
            Get
                Return _LinkedinId
            End Get
            Set(ByVal value As String)
                _LinkedinId = value
            End Set
        End Property
        <DataMember()> _
        Public Property LinkedinURL() As String
            Get
                Return _LinkedinURL
            End Get
            Set(ByVal value As String)
                _LinkedinURL = value
            End Set
        End Property
        '<DataMember()> _
        'Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal Value As Long)
        '        UserCntID = Value
        '    End Set
        'End Property
        <DataMember()> _
        Public Property TrackingID() As Long
            Get
                Return _TrackingID
            End Get
            Set(ByVal Value As Long)
                _TrackingID = Value
            End Set
        End Property
        <DataMember()> _
        Public Property TeamMemID() As Long
            Get
                Return _TeamMemID
            End Get
            Set(ByVal Value As Long)
                _TeamMemID = Value
            End Set
        End Property
        <DataMember()> _
        Public Property ListType() As Short
            Get
                Return _ListType
            End Get
            Set(ByVal Value As Short)
                _ListType = Value
            End Set
        End Property
        <DataMember()> _
        Public Property TeamType() As Short
            Get
                Return _TeamType
            End Get
            Set(ByVal Value As Short)
                _TeamType = Value
            End Set
        End Property
        <DataMember()> _
        Public Property strUserIDs() As String
            Get
                Return _strUserIDs
            End Get
            Set(ByVal Value As String)
                _strUserIDs = Value
            End Set
        End Property
        <DataMember()> _
        Public Property AltEmail() As String
            Get
                Return _AltEmail
            End Get
            Set(ByVal Value As String)
                _AltEmail = Value
            End Set
        End Property
        <DataMember()> _
        Public Property Title() As String
            Get
                Return _Title
            End Get
            Set(ByVal Value As String)
                _Title = Value
            End Set
        End Property
        <DataMember()> _
        Public Property ComFax() As String
            Get
                Return _ComFax
            End Get
            Set(ByVal Value As String)
                _ComFax = Value
            End Set
        End Property
        <DataMember()> _
        Public Property ComPhone() As String
            Get
                Return _ComPhone
            End Get
            Set(ByVal Value As String)
                _ComPhone = Value
            End Set
        End Property
        <DataMember()> _
        Public Property byteMode() As Short
            Get
                Return _byteMode
            End Get
            Set(ByVal Value As Short)
                _byteMode = Value
            End Set
        End Property
        <DataMember()> _
        Public Property Task() As Short
            Get
                Return _Task
            End Get
            Set(ByVal Value As Short)
                _Task = Value
            End Set
        End Property
        <DataMember()> _
        Public Property endDate() As Date
            Get
                Return _endDate
            End Get
            Set(ByVal Value As Date)
                _endDate = Value
            End Set
        End Property
        <DataMember()> _
        Public Property startDate() As Date
            Get
                Return _startDate
            End Get
            Set(ByVal Value As Date)
                _startDate = Value
            End Set
        End Property
        <DataMember()> _
        Public Property Condition() As Char
            Get
                Return _Condition
            End Get
            Set(ByVal Value As Char)
                _Condition = Value
            End Set
        End Property

        <DataMember()> _
        Public Property RepType() As Short
            Get
                Return _RepType
            End Get
            Set(ByVal Value As Short)
                _RepType = Value
            End Set
        End Property

        <DataMember()> _
        Public Property BillingDays() As Long
            Get
                Return _BillingDays
            End Get
            Set(ByVal Value As Long)
                _BillingDays = Value
            End Set
        End Property

        <DataMember()> _
        Public Property BillingTerms() As Short
            Get
                Return _BillingTerms
            End Get
            Set(ByVal Value As Short)
                _BillingTerms = Value
            End Set
        End Property

        <DataMember()> _
        Public Property InterestType() As Short
            Get
                Return _InterestType
            End Get
            Set(ByVal Value As Short)
                _InterestType = Value
            End Set
        End Property

        <DataMember()> _
        Public Property Interest() As Decimal
            Get
                Return _Interest
            End Get
            Set(ByVal Value As Decimal)
                _Interest = Value
            End Set
        End Property

        <DataMember()> _
        Public Property FollowUpHstrID() As Long
            Get
                Return _FollowUpHstrID
            End Get
            Set(ByVal Value As Long)
                _FollowUpHstrID = Value
            End Set
        End Property


        <DataMember()> _
        Public Property columnName() As String
            Get
                Return _columnName
            End Get
            Set(ByVal Value As String)
                _columnName = Value
            End Set
        End Property
        <DataMember()> _
        Public Property columnSortOrder() As String
            Get
                Return _columnSortOrder
            End Get
            Set(ByVal Value As String)
                _columnSortOrder = Value
            End Set
        End Property
        <DataMember()> _
        Public Property SortCharacter() As Char
            Get
                Return _SortCharacter
            End Get
            Set(ByVal Value As Char)
                _SortCharacter = Value
            End Set
        End Property
        <DataMember()> _
        Public Property TotalRecords() As Integer
            Get
                Return _TotalRecords
            End Get
            Set(ByVal Value As Integer)
                _TotalRecords = Value
            End Set
        End Property
        <DataMember()> _
        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal Value As Integer)
                _PageSize = Value
            End Set
        End Property
        <DataMember()> _
        Public Property CurrentPage() As Integer
            Get
                Return _CurrentPage
            End Get
            Set(ByVal Value As Integer)
                _CurrentPage = Value
            End Set
        End Property

        <DataMember()> _
        Public Property FollowUpStatus() As Long
            Get
                Return _FollowUpStatus
            End Get
            Set(ByVal Value As Long)
                _FollowUpStatus = Value
            End Set
        End Property

        <DataMember()> _
        Public Property ShowAll() As Integer
            Get
                Return _ShowAll
            End Get
            Set(ByVal Value As Integer)
                _ShowAll = Value
            End Set
        End Property
        <DataMember()> _
        Public Property SStreet() As String
            Get
                Return _Street
            End Get
            Set(ByVal Value As String)
                _SStreet = Value
            End Set
        End Property
        <DataMember()> _
        Public Property SPostalCode() As String
            Get
                Return _SPostalCode
            End Get
            Set(ByVal Value As String)
                _SPostalCode = Value
            End Set
        End Property
        <DataMember()> _
        Public Property SState() As Long
            Get
                Return _SState
            End Get
            Set(ByVal Value As Long)
                _SState = Value
            End Set
        End Property
        <DataMember()> _
        Public Property SCity() As String
            Get
                Return _SCity
            End Get
            Set(ByVal Value As String)
                _SCity = Value
            End Set
        End Property
        <DataMember()> _
        Public Property SCountry() As Long
            Get
                Return _SCountry
            End Get
            Set(ByVal Value As Long)
                _SCountry = Value
            End Set
        End Property
        <DataMember()> _
        Public Property SortChar() As String
            Get
                Return _SortChar
            End Get
            Set(ByVal Value As String)
                _SortChar = Value
            End Set
        End Property
        <DataMember()> _
        Public Property SortChar1() As String
            Get
                Return _SortChar1
            End Get
            Set(ByVal Value As String)
                _SortChar1 = Value
            End Set
        End Property
        <DataMember()> _
        Public Property PStreet() As String
            Get
                Return _PStreet
            End Get
            Set(ByVal Value As String)
                _PStreet = Value
            End Set
        End Property
        <DataMember()> _
        Public Property PCity() As String
            Get
                Return _PCity
            End Get
            Set(ByVal Value As String)
                _PCity = Value
            End Set
        End Property
        <DataMember()> _
        Public Property PPostalCode() As String
            Get
                Return _PPostalCode
            End Get
            Set(ByVal Value As String)
                _PPostalCode = Value
            End Set
        End Property
        <DataMember()> _
        Public Property PState() As Long
            Get
                Return _PState
            End Get
            Set(ByVal Value As Long)
                _PState = Value
            End Set
        End Property
        <DataMember()> _
        Public Property PCountry() As Long
            Get
                Return _PCountry
            End Get
            Set(ByVal Value As Long)
                _PCountry = Value
            End Set
        End Property
        <DataMember()> _
        Public Property DOB() As Date
            Get
                Return _DOB
            End Get
            Set(ByVal Value As Date)
                _DOB = Value
            End Set
        End Property
        <DataMember()> _
        Public Property Type() As String
            Get
                Return _Type
            End Get
            Set(ByVal Value As String)
                _Type = Value
            End Set
        End Property
        <DataMember()> _
        Public Property ContactLoc() As String
            Get
                Return _ContactLoc
            End Get
            Set(ByVal Value As String)
                _ContactLoc = Value
            End Set
        End Property
        <DataMember()> _
        Public Property AsstExt() As String
            Get
                Return _AsstExt
            End Get
            Set(ByVal Value As String)
                _AsstExt = Value
            End Set
        End Property
        <DataMember()> _
        Public Property AsstEmail() As String
            Get
                Return _AsstEmail
            End Get
            Set(ByVal Value As String)
                _AsstEmail = Value
            End Set
        End Property
        <DataMember()> _
        Public Property Gender() As String
            Get
                Return _Gender
            End Get
            Set(ByVal Value As String)
                _Gender = Value
            End Set
        End Property
        <DataMember()> _
        Public Property AsstFirstName() As String
            Get
                Return _AsstFirstName
            End Get
            Set(ByVal Value As String)
                _AsstFirstName = Value
            End Set
        End Property
        <DataMember()> _
        Public Property AsstLastName() As String
            Get
                Return _AsstLastName
            End Get
            Set(ByVal Value As String)
                _AsstLastName = Value
            End Set
        End Property
        <DataMember()> _
        Public Property AsstPhone() As String
            Get
                Return _AsstPhone
            End Get
            Set(ByVal Value As String)
                _AsstPhone = Value
            End Set
        End Property
        <DataMember()> _
        Public Property Cell() As String
            Get
                Return _Cell
            End Get
            Set(ByVal Value As String)
                _Cell = Value
            End Set
        End Property
        <DataMember()> _
        Public Property HomePhone() As String
            Get
                Return _HomePhone
            End Get
            Set(ByVal Value As String)
                _HomePhone = Value
            End Set
        End Property
        <DataMember()> _
        Public Property Fax() As String
            Get
                Return _fax
            End Get
            Set(ByVal Value As String)
                _fax = Value
            End Set
        End Property
        <DataMember()> _
        Public Property Category() As Long
            Get
                Return _Category
            End Get
            Set(ByVal Value As Long)
                _Category = Value
            End Set
        End Property
        <DataMember()> _
        Public Property GivenName() As String
            Get
                Return _GivenName
            End Get
            Set(ByVal Value As String)
                _GivenName = Value
            End Set
        End Property
        <DataMember()> _
        Public Property PhoneExt() As String
            Get
                Return _PhoneExt
            End Get
            Set(ByVal Value As String)
                _PhoneExt = Value
            End Set
        End Property
        <DataMember()> _
        Public Property Department() As Long
            Get
                Return _Department
            End Get
            Set(ByVal Value As Long)
                _Department = Value
            End Set
        End Property
        <DataMember()> _
        Public Property ContactType() As Long
            Get
                Return _ContactType
            End Get
            Set(ByVal Value As Long)
                _ContactType = Value
            End Set
        End Property
        <DataMember()> _
        Public Property ManagerID() As Long
            Get
                Return _ManagerID
            End Get
            Set(ByVal Value As Long)
                _ManagerID = Value
            End Set
        End Property
        <DataMember()> _
        Public Property Comments() As String
            Get
                Return Common.CCommon.ToString(_Comments).Replace("''''", "'").Replace("''", "'")
            End Get
            Set(ByVal Value As String)
                _Comments = Replace(Value, "'", "''")
            End Set
        End Property
        <DataMember()> _
        Public Property WebLabel1() As String
            Get
                Return _WebLabel1
            End Get
            Set(ByVal Value As String)
                _WebLabel1 = Value
            End Set
        End Property
        <DataMember()> _
        Public Property WebLink1() As String
            Get
                Return _WebLink1
            End Get
            Set(ByVal Value As String)
                If Not (Value.ToLower.Contains("http://") Or Value.ToLower.Contains("https://")) Then
                    _WebLink1 = "http://" & Value
                Else
                    _WebLink1 = Value
                End If
            End Set
        End Property
        <DataMember()> _
        Public Property WebLabel2() As String
            Get
                Return _WebLabel2
            End Get
            Set(ByVal Value As String)
                _WebLabel2 = Value
            End Set
        End Property
        <DataMember()> _
        Public Property WebLink2() As String
            Get
                Return _WebLink2
            End Get
            Set(ByVal Value As String)
                If Not (Value.ToLower.Contains("http://") Or Value.ToLower.Contains("https://")) Then
                    _WebLink2 = "http://" & Value
                Else
                    _WebLink2 = Value
                End If
            End Set
        End Property
        <DataMember()> _
        Public Property WebLabel3() As String
            Get
                Return _WebLabel3
            End Get
            Set(ByVal Value As String)
                _WebLabel3 = Value
            End Set
        End Property
        <DataMember()> _
        Public Property WebLink3() As String
            Get
                Return _WebLink3
            End Get
            Set(ByVal Value As String)
                If Not (Value.ToLower.Contains("http://") Or Value.ToLower.Contains("https://")) Then
                    _WebLink3 = "http://" & Value
                Else
                    _WebLink3 = Value
                End If
            End Set
        End Property
        <DataMember()> _
        Public Property WebLabel4() As String
            Get
                Return _WebLabel4
            End Get
            Set(ByVal Value As String)
                _WebLabel4 = Value
            End Set
        End Property
        <DataMember()> _
        Public Property WebLink4() As String
            Get
                Return _WebLink4
            End Get
            Set(ByVal Value As String)
                If Not (Value.ToLower.Contains("http://") Or Value.ToLower.Contains("https://")) Then
                    _WebLink4 = "http://" & Value
                Else
                    _WebLink4 = Value
                End If
            End Set
        End Property
        <DataMember()> _
        Public Property CompanyType() As Long
            Get
                Return _CompanyType
            End Get
            Set(ByVal Value As Long)
                _CompanyType = Value
            End Set
        End Property

        <DataMember()> _
        Public Property CompanyRating() As Long
            Get
                Return _CompanyRating
            End Get
            Set(ByVal Value As Long)
                _CompanyRating = Value
            End Set
        End Property
        <DataMember()> _
        Public Property CompanyIndustry() As Long
            Get
                Return _CompanyIndustry
            End Get
            Set(ByVal Value As Long)
                _CompanyIndustry = Value
            End Set
        End Property
        <DataMember()> _
        Public Property CompanyCredit() As Long
            Get
                Return _CompanyCredit
            End Get
            Set(ByVal Value As Long)
                _CompanyCredit = Value
            End Set
        End Property
        <DataMember()> _
        Public Property StatusID() As Long
            Get
                Return _StatusID
            End Get
            Set(ByVal Value As Long)
                _StatusID = Value
            End Set
        End Property
        <DataMember()> _
        Public Property CampaignID() As Long
            Get
                Return _CampaignID
            End Get
            Set(ByVal Value As Long)
                _CampaignID = Value
            End Set
        End Property
        <DataMember()> _
        Public Property PublicFlag() As Boolean
            Get
                Return _PublicFlag
            End Get
            Set(ByVal Value As Boolean)
                _PublicFlag = Value
            End Set
        End Property
        <DataMember()> _
        Public Property CRMType() As Integer
            Get
                Return _CRMType
            End Get
            Set(ByVal Value As Integer)
                _CRMType = Value
            End Set
        End Property
        <DataMember()> _
        Public Property LeadBoxFlg() As Integer
            Get
                Return _LeadBoxFlg
            End Get
            Set(ByVal Value As Integer)
                _LeadBoxFlg = Value
            End Set
        End Property
        <DataMember()> _
        Public Property PostalCode() As String
            Get
                Return _PostalCode
            End Get
            Set(ByVal Value As String)
                _PostalCode = Value
            End Set
        End Property
        <DataMember()> _
        Public Property SameAddr() As Integer
            Get
                Return _SameAddr
            End Get
            Set(ByVal Value As Integer)
                _SameAddr = Value
            End Set
        End Property
        <DataMember()> _
        Public Property TerritoryID() As Long
            Get
                Return _TerritoryID
            End Get
            Set(ByVal Value As Long)
                _TerritoryID = Value
            End Set
        End Property
        <DataMember()> _
        Public Property Profile() As Long
            Get
                Return _Profile
            End Get
            Set(ByVal Value As Long)
                _Profile = Value
            End Set
        End Property
        <DataMember()> _
        Public Property AnnualRevenue() As Long
            Get
                Return _AnnualRevenue
            End Get
            Set(ByVal Value As Long)
                _AnnualRevenue = Value
            End Set
        End Property
        <DataMember()> _
        Public Property NumOfEmp() As Long
            Get
                Return _NumOfEmp
            End Get
            Set(ByVal Value As Long)
                _NumOfEmp = Value
            End Set
        End Property
        <DataMember()> _
        Public Property InfoSource() As Long
            Get
                Return _InfoSource
            End Get
            Set(ByVal Value As Long)
                _InfoSource = Value
            End Set
        End Property
        <DataMember()> _
        Public Property Position() As Long
            Get
                Return _Position
            End Get
            Set(ByVal Value As Long)
                _Position = Value
            End Set
        End Property
        <DataMember()> _
        Public Property WebSite() As String
            Get
                Return _WebSite
            End Get
            Set(ByVal Value As String)
                If Not (Value.ToLower.Contains("http://") Or Value.ToLower.Contains("https://")) Then
                    _WebSite = "http://" & Value
                Else
                    _WebSite = Value
                End If
            End Set
        End Property
        <DataMember()> _
        Public Property Notes() As String
            Get
                Return _Notes
            End Get
            Set(ByVal Value As String)
                _Notes = Value
            End Set
        End Property
        <DataMember()> _
        Public Property Country() As Long
            Get
                Return _Country
            End Get
            Set(ByVal Value As Long)
                _Country = Value
            End Set
        End Property
        <DataMember()> _
        Public Property ContactPhone() As String
            Get
                Return _ContactPhone
            End Get
            Set(ByVal Value As String)
                _ContactPhone = Value
            End Set
        End Property
        <DataMember()> _
        Public Property GroupName() As String
            Get
                Return _GroupName
            End Get
            Set(ByVal Value As String)
                _GroupName = Value
            End Set
        End Property
        <DataMember()> _
        Public Property Email() As String
            Get
                Return _Email
            End Get
            Set(ByVal Value As String)
                _Email = Value
            End Set
        End Property
        <DataMember()> _
        Public Property Street() As String
            Get
                Return _Street
            End Get
            Set(ByVal Value As String)
                _Street = Value
            End Set
        End Property
        <DataMember()> _
        Public Property State() As Long
            Get
                Return _State
            End Get
            Set(ByVal Value As Long)
                _State = Value
            End Set
        End Property
        <DataMember()> _
        Public Property City() As String
            Get
                Return _City
            End Get
            Set(ByVal Value As String)
                _City = Value
            End Set
        End Property

        <DataMember()> _
        Public Property DivisionName() As String
            Get
                If _DivisionName = "" Then
                    Return "-"
                Else : Return _DivisionName
                End If
            End Get
            Set(ByVal Value As String)
                _DivisionName = Value
            End Set
        End Property
        '<DataMember()> _
        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property
        <DataMember()> _
        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property
        <DataMember()> _
        Public Property CompanyID() As Long
            Get
                Return _CompanyID
            End Get
            Set(ByVal Value As Long)
                _CompanyID = Value
            End Set
        End Property
        <DataMember()> _
        Public Property ContactID() As Long
            Get
                Return _ContactID
            End Get
            Set(ByVal Value As Long)
                _ContactID = Value
            End Set
        End Property

        <DataMember()> _
        Public Property CustName() As String
            Get
                Return _CustName
            End Get
            Set(ByVal Value As String)
                _CustName = Value
            End Set
        End Property
        <DataMember()> _
        Public Property FirstName() As String
            Get
                Return _FirstName
            End Get
            Set(ByVal Value As String)
                _FirstName = Value
            End Set
        End Property
        <DataMember()> _
        Public Property LastName() As String
            Get
                Return _LastName
            End Get
            Set(ByVal Value As String)
                _LastName = Value
            End Set
        End Property
        <DataMember()> _
        Public Property CompanyName() As String
            Get
                Return _CompanyName
            End Get
            Set(ByVal Value As String)
                _CompanyName = Value
            End Set
        End Property
        <DataMember()> _
        Public Property GroupID() As Long
            Get
                Return _GroupID
            End Get
            Set(ByVal Value As Long)
                _GroupID = Value
            End Set
        End Property
        <DataMember()> _
        Public Property UserRightType() As Long
            Get
                Return _UserRightType
            End Get
            Set(ByVal Value As Long)
                _UserRightType = Value
            End Set
        End Property

        Private _ClientTimeZoneOffset As Integer
        <DataMember()> _
        Public Property ClientTimeZoneOffset() As Integer
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal Value As Integer)
                _ClientTimeZoneOffset = Value
            End Set
        End Property

        Private _Password As String
        Public Property Password() As String
            Get
                Return _Password
            End Get
            Set(ByVal value As String)
                _Password = value.Trim
            End Set
        End Property

        Private _PasswordConfirm As String
        Public Property PasswordConfirm() As String
            Get
                Return _PasswordConfirm
            End Get
            Set(ByVal value As String)
                _PasswordConfirm = value.Trim
            End Set
        End Property

        Private _PrimaryContact As Boolean
        Public Property PrimaryContact() As Boolean
            Get
                Return _PrimaryContact
            End Get
            Set(ByVal value As Boolean)
                _PrimaryContact = value
            End Set
        End Property

        Private _OptOut As Boolean
        Public Property OptOut() As Boolean
            Get
                Return _OptOut
            End Get
            Set(ByVal Value As Boolean)
                _OptOut = Value
            End Set
        End Property

        Private _CompanyDiff As Long
        Public Property CompanyDiff() As Long
            Get
                Return _CompanyDiff
            End Get
            Set(ByVal Value As Long)
                _CompanyDiff = Value
            End Set
        End Property

        Private _CompanyDiffName As String
        Public Property CompanyDiffName() As String
            Get
                Return _CompanyDiffName
            End Get
            Set(ByVal Value As String)
                _CompanyDiffName = Value
            End Set
        End Property

        Private _CompanyDiffValue As String
        Public Property CompanyDiffValue() As String
            Get
                Return _CompanyDiffValue
            End Get
            Set(ByVal Value As String)
                _CompanyDiffValue = Value
            End Set
        End Property

        Private _ActiveInActive As Boolean
        Public Property ActiveInActive() As Boolean
            Get
                Return _ActiveInActive
            End Get
            Set(ByVal Value As Boolean)
                _ActiveInActive = Value
            End Set
        End Property

        Private _RegularSearchCriteria As String
        Public Property RegularSearchCriteria() As String
            Get
                Return _RegularSearchCriteria
            End Get
            Set(ByVal Value As String)
                _RegularSearchCriteria = Value
            End Set
        End Property

        Private _CustomSearchCriteria As String
        Public Property CustomSearchCriteria() As String
            Get
                Return _CustomSearchCriteria
            End Get
            Set(ByVal Value As String)
                _CustomSearchCriteria = Value
            End Set
        End Property

        Private _ShareRecordWith As String
        Public Property ShareRecordWith() As String
            Get
                Return _ShareRecordWith
            End Get
            Set(ByVal Value As String)
                _ShareRecordWith = Value
            End Set
        End Property

        Private _CurrencyID As String
        Public Property CurrencyID() As String
            Get
                Return _CurrencyID
            End Get
            Set(ByVal Value As String)
                _CurrencyID = Value
            End Set
        End Property

        Private _DefaultPaymentMethod As Long
        Public Property DefaultPaymentMethod() As Long
            Get
                Return _DefaultPaymentMethod
            End Get
            Set(ByVal Value As Long)
                _DefaultPaymentMethod = Value
            End Set
        End Property

        Private _DefaultCreditCard As Long
        Public Property DefaultCreditCard() As Long
            Get
                Return _DefaultCreditCard
            End Get
            Set(ByVal Value As Long)
                _DefaultCreditCard = Value
            End Set
        End Property

        Private _OnCreditHold As Boolean
        Public Property OnCreditHold() As Boolean
            Get
                Return _OnCreditHold
            End Get
            Set(ByVal Value As Boolean)
                _OnCreditHold = Value
            End Set
        End Property

        Private _strShipperAccountNo As String
        Public Property ShipperAccountNo() As String
            Get
                Return _strShipperAccountNo
            End Get
            Set(ByVal value As String)
                _strShipperAccountNo = value
            End Set
        End Property

        Private _intShippingCompany As Integer
        Public Property ShippingCompany() As Integer
            Get
                Return _intShippingCompany
            End Get
            Set(ByVal value As Integer)
                _intShippingCompany = value
            End Set
        End Property

        Private _bitEmailToCase As Boolean
        Public Property bitEmailToCase() As Boolean
            Get
                Return _bitEmailToCase
            End Get
            Set(ByVal value As Boolean)
                _bitEmailToCase = value
            End Set
        End Property

        Private _lngDefaultExpenseAccountId As Long
        Public Property DefaultExpenseAccountID() As Long
            Get
                Return _lngDefaultExpenseAccountId
            End Get
            Set(ByVal value As Long)
                _lngDefaultExpenseAccountId = value
            End Set
        End Property
        Private _intDefaultShippingService As Integer
        Public Property DefaultShippingService() As Integer
            Get
                Return _intDefaultShippingService
            End Get
            Set(ByVal value As Integer)
                _intDefaultShippingService = value
            End Set
        End Property

        Private _numAccountClass As Long
        Public Property AccountClass() As Long
            Get
                Return _numAccountClass
            End Get
            Set(ByVal value As Long)
                _numAccountClass = value
            End Set
        End Property

        Private _tintPriceLevel As Int32
        Public Property PriceLevel() As Int32
            Get
                Return _tintPriceLevel
            End Get
            Set(ByVal value As Int32)
                _tintPriceLevel = value
            End Set
        End Property
        Private _vcPartnerCode As String
        Public Property vcPartnerCode() As String
            Get
                Return _vcPartnerCode
            End Get
            Set(ByVal value As String)
                _vcPartnerCode = value
            End Set
        End Property
        Private _tintInbound850PickItem As Int32
        Public Property InBound850PickItem() As Int32
            Get
                Return _tintInbound850PickItem
            End Get
            Set(ByVal value As Int32)
                _tintInbound850PickItem = value
            End Set
        End Property

        Private _IsAutoCheckCustomerPart As Boolean
        Public Property IsAutoCheckCustomerPart() As Boolean
            Get
                Return _IsAutoCheckCustomerPart
            End Get
            Set(ByVal value As Boolean)
                _IsAutoCheckCustomerPart = value
            End Set
        End Property
        Public Property BillAddressID As Long
        Public Property ShipAddressID As Long
        Public Property vcCreditCards As String
        Public Property IsEcommerceAccess As Boolean
        Public Property vcPassword As String
        Public Property TaxID As String
        Public Property IsAllocateInventoryOnPickList As Boolean
        Public Property IsShippingLabelRequired As Boolean
        Public Property BuiltWithJson As String
        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  RemoveRecrod()
        '     Purpose					 :  To delete data from database 
        '     Example                    :  
        '     Parameters                 :  @ItemCode                     
        '                                 
        '     Outputs					 :  It return true  if deleted otherwise return false
        '						
        '     Author Name				 :  Ajeet Singh
        '     Date Written				 :  18/10/2004
        '     Cross References 			 :  List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Function RemoveRecrodGroup() As Boolean
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@GroupID", _GroupID, NpgsqlTypes.NpgsqlDbType.Bigint, 9))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                SqlDAL.ExecuteNonQuery(GetConnection.GetConnectionString, "usp_DeleteGroup", sqlParams.ToArray())

                Return True
            Catch ex As Exception
                Return False
                Throw ex
            Finally

            End Try

        End Function


        Public Function GetDivisionName() As String
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                arParms(0) = New Npgsql.NpgsqlParameter("@DivisionID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                _DivisionName = CStr(SqlDAL.ExecuteScalar(connString, "GetDivisionName", _DivisionID))
            Catch ex As Exception
                Throw ex
            Finally

            End Try

        End Function


        Public Function GetDivisionfromCompName() As Long

            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@vcCompanyName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(0).Value = _Email.Split("@")(1)

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_GetDivisionFromComName", arParms))

            Catch ex As Exception

            End Try
        End Function


        Public Function CreateRecordDivisionsInfo() As Long
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim SaveCSVDivisionsInfo As Long
            Try
                ' Set up parameters (1 input and 3 output) 
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDivisionID", _DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numCompanyID", _CompanyID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcDivisionName", IIf(_DivisionName = "", "-", _DivisionName), NpgsqlTypes.NpgsqlDbType.VarChar, 100))
                    .Add(SqlDAL.Add_Parameter("@vcBillStreet", _Street, NpgsqlTypes.NpgsqlDbType.VarChar, 100))
                    .Add(SqlDAL.Add_Parameter("@vcBillCity", _City, NpgsqlTypes.NpgsqlDbType.VarChar, 50))
                    .Add(SqlDAL.Add_Parameter("@vcBillState", _State, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcBillPostCode", _PostalCode, NpgsqlTypes.NpgsqlDbType.VarChar, 15))
                    .Add(SqlDAL.Add_Parameter("@vcBillCountry", _Country, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitSameAddr", If(_SameAddr = 1, True, False), NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@vcShipStreet", _SStreet, NpgsqlTypes.NpgsqlDbType.VarChar, 100))
                    .Add(SqlDAL.Add_Parameter("@vcShipCity", _SCity, NpgsqlTypes.NpgsqlDbType.VarChar, 50))
                    .Add(SqlDAL.Add_Parameter("@vcShipState", _SState, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcShipPostCode", _SPostalCode, NpgsqlTypes.NpgsqlDbType.VarChar, 15))
                    .Add(SqlDAL.Add_Parameter("@vcShipCountry", _SCountry, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numGrpId", _GroupID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numTerID", _TerritoryID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitPublicFlag", _PublicFlag, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@tintCRMType", _CRMType, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitLeadBoxFlg", If(_LeadBoxFlg = 1, True, False), NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@numStatusID", _StatusID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numCampaignID", _CampaignID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numFollowUpStatus", _FollowUpStatus, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintBillingTerms", _BillingTerms, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@numBillingDays", _BillingDays, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintInterestType", _InterestType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@fltInterest", _Interest, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@vcComFax", _ComFax, NpgsqlTypes.NpgsqlDbType.VarChar, 50))
                    .Add(SqlDAL.Add_Parameter("@vcComPhone", _ComPhone, NpgsqlTypes.NpgsqlDbType.VarChar, 50))
                    .Add(SqlDAL.Add_Parameter("@numAssignedTo", _AssignedTo, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitNoTax", _NoTax, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@UpdateDefaultTax", _UpdateDefaultTax, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@numCompanyDiff", _CompanyDiff, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcCompanyDiff", _CompanyDiffValue, NpgsqlTypes.NpgsqlDbType.VarChar, 100))
                    .Add(SqlDAL.Add_Parameter("@numCurrencyID", Convert.ToInt64(_CurrencyID), NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numAccountClass", _numAccountClass, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numBillAddressID", BillAddressID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numShipAddressID", ShipAddressID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcPartenerSource", _PartnerSource, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcPartenerContact", _PartnerContact, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numPartner", PartenerSource, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcBuiltWithJson", BuiltWithJson, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                SaveCSVDivisionsInfo = SqlDAL.ExecuteScalar(connString, "USP_ManageDivisions", sqlParams.ToArray())

                Me.DivisionID = SaveCSVDivisionsInfo

                Return SaveCSVDivisionsInfo
            Catch ex As Exception
                Throw ex
                Return False

            End Try
        End Function


        Public Function ManageCompanyDivisionsInfo1() As Long
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim SaveCSVDivisionsInfo As Long
            Try
                ' Set up parameters (1 input and 3 output) 
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(37) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@DivisionID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@CompanyID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _CompanyID

                arParms(2) = New Npgsql.NpgsqlParameter("@DivisionName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(2).Value = _DivisionName


                arParms(3) = New Npgsql.NpgsqlParameter("@GroupID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _GroupID


                arParms(4) = New Npgsql.NpgsqlParameter("@TerritoryID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = _TerritoryID

                arParms(5) = New Npgsql.NpgsqlParameter("@PublicFlag", NpgsqlTypes.NpgsqlDbType.Bit, 1)
                arParms(5).Value = _PublicFlag

                arParms(6) = New Npgsql.NpgsqlParameter("@CRMType", NpgsqlTypes.NpgsqlDbType.Integer, 4)
                arParms(6).Value = _CRMType

                arParms(7) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(7).Value = UserCntID

                arParms(8) = New Npgsql.NpgsqlParameter("@DomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(8).Value = DomainID

                arParms(9) = New Npgsql.NpgsqlParameter("@LeadBoxFlg", NpgsqlTypes.NpgsqlDbType.Bit, 1)
                arParms(9).Value = If(_LeadBoxFlg = 1, True, False)

                arParms(10) = New Npgsql.NpgsqlParameter("@StatusID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(10).Value = _StatusID

                arParms(11) = New Npgsql.NpgsqlParameter("@CampaignID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(11).Value = _CampaignID

                arParms(12) = New Npgsql.NpgsqlParameter("@numFollowUpStatus", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(12).Value = _FollowUpStatus

                arParms(13) = New Npgsql.NpgsqlParameter("@tintBillingTerms", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(13).Value = _BillingTerms

                arParms(14) = New Npgsql.NpgsqlParameter("@numBillingDays", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(14).Value = _BillingDays

                arParms(15) = New Npgsql.NpgsqlParameter("@tintInterestType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(15).Value = _InterestType

                arParms(16) = New Npgsql.NpgsqlParameter("@fltInterest", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(16).Value = _Interest

                arParms(17) = New Npgsql.NpgsqlParameter("@vcComPhone", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(17).Value = _ComPhone

                arParms(18) = New Npgsql.NpgsqlParameter("@vcComFax", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(18).Value = _ComFax

                arParms(19) = New Npgsql.NpgsqlParameter("@numAssignedTo", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(19).Value = _AssignedTo

                arParms(20) = New Npgsql.NpgsqlParameter("@bitNoTax", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(20).Value = _NoTax

                arParms(21) = New Npgsql.NpgsqlParameter("@bitSelectiveUpdate", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(21).Value = _IsSelectiveUpdate

                arParms(22) = New Npgsql.NpgsqlParameter("@numCompanyDiff", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(22).Value = _CompanyDiff

                arParms(23) = New Npgsql.NpgsqlParameter("@vcCompanyDiff", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(23).Value = _CompanyDiffValue

                arParms(24) = New Npgsql.NpgsqlParameter("@bitActiveInActive", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(24).Value = _ActiveInActive

                arParms(25) = New Npgsql.NpgsqlParameter("@numCurrencyID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(25).Value = Convert.ToInt64(_CurrencyID)

                arParms(26) = New Npgsql.NpgsqlParameter("@vcShipperAccountNo", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(26).Value = _strShipperAccountNo

                arParms(27) = New Npgsql.NpgsqlParameter("@intShippingCompany", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(27).Value = _intShippingCompany

                arParms(28) = New Npgsql.NpgsqlParameter("@numDefaultExpenseAccountID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(28).Value = _lngDefaultExpenseAccountId

                arParms(29) = New Npgsql.NpgsqlParameter("@numAccountClass", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(29).Value = _numAccountClass

                arParms(30) = New Npgsql.NpgsqlParameter("@tintPriceLevel", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(30).Value = _tintPriceLevel

                arParms(31) = New Npgsql.NpgsqlParameter("@vcPartnerCode", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(31).Value = _vcPartnerCode

                arParms(32) = New Npgsql.NpgsqlParameter("@numPartenerContactId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(32).Value = PartenerContact

                arParms(33) = New Npgsql.NpgsqlParameter("@numPartenerSourceId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(33).Value = PartenerSource

                arParms(34) = New Npgsql.NpgsqlParameter("@bitEmailToCase", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(34).Value = bitEmailToCase

                arParms(35) = New Npgsql.NpgsqlParameter("@bitShippingLabelRequired", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(35).Value = IsShippingLabelRequired

                arParms(36) = New Npgsql.NpgsqlParameter("@tintInbound850PickItem", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(36).Value = _tintInbound850PickItem

                arParms(37) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(37).Value = Nothing
                arParms(37).Direction = ParameterDirection.InputOutput

                SaveCSVDivisionsInfo = CLng(SqlDAL.ExecuteScalar(connString, "usp_CompanyDivision", arParms))

                Return SaveCSVDivisionsInfo
            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function

        Public Function CreateRecordCompanyInfo() As Long
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim SaveCSVCompanyInfo As Long
            Try
                ' Set up parameters (1 input and 3 output) 
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(24) {}
                arParms(0) = New Npgsql.NpgsqlParameter("@CompanyID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _CompanyID

                If _CompanyName = "" Then
                    If _LastName = "" And _FirstName = "" Then
                        _CompanyName = "-"
                    Else
                        _CompanyName = _LastName & ", " & _FirstName
                    End If
                End If

                arParms(1) = New Npgsql.NpgsqlParameter("@CompanyName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(1).Value = _CompanyName

                arParms(2) = New Npgsql.NpgsqlParameter("@numCompanyType", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _CompanyType

                arParms(3) = New Npgsql.NpgsqlParameter("@CompanyRating", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _CompanyRating


                arParms(4) = New Npgsql.NpgsqlParameter("@CompanyIndustry", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = _CompanyIndustry

                arParms(5) = New Npgsql.NpgsqlParameter("@CompanyCredit", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(5).Value = _CompanyCredit


                arParms(6) = New Npgsql.NpgsqlParameter("@Comments", NpgsqlTypes.NpgsqlDbType.Text, 16)
                arParms(6).Value = _Comments

                arParms(7) = New Npgsql.NpgsqlParameter("@WebSite", NpgsqlTypes.NpgsqlDbType.VarChar, 255)
                arParms(7).Value = _WebSite

                arParms(8) = New Npgsql.NpgsqlParameter("@WebLabel1", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(8).Value = _WebLabel1

                arParms(9) = New Npgsql.NpgsqlParameter("@WebLink1", NpgsqlTypes.NpgsqlDbType.VarChar, 255)
                arParms(9).Value = _WebLink1

                arParms(10) = New Npgsql.NpgsqlParameter("@WebLabel2", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(10).Value = _WebLabel2

                arParms(11) = New Npgsql.NpgsqlParameter("@WebLink2", NpgsqlTypes.NpgsqlDbType.VarChar, 255)
                arParms(11).Value = _WebLink2

                arParms(12) = New Npgsql.NpgsqlParameter("@WebLabel3", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(12).Value = _WebLabel3


                arParms(13) = New Npgsql.NpgsqlParameter("@WebLink3", NpgsqlTypes.NpgsqlDbType.VarChar, 255)
                arParms(13).Value = _WebLink3

                arParms(14) = New Npgsql.NpgsqlParameter("@WebLabel4", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(14).Value = _WebLabel4

                arParms(15) = New Npgsql.NpgsqlParameter("@WebLink4", NpgsqlTypes.NpgsqlDbType.VarChar, 255)
                arParms(15).Value = _WebLink4


                arParms(16) = New Npgsql.NpgsqlParameter("@AnnualRevenue", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(16).Value = _AnnualRevenue

                arParms(17) = New Npgsql.NpgsqlParameter("@NumOfEmp", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(17).Value = _NumOfEmp

                arParms(18) = New Npgsql.NpgsqlParameter("@How", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(18).Value = _InfoSource

                arParms(19) = New Npgsql.NpgsqlParameter("@Profile", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(19).Value = _Profile

                arParms(20) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(20).Value = UserCntID


                arParms(21) = New Npgsql.NpgsqlParameter("@PublicFlag", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(21).Value = _PublicFlag


                arParms(22) = New Npgsql.NpgsqlParameter("@DomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(22).Value = DomainID

                arParms(23) = New Npgsql.NpgsqlParameter("@bitSelectiveUpdate", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(23).Value = _IsSelectiveUpdate

                arParms(24) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(24).Value = Nothing
                arParms(24).Direction = ParameterDirection.InputOutput

                SaveCSVCompanyInfo = SqlDAL.ExecuteScalar(connString, "usp_ManageCompany", arParms)

                Return SaveCSVCompanyInfo
            Catch ex As Exception
                Throw ex
                Return SaveCSVCompanyInfo = 0
            End Try

        End Function

        Public Function CreateRecordAddContactInfo() As Long
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim SaveCSVAdditionalContactInfo As Long
            Try

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(41) {}
                arParms(0) = New Npgsql.NpgsqlParameter("@numcontactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ContactID

                arParms(1) = New Npgsql.NpgsqlParameter("@numContactType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ContactType

                arParms(2) = New Npgsql.NpgsqlParameter("@vcDepartment", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _Department

                arParms(3) = New Npgsql.NpgsqlParameter("@vcCategory", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _Category

                arParms(4) = New Npgsql.NpgsqlParameter("@vcGivenName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(4).Value = _GivenName

                arParms(5) = New Npgsql.NpgsqlParameter("@vcFirstName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(5).Value = _FirstName

                arParms(6) = New Npgsql.NpgsqlParameter("@vcLastName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(6).Value = _LastName

                arParms(7) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(7).Value = _DivisionID

                arParms(8) = New Npgsql.NpgsqlParameter("@numPhone", NpgsqlTypes.NpgsqlDbType.VarChar, 15)
                arParms(8).Value = _ContactPhone

                arParms(9) = New Npgsql.NpgsqlParameter("@numPhoneExtension", NpgsqlTypes.NpgsqlDbType.VarChar, 7)
                arParms(9).Value = _PhoneExt

                arParms(10) = New Npgsql.NpgsqlParameter("@numCell", NpgsqlTypes.NpgsqlDbType.VarChar, 15)
                arParms(10).Value = _Cell

                arParms(11) = New Npgsql.NpgsqlParameter("@NumHomePhone", NpgsqlTypes.NpgsqlDbType.VarChar, 15)
                arParms(11).Value = _HomePhone

                arParms(12) = New Npgsql.NpgsqlParameter("@vcFax", NpgsqlTypes.NpgsqlDbType.VarChar, 15)
                arParms(12).Value = _fax

                arParms(13) = New Npgsql.NpgsqlParameter("@vcEmail", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(13).Value = _Email


                arParms(14) = New Npgsql.NpgsqlParameter("@VcAsstFirstName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(14).Value = _AsstFirstName


                arParms(15) = New Npgsql.NpgsqlParameter("@vcAsstLastName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(15).Value = _AsstLastName

                arParms(16) = New Npgsql.NpgsqlParameter("@numAsstPhone", NpgsqlTypes.NpgsqlDbType.VarChar, 15)
                arParms(16).Value = _AsstPhone

                arParms(17) = New Npgsql.NpgsqlParameter("@numAsstExtn", NpgsqlTypes.NpgsqlDbType.VarChar, 6)
                arParms(17).Value = _AsstExt

                arParms(18) = New Npgsql.NpgsqlParameter("@vcAsstEmail", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(18).Value = _AsstEmail



                arParms(19) = New Npgsql.NpgsqlParameter("@charSex", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(19).Value = _Gender


                arParms(20) = New Npgsql.NpgsqlParameter("@bintDOB", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(20).Value = _DOB

                arParms(21) = New Npgsql.NpgsqlParameter("@vcPosition", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(21).Value = _Position

                arParms(22) = New Npgsql.NpgsqlParameter("@txtNotes", NpgsqlTypes.NpgsqlDbType.Text, 16)
                arParms(22).Value = _Notes


                arParms(23) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(23).Value = UserCntID


                arParms(24) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(24).Value = DomainID

                arParms(25) = New Npgsql.NpgsqlParameter("@vcPStreet", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(25).Value = _PStreet

                arParms(26) = New Npgsql.NpgsqlParameter("@vcPCity", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(26).Value = _PCity

                arParms(27) = New Npgsql.NpgsqlParameter("@vcPPostalCode", NpgsqlTypes.NpgsqlDbType.VarChar, 15)
                arParms(27).Value = _PPostalCode

                arParms(28) = New Npgsql.NpgsqlParameter("@vcPState", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(28).Value = _PState

                arParms(29) = New Npgsql.NpgsqlParameter("@vcPCountry", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(29).Value = _PCountry

                arParms(30) = New Npgsql.NpgsqlParameter("@numManagerID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(30).Value = _ManagerID

                arParms(31) = New Npgsql.NpgsqlParameter("@numTeam", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(31).Value = _Team

                arParms(32) = New Npgsql.NpgsqlParameter("@numEmpStatus", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(32).Value = _EmpStatus

                arParms(33) = New Npgsql.NpgsqlParameter("@vcTitle", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(33).Value = _Title

                arParms(34) = New Npgsql.NpgsqlParameter("@bitPrimaryContact", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(34).Value = _PrimaryContact

                arParms(35) = New Npgsql.NpgsqlParameter("@bitOptOut", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(35).Value = _OptOut

                arParms(36) = New Npgsql.NpgsqlParameter("@vcLinkedinId", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(36).Value = _LinkedinId


                arParms(37) = New Npgsql.NpgsqlParameter("@vcLinkedinUrl", NpgsqlTypes.NpgsqlDbType.VarChar, 300)
                arParms(37).Value = _LinkedinURL

                arParms(38) = New Npgsql.NpgsqlParameter("@numECampaignID", NpgsqlTypes.NpgsqlDbType.BigInt, 18)
                arParms(38).Value = _DripCampaign

                arParms(39) = New Npgsql.NpgsqlParameter("@vcPassword", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(39).Value = vcPassword

                arParms(40) = New Npgsql.NpgsqlParameter("@vcTaxID", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(40).Value = TaxID

                arParms(41) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(41).Value = Nothing
                arParms(41).Direction = ParameterDirection.InputOutput

                SaveCSVAdditionalContactInfo = SqlDAL.ExecuteScalar(connString, "usp_ManageAddlContInfo", arParms)
                Return SaveCSVAdditionalContactInfo
            Catch ex As Exception
                Throw ex
                Return SaveCSVAdditionalContactInfo = 0
            End Try


        End Function


        Public Function UpdateBillingTerms()

            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim SaveCSVDivisionsInfo As Long
            Try
                ' Set up parameters (1 input and 3 output) 
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(26) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _DivisionID

                arParms(2) = New Npgsql.NpgsqlParameter("@numCompanyID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _CompanyID

                arParms(3) = New Npgsql.NpgsqlParameter("@numCompanyCredit", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _CompanyCredit

                arParms(4) = New Npgsql.NpgsqlParameter("@vcDivisionName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(4).Value = IIf(_DivisionName = "", "-", _DivisionName)

                arParms(5) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(5).Value = UserCntID

                arParms(6) = New Npgsql.NpgsqlParameter("@tintCRMType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(6).Value = _CRMType

                arParms(7) = New Npgsql.NpgsqlParameter("@tintBillingTerms", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(7).Value = _BillingTerms

                arParms(8) = New Npgsql.NpgsqlParameter("@numBillingDays", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = _BillingDays

                arParms(9) = New Npgsql.NpgsqlParameter("@tintInterestType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(9).Value = _InterestType

                arParms(10) = New Npgsql.NpgsqlParameter("@fltInterest", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(10).Value = _Interest

                arParms(11) = New Npgsql.NpgsqlParameter("@bitNoTax", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(11).Value = _NoTax

                arParms(12) = New Npgsql.NpgsqlParameter("@numCurrencyID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(12).Value = Convert.ToInt64(_CurrencyID)

                arParms(13) = New Npgsql.NpgsqlParameter("@numDefaultPaymentMethod", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(13).Value = _DefaultPaymentMethod

                arParms(14) = New Npgsql.NpgsqlParameter("@numDefaultCreditCard", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(14).Value = _DefaultCreditCard

                arParms(15) = New Npgsql.NpgsqlParameter("@bitOnCreditHold", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(15).Value = _OnCreditHold

                arParms(16) = New Npgsql.NpgsqlParameter("@vcShipperAccountNo", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(16).Value = _strShipperAccountNo

                arParms(17) = New Npgsql.NpgsqlParameter("@intShippingCompany", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(17).Value = _intShippingCompany

                arParms(18) = New Npgsql.NpgsqlParameter("@bitEmailToCase", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(18).Value = _bitEmailToCase

                arParms(19) = New Npgsql.NpgsqlParameter("@numDefaultExpenseAccountID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(19).Value = _lngDefaultExpenseAccountId

                arParms(20) = New Npgsql.NpgsqlParameter("@numDefaultShippingServiceID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(20).Value = _intDefaultShippingService

                arParms(21) = New Npgsql.NpgsqlParameter("@numAccountClass", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(21).Value = _numAccountClass

                arParms(22) = New Npgsql.NpgsqlParameter("@tintPriceLevel", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(22).Value = _tintPriceLevel

                arParms(23) = New Npgsql.NpgsqlParameter("@bitShippingLabelRequired", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(23).Value = IsShippingLabelRequired

                arParms(24) = New Npgsql.NpgsqlParameter("@tintInbound850PickItem", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(24).Value = _tintInbound850PickItem

                arParms(25) = New Npgsql.NpgsqlParameter("@bitAllocateInventoryOnPickList", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(25).Value = IsAllocateInventoryOnPickList

                arParms(26) = New Npgsql.NpgsqlParameter("@bitAutoCheckCustomerPart", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(26).Value = IsAutoCheckCustomerPart

                SaveCSVDivisionsInfo = SqlDAL.ExecuteScalar(connString, "USP_UpdateBillingTerms", arParms)

                Return SaveCSVDivisionsInfo
            Catch ex As Exception
                Throw ex

            End Try

        End Function

        Public Function GetCSVListDetailId(ByRef LsName As String, ByVal intListID As Integer, ByRef lngUserId As Long, ByRef lngDomainId As Long) As Long
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@vcData", LsName, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numListID", intListID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numCreatedBy", lngUserId, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@bintCreatedDate", Format(Now(), "yyyyMMddhhmmss"), NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numDomainId", lngDomainId, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return CLng(SqlDAL.ExecuteScalar(connString, "usp_GetListID", sqlParams.ToArray()))
            Catch ex As Exception
                ' // some error occured. Bubble it to caller and encapsulate Exception object
                Throw New Exception("Error while fetching the List Id", ex)
            End Try
        End Function

        Public Function GetCSVTerritoryId(ByRef LsName As String, ByRef lngUserId As Long, ByRef lngDomainId As Long) As Long
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@v_vcTerrName", LsName, NpgsqlTypes.NpgsqlDbType.Varchar, 50))
                    .Add(SqlDAL.Add_Parameter("@v_numCreatedby", lngUserId, NpgsqlTypes.NpgsqlDbType.Numeric, 9))
                    .Add(SqlDAL.Add_Parameter("@v_numDomainId", lngDomainId, NpgsqlTypes.NpgsqlDbType.Numeric, 9))
                    .Add(SqlDAL.Add_Parameter("@v_bintCreatedDate", Format(Now(), "yyyyMMddhhmmss"), NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                GetCSVTerritoryId = CInt(SqlDAL.ExecuteScalar(GetConnection.GetConnectionString, "usp_GetTerritoryID", sqlParams.ToArray()))
            Catch ex As Exception
                Throw New Exception("Error while fetching the List Id", ex)
                GetCSVTerritoryId = 1
            End Try

            Return GetCSVTerritoryId
        End Function

        Public Function GetItemDescription(ByVal intItemCode As Integer, ByVal intNumFlg As Integer) As String
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numItemCode", intItemCode, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@numFlg", intNumFlg, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                GetItemDescription = CStr(SqlDAL.ExecuteScalar(GetConnection.GetConnectionString, "usp_GetItemDescription", sqlParams.ToArray()))
            Catch ex As Exception
                Throw New Exception("Error while fetching the List Id", ex)
                GetItemDescription = " "
            End Try
        End Function

        Public Function GetCompanyType(ByRef intListID As Integer, ByRef intDomainId As Integer, ByRef intCompId As Integer) As Integer
            '================================================================================
            ' Purpose:      This function is used for checking the type of the company.
            ' History
            ' Ver   Date        Ref     Author              Reason
            ' 1     10/03/2003          Madhan Kumar.v         Created
            '
            ' Non Compliance (any deviation from standards)
            '   No deviations from the standards.
            '================================================================================
            Try
                Dim ds As DataSet

                Using connection As New Npgsql.NpgsqlConnection(GetConnection.GetConnectionString)
                    Dim sqlAdapter As Npgsql.NpgsqlDataAdapter

                    Dim sqlCommand As New Npgsql.NpgsqlCommand
                    sqlCommand.Connection = connection
                    sqlCommand.CommandText = "usp_CheckCompany"
                    Dim p1 As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter("@numListId", NpgsqlTypes.NpgsqlDbType.Numeric)
                    p1.Value = intListID
                    Dim p2 As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter("@numDomainid", NpgsqlTypes.NpgsqlDbType.Numeric)
                    p2.Value = intDomainId
                    Dim p3 As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter("@numCompId", NpgsqlTypes.NpgsqlDbType.Numeric)
                    p3.Value = intCompId
                    Dim p4 As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                    p4.Direction = ParameterDirection.InputOutput
                    p4.Value = Nothing
                    sqlCommand.Parameters.Add(p1)
                    sqlCommand.Parameters.Add(p2)
                    sqlCommand.Parameters.Add(p3)
                    sqlCommand.Parameters.Add(p4)

                    Dim i As Int32 = 0
                    connection.Open()
                    Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                        sqlCommand.ExecuteNonQuery()

                        For Each parm As Npgsql.NpgsqlParameter In sqlCommand.Parameters
                            If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                    Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                    sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                    ds.Tables.Add(parm.Value.ToString())
                                    sqlAdapter.Fill(ds.Tables(i))
                                    i += 1
                                End If
                            End If
                        Next

                        objTransaction.Commit()
                    End Using
                    connection.Close()
                End Using

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    GetCompanyType = CInt(ds.Tables(0).Rows(0)(0))
                End If


            Catch ex As Exception
                ' // some error occured. Bubble it to caller and encapsulate Exception object
                Throw New Exception("Error while fetching the List Id", ex)
                GetCompanyType = 0
            End Try
        End Function

        Public Function GetUserID(ByVal intDomainId As Integer) As Integer
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = intDomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                GetUserID = CInt(SqlDAL.ExecuteScalar(connString, "usp_GetFirstUser", arParms))
            Catch ex As Exception
                ' // some error occured. Bubble it to caller and encapsulate Exception object
                Throw New Exception("Error while fetching the List Id", ex)
                GetUserID = 0
            End Try
        End Function

        Public Function GetLeadInfo() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ContactID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _ClientTimeZoneOffset

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "usp_LeadDetails", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function VerifyPartnerCode() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@vcPartnerCode", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(0).Value = _vcPartnerCode

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "usp_VerifyPartnerCode", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function PromoteLead() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ContactID

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = UserCntID


                SqlDAL.ExecuteNonQuery(connString, "USP_PromoteLead", arParms)

                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetCompanyName() As String
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return CStr(SqlDAL.ExecuteScalar(connString, "usp_GetCompanyName", arParms))
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Private _tintUpdateMode As Short
        Public Property tintUpdateMode() As Short
            Get
                Return _tintUpdateMode
            End Get
            Set(ByVal value As Short)
                _tintUpdateMode = value
            End Set
        End Property

        Public Function GetAddressDetailsForImport() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintUpdateMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _tintUpdateMode

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "Usp_GetAddressDetailsForImport", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function GetContactInfoPromote() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ContactID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _DivisionID

                arParms(2) = New Npgsql.NpgsqlParameter("@vtintUserRightType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = UserRightType

                arParms(3) = New Npgsql.NpgsqlParameter("@bitShowAll", NpgsqlTypes.NpgsqlDbType.Boolean)
                arParms(3).Value = True

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "usp_GetContactInfo", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function



        Public Function GetFollowUpHstr() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_FollowUpHstr", arParms).Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function DeleteFollowUpHstr() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numFollowUpStatusID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _FollowUpHstrID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteFollowUpHstr", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function UpdateAddConInfo() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(12) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@vcFirstName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(0).Value = _FirstName

                arParms(1) = New Npgsql.NpgsqlParameter("@vcLastName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(1).Value = _LastName

                arParms(2) = New Npgsql.NpgsqlParameter("@vcEmail", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(2).Value = _Email

                arParms(3) = New Npgsql.NpgsqlParameter("@numFollowUpStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _FollowUpStatus

                arParms(4) = New Npgsql.NpgsqlParameter("@numPosition", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _Position

                arParms(5) = New Npgsql.NpgsqlParameter("@numPhone", NpgsqlTypes.NpgsqlDbType.VarChar, 15)
                arParms(5).Value = _ContactPhone

                arParms(6) = New Npgsql.NpgsqlParameter("@numPhoneExtension", NpgsqlTypes.NpgsqlDbType.VarChar, 7)
                arParms(6).Value = _PhoneExt

                arParms(7) = New Npgsql.NpgsqlParameter("@Comments", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(7).Value = _Comments

                arParms(8) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = _ContactID


                arParms(9) = New Npgsql.NpgsqlParameter("@vcTitle", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(9).Value = _Title

                arParms(10) = New Npgsql.NpgsqlParameter("@vcAltEmail", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(10).Value = _AltEmail

                arParms(11) = New Npgsql.NpgsqlParameter("@numECampaignID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(11).Value = _DripCampaign

                arParms(12) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(12).Value = UserCntID

                SqlDAL.ExecuteNonQuery(connString, "USP_LeadsUpdateConInfo", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetLeadList() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(22) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numGrpID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _GroupID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintUserRightType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _UserRightType


                arParms(3) = New Npgsql.NpgsqlParameter("@CustName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(3).Value = IIf(_CustName = "", "", Replace(_CustName, "'", "''"))

                arParms(4) = New Npgsql.NpgsqlParameter("@FirstName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(4).Value = IIf(_FirstName = "", "", Replace(_FirstName, "'", "''"))

                arParms(5) = New Npgsql.NpgsqlParameter("@LastName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(5).Value = IIf(_LastName = "", "", Replace(_LastName, "'", "''"))

                arParms(6) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(6).Value = _SortCharacter

                arParms(7) = New Npgsql.NpgsqlParameter("@numFollowUpStatus", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(7).Value = _FollowUpStatus

                arParms(8) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(8).Value = _CurrentPage

                arParms(9) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(9).Value = _PageSize

                arParms(10) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(10).Value = _columnName

                arParms(11) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(11).Value = _columnSortOrder

                arParms(12) = New Npgsql.NpgsqlParameter("@ListType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(12).Value = _ListType

                arParms(13) = New Npgsql.NpgsqlParameter("@TeamType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(13).Value = _TeamType

                arParms(14) = New Npgsql.NpgsqlParameter("@TeamMemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(14).Value = _TeamMemID

                arParms(15) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(15).Value = DomainID

                arParms(16) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(16).Value = _ClientTimeZoneOffset

                arParms(17) = New Npgsql.NpgsqlParameter("@bitPartner", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(17).Value = _bitPartner

                arParms(18) = New Npgsql.NpgsqlParameter("@bitActiveInActive", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(18).Value = _ActiveInActive

                arParms(19) = New Npgsql.NpgsqlParameter("@vcRegularSearchCriteria", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(19).Value = _RegularSearchCriteria

                arParms(20) = New Npgsql.NpgsqlParameter("@vcCustomSearchCriteria", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(20).Value = _CustomSearchCriteria

                arParms(21) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(21).Value = Nothing
                arParms(21).Direction = ParameterDirection.InputOutput

                arParms(22) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(22).Value = Nothing
                arParms(22).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                Dim dtTable As DataTable

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetLeadList1", arParms)
                dtTable = ds.Tables(0)
                If dtTable.Rows.Count > 0 Then
                    _TotalRecords = CInt(dtTable.Rows(0).Item("TotalRowCount"))
                End If
                dtTable.Columns.Remove("TotalRowCount")
                ds.AcceptChanges()
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetTeamMembers() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = DomainID


                arParms(2) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(2).Value = _TeamType

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim dr As DataSet
                dr = SqlDAL.ExecuteDataset(connString, "USP_GetTeamMembers", arParms)
                Return dr.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetPerAnyCompanyList() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(13) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@RepType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _RepType


                arParms(2) = New Npgsql.NpgsqlParameter("@Condition", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(2).Value = _Condition

                arParms(3) = New Npgsql.NpgsqlParameter("@numStartDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(3).Value = _startDate

                arParms(4) = New Npgsql.NpgsqlParameter("@numEndDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(4).Value = _endDate

                arParms(5) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _CurrentPage

                arParms(6) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = _PageSize

                arParms(7) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(7).Direction = ParameterDirection.InputOutput
                arParms(7).Value = _TotalRecords

                arParms(8) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(8).Value = _columnName

                arParms(9) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(9).Value = _columnSortOrder

                arParms(10) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(10).Value = DomainID

                arParms(11) = New Npgsql.NpgsqlParameter("@strUserIDs", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(11).Value = _strUserIDs

                arParms(12) = New Npgsql.NpgsqlParameter("@numTeamType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(12).Value = _TeamType

                arParms(13) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(13).Value = Nothing
                arParms(13).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object
                objParam = arParms.ToArray()
                Dim dr As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_PerAnysCompanyList", objParam, True)
                _TotalRecords = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(7).Value)
                Return dr.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetPerAnaysActionItemList() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(14) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = DomainID


                arParms(2) = New Npgsql.NpgsqlParameter("@dtStartDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _startDate

                arParms(3) = New Npgsql.NpgsqlParameter("@dtEndDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(3).Value = _endDate

                arParms(4) = New Npgsql.NpgsqlParameter("@RepType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _RepType


                arParms(5) = New Npgsql.NpgsqlParameter("@Condition", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(5).Value = _Condition

                arParms(6) = New Npgsql.NpgsqlParameter("@bitTask", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _Task


                arParms(7) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(7).Value = _CurrentPage

                arParms(8) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(8).Value = _PageSize

                arParms(9) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(9).Direction = ParameterDirection.InputOutput
                arParms(9).Value = _TotalRecords

                arParms(10) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(10).Value = _columnName

                arParms(11) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(11).Value = _columnSortOrder

                arParms(12) = New Npgsql.NpgsqlParameter("@strUserIDs", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(12).Value = _strUserIDs

                arParms(13) = New Npgsql.NpgsqlParameter("@TeamType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(13).Value = _TeamType

                arParms(14) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(14).Value = Nothing
                arParms(14).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object
                objParam = arParms.ToArray()
                Dim dr As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_ActionItemlist", objParam, True)
                _TotalRecords = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(9).Value)

                Return dr.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetPerAnaysCaseList() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(13) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = DomainID


                arParms(2) = New Npgsql.NpgsqlParameter("@dtStartDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _startDate

                arParms(3) = New Npgsql.NpgsqlParameter("@dtEndDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(3).Value = _endDate

                arParms(4) = New Npgsql.NpgsqlParameter("@RepType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _RepType


                arParms(5) = New Npgsql.NpgsqlParameter("@Condition", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(5).Value = _Condition

                arParms(6) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = _CurrentPage

                arParms(7) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(7).Value = _PageSize

                arParms(8) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(8).Direction = ParameterDirection.InputOutput
                arParms(8).Value = _TotalRecords

                arParms(9) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(9).Value = _columnName

                arParms(10) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(10).Value = _columnSortOrder

                arParms(11) = New Npgsql.NpgsqlParameter("@strUserIDs", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(11).Value = _strUserIDs

                arParms(12) = New Npgsql.NpgsqlParameter("@TeamType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(12).Value = _TeamType

                arParms(13) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(13).Value = Nothing
                arParms(13).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object
                objParam = arParms.ToArray()
                Dim dr As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetPerAnaylsCaseList", objParam, True)
                _TotalRecords = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(8).Value)

                Return dr.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function WebLinks() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetWeblinkLabels", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Function UpdateWebLinks() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcWebLable1", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(1).Value = _WebLabel1

                arParms(2) = New Npgsql.NpgsqlParameter("@vcWebLable2", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(2).Value = _WebLabel2

                arParms(3) = New Npgsql.NpgsqlParameter("@vcWebLable3", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(3).Value = _WebLabel3


                arParms(4) = New Npgsql.NpgsqlParameter("@vcWebLable4", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(4).Value = _WebLabel4

                SqlDAL.ExecuteNonQuery(connString, "usp_SaveWeblinkLabels", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Function DemoteOrg() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _byteMode

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = UserCntID

                SqlDAL.ExecuteNonQuery(connString, "usp_DemoteAccount", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try

        End Function


        Public Function GetConIDCompIDDivIDFromEmail() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numCompanyID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _CompanyID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Direction = ParameterDirection.InputOutput
                arParms(1).Value = _DivisionID

                arParms(2) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Direction = ParameterDirection.InputOutput
                arParms(2).Value = _ContactID

                arParms(3) = New Npgsql.NpgsqlParameter("@vcCompanyName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(3).Direction = ParameterDirection.InputOutput
                arParms(3).Value = _CompanyName

                arParms(4) = New Npgsql.NpgsqlParameter("@Email", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(4).Value = _Email

                arParms(5) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = DomainID

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_GetComDivContactID", objParam, True)
                _CompanyID = CLng(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)
                _DivisionID = CLng(DirectCast(objParam, Npgsql.NpgsqlParameter())(1).Value)
                _ContactID = CLng(DirectCast(objParam, Npgsql.NpgsqlParameter())(2).Value)
                _CompanyName = CStr(DirectCast(objParam, Npgsql.NpgsqlParameter())(3).Value)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetWebAnlysDtl() As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetWebTrafficDetails", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetPagesVisited() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numTrackingID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _TrackingID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetPagesVisited", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function AdvancedSearch() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}


                Return True

            Catch ex As Exception

            End Try
        End Function
        Function CheckEmailWeb() As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@vcEmail", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(0).Value = _Email

                arParms(1) = New Npgsql.NpgsqlParameter("@vcWebsite", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(1).Value = _WebSite

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_CheckEmailWeb", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Function CheckDuplicate() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim boolIsDuplicate As Boolean = False

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@WhereCondition", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(0).Value = _WhereCondition

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_CheckDuplicate", arParms)
                If (ds.Tables.Count > 0) Then
                    If (ds.Tables(0).Rows.Count > 0) Then
                        boolIsDuplicate = True
                    End If
                End If
                Return boolIsDuplicate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GetDuplicateFields() As DataSet

            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

            arParms(0) = New Npgsql.NpgsqlParameter("@numFormId", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(0).Value = _FormId

            arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(1).Value = DomainID

            arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
            arParms(2).Value = Nothing
            arParms(2).Direction = ParameterDirection.InputOutput

            Dim ds As DataSet
            ds = SqlDAL.ExecuteDataset(connString, "USP_GetDuplicateFieldName", arParms)

            Return ds
        End Function


        Function MapDBColumnNameToProperty(ByVal dtFields As DataTable) As Hashtable
            Try
                Dim MappedFields As New Hashtable
                If (dtFields.Rows.Count > 0) Then
                    For Each dr As DataRow In dtFields.Rows
                        Select Case dr("vcDBColumnName").ToString()
                            Case "vcFirstName"
                                MappedFields.Add(dr("vcDBColumnName").ToString(), _FirstName) 'e.g. (DbFieldName,FieldsValue)
                            Case "vcLastName"
                                MappedFields.Add(dr("vcDBColumnName").ToString(), _LastName)
                            Case "vcEmail"
                                MappedFields.Add(dr("vcDBColumnName").ToString(), _Email)
                            Case "vcComPhone"
                                MappedFields.Add(dr("vcDBColumnName").ToString(), _ComPhone)
                            Case "vcStreet"
                                MappedFields.Add(dr("vcDBColumnName").ToString(), _PStreet)
                            Case "vcCity"
                                MappedFields.Add(dr("vcDBColumnName").ToString(), _PCity)
                            Case "numPhone"
                                MappedFields.Add(dr("vcDBColumnName").ToString(), _ContactPhone)
                            Case "numPhoneExtension"
                                MappedFields.Add(dr("vcDBColumnName").ToString(), _PhoneExt)
                            Case "vcWebSite"
                                MappedFields.Add(dr("vcDBColumnName").ToString(), _WebSite)
                            Case "numCell"
                                MappedFields.Add(dr("vcDBColumnName").ToString(), _Cell)
                            Case "numHomePhone"
                                MappedFields.Add(dr("vcDBColumnName").ToString(), _HomePhone)
                            Case "vcDivisionName"
                                MappedFields.Add(dr("vcDBColumnName").ToString(), _DivisionName)
                            Case "numCompanyIndustry"
                                MappedFields.Add(dr("vcDBColumnName").ToString(), _CompanyIndustry)
                            Case "vcPostalCode"
                                MappedFields.Add(dr("vcDBColumnName").ToString(), _PPostalCode)
                            Case "txtComments"
                                MappedFields.Add(dr("vcDBColumnName").ToString(), _Comments)
                            Case "vcCompanyName"
                                MappedFields.Add(dr("vcDBColumnName").ToString(), _CompanyName)
                            Case "vcFax"
                                MappedFields.Add(dr("vcDBColumnName").ToString(), _fax)
                            Case "vcShipPostCode"
                                MappedFields.Add(dr("vcDBColumnName").ToString(), _SPostalCode)
                            Case "vcShipCity"
                                MappedFields.Add(dr("vcDBColumnName").ToString(), _SCity)
                            Case "vcShipStreet"
                                MappedFields.Add(dr("vcDBColumnName").ToString(), _SStreet)
                            Case "vcComFax"
                                MappedFields.Add(dr("vcDBColumnName").ToString(), _ComFax)
                            Case "vcBillStreet"
                                MappedFields.Add(dr("vcDBColumnName").ToString(), _Street)
                            Case "vcBillCity"
                                MappedFields.Add(dr("vcDBColumnName").ToString(), _City)
                            Case "vcBillPostCode"
                                MappedFields.Add(dr("vcDBColumnName").ToString(), _PostalCode)
                        End Select
                    Next
                End If
                Return MappedFields
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Function GetStrWhere() As String
            Try
                Dim ds As DataSet
                Dim sbWhere As New System.Text.StringBuilder
                Dim strWhere As String
                Dim strValue As String
                DomainID = Web.HttpContext.Current.Session("DomainID")
                FormId = 24
                ds = GetDuplicateFields()
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        Dim MappedFields As New Hashtable
                        'Get Mapped fields from function
                        MappedFields = MapDBColumnNameToProperty(ds.Tables(0))
                        'Iterate All Fields and create where condition
                        Dim listEnumerator As IDictionaryEnumerator = MappedFields.GetEnumerator
                        While listEnumerator.MoveNext
                            If (listEnumerator.Value = Nothing) Then
                                strValue = String.Empty
                            Else
                                strValue = listEnumerator.Value.ToString().Trim().Replace("'", "''")
                            End If
                            If strValue.Length > 0 Then
                                sbWhere.Append(" and " & listEnumerator.Key & " = '" & strValue & "'")
                            End If
                        End While
                    End If
                End If
                If (sbWhere.ToString().Length > 2) Then
                    strWhere = sbWhere.ToString().Substring(4)
                Else
                    strWhere = sbWhere.Append(" 1=0 ").ToString()
                End If
                Return strWhere
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Function CheckDuplicateOrganization() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim boolIsDuplicate As Boolean = False

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@vcCompanyName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(0).Value = _CompanyName

                arParms(1) = New Npgsql.NpgsqlParameter("@vcWebsite", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(1).Value = _WebSite

                arParms(2) = New Npgsql.NpgsqlParameter("@vcComPhone", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(2).Value = _ComPhone

                arParms(3) = New Npgsql.NpgsqlParameter("@vcFirstName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(3).Value = _FirstName

                arParms(4) = New Npgsql.NpgsqlParameter("@vcLastName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(4).Value = _LastName

                arParms(5) = New Npgsql.NpgsqlParameter("@vcEmail", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(5).Value = _Email

                arParms(6) = New Npgsql.NpgsqlParameter("@numPhone", NpgsqlTypes.NpgsqlDbType.VarChar, 15)
                arParms(6).Value = _ContactPhone

                arParms(7) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = DomainID

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_CheckDuplicateOrganization", arParms)
                If (ds.Tables.Count > 0) Then
                    If (ds.Tables(0).Rows.Count > 0) Then
                        boolIsDuplicate = True
                    End If
                End If
                Return boolIsDuplicate
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Function CheckDuplicateContact() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim boolIsDuplicate As Boolean = False

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@vcFirstName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(0).Value = _FirstName

                arParms(1) = New Npgsql.NpgsqlParameter("@vcLastName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(1).Value = _LastName

                arParms(2) = New Npgsql.NpgsqlParameter("@vcEmail", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(2).Value = _Email

                arParms(3) = New Npgsql.NpgsqlParameter("@numPhone", NpgsqlTypes.NpgsqlDbType.VarChar, 15)
                arParms(3).Value = _ContactPhone

                arParms(4) = New Npgsql.NpgsqlParameter("@numcontactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _ContactID

                arParms(5) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(5).Value = DomainID

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_CheckDuplicateContact", arParms)
                If (ds.Tables.Count > 0) Then
                    If (ds.Tables(0).Rows.Count > 0) Then
                        boolIsDuplicate = True
                    End If
                End If
                Return boolIsDuplicate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageImportOrganizationContacts(Optional ByVal strNonBizCompanyID As Long = 0) As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numCompanyID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Direction = ParameterDirection.InputOutput
                arParms(1).Value = _CompanyID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Direction = ParameterDirection.InputOutput
                arParms(2).Value = _DivisionID

                arParms(3) = New Npgsql.NpgsqlParameter("@numNonBizCompanyID", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(3).Value = _strNonBizCompanyID

                arParms(4) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _byteMode

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageImportOrganizationContacts", objParam, True)
                _CompanyID = CLng(DirectCast(objParam, Npgsql.NpgsqlParameter())(1).Value)
                _DivisionID = CLng(DirectCast(objParam, Npgsql.NpgsqlParameter())(2).Value)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageImportActionItemReference(Optional ByVal strNonBizContactID As Long = 0) As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numCompanyID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Direction = ParameterDirection.InputOutput
                arParms(1).Value = _CompanyID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Direction = ParameterDirection.InputOutput
                arParms(2).Value = _DivisionID

                arParms(3) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Direction = ParameterDirection.InputOutput
                arParms(3).Value = _ContactID

                arParms(4) = New Npgsql.NpgsqlParameter("@numNonBizContactID", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(4).Value = If(_strNonBizContactID Is Nothing, strNonBizContactID, _strNonBizContactID)

                arParms(5) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _byteMode

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageImportActionItemReference", objParam, True)
                _CompanyID = CLng(DirectCast(objParam, Npgsql.NpgsqlParameter())(1).Value)
                _DivisionID = CLng(DirectCast(objParam, Npgsql.NpgsqlParameter())(2).Value)
                _ContactID = CLng(DirectCast(objParam, Npgsql.NpgsqlParameter())(3).Value)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function UpdateCustomerInformation() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ContactID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcFirstName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(2).Value = _FirstName

                arParms(3) = New Npgsql.NpgsqlParameter("@vcLastName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(3).Value = _LastName

                arParms(4) = New Npgsql.NpgsqlParameter("@vcEmail", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(4).Value = _Email

                arParms(5) = New Npgsql.NpgsqlParameter("@numPhone", NpgsqlTypes.NpgsqlDbType.VarChar, 15)
                arParms(5).Value = _ContactPhone

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateCustomerInformation", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try

        End Function
        Public Function GetCRMType() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID
                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DivisionID

                Dim ds As DataSet

                ds = SqlDAL.ExecuteDataset(connString, "USP_DivisionMaster_GetCRMType", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Sub GetOrganizationDetailForImport()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DivisionID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_Import_GetOrganizationDetails", arParms)

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        If Not IsDBNull(dr("numCompanyDiff")) Then
                            Me.CompanyDiff = CType(dr("numCompanyDiff"), Long)
                        End If

                        If Not IsDBNull(dr("vcCompanyDiff")) Then
                            Me.CompanyDiffValue = CType(dr("vcCompanyDiff"), String)
                        End If

                        If Not IsDBNull(dr("vcCompanyName")) Then
                            Me.CompanyName = CType(dr("vcCompanyName"), String)
                        End If

                        If Not IsDBNull(dr("vcProfile")) Then
                            Me.Profile = CType(dr("vcProfile"), Long)
                        End If

                        If Not IsDBNull(dr("numCompanyType")) Then
                            Me.CompanyType = CType(dr("numCompanyType"), Long)
                        End If

                        If Not IsDBNull(dr("numCampaignID")) Then
                            Me.CampaignID = CType(dr("numCampaignID"), Long)
                        End If

                        If Not IsDBNull(dr("vcComPhone")) Then
                            Me.ComPhone = CType(dr("vcComPhone"), String)
                        End If

                        If Not IsDBNull(dr("numAssignedTo")) Then
                            Me.AssignedTo = CType(dr("numAssignedTo"), Long)
                        End If

                        If Not IsDBNull(dr("numNoOfEmployeesId")) Then
                            Me.NumOfEmp = CType(dr("numNoOfEmployeesId"), Long)
                        End If

                        If Not IsDBNull(dr("numAnnualRevID")) Then
                            Me.AnnualRevenue = CType(dr("numAnnualRevID"), Long)
                        End If

                        If Not IsDBNull(dr("vcWebSite")) Then
                            Me.WebSite = CType(dr("vcWebSite"), String)
                        End If

                        If Not IsDBNull(dr("numFollowUpStatus")) Then
                            Me.FollowUpStatus = CType(dr("numFollowUpStatus"), Long)
                        End If

                        If Not IsDBNull(dr("numTerID")) Then
                            Me.TerritoryID = CType(dr("numTerID"), Long)
                        End If

                        If Not IsDBNull(dr("vcHow")) Then
                            Me.InfoSource = CType(dr("vcHow"), Long)
                        End If

                        If Not IsDBNull(dr("numCompanyIndustry")) Then
                            Me.CompanyIndustry = CType(dr("numCompanyIndustry"), Long)
                        End If

                        If Not IsDBNull(dr("numCompanyRating")) Then
                            Me.CompanyRating = CType(dr("numCompanyRating"), Long)
                        End If

                        If Not IsDBNull(dr("numStatusID")) Then
                            Me.StatusID = CType(dr("numStatusID"), Long)
                        End If

                        If Not IsDBNull(dr("numCompanyCredit")) Then
                            Me.CompanyCredit = CType(dr("numCompanyCredit"), Long)
                        End If

                        If Not IsDBNull(dr("vcComFax")) Then
                            Me.ComFax = CType(dr("vcComFax"), String)
                        End If

                        If Not IsDBNull(dr("txtComments")) Then
                            Me.Comments = CType(dr("txtComments"), String)
                        End If

                        If Not IsDBNull(dr("bitPublicFlag")) Then
                            Me.PublicFlag = CType(dr("bitPublicFlag"), Boolean)
                        End If

                        If Not IsDBNull(dr("bitActiveInActive")) Then
                            Me.ActiveInActive = CType(dr("bitActiveInActive"), Boolean)
                        End If

                        If Not IsDBNull(dr("vcWebLink1")) Then
                            Me.WebLink1 = CType(dr("vcWebLink1"), String)
                        End If

                        If Not IsDBNull(dr("vcWebLink2")) Then
                            Me.WebLink2 = CType(dr("vcWebLink2"), String)
                        End If

                        If Not IsDBNull(dr("vcWebLink3")) Then
                            Me.WebLink3 = CType(dr("vcWebLink3"), String)
                        End If

                        If Not IsDBNull(dr("vcWebLink4")) Then
                            Me.WebLink4 = CType(dr("vcWebLink4"), String)
                        End If

                        If Not IsDBNull(dr("numDivisionID")) Then
                            Me.DivisionID = CType(dr("numDivisionID"), Long)
                        End If

                        If Not IsDBNull(dr("numGrpID")) Then
                            Me.GroupID = CType(dr("numGrpID"), Long)
                        End If

                        If Not IsDBNull(dr("tintCRMType")) Then
                            Me.CRMType = CType(dr("tintCRMType"), Integer)
                        End If

                        If Not IsDBNull(dr("numCurrencyID")) Then
                            Me.CurrencyID = CType(dr("numCurrencyID"), Long)
                        End If

                        If Not IsDBNull(dr("bitNoTax")) Then
                            Me.NoTax = CType(dr("bitNoTax"), Boolean)
                        End If

                        If Not IsDBNull(dr("numBillingDays")) Then
                            Me.BillingDays = CType(dr("numBillingDays"), Long)
                        End If

                        If Not IsDBNull(dr("bitOnCreditHold")) Then
                            Me.OnCreditHold = CType(dr("bitOnCreditHold"), Boolean)
                        End If

                        If Not IsDBNull(dr("numDefaultExpenseAccountID")) Then
                            Me.DefaultExpenseAccountID = CType(dr("numDefaultExpenseAccountID"), Long)
                        End If

                        If Not IsDBNull(dr("vcNonBizCompanyID")) Then
                            Me.NonBizCompanyID = CType(dr("vcNonBizCompanyID"), String)
                        End If
                    Next
                Else
                    Me.DivisionID = 0
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub SaveOrganizationForImport()
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDivisionID", DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcCompanyName", CompanyName, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcProfile", Profile, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numCompanyType", CompanyType, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numNoOfEmployeesId", NumOfEmp, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numAnnualRevID", AnnualRevenue, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcWebSite", WebSite, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcHow", InfoSource, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numCompanyIndustry", CompanyIndustry, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numCompanyRating", CompanyRating, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numCompanyCredit", CompanyCredit, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@txtComments", Comments, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcWebLink1", WebLink1, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcWebLink2", WebLink2, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcWebLink3", WebLink3, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcWebLink4", WebLink4, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numCompanyDiff", CompanyDiff, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcCompanyDiff", CompanyDiffValue, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numCampaignID", CampaignID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcComPhone", ComPhone, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numAssignedTo", AssignedTo, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numFollowUpStatus", FollowUpStatus, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numTerID", TerritoryID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numStatusID", StatusID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcComFax", ComFax, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@bitPublicFlag", PublicFlag, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitActiveInActive", ActiveInActive, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@numGrpID", GroupID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintCRMType", CRMType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@numCurrencyID", Convert.ToInt64(CurrencyID), NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@bitNoTax", NoTax, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@numBillingDays", BillingDays, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitOnCreditHold", OnCreditHold, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@numDefaultExpenseAccountID", DefaultExpenseAccountID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numPartenerSource", PartenerSource, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_Import_SaveOrganizationDetails", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub
    End Class
End Namespace

