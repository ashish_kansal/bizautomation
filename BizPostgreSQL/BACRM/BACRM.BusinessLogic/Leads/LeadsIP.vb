﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Leads

    Public Class LeadsIP
        Inherits CLeads


        Private _DocCount As Integer
        Private _AssociationTo As Integer
        Private _AssociationFrom As Integer
        Private _Address As String
        Private _AssignedToName As String
        Private _CompanyIndustryName As String
        Private _CompanyRatingName As String
        Private _NumOfEmpName As String
        Private _CompanyTypeName As String
        Private _FollowUpStatusName As String
        Private _AnnualRevenueName As String
        Private _StatusIDName As String
        Private _CampaignIDName As String
        Private _ProfileName As String
        Private _TerritoryIDName As String
        Private _InfoSourceName As String
        Private _GroupIDName As String
        Private _PositionName As String

        Private _numPartenerSource As String
        Private _numPartenerContact As String
        Private _numPartenerContactId As Long
        Private _numPartenerSourceId As Long
        Private _LastFollowup As String
        Private _NextFollowup As String

        Public Property numPartenerContactId() As Long
            Get
                Return _numPartenerContactId
            End Get
            Set(ByVal Value As Long)
                _numPartenerContactId = Value
            End Set
        End Property
        Public Property numPartenerSource() As String
            Get
                Return _numPartenerSource
            End Get
            Set(ByVal Value As String)
                _numPartenerSource = Value
            End Set
        End Property

        Public Property numPartenerSourceId() As Long
            Get
                Return _numPartenerSourceId
            End Get
            Set(ByVal value As Long)
                _numPartenerSourceId = value
            End Set
        End Property
        Public Property numPartenerContact() As String
            Get
                Return _numPartenerContact
            End Get
            Set(ByVal Value As String)
                _numPartenerContact = Value
            End Set
        End Property
        Public Property PositionName() As String
            Get
                Return _PositionName
            End Get
            Set(ByVal Value As String)
                _PositionName = Value
            End Set
        End Property


        Private _DripCampaignName As String
        Public Property DripCampaignName() As String
            Get
                Return _DripCampaignName
            End Get
            Set(ByVal value As String)
                _DripCampaignName = value
            End Set
        End Property
        Public Property GroupIDName() As String
            Get
                Return _GroupIDName
            End Get
            Set(ByVal Value As String)
                _GroupIDName = Value
            End Set
        End Property



        Public Property InfoSourceName() As String
            Get
                Return _InfoSourceName
            End Get
            Set(ByVal Value As String)
                _InfoSourceName = Value
            End Set
        End Property



        Public Property TerritoryIDName() As String
            Get
                Return _TerritoryIDName
            End Get
            Set(ByVal Value As String)
                _TerritoryIDName = Value
            End Set
        End Property



        Public Property ProfileName() As String
            Get
                Return _ProfileName
            End Get
            Set(ByVal Value As String)
                _ProfileName = Value
            End Set
        End Property


        Public Property CampaignIDName() As String
            Get
                Return _CampaignIDName
            End Get
            Set(ByVal Value As String)
                _CampaignIDName = Value
            End Set
        End Property

        Public Property StatusIDName() As String
            Get
                Return _StatusIDName
            End Get
            Set(ByVal Value As String)
                _StatusIDName = Value
            End Set
        End Property

        Public Property AnnualRevenueName() As String
            Get
                Return _AnnualRevenueName
            End Get
            Set(ByVal Value As String)
                _AnnualRevenueName = Value
            End Set
        End Property

        Public Property FollowUpStatusName() As String
            Get
                Return _FollowUpStatusName
            End Get
            Set(ByVal Value As String)
                _FollowUpStatusName = Value
            End Set
        End Property

        Public Property CompanyTypeName() As String
            Get
                Return _CompanyTypeName
            End Get
            Set(ByVal Value As String)
                _CompanyTypeName = Value
            End Set
        End Property

        Public Property NumOfEmpName() As String
            Get
                Return _NumOfEmpName
            End Get
            Set(ByVal Value As String)
                _NumOfEmpName = Value
            End Set
        End Property

        Public Property CompanyRatingName() As String
            Get
                Return _CompanyRatingName
            End Get
            Set(ByVal Value As String)
                _CompanyRatingName = Value
            End Set
        End Property

        Public Property CompanyIndustryName() As String
            Get
                Return _CompanyIndustryName
            End Get
            Set(ByVal Value As String)
                _CompanyIndustryName = Value
            End Set
        End Property

        Public Property AssignedToName() As String
            Get
                Return _AssignedToName
            End Get
            Set(ByVal Value As String)
                _AssignedToName = Value
            End Set
        End Property

        Public Property Address() As String
            Get
                Return _Address
            End Get
            Set(ByVal Value As String)
                _Address = Value
            End Set
        End Property

        Public Property AssociationFrom() As Integer
            Get
                Return _AssociationFrom
            End Get
            Set(ByVal Value As Integer)
                _AssociationFrom = Value
            End Set
        End Property

        Public Property AssociationTo() As Integer
            Get
                Return _AssociationTo
            End Get
            Set(ByVal Value As Integer)
                _AssociationTo = Value
            End Set
        End Property

        Public Property DocCount() As Integer
            Get
                Return _DocCount
            End Get
            Set(ByVal Value As Integer)
                _DocCount = Value
            End Set
        End Property

        Private _ShippingAddress As String
        Public Property ShippingAddress() As String
            Get
                Return _ShippingAddress
            End Get
            Set(ByVal Value As String)
                _ShippingAddress = Value
            End Set
        End Property

        Private _PartnerCode As String
        Public Property PartnerCode() As String
            Get
                Return _PartnerCode
            End Get
            Set(ByVal Value As String)
                _PartnerCode = Value
            End Set
        End Property
        Public Property ShippingZone As Long

        Public Property LastFollowup() As String
            Get
                Return _LastFollowup
            End Get
            Set(ByVal Value As String)
                _LastFollowup = Value
            End Set
        End Property

        Public Property NextFollowup() As String
            Get
                Return _NextFollowup
            End Get
            Set(ByVal Value As String)
                _NextFollowup = Value
            End Set
        End Property

        Private _Purchaseincentives As String
        Public Property Purchaseincentives As String
            Get
                Return _Purchaseincentives
            End Get
            Set(value As String)
                _Purchaseincentives = value
            End Set
        End Property

        Private _intDropShip As Int16
        Public Property intDropShip As Int16
            Get
                Return _intDropShip
            End Get
            Set(value As Int16)
                _intDropShip = value
            End Set
        End Property

        Public Property intDropShipName As String

        Private _vcFropShip As String
        Public Property vcFropShip As String
            Get
                Return _vcFropShip
            End Get
            Set(value As String)
                _vcFropShip = value
            End Set
        End Property

        Public Property ShippingService As Long

        Private _ShippingServiceName As String
        Public Property ShippingServiceName() As String
            Get
                Return _ShippingServiceName
            End Get
            Set(ByVal value As String)
                _ShippingServiceName = value
            End Set
        End Property

        Private _SignatureTypeName As String
        Public Property SignatureTypeName() As String
            Get
                Return _SignatureTypeName
            End Get
            Set(ByVal Value As String)
                _SignatureTypeName = Value
            End Set
        End Property
        Private Property _numShipmentMethod As Integer
        Public Property ShipmentMethod() As Integer
            Get
                Return _numShipmentMethod
            End Get
            Set(value As Integer)
                _numShipmentMethod = value
            End Set
        End Property
        Public Property ShipmentMethodName() As String

        Public Function GetCompanyDetails() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = Me.DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = Me.DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = Me.ClientTimeZoneOffset

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim dt As DataTable = SqlDAL.ExecuteDatable(connString, "usp_GetCompanyInfoDtlPl", arParms)

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    For Each dr As DataRow In dt.Rows
                        If Not IsDBNull(dr("vcCompanyName")) Then
                            Me.CompanyName = dr("vcCompanyName").ToString
                        End If
                        If Not IsDBNull(dr("vcComPhone")) Then
                            Me.ComPhone = dr("vcComPhone").ToString
                        End If
                        If Not IsDBNull(dr("vcComFax")) Then
                            Me.ComFax = dr("vcComFax").ToString
                        End If
                        If Not IsDBNull(dr("numAssignedTo")) Then
                            Me.AssignedTo = CType(dr("numAssignedTo"), Long)
                        End If
                        If Not IsDBNull(dr("numCompanyIndustry")) Then
                            Me.CompanyIndustry = CType(dr("numCompanyIndustry"), Long)
                        End If
                        If Not IsDBNull(dr("numCompanyRating")) Then
                            Me.CompanyRating = CType(dr("numCompanyRating"), Long)
                        End If
                        If Not IsDBNull(dr("numNoOfEmployeesID")) Then
                            Me.NumOfEmp = CType(dr("numNoOfEmployeesID"), Long)
                        End If
                        If Not IsDBNull(dr("numAnnualRevID")) Then
                            Me.AnnualRevenue = CType(dr("numAnnualRevID"), Long)
                        End If
                        If Not IsDBNull(dr("numCompanyStatus")) Then
                            Me.StatusID = CType(dr("numCompanyStatus"), Long)
                        End If
                        If Not IsDBNull(dr("vcProfile")) Then
                            Me.Profile = CType(dr("vcProfile"), Long)
                        End If
                        If Not IsDBNull(dr("vcWebSite")) Then
                            Me.WebSite = CType(dr("vcWebSite"), String)
                        End If
                        If Not IsDBNull(dr("numCampaignID")) Then
                            Me.CampaignID = CType(dr("numCampaignID"), Long)
                        End If
                        If Not IsDBNull(dr("vcInfoSource")) Then
                            Me.InfoSource = CType(dr("vcInfoSource"), Long)
                        End If
                        If Not IsDBNull(dr("numTerID")) Then
                            Me.TerritoryID = CType(dr("numTerID"), Long)
                        End If
                        If Not IsDBNull(dr("numFollowUpStatus")) Then
                            Me.FollowUpStatus = CType(dr("numFollowUpStatus"), Long)
                        End If
                        If Not IsDBNull(dr("bitPublicFlag")) Then
                            Me.PublicFlag = CType(dr("bitPublicFlag"), Boolean)
                        End If
                        If Not IsDBNull(dr("DocumentCount")) Then
                            _DocCount = CType(dr("DocumentCount"), Integer)
                        End If
                        If Not IsDBNull(dr("AssociateCountTo")) Then
                            Me.AssociationTo = CType(dr("AssociateCountTo"), Integer)
                        End If
                        If Not IsDBNull(dr("numCompanyType")) Then
                            Me.CompanyType = CType(dr("numCompanyType"), Long)
                        End If
                        If Not IsDBNull(dr("AssociateCountFrom")) Then
                            Me.AssociationFrom = CType(dr("AssociateCountFrom"), Integer)
                        End If
                        If Not IsDBNull(dr("Address")) Then
                            Me.Address = CType(dr("Address"), String)
                        End If
                        If Not IsDBNull(dr("ShippingAddress")) Then
                            Me.ShippingAddress = CType(dr("ShippingAddress"), String)
                        End If
                        If Not IsDBNull(dr("vcWebLink1")) Then
                            Me.WebLink1 = CType(dr("vcWebLink1"), String)
                        End If
                        If Not IsDBNull(dr("vcWebLink2")) Then
                            Me.WebLink2 = CType(dr("vcWebLink2"), String)
                        End If
                        If Not IsDBNull(dr("vcWebLink3")) Then
                            Me.WebLink3 = CType(dr("vcWebLink3"), String)
                        End If
                        If Not IsDBNull(dr("vcWebLink4")) Then
                            Me.WebLink4 = CType(dr("vcWebLink4"), String)
                        End If
                        If Not IsDBNull(dr("vcWebLabel1")) Then
                            Me.WebLabel1 = CType(dr("vcWebLabel1"), String)
                        End If
                        If Not IsDBNull(dr("vcWebLabel2")) Then
                            Me.WebLabel2 = CType(dr("vcWebLabel2"), String)
                        End If
                        If Not IsDBNull(dr("vcWebLabel3")) Then
                            Me.WebLabel3 = CType(dr("vcWebLabel3"), String)
                        End If
                        If Not IsDBNull(dr("vcWebLabel4")) Then
                            Me.WebLabel4 = CType(dr("vcWebLabel4"), String)
                        End If
                        If Not IsDBNull(dr("numAssignedToName")) Then
                            Me.AssignedToName = CType(dr("numAssignedToName"), String)
                        End If
                        If Not IsDBNull(dr("numCompanyIndustryName")) Then
                            Me.CompanyIndustryName = CType(dr("numCompanyIndustryName"), String)
                        End If
                        If Not IsDBNull(dr("numCompanyRatingName")) Then
                            Me.CompanyRatingName = CType(dr("numCompanyRatingName"), String)
                        End If
                        If Not IsDBNull(dr("numNoOfEmployeesIDName")) Then
                            Me.NumOfEmpName = CType(dr("numNoOfEmployeesIDName"), String)
                        End If
                        If Not IsDBNull(dr("numCompanyTypeName")) Then
                            Me.CompanyTypeName = CType(dr("numCompanyTypeName"), String)
                        End If
                        If Not IsDBNull(dr("numFollowUpStatusName")) Then
                            Me.FollowUpStatusName = CType(dr("numFollowUpStatusName"), String)
                        End If
                        If Not IsDBNull(dr("numCompanyStatusName")) Then
                            Me.StatusIDName = CType(dr("numCompanyStatusName"), String)
                        End If
                        If Not IsDBNull(dr("numAnnualRevIDName")) Then
                            Me.AnnualRevenueName = CType(dr("numAnnualRevIDName"), String)
                        End If
                        If Not IsDBNull(dr("numCampaignIDName")) Then
                            Me.CampaignIDName = CType(dr("numCampaignIDName"), String)
                        End If
                        If Not IsDBNull(dr("vcProfileName")) Then
                            Me.ProfileName = CType(dr("vcProfileName"), String)
                        End If
                        If Not IsDBNull(dr("numTerIDName")) Then
                            Me.TerritoryIDName = CType(dr("numTerIDName"), String)
                        End If
                        If Not IsDBNull(dr("vcInfoSourceName")) Then
                            Me.InfoSourceName = CType(dr("vcInfoSourceName"), String)
                        End If
                        If Not IsDBNull(dr("numGrpID")) Then
                            Me.GroupID = CType(dr("numGrpID"), Long)
                        End If
                        If Not IsDBNull(dr("numGrpIDName")) Then
                            Me.GroupIDName = CType(dr("numGrpIDName"), String)
                        End If
                        If Not IsDBNull(dr("txtComments")) Then
                            Me.Comments = CType(dr("txtComments"), String)
                        End If

                        If Not IsDBNull(dr("numCompanyDiff")) Then
                            Me.CompanyDiff = CType(dr("numCompanyDiff"), Long)
                        End If

                        If Not IsDBNull(dr("numCompanyDiffName")) Then
                            Me.CompanyDiffName = CType(dr("numCompanyDiffName"), String)
                        End If

                        If Not IsDBNull(dr("vcCompanyDiff")) Then
                            Me.CompanyDiffValue = CType(dr("vcCompanyDiff"), String)
                        End If

                        If Not IsDBNull(dr("bitActiveInActive")) Then
                            Me.ActiveInActive = CType(dr("bitActiveInActive"), Boolean)
                        End If

                        If Not IsDBNull(dr("numShareWith")) Then
                            Me.ShareRecordWith = CType(dr("numShareWith"), String)
                        End If

                        If Not IsDBNull(dr("bitOnCreditHold")) Then
                            Me.OnCreditHold = CType(dr("bitOnCreditHold"), Boolean)
                        End If
                        If Not IsDBNull(dr("numBillingDays")) Then
                            Me.BillingDays = CType(dr("numBillingDays"), Long)
                        End If
                        If Not IsDBNull(dr("numDefaultPaymentMethod")) Then
                            Me.DefaultPaymentMethod = CType(dr("numDefaultPaymentMethod"), Long)
                        End If
                        If Not IsDBNull(dr("tintCRMType")) Then
                            Me.CRMType = CType(dr("tintCRMType"), Long)
                        End If
                        If Not IsDBNull(dr("vcPartnerCode")) Then
                            Me.PartnerCode = CType(dr("vcPartnerCode"), String)
                        End If
                        If Not IsDBNull(dr("numPartenerSourceId")) Then
                            Me.numPartenerSourceId = CType(dr("numPartenerSourceId"), Long)
                        End If
                        If Not IsDBNull(dr("numPartenerSource")) Then
                            Me.numPartenerSource = CType(dr("numPartenerSource"), String)
                        End If
                        If Not IsDBNull(dr("numPartenerContact")) Then
                            Me.numPartenerContact = CType(dr("numPartenerContact"), String)
                        End If
                        If Not IsDBNull(dr("numPartenerContactId")) Then
                            Me.numPartenerContactId = CType(dr("numPartenerContactId"), Long)
                        End If
                        If Not IsDBNull(dr("bitEmailToCase")) Then
                            Me.bitEmailToCase = CType(dr("bitEmailToCase"), Boolean)
                        End If
                        If Not IsDBNull(dr("vcCreditCards")) Then
                            Me.vcCreditCards = CType(dr("vcCreditCards"), String)
                        End If
                        If Not IsDBNull(dr("bitEcommerceAccess")) Then
                            Me.IsEcommerceAccess = CType(dr("bitEcommerceAccess"), Boolean)
                        End If
                        If Not IsDBNull(dr("numShippingZone")) Then
                            Me.ShippingZone = CType(dr("numShippingZone"), Long)
                        End If
                        If Not IsDBNull(dr("vcLastFollowup")) Then
                            Me.LastFollowup = CType(dr("vcLastFollowup"), String)
                        End If
                        If Not IsDBNull(dr("vcNextFollowup")) Then
                            Me.NextFollowup = CType(dr("vcNextFollowup"), String)
                        End If
                        If Not IsDBNull(dr("numShippingService")) Then
                            Me.ShippingService = CType(dr("numShippingService"), Long)
                        End If
                        If Not IsDBNull(dr("vcShippingService")) Then
                            Me.ShippingServiceName = CType(dr("vcShippingService"), String)
                        End If
                        If Not IsDBNull(dr("vcSignatureType")) Then
                            Me.SignatureTypeName = CType(dr("vcSignatureType"), String)
                        End If
                        If Not IsDBNull(dr("intShippingCompany")) Then
                            Me.ShipmentMethod = CType(dr("intShippingCompany"), Long)
                        End If
                        If Not IsDBNull(dr("vcShipVia")) Then
                            Me.ShipmentMethodName = CType(dr("vcShipVia"), String)
                        End If
                        If Not IsDBNull(dr("vcFirstName")) Then
                            Me.FirstName = dr("vcFirstName").ToString
                        End If
                        If Not IsDBNull(dr("vcLastName")) Then
                            Me.LastName = dr("vcLastName").ToString
                        End If
                        If Not IsDBNull(dr("numPhone")) Then
                            Me.ContactPhone = dr("numPhone").ToString
                        End If
                        If Not IsDBNull(dr("numPhoneExtension")) Then
                            Me.PhoneExt = dr("numPhoneExtension").ToString
                        End If
                        If Not IsDBNull(dr("vcEmail")) Then
                            Me.Email = dr("vcEmail").ToString
                        End If
                        If Not IsDBNull(dr("vcGivenName")) Then
                            Me.GivenName = dr("vcGivenName").ToString
                        End If
                        If Not IsDBNull(dr("vcLastFollowup")) Then
                            Me.LastFollowup = CType(dr("vcLastFollowup"), String)
                        End If
                        If Not IsDBNull(dr("vcNextFollowup")) Then
                            Me.NextFollowup = CType(dr("vcNextFollowup"), String)
                        End If
                        If Not IsDBNull(dr("Purchaseincentives")) Then
                            Me.Purchaseincentives = CType(dr("Purchaseincentives"), String)
                        End If
                        If Not IsDBNull(dr("intDropShip")) Then
                            Me.intDropShip = CType(dr("intDropShip"), Int16)
                        End If
                        If Not IsDBNull(dr("vcFropShip")) Then
                            Me.vcFropShip = CType(dr("vcFropShip"), String)
                        End If
                    Next
                End If
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function GetLeadDetails() As Boolean
            Try
                GetCompanyDetails()
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = ContactID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = ClientTimeZoneOffset

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim dt As DataTable = SqlDAL.ExecuteDatable(connString, "usp_GetContactDTlPL", arParms)

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    For Each dr As DataRow In dt.Rows
                        If Not IsDBNull(dr("vcFirstName")) Then
                            Me.FirstName = dr("vcFirstName").ToString
                        End If
                        If Not IsDBNull(dr("vcLastName")) Then
                            Me.LastName = dr("vcLastName").ToString
                        End If
                        If Not IsDBNull(dr("numPhone")) Then
                            Me.ContactPhone = dr("numPhone").ToString
                        End If

                        If Not IsDBNull(dr("numPhoneExtension")) Then
                            Me.PhoneExt = dr("numPhoneExtension").ToString
                        End If

                        If Not IsDBNull(dr("vcPosition")) Then
                            Me.Position = CType(dr("vcPosition"), Long)
                        End If
                        If Not IsDBNull(dr("vcEmail")) Then
                            Me.Email = dr("vcEmail").ToString
                        End If

                        If Not IsDBNull(dr("vcTitle")) Then
                            Me.Title = dr("vcTitle").ToString
                        End If
                        If Not IsDBNull(dr("vcPositionName")) Then
                            Me.PositionName = CType(dr("vcPositionName"), String)
                        End If
                        If Not IsDBNull(dr("vcECampName")) Then
                            Me.DripCampaignName = CType(dr("vcECampName"), String)
                        End If
                        If Not IsDBNull(dr("numECampaignID")) Then
                            Me.DripCampaign = CType(dr("numECampaignID"), Long)
                        End If
                        If Not IsDBNull(dr("numConEmailCampID")) Then
                            Me.ContactECampaignID = CType(dr("numConEmailCampID"), Long)
                        End If
                        If Not IsDBNull(dr("vcPartnerCode")) Then
                            Me.PartnerCode = CType(dr("vcPartnerCode"), String)
                        End If
                        If Not IsDBNull(dr("numPartenerSourceId")) Then
                            Me.numPartenerSourceId = CType(dr("numPartenerSourceId"), Long)
                        End If
                        If Not IsDBNull(dr("numPartenerSource")) Then
                            Me.numPartenerSource = CType(dr("numPartenerSource"), String)
                        End If
                        If Not IsDBNull(dr("numPartenerContact")) Then
                            Me.numPartenerContact = CType(dr("numPartenerContact"), String)
                        End If
                        If Not IsDBNull(dr("numPartenerContactId")) Then
                            Me.numPartenerContactId = CType(dr("numPartenerContactId"), Long)
                        End If
                        If Not IsDBNull(dr("vcLastFollowup")) Then
                            Me.LastFollowup = CType(dr("vcLastFollowup"), String)
                        End If
                        If Not IsDBNull(dr("vcNextFollowup")) Then
                            Me.NextFollowup = CType(dr("vcNextFollowup"), String)
                        End If
                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function


    End Class

End Namespace