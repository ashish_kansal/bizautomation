'**********************************************************************************
' <CCustomers.vb>
' 
' 	CHANGE CONTROL:
'	
'	AUTHOR: Goyal 	DATE:28-Feb-05 	VERSION	CHANGES	KEYSTRING:
'**********************************************************************************



Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Customers

    '**********************************************************************************
    ' Module Name  : None
    ' Module Type  : CCommon
    ' 
    ' Description  : This is the business logic classe for Common Stuff
    '**********************************************************************************
    Public Class CCustomers
        Inherits BACRM.BusinessLogic.CBusinessBase


        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Goyal 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Goyal 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region

        'defining column of table
        Private _CustomerID As String = String.Empty
        Private _CompanyName As String = String.Empty
        Private _ContactName As String = String.Empty
        Private _ContactTitle As String = String.Empty
        Private _Address As String = String.Empty
        Private _City As String = String.Empty
        Private _Region As String = String.Empty
        Private _PostalCode As String = String.Empty
        Private _Country As String = String.Empty
        Private _Phone As String = String.Empty
        Private _Fax As String = String.Empty

        Public Property CustomerID() As String
            Get
                Return _CustomerID
            End Get
            Set(ByVal Value As String)
                _CustomerID = Value
            End Set
        End Property
        Public Property CompanyName() As String
            Get
                Return _CompanyName
            End Get
            Set(ByVal Value As String)
                _CompanyName = Value
            End Set
        End Property

        Public Property ContactTitle() As String
            Get
                Return _ContactTitle
            End Get
            Set(ByVal Value As String)
                _ContactTitle = Value
            End Set
        End Property
        Public Property Address() As String
            Get
                Return _Address
            End Get
            Set(ByVal Value As String)
                _Address = Value
            End Set
        End Property
        Public Property City() As String
            Get
                Return _City
            End Get
            Set(ByVal Value As String)
                _City = Value
            End Set
        End Property
        Public Property Region() As String
            Get
                Return _Region
            End Get
            Set(ByVal Value As String)
                _Region = Value
            End Set
        End Property
        Public Property PostalCode() As String
            Get
                Return _PostalCode
            End Get
            Set(ByVal Value As String)
                _PostalCode = Value
            End Set
        End Property
        Public Property Country() As Integer
            Get
                Return _Country
            End Get
            Set(ByVal Value As Integer)
                _Country = Value
            End Set
        End Property

        Public Property Phone() As String
            Get
                Return _Phone
            End Get
            Set(ByVal Value As String)
                _Phone = Value
            End Set
        End Property
        Public Property Fax() As String
            Get
                Return _Fax
            End Get
            Set(ByVal Value As String)
                _Fax = Value
            End Set
        End Property

        Public Property SearchText As String
        Public Property PageIndex As Long
        Public Property PageSize As Long
        Public Property TotalRecords As Long

        'To Add new record in database
        Public Function CreateRecord() As Boolean
            'This connection will be replace by function ,which will get connection string from database
            Dim connection As New Npgsql.NpgSqlConnection("Server=ajeet;Database=Northwind;uid=sa;pwd=sa;")
            Dim Custint As Integer
            Try
                ' Set up parameters (1 input and 3 output) 
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(10) {}

                ' @CustomerID Out Parameter 
                arParms(0) = New Npgsql.NpgsqlParameter("@CustomerID", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(0).Value = _CustomerID

                ' @CompanyName Output Parameter
                arParms(1) = New Npgsql.NpgsqlParameter("@CompanyName", NpgsqlTypes.NpgsqlDbType.VarChar, 40)
                arParms(1).Value = _CompanyName

                ' @ContactName Output Parameter
                arParms(2) = New Npgsql.NpgsqlParameter("@ContactName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(2).Value = _ContactName

                ' @ContactTitle Output Parameter
                arParms(3) = New Npgsql.NpgsqlParameter("@ContactTitle", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(3).Value = _ContactTitle

                arParms(4) = New Npgsql.NpgsqlParameter("@Address", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(4).Value = _Address

                arParms(5) = New Npgsql.NpgsqlParameter("@City", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(5).Value = _City

                arParms(6) = New Npgsql.NpgsqlParameter("@Region", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(6).Value = _Region

                arParms(7) = New Npgsql.NpgsqlParameter("@PostalCode", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(7).Value = _PostalCode

                arParms(8) = New Npgsql.NpgsqlParameter("@Country", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(8).Value = _Country

                arParms(9) = New Npgsql.NpgsqlParameter("@Phone", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(9).Value = _Phone

                arParms(10) = New Npgsql.NpgsqlParameter("@Fax", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(10).Value = _Fax



                Custint = SqlDAL.ExecuteNonQuery(connection, "spCustomerAdd", arParms)

                Return True
            Catch ex As Exception
                Return False
            Finally
                If Not connection Is Nothing Then
                    CType(connection, IDisposable).Dispose()
                End If
            End Try
            connection.Close()
        End Function 'AddCustomer
        'To Update Database
        Public Function UpdateRecrod() As Boolean
            Dim connection As New Npgsql.NpgSqlConnection("Server=ajeet;Database=Northwind;uid=sa;pwd=sa;")
            Dim Custint As Integer
            Try
                ' Set up parameters (1 input and 3 output) 
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(10) {}

                ' @ProductID Input Parameter 
                arParms(0) = New Npgsql.NpgsqlParameter("@CustomerID", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(0).Value = _CustomerID

                ' @ProductName Output Parameter
                arParms(1) = New Npgsql.NpgsqlParameter("@CompanyName", NpgsqlTypes.NpgsqlDbType.VarChar, 40)
                arParms(1).Value = _CompanyName

                ' @UnitPrice Output Parameter
                arParms(2) = New Npgsql.NpgsqlParameter("@ContactName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(2).Value = _ContactName

                ' @QtyPerUnit Output Parameter
                arParms(3) = New Npgsql.NpgsqlParameter("@ContactTitle", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(3).Value = _ContactTitle

                arParms(4) = New Npgsql.NpgsqlParameter("@Address", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(4).Value = _Address

                arParms(5) = New Npgsql.NpgsqlParameter("@City", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(5).Value = _City

                arParms(6) = New Npgsql.NpgsqlParameter("@Region", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(6).Value = _Region

                arParms(7) = New Npgsql.NpgsqlParameter("@PostalCode", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(7).Value = _PostalCode

                arParms(8) = New Npgsql.NpgsqlParameter("@Country", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(8).Value = _Country

                arParms(9) = New Npgsql.NpgsqlParameter("@Phone", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(9).Value = _Phone

                arParms(10) = New Npgsql.NpgsqlParameter("@Fax", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(10).Value = _Fax

                ' Call ExecuteNonQuery static method of SqlDAL class
                ' We pass in database connection string,stored procedure name and an array of Npgsql.NpgsqlParameter object
                Custint = SqlDAL.ExecuteNonQuery(connection, "spCustomerUpdate", arParms)

                Return True
            Catch ex As Exception
                Return False
            Finally
                If Not connection Is Nothing Then
                    CType(connection, IDisposable).Dispose()
                End If
            End Try

        End Function
        'To Get Data from database
        Public Function ReturnRow() As Boolean
            Dim connection As New Npgsql.NpgSqlConnection("Server=ajeet;Database=Northwind;uid=sa;pwd=sa;")
            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
            Try
                '@CustomerID  Input Parameter 
                arParms(0) = New Npgsql.NpgsqlParameter("@CustomerID", NpgsqlTypes.NpgsqlDbType.Varchar, 25)
                arParms(0).Value = _CustomerID
                'here i am using dataset to retreive data ,this can be replace by othere 
                Dim ds As DataSet = SqlDAL.ExecuteDataset(connection, "spCustomerGet", arParms)

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        _CustomerID = dr("CustomerID").ToString()
                        _CompanyName = dr("CompanyName").ToString()
                        _ContactName = dr("ContactName").ToString()
                        _ContactTitle = dr("ContactTitle").ToString()
                        _Address = dr("Address").ToString()
                        _City = dr("City").ToString()
                        _Region = dr("Region").ToString()
                        _PostalCode = dr("PostalCode").ToString()
                        _Country = dr("Country").ToString()
                        _Phone = dr("Phone").ToString()
                        _Fax = dr("Fax").ToString()
                    Next
                End If

                Return True
            Catch ex As Exception
                Return False
            Finally
                If Not connection Is Nothing Then
                    CType(connection, IDisposable).Dispose()
                End If
            End Try
        End Function
        'To delete data from database
        Public Function RemoveRecrod() As Boolean
            Dim connection As New Npgsql.NpgSqlConnection("Server=ajeet;Database=Northwind;uid=sa;pwd=sa;")
            Dim Custint As Integer
            Try
                ' Set up parameters (1 input and 3 output) 
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                ' @ProductID Input Parameter 
                arParms(0) = New Npgsql.NpgsqlParameter("@CustomerID", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(0).Value = _CustomerID

                Custint = SqlDAL.ExecuteNonQuery(connection, "spCustomerDelete", _CustomerID)

                Return True
            Catch ex As Exception
                Return False
            Finally
                If Not connection Is Nothing Then
                    CType(connection, IDisposable).Dispose()
                End If
            End Try

        End Function

        Public Function SearchCustomerByName() As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@str", SearchText, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numPageIndex", PageIndex, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numPageSize", PageSize, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numTotalRecords", 0, NpgsqlTypes.NpgsqlDbType.Bigint, 18, ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object = sqlParams.ToArray()

                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_CompanyInfo_SearchByName", objParam, True)
                TotalRecords = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(4).Value)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class
End Namespace

