﻿'**********************************************************************************
' <CCollabaration.vb>
' 
' 	CHANGE CONTROL:
'	
'	AUTHOR: Prasanta 	DATE:20-JUl-20 	VERSION	CHANGES	KEYSTRING:
'**********************************************************************************

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Case
Imports System.Web.UI.WebControls
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.CCollabaration
    '**********************************************************************************
    ' Module Name  : None
    ' Module Type  : CAccount
    ' 
    ' Description  : This is the business logic classe for Accounts Module
    '**********************************************************************************

    Public Class CCollabaration
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _numTopicId As Long
        Private _intRecordType As Integer
        Private _numRecordId As Long
        Private _vcTopicTitle As String
        Private _numCreateBy As Long
        Private _dtmCreatedOn As DateTime
        Private _numUpdatedBy As Long
        Private _dtmUpdatedOn As DateTime
        Private _bitIsInternal As Boolean
        Private _numDomainId As Long
        Private _Status As String
        Private _numMessageId As Long
        Private _numParentMessageId As Long
        Private _vcMessage As String
        Private _numAttachmentId As Long
        Private _vcAttachmentName As String
        Private _vcAttachmentUrl As String
        Private _ClientMachineUTCTimeOffset As Int64
        Private _IsExternalUser As Boolean

        Public Property IsExternalUser() As Boolean
            Get
                Return _IsExternalUser
            End Get
            Set(value As Boolean)
                _IsExternalUser = value
            End Set
        End Property
        Public Property ClientMachineUTCTimeOffset() As Int64
            Get
                Return _ClientMachineUTCTimeOffset
            End Get
            Set(value As Int64)
                _ClientMachineUTCTimeOffset = value
            End Set
        End Property
        Public Property vcAttachmentUrl() As String
            Get
                Return _vcAttachmentUrl
            End Get
            Set(ByVal value As String)
                _vcAttachmentUrl = value
            End Set
        End Property

        Public Property vcAttachmentName() As String
            Get
                Return _vcAttachmentName
            End Get
            Set(ByVal value As String)
                _vcAttachmentName = value
            End Set
        End Property

        Public Property numAttachmentId() As Long
            Get
                Return _numAttachmentId
            End Get
            Set(ByVal value As Long)
                _numAttachmentId = value
            End Set
        End Property

        Public Property vcMessage() As String
            Get
                Return _vcMessage
            End Get
            Set(ByVal value As String)
                _vcMessage = value
            End Set
        End Property

        Public Property numParentMessageId() As Long
            Get
                Return _numParentMessageId
            End Get
            Set(ByVal value As Long)
                _numParentMessageId = value
            End Set
        End Property

        Public Property numMessageId() As Long
            Get
                Return _numMessageId
            End Get
            Set(ByVal value As Long)
                _numMessageId = value
            End Set
        End Property


        Public Property Status() As String
            Get
                Return _Status
            End Get
            Set(ByVal value As String)
                _Status = value
            End Set
        End Property

        Public Property numDomainId() As Long
            Get
                Return _numDomainId
            End Get
            Set(ByVal value As Long)
                _numDomainId = value
            End Set
        End Property
        Public Property bitIsInternal() As Boolean
            Get
                Return _bitIsInternal
            End Get
            Set(ByVal value As Boolean)
                _bitIsInternal = value
            End Set
        End Property
        Public Property dtmUpdatedOn() As DateTime
            Get
                Return _dtmUpdatedOn
            End Get
            Set(ByVal value As DateTime)
                _dtmUpdatedOn = value
            End Set
        End Property

        Public Property numUpdatedBy() As Long
            Get
                Return _numUpdatedBy
            End Get
            Set(ByVal value As Long)
                _numUpdatedBy = value
            End Set
        End Property
        Public Property dtmCreatedOn() As DateTime
            Get
                Return _dtmCreatedOn
            End Get
            Set(ByVal value As DateTime)
                _dtmCreatedOn = value
            End Set
        End Property

        Public Property numCreateBy() As Long
            Get
                Return _numCreateBy
            End Get
            Set(ByVal value As Long)
                _numCreateBy = value
            End Set
        End Property

        Public Property vcTopicTitle() As String
            Get
                Return _vcTopicTitle
            End Get
            Set(ByVal value As String)
                _vcTopicTitle = value
            End Set
        End Property
        Public Property numRecordId() As Long
            Get
                Return _numRecordId
            End Get
            Set(ByVal value As Long)
                _numRecordId = value
            End Set
        End Property
        Public Property intRecordType() As Integer
            Get
                Return _intRecordType
            End Get
            Set(ByVal value As Integer)
                _intRecordType = value
            End Set
        End Property
        Public Property numTopicId() As Long
            Get
                Return _numTopicId
            End Get
            Set(ByVal value As Long)
                _numTopicId = value
            End Set
        End Property

        Public Function ManageTopicMaster() As String
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numTopicId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = numTopicId

                arParms(1) = New Npgsql.NpgsqlParameter("@intRecordType", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = intRecordType

                arParms(2) = New Npgsql.NpgsqlParameter("@numRecordId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = numRecordId

                arParms(3) = New Npgsql.NpgsqlParameter("@vcTopicTitle", NpgsqlTypes.NpgsqlDbType.VarChar, 50000)
                arParms(3).Value = vcTopicTitle

                arParms(4) = New Npgsql.NpgsqlParameter("@numCreatedBy", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = CreatedBy

                arParms(5) = New Npgsql.NpgsqlParameter("@numUpdatedBy", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = numUpdatedBy

                arParms(6) = New Npgsql.NpgsqlParameter("@bitIsInternal", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(6).Value = bitIsInternal

                arParms(7) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = numDomainId

                arParms(8) = New Npgsql.NpgsqlParameter("@status", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(8).Value = Status
                arParms(8).Direction = ParameterDirection.Output

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "usp_SaveTopicMaster", objParam, True)
                _numTopicId = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

                Return Status
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Sub DeleteTOPICMASTER()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numTopicId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = numTopicId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = numDomainId

                SqlDAL.ExecuteScalar(connString, "USP_DELETE_TOPICMASTER", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Function GetTopicMaster() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@intRecordType", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = intRecordType

                arParms(1) = New Npgsql.NpgsqlParameter("@numRecordId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = numRecordId

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = numDomainId

                arParms(3) = New Npgsql.NpgsqlParameter("@bitExternalUser", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Value = IsExternalUser

                arParms(4) = New Npgsql.NpgsqlParameter("@numTopicId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = numTopicId

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetTopic", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageMessageMaster() As String
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(10) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numMessageId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = numMessageId

                arParms(1) = New Npgsql.NpgsqlParameter("@numTopicId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = numTopicId

                arParms(2) = New Npgsql.NpgsqlParameter("@numParentMessageId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = numParentMessageId

                arParms(3) = New Npgsql.NpgsqlParameter("@intRecordType", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = intRecordType

                arParms(4) = New Npgsql.NpgsqlParameter("@numRecordId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = numRecordId

                arParms(5) = New Npgsql.NpgsqlParameter("@vcMessage", NpgsqlTypes.NpgsqlDbType.VarChar, 500000)
                arParms(5).Value = vcMessage

                arParms(6) = New Npgsql.NpgsqlParameter("@numCreatedBy", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = CreatedBy

                arParms(7) = New Npgsql.NpgsqlParameter("@numUpdatedBy", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = numUpdatedBy

                arParms(8) = New Npgsql.NpgsqlParameter("@bitIsInternal", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(8).Value = bitIsInternal

                arParms(9) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = numDomainId

                arParms(10) = New Npgsql.NpgsqlParameter("@status", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(10).Value = Status
                arParms(10).Direction = ParameterDirection.Output

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "usp_SaveMessageMaster", objParam, True)
                Status = Convert.ToString(DirectCast(objParam, Npgsql.NpgsqlParameter())(10).Value)
                _numMessageId = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

                Return Status
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Sub DeleteMessageMaster()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numMessageId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = numMessageId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = numDomainId

                SqlDAL.ExecuteScalar(connString, "USP_DELETE_MESSAGEMASTER", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function GetMessageMaster() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numTopicId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = numTopicId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = numDomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@numMessageId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = numMessageId

                arParms(3) = New Npgsql.NpgsqlParameter("@bitExternalUser", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Value = IsExternalUser

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetTopicMessage", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageTopicMessageAttachments() As String
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numAttachmentId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = numAttachmentId

                arParms(1) = New Npgsql.NpgsqlParameter("@numTopicId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = numTopicId

                arParms(2) = New Npgsql.NpgsqlParameter("@numMessageId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = numMessageId

                arParms(3) = New Npgsql.NpgsqlParameter("@vcAttachmentName", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = vcAttachmentName

                arParms(4) = New Npgsql.NpgsqlParameter("@vcAttachmentUrl", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(4).Value = vcAttachmentUrl

                arParms(5) = New Npgsql.NpgsqlParameter("@numCreatedBy", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = CreatedBy

                arParms(6) = New Npgsql.NpgsqlParameter("@numUpdatedBy", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = numUpdatedBy

                arParms(7) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = numDomainId

                arParms(8) = New Npgsql.NpgsqlParameter("@status", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(8).Value = Status
                arParms(8).Direction = ParameterDirection.Output

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "usp_SaveTopicMessageAttachments", objParam, True)
                Status = Convert.ToString(DirectCast(objParam, Npgsql.NpgsqlParameter())(8).Value)

                Return Status
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Sub DeleteTopicMessageAttachments()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numAttachmentId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = numAttachmentId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = numDomainId

                SqlDAL.ExecuteScalar(connString, "USP_DELETE_TOPICMESSAGEATTACHMENTS", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace
