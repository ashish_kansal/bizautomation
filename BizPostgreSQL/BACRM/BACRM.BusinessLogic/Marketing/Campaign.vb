'Created By Anoop Jayaraj
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Configuration
Imports MailBee
Imports MailBee.Mime
Imports MailBee.SmtpMail
Imports MailBee.DnsMX
Imports Amazon
Imports Amazon.SimpleEmail
Imports Amazon.SimpleEmail.Model

Namespace BACRM.BusinessLogic.Marketing
    Public Class Campaign
        Inherits BACRM.BusinessLogic.CBusinessBase


        '#Region "Constructor"
        '        Public Sub New(ByVal intUserId As Integer)
        '            MyBase.New(intUserId)
        '        End Sub

        '        Public Sub New()
        '            MyBase.New()
        '        End Sub
        '#End Region

        'Private UserCntID As Long = 0
        Private _CampaignID As Long = 0
        Private _DivisionID As Long = 0
        ''Private DomainId As Long = 0
        Private _CurrentPage As Integer
        Private _PageSize As Integer
        Private _TotalRecords As Integer
        Private _SortCharacter As Char
        Private _columnSortOrder As String
        Private _columnName As String
        Private _CampaignName As String
        Private _CampaignStatus As Long
        Private _CampaignType As Long
        Private _CampaignRegion As Long
        Private _CampaignCost As Decimal
        Private _NoSent As Long
        Private _LaunchDate As Date
        Private _EndDate As Date
        Private _SearchType As Integer
        Private _BroadCastName As String
        Private _BroadCastSubject As String
        Private _BroadCastMessage As String
        Private _BroadCastBizMessage As String
        Private _strBroadCastDtls As String
        Private _EmailTemplateID As Long
        Private _TotalRecipients As Long
        Private _TotalSucessfull As Long
        Private _byteMode As Short
        Private _broadcastID As Long
        Private _EmailHstrId As Long
        Private _ContactId As Long
        Private _ECampaignID As Long
        Private _ECampaignName As String
        Private _CampDesc As String
        Private _strECampDtls As String
        Private _ConEmailCampID As Long
        Private _StartDate As Date
        Private _Engaged As Boolean
        Private _bitMarketCampaign As Boolean
        Private _Broadcasted As Boolean
        Private _SearchId As Long
        Private _MonthlyAmt As Decimal
        Private _YearlyAmt As Decimal
        Private _ClientTimeZoneOffset As Int32
        Private _strBroadcastingLink As String
        Private _numLinkId As Long
        Private _numGroupId As Long
        Private _numCategoryId As Long
        Public Property numGroupId() As Long
            Get
                Return _numGroupId
            End Get
            Set(ByVal Value As Long)
                _numGroupId = Value
            End Set
        End Property
        Public Property numCategoryId() As Long
            Get
                Return _numCategoryId
            End Get
            Set(ByVal Value As Long)
                _numCategoryId = Value
            End Set
        End Property
        Public Property numLinkId() As Long
            Get
                Return _numLinkId
            End Get
            Set(ByVal Value As Long)
                _numLinkId = Value
            End Set
        End Property
        Public Property strBroadcastingLink() As String
            Get
                Return _strBroadcastingLink
            End Get
            Set(ByVal Value As String)
                _strBroadcastingLink = Value
            End Set
        End Property


        Public Property ClientTimeZoneOffset() As Int32
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal Value As Int32)
                _ClientTimeZoneOffset = Value
            End Set
        End Property

        Private _TimeZone As Double
        Public Property TimeZone() As Double
            Get
                Return _TimeZone
            End Get
            Set(ByVal value As Double)
                _TimeZone = value
            End Set
        End Property

        Private _TimeZoneIndex As Short
        Public Property TimeZoneIndex() As Short
            Get
                Return _TimeZoneIndex
            End Get
            Set(ByVal value As Short)
                _TimeZoneIndex = value
            End Set
        End Property

        Private _FromField As Short
        Public Property FromField() As Short
            Get
                Return _FromField
            End Get
            Set(ByVal value As Short)
                _FromField = value
            End Set
        End Property

        Private _EmailGroupID As Long
        Public Property EmailGroupID() As Long
            Get
                Return _EmailGroupID
            End Get
            Set(ByVal value As Long)
                _EmailGroupID = value
            End Set
        End Property

        Private _ECampaignAssigneeID As Long
        Public Property ECampaignAssigneeID() As Long
            Get
                Return _ECampaignAssigneeID
            End Get
            Set(ByVal value As Long)
                _ECampaignAssigneeID = value
            End Set
        End Property

        Private _AlertDTLID As Long
        Public Property AlertDTLID() As Long
            Get
                Return _AlertDTLID
            End Get
            Set(ByVal Value As Long)
                _AlertDTLID = Value
            End Set
        End Property

        Private _IsDuplicate As Boolean
        Public Property IsDuplicate() As Boolean
            Get
                Return _IsDuplicate
            End Get
            Set(ByVal value As Boolean)
                _IsDuplicate = value
            End Set
        End Property

        Public Property Engaged() As Boolean
            Get
                Return _Engaged
            End Get
            Set(ByVal Value As Boolean)
                _Engaged = Value
            End Set
        End Property


        Public Property StartDate() As Date
            Get
                Return _StartDate
            End Get
            Set(ByVal Value As Date)
                _StartDate = Value
            End Set
        End Property


        Public Property ConEmailCampID() As Long
            Get
                Return _ConEmailCampID
            End Get
            Set(ByVal Value As Long)
                _ConEmailCampID = Value
            End Set
        End Property


        Public Property strECampDtls() As String
            Get
                Return _strECampDtls
            End Get
            Set(ByVal Value As String)
                _strECampDtls = Value
            End Set
        End Property


        Public Property CampDesc() As String
            Get
                Return _CampDesc
            End Get
            Set(ByVal Value As String)
                _CampDesc = Value
            End Set
        End Property


        Private _StartTime As Date
        Public Property StartTime() As Date
            Get
                Return _StartTime
            End Get
            Set(ByVal value As Date)
                _StartTime = value
            End Set
        End Property

        Public Property ECampaignName() As String
            Get
                Return _ECampaignName
            End Get
            Set(ByVal Value As String)
                _ECampaignName = Value
            End Set
        End Property


        Public Property ECampaignID() As Long
            Get
                Return _ECampaignID
            End Get
            Set(ByVal Value As Long)
                _ECampaignID = Value
            End Set
        End Property


        Public Property ContactId() As Long
            Get
                Return _ContactId
            End Get
            Set(ByVal Value As Long)
                _ContactId = Value
            End Set
        End Property


        Public Property EmailHstrId() As Long
            Get
                Return _EmailHstrId
            End Get
            Set(ByVal Value As Long)
                _EmailHstrId = Value
            End Set
        End Property


        Public Property broadcastID() As Long
            Get
                Return _broadcastID
            End Get
            Set(ByVal Value As Long)
                _broadcastID = Value
            End Set
        End Property


        Public Property byteMode() As Short
            Get
                Return _byteMode
            End Get
            Set(ByVal Value As Short)
                _byteMode = Value
            End Set
        End Property


        Public Property TotalSucessfull() As Long
            Get
                Return _TotalSucessfull
            End Get
            Set(ByVal Value As Long)
                _TotalSucessfull = Value
            End Set
        End Property


        Public Property TotalRecipients() As Long
            Get
                Return _TotalRecipients
            End Get
            Set(ByVal Value As Long)
                _TotalRecipients = Value
            End Set
        End Property


        Public Property EmailTemplateID() As Long
            Get
                Return _EmailTemplateID
            End Get
            Set(ByVal Value As Long)
                _EmailTemplateID = Value
            End Set
        End Property


        Public Property strBroadCastDtls() As String
            Get
                Return _strBroadCastDtls
            End Get
            Set(ByVal Value As String)
                _strBroadCastDtls = Value
            End Set
        End Property


        Private _bitUpdateFollowUpStatus As Boolean
        Public Property bitUpdateFollowUpStatus() As Boolean
            Get
                Return _bitUpdateFollowUpStatus
            End Get
            Set(ByVal Value As Boolean)
                _bitUpdateFollowUpStatus = Value
            End Set
        End Property


        Private _numFollowUpStatusId As Long
        Public Property numFollowUpStatusId() As Long
            Get
                Return _numFollowUpStatusId
            End Get
            Set(ByVal Value As Long)
                _numFollowUpStatusId = Value
            End Set
        End Property

        Public Property BroadCastMessage() As String
            Get
                Return _BroadCastMessage
            End Get
            Set(ByVal Value As String)
                _BroadCastMessage = Value
            End Set
        End Property
        Public Property BroadCastBizMessage() As String
            Get
                Return _BroadCastBizMessage
            End Get
            Set(ByVal Value As String)
                _BroadCastBizMessage = Value
            End Set
        End Property

        Public Property BroadCastSubject() As String
            Get
                Return _BroadCastSubject
            End Get
            Set(ByVal Value As String)
                _BroadCastSubject = Value
            End Set
        End Property


        Public Property BroadCastName() As String
            Get
                Return _BroadCastName
            End Get
            Set(ByVal Value As String)
                _BroadCastName = Value
            End Set
        End Property


        Public Property SearchType() As Integer
            Get
                Return _SearchType
            End Get
            Set(ByVal Value As Integer)
                _SearchType = Value
            End Set
        End Property


        Public Property EndDate() As Date
            Get
                Return _EndDate
            End Get
            Set(ByVal Value As Date)
                _EndDate = Value
            End Set
        End Property


        Public Property LaunchDate() As Date
            Get
                Return _LaunchDate
            End Get
            Set(ByVal Value As Date)
                _LaunchDate = Value
            End Set
        End Property


        Public Property NoSent() As Long
            Get
                Return _NoSent
            End Get
            Set(ByVal Value As Long)
                _NoSent = Value
            End Set
        End Property


        Public Property CampaignCost() As Decimal
            Get
                Return _CampaignCost
            End Get
            Set(ByVal Value As Decimal)
                _CampaignCost = Value
            End Set
        End Property


        Public Property CampaignRegion() As Long
            Get
                Return _CampaignRegion
            End Get
            Set(ByVal Value As Long)
                _CampaignRegion = Value
            End Set
        End Property


        Public Property CampaignType() As Long
            Get
                Return _CampaignType
            End Get
            Set(ByVal Value As Long)
                _CampaignType = Value
            End Set
        End Property


        Public Property CampaignStatus() As Long
            Get
                Return _CampaignStatus
            End Get
            Set(ByVal Value As Long)
                _CampaignStatus = Value
            End Set
        End Property


        Public Property CampaignName() As String
            Get
                Return _CampaignName
            End Get
            Set(ByVal Value As String)
                _CampaignName = Value
            End Set
        End Property


        Public Property columnName() As String
            Get
                Return _columnName
            End Get
            Set(ByVal Value As String)
                _columnName = Value
            End Set
        End Property

        Public Property columnSortOrder() As String
            Get
                Return _columnSortOrder
            End Get
            Set(ByVal Value As String)
                _columnSortOrder = Value
            End Set
        End Property

        Public Property SortCharacter() As Char
            Get
                Return _SortCharacter
            End Get
            Set(ByVal Value As Char)
                _SortCharacter = Value
            End Set
        End Property

        Public Property TotalRecords() As Integer
            Get
                Return _TotalRecords
            End Get
            Set(ByVal Value As Integer)
                _TotalRecords = Value
            End Set
        End Property

        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal Value As Integer)
                _PageSize = Value
            End Set
        End Property

        Public Property CurrentPage() As Integer
            Get
                Return _CurrentPage
            End Get
            Set(ByVal Value As Integer)
                _CurrentPage = Value
            End Set
        End Property
        Public Property CampaignID() As Long
            Get
                Return _CampaignID
            End Get
            Set(ByVal Value As Long)
                _CampaignID = Value
            End Set
        End Property
        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property

        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property


        'Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal Value As Long)
        '        UserCntID = Value
        '    End Set
        'End Property

        Public Property bitMarketCampaign() As Boolean
            Get
                Return _bitMarketCampaign
            End Get
            Set(ByVal Value As Boolean)
                _bitMarketCampaign = Value
            End Set
        End Property
        Public Property Broadcasted() As Boolean
            Get
                Return _Broadcasted
            End Get
            Set(ByVal Value As Boolean)
                _Broadcasted = Value
            End Set
        End Property
        Public Property SearchID() As Long
            Get
                Return _SearchId
            End Get
            Set(ByVal Value As Long)
                _SearchId = Value
            End Set
        End Property

        Public Property MonthlyAmt() As Decimal
            Get
                Return _MonthlyAmt
            End Get
            Set(ByVal Value As Decimal)
                _MonthlyAmt = Value
            End Set
        End Property


        Public Property YearlyAmt() As Decimal
            Get
                Return _YearlyAmt
            End Get
            Set(ByVal Value As Decimal)
                _YearlyAmt = Value
            End Set
        End Property

        Private _IsOnline As Boolean
        Public Property IsOnline() As Boolean
            Get
                Return _IsOnline
            End Get
            Set(ByVal value As Boolean)
                _IsOnline = value
            End Set
        End Property

        Private _CampaignCode As String
        Public Property CampaignCode() As String
            Get
                Return _CampaignCode
            End Get
            Set(ByVal value As String)
                _CampaignCode = value
            End Set
        End Property

        Private _CRMType As Short
        Public Property CRMType() As Short
            Get
                Return _CRMType
            End Get
            Set(ByVal value As Short)
                _CRMType = value
            End Set
        End Property

        Private _IsMonthly As Boolean
        Public Property IsMonthly() As Boolean
            Get
                Return _IsMonthly
            End Get
            Set(ByVal value As Boolean)
                _IsMonthly = value
            End Set
        End Property

        Private _ModuleID As Long
        Public Property ModuleID() As Long
            Get
                Return _ModuleID
            End Get
            Set(ByVal value As Long)
                _ModuleID = value
            End Set
        End Property

        Private _IsActive As Boolean
        Public Property IsActive() As Boolean
            Get
                Return _IsActive
            End Get
            Set(ByVal value As Boolean)
                _IsActive = value
            End Set
        End Property

        Private _ConfigurationId As Decimal
        Public Property ConfigurationId() As Decimal
            Get
                Return _ConfigurationId
            End Get
            Set(ByVal value As Decimal)
                _ConfigurationId = value
            End Set
        End Property
        Private _Mode As Integer
        Public Property Mode() As Integer
            Get
                Return _Mode
            End Get
            Set(ByVal value As Integer)
                _Mode = value
            End Set
        End Property
        Private _FromName As String
        Public Property FromName() As String
            Get
                Return _FromName
            End Get
            Set(ByVal value As String)
                _FromName = value
            End Set
        End Property


        Private _DomainName As String
        Public Property DomainName() As String
            Get
                Return _DomainName
            End Get
            Set(ByVal value As String)
                _DomainName = value
            End Set
        End Property


        Private _DkimPublicKey As String
        Public Property DkimPublicKey() As String
            Get
                Return _DkimPublicKey
            End Get
            Set(ByVal value As String)
                _DkimPublicKey = value
            End Set
        End Property


        Private _DkimPrivateKey As String
        Public Property DkimPrivateKey() As String
            Get
                Return _DkimPrivateKey
            End Get
            Set(ByVal value As String)
                _DkimPrivateKey = value
            End Set
        End Property


        Private _Enabled As Boolean
        Public Property Enabled() As Boolean
            Get
                Return _Enabled
            End Get
            Set(ByVal value As Boolean)
                _Enabled = value
            End Set
        End Property


        Private _SpfPass As Boolean
        Public Property SpfPass() As Boolean
            Get
                Return _SpfPass
            End Get
            Set(ByVal value As Boolean)
                _SpfPass = value
            End Set
        End Property


        Private _DkimPass As Boolean
        Public Property DkimPass() As Boolean
            Get
                Return _DkimPass
            End Get
            Set(ByVal value As Boolean)
                _DkimPass = value
            End Set
        End Property


        Private _DomainKeyPass As Boolean
        Public Property DomainKeyPass() As Boolean
            Get
                Return _DomainKeyPass
            End Get
            Set(ByVal value As Boolean)
                _DomainKeyPass = value
            End Set
        End Property


        Private _SenderIdPass As Boolean
        Public Property SenderIdPass() As Boolean
            Get
                Return _SenderIdPass
            End Get
            Set(ByVal value As Boolean)
                _SenderIdPass = value
            End Set
        End Property

        Private _LeadSourcesFromDate As DateTime
        Public Property LeadSourcesFromDate() As DateTime
            Get
                Return _LeadSourcesFromDate
            End Get
            Set(ByVal value As DateTime)
                _LeadSourcesFromDate = value
            End Set
        End Property

        Private _Screen As String
        Public Property screen() As String
            Get
                Return _Screen
            End Get
            Set(ByVal value As String)
                _Screen = value
            End Set
        End Property

        Public Property ReportFromDate As DateTime
        Public Property ReportToDate As DateTime
        Public Property ReportView As Short

#Region "Email Broadcasting"


        Public Function ManageBroadcastingLink() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@strBroadcastingLink", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(0).Value = _strBroadcastingLink

                arParms(1) = New Npgsql.NpgsqlParameter("@numLinkId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _numLinkId

                arParms(2) = New Npgsql.NpgsqlParameter("numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ContactId


                SqlDAL.ExecuteNonQuery(connString, "USP_AddBroadcastingLink", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetBroadcastingLink() As DataTable
            Try

                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numBroadcastId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _broadcastID

                arParms(1) = New Npgsql.NpgsqlParameter("@numLinkId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _numLinkId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetBroadcastingLink", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetEmailBroadcastConfiguration() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numConfigurationID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = ConfigurationId

                arParms(2) = New Npgsql.NpgsqlParameter("@intMode", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Value = Mode

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetEmailBroadcastConfiguration", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ManageEmailBroadcastConfiguration() As Boolean
            Try

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(10) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numConfigurationId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _ConfigurationId

                arParms(1) = New Npgsql.NpgsqlParameter("@vcFromName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(1).Value = _FromName

                arParms(2) = New Npgsql.NpgsqlParameter("@vcDomainName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(2).Value = _DomainName

                arParms(3) = New Npgsql.NpgsqlParameter("@vcDkimPublicKey", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(3).Value = _DkimPublicKey

                arParms(4) = New Npgsql.NpgsqlParameter("@vcDkimPrivateKey", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(4).Value = _DkimPrivateKey

                arParms(5) = New Npgsql.NpgsqlParameter("@bitEnabled", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(5).Value = _Enabled

                arParms(6) = New Npgsql.NpgsqlParameter("@bitSpfPass", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _SpfPass

                arParms(7) = New Npgsql.NpgsqlParameter("@bitDkimPass", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _DkimPass

                arParms(8) = New Npgsql.NpgsqlParameter("@bitDomainKeyPass", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(8).Value = _DomainKeyPass

                arParms(9) = New Npgsql.NpgsqlParameter("@bitSenderIdPass", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(9).Value = _SenderIdPass

                arParms(10) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(10).Value = DomainID

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageEmailBroadcastConfiguration", objParam, True)
                _ConfigurationId = CDec(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function DeleteEmailBroadcastConfiguration() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numConfigurationId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ConfigurationId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteEmailBroadcastConfiguration", arParms)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function CheckDomain(ByVal strDomainNameWithSelector As String) As String
            Try
                ' Get root TXT records (to filter SPF record from them).
                Smtp.LicenseKey = ConfigurationManager.AppSettings("SMTPLicense")
                Dim records As String() = Nothing
                Dim mailer As New Smtp()
                mailer.DnsServers.Autodetect()

                records = mailer.GetTxtData(strDomainNameWithSelector)
                Return ""
            Catch ex As Exception
                Return ex.Message.ToString()
            End Try
        End Function
        Public Function CheckSPF(ByVal strDomainName As String, ByVal strIP As String) As Boolean
            Try
                ' Get root TXT records (to filter SPF record from them).
                Smtp.LicenseKey = ConfigurationManager.AppSettings("SMTPLicense")
                Dim records As String() = Nothing
                Dim mailer As New Smtp()
                mailer.DnsServers.Autodetect()
                Try
                    records = mailer.GetTxtData(strDomainName)
                    If Not (records Is Nothing) Then
                        Dim checkVersion As Boolean = False
                        Dim checkIP As Boolean = False
                        Dim rec As String
                        For Each rec In records
                            Dim EachRec As String() = rec.Split(Convert.ToChar(" "))
                            Dim innerRecord As String
                            For Each innerRecord In EachRec
                                If innerRecord.ToLower().StartsWith("v=spf1") Then
                                    checkVersion = True
                                End If
                                If innerRecord.ToLower().StartsWith("ip4") Then
                                    If innerRecord.Contains(strIP) Then
                                        checkIP = True
                                    End If
                                End If

                            Next innerRecord
                        Next rec

                        If checkIP = True And checkVersion = True Then
                            Return True
                        Else
                            Return False
                        End If

                        Return False
                    Else
                        Return False
                    End If
                Catch e As MailBeeDnsNameErrorException
                    Return False
                Catch e As MailBeeDnsRecordsDisabledException
                    Return False
                End Try
            Catch ex As Exception
                Return False
            End Try
        End Function
        Public Function CheckDKIM(ByVal strDomainNameWithSelector As String, ByVal strPublicKey As String) As Boolean
            Try
                Smtp.LicenseKey = ConfigurationManager.AppSettings("SMTPLicense")
                Dim mailer As New Smtp()
                ' Get ready for making DNS queries.
                mailer.DnsServers.Autodetect()
                Dim records As String() = Nothing
                Try
                    records = mailer.GetTxtData(strDomainNameWithSelector)
                    If Not (records Is Nothing) Then
                        Dim checkVersion As Boolean = False
                        Dim checkPublicKey As Boolean = False
                        Dim rec As String
                        For Each rec In records
                            Dim EachRec As String() = rec.Split(Convert.ToChar(";"))
                            Dim innerRecord As String
                            For Each innerRecord In EachRec
                                If innerRecord.ToLower().StartsWith("k=rsa") Then
                                    checkVersion = True
                                End If
                                If innerRecord.ToLower().StartsWith("p=") Then
                                    If strPublicKey <> "" Then
                                        If innerRecord.Contains(strPublicKey) Then
                                            checkPublicKey = True
                                        End If
                                    End If
                                Else
                                    checkPublicKey = False
                                End If
                            Next innerRecord
                        Next rec
                        If checkPublicKey = True And checkVersion = True Then
                            Return True
                        Else
                            Return False
                        End If
                    End If
                Catch e As MailBeeDnsNameErrorException
                    Return False
                Catch e As MailBeeDnsRecordsDisabledException
                    Return False
                End Try
            Catch ex As Exception
                Return False
            End Try
        End Function
        Public Function CheckDomainKey(ByVal strDomainNameWithDomainKey As String) As Boolean
            Try
                Smtp.LicenseKey = ConfigurationManager.AppSettings("SMTPLicense")
                Dim mailer As New Smtp()
                ' Get ready for making DNS queries.
                mailer.DnsServers.Autodetect()
                Dim records As String() = Nothing
                Try
                    records = mailer.GetTxtData("_domainkey.bizautomationmail.com") 'strDomainNameWithDomainKey)
                    If Not (records Is Nothing) Then

                        Dim check As Boolean = False
                        Dim rec As String
                        For Each rec In records
                            Dim EachRec As String() = rec.Split(Convert.ToChar(";"))
                            Dim innerRecord As String
                            For Each innerRecord In EachRec
                                If innerRecord.ToLower().StartsWith("o=-") Or innerRecord.ToLower().StartsWith("t=y") Then
                                    check = True
                                End If
                            Next innerRecord
                        Next rec
                        If check = True Then
                            Return True
                        Else
                            Return False
                        End If
                    End If
                Catch e As MailBeeDnsNameErrorException
                    Return False
                Catch e As MailBeeDnsRecordsDisabledException
                    Return False
                End Try
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Function GetBroadcastConfiguration() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_EmailBroadcastConfiguration_GetByDomain", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function GetBroadCastEmailDetail() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numBroadCastID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = broadcastID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_BroadCastDtls_GetAllByBroadcastID", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function ValidateBroadcastConfiguration(ByVal AWSDomain As String, ByVal AWSAccessKey As String, ByVal AWSSecretKey As String) As Boolean
            Dim isValid As Boolean = False
            Dim isValidIdentity As Boolean = False
            Dim isValidDkim As Boolean = False
            Dim isDkimEnabled As Boolean = False

            Try
                If Not String.IsNullOrEmpty(AWSDomain) AndAlso Not String.IsNullOrEmpty(AWSAccessKey) AndAlso Not String.IsNullOrEmpty(AWSSecretKey) Then
                    Dim region As RegionEndpoint = RegionEndpoint.USWest2
                    Dim client As New AmazonSimpleEmailServiceClient(AWSAccessKey, AWSSecretKey, region)

                    'Check if configured domain for AMAZON SES is exists and verified in account: Marketing -> e-Mail Broadcast -> configuration
                    Dim requestIdentity As New GetIdentityVerificationAttributesRequest()
                    requestIdentity.Identities.Add(AWSDomain)

                    Dim responseIdentity As GetIdentityVerificationAttributesResponse = client.GetIdentityVerificationAttributes(requestIdentity)


                    If Not responseIdentity Is Nothing AndAlso responseIdentity.VerificationAttributes.Count > 0 AndAlso responseIdentity.VerificationAttributes.ContainsKey(AWSDomain) Then
                        Dim identityAttriubute As IdentityVerificationAttributes = responseIdentity.VerificationAttributes(AWSDomain)

                        If identityAttriubute.VerificationStatus = "Success" Then
                            isValidIdentity = True
                        Else
                            isValidIdentity = False
                        End If

                        'Check if DKIM is successfully verified for AMAZON SES account: Marketing -> e-Mail Broadcast -> configuration
                        Dim requestDkimIdentity As GetIdentityDkimAttributesRequest = New GetIdentityDkimAttributesRequest()
                        requestDkimIdentity.Identities.Add(AWSDomain)

                        Dim responseDkimIdentity As GetIdentityDkimAttributesResponse = client.GetIdentityDkimAttributes(requestDkimIdentity)
                        If responseDkimIdentity Is Nothing Or responseDkimIdentity.DkimAttributes.Count = 0 Or Not responseDkimIdentity.DkimAttributes.ContainsKey(AWSDomain) Or responseDkimIdentity.DkimAttributes(AWSDomain).DkimVerificationStatus <> "Success" Then
                            isValidDkim = False
                        Else
                            isValidDkim = True

                            If responseDkimIdentity.DkimAttributes(AWSDomain).DkimEnabled Then
                                isDkimEnabled = True
                            End If
                        End If

                        If isValidIdentity AndAlso isValidDkim AndAlso isDkimEnabled Then
                            isValid = True
                        Else
                            Throw New Exception("EMAIL_BROADCAST_CONFIGURATION_INVALID")
                        End If
                    Else
                        Throw New Exception("EMAIL_BROADCAST_CONFIGURATION_INVALID")
                    End If
                Else
                    Throw New Exception("EMAIL_BROADCAST_NOT_CONFIGURED")
                End If

                isValid = True
            Catch ex As Exception
                Throw
            End Try

            Return isValid
        End Function
        Public Sub SaveBroadcastConfiguration(ByVal AWSAccessKey As String, ByVal AWSSecretKey As String, ByVal AWSDomain As String, ByVal FromEmail As String)
            Try
                Try
                    Dim getconnection As New GetConnection
                    Dim connString As String = GetConnection.GetConnectionString
                    Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                    arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                    arParms(0).Value = DomainID

                    arParms(1) = New Npgsql.NpgsqlParameter("@vcAWSAccessKey", NpgsqlTypes.NpgsqlDbType.VarChar)
                    arParms(1).Value = AWSAccessKey

                    arParms(2) = New Npgsql.NpgsqlParameter("@vcAWSSecretKey", NpgsqlTypes.NpgsqlDbType.VarChar)
                    arParms(2).Value = AWSSecretKey

                    arParms(3) = New Npgsql.NpgsqlParameter("@vcAWSDomain", NpgsqlTypes.NpgsqlDbType.VarChar)
                    arParms(3).Value = AWSDomain

                    arParms(4) = New Npgsql.NpgsqlParameter("@vcFrom", NpgsqlTypes.NpgsqlDbType.VarChar)
                    arParms(4).Value = FromEmail

                    SqlDAL.ExecuteNonQuery(connString, "USP_EmailBroadcastConfiguration_Save", arParms)
                Catch ex As Exception
                    Throw
                End Try
            Catch ex As Exception
                Throw
            End Try
        End Sub
        Public Sub UpdateEmailSendStatus(ByVal configurationDtlID As Long)
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numBroadCastDtlID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = configurationDtlID

                SqlDAL.ExecuteNonQuery(connString, "USP_BroadCastDtls_UpdateSentStatus", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub
        Public Sub SaveEmailExceptionDetail(ByVal configurationDtlID As Long, ByVal exType As String, exMessage As String, exStackStrace As String)
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numBroadCastDtlID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = configurationDtlID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcType", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(1).Value = exType

                arParms(2) = New Npgsql.NpgsqlParameter("@vcMessage", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = exMessage

                arParms(3) = New Npgsql.NpgsqlParameter("@vcStackStrace", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = exStackStrace

                SqlDAL.ExecuteNonQuery(connString, "USP_BroadcastErrorLog_Insert", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub

#End Region

        Public Function CampaignManage() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(15) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numCampaignId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _CampaignID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcCampaignName", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(1).Value = _CampaignName

                arParms(2) = New Npgsql.NpgsqlParameter("@numCampaignStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _CampaignStatus

                arParms(3) = New Npgsql.NpgsqlParameter("@numCampaignType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _CampaignType

                arParms(4) = New Npgsql.NpgsqlParameter("@intLaunchDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(4).Value = _LaunchDate

                arParms(5) = New Npgsql.NpgsqlParameter("@intEndDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(5).Value = _EndDate

                arParms(6) = New Npgsql.NpgsqlParameter("@numRegion", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _CampaignRegion

                arParms(7) = New Npgsql.NpgsqlParameter("@numNoSent", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _NoSent

                arParms(8) = New Npgsql.NpgsqlParameter("@monCampaignCost", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(8).Value = _CampaignCost

                arParms(9) = New Npgsql.NpgsqlParameter("@bitIsOnline", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(9).Value = _IsOnline

                arParms(10) = New Npgsql.NpgsqlParameter("@bitIsMonthly", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(10).Value = _IsMonthly

                arParms(11) = New Npgsql.NpgsqlParameter("@vcCampaignCode", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(11).Value = _CampaignCode

                arParms(12) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(12).Value = DomainID

                arParms(13) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(13).Value = UserCntID

                arParms(14) = New Npgsql.NpgsqlParameter("@IsDuplicate", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(14).Direction = ParameterDirection.InputOutput
                arParms(14).Value = _IsDuplicate

                arParms(15) = New Npgsql.NpgsqlParameter("@bitActive", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(15).Value = _IsActive

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_CampaignManage", objParam, True)
                _CampaignID = CLng(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)
                _IsDuplicate = CBool(DirectCast(objParam, Npgsql.NpgsqlParameter())(14).Value)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function CampaignPerformanceReport() As DataSet
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintCRMtype", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _CRMType

                arParms(2) = New Npgsql.NpgsqlParameter("@dtStartDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _StartDate

                arParms(3) = New Npgsql.NpgsqlParameter("@dtEndDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(3).Value = _EndDate

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetCampaignPerformanceReport", arParms)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function CampaignDetails() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numCampaignId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CampaignID

                arParms(1) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _ClientTimeZoneOffset

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_CampaignDetails", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function RepCampaignDetails() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numCampaignId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CampaignID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_RepCampaignDetails", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function CampaignList() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(10) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numRegion", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _CampaignRegion

                arParms(3) = New Npgsql.NpgsqlParameter("@numCampaignType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _CampaignType

                arParms(4) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(4).Value = _SortCharacter

                arParms(5) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _CurrentPage

                arParms(6) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = _PageSize

                arParms(7) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(7).Direction = ParameterDirection.InputOutput
                arParms(7).Value = _TotalRecords

                arParms(8) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(8).Value = _columnName

                arParms(9) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(9).Value = _columnSortOrder

                arParms(10) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(10).Value = Nothing
                arParms(10).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_CampaignList", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(7).Value)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function CampaignCompList() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numCampaignid", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CampaignID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintComptype", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _SearchType

                arParms(2) = New Npgsql.NpgsqlParameter("@NoofRecds", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Direction = ParameterDirection.InputOutput
                arParms(2).Value = _TotalRecords

                arParms(3) = New Npgsql.NpgsqlParameter("@dtStartDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(3).Value = _StartDate

                arParms(4) = New Npgsql.NpgsqlParameter("@dtEndDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(4).Value = _EndDate

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_CampaignCompList", arParms)

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    _TotalRecords = ds.Tables(0).Rows.Count
                Else
                    _TotalRecords = 0
                End If

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetCostDetailsForCampaign() As DataTable
            Try
                Dim ds As DataSet
                Dim strSP As String = "Usp_GetCostDetailsForCampaign"
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numCampaignID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CampaignID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, strSP, arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try

        End Function

        Public Function CampaignOppList() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numCampaignid", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CampaignID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintComptype", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _SearchType

                arParms(2) = New Npgsql.NpgsqlParameter("@NoofRecds", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Direction = ParameterDirection.InputOutput
                arParms(2).Value = _TotalRecords

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_CampaignOppList", arParms)

                If Not ds Is Nothing AndAlso ds.Tables.Count() > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    _TotalRecords = ds.Tables(0).Rows.Count
                Else
                    _TotalRecords = 0
                End If

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function ManageBroadCast() As Long
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(14) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numBroadcastId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _broadcastID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcBroadCastName", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(1).Value = _BroadCastName

                arParms(2) = New Npgsql.NpgsqlParameter("@numEmailTemplateID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _EmailTemplateID

                arParms(3) = New Npgsql.NpgsqlParameter("@vcSubject", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(3).Value = _BroadCastSubject

                arParms(4) = New Npgsql.NpgsqlParameter("@txtMessage", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(4).Value = _BroadCastMessage

                arParms(5) = New Npgsql.NpgsqlParameter("@txtBizMessage", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(5).Value = _BroadCastBizMessage

                arParms(6) = New Npgsql.NpgsqlParameter("@numTotalRecipients", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _TotalRecipients

                arParms(7) = New Npgsql.NpgsqlParameter("@numTotalSussessfull", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _TotalSucessfull

                arParms(8) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = UserCntID

                arParms(9) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = DomainID

                arParms(10) = New Npgsql.NpgsqlParameter("@strBroadCast", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(10).Value = _strBroadCastDtls

                arParms(11) = New Npgsql.NpgsqlParameter("@bitBroadcasted", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(11).Value = _Broadcasted

                arParms(12) = New Npgsql.NpgsqlParameter("@numSearchID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(12).Value = _SearchId

                arParms(13) = New Npgsql.NpgsqlParameter("@numConfigurationId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(13).Value = _ConfigurationId

                arParms(14) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(14).Value = Nothing
                arParms(14).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_ManageBroadcast", arParms))
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetBroadCastDetails() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numBroadCastId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _broadcastID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetBroadcastDetails", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function GetBroadCastDataByID() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numBroadCastId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _broadcastID

                arParms(2) = New Npgsql.NpgsqlParameter("@numSearchID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _SearchId

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetBroadcastDataByID", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function ManageBroadCastDTLs() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numBroID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _broadcastID

                arParms(1) = New Npgsql.NpgsqlParameter("@strBroadCast", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = IIf(_strBroadCastDtls = "", "", Replace(_strBroadCastDtls, "'", "''"))

                arParms(2) = New Npgsql.NpgsqlParameter("@numTotalSussessfull", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _TotalSucessfull


                SqlDAL.ExecuteNonQuery(connString, "USP_ManageBroadcastDTLs", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetBroadcastList() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _CurrentPage

                arParms(3) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _PageSize

                arParms(4) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Direction = ParameterDirection.InputOutput
                arParms(4).Value = _TotalRecords

                arParms(5) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(5).Value = _columnName

                arParms(6) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(6).Value = _columnSortOrder

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_BroadCastList", arParms)
                _TotalRecords = arParms(4).Value
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetBroadcastDTLs() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numBroadCastID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _broadcastID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetBroadcastDTLs", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetEmailTemplates() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim inputParm() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                inputParm(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                inputParm(0).Value = DomainID

                inputParm(1) = New Npgsql.NpgsqlParameter("@numModuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                inputParm(1).Value = _ModuleID

                inputParm(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                inputParm(2).Value = Nothing
                inputParm(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetEmailtemplate", inputParm)
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function



        Public Function GetUserEmailTemplates() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim inputParm() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}


                inputParm(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                inputParm(0).Value = UserCntID

                inputParm(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                inputParm(1).Value = DomainID

                inputParm(2) = New Npgsql.NpgsqlParameter("@isMarketing", NpgsqlTypes.NpgsqlDbType.Bit)
                inputParm(2).Value = _bitMarketCampaign

                inputParm(3) = New Npgsql.NpgsqlParameter("@numGroupId", NpgsqlTypes.NpgsqlDbType.BigInt)
                inputParm(3).Value = numGroupId

                inputParm(4) = New Npgsql.NpgsqlParameter("@numCategoryId", NpgsqlTypes.NpgsqlDbType.BigInt)
                inputParm(4).Value = numCategoryId

                inputParm(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                inputParm(5).Value = Nothing
                inputParm(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetUserEmailtemplate", inputParm)
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetEmailTemplatesByMarketingDept(ByVal currentUser As Int32, ByVal currentDomainID As Int32, ByVal isForMarketingDept As Int32, ByVal numCategoryId As Int32, ByVal numGroupId As Int32) As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim inputParm() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                inputParm(0) = New Npgsql.NpgsqlParameter("@currentUserID", NpgsqlTypes.NpgsqlDbType.Integer)
                inputParm(0).Value = currentUser

                inputParm(1) = New Npgsql.NpgsqlParameter("@currentDomainID", NpgsqlTypes.NpgsqlDbType.Integer)
                inputParm(1).Value = currentDomainID

                inputParm(2) = New Npgsql.NpgsqlParameter("@isForMarketingDept", NpgsqlTypes.NpgsqlDbType.Integer)
                inputParm(2).Value = isForMarketingDept

                inputParm(3) = New Npgsql.NpgsqlParameter("@numGroupId", NpgsqlTypes.NpgsqlDbType.Integer)
                inputParm(3).Value = numGroupId

                inputParm(4) = New Npgsql.NpgsqlParameter("@numCategoryId", NpgsqlTypes.NpgsqlDbType.Integer)
                inputParm(4).Value = numCategoryId

                inputParm(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                inputParm(5).Value = Nothing
                inputParm(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "Get_Email_Template_By_Marketing_Dept", inputParm)
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function CampaignOverlapChecking() As Integer
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@intLaunchDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _LaunchDate

                arParms(2) = New Npgsql.NpgsqlParameter("@intEndDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _EndDate

                arParms(3) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _byteMode

                arParms(4) = New Npgsql.NpgsqlParameter("@numCampaignID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _CampaignID

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Return CInt(SqlDAL.ExecuteScalar(connString, "usp_CampaignOverlapChecking", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function UpdateFollowUpDates() As Integer
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@strContactIds", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(1).Value = strBroadCastDtls

                arParms(2) = New Npgsql.NpgsqlParameter("@bitUpdateFollowUpStatus", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(2).Value = bitUpdateFollowUpStatus

                arParms(3) = New Npgsql.NpgsqlParameter("@numFollowUpStatusId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = numFollowUpStatusId

                Return CInt(SqlDAL.ExecuteScalar(connString, "usp_UpdateFollowUpDatesByContactId", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteCampaign() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numCampaignId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CampaignID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteCampaign", arParms)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function DeleteFromCampaign() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ContactId

                arParms(1) = New Npgsql.NpgsqlParameter("@numECampaignID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ECampaignID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteFromCampaign", arParms)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function UpdateEmailHstrTracking() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numEmailHstrID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _EmailHstrId


                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateEmailHstrForETrack", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function UpdateBroadHstrTracking() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numBroadHstrID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _broadcastID

                arParms(1) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ContactId


                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateBroadHstrForETrack", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function ManageEcampaign() As Long
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(11) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numECampaignID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ECampaignID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcCampaignName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(1).Value = _ECampaignName

                arParms(2) = New Npgsql.NpgsqlParameter("@txtDesc", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(2).Value = _CampDesc

                arParms(3) = New Npgsql.NpgsqlParameter("@strECampaignDTLs", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(3).Value = _strECampDtls

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = DomainID

                arParms(5) = New Npgsql.NpgsqlParameter("@dtStartTime", NpgsqlTypes.NpgsqlDbType.Date)
                arParms(5).Value = _StartTime

                arParms(6) = New Npgsql.NpgsqlParameter("@intTimeZone", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(6).Value = _TimeZone

                arParms(7) = New Npgsql.NpgsqlParameter("@tintTimeZoneIndex", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(7).Value = _TimeZoneIndex

                arParms(8) = New Npgsql.NpgsqlParameter("@tintFromField", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(8).Value = _FromField

                arParms(9) = New Npgsql.NpgsqlParameter("@numFromContactID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(9).Value = _ContactId

                arParms(10) = New Npgsql.NpgsqlParameter("@numUserContactID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(10).Value = UserCntID

                arParms(11) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(11).Value = Nothing
                arParms(11).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_ManageECampaign", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ECampaignDtls() As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numECampaignID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _ECampaignID

                arParms(1) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _byteMode

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_EcampaginDTls", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function ECampaignList() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(2).Value = _SortCharacter

                arParms(3) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _CurrentPage

                arParms(4) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _PageSize

                arParms(5) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Direction = ParameterDirection.InputOutput
                arParms(5).Value = _TotalRecords

                arParms(6) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.Varchar, 50)
                arParms(6).Value = _columnName

                arParms(7) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.Varchar, 10)
                arParms(7).Value = _columnSortOrder

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_ECampaignList", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(5).Value)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function ConECampaignDtls() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _ContactId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_ConECampaign", arParms).Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function ECampaignTemplates() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_ECampaignTemplates", arParms).Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function ECampaignReport() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _CurrentPage

                arParms(2) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _PageSize

                arParms(3) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Direction = ParameterDirection.Output

                arParms(4) = New Npgsql.NpgsqlParameter("@numECampaingID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(4).Value = _CampaignID

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetECampaignDetailsReport", arParms)
                _TotalRecords = CInt(ds.Tables(1).Rows(0).Item(0).ToString())
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function ManageConECamp() As Long
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numConEmailCampID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _ConEmailCampID

                arParms(1) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _ContactId

                arParms(2) = New Npgsql.NpgsqlParameter("@numECampaignID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Value = _ECampaignID

                arParms(3) = New Npgsql.NpgsqlParameter("@intStartDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(3).Value = _StartDate

                arParms(4) = New Npgsql.NpgsqlParameter("@bitEngaged", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(4).Value = _Engaged

                arParms(5) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(5).Value = UserCntID

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_ManageConEmailCampaign", arParms))

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function ConECampaignHstr() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numConatctID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _ContactId

                arParms(1) = New Npgsql.NpgsqlParameter("@numConEmailCampID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _ConEmailCampID

                arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _ClientTimeZoneOffset

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_ECampaignHstr", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetMarketBudgetCampaign() As Long
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numCampaignId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _CampaignID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numListItemID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Value = _CampaignType

                arParms(3) = New Npgsql.NpgsqlParameter("@bitMarketCampaign", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Direction = ParameterDirection.InputOutput
                arParms(3).Value = _bitMarketCampaign

                arParms(4) = New Npgsql.NpgsqlParameter("@monmonthlyAmt", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(4).Direction = ParameterDirection.InputOutput
                arParms(4).Value = _MonthlyAmt

                arParms(5) = New Npgsql.NpgsqlParameter("@monTotalYearlyAmt", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(5).Direction = ParameterDirection.InputOutput
                arParms(5).Value = _YearlyAmt

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_MarketBudgetForCampaign", objParam, True)
                _bitMarketCampaign = CBool(DirectCast(objParam, Npgsql.NpgsqlParameter())(3).Value)
                _MonthlyAmt = CDec(DirectCast(objParam, Npgsql.NpgsqlParameter())(4).Value)
                _YearlyAmt = CDec(DirectCast(objParam, Npgsql.NpgsqlParameter())(5).Value)

            Catch ex As Exception

            End Try
        End Function



        Public Function GetECampaigns() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GET_ECampaign", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ManageEcampaignAssignee() As Long
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numECampaignAssigneeID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ECampaignAssigneeID

                arParms(1) = New Npgsql.NpgsqlParameter("@numECampaignID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ECampaignID

                arParms(2) = New Npgsql.NpgsqlParameter("@numEmailGroupID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _EmailGroupID

                arParms(3) = New Npgsql.NpgsqlParameter("@dtStartDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(3).Value = IIf(_StartDate = Nothing, DBNull.Value, _StartDate)

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = DomainID

                arParms(5) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = UserCntID

                arParms(6) = New Npgsql.NpgsqlParameter("@bitMode", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(6).Value = _byteMode

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_ManageECampaignAssignee", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetECampaignAssignee() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numECampaignAssigneeID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ECampaignAssigneeID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetECampaignAssignee", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'returns Score in %(percentage)  0% corresponds to absolutely non-spam message, 100% corresponds to absolutely spam message.
        Public Function GetSpamScore(ByVal Subject As String, ByVal Body As String) As Integer
            Try
                MailBee.AntiSpam.BayesFilter.LicenseKey = ConfigurationManager.AppSettings("AntiSpamLicense")
                Dim filter As MailBee.AntiSpam.BayesFilter = New MailBee.AntiSpam.BayesFilter()
                Dim msg As New MailBee.Mime.MailMessage
                Dim intScore As Integer
                ' Test our emails for spam.

                msg.Subject = Subject
                msg.BodyHtmlText = Body

                filter.AutoLearning = True
                filter.LoadDatabase("C:\AntiSpam\spam.dat", "C:\AntiSpam\nonspam.dat")
                intScore = filter.ScoreMessage(msg)
                filter.SaveDatabase("C:\AntiSpam\spam.dat", "C:\AntiSpam\nonspam.dat")
                Return intScore
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function DeleteBroadcastHistory() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numBroadCastId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _broadcastID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteBroadcastHistory", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Sub SetRadEditorPath(ByVal RadEditor As Telerik.Web.UI.RadEditor, ByVal lngDomainID As Long, ByVal page As System.Web.UI.Page)
            Try
                If lngDomainID > 0 Then
                    'apply path for image manager
                    Dim strPath As String = "~/" & ConfigurationManager.AppSettings("PortalDocumentsVirtualDirectory") & "/" & lngDomainID.ToString
                    RadEditor.ImageManager.ViewPaths = New String() {strPath}
                    RadEditor.ImageManager.UploadPaths = New String() {strPath}
                    RadEditor.ImageManager.DeletePaths = New String() {strPath}
                    'Template Manager
                    strPath = "~/" & ConfigurationManager.AppSettings("EMailTemplatesVirtualDirectory")
                    RadEditor.TemplateManager.ViewPaths = New String() {strPath}
                    page.ClientScript.RegisterHiddenField("hdnViewPath", ConfigurationManager.AppSettings("PortalDocumentsVirtualDirectory"))
                    page.ClientScript.RegisterHiddenField("hdnStaticFileHostedOn", ConfigurationManager.AppSettings("StaticFileHostedOn"))


                    Dim sb As New System.Text.StringBuilder
                    sb.AppendLine(" //www.telerik.com/help/aspnet-ajax/onclientpastehtml.html")
                    sb.AppendLine("function OnClientPasteHtml(sender, args) {")
                    sb.AppendLine("var commandName = args.get_commandName();")
                    sb.AppendLine("var value = args.get_value();")
                    sb.AppendLine("if (commandName == ""ImageManager"") {")
                    sb.AppendLine("//See if an img has an alt tag set)")
                    sb.AppendLine("var div = document.createElement(""DIV"");")
                    sb.AppendLine("//Do not use div.innerHTML as in IE this would cause the image's src or the link's href to be converted to absolute path.")
                    sb.AppendLine("//This is a severe IE quirk.")
                    sb.AppendLine("Telerik.Web.UI.Editor.Utils.setElementInnerHtml(div, value);")
                    sb.AppendLine("//Now check if there is alt attribute")
                    sb.AppendLine("var img = div.firstChild;")
                    sb.AppendLine("if (img.src) {")
                    sb.AppendLine("var viewPath = document.getElementById('hdnViewPath').value;")
                    sb.AppendLine("var ImagePath = img.src;")
                    sb.AppendLine("var PathToAppend = ImagePath.substring(ImagePath.indexOf(viewPath) + viewPath.length);")
                    sb.AppendLine("img.setAttribute(""src"", document.getElementById('hdnStaticFileHostedOn').value + PathToAppend);")
                    sb.AppendLine("//Set new content to be pasted into the editor")
                    sb.AppendLine("args.set_value(div.innerHTML);")
                    sb.AppendLine("}")
                    sb.AppendLine("}")
                    sb.AppendLine("}")

                    page.ClientScript.RegisterClientScriptBlock(page.GetType, "ImageManagerScript", sb.ToString, True)
                    'Dim hdnViewPath As New System.Web.UI.WebControls.HiddenField()
                    'hdnViewPath.ID = "hdnViewPath"
                    'hdnViewPath.ClientIDMode = Web.UI.ClientIDMode.Static
                    'hdnViewPath.Value = ConfigurationManager.AppSettings("PortalDocumentsVirtualDirectory") & "/" & lngDomainID.ToString
                    'page.Controls.Add(hdnViewPath)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function GetActualLeadSource() As DataTable
            Try

                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@FromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _LeadSourcesFromDate

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetActualLeadSource", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private _SourceDomain As String
        Public Property SourceDomain() As String
            Get
                Return _SourceDomain
            End Get
            Set(ByVal value As String)
                _SourceDomain = value
            End Set
        End Property

        Private _LeadsOrAccounts As Short
        Public Property LeadsOrAccounts() As Short
            Get
                Return _LeadsOrAccounts
            End Get
            Set(ByVal value As Short)
                _LeadsOrAccounts = value
            End Set
        End Property

        Public Function GetListWebSourceLeads() As DataTable
            Try

                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SourceDomain", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(1).Value = _SourceDomain

                arParms(2) = New Npgsql.NpgsqlParameter("@FromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _LeadSourcesFromDate

                arParms(3) = New Npgsql.NpgsqlParameter("@LeadsOrAccounts", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _LeadsOrAccounts

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetListWebSourceLeads", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Sub UpdateCampaignTracking(ByVal numConECampDTLID As Long)
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numConECampDTLID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = numConECampDTLID

                SqlDAL.ExecuteNonQuery(connString, "USP_ConECampaignDTL_SetTrackingStatus", arParms)
            Catch ex As Exception

            End Try
        End Sub

        Public Function SaveConECampaignDTLLink() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numConECampDTLID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ConEmailCampID

                arParms(1) = New Npgsql.NpgsqlParameter("@strBroadcastingLink", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _strBroadcastingLink

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_ConECampaignDTLLinks_Save", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function UpdateConECampaignDTLLinkClicked() As String
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numLinkID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _numLinkId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteScalar(connString, "USP_ConECampaignDTLLinks_UpdateLinkClicked", arParms).ToString()
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function End_Disengage_SelectedCampaign() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ContactId

                arParms(1) = New Npgsql.NpgsqlParameter("@numECampaignID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ECampaignID

                SqlDAL.ExecuteNonQuery(connString, "USP_End(Disengage)SelectedCampaign", arParms)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        'Created by Priya 23 Feb 2018 (populating Email Templates depending on screen )  
        Public Function GetUserEmailTemplates_ComposeEmail() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim inputParm() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                inputParm(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                inputParm(0).Value = UserCntID

                inputParm(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                inputParm(1).Value = DomainID

                inputParm(2) = New Npgsql.NpgsqlParameter("@isMarketing", NpgsqlTypes.NpgsqlDbType.Bit)
                inputParm(2).Value = _bitMarketCampaign

                inputParm(3) = New Npgsql.NpgsqlParameter("@Screen", NpgsqlTypes.NpgsqlDbType.Bit)
                inputParm(3).Value = _Screen

                inputParm(4) = New Npgsql.NpgsqlParameter("@numGroupId", NpgsqlTypes.NpgsqlDbType.BigInt)
                inputParm(4).Value = _numGroupId

                inputParm(5) = New Npgsql.NpgsqlParameter("@numCategoryId", NpgsqlTypes.NpgsqlDbType.BigInt)
                inputParm(5).Value = _numCategoryId

                inputParm(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                inputParm(6).Value = Nothing
                inputParm(6).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetUserEmailTemplate_ComposeEmail", inputParm)
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetCampaignReport() As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@ClientTimezoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@numLeadSource", CampaignID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintReportView", ReportView, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@dtFromDate", ReportFromDate, NpgsqlTypes.NpgsqlDbType.Timestamp))
                    .Add(SqlDAL.Add_Parameter("@dtToDate", ReportToDate, NpgsqlTypes.NpgsqlDbType.Timestamp))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_GetCampaignReport", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetConversionPercentOrders(ByVal vcDivisionIds As String) As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@ClientTimezoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@vcDivisionIds", vcDivisionIds, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_GetConversionPercentOrders", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class
End Namespace

