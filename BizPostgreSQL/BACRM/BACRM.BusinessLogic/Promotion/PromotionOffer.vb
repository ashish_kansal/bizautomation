﻿'Created Anoop Jayaraj
Option Explicit On
Option Strict On
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.Promotion
    Public Class PromotionOffer
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"

        Public Property PromotionID As Long
        'Public Property PromotionOfferBasedID As Long
        Public Property PromotionName As String
        Public Property ValidFrom As Nullable(Of DateTime)
        Public Property ValidTo As Nullable(Of DateTime)
        Public Property IsNeverExpires As Boolean
        Public Property IsAppliesToInternalOrder As Boolean
        Public Property IsAppliesToSite As Boolean
        Public Property IsRequireCouponCode As Boolean
        Public Property CouponCode As String
        Public Property UsageLimit As Int32
        Public Property IsEnableFreeShipping As Boolean
        Public Property FreeShippingOrderAmount As Int32
        Public Property FreeShippingCountry As Long
        Public Property IsEnableFixShipping1 As Boolean
        Public Property FixShipping1OrderAmount As Int32
        Public Property FixShipping1Charge As Int32
        Public Property IsEnableFixShipping2 As Boolean
        Public Property FixShipping2OrderAmount As Int32
        Public Property FixShipping2Charge As Int32
        Public Property IsDisplayPostUpSell As Boolean
        Public Property PostUpSellDiscount As Int32
        Public Property PromotionOfferTriggerValueType As Short ' 1 = Quantity, 2 = Amount
        Public Property PromotionOfferTriggerValue As Double
        Public Property PromotionOfferBasedOn As Short ' 1 = Individual Item, 2 = Item Classification
        Public Property DiscountType As Short ' 1 = Percentage, 2 = Flat Amount, 3 = Quantity
        Public Property DiscountValue As Double
        Public Property DiscountBasedOn As Short ' 1 = Individual Item, 2 = Item Classification, 3 = Related Items
        Public Property SiteID As Long
        Public Property Mode As Short
        Public Property DivisionID As Long

        Public Property POType As Short
        Public Property RuleAppType As Short
        Public Property POValue As Long
        Public Property Profile As Long
        Public Property PODTLID As Long
        Public Property RecordType As Short

        'Added by Neelam Kapila || 10/27/2017 - Added property to save customer
        Public Property PromotionOfferBasedOnCustomer As Short ' 1 - Individual, 2 - Relationship & Profile, 3 - All Customerss
        Public Property Relationship As Long
        Public Property PromotionOrganisationID As Long
        Public Property ShippingPromotionID As Long
        'Added by Neelam Kapila || 11/09/2017 - Added property to save between range in promotions
        Public Property PromotionOfferTriggerValueTypeRange As Short ' 1 = minimum of, 2 = between
        Public Property PromotionOfferTriggerValueRange As Double

        Private _CookieId As String
        Public Property CookieId As String
            Get
                Return _CookieId
            End Get
            Set(ByVal value As String)
                _CookieId = value
            End Set
        End Property

        Private _SaleType As Char
        Public Property SaleType() As Char
            Get
                Return _SaleType
            End Get
            Set(ByVal value As Char)
                _SaleType = value
            End Set
        End Property

        Private _SubTotal As Decimal
        Public Property SubTotal() As Decimal
            Get
                Return _SubTotal
            End Get
            Set(ByVal value As Decimal)
                _SubTotal = value
            End Set
        End Property

        Private _OldSubTotal As Decimal
        Public Property OldSubTotal() As Decimal
            Get
                Return _OldSubTotal
            End Get
            Set(ByVal value As Decimal)
                _OldSubTotal = value
            End Set
        End Property

        Private _OrderAmount As String
        Public Property OrderAmount() As String
            Get
                Return _OrderAmount
            End Get
            Set(ByVal value As String)
                _OrderAmount = value
            End Set
        End Property

        Private _DiscountOnOrder As String
        Public Property DiscountOnOrder() As String
            Get
                Return _DiscountOnOrder
            End Get
            Set(ByVal value As String)
                _DiscountOnOrder = value
            End Set
        End Property

        Private _numItemCode As Long
        Public Property numItemCode() As Long
            Get
                Return _numItemCode
            End Get
            Set(ByVal value As Long)
                _numItemCode = value
            End Set
        End Property

        Private _numDiscountId As Long
        Public Property numDiscountId() As Long
            Get
                Return _numDiscountId
            End Get
            Set(ByVal value As Long)
                _numDiscountId = value
            End Set
        End Property

        Public Property ClientTimeZoneOffset() As Integer

        Private _ItemCalculateDiscountOn As Short
        Public Property ItemCalculateDiscountOn() As Short
            Get
                Return _ItemCalculateDiscountOn
            End Get
            Set(ByVal value As Short)
                _ItemCalculateDiscountOn = value
            End Set
        End Property

        Private _ShortDesc As String
        Public Property ShortDesc() As String
            Get
                Return _ShortDesc
            End Get
            Set(ByVal value As String)
                _ShortDesc = value
            End Set
        End Property

        Private _LongDesc As String
        Public Property LongDesc() As String
            Get
                Return _LongDesc
            End Get
            Set(ByVal value As String)
                _LongDesc = value
            End Set
        End Property

        Public Property IsUseForCouponManagement As Boolean
        Public Property IsUseOrderPromotion As Boolean
        Public Property OrderPromotionID As Long
        Public Property DiscountedItemPrice As Decimal
        Public Property CurrencyID As Long
#End Region

#Region "Constructor"

        Public Sub New()
            'Constructor
            MyBase.New()
        End Sub
#End Region

#Region "Public Methods"

        Public Function GetPromotionOffer() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = PromotionID

                arParms(1) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = Mode

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = ClientTimeZoneOffset

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetPromotionOffer", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DelPromotionOffer() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = PromotionID

                SqlDAL.ExecuteNonQuery(connString, "USP_DelPromotionOffer", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteOrderBasedPromotionRule() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@fltSubTotal", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(1).Value = SubTotal

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteOrderBasedPromotionRule", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ApplyPromotionOfferCoupon() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _SiteID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcCouponCode", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = CouponCode

                arParms(3) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = DivisionID

                ds = SqlDAL.ExecuteDataset(connString, "USP_ApplyPromotionOffer", arParms)

                Return ds
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetPromotionOfferSites() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numProID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = PromotionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_PromotionOfferSites_GetByPromotionID", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub SavePromotionOfferSites(ByVal vcSelectedSites As String)
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numProID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = PromotionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcSelectedSites", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = vcSelectedSites

                SqlDAL.ExecuteDataset(connString, "USP_PromotionOfferSites_Save", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetPromotionOfferDtl() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numProID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = PromotionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintRuleAppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = RuleAppType

                arParms(3) = New Npgsql.NpgsqlParameter("@numSiteId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = SiteID

                arParms(4) = New Npgsql.NpgsqlParameter("@tintRecordType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = RecordType

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetPromotionOfferDtl", arParms)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManagePromotionOfferDTL() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numProID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = PromotionID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintRuleAppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = RuleAppType

                arParms(2) = New Npgsql.NpgsqlParameter("@numValue", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = POValue

                arParms(3) = New Npgsql.NpgsqlParameter("@numProfile", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = Profile

                arParms(4) = New Npgsql.NpgsqlParameter("@numPODTLID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = PODTLID

                arParms(5) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = Mode

                arParms(6) = New Npgsql.NpgsqlParameter("@tintRecordType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(6).Value = RecordType

                SqlDAL.ExecuteScalar(connString, "USP_ManagePromotionOfferDTL", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetPromotionOfferItems(ByVal recordType As Short, ByVal itemSelectionType As Short) As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numProID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = PromotionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintRecordType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = recordType

                arParms(3) = New Npgsql.NpgsqlParameter("@tintItemSelectionType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = itemSelectionType

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_PromotionOffer_GetItemSelections", arParms)

                Return ds
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ManagePromotionOffer(ByVal vcSelectedSites As String, ByVal vcPromotionItems As String, ByVal vcDiscountItems As String) As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numProId", PromotionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcProName", PromotionName, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@dtValidFrom", ValidFrom, NpgsqlTypes.NpgsqlDbType.Date))
                    .Add(SqlDAL.Add_Parameter("@dtValidTo", ValidTo, NpgsqlTypes.NpgsqlDbType.Date))
                    .Add(SqlDAL.Add_Parameter("@IsNeverExpires", IsNeverExpires, NpgsqlTypes.NpgsqlDbType.Boolean))
                    .Add(SqlDAL.Add_Parameter("@bitUseOrderPromotion", IsUseOrderPromotion, NpgsqlTypes.NpgsqlDbType.Boolean))
                    .Add(SqlDAL.Add_Parameter("@numOrderPromotionID", OrderPromotionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintOfferTriggerValueType", PromotionOfferTriggerValueType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@fltOfferTriggerValue", PromotionOfferTriggerValue, NpgsqlTypes.NpgsqlDbType.Double))
                    .Add(SqlDAL.Add_Parameter("@tintOfferBasedOn", PromotionOfferBasedOn, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@tintDiscountType", DiscountType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@fltDiscountValue", DiscountValue, NpgsqlTypes.NpgsqlDbType.Double))
                    .Add(SqlDAL.Add_Parameter("@tintDiscoutBaseOn", DiscountBasedOn, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintCustomersBasedOn", PromotionOfferBasedOnCustomer, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@tintOfferTriggerValueTypeRange", PromotionOfferTriggerValueTypeRange, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@fltOfferTriggerValueRange", PromotionOfferTriggerValueRange, NpgsqlTypes.NpgsqlDbType.Double))
                    .Add(SqlDAL.Add_Parameter("@vcShortDesc", ShortDesc, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcLongDesc", LongDesc, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@tintItemCalDiscount", ItemCalculateDiscountOn, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@monDiscountedItemPrice", DiscountedItemPrice, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_ManagePromotionOffer", sqlParams.ToArray()))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Added by Neelam for Order Based Promotion Change on Jan 29, 18
        Public Function ManagePromotionOfferOrderBased() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = PromotionID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcProName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(1).Value = PromotionName

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@fltSubTotal", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(4).Value = SubTotal

                arParms(5) = New Npgsql.NpgsqlParameter("@cSaleType", NpgsqlTypes.NpgsqlDbType.Char)
                arParms(5).Value = SaleType

                arParms(6) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(6).Value = Mode

                arParms(7) = New Npgsql.NpgsqlParameter("@fltOldSubTotal", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(7).Value = OldSubTotal

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_ManagePromotionOfferOrderBased", arParms))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Added by Neelam for Order Based Promotion Change on Jan 29, 18
        Public Function GetPromotionOfferOrderBasedItems() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numProID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = PromotionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = SiteID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetPromotionOfferOrderBasedItems", arParms)

                Return ds
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub EnableDisablePromotionOffer()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = PromotionID

                SqlDAL.ExecuteNonQuery(connString, "USP_PromotionOffer_EnableDisable", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        'Added by Neelam Kapila || 10/27/2017 - Added function to Upsert Customer Data
        Public Function ManagePromotionOfferCustomersDTL() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numProID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = PromotionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numProOrgId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = PromotionOrganisationID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintRuleAppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = RuleAppType

                arParms(3) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(3).Value = DivisionID

                arParms(4) = New Npgsql.NpgsqlParameter("@numRelationship", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(4).Value = Relationship

                arParms(5) = New Npgsql.NpgsqlParameter("@numProfile", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(5).Value = Profile

                arParms(6) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(6).Value = Mode

                arParms(7) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(7).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_ManagePromotionOfferCustomersDTL", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        'Added by Neelam Kapila || 10/27/2017 - Added function to fetch Customer Data in promotions
        Public Function GetPromotionOfferCustomers() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numProID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = PromotionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintItemSelectionType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = RuleAppType

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_PromotionOffer_GetCustomerSelections", arParms)

                Return ds
            Catch ex As Exception
                Throw
            End Try
        End Function

#Region "Shipping Promotions"
        'Added by Neelam Kapila || 10/27/2017 - Added function to fetch shipping data in promotions
        Public Function GetShippingPromotions(ByVal numShippingCountry As Long) As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim dt As DataTable
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = Mode

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = UserCntID

                arParms(3) = New Npgsql.NpgsqlParameter("@numShippingCountry", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = numShippingCountry

                arParms(4) = New Npgsql.NpgsqlParameter("@cookieId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(4).Value = CookieId

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                dt = SqlDAL.ExecuteDatable(connString, "USP_GetShippingPromotions", arParms)

                Return dt
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Added by Neelam Kapila || 10/27/2017 - Added function to upsert shipping data in promotions
        Public Function ManageShippingPromotions() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(11) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@bitFreeShiping", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(0).Value = IsEnableFreeShipping

                arParms(1) = New Npgsql.NpgsqlParameter("@monFreeShippingOrderAmount", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(1).Value = FreeShippingOrderAmount

                arParms(2) = New Npgsql.NpgsqlParameter("@numFreeShippingCountry", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = FreeShippingCountry

                arParms(3) = New Npgsql.NpgsqlParameter("@bitFixShipping1", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Value = IsEnableFixShipping1

                arParms(4) = New Npgsql.NpgsqlParameter("@monFixShipping1OrderAmount", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(4).Value = FixShipping1OrderAmount

                arParms(5) = New Npgsql.NpgsqlParameter("@monFixShipping1Charge", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(5).Value = FixShipping1Charge

                arParms(6) = New Npgsql.NpgsqlParameter("@bitFixShipping2", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(6).Value = IsEnableFixShipping2

                arParms(7) = New Npgsql.NpgsqlParameter("@monFixShipping2OrderAmount", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(7).Value = FixShipping2OrderAmount

                arParms(8) = New Npgsql.NpgsqlParameter("@monFixShipping2Charge", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(8).Value = FixShipping2Charge

                arParms(9) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = DomainID

                arParms(10) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(10).Value = UserCntID

                arParms(11) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(11).Value = Nothing
                arParms(11).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_ManageShippingPromotions", arParms))
            Catch ex As Exception
                Throw ex
            End Try
        End Function
#End Region

        Public Function GetDuplicateCouponCodes() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numProID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = PromotionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@txtCouponCode", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(2).Value = CouponCode

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetDuplicateCouponCode", arParms)

                Return ds
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function InsertUpdatePromotionOfferForOrder() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numProId", PromotionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcProName", PromotionName, NpgsqlTypes.NpgsqlDbType.VarChar, 100))
                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@dtValidFrom", ValidFrom, NpgsqlTypes.NpgsqlDbType.Timestamp))
                    .Add(SqlDAL.Add_Parameter("@dtValidTo", ValidTo, NpgsqlTypes.NpgsqlDbType.Timestamp))
                    .Add(SqlDAL.Add_Parameter("@bitNeverExpires", IsNeverExpires, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitUseForCouponManagement", IsUseForCouponManagement, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitRequireCouponCode", IsRequireCouponCode, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@txtCouponCode", CouponCode, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@tintUsageLimit", UsageLimit, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintCustomersBasedOn", PromotionOfferBasedOnCustomer, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@strOrderAmount", OrderAmount, NpgsqlTypes.NpgsqlDbType.VarChar, 1000))
                    .Add(SqlDAL.Add_Parameter("@strfltDiscount", DiscountOnOrder, NpgsqlTypes.NpgsqlDbType.VarChar, 1000))
                    .Add(SqlDAL.Add_Parameter("@tintDiscountType", DiscountType, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_InsertUpdatePromotionOfferForOrder", sqlParams.ToArray()))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetPromotionOfferDetailsForOrder() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numProID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = PromotionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = ClientTimeZoneOffset

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetPromotionOfferDetailsForOrder", arParms)

                Return ds
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetAuditTrailDetailsForOrderPromotion() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numProID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = PromotionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = numItemCode

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetOrderProAuditTrailDetails", arParms)

                Return ds
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function DeleteDiscountCodeForOrderPromotion(ByVal numOppId As Long, ByVal numDivisionId As Long) As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numProID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = PromotionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDiscountId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = numDiscountId

                arParms(3) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = numOppId

                arParms(4) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = numDivisionId

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteOrderProDiscountCode", arParms)

                Return True
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetUsedDiscountCodes() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim dt As DataTable
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numProID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = PromotionID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                dt = SqlDAL.ExecuteDatable(connString, "USP_GetUsedDiscountCodes", arParms)

                Return dt
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetItemPromotionDetails(ByVal itemCode As Long, ByVal unitHour As Double, ByVal totalAmount As Double) As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDivisionID", DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numItemCode", itemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numPromotionID", PromotionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUnitHour", unitHour, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@monTotalAmount", totalAmount, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_GetItemPromotionDetails", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetItemPromotionDiscountItems(ByVal divisionID As Long, ByVal itemCode As Long, ByVal warehouseItemID As Long, ByVal pageIndex As Integer, ByVal pageSize As Integer) As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDivisionID", divisionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numPromotionID", PromotionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numItemCode", itemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWarehouseItemID", warehouseItemID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numPageIndex", pageIndex, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numPageSize", pageSize, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_GetItemPromotionDiscountItems", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ApplyItemPromotionToOrder(ByVal orderedItems As String, ByVal isBasedOnDiscountCode As Boolean, ByVal vcDiscountCode As String) As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDivisionID", DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcItems", orderedItems, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@bitBasedOnDiscountCode", isBasedOnDiscountCode, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@vcDiscountCode", vcDiscountCode, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numCurrencyID", CurrencyID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_PromotionOffer_ApplyItemPromotionToOrder", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ClearCouponCodeItemPromotion(ByVal orderedItems As String) As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDivisionID", DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numPromotionID", PromotionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcItems", orderedItems, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numCurrencyID", CurrencyID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_PromotionOffer_ClearCouponCodeItemPromotion", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetCouponBasedOrderPromotion() As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_PromotionOffer_GetCouponBasedOrderPromotion", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetPromotionApplicableToItem() As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDivisionID", DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numSiteID", SiteID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numItemCode", numItemCode, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_PromotionOffer_GetPromotionApplicableToItem", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function
#End Region

    End Class
End Namespace