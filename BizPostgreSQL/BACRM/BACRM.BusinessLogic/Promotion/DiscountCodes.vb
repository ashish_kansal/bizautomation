﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.Promotion
    Public Class DiscountCodes
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"

        Public Property DiscountID As Long
        Public Property PromotionID As Long
        Public Property DivisionID As Long
        Public Property SiteID As Long
        Public Property DiscountCode As String
        Public Property UsageLimit As Long

#End Region

#Region "Public Methods"

        Public Function ValidateDiscountCode() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numSiteID", SiteID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDivisionId", DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@txtCouponCode", DiscountCode, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_DiscountCodes_Validate", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

#End Region

    End Class
End Namespace
