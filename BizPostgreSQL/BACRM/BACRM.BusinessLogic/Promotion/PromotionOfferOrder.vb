﻿Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Collections.Generic
Imports System.Data.SqlClient

Namespace BACRM.BusinessLogic.Promotion
    Public Class PromotionOfferOrder
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"

        Public Property PromotionID As Long

#End Region

        Public Function GetPromotionOfferOrderByPromotionID() As DataSet
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numPromotionID", PromotionID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur3", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDataset(connString, "USP_PromotionOfferOrder_GetByPromotionID", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class
End Namespace