'**********************************************************************************
' <CAccount.vb>
' 
' 	CHANGE CONTROL:
'	
'	AUTHOR: Goyal 	DATE:28-Feb-05 	VERSION	CHANGES	KEYSTRING:
'**********************************************************************************

Option Explicit On
Option Strict On

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports Npgsql

Namespace BACRM.BusinessLogic.Campaign

    '**********************************************************************************
    ' Module Name  : None
    ' Module Type  : CAccount
    ' 
    ' Description  : This is the business logic classe for Accounts Module
    '**********************************************************************************
    Public Class CCampaign
        Inherits BACRM.BusinessLogic.CBusinessBase


        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Goyal 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Goyal 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region

        'Defining private variables
        ''Private DomainId As Long = 0
        Private _PublicPrivate As Integer = 0
        Private _FirstName As String = String.Empty
        Private _LastName As String = String.Empty
        Private _CompanyName As String = String.Empty
        Private _DivisionName As String = String.Empty
        Private _Country As String = String.Empty
        Private _ContactType As Integer = 0
        Private _Credit As Integer = 0
        Private _CompanyType As Integer = 0
        Private _Rating As Integer = 0
        Private _Industry As Integer = 0
        Private _OpptAmount As Decimal = 0
        Private _Stage As Integer = 0
        Private _Group As Integer = 0
        Private _Profile As String = String.Empty
        Private _TerritoryID As Integer = 0
        Private _AOIList As String = String.Empty
        Private _SortChar As String = String.Empty
        Private _UserID As Integer = 0
        Private _UserTerID As Integer = 0
        Private _UserRights As Integer = 0
        Private _CustomerID As Integer = 0
        Private _State As String = String.Empty
        Private _City As String = String.Empty
        Private _LeadsID As Integer = 0
        Private _ProspectsID As Integer = 0
        Private _AccountsID As Integer = 0
        Private _StartDate As Long = 0
        Private _EndDate As Long = 0
        Private _CampaignID As Long = 0
        Private _Gender As String = String.Empty
        Private _AgeFrom As Integer = 0
        Private _AgeTo As Integer = 0
        Private _PostalCodeFrom As String = String.Empty
        Private _PostalCodeTo As String = String.Empty
        Private _SicCode As String = String.Empty
        Private _Status As Long = 0
        Private _DivisionID As Long = 0
        Private _Position As String = String.Empty
        Private _GroupName As String = String.Empty
        Private _Interested1 As String = String.Empty
        Private _Interested2 As String = String.Empty
        Private _Interested3 As String = String.Empty
        Private _Interested4 As String = String.Empty
        Private _Interested5 As String = String.Empty
        Private _Interested6 As String = String.Empty
        Private _Interested7 As String = String.Empty
        Private _Interested8 As String = String.Empty


        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property
        Public Property Gender() As String
            Get
                Return _Gender
            End Get
            Set(ByVal Value As String)
                _Gender = Value
            End Set
        End Property
        Public Property AgeFrom() As Integer
            Get
                Return _AgeFrom
            End Get
            Set(ByVal Value As Integer)
                _AgeFrom = Value
            End Set
        End Property
        Public Property AgeTo() As Integer
            Get
                Return _AgeTo
            End Get
            Set(ByVal Value As Integer)
                _AgeTo = Value
            End Set
        End Property
        Public Property Position() As String
            Get
                Return _Position
            End Get
            Set(ByVal Value As String)
                _Position = Value
            End Set
        End Property
        Public Property State() As String
            Get
                Return _State
            End Get
            Set(ByVal Value As String)
                _State = Value
            End Set
        End Property
        Public Property Country() As String
            Get
                Return _Country
            End Get
            Set(ByVal Value As String)
                _Country = Value
            End Set
        End Property
        Public Property PostalCodeFrom() As String
            Get
                Return _PostalCodeFrom
            End Get
            Set(ByVal Value As String)
                _PostalCodeFrom = Value
            End Set
        End Property
        Public Property PostalCodeTo() As String
            Get
                Return _PostalCodeTo
            End Get
            Set(ByVal Value As String)
                _PostalCodeTo = Value
            End Set
        End Property
        Public Property SicCode() As String
            Get
                Return _SicCode
            End Get
            Set(ByVal Value As String)
                _SicCode = Value
            End Set
        End Property
        Public Property Industry() As Integer
            Get
                Return _Industry
            End Get
            Set(ByVal Value As Integer)
                _Industry = Value
            End Set
        End Property
        Public Property Status() As Long
            Get
                Return _Status
            End Get
            Set(ByVal Value As Long)
                _Status = Value
            End Set
        End Property
        Public Property CompanyType() As Integer
            Get
                Return _CompanyType
            End Get
            Set(ByVal Value As Integer)
                _CompanyType = Value
            End Set
        End Property
        Public Property ContactType() As Integer
            Get
                Return _ContactType
            End Get
            Set(ByVal Value As Integer)
                _ContactType = Value
            End Set
        End Property
        Public Property Profile() As String
            Get
                Return _Profile
            End Get
            Set(ByVal Value As String)
                _Profile = Value
            End Set
        End Property
        Public Property Rating() As Integer
            Get
                Return _Rating
            End Get
            Set(ByVal Value As Integer)
                _Rating = Value
            End Set
        End Property
        Public Property GroupName() As String
            Get
                Return _GroupName
            End Get
            Set(ByVal Value As String)
                _GroupName = Value
            End Set
        End Property
        Public Property TerritoryID() As Integer
            Get
                Return _TerritoryID
            End Get
            Set(ByVal Value As Integer)
                _TerritoryID = Value
            End Set
        End Property
        Public Property Interested1() As String
            Get
                Return _Interested1
            End Get
            Set(ByVal Value As String)
                _Interested1 = Value
            End Set
        End Property
        Public Property Interested2() As String
            Get
                Return _Interested2
            End Get
            Set(ByVal Value As String)
                _Interested2 = Value
            End Set
        End Property
        Public Property Interested3() As String
            Get
                Return _Interested3
            End Get
            Set(ByVal Value As String)
                _Interested3 = Value
            End Set
        End Property
        Public Property Interested4() As String
            Get
                Return _Interested4
            End Get
            Set(ByVal Value As String)
                _Interested4 = Value
            End Set
        End Property
        Public Property Interested5() As String
            Get
                Return _Interested5
            End Get
            Set(ByVal Value As String)
                _Interested5 = Value
            End Set
        End Property
        Public Property Interested6() As String
            Get
                Return _Interested6
            End Get
            Set(ByVal Value As String)
                _Interested6 = Value
            End Set
        End Property
        Public Property Interested7() As String
            Get
                Return _Interested7
            End Get
            Set(ByVal Value As String)
                _Interested1 = Value
            End Set
        End Property
        Public Property Interested8() As String
            Get
                Return _Interested8
            End Get
            Set(ByVal Value As String)
                _Interested8 = Value
            End Set
        End Property
        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property
        Public Property AOIList() As String
            Get
                Return _AOIList
            End Get
            Set(ByVal Value As String)
                _AOIList = Value
            End Set
        End Property
        Public Property LeadsID() As Integer
            Get
                Return _LeadsID
            End Get
            Set(ByVal Value As Integer)
                _LeadsID = Value
            End Set
        End Property
        Public Property ProspectsID() As Integer
            Get
                Return _ProspectsID
            End Get
            Set(ByVal Value As Integer)
                _ProspectsID = Value
            End Set
        End Property
        Public Property AccountsID() As Integer
            Get
                Return _AccountsID
            End Get
            Set(ByVal Value As Integer)
                _AccountsID = Value
            End Set
        End Property
        Public Property StartDate() As Long
            Get
                Return _StartDate
            End Get
            Set(ByVal Value As Long)
                _StartDate = Value
            End Set
        End Property
        Public Property EndDate() As Long
            Get
                Return _EndDate
            End Get
            Set(ByVal Value As Long)
                _EndDate = Value
            End Set
        End Property
        Public Property CampaignID() As Long
            Get
                Return _CampaignID
            End Get
            Set(ByVal Value As Long)
                _CampaignID = Value
            End Set
        End Property
        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  SearchCampaignEmail()
        '     Purpose					 :  This function will return  search records.  
        '     Example                    :  
        '     Parameters                 :                     
        '                                 
        '     Outputs					 :  It return search records.  
        '						
        '     Author Name				 :  Ajeet Singh
        '     Date Written				 :  23/12/2004
        '     Cross References 			 :  List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Function SearchCampaignEmail() As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(32) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@Gender", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(0).Value = _Gender

                arParms(1) = New Npgsql.NpgsqlParameter("@AgeFrom", NpgsqlTypes.NpgsqlDbType.Integer, 4)
                arParms(1).Value = _AgeFrom

                arParms(2) = New Npgsql.NpgsqlParameter("@AgeTo", NpgsqlTypes.NpgsqlDbType.Integer, 4)
                arParms(2).Value = _AgeTo

                arParms(3) = New Npgsql.NpgsqlParameter("@Position", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(3).Value = _Position

                arParms(4) = New Npgsql.NpgsqlParameter("@State", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(4).Value = _State

                arParms(5) = New Npgsql.NpgsqlParameter("@Country", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(5).Value = _Country

                arParms(6) = New Npgsql.NpgsqlParameter("@PostalCodeFrom", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(6).Value = _PostalCodeFrom

                arParms(7) = New Npgsql.NpgsqlParameter("@PostalCodeTo", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(7).Value = _PostalCodeTo

                arParms(8) = New Npgsql.NpgsqlParameter("@SicCode", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(8).Value = _SicCode

                arParms(9) = New Npgsql.NpgsqlParameter("@ContactType", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(9).Value = _ContactType


                arParms(10) = New Npgsql.NpgsqlParameter("@Industry", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(10).Value = _Industry

                arParms(11) = New Npgsql.NpgsqlParameter("@Status", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(11).Value = _Status

                arParms(12) = New Npgsql.NpgsqlParameter("@CompanyType", NpgsqlTypes.NpgsqlDbType.Integer, 4)
                arParms(12).Value = _CompanyType

                arParms(13) = New Npgsql.NpgsqlParameter("@Profile", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(13).Value = _Profile

                arParms(14) = New Npgsql.NpgsqlParameter("@Rating", NpgsqlTypes.NpgsqlDbType.Integer, 4)
                arParms(14).Value = _Rating

                arParms(15) = New Npgsql.NpgsqlParameter("@GroupName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(15).Value = _GroupName

                arParms(16) = New Npgsql.NpgsqlParameter("@TerritoryID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(16).Value = _TerritoryID

                arParms(17) = New Npgsql.NpgsqlParameter("@Interested1", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(17).Value = _Interested1

                arParms(18) = New Npgsql.NpgsqlParameter("@Interested2", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(18).Value = _Interested2

                arParms(19) = New Npgsql.NpgsqlParameter("@Interested3", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(19).Value = _Interested3

                arParms(20) = New Npgsql.NpgsqlParameter("@Interested4", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(20).Value = _Interested4

                arParms(21) = New Npgsql.NpgsqlParameter("@Interested5", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(21).Value = _Interested5

                arParms(22) = New Npgsql.NpgsqlParameter("@Interested6", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(22).Value = _Interested6

                arParms(23) = New Npgsql.NpgsqlParameter("@Interested7", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(23).Value = _Interested7

                arParms(24) = New Npgsql.NpgsqlParameter("@Interested8", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(24).Value = _Interested8

                arParms(25) = New Npgsql.NpgsqlParameter("@DomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(25).Value = DomainID

                arParms(26) = New Npgsql.NpgsqlParameter("@AOIList", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(26).Value = _AOIList

                arParms(27) = New Npgsql.NpgsqlParameter("@LeadsID", NpgsqlTypes.NpgsqlDbType.Smallint, 4)
                arParms(27).Value = _LeadsID

                arParms(28) = New Npgsql.NpgsqlParameter("@ProspectsID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(28).Value = _ProspectsID

                arParms(29) = New Npgsql.NpgsqlParameter("@AccountsID", NpgsqlTypes.NpgsqlDbType.Smallint, 4)
                arParms(29).Value = _AccountsID

                arParms(30) = New Npgsql.NpgsqlParameter("@StartDate", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(30).Value = _StartDate

                arParms(31) = New Npgsql.NpgsqlParameter("@EndDate", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(31).Value = _EndDate

                arParms(32) = New Npgsql.NpgsqlParameter("@CampaignID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(32).Value = _CampaignID

                Dim ds As DataSet
                'Call ExecuteReader static method of SqlDal class that returns an Object. Then cast the return value to string.
                ' We pass in database connection string, command type, stored procedure name and productID Npgsql.NpgsqlParameter
                ds = SqlDAL.ExecuteDataset(connString, "usp_Campaign_Profile", arParms)

                'total no of records 
                Return ds 'return dataset 
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  UpdateCampaign()
        '     Purpose					 :  This function will update Campaign  
        '     Example                    :  
        '     Parameters                 :                     
        '                                 
        '     Outputs					 :  It return search records.  
        '						
        '     Author Name				 :  Ajeet Singh
        '     Date Written				 :  27/12/2004
        '     Cross References 			 :  List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Function UpdateCampaign() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@DivisionID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@CampaignID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _CampaignID

                'Call ExecuteReader static method of SqlDal class that returns an Object. Then cast the return value to string.
                ' We pass in database connection string, command type, stored procedure name and productID Npgsql.NpgsqlParameter
                SqlDAL.ExecuteNonQuery(connString, "usp_UpdateCampaign", arParms)
                'total no of records 
                Return True 'return dataset 
            Catch ex As Exception
                ' Log exception details
                Return False
                Throw ex
            End Try
        End Function

        Public Function SaveCommunicationinfo(ByVal varCompaignName As String, ByVal intType As Integer, ByVal intLevel As Integer, ByVal intFocus As Integer, ByVal lngLaunchDate As Long, ByVal lngCompaignCost As Long, ByVal intCreatedBy As Integer, ByVal lngCreateDate As Long, ByVal intModifiedBy As Integer, Optional ByVal lngModifiedDate As Long = 0) As Long
            '================================================================================
            ' Purpose: This function is used for inserting records into the database. 
            '          The function returns the communication id
            '
            ' History
            ' Ver   Date        Ref     Author              Reason
            ' 1     13/05/2004          Nisheeth Kaushal         Created
            '                                               
            '
            ' Non Compliance (any deviation from standards)
            '   No deviations from the standards.
            '================================================================================        

            Dim Cnn As New NpgsqlConnection(GetConnection.GetConnectionString) 'Connection string
            Cnn.Open()
            Dim Transobj As NpgsqlTransaction 'Transaction object

            'Dim cnn As New Npgsql.NpgSqlConnection(clsConnection.fn_GetConnectionString)
            Dim CmdToExecute As NpgsqlCommand = New NpgsqlCommand
            Dim StrQuery As String

            Transobj = Cnn.BeginTransaction
            CmdToExecute.CommandType = CommandType.Text
            'CmdToExecute.Connection = cnn
            CmdToExecute.Connection = Cnn
            CmdToExecute.Transaction = Transobj

            Try
                StrQuery = "EXEC usp_InsertCampaignMaster"
                StrQuery = StrQuery & "  @pvcCampaign_Name='" & varCompaignName & "'"
                StrQuery = StrQuery & ", @pnumCampaignType_ID=" & intType
                StrQuery = StrQuery & ", @pnumCampaignLevel_ID=" & intLevel
                StrQuery = StrQuery & ", @pnumCampaignFocus_ID=" & intFocus
                StrQuery = StrQuery & ", @pbintLaunchDate=" & lngLaunchDate
                StrQuery = StrQuery & ", @pmonCampaignCost=" & lngCompaignCost
                StrQuery = StrQuery & ", @pnumCreatedBy=" & intCreatedBy
                StrQuery = StrQuery & ", @pbintCreatedDate='" & lngCreateDate & "'"
                StrQuery = StrQuery & ", @pnumModifiedBy='" & intModifiedBy & "'"      'Corresponding entry in the calendar in exchange
                StrQuery = StrQuery & ", @pbintModifiedDate='" & lngModifiedDate & "'"
                StrQuery = StrQuery & ", @pbitDeleted='" & 0 & "'"

                'SaveCommunicationinfo = StrQuery
                'Exit Function
                CmdToExecute.CommandText = StrQuery

                'If cnn.State = ConnectionState.Closed Then cnn.Open()
                If Cnn.State = ConnectionState.Closed Then Cnn.Open()
                SaveCommunicationinfo = CLng(CmdToExecute.ExecuteScalar())

                '     On Error Resume Next
                Transobj.Commit()
                Transobj.Dispose()
            Catch ex As Exception
                SaveCommunicationinfo = 0
                Transobj.Rollback()
                ' // some error occured. Bubble it to caller and encapsulate Exception object
                Throw New Exception("Error while inserting Communciation", ex)

            Finally
                CmdToExecute.Dispose()
                'cnn.Close()
                CmdToExecute = Nothing
                'cnn = Nothing
                'transobj.dispose
                Cnn.Close()
                Cnn = Nothing
            End Try
        End Function

        Public Function EditCampaigns(ByVal CamID As Long, ByVal varCompaignName As String, ByVal intType As Integer, ByVal intLevel As Integer, ByVal intFocus As Integer, ByVal lngLaunchDate As Long, ByVal lngCompaignCost As Long, Optional ByVal intCreatedBy As Integer = 0, Optional ByVal lngCreateDate As Long = 0, Optional ByVal intModifiedBy As Integer = 0, Optional ByVal lngModifiedDate As Long = 0) As Long
            '================================================================================
            ' Purpose: This function is used for inserting records into the database. 
            '          The function returns the communication id
            '
            ' History
            ' Ver   Date        Ref     Author              Reason
            ' 1     13/05/2004          Rumina         Created
            '                                               
            '
            ' Non Compliance (any deviation from standards)
            '   No deviations from the standards.
            '================================================================================        

            Dim Cnn As New NpgsqlConnection(GetConnection.GetConnectionString) 'Connection string
            Cnn.Open()
            Dim Transobj As NpgsqlTransaction 'Transaction object

            'Dim cnn As New Npgsql.NpgSqlConnection(clsConnection.fn_GetConnectionString)
            Dim CmdToExecute As NpgsqlCommand = New NpgsqlCommand
            Dim StrQuery As String

            Transobj = Cnn.BeginTransaction
            CmdToExecute.CommandType = CommandType.Text
            'CmdToExecute.Connection = cnn
            CmdToExecute.Connection = Cnn
            CmdToExecute.Transaction = Transobj

            Try
                StrQuery = "EXEC usp_UpdateCampaignMaster"
                StrQuery = StrQuery & "  @pnumCampaign_ID=" & CamID
                StrQuery = StrQuery & ",  @pvcCampaign_Name='" & varCompaignName & "'"
                StrQuery = StrQuery & ", @pnumCampaignType_ID=" & intType
                StrQuery = StrQuery & ", @pnumCampaignLevel_ID=" & intLevel
                StrQuery = StrQuery & ", @pnumCampaignFocus_ID=" & intFocus
                StrQuery = StrQuery & ", @pbintLaunchDate=" & lngLaunchDate
                StrQuery = StrQuery & ", @pmonCampaignCost=" & lngCompaignCost
                StrQuery = StrQuery & ", @pnumCreatedBy=" & intCreatedBy
                StrQuery = StrQuery & ", @pbintCreatedDate='" & lngCreateDate & "'"
                StrQuery = StrQuery & ", @pnumModifiedBy='" & intModifiedBy & "'"      'Corresponding entry in the calendar in exchange
                StrQuery = StrQuery & ", @pbintModifiedDate='" & lngModifiedDate & "'"
                StrQuery = StrQuery & ", @pbitDeleted='" & 0 & "'"

                'SaveCommunicationinfo = StrQuery
                'Exit Function
                CmdToExecute.CommandText = StrQuery

                'If cnn.State = ConnectionState.Closed Then cnn.Open()
                If Cnn.State = ConnectionState.Closed Then Cnn.Open()
                EditCampaigns = CLng(CmdToExecute.ExecuteScalar())

                '     On Error Resume Next
                Transobj.Commit()
                Transobj.Dispose()

            Catch ex As Exception
                EditCampaigns = 0
                Transobj.Rollback()
                ' // some error occured. Bubble it to caller and encapsulate Exception object
                Throw New Exception("Error while inserting Communciation", ex)
            Finally
                CmdToExecute.Dispose()
                'cnn.Close()
                CmdToExecute = Nothing
                'cnn = Nothing
                'transobj.dispose
                Cnn.Close()
                Cnn = Nothing
            End Try
        End Function

        Public Function fn_GetDateFromNumber(ByVal lngDate As Long, ByVal strFormat As String) As String
            '================================================================================
            ' Purpose: This Function will take Date in Number format, the Date Format as 
            '           String and convert teh Number formatted date into String format supplied.
            '
            ' Parameters:
            '           lngDate = This will be the Date is number format. The Date in number 
            '                       format will always be YYYYMMDDhhmmss
            '           strFormat = This will be the Format for the Date required. 
            '                   The format can be anyone of the following:
            '           FORMAT			REPRESENTATION
            '           MM/DD/YYYY		03/12/2003
            '           MM/DD/YY		03/12/03
            '           MM-DD-YYYY		03-12-2003
            '           MM-DD-YY		03-12-03
            '           DD/MON/YYYY		12/MAR/2003
            '           DD/MON/YY		12/MAR/03
            '           DD-MON-YYYY		12-MAR-2003
            '           DD-MON-YY		12-MAR-03
            '           DD/MONTH/YYYY	12/MARCH/2003
            '           DD/MONTH/YY		12/MARCH/03
            '           DD-MONTH-YYYY	12-MARCH-2003
            '           DD-MONTH-YY		12-MARCH-03	
            '           DD/MM/YYYY		12/MAR/2003
            '           DD/MM/YY		12/03/03
            '           DD-MM-YYYY		12-03-2003
            '			DD-MM-YY		12-03-03
            '
            ' Return Value: The function returns a date in the format supplied by the above 
            '               string parameter.
            '
            ' History
            ' Ver   Date        Ref     Author              Reason
            ' 1     13/03/2003          Anup Jishnu         Created
            '
            ' Non Compliance (any deviation from standards)
            '   No deviations from the standards.
            '================================================================================
            'Convert the Number format to simple string.
            Dim strGotDate As String = CStr(lngDate)
            'Set the Year in 4 digits to a variable.
            Dim str4Yr As String = Left(strGotDate, 4)
            'Set the Year in 2 Digits to a Variable
            Dim str2Yr As String = Mid(strGotDate, 3, 2)
            'Set the Month in 2 Digits to a Variable
            Dim strIntMonth As String = Mid(strGotDate, 5, 2)
            'Set the Date in 2 Digits to a Variable
            Dim strDate As String = Mid(strGotDate, 7, 2)
            'Set the Abbrivated Month Name to a Variable
            Dim str3Month As String = MonthName(CInt(strIntMonth), True)
            'Set the Full Month Name to a Variable
            Dim strFullMonth As String = MonthName(CInt(strIntMonth), False)

            'As the Date Format will be one of the above mentioned formats,
            'we need to replace the required string to get the formatted date.
            strFormat = Replace(strFormat, "DD", strDate)
            strFormat = Replace(strFormat, "YYYY", str4Yr)
            strFormat = Replace(strFormat, "YY", str2Yr)
            strFormat = Replace(strFormat, "MM", strIntMonth)
            strFormat = Replace(strFormat, "MONTH", strFullMonth)
            strFormat = Replace(strFormat, "MON", str3Month)

            'Return the formatted date.        
            fn_GetDateFromNumber = strFormat

        End Function
    End Class
End Namespace

