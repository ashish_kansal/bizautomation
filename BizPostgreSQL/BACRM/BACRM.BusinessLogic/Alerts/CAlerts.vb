Option Explicit On
Option Strict On
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRMAPI.DataAccessLayer
Imports System.Data.SqlClient
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Documents

Namespace BACRM.BusinessLogic.Alerts
    Public Class CAlerts
        Inherits BACRM.BusinessLogic.CBusinessBase







        'Private DomainId As Long
        Private _EmailCampaignId As Long
        Private _AlertID As Long
        Private _AlertDTLID As Long
        Private _EmailTemplateID As Long
        Private _CCManager As Boolean
        Private _DaysAfterDue As Long
        Private _Amount As Decimal
        Private _BizDocs As Long
        Private _AlertOn As Short
        Private _DepartMent As Long
        Private _NoofItems As Long
        Private _AlertEmailID As Long
        Private _byteMode As Short
        Private _vcEmail As String
        Private _Age As Long
        Private _ContactID As Long
        Private _AlertConID As Long
        Private _BizDocCreated As Boolean
        Private _BizDocModified As Boolean
        Private _OppOwner As Boolean
        Private _PriContact As Boolean
        Private _BizDocAlertID As Long
        Private _Approved As Boolean
        ' Added by Pratik Vasani
        Public Property EmailCampaignId() As Long
            Get
                Return _EmailCampaignId
            End Get
            Set(ByVal value As Long)
                _EmailCampaignId = value
            End Set
        End Property

        Public Property Approved() As Boolean
            Get
                Return _Approved
            End Get
            Set(ByVal Value As Boolean)
                _Approved = Value
            End Set
        End Property

        Public Property BizDocAlertID() As Long
            Get
                Return _BizDocAlertID
            End Get
            Set(ByVal Value As Long)
                _BizDocAlertID = Value
            End Set
        End Property

        Public Property PriContact() As Boolean
            Get
                Return _PriContact
            End Get
            Set(ByVal Value As Boolean)
                _PriContact = Value
            End Set
        End Property

        Public Property OppOwner() As Boolean
            Get
                Return _OppOwner
            End Get
            Set(ByVal Value As Boolean)
                _OppOwner = Value
            End Set
        End Property

        Public Property BizDocModified() As Boolean
            Get
                Return _BizDocModified
            End Get
            Set(ByVal Value As Boolean)
                _BizDocModified = Value
            End Set
        End Property

        Public Property BizDocCreated() As Boolean
            Get
                Return _BizDocCreated
            End Get
            Set(ByVal Value As Boolean)
                _BizDocCreated = Value
            End Set
        End Property

        Public Property AlertConID() As Long
            Get
                Return _AlertConID
            End Get
            Set(ByVal Value As Long)
                _AlertConID = Value
            End Set
        End Property

        Public Property ContactID() As Long
            Get
                Return _ContactID
            End Get
            Set(ByVal Value As Long)
                _ContactID = Value
            End Set
        End Property

        Public Property Age() As Long
            Get
                Return _Age
            End Get
            Set(ByVal Value As Long)
                _Age = Value
            End Set
        End Property

        Public Property vcEmail() As String
            Get
                Return _vcEmail
            End Get
            Set(ByVal Value As String)
                _vcEmail = Value
            End Set
        End Property

        Public Property byteMode() As Short
            Get
                Return _byteMode
            End Get
            Set(ByVal Value As Short)
                _byteMode = Value
            End Set
        End Property

        Public Property AlertEmailID() As Long
            Get
                Return _AlertEmailID
            End Get
            Set(ByVal Value As Long)
                _AlertEmailID = Value
            End Set
        End Property

        Public Property NoofItems() As Long
            Get
                Return _NoofItems
            End Get
            Set(ByVal Value As Long)
                _NoofItems = Value
            End Set
        End Property

        Public Property Department() As Long
            Get
                Return _DepartMent
            End Get
            Set(ByVal Value As Long)
                _DepartMent = Value
            End Set
        End Property

        Public Property AlertOn() As Short
            Get
                Return _AlertOn
            End Get
            Set(ByVal Value As Short)
                _AlertOn = Value
            End Set
        End Property

        Public Property BizDocs() As Long
            Get
                Return _BizDocs
            End Get
            Set(ByVal Value As Long)
                _BizDocs = Value
            End Set
        End Property

        Public Property Amount() As Decimal
            Get
                Return _Amount
            End Get
            Set(ByVal Value As Decimal)
                _Amount = Value
            End Set
        End Property

        Public Property DaysAfterDue() As Long
            Get
                Return _DaysAfterDue
            End Get
            Set(ByVal Value As Long)
                _DaysAfterDue = Value
            End Set
        End Property

        Public Property CCManager() As Boolean
            Get
                Return _CCManager
            End Get
            Set(ByVal Value As Boolean)
                _CCManager = Value
            End Set
        End Property

        Public Property EmailTemplateID() As Long
            Get
                Return _EmailTemplateID
            End Get
            Set(ByVal Value As Long)
                _EmailTemplateID = Value
            End Set
        End Property

        Public Property AlertDTLID() As Long
            Get
                Return _AlertDTLID
            End Get
            Set(ByVal Value As Long)
                _AlertDTLID = Value
            End Set
        End Property

        Public Property AlertID() As Long
            Get
                Return _AlertID
            End Get
            Set(ByVal Value As Long)
                _AlertID = Value
            End Set
        End Property

        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property

        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop Jayara
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop Jayaraj
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region

        Public Function GetBAM() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetBAM", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function ManageAlerts() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(12) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numAlertID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _AlertID

                arParms(1) = New Npgsql.NpgsqlParameter("@numAlertDTLid", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _AlertDTLID

                arParms(2) = New Npgsql.NpgsqlParameter("@numEmailTemplate", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _EmailTemplateID

                arParms(3) = New Npgsql.NpgsqlParameter("@tintCCManager", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = If(_CCManager, 1, 0)

                arParms(4) = New Npgsql.NpgsqlParameter("@numDaysAfterDue", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _DaysAfterDue


                arParms(5) = New Npgsql.NpgsqlParameter("@monAmount", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(5).Value = _Amount

                arParms(6) = New Npgsql.NpgsqlParameter("@numBizDocs", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _BizDocs

                arParms(7) = New Npgsql.NpgsqlParameter("@tintAlertOn", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(7).Value = _AlertOn

                arParms(8) = New Npgsql.NpgsqlParameter("@numDepartMentID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = _DepartMent

                arParms(9) = New Npgsql.NpgsqlParameter("@numNoOfItems", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = _NoofItems


                arParms(10) = New Npgsql.NpgsqlParameter("@numAge", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(10).Value = _Age

                arParms(11) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(11).Value = DomainId

                arParms(12) = New Npgsql.NpgsqlParameter("@numEmailCampaignId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(12).Value = _EmailCampaignId

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageAlerts", arParms)

                Return True

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function GetAlertDetails() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numAlertID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _AlertID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetAlertdetails", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function ManageEmails() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numAlertDTLID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _AlertDTLID

                arParms(1) = New Npgsql.NpgsqlParameter("@numAlertEmailID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _AlertEmailID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcEmailID", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(2).Value = _vcEmail

                arParms(3) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _byteMode

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = DomainId


                SqlDAL.ExecuteNonQuery(connString, "USP_ManageAlertEmails", arParms)

                Return True

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetAlertEmails() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numAlertDTLID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _AlertDTLID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetAlertEmails", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetAlertConAndCompany() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numAlerDTLId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _AlertDTLID

                arParms(1) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _byteMode

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetAlertsConAndCom", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ManageAlertConAndCompany() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numAlerDTLId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _AlertDTLID

                arParms(1) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ContactID

                arParms(2) = New Npgsql.NpgsqlParameter("@numAlertConID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _AlertConID

                arParms(3) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _byteMode


                SqlDAL.ExecuteNonQuery(connString, "USP_ManagaeAlertsConAndCom", arParms)

                Return True

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetAlertdtlForPastDuebills() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDepartMentID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DepartMent

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetAlertDtlforPastDueBills", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function DeleteAlertdtlForPastDuebills() As Boolean
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numAlertDTLid", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _AlertDTLID


                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteAlertDtlforPastDueBills", arParms)

                Return True

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetIndAlertDTL() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numAlertDTLID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _AlertDTLID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetAlertDTL", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetCountCompany() As Integer
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numAlerDTLId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _AlertDTLID

                arParms(1) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ContactID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return CInt(SqlDAL.ExecuteScalar(connString, "USP_GetCountOfCompany", arParms))



            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function GetAlerDetailIds() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numAlertId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _AlertID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetAlertIds", arParms)
                Return ds.Tables(0)


            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function ManageBizDocAlerts() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _BizDocs

                arParms(1) = New Npgsql.NpgsqlParameter("@bitCreated", NpgsqlTypes.NpgsqlDbType.Boolean)
                arParms(1).Value = _BizDocCreated

                arParms(2) = New Npgsql.NpgsqlParameter("@bitModified", NpgsqlTypes.NpgsqlDbType.Boolean)
                arParms(2).Value = _BizDocModified

                arParms(3) = New Npgsql.NpgsqlParameter("@numEmailTemplate", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _EmailTemplateID

                arParms(4) = New Npgsql.NpgsqlParameter("@bitOppOwner", NpgsqlTypes.NpgsqlDbType.Boolean)
                arParms(4).Value = _OppOwner

                arParms(5) = New Npgsql.NpgsqlParameter("@bitCCManager", NpgsqlTypes.NpgsqlDbType.Boolean)
                arParms(5).Value = _CCManager

                arParms(6) = New Npgsql.NpgsqlParameter("@bitPriConatct", NpgsqlTypes.NpgsqlDbType.Boolean)
                arParms(6).Value = _PriContact

                arParms(7) = New Npgsql.NpgsqlParameter("@bitApproved", NpgsqlTypes.NpgsqlDbType.Boolean)
                arParms(7).Value = _Approved

                arParms(8) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = DomainId

                SqlDAL.ExecuteNonQuery(connString, "USP_BizDOcAlertManage", arParms)

                Return True

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function BizDocAlertDTLs() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _BizDocs

                arParms(1) = New Npgsql.NpgsqlParameter("@bitCreated", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(1).Value = _BizDocCreated

                arParms(2) = New Npgsql.NpgsqlParameter("@bitCreated", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(2).Value = _BizDocModified

                arParms(3) = New Npgsql.NpgsqlParameter("@bitCreated", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Value = _Approved

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = DomainID

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_BizDOcAlertDtls", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteBizDocAlerts() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numBizDocAlertID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _BizDocAlertID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteBizDocAlert", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Enum enmBizDoc
            IsCreated
            IsModified
            IsApproved
        End Enum

        Public Sub SendBizDocAlerts(ByVal lngOppID As Long, ByVal lngOppBizDocID As Long, ByVal BizDoc As Long, ByVal lngDomainID As Long, ByVal enm As enmBizDoc)
            Dim dtOppBizAddDtl As DataTable
            Dim objOppBizDocs As New OppBizDocs
            objOppBizDocs.OppBizDocId = lngOppBizDocID
            objOppBizDocs.OppId = lngOppID
            objOppBizDocs.DomainID = lngDomainID
            dtOppBizAddDtl = objOppBizDocs.GetOppAddressDetail

            Dim objAlerts As New CAlerts
            Dim dtAlerts As DataTable
            objAlerts.BizDocs = BizDoc
            objAlerts.DomainID = lngDomainID
            If enm = enmBizDoc.IsModified Then
                'Send email when BizDoc is Modified
                objAlerts.BizDocModified = True
            ElseIf enm = enmBizDoc.IsCreated Then
                'Send email when BizDoc is created
                objAlerts.BizDocCreated = True
            ElseIf enm = enmBizDoc.IsApproved Then
                'Send email when BizDoc is Approved
                objAlerts.Approved = True
            End If
            dtAlerts = objAlerts.BizDocAlertDTLs
            Dim strTo As String = String.Empty
            Dim objCommon As New CCommon

            For Each dr As DataRow In dtAlerts.Rows
                'Get email template
                Dim dtEmailTemplate As DataTable
                Dim objDocuments As New DocumentList
                objDocuments.GenDocID = CCommon.ToLong(dr("numEmailTemplate"))
                objDocuments.DomainID = lngDomainID
                dtEmailTemplate = objDocuments.GetDocByGenDocID
                If dtEmailTemplate.Rows.Count > 0 Then
                    'SendEmail function has to use SMTP details from Domain details
                    System.Web.HttpContext.Current.Session("SMTPServerIntegration") = True

                    objCommon.byteMode = 1
                    'Send to Record Owner of deal ?
                    If CCommon.ToBool(dr("bitOppOwner")) = True Then
                        objCommon.ContactID = CCommon.ToLong(dtOppBizAddDtl.Rows(0).Item("Owner"))
                        strTo = objCommon.GetAccountHoldersEmail
                    End If
                    'Send to Primary Contact the Opportunity/Deal is for ?
                    If CCommon.ToBool(dr("bitPriConatct")) = True Then
                        objCommon.Mode = 1
                        objCommon.ContactID = CCommon.ToLong(dtOppBizAddDtl.Rows(0).Item("numContactid"))
                        strTo = objCommon.GetContactsEmail()
                    End If
                    'Send to Manager of Record Owner
                    If CCommon.ToBool(dr("bitCCManager")) = True Then
                        objCommon.ContactID = CCommon.ToLong(dtOppBizAddDtl.Rows(0).Item("Owner"))
                        strTo = objCommon.GetManagerEmail()
                    End If

                    If strTo.Trim.Length > 2 Then
                        Dim objSendEmail As New Email

                        objSendEmail.DomainID = lngDomainID
                        objSendEmail.DocID = CCommon.ToLong(dr("numEmailTemplate"))
                        objSendEmail.TemplateCode = "" '  "#SYS#EMAIL_ALERT:BIZDOC_CREATED/MODIFIED/APPROVAL"
                        objSendEmail.ModuleID = 8
                        objSendEmail.RecordIds = lngOppBizDocID.ToString
                        objSendEmail.AdditionalMergeFields.Add("LoggedInUser", System.Web.HttpContext.Current.Session("ContactName"))
                        objSendEmail.AdditionalMergeFields.Add("BizDocAlertBasedOn", IIf(enm = enmBizDoc.IsModified, "Modified", IIf(enm = enmBizDoc.IsCreated, "Created", "Approved")))
                        objSendEmail.FromEmail = CCommon.ToString(System.Web.HttpContext.Current.Session("UserEmail"))
                        objSendEmail.ToEmail = strTo
                        objSendEmail.SendEmailTemplate()

                    End If

                    'objSendMail.AddEmailToJob(CCommon.ToString(dtEmailTemplate.Rows(0).Item("vcSubject")), CCommon.ToString(dtEmailTemplate.Rows(0).Item("vcDocdesc")), "", CCommon.ToString(System.Web.HttpContext.Current.Session("UserEmail")), strTo, dtMergeFields, "", "", lngDomainID)
                    strTo = ""
                End If
            Next
        End Sub
        'Private Sub AddRowtoMergeTable(ByVal dtOppBizAddDtl As DataTable, ByRef dtMergeFields As DataTable, ByVal strEmail As String)
        '    Try
        '        Dim drNew As DataRow
        '        drNew = dtMergeFields.NewRow
        '        drNew("BizDocID") = dtOppBizAddDtl.Rows(0).Item("BizDocID")
        '        drNew("DealAmount") = dtOppBizAddDtl.Rows(0).Item("Amount")
        '        drNew("vcCompanyName") = dtOppBizAddDtl.Rows(0).Item("CompName")
        '        drNew("vcFirstName") = ""
        '        drNew("vcLastName") = ""
        '        drNew("Email") = strEmail
        '        dtMergeFields.Rows.Add(drNew)
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub


        Shared Sub SendWebLeadAlerts(ByVal lngEmailTemplateID As Long, ByVal lnDivID As Long, ByVal lnCntID As Long, ByVal lngSendEmailTo As Long, ByVal lngRecordOwner As Long, ByVal lngDomainID As Long)
            Try
                Dim objCommon As New CCommon
                Dim strFrmEmail As String = ""
                Dim strSignature As String = ""
                If lngRecordOwner > 0 Then
                    objCommon.ContactID = lngRecordOwner
                    strFrmEmail = objCommon.GetContactsEmail
                End If

                Dim strToEmail As String = ""
                objCommon.ContactID = lngSendEmailTo
                strToEmail = objCommon.GetContactsEmail()


                objCommon.DomainID = lngDomainID
                objCommon.Mode = 23
                objCommon.Str = CCommon.ToString(lngRecordOwner)
                strSignature = CCommon.ToString(objCommon.GetSingleFieldValue())

                Dim objSendEmail As New Email

                objSendEmail.DomainID = lngDomainID
                objSendEmail.DocID = lngEmailTemplateID
                objSendEmail.TemplateCode = "" '  "#SYS#EMAIL_ALERT:NEW_WEBLEAD_ARRIVAL"
                objSendEmail.ModuleID = 1
                objSendEmail.RecordIds = lnCntID.ToString
                objSendEmail.FromEmail = strFrmEmail
                objSendEmail.AdditionalMergeFields.Add("Signature", strSignature)
                objSendEmail.ToEmail = IIf(strToEmail.Length > 0, strToEmail, "##ContactFirstName## <##ContactEmail##>").ToString()
                objSendEmail.SendEmailTemplate()

            Catch ex As Exception
                Throw ex
            End Try

        End Sub
        ''' <summary>
        ''' Notify Assignee that they have been assigned selected record
        ''' </summary>
        ''' <param name="lngModuleID">
        ''' 1	Leads/Prospects/Accounts
        ''' 2	Opportunity
        ''' 3	Items
        ''' 4	Projects
        ''' 5	Cases
        ''' 6	Tickler
        ''' 7	Documents
        ''' 8	BizDocs
        ''' SELECT * FROM EmailMergeModule
        ''' </param>
        ''' <param name="lngRecordOwner"></param>
        ''' <param name="lngAssignee"></param>
        ''' <param name="lngDomainID"></param>
        ''' <remarks></remarks>
        Shared Sub SendAlertToAssignee(ByVal lngModuleID As Long, ByVal lngRecordOwner As Long, ByVal lngAssignee As Long, ByVal lngRecordID As Long, ByVal lngDomainID As Long)
            Try
                Dim objCommon As New CCommon
                Dim strFrmEmail As String = ""
                If lngRecordOwner > 0 Then
                    objCommon.ContactID = lngRecordOwner
                    strFrmEmail = objCommon.GetContactsEmail
                End If

                Dim strToEmail As String = ""
                If lngAssignee > 0 Then
                    objCommon.ContactID = lngAssignee
                    strToEmail = objCommon.GetContactsEmail()
                End If

                Dim objSendEmail As New Email

                objSendEmail.DomainID = lngDomainID
                objSendEmail.ModuleID = lngModuleID
                objSendEmail.AdditionalMergeFields.Add("Signature", System.Web.HttpContext.Current.Session("Signature"))

                Select Case lngModuleID
                   
                    Case 2
                        objSendEmail.TemplateCode = "#SYS#EMAIL_ALERT:OPPORTUNITY_ASSIGNED"
                    Case 1
                        objSendEmail.TemplateCode = "#SYS#EMAIL_ALERT:LEAD_ASSIGNED"
                        objSendEmail.AdditionalMergeFields.Add("ContactOpt-OutLink", "")
                    Case 4
                        objSendEmail.TemplateCode = "#SYS#EMAIL_ALERT:PROJECT_ASSIGNED"
                    Case 5
                        objSendEmail.TemplateCode = "#SYS#EMAIL_ALERT:CASE_ASSIGNED"
                    Case 6
                        objSendEmail.TemplateCode = "#SYS#EMAIL_ALERT:TICKLER_ASSIGNED"
                        objSendEmail.AdditionalMergeFields.Add("TicklerAcceptLink", "")
                        objSendEmail.AdditionalMergeFields.Add("TicklerCompanyContact", "")

                    Case Else
                End Select

                objSendEmail.RecordIds = lngRecordID.ToString
                objSendEmail.AdditionalMergeFields.Add("LoggedInUser", System.Web.HttpContext.Current.Session("ContactName"))
                objSendEmail.FromEmail = strFrmEmail
                objSendEmail.ToEmail = strToEmail
                If strToEmail.Length > 2 Then objSendEmail.SendEmailTemplate()
            Catch ex As Exception
                Throw ex
            End Try

        End Sub

     
    End Class

End Namespace
