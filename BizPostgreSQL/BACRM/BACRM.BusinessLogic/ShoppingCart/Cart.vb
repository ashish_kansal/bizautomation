﻿Imports BACRM.BusinessLogic.Item
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRM.BusinessLogic.Common
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.ShioppingCart
    Public Class Cart
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _vcCoupon As String
        Public Property vcCoupon As String
            Get
                Return _vcCoupon
            End Get
            Set(ByVal value As String)
                _vcCoupon = value
            End Set
        End Property

        Private _CartId As Long
        Public Property CartId As Long
            Get
                Return _CartId
            End Get
            Set(ByVal value As Long)
                _CartId = value
            End Set
        End Property

        Private _intpostselldiscount As Int16
        Public Property intpostselldiscount As Long
            Get
                Return _intpostselldiscount
            End Get
            Set(ByVal value As Long)
                _intpostselldiscount = value
            End Set
        End Property

        Private _ContactId As Long
        Public Property ContactId As Long
            Get
                Return _ContactId
            End Get
            Set(ByVal value As Long)
                _ContactId = value
            End Set
        End Property
        'Private DomainId As Long
        'Public Property DomainId As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal value As Long)
        '        DomainId = value
        '    End Set
        'End Property
        Private _CookieId As String
        Public Property CookieId As String
            Get
                Return _CookieId
            End Get
            Set(ByVal value As String)
                _CookieId = value
            End Set
        End Property
        Private _OppItemCode As Long
        Public Property OppItemCode As Long
            Get
                Return _OppItemCode
            End Get
            Set(ByVal value As Long)
                _OppItemCode = value
            End Set
        End Property
        Private _ItemCode As Long
        Public Property ItemCode As Long
            Get
                Return _ItemCode
            End Get
            Set(ByVal value As Long)
                _ItemCode = value
            End Set
        End Property
        Private _UnitHour As Long
        Public Property UnitHour As Long
            Get
                Return _UnitHour
            End Get
            Set(ByVal value As Long)
                _UnitHour = value
            End Set
        End Property
        Private _price As Decimal
        Public Property Price As Decimal
            Get
                Return _price
            End Get
            Set(ByVal value As Decimal)
                _price = value
            End Set
        End Property
        Private _SourceId As Long
        Public Property SourceId As Long
            Get
                Return _SourceId
            End Get
            Set(ByVal value As Long)
                _SourceId = value
            End Set
        End Property
        Private _ItemDesc As String
        Public Property ItemDesc As String
            Get
                Return _ItemDesc
            End Get
            Set(ByVal value As String)
                _ItemDesc = value
            End Set
        End Property
        Private _WarehouseId As Long
        Public Property WarehouseId As Long
            Get
                Return _WarehouseId
            End Get
            Set(ByVal value As Long)
                _WarehouseId = value
            End Set
        End Property
        Private _ItemName As String
        Public Property ItemName As String
            Get
                Return _ItemName
            End Get
            Set(ByVal value As String)
                _ItemName = value
            End Set
        End Property
        Private _Warehouse As String
        Public Property Warehouse As String
            Get
                Return _Warehouse
            End Get
            Set(ByVal value As String)
                _Warehouse = value
            End Set
        End Property
        Private _WarehouseItemsId As Long
        Public Property WarehouseItemsId As Long
            Get
                Return _WarehouseItemsId
            End Get
            Set(ByVal value As Long)
                _WarehouseItemsId = value
            End Set
        End Property
        Private _ItemType As String
        Public Property ItemType As String
            Get
                Return _ItemType
            End Get
            Set(ByVal value As String)
                _ItemType = value
            End Set
        End Property
        Private _Attributes As String
        Public Property Attributes As String
            Get
                Return _Attributes
            End Get
            Set(ByVal value As String)
                _Attributes = value
            End Set
        End Property
        Private _AttributesValue As String
        Public Property AttributesValue As String
            Get
                Return _AttributesValue
            End Get
            Set(ByVal value As String)
                _AttributesValue = value
            End Set
        End Property
        Private _FreeShipping As Boolean
        Public Property FreeShipping As Boolean
            Get
                Return _FreeShipping
            End Get
            Set(ByVal value As Boolean)
                _FreeShipping = value
            End Set
        End Property
        Private _Weight As Decimal
        Public Property Weight As Decimal
            Get
                Return _Weight
            End Get
            Set(ByVal value As Decimal)
                _Weight = value
            End Set
        End Property
        Private _OPFlag As Short
        Public Property OPFlag As Short
            Get
                Return _OPFlag
            End Get
            Set(ByVal value As Short)
                _OPFlag = value
            End Set
        End Property
        Private _DiscountType As Boolean
        Public Property DiscountType As Boolean
            Get
                Return _DiscountType
            End Get
            Set(ByVal value As Boolean)
                _DiscountType = value
            End Set
        End Property
        Private _Discount As Double
        Public Property Discount As Double
            Get
                Return _Discount
            End Get
            Set(ByVal value As Double)
                _Discount = value
            End Set
        End Property
        Private _TotAmtBeforeDisc As Decimal
        Public Property TotAmtBeforeDisc As Decimal
            Get
                Return _TotAmtBeforeDisc
            End Get
            Set(ByVal value As Decimal)
                _TotAmtBeforeDisc = value
            End Set
        End Property
        Private _ItemURL As String
        Public Property ItemURL As String
            Get
                Return _ItemURL
            End Get
            Set(ByVal value As String)
                _ItemURL = value
            End Set
        End Property
        Private _UOM As Long
        Public Property UOM As Long
            Get
                Return _UOM
            End Get
            Set(ByVal value As Long)
                _UOM = value
            End Set
        End Property
        Private _UOMName As String
        Public Property UOMName As String
            Get
                Return _UOMName
            End Get
            Set(ByVal value As String)
                _UOMName = value
            End Set
        End Property
        Private _UOMConversionFactor As Decimal
        Public Property UOMConversionFactor As Decimal
            Get
                Return _UOMConversionFactor
            End Get
            Set(ByVal value As Decimal)
                _UOMConversionFactor = value
            End Set
        End Property
        Private _Height As Long
        Public Property Height As Long
            Get
                Return _Height
            End Get
            Set(ByVal value As Long)
                _Height = value
            End Set
        End Property
        Private _Lenght As Long
        Public Property Length As Long
            Get
                Return _Lenght
            End Get
            Set(ByVal value As Long)
                _Lenght = value
            End Set
        End Property
        Private _Width As Long
        Public Property Width As Long
            Get
                Return _Width
            End Get
            Set(ByVal value As Long)
                _Width = value
            End Set
        End Property
        Private _ShippingMethod As String
        Public Property ShippingMethod As String
            Get
                Return _ShippingMethod
            End Get
            Set(ByVal value As String)
                _ShippingMethod = value
            End Set
        End Property
        Private _ServiceTypeId As Long
        Public Property ServiceTypeId As Long
            Get
                Return _ServiceTypeId
            End Get
            Set(ByVal value As Long)
                _ServiceTypeId = value
            End Set
        End Property
        Private _ShippingCharge As Decimal
        Public Property ShippingCharge As Decimal
            Get
                Return _ShippingCharge
            End Get
            Set(ByVal value As Decimal)
                _ShippingCharge = value
            End Set
        End Property
        Private _ShippingCompany As Long
        Public Property ShippingCompany As Long
            Get
                Return _ShippingCompany
            End Get
            Set(ByVal value As Long)
                _ShippingCompany = value
            End Set
        End Property
        Private _ServiceType As Short
        Public Property ServiceType As Short
            Get
                Return _ServiceType
            End Get
            Set(ByVal value As Short)
                _ServiceType = value
            End Set
        End Property
        Private _DeliveryDate As DateTime
        Public Property DeliveryDate As DateTime
            Get
                Return _DeliveryDate
            End Get
            Set(ByVal value As DateTime)
                _DeliveryDate = value
            End Set
        End Property
        Private _strXML As String
        Public Property strXML As String
            Get
                Return _strXML
            End Get
            Set(ByVal value As String)
                _strXML = value
            End Set
        End Property
        Private _ToTAmount As Decimal
        Public Property TotAmount As Decimal
            Get
                Return _ToTAmount
            End Get
            Set(ByVal value As Decimal)
                _ToTAmount = value
            End Set
        End Property
        Private _bitDeleteAll As Boolean = False
        Public Property bitDeleteAll As Boolean
            Get
                Return _bitDeleteAll
            End Get
            Set(ByVal value As Boolean)
                _bitDeleteAll = value
            End Set
        End Property


        Private _DivisionID As Long
        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal value As Long)
                _DivisionID = value
            End Set
        End Property

        Private _BaseTaxOn As Integer
        Public Property BaseTaxOn() As Integer
            Get
                Return _BaseTaxOn
            End Get
            Set(ByVal value As Integer)
                _BaseTaxOn = value
            End Set
        End Property
        Private _CountryID As Integer
        Public Property CountryID() As Integer
            Get
                Return _CountryID
            End Get
            Set(ByVal value As Integer)
                _CountryID = value
            End Set
        End Property
        Private _StateID As Integer
        Public Property StateID() As Integer
            Get
                Return _StateID
            End Get
            Set(ByVal value As Integer)
                _StateID = value
            End Set
        End Property
        Private _City As String
        Public Property City() As String
            Get
                Return _City
            End Get
            Set(ByVal value As String)
                _City = value
            End Set
        End Property
        Private _ZipPostalCode As String
        Public Property ZipPostalCode() As String
            Get
                Return _ZipPostalCode
            End Get
            Set(ByVal value As String)
                _ZipPostalCode = value
            End Set
        End Property
        Private _TotalTaxAmount As Decimal
        Public Property TotalTaxAmount() As Decimal
            Get
                Return _TotalTaxAmount
            End Get
            Set(ByVal value As Decimal)
                _TotalTaxAmount = value
            End Set
        End Property

        Private _PromotionID As Long
        Public Property PromotionID() As Long
            Get
                Return _PromotionID
            End Get
            Set(ByVal value As Long)
                _PromotionID = value
            End Set
        End Property
        Private _PromotionDescription As String
        Public Property PromotionDescription() As String
            Get
                Return _PromotionDescription
            End Get
            Set(ByVal value As String)
                _PromotionDescription = value
            End Set
        End Property

        Private _ShippingPromotionID As Long
        Public Property ShippingPromotionID() As Long
            Get
                Return _ShippingPromotionID
            End Get
            Set(ByVal value As Long)
                _ShippingPromotionID = value
            End Set
        End Property

        Private _bitPromotionUsed As Boolean
        Public Property bitPromotionUsed() As Boolean
            Get
                Return _bitPromotionUsed
            End Get
            Set(ByVal value As Boolean)
                _bitPromotionUsed = value
            End Set
        End Property

        Public Property SiteID As Long
        Public Property ParentItemCode As Long
        Public Property ChildKitItemSelection As String
        Public Property PreferredPromotion As String

        Public Function GetCartItems() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = ContactId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcCookieId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = CookieId

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetCartItem", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetCartItemPromotionDetail(ByVal numUOMID As Long, ByVal vcSelectedKitChildItems As String, ByVal numShippingCountry As Long, Optional ByVal vcSendCoupon As String = "0") As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = ItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@numUOMID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = numUOMID

                arParms(3) = New Npgsql.NpgsqlParameter("@numShippingCountry", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = numShippingCountry

                arParms(4) = New Npgsql.NpgsqlParameter("@cookieId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(4).Value = CookieId

                arParms(5) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(5).Value = UserCntID

                arParms(6) = New Npgsql.NpgsqlParameter("@vcSendCoupon", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(6).Value = vcSendCoupon

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_GetCartItemPromotionDetail", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function


        Public Function RemoveCouponFromCart(Optional ByVal vcSendCoupon As String = "0") As String
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = ItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@cookieId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = CookieId

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@vcSendCoupon", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(4).Value = vcSendCoupon

                arParms(5) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = SiteID

                SqlDAL.ExecuteNonQuery(connString, "USP_RemoveCouponCode", arParms)
                Return "1"
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ApplyCouponCodetoCart(Optional ByVal vcSendCoupon As String = "0") As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Direction = ParameterDirection.InputOutput
                arParms(1).Value = ItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@cookieId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = CookieId

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@vcSendCoupon", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(4).Value = vcSendCoupon

                arParms(5) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = SiteID

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_ApplyCouponCodetoCart", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetShippingChargebyPromotion() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numShippingCountry", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = CountryID

                arParms(2) = New Npgsql.NpgsqlParameter("@cookieId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = CookieId

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@monShippingAmount", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(4).Direction = ParameterDirection.InputOutput
                arParms(4).Value = ShippingCharge

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_GetShippingCharge", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function GetTaxOfCartItems() As Double
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(15) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DivisionID

                arParms(2) = New Npgsql.NpgsqlParameter("@BaseTaxOn", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = BaseTaxOn

                arParms(3) = New Npgsql.NpgsqlParameter("@CookieId", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(3).Value = CookieId

                arParms(4) = New Npgsql.NpgsqlParameter("@numCountryID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = CountryID

                arParms(5) = New Npgsql.NpgsqlParameter("@numStateID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = StateID

                arParms(6) = New Npgsql.NpgsqlParameter("@vcCity", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(6).Value = City

                arParms(7) = New Npgsql.NpgsqlParameter("@vcZipPostalCode", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(7).Value = ZipPostalCode

                arParms(8) = New Npgsql.NpgsqlParameter("@fltTotalTaxAmount", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(8).Direction = ParameterDirection.InputOutput
                arParms(8).Value = TotalTaxAmount

                arParms(9) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = ContactId

                arParms(10) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(10).Value = Nothing
                arParms(10).Direction = ParameterDirection.InputOutput

                arParms(11) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(11).Value = Nothing
                arParms(11).Direction = ParameterDirection.InputOutput

                arParms(12) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(12).Value = Nothing
                arParms(12).Direction = ParameterDirection.InputOutput

                arParms(13) = New Npgsql.NpgsqlParameter("@SWV_RefCur4", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(13).Value = Nothing
                arParms(13).Direction = ParameterDirection.InputOutput

                arParms(14) = New Npgsql.NpgsqlParameter("@SWV_RefCur5", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(14).Value = Nothing
                arParms(14).Direction = ParameterDirection.InputOutput

                arParms(15) = New Npgsql.NpgsqlParameter("@SWV_RefCur6", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(15).Value = Nothing
                arParms(15).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_GetTaxOfCartItems", objParam, True)
                _TotalTaxAmount = Convert.ToDecimal(DirectCast(objParam, Npgsql.NpgsqlParameter())(8).Value)

                Return _TotalTaxAmount

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function InsertCartItems() As Boolean
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numUserCntId", ContactId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcCookieId", CookieId, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numOppItemCode", OppItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numItemCode", ItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUnitHour", UnitHour, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@monPrice", Price, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@numSourceId", SourceId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcItemDesc", ItemDesc, NpgsqlTypes.NpgsqlDbType.VarChar, 2000))
                    .Add(SqlDAL.Add_Parameter("@numWarehouseId", WarehouseId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcItemName", ItemName, NpgsqlTypes.NpgsqlDbType.VarChar, 200))
                    .Add(SqlDAL.Add_Parameter("@vcWarehouse", Warehouse, NpgsqlTypes.NpgsqlDbType.VarChar, 200))
                    .Add(SqlDAL.Add_Parameter("@numWarehouseItmsID", WarehouseItemsId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcItemType", ItemType, NpgsqlTypes.NpgsqlDbType.VarChar, 200))
                    .Add(SqlDAL.Add_Parameter("@vcAttributes", Attributes, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcAttrValues", AttributesValue, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@bitFreeShipping", FreeShipping, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@numWeight", Weight, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@tintOpFlag", OPFlag, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@bitDiscountType", DiscountType, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@fltDiscount", Discount, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@monTotAmtBefDiscount", TotAmtBeforeDisc, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@ItemURL", ItemURL, NpgsqlTypes.NpgsqlDbType.VarChar, 200))
                    .Add(SqlDAL.Add_Parameter("@numUOM", UOM, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcUOMName", UOMName, NpgsqlTypes.NpgsqlDbType.VarChar, 200))
                    .Add(SqlDAL.Add_Parameter("@decUOMConversionFactor", UOMConversionFactor, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@numHeight", Height, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numLength", Length, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWidth", Width, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcShippingMethod", ShippingMethod, NpgsqlTypes.NpgsqlDbType.VarChar, 200))
                    .Add(SqlDAL.Add_Parameter("@numServiceTypeId", ServiceTypeId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@decShippingCharge", ShippingCharge, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@numShippingCompany", ShippingCharge, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintServicetype", ServiceType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@dtDeliveryDate", DeliveryDate, NpgsqlTypes.NpgsqlDbType.Timestamp))
                    .Add(SqlDAL.Add_Parameter("@monTotAmount", TotAmount, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@numSiteID", SiteID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numParentItemCode", ParentItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcChildKitItemSelection", ChildKitItemSelection, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcPreferredPromotions", PreferredPromotion, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                CartId = CCommon.ToLong(SqlDAL.ExecuteScalar(connString, "USP_InsertCartItem", sqlParams.ToArray()))

                If CartId <= 0 Then
                    Return False
                End If
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function UpdateCartItem() As Integer
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet
                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = ContactId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcCookieId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = CookieId

                arParms(3) = New Npgsql.NpgsqlParameter("@strXML", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(3).Value = strXML

                arParms(4) = New Npgsql.NpgsqlParameter("@postselldiscount", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = intpostselldiscount

                arParms(5) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = SiteID

                arParms(6) = New Npgsql.NpgsqlParameter("@vcPreferredPromotions", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(6).Value = PreferredPromotion

                Return SqlDAL.ExecuteNonQuery(connString, "USP_UpdateCartItem", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function RemoveItemFromCart()
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = ContactId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcCookieId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = CookieId

                arParms(3) = New Npgsql.NpgsqlParameter("@numCartId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = CartId

                arParms(4) = New Npgsql.NpgsqlParameter("@bitDeleteAll", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(4).Value = bitDeleteAll

                arParms(5) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = SiteID

                arParms(6) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = ItemCode

                Return SqlDAL.ExecuteNonQuery(connString, "USP_DeleteCartItem", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetParentToAddRequiredItem() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_GetParentToAddRequiredItem", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Public Function AddItemToCart()
        '    TryF
        '        'dsTemp = HttpContext.Current.Session("Data")
        '        Dim dtItem As DataTable
        '        'dtItem = dsTemp.Tables(0)
        '        Dim dr As DataRow
        '        dr = dtItem.NewRow
        '        dr("numoppitemtCode") = CType(IIf(IsDBNull(dtItem.Compute("MAX(numoppitemtCode)", "")), 0, dtItem.Compute("MAX(numoppitemtCode)", "")), Integer) + 1
        '        dr("numItemCode") = _ItemCode
        '        dr("numUnitHour") = _Units
        '        If Not FreeShipping Then dr("Weight") = _Weight

        '        Dim objPbook As New PriceBookRule
        '        objPbook.ItemID = _ItemCode
        '        objPbook.QntyofItems = _Units 'dr("numUnitHour")
        '        objPbook.DomainID = DomainId
        '        objPbook.DivisionID = _DivisionID
        '        Dim dtCalPrice As DataTable
        '        dtCalPrice = objPbook.GetPriceBasedonPriceBook
        '        If dtCalPrice.Rows.Count > 0 Then
        '            dr("monPrice") = dtCalPrice.Rows(0).Item("DiscItemPrice")
        '        Else : dr("monPrice") = _ItemUnitPrice 'lblUnitCost.Text
        '        End If

        '        dr("bitDiscountType") = 0
        '        dr("fltDiscount") = 0
        '        dr("monTotAmtBefDiscount") = IIf(IsDBNull(dr("numUnitHour")), 0, dr("numUnitHour")) * IIf(IsDBNull(dr("monPrice")), 0, dr("monPrice"))
        '        dr("monTotAmount") = dr("monTotAmtBefDiscount")

        '        dr("vcItemDesc") = ""
        '        dr("numWarehouseID") = ""
        '        dr("vcItemName") = _ItemName 'lblModelName.Text
        '        'If txtHidValue.Text = "False" Then
        '        '    'dr("Warehouse") = ddlWarehouse.SelectedItem.Text
        '        '    dr("numWarehouseItmsID") = txtWareHouseItemID.Text
        '        '    If txtHidValue.Text = "False" Then
        '        '        dr("Attributes") = objCommon.GetAttributesForWarehouseItem(dr("numWarehouseItmsID"), 0)
        '        '    End If
        '        '    Dim i As Integer
        '        '    Dim controlID As String
        '        '    Dim ddl As DropDownList
        '        '    For i = 0 To dtOppAtributes.Rows.Count - 1
        '        '        controlID = "Attr" & dtOppAtributes.Rows(i).Item("fld_id")
        '        '        ddl = tblItemAttr.FindControl(controlID)
        '        '        If ddl.SelectedIndex > 0 Then strValues = strValues & ddl.SelectedValue & ","
        '        '    Next
        '        '    dr("AttrValues") = strValues.TrimEnd(",")
        '        'End If
        '        'dr("ItemType") = ""
        '        'dr("Op_Flag") = 1
        '        'dtItem.Rows.Add(dr)
        '        'dsTemp.AcceptChanges()
        '        'Dim dtTable As DataTable
        '        'dtTable = dsTemp.Tables(0)
        '        'txtAddedItems.Text = ""
        '        'For Each dr In dtTable.Rows
        '        '    txtAddedItems.Text = txtAddedItems.Text & dr("numWarehouseItmsID") & ","
        '        'Next
        '        'txtAddedItems.Text = txtAddedItems.Text.Trim(",")
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function

        Public Function GetItemTotal(ByVal ds As DataSet) As String()
            Try
                Dim arr(1) As String
                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    arr(0) = CCommon.ToInteger(ds.Tables(0).Compute("sum(numUnitHour)", ""))
                    arr(1) = CCommon.ToDecimal(ds.Tables(0).Compute("sum(monTotAmount)", ""))
                Else
                    arr(0) = 0
                    arr(1) = 0
                End If
                Return arr
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Sub ApplyCouponCodePromotion()
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntId", ContactId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numSiteID", SiteID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcCookieId", CookieId, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@bitBasedOnDiscountCode", True, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@vcDiscountCode", vcCoupon, NpgsqlTypes.NpgsqlDbType.VarChar))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_PromotionOffer_ApplyItemPromotionToECommerce", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub ClearCouponCodePromotion()
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntId", ContactId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numSiteID", SiteID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcCookieId", CookieId, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numPromotionID", PromotionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_PromotionOffer_ClearCouponCodeECommerce", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub
    End Class
End Namespace
