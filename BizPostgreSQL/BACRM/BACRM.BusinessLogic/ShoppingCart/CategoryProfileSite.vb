﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient

Namespace BACRM.BusinessLogic.ShioppingCart
    Public Class CategoryProfileSite
        Inherits CBusinessBase
#Region "Public Properties"
        Public ID As Long
        Public CategoryProfileID As Long
        Public SiteID As Long
        Public SiteName As String
#End Region
    End Class
End Namespace