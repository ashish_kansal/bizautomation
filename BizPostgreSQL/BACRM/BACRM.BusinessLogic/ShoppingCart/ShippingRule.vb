﻿Option Explicit On
Option Strict On
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Web
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Item
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.ShioppingCart
    Public Class ShippingRule
        Inherits BACRM.BusinessLogic.CBusinessBase


        Private _RuleID As Long
        Public Property RuleID() As Long
            Get
                Return _RuleID
            End Get
            Set(ByVal value As Long)
                _RuleID = value
            End Set
        End Property

        Private _RuleName As String
        Public Property RuleName() As String
            Get
                Return _RuleName
            End Get
            Set(ByVal value As String)
                _RuleName = value
            End Set
        End Property

        Private _Description As String
        Public Property Description() As String
            Get
                Return _Description
            End Get
            Set(ByVal value As String)
                _Description = value
            End Set
        End Property

        Private _BasedOn As Short
        Public Property BasedOn() As Short
            Get
                Return _BasedOn
            End Get
            Set(ByVal value As Short)
                _BasedOn = value
            End Set
        End Property

        Private _ShippingMethod As Integer
        Public Property ShippingMethod() As Integer
            Get
                Return _ShippingMethod
            End Get
            Set(ByVal value As Integer)
                _ShippingMethod = value
            End Set
        End Property

        Private _AppliesTo As Short
        Public Property AppliesTo() As Short
            Get
                Return _AppliesTo
            End Get
            Set(ByVal value As Short)
                _AppliesTo = value
            End Set
        End Property

        Private _ContactsType As Short
        Public Property ContactsType() As Short
            Get
                Return _ContactsType
            End Get
            Set(ByVal value As Short)
                _ContactsType = value
            End Set
        End Property

        Private _SiteID As Long
        Public Property SiteID() As Long
            Get
                Return _SiteID
            End Get
            Set(ByVal value As Long)
                _SiteID = value
            End Set
        End Property

        Private _byteMode As Short
        Public Property byteMode() As Short
            Get
                Return _byteMode
            End Get
            Set(ByVal Value As Short)
                _byteMode = Value
            End Set
        End Property

        Private _ServiceTypeID As Long
        Public Property ServiceTypeID() As Long
            Get
                Return _ServiceTypeID
            End Get
            Set(ByVal value As Long)
                _ServiceTypeID = value
            End Set
        End Property

        Private _ServiceName As String
        Public Property ServiceName() As String
            Get
                Return _ServiceName
            End Get
            Set(ByVal value As String)
                _ServiceName = value
            End Set
        End Property

        Private _From As Integer
        Public Property From() As Integer
            Get
                Return _From
            End Get
            Set(ByVal value As Integer)
                _From = value
            End Set
        End Property

        Private _To As Integer
        Public Property ToValue() As Integer
            Get
                Return _To
            End Get
            Set(ByVal value As Integer)
                _To = value
            End Set
        End Property

        Private _MarkUp As Double
        Public Property MarkUp() As Double
            Get
                Return _MarkUp
            End Get
            Set(ByVal value As Double)
                _MarkUp = value
            End Set
        End Property

        Private _MarkUpType As Boolean
        Public Property MarkUpType() As Boolean
            Get
                Return _MarkUpType
            End Get
            Set(ByVal value As Boolean)
                _MarkUpType = value
            End Set
        End Property

        Private _Rate As Decimal
        Public Property Rate() As Decimal
            Get
                Return _Rate
            End Get
            Set(ByVal value As Decimal)
                _Rate = value
            End Set
        End Property

        Private _Enabled As Boolean
        Public Property Enabled() As Boolean
            Get
                Return _Enabled
            End Get
            Set(ByVal value As Boolean)
                _Enabled = value
            End Set
        End Property

        Private _str As String
        Public Property Str() As String
            Get
                Return _str
            End Get
            Set(ByVal value As String)
                _str = value
            End Set
        End Property

        Private _StateID As Long
        Public Property StateID() As Long
            Get
                Return _StateID
            End Get
            Set(ByVal value As Long)
                _StateID = value
            End Set
        End Property

        Private _CountryID As Long
        Public Property CountryID() As Long
            Get
                Return _CountryID
            End Get
            Set(ByVal value As Long)
                _CountryID = value
            End Set
        End Property

        Private _Type As Short
        Public Property Type() As Short
            Get
                Return _Type
            End Get
            Set(ByVal value As Short)
                _Type = value
            End Set
        End Property

        Private _RuleStateID As Long
        Public Property RuleStateID() As Long
            Get
                Return _RuleStateID
            End Get
            Set(ByVal value As Long)
                _RuleStateID = value
            End Set
        End Property

        Private _DivisionID As Long
        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal value As Long)
                _DivisionID = value
            End Set
        End Property

        Private _RelationshipID As Long
        Public Property RelationshipID() As Long
            Get
                Return _RelationshipID
            End Get
            Set(ByVal value As Long)
                _RelationshipID = value
            End Set
        End Property

        Private _ProfileId As Long
        Public Property ProfileID() As Long
            Get
                Return _ProfileId
            End Get
            Set(ByVal value As Long)
                _ProfileId = value
            End Set
        End Property

        Private _CategoryID As Long
        Public Property CategoryID() As Long
            Get
                Return _CategoryID
            End Get
            Set(ByVal value As Long)
                _CategoryID = value
            End Set
        End Property

        Private _ItemID As Long
        Public Property ItemID() As Long
            Get
                Return _ItemID
            End Get
            Set(ByVal value As Long)
                _ItemID = value
            End Set
        End Property

        Private _TaxMode As Integer
        Public Property TaxMode() As Integer
            Get
                Return _TaxMode
            End Get
            Set(ByVal value As Integer)
                _TaxMode = value
            End Set
        End Property

        Private _FixedShippingQuotesMode As Integer
        Public Property FixedShippingQuotesMode() As Integer
            Get
                Return _FixedShippingQuotesMode
            End Get
            Set(ByVal value As Integer)
                _FixedShippingQuotesMode = value
            End Set
        End Property

        Public _Weight As Double = 0
        Public _Height As Double = 0
        Public _Length As Double = 0
        Public _Width As Double = 0
        Public _Country As Long = 0
        Public _State As Long = 0
        Public Property Width() As Double
            Get
                Return _Width
            End Get
            Set(ByVal Value As Double)
                _Width = Value
            End Set
        End Property

        Public Property Length() As Double
            Get
                Return _Length
            End Get
            Set(ByVal Value As Double)
                _Length = Value
            End Set
        End Property

        Public Property Height() As Double
            Get
                Return _Height
            End Get
            Set(ByVal Value As Double)
                _Height = Value
            End Set
        End Property

        Public Property Weight() As Double
            Get
                Return _Weight
            End Get
            Set(ByVal Value As Double)
                _Weight = Value
            End Set
        End Property

        Public Property Country() As Long
            Get
                Return _Country
            End Get
            Set(ByVal Value As Long)
                _Country = Value
            End Set
        End Property

        Public Property State() As Long
            Get
                Return _State
            End Get
            Set(ByVal Value As Long)
                _State = Value
            End Set
        End Property

        Private _ZipCode As String
        Public Property ZipCode() As String
            Get
                Return _ZipCode
            End Get
            Set(ByVal value As String)
                _ZipCode = value
            End Set
        End Property

        'Added by :Sachin Sadhu||date:31stJuly2014
        'Purpose :To consolidate Packaging n shipping rules

        Private _PackagingShipRuleID As Long
        Public Property PackagingShipRuleID() As Long
            Get
                Return _PackagingShipRuleID
            End Get
            Set(ByVal value As Long)
                _PackagingShipRuleID = value
            End Set
        End Property
        'end of code
        Private _bitFreeshipping As Boolean
        Public Property bitFreeshipping() As Boolean
            Get
                Return _bitFreeshipping
            End Get
            Set(ByVal value As Boolean)
                _bitFreeshipping = value
            End Set
        End Property

        Private _FreeShippingAmt As Integer
        Public Property FreeShippingAmt() As Integer
            Get
                Return _FreeShippingAmt
            End Get
            Set(ByVal value As Integer)
                _FreeShippingAmt = value
            End Set
        End Property

        Private _numRelationship As Long
        Public Property numRelationship() As Long
            Get
                Return _numRelationship
            End Get
            Set(ByVal value As Long)
                _numRelationship = value
            End Set
        End Property

        Private _numProfile As Long
        Public Property numProfile() As Long
            Get
                Return _numProfile
            End Get
            Set(ByVal value As Long)
                _numProfile = value
            End Set
        End Property

        Private _numWarehouseId As String
        Public Property numWarehouseId() As String
            Get
                Return _numWarehouseId
            End Get
            Set(ByVal value As String)
                _numWarehouseId = value
            End Set
        End Property

        Private _vcZipPostalRange As String
        Public Property vcZipPostalRange() As String
            Get
                Return _vcZipPostalRange
            End Get
            Set(ByVal value As String)
                _vcZipPostalRange = value
            End Set
        End Property

        Private _RelationshipProfileId As String
        Public Property RelationshipProfileId() As String
            Get
                Return _RelationshipProfileId
            End Get
            Set(ByVal value As String)
                _RelationshipProfileId = value
            End Set
        End Property

        Private _numClassificationID As Long
        Public Property numClassificationID() As Long
            Get
                Return _numClassificationID
            End Get
            Set(ByVal value As Long)
                _numClassificationID = value
            End Set
        End Property

        Private _PercentAbove As Double
        Public Property PercentAbove() As Double
            Get
                Return _PercentAbove
            End Get
            Set(ByVal value As Double)
                _PercentAbove = value
            End Set
        End Property

        Private _FlatAmt As Double
        Public Property FlatAmt() As Double
            Get
                Return _FlatAmt
            End Get
            Set(ByVal value As Double)
                _FlatAmt = value
            End Set
        End Property

        Private _numShippingExceptionID As Long
        Public Property numShippingExceptionID() As Long
            Get
                Return _numShippingExceptionID
            End Get
            Set(ByVal value As Long)
                _numShippingExceptionID = value
            End Set
        End Property

        Private _minShippingCost As Double
        Public Property minShippingCost() As Double
            Get
                Return _minShippingCost
            End Get
            Set(ByVal value As Double)
                _minShippingCost = value
            End Set
        End Property

        Private _bitEnableStaticShipping As Boolean
        Public Property bitEnableStaticShipping() As Boolean
            Get
                Return _bitEnableStaticShipping
            End Get
            Set(ByVal value As Boolean)
                _bitEnableStaticShipping = value
            End Set
        End Property

        Private _bitEnableShippingExceptions As Boolean
        Public Property bitEnableShippingExceptions() As Boolean
            Get
                Return _bitEnableShippingExceptions
            End Get
            Set(ByVal value As Boolean)
                _bitEnableShippingExceptions = value
            End Set
        End Property

        Private _ItemClassification As String
        Public Property ItemClassification() As String
            Get
                Return _ItemClassification
            End Get
            Set(ByVal value As String)
                _ItemClassification = value
            End Set
        End Property

        Private _bitItemClassification As Boolean
        Public Property bitItemClassification() As Boolean
            Get
                Return _bitItemClassification
            End Get
            Set(ByVal value As Boolean)
                _bitItemClassification = value
            End Set
        End Property

        Private _numSiteId As Long
        Public Property numSiteId() As Long
            Get
                Return _numSiteId
            End Get
            Set(ByVal value As Long)
                _numSiteId = value
            End Set
        End Property
        Public Function GetShippingRule() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numRuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _RuleID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _byteMode

                arParms(3) = New Npgsql.NpgsqlParameter("@numRelProfileID", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(3).Value = RelationshipProfileId

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetShippingRule", arParms)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DelShippingRule() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numRuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _RuleID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteShippingRule", arParms)

                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function ManageShippingRule() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numRuleID", _RuleID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcRuleName", _RuleName, NpgsqlTypes.NpgsqlDbType.VarChar, 100))
                    .Add(SqlDAL.Add_Parameter("@vcDescription", _Description, NpgsqlTypes.NpgsqlDbType.BigInt, 1000))
                    .Add(SqlDAL.Add_Parameter("@tintBasedOn", _BasedOn, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@tinShippingMethod", _ShippingMethod, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@tintType", _Type, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintTaxMode", TaxMode, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@tintFixedShippingQuotesMode", FixedShippingQuotesMode, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@bitFreeshipping", bitFreeshipping, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@FreeShippingAmt", FreeShippingAmt, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@numRelationship", numRelationship, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numProfile", numProfile, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitItemClassification", bitItemClassification, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@numSiteId", numSiteId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@strWarehouseIds", numWarehouseId, NpgsqlTypes.NpgsqlDbType.Varchar, 1000))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_ManageShippingRule", sqlParams.ToArray()))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetShippingServiceType() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numRuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _RuleID

                arParms(2) = New Npgsql.NpgsqlParameter("@numServiceTypeID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ServiceTypeID

                arParms(3) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _byteMode

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetShippingServiceTypes", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Sub ManageShippingServiceTypes()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(13) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = _byteMode

                arParms(1) = New Npgsql.NpgsqlParameter("@numServiceTypeID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _ServiceTypeID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@numRuleID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(3).Value = _RuleID

                arParms(4) = New Npgsql.NpgsqlParameter("@vcServiceName", NpgsqlTypes.NpgsqlDbType.Varchar, 100)
                arParms(4).Value = _ServiceName

                arParms(5) = New Npgsql.NpgsqlParameter("@intNsoftEnum", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = 0

                arParms(6) = New Npgsql.NpgsqlParameter("@intFrom", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = _From

                arParms(7) = New Npgsql.NpgsqlParameter("@intTo", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(7).Value = _To

                arParms(8) = New Npgsql.NpgsqlParameter("@fltMarkup", NpgsqlTypes.NpgsqlDbType.Double)
                arParms(8).Value = _MarkUp

                arParms(9) = New Npgsql.NpgsqlParameter("@bitMarkupType", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(9).Value = _MarkUpType

                arParms(10) = New Npgsql.NpgsqlParameter("@monRate", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(10).Value = _Rate

                arParms(11) = New Npgsql.NpgsqlParameter("@bitEnabled", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(11).Value = _Enabled

                arParms(12) = New Npgsql.NpgsqlParameter("@vcItemClassification", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(12).Value = _ItemClassification

                arParms(13) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.Xml)
                arParms(13).Value = _str

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageShippingServiceType", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub ManageShippingRuleStates()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numCountryID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _CountryID

                arParms(1) = New Npgsql.NpgsqlParameter("@numStateID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _StateID

                arParms(2) = New Npgsql.NpgsqlParameter("@numRuleID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Value = _RuleID

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(3).Value = DomainID

                arParms(4) = New Npgsql.NpgsqlParameter("@vcZipPostalRange", NpgsqlTypes.NpgsqlDbType.Varchar, 200)
                arParms(4).Value = _vcZipPostalRange

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageShippingRuleStates", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function GetShippingRuleStates() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numRuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _RuleID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetShippingRuleStateList", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function EstimateShipping() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numShipVia", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = ShippingCompanyID

                arParms(2) = New Npgsql.NpgsqlParameter("@numShipService", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = ShippingMethod

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetEstimateShipping", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetShippingMethodForItem(ByVal ddl As System.Web.UI.WebControls.DropDownList, ByVal ShowShippingAmount As Boolean) As String
            Try
                Dim dt As DataTable = EstimateShipping()
                CCommon.AddColumnsToDataTable(dt, "numServiceTypeID1")
                Dim strMessage As String = ""
                Dim decShipingAmount As Decimal
                For Each dr As DataRow In dt.Rows
                    If Sites.ToInteger(dr("intNsoftEnum")) = 0 Then 'Fixed Rate Shipping method
                        decShipingAmount = Sites.ToDecimal(dr("monRate")) / Sites.ToDecimal(HttpContext.Current.Session("ExchangeRate").ToString())
                        dr("numServiceTypeID1") = dr("numServiceTypeID").ToString() + "~" + decShipingAmount.ToString + "~0~0~0~" + CCommon.ToString(dr("vcServiceName"))
                        If ShowShippingAmount Then
                            dr("vcServiceName") = CCommon.ToString(dr("vcServiceName")) & " - " & Sites.ToString(HttpContext.Current.Session("CurrSymbol")) & " " & String.Format("{0:#,##0.00}", decShipingAmount)
                        Else
                            dr("vcServiceName") = CCommon.ToString(dr("vcServiceName"))
                        End If
                    Else ' Get rates from API
                        Dim objShipping As New Shipping()
                        With objShipping
                            .DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                            .WeightInLbs = _Weight
                            .NoOfPackages = 1
                            .UseDimentions = If(Sites.ToInteger(dr("numShippingCompanyID")) = 91, True, False)
                            'for fedex dimentions are must
                            .GroupCount = 1
                            If .UseDimentions Then
                                .Height = _Height
                                .Width = _Width
                                .Length = _Length
                            End If

                            'Get default warehouse address
                            Dim objItem As New CItems()
                            Dim dtTable As DataTable = Nothing
                            objItem.WarehouseID = Sites.ToLong(HttpContext.Current.Session("DefaultWareHouseID"))
                            objItem.DomainID = Sites.ToLong(HttpContext.Current.Session("DomainID"))
                            dtTable = objItem.GetWareHouses()

                            Dim objOppBizDocs As New OppBizDocs()
                            objOppBizDocs.ShipCompany = Sites.ToInteger(dr("numShippingCompanyID"))


                            objOppBizDocs.ShipFromCountry = Sites.ToLong(dtTable.Rows(0)("numWCountry"))
                            objOppBizDocs.ShipFromState = Sites.ToLong(dtTable.Rows(0)("numWState"))
                            objOppBizDocs.ShipToCountry = _Country
                            objOppBizDocs.ShipToState = _State
                            objOppBizDocs.strShipFromCountry = ""
                            objOppBizDocs.strShipFromState = ""
                            objOppBizDocs.strShipToCountry = ""
                            objOppBizDocs.strShipToState = ""
                            objOppBizDocs.GetShippingAbbreviation()


                            .SenderState = objOppBizDocs.strShipFromState
                            .SenderZipCode = Sites.ToString(dtTable.Rows(0)("vcWPinCode"))
                            .SenderCountryCode = objOppBizDocs.strShipFromCountry

                            .RecepientState = objOppBizDocs.strShipToState
                            .RecepientZipCode = _ZipCode
                            .RecepientCountryCode = objOppBizDocs.strShipToCountry

                            .ServiceType = Short.Parse(dr("intNsoftEnum").ToString())
                            .PackagingType = 21
                            'ptYourPackaging 
                            .ItemCode = _ItemID
                            .OppBizDocItemID = 0
                            .ID = 9999
                            .Provider = Sites.ToInteger(dr("numShippingCompanyID"))

                            Dim dtShipRates As DataTable = .GetRates()
                            If Not String.IsNullOrEmpty(.ErrorMsg) Then
                                strMessage = .ErrorMsg
                            End If

                            If dtShipRates.Rows.Count > 0 Then
                                If Sites.ToDecimal(dtShipRates.Rows(0)("Rate")) > 0 Then
                                    'Currency conversion of shipping rate
                                    decShipingAmount = Sites.ToDecimal(dtShipRates.Rows(0)("Rate")) / Sites.ToDecimal(HttpContext.Current.Session("ExchangeRate").ToString())

                                    If Sites.ToBool(dr("bitMarkupType")) = True AndAlso Sites.ToDecimal(dr("fltMarkup")) > 0 Then
                                        dr("fltMarkup") = decShipingAmount / Sites.ToDecimal(dr("fltMarkup"))
                                    End If
                                    'Percentage
                                    decShipingAmount = decShipingAmount + Sites.ToDecimal(dr("fltMarkup"))

                                    dr("numServiceTypeID1") = dr("numServiceTypeID").ToString() + "~" + decShipingAmount.ToString() + "~" + dr("intNsoftEnum").ToString + "~" + dr("numShippingCompanyID").ToString + "~" + dtShipRates.Rows(0)("Date").ToString + "~" + CCommon.ToString(dr("vcServiceName"))

                                    If ShowShippingAmount Then
                                        dr("vcServiceName") = CCommon.ToString(dr("vcServiceName")) & " - " & Sites.ToString(HttpContext.Current.Session("CurrSymbol")) & " " & String.Format("{0:#,##0.00}", decShipingAmount)
                                    End If

                                End If

                            End If
                        End With

                    End If
                Next

                ddl.DataTextField = "vcServiceName"
                ddl.DataValueField = "numServiceTypeID1"
                ddl.DataSource = dt
                ddl.DataBind()
                Return strMessage
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetShippingMethodForItem1() As DataTable
            Try
                Dim strMessage As String = ""
                Dim dt As DataTable = EstimateShipping()

                'For Each dr As DataRow In dt.Rows
                '    Dim decShipingAmount As Decimal
                '    decShipingAmount = Sites.ToDecimal(dr("monRate")) / Sites.ToDecimal(HttpContext.Current.Session("ExchangeRate").ToString())
                '    'dr("numServiceTypeID1") = dr("numServiceTypeID").ToString() + "~" + decShipingAmount.ToString + "~0~0~0~" + CCommon.ToString(dr("vcServiceName"))
                '    If ShowShippingAmount Then
                '        dr("vcServiceName") = CCommon.ToString(dr("vcServiceName")) & " - " & Sites.ToString(HttpContext.Current.Session("CurrSymbol")) & " " & String.Format("{0:#,##0.00}", decShipingAmount)
                '    Else
                '        dr("vcServiceName") = CCommon.ToString(dr("vcServiceName"))
                '    End If
                'Next
                Return dt
            Catch ex As Exception
                Throw ex
            End Try
        End Function


#Region "SHIPPING LABEL CONFIGURATION AREA"

#Region "PROPERTIES FOR SHIPPING LABEL CONFIGURATION"

        Private strOrderStatus As String
        Public Property OrderStatus() As String
            Get
                Return strOrderStatus
            End Get
            Set(ByVal value As String)
                strOrderStatus = value
            End Set
        End Property

        Private lngErrorStatus As Long
        Public Property ErrorStatus() As Long
            Get
                Return lngErrorStatus
            End Get
            Set(ByVal value As Long)
                lngErrorStatus = value
            End Set
        End Property

        Private lngSuccessStatus As Long
        Public Property SuccessStatus() As Long
            Get
                Return lngSuccessStatus
            End Get
            Set(ByVal value As Long)
                lngSuccessStatus = value
            End Set
        End Property

        Private lngReadyToShipStatus As Long
        Public Property ReadyToShipStatus() As Long
            Get
                Return lngReadyToShipStatus
            End Get
            Set(ByVal value As Long)
                lngReadyToShipStatus = value
            End Set
        End Property

        Private _intBizDocType As Long
        Public Property BizDocType() As Long
            Get
                Return _intBizDocType
            End Get
            Set(ByVal value As Long)
                _intBizDocType = value
            End Set
        End Property

#End Region

#Region "SAVE METHOD FOR CONFIGURATION"

        Public Function Save() As Integer
            Dim intResult As Integer
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@vcOrderStatus", strOrderStatus, NpgsqlTypes.NpgsqlDbType.VarChar))

                    .Add(SqlDAL.Add_Parameter("@numErrorStatus", lngErrorStatus, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numSuccessStatus", lngSuccessStatus, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numReadyToShipStatus", lngReadyToShipStatus, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@intMode", 0, NpgsqlTypes.NpgsqlDbType.Integer))

                    .Add(SqlDAL.Add_Parameter("@numBizDocType", _intBizDocType, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                intResult = CCommon.ToInteger(SqlDAL.ExecuteDataset(connString, "USP_MANAGE_ORDER_STATUS", sqlParams.ToArray()))
            Catch ex As Exception
                intResult = Nothing
                Throw ex
            End Try
            Return intResult
        End Function

#End Region

#Region "GET SHIPPING LABEL CONFIGURATION"

        Public Function GetShippingLabelConfiguration() As DataTable
            Dim dtResult As New DataTable
            Try
                Dim ds As New DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@vcOrderStatus", strOrderStatus, NpgsqlTypes.NpgsqlDbType.VarChar))

                    .Add(SqlDAL.Add_Parameter("@numErrorStatus", lngErrorStatus, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numSuccessStatus", lngSuccessStatus, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numReadyToShipStatus", lngReadyToShipStatus, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@intMode", 1, NpgsqlTypes.NpgsqlDbType.Integer))

                    .Add(SqlDAL.Add_Parameter("@numBizDocType", _intBizDocType, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                ds = SqlDAL.ExecuteDataset(connString, "USP_MANAGE_ORDER_STATUS", sqlParams.ToArray())
                dtResult = ds.Tables(0)
            Catch ex As Exception
                dtResult = Nothing
                Throw ex
            End Try

            Return dtResult
        End Function

#End Region

#End Region

#Region "SHIPPING LABEL BATCH GENERATION AREA"

#Region "PUBLIC PROPERTIES"

        Private _ShipBasedOn As Byte
        Public Property ShipBasedOn() As Byte
            Get
                Return _ShipBasedOn
            End Get
            Set(ByVal value As Byte)
                _ShipBasedOn = value
            End Set
        End Property

        Private _SourceCompanyID As Decimal
        Public Property SourceCompanyID() As Decimal
            Get
                Return _SourceCompanyID
            End Get
            Set(ByVal value As Decimal)
                _SourceCompanyID = value
            End Set
        End Property

        Private _SourceShipID As Decimal
        Public Property SourceShipID() As Decimal
            Get
                Return _SourceShipID
            End Get
            Set(ByVal value As Decimal)
                _SourceShipID = value
            End Set
        End Property

        Private _ItemAffected As Integer
        Public Property ItemAffected() As Integer
            Get
                Return _ItemAffected
            End Get
            Set(ByVal value As Integer)
                _ItemAffected = value
            End Set
        End Property

        Private _sType As Short
        Public Property SType() As Short
            Get
                Return _sType
            End Get
            Set(ByVal value As Short)
                _sType = value
            End Set
        End Property

        Private _ChildShipID As Decimal
        Public Property ChildShipID() As Decimal
            Get
                Return _ChildShipID
            End Get
            Set(ByVal value As Decimal)
                _ChildShipID = value
            End Set
        End Property

        Private _dblFromValue As Decimal
        Public Property FromVal() As Decimal
            Get
                Return _dblFromValue
            End Get
            Set(ByVal value As Decimal)
                _dblFromValue = value
            End Set
        End Property

        Private _ToValue As Decimal
        Public Property ToVal() As Decimal
            Get
                Return _ToValue
            End Get
            Set(ByVal value As Decimal)
                _ToValue = value
            End Set
        End Property

        Private _DomesticShipID As Decimal
        Public Property DomesticShipID() As Decimal
            Get
                Return _DomesticShipID
            End Get
            Set(ByVal value As Decimal)
                _DomesticShipID = value
            End Set
        End Property

        Private _InternationalShipID As Decimal
        Public Property InternationalShipID() As Decimal
            Get
                Return _InternationalShipID
            End Get
            Set(ByVal value As Decimal)
                _InternationalShipID = value
            End Set
        End Property

        Private _PackageTypeID As Decimal
        Public Property PackageTypeID() As Decimal
            Get
                Return _PackageTypeID
            End Get
            Set(ByVal value As Decimal)
                _PackageTypeID = value
            End Set
        End Property

        Private _Mode As Byte
        Public Property Mode() As Byte
            Get
                Return _Mode
            End Get
            Set(ByVal value As Byte)
                _Mode = value
            End Set
        End Property

        Private _FromZip As String
        Public Property FromZip() As String
            Get
                Return _FromZip
            End Get
            Set(ByVal value As String)
                _FromZip = value
            End Set
        End Property

        Private _ToZip As String
        Public Property ToZip() As String
            Get
                Return _ToZip
            End Get
            Set(ByVal value As String)
                _ToZip = value
            End Set
        End Property


        Private _strExceptionMsg As String
        Public Property ExceptionMsg() As String
            Get
                Return _strExceptionMsg
            End Get
            Set(ByVal value As String)
                _strExceptionMsg = value
            End Set
        End Property

        Private _lngShipClassID As Long
        Public Property ShipClassID() As Long
            Get
                Return _lngShipClassID
            End Get
            Set(ByVal value As Long)
                _lngShipClassID = value
            End Set
        End Property

#End Region

#Region "METHODS"

        Function GetShippingLabelRuleMaster() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numShippingRuleID", _RuleID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With
                ds = SqlDAL.ExecuteDataset(connString, "USP_GET_SHIPPING_LABEL_RULEMASTER", sqlParams.ToArray())
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ManageShippingLabelRuleMaster() As Integer
            Dim intResult As Integer = 0
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numShippingRuleID", _RuleID, NpgsqlTypes.NpgsqlDbType.BigInt, 9, ParameterDirection.InputOutput))

                    .Add(SqlDAL.Add_Parameter("@vcRuleName", _RuleName, NpgsqlTypes.NpgsqlDbType.VarChar, 100))

                    .Add(SqlDAL.Add_Parameter("@tintShipBasedOn", _ShipBasedOn, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@numSourceCompanyID", _SourceCompanyID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numSourceShipID", _SourceShipID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@intItemAffected", _ItemAffected, NpgsqlTypes.NpgsqlDbType.Integer))

                    .Add(SqlDAL.Add_Parameter("@tintType", _Type, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With

                _strExceptionMsg = ""

                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                intResult = SqlDAL.ExecuteNonQuery(connString, "USP_MANAGE_SHIPPING_LABEL_RULEMASTER", objParam, True)
                _RuleID = CCommon.ToLong(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

            Catch ex As Exception
                intResult = Nothing

                If DirectCast(ex, System.Data.SqlClient.SqlException).Class = 16 AndAlso DirectCast(ex, System.Data.SqlClient.SqlException).State = 1 Then
                    _strExceptionMsg = DirectCast(ex, System.Data.SqlClient.SqlException).Message.ToString()
                Else
                    _strExceptionMsg = ""
                    Throw ex
                End If
            End Try

            Return intResult
        End Function

        Function DeleteShippingLabelRuleMaster() As Integer
            Dim intResult As Integer = 0
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numShippingRuleID", _RuleID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With
                intResult = SqlDAL.ExecuteNonQuery(connString, "USP_DELETE_SHIPPING_LABEL_RULEMASTER", sqlParams.ToArray())

            Catch ex As Exception
                intResult = Nothing
                Throw ex
            End Try

            Return intResult
        End Function

        Function GetShippingLabelRuleChild() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numShippingRuleID", _RuleID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With
                ds = SqlDAL.ExecuteDataset(connString, "USP_GET_SHIPPING_LABEL_RULE_CHILD", sqlParams.ToArray())
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ManageShippingLabelRuleChild() As Integer
            Dim intResult As Integer = 0
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numChildShipID", _ChildShipID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numShippingRuleID", _RuleID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numFromValue", _dblFromValue, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numToValue", _ToValue, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomesticShipID", _DomesticShipID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numInternationalShipID", _InternationalShipID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numPackageTypeID", _PackageTypeID, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With

                intResult = SqlDAL.ExecuteNonQuery(connString, "USP_MANAGE_SHIPPING_LABEL_RULE_CHILD", sqlParams.ToArray())

            Catch ex As Exception
                intResult = Nothing
                Throw ex
            End Try

            Return intResult
        End Function

        Function DeleteShippingLabelRuleChild() As Integer
            Dim intResult As Integer = 0
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numChildShipID", _ChildShipID, NpgsqlTypes.NpgsqlDbType.BigInt, 9, ParameterDirection.InputOutput))

                End With

                intResult = SqlDAL.ExecuteNonQuery(connString, "USP_DELETE_SHIPPING_LABEL_RULE_CHILD", sqlParams.ToArray())

            Catch ex As Exception
                intResult = Nothing
                Throw ex
            End Try

            Return intResult
        End Function

#End Region

#End Region

#Region "PACKAGING RULE AREA"

#Region "PROPERTIES"

        Private _FromQty As Decimal
        Public Property FromQty() As Decimal
            Get
                Return _FromQty
            End Get
            Set(ByVal value As Decimal)
                _FromQty = value
            End Set
        End Property

        Private _ToQty As Decimal
        Public Property ToQty() As Decimal
            Get
                Return _ToQty
            End Get
            Set(ByVal value As Decimal)
                _ToQty = value
            End Set
        End Property

#End Region

#Region "METHODS"

        Function GetPackagingRules() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numPackagingRuleID", _RuleID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With
                ds = SqlDAL.ExecuteDataset(connString, "USP_GET_PACKAGING_RULE", sqlParams.ToArray())
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ManagePackagingRules() As Integer
            Dim intResult As Integer = 0
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numPackagingRuleID", _RuleID, NpgsqlTypes.NpgsqlDbType.BigInt, 9, ParameterDirection.InputOutput))

                    .Add(SqlDAL.Add_Parameter("@vcRuleName", _RuleName, NpgsqlTypes.NpgsqlDbType.VarChar, 1000))

                    .Add(SqlDAL.Add_Parameter("@numPackagingTypeID", _PackageTypeID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numFromQty", _FromQty, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numShipClassId", _lngShipClassID, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    'Added by :Sachin Sadhu||date:31stJuly2014
                    'Purpose :To consolidate Packaging n shipping rules
                    .Add(SqlDAL.Add_Parameter("@numShippingRuleID", _PackagingShipRuleID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    'end code

                End With

                _strExceptionMsg = ""

                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                intResult = SqlDAL.ExecuteNonQuery(connString, "USP_MANAGE_PACKAGING_RULE", objParam, True)
                _RuleID = CCommon.ToLong(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

            Catch ex As Exception
                intResult = Nothing

                If DirectCast(ex, System.Data.SqlClient.SqlException).Class = 16 AndAlso DirectCast(ex, System.Data.SqlClient.SqlException).State = 1 Then
                    _strExceptionMsg = DirectCast(ex, System.Data.SqlClient.SqlException).Message.ToString()
                Else
                    _strExceptionMsg = ""
                    Throw ex
                End If
            End Try
            Return intResult
        End Function

        Function DeletePackagingRules() As Integer
            Dim intResult As Integer
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numPackagingRuleID", _RuleID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With

                intResult = SqlDAL.ExecuteNonQuery(connString, "USP_DELETE_PACKAGING_RULE", sqlParams.ToArray())

            Catch ex As Exception
                intResult = Nothing
                Throw ex
            End Try

            Return intResult
        End Function

        'Added by :Sachin Sadhu||date:31stJuly2014
        'Purpose :To consolidate Packaging n shipping rules
        Function DeletePackagingRulesByShipRuleID() As Integer
            Dim intResult As Integer
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numShippingRuleID", _PackagingShipRuleID, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With

                intResult = SqlDAL.ExecuteNonQuery(connString, "USP_PackagingRules_DeleteAll", sqlParams.ToArray())

            Catch ex As Exception
                intResult = Nothing
                Throw ex
            End Try

            Return intResult
        End Function
        'end of code
#End Region

#End Region

#Region "CUSTOM BOX AREA"

#Region "PROPERTIES"

        Private _CustomPackageID As Decimal
        Public Property CustomPackageID() As Decimal
            Get
                Return _CustomPackageID
            End Get
            Set(ByVal value As Decimal)
                _CustomPackageID = value
            End Set
        End Property

        Private _PackageName As String
        Public Property PackageName() As String
            Get
                Return _PackageName
            End Get
            Set(ByVal value As String)
                _PackageName = value
            End Set
        End Property


        Private _FltWidth As Decimal
        Public Property FltWidth() As Decimal
            Get
                Return _FltWidth
            End Get
            Set(ByVal value As Decimal)
                _FltWidth = value
            End Set
        End Property


        Private _FltHeight As Decimal
        Public Property FltHeight() As Decimal
            Get
                Return _FltHeight
            End Get
            Set(ByVal value As Decimal)
                _FltHeight = value
            End Set
        End Property


        Private _FltLength As Decimal
        Public Property FltLength() As Decimal
            Get
                Return _FltLength
            End Get
            Set(ByVal value As Decimal)
                _FltLength = value
            End Set
        End Property


        Private _FltTotalWeight As Decimal
        Public Property FltTotalWeight() As Decimal
            Get
                Return _FltTotalWeight
            End Get
            Set(ByVal value As Decimal)
                _FltTotalWeight = value
            End Set
        End Property


        Private _ShippingCompanyID As Integer
        Public Property ShippingCompanyID() As Integer
            Get
                Return _ShippingCompanyID
            End Get
            Set(ByVal value As Integer)
                _ShippingCompanyID = value
            End Set
        End Property

#End Region

#Region "METHODS"

        Function GetCustomPackages() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numPackageTypeID", _PackageTypeID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With
                ds = SqlDAL.ExecuteDataset(connString, "USP_GET_CUSTOM_BOXES", sqlParams.ToArray())
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ManageCustomPackages() As Integer
            Dim intResult As Integer = 0
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numCustomPackageID", _CustomPackageID, NpgsqlTypes.NpgsqlDbType.BigInt, 9, ParameterDirection.InputOutput))

                    .Add(SqlDAL.Add_Parameter("@vcPackageName", _PackageName, NpgsqlTypes.NpgsqlDbType.VarChar, 1000))

                    .Add(SqlDAL.Add_Parameter("@fltWidth", _FltWidth, NpgsqlTypes.NpgsqlDbType.Numeric))

                    .Add(SqlDAL.Add_Parameter("@fltHeight", _FltHeight, NpgsqlTypes.NpgsqlDbType.Numeric))

                    .Add(SqlDAL.Add_Parameter("@fltLength", _FltLength, NpgsqlTypes.NpgsqlDbType.Numeric))

                    .Add(SqlDAL.Add_Parameter("@fltTotalWeight", _FltTotalWeight, NpgsqlTypes.NpgsqlDbType.Numeric))

                End With

                _strExceptionMsg = ""

                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                intResult = SqlDAL.ExecuteNonQuery(connString, "USP_MANAGE_CUSTOM_BOX", objParam, True)
                _CustomPackageID = CCommon.ToLong(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

            Catch ex As Exception
                intResult = Nothing

                If DirectCast(ex, System.Data.SqlClient.SqlException).Class = 16 AndAlso DirectCast(ex, System.Data.SqlClient.SqlException).State = 1 Then
                    _strExceptionMsg = DirectCast(ex, System.Data.SqlClient.SqlException).Message.ToString()
                Else
                    _strExceptionMsg = ""
                    Throw ex
                End If
            End Try

            Return intResult

        End Function

        Function DeleteCustomPackages() As Integer
            Dim intResult As Integer
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numPackageTypeID", _PackageTypeID, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With

                intResult = SqlDAL.ExecuteNonQuery(connString, "USP_DELETE_CUSTOM_BOXES", sqlParams.ToArray())

            Catch ex As Exception
                intResult = Nothing
                Throw ex
            End Try

            Return intResult
        End Function

#End Region

#End Region

        Public Function GetShippingRuleWarehouses() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetShippingRuleWarehousesList", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function CreateCloneRule() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numRuleId", _RuleID, NpgsqlTypes.NpgsqlDbType.BigInt, 18, ParameterDirection.InputOutput))

                    .Add(SqlDAL.Add_Parameter("@vcRuleName", RuleName, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@numRelationship", numRelationship, NpgsqlTypes.NpgsqlDbType.BigInt, 18))

                    .Add(SqlDAL.Add_Parameter("@numProfile", numProfile, NpgsqlTypes.NpgsqlDbType.BigInt, 18))

                End With

                Dim objParam() As Object = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "Usp_CreateCloneShippingRule", objParam, True)
                _RuleID = CCommon.ToLong(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

                Return _RuleID
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetShippingRelationshipsProfiles() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetShippingRelationshipsProfiles", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetShippingExceptions() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetShippingExceptions", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function AddDeleteShippingExceptions() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numClassificationID", NpgsqlTypes.NpgsqlDbType.BigInt, 18)
                arParms(1).Value = numClassificationID

                arParms(2) = New Npgsql.NpgsqlParameter("@PercentAbove", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(2).Value = PercentAbove

                arParms(3) = New Npgsql.NpgsqlParameter("@FlatAmt", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(3).Value = FlatAmt

                arParms(4) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = byteMode

                arParms(5) = New Npgsql.NpgsqlParameter("@numShippingExceptionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = numShippingExceptionID

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_AddDelShippingExceptions", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageShippingGlobalValues() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@minShippingCost", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(1).Value = minShippingCost

                arParms(2) = New Npgsql.NpgsqlParameter("@bitEnableStaticShipping", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(2).Value = bitEnableStaticShipping

                arParms(3) = New Npgsql.NpgsqlParameter("@bitEnableShippingExceptions", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Value = bitEnableShippingExceptions

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_ManageShippingGlobalValues", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteShippingStateRule() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numCountryID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = CountryID

                arParms(2) = New Npgsql.NpgsqlParameter("@numStateID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = StateID

                arParms(3) = New Npgsql.NpgsqlParameter("@numRuleId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = RuleID

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_DeleteShippingRuleStateList", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetShippingRule(ByVal zipCode As String, ByVal stateID As Long, ByVal countryID As Long, ByVal divisionID As Integer, ByVal warehouseID As Long, ByVal numSubTotal As Double, ByVal itemCodes As String) As Tuple(Of Boolean, String, Double, DataTable)
            Try
                Dim isShippingRuleAvailable As Boolean = True
                Dim shippingRuleDescription As String = ""
                Dim shippingCharge As Double = 0
                Dim isFreeShipping As Boolean = False
                Dim dtShipExceptions As DataTable

                Dim objOpportunity As New COpportunities
                objOpportunity.DomainID = Me.DomainID
                objOpportunity.DivisionID = divisionID
                objOpportunity.WarehouseID = warehouseID

                Dim dsShippingProm As DataSet = objOpportunity.GetShippingRuleDetails(zipCode, stateID, countryID, siteID:=Me.SiteID)

                If dsShippingProm IsNot Nothing And dsShippingProm.Tables.Count > 0 Then

                    Dim dtShipRule As DataTable = dsShippingProm.Tables(0)
                    Dim dtRates As DataTable = dsShippingProm.Tables(1)

                    If (dtShipRule.Rows.Count > 0) Then
                        If (dtRates.Rows.Count > 0) Then
                            Dim RowCount As Integer = dtRates.Rows.Count

                            If numSubTotal = 0 Then
                                If zipCode <> "" Then
                                    shippingRuleDescription = "Shipping to " & zipCode & " is $" & dtRates.Rows(0)("monRate").ToString() & "."
                                Else
                                    shippingRuleDescription = "Shipping to " & stateID & " is $" & dtRates.Rows(0)("monRate").ToString() & "."
                                End If
                                If (CCommon.ToBool(dtShipRule.Rows(0)("bitFreeShipping")) = True) Then
                                    shippingRuleDescription = shippingRuleDescription + "Orders over $" & dtShipRule.Rows(0)("FreeShippingOrderAmt").ToString & " get free shipping !"
                                End If
                            End If
                            If numSubTotal > 0 Then
                                If (CCommon.ToBool(dtShipRule.Rows(0)("bitFreeShipping")) = True) Then
                                    If (numSubTotal < CCommon.ToDouble(dtShipRule.Rows(0)("FreeShippingOrderAmt"))) Then
                                        shippingRuleDescription = "Orders over $" & dtShipRule.Rows(0)("FreeShippingOrderAmt").ToString & " get free shipping !"
                                        For Each drrow As DataRow In dtRates.Rows
                                            If ((numSubTotal >= CCommon.ToDouble(drrow("intFrom")) And numSubTotal <= CCommon.ToDouble(drrow("intTo"))) And CCommon.ToInteger(drrow("RowNum")) = 1) Then
                                                shippingRuleDescription = shippingRuleDescription + " , order already qualifies for $" & drrow("monRate").ToString() & " shipping."
                                                shippingCharge = CCommon.ToDouble(drrow("monRate"))
                                                isFreeShipping = False
                                            ElseIf (numSubTotal < CCommon.ToDouble(drrow("intFrom"))) Then
                                                shippingRuleDescription = shippingRuleDescription + " or spend $" & (CCommon.ToDouble(drrow("intTo")) - numSubTotal) & " more for $" & drrow("monRate").ToString() & " shipping."
                                            ElseIf (numSubTotal >= CCommon.ToDouble(drrow("intFrom")) And numSubTotal <= CCommon.ToDouble(drrow("intTo"))) Then
                                                shippingRuleDescription = shippingRuleDescription + " , order already qualifies for $" & drrow("monRate").ToString() & " shipping."
                                                shippingCharge = CCommon.ToDouble(drrow("monRate"))
                                                isFreeShipping = False
                                            ElseIf numSubTotal >= CCommon.ToDouble(drrow("intTo")) And CCommon.ToInteger(drrow("RowNum")) = RowCount Then
                                                shippingRuleDescription = "This order has qualified for $" & drrow("monRate").ToString() & " shipping."
                                                shippingCharge = CCommon.ToDouble(drrow("monRate"))
                                                isFreeShipping = False
                                            End If
                                        Next
                                    ElseIf (numSubTotal >= CCommon.ToDouble(dtShipRule.Rows(0)("FreeShippingOrderAmt"))) Then
                                        shippingRuleDescription = "This order has qualified for free shipping ! "
                                        shippingCharge = 0
                                        isFreeShipping = True
                                    End If

                                ElseIf (CCommon.ToBool(dtShipRule.Rows(0)("bitFreeShipping")) = False And numSubTotal > 0) Then
                                    For Each drrow As DataRow In dtRates.Rows
                                        If ((numSubTotal >= CCommon.ToDouble(drrow("intFrom")) And numSubTotal <= CCommon.ToDouble(drrow("intTo"))) And CCommon.ToInteger(drrow("RowNum")) = 1) Then
                                            shippingRuleDescription = "Order already qualifies for $" & drrow("monRate").ToString() & " shipping."
                                            shippingCharge = CCommon.ToDouble(drrow("monRate"))
                                            isFreeShipping = False
                                        ElseIf (numSubTotal < CCommon.ToDouble(drrow("intFrom"))) Then
                                            shippingRuleDescription = shippingRuleDescription + " or spend $" & (CCommon.ToDouble(drrow("intTo")) - numSubTotal) & " more for $" & drrow("monRate").ToString() & " shipping."
                                        ElseIf (numSubTotal >= CCommon.ToDouble(drrow("intFrom")) And numSubTotal <= CCommon.ToDouble(drrow("intTo"))) Then
                                            shippingRuleDescription = shippingRuleDescription + " Order already qualifies for $" & drrow("monRate").ToString() & " shipping."
                                            shippingCharge = CCommon.ToDouble(drrow("monRate"))
                                            isFreeShipping = False
                                        ElseIf numSubTotal >= CCommon.ToDouble(drrow("intTo")) And CCommon.ToInteger(drrow("RowNum")) = RowCount Then
                                            shippingRuleDescription = "This order has qualified for $" & drrow("monRate").ToString() & " shipping."
                                            shippingCharge = CCommon.ToDouble(drrow("monRate"))
                                            isFreeShipping = False
                                        End If
                                    Next
                                End If
                            End If
                        ElseIf CCommon.ToBool(dtShipRule.Rows(0)("bitFreeShipping")) AndAlso numSubTotal >= CCommon.ToDouble(dtShipRule.Rows(0)("FreeShippingOrderAmt")) Then
                            shippingRuleDescription = "This order has qualified for free shipping ! "
                            shippingCharge = 0
                            isFreeShipping = True
                        Else
                            isShippingRuleAvailable = False
                        End If
                    Else
                        isShippingRuleAvailable = False
                    End If

                    If itemCodes.Length > 0 Then
                        Dim objOpp As New COpportunities
                        objOpp.DomainID = DomainID
                        dtShipExceptions = objOpp.GetShippingExceptionsForOrder(itemCodes, shippingCharge, isFreeShipping)

                        If Not dtShipExceptions Is Nothing AndAlso dtShipExceptions.Rows.Count > 0 Then
                            For Each dr As DataRow In dtShipExceptions.Rows
                                shippingCharge = shippingCharge + CCommon.ToDouble(dr("FlatAmt"))
                            Next
                        End If
                    End If
                Else
                    isShippingRuleAvailable = False
                End If

                Return New Tuple(Of Boolean, String, Double, DataTable)(isShippingRuleAvailable, shippingRuleDescription, shippingCharge, dtShipExceptions)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetShippingMethodForEcommerce() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_GetEstimateShippingEcommerce", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

End Namespace
