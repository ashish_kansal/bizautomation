﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports GCheckout.Checkout
Imports GCheckout.Util
Imports BACRM.BusinessLogic.Common
Imports System.Web
Imports BACRM.BusinessLogic.Admin

Namespace BACRM.BusinessLogic.ShioppingCart
    Public Class GoogleCheckout
        Inherits BACRM.BusinessLogic.CBusinessBase
        Public Function GoogleCheckoutRequest(ByVal MerchantID As String, ByVal MerchantKey As String, ByVal IsSandbox As Boolean, ByVal OppID As String, ByVal DomainID As Double, ByVal strQueryString As String, ByVal Tax As Decimal, ByVal dsCart As DataSet) As String
            Try
                Dim Req As New CheckoutShoppingCartRequest(MerchantID, MerchantKey, If(IsSandbox, GCheckout.EnvironmentType.Sandbox, GCheckout.EnvironmentType.Production), "USD", 20)

                Dim decShippingCharges As Decimal = 0


                'Add Opportunity ID in a Merchant Private Data Node to update Status at the time of Notification API Call
                Dim doc As New System.Xml.XmlDocument()
                doc.LoadXml("<root />")
                Dim element As System.Xml.XmlElement = doc.CreateElement("OppID")
                element.InnerText = OppID
                doc.DocumentElement.AppendChild(element)

                element = doc.CreateElement("DomainID")
                element.InnerText = DomainID
                doc.DocumentElement.AppendChild(element)

                Req.AddMerchantPrivateDataNode(doc.DocumentElement)


                If dsCart IsNot Nothing Then
                    If dsCart.Tables(0).Rows.Count > 0 Then
                        For Each dRow As DataRow In dsCart.Tables(0).Rows

                            'Google Shopping Cart
                            Dim si As New ShoppingCartItem()

                            ' Due to this i cant get Shipping rate in google check out . So Be careful .
                            'si.DigitalContent = new DigitalItem("Digital Item Key", "Digital Item Description");
                            'si.MerchantItemID = "Merchant Item ID";

                            '--> Use of Merchant Item Private Data ...

                            'Add Item
                            si.Name = CCommon.ToString(dRow("vcItemName"))
                            si.Description = "Description"
                            si.Price = CCommon.ToDecimal(dRow("monTotAmtBefDiscount"))
                            si.Quantity = CCommon.ToInteger(dRow("numUnitHour"))

                            Req.AddItem(si)

                            'Add shipping Charges .
                            'If CCommon.ToDecimal(dRow("decShippingCharge")) > 0 Then
                            '    decShippingCharges = decShippingCharges + CCommon.ToDecimal(dRow("decShippingCharge"))
                            'End If

                            'If Item has a discount then add virtual Item to show discount .
                            If CCommon.ToDouble(dRow("fltDiscount")) > 0 Then
                                Dim siDiscount As New ShoppingCartItem()
                                siDiscount.Name = "Cart Discount"
                                siDiscount.Description = "A virtual Item to reflact discount total."
                                siDiscount.Price = CCommon.ToDecimal("- " + CCommon.ToString(dRow("fltDiscount")))
                                siDiscount.Quantity = 1
                                Req.AddItem(siDiscount)
                            End If

                        Next

                    End If
                End If

                If strQueryString <> "" Then
                    Req.ContinueShoppingUrl = "http://" + HttpContext.Current.Request.Url.Host + "/ThankYou.aspx?" + strQueryString
                End If

                'Tax Calculations ...
                If True Then
                    Dim siTax As New ShoppingCartItem()
                    siTax.Name = "[Sales Tax]"
                    siTax.Description = ""
                    siTax.Price = Tax
                    siTax.Quantity = 1
                    Req.AddItem(siTax)
                End If

                'Shipping Charges ...
                'If decShippingCharges > 0 Then
                '    Req.AddFlatRateShippingMethod("Shipping Charge", decShippingCharges)
                'Else
                '    Req.AddFlatRateShippingMethod("Shipping Charge", 0)
                'End If

                'Finally Send whole cart with all settings to Google Checkout for Palacing order...
                Dim Resp As GCheckoutResponse = Req.Send()
                If Resp.IsGood Then
                    Return Resp.RedirectUrl
                Else
                    Return ""
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        ' It checks whether given Merchant Id and Merchant Key is Valid or Not
        Public Function CheckGoogleCheckoutConfiguration(ByVal GoogleMerchantID As String, ByVal GoogleMerchantKey As String, ByVal IsSandbox As Boolean) As Boolean
            Try
                If GoogleMerchantID <> "" AndAlso GoogleMerchantKey <> "" Then
                    Dim Req As CheckoutShoppingCartRequest
                    Req = New CheckoutShoppingCartRequest(GoogleMerchantID, GoogleMerchantKey, If(IsSandbox, GCheckout.EnvironmentType.Sandbox, GCheckout.EnvironmentType.Production), "USD", 20)
                    Req.AddItem("Mars bar", "Packed with peanuts", 0.75D, 2)
                    Dim Resp As GCheckoutResponse = Req.Send()
                    If Resp.IsGood Then
                        Return True
                    End If
                End If
                Return False
            Catch ex As Exception
                Return False
            End Try
        End Function
    End Class
End Namespace

