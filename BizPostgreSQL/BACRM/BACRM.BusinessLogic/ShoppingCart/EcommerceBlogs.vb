﻿Imports BACRM.BusinessLogic.Item
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRM.BusinessLogic.Common
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.ShioppingCart

    Public Class EcommerceBlogs
        Inherits BACRM.BusinessLogic.CBusinessBase
        Private _numBlogsID As Long
        Public Property numBlogsID As Long
            Get
                Return _numBlogsID
            End Get
            Set(ByVal value As Long)
                _numBlogsID = value
            End Set
        End Property

        Private _numSiteId As Long
        Public Property numSiteId As Long
            Get
                Return _numSiteId
            End Get
            Set(ByVal value As Long)
                _numSiteId = value
            End Set
        End Property

        Private _numDomainID As Long
        Public Property numDomainID As Long
            Get
                Return _numDomainID
            End Get
            Set(ByVal value As Long)
                _numDomainID = value
            End Set
        End Property

        Private _intMode As Integer
        Public Property intMode As Integer
            Get
                Return _intMode
            End Get
            Set(ByVal value As Integer)
                _intMode = value
            End Set
        End Property

        Private _vcContent As String
        Public Property vcContent As String
            Get
                Return _vcContent
            End Get
            Set(ByVal value As String)
                _vcContent = value
            End Set
        End Property

        Private _vcTitle As String
        Public Property vcTitle As String
            Get
                Return _vcTitle
            End Get
            Set(ByVal value As String)
                _vcTitle = value
            End Set
        End Property

        Private _vcUrl As String
        Public Property vcUrl As String
            Get
                Return _vcUrl
            End Get
            Set(ByVal value As String)
                _vcUrl = value
            End Set
        End Property

        Private _output As String
        Public Property output As String
            Get
                Return _output
            End Get
            Set(ByVal value As String)
                _output = value
            End Set
        End Property

        Public Function GetEcommerceBlogs() As DataTable
            Try
                Dim arParms() As SqlParameter = New SqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New SqlParameter("@numBlogsID", SqlDbType.BigInt)
                arParms(0).Value = numBlogsID

                arParms(1) = New SqlParameter("@numSiteId", SqlDbType.VarChar)
                arParms(1).Value = numSiteId

                arParms(2) = New SqlParameter("@numDomainId", SqlDbType.BigInt)
                arParms(2).Value = numDomainID

                arParms(3) = New SqlParameter("@intMode", SqlDbType.Int)
                arParms(3).Value = intMode

                Return SqlDAL.ExecuteDataset(connString, "USP_GetEcommerceBlogs", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageEcommerceBlogs() As DataTable
            Try
                Dim arParms() As SqlParameter = New SqlParameter(6) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New SqlParameter("@numBlogsID", SqlDbType.BigInt)
                arParms(0).Value = numBlogsID

                arParms(1) = New SqlParameter("@numSiteId", SqlDbType.VarChar)
                arParms(1).Value = numSiteId

                arParms(2) = New SqlParameter("@numDomainId", SqlDbType.BigInt)
                arParms(2).Value = numDomainID

                arParms(2) = New SqlParameter("@vcContent", SqlDbType.VarChar, 5000000)
                arParms(2).Value = vcContent

                arParms(3) = New SqlParameter("@vcContent", SqlDbType.VarChar, 5000000)
                arParms(3).Value = vcContent

                arParms(4) = New SqlParameter("@vcUrl", SqlDbType.VarChar, 50000)
                arParms(4).Value = vcUrl

                arParms(5) = New SqlParameter("@intMode", SqlDbType.Int)
                arParms(5).Value = intMode

                arParms(6) = New SqlParameter("@output", SqlDbType.VarChar, 50)
                arParms(6).Direction = ParameterDirection.InputOutput
                arParms(6).Value = output

                Return SqlDAL.ExecuteDataset(connString, "USP_ManageEcommerceBlogs", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


    End Class
End Namespace
