﻿Imports BACRM.BusinessLogic.Item
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRM.BusinessLogic.Common
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.ShioppingCart
    Public Class CartItemsFilters
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _FieldID As Decimal
        Public Property FieldID() As Decimal
            Get
                Return _FieldID
            End Get
            Set(ByVal value As Decimal)
                _FieldID = value
            End Set
        End Property

        Private _FormID As Decimal
        Public Property FormID() As Decimal
            Get
                Return _FormID
            End Get
            Set(ByVal value As Decimal)
                _FormID = value
            End Set
        End Property

        Private _FilterType As Integer
        Public Property FilterType() As Integer
            Get
                Return _FilterType
            End Get
            Set(ByVal value As Integer)
                _FilterType = value
            End Set
        End Property

        Private _CustomField As Boolean
        Public Property CustomField() As Boolean
            Get
                Return _CustomField
            End Get
            Set(ByVal value As Boolean)
                _CustomField = value
            End Set
        End Property

        Private _Order As Byte
        Public Property Order() As Byte
            Get
                Return _Order
            End Get
            Set(ByVal value As Byte)
                _Order = value
            End Set
        End Property

        Private _SiteID As Decimal
        Public Property SiteID() As Decimal
            Get
                Return _SiteID
            End Get
            Set(ByVal value As Decimal)
                _SiteID = value
            End Set
        End Property

        Public Function GetDycCartFilters() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numFormID", _FormID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numSiteID", _SiteID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetDycCartFilter", sqlParams.ToArray())
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageDycCartFilters() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numFieldID", _FieldID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numFormID", _FormID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numSiteID", _SiteID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numFilterType", _FilterType, NpgsqlTypes.NpgsqlDbType.Integer))

                    .Add(SqlDAL.Add_Parameter("@bitCustomField", _CustomField, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@tintOrder", _Order, NpgsqlTypes.NpgsqlDbType.Smallint))

                End With

                Dim lngResult As Long
                lngResult = SqlDAL.ExecuteNonQuery(connString, "USP_ManageDycCartFilter", sqlParams.ToArray())
                Return lngResult

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteDycCartFilters() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numFieldID", _FieldID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@numFormID", _FormID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@numSiteID", _SiteID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With

                Dim lngResult As Long
                lngResult = SqlDAL.ExecuteNonQuery(connString, "USP_DeleteDycCartFilter", sqlParams.ToArray())
                Return lngResult

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        
    End Class
End Namespace