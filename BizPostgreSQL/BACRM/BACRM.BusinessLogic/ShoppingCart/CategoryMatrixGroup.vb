﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports BACRMAPI.DataAccessLayer
Imports BACRM.BusinessLogic.Common

Namespace BACRM.BusinessLogic.ShioppingCart
    Public Class CategoryMatrixGroup
        Inherits CBusinessBase

#Region "Public Properties"

        Public Property CategoryID As Long
        Public Property ItemGroupID As Long

#End Region

#Region "Public Methods"

        Public Function GetByCategoryID() As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numCategoryID", CategoryID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_CategoryMatrixGroup_GetByCategoryID", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function


#End Region

    End Class
End Namespace