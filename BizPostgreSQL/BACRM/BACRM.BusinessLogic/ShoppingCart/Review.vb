﻿Imports BACRM.BusinessLogic.Item
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRM.BusinessLogic.Common
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer


Namespace BACRM.BusinessLogic.ShioppingCart
    Public Class Review
        Inherits CBusinessBase
#Region "Properties"

#Region "Review"
        Private _ReviewId As Long
        Public Property ReviewId As Long
            Get
                Return _ReviewId
            End Get
            Set(ByVal value As Long)
                _ReviewId = value
            End Set
        End Property
        Private _ReviewTitle As String
        Public Property ReviewTitle As String
            Get
                Return _ReviewTitle
            End Get
            Set(ByVal value As String)
                _ReviewTitle = value
            End Set
        End Property
        Private _ReviewComment As String
        Public Property ReviewComment As String
            Get
                Return _ReviewComment
            End Get
            Set(ByVal value As String)
                _ReviewComment = value
            End Set
        End Property
        Private _EmailMe As Boolean
        Public Property EmailMe As Boolean
            Get
                Return _EmailMe
            End Get
            Set(ByVal value As Boolean)
                _EmailMe = value
            End Set
        End Property
      
        Private _Hide As Boolean
        Public Property Hide As Boolean
            Get
                Return _Hide
            End Get
            Set(ByVal value As Boolean)
                _Hide = value
            End Set
        End Property
        Private _Approved As Boolean
        Public Property Approved As Boolean
            Get
                Return _Approved
            End Get
            Set(ByVal value As Boolean)
                _Approved = value
            End Set
        End Property
      
#End Region
#Region "Rating"
        Private _RatingId As Long
        Public Property RatingId As Long
            Get
                Return _RatingId
            End Get
            Set(ByVal value As Long)
                _RatingId = value
            End Set
        End Property
        Private _RatingCount As Integer
        Public Property RatingCount As Integer
            Get
                Return _RatingCount
            End Get
            Set(ByVal value As Integer)
                _RatingCount = value
            End Set
        End Property

#End Region
#Region "ReviewDetail"
        Private _ReviewDetailId As Long
        Public Property ReviewDetailId As Long
            Get
                Return _ReviewDetailId
            End Get
            Set(ByVal value As Long)
                _ReviewDetailId = value
            End Set
        End Property
      
        Private _Abuse As Boolean
        Public Property Abuse As Boolean
            Get
                Return _Abuse
            End Get
            Set(ByVal value As Boolean)
                _Abuse = value
            End Set
        End Property

        Private _Helpful As Boolean
        Public Property Helpful As Boolean
            Get
                Return _Helpful
            End Get
            Set(ByVal value As Boolean)
                _Helpful = value
            End Set
        End Property
#End Region
#Region "Common"
        Private _TypeId As Integer
        Public Property TypeId As Integer
            Get
                Return _TypeId
            End Get
            Set(ByVal value As Integer)
                _TypeId = value
            End Set
        End Property
        Private _ReferenceId As String
        Public Property ReferenceId As String
            Get
                Return _ReferenceId
            End Get
            Set(ByVal value As String)
                _ReferenceId = value
            End Set
        End Property
        Private _IpAddress As String
        Public Property IpAddress As String
            Get
                Return _IpAddress
            End Get
            Set(ByVal value As String)
                _IpAddress = value
            End Set
        End Property
        Private _ContactId As Long
        Public Property ContactId As Long
            Get
                Return _ContactId
            End Get
            Set(ByVal value As Long)
                _ContactId = value
            End Set
        End Property
        Private _SiteId As Long
        Public Property SiteId As Long
            Get
                Return _SiteId
            End Get
            Set(ByVal value As Long)
                _SiteId = value
            End Set
        End Property
        Private _CreatedDate As String
        Public Property CreatedDate As String
            Get
                Return _CreatedDate
            End Get
            Set(ByVal value As String)
                _CreatedDate = value
            End Set
        End Property
        Private _Mode As Integer
        Public Property Mode As Integer
            Get
                Return _Mode
            End Get
            Set(ByVal value As Integer)
                _Mode = value
            End Set
        End Property
        Private _SearchMode As Integer
        Public Property SearchMode As Integer
            Get
                Return _SearchMode
            End Get
            Set(ByVal value As Integer)
                _SearchMode = value
            End Set
        End Property

#End Region

#End Region


#Region "Methods"
#Region "Review"

        Public Function ManageReview() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(12) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numReviewId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = ReviewId

                arParms(1) = New Npgsql.NpgsqlParameter("@tintTypeId", NpgsqlTypes.NpgsqlDbType.SmallInt)
                arParms(1).Value = TypeId

                arParms(2) = New Npgsql.NpgsqlParameter("@vcReferenceId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = ReferenceId

                arParms(3) = New Npgsql.NpgsqlParameter("@vcReviewTitle", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = ReviewTitle

                arParms(4) = New Npgsql.NpgsqlParameter("@vcReviewComment", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(4).Value = ReviewComment


                arParms(5) = New Npgsql.NpgsqlParameter("@bitEmailMe", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(5).Value = EmailMe

                arParms(6) = New Npgsql.NpgsqlParameter("@vcIpAddress", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(6).Value = IpAddress

                arParms(7) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = ContactId

                arParms(8) = New Npgsql.NpgsqlParameter("@numSiteId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = SiteId

                arParms(9) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = DomainID

                arParms(10) = New Npgsql.NpgsqlParameter("@vcCreatedDate", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(10).Value = CreatedDate


                arParms(11) = New Npgsql.NpgsqlParameter("@bitHide", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(11).Value = Hide

                arParms(12) = New Npgsql.NpgsqlParameter("@bitApproved", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(12).Value = Approved

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageReview", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetReviews() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numReviewId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = ReviewId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numSiteId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = SiteId

                arParms(3) = New Npgsql.NpgsqlParameter("@vcReferenceId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = ReferenceId

                arParms(4) = New Npgsql.NpgsqlParameter("@tintTypeId", NpgsqlTypes.NpgsqlDbType.SmallInt)
                arParms(4).Value = TypeId

                arParms(5) = New Npgsql.NpgsqlParameter("@Mode", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = Mode

                arParms(6) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = ContactId

                arParms(7) = New Npgsql.NpgsqlParameter("@SearchMode", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(7).Value = SearchMode

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetReviews", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function DeleteReview() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numReviewId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = ReviewId

                Return SqlDAL.ExecuteNonQuery(connString, "USP_DeleteReview", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
#End Region

#Region "ReviewDetail"
        Public Function GetReviewDetail() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numReviewDetailId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = ReviewId

                arParms(1) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = ContactId

                arParms(2) = New Npgsql.NpgsqlParameter("@Mode", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = Mode

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetReviewDetail", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageReviewDetail() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numReviewId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = ReviewId

                arParms(1) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = ContactId

                arParms(2) = New Npgsql.NpgsqlParameter("@bitAbuse", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(2).Value = Abuse

                arParms(3) = New Npgsql.NpgsqlParameter("@bitHelpful", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Value = Helpful

                arParms(4) = New Npgsql.NpgsqlParameter("@Mode", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = Mode


                SqlDAL.ExecuteNonQuery(connString, "USP_ManageReviewDetail", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

#End Region

#Region "Rating"
        Public Function ManageRatings() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numRatingId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = RatingId

                arParms(1) = New Npgsql.NpgsqlParameter("@tintTypeId", NpgsqlTypes.NpgsqlDbType.SmallInt)
                arParms(1).Value = TypeId

                arParms(2) = New Npgsql.NpgsqlParameter("@vcReferenceId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = ReferenceId

                arParms(3) = New Npgsql.NpgsqlParameter("@intRatingCount", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = RatingCount

                arParms(4) = New Npgsql.NpgsqlParameter("@vcIpAddress", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(4).Value = IpAddress

                arParms(5) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = ContactId

                arParms(6) = New Npgsql.NpgsqlParameter("@numSiteId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = SiteId

                arParms(7) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = DomainID

                arParms(8) = New Npgsql.NpgsqlParameter("@vcCreatedDate", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(8).Value = CreatedDate

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageRatings", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetRatings() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@tintTypeId", NpgsqlTypes.NpgsqlDbType.SmallInt)
                arParms(0).Value = TypeId

                arParms(1) = New Npgsql.NpgsqlParameter("@vcReferenceId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(1).Value = ReferenceId

                arParms(2) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = ContactId

                arParms(3) = New Npgsql.NpgsqlParameter("@numSiteId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = SiteId

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = DomainID

                arParms(5) = New Npgsql.NpgsqlParameter("@Mode", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = Mode

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetRatings", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
#End Region

#End Region


    End Class
End Namespace

