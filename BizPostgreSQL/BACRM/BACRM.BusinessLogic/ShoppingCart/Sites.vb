﻿'Created by Chintan Prajapati
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Net
Imports System.IO
Imports System.Web.UI
Imports System.Web
Imports System.Configuration
Imports BACRM.BusinessLogic.Item

Imports System.Text
'Imports System.IO
Imports System.Web.Mail
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Leads
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports BACRM.BusinessLogic.Tracking
Imports System.Text.RegularExpressions
Imports BACRM.BusinessLogic.Common
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.ShioppingCart
    Public Class Sites
        Inherits BACRM.BusinessLogic.CBusinessBase


        'Private DomainId As Long
        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal value As Long)
        '        DomainId = value
        '    End Set
        'End Property
        Private _SiteID As Long
        Public Property SiteID() As Long
            Get
                Return _SiteID
            End Get
            Set(ByVal value As Long)
                _SiteID = value
            End Set
        End Property
        Private _SiteName As String
        Public Property SiteName() As String
            Get
                Return _SiteName
            End Get
            Set(ByVal value As String)
                _SiteName = value
            End Set
        End Property
        Private _SiteDesc As String
        Public Property SiteDesc() As String
            Get
                Return _SiteDesc
            End Get
            Set(ByVal value As String)
                _SiteDesc = value
            End Set
        End Property
        Private _HostName As String
        Public Property HostName() As String
            Get
                Return _HostName
            End Get
            Set(ByVal value As String)
                _HostName = value
            End Set
        End Property

        Private _LiveURL As String
        Public Property LiveURL() As String
            Get
                Return _LiveURL
            End Get
            Set(ByVal value As String)
                _LiveURL = value
            End Set
        End Property

        Private _IsActive As Boolean
        Public Property IsActive() As Boolean
            Get
                Return _IsActive
            End Get
            Set(ByVal value As Boolean)
                _IsActive = value
            End Set
        End Property
        Private _IsMaintainScroll As Boolean
        Public Property IsMaintainScroll() As Boolean
            Get
                Return _IsMaintainScroll
            End Get
            Set(ByVal value As Boolean)
                _IsMaintainScroll = value
            End Set
        End Property
        Private _CssID As Long
        Public Property CssID() As Long
            Get
                Return _CssID
            End Get
            Set(ByVal value As Long)
                _CssID = value
            End Set
        End Property
        Private _StyleName As String
        Public Property StyleName() As String
            Get
                Return _StyleName
            End Get
            Set(ByVal value As String)
                _StyleName = value
            End Set
        End Property
        Private _StyleFile As String
        Public Property StyleFile() As String
            Get
                Return _StyleFile
            End Get
            Set(ByVal value As String)
                _StyleFile = value
            End Set
        End Property
        'Private UserCntID As Long
        'Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal value As Long)
        '        UserCntID = value
        '    End Set
        'End Property
        Private _TemplateID As Long
        Public Property TemplateID() As Long
            Get
                Return _TemplateID
            End Get
            Set(ByVal value As Long)
                _TemplateID = value
            End Set
        End Property
        Private _TemplateName As String
        Public Property TemplateName() As String
            Get
                Return _TemplateName
            End Get
            Set(ByVal value As String)
                _TemplateName = value
            End Set
        End Property
        Private _TemplateContent As String
        Public Property TemplateContent() As String
            Get
                Return _TemplateContent
            End Get
            Set(ByVal value As String)
                _TemplateContent = value
            End Set
        End Property
        Private _PageID As Long
        Public Property PageID() As Long
            Get
                Return _PageID
            End Get
            Set(ByVal value As Long)
                _PageID = value
            End Set
        End Property
        Private _PageName As String
        Public Property PageName() As String
            Get
                Return _PageName
            End Get
            Set(ByVal value As String)
                _PageName = value
            End Set
        End Property
        Private _PageURL As String
        Public Property PageURL() As String
            Get
                Return _PageURL
            End Get
            Set(ByVal value As String)
                _PageURL = value
            End Set
        End Property
        Private _PageType As Short
        Public Property PageType() As Short
            Get
                Return _PageType
            End Get
            Set(ByVal value As Short)
                _PageType = value
            End Set
        End Property
        Private _PageTitle As String
        Public Property PageTitle() As String
            Get
                Return _PageTitle
            End Get
            Set(ByVal value As String)
                _PageTitle = value
            End Set
        End Property
        Private _StyleType As Short
        Public Property StyleType() As Short
            Get
                Return _StyleType
            End Get
            Set(ByVal value As Short)
                _StyleType = value
            End Set
        End Property
        Private _MetaID As Long
        Public Property MetaID() As Long
            Get
                Return _MetaID
            End Get
            Set(ByVal value As Long)
                _MetaID = value
            End Set
        End Property
        Private _MetaTags As String
        Public Property MetaTags() As String
            Get
                Return _MetaTags
            End Get
            Set(ByVal value As String)
                _MetaTags = value
            End Set
        End Property
        Private _MetaKeywords As String
        Public Property MetaKeywords() As String
            Get
                Return _MetaKeywords
            End Get
            Set(ByVal value As String)
                _MetaKeywords = value
            End Set
        End Property
        Private _MetaDescription As String
        Public Property MetaDescription() As String
            Get
                Return _MetaDescription
            End Get
            Set(ByVal value As String)
                _MetaDescription = value
            End Set
        End Property
        Private _MetaTagFor As Short
        Public Property MetaTagFor() As Short
            Get
                Return _MetaTagFor
            End Get
            Set(ByVal value As Short)
                _MetaTagFor = value
            End Set
        End Property
        Private _ReferenceID As Long
        Public Property ReferenceID() As Long
            Get
                Return _ReferenceID
            End Get
            Set(ByVal value As Long)
                _ReferenceID = value
            End Set
        End Property
        Private _StrItems As String
        Public Property StrItems() As String
            Get
                Return _StrItems
            End Get
            Set(ByVal value As String)
                _StrItems = value
            End Set
        End Property
        Private _ElementID As Long
        Public Property ElementID() As Long
            Get
                Return _ElementID
            End Get
            Set(ByVal value As Long)
                _ElementID = value
            End Set
        End Property
        Private _MenuID As Long
        Public Property MenuID() As Long
            Get
                Return _MenuID
            End Get
            Set(ByVal value As Long)
                _MenuID = value
            End Set
        End Property
        Private _MenuTitle As String
        Public Property MenuTitle() As String
            Get
                Return _MenuTitle
            End Get
            Set(ByVal value As String)
                _MenuTitle = value
            End Set
        End Property
        Private _NavigationURL As String
        Public Property NavigationURL() As String
            Get
                Return _NavigationURL
            End Get
            Set(ByVal value As String)
                _NavigationURL = value
            End Set
        End Property
        Private _Level As Short
        Public Property Level() As Short
            Get
                Return _Level
            End Get
            Set(ByVal value As Short)
                _Level = value
            End Set
        End Property
        Private _ParentMenuID As Long
        Public Property ParentMenuID() As Long
            Get
                Return _ParentMenuID
            End Get
            Set(ByVal value As Long)
                _ParentMenuID = value
            End Set
        End Property
        Private _Status As Boolean
        Public Property Status() As Boolean
            Get
                Return _Status
            End Get
            Set(ByVal value As Boolean)
                _Status = value
            End Set
        End Property
        Private _Email As String
        Public Property Email() As String
            Get
                Return _Email
            End Get
            Set(ByVal value As String)
                _Email = value
            End Set
        End Property
        Private _IsSubscribe As Boolean
        Public Property IsSubscribe() As Boolean
            Get
                Return _IsSubscribe
            End Get
            Set(ByVal value As Boolean)
                _IsSubscribe = value
            End Set
        End Property
        Private _BreadCrumbID As Long
        Public Property BreadCrumbID() As Long
            Get
                Return _BreadCrumbID
            End Get
            Set(ByVal value As Long)
                _BreadCrumbID = value
            End Set
        End Property

        Private _DisplayName As String
        Public Property DisplayName() As String
            Get
                Return _DisplayName
            End Get
            Set(ByVal value As String)
                _DisplayName = value
            End Set
        End Property


        Private _ContactID As Long
        Public Property ContactID() As Long
            Get
                Return _ContactID
            End Get
            Set(ByVal value As Long)
                _ContactID = value
            End Set
        End Property

        Private _CurrencyID As Long
        Public Property CurrencyID() As Long
            Get
                Return _CurrencyID
            End Get
            Set(ByVal value As Long)
                _CurrencyID = value
            End Set
        End Property

        Private _vcElementName As String
        Public Property vcElementName As String
            Get
                Return _vcElementName
            End Get
            Set(ByVal value As String)
                _vcElementName = value
            End Set
        End Property
        Private _vcUserControlPath As String
        Public Property vcUserControlPath As String
            Get
                Return _vcUserControlPath
            End Get
            Set(ByVal value As String)
                _vcUserControlPath = value
            End Set
        End Property

        Private _vcTagName As String
        Public Property vcTagName As String
            Get
                Return _vcTagName
            End Get
            Set(ByVal value As String)
                _vcTagName = value
            End Set
        End Property
        Private _bitCustomization As Boolean
        Public Property bitCustomization As Boolean
            Get
                Return _bitCustomization
            End Get
            Set(ByVal value As Boolean)
                _bitCustomization = value
            End Set
        End Property
        Private _bitAdd As Boolean
        Public Property bitAdd As Boolean
            Get
                Return _bitAdd
            End Get
            Set(ByVal value As Boolean)
                _bitAdd = value
            End Set
        End Property
        Private _bitDelete As Boolean
        Public Property bitDelete As Boolean
            Get
                Return _bitDelete
            End Get
            Set(ByVal value As Boolean)
                _bitDelete = value
            End Set
        End Property
        Private _bitFlag As Boolean
        Public Property bitFlag As Boolean
            Get
                Return _bitFlag
            End Get
            Set(ByVal value As Boolean)
                _bitFlag = value
            End Set
        End Property
        Private _numElementId As Long
        Public Property numElementId As Long
            Get
                Return _numElementId
            End Get
            Set(ByVal value As Long)
                _numElementId = value
            End Set
        End Property

        Private _IsOnePageCheckout As Boolean
        Public Property IsOnePageCheckout() As Boolean
            Get
                Return _IsOnePageCheckout
            End Get
            Set(ByVal value As Boolean)
                _IsOnePageCheckout = value
            End Set
        End Property
        Private _RateType As Integer
        Public Property RateType() As Integer
            Get
                Return _RateType
            End Get
            Set(ByVal value As Integer)
                _RateType = value
            End Set
        End Property
        Private _ListOfCategories As String
        Public Property ListOfCategories() As String
            Get
                Return _ListOfCategories
            End Get
            Set(ByVal value As String)
                _ListOfCategories = value
            End Set
        End Property
        Private _Mode As Integer
        Public Property Mode() As Integer
            Get
                Return _Mode
            End Get
            Set(ByVal value As Integer)
                _Mode = value
            End Set
        End Property


        Private _RedirectConfigID As Long
        Public Property RedirectConfigID() As Long
            Get
                Return _RedirectConfigID
            End Get
            Set(ByVal value As Long)
                _RedirectConfigID = value
            End Set
        End Property

        Private _ReferenceType As Long
        Public Property ReferenceType() As Long
            Get
                Return _ReferenceType
            End Get
            Set(ByVal value As Long)
                _ReferenceType = value
            End Set
        End Property

        Private _RedirectType As Long
        Public Property RedirectType() As Long
            Get
                Return _RedirectType
            End Get
            Set(ByVal value As Long)
                _RedirectType = value
            End Set
        End Property

        Private _OldUrl As String
        Public Property OldUrl() As String
            Get
                Return _OldUrl
            End Get
            Set(ByVal value As String)
                _OldUrl = value
            End Set
        End Property

        Private _NewUrl As String
        Public Property NewUrl() As String
            Get
                Return _NewUrl
            End Get
            Set(ByVal value As String)
                _NewUrl = value
            End Set
        End Property

        Private _TotRecs As Integer

        Private _PageSize As Integer

        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal value As Integer)
                _PageSize = value
            End Set
        End Property

        Public Property TotRecs() As Integer
            Get
                Return _TotRecs
            End Get
            Set(ByVal value As Integer)
                _TotRecs = value
            End Set
        End Property

        Private _CurrentPage As Integer
        Public Property CurrentPage() As Integer
            Get
                Return _CurrentPage
            End Get
            Set(ByVal value As Integer)
                _CurrentPage = value
            End Set
        End Property

        Private _lngConfigAddressID As Long
        Public Property ConfigAddressID() As Long
            Get
                Return _lngConfigAddressID
            End Get
            Set(ByVal value As Long)
                _lngConfigAddressID = value
            End Set
        End Property

        Private _isHtml5 As Boolean
        Public Property IsHtml5() As Boolean
            Get
                Return _isHtml5
            End Get
            Set(ByVal value As Boolean)
                _isHtml5 = value
            End Set
        End Property

        Private _isSSLRedirectEnabled As Boolean
        Public Property IsSSLRedirectEnabled() As Boolean
            Get
                Return _isSSLRedirectEnabled
            End Get
            Set(ByVal value As Boolean)
                _isSSLRedirectEnabled = value
            End Set
        End Property

        Private _vcHtml5MetaTags As String
        Public Property Html5MetaTags() As String
            Get
                Return _vcHtml5MetaTags
            End Get
            Set(ByVal value As String)
                _vcHtml5MetaTags = value
            End Set
        End Property

        Public Property CategoryID As Long

        Public Function GetSites() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _SiteID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetSites", arParms).Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Shared Function GetSitesAll() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(0).Value = Nothing
                arParms(0).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetSites_All", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetSiteDetails() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _SiteID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetSitesDetail", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Sub InitialiseSession()
            Dim lngSiteID As Long
            'HttpContext.Current.Session("SiteID") = 75
            Dim objSite As New Sites()
            If ToLong(HttpContext.Current.Session("SiteID")) = 0 Then
                Dim strArr() As String = HttpContext.Current.Request.PhysicalApplicationPath.Split(New Char() {"\"}, StringSplitOptions.RemoveEmptyEntries)
                lngSiteID = Sites.ToLong(strArr(strArr.Length - 1))
                If lngSiteID > 0 Then
                    objSite.SetSessionsForCurrentSite(lngSiteID)
                End If
                'TODO:           Commented because of performace issue

                objSite.GetUserLocationFromIP()
            End If

        End Sub

        Public Sub SetSessionsForCurrentSite(ByVal SiteID As Long)
            Try
                'Note this Session values belongs to Site Owner. 
                ' Site User can have different Division ID 
                HttpContext.Current.Session("SiteID") = SiteID
                _SiteID = SiteID
                Dim dt As DataTable = GetSiteDetails()

                If dt.Rows.Count > 0 Then
                    HttpContext.Current.Session("DomainID") = ToString(dt.Rows(0)("numDomainID"))
                    HttpContext.Current.Session("DivisionID") = ToString(dt.Rows(0)("numDivisionID"))
                    HttpContext.Current.Session("SitePath") = HttpContext.Current.Request.ApplicationPath.TrimEnd("/") '+ "/Pages/" + SiteID.ToString()
                    HttpContext.Current.Session("WareHouseID") = ToString(dt.Rows(0)("numDefaultWareHouseID")) 'Will be overwritten by Zip code mapped WarehouseID
                    HttpContext.Current.Session("DefaultWareHouseID") = ToString(dt.Rows(0)("numDefaultWareHouseID")) 'will always be same as in ecom settings
                    HttpContext.Current.Session("DefaultRelationship") = ToLong(dt.Rows(0)("numRelationshipId"))
                    HttpContext.Current.Session("DefaultClass") = ToLong(dt.Rows(0)("numDefaultClass"))
                    HttpContext.Current.Session("DefaultProfile") = ToLong(dt.Rows(0)("numProfileId"))
                    HttpContext.Current.Session("vcRedirectThankYouUrl") = ToString(dt.Rows(0)("vcRedirectThankYouUrl"))
                    HttpContext.Current.Session("vcSalesOrderTabs") = ToString(dt.Rows(0)("vcSalesOrderTabs"))
                    HttpContext.Current.Session("vcSalesQuotesTabs") = ToString(dt.Rows(0)("vcSalesQuotesTabs"))
                    HttpContext.Current.Session("vcItemPurchaseHistoryTabs") = ToString(dt.Rows(0)("vcItemPurchaseHistoryTabs"))
                    HttpContext.Current.Session("vcItemsFrequentlyPurchasedTabs") = ToString(dt.Rows(0)("vcItemsFrequentlyPurchasedTabs"))
                    HttpContext.Current.Session("vcOpenCasesTabs") = ToString(dt.Rows(0)("vcOpenCasesTabs"))
                    HttpContext.Current.Session("vcOpenRMATabs") = ToString(dt.Rows(0)("vcOpenRMATabs"))
                    HttpContext.Current.Session("vcSupportTabs") = ToString(dt.Rows(0)("vcSupportTabs"))
                    HttpContext.Current.Session("bitSalesOrderTabs") = ToString(dt.Rows(0)("bitSalesOrderTabs"))
                    HttpContext.Current.Session("bitSupportTabs") = ToString(dt.Rows(0)("bitSupportTabs"))
                    HttpContext.Current.Session("bitSalesQuotesTabs") = ToString(dt.Rows(0)("bitSalesQuotesTabs"))
                    HttpContext.Current.Session("bitItemPurchaseHistoryTabs") = ToString(dt.Rows(0)("bitItemPurchaseHistoryTabs"))
                    HttpContext.Current.Session("bitItemsFrequentlyPurchasedTabs") = ToString(dt.Rows(0)("bitItemsFrequentlyPurchasedTabs"))
                    HttpContext.Current.Session("bitOpenCasesTabs") = ToString(dt.Rows(0)("bitOpenCasesTabs"))
                    HttpContext.Current.Session("bitOpenRMATabs") = ToString(dt.Rows(0)("bitOpenRMATabs"))
                    HttpContext.Current.Session("bitDefaultProfileURL") = ToString(dt.Rows(0)("bitDefaultProfileURL"))
                    HttpContext.Current.Session("HidePriceBeforeLogin") = Boolean.Parse(dt.Rows(0)("bitHidePriceBeforeLogin"))
                    HttpContext.Current.Session("HideAddtoCartBeforeLogin") = Boolean.Parse(dt.Rows(0)("bitHideAddtoCart"))

                    'Common Flag for hiding Pricing between HidePriceBeforeLogin and HidePriceAfterLogin
                    HttpContext.Current.Session("HidePrice") = False
                    HttpContext.Current.Session("ItemImagePath") = "http://" + HttpContext.Current.Request.ServerVariables("SERVER_NAME") + "/PortalDocs/" + Sites.ToString(HttpContext.Current.Session("DomainID"))
                    HttpContext.Current.Session("DomainAdminEmail") = dt.Rows(0)("DomainAdminEmail").ToString
                    HttpContext.Current.Session("SiteLiveURL") = dt.Rows(0)("vcLiveURL").ToString
                    HttpContext.Current.Application("CreditCardAuthOnly") = Sites.ToBool(dt.Rows(0)("bitAuthOnlyCreditCard"))
                    HttpContext.Current.Session("DefCountry") = Sites.ToLong(dt.Rows(0)("numDefCountry"))
                    HttpContext.Current.Session("DefCountry") = Sites.ToLong(dt.Rows(0)("numDefCountry"))
                    HttpContext.Current.Session("EmailStatus") = Sites.ToLong(dt.Rows(0)("bitSendMail"))
                    HttpContext.Current.Session("AuthSalesBizDoc") = Sites.ToLong(dt.Rows(0)("AuthSalesBizDoc"))
                    HttpContext.Current.Session("CreditTermBizDocID") = Sites.ToLong(dt.Rows(0)("numCreditTermBizDocID"))
                    HttpContext.Current.Session("CreditCardBizDocID") = Sites.ToLong(dt.Rows(0)("numCreditCardBizDocID"))
                    HttpContext.Current.Session("GoogleCheckoutBizDocID") = Sites.ToLong(dt.Rows(0)("numGoogleCheckoutBizDocID"))
                    HttpContext.Current.Session("SaveCreditCardInfo") = Sites.ToBool(dt.Rows(0)("bitSaveCreditCardInfo"))
                    HttpContext.Current.Session("IsOnePageCheckout") = Sites.ToBool(dt.Rows(0)("bitOnePageCheckout"))
                    HttpContext.Current.Session("BaseTaxCalcOn") = Sites.ToInteger(dt.Rows(0)("tintBaseTaxOnArea"))
                    HttpContext.Current.Session("ShippingServiceItemID") = Sites.ToInteger(dt.Rows(0)("numShippingServiceItemID"))
                    HttpContext.Current.Session("DiscountServiceItemID") = Sites.ToInteger(dt.Rows(0)("numDiscountServiceItemID"))
                    HttpContext.Current.Session("AutolinkUnappliedPayment") = Sites.ToInteger(dt.Rows(0)("bitAutolinkUnappliedPayment"))
                    HttpContext.Current.Session("PaypalCheckoutBizDocID") = Sites.ToLong(dt.Rows(0)("numPaypalCheckoutBizDocID"))
                    HttpContext.Current.Session("BaseTaxOn") = CCommon.ToShort(dt.Rows(0)("tintBaseTax"))
                    HttpContext.Current.Session("SkipStep2") = CCommon.ToShort(dt.Rows(0)("bitSkipStep2"))
                    HttpContext.Current.Session("DisplayCategory") = CCommon.ToShort(dt.Rows(0)("bitDisplayCategory"))
                    HttpContext.Current.Session("numSalesInquiryBizDocID") = Sites.ToLong(dt.Rows(0)("numSalesInquiryBizDocID"))
                    HttpContext.Current.Session("ShowPriceUsingPriceLevel") = CCommon.ToShort(dt.Rows(0)("bitShowPriceUsingPriceLevel"))
                    HttpContext.Current.Session("PreUpSell") = CCommon.ToBool(dt.Rows(0)("bitPreSellUp"))
                    HttpContext.Current.Session("PostUpSell") = CCommon.ToBool(dt.Rows(0)("bitPostSellUp"))
                    HttpContext.Current.Session("vcPreSellUp") = CCommon.ToString(dt.Rows(0)("vcPreSellUp"))
                    HttpContext.Current.Session("vcPostSellUp") = CCommon.ToString(dt.Rows(0)("vcPostSellUp"))
                    HttpContext.Current.Session("bitConfirmAddressRequired") = CCommon.ToBool(dt.Rows(0)("bitConfirmAddressRequired"))
                    HttpContext.Current.Session("IsShowPromoDetailsLink") = CCommon.ToBool(dt.Rows(0)("bitShowPromoDetailsLink"))
                End If

                Dim objBusinessClass As New BusinessClass
                objBusinessClass.IPNo = objBusinessClass.GetIPNumberFromIPAddress(HttpContext.Current.Request.UserHostAddress)
                Dim importWiz As New ImportWizard
                HttpContext.Current.Session("CountryID") = importWiz.GetStateAndCountry(0, objBusinessClass.GetCountryFromIPaddress(ConfigurationManager.AppSettings("ConnectionString")), HttpContext.Current.Session("DomainID"))

                Dim objCurrency As New CurrencyRates
                objCurrency.DomainID = HttpContext.Current.Session("DomainID")
                objCurrency.CountryID = HttpContext.Current.Session("CountryID")
                objCurrency.SiteID = HttpContext.Current.Session("SiteID")

                Dim dtCurrency As DataTable = objCurrency.GetCurrencyFromCountry()

                If dtCurrency.Rows.Count > 0 Then
                    HttpContext.Current.Session("CurrencyID") = dtCurrency.Rows(0)("numCurrencyID").ToString()
                    HttpContext.Current.Session("CurrSymbol") = dtCurrency.Rows(0)("varCurrSymbol").ToString()
                    HttpContext.Current.Session("ExchangeRate") = dtCurrency.Rows(0)("fltExchangeRate").ToString()
                End If
                Dim objSearch As New Common.CSearch()
                objSearch.DomainID = HttpContext.Current.Session("DomainID")
                objSearch.SiteID = HttpContext.Current.Session("SiteID")
                objSearch.byteMode = 0
                objSearch.SchemaOnly = True
                dt = objSearch.GetDataForIndexing()
                Dim strBizCartSearchFields As String
                For Each dr As DataRow In dt.Rows
                    strBizCartSearchFields = strBizCartSearchFields + CCommon.ToString(dr("vcFormFieldName")).Replace(" ", "") + ":(#SearchText#) "
                Next
                HttpContext.Current.Session("SearchFields") = strBizCartSearchFields

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub GetUserLocationFromIP()
            Try
                HttpContext.Current.Session("IsIPLocationLookUpCompleted") = True

                Dim ipAddress As String = HttpContext.Current.Request.ServerVariables("HTTP_X_FORWARDED_FOR")

                If String.IsNullOrEmpty(ipAddress) Then
                    ipAddress = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
                End If
                'ipAddress = "45.115.89.65"
                Dim APIKey As String = ""
                APIKey = System.Configuration.ConfigurationManager.AppSettings("IPStackAccessKey")
                Dim url As String = String.Format("http://api.ipstack.com/{0}?access_key={1}", ipAddress, APIKey)

                Using client As New WebClient()
                    Dim json As String = client.DownloadString(url)

                    Dim location As UserLocation = (New Script.Serialization.JavaScriptSerializer().Deserialize(Of UserLocation)(json))
                    If (location.zip <> "" AndAlso location.zip <> "-") Or (location.region_name <> "" AndAlso location.region_name <> "-") Then
                        HttpContext.Current.Session("UserIPZipcode") = location.zip

                        Dim objUserAccess As UserAccess = New UserAccess
                        objUserAccess.DomainID = Sites.ToLong(HttpContext.Current.Session("DomainID"))
                        objUserAccess.Country = 0
                        objUserAccess.vcAbbreviations = location.region_name
                        Dim dt As DataTable = objUserAccess.SelState
                        If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                            HttpContext.Current.Session("UserIPStateID") = CCommon.ToLong(dt.Rows(0)("numStateID"))
                            HttpContext.Current.Session("UserIPCountryID") = CCommon.ToLong(dt.Rows(0)("numCountryID"))
                        End If
                    End If
                End Using
            Catch ex As Exception
                'DO NOT THROW ERROR
            End Try
        End Sub


        Public Function ManageSites() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(15) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _SiteID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcSiteName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(1).Value = _SiteName

                arParms(2) = New Npgsql.NpgsqlParameter("@vcDescription", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(2).Value = _SiteDesc

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = DomainID

                arParms(5) = New Npgsql.NpgsqlParameter("@vcHostName", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(5).Value = _HostName

                arParms(6) = New Npgsql.NpgsqlParameter("@bitIsActive", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(6).Value = _IsActive

                arParms(7) = New Npgsql.NpgsqlParameter("@vcLiveURL", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(7).Value = _LiveURL

                arParms(8) = New Npgsql.NpgsqlParameter("@numCurrencyID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = _CurrencyID

                arParms(9) = New Npgsql.NpgsqlParameter("@bitOnePageCheckout", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(9).Value = _IsOnePageCheckout

                arParms(10) = New Npgsql.NpgsqlParameter("@tintRateType", NpgsqlTypes.NpgsqlDbType.SmallInt)
                arParms(10).Value = _RateType

                arParms(11) = New Npgsql.NpgsqlParameter("@vcCategories", NpgsqlTypes.NpgsqlDbType.VarChar, 2000)
                arParms(11).Value = _ListOfCategories

                arParms(12) = New Npgsql.NpgsqlParameter("@Mode", NpgsqlTypes.NpgsqlDbType.SmallInt)
                arParms(12).Value = _Mode

                arParms(13) = New Npgsql.NpgsqlParameter("@bitHtml5", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(13).Value = _isHtml5

                arParms(14) = New Npgsql.NpgsqlParameter("@vcMetaTags", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(14).Value = _vcHtml5MetaTags

                arParms(15) = New Npgsql.NpgsqlParameter("@bitSSLRedirect", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(15).Value = _isSSLRedirectEnabled

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageSites", objParam, True)
                _SiteID = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

                If _SiteID <= 0 Then
                    Return False
                End If
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function UpdateSiteStatus() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _SiteID

                arParms(1) = New Npgsql.NpgsqlParameter("@bitIsActive", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(1).Value = _IsActive

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateSiteStatus", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function DeleteSite() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _SiteID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                Return SqlDAL.ExecuteNonQuery(connString, "USP_DeleteSite", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function SaveJavascriptDisplayOrder() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(0).Value = _StrItems

                Return SqlDAL.ExecuteNonQuery(connString, "USP_SaveJavascriptDisplayOrder", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageStyles() As Long
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numCssID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CssID

                arParms(1) = New Npgsql.NpgsqlParameter("@StyleName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(1).Value = _StyleName

                arParms(2) = New Npgsql.NpgsqlParameter("@StyleFileName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(2).Value = _StyleFile

                arParms(3) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _SiteID

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = DomainID

                arParms(5) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _StyleType

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                Return Long.Parse(SqlDAL.ExecuteScalar(connString, "USP_ManageStyleSheets", arParms))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetContentPageDetails() As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@vcPageURL", NpgsqlTypes.NpgsqlDbType.Varchar, 1000)
                arParms(0).Value = _PageURL

                arParms(1) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _SiteID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcTemplateName", NpgsqlTypes.NpgsqlDbType.Varchar, 1000)
                arParms(2).Value = _TemplateName

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput


                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur1", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput


                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput
                Return SqlDAL.ExecuteDataset(connString, "USP_GetEcommercePageDetail", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetStyles() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numCssID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CssID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _SiteID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _StyleType

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                Dim dtTable As DataTable
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetStyleSheets", arParms)
                If ds.Tables.Count <> 0 Then
                    dtTable = ds.Tables(0)
                Else
                    dtTable = New DataTable
                End If

                Return dtTable
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function DeleteStyle() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numCssID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CssID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                Return SqlDAL.ExecuteNonQuery(connString, "USP_DeleteStyleSheets", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function WriteToFile(ByVal strData As String, ByVal FullPath As String) As Boolean
            Dim Contents As String
            Dim bAns As Boolean = False
            Dim objReader As StreamWriter
            Try
                objReader = New StreamWriter(FullPath, False, System.Text.Encoding.UTF8)
                objReader.Write(strData)
                objReader.Close()
                bAns = True
                Return bAns
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function ReadFile(ByVal FullPath As String) As String
            If (Not File.Exists(FullPath)) Then
                Return ""
            End If
            Dim strContents As String
            Dim objReader As StreamReader
            Try
                objReader = New StreamReader(FullPath)
                strContents = objReader.ReadToEnd()
                objReader.Close()
                'If (File.GetAttributes(FullPath) & FileAttribute.ReadOnly) = FileAttribute.ReadOnly Then
                File.SetAttributes(FullPath, FileAttributes.Normal)
                'End If
                Return strContents
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ManageSiteTemplates() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numTemplateID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _TemplateID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcTemplateName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(1).Value = _TemplateName

                arParms(2) = New Npgsql.NpgsqlParameter("@txtTemplateHTML", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(2).Value = _TemplateContent

                arParms(3) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _SiteID

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = DomainID

                arParms(5) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = UserCntID

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageSiteTemplates", objParam, True)
                _TemplateID = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

                If _TemplateID <= 0 Then
                    Return False
                End If

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetSiteTemplates() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numTemplateID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _TemplateID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _SiteID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetSiteTemplates", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteSiteTemplates() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numTemplateID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _TemplateID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                Return SqlDAL.ExecuteNonQuery(connString, "USP_DeleteSiteTemplates", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetPages() As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numPageID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _PageID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _SiteID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetSitePages", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetPageDetails() As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@vcPageURL", NpgsqlTypes.NpgsqlDbType.Varchar, 1000)
                arParms(0).Value = _PageURL

                arParms(1) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _SiteID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur4", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetSitePageDetail", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ManagePages() As Long
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(12) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numPageID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _PageID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcPageName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(1).Value = _PageName

                arParms(2) = New Npgsql.NpgsqlParameter("@vcPageURL", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(2).Value = _PageURL

                arParms(3) = New Npgsql.NpgsqlParameter("@tintPageType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _PageType

                arParms(4) = New Npgsql.NpgsqlParameter("@vcPageTitle", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(4).Value = _PageTitle

                arParms(5) = New Npgsql.NpgsqlParameter("@numTemplateID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _TemplateID

                arParms(6) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _SiteID

                arParms(7) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = DomainID

                arParms(8) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = UserCntID

                arParms(9) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.Xml)
                arParms(9).Value = _StrItems

                arParms(10) = New Npgsql.NpgsqlParameter("@bitIsActive", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(10).Value = _IsActive

                arParms(11) = New Npgsql.NpgsqlParameter("@bitIsMaintainScroll", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(11).Value = _IsMaintainScroll

                arParms(12) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(12).Value = Nothing
                arParms(12).Direction = ParameterDirection.InputOutput

                Return Long.Parse(SqlDAL.ExecuteScalar(connString, "USP_ManageSitePages", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function DeleteSitePages() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numPageID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _PageID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                Return SqlDAL.ExecuteNonQuery(connString, "USP_DeleteSitePage", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function UpdateSitePageStatus() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numPageID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _PageID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _SiteID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@bitIsActive", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(4).Value = _IsActive

                Return SqlDAL.ExecuteNonQuery(connString, "USP_UpdateSitePageStatus", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetMetaTag() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numMetaID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _PageID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintMetatagFor", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _MetaTagFor

                arParms(2) = New Npgsql.NpgsqlParameter("@numReferenceID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ReferenceID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetMetaTags", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ManageMetaTags() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numMetaID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _MetaID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintMetatagFor", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _MetaTagFor

                arParms(2) = New Npgsql.NpgsqlParameter("@numReferenceID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ReferenceID

                arParms(3) = New Npgsql.NpgsqlParameter("@vcMetaKeywords", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = _MetaKeywords

                arParms(4) = New Npgsql.NpgsqlParameter("@vcMetaDescription", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(4).Value = _MetaDescription

                arParms(5) = New Npgsql.NpgsqlParameter("@vcPageTitle", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(5).Value = _PageTitle

                Return SqlDAL.ExecuteNonQuery(connString, "USP_ManageMetaTags", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetPageElementsMaster() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                arParms(0) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _SiteID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetPageElementMaster", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ManagePageElementMaster() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numElementId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _numElementId

                arParms(1) = New Npgsql.NpgsqlParameter("@vcElementName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(1).Value = _vcElementName

                arParms(2) = New Npgsql.NpgsqlParameter("@vcUserControlPath", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(2).Value = _vcUserControlPath

                arParms(3) = New Npgsql.NpgsqlParameter("@vcTagName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(3).Value = _vcTagName

                arParms(4) = New Npgsql.NpgsqlParameter("@bitCustomization", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(4).Value = _bitCustomization

                arParms(5) = New Npgsql.NpgsqlParameter("@bitAdd", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(5).Value = _bitAdd

                arParms(6) = New Npgsql.NpgsqlParameter("@bitDelete", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(6).Value = _bitDelete

                arParms(7) = New Npgsql.NpgsqlParameter("@bitFlag", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(7).Value = _bitFlag



                Return SqlDAL.ExecuteNonQuery(connString, "USP_ManagePageElementMaster", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetPageElementAttributes() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet
                arParms(0) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _SiteID

                arParms(1) = New Npgsql.NpgsqlParameter("@numElementID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ElementID

                arParms(2) = New Npgsql.NpgsqlParameter("@numPageID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _PageID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetPageElementAttributes", arParms)
                If ds.Tables.Count > 0 Then
                    Return ds.Tables(0)
                Else
                    Return New DataTable
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function ManageAspxPages(ByVal SiteID As Long, ByVal PageURL As String, ByVal IsMaintainScroll As Boolean) As String
            Try
                Dim strHead As String
                Dim strBody As String
                Dim strMaintainScroll As String = ""

                Dim dt As DataTable = GetSites()

                strBody = GenerateHTMLfromTemplate(SiteID, PageURL, strHead)

                If IsMaintainScroll Then
                    strMaintainScroll = " MaintainScrollPositionOnPostback=""true"" "
                End If

                Dim sbStart As New System.Text.StringBuilder
                sbStart.Append("<%@ Page Language=""C#"" Inherits=""BizCart.BizPage"" " & strMaintainScroll & " %>" & vbCrLf)
                sbStart.Append("<%@ Register TagPrefix=""url"" Namespace=""Intelligencia.UrlRewriter"" Assembly=""Intelligencia.UrlRewriter"" %>" & vbCrLf) ' Fixed this issue http://www.tutorialsasp.net/tutorials/fixing-postbacks-while-using-urlrewriternet/
                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 AndAlso CCommon.ToBool(dt.Rows(0)("bitHtml5")) Then
                    sbStart.Append("<!DOCTYPE html>" & vbCrLf)
                Else
                    sbStart.Append("<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd""> " & vbCrLf)
                End If

                sbStart.Append("<script runat=""server""> " & vbCrLf)
                sbStart.Append("</script> " & vbCrLf)
                sbStart.Append("<html xmlns=""http://www.w3.org/1999/xhtml""> " & vbCrLf)
                sbStart.Append("<head runat=""server"">")

                'Add MetaTags added to site level and this tags are added in all pages
                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcMetaTags"))) Then
                    sbStart.Append(CCommon.ToString(dt.Rows(0)("vcMetaTags")))
                End If

                'Start Head 
                sbStart.Append(strHead) 'get Title,get Metatags,get Css and JS 
                'End Head
                sbStart.Append("</head> " & vbCrLf)
                sbStart.Append("<body> " & vbCrLf)
                sbStart.Append("    <url:form runat=""server"" id=""form1"">" & vbCrLf)

                'Start Body
                sbStart.Append(strBody) 'get HTML templete, replace Page Element with Actual User Controls,get generated HTML
                'End Body
                sbStart.Append("</url:form> " & vbCrLf)
                sbStart.Append("</body> " & vbCrLf)
                sbStart.Append("</html>")

                'Save File as aspx page
                Dim strDirectory As String = ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & SiteID.ToString()
                If Directory.Exists(strDirectory) = False Then ' If Folder Does not exists create New Folder.
                    Directory.CreateDirectory(strDirectory)
                End If
                WriteToFile(sbStart.ToString(), strDirectory & "\" & _PageURL)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GenerateHTMLfromTemplate(ByVal SiteID As Long, ByVal PageURL As String, ByRef IncludeScript As String) As String
            Try

                'get Title
                'get Metatags
                'get Css and JS 
                'get HTML templete
                'replace Page Element with Actual User Controls
                'return generated HTML

                Dim sbInclude As New System.Text.StringBuilder()
                Dim strIncludeDirectoryPath As String = String.Empty
                Dim BodyHTML As New System.Text.StringBuilder

                _SiteID = SiteID
                _PageURL = PageURL
                Dim ds As DataSet = GetPageDetails()
                If ds.Tables(0).Rows.Count > 0 Then
                    DomainID = ds.Tables(0).Rows(0)("numDomainID")

                    sbInclude.Append("<title>" & ds.Tables(0).Rows(0)("vcPageTitle").ToString() & "</title>")

                    'Commented by chintan as full path is not required
                    'strIncludeDirectoryPath = "/" + System.Configuration.ConfigurationManager.AppSettings("CartVirtualDirectoryName") + "/Pages/" + System.Web.HttpContext.Current.Session("SiteID").ToString() & "/"

                    'kishan In case of product.aspx as meta tags and keywords binds dynamically on the basis of Item from product.ascx
                    If PageURL <> "product.aspx" Then
                        sbInclude.Append(System.Web.HttpUtility.HtmlDecode(GenerateMetaTags(CCommon.ToString(ds.Tables(0).Rows(0)("vcMetaKeywords")), CCommon.ToString(ds.Tables(0).Rows(0)("vcMetaDescription")))))
                    End If

                    For Each dr As DataRow In ds.Tables(1).Rows
                        sbInclude.Append("<link rel=""stylesheet"" type=""text/css"" href=""/" & dr("StyleFileName").ToString() & """ />")
                    Next
                    For Each dr As DataRow In ds.Tables(2).Rows
                        sbInclude.Append("<script type=""text/javascript"" src=""/Js/" & dr("StyleFileName").ToString() & """ ></script>")
                    Next
                    IncludeScript = HttpUtility.HtmlDecode(sbInclude.ToString())
                End If

                If ds.Tables(3).Rows.Count > 0 Then
                    BodyHTML.Append(ds.Tables(3).Rows(0)("txtTemplateHTML").ToString())
                    'Find and Replace Template
                    _TemplateID = 0
                    Dim dtTemplates As DataTable = GetSiteTemplates()
                    Dim dtSelectedTemplate As DataTable
                    For Each dr As DataRow In dtTemplates.Rows
                        If BodyHTML.ToString().ToLower().IndexOf(dr("vcTemplateCode").ToString().ToLower()) >= 0 Then
                            _TemplateID = dr("numTemplateID").ToString()
                            dtSelectedTemplate = GetSiteTemplates()
                            BodyHTML.Replace(dr("vcTemplateCode").ToString(), dtSelectedTemplate.Rows(0)("txtTemplateHTML").ToString())
                        End If
                    Next
                    'replace Page elements(usercontrols)
                    Dim dtUserControls As DataTable = GetPageElementsMaster()
                    Dim strUserControl As String
                    Dim i As Integer = 1
                    Dim strAttributes As String
                    For Each dr1 As DataRow In dtUserControls.Rows
                        Dim intStartIndex As Integer = BodyHTML.ToString().IndexOf(dr1("vcTagName").ToString(), System.StringComparison.CurrentCultureIgnoreCase)
                        Dim intEndIndex As Integer = BodyHTML.ToString().LastIndexOf(dr1("vcTagName").ToString(), System.StringComparison.CurrentCultureIgnoreCase)

                        If intStartIndex >= 0 Then

                            If intStartIndex = intEndIndex Then 'i.e: Ony one occurrence of Specified UserControl
                                strUserControl = " <%@ Register src=""" + dr1("vcUserControlPath").ToString() + """ tagname=""" + dr1("vcElementName").ToString() + """ tagprefix=""uc1"" %> "
                                strUserControl = strUserControl + "<uc1:" + dr1("vcElementName").ToString() + " ID=""" + dr1("vcElementName").ToString() + i.ToString() + """ runat=""server""  " + GetUserControlProperties(dr1("numElementID"), _SiteID) + "  /> "

                                BodyHTML.Replace(dr1("vcTagName").ToString(), strUserControl, intStartIndex, dr1("vcTagName").ToString().Length)
                                i = i + 1
                            Else 'i.e: multiple occurrence of Specified UserControl in same page
                                'replace first occurrence
                                strUserControl = " <%@ Register src=""" + dr1("vcUserControlPath").ToString() + """ tagname=""" + dr1("vcElementName").ToString() + """ tagprefix=""uc1"" %> "
                                strUserControl = strUserControl + "<uc1:" + dr1("vcElementName").ToString() + " ID=""" + dr1("vcElementName").ToString() + i.ToString() + """ runat=""server""  " + GetUserControlProperties(dr1("numElementID"), _SiteID) + "  /> "

                                BodyHTML.Replace(dr1("vcTagName").ToString(), strUserControl, intStartIndex, dr1("vcTagName").ToString().Length)
                                i = i + 1
                                'replace rest all  Without @Register tag
                                intStartIndex = BodyHTML.ToString().IndexOf(dr1("vcTagName").ToString(), System.StringComparison.CurrentCultureIgnoreCase)
                                While intStartIndex > 0
                                    strUserControl = "<uc1:" + dr1("vcElementName").ToString() + " ID=""" + dr1("vcElementName").ToString() + i.ToString() + """ runat=""server""  " + GetUserControlProperties(dr1("numElementID"), _SiteID) + "  /> "

                                    BodyHTML.Replace(dr1("vcTagName").ToString(), strUserControl, intStartIndex, dr1("vcTagName").ToString().Length)
                                    i = i + 1
                                    intStartIndex = BodyHTML.ToString().IndexOf(dr1("vcTagName").ToString(), System.StringComparison.CurrentCultureIgnoreCase)
                                End While

                            End If
                        End If
                    Next

                    Return HttpUtility.HtmlDecode(BodyHTML.ToString())
                Else
                    Return ""
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GenerateHTMLfromTemplateForCustomPagesBlogs(ByVal SiteID As Long, ByVal templateName As String, ByRef IncludeScript As String) As String
            Try

                'get Title
                'get Metatags
                'get Css and JS 
                'get HTML templete
                'replace Page Element with Actual User Controls
                'return generated HTML

                Dim sbInclude As New System.Text.StringBuilder()
                Dim strIncludeDirectoryPath As String = String.Empty
                Dim BodyHTML As New System.Text.StringBuilder
                _TemplateName = templateName
                _SiteID = SiteID
                _PageURL = PageURL
                Dim ds As DataSet = GetContentPageDetails()
                If ds.Tables(0).Rows.Count > 0 Then
                    'DomainID = ds.Tables(0).Rows(0)("numDomainID")

                    'sbInclude.Append("<title>" & ds.Tables(0).Rows(0)("vcPageTitle").ToString() & "</title>")

                    'Commented by chintan as full path is not required
                    'strIncludeDirectoryPath = "/" + System.Configuration.ConfigurationManager.AppSettings("CartVirtualDirectoryName") + "/Pages/" + System.Web.HttpContext.Current.Session("SiteID").ToString() & "/"

                    'kishan In case of product.aspx as meta tags and keywords binds dynamically on the basis of Item from product.ascx

                    'commented By prasanta
                    'If PageURL <> "product.aspx" Then
                    '    sbInclude.Append(System.Web.HttpUtility.HtmlDecode(GenerateMetaTags(CCommon.ToString(ds.Tables(0).Rows(0)("vcMetaKeywords")), CCommon.ToString(ds.Tables(0).Rows(0)("vcMetaDescription")))))
                    'End If

                    For Each dr As DataRow In ds.Tables(1).Rows
                        sbInclude.Append("<link rel=""stylesheet"" type=""text/css"" href=""/" & dr("StyleFileName").ToString() & """ />")
                    Next
                    For Each dr As DataRow In ds.Tables(2).Rows
                        sbInclude.Append("<script type=""text/javascript"" src=""/Js/" & dr("StyleFileName").ToString() & """ ></script>")
                    Next
                    IncludeScript = HttpUtility.HtmlDecode(sbInclude.ToString())
                    'commented By prasanta
                End If

                If ds.Tables(3).Rows.Count > 0 Then
                    BodyHTML.Append(ds.Tables(3).Rows(0)("txtTemplateHTML").ToString())
                    'Find and Replace Template
                    _TemplateID = 0
                    Dim dtTemplates As DataTable = GetSiteTemplates()
                    Dim dtSelectedTemplate As DataTable
                    For Each dr As DataRow In dtTemplates.Rows
                        If BodyHTML.ToString().ToLower().IndexOf(dr("vcTemplateCode").ToString().ToLower()) >= 0 Then
                            _TemplateID = dr("numTemplateID").ToString()
                            dtSelectedTemplate = GetSiteTemplates()
                            BodyHTML.Replace(dr("vcTemplateCode").ToString(), dtSelectedTemplate.Rows(0)("txtTemplateHTML").ToString())
                        End If
                    Next
                    'replace Page elements(usercontrols)
                    Dim dtUserControls As DataTable = GetPageElementsMaster()
                    Dim strUserControl As String
                    Dim i As Integer = 1
                    Dim strAttributes As String
                    For Each dr1 As DataRow In dtUserControls.Rows
                        Dim intStartIndex As Integer = BodyHTML.ToString().IndexOf(dr1("vcTagName").ToString(), System.StringComparison.CurrentCultureIgnoreCase)
                        Dim intEndIndex As Integer = BodyHTML.ToString().LastIndexOf(dr1("vcTagName").ToString(), System.StringComparison.CurrentCultureIgnoreCase)

                        If intStartIndex >= 0 Then

                            If intStartIndex = intEndIndex Then 'i.e: Ony one occurrence of Specified UserControl
                                strUserControl = " <%@ Register src=""" + dr1("vcUserControlPath").ToString() + """ tagname=""" + dr1("vcElementName").ToString() + """ tagprefix=""uc1"" %> "
                                strUserControl = strUserControl + "<uc1:" + dr1("vcElementName").ToString() + " ID=""" + dr1("vcElementName").ToString() + i.ToString() + """ runat=""server""  " + GetUserControlProperties(dr1("numElementID"), _SiteID) + "  /> "

                                BodyHTML.Replace(dr1("vcTagName").ToString(), strUserControl, intStartIndex, dr1("vcTagName").ToString().Length)
                                i = i + 1
                            Else 'i.e: multiple occurrence of Specified UserControl in same page
                                'replace first occurrence
                                strUserControl = " <%@ Register src=""" + dr1("vcUserControlPath").ToString() + """ tagname=""" + dr1("vcElementName").ToString() + """ tagprefix=""uc1"" %> "
                                strUserControl = strUserControl + "<uc1:" + dr1("vcElementName").ToString() + " ID=""" + dr1("vcElementName").ToString() + i.ToString() + """ runat=""server""  " + GetUserControlProperties(dr1("numElementID"), _SiteID) + "  /> "

                                BodyHTML.Replace(dr1("vcTagName").ToString(), strUserControl, intStartIndex, dr1("vcTagName").ToString().Length)
                                i = i + 1
                                'replace rest all  Without @Register tag
                                intStartIndex = BodyHTML.ToString().IndexOf(dr1("vcTagName").ToString(), System.StringComparison.CurrentCultureIgnoreCase)
                                While intStartIndex > 0
                                    strUserControl = "<uc1:" + dr1("vcElementName").ToString() + " ID=""" + dr1("vcElementName").ToString() + i.ToString() + """ runat=""server""  " + GetUserControlProperties(dr1("numElementID"), _SiteID) + "  /> "

                                    BodyHTML.Replace(dr1("vcTagName").ToString(), strUserControl, intStartIndex, dr1("vcTagName").ToString().Length)
                                    i = i + 1
                                    intStartIndex = BodyHTML.ToString().IndexOf(dr1("vcTagName").ToString(), System.StringComparison.CurrentCultureIgnoreCase)
                                End While

                            End If
                        End If
                    Next
                    BodyHTML.Replace("##Title##", ds.Tables(0).Rows(0)("vcPageTitle"))
                    BodyHTML.Replace("##CategoryName##", ds.Tables(0).Rows(0)("vcCategoryName"))
                    BodyHTML.Replace("##CategoryId##", ds.Tables(0).Rows(0)("numCategoryId"))
                    BodyHTML.Replace("##UpdatedBy##", ds.Tables(0).Rows(0)("vcUpdatedBy"))
                    BodyHTML.Replace("##UpdatedOn##", ds.Tables(0).Rows(0)("dtmUpdatedOn"))
                    BodyHTML.Replace("##contentDynmicWithData##", ds.Tables(0).Rows(0)("vcContent"))
                    Return HttpUtility.HtmlDecode(BodyHTML.ToString())
                Else
                    Return ""
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GenerateMetaTags(ByVal strMetaKeywords As String, ByVal strMetaDescription As String) As String
            Try
                Dim strMetaTag As String = ""

                strMetaTag = strMetaTag + " <meta name=""keywords"" content=""" + strMetaKeywords + """>"
                strMetaTag = strMetaTag + " <meta name=""description"" content=""" + strMetaDescription + """>"

                Return strMetaTag
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetUserControlProperties(ByVal ElementID As Long, ByVal SiteID As Long)
            Try
                Dim dt As New DataTable
                _ElementID = ElementID
                _SiteID = SiteID
                dt = GetPageElementAttributes()
                Dim sbProperty As New System.Text.StringBuilder
                For Each dr As DataRow In dt.Rows
                    sbProperty.Append(dr("vcAttributeName").ToString().Replace(" ", "") & "=""" & HttpUtility.HtmlEncode(dr("vcAttributeValue").ToString()) & """ ")
                Next
                Return sbProperty.ToString()
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Sub ManagePageElementDetails()
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numElementID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ElementID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _SiteID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.Xml)
                arParms(3).Value = _StrItems

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                SqlDAL.ExecuteScalar(connString, "USP_ManagePageElementDetails", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Function GenerateSEOFriendlyURL(ByVal title As String) As String
            Try
                If (String.IsNullOrEmpty(title)) Then Return ""
                ' make it all lower case
                title = title.ToLower()
                ' remove entities
                title = Text.RegularExpressions.Regex.Replace(title, "&\w+;", "")
                ' remove anything that is not letters, numbers, dash, or space
                title = Text.RegularExpressions.Regex.Replace(title, "[^a-z0-9\-\s()]", "")
                ' replace spaces
                title = title.Replace(" "c, "-"c)
                ' collapse dashes
                title = Text.RegularExpressions.Regex.Replace(title, "-{2,}", "-")
                ' trim excessive dashes at the beginning
                title = title.TrimStart(New Char() {"-"})
                ' if it's too long, clip it
                If title.Length > 80 Then
                    title = title.Substring(0, 79)
                End If
                ' remove trailing dashes
                title = title.TrimEnd(New Char() {"-"})
                Return title
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'object  to string conversion
        Public Shared Function ToString(ByVal obj As Object) As String
            Try
                If IsDBNull(obj) Then
                    Return ""
                ElseIf obj = Nothing Then
                    Return ""
                Else
                    Return Convert.ToString(obj)
                End If
            Catch ex As Exception
                Return ""
            End Try
        End Function
        Public Shared Function ToLong(ByVal value As Object) As Int64
            Try
                If IsDBNull(value) Then
                    Return 0
                ElseIf value = Nothing Then
                    Return 0
                ElseIf value.ToString() = "" Then
                    Return 0
                Else
                    Return System.Convert.ToInt64(value)
                End If
            Catch ex As Exception
                Return 0
            End Try
        End Function
        Public Shared Function ToInteger(ByVal value As Object) As Int32
            Try
                If IsDBNull(value) Then
                    Return 0
                ElseIf value = Nothing Then
                    Return 0
                ElseIf value.ToString() = "" Then
                    Return 0
                Else
                    Return System.Convert.ToInt32(value)
                End If
            Catch ex As Exception
                Return 0
            End Try
        End Function
        Public Shared Function ToDecimal(ByVal value As Object) As Decimal
            Try
                If IsDBNull(value) Then
                    Return 0
                ElseIf value = Nothing Then
                    Return 0
                ElseIf value.ToString() = "" Then
                    Return 0
                Else
                    Return System.Convert.ToDecimal(value)
                End If
            Catch ex As Exception
                Return 0
            End Try
        End Function
        Public Shared Function ToDouble(ByVal value As Object) As Double
            Try
                If IsDBNull(value) Then
                    Return 0
                ElseIf value = Nothing Then
                    Return 0
                ElseIf value.ToString() = "" Then
                    Return 0
                Else
                    Return System.Convert.ToDouble(value)
                End If
            Catch ex As Exception
                Return 0
            End Try
        End Function
        Public Shared Function ToBool(ByVal value As Object) As Boolean
            Try
                If IsDBNull(value) Then
                    Return False
                ElseIf value = Nothing Then
                    Return False
                ElseIf value.ToString() = "" Then
                    Return False
                Else
                    Return System.Convert.ToBoolean(value)
                End If
            Catch ex As Exception
                Return False
            End Try
        End Function
        ' returns 2009-11-25 06:46:41.140
        Public Shared Function ToSqlDate(ByVal value As Object) As Date
            Try
                If value = Nothing Then
                    Return Now.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.000")
                ElseIf IsDBNull(value) Then
                    Return Now.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.000")
                ElseIf value.ToString() = "" Then
                    Return Now.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.000")
                Else
                    Dim parsedDatetime As Date = Date.Parse(value)
                    Return parsedDatetime.ToString("yyyy-MM-dd HH:mm:ss.000")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ManageSiteMenu() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numMenuID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _MenuID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcTitle", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(1).Value = _MenuTitle

                arParms(2) = New Npgsql.NpgsqlParameter("@vcNavigationURL", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(2).Value = _NavigationURL

                arParms(3) = New Npgsql.NpgsqlParameter("@tintLevel", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _Level

                arParms(4) = New Npgsql.NpgsqlParameter("@numParentID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _ParentMenuID

                arParms(5) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _SiteID

                arParms(6) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = DomainID

                arParms(7) = New Npgsql.NpgsqlParameter("@numPageID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _PageID

                arParms(8) = New Npgsql.NpgsqlParameter("@bitStatus", NpgsqlTypes.NpgsqlDbType.Boolean)
                arParms(8).Value = _Status

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageSiteMenu", objParam, True)
                _MenuID = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

                If _MenuID <= 0 Then
                    Return False
                End If

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetSiteMenu() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numMenuID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _MenuID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _SiteID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetSiteMenu", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function DeleteSiteMenu() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numTemplateID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _MenuID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _SiteID

                Return SqlDAL.ExecuteNonQuery(connString, "USP_DeleteSiteMenu", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function SaveSiteMenuDisplayOrder() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(0).Value = _StrItems

                Return SqlDAL.ExecuteNonQuery(connString, "USP_SiteMenuDisplayOrder", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageSiteSubscription() As Boolean
            Dim arParms As Npgsql.NpgsqlParameter() = New Npgsql.NpgsqlParameter(5) {}
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim ds As New DataSet()

            arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(0).Value = DomainID

            arParms(1) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(1).Value = _SiteID

            arParms(2) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(2).Value = _ContactID

            arParms(3) = New Npgsql.NpgsqlParameter("@vcEmail", NpgsqlTypes.NpgsqlDbType.VarChar)
            arParms(3).Value = _Email

            arParms(4) = New Npgsql.NpgsqlParameter("@bitToSubscribe", NpgsqlTypes.NpgsqlDbType.Bit)
            arParms(4).Value = _IsSubscribe

            arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
            arParms(5).Value = Nothing
            arParms(5).Direction = ParameterDirection.InputOutput

            Return Convert.ToBoolean(SqlDAL.ExecuteNonQuery(connString, "USP_ManageSiteSubscription", arParms))
        End Function

        Public Function GetBreadCrumb() As DataTable
            Dim arParms As Npgsql.NpgsqlParameter() = New Npgsql.NpgsqlParameter(3) {}
            Dim connString As String = ConfigurationManager.AppSettings("ConnectionString")
            Dim ds As New DataSet()

            arParms(0) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(0).Value = SiteID

            arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(1).Value = DomainID

            arParms(2) = New Npgsql.NpgsqlParameter("@numBreadCrumbID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(2).Value = _BreadCrumbID

            arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
            arParms(3).Value = Nothing
            arParms(3).Direction = ParameterDirection.InputOutput

            Return SqlDAL.ExecuteDataset(connString, "USP_GetSiteBreadCrumb", arParms).Tables(0)
        End Function
        Public Function UpdateBreadCrumbLevel() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.Xml)
                arParms(0).Value = _StrItems

                Return SqlDAL.ExecuteNonQuery(connString, "USP_UpdateBreadCrumLevel", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function DeleteBreadCrumb() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numBreadCrumbID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _BreadCrumbID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _SiteID

                Return SqlDAL.ExecuteNonQuery(connString, "USP_DeleteBreadCrumb", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ManageSiteBreadCumbs() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numBreadCrumbID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _BreadCrumbID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _SiteID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@numPageID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _PageID

                arParms(4) = New Npgsql.NpgsqlParameter("@tintLevel", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _Level

                arParms(5) = New Npgsql.NpgsqlParameter("@vcDisplayName", NpgsqlTypes.NpgsqlDbType.VarChar, 15)
                arParms(5).Value = _DisplayName

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageSiteBreadCumbs", objParam, True)
                _BreadCrumbID = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

                If _BreadCrumbID <= 0 Then
                    Return False
                End If

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function CheckDuplicateSiteSubscription() As Boolean
            Dim arParms As Npgsql.NpgsqlParameter() = New Npgsql.NpgsqlParameter(3) {}
            Dim connString As String = ConfigurationManager.AppSettings("ConnectionString")
            Dim ds As New DataSet()

            arParms(0) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(0).Value = _SiteID

            arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(1).Value = DomainID

            arParms(2) = New Npgsql.NpgsqlParameter("@vcEmail", NpgsqlTypes.NpgsqlDbType.VarChar)
            arParms(2).Value = _Email

            arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
            arParms(3).Value = Nothing
            arParms(3).Direction = ParameterDirection.InputOutput

            Return Convert.ToBoolean(SqlDAL.ExecuteScalar(connString, "USP_CheckDuplicateSiteSubscription", arParms))
        End Function

        Public Sub UpdateDefaultWarehouseForLoggedInUser(ByVal lngDivisionID As Long)
            'Update default WarehouseID based on Warehouse Mapping to Shipping Zip
            Dim lngWarehouseID As Long = 0
            Dim objItems As New CItems()
            objItems.DivisionID = lngDivisionID
            objItems.DomainID = ToLong(HttpContext.Current.Session("DomainID"))
            objItems.byteMode = 1
            lngWarehouseID = objItems.GetWarehouseFromDivID()
            If lngWarehouseID > 0 Then
                HttpContext.Current.Session("WarehouseID") = lngWarehouseID
            Else
                HttpContext.Current.Session("WarehouseID") = HttpContext.Current.Session("DefaultWareHouseID")
            End If
        End Sub

        Public Shared Function GetUploadPath() As String
            Try
                If Sites.ToInteger(HttpContext.Current.Session("SiteID")) > 0 Then
                    Return ConfigurationManager.AppSettings("CartLocation") & "\Pages\" & Sites.ToString(HttpContext.Current.Session("SiteID"))
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function RemoveMatchedPattern(ByVal strUI As String, ByVal LoginPattern As String) As String
            Try
                If Regex.IsMatch(strUI, LoginPattern) Then
                    Dim matches As MatchCollection = Regex.Matches(strUI, LoginPattern)
                    If (matches.Count > 0) Then
                        strUI = strUI.Replace(matches(0).Value, "")
                    End If
                End If
                Return strUI
            Catch ex As Exception
                Throw ex
            End Try

        End Function
        Public Shared Function ReplaceMatchedPatternWithContent(ByVal strUI As String, ByVal LoginPattern As String) As String
            Try
                If Regex.IsMatch(strUI, LoginPattern) Then
                    Dim matches As MatchCollection = Regex.Matches(strUI, LoginPattern)
                    If (matches.Count > 0) Then
                        strUI = Regex.Replace(strUI, LoginPattern, matches(0).Groups("Content").Value)
                    End If
                End If
                Return strUI
            Catch ex As Exception
                Throw ex
            End Try

        End Function
        Public CssIdList As New ArrayList
        Function CreateTemplate(ByVal SiteID As Long, ByVal TemplateName As String, ByVal PageElementCode As String, ByVal strSiteName As String, ByVal lngDomainID As Long, ByVal lngUserCntID As Long, Optional ByVal TemplateFileName As String = "Default.htm") As Long
            Try
                Dim objTemplate As New Sites
                Dim strDefaultTemplateFile As String = ConfigurationManager.AppSettings("CartLocation") & "\Default\" & TemplateFileName

                objTemplate.SiteID = SiteID
                objTemplate.TemplateID = 0
                objTemplate.TemplateName = HttpContext.Current.Server.HtmlEncode(TemplateName)
                objTemplate.TemplateContent = objTemplate.ReadFile(strDefaultTemplateFile).Replace("<#CONTENT#>", PageElementCode) 'Server.HtmlEncode(oEditHtml.Text.Trim())
                objTemplate.TemplateContent = objTemplate.TemplateContent.Replace("{#SiteName#}", strSiteName.Trim()) 'replace site name 
                objTemplate.DomainID = lngDomainID
                objTemplate.UserCntID = lngUserCntID
                objTemplate.ManageSiteTemplates()
                Return objTemplate.TemplateID
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Sub CreatePage(ByVal SiteID As Long, ByVal TemplateID As Long, ByVal PageName As String, ByVal PageTitle As String, ByVal PageURL As String, ByVal CreateMenu As Boolean, ByVal lngDomainID As Long, ByVal lngUserCntID As Long, ByVal IsMaintainScroll As Boolean)
            Try
                Dim objSite As New Sites
                Dim lngPageID As Long
                With objSite
                    .SiteID = SiteID
                    .PageID = 0
                    .PageName = PageName
                    .PageTitle = HttpUtility.HtmlEncode(PageTitle)
                    .PageURL = PageURL
                    .PageType = 1 'system page
                    .TemplateID = TemplateID
                    .DomainID = lngDomainID
                    .StrItems = GetCSSItems()
                    .UserCntID = lngUserCntID
                    .IsActive = True
                    .IsMaintainScroll = IsMaintainScroll
                    lngPageID = objSite.ManagePages()

                    'Add Menu Item for Created Page
                    If CreateMenu Then
                        .MenuID = 0
                        .MenuTitle = HttpContext.Current.Server.HtmlEncode(PageName)
                        .Status = 1
                        .PageID = lngPageID
                        .NavigationURL = ""
                        .DomainID = lngDomainID
                        .ManageSiteMenu()
                    End If
                    'Comented Undo when procudure is avail
                    '.PageID = lngPageID
                    '.Level = 1
                    '.DisplayName = PageName
                    '.ManageSiteBreadCumbs()

                    HttpContext.Current.Session("SiteID") = .SiteID
                    .ManageAspxPages(.SiteID, .PageURL, IsMaintainScroll)

                End With
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub CreateCSS(ByVal SiteID As Long, ByVal lngDomainID As Long)
            Try
                Dim objSite As New Sites
                objSite.SiteID = SiteID
                objSite.CssID = 0
                objSite.DomainID = lngDomainID
                objSite.StyleType = 0
                objSite.StyleFile = "PagingDarkStyle.css"
                objSite.StyleName = "PagingDarkStyle"
                CssIdList.Add(objSite.ManageStyles())
                objSite.StyleFile = "PagingLightStyle.css"
                objSite.StyleName = "PagingLightStyle"
                objSite.ManageStyles()
                objSite.StyleFile = "default.css"
                objSite.StyleName = "default"
                CssIdList.Add(objSite.ManageStyles())
                objSite.StyleFile = "smart_wizard.css"
                objSite.StyleName = "OnePageCheckout"
                CssIdList.Add(objSite.ManageStyles())
                objSite.StyleFile = "SliderStyle.css"
                objSite.StyleName = "SliderStyle"
                CssIdList.Add(objSite.ManageStyles())


            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub CreateJavascript(ByVal SiteID As Long, ByVal lngDomainID As Long)
            Try
                Dim objSite As New Sites
                objSite.SiteID = SiteID
                objSite.CssID = 0
                objSite.DomainID = lngDomainID
                objSite.StyleType = 1

                objSite.StyleFile = "Common.js"
                objSite.StyleName = "Common"
                CssIdList.Add(objSite.ManageStyles())

                objSite.StyleFile = "jquery-1.4.2.min.js"
                objSite.StyleName = "jquery-1.4.2-Min"
                objSite.ManageStyles()

                objSite.StyleFile = "jquery-ui.min.js"
                objSite.StyleName = "jquery-Min"
                CssIdList.Add(objSite.ManageStyles())

                objSite.StyleFile = "jquery.js"
                objSite.StyleName = "Jquery"
                CssIdList.Add(objSite.ManageStyles())

                objSite.StyleFile = "jquery.MetaData.js"
                objSite.StyleName = "Jquery_Metadata"
                CssIdList.Add(objSite.ManageStyles())

                objSite.StyleFile = "jquery.rating.js"
                objSite.StyleName = "Rating"
                CssIdList.Add(objSite.ManageStyles())

                objSite.StyleFile = "jquery.jqzoom-core.js"
                objSite.StyleName = "Zoom"
                CssIdList.Add(objSite.ManageStyles())

                objSite.StyleFile = "jquery.ae.image.resize.min.js"
                objSite.StyleName = "Image-Resize"
                CssIdList.Add(objSite.ManageStyles())

                objSite.StyleFile = "jquery.review.js"
                objSite.StyleName = "Review"
                CssIdList.Add(objSite.ManageStyles())

                objSite.StyleFile = "jquery.smartWizard-2.0.js"
                objSite.StyleName = "smartWizard"
                CssIdList.Add(objSite.ManageStyles())

            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Function GetCSSItems() As String
            Try
                Dim ds As New DataSet
                Dim dt As New DataTable
                dt.Columns.Add("numCssID")

                If CssIdList.Count > 0 Then
                    For i As Integer = 0 To CssIdList.Count - 1
                        Dim dr As DataRow = dt.NewRow
                        dr("numCssID") = CssIdList.Item(i).ToString()
                        dt.Rows.Add(dr)
                    Next
                End If

                ds.Tables.Add(dt.Copy)
                ds.Tables(0).TableName = "Item"
                Return ds.GetXml()
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Shared Function IsHostNameInUse(ByVal strHostName As String, ByVal SiteID As Long, ByVal DomainID As Long) As Boolean
            Try

                Dim objCommon As New CCommon
                objCommon.DomainID = DomainID
                objCommon.Str = strHostName
                objCommon.Mode = 3
                Dim intPortalCount As Integer = CCommon.ToInteger(objCommon.GetSingleFieldValue())

                objCommon.DomainID = SiteID
                objCommon.Str = strHostName
                objCommon.Mode = 2
                Dim intSiteCount As Integer = CCommon.ToInteger(objCommon.GetSingleFieldValue())

                If intPortalCount > 0 Or intSiteCount > 0 Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' <summary>
        ''' utility from http://weblogs.asp.net/zowens/archive/2008/02/15/improve-asp-net-performance-cssmin.aspx
        ''' </summary>
        ''' <param name="body"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function CompressCSS(ByVal body As String) As String
            Try
                body = Regex.Replace(body, "/\*.+?\*/", "", RegexOptions.Singleline)

                body = body.Replace("  ", String.Empty)

                body = body.Replace(Environment.NewLine + Environment.NewLine + Environment.NewLine, String.Empty)

                body = body.Replace(Environment.NewLine + Environment.NewLine, Environment.NewLine)

                body = body.Replace(Environment.NewLine, String.Empty)

                body = body.Replace("\t", String.Empty)

                body = body.Replace(" {", "{")

                body = body.Replace(" :", ":")

                body = body.Replace(": ", ":")

                body = body.Replace(", ", ",")

                body = body.Replace("; ", ";")

                body = body.Replace(";}", "}")

                body = Regex.Replace(body, "/\*[^\*]*\*+([^/\*]*\*+)*/", "$1")

                body = Regex.Replace(body, "(?<=[>])\s{2,}(?=[<])|(?<=[>])\s{2,}(?=&nbsp;)|(?<=&ndsp;)\s{2,}(?=[<])", String.Empty)

                Return body
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GeteCommercePaymentConfig(ByVal PaymentMethodId As Long) As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _SiteID

                arParms(2) = New Npgsql.NpgsqlParameter("@numPaymentMethodId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = PaymentMethodId

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GeteCommercePaymentConfig", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetRedirectConfig() As DataTable
            Try
                Dim ds As DataSet

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _SiteID

                arParms(2) = New Npgsql.NpgsqlParameter("@numRedirectConfigID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _RedirectConfigID

                arParms(3) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _CurrentPage

                arParms(4) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _PageSize

                arParms(5) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Direction = ParameterDirection.InputOutput
                arParms(5).Value = _TotRecs

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                'ds = SqlDAL.ExecuteDataset(connString, "USP_GetRedirectConfig", arParms)
                '_TotRecs = arParms(5).Value()

                Dim objParam() As Object
                objParam = arParms.ToArray()
                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetRedirectConfig", objParam, True)
                _TotRecs = DirectCast(objParam(5).Value, Integer)

                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteRedirectConfig() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _SiteID

                arParms(2) = New Npgsql.NpgsqlParameter("@numRedirectConfigID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _RedirectConfigID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteRedirectConfig", arParms)
                Return True

            Catch ex As Exception
                Throw ex

            End Try

        End Function


        Public Function ManageRedirectConfig() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numRedirectConfigID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _RedirectConfigID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ContactID

                arParms(3) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _SiteID

                arParms(4) = New Npgsql.NpgsqlParameter("@vcOldUrl", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(4).Value = _OldUrl

                arParms(5) = New Npgsql.NpgsqlParameter("@vcNewUrl ", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(5).Value = _NewUrl

                arParms(6) = New Npgsql.NpgsqlParameter("@numRedirectType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _RedirectType

                arParms(7) = New Npgsql.NpgsqlParameter("@numReferenceType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _ReferenceType

                arParms(8) = New Npgsql.NpgsqlParameter("@numReferenceNo", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = _ReferenceID

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageRedirectConfig", objParam, True)
                _BreadCrumbID = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

                If _BreadCrumbID <= 0 Then
                    Return False
                End If

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Sub PublishSitePagesByTemplate(ByVal templateID As Long)
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numRedirectConfigID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = templateID

                arParms(2) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = SiteID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim dt As DataTable = SqlDAL.ExecuteDatable(connString, "USP_SitePages_GetByTemplateID", arParms)

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    For Each dr As DataRow In dt.Rows
                        ManageAspxPages(CCommon.ToLong(dr("numSiteID")), CCommon.ToString(dr("vcPageURL")), CCommon.ToBool(dr("bitIsMaintainScroll")))
                    Next
                End If

            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetCategoryFromName(ByVal category As String) As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcCategotyName", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(1).Value = category

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_Category_GetByName", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function


        Public Function GetSiteCategories() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = SiteID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_Sites_GetCategoties", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetSiteRelationProfileSettings() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = SiteID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_EcommerceRelationshipProfile_GetBySiteID", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub InsertSiteRelationProfileSettings(ByVal relationship As Long, ByVal profile As Long, ByVal itemClassificationIds As String)
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = SiteID

                arParms(1) = New Npgsql.NpgsqlParameter("@numRelationship", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = relationship

                arParms(2) = New Npgsql.NpgsqlParameter("@numProfile", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = profile

                arParms(3) = New Npgsql.NpgsqlParameter("@vcItemClassificationIds", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = itemClassificationIds

                SqlDAL.ExecuteNonQuery(connString, "USP_EcommerceRelationshipProfile_Save", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub DeleteSiteRelationProfileSettings(ByVal selectedIds As String)
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = SiteID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcIds", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(1).Value = selectedIds

                SqlDAL.ExecuteNonQuery(connString, "USP_EcommerceRelationshipProfile_Delete", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Shared Function GetSettingFieldValue(ByVal lngDomainID As Long, ByVal lngSiteID As Long, ByVal vcFieldName As String) As Object
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", lngDomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numSiteID", lngSiteID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcFieldName", vcFieldName, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteScalar(connString, "USP_Sites_GetSettingFieldValue", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetParentCategories() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numCategoryID", CategoryID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_Category_GetParentCategories", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetPageElementHtml() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numSiteID", SiteID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numElementID", ElementID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_PageElementDetail_GetHtml", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetCategoryFilters() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numCategoryID", CategoryID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_Sites_GetCategoryFilters", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetCategoryByID(ByVal categoryID As Long) As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As New DataSet

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numCategoryID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = categoryID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "usp_category_getbyid", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    Class UserLocation
        Public Property ip As String
        Public Property country_name As String
        Public Property country_code As String
        Public Property city As String
        Public Property region_name As String
        Public Property zip As String
        Public Property latitude As String
        Public Property longitude As String
    End Class

End Namespace

