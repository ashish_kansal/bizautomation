﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports BACRMAPI.DataAccessLayer
Imports BACRM.BusinessLogic.Common

Namespace BACRM.BusinessLogic.ShioppingCart
    Public Class CategoryProfile
        Inherits CBusinessBase

#Region "Public Properties"
        Public ID As Long
        Public vcName As String
        Public ListCategoryProfileSites As List(Of CategoryProfileSite)
#End Region

#Region "Public Methods"

        Public Function GetAll() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_CategoryProfile_GetAll", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub GetByIDWithSites()
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@ID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = ID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_CategoryProfile_GetWithSites", arParms)

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        ID = If(ds.Tables(0).Rows(0)("ID") Is DBNull.Value, 0, CCommon.ToLong(ds.Tables(0).Rows(0)("ID")))
                        vcName = If(ds.Tables(0).Rows(0)("vcName") Is DBNull.Value, "", CCommon.ToString(ds.Tables(0).Rows(0)("vcName")))
                    End If

                    If ds.Tables.Count > 1 AndAlso ds.Tables(1).Rows.Count > 0 Then
                        Dim objCategoryProfileSite As CategoryProfileSite
                        ListCategoryProfileSites = New List(Of CategoryProfileSite)

                        For Each dr As DataRow In ds.Tables(1).Rows
                            objCategoryProfileSite = New CategoryProfileSite
                            objCategoryProfileSite.ID = If(dr("ID") Is DBNull.Value, 0, CCommon.ToLong(dr("ID")))
                            objCategoryProfileSite.CategoryProfileID = If(dr("numCategoryProfileID") Is DBNull.Value, 0, CCommon.ToLong(dr("numCategoryProfileID")))
                            objCategoryProfileSite.SiteID = If(dr("numSiteID") Is DBNull.Value, 0, CCommon.ToLong(dr("numSiteID")))
                            objCategoryProfileSite.SiteName = If(dr("vcSiteName") Is DBNull.Value, "", CCommon.ToString(dr("vcSiteName")))

                            ListCategoryProfileSites.Add(objCategoryProfileSite)
                        Next
                    End If
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub Delete()
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@ID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = ID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_CategoryProfile_Delete", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub Save(ByVal vcSiteIds As String)
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@ID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = ID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = UserCntID

                arParms(3) = New Npgsql.NpgsqlParameter("@vcName", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = vcName

                arParms(4) = New Npgsql.NpgsqlParameter("@vcSiteIDs", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(4).Value = vcSiteIds

                SqlDAL.ExecuteNonQuery(connString, "USP_CategoryProfile_Save", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub
#End Region

    End Class
End Namespace