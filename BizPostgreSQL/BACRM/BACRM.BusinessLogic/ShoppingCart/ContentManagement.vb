﻿Imports BACRM.BusinessLogic.Item
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRM.BusinessLogic.Common
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.ShioppingCart

    Public Class ContentManagement
        Inherits BACRM.BusinessLogic.CBusinessBase
        Private _intType As Int16
        Public Property intType As Int16
            Get
                Return _intType
            End Get
            Set(value As Int16)
                _intType = value
            End Set
        End Property

        Private _numContentID As Long
        Public Property numContentID As Long
            Get
                Return _numContentID
            End Get
            Set(ByVal value As Long)
                _numContentID = value
            End Set
        End Property

        Private _numSiteId As Long
        Public Property numSiteId As Long
            Get
                Return _numSiteId
            End Get
            Set(ByVal value As Long)
                _numSiteId = value
            End Set
        End Property

        Private _numDomainID As Long
        Public Property numDomainID As Long
            Get
                Return _numDomainID
            End Get
            Set(ByVal value As Long)
                _numDomainID = value
            End Set
        End Property

        Private _numCategoryId As Long
        Public Property numCategoryId As Long
            Get
                Return _numCategoryId
            End Get
            Set(ByVal value As Long)
                _numCategoryId = value
            End Set
        End Property

        Private _numCreatedBy As Long
        Public Property numCreatedBy As Long
            Get
                Return _numCreatedBy
            End Get
            Set(ByVal value As Long)
                _numCreatedBy = value
            End Set
        End Property

        Private _intMode As Integer
        Public Property intMode As Integer
            Get
                Return _intMode
            End Get
            Set(ByVal value As Integer)
                _intMode = value
            End Set
        End Property

        Private _vcContent As String
        Public Property vcContent As String
            Get
                Return _vcContent
            End Get
            Set(ByVal value As String)
                _vcContent = value
            End Set
        End Property


        Private _bitIsSpecificContent As Boolean
        Public Property bitIsSpecificContent As Boolean
            Get
                Return _bitIsSpecificContent
            End Get
            Set(ByVal value As Boolean)
                _bitIsSpecificContent = value
            End Set
        End Property

        Private _bitIsActive As Boolean
        Public Property bitIsActive As Boolean
            Get
                Return _bitIsActive
            End Get
            Set(ByVal value As Boolean)
                _bitIsActive = value
            End Set
        End Property


        Private _vcContentKey As String
        Public Property vcContentKey As String
            Get
                Return _vcContentKey
            End Get
            Set(ByVal value As String)
                _vcContentKey = value
            End Set
        End Property

        Private _vcUrl As String
        Public Property vcUrl As String
            Get
                Return _vcUrl
            End Get
            Set(ByVal value As String)
                _vcUrl = value
            End Set
        End Property

        Private _output As String
        Public Property output As String
            Get
                Return _output
            End Get
            Set(ByVal value As String)
                _output = value
            End Set
        End Property

        Private _vcTitle As String
        Public Property vcTitle As String
            Get
                Return _vcTitle
            End Get
            Set(ByVal value As String)
                _vcTitle = value
            End Set
        End Property

        Private _vcMetaDesc As String
        Public Property vcMetaDesc As String
            Get
                Return _vcMetaDesc
            End Get
            Set(ByVal value As String)
                _vcMetaDesc = value
            End Set
        End Property
        Public Function GetEcommercePages() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numContentID", SqlDbType.BigInt)
                arParms(0).Value = numContentID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSiteId", SqlDbType.VarChar)
                arParms(1).Value = numSiteId

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainId", SqlDbType.BigInt)
                arParms(2).Value = numDomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@intMode", SqlDbType.Int)
                arParms(3).Value = intMode

                arParms(4) = New Npgsql.NpgsqlParameter("@intType", SqlDbType.Int)
                arParms(4).Value = intType

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetEcommercePages", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function ManageEcommercePages() As String
            Dim intResult As String = ""
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numContentID", numContentID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numSiteId", numSiteId, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numDomainId", numDomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@vcContent", vcContent, NpgsqlTypes.NpgsqlDbType.Varchar, 900000))
                    .Add(SqlDAL.Add_Parameter("@vcUrl", vcUrl, NpgsqlTypes.NpgsqlDbType.Varchar, 500))
                    .Add(SqlDAL.Add_Parameter("@vcPageTitle", vcTitle, NpgsqlTypes.NpgsqlDbType.Varchar, 1500))
                    .Add(SqlDAL.Add_Parameter("@vcMetaDesc", vcMetaDesc, NpgsqlTypes.NpgsqlDbType.Varchar, 900000))
                    .Add(SqlDAL.Add_Parameter("@intMode", intMode, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intType", intType, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@numCategoryId", numCategoryId, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numCreatedBy", numCreatedBy, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@output", output, NpgsqlTypes.NpgsqlDbType.Varchar, ParameterDirection.InputOutput))
                End With
                intResult = SqlDAL.ExecuteNonQuery(connString, "USP_ManageEcommercePages", sqlParams.ToArray())

            Catch ex As Exception
                intResult = ex.ToString()
                Throw ex
            End Try

            Return intResult
        End Function

        Public Function GetBizHelpContents() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@vcContentKey", SqlDbType.VarChar)
                arParms(0).Value = vcContentKey

                arParms(1) = New Npgsql.NpgsqlParameter("@bitIsSpecificContent", SqlDbType.Bit)
                arParms(1).Value = bitIsSpecificContent

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainId", SqlDbType.BigInt)
                arParms(2).Value = numDomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetBizHelpContents", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ManageBizHelpContents() As String
            Dim intResult As String = ""
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numHelpContentId", numContentID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numDomainId", numDomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@vcContent", vcContent, NpgsqlTypes.NpgsqlDbType.Varchar, 900000))
                    .Add(SqlDAL.Add_Parameter("@vcContentKey", vcContentKey, NpgsqlTypes.NpgsqlDbType.Varchar, 500))
                    .Add(SqlDAL.Add_Parameter("@bitIsSpecificContent", bitIsSpecificContent, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitIsActive", bitIsActive, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@intMode", intMode, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@output", output, NpgsqlTypes.NpgsqlDbType.Varchar, ParameterDirection.InputOutput))
                End With
                intResult = SqlDAL.ExecuteNonQuery(connString, "USP_ManageBizHelpContents", sqlParams.ToArray())

            Catch ex As Exception
                intResult = ex.ToString()
                Throw ex
            End Try

            Return intResult
        End Function

    End Class

End Namespace