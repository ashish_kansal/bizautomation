Option Explicit On
Option Strict On

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRM.BusinessLogic.Notification
Imports System.Configuration
Imports BACRM.BusinessLogic
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.WebAPI
Imports BACRM.BusinessLogic.Workflow

Namespace BACRM.BusinessLogic.Accounting
    Public Class RecurringEvents
        Inherits BACRM.BusinessLogic.CBusinessBase

        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Siva 	DATE:21-Feb-07
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Siva 	DATE:21-Feb-07
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub

        '#End Region
        'define private vairable
        Private _RecurringId As Integer
        Private _TotalRecords As Long
        Public Property TotalRecords() As Long
            Get
                Return _TotalRecords
            End Get
            Set(ByVal value As Long)
                _TotalRecords = value
            End Set
        End Property
        Private _PageSize As Integer
        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal value As Integer)
                _PageSize = value
            End Set
        End Property
        Private _RecurringType As Short
        Public Property RecurringType() As Short
            Get
                Return _RecurringType
            End Get
            Set(ByVal value As Short)
                _RecurringType = value
            End Set
        End Property


        Private _RecurringTemplateName As String = String.Empty
        Private _IntervalType As Char = CChar("A")
        Private _IntervalDays As Int32
        Private _MonthlyType As Int32
        Private _FirstDet As Int32
        Private _WeekDays As Int32
        Private _Months As Int32
        Private _EndType As Boolean
        Private _dtStartDate As Date
        Private _dtEndDate As Date = CDate("1/1/1753")
        Private _ChecksOrCashOrJournal As Integer
        Private _AcntTypeId As Integer
        Private _CustomerOrVendorSearch As String
        Private _AccountTypeMode As Integer
        Private _PrimaryKeyId As Integer
        Private _EndTransactionType As Boolean
        Private _NoTransaction As Integer
        'Private DomainId As Integer
        Private _ClientTimeZoneOffset As Int32
        Private _OpportunityId As Integer
        Public Property OpportunityId() As Integer
            Get
                Return _OpportunityId
            End Get
            Set(ByVal value As Integer)
                _OpportunityId = value
            End Set
        End Property



        Private _OppBizDocID As Long
        Public Property OppBizDocID() As Long
            Get
                Return _OppBizDocID
            End Get
            Set(ByVal value As Long)
                _OppBizDocID = value
            End Set
        End Property


        Public Property RecurringId() As Integer
            Get
                Return _RecurringId
            End Get
            Set(ByVal Value As Integer)
                _RecurringId = Value
            End Set
        End Property

        Public Property RecurringTemplateName() As String
            Get
                Return _RecurringTemplateName
            End Get
            Set(ByVal Value As String)
                _RecurringTemplateName = Value
            End Set
        End Property

        Public Property IntervalType() As Char
            Get
                Return _IntervalType
            End Get
            Set(ByVal Value As Char)
                _IntervalType = Value
            End Set
        End Property

        Public Property IntervalDays() As Int32
            Get
                Return _IntervalDays
            End Get
            Set(ByVal Value As Int32)
                _IntervalDays = Value
            End Set
        End Property

        Public Property MonthlyType() As Int32
            Get
                Return _MonthlyType
            End Get
            Set(ByVal Value As Int32)
                _MonthlyType = Value
            End Set
        End Property

        Public Property FirstDet() As Int32
            Get
                Return _FirstDet
            End Get
            Set(ByVal Value As Int32)
                _FirstDet = Value
            End Set
        End Property

        Public Property WeekDays() As Int32
            Get
                Return _WeekDays
            End Get
            Set(ByVal Value As Int32)
                _WeekDays = Value
            End Set
        End Property

        Public Property EndType() As Boolean
            Get
                Return _EndType
            End Get
            Set(ByVal Value As Boolean)
                _EndType = Value
            End Set
        End Property

        Public Property Months() As Int32
            Get
                Return _Months
            End Get
            Set(ByVal Value As Int32)
                _Months = Value
            End Set
        End Property

        Public Property dtStartDate() As Date
            Get
                Return _dtStartDate
            End Get
            Set(ByVal value As Date)
                _dtStartDate = value
            End Set
        End Property

        Public Property dtEndDate() As Date
            Get
                Return _dtEndDate
            End Get
            Set(ByVal value As Date)
                _dtEndDate = value
            End Set
        End Property

        Public Property ChecksOrCashOrJournal() As Integer
            Get
                Return _ChecksOrCashOrJournal
            End Get
            Set(ByVal value As Integer)
                _ChecksOrCashOrJournal = value
            End Set
        End Property

        Public Property AcntTypeId() As Integer
            Get
                Return _AcntTypeId
            End Get
            Set(ByVal value As Integer)
                _AcntTypeId = value
            End Set
        End Property

        Public Property CustomerOrVendorSearch() As String
            Get
                Return _CustomerOrVendorSearch
            End Get
            Set(ByVal value As String)
                _CustomerOrVendorSearch = value
            End Set
        End Property

        Public Property AccountTypeMode() As Integer
            Get
                Return _AccountTypeMode
            End Get
            Set(ByVal value As Integer)
                _AccountTypeMode = value
            End Set
        End Property

        Public Property PrimaryKeyId() As Integer
            Get
                Return _PrimaryKeyId
            End Get
            Set(ByVal value As Integer)
                _PrimaryKeyId = value
            End Set
        End Property

        Public Property EndTransactionType() As Boolean
            Get
                Return _EndTransactionType
            End Get
            Set(ByVal value As Boolean)
                _EndTransactionType = value
            End Set
        End Property

        Public Property NoTransaction() As Integer
            Get
                Return _NoTransaction
            End Get
            Set(ByVal value As Integer)
                _NoTransaction = value
            End Set
        End Property

        'Public Property DomainId() As Integer
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal value As Integer)
        '        DomainId = value
        '    End Set
        'End Property

        Public Property ClientTimeZoneOffset() As Int32
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal value As Int32)
                _ClientTimeZoneOffset = value
            End Set
        End Property

        Public Function SaveRecurringTemplate() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(18) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numRecurringId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _RecurringId

                arParms(1) = New Npgsql.NpgsqlParameter("@varRecurringTemplateName", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(1).Value = _RecurringTemplateName

                arParms(2) = New Npgsql.NpgsqlParameter("@chrIntervalType", NpgsqlTypes.NpgsqlDbType.Char)
                arParms(2).Value = _IntervalType

                arParms(3) = New Npgsql.NpgsqlParameter("@tintIntervalDays", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _IntervalDays

                arParms(4) = New Npgsql.NpgsqlParameter("@tintMonthlyType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _MonthlyType

                arParms(5) = New Npgsql.NpgsqlParameter("@tintFirstDet", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _FirstDet

                arParms(6) = New Npgsql.NpgsqlParameter("@tintWeekDays", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(6).Value = _WeekDays


                arParms(7) = New Npgsql.NpgsqlParameter("@tintMonths", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(7).Value = _Months


                arParms(8) = New Npgsql.NpgsqlParameter("@dtStartDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(8).Value = _dtStartDate

                arParms(9) = New Npgsql.NpgsqlParameter("@dtEndDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(9).Value = _dtEndDate

                arParms(10) = New Npgsql.NpgsqlParameter("@bitEndType", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(10).Value = _EndType

                arParms(11) = New Npgsql.NpgsqlParameter("@bitEndTransactionType", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(11).Value = _EndTransactionType

                arParms(12) = New Npgsql.NpgsqlParameter("@numtNoTransaction", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(12).Value = _NoTransaction

                arParms(13) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(13).Value = DomainID

                arParms(14) = New Npgsql.NpgsqlParameter("@intBillingBreakup", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(14).Value = _BillingBreakUp

                arParms(15) = New Npgsql.NpgsqlParameter("@vcBreakupPercentage", NpgsqlTypes.NpgsqlDbType.VarChar, 2000)
                arParms(15).Value = _BreakUpPercentage

                arParms(16) = New Npgsql.NpgsqlParameter("@bitBillingTerms", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(16).Value = _IsBillingTerm

                arParms(17) = New Npgsql.NpgsqlParameter("@numBillingDays", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(17).Value = _BillingDays

                arParms(18) = New Npgsql.NpgsqlParameter("@bitBizDocBreakup", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(18).Value = _EnableBreakup

                SqlDAL.ExecuteNonQuery(connString, "USP_InsertRecurringTemplate", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private _BreakupPercentageDec As Decimal
        Public Property BreakupPercentageDec() As Decimal
            Get
                Return _BreakupPercentageDec
            End Get
            Set(ByVal value As Decimal)
                _BreakupPercentageDec = value
            End Set
        End Property

        Private _BreakUpPercentage As String
        Public Property BreakupPercentage() As String
            Get
                Return _BreakUpPercentage
            End Get
            Set(ByVal value As String)
                _BreakUpPercentage = value
            End Set
        End Property

        Private _IsBillingTerm As Boolean
        Public Property IsBillingTerm() As Boolean
            Get
                Return _IsBillingTerm
            End Get
            Set(ByVal value As Boolean)
                _IsBillingTerm = value
            End Set
        End Property

        Private _BillingDays As Long
        Public Property BillingDays() As Long
            Get
                Return _BillingDays
            End Get
            Set(ByVal value As Long)
                _BillingDays = value
            End Set
        End Property

        Private _BillingBreakUp As Integer
        Public Property BillingBreakup() As Integer
            Get
                Return _BillingBreakUp
            End Get
            Set(ByVal value As Integer)
                _BillingBreakUp = value
            End Set
        End Property

        Private _EnableBreakup As Boolean
        Public Property EnableBreakup() As Boolean
            Get
                Return _EnableBreakup
            End Get
            Set(ByVal value As Boolean)
                _EnableBreakup = value
            End Set
        End Property

        Public Function GetTemplateDetails() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim ds As DataSet
                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@bitBizDocBreakup", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(1).Value = _IsBillingTerm

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_RecurringTemplateList", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetRecurringTransactionReport(ByVal numCurrentPage As Integer) As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim ds As DataSet
                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID
                arParms(1) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = numCurrentPage
                arParms(2) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _PageSize

                arParms(3) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Direction = ParameterDirection.Output
                arParms(3).Value = _TotalRecords

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_RecurringTransactionReport", arParms)
                _TotalRecords = CLng(ds.Tables(1).Rows(0).Item(0).ToString())
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetRecurringTransactionDetailReport() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim ds As DataSet
                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_RecurringTransactionDetailReport", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetOpportunityRecurringList() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim ds As DataSet
                arParms(0) = New Npgsql.NpgsqlParameter("@numSeedOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OpportunityId
                arParms(1) = New Npgsql.NpgsqlParameter("@tintRecType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _RecurringType
                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetOpportunityRecurringList", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetRecurringTemplateDet() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numRecurringId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _RecurringId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_RecurringTemplateDetails", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageOpportunityRecurring(ByVal blnAmt As Boolean, ByVal intRecurringType As Integer) As String
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(10) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OppBizDocID

                arParms(2) = New Npgsql.NpgsqlParameter("@numRecurringId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _RecurringId

                arParms(3) = New Npgsql.NpgsqlParameter("@tintRecurringType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = intRecurringType

                arParms(4) = New Npgsql.NpgsqlParameter("@dtRecurringDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(4).Value = _dtStartDate

                arParms(5) = New Npgsql.NpgsqlParameter("@bitRecurringZeroAmt", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(5).Value = blnAmt

                arParms(6) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = DomainID

                arParms(7) = New Npgsql.NpgsqlParameter("@bitBillingTerms", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(7).Value = _IsBillingTerm

                arParms(8) = New Npgsql.NpgsqlParameter("@numBillingDays", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = _BillingDays

                arParms(9) = New Npgsql.NpgsqlParameter("@fltBreakupPercentage", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(9).Value = _BreakupPercentageDec

                arParms(10) = New Npgsql.NpgsqlParameter("@ValidationMessage", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(10).Direction = ParameterDirection.Output
                arParms(10).Value = DBNull.Value

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_RecurringAddOpportunity", objParam, True)
                Return Convert.ToString(DirectCast(objParam, Npgsql.NpgsqlParameter())(10).Value)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetRecurringTemplateId() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetRecurringTemplateID", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function DeleteRecurringTemplate() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numRecurringId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = _RecurringId

                arparms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(1).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteRecurringTemplate", arparms)
                Return True
            Catch ex As Exception
                Return False
                Throw ex
            End Try
        End Function
        Public Function DeleteRecurringRecord() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numOppRecID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = _RecurringId

                arparms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(1).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteRecurringRecord", arparms)
                Return True
            Catch ex As Exception
                Return False
                Throw ex
            End Try
        End Function

        Public Function UpdateRecurringTemplateForRecurringTransaction() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numAccountTypeMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arparms(0).Value = _AccountTypeMode  ' 1 - Journal 2-Checks, 3-CashCreditCard, 4--OpportunityMaster

                arparms(1) = New Npgsql.NpgsqlParameter("@numPrimaryKeyId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(1).Value = _PrimaryKeyId

                arparms(2) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(2).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateRecurringTemplateForRecurringTransaction", arparms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function BillRemainingBalance() As Integer
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = _OpportunityId

                arparms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(1).Value = DomainID

                arparms(2) = New Npgsql.NpgsqlParameter("@numOppRecID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(2).Direction = ParameterDirection.InputOutput
                arparms(2).Value = _RecurringId

                Dim objParam() As Object
                objParam = arparms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_RecurringBillRemaining", objParam, True)
                _RecurringId = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(2).Value)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Dim objNotification As New Notification.Notification
        Dim JournalId As Long
        Dim lngDomainId, lngOppId, lngOppBizDocId, lngUserCntID, lngDivisionID As Long
        Dim objJournalEntries As New JournalEntry
        Dim ds As New DataSet
        Dim dtRecurring, dtOppBiDocDtl, dtOppBiDocItems As DataTable
        Dim objOppBizDocs As New Opportunities.OppBizDocs

        Public Sub CreateRecurringBizDocs(Optional ByVal OppRecurringID As Long = 0)
            Try
                'get all recurring biz docs for today.-- [USP_CreateRecurringOpportunityBizDocs]
                objNotification.StartDate = CDate(Date.Now.ToShortDateString)
                objNotification.EndDate = CDate(Date.Now.ToShortDateString & " 23:59:59")
                objNotification.OppRecurringID = OppRecurringID
                ds = objNotification.GetRecurringTransactions(2)
                If ds.Tables.Count > 0 Then
                    dtRecurring = ds.Tables(0)
                End If
                'Dim bitAllowPPVariance As Boolean = False

                If dtRecurring.Rows.Count > 0 Then
                    For Each dr As DataRow In dtRecurring.Rows
                        lngOppId = CCommon.ToLong(dr("numOppId"))
                        lngUserCntID = CCommon.ToLong(dr("numRecOwner"))
                        lngDomainId = CCommon.ToLong(dr("numDomainId"))
                        lngDivisionID = CCommon.ToLong(dr("numDivisionID"))
                        'bitAllowPPVariance = CCommon.ToBool(dr("bitAllowPPVariance"))
                        Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                            If CCommon.ToInteger(dr("tintRecurringType")) = 4 Then 'Deferred BizDocs : Only Create Account Journal Entries
                                lngOppBizDocId = CCommon.ToLong(dr("numOppBizDocID"))
                                objOppBizDocs.OppType = CCommon.ToInteger(dr("tintOppType"))
                                objOppBizDocs.OppId = lngOppId
                                objOppBizDocs.BizDocId = CCommon.ToLong(dr("AuthBizDocID"))
                                objOppBizDocs.UserCntID = lngUserCntID
                            Else
                                'Create Opportunity BizDocs
                                objOppBizDocs.boolRecurringBizDoc = True
                                objOppBizDocs.OppId = lngOppId
                                objOppBizDocs.OppBizDocId = 0
                                objOppBizDocs.BizDocId = CCommon.ToLong(dr("AuthBizDocID"))
                                objOppBizDocs.UserCntID = lngUserCntID
                                objOppBizDocs.OppType = CCommon.ToInteger(dr("tintOppType"))
                                objOppBizDocs.bitPartialShipment = True 'Partial fulfillment
                                'objOppBizDocs.Discount = 0
                                'objOppBizDocs.boolBillingTerms = CCommon.ToBool(dr("bitBillingTerms"))
                                objOppBizDocs.ShipCompany = 0
                                'objOppBizDocs.BillingDays = CCommon.ToInteger(dr("numBillingDays"))
                                objOppBizDocs.FromDate = CDate(dr("dtRecurringDate"))
                                'objOppBizDocs.boolInterestType = False
                                'objOppBizDocs.Interest = 0
                                'objOppBizDocs.boolDiscType = False
                                'objOppBizDocs.ShipCost = 0
                                'objOppBizDocs.ShipDoc = ddlShipDoc.SelectedValue
                                objOppBizDocs.BizDocStatus = 0
                                objOppBizDocs.BizDocName = ""
                                objOppBizDocs.vcComments = ""
                                objOppBizDocs.vcPONo = ""
                                objOppBizDocs.strBizDocItems = GetBizDocItems(lngDomainId, CCommon.ToDecimal(dr("fltBreakupPercentage")), lngOppId)
                                objOppBizDocs.byteMode = 0 'Add (1) /Remove (2) Sales Tax

                                Dim objCommon As CCommon
                                objCommon = New CCommon
                                objCommon.DomainID = lngDomainId
                                objCommon.Mode = 33
                                objCommon.Str = CCommon.ToLong(dr("AuthBizDocID")).ToString
                                objOppBizDocs.SequenceId = objCommon.GetSingleFieldValue().ToString

                                lngOppBizDocId = objOppBizDocs.SaveBizDoc()
                            End If

                            If lngOppBizDocId > 0 Then 'If successfull then Returns lngOppBizDocId 
                                '' Authorizative Accounting BizDocs

                                ''--------------------------------------------------------------------------''
                                Dim ds As New DataSet
                                objOppBizDocs.OppBizDocId = lngOppBizDocId
                                ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                                dtOppBiDocItems = ds.Tables(0)

                                Dim objCalculateDealAmount As New CalculateDealAmount
                                objCalculateDealAmount.CalculateDealAmount(lngOppId, lngOppBizDocId, CShort(objOppBizDocs.OppType), lngDomainId, dtOppBiDocItems)

                                'Create new journal id
                                objOppBizDocs.DomainID = lngDomainId
                                JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, CDate(CDate(dr("dtRecurringDate")).ToShortDateString), Description:=CStr(ds.Tables(1).Rows(0).Item("vcBizDocID")))

                                Dim objJournalEntries As New JournalEntry
                                If objOppBizDocs.OppType = 1 Then
                                    If objOppBizDocs.IsSalesClearingAccountEntryExists() Then
                                        objJournalEntries.SaveJournalEntriesSales(lngOppId, lngDomainId, dtOppBiDocItems, JournalId, lngOppBizDocId, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, lngDivisionID, CCommon.ToLong(ds.Tables(1).Rows(0).Item("numCurrencyID")), CCommon.ToDouble(ds.Tables(1).Rows(0).Item("fltExchangeRate")), DiscAcntType:=0, lngShippingMethod:=0)
                                    Else
                                        objJournalEntries.SaveJournalEntriesSalesNew(lngOppId, lngDomainId, dtOppBiDocItems, JournalId, lngOppBizDocId, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, lngDivisionID, CCommon.ToLong(ds.Tables(1).Rows(0).Item("numCurrencyID")), CCommon.ToDouble(ds.Tables(1).Rows(0).Item("fltExchangeRate")), DiscAcntType:=0, lngShippingMethod:=0)
                                    End If
                                Else
                                    If CCommon.ToBool(ds.Tables(1).Rows(0).Item("bitPPVariance")) Then
                                        objJournalEntries.SaveJournalEntriesPurchaseVariance(lngOppId, lngDomainId, dtOppBiDocItems, JournalId, lngDivisionID, lngOppBizDocId, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, CCommon.ToLong(ds.Tables(1).Rows(0).Item("numCurrencyID")), CCommon.ToDouble(ds.Tables(1).Rows(0).Item("fltExchangeRate")), CCommon.ToDouble(ds.Tables(1).Rows(0).Item("fltExchangeRateBizDoc")), DiscAcntType:=0, lngShippingMethod:=0, vcBaseCurrency:=CStr(ds.Tables(1).Rows(0).Item("vcBaseCurrency")), vcForeignCurrency:=CStr(ds.Tables(1).Rows(0).Item("vcForeignCurrency")))
                                    Else
                                        objJournalEntries.SaveJournalEntriesPurchase(lngOppId, lngDomainId, dtOppBiDocItems, JournalId, lngDivisionID, lngOppBizDocId, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, CCommon.ToLong(ds.Tables(1).Rows(0).Item("numCurrencyID")), CCommon.ToDouble(ds.Tables(1).Rows(0).Item("fltExchangeRate")), DiscAcntType:=0, lngShippingMethod:=0)
                                    End If
                                End If

                                'Update BizDocID into recurring table
                                Dim objCommon As New CCommon
                                objCommon.Mode = 6
                                objCommon.UpdateRecordID = CCommon.ToLong(dr("numOppRecID"))
                                objCommon.UpdateValueID = lngOppBizDocId
                                objCommon.UpdateSingleFieldValue()

                                'Update Deferred=0 for OpportunityBizDocs
                                objCommon.Mode = 18
                                objCommon.UpdateRecordID = lngOppBizDocId
                                objCommon.UpdateValueID = 0
                                objCommon.UpdateSingleFieldValue()
                            End If
                            If OppRecurringID = 0 Then
                                WriteMessage("----Recurring BizDoc Created--ID:" & lngOppBizDocId.ToString & " " & DateTime.Now)
                            End If

                            objTransactionScope.Complete()
                        End Using


                        ''Added By Sachin Sadhu||Date:1stMay2014
                        ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
                        ''          Using Change tracking
                        Dim objWfA As New Workflow.Workflow()
                        objWfA.DomainID = DomainID
                        objWfA.UserCntID = UserCntID
                        objWfA.RecordID = lngOppBizDocId
                        objWfA.SaveWFBizDocQueue()
                        'end of code
                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Function GetBizDocItems(ByVal DomainID As Long, ByVal decBreakupPercentage As Decimal, ByVal OppID As Long) As String
            Try
                Dim dtBizDocItems As New DataTable
                Dim strOppBizDocItems As String
                dtBizDocItems.TableName = "BizDocItems"
                dtBizDocItems.Columns.Add("OppItemID")
                dtBizDocItems.Columns.Add("Quantity")
                dtBizDocItems.Columns.Add("Notes")
                dtBizDocItems.Columns.Add("monPrice")

                Dim dtItems As DataTable
                Dim objOpportunity As New MOpportunity
                objOpportunity.OpportunityId = OppID
                objOpportunity.DomainID = DomainID
                objOpportunity.Mode = 1
                dtItems = objOpportunity.GetOrderItems().Tables(0)
                'Split Total Unit into Breakup Percentage 
                Dim dr As DataRow
                For Each row As DataRow In dtItems.Rows
                    If Not CCommon.ToBool(row("bitMappingRequired")) Then
                        dr = dtBizDocItems.NewRow
                        dr("OppItemID") = row("numoppitemtCode")
                        dr("Quantity") = CCommon.ToDecimal(row("numOriginalUnitHour")) * decBreakupPercentage / 100
                        dr("Notes") = ""
                        dr("monPrice") = CCommon.ToDecimal(row("monPrice"))
                        dtBizDocItems.Rows.Add(dr)
                    End If
                Next

                Dim dsNew As New DataSet
                dsNew.Tables.Add(dtBizDocItems)
                strOppBizDocItems = dsNew.GetXml
                dsNew.Tables.Remove(dsNew.Tables(0))
                Return strOppBizDocItems
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Shared Sub WriteMessage(ByVal Message As String)
            Try
                WebAPI.WebAPI.WriteMessage(Message, System.Configuration.ConfigurationManager.AppSettings("LogFilePath"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace
