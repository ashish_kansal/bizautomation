Option Explicit On
Option Strict On

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Namespace BACRM.BusinessLogic.Accounting
    Public Class NewFixedAsset
        Inherits BACRM.BusinessLogic.CBusinessBase

        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Siva 	DATE:22-Dec-06
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Siva 	DATE:22-Dec-06
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region

        'define private vairable
        'Private DomainId As Integer
        Private _ParentAccountId As Integer = 0
        Private _AccountType As Integer
        Private _CategoryName As String = String.Empty
        Private _CategoryDescription As String = String.Empty
        Private _numOriginalOpeningBal As Decimal
        Private _OpeningBalance As Decimal
        Private _OpeningDate As DateTime
        Private _Active As Boolean
        Private _TypeId As Integer
        Private _AccountId As Integer
        Private _BitFixed As Boolean
        Private _BitDepreciation As Boolean
        Private _OriginalCostDate As Date = New Date(1753, 1, 1)
        Private _DepreciationCostDate As Date = New Date(1753, 1, 1)
        Private _OriginalCost As Decimal
        Private _DepreciationCost As Decimal



        ''Public Property DomainId() As Integer
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Integer)
        '        DomainId = Value
        '    End Set
        'End Property

        Public Property ParentAccountId() As Integer
            Get
                Return _ParentAccountId
            End Get
            Set(ByVal Value As Integer)
                _ParentAccountId = Value
            End Set
        End Property

        Public Property AccountType() As Integer
            Get
                Return _AccountType
            End Get
            Set(ByVal Value As Integer)
                _AccountType = Value
            End Set
        End Property

        Public Property CategoryName() As String
            Get
                Return _CategoryName
            End Get
            Set(ByVal Value As String)
                _CategoryName = Value
            End Set
        End Property

        Public Property CategoryDescription() As String
            Get
                Return _CategoryDescription
            End Get
            Set(ByVal Value As String)
                _CategoryDescription = Value
            End Set
        End Property

        Public Property numOriginalOpeningBal() As Decimal
            Get
                Return _numOriginalOpeningBal
            End Get
            Set(ByVal Value As Decimal)
                _numOriginalOpeningBal = Value
            End Set
        End Property

        Public Property OpeningBalance() As Decimal
            Get
                Return _OpeningBalance
            End Get
            Set(ByVal Value As Decimal)
                _OpeningBalance = Value
            End Set
        End Property

        Public Property OpeningDate() As DateTime
            Get
                Return _OpeningDate
            End Get
            Set(ByVal Value As DateTime)
                _OpeningDate = Value
            End Set
        End Property

        Public Property Active() As Boolean
            Get
                Return _Active
            End Get
            Set(ByVal Value As Boolean)
                _Active = Value
            End Set
        End Property

        Public Property TypeId() As Integer
            Get
                Return _TypeId
            End Get
            Set(ByVal value As Integer)
                _TypeId = value
            End Set
        End Property

        Public Property AccountId() As Integer
            Get
                Return _AccountId
            End Get
            Set(ByVal value As Integer)
                _AccountId = value
            End Set
        End Property

        Public Property BitFixed() As Boolean
            Get
                Return _BitFixed
            End Get
            Set(ByVal Value As Boolean)
                _BitFixed = Value
            End Set
        End Property

        Public Property BitDepreciation() As Boolean
            Get
                Return _BitDepreciation
            End Get
            Set(ByVal Value As Boolean)
                _BitDepreciation = Value
            End Set
        End Property

        Public Property OriginalCostDate() As Date
            Get
                Return _OriginalCostDate
            End Get
            Set(ByVal Value As Date)
                _OriginalCostDate = Value
            End Set
        End Property


        Public Property DepreciationCostDate() As Date
            Get
                Return _DepreciationCostDate
            End Get
            Set(ByVal Value As Date)
                _DepreciationCostDate = Value
            End Set
        End Property

        Public Property OriginalCost() As Decimal
            Get
                Return _OriginalCost
            End Get
            Set(ByVal Value As Decimal)
                _OriginalCost = Value
            End Set
        End Property

        Public Property DepreciationCost() As Decimal
            Get
                Return _DepreciationCost
            End Get
            Set(ByVal Value As Decimal)
                _DepreciationCost = Value
            End Set
        End Property


        Public Function InsertFixedAssetInChartAcnt() As Long
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Try

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(15) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numParntAcntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ParentAccountId

                arParms(1) = New Npgsql.NpgsqlParameter("@numAcntType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _AccountType

                arParms(2) = New Npgsql.NpgsqlParameter("@vcCatgyName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(2).Value = _CategoryName

                arParms(3) = New Npgsql.NpgsqlParameter("@vcCatgyDescription", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(3).Value = _CategoryDescription

                arParms(4) = New Npgsql.NpgsqlParameter("@numOriginalOpeningBal", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(4).Value = _numOriginalOpeningBal


                arParms(5) = New Npgsql.NpgsqlParameter("@numOpeningBal", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(5).Value = _OpeningBalance

                arParms(6) = New Npgsql.NpgsqlParameter("@dtOpeningDate", NpgsqlTypes.NpgsqlDbType.Timestamp, 50)
                arParms(6).Value = _OpeningDate

                arParms(7) = New Npgsql.NpgsqlParameter("@bitActive", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(7).Value = _Active

                arParms(8) = New Npgsql.NpgsqlParameter("@numAccountId", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(8).Value = _AccountId

                arParms(9) = New Npgsql.NpgsqlParameter("@bitFixed", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(9).Value = _BitFixed

                arParms(10) = New Npgsql.NpgsqlParameter("@bitDepreciation", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(10).Value = _BitDepreciation

                arParms(11) = New Npgsql.NpgsqlParameter("@dtOriginalCostDate", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(11).Value = _OriginalCostDate

                arParms(12) = New Npgsql.NpgsqlParameter("@dtDepreciationCostDate", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(12).Value = _DepreciationCostDate

                arParms(13) = New Npgsql.NpgsqlParameter("@monOriginalCost", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(13).Value = _OriginalCost

                arParms(14) = New Npgsql.NpgsqlParameter("@monDepreciationCost", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(14).Value = _DepreciationCost

                arParms(15) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(15).Value = DomainId


                SqlDAL.ExecuteNonQuery(connString, "usp_InsertFixedAssetCOA", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetParentCategoryForSelType() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numAcntType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _AccountType

                arParms(1) = New Npgsql.NpgsqlParameter("@numAccountId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _AccountId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetParentCategoryForSelType", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace

