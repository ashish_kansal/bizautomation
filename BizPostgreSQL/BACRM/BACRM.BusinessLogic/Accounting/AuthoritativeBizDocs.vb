Option Explicit On
Option Strict On

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Namespace BACRM.BusinessLogic.Accounting

    Public Class AuthoritativeBizDocs
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Constructor"
        '**********************************************************************************
        ' Name         : New
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal intUserId As Int32               
        ' Description  : The constructor                
        ' Notes        : N/A                
        ' Created By   : Siva 	DATE:17-May-07
        '**********************************************************************************
        'Public Sub New(ByVal intUserId As Integer)
        '    'Constructor
        '    MyBase.New(intUserId)
        'End Sub

        '**********************************************************************************
        ' Name         : New
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal intUserId As Int32              
        ' Description  : The constructor                
        ' Notes        : N/A                
        ' Created By   : Siva 	DATE:17-May-07
        '**********************************************************************************
        'Public Sub New()
        '    'Constructor
        '    MyBase.New()
        'End Sub

#End Region

        'define private vairable
        'Private DomainId As Long
        Private _BizDocsId As Long
        Private _OppId As Long
        Private _Amount As Decimal
        Private _Entry_Date As Date = New Date(1753, 1, 1)
        Private _JournalId As Long
        Private _RecurringId As Long
        'Private UserCntID As Long
        Private _OppBIzDocID As Long
        Private _JournalDetails As String = String.Empty
        Private _Mode As Int16
        Private _RecurringMode As Int16
        Private _chBizDocItems As String
        Private _AccountTypeId As Long


        ''Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal value As Long)
        '        DomainId = value
        '    End Set
        'End Property

        Public Property BizDocsId() As Long
            Get
                Return _BizDocsId
            End Get
            Set(ByVal value As Long)
                _BizDocsId = value
            End Set
        End Property

        Public Property OppId() As Long
            Get
                Return _OppId
            End Get
            Set(ByVal value As Long)
                _OppId = value
            End Set
        End Property

        Public Property Amount() As Decimal
            Get
                Return _Amount
            End Get
            Set(ByVal value As Decimal)
                _Amount = value
            End Set
        End Property

        Public Property Entry_Date() As DateTime
            Get
                Return _Entry_Date
            End Get
            Set(ByVal Value As DateTime)
                _Entry_Date = Value
            End Set
        End Property

        Public Property JournalId() As Long
            Get
                Return _JournalId
            End Get
            Set(ByVal value As Long)
                _JournalId = value
            End Set
        End Property

        Public Property RecurringId() As Long
            Get
                Return _RecurringId
            End Get
            Set(ByVal value As Long)
                _RecurringId = value
            End Set
        End Property

        'Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal value As Long)
        '        UserCntID = value
        '    End Set
        'End Property

        Public Property OppBIzDocID() As Long
            Get
                Return _OppBIzDocID
            End Get
            Set(ByVal value As Long)
                _OppBIzDocID = value
            End Set
        End Property

        Public Property JournalDetails() As String
            Get
                Return _JournalDetails
            End Get
            Set(ByVal value As String)
                _JournalDetails = value
            End Set
        End Property

        Public Property Mode() As Int16
            Get
                Return _Mode
            End Get
            Set(ByVal Value As Int16)
                _Mode = Value
            End Set
        End Property

        Public Property RecurringMode() As Int16
            Get
                Return _RecurringMode
            End Get
            Set(ByVal Value As Int16)
                _RecurringMode = Value
            End Set
        End Property

        Public Property chBizDocItems() As String
            Get
                Return _chBizDocItems
            End Get
            Set(ByVal value As String)
                _chBizDocItems = value
            End Set
        End Property

        Public Property AccountTypeId() As Long
            Get
                Return _AccountTypeId
            End Get
            Set(ByVal value As Long)
                _AccountTypeId = value
            End Set
        End Property

        Private _ChargeCode As String
        Public Property ChargeCode() As String
            Get
                Return _ChargeCode
            End Get
            Set(ByVal value As String)
                _ChargeCode = value
            End Set
        End Property
        Private _BizDocsPaymentDetId As Long
        Public Property BizDocsPaymentDetId() As Long
            Get
                Return _BizDocsPaymentDetId
            End Get
            Set(ByVal value As Long)
                _BizDocsPaymentDetId = value
            End Set
        End Property

        Private _CategoryHDRID As Long
        Public Property CategoryHDRID() As Long
            Get
                Return _CategoryHDRID
            End Get
            Set(ByVal value As Long)
                _CategoryHDRID = value
            End Set
        End Property

        'Private _AccountCode As String
        'Public Property AccountCode() As String
        '    Get
        '        Return _AccountCode
        '    End Get
        '    Set(ByVal value As String)
        '        _AccountCode = value
        '    End Set
        'End Property

    End Class
End Namespace
