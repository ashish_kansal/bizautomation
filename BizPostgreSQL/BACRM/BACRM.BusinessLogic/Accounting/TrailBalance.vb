Option Explicit On
Option Strict On
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.Accounting
    Public Class TrailBalance
        Inherits BACRM.BusinessLogic.CBusinessBase

        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Siva 	DATE:31-Mar-07
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Siva 	DATE:31-Mar-07
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region
        'Define Private Vairable
        'Private DomainId As Integer
        Private _FromDate As Date
        Private _ToDate As Date
        Private _ParentAccountId As Integer
        Private _ChartAcntId As Integer

        'Public Property DomainId() As Integer
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal value As Integer)
        '        DomainId = value
        '    End Set
        'End Property

        Public Property FromDate() As Date
            Get
                Return _FromDate
            End Get
            Set(ByVal Value As Date)
                _FromDate = Value
            End Set
        End Property

        Public Property ToDate() As Date
            Get
                Return _ToDate
            End Get
            Set(ByVal Value As Date)
                _ToDate = Value
            End Set
        End Property

        Public Property ParentAccountId() As Integer
            Get
                Return _ParentAccountId
            End Get
            Set(ByVal Value As Integer)
                _ParentAccountId = Value
            End Set
        End Property

        Public Property ChartAcntId() As Integer
            Get
                Return _ChartAcntId
            End Get
            Set(ByVal value As Integer)
                _ChartAcntId = value
            End Set
        End Property

        Private _AccountClass As Long
        Public Property AccountClass() As Long
            Get
                Return _AccountClass
            End Get
            Set(ByVal value As Long)
                _AccountClass = value
            End Set
        End Property

        Public Property IsAddTotalRow As Boolean

        Public Function GetChartAcntDetails() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = DomainID

                arparms(1) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(1).Value = _FromDate

                arparms(2) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(2).Value = _ToDate

                arparms(3) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer) 'Param added by Chintan (Since Date and Time must be displayed w.r.t. the client machine)
                arparms(3).Value = Common.CCommon.ToInteger(System.Web.HttpContext.Current.Session("ClientMachineUTCTimeOffset"))

                arparms(4) = New Npgsql.NpgsqlParameter("@numAccountClass ", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(4).Value = _AccountClass

                arparms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(5).Value = Nothing
                arparms(5).Direction = ParameterDirection.InputOutput

                arparms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(6).Value = Nothing
                arparms(6).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetChartAcntDetails", arparms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetChartOfAcntsTypeId() As Long
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numAccountId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ChartAcntId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_GetAcntTypeId", arParms))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Public Function GetChildCategory() As DataTable
        '    Try
        '        Dim getconnection As New GetConnection
        '        Dim connString As String = getconnection.GetConnectionString
        '        Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

        '        arparms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arparms(0).Value = DomainId

        '        arparms(1) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
        '        arparms(1).Value = _FromDate

        '        arparms(2) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
        '        arparms(2).Value = _ToDate

        '        arparms(3) = New Npgsql.NpgsqlParameter("@numParntAcntId", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arparms(3).Value = _ParentAccountId


        '        Dim ds As DataSet
        '        ds = SqlDAL.ExecuteDataset(connString, "USP_ChildTrailBalanceList", arparms)
        '        Return ds.Tables(0)
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function

        Public Function GetChildCategory() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = DomainID

                arparms(1) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(1).Value = _FromDate

                arparms(2) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(2).Value = _ToDate

                arparms(3) = New Npgsql.NpgsqlParameter("@numParntAcntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(3).Value = _ParentAccountId

                arparms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(4).Value = Nothing
                arparms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetChartAcntChildDetails", arparms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetTrialBalance(ByVal dateFilter As String, ByVal reportColumn As String) As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet = New DataSet()

                Using connection As New Npgsql.NpgsqlConnection(connString)
                    Dim sqlAdapter As Npgsql.NpgsqlDataAdapter
                    Dim sqlCommand As New Npgsql.NpgsqlCommand
                    sqlCommand.Connection = connection
                    sqlCommand.CommandText = "usp_gettrialbalance"
                    sqlCommand.CommandType = CommandType.StoredProcedure
                    sqlCommand.CommandTimeout = 1800
                    sqlCommand.Parameters.Add(SqlDAL.Add_Parameter("v_numdomainid", DomainID, NpgsqlTypes.NpgsqlDbType.Numeric))
                    sqlCommand.Parameters.Add(SqlDAL.Add_Parameter("v_dtfromdate", IIf(dateFilter = "Custom", _FromDate, DBNull.Value), NpgsqlTypes.NpgsqlDbType.Timestamp))
                    sqlCommand.Parameters.Add(SqlDAL.Add_Parameter("v_dttodate", IIf(dateFilter = "Custom", _ToDate, DBNull.Value), NpgsqlTypes.NpgsqlDbType.Timestamp))
                    sqlCommand.Parameters.Add(SqlDAL.Add_Parameter("v_clienttimezoneoffset", Common.CCommon.ToInteger(System.Web.HttpContext.Current.Session("ClientMachineUTCTimeOffset")), NpgsqlTypes.NpgsqlDbType.Integer))
                    sqlCommand.Parameters.Add(SqlDAL.Add_Parameter("v_numaccountclass", _AccountClass, NpgsqlTypes.NpgsqlDbType.Numeric))
                    sqlCommand.Parameters.Add(SqlDAL.Add_Parameter("v_datefilter", dateFilter, NpgsqlTypes.NpgsqlDbType.Varchar))
                    sqlCommand.Parameters.Add(SqlDAL.Add_Parameter("v_reportcolumn", If(reportColumn Is Nothing, "", reportColumn), NpgsqlTypes.NpgsqlDbType.Varchar))
                    sqlCommand.Parameters.Add(SqlDAL.Add_Parameter("v_bitaddtotalrow", IsAddTotalRow, NpgsqlTypes.NpgsqlDbType.Boolean))
                    sqlCommand.Parameters.Add(SqlDAL.Add_Parameter("swv_refcur", DBNull.Value, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))

                    Dim i As Int32 = 0
                    connection.Open()
                    Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                        sqlCommand.ExecuteNonQuery()

                        For Each parm As Npgsql.NpgsqlParameter In sqlCommand.Parameters
                            If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                    Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                    sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                    ds.Tables.Add(parm.Value.ToString())
                                    sqlAdapter.Fill(ds.Tables(i))
                                    i += 1
                                End If
                            End If
                        Next

                        objTransaction.Commit()
                    End Using
                    connection.Close()
                End Using

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class
End Namespace

