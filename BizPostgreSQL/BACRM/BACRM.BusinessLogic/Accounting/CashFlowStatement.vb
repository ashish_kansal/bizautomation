Option Explicit On
Option Strict On

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRM.BusinessLogic.Common
Imports System.Collections.Generic

Public Class CashFlowStatement
    Inherits BACRM.BusinessLogic.CBusinessBase
    '#Region "Constructor"
    '    '**********************************************************************************
    '    ' Name         : New
    '    ' Type         : Sub
    '    ' Scope        : Public
    '    ' Returns      : N/A
    '    ' Parameters   : ByVal intUserId As Int32               
    '    ' Description  : The constructor                
    '    ' Notes        : N/A                
    '    ' Created By   : Siva 	DATE:26-Apr-07
    '    '**********************************************************************************
    '    Public Sub New(ByVal intUserId As Integer)
    '        'Constructor
    '        MyBase.New(intUserId)
    '    End Sub

    '    '**********************************************************************************
    '    ' Name         : New
    '    ' Type         : Sub
    '    ' Scope        : Public
    '    ' Returns      : N/A
    '    ' Parameters   : ByVal intUserId As Int32              
    '    ' Description  : The constructor                
    '    ' Notes        : N/A                
    '    ' Created By   : Siva 	DATE:26-Apr-07
    '    '**********************************************************************************
    '    Public Sub New()
    '        'Constructor
    '        MyBase.New()
    '    End Sub
    '#End Region

    'Define Private Vairable
    'Private DomainId As Integer
    Private _FromDate As Date
    Private _ToDate As Date
    Private _ParentAccountId As Integer
    Private _ChartAcntId As Integer
    Private _AcntTypeId As Integer
    Private _ByteMode As Int16

    ''Public Property DomainId() As Integer
    '    Get
    '        Return DomainId
    '    End Get
    '    Set(ByVal value As Integer)
    '        DomainId = value
    '    End Set
    'End Property

    Public Property FromDate() As Date
        Get
            Return _FromDate
        End Get
        Set(ByVal Value As Date)
            _FromDate = Value
        End Set
    End Property

    Public Property ToDate() As Date
        Get
            Return _ToDate
        End Get
        Set(ByVal Value As Date)
            _ToDate = Value
        End Set
    End Property

    Public Property ParentAccountId() As Integer
        Get
            Return _ParentAccountId
        End Get
        Set(ByVal Value As Integer)
            _ParentAccountId = Value
        End Set
    End Property

    Public Property ChartAcntId() As Integer
        Get
            Return _ChartAcntId
        End Get
        Set(ByVal value As Integer)
            _ChartAcntId = value
        End Set
    End Property

    Public Property AcntTypeId() As Integer
        Get
            Return _AcntTypeId
        End Get
        Set(ByVal value As Integer)
            _AcntTypeId = value
        End Set
    End Property

    Public Property ByteMode() As Int16
        Get
            Return _ByteMode
        End Get
        Set(ByVal value As Int16)
            _ByteMode = value
        End Set
    End Property

    Private _AccountClass As Long
    Public Property AccountClass() As Long
        Get
            Return _AccountClass
        End Get
        Set(ByVal value As Long)
            _AccountClass = value
        End Set
    End Property

    Public Function GetCashFlowDetails() As DataTable
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

            arparms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
            arparms(0).Value = DomainID

            arparms(1) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
            arparms(1).Value = _FromDate

            arparms(2) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
            arparms(2).Value = _ToDate

            arparms(3) = New Npgsql.NpgsqlParameter("@tintByteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
            arparms(3).Value = _ByteMode

            arparms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
            arparms(4).Value = Nothing
            arparms(4).Direction = ParameterDirection.InputOutput

            Dim ds As DataSet
            ds = SqlDAL.ExecuteDataset(connString, "USP_GetCashFlowDetails", arparms)
            Return ds.Tables(0)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetCashFlow() As DataSet
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

            arparms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
            arparms(0).Value = DomainID

            arparms(1) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
            arparms(1).Value = _FromDate

            arparms(2) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
            arparms(2).Value = _ToDate

            arparms(3) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer) 'Param added by Chintan (Since Date and Time must be displayed w.r.t. the client machine)
            arparms(3).Value = CCommon.ToInteger(System.Web.HttpContext.Current.Session("ClientMachineUTCTimeOffset"))

            arparms(4) = New Npgsql.NpgsqlParameter("@numAccountClass ", NpgsqlTypes.NpgsqlDbType.BigInt)
            arparms(4).Value = _AccountClass

            arparms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
            arparms(5).Value = Nothing
            arparms(5).Direction = ParameterDirection.InputOutput

            arparms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
            arparms(6).Value = Nothing
            arparms(6).Direction = ParameterDirection.InputOutput

            Return SqlDAL.ExecuteDataset(connString, "USP_GetCashFlow", arparms)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function GetCashFlowReport(ByVal dateFilter As String, ByVal reportColumn As String) As DataTable
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

            With sqlParams
                .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                .Add(SqlDAL.Add_Parameter("@dtFromDate", IIf(dateFilter = "Custom", _FromDate, Nothing), NpgsqlTypes.NpgsqlDbType.Timestamp))
                .Add(SqlDAL.Add_Parameter("@dtToDate", IIf(dateFilter = "Custom", _ToDate, Nothing), NpgsqlTypes.NpgsqlDbType.Timestamp))
                .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", CCommon.ToInteger(System.Web.HttpContext.Current.Session("ClientMachineUTCTimeOffset")), NpgsqlTypes.NpgsqlDbType.Integer))
                .Add(SqlDAL.Add_Parameter("@numAccountClass", _AccountClass, NpgsqlTypes.NpgsqlDbType.BigInt))
                .Add(SqlDAL.Add_Parameter("@DateFilter", dateFilter, NpgsqlTypes.NpgsqlDbType.BigInt))
                .Add(SqlDAL.Add_Parameter("@ReportColumn", reportColumn, NpgsqlTypes.NpgsqlDbType.Bigint))
                .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
            End With

            Return SqlDAL.ExecuteDatable(connString, "USP_GetCashFlowReport", sqlParams.ToArray())
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetBeginningAmtForCashFlow() As Decimal
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}


            arparms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
            arparms(0).Value = DomainID

            arparms(1) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
            arparms(1).Value = _FromDate

            arparms(2) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
            arparms(2).Value = _ToDate


            Return CDec(SqlDAL.ExecuteScalar(connString, "USP_GetBeginningAmtForCashFlow", arparms))
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Not Being Used 
    Public Function GetCashFlowStatement() As DataTable
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

            arparms(0) = New Npgsql.NpgsqlParameter("@numChartAcntId", NpgsqlTypes.NpgsqlDbType.BigInt)
            arparms(0).Value = _ChartAcntId

            arparms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
            arparms(1).Value = DomainID

            arparms(2) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
            arparms(2).Value = _FromDate

            arparms(3) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
            arparms(3).Value = _ToDate

            arparms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
            arparms(4).Value = Nothing
            arparms(4).Direction = ParameterDirection.InputOutput

            Dim ds As DataSet
            ds = SqlDAL.ExecuteDataset(connString, "USP_CashFlowStatementList", arparms)
            Return ds.Tables(0)
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function GetChartOfAcntsTypeId() As Long
        Try

            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

            arParms(0) = New Npgsql.NpgsqlParameter("@numAccountId", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(0).Value = _ChartAcntId

            arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(1).Value = DomainID

            arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
            arParms(2).Value = Nothing
            arParms(2).Direction = ParameterDirection.InputOutput

            Return CLng(SqlDAL.ExecuteScalar(connString, "USP_GetAcntTypeId", arParms))
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
