Option Explicit On
Option Strict On

Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRM.BusinessLogic.Common
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports Npgsql

Namespace BACRM.BusinessLogic.Accounting

    Public Class MakeDeposit
        Inherits BACRM.BusinessLogic.CBusinessBase

        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Siva 	DATE:23-May-07
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Siva 	DATE:23-May-07
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub

        '#End Region

        'Define Private Vairable
        'Private DomainId As Integer
        Private _AccountType As Integer
        Private _strWhereCondition As String
        Private _AccountId As Integer
        Private _Entry_Date As Date = New Date(1753, 1, 1)
        Private _numAmount As Decimal
        Private _JournalId As Integer
        Private _DepositId As Integer
        Private _JournalDetails As String = String.Empty
        Private _Mode As Int16
        Private _DivisionId As Integer
        Private _RecurringMode As Int16
        Private _RecurringId As Integer
        Private _AuthorizativeAccounting As Int16
        Private _OppId As Integer
        Private _CheckId As Integer
        Private _OppBizDocsId As Integer
        Private _CurrentPage As Integer
        Private _PageSize As Integer
        Private _TotalRecords As Integer

        Public Property TotalRecords() As Integer
            Get
                Return _TotalRecords
            End Get
            Set(ByVal Value As Integer)
                _TotalRecords = Value
            End Set
        End Property

        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal Value As Integer)
                _PageSize = Value
            End Set
        End Property

        Public Property CurrentPage() As Integer
            Get
                Return _CurrentPage
            End Get
            Set(ByVal Value As Integer)
                _CurrentPage = Value
            End Set
        End Property

        Private _Description As String
        Public Property Description() As String
            Get
                Return _Description
            End Get
            Set(ByVal value As String)
                _Description = value
            End Set
        End Property

        ''Public Property DomainId() As Integer
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Integer)
        '        DomainId = Value
        '    End Set
        'End Property

        Public Property AccountType() As Integer
            Get
                Return _AccountType
            End Get
            Set(ByVal Value As Integer)
                _AccountType = Value
            End Set
        End Property

        Public Property strWhereCondition() As String
            Get
                Return _strWhereCondition
            End Get
            Set(ByVal value As String)
                _strWhereCondition = value
            End Set
        End Property

        Public Property AccountId() As Integer
            Get
                Return _AccountId
            End Get
            Set(ByVal Value As Integer)
                _AccountId = Value
            End Set
        End Property

        Public Property Entry_Date() As DateTime
            Get
                Return _Entry_Date
            End Get
            Set(ByVal Value As DateTime)
                _Entry_Date = Value
            End Set
        End Property

        Public Property numAmount() As Decimal
            Get
                Return _numAmount
            End Get
            Set(ByVal Value As Decimal)
                _numAmount = Value
            End Set
        End Property

        Public Property JournalId() As Integer
            Get
                Return _JournalId
            End Get
            Set(ByVal Value As Integer)
                _JournalId = Value
            End Set
        End Property

        Public Property DepositId() As Integer
            Get
                Return _DepositId
            End Get
            Set(ByVal Value As Integer)
                _DepositId = Value
            End Set
        End Property

        Public Property JournalDetails() As String
            Get
                Return _JournalDetails
            End Get
            Set(ByVal value As String)
                _JournalDetails = value
            End Set
        End Property

        Public Property Mode() As Int16
            Get
                Return _Mode
            End Get
            Set(ByVal Value As Int16)
                _Mode = Value
            End Set
        End Property

        Public Property DivisionId() As Integer
            Get
                Return _DivisionId
            End Get
            Set(ByVal Value As Integer)
                _DivisionId = Value
            End Set
        End Property

        Public Property RecurringMode() As Int16
            Get
                Return _RecurringMode
            End Get
            Set(ByVal Value As Int16)
                _RecurringMode = Value
            End Set
        End Property

        Public Property RecurringId() As Integer
            Get
                Return _RecurringId
            End Get
            Set(ByVal value As Integer)
                _RecurringId = value
            End Set
        End Property

        'Public Property UserCntID() As Integer
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal value As Integer)
        '        UserCntID = value
        '    End Set
        'End Property


        Public Property AuthorizativeAccounting() As Int16
            Get
                Return _AuthorizativeAccounting
            End Get
            Set(ByVal value As Int16)
                _AuthorizativeAccounting = value
            End Set
        End Property

        Public Property OppId() As Integer
            Get
                Return _OppId
            End Get
            Set(ByVal value As Integer)
                _OppId = value
            End Set
        End Property

        Public Property CheckId() As Integer
            Get
                Return _CheckId
            End Get
            Set(ByVal value As Integer)
                _CheckId = value
            End Set
        End Property

        Public Property OppBizDocsId() As Integer
            Get
                Return _OppBizDocsId
            End Get
            Set(ByVal value As Integer)
                _OppBizDocsId = value
            End Set
        End Property
        Private _StrItems As String
        Public Property StrItems() As String
            Get
                Return _StrItems
            End Get
            Set(ByVal value As String)
                _StrItems = value
            End Set
        End Property
        Private _PaymentMethod As Long
        Public Property PaymentMethod() As Long
            Get
                Return _PaymentMethod
            End Get
            Set(ByVal value As Long)
                _PaymentMethod = value
            End Set
        End Property
        Private _Reference As String
        Public Property Reference() As String
            Get
                Return _Reference
            End Get
            Set(ByVal value As String)
                _Reference = value
            End Set
        End Property
        Private _Memo As String
        Public Property Memo() As String
            Get
                Return _Memo
            End Get
            Set(ByVal value As String)
                _Memo = value
            End Set
        End Property
        Private _DepositeToType As Short
        Public Property DepositeToType() As Short
            Get
                Return _DepositeToType
            End Get
            Set(ByVal value As Short)
                _DepositeToType = value
            End Set
        End Property

        Private _DepositePage As Short
        Public Property DepositePage() As Short
            Get
                Return _DepositePage
            End Get
            Set(ByVal value As Short)
                _DepositePage = value
            End Set
        End Property
        Private _TransactionHistoryID As Long
        Public Property TransactionHistoryID() As Long
            Get
                Return _TransactionHistoryID
            End Get
            Set(ByVal value As Long)
                _TransactionHistoryID = value
            End Set
        End Property

        Private _dtFromDate As DateTime
        Public Property dtFromDate() As DateTime
            Get
                Return _dtFromDate
            End Get
            Set(ByVal value As DateTime)
                _dtFromDate = value
            End Set
        End Property

        Private _dtToDate As DateTime
        Public Property dtToDate() As DateTime
            Get
                Return _dtToDate
            End Get
            Set(ByVal value As DateTime)
                _dtToDate = value
            End Set
        End Property

        Private _ReturnHeaderID As Long
        Public Property ReturnHeaderID() As Long
            Get
                Return _ReturnHeaderID
            End Get
            Set(ByVal value As Long)
                _ReturnHeaderID = value
            End Set
        End Property
        Private _CurrencyID As Long
        Public Property CurrencyID() As Long
            Get
                Return _CurrencyID
            End Get
            Set(ByVal value As Long)
                _CurrencyID = value
            End Set
        End Property
        Private _ExchangeRate As Double
        Public Property ExchangeRate() As Double
            Get
                Return _ExchangeRate
            End Get
            Set(ByVal value As Double)
                _ExchangeRate = value
            End Set
        End Property

        Private _AccountClass As Long
        Public Property AccountClass() As Long
            Get
                Return _AccountClass
            End Get
            Set(ByVal value As Long)
                _AccountClass = value
            End Set
        End Property

        Private _SortColumn As String = ""

        Public Property SortColumn() As String
            Get
                Return _SortColumn
            End Get
            Set(ByVal Value As String)
                _SortColumn = Value
            End Set
        End Property

        Private _columnSortOrder As String = ""

        Public Property columnSortOrder() As String
            Get
                Return _columnSortOrder
            End Get
            Set(ByVal Value As String)
                _columnSortOrder = Value
            End Set
        End Property

        Private _strCondition As String = ""

        Public Property strCondition() As String
            Get
                Return _strCondition
            End Get
            Set(ByVal Value As String)
                _strCondition = Value
            End Set
        End Property

        Public Function GetCheckJournalIdForDeposit() As Long
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDepositId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DepositId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_GetCheckJournalIdForDeposit", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetDepositJournalDetails() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numJournalId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _JournalId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetDepositJournalDetails", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveDataToMakeDepositDetails() As Long
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(20) {}
                Dim lngDepositeID As Long

                arParms(0) = New Npgsql.NpgsqlParameter("@numDepositId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DepositId

                arParms(1) = New Npgsql.NpgsqlParameter("@numChartAcntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _AccountId

                arParms(2) = New Npgsql.NpgsqlParameter("@datEntry_Date", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _Entry_Date

                arParms(3) = New Npgsql.NpgsqlParameter("@numAmount", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(3).Value = _numAmount

                arParms(4) = New Npgsql.NpgsqlParameter("@numRecurringId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _RecurringId

                arParms(5) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = DomainID

                arParms(6) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = UserCntID

                arParms(7) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(7).Value = _StrItems

                arParms(8) = New Npgsql.NpgsqlParameter("@numPaymentMethod", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = _PaymentMethod

                arParms(9) = New Npgsql.NpgsqlParameter("@vcReference", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(9).Value = _Reference

                arParms(10) = New Npgsql.NpgsqlParameter("@vcMemo", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(10).Value = _Memo

                arParms(11) = New Npgsql.NpgsqlParameter("@tintDepositeToType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(11).Value = _DepositeToType

                arParms(12) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(12).Value = _DivisionId

                arParms(13) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(13).Value = _Mode

                arParms(14) = New Npgsql.NpgsqlParameter("@tintDepositePage", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(14).Value = _DepositePage

                arParms(15) = New Npgsql.NpgsqlParameter("@numTransHistoryID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(15).Value = _TransactionHistoryID

                arParms(16) = New Npgsql.NpgsqlParameter("@numReturnHeaderID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(16).Value = _ReturnHeaderID

                arParms(17) = New Npgsql.NpgsqlParameter("@numCurrencyID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(17).Value = _CurrencyID

                arParms(18) = New Npgsql.NpgsqlParameter("@fltExchangeRate", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(18).Value = _ExchangeRate

                arParms(19) = New Npgsql.NpgsqlParameter("@numAccountClass", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(19).Value = _AccountClass

                arParms(20) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(20).Value = Nothing
                arParms(20).Direction = ParameterDirection.InputOutput

                lngDepositeID = CLng(SqlDAL.ExecuteScalar(connString, "USP_InsertDepositHeaderDet", arParms))
                Return lngDepositeID

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveDataToMakeDepositDetails(ByVal objTransaction As NpgsqlTransaction) As Long
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(20) {}
                Dim lngDepositeID As Long

                arParms(0) = New Npgsql.NpgsqlParameter("@numDepositId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DepositId

                arParms(1) = New Npgsql.NpgsqlParameter("@numChartAcntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _AccountId

                arParms(2) = New Npgsql.NpgsqlParameter("@datEntry_Date", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _Entry_Date

                arParms(3) = New Npgsql.NpgsqlParameter("@numAmount", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(3).Value = _numAmount

                arParms(4) = New Npgsql.NpgsqlParameter("@numRecurringId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _RecurringId

                arParms(5) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = DomainID

                arParms(6) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = UserCntID

                arParms(7) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(7).Value = _StrItems

                arParms(8) = New Npgsql.NpgsqlParameter("@numPaymentMethod", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = _PaymentMethod

                arParms(9) = New Npgsql.NpgsqlParameter("@vcReference", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(9).Value = _Reference

                arParms(10) = New Npgsql.NpgsqlParameter("@vcMemo", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(10).Value = _Memo

                arParms(11) = New Npgsql.NpgsqlParameter("@tintDepositeToType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(11).Value = _DepositeToType

                arParms(12) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(12).Value = _DivisionId

                arParms(13) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(13).Value = _Mode

                arParms(14) = New Npgsql.NpgsqlParameter("@tintDepositePage", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(14).Value = _DepositePage

                arParms(15) = New Npgsql.NpgsqlParameter("@numTransHistoryID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(15).Value = _TransactionHistoryID

                arParms(16) = New Npgsql.NpgsqlParameter("@numReturnHeaderID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(16).Value = _ReturnHeaderID

                arParms(17) = New Npgsql.NpgsqlParameter("@numCurrencyID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(17).Value = _CurrencyID

                arParms(18) = New Npgsql.NpgsqlParameter("@fltExchangeRate", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(18).Value = _ExchangeRate

                arParms(19) = New Npgsql.NpgsqlParameter("@numAccountClass", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(19).Value = _AccountClass

                arParms(20) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(20).Value = Nothing
                arParms(20).Direction = ParameterDirection.InputOutput

                If Not objTransaction Is Nothing Then
                    lngDepositeID = CLng(SqlDAL.ExecuteScalar(objTransaction, "USP_InsertDepositHeaderDet", arParms))
                Else
                    lngDepositeID = CLng(SqlDAL.ExecuteScalar(connString, "USP_InsertDepositHeaderDet", arParms))
                End If

                Return lngDepositeID

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetDepositDetails() As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(9) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDepositId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DepositId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _Mode

                arParms(3) = New Npgsql.NpgsqlParameter("@numCurrencyID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _CurrencyID

                arParms(4) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _CurrentPage

                arParms(5) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _PageSize

                arParms(6) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Direction = ParameterDirection.InputOutput
                arParms(6).Value = _TotalRecords

                arParms(7) = New Npgsql.NpgsqlParameter("@numAccountClass", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _AccountClass

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                arParms(9) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(9).Value = Nothing
                arParms(9).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetDepositDetails", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(6).Value)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetDepositDetailsWithClass() As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(12) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDepositId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DepositId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _Mode

                arParms(3) = New Npgsql.NpgsqlParameter("@numCurrencyID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _CurrencyID

                arParms(4) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _CurrentPage

                arParms(5) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _PageSize

                arParms(6) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Direction = ParameterDirection.InputOutput
                arParms(6).Value = _TotalRecords

                arParms(7) = New Npgsql.NpgsqlParameter("@numAccountClass", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _AccountClass

                arParms(8) = New Npgsql.NpgsqlParameter("@SortColumn", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(8).Value = _SortColumn

                arParms(9) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(9).Value = _columnSortOrder

                arParms(10) = New Npgsql.NpgsqlParameter("@strCondition", NpgsqlTypes.NpgsqlDbType.VarChar, 2000)
                arParms(10).Value = _strCondition

                arParms(11) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(11).Value = Nothing
                arParms(11).Direction = ParameterDirection.InputOutput

                arParms(12) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(12).Value = Nothing
                arParms(12).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetDepositDetailsWithClass", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(6).Value)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetDepositDetails(ByVal objTransaction As NpgsqlTransaction) As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(9) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDepositId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DepositId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _Mode

                arParms(3) = New Npgsql.NpgsqlParameter("@numCurrencyID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _CurrencyID

                arParms(4) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _CurrentPage

                arParms(5) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _PageSize

                arParms(6) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Direction = ParameterDirection.InputOutput
                arParms(6).Value = _TotalRecords

                arParms(7) = New Npgsql.NpgsqlParameter("@numAccountClass", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _AccountClass

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                arParms(9) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(9).Value = Nothing
                arParms(9).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                Dim ds As DataSet
                If objTransaction IsNot Nothing Then
                    ds = SqlDAL.ExecuteDataset(objTransaction, "USP_GetDepositDetails", objParam)
                Else
                    ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetDepositDetails", objParam, True)
                End If

                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(6).Value)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetMainDepositId() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim lintTransactionId As Long

                arParms(0) = New Npgsql.NpgsqlParameter("@numDepositId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DepositId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                lintTransactionId = CLng(SqlDAL.ExecuteScalar(connString, "USP_GetDepositMainDetailsId", arParms))
                Return lintTransactionId
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetOpeningBalanceForDeposit() As Decimal
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim ldecOpeningBal As Decimal

                arParms(0) = New Npgsql.NpgsqlParameter("@numAccountId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _AccountId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ldecOpeningBal = CDec(SqlDAL.ExecuteScalar(connString, "USP_GetCheckOpeningBalance", arParms))
                Return ldecOpeningBal
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetBizDocsForReceivePayment() As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDepositId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DepositId

                arParms(2) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _DivisionId

                arParms(3) = New Npgsql.NpgsqlParameter("@numCurrencyID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _CurrencyID

                arParms(4) = New Npgsql.NpgsqlParameter("@numAccountClass", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _AccountClass

                arParms(5) = New Npgsql.NpgsqlParameter("@numCurrentPage", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = CurrentPage

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetReceivePayment", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetMatchingDeposits() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@monAmount", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(0).Value = _numAmount

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _dtFromDate

                arParms(3) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(3).Value = _dtToDate

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetMatchingDeposits", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function SaveDeposite(ByVal EntryDate As DateTime, ByVal COAccountId As Integer, ByVal Amount As Decimal, ByVal strItems As String) As Long
            Try
                Dim objMakeDeposit As New MakeDeposit
                Dim lngDepositeID As Long

                With objMakeDeposit
                    .DivisionId = 0
                    .Entry_Date = EntryDate 'calMakeDeposit.SelectedDate
                    .PaymentMethod = 0
                    .DepositeToType = 1 ' Direct deposite to Bank
                    .DepositePage = 1
                    .numAmount = Amount 'Replace(hdnDepositTotalAmt.Value, ",", "")
                    .AccountId = COAccountId 'ddlDepositTo.SelectedValue
                    .RecurringId = 0
                    .DepositId = 0
                    .UserCntID = UserCntID
                    .DomainID = DomainID
                    .StrItems = strItems 'Selected Deposits entries of undeposited funds to Bank Account
                    .Mode = 1
                    .AccountClass = CCommon.ToLong(System.Web.HttpContext.Current.Session("DefaultClass"))
                    lngDepositeID = .SaveDataToMakeDepositDetails()

                    Return lngDepositeID
                End With
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        ''To Save Header details in General_Journal_Header table
        Public Function SaveDataToHeader(ByVal EntryDate As Date, ByVal Description As String, ByVal Amount As Decimal, ByVal COAccountId As Long,
                                          ByVal COAccountName As String, ByVal lngDepositeID As Long) As Long
            Try
                Dim objJEHeader As New JournalEntryHeader
                Dim lngJournalID As Long

                With objJEHeader
                    .JournalId = 0 'TransactionInfo1.JournalID 'for Edit Only
                    .RecurringId = 0
                    .EntryDate = EntryDate 'CDate(calMakeDeposit.SelectedDate & " 12:00:00")
                    .Description = Description '"Deposit: " & COAccountName 'ddlDepositTo.SelectedItem.Text
                    .Amount = Amount ' CCommon.ToDecimal(hdnDepositTotalAmt.Value)
                    .CheckId = 0
                    .CashCreditCardId = 0
                    .ChartAcntId = COAccountId 'ddlDepositTo.SelectedValue
                    .OppId = 0
                    .OppBizDocsId = 0
                    .DepositId = lngDepositeID
                    .BizDocsPaymentDetId = 0
                    .IsOpeningBalance = CCommon.ToBool(0)
                    .LastRecurringDate = Date.Now
                    .NoTransactions = 0
                    .CategoryHDRID = 0
                    .ReturnID = 0
                    .CheckHeaderID = 0
                    .BillID = 0
                    .BillPaymentID = 0
                    .UserCntID = UserCntID
                    .DomainID = DomainID
                    .ReconcileID = 0
                End With
                lngJournalID = objJEHeader.Save()
                Return lngJournalID
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Sub SaveDataToGeneralJournalDetails(ByVal lngDepositeID As Long, ByVal lngJournalID As Long, ByVal COAccountId As Long, ByVal COAccountName As String)
            'Dim ds As New DataSet
            Dim dtrow As DataRow
            Dim objJE As JournalEntryNew
            Dim objJEList As New JournalEntryCollection
            Dim dtItems As DataTable
            Try
                Dim objMakeDeposit As New MakeDeposit
                objMakeDeposit.DomainID = DomainID
                objMakeDeposit.DepositId = CCommon.ToInteger(lngDepositeID)
                objMakeDeposit.Mode = 0
                dtItems = objMakeDeposit.GetDepositDetails().Tables(1)

                For Each dr As DataRow In dtItems.Rows
                    objJE = New JournalEntryNew()
                    objJE.TransactionId = CCommon.ToLong(dr("numTransactionID")) 'CCommon.ToLong(hfHeaderTransactionId.Value)
                    objJE.DebitAmt = 0
                    objJE.CreditAmt = Math.Abs(CCommon.ToDecimal(dr("monAmountPaid")))
                    If CCommon.ToLong(dr("numChildDepositID")) > 0 Then
                        objJE.ChartAcntId = ChartOfAccounting.GetDefaultAccount("UF", DomainID) 'Undeposited funds to bank account
                    Else ' New deposit
                        objJE.ChartAcntId = CCommon.ToLong(dr("numAccountID"))
                    End If
                    objJE.Description = CCommon.ToString(dr("vcMemo"))
                    objJE.CustomerId = CCommon.ToLong(dr("numReceivedFrom"))
                    objJE.MainDeposit = CCommon.ToBool(0)
                    objJE.MainCheck = CCommon.ToBool(0)
                    objJE.MainCashCredit = CCommon.ToBool(0)
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = ""
                    objJE.Reference = CCommon.ToString(dr("vcReference"))
                    objJE.PaymentMethod = CCommon.ToLong(dr("numPaymentMethod"))
                    objJE.Reconcile = False
                    objJE.CurrencyID = 0
                    objJE.FltExchangeRate = 0
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = CCommon.ToLong(dr("numProjectID"))
                    objJE.ClassID = CCommon.ToLong(dr("numClassID"))
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = CCommon.ToBool(1)
                    objJE.ReferenceType = CCommon.ToShort(enmReferenceType.DepositDetail)
                    objJE.ReferenceID = CCommon.ToLong(dr("numDepositeDetailID"))
                    objJEList.Add(objJE)
                    'End If
                Next



                'To Add the Debit Amount in JournalDetails table [i.e] For Bank Account Selected
                objJE = New JournalEntryNew()
                objJE.TransactionId = CCommon.ToLong(dtItems.Rows(0)("numTransactionIDHeader"))
                objJE.DebitAmt = Math.Abs(CCommon.ToDecimal(dtItems.Compute("sum(monAmountPaid)", "")))
                objJE.CreditAmt = 0
                objJE.ChartAcntId = COAccountId
                objJE.Description = CCommon.ToString("Deposit Amount to " + COAccountName)
                objJE.CustomerId = 0
                objJE.MainDeposit = CCommon.ToBool(1)
                objJE.MainCheck = CCommon.ToBool(0)
                objJE.MainCashCredit = CCommon.ToBool(0)
                objJE.OppitemtCode = 0
                objJE.BizDocItems = ""
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 0
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = CCommon.ToBool(1)
                objJE.ReferenceType = CCommon.ToShort(enmReferenceType.DepositHeader)
                objJE.ReferenceID = lngDepositeID
                objJEList.Add(objJE)

                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalID, DomainID)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function CheckBizDocAmountsForReceivePayment() As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionId

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppBizDocId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OppBizDocsId

                arParms(2) = New Npgsql.NpgsqlParameter("@numAmount", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(2).Value = _numAmount

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = DomainID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_CheckAndDepositAmountForBizdoc", arParms)
                Return ds

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Sub UpdateDepositeTransactionID()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDepositId", DepositId, NpgsqlTypes.NpgsqlDbType.BigInt, 18))
                    .Add(SqlDAL.Add_Parameter("@numTransHistoryID", TransactionHistoryID, NpgsqlTypes.NpgsqlDbType.BigInt, 18))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_DepositMaster_UpdateTransactionHistoryID", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

    End Class
End Namespace
