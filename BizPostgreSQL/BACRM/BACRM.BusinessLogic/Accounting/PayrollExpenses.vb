''Created By Siva
Option Explicit On
Option Strict On
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRM.BusinessLogic.Common
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.Accounting
    Public Class PayrollExpenses
        Inherits BACRM.BusinessLogic.CBusinessBase


        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Siva 	DATE:23-07-07
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Siva 	DATE:23-07-07
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region

        'Define private vairable
        Private _bitHourlyRate As Boolean
        Private _HourlyRate As Decimal
        Private _bitSalary As Boolean
        Private _DailyHours As Double
        Private _bitOverTime As Boolean
        Private _LimDailyHrs As Double
        Private _OverTimeRate As Decimal
        ''Private _bitMainComm As Boolean
        ''Private _MainCommPer As Decimal
        Private _bitRoleComm As Boolean
        Private _Roles As String
        Private _strEmail As String
        Private _bitPaidLeave As Boolean
        Private _PaidLeaveHrs As Double
        Private _PaidLeaveEmpHrs As Double
        Private _bitCommOwner As Boolean
        Private _bitCommAssignee As Boolean
        Private _CommAssignee As Double
        Private _CommOwner As Double
        Private _bitEmpNetAccount As Boolean
        Private _dtHired As DateTime
        Private _dtReleased As DateTime = New DateTime(1753, 1, 1)
        Private _DivisionID As Long
        Private _UserId As Long
        'Private DomainId As Long
        'Private UserCntID As Long
        Private _EmployeeId As String
        Private _EmergencyContactInfo As String
        Private _dtSalaryDateTime As DateTime = New DateTime(1753, 1, 1, 0, 0, 0)
        Private _bitPayroll As Boolean
        Private _CurrentBalancePaidLeaveHrs As Double
        Private _DepartmentId As Long
        Private _EmpRating As Double
        Private _EmpRatingDate As DateTime = New DateTime(1753, 1, 1)
        Private _NetPay As Decimal
        Private _AmtWithHeld As Decimal
        Private _LimWeeklyHrs As Double
        Private _OverTimeWeeklyRate As Decimal
        Private _bitOverTimeHrsDailyOrWeekly As Boolean
        Private _StartDate As DateTime = New DateTime(1753, 1, 1)
        Private _EndDate As DateTime = New DateTime(1753, 1, 1)
        Private _ClientTimeZoneOffset As Integer


        Private _PayrollStatus As Long
        Public Property PayrollStatus() As Long
            Get
                Return _PayrollStatus
            End Get
            Set(ByVal value As Long)
                _PayrollStatus = value
            End Set
        End Property

        Public Property bitRoleComm() As Boolean
            Get
                Return _bitRoleComm
            End Get
            Set(ByVal Value As Boolean)
                _bitRoleComm = Value
            End Set
        End Property

        ''Public Property MainCommPer() As Decimal
        ''    Get
        ''       Return _MainCommPer
        ''    End Get
        ''    Set(ByVal Value As Decimal)
        ''        _MainCommPer = Value
        ''    End Set
        ''End Property

        ''Public Property bitMainComm() As Boolean
        ''    Get
        ''        Return _bitMainComm
        ''    End Get
        ''    Set(ByVal Value As Boolean)
        ''        _bitMainComm = Value
        ''    End Set
        ''End Property

        Public Property OverTimeRate() As Decimal
            Get
                Return _OverTimeRate
            End Get
            Set(ByVal Value As Decimal)
                _OverTimeRate = Value
            End Set
        End Property

        Public Property LimDailyHrs() As Double
            Get
                Return _LimDailyHrs
            End Get
            Set(ByVal Value As Double)
                _LimDailyHrs = Value
            End Set
        End Property

        Public Property bitOverTime() As Boolean
            Get
                Return _bitOverTime
            End Get
            Set(ByVal Value As Boolean)
                _bitOverTime = Value
            End Set
        End Property

        Public Property DailyHours() As Double
            Get
                Return _DailyHours
            End Get
            Set(ByVal Value As Double)
                _DailyHours = Value
            End Set
        End Property

        Public Property bitSalary() As Boolean
            Get
                Return _bitSalary
            End Get
            Set(ByVal Value As Boolean)
                _bitSalary = Value
            End Set
        End Property

        Public Property HourlyRate() As Decimal
            Get
                Return _HourlyRate
            End Get
            Set(ByVal Value As Decimal)
                _HourlyRate = Value
            End Set
        End Property

        Public Property bitHourlyRate() As Boolean
            Get
                Return _bitHourlyRate
            End Get
            Set(ByVal Value As Boolean)
                _bitHourlyRate = Value
            End Set
        End Property

        Public Property UserId() As Long
            Get
                Return _UserId
            End Get
            Set(ByVal Value As Long)
                _UserId = Value
            End Set
        End Property

        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property

        Public Property bitPaidLeave() As Boolean
            Get
                Return _bitPaidLeave
            End Get
            Set(ByVal value As Boolean)
                _bitPaidLeave = value
            End Set
        End Property

        Public Property PaidLeaveHrs() As Double
            Get
                Return _PaidLeaveHrs
            End Get
            Set(ByVal value As Double)
                _PaidLeaveHrs = value
            End Set
        End Property

        Public Property PaidLeaveEmpHrs() As Double
            Get
                Return _PaidLeaveEmpHrs
            End Get
            Set(ByVal value As Double)
                _PaidLeaveEmpHrs = value
            End Set
        End Property


        Private _bitManagerOfOwner As Boolean
        Public Property bitManagerOfOwner() As Boolean
            Get
                Return _bitManagerOfOwner
            End Get
            Set(ByVal value As Boolean)
                _bitManagerOfOwner = value
            End Set
        End Property


        Public Property bitCommOwner() As Boolean
            Get
                Return _bitCommOwner
            End Get
            Set(ByVal value As Boolean)
                _bitCommOwner = value
            End Set
        End Property

        Public Property bitCommAssignee() As Boolean
            Get
                Return _bitCommAssignee
            End Get
            Set(ByVal value As Boolean)
                _bitCommAssignee = value
            End Set
        End Property

        Public Property CommAssignee() As Double
            Get
                Return _CommAssignee
            End Get
            Set(ByVal value As Double)
                _CommAssignee = value
            End Set
        End Property


        Private _ManagerOfOwner As Double
        Public Property ManagerOfOwner() As Double
            Get
                Return _ManagerOfOwner
            End Get
            Set(ByVal value As Double)
                _ManagerOfOwner = value
            End Set
        End Property

        Public Property CommOwner() As Double
            Get
                Return _CommOwner
            End Get
            Set(ByVal value As Double)
                _CommOwner = value
            End Set
        End Property

        Public Property bitEmpNetAccount() As Boolean
            Get
                Return _bitEmpNetAccount
            End Get
            Set(ByVal value As Boolean)
                _bitEmpNetAccount = value
            End Set
        End Property

        Public Property dtHired() As DateTime
            Get
                Return _dtHired
            End Get
            Set(ByVal value As DateTime)
                _dtHired = value
            End Set
        End Property

        Public Property dtReleased() As DateTime
            Get
                Return _dtReleased
            End Get
            Set(ByVal value As DateTime)
                _dtReleased = value
            End Set
        End Property

        ''Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal Value As Long)
        '        UserCntID = Value
        '    End Set
        'End Property

        ''Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property

        Public Property EmployeeId() As String
            Get
                Return _EmployeeId
            End Get
            Set(ByVal value As String)
                _EmployeeId = value
            End Set
        End Property

        Public Property EmergencyContactInfo() As String
            Get
                Return _EmergencyContactInfo
            End Get
            Set(ByVal value As String)
                _EmergencyContactInfo = value
            End Set
        End Property

        Public Property dtSalaryDateTime() As DateTime
            Get
                Return _dtSalaryDateTime
            End Get
            Set(ByVal value As DateTime)
                _dtSalaryDateTime = value
            End Set
        End Property

        Public Property bitPayroll() As Boolean
            Get
                Return _bitPayroll
            End Get
            Set(ByVal value As Boolean)
                _bitPayroll = value
            End Set
        End Property

        Public Property CurrentBalancePaidLeaveHrs() As Double
            Get
                Return _CurrentBalancePaidLeaveHrs
            End Get
            Set(ByVal value As Double)
                _CurrentBalancePaidLeaveHrs = value
            End Set
        End Property

        Public Property DepartmentId() As Long
            Get
                Return _DepartmentId
            End Get
            Set(ByVal value As Long)
                _DepartmentId = value
            End Set
        End Property

        Public Property EmpRating() As Double
            Get
                Return _EmpRating
            End Get
            Set(ByVal value As Double)
                _EmpRating = value
            End Set
        End Property

        Public Property EmpRatingDate() As DateTime
            Get
                Return _EmpRatingDate
            End Get
            Set(ByVal value As DateTime)
                _EmpRatingDate = value
            End Set
        End Property

        Public Property NetPay() As Decimal
            Get
                Return _NetPay
            End Get
            Set(ByVal value As Decimal)
                _NetPay = value
            End Set
        End Property

        Public Property AmtWithHeld() As Decimal
            Get
                Return _AmtWithHeld
            End Get
            Set(ByVal value As Decimal)
                _AmtWithHeld = value
            End Set
        End Property

        Public Property LimWeeklyHrs() As Double
            Get
                Return _LimWeeklyHrs
            End Get
            Set(ByVal value As Double)
                _LimWeeklyHrs = value
            End Set
        End Property

        Public Property OverTimeWeeklyRate() As Decimal
            Get
                Return _OverTimeWeeklyRate
            End Get
            Set(ByVal value As Decimal)
                _OverTimeWeeklyRate = value
            End Set
        End Property

        Public Property bitOverTimeHrsDailyOrWeekly() As Boolean
            Get
                Return _bitOverTimeHrsDailyOrWeekly
            End Get
            Set(ByVal value As Boolean)
                _bitOverTimeHrsDailyOrWeekly = value
            End Set
        End Property

        Public Property StartDate() As DateTime
            Get
                Return _StartDate
            End Get
            Set(ByVal value As DateTime)
                _StartDate = value
            End Set
        End Property

        Public Property EndDate() As DateTime
            Get
                Return _EndDate
            End Get
            Set(ByVal value As DateTime)
                _EndDate = value
            End Set
        End Property

        Public Property ClientTimeZoneOffset() As Integer
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal value As Integer)
                _ClientTimeZoneOffset = value
            End Set
        End Property

        Private _OppBizDocsId As Long
        Public Property OppBizDocsId() As Long
            Get
                Return _OppBizDocsId
            End Get
            Set(ByVal Value As Long)
                _OppBizDocsId = Value
            End Set
        End Property

        Private _ComRuleID As Long
        Public Property ComRuleID() As Long
            Get
                Return _ComRuleID
            End Get
            Set(ByVal Value As Long)
                _ComRuleID = Value
            End Set
        End Property

        Private _ItemCode As Long
        Public Property ItemCode() As Long
            Get
                Return _ItemCode
            End Get
            Set(ByVal Value As Long)
                _ItemCode = Value
            End Set
        End Property

        Private _bitCommContact As Boolean
        Public Property bitCommContact() As Boolean
            Get
                Return _bitCommContact
            End Get
            Set(ByVal value As Boolean)
                _bitCommContact = value
            End Set
        End Property

        Private _PayrollHeaderID As Long
        Public Property PayrollHeaderID() As Long
            Get
                Return _PayrollHeaderID
            End Get
            Set(ByVal Value As Long)
                _PayrollHeaderID = Value
            End Set
        End Property

        Private _PayrolllReferenceNo As Long
        Public Property PayrolllReferenceNo() As Long
            Get
                Return _PayrolllReferenceNo
            End Get
            Set(ByVal Value As Long)
                _PayrolllReferenceNo = Value
            End Set
        End Property

        Private _dtPaydate As DateTime
        Public Property dtPaydate() As DateTime
            Get
                Return _dtPaydate
            End Get
            Set(ByVal value As DateTime)
                _dtPaydate = value
            End Set
        End Property

        Private _PayrollDetailID As Long
        Public Property PayrollDetailID() As Long
            Get
                Return _PayrollDetailID
            End Get
            Set(ByVal Value As Long)
                _PayrollDetailID = Value
            End Set
        End Property

        Private _Mode As Short
        Public Property Mode() As Short
            Get
                Return _Mode
            End Get
            Set(ByVal Value As Short)
                _Mode = Value
            End Set
        End Property

        Private _strItems As String
        Public Property strItems() As String
            Get
                Return _strItems
            End Get
            Set(ByVal Value As String)
                _strItems = Value
            End Set
        End Property

        Private _CheckStatus As Long
        Public Property CheckStatus() As Long
            Get
                Return _CheckStatus
            End Get
            Set(ByVal Value As Long)
                _CheckStatus = Value
            End Set
        End Property

        Private _strExceptionMsg As String
        Public Property ExceptionMsg() As String
            Get
                Return _strExceptionMsg
            End Get
            Set(ByVal value As String)
                _strExceptionMsg = value
            End Set
        End Property

        Private _numComPayPeriodID As Long
        Public Property ComPayPeriodID() As Long
            Get
                Return _numComPayPeriodID
            End Get
            Set(ByVal value As Long)
                _numComPayPeriodID = value
            End Set
        End Property

        Private _tintPayPeriod As Integer
        Public Property PayPeriod() As Integer
            Get
                Return _tintPayPeriod
            End Get
            Set(ByVal value As Integer)
                _tintPayPeriod = value
            End Set
        End Property

        Public Property UserRightType As Integer
        Public Property UserUpdateRightType As Integer
        Public Property OppID As Long

        Public Function UpdateUserAccessDetailsForPE() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(31) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _UserId

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@bitHourlyRate", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Value = _bitHourlyRate

                arParms(4) = New Npgsql.NpgsqlParameter("@monHourlyRate", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(4).Value = _HourlyRate

                arParms(5) = New Npgsql.NpgsqlParameter("@bitSalary", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(5).Value = _bitSalary

                arParms(6) = New Npgsql.NpgsqlParameter("@numDailyHours", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(6).Value = _DailyHours

                arParms(7) = New Npgsql.NpgsqlParameter("@bitPaidLeave", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(7).Value = _bitPaidLeave

                arParms(8) = New Npgsql.NpgsqlParameter("@fltPaidLeave", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(8).Value = _PaidLeaveHrs

                arParms(9) = New Npgsql.NpgsqlParameter("@fltPaidLeaveEmpHrs", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(9).Value = _PaidLeaveEmpHrs

                arParms(10) = New Npgsql.NpgsqlParameter("@bitOverTime", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(10).Value = _bitOverTime

                arParms(11) = New Npgsql.NpgsqlParameter("@bitOverTimeHrsDailyOrWeekly", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(11).Value = _bitOverTimeHrsDailyOrWeekly

                arParms(12) = New Npgsql.NpgsqlParameter("@numLimDailHrs", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(12).Value = _LimDailyHrs

                arParms(13) = New Npgsql.NpgsqlParameter("@monOverTimeRate", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(13).Value = _OverTimeRate

                arParms(14) = New Npgsql.NpgsqlParameter("@bitManagerOfOwner", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(14).Value = _bitManagerOfOwner

                arParms(15) = New Npgsql.NpgsqlParameter("@fltManagerOfOwner", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(15).Value = _ManagerOfOwner

                arParms(16) = New Npgsql.NpgsqlParameter("@bitCommOwner", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(16).Value = _bitCommOwner

                arParms(17) = New Npgsql.NpgsqlParameter("@fltCommOwner", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(17).Value = _CommOwner

                arParms(18) = New Npgsql.NpgsqlParameter("@bitCommAssignee", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(18).Value = _bitCommAssignee

                arParms(19) = New Npgsql.NpgsqlParameter("@fltCommAssignee", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(19).Value = _CommAssignee

                arParms(20) = New Npgsql.NpgsqlParameter("@bitRoleComm", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(20).Value = _bitRoleComm

                arParms(21) = New Npgsql.NpgsqlParameter("@bitEmpNetAccount", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(21).Value = _bitEmpNetAccount

                arParms(22) = New Npgsql.NpgsqlParameter("@dtHired", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(22).Value = _dtHired

                arParms(23) = New Npgsql.NpgsqlParameter("@dtReleased", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(23).Value = _dtReleased

                arParms(24) = New Npgsql.NpgsqlParameter("@vcEmployeeId", NpgsqlTypes.NpgsqlDbType.Varchar, 150)
                arParms(24).Value = _EmployeeId

                arParms(25) = New Npgsql.NpgsqlParameter("@vcEmergencyContactInfo", NpgsqlTypes.NpgsqlDbType.Varchar, 250)
                arParms(25).Value = _EmergencyContactInfo

                arParms(26) = New Npgsql.NpgsqlParameter("@dtSalaryDateTime", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(26).Value = _dtSalaryDateTime

                arParms(27) = New Npgsql.NpgsqlParameter("@bitPayroll", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(27).Value = _bitPayroll

                arParms(28) = New Npgsql.NpgsqlParameter("@fltEmpRating", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(28).Value = _EmpRating

                arParms(29) = New Npgsql.NpgsqlParameter("@dtEmpRating", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(29).Value = _EmpRatingDate

                arParms(30) = New Npgsql.NpgsqlParameter("@monNetPay", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(30).Value = _NetPay

                arParms(31) = New Npgsql.NpgsqlParameter("@monAmtWithheld", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(31).Value = _AmtWithHeld

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateUserMasterPayrollExpense", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetPayrollLiabilityDetails() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                connString = connString.TrimEnd(CChar(";")) & ";Timeout=500"  'in seconds 

                Dim i As Int32 = 0
                Dim ds As New DataSet
                Dim sda As Npgsql.NpgsqlDataAdapter
                Using conn As New Npgsql.NpgsqlConnection(connString)
                    Dim cmd As New Npgsql.NpgsqlCommand()
                    cmd.Connection = conn
                    cmd.CommandTimeout = 500
                    cmd.CommandText = "usp_payrollliabilitylist"
                    cmd.CommandType = CommandType.StoredProcedure

                    cmd.Parameters.Add(SqlDAL.Add_Parameter("v_numdomainid", DomainID, NpgsqlTypes.NpgsqlDbType.Numeric))
                    cmd.Parameters.Add(SqlDAL.Add_Parameter("v_numdepartmentid", _DepartmentId, NpgsqlTypes.NpgsqlDbType.Numeric))
                    cmd.Parameters.Add(SqlDAL.Add_Parameter("v_numcompayperiodid", ComPayPeriodID, NpgsqlTypes.NpgsqlDbType.Numeric))
                    cmd.Parameters.Add(SqlDAL.Add_Parameter("v_clienttimezoneoffset", _ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    cmd.Parameters.Add(SqlDAL.Add_Parameter("v_numorgusercntid", UserCntID, NpgsqlTypes.NpgsqlDbType.Numeric))
                    cmd.Parameters.Add(SqlDAL.Add_Parameter("v_tintuserrighttype", UserRightType, NpgsqlTypes.NpgsqlDbType.Integer))
                    cmd.Parameters.Add(SqlDAL.Add_Parameter("v_tintuserrightforupdate", UserUpdateRightType, NpgsqlTypes.NpgsqlDbType.Integer))
                    cmd.Parameters.Add(SqlDAL.Add_Parameter("swv_refcur", DBNull.Value, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))

                    conn.Open()
                    Using objTransaction As Npgsql.NpgsqlTransaction = conn.BeginTransaction()
                        cmd.ExecuteNonQuery()

                        For Each parm As Npgsql.NpgsqlParameter In cmd.Parameters
                            If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                    Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                    sda = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), conn)
                                    ds.Tables.Add(parm.Value.ToString())
                                    sda.Fill(ds.Tables(i))
                                    i += 1
                                End If
                            End If
                        Next

                        objTransaction.Commit()
                    End Using
                    conn.Close()
                End Using

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetUserAccessDetails() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(0).Value = _UserId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_UserMasterDetails", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetTimeAndExpensesDetails() As DataSet
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numComPayPeriodID", ComPayPeriodID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", _ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDataset(connString, "USP_TimeAndExpensesDetailsForPay", sqlParams.ToArray())
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetOpportuntityCommission(ByVal mode As Short) As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numUserID", _UserId, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numComPayPeriodID", PayPeriod, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", _ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@numOppBizDocsId", _OppBizDocsId, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@tintMode", mode, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@numComRuleID", ComRuleID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numDivisionID", DivisionID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_GetOpportuntityCommission", sqlParams.ToArray())
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetBalancePaidLeave() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(0).Value = _UserId

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@decCurrentBalancePaidLeaveHrs", NpgsqlTypes.NpgsqlDbType.Double)
                arParms(3).Direction = ParameterDirection.InputOutput
                arParms(3).Value = _CurrentBalancePaidLeaveHrs

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_BalancePaidLeave", objParam, True)
                _CurrentBalancePaidLeaveHrs = Convert.ToDouble(DirectCast(objParam, Npgsql.NpgsqlParameter())(3).Value)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetPayPeriod() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return CInt(SqlDAL.ExecuteScalar(connString, "USP_GetPayPeriod", arParms))
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Function IsSalariedEmployee(ByVal lngUserID As Long, ByVal lngDomainID As Long, ByRef dblHourlyRate As Double, ByRef dblDailyHours As Double) As Boolean
            Try
                Dim dtUserAccessDetails As DataTable
                UserId = lngUserID
                DomainID = lngDomainID
                dtUserAccessDetails = GetUserAccessDetails()
                If dtUserAccessDetails.Rows.Count > 0 Then
                    If Not IsDBNull(dtUserAccessDetails.Rows(0).Item("bitSalary")) Then
                        If CBool(dtUserAccessDetails.Rows(0).Item("bitSalary")) = True Then
                            dblHourlyRate = CCommon.ToDouble(dtUserAccessDetails.Rows(0).Item("monHourlyRate"))
                            dblDailyHours = CCommon.ToDouble(dtUserAccessDetails.Rows(0).Item("numDailyHours"))
                            Return True
                        End If
                    End If
                End If
                Return False
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetUserCommission_CommRule() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numComRuleID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(2).Value = _ComRuleID

                arParms(3) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(3).Value = _ItemCode

                arParms(4) = New Npgsql.NpgsqlParameter("@bitCommContact", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(4).Value = _bitCommContact

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetUserCommission_CommRule", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ManagePayrollHeader() As Long
            Dim lngResult As Long = 0
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))

                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))

                    .Add(SqlDAL.Add_Parameter("@numPayrollHeaderID", _PayrollHeaderID, NpgsqlTypes.NpgsqlDbType.Bigint, 18, ParameterDirection.InputOutput))

                    .Add(SqlDAL.Add_Parameter("@numPayrolllReferenceNo", _PayrolllReferenceNo, NpgsqlTypes.NpgsqlDbType.Bigint))

                    .Add(SqlDAL.Add_Parameter("@dtFromDate", _StartDate, NpgsqlTypes.NpgsqlDbType.Timestamp))

                    .Add(SqlDAL.Add_Parameter("@dtToDate", _EndDate, NpgsqlTypes.NpgsqlDbType.Timestamp))

                    .Add(SqlDAL.Add_Parameter("@dtPaydate", _dtPaydate, NpgsqlTypes.NpgsqlDbType.Timestamp))

                    .Add(SqlDAL.Add_Parameter("@strItems", _strItems, NpgsqlTypes.NpgsqlDbType.Text))

                    .Add(SqlDAL.Add_Parameter("@tintMode", _Mode, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", _ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))

                    .Add(SqlDAL.Add_Parameter("@numPayrollStatus", _PayrollStatus, NpgsqlTypes.NpgsqlDbType.Integer))

                End With

                _strExceptionMsg = ""
                Dim objParam() As Object = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManagePayrollHeader", objParam, True)

                Return Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(2).Value)
            Catch ex As Exception
                lngResult = 0

                If DirectCast(ex, System.Data.SqlClient.SqlException).Class = 16 AndAlso DirectCast(ex, System.Data.SqlClient.SqlException).State = 1 Then
                    _strExceptionMsg = DirectCast(ex, System.Data.SqlClient.SqlException).Message.ToString()
                Else
                    _strExceptionMsg = ""
                    Throw ex
                End If
            End Try
        End Function
        Public Function GetPayrollDetail() As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@numPayrollHeaderID", _PayrollHeaderID, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", _ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur3", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetPayrollDetail", sqlParams.ToArray())

                Return ds

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetPayrollDetailTracking() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))

                    .Add(SqlDAL.Add_Parameter("@numPayrollHeaderID", _PayrollHeaderID, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@numPayrollDetailID", _PayrollDetailID, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@tintMode", _Mode, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", _ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetPayrollDetailTracking", sqlParams.ToArray())

                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManagePayrollTracking() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))

                    .Add(SqlDAL.Add_Parameter("@numPayrollHeaderID", _PayrollHeaderID, NpgsqlTypes.NpgsqlDbType.Bigint))

                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))

                    .Add(SqlDAL.Add_Parameter("@strItems", _strItems, NpgsqlTypes.NpgsqlDbType.Text))

                    .Add(SqlDAL.Add_Parameter("@tintMode", _Mode, NpgsqlTypes.NpgsqlDbType.Smallint))

                End With

                Dim objParam() As Object = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManagePayrollTracking", objParam, True)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetPayrollHistory() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint, 9))
                    .Add(SqlDAL.Add_Parameter("@numPayrollHeaderID", _PayrollHeaderID, NpgsqlTypes.NpgsqlDbType.Bigint, 9))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", _ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetPayrollHistory", sqlParams.ToArray())

                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManagePayrollDetail() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))

                    .Add(SqlDAL.Add_Parameter("@numPayrollHeaderID", _PayrollHeaderID, NpgsqlTypes.NpgsqlDbType.Bigint))

                    .Add(SqlDAL.Add_Parameter("@PayrollDetailID", _PayrollDetailID, NpgsqlTypes.NpgsqlDbType.Bigint))

                    .Add(SqlDAL.Add_Parameter("@numCheckStatus", _CheckStatus, NpgsqlTypes.NpgsqlDbType.Bigint))

                    .Add(SqlDAL.Add_Parameter("@tintMode", _Mode, NpgsqlTypes.NpgsqlDbType.Smallint))

                End With

                Dim objParam() As Object = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManagePayrollDetail", objParam, True)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetProjectCommission(Optional ByVal ProID As Long = 0) As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _ClientTimeZoneOffset

                arParms(3) = New Npgsql.NpgsqlParameter("@dtStartDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(3).Value = IIf(_StartDate = Nothing, Date.Today.ToShortDateString() & " 23:59:59", _StartDate)

                arParms(4) = New Npgsql.NpgsqlParameter("@dtEndDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(4).Value = IIf(_EndDate = Nothing, Date.Today.ToShortDateString() & " 23:59:59", _EndDate)

                arParms(5) = New Npgsql.NpgsqlParameter("@numProID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(5).Value = ProID

                arParms(6) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(6).Value = _Mode

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetProjectCommission", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCommissionPayPeriod() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@dtStartDate", NpgsqlTypes.NpgsqlDbType.Date)
                arParms(1).Value = _StartDate

                arParms(2) = New Npgsql.NpgsqlParameter("@dtEndDate", NpgsqlTypes.NpgsqlDbType.Date)
                arParms(2).Value = _EndDate

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_CommissionPayPeriod_Get", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function GetCommissionDatePayPeriod() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_CommissionDatePayPeriod_Get", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function CalculateCommission() As Long
            Try
                Dim payPeriodID As Long
                Dim ds As New DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                connString = connString.TrimEnd(CChar(";")) & ";Timeout=500"  'in seconds 
                Using conn As New Npgsql.NpgSqlConnection(connString)
                    Dim cmd As New Npgsql.NpgsqlCommand()
                    cmd.Connection = conn
                    cmd.CommandTimeout = 500
                    cmd.CommandText = "usp_commissionpayperiod_save"
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(SqlDAL.Add_Parameter("v_numcompayperiodid", ComPayPeriodID, NpgsqlTypes.NpgsqlDbType.Numeric))
                    cmd.Parameters.Add(SqlDAL.Add_Parameter("v_numdomainid", DomainID, NpgsqlTypes.NpgsqlDbType.Numeric))
                    cmd.Parameters.Add(SqlDAL.Add_Parameter("v_dtstart", StartDate, NpgsqlTypes.NpgsqlDbType.Date))
                    cmd.Parameters.Add(SqlDAL.Add_Parameter("v_dtend", EndDate, NpgsqlTypes.NpgsqlDbType.Date))
                    cmd.Parameters.Add(SqlDAL.Add_Parameter("v_tintpayperiod", PayPeriod, NpgsqlTypes.NpgsqlDbType.Integer))
                    cmd.Parameters.Add(SqlDAL.Add_Parameter("v_numuserid", UserCntID, NpgsqlTypes.NpgsqlDbType.Numeric))
                    cmd.Parameters.Add(SqlDAL.Add_Parameter("v_clienttimezoneoffset", _ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    cmd.Parameters.Add(SqlDAL.Add_Parameter("swv_refcur", DBNull.Value, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))


                    Dim i As Int32 = 0
                    Dim sqlAdapter As Npgsql.NpgsqlDataAdapter
                    conn.Open()
                    Using objTransaction As Npgsql.NpgsqlTransaction = conn.BeginTransaction()

                        cmd.ExecuteNonQuery()

                        For Each parm As Npgsql.NpgsqlParameter In cmd.Parameters
                            If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                    Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                    sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), conn)
                                    ds.Tables.Add(parm.Value.ToString())
                                    sqlAdapter.Fill(ds.Tables(i))
                                    i += 1
                                End If
                            End If
                        Next

                        objTransaction.Commit()

                    End Using
                    conn.Close()
                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        payPeriodID = CCommon.ToLong(ds.Tables(0).Rows(0)(0))
                    End If
                End Using

                Return payPeriodID
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub MarkCommissionAsPaidForDateRange(ByVal dtFromDate As DateTime, ByVal dtToDate As DateTime)
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = dtFromDate

                arParms(2) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = dtToDate

                SqlDAL.ExecuteNonQuery(connString, "USP_BizDocComission_MarkAsPaidForSelectedDateRange", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetCommissionSalesReturn() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numUserID", _UserId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", _ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@numComPayPeriodID", PayPeriod, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numComRuleID", ComRuleID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_GetCommissionCreditMemoOrRefundDetail", sqlParams.ToArray())
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Sub PayTimeExpense()
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numComPayPeriodID", ComPayPeriodID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcItems", strItems, NpgsqlTypes.NpgsqlDbType.VarChar))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_PayrollHeader_PayTimeExpense", sqlParams.ToArray())
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub UpdatePayrollAmountPaid()
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numComPayPeriodID", ComPayPeriodID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintMode", Mode, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@strPayrollDetail", strItems, NpgsqlTypes.NpgsqlDbType.VarChar))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_PayrollHeader_Save", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetCommissionEarnedDetail() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDivisionID", DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numComPayPeriodID", ComPayPeriodID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@tintMode", Mode, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@numOppID", OppID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppBizDocID", OppBizDocsId, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_BizDocComission_GetDetail", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

    End Class
End Namespace