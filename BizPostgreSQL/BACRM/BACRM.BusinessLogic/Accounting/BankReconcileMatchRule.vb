﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.Accounting


    Public Class BankReconcileMatchRule
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"

        Public Property ID As Long
        Public Property Name As String
        Public Property BankAccounts As String
        Public Property IsMatchAllConditions As String
        Public Property RuleConditions As String

#End Region

#Region "Public Methods"

        Public Sub Save()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt, 18))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt, 18))
                    .Add(SqlDAL.Add_Parameter("@vcName", Name, NpgsqlTypes.NpgsqlDbType.VarChar, 200))
                    .Add(SqlDAL.Add_Parameter("@vcBankAccounts", BankAccounts, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@bitMatchAllConditions", IsMatchAllConditions, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@vcConditions", RuleConditions, NpgsqlTypes.NpgsqlDbType.Text))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_BankReconcileMatchRule_Save", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub Delete()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt, 18))
                    .Add(SqlDAL.Add_Parameter("@numRuleID", ID, NpgsqlTypes.NpgsqlDbType.BigInt, 18))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_BankReconcileMatchRule_Delete", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetAll() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint, 18))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_BankReconcileMatchRule_GetAll", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub UpdatePriority(ByVal vcRules As String)
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt, 18))
                    .Add(SqlDAL.Add_Parameter("@vcRules", vcRules, NpgsqlTypes.NpgsqlDbType.Text))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_BankReconcileMatchRule_UpdateOrder", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

#End Region
    End Class
End Namespace