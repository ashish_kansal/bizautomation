Option Explicit On
Option Strict Off

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRM.BusinessLogic.Common
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.Accounting
    Public Class ChartOfAccounting
        Inherits BACRM.BusinessLogic.CBusinessBase


        Private _ParentAccountId As Integer = 0
        Private _AccountType As Integer
        Private _CategoryName As String = String.Empty
        Private _CategoryDescription As String = String.Empty
        Private _decOriginalOpeningBal As Decimal
        Private _decOpeningBalance As Decimal
        Private _OpeningDate As Date = New Date(1753, 1, 1)
        Private _Active As Boolean
        Private _TypeId As Integer
        Private _AccountId As Integer
        Private _BitFixed As Boolean
        Private _numListItemID As Integer
        Private _BitDepreciation As Boolean
        Private _OriginalCostDate As Date = New Date(1753, 1, 1)
        Private _DepreciationCostDate As Date = New Date(1753, 1, 1)
        Private _OriginalCost As Decimal
        Private _DepreciationCost As Decimal
        Private _ChildCount As Integer
        Private _JournalEntryCount As Integer

        Private _IsBankAccount As Boolean
        Public Property IsBankAccount() As Boolean
            Get
                Return _IsBankAccount
            End Get
            Set(ByVal value As Boolean)
                _IsBankAccount = value
            End Set
        End Property

        Private _StartCheckNumber As String
        Public Property StartCheckNumber() As String
            Get
                Return _StartCheckNumber
            End Get
            Set(ByVal value As String)
                _StartCheckNumber = value
            End Set
        End Property

        Private _BankRoutingNumber As String
        Public Property BankRoutingNumber() As String
            Get
                Return _BankRoutingNumber
            End Get
            Set(ByVal value As String)
                _BankRoutingNumber = value
            End Set
        End Property

        Private _IsConnected As Boolean
        Public Property IsConnected() As Boolean
            Get
                Return _IsConnected
            End Get
            Set(ByVal value As Boolean)
                _IsConnected = value
            End Set
        End Property

        Private _BankDetailID As Long
        Public Property BankDetailID() As Long
            Get
                Return _BankDetailID
            End Get
            Set(ByVal value As Long)
                _BankDetailID = value
            End Set
        End Property

        Private _AccountTypeID As Long
        Public Property AccountTypeID() As Long
            Get
                Return _AccountTypeID
            End Get
            Set(ByVal value As Long)
                _AccountTypeID = value
            End Set
        End Property

        Private _AccountTypeCode As String
        Public Property AccountTypeCode() As String
            Get
                Return _AccountTypeCode
            End Get
            Set(ByVal value As String)
                _AccountTypeCode = value
            End Set
        End Property

        Private _AccountTypeName As String
        Public Property AccountTypeName() As String
            Get
                Return _AccountTypeName
            End Get
            Set(ByVal value As String)
                _AccountTypeName = value
            End Set
        End Property

        Private _ParentAccountTypeID As Long
        Public Property ParentAccountTypeID() As Long
            Get
                Return _ParentAccountTypeID
            End Get
            Set(ByVal value As Long)
                _ParentAccountTypeID = value
            End Set
        End Property

        Private _Mode As Short
        Public Property Mode() As Short
            Get
                Return _Mode
            End Get
            Set(ByVal value As Short)
                _Mode = value
            End Set
        End Property

        Private _IsProfitLossAcnt As Boolean
        Public Property IsProfitLossAcnt() As Boolean
            Get
                Return _IsProfitLossAcnt
            End Get
            Set(ByVal value As Boolean)
                _IsProfitLossAcnt = value
            End Set
        End Property


        Private _AccountCode As String
        Public Property AccountCode() As String
            Get
                Return _AccountCode
            End Get
            Set(ByVal value As String)
                _AccountCode = value
            End Set
        End Property

        Private _COARelationshipID As Long
        Public Property COARelationshipID() As Long
            Get
                Return _COARelationshipID
            End Get
            Set(ByVal value As Long)
                _COARelationshipID = value
            End Set
        End Property

        Private _RelationshipID As Long
        Public Property RelationshipID() As Long
            Get
                Return _RelationshipID
            End Get
            Set(ByVal value As Long)
                _RelationshipID = value
            End Set
        End Property

        Private _DivisionID As Long
        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal value As Long)
                _DivisionID = value
            End Set
        End Property

        Private _ARAccountID As Long
        Public Property ARAccountID() As Long
            Get
                Return _ARAccountID
            End Get
            Set(ByVal value As Long)
                _ARAccountID = value
            End Set
        End Property

        Private _APAccountID As Long
        Public Property APAccountID() As Long
            Get
                Return _APAccountID
            End Get
            Set(ByVal value As Long)
                _APAccountID = value
            End Set
        End Property


        Private _TransChargeID As Long
        Public Property TransChargeID() As Long
            Get
                Return _TransChargeID
            End Get
            Set(ByVal value As Long)
                _TransChargeID = value
            End Set
        End Property

        Private _CreditCardTypeId As Long
        Public Property CreditCardTypeId() As Long
            Get
                Return _CreditCardTypeId
            End Get
            Set(ByVal value As Long)
                _CreditCardTypeId = value
            End Set
        End Property

        Private _TransactionCharge As Double
        Public Property TransactionCharge() As Double
            Get
                Return _TransactionCharge
            End Get
            Set(ByVal value As Double)
                _TransactionCharge = value
            End Set
        End Property
        Private _TransactionChargeBareBy As Short
        Public Property TransactionChargeBareBy() As Short
            Get
                Return _TransactionChargeBareBy
            End Get
            Set(ByVal value As Short)
                _TransactionChargeBareBy = value
            End Set
        End Property

        Private _ChartAcntIds As String
        Public Property ChartAcntIds() As String
            Get
                Return _ChartAcntIds
            End Get
            Set(ByVal value As String)
                _ChartAcntIds = value
            End Set
        End Property

        Private _FromDate As Date
        Public Property FromDate() As Date
            Get
                Return _FromDate
            End Get
            Set(ByVal value As Date)
                _FromDate = value
            End Set
        End Property

        Private _ToDate As Date
        Public Property ToDate() As Date
            Get
                Return _ToDate
            End Get
            Set(ByVal value As Date)
                _ToDate = value
            End Set
        End Property

        Private _ShippingMethodID As Long
        Public Property ShippingMethodID() As Long
            Get
                Return _ShippingMethodID
            End Get
            Set(ByVal value As Long)
                _ShippingMethodID = value
            End Set
        End Property

        Private _ShippingMappingID As Long
        Public Property ShippingMappingID() As Long
            Get
                Return _ShippingMappingID
            End Get
            Set(ByVal value As Long)
                _ShippingMappingID = value
            End Set
        End Property

        Private _FinancialYearID As Long
        Public Property FinancialYearID() As Long
            Get
                Return _FinancialYearID
            End Get
            Set(ByVal value As Long)
                _FinancialYearID = value
            End Set
        End Property
        Private _NextFinancialYearID As Long
        Public Property NextFinancialYearID() As Long
            Get
                Return _NextFinancialYearID
            End Get
            Set(ByVal value As Long)
                _NextFinancialYearID = value
            End Set
        End Property

        Private _FinDescription As String
        Public Property FinDescription() As String
            Get
                Return _FinDescription
            End Get
            Set(ByVal value As String)
                _FinDescription = value
            End Set
        End Property

        Private _bitCloseStatus As Boolean
        Public Property bitCloseStatus() As Boolean
            Get
                Return _bitCloseStatus
            End Get
            Set(ByVal value As Boolean)
                _bitCloseStatus = value
            End Set
        End Property

        Private _bitAuditStatus As Boolean
        Public Property bitAuditStatus() As Boolean
            Get
                Return _bitAuditStatus
            End Get
            Set(ByVal value As Boolean)
                _bitAuditStatus = value
            End Set
        End Property

        Private _bitCurrentYear As Boolean
        Public Property bitCurrentYear() As Boolean
            Get
                Return _bitCurrentYear
            End Get
            Set(ByVal value As Boolean)
                _bitCurrentYear = value
            End Set
        End Property

        Private _Str As String
        Public Property Str() As String
            Get
                Return _Str
            End Get
            Set(ByVal value As String)
                _Str = value
            End Set
        End Property

        Private _ARAccountTypeID As Long
        Public Property ARAccountTypeID() As Long
            Get
                Return _ARAccountTypeID
            End Get
            Set(ByVal value As Long)
                _ARAccountTypeID = value
            End Set
        End Property

        Private _APAccountTypeID As Long
        Public Property APAccountTypeID() As Long
            Get
                Return _APAccountTypeID
            End Get
            Set(ByVal value As Long)
                _APAccountTypeID = value
            End Set
        End Property

        Private _MappingID As Long
        Public Property MappingID() As Long
            Get
                Return _MappingID
            End Get
            Set(ByVal value As Long)
                _MappingID = value
            End Set
        End Property

        Private _ContactTypeID As Long
        Public Property ContactTypeID() As Long
            Get
                Return _ContactTypeID
            End Get
            Set(ByVal value As Long)
                _ContactTypeID = value
            End Set
        End Property

        ''Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property

        Public Property ParentAccountId() As Integer
            Get
                Return _ParentAccountId
            End Get
            Set(ByVal Value As Integer)
                _ParentAccountId = Value
            End Set
        End Property

        Public Property AccountType() As Integer
            Get
                Return _AccountType
            End Get
            Set(ByVal Value As Integer)
                _AccountType = Value
            End Set
        End Property

        Public Property CategoryName() As String
            Get
                Return _CategoryName
            End Get
            Set(ByVal Value As String)
                _CategoryName = Value
            End Set
        End Property

        Public Property CategoryDescription() As String
            Get
                Return _CategoryDescription
            End Get
            Set(ByVal Value As String)
                _CategoryDescription = Value
            End Set
        End Property

        Public Property decOriginalOpeningBal() As Decimal
            Get
                Return _decOriginalOpeningBal
            End Get
            Set(ByVal Value As Decimal)
                _decOriginalOpeningBal = Value
            End Set
        End Property

        Public Property decOpeningBalance() As Decimal
            Get
                Return _decOpeningBalance
            End Get
            Set(ByVal Value As Decimal)
                _decOpeningBalance = Value
            End Set
        End Property

        Public Property OpeningDate() As Date
            Get
                Return _OpeningDate
            End Get
            Set(ByVal Value As Date)
                _OpeningDate = Value
            End Set
        End Property

        Public Property Active() As Boolean
            Get
                Return _Active
            End Get
            Set(ByVal Value As Boolean)
                _Active = Value
            End Set
        End Property

        Public Property TypeId() As Integer
            Get
                Return _TypeId
            End Get
            Set(ByVal value As Integer)
                _TypeId = value
            End Set
        End Property

        Public Property AccountId() As Integer
            Get
                Return _AccountId
            End Get
            Set(ByVal value As Integer)
                _AccountId = value
            End Set
        End Property

        Public Property BitFixed() As Boolean
            Get
                Return _BitFixed
            End Get
            Set(ByVal Value As Boolean)
                _BitFixed = Value
            End Set
        End Property

        Public Property numListItemID() As Integer
            Get
                Return _numListItemID
            End Get
            Set(ByVal value As Integer)
                _numListItemID = value
            End Set
        End Property

        Public Property BitDepreciation() As Boolean
            Get
                Return _BitDepreciation
            End Get
            Set(ByVal Value As Boolean)
                _BitDepreciation = Value
            End Set
        End Property

        Public Property OriginalCostDate() As Date
            Get
                Return _OriginalCostDate
            End Get
            Set(ByVal Value As Date)
                _OriginalCostDate = Value
            End Set
        End Property


        Public Property DepreciationCostDate() As Date
            Get
                Return _DepreciationCostDate
            End Get
            Set(ByVal Value As Date)
                _DepreciationCostDate = Value
            End Set
        End Property

        Public Property OriginalCost() As Decimal
            Get
                Return _OriginalCost
            End Get
            Set(ByVal Value As Decimal)
                _OriginalCost = Value
            End Set
        End Property

        Public Property DepreciationCost() As Decimal
            Get
                Return _DepreciationCost
            End Get
            Set(ByVal Value As Decimal)
                _DepreciationCost = Value
            End Set
        End Property

        'Public Property UserCntID() As Integer
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal value As Integer)
        '        UserCntID = value
        '    End Set
        'End Property

        Public Property ChildCount() As Integer
            Get
                Return _ChildCount
            End Get
            Set(ByVal value As Integer)
                _ChildCount = value
            End Set
        End Property

        Public Property JournalEntryCount() As Integer
            Get
                Return _JournalEntryCount
            End Get
            Set(ByVal value As Integer)
                _JournalEntryCount = value
            End Set
        End Property


        Private _strNumber As String
        Public Property strNumber() As String
            Get
                Return _strNumber
            End Get
            Set(ByVal value As String)
                _strNumber = value
            End Set
        End Property

        Private _blnIsSubaccount As Boolean
        Public Property IsSubAccount() As Boolean
            Get
                Return _blnIsSubaccount
            End Get
            Set(ByVal value As Boolean)
                _blnIsSubaccount = value
            End Set
        End Property

        Private _lngParentAccountId As Long
        Public Property lngParentAccountId() As Long
            Get
                Return _lngParentAccountId
            End Get
            Set(ByVal value As Long)
                _lngParentAccountId = value
            End Set
        End Property

        Private _CheckHeaderID As Long
        Public Property CheckHeaderID() As Long
            Get
                Return _CheckHeaderID
            End Get
            Set(ByVal value As Long)
                _CheckHeaderID = value
            End Set
        End Property

        Public Property AccountClass As Long
        Public Property IsActive As Boolean

        Public Function CreateRecordAddAccountChartInfo() As Long
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Try

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(25) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numAcntTypeId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _AccountTypeID

                arParms(1) = New Npgsql.NpgsqlParameter("@numParntAcntTypeID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ParentAccountId

                arParms(2) = New Npgsql.NpgsqlParameter("@vcAccountName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(2).Value = _CategoryName

                arParms(3) = New Npgsql.NpgsqlParameter("@vcAccountDescription", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(3).Value = _CategoryDescription

                arParms(4) = New Npgsql.NpgsqlParameter("@monOriginalOpeningBal", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(4).Value = _decOriginalOpeningBal

                arParms(5) = New Npgsql.NpgsqlParameter("@monOpeningBal", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(5).Value = _decOpeningBalance

                arParms(6) = New Npgsql.NpgsqlParameter("@dtOpeningDate", NpgsqlTypes.NpgsqlDbType.Timestamp, 50)
                arParms(6).Value = _OpeningDate

                arParms(7) = New Npgsql.NpgsqlParameter("@bitActive", NpgsqlTypes.NpgsqlDbType.Boolean)
                arParms(7).Value = _Active

                arParms(8) = New Npgsql.NpgsqlParameter("@numAccountId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(8).Direction = ParameterDirection.InputOutput
                arParms(8).Value = _AccountId

                arParms(9) = New Npgsql.NpgsqlParameter("@bitFixed", NpgsqlTypes.NpgsqlDbType.Boolean)
                arParms(9).Value = _BitFixed

                arParms(10) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(10).Value = DomainID

                arParms(11) = New Npgsql.NpgsqlParameter("@numListItemID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(11).Value = _numListItemID

                arParms(12) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(12).Value = UserCntID

                arParms(13) = New Npgsql.NpgsqlParameter("@bitProfitLoss", NpgsqlTypes.NpgsqlDbType.Boolean)
                arParms(13).Value = _IsProfitLossAcnt

                arParms(14) = New Npgsql.NpgsqlParameter("@bitDepreciation", NpgsqlTypes.NpgsqlDbType.Boolean)
                arParms(14).Value = _BitDepreciation

                arParms(15) = New Npgsql.NpgsqlParameter("@dtDepreciationCostDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(15).Value = _DepreciationCostDate

                arParms(16) = New Npgsql.NpgsqlParameter("@monDepreciationCost", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(16).Value = _DepreciationCost

                arParms(17) = New Npgsql.NpgsqlParameter("@vcNumber", NpgsqlTypes.NpgsqlDbType.Varchar, 50)
                arParms(17).Value = _strNumber

                arParms(18) = New Npgsql.NpgsqlParameter("@bitIsSubAccount", NpgsqlTypes.NpgsqlDbType.Boolean)
                arParms(18).Value = _blnIsSubaccount

                arParms(19) = New Npgsql.NpgsqlParameter("@numParentAccId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(19).Value = _lngParentAccountId

                arParms(20) = New Npgsql.NpgsqlParameter("@IsBankAccount", NpgsqlTypes.NpgsqlDbType.Boolean)
                arParms(20).Value = _IsBankAccount

                arParms(21) = New Npgsql.NpgsqlParameter("@vcStartingCheckNumber", NpgsqlTypes.NpgsqlDbType.Varchar, 50)
                arParms(21).Value = _StartCheckNumber

                arParms(22) = New Npgsql.NpgsqlParameter("@vcBankRountingNumber", NpgsqlTypes.NpgsqlDbType.Varchar, 50)
                arParms(22).Value = _BankRoutingNumber

                arParms(23) = New Npgsql.NpgsqlParameter("@bitIsConnected", NpgsqlTypes.NpgsqlDbType.Boolean)
                arParms(23).Value = _IsConnected

                arParms(24) = New Npgsql.NpgsqlParameter("@numBankDetailID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(24).Value = _BankDetailID

                arParms(25) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(25).Value = Nothing
                arParms(25).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "usp_InsertNewChartAccountDetails", objParam, True)
                _AccountId = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(8).Value)

                Return _AccountId
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetTypeDetails(ByVal lngType As Long) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numTypeId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _TypeId
                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetChartType", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetParentCategory() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(0).Value = DomainID

                arparms(1) = New Npgsql.NpgsqlParameter("@vcInitialCode", NpgsqlTypes.NpgsqlDbType.Varchar, 50)
                arparms(1).Value = _AccountCode

                arparms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(2).Value = Nothing
                arparms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetParentCategory", arparms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetRootNode() As Integer
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(0).Value = DomainID

                arparms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(1).Value = Nothing
                arparms(1).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                Return CInt(SqlDAL.ExecuteScalar(connString, "USP_GetRootNode", arparms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetChartDetails() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numAcntId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _AccountId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetChartDetails", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function ChartofAccounts() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = UserCntID

                ds = SqlDAL.ExecuteDataset(connString, "USP_ChartofAccounts", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' <summary>
        ''' Created on : 25-July-2008
        ''' Author : Ajit Singh
        ''' Description : This Method is use for fetching the Child Account records against Parent Account type Id.
        ''' </summary>
        ''' <param name="mNumDomainId"></param>
        ''' <param name="mNumUserCntId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function ChartofChildAccounts(ByVal sChartSortType As String) As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@strSortOn", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Value = sChartSortType

                arParms(3) = New Npgsql.NpgsqlParameter("@strSortDirection", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(3).Value = "ASCENDING"

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_ChartofChildAccounts", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' <summary>
        ''' Created on : 28-July-2008
        ''' Author : Ajit Singh
        ''' Description : This method is use for Update the Account Type against Account Id
        ''' </summary>
        ''' <param name="numDomainId"></param>
        ''' <param name="numAccountId"></param>
        ''' <param name="numAcntType"></param>
        ''' <param name="vcCatgyName"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function RenameAcntType() As Integer
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numAcntType", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _AccountType

                arParms(2) = New Npgsql.NpgsqlParameter("@vcRenamedListName", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(2).Value = _CategoryName

                SqlDAL.ExecuteNonQuery(connString, "USP_RenameAccntType", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' <summary>
        ''' Created on : 03-September-2008
        ''' Author : Ajit Singh
        ''' Description : This Method is used for fetching the record against DomainID from DomainWiseSort Table.
        ''' </summary>
        ''' <param name="iNumDomainId"></param>
        ''' <param name="sModuleName"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function GetDomainWiseSort(ByVal iNumDomainId As Integer, ByVal sModuleName As String) As String
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim _sortOn As String = String.Empty
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = iNumDomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@vcModuleType", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(1).Value = sModuleName

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                _sortOn = TryCast(SqlDAL.ExecuteScalar(connString, "USP_GetDomainWiseSort", arParms), String)
                Return _sortOn
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' <summary>
        ''' Created on : 03-September-2008
        ''' Author : Ajit Singh
        ''' Description : This Method is used for Store Module's Sorting Order according to Doamin ID 
        ''' in DomainWiseSort Table.
        ''' </summary>
        ''' <param name="sModuleName"></param>
        ''' <param name="sSortOn"></param>
        ''' <param name="iUserId"></param>
        ''' <param name="dCurrentDate"></param>
        ''' <param name="iNumDomainId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function SaveDomainWiseSort(ByVal sModuleName As String, ByVal sSortOn As String, ByVal iUserId As Integer, ByVal dCurrentDate As Date, ByVal iNumDomainId As Integer) As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@vcModuleType", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(0).Value = sModuleName

                arParms(1) = New Npgsql.NpgsqlParameter("@vcSortOn", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(1).Value = sSortOn

                arParms(2) = New Npgsql.NpgsqlParameter("@numCreatedAndModifiedBY", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Value = iUserId

                arParms(3) = New Npgsql.NpgsqlParameter("@bintCreatedAndModifiedDate", NpgsqlTypes.NpgsqlDbType.Date)
                arParms(3).Value = dCurrentDate

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(4).Value = iNumDomainId

                SqlDAL.ExecuteNonQuery(connString, "USP_SaveDomainWiseSort", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetParentCategoryForSelType() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numAcntType", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _AccountType

                arParms(1) = New Npgsql.NpgsqlParameter("@numAccountId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _AccountId

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetParentCategoryForSelType", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCountOfParentCategory() As Integer
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numParntAcntId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _ParentAccountId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@intCount", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Direction = ParameterDirection.InputOutput
                arParms(2).Value = _ChildCount

                arParms(3) = New Npgsql.NpgsqlParameter("@intJournalEntryCount", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Direction = ParameterDirection.InputOutput
                arParms(3).Value = _JournalEntryCount


                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_GetCountOfParentCategory", objParam, True)
                _ChildCount = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(2).Value)
                _JournalEntryCount = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(3).Value)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetJournalEntryCount() As Integer
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim Count As Integer
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numParntAcntId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _ParentAccountId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Count = CType(SqlDAL.ExecuteScalar(connString, "USP_GetCountOfJournalEntry", arParms), Integer)
                Return Count
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetBitFixedDetails() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim bitFixed As Boolean
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numAccountId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _AccountId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                bitFixed = CType(SqlDAL.ExecuteScalar(connString, "USP_ChartCategoryBitFixed", arParms), Boolean)
                Return bitFixed
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteChartCategory() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numAccountId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _AccountId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteChartCategory", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function UpdateChartAcntDragDrop() As Integer
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim Count As Integer
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numAccountId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _AccountId

                arParms(1) = New Npgsql.NpgsqlParameter("@numParntAcntId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _ParentAccountId


                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateChartAcntDragDrop", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetAcntType() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim Count As Integer
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numAccountId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _AccountId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_GetAcntType", arParms))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetAcntTypeForParent() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim Count As Integer
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numAccountId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _ParentAccountId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_GetAcntType", arParms))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetFixedAsset() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                arparms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(0).Value = Nothing
                arparms(0).Direction = ParameterDirection.InputOutput
                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_FixedAsset", arparms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function InsertFixedAssetInChartAcnt() As Long
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Try

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(14) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numParntAcntId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _ParentAccountId

                arParms(1) = New Npgsql.NpgsqlParameter("@numAcntType", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _AccountType

                arParms(2) = New Npgsql.NpgsqlParameter("@vcCatgyName", NpgsqlTypes.NpgsqlDbType.Varchar, 100)
                arParms(2).Value = _CategoryName

                arParms(3) = New Npgsql.NpgsqlParameter("@vcCatgyDescription", NpgsqlTypes.NpgsqlDbType.Varchar, 100)
                arParms(3).Value = _CategoryDescription

                arParms(4) = New Npgsql.NpgsqlParameter("@monOriginalOpeningBal", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(4).Value = _decOriginalOpeningBal

                arParms(5) = New Npgsql.NpgsqlParameter("@monOpeningBal", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(5).Value = _decOpeningBalance

                arParms(6) = New Npgsql.NpgsqlParameter("@dtOpeningDate", NpgsqlTypes.NpgsqlDbType.Timestamp, 50)
                arParms(6).Value = _OpeningDate

                arParms(7) = New Npgsql.NpgsqlParameter("@bitActive", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(7).Value = _Active

                arParms(8) = New Npgsql.NpgsqlParameter("@numAccountId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(8).Direction = ParameterDirection.InputOutput
                arParms(8).Value = _AccountId

                arParms(9) = New Npgsql.NpgsqlParameter("@bitFixed", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(9).Value = _BitFixed

                arParms(10) = New Npgsql.NpgsqlParameter("@bitDepreciation", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(10).Value = _BitDepreciation

                ''arParms(11) = New Npgsql.NpgsqlParameter("@dtOriginalCostDate", NpgsqlTypes.NpgsqlDbType.Bit)
                ''arParms(11).Value = _OriginalCostDate

                arParms(11) = New Npgsql.NpgsqlParameter("@dtDepreciationCostDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(11).Value = _DepreciationCostDate

                ''arParms(13) = New Npgsql.NpgsqlParameter("@monOriginalCost", NpgsqlTypes.NpgsqlDbType.Bit)
                ''arParms(13).Value = _OriginalCost

                arParms(12) = New Npgsql.NpgsqlParameter("@monDepreciationCost", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(12).Value = _DepreciationCost

                arParms(13) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(13).Value = DomainID

                arParms(14) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(14).Value = UserCntID


                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "usp_InsertFixedAssetCOA", objParam, True)
                _AccountId = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(8).Value)

                Return _AccountId

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetAccountTypes() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numAccountTypeID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _AccountTypeID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _Mode

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetAccountTypes", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ManageAccountType() As Long
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Try

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numAccountTypeID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _AccountTypeID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcAccountCode", NpgsqlTypes.NpgsqlDbType.Varchar, 50)
                arParms(1).Value = _AccountTypeCode

                arParms(2) = New Npgsql.NpgsqlParameter("@vcAccountType", NpgsqlTypes.NpgsqlDbType.Varchar, 100)
                arParms(2).Value = _AccountTypeName

                arParms(3) = New Npgsql.NpgsqlParameter("@numParentID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(3).Value = _ParentAccountTypeID

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(4).Value = DomainID

                arParms(5) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _Mode

                arParms(6) = New Npgsql.NpgsqlParameter("@bitActive", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(6).Value = IsActive

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageAccountType", objParam, True)
                _AccountTypeID = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

                Return _AccountTypeID
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ManageCOARelationship() As Long
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numCOARelationshipID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _COARelationshipID

                arParms(1) = New Npgsql.NpgsqlParameter("@numRelationshipID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _RelationshipID

                arParms(2) = New Npgsql.NpgsqlParameter("@numARAccountId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Value = _ARAccountID

                arParms(3) = New Npgsql.NpgsqlParameter("@numAPAccountId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(3).Value = _APAccountID

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(4).Value = DomainID

                arParms(5) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(5).Value = UserCntID

                arParms(6) = New Npgsql.NpgsqlParameter("@numARParentAcntTypeID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(6).Value = _ARAccountTypeID

                arParms(7) = New Npgsql.NpgsqlParameter("@numAPParentAcntTypeID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(7).Value = _APAccountTypeID

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageCOARelationship", objParam, True)
                _COARelationshipID = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

                Return _COARelationshipID
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetCOARelationship() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numCOARelationshipID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _COARelationshipID

                arParms(1) = New Npgsql.NpgsqlParameter("@numRelationshipID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _RelationshipID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(3).Value = _DivisionID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetCOARelationship", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetCOACreditCardCharge() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numTransChargeID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _TransChargeID

                arParms(1) = New Npgsql.NpgsqlParameter("@numCreditCardTypeId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _CreditCardTypeId

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetCOACreditCardCharge", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ManageCOACreditCardCharge() As Long
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numTransChargeID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _TransChargeID

                arParms(1) = New Npgsql.NpgsqlParameter("@numAccountId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _AccountId

                arParms(2) = New Npgsql.NpgsqlParameter("@fltTransactionCharge", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(2).Value = _TransactionCharge

                arParms(3) = New Npgsql.NpgsqlParameter("@tintBareBy", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _TransactionChargeBareBy

                arParms(4) = New Npgsql.NpgsqlParameter("@numCreditCardTypeId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(4).Value = _CreditCardTypeId

                arParms(5) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(5).Value = DomainID

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageCOACreditCardCharge", objParam, True)
                _TransChargeID = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

                Return _TransChargeID
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetMonthlySummary() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(0).Value = DomainID

                arparms(1) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(1).Value = _FromDate

                arparms(2) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(2).Value = _ToDate

                arparms(3) = New Npgsql.NpgsqlParameter("@vcChartAcntId", NpgsqlTypes.NpgsqlDbType.Varchar, 500)
                arparms(3).Value = _ChartAcntIds

                arparms(4) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer) 'Param added by Chintan (Since Date and Time must be displayed w.r.t. the client machine)
                arparms(4).Value = CCommon.ToInteger(System.Web.HttpContext.Current.Session("ClientMachineUTCTimeOffset"))

                arparms(5) = New Npgsql.NpgsqlParameter("@numAccountClass", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(5).Value = AccountClass

                arparms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(6).Value = Nothing
                arparms(6).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetMonthlySummary", arparms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetCOAShippingMapping() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numShippingMethodID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _ShippingMethodID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetCOAShippingMapping", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ManageCOAShippingMapping() As Long
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numShippingMappingID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _ShippingMappingID

                arParms(1) = New Npgsql.NpgsqlParameter("@numShippingMethodID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _ShippingMethodID

                arParms(2) = New Npgsql.NpgsqlParameter("@numIncomeAccountID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Value = _AccountId

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(3).Value = DomainID

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageCOAShippingMapping", objParam, True)
                _ShippingMappingID = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

                Return _ShippingMappingID
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetFinancialYear() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numFinYearId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _FinancialYearID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _Mode

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetFinancialYear", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ManageFinancialYear() As Long
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numFinYearId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _FinancialYearID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcFinYearDesc", NpgsqlTypes.NpgsqlDbType.Varchar, 50)
                arParms(2).Value = _FinDescription

                arParms(3) = New Npgsql.NpgsqlParameter("@dtPeriodFrom", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(3).Value = _FromDate

                arParms(4) = New Npgsql.NpgsqlParameter("@dtPeriodTo", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(4).Value = _ToDate

                arParms(5) = New Npgsql.NpgsqlParameter("@bitCloseStatus", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(5).Value = _bitCloseStatus

                arParms(6) = New Npgsql.NpgsqlParameter("@bitAuditStatus", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(6).Value = _bitAuditStatus

                arParms(7) = New Npgsql.NpgsqlParameter("@bitCurrentYear", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(7).Value = _bitCurrentYear

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageFinancialYear", objParam, True)
                _FinancialYearID = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

                Return _FinancialYearID
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function DeleteFinancialYear() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numFinYearId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _FinancialYearID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteFinancialYear", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function CloseFinancialYear() As Boolean
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numFinYearId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _FinancialYearID

                arParms(2) = New Npgsql.NpgsqlParameter("@numNextFinYearId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Value = _NextFinancialYearID

                arParms(3) = New Npgsql.NpgsqlParameter("@tintCloseMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _Mode

                SqlDAL.ExecuteNonQuery(connString, "USP_CloseFinancialYear", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Sub ManageDefaultAccountsForDomain()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@str", NpgsqlTypes.NpgsqlDbType.VarChar, 8000)
                arParms(1).Value = _Str

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageDefaultAccountsForDomain", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Function GetDefaultAccounts() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetDefaultAccounts", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetChartOfAccountsDetail() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = DomainID

                arparms(1) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(1).Value = _ToDate

                arparms(2) = New Npgsql.NpgsqlParameter("@vcAccountId", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arparms(2).Value = _ChartAcntIds

                arparms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(3).Value = Nothing
                arparms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_ChartofAccountsDetail", arparms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetContactTypeMapping() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numMappingID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _MappingID

                arParms(1) = New Npgsql.NpgsqlParameter("@numContactTypeID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ContactTypeID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetContactTypeMapping", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ManageContactTypeMapping() As Long
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numMappingID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _MappingID

                arParms(1) = New Npgsql.NpgsqlParameter("@numContactTypeID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ContactTypeID

                arParms(2) = New Npgsql.NpgsqlParameter("@numAccountId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _AccountId

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = DomainID

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageContactTypeMapping", objParam, True)
                _MappingID = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

                Return _MappingID
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetAccountsForDropDown() As DataTable

            '            CREATE PROCEDURE [dbo].[USP_GetAccounts]
            '(
            '	@numAccountID		NUMERIC(18,0),
            '	@numDomainID		NUMERIC(18,0),
            '	@numParntAcntTypeId	NUMERIC(18,0)
            ')
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim dtResult As New DataTable
            Try
                ' Set up parameters 
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numAccountID", _AccountId, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@numParntAcntTypeId", _ParentAccountTypeID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                dtResult = SqlDAL.ExecuteDatable(connString, "USP_GetAccounts", sqlParams.ToArray())

            Catch ex As Exception
                dtResult = Nothing
                Throw ex
            End Try

            Return dtResult
        End Function

        Public Shared Function GetDefaultAccount(AccountingChargeCode As String, DomainID As Long) As Long
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@chChargeCode", NpgsqlTypes.NpgsqlDbType.Char, 10)
                arParms(1).Value = AccountingChargeCode

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return CCommon.ToLong(SqlDAL.ExecuteScalar(connString, "USP_GetChartOfAccountIdForAuthoritativeBizDocs", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function UpdateCOACheckNumber() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numAccountID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _AccountId

                arParms(2) = New Npgsql.NpgsqlParameter("@vcStartingCheckNumber", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = _StartCheckNumber

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateCOACheckNumber", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ValidateCheckNo(ByVal CheckNumbersList As String) As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numChartAcntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _AccountId

                arParms(2) = New Npgsql.NpgsqlParameter("@vcCheckNos", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(2).Value = CheckNumbersList

                arParms(3) = New Npgsql.NpgsqlParameter("@numCheckHeaderID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _CheckHeaderID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_ValidateCheckNo", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Sub MakeChartOfAccountOpeningBalanceJournalEntry(lngAccountID As Long, Openingdate As Date)
            Try

                Dim decDebitAmtCOA As Decimal
                Dim decCreditAmtCOA As Decimal
                Dim decDebitAmtEquity As Decimal
                Dim decCreditAmtEqity As Decimal


                If decOriginalOpeningBal < 0 Then
                    decDebitAmtEquity = Math.Abs(decOriginalOpeningBal)
                    decCreditAmtEqity = 0
                    decDebitAmtCOA = 0
                    decCreditAmtCOA = Math.Abs(decOriginalOpeningBal)
                ElseIf decOriginalOpeningBal > 0 Then
                    decDebitAmtEquity = 0
                    decCreditAmtEqity = Math.Abs(decOriginalOpeningBal)
                    decDebitAmtCOA = Math.Abs(decOriginalOpeningBal)
                    decCreditAmtCOA = 0
                End If

                Dim objJEHeader As New JournalEntryHeader

                Dim objCommon As New CCommon
                objCommon.DomainID = DomainID
                objCommon.Mode = 36
                objCommon.Str = lngAccountID.ToString
                'check for existing journal entry
                Dim lngJournalId As Long = CCommon.ToLong(objCommon.GetSingleFieldValue())



                With objJEHeader
                    .JournalId = lngJournalId
                    .RecurringId = 0
                    .EntryDate = CDate(Openingdate.ToShortDateString() & " 12:00:00")
                    .Description = "Opening Balance: " & String.Format("{0:###0.00}", Math.Abs(decOriginalOpeningBal))
                    .Amount = Math.Abs(decOriginalOpeningBal)
                    .CheckId = 0
                    .CashCreditCardId = 0
                    .ChartAcntId = lngAccountID
                    .OppId = 0
                    .OppBizDocsId = 0
                    .DepositId = 0
                    .BizDocsPaymentDetId = 0
                    .IsOpeningBalance = 0
                    .LastRecurringDate = Date.Now
                    .NoTransactions = 0
                    .CategoryHDRID = 0
                    .ReturnID = 0
                    .CheckHeaderID = 0
                    .BillID = 0
                    .BillPaymentID = 0
                    .UserCntID = UserCntID
                    .DomainID = DomainID
                End With
                lngJournalId = objJEHeader.Save()




                Dim objJEList As New JournalEntryCollection
                Dim objJE As New JournalEntryNew()

                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = decDebitAmtEquity
                objJE.CreditAmt = decCreditAmtEqity
                objJE.ChartAcntId = ChartOfAccounting.GetDefaultAccount("OE", DomainID) 'Opening Balance Equity
                objJE.Description = CCommon.Truncate(_CategoryName, 50)
                objJE.CustomerId = 0
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = "OE" ' using OE1 for Item Opening balance, OE for COA opening balance
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 0
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)


                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = decDebitAmtCOA
                objJE.CreditAmt = decCreditAmtCOA
                objJE.ChartAcntId = lngAccountID 'COA
                objJE.Description = CCommon.Truncate(_CategoryName, 50)
                objJE.CustomerId = 0
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = "OE" ' using OE1 for Item Opening balance, OE for COA opening balance
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 0
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)

                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, DomainID)

            Catch ex As Exception
                Throw ex
            End Try

        End Sub
        Public Function GetCOAwithOpeningBalanceFirsttimeOnly() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(0).Value = Nothing
                arParms(0).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_GetCOAwithOpeningBalance_Temp", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetDefaultARAndChildAccounts() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_ChartOfAccounts_GetARAndChildAccounts", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub SaveAccountTypeOrder(ByVal accountTypes As String)
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcAccountTypes", accountTypes, NpgsqlTypes.NpgsqlDbType.VarChar))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_ChartOfAccounts_SaveAccountTypeOrder", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub
    End Class
End Namespace