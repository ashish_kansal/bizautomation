Option Explicit On
Option Strict On

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports System.Web
Imports BACRM.BusinessLogic.Opportunities
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.Accounting

    Public Class JournalEntry
        Inherits BACRM.BusinessLogic.CBusinessBase


        'Private _ParentRootNode As Integer
        Private _JournalId As Integer = 0
        Private _Entry_Date As Date = New Date(1753, 1, 1)
        Private _Description As String = String.Empty
        Private _Amount As Decimal
        Private _TypeId As Integer
        Private _JournalDetails As String = String.Empty
        Private _Balance As Decimal
        'Private DomainId As Long
        Private _ChartAcntId As Long
        Private _AcntType As Long
        Private _DebitAmt As Decimal
        Private _CreditAmt As Decimal
        Private _CustomerId As Integer
        Private _TransactionId As Integer
        Private _UpdateType As Integer
        Private _OpeningBalance As Decimal
        Private _strWhereCondition As String
        Private _DivisionId As Integer
        Private _Mode As Integer
        Private _Memo As String = String.Empty
        Private _CheckId As Integer
        Private _CashCreditCardId As Integer
        Private _RecurringId As Integer
        Private _RecurringMode As Integer
        'Private UserCntID As Long
        Private _CreatedDate As DateTime
        Private _FromDate As Date
        Private _ToDate As Date
        Private _OppBIzDocID As Long
        Private _OppId As Long
        Private _AuthorizativeAccounting As Int16
        Private _DepositId As Long
        Private _CheckNo As Long
        Private _BankRoutingNo As Long
        Private _BankAccountNo As Long
        Private _CheckCompanyId As Long
        Private _FilterbyEntry_Date As Date = New Date(1753, 1, 1)

        Private _ProjectID As Long
        Public Property ProjectID() As Long
            Get
                Return _ProjectID
            End Get
            Set(ByVal value As Long)
                _ProjectID = value
            End Set
        End Property

        'Public Property ParentRootNode() As Integer
        '    Get
        '        Return _ParentRootNode
        '    End Get
        '    Set(ByVal Value As Integer)
        '        _ParentRootNode = Value
        '    End Set
        'End Property


        Public Property JournalId() As Integer
            Get
                Return _JournalId
            End Get
            Set(ByVal Value As Integer)
                _JournalId = Value
            End Set
        End Property
        Public Property Entry_Date() As DateTime
            Get
                Return _Entry_Date
            End Get
            Set(ByVal Value As DateTime)
                _Entry_Date = Value
            End Set
        End Property
        Public Property Description() As String
            Get
                Return _Description
            End Get
            Set(ByVal Value As String)
                _Description = Value
            End Set
        End Property

        Public Property Amount() As Decimal
            Get
                Return _Amount
            End Get
            Set(ByVal Value As Decimal)
                _Amount = Value
            End Set
        End Property


        Public Property Balance() As Decimal
            Get
                Return _Balance
            End Get
            Set(ByVal Value As Decimal)
                _Balance = Value
            End Set
        End Property
        Public Property TypeId() As Integer
            Get
                Return _TypeId
            End Get
            Set(ByVal value As Integer)
                _TypeId = value
            End Set
        End Property

        Public Property JournalDetails() As String
            Get
                Return _JournalDetails
            End Get
            Set(ByVal value As String)
                _JournalDetails = value
            End Set
        End Property


        Public Property ChartAcntId() As Long
            Get
                Return _ChartAcntId
            End Get
            Set(ByVal value As Long)
                _ChartAcntId = value
            End Set
        End Property

        Public Property AcntType() As Long
            Get
                Return _AcntType
            End Get
            Set(ByVal value As Long)
                _AcntType = value
            End Set
        End Property

        Public Property DebitAmt() As Decimal
            Get
                Return _DebitAmt
            End Get
            Set(ByVal value As Decimal)
                _DebitAmt = value
            End Set
        End Property

        Public Property CreditAmt() As Decimal
            Get
                Return _CreditAmt
            End Get
            Set(ByVal value As Decimal)
                _CreditAmt = value
            End Set
        End Property
        Public Property CustomerId() As Integer
            Get
                Return _CustomerId
            End Get
            Set(ByVal value As Integer)
                _CustomerId = value
            End Set
        End Property

        Public Property TransactionId() As Integer
            Get
                Return _TransactionId
            End Get
            Set(ByVal value As Integer)
                _TransactionId = value
            End Set
        End Property

        Public Property UpdateType() As Integer
            Get
                Return _UpdateType
            End Get
            Set(ByVal value As Integer)
                _UpdateType = value
            End Set
        End Property

        Public Property OpeningBalance() As Decimal
            Get
                Return _OpeningBalance
            End Get
            Set(ByVal value As Decimal)
                _OpeningBalance = value
            End Set
        End Property
        Public Property Mode() As Integer
            Get
                Return _Mode
            End Get
            Set(ByVal value As Integer)
                _Mode = value
            End Set
        End Property

        Public Property strWhereCondition() As String
            Get
                Return _strWhereCondition
            End Get
            Set(ByVal value As String)
                _strWhereCondition = value
            End Set
        End Property

        Public Property DivisionId() As Integer
            Get
                Return _DivisionId
            End Get
            Set(ByVal value As Integer)
                _DivisionId = value
            End Set
        End Property

        Public Property Memo() As String
            Get
                Return _Memo
            End Get
            Set(ByVal value As String)
                _Memo = value
            End Set
        End Property

        Public Property CheckId() As Integer
            Get
                Return _CheckId
            End Get
            Set(ByVal value As Integer)
                _CheckId = value
            End Set
        End Property

        Public Property CashCreditCardId() As Integer
            Get
                Return _CashCreditCardId
            End Get
            Set(ByVal value As Integer)
                _CashCreditCardId = value
            End Set
        End Property

        Public Property RecurringId() As Integer
            Get
                Return _RecurringId
            End Get
            Set(ByVal value As Integer)
                _RecurringId = value
            End Set
        End Property

        Public Property RecurringMode() As Integer
            Get
                Return _RecurringMode
            End Get
            Set(ByVal value As Integer)
                _RecurringMode = value
            End Set
        End Property

        Public Property CreatedDate() As DateTime
            Get
                Return _CreatedDate
            End Get
            Set(ByVal value As DateTime)
                _CreatedDate = value
            End Set
        End Property

        Public Property FromDate() As Date
            Get
                Return _FromDate
            End Get
            Set(ByVal Value As Date)
                _FromDate = Value
            End Set
        End Property

        Public Property ToDate() As Date
            Get
                Return _ToDate
            End Get
            Set(ByVal Value As Date)
                _ToDate = Value
            End Set
        End Property

        Public Property OppBIzDocID() As Long
            Get
                Return _OppBIzDocID
            End Get
            Set(ByVal value As Long)
                _OppBIzDocID = value
            End Set
        End Property

        Public Property OppId() As Long
            Get
                Return _OppId
            End Get
            Set(ByVal value As Long)
                _OppId = value
            End Set
        End Property

        Private _LandedCostOppId As Long
        Public Property LandedCostOppId() As Long
            Get
                Return _LandedCostOppId
            End Get
            Set(ByVal value As Long)
                _LandedCostOppId = value
            End Set
        End Property

        Public Property AuthorizativeAccounting() As Int16
            Get
                Return _AuthorizativeAccounting
            End Get
            Set(ByVal value As Int16)
                _AuthorizativeAccounting = value
            End Set
        End Property

        Public Property DepositId() As Long
            Get
                Return _DepositId
            End Get
            Set(ByVal value As Long)
                _DepositId = value
            End Set
        End Property

        Public Property CheckNo() As Long
            Get
                Return _CheckNo
            End Get
            Set(ByVal value As Long)
                _CheckNo = value
            End Set
        End Property

        Public Property BankRoutingNo() As Long
            Get
                Return _BankRoutingNo
            End Get
            Set(ByVal value As Long)
                _BankRoutingNo = value
            End Set
        End Property

        Public Property BankAccountNo() As Long
            Get
                Return _BankAccountNo
            End Get
            Set(ByVal value As Long)
                _BankAccountNo = value
            End Set
        End Property

        Public Property CheckCompanyId() As Long
            Get
                Return _CheckCompanyId
            End Get
            Set(ByVal value As Long)
                _CheckCompanyId = value
            End Set
        End Property


        Public Property FilterbyEntry_Date() As Date
            Get
                Return _FilterbyEntry_Date
            End Get
            Set(ByVal value As Date)
                _FilterbyEntry_Date = value
            End Set
        End Property


        Private _StrText As String
        Public Property StrText() As String
            Get
                Return _StrText
            End Get
            Set(ByVal value As String)
                _StrText = value
            End Set
        End Property


        Private _ClassID As Long
        Public Property ClassID() As Long
            Get
                Return _ClassID
            End Get
            Set(ByVal value As Long)
                _ClassID = value
            End Set
        End Property

        Private _ReconcileID As Long
        Public Property ReconcileID() As Long
            Get
                Return _ReconcileID
            End Get
            Set(ByVal Value As Long)
                _ReconcileID = Value
            End Set
        End Property

        Private _CheckHeaderID As Long
        Public Property CheckHeaderID() As Long
            Get
                Return _CheckHeaderID
            End Get
            Set(ByVal Value As Long)
                _CheckHeaderID = Value
            End Set
        End Property
        Private _BizDocsPaymentDetId As String
        Public Property BizDocsPaymentDetId() As String
            Get
                Return _BizDocsPaymentDetId
            End Get
            Set(ByVal value As String)
                _BizDocsPaymentDetId = value
            End Set
        End Property

        Private _IsOpeningBalance As Boolean
        Public Property IsOpeningBalance() As Boolean
            Get
                Return _IsOpeningBalance
            End Get
            Set(ByVal value As Boolean)
                _IsOpeningBalance = value
            End Set
        End Property

        Private _CategoryHDRID As Long
        Public Property CategoryHDRID() As Long
            Get
                Return _CategoryHDRID
            End Get
            Set(ByVal value As Long)
                _CategoryHDRID = value
            End Set
        End Property

        Private _ReturnID As Long
        Public Property ReturnID() As Long
            Get
                Return _ReturnID
            End Get
            Set(ByVal value As Long)
                _ReturnID = value
            End Set
        End Property

        Private _BillPaymentID As Long
        Public Property BillPaymentID() As Long
            Get
                Return _BillPaymentID
            End Get
            Set(ByVal value As Long)
                _BillPaymentID = value
            End Set
        End Property

        Private _BillID As Long
        Public Property BillID() As Long
            Get
                Return _BillID
            End Get
            Set(ByVal value As Long)
                _BillID = value
            End Set
        End Property

        Private _ClientTimeZoneOffset As Integer
        Public Property ClientTimeZoneOffset() As Integer
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal value As Integer)
                _ClientTimeZoneOffset = value
            End Set
        End Property
        Public Function GetJournalEntryListForBankReconciliation(ByVal flag As Short, ByVal DateHide As Boolean, ByVal tintReconcile As Short) As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numChartAcntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ChartAcntId

                arParms(1) = New Npgsql.NpgsqlParameter("@tintFlag", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = flag

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@bitDateHide", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Value = DateHide

                arParms(4) = New Npgsql.NpgsqlParameter("@numReconcileID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = ReconcileID

                arParms(5) = New Npgsql.NpgsqlParameter("@tintReconcile", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = tintReconcile

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_Journal_EntryListForBankReconciliation", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function SaveUpdatedReconcileBalance() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@strText", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(0).Value = _StrText

                arParms(1) = New Npgsql.NpgsqlParameter("@numReconcileID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ReconcileID

                SqlDAL.ExecuteNonQuery(connString, "USP_SaveUpdatedReconcileBalance", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetGenernalLedgerList() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numChartAcntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ChartAcntId

                arParms(1) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _FromDate

                arParms(2) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _ToDate

                arParms(3) = New Npgsql.NpgsqlParameter("@BoolOpeningBalance", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Value = 0

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = DomainID

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GeneralLedgerList", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetBeginingBalanceGeneralLedgerList() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numChartAcntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ChartAcntId

                arParms(1) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _FromDate

                arParms(2) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _ToDate

                arParms(3) = New Npgsql.NpgsqlParameter("@BoolOpeningBalance", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Value = 1

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = DomainID

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GeneralLedgerList", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetJournalEntryDetails() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numJournalId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _JournalId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_Journal_EntryDetails", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Public Function GetCompanyDetails() As DataTable
        '    Try
        '        Dim getconnection As New GetConnection
        '        Dim connString As String = getconnection.GetConnectionString
        '        Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

        '        arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(0).Value = DomainID

        '        arParms(1) = New Npgsql.NpgsqlParameter("@varCompanyName", NpgsqlTypes.NpgsqlDbType.VarChar)
        '        arParms(1).Value = _strWhereCondition

        '        Dim ds As DataSet
        '        ds = SqlDAL.ExecuteDataset(connString, "USP_Journal_CompanyDetails", arParms)
        '        Return ds.Tables(0)
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function

        'Public Function GetSumDebitCreditValues() As Decimal
        '    Try
        '        Dim getconnection As New GetConnection
        '        Dim connString As String = getconnection.GetConnectionString
        '        Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

        '        arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(0).Value = DomainID

        '        arParms(1) = New Npgsql.NpgsqlParameter("@numJournalId", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(1).Value = _JournalId

        '        arParms(2) = New Npgsql.NpgsqlParameter("@numAccountsType", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(2).Value = _AcntType

        '        Return CDec(SqlDAL.ExecuteScalar(connString, "USP_SumDebitCreditValues", arParms))
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function

        'Public Function UpdateJournalDetails() As Long
        '    Try
        '        Dim getconnection As New GetConnection
        '        Dim connString As String = getconnection.GetConnectionString
        '        Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(11) {}

        '        arParms(0) = New Npgsql.NpgsqlParameter("@numTransactionId", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(0).Value = _TransactionId

        '        arParms(1) = New Npgsql.NpgsqlParameter("@numDebitAmt", NpgsqlTypes.NpgsqlDbType.Numeric)
        '        arParms(1).Value = _DebitAmt

        '        arParms(2) = New Npgsql.NpgsqlParameter("@numCreditAmt", NpgsqlTypes.NpgsqlDbType.Numeric)
        '        arParms(2).Value = _CreditAmt

        '        arParms(3) = New Npgsql.NpgsqlParameter("@varDescription", NpgsqlTypes.NpgsqlDbType.VarChar)
        '        arParms(3).Value = _Description

        '        arParms(4) = New Npgsql.NpgsqlParameter("@numCustomerId", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(4).Value = _CustomerId

        '        arParms(5) = New Npgsql.NpgsqlParameter("@numJournalId", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(5).Value = _JournalId

        '        arParms(6) = New Npgsql.NpgsqlParameter("@datEntry_Date", NpgsqlTypes.NpgsqlDbType.Timestamp)
        '        arParms(6).Value = _Entry_Date

        '        arParms(7) = New Npgsql.NpgsqlParameter("@numUpdateType", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(7).Value = _UpdateType

        '        arParms(8) = New Npgsql.NpgsqlParameter("@numBalance", NpgsqlTypes.NpgsqlDbType.Numeric)
        '        arParms(8).Value = _Balance

        '        arParms(9) = New Npgsql.NpgsqlParameter("@numCheckId", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(9).Value = _CheckId

        '        arParms(10) = New Npgsql.NpgsqlParameter("@numCashCreditCardId", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(10).Value = _CashCreditCardId

        '        arParms(11) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(11).Value = DomainID

        '        SqlDAL.ExecuteNonQuery(connString, "USP_UpdateJournalDetails", arParms)
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function

        'Public Function UpdateChartAcntOpenBal() As Long
        '    Try
        '        Dim getconnection As New GetConnection
        '        Dim connString As String = getconnection.GetConnectionString
        '        Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

        '        arParms(0) = New Npgsql.NpgsqlParameter("@numChartAcntId", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(0).Value = _ChartAcntId

        '        arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(1).Value = DomainID


        '        arParms(2) = New Npgsql.NpgsqlParameter("@numOpeningBal", NpgsqlTypes.NpgsqlDbType.Numeric)
        '        arParms(2).Value = _OpeningBalance

        '        SqlDAL.ExecuteScalar(connString, "USP_UpdateChartAcntOpenBal", arParms)
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function

        Public Function DeleteJournalEntryDetails() As Decimal
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@numJournalID", _JournalId, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@numBillPaymentID", _BillPaymentID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@numDepositID", _DepositId, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@numCheckHeaderID", _CheckHeaderID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@numBillID", _BillID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@numCategoryHDRID", _CategoryHDRID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@numReturnID", _ReturnID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteJournalDetails", sqlParams.ToArray())

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetOppBizDocsReceivePayment() As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@numOppID", _OppId, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@numOppBizDocsID", _OppBIzDocID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@numBillID", _BillID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@numReturnHeaderID", _ReturnID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@tintClientTimeZoneOffset", _ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))

                    .Add(SqlDAL.Add_Parameter("@numLandedCostOppId", _LandedCostOppId, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetOppBizDocsReceivePayment", sqlParams.ToArray())

                Return ds

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        '<Obsolete("procedure code is obs and function is not referenced by any code")>
        'Public Function DeleteOpeningBalanceDetails() As Decimal
        '    Try
        '        Dim getconnection As New GetConnection
        '        Dim connString As String = getconnection.GetConnectionString
        '        Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

        '        arParms(0) = New Npgsql.NpgsqlParameter("@numChartAcntId", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(0).Value = _ChartAcntId

        '        arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(1).Value = DomainID

        '        SqlDAL.ExecuteNonQuery(connString, "USP_DeleteOpeningBalanceDetails", arParms)
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function

        'Public Function SaveDataToCheckDetailsEdit() As Long
        '    Try
        '        Dim getconnection As New GetConnection
        '        Dim connString As String = getconnection.GetConnectionString
        '        Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(12) {}

        '        arParms(0) = New Npgsql.NpgsqlParameter("@numCheckId", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(0).Value = _CheckId

        '        arParms(1) = New Npgsql.NpgsqlParameter("@datEntry_Date", NpgsqlTypes.NpgsqlDbType.Timestamp)
        '        arParms(1).Value = _Entry_Date

        '        arParms(2) = New Npgsql.NpgsqlParameter("@numAmount", NpgsqlTypes.NpgsqlDbType.Numeric)
        '        arParms(2).Value = _Amount

        '        arParms(3) = New Npgsql.NpgsqlParameter("@vcMemo", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
        '        arParms(3).Value = _Memo

        '        arParms(4) = New Npgsql.NpgsqlParameter("@numCustomerId", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(4).Value = _CustomerId

        '        arParms(5) = New Npgsql.NpgsqlParameter("@numAcntTypeId", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(5).Value = _AcntType

        '        ''arParms(6) = New Npgsql.NpgsqlParameter("@numRecurringId", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        ''arParms(6).Value = _RecurringId

        '        ''arParms(7) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        ''arParms(7).Value = DomainId

        '        ''arParms(8) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        ''arParms(8).Value = UserCntID

        '        arParms(6) = New Npgsql.NpgsqlParameter("@numCheckNo", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(6).Value = _CheckNo

        '        arParms(7) = New Npgsql.NpgsqlParameter("@numBankRoutingNo", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(7).Value = _BankRoutingNo

        '        arParms(8) = New Npgsql.NpgsqlParameter("@numBankAccountNo", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(8).Value = _BankAccountNo

        '        arParms(9) = New Npgsql.NpgsqlParameter("@numCheckCompanyId", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(9).Value = _CheckCompanyId

        '        arParms(10) = New Npgsql.NpgsqlParameter("@numRecurringId", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(10).Value = _RecurringId

        '        arParms(11) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(11).Value = DomainID

        '        arParms(12) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(12).Value = UserCntID


        '        SqlDAL.ExecuteScalar(connString, "USP_InsertCheckHeaderDet", arParms)
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function
        Public Function SaveJournalEntriesSales(ByVal lngOppID As Long, ByVal lngDomainID As Long, ByVal dtOppBiDocItems As DataTable, ByVal lngJournalID As Long, ByVal lngBizDocID As Long, ByVal TotalAmount As Decimal, ByVal decDiscAmt As Decimal, ByVal decShipCost As Decimal, ByVal decLateCharge As Decimal, ByVal lngDivisionID As Long, ByVal CurrencyID As Long, ByVal ExchangeRate As Double, Optional ByVal DiscAcntType As Long = 0, Optional ByVal lngShippingMethod As Long = 0, Optional ByVal bitInvoiceAgainstDeferredIncomeBizDoc As Boolean = False) As Boolean
            Try
                If ExchangeRate = 0 Then
                    Throw New Exception("EXCHANGE_RATE_CAN_NOT_BE_0")
                    Exit Function
                End If

                Dim i As Integer
                Dim objAuthoritativeBizDocs As New AuthoritativeBizDocs
                Dim decACProductCost, decTaxAmount As Decimal
                Dim lngItemIncomeAccount, lngItemAssetAccount, lngItemCOGsAccount, lngSalesClearingAccount, lngDeferredIncomeAccount As Long
                'ExchangeRate = 1 / ExchangeRate

                lngSalesClearingAccount = ChartOfAccounting.GetDefaultAccount("SC", lngDomainID)
                If lngSalesClearingAccount = 0 Then
                    Throw New Exception("DEFAULT_SALES_CLEARING_ACCOUNT_NOT_AVAILABLE")
                    Exit Function
                End If

                If bitInvoiceAgainstDeferredIncomeBizDoc Then
                    lngDeferredIncomeAccount = ChartOfAccounting.GetDefaultAccount("DI", lngDomainID)
                    If lngDeferredIncomeAccount = 0 Then
                        Throw New Exception("DEFAULT_DEFERRED_INCOME_ACCOUNT_NOT_AVAILABLE")
                        Exit Function
                    End If
                End If

                Dim objJEList As New JournalEntryCollection

                Dim objJE As New JournalEntryNew()

                Dim dvOppBiDocItems As DataView

                dvOppBiDocItems = New DataView(dtOppBiDocItems)
                '' dvOppBiDocItems.RowFilter = " ItemCode <> 0 "  -- Temporarily Commented
                Dim decAmount, decDebitAmt, decCreditAmt As Decimal

                For i = 0 To dvOppBiDocItems.Count - 1
                    decAmount = CDec(CCommon.ToDecimal(dvOppBiDocItems.Item(i).Item("Amount")) * ExchangeRate)
                    decDebitAmt = 0
                    decCreditAmt = 0
                    If decAmount <> 0 Then
                        If decAmount > 0.0 Then
                            decDebitAmt = 0
                            decCreditAmt = Math.Abs(decAmount)
                        ElseIf decAmount < 0.0 Then
                            decDebitAmt = Math.Abs(decAmount)
                            decCreditAmt = 0
                        End If

                        If CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemIncomeAccount")) > 0 Then
                            lngItemIncomeAccount = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemIncomeAccount"))
                        Else
                            lngItemIncomeAccount = ChartOfAccounting.GetDefaultAccount("BE", lngDomainID)
                        End If

                        If CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemInventoryAsset")) > 0 Then
                            lngItemAssetAccount = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemInventoryAsset"))
                        End If

                        If CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemCoGs")) > 0 Then
                            lngItemCOGsAccount = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemCoGs"))
                        Else
                            lngItemCOGsAccount = ChartOfAccounting.GetDefaultAccount("CG", lngDomainID)
                        End If

                        objJE = New JournalEntryNew()

                        objJE.TransactionId = 0
                        objJE.DebitAmt = decDebitAmt
                        objJE.CreditAmt = decCreditAmt
                        objJE.ChartAcntId = lngItemIncomeAccount
                        objJE.Description = "Income" '"Authoritative Accounts"
                        objJE.CustomerId = lngDivisionID
                        objJE.MainDeposit = False
                        objJE.MainCheck = False
                        objJE.MainCashCredit = False
                        objJE.OppitemtCode = CLng(dvOppBiDocItems.Item(i).Item("numoppitemtCode"))
                        objJE.BizDocItems = CStr(dvOppBiDocItems.Item(i).Item("ItemType"))
                        objJE.Reference = ""
                        objJE.PaymentMethod = 0
                        objJE.Reconcile = False
                        objJE.CurrencyID = CurrencyID
                        objJE.FltExchangeRate = ExchangeRate
                        objJE.TaxItemID = 0
                        objJE.BizDocsPaymentDetailsId = 0
                        objJE.ContactID = 0
                        objJE.ItemID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemCode"))
                        objJE.ProjectID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numProjectID"))
                        objJE.ClassID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numClassID"))
                        objJE.CommissionID = 0
                        objJE.ReconcileID = 0
                        objJE.Cleared = False
                        objJE.ReferenceType = 0
                        objJE.ReferenceID = 0

                        objJEList.Add(objJE)
                    End If

                    If CStr(dvOppBiDocItems.Item(i).Item("Type")) = "P" And CDec(dvOppBiDocItems.Item(i).Item("AverageCost")) > 0 And CBool(dvOppBiDocItems.Item(i).Item("bitDropShip")) <> True Then

                        'Do not include Kit Items for Average Cost
                        If CBool(dvOppBiDocItems.Item(i).Item("bitKitParent")) = False Then
                            decACProductCost = CDec(dvOppBiDocItems.Item(i).Item("unit")) * CDec(dvOppBiDocItems.Item(i).Item("AverageCost"))
                            If decACProductCost > 0 Then
                                objJE = New JournalEntryNew()

                                objJE.TransactionId = 0
                                objJE.DebitAmt = 0
                                objJE.CreditAmt = decACProductCost '* ExchangeRate 'Removed by chintan as average cost is always going to same whenever we place order in any other currency
                                objJE.ChartAcntId = lngSalesClearingAccount
                                objJE.Description = "Sales Clearing" '"Authoritative Accounts"
                                objJE.CustomerId = lngDivisionID
                                objJE.MainDeposit = False
                                objJE.MainCheck = False
                                objJE.MainCashCredit = False
                                objJE.OppitemtCode = CLng(dvOppBiDocItems.Item(i).Item("numoppitemtCode"))
                                objJE.BizDocItems = "SC"
                                objJE.Reference = ""
                                objJE.PaymentMethod = 0
                                objJE.Reconcile = False
                                objJE.CurrencyID = CurrencyID
                                objJE.FltExchangeRate = ExchangeRate
                                objJE.TaxItemID = 0
                                objJE.BizDocsPaymentDetailsId = 0
                                objJE.ContactID = 0
                                objJE.ItemID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemCode"))
                                objJE.ProjectID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numProjectID"))
                                objJE.ClassID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numClassID"))
                                objJE.CommissionID = 0
                                objJE.ReconcileID = 0
                                objJE.Cleared = False
                                objJE.ReferenceType = 0
                                objJE.ReferenceID = 0

                                objJEList.Add(objJE)


                                objJE = New JournalEntryNew()

                                objJE.TransactionId = 0
                                objJE.DebitAmt = decACProductCost '* ExchangeRate 'Removed by chintan as average cost is always going to same whenever we place order in any other currency
                                objJE.CreditAmt = 0
                                objJE.ChartAcntId = lngItemCOGsAccount
                                objJE.Description = "Cost of Goods Sold" '"Authoritative Accounts"
                                objJE.CustomerId = lngDivisionID
                                objJE.MainDeposit = False
                                objJE.MainCheck = False
                                objJE.MainCashCredit = False
                                objJE.OppitemtCode = CLng(dvOppBiDocItems.Item(i).Item("numoppitemtCode"))
                                objJE.BizDocItems = "COG"
                                objJE.Reference = ""
                                objJE.PaymentMethod = 0
                                objJE.Reconcile = False
                                objJE.CurrencyID = CurrencyID
                                objJE.FltExchangeRate = ExchangeRate
                                objJE.TaxItemID = 0
                                objJE.BizDocsPaymentDetailsId = 0
                                objJE.ContactID = 0
                                objJE.ItemID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemCode"))
                                objJE.ProjectID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numProjectID"))
                                objJE.ClassID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numClassID"))
                                objJE.CommissionID = 0
                                objJE.ReconcileID = 0
                                objJE.Cleared = False
                                objJE.ReferenceType = 0
                                objJE.ReferenceID = 0

                                objJEList.Add(objJE)
                            End If
                        End If
                    End If
                Next

                'OpportunityKitItems
                Dim ds As New DataSet
                Dim objOppBizDocs As OppBizDocs = New OppBizDocs
                objOppBizDocs.OppId = lngOppID
                objOppBizDocs.OppBizDocId = lngBizDocID
                Dim dtOppKitItems As DataTable = objOppBizDocs.GetOpportunityKitItemsForAuthorizativeAccounting.Tables(0)
                If dtOppKitItems.Rows.Count > 0 Then
                    For i = 0 To dtOppKitItems.Rows.Count - 1
                        If CCommon.ToLong(dtOppKitItems.Rows(i).Item("ItemIncomeAccount")) > 0 Then
                            lngItemIncomeAccount = CCommon.ToLong(dtOppKitItems.Rows(i).Item("ItemIncomeAccount"))
                        Else
                            lngItemIncomeAccount = ChartOfAccounting.GetDefaultAccount("BE", lngDomainID)
                        End If

                        If CCommon.ToLong(dtOppKitItems.Rows(i).Item("ItemInventoryAsset")) > 0 Then
                            lngItemAssetAccount = CCommon.ToLong(dtOppKitItems.Rows(i).Item("ItemInventoryAsset"))
                        End If

                        If CCommon.ToLong(dtOppKitItems.Rows(i).Item("ItemCoGs")) > 0 Then
                            lngItemCOGsAccount = CCommon.ToLong(dtOppKitItems.Rows(i).Item("ItemCoGs"))
                        Else
                            lngItemCOGsAccount = ChartOfAccounting.GetDefaultAccount("CG", lngDomainID)
                        End If

                        If CStr(dtOppKitItems.Rows(i).Item("Type")) = "P" And CDec(dtOppKitItems.Rows(i).Item("AverageCost")) > 0 And CBool(dtOppKitItems.Rows(i).Item("bitDropShip")) <> True Then
                            decACProductCost = CDec(dtOppKitItems.Rows(i).Item("unit")) * CDec(dtOppKitItems.Rows(i).Item("AverageCost"))
                            If decACProductCost > 0 Then
                                objJE = New JournalEntryNew()

                                objJE.TransactionId = 0
                                objJE.DebitAmt = 0
                                objJE.CreditAmt = decACProductCost '* ExchangeRate 'Removed by chintan as average cost is always going to same whenever we place order in any other currency
                                objJE.ChartAcntId = lngSalesClearingAccount
                                objJE.Description = "Sales Clearing - Kit/Assembly" '"Authoritative Accounts"
                                objJE.CustomerId = lngDivisionID
                                objJE.MainDeposit = False
                                objJE.MainCheck = False
                                objJE.MainCashCredit = False
                                objJE.OppitemtCode = CLng(dtOppKitItems.Rows(i).Item("numoppitemtCode"))
                                objJE.BizDocItems = "SC"
                                objJE.Reference = ""
                                objJE.PaymentMethod = 0
                                objJE.Reconcile = False
                                objJE.CurrencyID = CurrencyID
                                objJE.FltExchangeRate = ExchangeRate
                                objJE.TaxItemID = 0
                                objJE.BizDocsPaymentDetailsId = 0
                                objJE.ContactID = 0
                                objJE.ItemID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("ItemCode"))
                                objJE.ProjectID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("numProjectID"))
                                objJE.ClassID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("numClassID"))
                                objJE.CommissionID = 0
                                objJE.ReconcileID = 0
                                objJE.Cleared = False
                                objJE.ReferenceType = 0
                                objJE.ReferenceID = 0

                                objJEList.Add(objJE)


                                objJE = New JournalEntryNew()

                                objJE.TransactionId = 0
                                objJE.DebitAmt = decACProductCost '* ExchangeRate 'Removed by chintan as average cost is always going to same whenever we place order in any other currency
                                objJE.CreditAmt = 0
                                objJE.ChartAcntId = lngItemCOGsAccount
                                objJE.Description = "Cost of Goods Sold - Kit/Assembly" '"Authoritative Accounts"
                                objJE.CustomerId = lngDivisionID
                                objJE.MainDeposit = False
                                objJE.MainCheck = False
                                objJE.MainCashCredit = False
                                objJE.OppitemtCode = CLng(dtOppKitItems.Rows(i).Item("numoppitemtCode"))
                                objJE.BizDocItems = "COG"
                                objJE.Reference = ""
                                objJE.PaymentMethod = 0
                                objJE.Reconcile = False
                                objJE.CurrencyID = CurrencyID
                                objJE.FltExchangeRate = ExchangeRate
                                objJE.TaxItemID = 0
                                objJE.BizDocsPaymentDetailsId = 0
                                objJE.ContactID = 0
                                objJE.ItemID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("ItemCode"))
                                objJE.ProjectID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("numProjectID"))
                                objJE.ClassID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("numClassID"))
                                objJE.CommissionID = 0
                                objJE.ReconcileID = 0
                                objJE.Cleared = False
                                objJE.ReferenceType = 0
                                objJE.ReferenceID = 0

                                objJEList.Add(objJE)
                            End If
                        End If
                    Next
                End If

                ''For Sales Tax Payable  as Credit in journal Entry
                '' If decTaxAmount <> 0 Then

                'objAuthoritativeBizDocs.AccountTypeId = "0102" ' 827 Sales Tax Payable
                Dim objTaxDetails As New TaxDetails
                objTaxDetails.DomainID = lngDomainID
                objTaxDetails.OppID = lngOppID
                objTaxDetails.DivisionID = lngDivisionID
                objTaxDetails.OppBizDocID = lngBizDocID
                objTaxDetails.TaxItemID = 0

                decTaxAmount = objTaxDetails.GetTaxAmtOppForTaxItem1
                If Math.Abs(decTaxAmount) > 0 Then
                    objJE = New JournalEntryNew()
                    decDebitAmt = 0
                    decCreditAmt = 0
                    If decTaxAmount > 0 Then
                        decDebitAmt = 0
                        decCreditAmt = CDec(Math.Abs(decTaxAmount) * ExchangeRate)
                    ElseIf decTaxAmount < 0 Then
                        decDebitAmt = CDec(Math.Abs(decTaxAmount) * ExchangeRate)
                        decCreditAmt = 0
                    End If


                    objJE.TransactionId = 0
                    objJE.DebitAmt = decDebitAmt
                    objJE.CreditAmt = decCreditAmt
                    objJE.ChartAcntId = ChartOfAccounting.GetDefaultAccount("ST", lngDomainID)
                    objJE.Description = "Sales" '"Authoritative Accounts"
                    objJE.CustomerId = lngDivisionID
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = "ST"
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = CurrencyID
                    objJE.FltExchangeRate = ExchangeRate
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)
                End If

                'CRV TAX
                objTaxDetails.TaxItemID = 1
                decTaxAmount = objTaxDetails.GetTaxAmtOppForTaxItem1
                If Math.Abs(decTaxAmount) > 0 Then
                    objJE = New JournalEntryNew()
                    decDebitAmt = 0
                    decCreditAmt = 0
                    If decTaxAmount > 0 Then
                        decDebitAmt = 0
                        decCreditAmt = CDec(Math.Abs(decTaxAmount) * ExchangeRate)
                    ElseIf decTaxAmount < 0 Then
                        decDebitAmt = CDec(Math.Abs(decTaxAmount) * ExchangeRate)
                        decCreditAmt = 0
                    End If


                    objJE.TransactionId = 0
                    objJE.DebitAmt = decDebitAmt
                    objJE.CreditAmt = decCreditAmt
                    objJE.ChartAcntId = ChartOfAccounting.GetDefaultAccount("ST", lngDomainID)
                    objJE.Description = "Sales" '"Authoritative Accounts"
                    objJE.CustomerId = lngDivisionID
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = "ST"
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = CurrencyID
                    objJE.FltExchangeRate = ExchangeRate
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)
                End If

                ''End If

                Dim dtSalesTax As DataTable
                Dim k As Integer
                dtSalesTax = objTaxDetails.GetTaxItems()
                For k = 0 To dtSalesTax.Rows.Count - 1
                    objTaxDetails.TaxItemID = CLng(dtSalesTax.Rows(k).Item("numTaxItemID"))
                    decTaxAmount = objTaxDetails.GetTaxAmtOppForTaxItem1
                    If Math.Abs(decTaxAmount) > 0 Then
                        objJE = New JournalEntryNew()
                        decDebitAmt = 0
                        decCreditAmt = 0
                        If decTaxAmount > 0 Then
                            decDebitAmt = 0
                            decCreditAmt = CDec(Math.Abs(decTaxAmount) * ExchangeRate)
                        ElseIf decTaxAmount < 0 Then
                            decDebitAmt = CDec(Math.Abs(decTaxAmount) * ExchangeRate)
                            decCreditAmt = 0
                        End If


                        objJE.TransactionId = 0
                        objJE.DebitAmt = decDebitAmt
                        objJE.CreditAmt = decCreditAmt
                        objJE.ChartAcntId = CLng(dtSalesTax.Rows(k).Item("numChartOfAcntID"))
                        objJE.Description = "Sales" ' "Authoritative Accounts"
                        objJE.CustomerId = lngDivisionID
                        objJE.MainDeposit = False
                        objJE.MainCheck = False
                        objJE.MainCashCredit = False
                        objJE.OppitemtCode = 0
                        objJE.BizDocItems = "OT"
                        objJE.Reference = ""
                        objJE.PaymentMethod = 0
                        objJE.Reconcile = False
                        objJE.CurrencyID = CurrencyID
                        objJE.FltExchangeRate = ExchangeRate
                        objJE.TaxItemID = CLng(dtSalesTax.Rows(k).Item("numTaxItemID"))
                        objJE.BizDocsPaymentDetailsId = 0
                        objJE.ContactID = 0
                        objJE.ItemID = 0
                        objJE.ProjectID = 0
                        objJE.ClassID = 0
                        objJE.CommissionID = 0
                        objJE.ReconcileID = 0
                        objJE.Cleared = False
                        objJE.ReferenceType = 0
                        objJE.ReferenceID = 0

                        objJEList.Add(objJE)
                    End If

                Next




                '' For Discount Given as Debit in journal Entry
                If decDiscAmt <> 0 Then
                    'discount given
                    'objAuthoritativeBizDocs.AccountTypeId = 822

                    objJE = New JournalEntryNew()

                    objJE.TransactionId = 0
                    objJE.DebitAmt = CDec(decDiscAmt * ExchangeRate)
                    objJE.CreditAmt = 0
                    objJE.ChartAcntId = CLng(IIf(DiscAcntType > 0, DiscAcntType, ChartOfAccounting.GetDefaultAccount("DG", lngDomainID))) ' 18
                    objJE.Description = "Sales" '"Authoritative Accounts"
                    objJE.CustomerId = lngDivisionID
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = "DG"
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = CurrencyID
                    objJE.FltExchangeRate = ExchangeRate
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)
                End If

                ''store Shipping Cost in Income Account  as Debit Entry -- Reffer Bug ID:222 Solution 1 for change request -by chintan
                ''Follow Journal pattern for service item Debit AR account and credit shipping income account, AR entry already includes Shipping amount.
                If decShipCost <> 0 Then
                    Dim lngShippingIncomeAccountID As Long = GetShippingIncomeAccount(lngShippingMethod, lngDomainID)
                    If lngShippingIncomeAccountID = 0 Then 'get default Shipping account from AccountCharges table
                        lngShippingIncomeAccountID = ChartOfAccounting.GetDefaultAccount("SI", lngDomainID) 'Shippig income
                    End If

                    objJE = New JournalEntryNew()

                    objJE.TransactionId = 0
                    objJE.DebitAmt = 0
                    objJE.CreditAmt = CDec(decShipCost * ExchangeRate)
                    objJE.ChartAcntId = lngShippingIncomeAccountID
                    objJE.Description = "Sales" ' "Authoritative Accounts"
                    objJE.CustomerId = lngDivisionID
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = "SI" ''Shipping income
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = CurrencyID
                    objJE.FltExchangeRate = ExchangeRate
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)

                    ''''' Adding bill for shipping expense will be made by user manually when shipping company sends them bill
                    ''Add shipping amount as bill from money out section
                    'Dim objOppInvoice As New Opportunities.OppInvoice
                    'objOppInvoice.AmtPaid = decShipCost * CCommon.ToDecimal(ExchangeRate)
                    'objOppInvoice.UserCntID = CCommon.ToLong(System.Web.HttpContext.Current.Session("UserContactID"))
                    'objOppInvoice.PaymentMethod = 4
                    'objOppInvoice.Reference = "Pay Shipping Amount to Vendor"
                    'objOppInvoice.Memo = ""
                    'objOppInvoice.DomainId = CCommon.ToInteger(System.Web.HttpContext.Current.Session("DomainId"))
                    'objOppInvoice.ExpenseAcount = lngShippingAccountID
                    'objOppInvoice.DivisionID = lngDivisionID
                    'objOppInvoice.DueDate = Now.UtcNow
                    'objOppInvoice.CurrencyID = CCommon.ToLong(System.Web.HttpContext.Current.Session("BaseCurrencyID"))
                    'objOppInvoice.AddBillDetails()
                End If

                ''For Late Charges  as Credit in journal Entry
                If decLateCharge <> 0 Then

                    'objAuthoritativeBizDocs.AccountTypeId = 822

                    objJE = New JournalEntryNew()

                    objJE.TransactionId = 0
                    objJE.DebitAmt = 0
                    objJE.CreditAmt = CDec(decLateCharge * ExchangeRate)
                    objJE.ChartAcntId = ChartOfAccounting.GetDefaultAccount("LC", lngDomainID) ' 21
                    objJE.Description = "Sales" ' "Authoritative Accounts"
                    objJE.CustomerId = lngDivisionID
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = "LC" ''Late Charges
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = CurrencyID
                    objJE.FltExchangeRate = ExchangeRate
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)
                End If

                'Add Embedded Cost entries 

                'Impact to accounting:
                'Credit : A/P (And save the Division ID, so that we can show the vendor name in GL and save the expense name in the description of journal details table) OR the Expense A/C Selected - 100

                Dim objOpp As New MOpportunity
                Dim lngVendorAPAccountID, lngVendorDivID As Long
                objOpp.DomainID = lngDomainID
                objOpp.CostCategoryID = 0
                objOpp.OppBizDocID = lngBizDocID
                Dim dtEmbeddedCost As DataTable = objOpp.GetEmbeddedCost()
                Dim decEmbeddedCost As Decimal = 0
                objOppBizDocs = New Opportunities.OppBizDocs

                If dtEmbeddedCost.Rows.Count > 0 Then
                    For Each row As DataRow In dtEmbeddedCost.Rows

                        'Add Journal on actual cost
                        If CCommon.ToDecimal(row("monActualCost")) > 0 Then
                            If CCommon.ToLong(row("numDivisionID")) > 0 Then
                                lngVendorAPAccountID = objOppBizDocs.ValidateCustomerAR_APAccounts("AP", lngDomainID, CCommon.ToLong(row("numDivisionID")))
                                lngVendorDivID = CCommon.ToLong(row("numDivisionID"))
                            Else
                                lngVendorAPAccountID = CCommon.ToLong(row("numAccountID"))
                                lngVendorDivID = lngDivisionID
                            End If

                            objJE = New JournalEntryNew()

                            objJE.TransactionId = 0
                            objJE.DebitAmt = 0
                            objJE.CreditAmt = CDec(CCommon.ToDecimal(row("monActualCost")) * ExchangeRate)
                            objJE.ChartAcntId = lngVendorAPAccountID
                            objJE.Description = "Embedded Cost" & " (" & row("vcAccountName").ToString & ")"
                            objJE.CustomerId = lngVendorDivID
                            objJE.MainDeposit = False
                            objJE.MainCheck = False
                            objJE.MainCashCredit = False
                            objJE.OppitemtCode = 0
                            objJE.BizDocItems = "EC" ''Embedded Cost
                            objJE.Reference = ""
                            objJE.PaymentMethod = 0
                            objJE.Reconcile = False
                            objJE.CurrencyID = CurrencyID
                            objJE.FltExchangeRate = ExchangeRate
                            objJE.TaxItemID = 0
                            objJE.BizDocsPaymentDetailsId = 0
                            objJE.ContactID = 0
                            objJE.ItemID = 0
                            objJE.ProjectID = 0
                            objJE.ClassID = 0
                            objJE.CommissionID = 0
                            objJE.ReconcileID = 0
                            objJE.Cleared = False
                            objJE.ReferenceType = 0
                            objJE.ReferenceID = 0

                            objJEList.Add(objJE)
                        End If

                        'Add Bill first time, rest time it will be update amount to bill directly
                        If CCommon.ToLong(row("numBizDocsPaymentDetId")) = 0 And CCommon.ToDecimal(row("monActualCost")) > 0 Then
                            Dim objOppInvoice As New Opportunities.OppInvoice
                            objOppInvoice.EmbeddedCostID = CCommon.ToLong(row("numEmbeddedCostID"))
                            objOppInvoice.AmtPaid = CCommon.ToDecimal(row("monActualCost")) * CCommon.ToDecimal(ExchangeRate)
                            objOppInvoice.UserCntID = CCommon.ToLong(System.Web.HttpContext.Current.Session("UserContactID"))
                            objOppInvoice.PaymentMethod = CCommon.ToInteger(row("numPaymentMethod"))
                            objOppInvoice.Reference = "Pay Embedded Cost to Vendor"
                            objOppInvoice.Memo = CCommon.ToString(row("vcMemo"))
                            objOppInvoice.DomainID = CCommon.ToInteger(lngDomainID)
                            objOppInvoice.ExpenseAcount = CCommon.ToLong(row("numAccountID"))
                            objOppInvoice.DivisionID = CCommon.ToLong(row("numDivisionID"))
                            objOppInvoice.DueDate = CDate(row("dtDueDate1"))
                            objOppInvoice.CurrencyID = CCommon.ToLong(System.Web.HttpContext.Current.Session("BaseCurrencyID"))
                            objOppInvoice.PaymentType = OppInvoice.BillPaymentType.Bill
                            objOppInvoice.AddBillDetails()
                        End If
                    Next
                    decEmbeddedCost = CCommon.ToDecimal(dtEmbeddedCost.Compute("sum(monActualCost)", ""))
                End If


                'For Account Receivable as Debit Entry in Journal
                'GetDefaultAccountID("AR", lngDomainID)
                'objAuthoritativeBizDocs.DomainId = lngDomainID
                'objAuthoritativeBizDocs.ChargeCode = "AR"
                decAmount = CDec(TotalAmount * ExchangeRate)
                decDebitAmt = 0
                decCreditAmt = 0
                If decAmount > 0.0 Then
                    decDebitAmt = Math.Abs(decAmount) + decEmbeddedCost
                    decCreditAmt = 0
                ElseIf decAmount < 0.0 Then
                    decDebitAmt = 0
                    decCreditAmt = Math.Abs(decAmount) + decEmbeddedCost
                End If

                If decDebitAmt <> 0 Or decCreditAmt <> 0 Then
                    Dim lngARAccountId As Long
                    objOppBizDocs.OppBizDocId = lngBizDocID
                    lngARAccountId = CCommon.ToLong(objOppBizDocs.GetBizDocARAccountID())

                    If lngARAccountId = 0 Then
                        lngARAccountId = objOppBizDocs.ValidateCustomerAR_APAccounts("AR", lngDomainID, lngDivisionID)
                    End If

                    objJE = New JournalEntryNew()

                    objJE.TransactionId = 0
                    objJE.DebitAmt = decDebitAmt
                    objJE.CreditAmt = decCreditAmt
                    If bitInvoiceAgainstDeferredIncomeBizDoc Then
                        objJE.ChartAcntId = lngDeferredIncomeAccount
                    Else
                        objJE.ChartAcntId = lngARAccountId 'objAuthoritativeBizDocs.GetChartAcountIdForAuthorizativeBizDocs '2
                    End If
                    If bitInvoiceAgainstDeferredIncomeBizDoc Then
                        objJE.Description = "Sales - Deferred Income" ' "Authoritative Accounts"
                    Else
                        objJE.Description = "Sales" ' "Authoritative Accounts"
                    End If
                    objJE.CustomerId = lngDivisionID
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = 0
                    If bitInvoiceAgainstDeferredIncomeBizDoc Then
                        objJE.BizDocItems = "DI"
                    Else
                        objJE.BizDocItems = "AR"
                    End If
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = CurrencyID
                    objJE.FltExchangeRate = ExchangeRate
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)
                End If
                'Add Commission Journal Entry
                'Default Payroll Liability Account is Credited
                'Default Commission Exp Account is Debited 
                Dim EmployeeExpensesAccount As Long
                Dim ContractEmployeeExpenses As Long
                Dim APAccount As Long
                Dim objReceivePayment As New ReceivePayment
                Dim dtComission As New DataTable
                Dim objPayRoll As PayrollExpenses
                'Dim lngComJnID As Long
                'Dim lngComTotal As Double
                'Dim intRow As Integer

                objPayRoll = New PayrollExpenses
                objPayRoll.DomainID = lngDomainID

                'Below line is commented because now commission is not calculated when bizdoc is added but its calculated from Accounting -> Payroll Expense
                'dtComission = objPayRoll.GetComissionDetails(lngBizDocID, lngOppID)

                ''Don't make Commission Journal entry when Auth BizDoc Created(Only calculate commission and insert into BizDocCommission). We already create journal entry from Payroll when user approve payroll.

                'If dtComission.Rows.Count > 0 Then
                '    objAuthoritativeBizDocs.DomainID = lngDomainID

                '    'Employee Payroll Expense
                '    EmployeeExpensesAccount = ChartOfAccounting.GetDefaultAccount("EP", lngDomainID)

                '    'Contract Employee Payroll Expense
                '    ContractEmployeeExpenses = ChartOfAccounting.GetDefaultAccount("CP", lngDomainID)

                '    'Payroll Liability 
                '    APAccount = ChartOfAccounting.GetDefaultAccount("PL", lngDomainID)
                '    Dim comRow As DataRow
                '    'Dim lngTotalCom As Double

                '    'lngTotalCom = 0

                '    Dim lngDomainDivID As Long = 0

                '    Dim objCommon As New CCommon
                '    objCommon.Mode = 18
                '    objCommon.DomainID = lngDomainID
                '    lngDomainDivID = CCommon.ToLong(objCommon.GetSingleFieldValue())

                '    For Each comRow In dtComission.Rows
                '        objJE = New JournalEntryNew()

                '        objJE.TransactionId = 0
                '        objJE.DebitAmt = CDec(CCommon.ToDecimal(comRow.Item("numComission")) * ExchangeRate)
                '        objJE.CreditAmt = 0

                '        If CCommon.ToBool(comRow.Item("bitEmpContract")) = True Then
                '            objJE.ChartAcntId = ContractEmployeeExpenses
                '            objJE.BizDocItems = "CP"
                '        Else
                '            objJE.ChartAcntId = EmployeeExpensesAccount
                '            objJE.BizDocItems = "EP"
                '        End If

                '        objJE.Description = "Sales" ' "Commission Expenses (" & CCommon.ToString(comRow.Item("vcItemName")) & ") - " & CCommon.ToString(comRow.Item("vcContactName"))
                '        objJE.CustomerId = lngDomainDivID
                '        objJE.MainDeposit = False
                '        objJE.MainCheck = False
                '        objJE.MainCashCredit = False
                '        objJE.OppitemtCode = CLng(comRow.Item("numoppitemtCode"))
                '        objJE.Reference = ""
                '        objJE.PaymentMethod = 0
                '        objJE.Reconcile = False
                '        objJE.CurrencyID = CurrencyID
                '        objJE.FltExchangeRate = ExchangeRate
                '        objJE.TaxItemID = 0
                '        objJE.BizDocsPaymentDetailsId = 0
                '        objJE.ContactID = CLng(comRow.Item("numcontactid"))
                '        objJE.ItemID = 0
                '        objJE.ProjectID = 0
                '        objJE.ClassID = 0
                '        objJE.CommissionID = CLng(comRow.Item("numComissionID"))
                '        objJE.ReconcileID = 0
                '        objJE.Cleared = False
                '        objJE.ReferenceType = 0
                '        objJE.ReferenceID = 0

                '        objJEList.Add(objJE)

                '        'Debit : Commission Expense
                '        objJE = New JournalEntryNew()

                '        objJE.TransactionId = 0
                '        objJE.DebitAmt = 0
                '        objJE.CreditAmt = CDec(CCommon.ToDecimal(comRow.Item("numComission")) * ExchangeRate)
                '        objJE.ChartAcntId = APAccount
                '        objJE.Description = "Sales" ' "Commission Expenses (" & CCommon.ToString(comRow.Item("vcItemName")) & ") - " & CCommon.ToString(comRow.Item("vcContactName"))
                '        objJE.CustomerId = lngDomainDivID
                '        objJE.MainDeposit = False
                '        objJE.MainCheck = False
                '        objJE.MainCashCredit = False
                '        objJE.OppitemtCode = CLng(comRow.Item("numoppitemtCode"))
                '        objJE.BizDocItems = "PL"
                '        objJE.Reference = ""
                '        objJE.PaymentMethod = 0
                '        objJE.Reconcile = False
                '        objJE.CurrencyID = CurrencyID
                '        objJE.FltExchangeRate = ExchangeRate
                '        objJE.TaxItemID = 0
                '        objJE.BizDocsPaymentDetailsId = 0
                '        objJE.ContactID = CLng(comRow.Item("numcontactid"))
                '        objJE.ItemID = 0
                '        objJE.ProjectID = 0
                '        objJE.ClassID = 0
                '        objJE.CommissionID = CLng(comRow.Item("numComissionID"))
                '        objJE.ReconcileID = 0
                '        objJE.Cleared = False
                '        objJE.ReferenceType = 0
                '        objJE.ReferenceID = 0

                '        objJEList.Add(objJE)
                '    Next
                'End If

                '--------Commission Expense Ends----------------------

                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalID, lngDomainID)

                'ds.Tables.Add(dt)
                'ValidateJournalEntries(ds, lngJournalID, lngDomainID) 'Added to detect imbalance in journals before posting it to accounting system
                'lstr = ds.GetXml()
                'objAuthoritativeBizDocs.JournalId = lngJournalID
                'objAuthoritativeBizDocs.Mode = 0
                'objAuthoritativeBizDocs.RecurringMode = 0
                'objAuthoritativeBizDocs.JournalDetails = lstr
                'objAuthoritativeBizDocs.DomainID = lngDomainID
                'objAuthoritativeBizDocs.OppBIzDocID = lngBizDocID
                'objAuthoritativeBizDocs.OppId = lngOppID
                'objAuthoritativeBizDocs.SaveDataToJournalDetailsForAuthoritativeBizDocs()

                'Distibute Total cost among bizdoc items and remove old cost when no cost specified
                objOpp.OppBizDocID = lngBizDocID
                objOpp.Mode = 1
                objOpp.ManageEmbeddedCost()
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' <summary>
        ''' In new method for sales journal entries, we are not impacting inventory and cost of goods sold account when invoice are added. Inventory and cogs account are impacted when fulfillment bizdocs are added.
        ''' </summary>
        ''' <param name="lngOppID"></param>
        ''' <param name="lngDomainID"></param>
        ''' <param name="dtOppBiDocItems"></param>
        ''' <param name="lngJournalID"></param>
        ''' <param name="lngBizDocID"></param>
        ''' <param name="TotalAmount"></param>
        ''' <param name="decDiscAmt"></param>
        ''' <param name="decShipCost"></param>
        ''' <param name="decLateCharge"></param>
        ''' <param name="lngDivisionID"></param>
        ''' <param name="CurrencyID"></param>
        ''' <param name="ExchangeRate"></param>
        ''' <param name="DiscAcntType"></param>
        ''' <param name="lngShippingMethod"></param>
        ''' <param name="bitInvoiceAgainstDeferredIncomeBizDoc"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function SaveJournalEntriesSalesNew(ByVal lngOppID As Long, ByVal lngDomainID As Long, ByVal dtOppBiDocItems As DataTable, ByVal lngJournalID As Long, ByVal lngBizDocID As Long, ByVal TotalAmount As Decimal, ByVal decDiscAmt As Decimal, ByVal decShipCost As Decimal, ByVal decLateCharge As Decimal, ByVal lngDivisionID As Long, ByVal CurrencyID As Long, ByVal ExchangeRate As Double, Optional ByVal DiscAcntType As Long = 0, Optional ByVal lngShippingMethod As Long = 0, Optional ByVal bitInvoiceAgainstDeferredIncomeBizDoc As Boolean = False) As Boolean
            Try
                If ExchangeRate = 0 Then
                    Throw New Exception("EXCHANGE_RATE_CAN_NOT_BE_0")
                    Exit Function
                End If

                Dim i As Integer
                Dim objAuthoritativeBizDocs As New AuthoritativeBizDocs
                Dim decACProductCost, decTaxAmount As Decimal
                Dim lngItemIncomeAccount, lngDeferredIncomeAccount As Long

                If bitInvoiceAgainstDeferredIncomeBizDoc Then
                    lngDeferredIncomeAccount = ChartOfAccounting.GetDefaultAccount("DI", lngDomainID)
                    If lngDeferredIncomeAccount = 0 Then
                        Throw New Exception("DEFAULT_DEFERRED_INCOME_ACCOUNT_NOT_AVAILABLE")
                        Exit Function
                    End If
                End If

                Dim objJEList As New JournalEntryCollection

                Dim objJE As New JournalEntryNew()

                Dim dvOppBiDocItems As DataView

                dvOppBiDocItems = New DataView(dtOppBiDocItems)
                '' dvOppBiDocItems.RowFilter = " ItemCode <> 0 "  -- Temporarily Commented
                Dim decAmount, decDebitAmt, decCreditAmt As Decimal

                For i = 0 To dvOppBiDocItems.Count - 1
                    decAmount = CDec(CCommon.ToDecimal(dvOppBiDocItems.Item(i).Item("Amount")) * ExchangeRate)
                    decDebitAmt = 0
                    decCreditAmt = 0
                    If decAmount <> 0 Then
                        If decAmount > 0.0 Then
                            decDebitAmt = 0
                            decCreditAmt = Math.Abs(decAmount)
                        ElseIf decAmount < 0.0 Then
                            decDebitAmt = Math.Abs(decAmount)
                            decCreditAmt = 0
                        End If

                        If CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemIncomeAccount")) > 0 Then
                            lngItemIncomeAccount = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemIncomeAccount"))
                        Else
                            lngItemIncomeAccount = ChartOfAccounting.GetDefaultAccount("BE", lngDomainID)
                        End If

                        objJE = New JournalEntryNew()

                        objJE.TransactionId = 0
                        objJE.DebitAmt = decDebitAmt
                        objJE.CreditAmt = decCreditAmt
                        objJE.ChartAcntId = lngItemIncomeAccount
                        objJE.Description = "Income" '"Authoritative Accounts"
                        objJE.CustomerId = lngDivisionID
                        objJE.MainDeposit = False
                        objJE.MainCheck = False
                        objJE.MainCashCredit = False
                        objJE.OppitemtCode = CLng(dvOppBiDocItems.Item(i).Item("numoppitemtCode"))
                        objJE.BizDocItems = CStr(dvOppBiDocItems.Item(i).Item("ItemType"))
                        objJE.Reference = ""
                        objJE.PaymentMethod = 0
                        objJE.Reconcile = False
                        objJE.CurrencyID = CurrencyID
                        objJE.FltExchangeRate = ExchangeRate
                        objJE.TaxItemID = 0
                        objJE.BizDocsPaymentDetailsId = 0
                        objJE.ContactID = 0
                        objJE.ItemID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemCode"))
                        objJE.ProjectID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numProjectID"))
                        objJE.ClassID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numClassID"))
                        objJE.CommissionID = 0
                        objJE.ReconcileID = 0
                        objJE.Cleared = False
                        objJE.ReferenceType = 0
                        objJE.ReferenceID = 0

                        objJEList.Add(objJE)
                    End If
                Next

                ''For Sales Tax Payable  as Credit in journal Entry
                '' If decTaxAmount <> 0 Then

                'objAuthoritativeBizDocs.AccountTypeId = "0102" ' 827 Sales Tax Payable
                Dim objTaxDetails As New TaxDetails
                objTaxDetails.DomainID = lngDomainID
                objTaxDetails.OppID = lngOppID
                objTaxDetails.DivisionID = lngDivisionID
                objTaxDetails.OppBizDocID = lngBizDocID
                objTaxDetails.TaxItemID = 0

                decTaxAmount = objTaxDetails.GetTaxAmtOppForTaxItem1
                If Math.Abs(decTaxAmount) > 0 Then
                    objJE = New JournalEntryNew()
                    decDebitAmt = 0
                    decCreditAmt = 0
                    If decTaxAmount > 0 Then
                        decDebitAmt = 0
                        decCreditAmt = CDec(Math.Abs(decTaxAmount) * ExchangeRate)
                    ElseIf decTaxAmount < 0 Then
                        decDebitAmt = CDec(Math.Abs(decTaxAmount) * ExchangeRate)
                        decCreditAmt = 0
                    End If


                    objJE.TransactionId = 0
                    objJE.DebitAmt = decDebitAmt
                    objJE.CreditAmt = decCreditAmt
                    objJE.ChartAcntId = ChartOfAccounting.GetDefaultAccount("ST", lngDomainID)
                    objJE.Description = "Sales" '"Authoritative Accounts"
                    objJE.CustomerId = lngDivisionID
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = "ST"
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = CurrencyID
                    objJE.FltExchangeRate = ExchangeRate
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)
                End If

                'CRV TAX
                objTaxDetails.TaxItemID = 1
                decTaxAmount = objTaxDetails.GetTaxAmtOppForTaxItem1
                If Math.Abs(decTaxAmount) > 0 Then
                    objJE = New JournalEntryNew()
                    decDebitAmt = 0
                    decCreditAmt = 0
                    If decTaxAmount > 0 Then
                        decDebitAmt = 0
                        decCreditAmt = CDec(Math.Abs(decTaxAmount) * ExchangeRate)
                    ElseIf decTaxAmount < 0 Then
                        decDebitAmt = CDec(Math.Abs(decTaxAmount) * ExchangeRate)
                        decCreditAmt = 0
                    End If


                    objJE.TransactionId = 0
                    objJE.DebitAmt = decDebitAmt
                    objJE.CreditAmt = decCreditAmt
                    objJE.ChartAcntId = ChartOfAccounting.GetDefaultAccount("ST", lngDomainID)
                    objJE.Description = "Sales" '"Authoritative Accounts"
                    objJE.CustomerId = lngDivisionID
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = "ST"
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = CurrencyID
                    objJE.FltExchangeRate = ExchangeRate
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)
                End If

                ''End If

                Dim dtSalesTax As DataTable
                Dim k As Integer
                dtSalesTax = objTaxDetails.GetTaxItems()
                For k = 0 To dtSalesTax.Rows.Count - 1
                    objTaxDetails.TaxItemID = CLng(dtSalesTax.Rows(k).Item("numTaxItemID"))
                    decTaxAmount = objTaxDetails.GetTaxAmtOppForTaxItem1
                    If Math.Abs(decTaxAmount) > 0 Then
                        objJE = New JournalEntryNew()
                        decDebitAmt = 0
                        decCreditAmt = 0
                        If decTaxAmount > 0 Then
                            decDebitAmt = 0
                            decCreditAmt = CDec(Math.Abs(decTaxAmount) * ExchangeRate)
                        ElseIf decTaxAmount < 0 Then
                            decDebitAmt = CDec(Math.Abs(decTaxAmount) * ExchangeRate)
                            decCreditAmt = 0
                        End If


                        objJE.TransactionId = 0
                        objJE.DebitAmt = decDebitAmt
                        objJE.CreditAmt = decCreditAmt
                        objJE.ChartAcntId = CLng(dtSalesTax.Rows(k).Item("numChartOfAcntID"))
                        objJE.Description = "Sales" ' "Authoritative Accounts"
                        objJE.CustomerId = lngDivisionID
                        objJE.MainDeposit = False
                        objJE.MainCheck = False
                        objJE.MainCashCredit = False
                        objJE.OppitemtCode = 0
                        objJE.BizDocItems = "OT"
                        objJE.Reference = ""
                        objJE.PaymentMethod = 0
                        objJE.Reconcile = False
                        objJE.CurrencyID = CurrencyID
                        objJE.FltExchangeRate = ExchangeRate
                        objJE.TaxItemID = CLng(dtSalesTax.Rows(k).Item("numTaxItemID"))
                        objJE.BizDocsPaymentDetailsId = 0
                        objJE.ContactID = 0
                        objJE.ItemID = 0
                        objJE.ProjectID = 0
                        objJE.ClassID = 0
                        objJE.CommissionID = 0
                        objJE.ReconcileID = 0
                        objJE.Cleared = False
                        objJE.ReferenceType = 0
                        objJE.ReferenceID = 0

                        objJEList.Add(objJE)
                    End If

                Next

                '' For Discount Given as Debit in journal Entry
                If decDiscAmt <> 0 Then
                    'discount given
                    'objAuthoritativeBizDocs.AccountTypeId = 822

                    objJE = New JournalEntryNew()

                    objJE.TransactionId = 0
                    objJE.DebitAmt = CDec(decDiscAmt * ExchangeRate)
                    objJE.CreditAmt = 0
                    objJE.ChartAcntId = CLng(IIf(DiscAcntType > 0, DiscAcntType, ChartOfAccounting.GetDefaultAccount("DG", lngDomainID))) ' 18
                    objJE.Description = "Sales" '"Authoritative Accounts"
                    objJE.CustomerId = lngDivisionID
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = "DG"
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = CurrencyID
                    objJE.FltExchangeRate = ExchangeRate
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)
                End If

                ''store Shipping Cost in Income Account  as Debit Entry -- Reffer Bug ID:222 Solution 1 for change request -by chintan
                ''Follow Journal pattern for service item Debit AR account and credit shipping income account, AR entry already includes Shipping amount.
                If decShipCost <> 0 Then
                    Dim lngShippingIncomeAccountID As Long = GetShippingIncomeAccount(lngShippingMethod, lngDomainID)
                    If lngShippingIncomeAccountID = 0 Then 'get default Shipping account from AccountCharges table
                        lngShippingIncomeAccountID = ChartOfAccounting.GetDefaultAccount("SI", lngDomainID) 'Shippig income
                    End If

                    objJE = New JournalEntryNew()

                    objJE.TransactionId = 0
                    objJE.DebitAmt = 0
                    objJE.CreditAmt = CDec(decShipCost * ExchangeRate)
                    objJE.ChartAcntId = lngShippingIncomeAccountID
                    objJE.Description = "Sales" ' "Authoritative Accounts"
                    objJE.CustomerId = lngDivisionID
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = "SI" ''Shipping income
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = CurrencyID
                    objJE.FltExchangeRate = ExchangeRate
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)
                End If

                ''For Late Charges  as Credit in journal Entry
                If decLateCharge <> 0 Then

                    'objAuthoritativeBizDocs.AccountTypeId = 822

                    objJE = New JournalEntryNew()

                    objJE.TransactionId = 0
                    objJE.DebitAmt = 0
                    objJE.CreditAmt = CDec(decLateCharge * ExchangeRate)
                    objJE.ChartAcntId = ChartOfAccounting.GetDefaultAccount("LC", lngDomainID) ' 21
                    objJE.Description = "Sales" ' "Authoritative Accounts"
                    objJE.CustomerId = lngDivisionID
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = "LC" ''Late Charges
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = CurrencyID
                    objJE.FltExchangeRate = ExchangeRate
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)
                End If

                'Add Embedded Cost entries 

                'Impact to accounting:
                'Credit : A/P (And save the Division ID, so that we can show the vendor name in GL and save the expense name in the description of journal details table) OR the Expense A/C Selected - 100

                Dim objOpp As New MOpportunity
                Dim lngVendorAPAccountID, lngVendorDivID As Long
                objOpp.DomainID = lngDomainID
                objOpp.CostCategoryID = 0
                objOpp.OppBizDocID = lngBizDocID
                Dim dtEmbeddedCost As DataTable = objOpp.GetEmbeddedCost()
                Dim decEmbeddedCost As Decimal = 0
                Dim objOppBizDocs As OppBizDocs = New OppBizDocs

                If dtEmbeddedCost.Rows.Count > 0 Then
                    For Each row As DataRow In dtEmbeddedCost.Rows

                        'Add Journal on actual cost
                        If CCommon.ToDecimal(row("monActualCost")) > 0 Then
                            If CCommon.ToLong(row("numDivisionID")) > 0 Then
                                lngVendorAPAccountID = objOppBizDocs.ValidateCustomerAR_APAccounts("AP", lngDomainID, CCommon.ToLong(row("numDivisionID")))
                                lngVendorDivID = CCommon.ToLong(row("numDivisionID"))
                            Else
                                lngVendorAPAccountID = CCommon.ToLong(row("numAccountID"))
                                lngVendorDivID = lngDivisionID
                            End If

                            objJE = New JournalEntryNew()

                            objJE.TransactionId = 0
                            objJE.DebitAmt = 0
                            objJE.CreditAmt = CDec(CCommon.ToDecimal(row("monActualCost")) * ExchangeRate)
                            objJE.ChartAcntId = lngVendorAPAccountID
                            objJE.Description = "Embedded Cost" & " (" & row("vcAccountName").ToString & ")"
                            objJE.CustomerId = lngVendorDivID
                            objJE.MainDeposit = False
                            objJE.MainCheck = False
                            objJE.MainCashCredit = False
                            objJE.OppitemtCode = 0
                            objJE.BizDocItems = "EC" ''Embedded Cost
                            objJE.Reference = ""
                            objJE.PaymentMethod = 0
                            objJE.Reconcile = False
                            objJE.CurrencyID = CurrencyID
                            objJE.FltExchangeRate = ExchangeRate
                            objJE.TaxItemID = 0
                            objJE.BizDocsPaymentDetailsId = 0
                            objJE.ContactID = 0
                            objJE.ItemID = 0
                            objJE.ProjectID = 0
                            objJE.ClassID = 0
                            objJE.CommissionID = 0
                            objJE.ReconcileID = 0
                            objJE.Cleared = False
                            objJE.ReferenceType = 0
                            objJE.ReferenceID = 0

                            objJEList.Add(objJE)
                        End If

                        'Add Bill first time, rest time it will be update amount to bill directly
                        If CCommon.ToLong(row("numBizDocsPaymentDetId")) = 0 And CCommon.ToDecimal(row("monActualCost")) > 0 Then
                            Dim objOppInvoice As New Opportunities.OppInvoice
                            objOppInvoice.EmbeddedCostID = CCommon.ToLong(row("numEmbeddedCostID"))
                            objOppInvoice.AmtPaid = CCommon.ToDecimal(row("monActualCost")) * CCommon.ToDecimal(ExchangeRate)
                            objOppInvoice.UserCntID = CCommon.ToLong(System.Web.HttpContext.Current.Session("UserContactID"))
                            objOppInvoice.PaymentMethod = CCommon.ToInteger(row("numPaymentMethod"))
                            objOppInvoice.Reference = "Pay Embedded Cost to Vendor"
                            objOppInvoice.Memo = CCommon.ToString(row("vcMemo"))
                            objOppInvoice.DomainID = CCommon.ToInteger(lngDomainID)
                            objOppInvoice.ExpenseAcount = CCommon.ToLong(row("numAccountID"))
                            objOppInvoice.DivisionID = CCommon.ToLong(row("numDivisionID"))
                            objOppInvoice.DueDate = CDate(row("dtDueDate1"))
                            objOppInvoice.CurrencyID = CCommon.ToLong(System.Web.HttpContext.Current.Session("BaseCurrencyID"))
                            objOppInvoice.PaymentType = OppInvoice.BillPaymentType.Bill
                            objOppInvoice.AddBillDetails()
                        End If
                    Next
                    decEmbeddedCost = CCommon.ToDecimal(dtEmbeddedCost.Compute("sum(monActualCost)", ""))
                End If


                'For Account Receivable as Debit Entry in Journal
                'GetDefaultAccountID("AR", lngDomainID)
                'objAuthoritativeBizDocs.DomainId = lngDomainID
                'objAuthoritativeBizDocs.ChargeCode = "AR"
                decAmount = CDec(TotalAmount * ExchangeRate)
                decDebitAmt = 0
                decCreditAmt = 0
                If decAmount > 0.0 Then
                    decDebitAmt = Math.Abs(decAmount) + decEmbeddedCost
                    decCreditAmt = 0
                ElseIf decAmount < 0.0 Then
                    decDebitAmt = 0
                    decCreditAmt = Math.Abs(decAmount) + decEmbeddedCost
                End If

                If decDebitAmt <> 0 Or decCreditAmt <> 0 Then
                    Dim lngARAccountId As Long
                    objOppBizDocs.OppBizDocId = lngBizDocID
                    lngARAccountId = CCommon.ToLong(objOppBizDocs.GetBizDocARAccountID())

                    If lngARAccountId = 0 Then
                        lngARAccountId = objOppBizDocs.ValidateCustomerAR_APAccounts("AR", lngDomainID, lngDivisionID)
                    End If

                    objJE = New JournalEntryNew()

                    objJE.TransactionId = 0
                    objJE.DebitAmt = decDebitAmt
                    objJE.CreditAmt = decCreditAmt
                    If bitInvoiceAgainstDeferredIncomeBizDoc Then
                        objJE.ChartAcntId = lngDeferredIncomeAccount
                    Else
                        objJE.ChartAcntId = lngARAccountId 'objAuthoritativeBizDocs.GetChartAcountIdForAuthorizativeBizDocs '2
                    End If
                    If bitInvoiceAgainstDeferredIncomeBizDoc Then
                        objJE.Description = "Sales - Deferred Income" ' "Authoritative Accounts"
                    Else
                        objJE.Description = "Sales" ' "Authoritative Accounts"
                    End If
                    objJE.CustomerId = lngDivisionID
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = 0
                    If bitInvoiceAgainstDeferredIncomeBizDoc Then
                        objJE.BizDocItems = "DI"
                    Else
                        objJE.BizDocItems = "AR"
                    End If
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = CurrencyID
                    objJE.FltExchangeRate = ExchangeRate
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)
                End If

                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalID, lngDomainID)

                'Distibute Total cost among bizdoc items and remove old cost when no cost specified
                objOpp.OppBizDocID = lngBizDocID
                objOpp.Mode = 1
                objOpp.ManageEmbeddedCost()
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        'Added by sandeep for BizAPi. please change logic if changes are made in method SaveJournalEntriesSales() Method
        Public Function SaveJournalEntriesSalesBizAPI(ByVal lngOppID As Long, ByVal lngDomainID As Long, ByVal dtOppBiDocItems As DataTable, ByVal lngJournalID As Long, ByVal lngBizDocID As Long, ByVal TotalAmount As Decimal, ByVal decDiscAmt As Decimal, ByVal decShipCost As Decimal, ByVal decLateCharge As Decimal, ByVal lngDivisionID As Long, ByVal CurrencyID As Long, ByVal ExchangeRate As Double, ByVal userContactID As Long, ByVal baseCurrencyID As Long, Optional ByVal DiscAcntType As Long = 0, Optional ByVal lngShippingMethod As Long = 0) As Boolean
            Try
                If ExchangeRate = 0 Then
                    Throw New Exception("EXCHANGE_RATE_CAN_NOT_BE_0")
                    Exit Function
                End If

                Dim i As Integer
                Dim objAuthoritativeBizDocs As New AuthoritativeBizDocs
                Dim decACProductCost, decTaxAmount As Decimal
                Dim lngItemIncomeAccount As Long
                Dim lngItemAssetAccount As Long
                Dim lngItemCOGsAccount As Long

                'ExchangeRate = 1 / ExchangeRate

                Dim objJEList As New JournalEntryCollection

                Dim objJE As New JournalEntryNew()

                Dim dvOppBiDocItems As DataView

                dvOppBiDocItems = New DataView(dtOppBiDocItems)
                '' dvOppBiDocItems.RowFilter = " ItemCode <> 0 "  -- Temporarily Commented
                Dim decAmount, decDebitAmt, decCreditAmt As Decimal

                For i = 0 To dvOppBiDocItems.Count - 1
                    decAmount = CDec(CCommon.ToDecimal(dvOppBiDocItems.Item(i).Item("Amount")) * ExchangeRate)
                    decDebitAmt = 0
                    decCreditAmt = 0
                    If decAmount <> 0 Then
                        If decAmount > 0.0 Then
                            decDebitAmt = 0
                            decCreditAmt = Math.Abs(decAmount)
                        ElseIf decAmount < 0.0 Then
                            decDebitAmt = Math.Abs(decAmount)
                            decCreditAmt = 0
                        End If

                        If CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemIncomeAccount")) > 0 Then
                            lngItemIncomeAccount = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemIncomeAccount"))
                        Else
                            lngItemIncomeAccount = ChartOfAccounting.GetDefaultAccount("BE", lngDomainID)
                        End If
                        If CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemInventoryAsset")) > 0 Then
                            lngItemAssetAccount = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemInventoryAsset"))
                        End If
                        If CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemCoGs")) > 0 Then
                            lngItemCOGsAccount = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemCoGs"))
                        Else
                            lngItemCOGsAccount = ChartOfAccounting.GetDefaultAccount("CG", lngDomainID)
                        End If

                        objJE = New JournalEntryNew()

                        objJE.TransactionId = 0
                        objJE.DebitAmt = decDebitAmt
                        objJE.CreditAmt = decCreditAmt
                        objJE.ChartAcntId = lngItemIncomeAccount
                        objJE.Description = "Cost of Goods Sold" '"Authoritative Accounts"
                        objJE.CustomerId = lngDivisionID
                        objJE.MainDeposit = False
                        objJE.MainCheck = False
                        objJE.MainCashCredit = False
                        objJE.OppitemtCode = CLng(dvOppBiDocItems.Item(i).Item("numoppitemtCode"))
                        objJE.BizDocItems = CStr(dvOppBiDocItems.Item(i).Item("ItemType"))
                        objJE.Reference = ""
                        objJE.PaymentMethod = 0
                        objJE.Reconcile = False
                        objJE.CurrencyID = CurrencyID
                        objJE.FltExchangeRate = ExchangeRate
                        objJE.TaxItemID = 0
                        objJE.BizDocsPaymentDetailsId = 0
                        objJE.ContactID = 0
                        objJE.ItemID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemCode"))
                        objJE.ProjectID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numProjectID"))
                        objJE.ClassID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numClassID"))
                        objJE.CommissionID = 0
                        objJE.ReconcileID = 0
                        objJE.Cleared = False
                        objJE.ReferenceType = 0
                        objJE.ReferenceID = 0

                        objJEList.Add(objJE)
                    End If

                    If CStr(dvOppBiDocItems.Item(i).Item("Type")) = "P" And CDec(dvOppBiDocItems.Item(i).Item("AverageCost")) > 0 And CBool(dvOppBiDocItems.Item(i).Item("bitDropShip")) <> True Then

                        'Do not include Kit Items for Average Cost
                        If CBool(dvOppBiDocItems.Item(i).Item("bitKitParent")) = False Then
                            decACProductCost = CDec(dvOppBiDocItems.Item(i).Item("unit")) * CDec(dvOppBiDocItems.Item(i).Item("AverageCost"))
                            If decACProductCost > 0 Then
                                objJE = New JournalEntryNew()

                                objJE.TransactionId = 0
                                objJE.DebitAmt = 0
                                objJE.CreditAmt = decACProductCost '* ExchangeRate 'Removed by chintan as average cost is always going to same whenever we place order in any other currency
                                objJE.ChartAcntId = lngItemAssetAccount
                                objJE.Description = "Cost of Goods Sold" '"Authoritative Accounts"
                                objJE.CustomerId = lngDivisionID
                                objJE.MainDeposit = False
                                objJE.MainCheck = False
                                objJE.MainCashCredit = False
                                objJE.OppitemtCode = CLng(dvOppBiDocItems.Item(i).Item("numoppitemtCode"))
                                objJE.BizDocItems = "IA"
                                objJE.Reference = ""
                                objJE.PaymentMethod = 0
                                objJE.Reconcile = False
                                objJE.CurrencyID = CurrencyID
                                objJE.FltExchangeRate = ExchangeRate
                                objJE.TaxItemID = 0
                                objJE.BizDocsPaymentDetailsId = 0
                                objJE.ContactID = 0
                                objJE.ItemID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemCode"))
                                objJE.ProjectID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numProjectID"))
                                objJE.ClassID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numClassID"))
                                objJE.CommissionID = 0
                                objJE.ReconcileID = 0
                                objJE.Cleared = False
                                objJE.ReferenceType = 0
                                objJE.ReferenceID = 0

                                objJEList.Add(objJE)


                                objJE = New JournalEntryNew()

                                objJE.TransactionId = 0
                                objJE.DebitAmt = decACProductCost '* ExchangeRate 'Removed by chintan as average cost is always going to same whenever we place order in any other currency
                                objJE.CreditAmt = 0
                                objJE.ChartAcntId = lngItemCOGsAccount
                                objJE.Description = "Cost of Goods Sold" '"Authoritative Accounts"
                                objJE.CustomerId = lngDivisionID
                                objJE.MainDeposit = False
                                objJE.MainCheck = False
                                objJE.MainCashCredit = False
                                objJE.OppitemtCode = CLng(dvOppBiDocItems.Item(i).Item("numoppitemtCode"))
                                objJE.BizDocItems = "COG"
                                objJE.Reference = ""
                                objJE.PaymentMethod = 0
                                objJE.Reconcile = False
                                objJE.CurrencyID = CurrencyID
                                objJE.FltExchangeRate = ExchangeRate
                                objJE.TaxItemID = 0
                                objJE.BizDocsPaymentDetailsId = 0
                                objJE.ContactID = 0
                                objJE.ItemID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemCode"))
                                objJE.ProjectID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numProjectID"))
                                objJE.ClassID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numClassID"))
                                objJE.CommissionID = 0
                                objJE.ReconcileID = 0
                                objJE.Cleared = False
                                objJE.ReferenceType = 0
                                objJE.ReferenceID = 0

                                objJEList.Add(objJE)
                            End If
                        End If
                    End If
                Next

                'OpportunityKitItems
                Dim ds As New DataSet
                Dim objOppBizDocs As OppBizDocs = New OppBizDocs
                objOppBizDocs.OppId = lngOppID
                objOppBizDocs.OppBizDocId = lngBizDocID
                Dim dtOppKitItems As DataTable = objOppBizDocs.GetOpportunityKitItemsForAuthorizativeAccounting.Tables(0)
                If dtOppKitItems.Rows.Count > 0 Then
                    For i = 0 To dtOppKitItems.Rows.Count - 1
                        If CCommon.ToLong(dtOppKitItems.Rows(i).Item("ItemIncomeAccount")) > 0 Then
                            lngItemIncomeAccount = CCommon.ToLong(dtOppKitItems.Rows(i).Item("ItemIncomeAccount"))
                        Else
                            lngItemIncomeAccount = ChartOfAccounting.GetDefaultAccount("BE", lngDomainID)
                        End If
                        If CCommon.ToLong(dtOppKitItems.Rows(i).Item("ItemInventoryAsset")) > 0 Then
                            lngItemAssetAccount = CCommon.ToLong(dtOppKitItems.Rows(i).Item("ItemInventoryAsset"))
                        End If
                        If CCommon.ToLong(dtOppKitItems.Rows(i).Item("ItemCoGs")) > 0 Then
                            lngItemCOGsAccount = CCommon.ToLong(dtOppKitItems.Rows(i).Item("ItemCoGs"))
                        Else
                            lngItemCOGsAccount = ChartOfAccounting.GetDefaultAccount("CG", lngDomainID)
                        End If

                        If CStr(dtOppKitItems.Rows(i).Item("Type")) = "P" And CDec(dtOppKitItems.Rows(i).Item("AverageCost")) > 0 And CBool(dtOppKitItems.Rows(i).Item("bitDropShip")) <> True Then
                            decACProductCost = CDec(dtOppKitItems.Rows(i).Item("unit")) * CDec(dtOppKitItems.Rows(i).Item("AverageCost"))
                            If decACProductCost > 0 Then
                                objJE = New JournalEntryNew()

                                objJE.TransactionId = 0
                                objJE.DebitAmt = 0
                                objJE.CreditAmt = decACProductCost '* ExchangeRate 'Removed by chintan as average cost is always going to same whenever we place order in any other currency
                                objJE.ChartAcntId = lngItemAssetAccount
                                objJE.Description = "Cost of Goods Sold - Kit" '"Authoritative Accounts"
                                objJE.CustomerId = lngDivisionID
                                objJE.MainDeposit = False
                                objJE.MainCheck = False
                                objJE.MainCashCredit = False
                                objJE.OppitemtCode = CLng(dtOppKitItems.Rows(i).Item("numoppitemtCode"))
                                objJE.BizDocItems = "IA"
                                objJE.Reference = ""
                                objJE.PaymentMethod = 0
                                objJE.Reconcile = False
                                objJE.CurrencyID = CurrencyID
                                objJE.FltExchangeRate = ExchangeRate
                                objJE.TaxItemID = 0
                                objJE.BizDocsPaymentDetailsId = 0
                                objJE.ContactID = 0
                                objJE.ItemID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("ItemCode"))
                                objJE.ProjectID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("numProjectID"))
                                objJE.ClassID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("numClassID"))
                                objJE.CommissionID = 0
                                objJE.ReconcileID = 0
                                objJE.Cleared = False
                                objJE.ReferenceType = 0
                                objJE.ReferenceID = 0

                                objJEList.Add(objJE)


                                objJE = New JournalEntryNew()

                                objJE.TransactionId = 0
                                objJE.DebitAmt = decACProductCost '* ExchangeRate 'Removed by chintan as average cost is always going to same whenever we place order in any other currency
                                objJE.CreditAmt = 0
                                objJE.ChartAcntId = lngItemCOGsAccount
                                objJE.Description = "Cost of Goods Sold - Kit" '"Authoritative Accounts"
                                objJE.CustomerId = lngDivisionID
                                objJE.MainDeposit = False
                                objJE.MainCheck = False
                                objJE.MainCashCredit = False
                                objJE.OppitemtCode = CLng(dtOppKitItems.Rows(i).Item("numoppitemtCode"))
                                objJE.BizDocItems = "COG"
                                objJE.Reference = ""
                                objJE.PaymentMethod = 0
                                objJE.Reconcile = False
                                objJE.CurrencyID = CurrencyID
                                objJE.FltExchangeRate = ExchangeRate
                                objJE.TaxItemID = 0
                                objJE.BizDocsPaymentDetailsId = 0
                                objJE.ContactID = 0
                                objJE.ItemID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("ItemCode"))
                                objJE.ProjectID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("numProjectID"))
                                objJE.ClassID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("numClassID"))
                                objJE.CommissionID = 0
                                objJE.ReconcileID = 0
                                objJE.Cleared = False
                                objJE.ReferenceType = 0
                                objJE.ReferenceID = 0

                                objJEList.Add(objJE)
                            End If
                        End If
                    Next
                End If

                ''For Sales Tax Payable  as Credit in journal Entry
                '' If decTaxAmount <> 0 Then

                'objAuthoritativeBizDocs.AccountTypeId = "0102" ' 827 Sales Tax Payable
                Dim objTaxDetails As New TaxDetails
                objTaxDetails.DomainID = lngDomainID
                objTaxDetails.OppID = lngOppID
                objTaxDetails.DivisionID = lngDivisionID
                objTaxDetails.OppBizDocID = lngBizDocID
                objTaxDetails.TaxItemID = 0

                decTaxAmount = objTaxDetails.GetTaxAmtOppForTaxItem1
                If Math.Abs(decTaxAmount) > 0 Then
                    objJE = New JournalEntryNew()
                    decDebitAmt = 0
                    decCreditAmt = 0
                    If decTaxAmount > 0 Then
                        decDebitAmt = 0
                        decCreditAmt = CDec(Math.Abs(decTaxAmount) * ExchangeRate)
                    ElseIf decTaxAmount < 0 Then
                        decDebitAmt = CDec(Math.Abs(decTaxAmount) * ExchangeRate)
                        decCreditAmt = 0
                    End If


                    objJE.TransactionId = 0
                    objJE.DebitAmt = decDebitAmt
                    objJE.CreditAmt = decCreditAmt
                    objJE.ChartAcntId = ChartOfAccounting.GetDefaultAccount("ST", lngDomainID)
                    objJE.Description = "Sales" '"Authoritative Accounts"
                    objJE.CustomerId = lngDivisionID
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = "ST"
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = CurrencyID
                    objJE.FltExchangeRate = ExchangeRate
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)
                End If


                'CRV TAX
                objTaxDetails.TaxItemID = 1
                decTaxAmount = objTaxDetails.GetTaxAmtOppForTaxItem1
                If Math.Abs(decTaxAmount) > 0 Then
                    objJE = New JournalEntryNew()
                    decDebitAmt = 0
                    decCreditAmt = 0
                    If decTaxAmount > 0 Then
                        decDebitAmt = 0
                        decCreditAmt = CDec(Math.Abs(decTaxAmount) * ExchangeRate)
                    ElseIf decTaxAmount < 0 Then
                        decDebitAmt = CDec(Math.Abs(decTaxAmount) * ExchangeRate)
                        decCreditAmt = 0
                    End If


                    objJE.TransactionId = 0
                    objJE.DebitAmt = decDebitAmt
                    objJE.CreditAmt = decCreditAmt
                    objJE.ChartAcntId = ChartOfAccounting.GetDefaultAccount("ST", lngDomainID)
                    objJE.Description = "Sales" '"Authoritative Accounts"
                    objJE.CustomerId = lngDivisionID
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = "ST"
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = CurrencyID
                    objJE.FltExchangeRate = ExchangeRate
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)
                End If

                ''End If

                Dim dtSalesTax As DataTable
                Dim k As Integer
                dtSalesTax = objTaxDetails.GetTaxItems()
                For k = 0 To dtSalesTax.Rows.Count - 1
                    objTaxDetails.TaxItemID = CLng(dtSalesTax.Rows(k).Item("numTaxItemID"))
                    decTaxAmount = objTaxDetails.GetTaxAmtOppForTaxItem1
                    If Math.Abs(decTaxAmount) > 0 Then
                        objJE = New JournalEntryNew()
                        decDebitAmt = 0
                        decCreditAmt = 0
                        If decTaxAmount > 0 Then
                            decDebitAmt = 0
                            decCreditAmt = CDec(Math.Abs(decTaxAmount) * ExchangeRate)
                        ElseIf decTaxAmount < 0 Then
                            decDebitAmt = CDec(Math.Abs(decTaxAmount) * ExchangeRate)
                            decCreditAmt = 0
                        End If


                        objJE.TransactionId = 0
                        objJE.DebitAmt = decDebitAmt
                        objJE.CreditAmt = decCreditAmt
                        objJE.ChartAcntId = CLng(dtSalesTax.Rows(k).Item("numChartOfAcntID"))
                        objJE.Description = "Sales" ' "Authoritative Accounts"
                        objJE.CustomerId = lngDivisionID
                        objJE.MainDeposit = False
                        objJE.MainCheck = False
                        objJE.MainCashCredit = False
                        objJE.OppitemtCode = 0
                        objJE.BizDocItems = "OT"
                        objJE.Reference = ""
                        objJE.PaymentMethod = 0
                        objJE.Reconcile = False
                        objJE.CurrencyID = CurrencyID
                        objJE.FltExchangeRate = ExchangeRate
                        objJE.TaxItemID = CLng(dtSalesTax.Rows(k).Item("numTaxItemID"))
                        objJE.BizDocsPaymentDetailsId = 0
                        objJE.ContactID = 0
                        objJE.ItemID = 0
                        objJE.ProjectID = 0
                        objJE.ClassID = 0
                        objJE.CommissionID = 0
                        objJE.ReconcileID = 0
                        objJE.Cleared = False
                        objJE.ReferenceType = 0
                        objJE.ReferenceID = 0

                        objJEList.Add(objJE)
                    End If

                Next




                '' For Discount Given as Debit in journal Entry
                If decDiscAmt <> 0 Then
                    'discount given
                    'objAuthoritativeBizDocs.AccountTypeId = 822

                    objJE = New JournalEntryNew()

                    objJE.TransactionId = 0
                    objJE.DebitAmt = CDec(decDiscAmt * ExchangeRate)
                    objJE.CreditAmt = 0
                    objJE.ChartAcntId = CLng(IIf(DiscAcntType > 0, DiscAcntType, ChartOfAccounting.GetDefaultAccount("DG", lngDomainID))) ' 18
                    objJE.Description = "Sales" '"Authoritative Accounts"
                    objJE.CustomerId = lngDivisionID
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = "DG"
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = CurrencyID
                    objJE.FltExchangeRate = ExchangeRate
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)
                End If

                ''store Shipping Cost in Income Account  as Debit Entry -- Reffer Bug ID:222 Solution 1 for change request -by chintan
                ''Follow Journal pattern for service item Debit AR account and credit shipping income account, AR entry already includes Shipping amount.
                If decShipCost <> 0 Then
                    Dim lngShippingIncomeAccountID As Long = GetShippingIncomeAccount(lngShippingMethod, lngDomainID)
                    If lngShippingIncomeAccountID = 0 Then 'get default Shipping account from AccountCharges table
                        lngShippingIncomeAccountID = ChartOfAccounting.GetDefaultAccount("SI", lngDomainID) 'Shippig income
                    End If

                    objJE = New JournalEntryNew()

                    objJE.TransactionId = 0
                    objJE.DebitAmt = 0
                    objJE.CreditAmt = CDec(decShipCost * ExchangeRate)
                    objJE.ChartAcntId = lngShippingIncomeAccountID
                    objJE.Description = "Sales" ' "Authoritative Accounts"
                    objJE.CustomerId = lngDivisionID
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = "SI" ''Shipping income
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = CurrencyID
                    objJE.FltExchangeRate = ExchangeRate
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)

                    ''''' Adding bill for shipping expense will be made by user manually when shipping company sends them bill
                    ''Add shipping amount as bill from money out section
                    'Dim objOppInvoice As New Opportunities.OppInvoice
                    'objOppInvoice.AmtPaid = decShipCost * CCommon.ToDecimal(ExchangeRate)
                    'objOppInvoice.UserCntID = CCommon.ToLong(System.Web.HttpContext.Current.Session("UserContactID"))
                    'objOppInvoice.PaymentMethod = 4
                    'objOppInvoice.Reference = "Pay Shipping Amount to Vendor"
                    'objOppInvoice.Memo = ""
                    'objOppInvoice.DomainId = CCommon.ToInteger(System.Web.HttpContext.Current.Session("DomainId"))
                    'objOppInvoice.ExpenseAcount = lngShippingAccountID
                    'objOppInvoice.DivisionID = lngDivisionID
                    'objOppInvoice.DueDate = Now.UtcNow
                    'objOppInvoice.CurrencyID = CCommon.ToLong(System.Web.HttpContext.Current.Session("BaseCurrencyID"))
                    'objOppInvoice.AddBillDetails()
                End If

                ''For Late Charges  as Credit in journal Entry
                If decLateCharge <> 0 Then

                    'objAuthoritativeBizDocs.AccountTypeId = 822

                    objJE = New JournalEntryNew()

                    objJE.TransactionId = 0
                    objJE.DebitAmt = 0
                    objJE.CreditAmt = CDec(decLateCharge * ExchangeRate)
                    objJE.ChartAcntId = ChartOfAccounting.GetDefaultAccount("LC", lngDomainID) ' 21
                    objJE.Description = "Sales" ' "Authoritative Accounts"
                    objJE.CustomerId = lngDivisionID
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = "LC" ''Late Charges
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = CurrencyID
                    objJE.FltExchangeRate = ExchangeRate
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)
                End If

                'Add Embedded Cost entries 

                'Impact to accounting:
                'Credit : A/P (And save the Division ID, so that we can show the vendor name in GL and save the expense name in the description of journal details table) OR the Expense A/C Selected - 100

                Dim objOpp As New MOpportunity
                Dim lngVendorAPAccountID, lngVendorDivID As Long
                objOpp.DomainID = lngDomainID
                objOpp.CostCategoryID = 0
                objOpp.OppBizDocID = lngBizDocID
                Dim dtEmbeddedCost As DataTable = objOpp.GetEmbeddedCost()
                Dim decEmbeddedCost As Decimal = 0
                objOppBizDocs = New Opportunities.OppBizDocs

                If dtEmbeddedCost.Rows.Count > 0 Then
                    For Each row As DataRow In dtEmbeddedCost.Rows

                        'Add Journal on actual cost
                        If CCommon.ToDecimal(row("monActualCost")) > 0 Then
                            If CCommon.ToLong(row("numDivisionID")) > 0 Then
                                lngVendorAPAccountID = objOppBizDocs.ValidateCustomerAR_APAccounts("AP", lngDomainID, CCommon.ToLong(row("numDivisionID")))
                                lngVendorDivID = CCommon.ToLong(row("numDivisionID"))
                            Else
                                lngVendorAPAccountID = CCommon.ToLong(row("numAccountID"))
                                lngVendorDivID = lngDivisionID
                            End If

                            objJE = New JournalEntryNew()

                            objJE.TransactionId = 0
                            objJE.DebitAmt = 0
                            objJE.CreditAmt = CDec(CCommon.ToDecimal(row("monActualCost")) * ExchangeRate)
                            objJE.ChartAcntId = lngVendorAPAccountID
                            objJE.Description = "Embedded Cost" & " (" & row("vcAccountName").ToString & ")"
                            objJE.CustomerId = lngVendorDivID
                            objJE.MainDeposit = False
                            objJE.MainCheck = False
                            objJE.MainCashCredit = False
                            objJE.OppitemtCode = 0
                            objJE.BizDocItems = "EC" ''Embedded Cost
                            objJE.Reference = ""
                            objJE.PaymentMethod = 0
                            objJE.Reconcile = False
                            objJE.CurrencyID = CurrencyID
                            objJE.FltExchangeRate = ExchangeRate
                            objJE.TaxItemID = 0
                            objJE.BizDocsPaymentDetailsId = 0
                            objJE.ContactID = 0
                            objJE.ItemID = 0
                            objJE.ProjectID = 0
                            objJE.ClassID = 0
                            objJE.CommissionID = 0
                            objJE.ReconcileID = 0
                            objJE.Cleared = False
                            objJE.ReferenceType = 0
                            objJE.ReferenceID = 0

                            objJEList.Add(objJE)
                        End If

                        'Add Bill first time, rest time it will be update amount to bill directly
                        If CCommon.ToLong(row("numBizDocsPaymentDetId")) = 0 And CCommon.ToDecimal(row("monActualCost")) > 0 Then
                            Dim objOppInvoice As New Opportunities.OppInvoice
                            objOppInvoice.EmbeddedCostID = CCommon.ToLong(row("numEmbeddedCostID"))
                            objOppInvoice.AmtPaid = CCommon.ToDecimal(row("monActualCost")) * CCommon.ToDecimal(ExchangeRate)
                            objOppInvoice.UserCntID = userContactID
                            objOppInvoice.PaymentMethod = CCommon.ToInteger(row("numPaymentMethod"))
                            objOppInvoice.Reference = "Pay Embedded Cost to Vendor"
                            objOppInvoice.Memo = CCommon.ToString(row("vcMemo"))
                            objOppInvoice.DomainID = CCommon.ToInteger(lngDomainID)
                            objOppInvoice.ExpenseAcount = CCommon.ToLong(row("numAccountID"))
                            objOppInvoice.DivisionID = CCommon.ToLong(row("numDivisionID"))
                            objOppInvoice.DueDate = CDate(row("dtDueDate1"))
                            objOppInvoice.CurrencyID = baseCurrencyID
                            objOppInvoice.PaymentType = OppInvoice.BillPaymentType.Bill
                            objOppInvoice.AddBillDetails()
                        End If
                    Next
                    decEmbeddedCost = CCommon.ToDecimal(dtEmbeddedCost.Compute("sum(monActualCost)", ""))
                End If

                'For Account Receivable as Debit Entry in Journal
                'GetDefaultAccountID("AR", lngDomainID)
                'objAuthoritativeBizDocs.DomainId = lngDomainID
                'objAuthoritativeBizDocs.ChargeCode = "AR"
                decAmount = CDec(TotalAmount * ExchangeRate)
                decDebitAmt = 0
                decCreditAmt = 0
                If decAmount > 0.0 Then
                    decDebitAmt = Math.Abs(decAmount) + decEmbeddedCost
                    decCreditAmt = 0
                ElseIf decAmount < 0.0 Then
                    decDebitAmt = 0
                    decCreditAmt = Math.Abs(decAmount) + decEmbeddedCost
                End If

                Dim lngARAccountId As Long

                lngARAccountId = objOppBizDocs.ValidateCustomerAR_APAccounts("AR", lngDomainID, lngDivisionID)

                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = decDebitAmt
                objJE.CreditAmt = decCreditAmt
                objJE.ChartAcntId = lngARAccountId 'objAuthoritativeBizDocs.GetChartAcountIdForAuthorizativeBizDocs '2
                objJE.Description = "Sales" ' "Authoritative Accounts"
                objJE.CustomerId = lngDivisionID
                objJE.MainDeposit = False
                objJE.MainCheck = False
                objJE.MainCashCredit = False
                objJE.OppitemtCode = 0
                objJE.BizDocItems = "AR"
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = CurrencyID
                objJE.FltExchangeRate = ExchangeRate
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = False
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)

                'Add Commission Journal Entry
                'Default Payroll Liability Account is Credited
                'Default Commission Exp Account is Debited 
                Dim EmployeeExpensesAccount As Long
                Dim ContractEmployeeExpenses As Long
                Dim APAccount As Long
                Dim objReceivePayment As New ReceivePayment
                Dim dtComission As New DataTable
                Dim objPayRoll As PayrollExpenses
                'Dim lngComJnID As Long
                'Dim lngComTotal As Double
                'Dim intRow As Integer

                objPayRoll = New PayrollExpenses
                objPayRoll.DomainID = lngDomainID
                'Below line is commented because now commission is not calculated when bizdoc is added but its calculated from Accounting -> Payroll Expense
                'dtComission = objPayRoll.GetComissionDetails(lngBizDocID, lngOppID)

                ''Don't make Commission Journal entry when Auth BizDoc Created(Only calculate commission and insert into BizDocCommission). We already create journal entry from Payroll when user approve payroll.

                'If dtComission.Rows.Count > 0 Then
                '    objAuthoritativeBizDocs.DomainID = lngDomainID

                '    'Employee Payroll Expense
                '    EmployeeExpensesAccount = ChartOfAccounting.GetDefaultAccount("EP", lngDomainID)

                '    'Contract Employee Payroll Expense
                '    ContractEmployeeExpenses = ChartOfAccounting.GetDefaultAccount("CP", lngDomainID)

                '    'Payroll Liability 
                '    APAccount = ChartOfAccounting.GetDefaultAccount("PL", lngDomainID)
                '    Dim comRow As DataRow
                '    'Dim lngTotalCom As Double

                '    'lngTotalCom = 0

                '    Dim lngDomainDivID As Long = 0

                '    Dim objCommon As New CCommon
                '    objCommon.Mode = 18
                '    objCommon.DomainID = lngDomainID
                '    lngDomainDivID = CCommon.ToLong(objCommon.GetSingleFieldValue())

                '    For Each comRow In dtComission.Rows
                '        objJE = New JournalEntryNew()

                '        objJE.TransactionId = 0
                '        objJE.DebitAmt = CDec(CCommon.ToDecimal(comRow.Item("numComission")) * ExchangeRate)
                '        objJE.CreditAmt = 0

                '        If CCommon.ToBool(comRow.Item("bitEmpContract")) = True Then
                '            objJE.ChartAcntId = ContractEmployeeExpenses
                '            objJE.BizDocItems = "CP"
                '        Else
                '            objJE.ChartAcntId = EmployeeExpensesAccount
                '            objJE.BizDocItems = "EP"
                '        End If

                '        objJE.Description = "Sales" ' "Commission Expenses (" & CCommon.ToString(comRow.Item("vcItemName")) & ") - " & CCommon.ToString(comRow.Item("vcContactName"))
                '        objJE.CustomerId = lngDomainDivID
                '        objJE.MainDeposit = False
                '        objJE.MainCheck = False
                '        objJE.MainCashCredit = False
                '        objJE.OppitemtCode = CLng(comRow.Item("numoppitemtCode"))
                '        objJE.Reference = ""
                '        objJE.PaymentMethod = 0
                '        objJE.Reconcile = False
                '        objJE.CurrencyID = CurrencyID
                '        objJE.FltExchangeRate = ExchangeRate
                '        objJE.TaxItemID = 0
                '        objJE.BizDocsPaymentDetailsId = 0
                '        objJE.ContactID = CLng(comRow.Item("numcontactid"))
                '        objJE.ItemID = 0
                '        objJE.ProjectID = 0
                '        objJE.ClassID = 0
                '        objJE.CommissionID = CLng(comRow.Item("numComissionID"))
                '        objJE.ReconcileID = 0
                '        objJE.Cleared = False
                '        objJE.ReferenceType = 0
                '        objJE.ReferenceID = 0

                '        objJEList.Add(objJE)

                '        'Debit : Commission Expense
                '        objJE = New JournalEntryNew()

                '        objJE.TransactionId = 0
                '        objJE.DebitAmt = 0
                '        objJE.CreditAmt = CDec(CCommon.ToDecimal(comRow.Item("numComission")) * ExchangeRate)
                '        objJE.ChartAcntId = APAccount
                '        objJE.Description = "Sales" ' "Commission Expenses (" & CCommon.ToString(comRow.Item("vcItemName")) & ") - " & CCommon.ToString(comRow.Item("vcContactName"))
                '        objJE.CustomerId = lngDomainDivID
                '        objJE.MainDeposit = False
                '        objJE.MainCheck = False
                '        objJE.MainCashCredit = False
                '        objJE.OppitemtCode = CLng(comRow.Item("numoppitemtCode"))
                '        objJE.BizDocItems = "PL"
                '        objJE.Reference = ""
                '        objJE.PaymentMethod = 0
                '        objJE.Reconcile = False
                '        objJE.CurrencyID = CurrencyID
                '        objJE.FltExchangeRate = ExchangeRate
                '        objJE.TaxItemID = 0
                '        objJE.BizDocsPaymentDetailsId = 0
                '        objJE.ContactID = CLng(comRow.Item("numcontactid"))
                '        objJE.ItemID = 0
                '        objJE.ProjectID = 0
                '        objJE.ClassID = 0
                '        objJE.CommissionID = CLng(comRow.Item("numComissionID"))
                '        objJE.ReconcileID = 0
                '        objJE.Cleared = False
                '        objJE.ReferenceType = 0
                '        objJE.ReferenceID = 0

                '        objJEList.Add(objJE)
                '    Next
                'End If

                '--------Commission Expense Ends----------------------

                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalID, lngDomainID)

                'ds.Tables.Add(dt)
                'ValidateJournalEntries(ds, lngJournalID, lngDomainID) 'Added to detect imbalance in journals before posting it to accounting system
                'lstr = ds.GetXml()
                'objAuthoritativeBizDocs.JournalId = lngJournalID
                'objAuthoritativeBizDocs.Mode = 0
                'objAuthoritativeBizDocs.RecurringMode = 0
                'objAuthoritativeBizDocs.JournalDetails = lstr
                'objAuthoritativeBizDocs.DomainID = lngDomainID
                'objAuthoritativeBizDocs.OppBIzDocID = lngBizDocID
                'objAuthoritativeBizDocs.OppId = lngOppID
                'objAuthoritativeBizDocs.SaveDataToJournalDetailsForAuthoritativeBizDocs()

                'Distibute Total cost among bizdoc items and remove old cost when no cost specified
                objOpp.OppBizDocID = lngBizDocID
                objOpp.Mode = 1
                objOpp.ManageEmbeddedCost()
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveJournalEntriesPurchase(ByVal lngOppID As Long, ByVal lngDomainID As Long, ByVal dtOppBiDocItems As DataTable, ByVal lngJournalId As Long, ByVal lngDivisionID As Long, ByVal lngBizDocID As Long, ByVal TotalAmount As Decimal, ByVal decDiscAmt As Decimal, ByVal decShipCost As Decimal, ByVal decLateCharge As Decimal, ByVal CurrencyID As Long, ByVal ExchangeRate As Double, Optional ByVal DiscAcntType As Long = 0, Optional ByVal lngShippingMethod As Long = 0) As Boolean
            Try
                If ExchangeRate = 0 Then
                    Throw New Exception("EXCHANGE_RATE_CAN_NOT_BE_0")
                    Exit Function
                End If

                Dim i As Integer
                Dim ds As New DataSet
                Dim objAuthoritativeBizDocs As New AuthoritativeBizDocs
                'ExchangeRate = 1 / ExchangeRate
                Dim objJEList As New JournalEntryCollection

                Dim objJE As New JournalEntryNew()

                Dim dtrow As DataRow
                Dim dvOppBiDocItems As DataView

                dvOppBiDocItems = New DataView(dtOppBiDocItems)
                '' dvOppBiDocItems.RowFilter = " ItemCode <> 0 "  -- Temporarily Commented

                'objAuthoritativeBizDocs.AccountCode = "0104" '824 ',Billable Time & Expenses
                Dim decAmount, decDebitAmt, decCreditAmt As Decimal

                For i = 0 To dvOppBiDocItems.Count - 1
                    decAmount = CDec(CCommon.ToDecimal(dvOppBiDocItems.Item(i).Item("Amount")) * ExchangeRate)

                    decDebitAmt = 0
                    decCreditAmt = 0

                    If decAmount > 0.0 Then
                        decDebitAmt = Math.Abs(decAmount)
                        decCreditAmt = 0
                    ElseIf decAmount < 0.0 Then
                        decDebitAmt = 0
                        decCreditAmt = Math.Abs(decAmount)
                    End If

                    objJE = New JournalEntryNew()

                    objJE.TransactionId = 0
                    objJE.DebitAmt = decDebitAmt
                    objJE.CreditAmt = decCreditAmt

                    If CStr(dvOppBiDocItems.Item(i).Item("Type")) = "P" And CBool(dvOppBiDocItems.Item(i).Item("bitDropShip")) <> True Then
                        objJE.ChartAcntId = CLng(IIf(CInt(dvOppBiDocItems.Item(i).Item("ItemInventoryAsset")) = 0, ChartOfAccounting.GetDefaultAccount("BE", lngDomainID), dvOppBiDocItems.Item(i).Item("ItemInventoryAsset"))) '' 19 -- For Billable Time & Expenses in COA
                    ElseIf CStr(dvOppBiDocItems.Item(i).Item("Type")) = "P" And CBool(dvOppBiDocItems.Item(i).Item("bitDropShip")) = True Then
                        objJE.ChartAcntId = CLng(IIf(CInt(dvOppBiDocItems.Item(i).Item("ItemCoGs")) = 0, ChartOfAccounting.GetDefaultAccount("BE", lngDomainID), dvOppBiDocItems.Item(i).Item("ItemCoGs"))) '' 19 --COGS
                    Else
                        If CCommon.ToBool(HttpContext.Current.Session("EnableNonInventoryItemExpense")) AndAlso CCommon.ToLong(dvOppBiDocItems.Item(i).Item("itemCoGs")) > 0 Then
                            objJE.ChartAcntId = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("itemCoGs"))
                        Else
                            objJE.ChartAcntId = ChartOfAccounting.GetDefaultAccount("CG", lngDomainID)
                        End If
                    End If

                    objJE.Description = "Purchase" '"Authoritative Accounts"
                    objJE.CustomerId = lngDivisionID
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = CLng(dvOppBiDocItems.Item(i).Item("numoppitemtCode"))
                    objJE.BizDocItems = CStr(dvOppBiDocItems.Item(i).Item("ItemType"))
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = CurrencyID
                    objJE.FltExchangeRate = ExchangeRate
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemCode"))
                    objJE.ProjectID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numProjectID"))
                    objJE.ClassID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numClassID"))
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)
                Next


                'For Account Payable as Credit Entry in Journal
                decAmount = CDec(TotalAmount * ExchangeRate)

                decDebitAmt = 0
                decCreditAmt = 0

                If decAmount > 0.0 Then
                    decDebitAmt = 0
                    decCreditAmt = Math.Abs(decAmount)
                ElseIf decAmount < 0.0 Then
                    decDebitAmt = Math.Abs(decAmount)
                    decCreditAmt = 0
                End If

                Dim objOppBizDocs As New Opportunities.OppBizDocs
                Dim lngAPAccountId As Long = objOppBizDocs.ValidateCustomerAR_APAccounts("AP", lngDomainID, lngDivisionID)

                'objAuthoritativeBizDocs.DomainId = lngDomainID
                'objAuthoritativeBizDocs.ChargeCode = "AP"
                ''objAuthoritativeBizDocs.AccountTypeId = 815
                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = decDebitAmt
                objJE.CreditAmt = decCreditAmt
                objJE.ChartAcntId = lngAPAccountId 'objAuthoritativeBizDocs.GetChartAcountIdForAuthorizativeBizDocs '3
                objJE.Description = "Purchase" ' "Authoritative Accounts"
                objJE.CustomerId = lngDivisionID
                objJE.MainDeposit = False
                objJE.MainCheck = False
                objJE.MainCashCredit = False
                objJE.OppitemtCode = 0
                objJE.BizDocItems = "AP" ''Account Payable
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = CurrencyID
                objJE.FltExchangeRate = ExchangeRate
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = False
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)


                '' For Discount Given as Debit in journal Entry
                If decDiscAmt <> 0 Then
                    'objAuthoritativeBizDocs.AccountTypeId = 822
                    objJE = New JournalEntryNew()

                    objJE.TransactionId = 0
                    objJE.DebitAmt = 0
                    objJE.CreditAmt = CDec(decDiscAmt * ExchangeRate)
                    objJE.ChartAcntId = CLng(IIf(DiscAcntType > 0, DiscAcntType, ChartOfAccounting.GetDefaultAccount("DG", lngDomainID)))
                    objJE.Description = "Purchase" ' "Authoritative Accounts"
                    objJE.CustomerId = lngDivisionID
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = "DG" ''Discount Given
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = CurrencyID
                    objJE.FltExchangeRate = ExchangeRate
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)
                End If

                ''For Shipping Income  as Credit in journal Entry
                If decShipCost <> 0 Then
                    objAuthoritativeBizDocs.DomainID = lngDomainID
                    objAuthoritativeBizDocs.ChargeCode = "SI"

                    Dim lngShippingIncomeAccountID As Long = GetShippingIncomeAccount(lngShippingMethod, lngDomainID)
                    If lngShippingIncomeAccountID = 0 Then 'get default Shipping account from AccountCharges table
                        lngShippingIncomeAccountID = ChartOfAccounting.GetDefaultAccount("SI", lngDomainID) 'Shippig income
                    End If

                    objJE = New JournalEntryNew()

                    objJE.TransactionId = 0
                    objJE.DebitAmt = CDec(decShipCost * ExchangeRate)
                    objJE.CreditAmt = 0
                    objJE.ChartAcntId = lngShippingIncomeAccountID '20
                    objJE.Description = "Purchase" ' "Authoritative Accounts"
                    objJE.CustomerId = lngDivisionID
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = "SI" ''Shipping Income
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = CurrencyID
                    objJE.FltExchangeRate = ExchangeRate
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)
                End If

                ''For Late Charges  as Credit in journal Entry
                If decLateCharge <> 0 Then
                    'objAuthoritativeBizDocs.AccountTypeId = 822

                    objJE = New JournalEntryNew()

                    objJE.TransactionId = 0
                    objJE.DebitAmt = CDec(decLateCharge * ExchangeRate)
                    objJE.CreditAmt = 0
                    objJE.ChartAcntId = ChartOfAccounting.GetDefaultAccount("LC", lngDomainID) '21
                    objJE.Description = "Purchase" ' "Authoritative Accounts"
                    objJE.CustomerId = lngDivisionID
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = "LC" ''Late Charges
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = CurrencyID
                    objJE.FltExchangeRate = ExchangeRate
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)
                End If



                Dim dtTable As DataTable
                Dim objUserAccess As New UserAccess
                objUserAccess.DomainID = lngDomainID
                dtTable = objUserAccess.GetDomainDetails()
                Dim decTaxAmount As Decimal = 0

                If dtTable.Rows.Count > 0 AndAlso CCommon.ToBool(dtTable.Rows(0).Item("bitPurchaseTaxCredit")) Then
                    Dim lngPurchaseTaxCredit As Long = ChartOfAccounting.GetDefaultAccount("PT", lngDomainID)

                    If lngPurchaseTaxCredit > 0 Then
                        Dim objTaxDetails As New TaxDetails
                        objTaxDetails.DomainID = lngDomainID
                        objTaxDetails.OppID = lngOppID
                        objTaxDetails.DivisionID = lngDivisionID
                        objTaxDetails.OppBizDocID = lngBizDocID
                        objTaxDetails.TaxItemID = 0

                        decTaxAmount = objTaxDetails.GetTaxAmtOppForTaxItem1
                        If Math.Abs(decTaxAmount) > 0 Then
                            objJE = New JournalEntryNew()
                            decDebitAmt = 0
                            decCreditAmt = 0
                            If decTaxAmount > 0 Then
                                decDebitAmt = 0
                                decCreditAmt = CDec(Math.Abs(decTaxAmount) * ExchangeRate)
                            ElseIf decTaxAmount < 0 Then
                                decDebitAmt = CDec(Math.Abs(decTaxAmount) * ExchangeRate)
                                decCreditAmt = 0
                            End If

                            objJE.TransactionId = 0
                            objJE.DebitAmt = decCreditAmt
                            objJE.CreditAmt = decDebitAmt
                            objJE.ChartAcntId = lngPurchaseTaxCredit  'ChartOfAccounting.GetDefaultAccount("PT", lngDomainID)
                            objJE.Description = "Purchase" '"Authoritative Accounts"
                            objJE.CustomerId = lngDivisionID
                            objJE.MainDeposit = False
                            objJE.MainCheck = False
                            objJE.MainCashCredit = False
                            objJE.OppitemtCode = 0
                            objJE.BizDocItems = "PT"
                            objJE.Reference = ""
                            objJE.PaymentMethod = 0
                            objJE.Reconcile = False
                            objJE.CurrencyID = CurrencyID
                            objJE.FltExchangeRate = ExchangeRate
                            objJE.TaxItemID = 0
                            objJE.BizDocsPaymentDetailsId = 0
                            objJE.ContactID = 0
                            objJE.ItemID = 0
                            objJE.ProjectID = 0
                            objJE.ClassID = 0
                            objJE.CommissionID = 0
                            objJE.ReconcileID = 0
                            objJE.Cleared = False
                            objJE.ReferenceType = 0
                            objJE.ReferenceID = 0

                            objJEList.Add(objJE)
                        End If

                        'CRV TAX
                        objTaxDetails.TaxItemID = 1
                        decTaxAmount = objTaxDetails.GetTaxAmtOppForTaxItem1
                        If Math.Abs(decTaxAmount) > 0 Then
                            objJE = New JournalEntryNew()
                            decDebitAmt = 0
                            decCreditAmt = 0
                            If decTaxAmount > 0 Then
                                decDebitAmt = 0
                                decCreditAmt = CDec(Math.Abs(decTaxAmount) * ExchangeRate)
                            ElseIf decTaxAmount < 0 Then
                                decDebitAmt = CDec(Math.Abs(decTaxAmount) * ExchangeRate)
                                decCreditAmt = 0
                            End If

                            objJE.TransactionId = 0
                            objJE.DebitAmt = decCreditAmt
                            objJE.CreditAmt = decDebitAmt
                            objJE.ChartAcntId = lngPurchaseTaxCredit  'ChartOfAccounting.GetDefaultAccount("PT", lngDomainID)
                            objJE.Description = "Purchase" '"Authoritative Accounts"
                            objJE.CustomerId = lngDivisionID
                            objJE.MainDeposit = False
                            objJE.MainCheck = False
                            objJE.MainCashCredit = False
                            objJE.OppitemtCode = 0
                            objJE.BizDocItems = "PT"
                            objJE.Reference = ""
                            objJE.PaymentMethod = 0
                            objJE.Reconcile = False
                            objJE.CurrencyID = CurrencyID
                            objJE.FltExchangeRate = ExchangeRate
                            objJE.TaxItemID = 0
                            objJE.BizDocsPaymentDetailsId = 0
                            objJE.ContactID = 0
                            objJE.ItemID = 0
                            objJE.ProjectID = 0
                            objJE.ClassID = 0
                            objJE.CommissionID = 0
                            objJE.ReconcileID = 0
                            objJE.Cleared = False
                            objJE.ReferenceType = 0
                            objJE.ReferenceID = 0

                            objJEList.Add(objJE)
                        End If

                        ''End If

                        Dim dtPurchaseTax As DataTable
                        Dim k As Integer
                        dtPurchaseTax = objTaxDetails.GetTaxItems()
                        For k = 0 To dtPurchaseTax.Rows.Count - 1
                            objTaxDetails.TaxItemID = CLng(dtPurchaseTax.Rows(k).Item("numTaxItemID"))
                            decTaxAmount = objTaxDetails.GetTaxAmtOppForTaxItem1
                            If Math.Abs(decTaxAmount) > 0 Then
                                objJE = New JournalEntryNew()
                                decDebitAmt = 0
                                decCreditAmt = 0
                                If decTaxAmount > 0 Then
                                    decDebitAmt = 0
                                    decCreditAmt = CDec(Math.Abs(decTaxAmount) * ExchangeRate)
                                ElseIf decTaxAmount < 0 Then
                                    decDebitAmt = CDec(Math.Abs(decTaxAmount) * ExchangeRate)
                                    decCreditAmt = 0
                                End If

                                objJE.TransactionId = 0
                                objJE.DebitAmt = decCreditAmt
                                objJE.CreditAmt = decDebitAmt
                                objJE.ChartAcntId = CLng(dtPurchaseTax.Rows(k).Item("numChartOfAcntID"))
                                objJE.Description = "Purchase" ' "Authoritative Accounts"
                                objJE.CustomerId = lngDivisionID
                                objJE.MainDeposit = False
                                objJE.MainCheck = False
                                objJE.MainCashCredit = False
                                objJE.OppitemtCode = 0
                                objJE.BizDocItems = "OT"
                                objJE.Reference = ""
                                objJE.PaymentMethod = 0
                                objJE.Reconcile = False
                                objJE.CurrencyID = CurrencyID
                                objJE.FltExchangeRate = ExchangeRate
                                objJE.TaxItemID = CLng(dtPurchaseTax.Rows(k).Item("numTaxItemID"))
                                objJE.BizDocsPaymentDetailsId = 0
                                objJE.ContactID = 0
                                objJE.ItemID = 0
                                objJE.ProjectID = 0
                                objJE.ClassID = 0
                                objJE.CommissionID = 0
                                objJE.ReconcileID = 0
                                objJE.Cleared = False
                                objJE.ReferenceType = 0
                                objJE.ReferenceID = 0

                                objJEList.Add(objJE)
                            End If
                        Next
                    End If

                End If


                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, lngDomainID)
                'ds.Tables.Add(dt)
                'ValidateJournalEntries(ds, lngJournalId, lngDomainID)  'Added to detect imbalance in journals before posting it to accounting system
                'lstr = ds.GetXml()
                'objAuthoritativeBizDocs.JournalId = lngJournalId
                'objAuthoritativeBizDocs.Mode = 0

                'objAuthoritativeBizDocs.RecurringMode = 0
                'objAuthoritativeBizDocs.JournalDetails = lstr
                'objAuthoritativeBizDocs.DomainID = lngDomainID
                'objAuthoritativeBizDocs.OppBIzDocID = lngBizDocID
                'objAuthoritativeBizDocs.OppId = lngOppID
                'objAuthoritativeBizDocs.SaveDataToJournalDetailsForAuthoritativeBizDocs()
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetShippingIncomeAccount(ByVal lngShippingMethod As Long, ByVal lngDomainID As Long) As Long
            Try
                Dim lngShippingIncomeAccountID As Long
                If lngShippingMethod > 0 Then
                    Dim objCOA As New ChartOfAccounting
                    Dim ds1 As DataSet
                    With objCOA
                        .ShippingMappingID = 0
                        .ShippingMethodID = lngShippingMethod
                        .DomainID = lngDomainID
                        ds1 = .GetCOAShippingMapping()
                    End With
                    If ds1.Tables(0).Rows.Count > 0 Then
                        lngShippingIncomeAccountID = CCommon.ToLong(ds1.Tables(0).Rows(0).Item("numIncomeAccountID"))
                    End If
                End If

                Return lngShippingIncomeAccountID
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetMaxJournalReferenceNo() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "usp_GetMaxJournalReferenceNo", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveJournalEntriesPurchaseClearing(ByVal lngOppID As Long, ByVal lngDomainID As Long, ByVal dtOppItems As DataTable, ByVal lngJournalId As Long) As Boolean
            Try
                Dim i As Integer
                Dim ds As New DataSet
                Dim objAuthoritativeBizDocs As New AuthoritativeBizDocs
                'ExchangeRate = 1 / ExchangeRate
                Dim objJEList As New JournalEntryCollection

                Dim objJE As New JournalEntryNew()

                Dim dvOppBiDocItems As DataView

                dvOppBiDocItems = New DataView(dtOppItems)

                Dim decAmount, decDebitAmt, decCreditAmt As Decimal

                For i = 0 To dvOppBiDocItems.Count - 1
                    If CDec(dvOppBiDocItems.Item(i).Item("fltExchangeRate")) = 0 Then
                        Throw New Exception("EXCHANGE_RATE_CAN_NOT_BE_0")
                        Exit Function
                    End If

                    decAmount = CDec(CDec(dvOppBiDocItems.Item(i).Item("monPrice")) * CDec(dvOppBiDocItems.Item(i).Item("numUnitReceived")) * CDec(dvOppBiDocItems.Item(i).Item("fltExchangeRate")))
                    decDebitAmt = 0
                    decCreditAmt = 0
                    If decAmount > 0.0 Then
                        decDebitAmt = Math.Abs(decAmount)
                        decCreditAmt = 0
                    ElseIf decAmount < 0.0 Then
                        decDebitAmt = 0
                        decCreditAmt = Math.Abs(decAmount)
                    End If

                    objJE = New JournalEntryNew()

                    objJE.TransactionId = 0
                    objJE.DebitAmt = decDebitAmt
                    objJE.CreditAmt = decCreditAmt

                    If CStr(dvOppBiDocItems.Item(i).Item("charItemType")) = "P" And CBool(dvOppBiDocItems.Item(i).Item("DropShip")) <> True Then
                        objJE.ChartAcntId = CLng(IIf(CInt(dvOppBiDocItems.Item(i).Item("ItemInventoryAsset")) = 0, ChartOfAccounting.GetDefaultAccount("BE", lngDomainID), dvOppBiDocItems.Item(i).Item("ItemInventoryAsset"))) '' 19 -- For Billable Time & Expenses in COA
                    ElseIf CStr(dvOppBiDocItems.Item(i).Item("charItemType")) = "P" And CBool(dvOppBiDocItems.Item(i).Item("DropShip")) = True Then
                        objJE.ChartAcntId = CLng(IIf(CInt(dvOppBiDocItems.Item(i).Item("ItemCoGs")) = 0, ChartOfAccounting.GetDefaultAccount("BE", lngDomainID), dvOppBiDocItems.Item(i).Item("ItemCoGs"))) '' 19 --COGS
                    Else
                        If CCommon.ToBool(HttpContext.Current.Session("EnableNonInventoryItemExpense")) AndAlso CCommon.ToLong(dvOppBiDocItems.Item(i).Item("itemCoGs")) > 0 Then
                            objJE.ChartAcntId = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("itemCoGs"))
                        Else
                            objJE.ChartAcntId = ChartOfAccounting.GetDefaultAccount("CG", lngDomainID)
                        End If
                    End If

                    objJE.Description = String.Format("{0:N2} Units : {1}", CDec(dvOppBiDocItems.Item(i).Item("numUnitReceived")), CCommon.Truncate(dvOppBiDocItems.Item(i).Item("vcItemName").ToString(), 30))  '"Authoritative Accounts"
                    objJE.CustomerId = CLng(dvOppBiDocItems.Item(i).Item("numDivisionID"))
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = CLng(dvOppBiDocItems.Item(i).Item("numoppitemtCode"))
                    objJE.BizDocItems = CStr(dvOppBiDocItems.Item(i).Item("ItemType"))
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = CLng(dvOppBiDocItems.Item(i).Item("numCurrencyID"))
                    objJE.FltExchangeRate = CDec(dvOppBiDocItems.Item(i).Item("fltExchangeRate"))
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numItemCode"))
                    objJE.ProjectID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numProjectID"))
                    objJE.ClassID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numClassID"))
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)


                    objJE = New JournalEntryNew()
                    objJE.TransactionId = 0
                    objJE.DebitAmt = decCreditAmt
                    objJE.CreditAmt = decDebitAmt
                    objJE.ChartAcntId = CLng(ChartOfAccounting.GetDefaultAccount("PC", lngDomainID))
                    objJE.Description = String.Format("{0:N2} Units : {1}", CDec(dvOppBiDocItems.Item(i).Item("numUnitReceived")), CCommon.Truncate(dvOppBiDocItems.Item(i).Item("vcItemName").ToString(), 30))    '"Authoritative Accounts"
                    objJE.CustomerId = CLng(dvOppBiDocItems.Item(i).Item("numDivisionID"))
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = CLng(dvOppBiDocItems.Item(i).Item("numoppitemtCode"))
                    objJE.BizDocItems = "PC"
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = CLng(dvOppBiDocItems.Item(i).Item("numCurrencyID"))
                    objJE.FltExchangeRate = CDec(dvOppBiDocItems.Item(i).Item("fltExchangeRate"))
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numItemCode"))
                    objJE.ProjectID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numProjectID"))
                    objJE.ClassID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numClassID"))
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)
                Next

                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, lngDomainID)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveJournalEntriesRevertPurchaseClearing(ByVal lngOppID As Long, ByVal lngDomainID As Long, ByVal dtOppItems As DataTable, ByVal lngJournalId As Long) As Boolean
            Try
                Dim i As Integer
                Dim ds As New DataSet
                Dim objAuthoritativeBizDocs As New AuthoritativeBizDocs
                'ExchangeRate = 1 / ExchangeRate
                Dim objJEList As New JournalEntryCollection

                Dim objJE As New JournalEntryNew()

                Dim dvOppBiDocItems As DataView

                dvOppBiDocItems = New DataView(dtOppItems)

                Dim decAmount, decDebitAmt, decCreditAmt As Decimal

                For i = 0 To dvOppBiDocItems.Count - 1
                    If CDec(dvOppBiDocItems.Item(i).Item("fltExchangeRate")) = 0 Then
                        Throw New Exception("EXCHANGE_RATE_CAN_NOT_BE_0")
                        Exit Function
                    End If

                    decAmount = CDec(CDec(dvOppBiDocItems.Item(i).Item("monPrice")) * CDec(dvOppBiDocItems.Item(i).Item("numUnitHourReceived")) * CDec(dvOppBiDocItems.Item(i).Item("fltExchangeRate")))
                    decDebitAmt = 0
                    decCreditAmt = 0
                    If decAmount > 0.0 Then
                        decDebitAmt = Math.Abs(decAmount)
                        decCreditAmt = 0
                    ElseIf decAmount < 0.0 Then
                        decDebitAmt = 0
                        decCreditAmt = Math.Abs(decAmount)
                    End If

                    objJE = New JournalEntryNew()

                    objJE.TransactionId = 0
                    objJE.DebitAmt = decDebitAmt
                    objJE.CreditAmt = decCreditAmt
                    objJE.ChartAcntId = CLng(ChartOfAccounting.GetDefaultAccount("PC", lngDomainID))

                    objJE.Description = String.Format("Unreceived {0:N2} Units : {1}", CDec(dvOppBiDocItems.Item(i).Item("numUnitHourReceived")), CCommon.Truncate(dvOppBiDocItems.Item(i).Item("vcItemName").ToString(), 30))  '"Authoritative Accounts"
                    objJE.CustomerId = CLng(dvOppBiDocItems.Item(i).Item("numDivisionID"))
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = CLng(dvOppBiDocItems.Item(i).Item("numoppitemtCode"))
                    objJE.BizDocItems = CStr(dvOppBiDocItems.Item(i).Item("charItemType"))
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = CLng(dvOppBiDocItems.Item(i).Item("numCurrencyID"))
                    objJE.FltExchangeRate = CDec(dvOppBiDocItems.Item(i).Item("fltExchangeRate"))
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numItemCode"))
                    objJE.ProjectID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numProjectID"))
                    objJE.ClassID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numClassID"))
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)


                    objJE = New JournalEntryNew()
                    objJE.TransactionId = 0
                    objJE.DebitAmt = decCreditAmt
                    objJE.CreditAmt = decDebitAmt
                    If CStr(dvOppBiDocItems.Item(i).Item("charItemType")) = "P" And CBool(dvOppBiDocItems.Item(i).Item("DropShip")) <> True Then
                        objJE.ChartAcntId = CLng(IIf(CInt(dvOppBiDocItems.Item(i).Item("ItemInventoryAsset")) = 0, ChartOfAccounting.GetDefaultAccount("BE", lngDomainID), dvOppBiDocItems.Item(i).Item("ItemInventoryAsset"))) '' 19 -- For Billable Time & Expenses in COA
                    ElseIf CStr(dvOppBiDocItems.Item(i).Item("charItemType")) = "P" And CBool(dvOppBiDocItems.Item(i).Item("DropShip")) = True Then
                        objJE.ChartAcntId = CLng(IIf(CInt(dvOppBiDocItems.Item(i).Item("ItemCoGs")) = 0, ChartOfAccounting.GetDefaultAccount("BE", lngDomainID), dvOppBiDocItems.Item(i).Item("ItemCoGs"))) '' 19 --COGS
                    Else
                        If CCommon.ToBool(HttpContext.Current.Session("EnableNonInventoryItemExpense")) AndAlso CCommon.ToLong(dvOppBiDocItems.Item(i).Item("itemCoGs")) > 0 Then
                            objJE.ChartAcntId = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("itemCoGs"))
                        Else
                            objJE.ChartAcntId = ChartOfAccounting.GetDefaultAccount("CG", lngDomainID)
                        End If
                    End If
                    objJE.Description = String.Format("Unreceived {0:N2} Units : {1}", CDec(dvOppBiDocItems.Item(i).Item("numUnitHourReceived")), CCommon.Truncate(dvOppBiDocItems.Item(i).Item("vcItemName").ToString(), 30))    '"Authoritative Accounts"
                    objJE.CustomerId = CLng(dvOppBiDocItems.Item(i).Item("numDivisionID"))
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = CLng(dvOppBiDocItems.Item(i).Item("numoppitemtCode"))
                    objJE.BizDocItems = "PC"
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = CLng(dvOppBiDocItems.Item(i).Item("numCurrencyID"))
                    objJE.FltExchangeRate = CDec(dvOppBiDocItems.Item(i).Item("fltExchangeRate"))
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numItemCode"))
                    objJE.ProjectID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numProjectID"))
                    objJE.ClassID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numClassID"))
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)
                Next

                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, lngDomainID)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveJournalEntriesPurchaseVariance(ByVal lngOppID As Long, ByVal lngDomainID As Long, ByVal dtOppBiDocItems As DataTable, ByVal lngJournalId As Long, ByVal lngDivisionID As Long, ByVal lngBizDocID As Long, ByVal TotalAmount As Decimal, ByVal decDiscAmt As Decimal, ByVal decShipCost As Decimal, ByVal decLateCharge As Decimal, ByVal CurrencyID As Long, ByVal ExchangeRate As Double, ByVal ExchangeRateBizDoc As Double, Optional ByVal DiscAcntType As Long = 0, Optional ByVal lngShippingMethod As Long = 0, Optional ByVal vcBaseCurrency As String = "", Optional ByVal vcForeignCurrency As String = "") As Boolean
            Try
                If ExchangeRate = 0 Then
                    Throw New Exception("EXCHANGE_RATE_CAN_NOT_BE_0")
                    Exit Function
                End If

                Dim i As Integer
                Dim ds As New DataSet
                Dim objAuthoritativeBizDocs As New AuthoritativeBizDocs
                'ExchangeRate = 1 / ExchangeRate
                Dim objJEList As New JournalEntryCollection

                Dim objJE As New JournalEntryNew()

                Dim dvOppBiDocItems As DataView

                dvOppBiDocItems = New DataView(dtOppBiDocItems)
                '' dvOppBiDocItems.RowFilter = " ItemCode <> 0 "  -- Temporarily Commented

                'objAuthoritativeBizDocs.AccountCode = "0104" '824 ',Billable Time & Expenses

                Dim decTotalPriceVariance As Decimal = 0

                Dim decAmount, decDebitAmt, decCreditAmt As Decimal

                For i = 0 To dvOppBiDocItems.Count - 1
                    decAmount = CDec(dvOppBiDocItems.Item(i).Item("ItemTotalAmount"))
                    decDebitAmt = 0
                    decCreditAmt = 0
                    If decAmount > 0.0 Then
                        decDebitAmt = CDec(Math.Abs(decAmount) * ExchangeRate)
                        decCreditAmt = 0
                    ElseIf decAmount < 0.0 Then
                        decDebitAmt = 0
                        decCreditAmt = CDec(Math.Abs(decAmount) * ExchangeRate)
                    End If

                    Dim decPriceVariance As Decimal = CDec(dvOppBiDocItems.Item(i).Item("Amount")) - decAmount
                    decTotalPriceVariance += decPriceVariance

                    objJE = New JournalEntryNew()
                    objJE.TransactionId = 0
                    objJE.DebitAmt = decDebitAmt
                    objJE.CreditAmt = decCreditAmt
                    objJE.ChartAcntId = CLng(ChartOfAccounting.GetDefaultAccount("PC", lngDomainID))
                    objJE.Description = String.Format("{0:N2} Units : {1}", CDec(dvOppBiDocItems.Item(i).Item("Unit")), CCommon.Truncate(dvOppBiDocItems.Item(i).Item("Item").ToString(), 30)) '"Authoritative Accounts"
                    objJE.CustomerId = lngDivisionID
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = CLng(dvOppBiDocItems.Item(i).Item("numoppitemtCode"))
                    objJE.BizDocItems = "PC"
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = CurrencyID
                    objJE.FltExchangeRate = ExchangeRate
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemCode"))
                    objJE.ProjectID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numProjectID"))
                    objJE.ClassID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numClassID"))
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)

                    If decPriceVariance <> 0 Then
                        objJE = New JournalEntryNew()

                        objJE.TransactionId = 0
                        objJE.DebitAmt = CDec(CDec(IIf(decPriceVariance > 0, decPriceVariance, 0)) * ExchangeRate)
                        objJE.CreditAmt = CDec(CDec(IIf(decPriceVariance > 0, 0, decPriceVariance * -1)) * ExchangeRate)
                        objJE.ChartAcntId = CLng(ChartOfAccounting.GetDefaultAccount("PV", lngDomainID))
                        objJE.Description = String.Format("{0:N2} Units : {1}", CDec(dvOppBiDocItems.Item(i).Item("Unit")), CCommon.Truncate(dvOppBiDocItems.Item(i).Item("Item").ToString(), 30))  '"Authoritative Accounts"
                        objJE.CustomerId = lngDivisionID
                        objJE.MainDeposit = False
                        objJE.MainCheck = False
                        objJE.MainCashCredit = False
                        objJE.OppitemtCode = CLng(dvOppBiDocItems.Item(i).Item("numoppitemtCode"))
                        objJE.BizDocItems = "PV"
                        objJE.Reference = ""
                        objJE.PaymentMethod = 0
                        objJE.Reconcile = False
                        objJE.CurrencyID = CurrencyID
                        objJE.FltExchangeRate = ExchangeRate
                        objJE.TaxItemID = 0
                        objJE.BizDocsPaymentDetailsId = 0
                        objJE.ContactID = 0
                        objJE.ItemID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemCode"))
                        objJE.ProjectID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numProjectID"))
                        objJE.ClassID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numClassID"))
                        objJE.CommissionID = 0
                        objJE.ReconcileID = 0
                        objJE.Cleared = False
                        objJE.ReferenceType = 0
                        objJE.ReferenceID = 0

                        objJEList.Add(objJE)
                    End If
                Next

                'For Account Payable as Credit Entry in Journal
                decAmount = TotalAmount

                decDebitAmt = 0
                decCreditAmt = 0

                If decAmount > 0.0 Then
                    decDebitAmt = 0
                    decCreditAmt = CDec(Math.Abs(decAmount) * ExchangeRateBizDoc)
                ElseIf decAmount < 0.0 Then
                    decDebitAmt = CDec(Math.Abs(decAmount) * ExchangeRateBizDoc)
                    decCreditAmt = 0
                End If

                Dim objOppBizDocs As New Opportunities.OppBizDocs
                Dim lngAPAccountId As Long = objOppBizDocs.ValidateCustomerAR_APAccounts("AP", lngDomainID, lngDivisionID)

                'objAuthoritativeBizDocs.DomainId = lngDomainID
                'objAuthoritativeBizDocs.ChargeCode = "AP"
                ''objAuthoritativeBizDocs.AccountTypeId = 815
                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = decDebitAmt
                objJE.CreditAmt = decCreditAmt '+ CDec(IIf(decTotalPriceVariance > 0, decTotalPriceVariance, decTotalPriceVariance))
                objJE.ChartAcntId = lngAPAccountId 'objAuthoritativeBizDocs.GetChartAcountIdForAuthorizativeBizDocs '3
                objJE.Description = String.Format("Purchase")  ' "Authoritative Accounts"
                objJE.CustomerId = lngDivisionID
                objJE.MainDeposit = False
                objJE.MainCheck = False
                objJE.MainCashCredit = False
                objJE.OppitemtCode = 0
                objJE.BizDocItems = "AP" ''Account Payable
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = CurrencyID
                objJE.FltExchangeRate = ExchangeRateBizDoc
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = False
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)

                '' For Discount Given as Debit in journal Entry
                If decDiscAmt <> 0 Then
                    'objAuthoritativeBizDocs.AccountTypeId = 822
                    objJE = New JournalEntryNew()

                    objJE.TransactionId = 0
                    objJE.DebitAmt = 0
                    objJE.CreditAmt = CDec(decDiscAmt * ExchangeRateBizDoc)
                    objJE.ChartAcntId = CLng(IIf(DiscAcntType > 0, DiscAcntType, ChartOfAccounting.GetDefaultAccount("DG", lngDomainID)))
                    objJE.Description = String.Format("Purchase")  ' "Authoritative Accounts"
                    objJE.CustomerId = lngDivisionID
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = "DG" ''Discount Given
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = CurrencyID
                    objJE.FltExchangeRate = ExchangeRateBizDoc
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)
                End If

                ''For Shipping Income  as Credit in journal Entry
                If decShipCost <> 0 Then
                    objAuthoritativeBizDocs.DomainID = lngDomainID
                    objAuthoritativeBizDocs.ChargeCode = "SI"

                    Dim lngShippingIncomeAccountID As Long = GetShippingIncomeAccount(lngShippingMethod, lngDomainID)
                    If lngShippingIncomeAccountID = 0 Then 'get default Shipping account from AccountCharges table
                        lngShippingIncomeAccountID = ChartOfAccounting.GetDefaultAccount("SI", lngDomainID) 'Shippig income
                    End If

                    objJE = New JournalEntryNew()

                    objJE.TransactionId = 0
                    objJE.DebitAmt = CDec(decShipCost * ExchangeRateBizDoc)
                    objJE.CreditAmt = 0
                    objJE.ChartAcntId = lngShippingIncomeAccountID '20
                    objJE.Description = String.Format("Purchase") ' "Authoritative Accounts"
                    objJE.CustomerId = lngDivisionID
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = "SI" ''Shipping Income
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = CurrencyID
                    objJE.FltExchangeRate = ExchangeRateBizDoc
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)
                End If

                ''For Late Charges  as Credit in journal Entry
                If decLateCharge <> 0 Then
                    'objAuthoritativeBizDocs.AccountTypeId = 822

                    objJE = New JournalEntryNew()

                    objJE.TransactionId = 0
                    objJE.DebitAmt = CDec(decLateCharge * ExchangeRateBizDoc)
                    objJE.CreditAmt = 0
                    objJE.ChartAcntId = ChartOfAccounting.GetDefaultAccount("LC", lngDomainID) '21
                    objJE.Description = String.Format("Purchase")  ' "Authoritative Accounts"
                    objJE.CustomerId = lngDivisionID
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = "LC" ''Late Charges
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = CurrencyID
                    objJE.FltExchangeRate = ExchangeRateBizDoc
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)
                End If




                Dim dtTable As DataTable
                Dim objUserAccess As New UserAccess
                objUserAccess.DomainID = lngDomainID
                dtTable = objUserAccess.GetDomainDetails()
                Dim decTaxAmount As Decimal = 0
                Dim decTotalTaxAmount As Decimal = 0

                If dtTable.Rows.Count > 0 AndAlso CCommon.ToBool(dtTable.Rows(0).Item("bitPurchaseTaxCredit")) Then
                    Dim lngPurchaseTaxCredit As Long = ChartOfAccounting.GetDefaultAccount("PT", lngDomainID)

                    If lngPurchaseTaxCredit > 0 Then
                        Dim objTaxDetails As New TaxDetails
                        objTaxDetails.DomainID = lngDomainID
                        objTaxDetails.OppID = lngOppID
                        objTaxDetails.DivisionID = lngDivisionID
                        objTaxDetails.OppBizDocID = lngBizDocID
                        objTaxDetails.TaxItemID = 0

                        decTaxAmount = objTaxDetails.GetTaxAmtOppForTaxItem1
                        If Math.Abs(decTaxAmount) > 0 Then
                            objJE = New JournalEntryNew()
                            decDebitAmt = 0
                            decCreditAmt = 0
                            If decTaxAmount > 0 Then
                                decDebitAmt = 0
                                decCreditAmt = CDec(Math.Abs(decTaxAmount) * ExchangeRate)
                            ElseIf decTaxAmount < 0 Then
                                decDebitAmt = CDec(Math.Abs(decTaxAmount) * ExchangeRate)
                                decCreditAmt = 0
                            End If

                            decTotalTaxAmount += Math.Abs(decTaxAmount)

                            objJE.TransactionId = 0
                            objJE.DebitAmt = decCreditAmt
                            objJE.CreditAmt = decDebitAmt
                            objJE.ChartAcntId = lngPurchaseTaxCredit  'ChartOfAccounting.GetDefaultAccount("PT", lngDomainID)
                            objJE.Description = "Purchase" '"Authoritative Accounts"
                            objJE.CustomerId = lngDivisionID
                            objJE.MainDeposit = False
                            objJE.MainCheck = False
                            objJE.MainCashCredit = False
                            objJE.OppitemtCode = 0
                            objJE.BizDocItems = "PT"
                            objJE.Reference = ""
                            objJE.PaymentMethod = 0
                            objJE.Reconcile = False
                            objJE.CurrencyID = CurrencyID
                            objJE.FltExchangeRate = ExchangeRate
                            objJE.TaxItemID = 0
                            objJE.BizDocsPaymentDetailsId = 0
                            objJE.ContactID = 0
                            objJE.ItemID = 0
                            objJE.ProjectID = 0
                            objJE.ClassID = 0
                            objJE.CommissionID = 0
                            objJE.ReconcileID = 0
                            objJE.Cleared = False
                            objJE.ReferenceType = 0
                            objJE.ReferenceID = 0

                            objJEList.Add(objJE)
                        End If

                        'CRV TAX
                        objTaxDetails.TaxItemID = 1
                        decTaxAmount = objTaxDetails.GetTaxAmtOppForTaxItem1
                        If Math.Abs(decTaxAmount) > 0 Then
                            objJE = New JournalEntryNew()
                            decDebitAmt = 0
                            decCreditAmt = 0
                            If decTaxAmount > 0 Then
                                decDebitAmt = 0
                                decCreditAmt = CDec(Math.Abs(decTaxAmount) * ExchangeRate)
                            ElseIf decTaxAmount < 0 Then
                                decDebitAmt = CDec(Math.Abs(decTaxAmount) * ExchangeRate)
                                decCreditAmt = 0
                            End If

                            decTotalTaxAmount += Math.Abs(decTaxAmount)

                            objJE.TransactionId = 0
                            objJE.DebitAmt = decCreditAmt
                            objJE.CreditAmt = decDebitAmt
                            objJE.ChartAcntId = lngPurchaseTaxCredit  'ChartOfAccounting.GetDefaultAccount("PT", lngDomainID)
                            objJE.Description = "Purchase" '"Authoritative Accounts"
                            objJE.CustomerId = lngDivisionID
                            objJE.MainDeposit = False
                            objJE.MainCheck = False
                            objJE.MainCashCredit = False
                            objJE.OppitemtCode = 0
                            objJE.BizDocItems = "PT"
                            objJE.Reference = ""
                            objJE.PaymentMethod = 0
                            objJE.Reconcile = False
                            objJE.CurrencyID = CurrencyID
                            objJE.FltExchangeRate = ExchangeRate
                            objJE.TaxItemID = 0
                            objJE.BizDocsPaymentDetailsId = 0
                            objJE.ContactID = 0
                            objJE.ItemID = 0
                            objJE.ProjectID = 0
                            objJE.ClassID = 0
                            objJE.CommissionID = 0
                            objJE.ReconcileID = 0
                            objJE.Cleared = False
                            objJE.ReferenceType = 0
                            objJE.ReferenceID = 0

                            objJEList.Add(objJE)
                        End If

                        ''End If

                        Dim dtPurchaseTax As DataTable
                        Dim k As Integer
                        dtPurchaseTax = objTaxDetails.GetTaxItems()
                        For k = 0 To dtPurchaseTax.Rows.Count - 1
                            objTaxDetails.TaxItemID = CLng(dtPurchaseTax.Rows(k).Item("numTaxItemID"))
                            decTaxAmount = objTaxDetails.GetTaxAmtOppForTaxItem1
                            If Math.Abs(decTaxAmount) > 0 Then
                                objJE = New JournalEntryNew()
                                decDebitAmt = 0
                                decCreditAmt = 0
                                If decTaxAmount > 0 Then
                                    decDebitAmt = 0
                                    decCreditAmt = CDec(Math.Abs(decTaxAmount) * ExchangeRate)
                                ElseIf decTaxAmount < 0 Then
                                    decDebitAmt = CDec(Math.Abs(decTaxAmount) * ExchangeRate)
                                    decCreditAmt = 0
                                End If

                                decTotalTaxAmount += Math.Abs(decTaxAmount)

                                objJE.TransactionId = 0
                                objJE.DebitAmt = decCreditAmt
                                objJE.CreditAmt = decDebitAmt
                                objJE.ChartAcntId = CLng(dtPurchaseTax.Rows(k).Item("numChartOfAcntID"))
                                objJE.Description = "Purchase" ' "Authoritative Accounts"
                                objJE.CustomerId = lngDivisionID
                                objJE.MainDeposit = False
                                objJE.MainCheck = False
                                objJE.MainCashCredit = False
                                objJE.OppitemtCode = 0
                                objJE.BizDocItems = "OT"
                                objJE.Reference = ""
                                objJE.PaymentMethod = 0
                                objJE.Reconcile = False
                                objJE.CurrencyID = CurrencyID
                                objJE.FltExchangeRate = ExchangeRate
                                objJE.TaxItemID = CLng(dtPurchaseTax.Rows(k).Item("numTaxItemID"))
                                objJE.BizDocsPaymentDetailsId = 0
                                objJE.ContactID = 0
                                objJE.ItemID = 0
                                objJE.ProjectID = 0
                                objJE.ClassID = 0
                                objJE.CommissionID = 0
                                objJE.ReconcileID = 0
                                objJE.Cleared = False
                                objJE.ReferenceType = 0
                                objJE.ReferenceID = 0

                                objJEList.Add(objJE)
                            End If
                        Next
                    End If

                End If

                'Dim decTotalBizDocsItemTotalAmount As Decimal = CDec(CCommon.ToDecimal(dtOppBiDocItems.Compute("sum(Amount)", "Amount>0")) * ExchangeRate)
                Dim decTotalItemTotalAmount As Decimal = CDec(CCommon.ToDecimal(dtOppBiDocItems.Compute("sum(ItemTotalAmount)", "")) * ExchangeRate)
                Dim decExchangeFlactuation As Decimal = CDec((decTotalItemTotalAmount + (decTotalPriceVariance * ExchangeRate)) + CDec(decShipCost * ExchangeRateBizDoc) _
                                                    + CDec(decLateCharge * ExchangeRateBizDoc) - (CDec(TotalAmount * ExchangeRateBizDoc) _
                                                        + CDec(decDiscAmt * ExchangeRateBizDoc)) + (CDec(decTotalTaxAmount * ExchangeRateBizDoc)))

                If decExchangeFlactuation <> 0 Then
                    'objAuthoritativeBizDocs.AccountTypeId = 822

                    objJE = New JournalEntryNew()

                    objJE.TransactionId = 0
                    objJE.DebitAmt = CDec(IIf(decExchangeFlactuation > 0, 0, decExchangeFlactuation * -1))
                    objJE.CreditAmt = CDec(IIf(decExchangeFlactuation > 0, decExchangeFlactuation, 0))
                    objJE.ChartAcntId = ChartOfAccounting.GetDefaultAccount("FE", lngDomainID) '21
                    objJE.Description = String.Format("1 {0} = Order : {1} {3} / BD : {2} {3}", vcForeignCurrency, ExchangeRate, ExchangeRateBizDoc, vcBaseCurrency) ' "Authoritative Accounts"
                    objJE.CustomerId = lngDivisionID
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = "FE" ''Late Charges
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = CurrencyID
                    objJE.FltExchangeRate = ExchangeRateBizDoc
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)
                End If



                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, lngDomainID)
                'ds.Tables.Add(dt)
                'ValidateJournalEntries(ds, lngJournalId, lngDomainID)  'Added to detect imbalance in journals before posting it to accounting system
                'lstr = ds.GetXml()
                'objAuthoritativeBizDocs.JournalId = lngJournalId
                'objAuthoritativeBizDocs.Mode = 0

                'objAuthoritativeBizDocs.RecurringMode = 0
                'objAuthoritativeBizDocs.JournalDetails = lstr
                'objAuthoritativeBizDocs.DomainID = lngDomainID
                'objAuthoritativeBizDocs.OppBIzDocID = lngBizDocID
                'objAuthoritativeBizDocs.OppId = lngOppID
                'objAuthoritativeBizDocs.SaveDataToJournalDetailsForAuthoritativeBizDocs()
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' <summary>
        ''' This methods creates sales clearing enteries for qty fulfilled using sales fulfillment page. 
        ''' Code is part of code from method SaveJournalEntriesSales which is copy pasted but instead of debiting cost of goods sold account it will debit sales clearing account
        ''' </summary>
        ''' <param name="lngOppID">Opportunity ID</param>
        ''' <param name="lngDomainID">Domain ID</param>
        ''' <param name="dtOppBiDocItems">BizDoC Items DataTable</param>
        ''' <param name="lngJournalId">Journal Header ID</param>
        ''' <param name="lngDivisionID">Division ID</param>
        ''' <param name="lngBizDocID">BizDoc ID</param>
        ''' <param name="CurrencyID">Currency ID</param>
        ''' <param name="ExchangeRate">Exchange Rate</param>
        ''' <remarks></remarks>
        Public Sub SaveJournalEnteriesSalesClearing(ByVal lngOppID As Long, ByVal lngDomainID As Long, ByVal dtOppBiDocItems As DataTable, ByVal lngJournalId As Long, ByVal lngDivisionID As Long, ByVal lngBizDocID As Long, ByVal CurrencyID As Long, ByVal ExchangeRate As Double)
            Try
                If ExchangeRate = 0 Then
                    Throw New Exception("EXCHANGE_RATE_CAN_NOT_BE_0")
                    Exit Sub
                End If

                Dim i As Integer
                Dim decACProductCost, decTaxAmount As Decimal
                Dim lngItemAssetAccount, lngSalesClearingAccount As Long
                'ExchangeRate = 1 / ExchangeRate

                Dim objJEList As New JournalEntryCollection

                Dim objJE As New JournalEntryNew()

                Dim dvOppBiDocItems As DataView

                dvOppBiDocItems = New DataView(dtOppBiDocItems)
                '' dvOppBiDocItems.RowFilter = " ItemCode <> 0 "  -- Temporarily Commented
                Dim decDebitAmt, decCreditAmt As Decimal

                For i = 0 To dvOppBiDocItems.Count - 1
                    If CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemInventoryAsset")) > 0 Then
                        lngItemAssetAccount = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemInventoryAsset"))
                    End If

                    lngSalesClearingAccount = ChartOfAccounting.GetDefaultAccount("SC", lngDomainID)
                    If lngSalesClearingAccount = 0 Then
                        Throw New Exception("DEFAULT_SALES_CLEARING_ACCOUNT_NOT_AVAILABLE")
                        Exit Sub
                    End If

                    If CStr(dvOppBiDocItems.Item(i).Item("Type")) = "P" And CDec(dvOppBiDocItems.Item(i).Item("AverageCost")) > 0 And CBool(dvOppBiDocItems.Item(i).Item("bitDropShip")) <> True Then
                        'Do not include Kit and Assembly Items for Average Cost
                        If CBool(dvOppBiDocItems.Item(i).Item("bitKitParent")) = False Then
                            decACProductCost = CDec(dvOppBiDocItems.Item(i).Item("unit")) * CDec(dvOppBiDocItems.Item(i).Item("AverageCost"))
                            If decACProductCost > 0 Then
                                objJE = New JournalEntryNew()

                                objJE.TransactionId = 0
                                objJE.DebitAmt = 0
                                objJE.CreditAmt = decACProductCost '* ExchangeRate 'Removed by chintan as average cost is always going to same whenever we place order in any other currency
                                objJE.ChartAcntId = lngItemAssetAccount
                                objJE.Description = "Item Shipped" '"Authoritative Accounts"
                                objJE.CustomerId = lngDivisionID
                                objJE.MainDeposit = False
                                objJE.MainCheck = False
                                objJE.MainCashCredit = False
                                objJE.OppitemtCode = CLng(dvOppBiDocItems.Item(i).Item("numoppitemtCode"))
                                objJE.BizDocItems = "IA"
                                objJE.Reference = ""
                                objJE.PaymentMethod = 0
                                objJE.Reconcile = False
                                objJE.CurrencyID = CurrencyID
                                objJE.FltExchangeRate = ExchangeRate
                                objJE.TaxItemID = 0
                                objJE.BizDocsPaymentDetailsId = 0
                                objJE.ContactID = 0
                                objJE.ItemID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemCode"))
                                objJE.ProjectID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numProjectID"))
                                objJE.ClassID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numClassID"))
                                objJE.CommissionID = 0
                                objJE.ReconcileID = 0
                                objJE.Cleared = False
                                objJE.ReferenceType = 0
                                objJE.ReferenceID = 0

                                objJEList.Add(objJE)


                                objJE = New JournalEntryNew()

                                objJE.TransactionId = 0
                                objJE.DebitAmt = decACProductCost '* ExchangeRate 'Removed by chintan as average cost is always going to same whenever we place order in any other currency
                                objJE.CreditAmt = 0
                                objJE.ChartAcntId = lngSalesClearingAccount
                                objJE.Description = "Sales Clearing" '"Authoritative Accounts"
                                objJE.CustomerId = lngDivisionID
                                objJE.MainDeposit = False
                                objJE.MainCheck = False
                                objJE.MainCashCredit = False
                                objJE.OppitemtCode = CLng(dvOppBiDocItems.Item(i).Item("numoppitemtCode"))
                                objJE.BizDocItems = "SC"
                                objJE.Reference = ""
                                objJE.PaymentMethod = 0
                                objJE.Reconcile = False
                                objJE.CurrencyID = CurrencyID
                                objJE.FltExchangeRate = ExchangeRate
                                objJE.TaxItemID = 0
                                objJE.BizDocsPaymentDetailsId = 0
                                objJE.ContactID = 0
                                objJE.ItemID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemCode"))
                                objJE.ProjectID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numProjectID"))
                                objJE.ClassID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numClassID"))
                                objJE.CommissionID = 0
                                objJE.ReconcileID = 0
                                objJE.Cleared = False
                                objJE.ReferenceType = 0
                                objJE.ReferenceID = 0

                                objJEList.Add(objJE)
                            End If
                        End If
                    End If
                Next

                'OpportunityKitItems
                Dim ds As New DataSet
                Dim objOppBizDocs As OppBizDocs = New OppBizDocs
                objOppBizDocs.OppId = lngOppID
                objOppBizDocs.OppBizDocId = lngBizDocID
                Dim dtOppKitItems As DataTable = objOppBizDocs.GetOpportunityKitItemsForAuthorizativeAccounting.Tables(0)
                If dtOppKitItems.Rows.Count > 0 Then
                    For i = 0 To dtOppKitItems.Rows.Count - 1

                        If CCommon.ToLong(dtOppKitItems.Rows(i).Item("ItemInventoryAsset")) > 0 Then
                            lngItemAssetAccount = CCommon.ToLong(dtOppKitItems.Rows(i).Item("ItemInventoryAsset"))
                        End If

                        lngSalesClearingAccount = ChartOfAccounting.GetDefaultAccount("SC", lngDomainID)
                        If lngSalesClearingAccount = 0 Then
                            Throw New Exception("DEFAULT_SALES_CLEARING_ACCOUNT_NOT_AVAILABLE")
                            Exit Sub
                        End If

                        If CStr(dtOppKitItems.Rows(i).Item("Type")) = "P" And CDec(dtOppKitItems.Rows(i).Item("AverageCost")) > 0 And CBool(dtOppKitItems.Rows(i).Item("bitDropShip")) <> True Then
                            decACProductCost = CDec(dtOppKitItems.Rows(i).Item("unit")) * CDec(dtOppKitItems.Rows(i).Item("AverageCost"))
                            If decACProductCost > 0 Then
                                objJE = New JournalEntryNew()

                                objJE.TransactionId = 0
                                objJE.DebitAmt = 0
                                objJE.CreditAmt = decACProductCost '* ExchangeRate 'Removed by chintan as average cost is always going to same whenever we place order in any other currency
                                objJE.ChartAcntId = lngItemAssetAccount
                                objJE.Description = "Items Shipped - Kit/Assembly" '"Authoritative Accounts"
                                objJE.CustomerId = lngDivisionID
                                objJE.MainDeposit = False
                                objJE.MainCheck = False
                                objJE.MainCashCredit = False
                                objJE.OppitemtCode = CLng(dtOppKitItems.Rows(i).Item("numoppitemtCode"))
                                objJE.BizDocItems = "IA"
                                objJE.Reference = ""
                                objJE.PaymentMethod = 0
                                objJE.Reconcile = False
                                objJE.CurrencyID = CurrencyID
                                objJE.FltExchangeRate = ExchangeRate
                                objJE.TaxItemID = 0
                                objJE.BizDocsPaymentDetailsId = 0
                                objJE.ContactID = 0
                                objJE.ItemID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("ItemCode"))
                                objJE.ProjectID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("numProjectID"))
                                objJE.ClassID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("numClassID"))
                                objJE.CommissionID = 0
                                objJE.ReconcileID = 0
                                objJE.Cleared = False
                                objJE.ReferenceType = 0
                                objJE.ReferenceID = 0

                                objJEList.Add(objJE)


                                objJE = New JournalEntryNew()

                                objJE.TransactionId = 0
                                objJE.DebitAmt = decACProductCost '* ExchangeRate 'Removed by chintan as average cost is always going to same whenever we place order in any other currency
                                objJE.CreditAmt = 0
                                objJE.ChartAcntId = lngSalesClearingAccount
                                objJE.Description = "Sales Clearing - Kit/Assembly" '"Authoritative Accounts"
                                objJE.CustomerId = lngDivisionID
                                objJE.MainDeposit = False
                                objJE.MainCheck = False
                                objJE.MainCashCredit = False
                                objJE.OppitemtCode = CLng(dtOppKitItems.Rows(i).Item("numoppitemtCode"))
                                objJE.BizDocItems = "SC"
                                objJE.Reference = ""
                                objJE.PaymentMethod = 0
                                objJE.Reconcile = False
                                objJE.CurrencyID = CurrencyID
                                objJE.FltExchangeRate = ExchangeRate
                                objJE.TaxItemID = 0
                                objJE.BizDocsPaymentDetailsId = 0
                                objJE.ContactID = 0
                                objJE.ItemID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("ItemCode"))
                                objJE.ProjectID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("numProjectID"))
                                objJE.ClassID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("numClassID"))
                                objJE.CommissionID = 0
                                objJE.ReconcileID = 0
                                objJE.Cleared = False
                                objJE.ReferenceType = 0
                                objJE.ReferenceID = 0

                                objJEList.Add(objJE)
                            End If
                        End If
                    Next
                End If

                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, lngDomainID)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        ''' <summary>
        ''' In new sales clearing journal entries we are debiting cost of goods sold account instead of sales clearing account
        ''' </summary>
        ''' <param name="lngOppID"></param>
        ''' <param name="lngDomainID"></param>
        ''' <param name="dtOppBiDocItems"></param>
        ''' <param name="lngJournalId"></param>
        ''' <param name="lngDivisionID"></param>
        ''' <param name="lngBizDocID"></param>
        ''' <param name="CurrencyID"></param>
        ''' <param name="ExchangeRate"></param>
        ''' <remarks></remarks>
        Public Sub SaveJournalEnteriesSalesClearingNew(ByVal lngOppID As Long, ByVal lngDomainID As Long, ByVal dtOppBiDocItems As DataTable, ByVal lngJournalId As Long, ByVal lngDivisionID As Long, ByVal lngBizDocID As Long, ByVal CurrencyID As Long, ByVal ExchangeRate As Double)
            Try
                If ExchangeRate = 0 Then
                    Throw New Exception("EXCHANGE_RATE_CAN_NOT_BE_0")
                    Exit Sub
                End If

                Dim i As Integer
                Dim decACProductCost, decTaxAmount As Decimal
                Dim lngItemAssetAccount, lngItemCOGsAccount, lngItemExpenseAccount As Long
                'ExchangeRate = 1 / ExchangeRate

                Dim objJEList As New JournalEntryCollection

                Dim objJE As New JournalEntryNew()

                Dim dvOppBiDocItems As DataView

                dvOppBiDocItems = New DataView(dtOppBiDocItems)
                '' dvOppBiDocItems.RowFilter = " ItemCode <> 0 "  -- Temporarily Commented
                Dim decDebitAmt, decCreditAmt As Decimal

                For i = 0 To dvOppBiDocItems.Count - 1
                    If CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemInventoryAsset")) > 0 Then
                        lngItemAssetAccount = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemInventoryAsset"))
                    End If

                    If CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemCoGs")) > 0 Then
                        lngItemCOGsAccount = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemCoGs"))
                    Else
                        lngItemCOGsAccount = ChartOfAccounting.GetDefaultAccount("CG", lngDomainID)
                    End If

                    If CCommon.ToLong(dvOppBiDocItems.Item(i).Item("itemExpenseAccount")) > 0 Then
                        lngItemExpenseAccount = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("itemExpenseAccount"))
                    End If

                    If CStr(dvOppBiDocItems.Item(i).Item("Type")) = "P" And CDec(dvOppBiDocItems.Item(i).Item("ShippedAverageCost")) > 0 And CBool(dvOppBiDocItems.Item(i).Item("bitDropShip")) <> True Then

                        'Do not include Kit and Assembly Items for Average Cost
                        If CBool(dvOppBiDocItems.Item(i).Item("bitKitParent")) = False Then
                            decACProductCost = CDec(dvOppBiDocItems.Item(i).Item("unit")) * CDec(dvOppBiDocItems.Item(i).Item("ShippedAverageCost"))
                            If decACProductCost > 0 Then
                                If lngItemAssetAccount = 0 Then
                                    Throw New Exception("Asset account does not exist(s).")
                                    Exit Sub
                                End If

                                If lngItemCOGsAccount = 0 Then
                                    Throw New Exception("Cost Of Goods sold account does not exist(s).")
                                    Exit Sub
                                End If

                                objJE = New JournalEntryNew()
                                objJE.TransactionId = 0
                                objJE.DebitAmt = 0
                                objJE.CreditAmt = decACProductCost '* ExchangeRate 'Removed by chintan as average cost is always going to same whenever we place order in any other currency
                                objJE.ChartAcntId = lngItemAssetAccount
                                objJE.Description = "Item Shipped" '"Authoritative Accounts"
                                objJE.CustomerId = lngDivisionID
                                objJE.MainDeposit = False
                                objJE.MainCheck = False
                                objJE.MainCashCredit = False
                                objJE.OppitemtCode = CLng(dvOppBiDocItems.Item(i).Item("numoppitemtCode"))
                                objJE.BizDocItems = "IA"
                                objJE.Reference = ""
                                objJE.PaymentMethod = 0
                                objJE.Reconcile = False
                                objJE.CurrencyID = CurrencyID
                                objJE.FltExchangeRate = ExchangeRate
                                objJE.TaxItemID = 0
                                objJE.BizDocsPaymentDetailsId = 0
                                objJE.ContactID = 0
                                objJE.ItemID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemCode"))
                                objJE.ProjectID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numProjectID"))
                                objJE.ClassID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numClassID"))
                                objJE.CommissionID = 0
                                objJE.ReconcileID = 0
                                objJE.Cleared = False
                                objJE.ReferenceType = 0
                                objJE.ReferenceID = 0

                                objJEList.Add(objJE)


                                objJE = New JournalEntryNew()

                                objJE.TransactionId = 0
                                objJE.DebitAmt = decACProductCost '* ExchangeRate 'Removed by chintan as average cost is always going to same whenever we place order in any other currency
                                objJE.CreditAmt = 0
                                objJE.ChartAcntId = lngItemCOGsAccount
                                objJE.Description = "Cost of Goods Sold" '"Authoritative Accounts"
                                objJE.CustomerId = lngDivisionID
                                objJE.MainDeposit = False
                                objJE.MainCheck = False
                                objJE.MainCashCredit = False
                                objJE.OppitemtCode = CLng(dvOppBiDocItems.Item(i).Item("numoppitemtCode"))
                                objJE.BizDocItems = "COG"
                                objJE.Reference = ""
                                objJE.PaymentMethod = 0
                                objJE.Reconcile = False
                                objJE.CurrencyID = CurrencyID
                                objJE.FltExchangeRate = ExchangeRate
                                objJE.TaxItemID = 0
                                objJE.BizDocsPaymentDetailsId = 0
                                objJE.ContactID = 0
                                objJE.ItemID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemCode"))
                                objJE.ProjectID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numProjectID"))
                                objJE.ClassID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numClassID"))
                                objJE.CommissionID = 0
                                objJE.ReconcileID = 0
                                objJE.Cleared = False
                                objJE.ReferenceType = 0
                                objJE.ReferenceID = 0

                                objJEList.Add(objJE)
                            End If
                        End If
                    ElseIf CStr(dvOppBiDocItems.Item(i).Item("Type")) = "S" AndAlso CCommon.ToBool(dvOppBiDocItems.Item(i).Item("bitExpenseItem")) Then
                        decACProductCost = CDec(dvOppBiDocItems.Item(i).Item("Amount"))
                        If decACProductCost > 0 Then
                            If lngItemExpenseAccount = 0 Then
                                Throw New Exception("Expense account does not exist(s).")
                                Exit Sub
                            End If

                            objJE = New JournalEntryNew()
                            objJE.TransactionId = 0
                            objJE.DebitAmt = decACProductCost
                            objJE.CreditAmt = 0 '* ExchangeRate 'Removed by chintan as average cost is always going to same whenever we place order in any other currency
                            objJE.ChartAcntId = lngItemAssetAccount
                            objJE.Description = "Inventoriable Cost"
                            objJE.CustomerId = lngDivisionID
                            objJE.MainDeposit = False
                            objJE.MainCheck = False
                            objJE.MainCashCredit = False
                            objJE.OppitemtCode = CLng(dvOppBiDocItems.Item(i).Item("numoppitemtCode"))
                            objJE.BizDocItems = "IA"
                            objJE.Reference = ""
                            objJE.PaymentMethod = 0
                            objJE.Reconcile = False
                            objJE.CurrencyID = CurrencyID
                            objJE.FltExchangeRate = ExchangeRate
                            objJE.TaxItemID = 0
                            objJE.BizDocsPaymentDetailsId = 0
                            objJE.ContactID = 0
                            objJE.ItemID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemCode"))
                            objJE.ProjectID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numProjectID"))
                            objJE.ClassID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numClassID"))
                            objJE.CommissionID = 0
                            objJE.ReconcileID = 0
                            objJE.Cleared = False
                            objJE.ReferenceType = 0
                            objJE.ReferenceID = 0

                            objJEList.Add(objJE)

                            objJE = New JournalEntryNew()
                            objJE.TransactionId = 0
                            objJE.DebitAmt = 0
                            objJE.CreditAmt = decACProductCost
                            objJE.ChartAcntId = lngItemExpenseAccount
                            objJE.Description = "Inventoriable Cost"
                            objJE.CustomerId = lngDivisionID
                            objJE.MainDeposit = False
                            objJE.MainCheck = False
                            objJE.MainCashCredit = False
                            objJE.OppitemtCode = CLng(dvOppBiDocItems.Item(i).Item("numoppitemtCode"))
                            objJE.BizDocItems = "IE"
                            objJE.Reference = ""
                            objJE.PaymentMethod = 0
                            objJE.Reconcile = False
                            objJE.CurrencyID = CurrencyID
                            objJE.FltExchangeRate = ExchangeRate
                            objJE.TaxItemID = 0
                            objJE.BizDocsPaymentDetailsId = 0
                            objJE.ContactID = 0
                            objJE.ItemID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemCode"))
                            objJE.ProjectID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numProjectID"))
                            objJE.ClassID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numClassID"))
                            objJE.CommissionID = 0
                            objJE.ReconcileID = 0
                            objJE.Cleared = False
                            objJE.ReferenceType = 0
                            objJE.ReferenceID = 0

                            objJEList.Add(objJE)


                            objJE = New JournalEntryNew()
                            objJE.TransactionId = 0
                            objJE.DebitAmt = 0
                            objJE.CreditAmt = decACProductCost '* ExchangeRate 'Removed by chintan as average cost is always going to same whenever we place order in any other currency
                            objJE.ChartAcntId = lngItemAssetAccount
                            objJE.Description = "Item Shipped" '"Authoritative Accounts"
                            objJE.CustomerId = lngDivisionID
                            objJE.MainDeposit = False
                            objJE.MainCheck = False
                            objJE.MainCashCredit = False
                            objJE.OppitemtCode = CLng(dvOppBiDocItems.Item(i).Item("numoppitemtCode"))
                            objJE.BizDocItems = "IA"
                            objJE.Reference = ""
                            objJE.PaymentMethod = 0
                            objJE.Reconcile = False
                            objJE.CurrencyID = CurrencyID
                            objJE.FltExchangeRate = ExchangeRate
                            objJE.TaxItemID = 0
                            objJE.BizDocsPaymentDetailsId = 0
                            objJE.ContactID = 0
                            objJE.ItemID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemCode"))
                            objJE.ProjectID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numProjectID"))
                            objJE.ClassID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numClassID"))
                            objJE.CommissionID = 0
                            objJE.ReconcileID = 0
                            objJE.Cleared = False
                            objJE.ReferenceType = 0
                            objJE.ReferenceID = 0

                            objJEList.Add(objJE)


                            objJE = New JournalEntryNew()

                            objJE.TransactionId = 0
                            objJE.DebitAmt = decACProductCost '* ExchangeRate 'Removed by chintan as average cost is always going to same whenever we place order in any other currency
                            objJE.CreditAmt = 0
                            objJE.ChartAcntId = lngItemCOGsAccount
                            objJE.Description = "Cost of Goods Sold" '"Authoritative Accounts"
                            objJE.CustomerId = lngDivisionID
                            objJE.MainDeposit = False
                            objJE.MainCheck = False
                            objJE.MainCashCredit = False
                            objJE.OppitemtCode = CLng(dvOppBiDocItems.Item(i).Item("numoppitemtCode"))
                            objJE.BizDocItems = "COG"
                            objJE.Reference = ""
                            objJE.PaymentMethod = 0
                            objJE.Reconcile = False
                            objJE.CurrencyID = CurrencyID
                            objJE.FltExchangeRate = ExchangeRate
                            objJE.TaxItemID = 0
                            objJE.BizDocsPaymentDetailsId = 0
                            objJE.ContactID = 0
                            objJE.ItemID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("ItemCode"))
                            objJE.ProjectID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numProjectID"))
                            objJE.ClassID = CCommon.ToLong(dvOppBiDocItems.Item(i).Item("numClassID"))
                            objJE.CommissionID = 0
                            objJE.ReconcileID = 0
                            objJE.Cleared = False
                            objJE.ReferenceType = 0
                            objJE.ReferenceID = 0

                            objJEList.Add(objJE)
                        End If

                        
                    End If
                Next

                'OpportunityKitItems
                Dim ds As New DataSet
                Dim objOppBizDocs As OppBizDocs = New OppBizDocs
                objOppBizDocs.OppId = lngOppID
                objOppBizDocs.OppBizDocId = lngBizDocID
                Dim dtOppKitItems As DataTable = objOppBizDocs.GetOpportunityBizDocKitItemsForAuthorizativeAccounting().Tables(0)
                If dtOppKitItems.Rows.Count > 0 Then
                    For i = 0 To dtOppKitItems.Rows.Count - 1
                        lngItemAssetAccount = CCommon.ToLong(dtOppKitItems.Rows(i).Item("ItemInventoryAsset"))

                        If CCommon.ToLong(dtOppKitItems.Rows(i).Item("ItemCoGs")) > 0 Then
                            lngItemCOGsAccount = CCommon.ToLong(dtOppKitItems.Rows(i).Item("ItemCoGs"))
                        Else
                            lngItemCOGsAccount = ChartOfAccounting.GetDefaultAccount("CG", lngDomainID)
                        End If

                        lngItemExpenseAccount = CCommon.ToLong(dtOppKitItems.Rows(i).Item("itemExpenseAccount"))

                        If CStr(dtOppKitItems.Rows(i).Item("Type")) = "P" And CDec(dtOppKitItems.Rows(i).Item("ShippedAverageCost")) > 0 And CBool(dtOppKitItems.Rows(i).Item("bitDropShip")) <> True Then
                            decACProductCost = CDec(dtOppKitItems.Rows(i).Item("unit")) * CDec(dtOppKitItems.Rows(i).Item("ShippedAverageCost"))
                            If decACProductCost > 0 Then
                                If lngItemAssetAccount = 0 Then
                                    Throw New Exception("Asset account does not exist(s).")
                                    Exit Sub
                                End If

                                If lngItemCOGsAccount = 0 Then
                                    Throw New Exception("Cost Of Goods sold account does not exist(s).")
                                    Exit Sub
                                End If


                                objJE = New JournalEntryNew()

                                objJE.TransactionId = 0
                                objJE.DebitAmt = 0
                                objJE.CreditAmt = decACProductCost '* ExchangeRate 'Removed by chintan as average cost is always going to same whenever we place order in any other currency
                                objJE.ChartAcntId = lngItemAssetAccount
                                objJE.Description = "Items Shipped - Kit/Assembly" '"Authoritative Accounts"
                                objJE.CustomerId = lngDivisionID
                                objJE.MainDeposit = False
                                objJE.MainCheck = False
                                objJE.MainCashCredit = False
                                objJE.OppitemtCode = CLng(dtOppKitItems.Rows(i).Item("numoppitemtCode"))
                                objJE.BizDocItems = "IA"
                                objJE.Reference = ""
                                objJE.PaymentMethod = 0
                                objJE.Reconcile = False
                                objJE.CurrencyID = CurrencyID
                                objJE.FltExchangeRate = ExchangeRate
                                objJE.TaxItemID = 0
                                objJE.BizDocsPaymentDetailsId = 0
                                objJE.ContactID = 0
                                objJE.ItemID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("ItemCode"))
                                objJE.ProjectID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("numProjectID"))
                                objJE.ClassID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("numClassID"))
                                objJE.CommissionID = 0
                                objJE.ReconcileID = 0
                                objJE.Cleared = False
                                objJE.ReferenceType = 0
                                objJE.ReferenceID = 0

                                objJEList.Add(objJE)


                                objJE = New JournalEntryNew()

                                objJE.TransactionId = 0
                                objJE.DebitAmt = decACProductCost '* ExchangeRate 'Removed by chintan as average cost is always going to same whenever we place order in any other currency
                                objJE.CreditAmt = 0
                                objJE.ChartAcntId = lngItemCOGsAccount
                                objJE.Description = "Cost of Goods Sold - Kit/Assembly" '"Authoritative Accounts"
                                objJE.CustomerId = lngDivisionID
                                objJE.MainDeposit = False
                                objJE.MainCheck = False
                                objJE.MainCashCredit = False
                                objJE.OppitemtCode = CLng(dtOppKitItems.Rows(i).Item("numoppitemtCode"))
                                objJE.BizDocItems = "COG"
                                objJE.Reference = ""
                                objJE.PaymentMethod = 0
                                objJE.Reconcile = False
                                objJE.CurrencyID = CurrencyID
                                objJE.FltExchangeRate = ExchangeRate
                                objJE.TaxItemID = 0
                                objJE.BizDocsPaymentDetailsId = 0
                                objJE.ContactID = 0
                                objJE.ItemID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("ItemCode"))
                                objJE.ProjectID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("numProjectID"))
                                objJE.ClassID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("numClassID"))
                                objJE.CommissionID = 0
                                objJE.ReconcileID = 0
                                objJE.Cleared = False
                                objJE.ReferenceType = 0
                                objJE.ReferenceID = 0

                                objJEList.Add(objJE)
                            End If
                        ElseIf CStr(dtOppKitItems.Rows(i).Item("Type")) = "S" AndAlso CCommon.ToBool(dtOppKitItems.Rows(i).Item("bitExpenseItem")) Then
                            decACProductCost = CDec(dtOppKitItems.Rows(i).Item("unit")) * CDec(dtOppKitItems.Rows(i).Item("ShippedAverageCost"))
                            If decACProductCost > 0 Then
                                If lngItemExpenseAccount = 0 Then
                                    Throw New Exception("Expense account does not exist(s).")
                                    Exit Sub
                                End If

                                objJE = New JournalEntryNew()
                                objJE.TransactionId = 0
                                objJE.DebitAmt = decACProductCost
                                objJE.CreditAmt = 0 '* ExchangeRate 'Removed by chintan as average cost is always going to same whenever we place order in any other currency
                                objJE.ChartAcntId = lngItemAssetAccount
                                objJE.Description = "Inventoriable Cost"
                                objJE.CustomerId = lngDivisionID
                                objJE.MainDeposit = False
                                objJE.MainCheck = False
                                objJE.MainCashCredit = False
                                objJE.OppitemtCode = CLng(dtOppKitItems.Rows(i).Item("numoppitemtCode"))
                                objJE.BizDocItems = "IA"
                                objJE.Reference = ""
                                objJE.PaymentMethod = 0
                                objJE.Reconcile = False
                                objJE.CurrencyID = CurrencyID
                                objJE.FltExchangeRate = ExchangeRate
                                objJE.TaxItemID = 0
                                objJE.BizDocsPaymentDetailsId = 0
                                objJE.ContactID = 0
                                objJE.ItemID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("ItemCode"))
                                objJE.ProjectID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("numProjectID"))
                                objJE.ClassID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("numClassID"))
                                objJE.CommissionID = 0
                                objJE.ReconcileID = 0
                                objJE.Cleared = False
                                objJE.ReferenceType = 0
                                objJE.ReferenceID = 0

                                objJEList.Add(objJE)

                                objJE = New JournalEntryNew()
                                objJE.TransactionId = 0
                                objJE.DebitAmt = 0
                                objJE.CreditAmt = decACProductCost
                                objJE.ChartAcntId = lngItemExpenseAccount
                                objJE.Description = "Inventoriable Cost"
                                objJE.CustomerId = lngDivisionID
                                objJE.MainDeposit = False
                                objJE.MainCheck = False
                                objJE.MainCashCredit = False
                                objJE.OppitemtCode = CLng(dtOppKitItems.Rows(i).Item("numoppitemtCode"))
                                objJE.BizDocItems = "IE"
                                objJE.Reference = ""
                                objJE.PaymentMethod = 0
                                objJE.Reconcile = False
                                objJE.CurrencyID = CurrencyID
                                objJE.FltExchangeRate = ExchangeRate
                                objJE.TaxItemID = 0
                                objJE.BizDocsPaymentDetailsId = 0
                                objJE.ContactID = 0
                                objJE.ItemID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("ItemCode"))
                                objJE.ProjectID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("numProjectID"))
                                objJE.ClassID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("numClassID"))
                                objJE.CommissionID = 0
                                objJE.ReconcileID = 0
                                objJE.Cleared = False
                                objJE.ReferenceType = 0
                                objJE.ReferenceID = 0

                                objJEList.Add(objJE)


                                objJE = New JournalEntryNew()
                                objJE.TransactionId = 0
                                objJE.DebitAmt = 0
                                objJE.CreditAmt = decACProductCost '* ExchangeRate 'Removed by chintan as average cost is always going to same whenever we place order in any other currency
                                objJE.ChartAcntId = lngItemAssetAccount
                                objJE.Description = "Item Shipped" '"Authoritative Accounts"
                                objJE.CustomerId = lngDivisionID
                                objJE.MainDeposit = False
                                objJE.MainCheck = False
                                objJE.MainCashCredit = False
                                objJE.OppitemtCode = CLng(dtOppKitItems.Rows(i).Item("numoppitemtCode"))
                                objJE.BizDocItems = "IA - Kit/Assembly"
                                objJE.Reference = ""
                                objJE.PaymentMethod = 0
                                objJE.Reconcile = False
                                objJE.CurrencyID = CurrencyID
                                objJE.FltExchangeRate = ExchangeRate
                                objJE.TaxItemID = 0
                                objJE.BizDocsPaymentDetailsId = 0
                                objJE.ContactID = 0
                                objJE.ItemID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("ItemCode"))
                                objJE.ProjectID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("numProjectID"))
                                objJE.ClassID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("numClassID"))
                                objJE.CommissionID = 0
                                objJE.ReconcileID = 0
                                objJE.Cleared = False
                                objJE.ReferenceType = 0
                                objJE.ReferenceID = 0

                                objJEList.Add(objJE)


                                objJE = New JournalEntryNew()

                                objJE.TransactionId = 0
                                objJE.DebitAmt = decACProductCost '* ExchangeRate 'Removed by chintan as average cost is always going to same whenever we place order in any other currency
                                objJE.CreditAmt = 0
                                objJE.ChartAcntId = lngItemCOGsAccount
                                objJE.Description = "Cost of Goods Sold - Kit/Assembly" '"Authoritative Accounts"
                                objJE.CustomerId = lngDivisionID
                                objJE.MainDeposit = False
                                objJE.MainCheck = False
                                objJE.MainCashCredit = False
                                objJE.OppitemtCode = CLng(dtOppKitItems.Rows(i).Item("numoppitemtCode"))
                                objJE.BizDocItems = "COG"
                                objJE.Reference = ""
                                objJE.PaymentMethod = 0
                                objJE.Reconcile = False
                                objJE.CurrencyID = CurrencyID
                                objJE.FltExchangeRate = ExchangeRate
                                objJE.TaxItemID = 0
                                objJE.BizDocsPaymentDetailsId = 0
                                objJE.ContactID = 0
                                objJE.ItemID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("ItemCode"))
                                objJE.ProjectID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("numProjectID"))
                                objJE.ClassID = CCommon.ToLong(dtOppKitItems.Rows(i).Item("numClassID"))
                                objJE.CommissionID = 0
                                objJE.ReconcileID = 0
                                objJE.Cleared = False
                                objJE.ReferenceType = 0
                                objJE.ReferenceID = 0

                                objJEList.Add(objJE)
                            End If
                        End If
                    Next
                End If

                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, lngDomainID)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        ''' <summary>
        ''' This methods create journal entries for deferred income bizdoc 
        ''' which will debit Deferred Income account 
        ''' instead of A/R account which will happen when invoice journal entry is made.
        ''' User have to enable Deferred Income from Global Settings -> Accounting after that they can add differed income bizdoc
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub SaveJournalEntriesDeferredIncome(ByVal lngOppID As Long, ByVal lngDomainID As Long, ByVal dtOppBiDocItems As DataTable, ByVal lngJournalId As Long, ByVal lngDivisionID As Long, ByVal lngBizDocID As Long, ByVal CurrencyID As Long, ByVal ExchangeRate As Double, ByVal TotalAmount As Decimal)
            Try
                If ExchangeRate = 0 Then
                    Throw New Exception("EXCHANGE_RATE_CAN_NOT_BE_0")
                    Exit Sub
                End If

                Dim objJE As New JournalEntryNew()
                Dim objJEList As New JournalEntryCollection
                Dim lngARAccountId, lngDeferredIncomeAccount As Long

                Dim objOppBizDocs As New Opportunities.OppBizDocs
                objOppBizDocs.OppBizDocId = lngBizDocID
                lngARAccountId = CCommon.ToLong(objOppBizDocs.GetBizDocARAccountID())

                If lngARAccountId = 0 Then
                    lngARAccountId = objOppBizDocs.ValidateCustomerAR_APAccounts("AR", lngDomainID, lngDivisionID)
                End If

                If lngARAccountId = 0 Then
                    Throw New Exception("DEFAULT_A/R_ACCOUNT_NOT_AVAILABLE")
                    Exit Sub
                End If

                lngDeferredIncomeAccount = ChartOfAccounting.GetDefaultAccount("DI", lngDomainID)
                If lngDeferredIncomeAccount = 0 Then
                    Throw New Exception("DEFAULT_DEFERRED_INCOME_ACCOUNT_NOT_AVAILABLE")
                    Exit Sub
                End If


                Dim decAmount, decDebitAmt, decCreditAmt As Decimal

                decAmount = CDec(TotalAmount * ExchangeRate)
                decDebitAmt = 0
                decCreditAmt = 0
                If decAmount > 0.0 Then
                    decDebitAmt = Math.Abs(decAmount)
                    decCreditAmt = 0
                ElseIf decAmount < 0.0 Then
                    decDebitAmt = 0
                    decCreditAmt = Math.Abs(decAmount)
                End If

                'FOR ACCOUNT RECEIVABLE AS DEBIT ENTRY IN JOURNAL
                objJE = New JournalEntryNew()
                objJE.TransactionId = 0
                objJE.DebitAmt = decDebitAmt
                objJE.CreditAmt = decCreditAmt
                objJE.ChartAcntId = lngARAccountId
                objJE.Description = "Sales"
                objJE.CustomerId = lngDivisionID
                objJE.MainDeposit = False
                objJE.MainCheck = False
                objJE.MainCashCredit = False
                objJE.OppitemtCode = 0
                objJE.BizDocItems = "AR"
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = CurrencyID
                objJE.FltExchangeRate = ExchangeRate
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = False
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0
                objJEList.Add(objJE)

                'FOR DEFERRED INCOME AS CREDIT ENTRY IN JOURNAL
                objJE = New JournalEntryNew()
                objJE.TransactionId = 0
                objJE.DebitAmt = decCreditAmt 'JUST REVERESED THE VARIABLE FROM AVOABE ENTRY decCreditAmt TO DebitAmt AND decDebitAmt TO CreditAmt
                objJE.CreditAmt = decDebitAmt
                objJE.ChartAcntId = lngDeferredIncomeAccount
                objJE.Description = "Sales - Deferred Income"
                objJE.CustomerId = lngDivisionID
                objJE.MainDeposit = False
                objJE.MainCheck = False
                objJE.MainCashCredit = False
                objJE.OppitemtCode = 0
                objJE.BizDocItems = "DI"
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = CurrencyID
                objJE.FltExchangeRate = ExchangeRate
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = False
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0
                objJEList.Add(objJE)

                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, lngDomainID)
            Catch ex As Exception
                Throw
            End Try
        End Sub
    End Class
End Namespace
