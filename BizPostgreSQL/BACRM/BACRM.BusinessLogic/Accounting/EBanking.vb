﻿Option Explicit On
Option Strict On

Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Collections.Generic
Imports System.Xml
Imports System.Xml.Serialization
Imports nsoftware.InEBank
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting
Imports System.IO

Namespace BACRM.BusinessLogic.Accounting

    Public Class EBanking

#Region "Members"

        Delegate Sub UpdateBankAccountBalance(ByVal dr As DataRow)

#End Region

#Region "Properties"


        Private _DomainID As Long
        Public Property DomainID() As Long
            Get
                Return _DomainID
            End Get
            Set(ByVal value As Long)
                _DomainID = value
            End Set
        End Property



        Private _BankMasterID As Long
        Public Property BankMasterID() As Long
            Get
                Return _BankMasterID
            End Get
            Set(ByVal value As Long)
                _BankMasterID = value
            End Set
        End Property


        Private _BankType As Byte
        Public Property BankType() As Byte
            Get
                Return _BankType
            End Get
            Set(ByVal value As Byte)
                _BankType = value
            End Set
        End Property


        Private _FIId As String
        Public Property FIId() As String
            Get
                Return _FIId
            End Get
            Set(ByVal value As String)
                _FIId = value
            End Set
        End Property


        Private _FIName As String
        Public Property FIName() As String
            Get
                Return _FIName
            End Get
            Set(ByVal value As String)
                _FIName = value
            End Set
        End Property


        Private _FIOrganization As String
        Public Property FIOrganization() As String
            Get
                Return _FIOrganization
            End Get
            Set(ByVal value As String)
                _FIOrganization = value
            End Set
        End Property


        Private _FIOFXServerUrl As String
        Public Property FIOFXServerUrl() As String
            Get
                Return _FIOFXServerUrl
            End Get
            Set(ByVal value As String)
                _FIOFXServerUrl = value
            End Set
        End Property


        Private _BankID As String
        Public Property BankID() As String
            Get
                Return _BankID
            End Get
            Set(ByVal value As String)
                _BankID = value
            End Set
        End Property


        Private _OFXAccessKey As String
        Public Property OFXAccessKey() As String
            Get
                Return _OFXAccessKey
            End Get
            Set(ByVal value As String)
                _OFXAccessKey = value
            End Set
        End Property


        Private _DtPaymentDueDate As DateTime
        Public Property DtPaymentDueDate() As DateTime
            Get
                Return _DtPaymentDueDate
            End Get
            Set(ByVal value As DateTime)
                _DtPaymentDueDate = value
            End Set
        End Property

        Private _DtCreatedDate As DateTime
        Public Property DtCreatedDate() As DateTime
            Get
                Return _DtCreatedDate
            End Get
            Set(ByVal value As DateTime)
                _DtCreatedDate = value
            End Set
        End Property


        Private _DtModifiedDate As DateTime
        Public Property DtModifiedDate() As DateTime
            Get
                Return _DtModifiedDate
            End Get
            Set(ByVal value As DateTime)
                _DtModifiedDate = value
            End Set
        End Property


        Private _BankPhone As String
        Public Property BankPhone() As String
            Get
                Return _BankPhone
            End Get
            Set(ByVal value As String)
                _BankPhone = value
            End Set
        End Property



        Private _BankWebsite As String
        Public Property BankWebsite() As String
            Get
                Return _BankWebsite
            End Get
            Set(ByVal value As String)
                _BankWebsite = value
            End Set
        End Property


        Private _UserIdLabel As String
        Public Property UserIdLabel() As String
            Get
                Return _UserIdLabel
            End Get
            Set(ByVal value As String)
                _UserIdLabel = value
            End Set
        End Property


        Private _BankDetailID As Long
        Public Property BankDetailID() As Long
            Get
                Return _BankDetailID
            End Get
            Set(ByVal value As Long)
                _BankDetailID = value
            End Set
        End Property


        Private _UserContactID As Long
        Public Property UserContactID() As Long
            Get
                Return _UserContactID
            End Get
            Set(ByVal value As Long)
                _UserContactID = value
            End Set
        End Property


        Private _AccountID As Long
        Public Property AccountID() As Long
            Get
                Return _AccountID
            End Get
            Set(ByVal value As Long)
                _AccountID = value
            End Set
        End Property


        Private _AccountNumber As String
        Public Property AccountNumber() As String
            Get
                Return _AccountNumber
            End Get
            Set(ByVal value As String)
                _AccountNumber = value
            End Set
        End Property


        Private _AccountType As String
        Public Property AccountType() As String
            Get
                Return _AccountType
            End Get
            Set(ByVal value As String)
                _AccountType = value
            End Set
        End Property


        Private _UserName As String
        Public Property UserName() As String
            Get
                Return _UserName
            End Get
            Set(ByVal value As String)
                _UserName = value
            End Set
        End Property


        Private _Password As String
        Public Property Password() As String
            Get
                Return _Password
            End Get
            Set(ByVal value As String)
                _Password = value
            End Set
        End Property


        Private _StatementID As Long
        Public Property StatementID() As Long
            Get
                Return _StatementID
            End Get
            Set(ByVal value As Long)
                _StatementID = value
            End Set
        End Property


        Private _FIStatementID As String
        Public Property FIStatementID() As String
            Get
                Return _FIStatementID
            End Get
            Set(ByVal value As String)
                _FIStatementID = value
            End Set
        End Property


        Private _AvailableBalance As Decimal
        Public Property AvailableBalance() As Decimal
            Get
                Return _AvailableBalance
            End Get
            Set(ByVal value As Decimal)
                _AvailableBalance = value
            End Set
        End Property


        Private _DtAvailableBalanceDate As DateTime
        Public Property DtAvailableBalanceDate() As DateTime
            Get
                Return _DtAvailableBalanceDate
            End Get
            Set(ByVal value As DateTime)
                _DtAvailableBalanceDate = value
            End Set
        End Property


        Private _CurrencyID As Long
        Public Property CurrencyID() As Long
            Get
                Return _CurrencyID
            End Get
            Set(ByVal value As Long)
                _CurrencyID = value
            End Set
        End Property


        Private _LedgerBalance As Decimal
        Public Property LedgerBalance() As Decimal
            Get
                Return _LedgerBalance
            End Get
            Set(ByVal value As Decimal)
                _LedgerBalance = value
            End Set
        End Property


        Private _DtLedgerBalanceDate As DateTime
        Public Property DtLedgerBalanceDate() As DateTime
            Get
                Return _DtLedgerBalanceDate
            End Get
            Set(ByVal value As DateTime)
                _DtLedgerBalanceDate = value
            End Set
        End Property


        Private _DtStartDate As DateTime
        Public Property DtStartDate() As DateTime
            Get
                Return _DtStartDate
            End Get
            Set(ByVal value As DateTime)
                _DtStartDate = value
            End Set
        End Property


        Private _DtEndDate As DateTime
        Public Property DtEndDate() As DateTime
            Get
                Return _DtEndDate
            End Get
            Set(ByVal value As DateTime)
                _DtEndDate = value
            End Set
        End Property



        Private _TransMappingId As Long
        Public Property TransMappingId() As Long
            Get
                Return _TransMappingId
            End Get
            Set(ByVal value As Long)
                _TransMappingId = value
            End Set
        End Property


        Private _TransactionID As Long
        Public Property TransactionID() As Long
            Get
                Return _TransactionID
            End Get
            Set(ByVal value As Long)
                _TransactionID = value
            End Set
        End Property


        Private _Amount As Decimal
        Public Property Amount() As Decimal
            Get
                Return _Amount
            End Get
            Set(ByVal value As Decimal)
                _Amount = value
            End Set
        End Property


        Private _CheckNumber As String
        Public Property CheckNumber() As String
            Get
                Return _CheckNumber
            End Get
            Set(ByVal value As String)
                _CheckNumber = value
            End Set
        End Property


        Private _DtDatePosted As DateTime
        Public Property DtDatePosted() As DateTime
            Get
                Return _DtDatePosted
            End Get
            Set(ByVal value As DateTime)
                _DtDatePosted = value
            End Set
        End Property


        Private _FITransactionID As String
        Public Property FITransactionID() As String
            Get
                Return _FITransactionID
            End Get
            Set(ByVal value As String)
                _FITransactionID = value
            End Set
        End Property


        Private _PayeeName As String
        Public Property PayeeName() As String
            Get
                Return _PayeeName
            End Get
            Set(ByVal value As String)
                _PayeeName = value
            End Set
        End Property


        Private _Memo As String
        Public Property Memo() As String
            Get
                Return _Memo
            End Get
            Set(ByVal value As String)
                _Memo = value
            End Set
        End Property


        Private _TxType As String
        Public Property TxType() As String
            Get
                Return _TxType
            End Get
            Set(ByVal value As String)
                _TxType = value
            End Set
        End Property

        Private _tintMode As Integer
        Public Property TintMode() As Integer
            Get
                Return _tintMode
            End Get
            Set(ByVal value As Integer)
                _tintMode = value
            End Set
        End Property

        Private _IsActive As Short

        Public Property IsActive() As Short
            Get
                Return _IsActive
            End Get
            Set(ByVal value As Short)
                _IsActive = value
            End Set
        End Property

        Private _ReferenceType As Integer
        Public Property ReferenceType() As Integer
            Get
                Return _ReferenceType
            End Get
            Set(ByVal value As Integer)
                _ReferenceType = value
            End Set
        End Property

        Private _ReferenceID As Long
        Public Property ReferenceID() As Long
            Get
                Return _ReferenceID
            End Get
            Set(ByVal value As Long)
                _ReferenceID = value
            End Set
        End Property





        Private _PayeeDetailId As Long
        <XmlElement("numPayeeDetailId")>
        Public Property PayeeDetailId() As Long
            Get
                Return _PayeeDetailId
            End Get
            Set(ByVal value As Long)
                _PayeeDetailId = value
            End Set
        End Property


        Private _PayeeFIId As String
        <XmlElement("vcPayeeFIId")>
        Public Property PayeeFIId() As String
            Get
                Return _PayeeFIId
            End Get
            Set(ByVal value As String)
                _PayeeFIId = value
            End Set
        End Property


        Private _PayeeFIListID As String
        <XmlElement("vcPayeeFIListID")>
        Public Property PayeeFIListID() As String
            Get
                Return _PayeeFIListID
            End Get
            Set(ByVal value As String)
                _PayeeFIListID = value
            End Set
        End Property




        Private _Account As String
        <XmlElement("vcAccount")>
        Public Property Account() As String
            Get
                Return _Account
            End Get
            Set(ByVal value As String)
                _Account = value
            End Set
        End Property


        Private _Address1 As String
        <XmlElement("vcAddress1")>
        Public Property Address1() As String
            Get
                Return _Address1
            End Get
            Set(ByVal value As String)
                _Address1 = value
            End Set
        End Property


        Private _Address2 As String
        <XmlElement("vcAddress2")>
        Public Property Address2() As String
            Get
                Return _Address2
            End Get
            Set(ByVal value As String)
                _Address2 = value
            End Set
        End Property


        Private _Address3 As String
        <XmlElement("vcAddress3")>
        Public Property Address3() As String
            Get
                Return _Address3
            End Get
            Set(ByVal value As String)
                _Address3 = value
            End Set
        End Property


        Private _City As String
        <XmlElement("vcCity")>
        Public Property City() As String
            Get
                Return _City
            End Get
            Set(ByVal value As String)
                _City = value
            End Set
        End Property


        Private _State As String
        <XmlElement("vcState")>
        Public Property State() As String
            Get
                Return _State
            End Get
            Set(ByVal value As String)
                _State = value
            End Set
        End Property


        Private _PostalCode As String
        <XmlElement("vcPostalCode")>
        Public Property PostalCode() As String
            Get
                Return _PostalCode
            End Get
            Set(ByVal value As String)
                _PostalCode = value
            End Set
        End Property

        Private _PayeePhone As String
        <XmlElement("vcPhone")>
        Public Property PayeePhone() As String
            Get
                Return _PayeePhone
            End Get
            Set(ByVal value As String)
                _PayeePhone = value
            End Set
        End Property

        Private _Country As String
        <XmlElement("vcCountry")>
        Public Property Country() As String
            Get
                Return _Country
            End Get
            Set(ByVal value As String)
                _Country = value
            End Set
        End Property


        Private _CreatedDate As DateTime
        <XmlElement("dtCreatedDate")>
        Public Property CreatedDate() As DateTime
            Get
                Return _CreatedDate
            End Get
            Set(ByVal value As DateTime)
                _CreatedDate = value
            End Set
        End Property


        Private _ModifiedDate As DateTime
        <XmlElement("dtModifiedDate")>
        Public Property ModifiedDate() As DateTime
            Get
                Return _ModifiedDate
            End Get
            Set(ByVal value As DateTime)
                _ModifiedDate = value
            End Set
        End Property

        Private _CurrentPage As Integer

        Public Property CurrentPage() As Integer
            Get
                Return _CurrentPage
            End Get
            Set(ByVal value As Integer)
                _CurrentPage = value
            End Set
        End Property

        Private _PageSize As Integer

        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal value As Integer)
                _PageSize = value
            End Set
        End Property

        Private _TotRecs As Integer

        Public Property TotRecs() As Integer
            Get
                Return _TotRecs
            End Get
            Set(ByVal value As Integer)
                _TotRecs = value
            End Set
        End Property

        Private _JournalUpdateId As Long
        Public Property JournalUpdateId() As Long
            Get
                Return _JournalUpdateId
            End Get
            Set(ByVal value As Long)
                _JournalUpdateId = value
            End Set
        End Property

        Private _BitCheck As Short
        Public Property BitCheck() As Short
            Get
                Return _BitCheck
            End Get
            Set(ByVal value As Short)
                _BitCheck = value
            End Set
        End Property

        Private _BitDeposit As Short
        Public Property BitDeposit() As Short
            Get
                Return _BitDeposit
            End Get
            Set(ByVal value As Short)
                _BitDeposit = value
            End Set
        End Property


#End Region

#Region "Methods"


        Function ManageOnlineBillPayeeDetail() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numPayeeDetailId", _PayeeDetailId, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@vcPayeeFIId", _PayeeFIId, NpgsqlTypes.NpgsqlDbType.Char, 10))

                    .Add(SqlDAL.Add_Parameter("@vcPayeeFIListID", _PayeeFIListID, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcPayeeName", _PayeeName, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcAccount", _Account, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcAddress1", _Address1, NpgsqlTypes.NpgsqlDbType.VarChar, 350))

                    .Add(SqlDAL.Add_Parameter("@vcAddress2", _Address2, NpgsqlTypes.NpgsqlDbType.VarChar, 350))

                    .Add(SqlDAL.Add_Parameter("@vcAddress3", _Address3, NpgsqlTypes.NpgsqlDbType.VarChar, 350))

                    .Add(SqlDAL.Add_Parameter("@vcCity", _City, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcState", _State, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcPostalCode", _PostalCode, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcPhone", _PayeePhone, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcCountry", _Country, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@numBankDetailId", _BankDetailID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With
                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_ManageOnlineBillPayeeDetail", sqlParams.ToArray()))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function DeleteOnlineBillPayeeDetail() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numBankDetailId", _BankDetailID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numPayeeDetailId", _PayeeDetailId, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With
                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteOnlineBillPayeeDetail", sqlParams.ToArray())

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GetOnlineBillPayeeDetail() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numPayeeDetailId", _PayeeDetailId, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetOnlineBillPayeeDetail", sqlParams.ToArray())
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetBankMasterList() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(0).Value = Nothing
                arParms(0).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetBankMasterList", arParms).Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Function GetBankDetails() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@tintMode", _tintMode, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numBankDetailID", _BankDetailID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numAccountID", _AccountID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", _DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With
                ds = SqlDAL.ExecuteDataset(connString, "usp_GetBankDetail", sqlParams.ToArray())
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GetBankMaster() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numBankMasterID", _BankMasterID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetBankMaster", sqlParams.ToArray())
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GetBankDetailsList() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@tintMode", _tintMode, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", _DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numUserContactID", _UserContactID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numBankMasterID", _BankMasterID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@vcUserName", _UserName, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcPassword", _Password, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetBankDetailsList", sqlParams.ToArray())
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GetUserBankAccounts() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numUserContactID", _UserContactID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetUserBankAccounts", sqlParams.ToArray())
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GetConnectedCOABankAccounts() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@tintMode", _tintMode, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numUserContactID", _UserContactID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetConnectedCOABankAccounts", sqlParams.ToArray())
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Function GetOnlineBillPayeeList() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numBankDetailId", _BankDetailID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetOnlineBillPayeeList", sqlParams.ToArray())
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ManageBankDetails() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numBankDetailID", _BankDetailID, NpgsqlTypes.NpgsqlDbType.BigInt)) ', 9, ParameterDirection.InputOutput))


                    .Add(SqlDAL.Add_Parameter("@numBankMasterID", _BankMasterID, NpgsqlTypes.NpgsqlDbType.BigInt))


                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))


                    .Add(SqlDAL.Add_Parameter("@numUserContactID", _UserContactID, NpgsqlTypes.NpgsqlDbType.BigInt))


                    .Add(SqlDAL.Add_Parameter("@numAccountID", _AccountID, NpgsqlTypes.NpgsqlDbType.BigInt))


                    .Add(SqlDAL.Add_Parameter("@vcBankID", _BankID, NpgsqlTypes.NpgsqlDbType.VarChar, 50))


                    .Add(SqlDAL.Add_Parameter("@vcAccountNumber", _AccountNumber, NpgsqlTypes.NpgsqlDbType.VarChar, 50))


                    .Add(SqlDAL.Add_Parameter("@vcAccountType", _AccountType, NpgsqlTypes.NpgsqlDbType.VarChar, 50))


                    .Add(SqlDAL.Add_Parameter("@vcUserName", _UserName, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcPassword", _Password, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@bitIsActive", _IsActive, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object = sqlParams.ToArray()
                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_ManageBankDetail", objParam))


            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function DeleteBankDetails() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numBankDetailID", _BankDetailID, NpgsqlTypes.NpgsqlDbType.BigInt)) ', 9, ParameterDirection.InputOutput))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With
                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteBankDetails", sqlParams.ToArray())

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function DeleteBankDetailConnection() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numBankDetailID", _BankDetailID, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With
                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteBankDetailConnection", sqlParams.ToArray())

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GetBankStatementHeader() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numStatementID", _StatementID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetBankStatementHeader", sqlParams.ToArray())
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GetAllTransactionsOfCOAId() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numAccountID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _AccountID

                arParms(1) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _CurrentPage

                arParms(2) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _PageSize

                arParms(3) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Direction = ParameterDirection.InputOutput
                arParms(3).Value = _TotRecs

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object
                objParam = arParms.ToArray()
                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetAllTransactionsOfCOAId", objParam, True)
                _TotRecs = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(3).Value)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GetMatchedTransactionsOfCOAId() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numAccountID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _AccountID

                arParms(1) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _CurrentPage

                arParms(2) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _PageSize

                arParms(3) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Direction = ParameterDirection.InputOutput
                arParms(3).Value = _TotRecs

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object
                objParam = arParms.ToArray()
                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetMatchedTransactionsOfCOAId", objParam, True)
                _TotRecs = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(3).Value)

                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GetTransactionDetails() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numTransactionID", _TransactionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetTransactionDetails", sqlParams.ToArray())
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ManageBankStatementHeader() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams


                    .Add(SqlDAL.Add_Parameter("@numStatementID", _StatementID, NpgsqlTypes.NpgsqlDbType.BigInt)) ', 9, ParameterDirection.InputOutput))


                    .Add(SqlDAL.Add_Parameter("@numBankDetailID", _BankDetailID, NpgsqlTypes.NpgsqlDbType.BigInt))


                    .Add(SqlDAL.Add_Parameter("@vcFIStatementID", _FIStatementID, NpgsqlTypes.NpgsqlDbType.VarChar, 50))


                    .Add(SqlDAL.Add_Parameter("@monAvailableBalance", _AvailableBalance, NpgsqlTypes.NpgsqlDbType.Numeric))


                    .Add(SqlDAL.Add_Parameter("@dtAvailableBalanceDate", _DtAvailableBalanceDate, NpgsqlTypes.NpgsqlDbType.Timestamp))


                    .Add(SqlDAL.Add_Parameter("@numCurrencyID", _CurrencyID, NpgsqlTypes.NpgsqlDbType.BigInt))


                    .Add(SqlDAL.Add_Parameter("@monLedgerBalance", _LedgerBalance, NpgsqlTypes.NpgsqlDbType.Numeric))


                    .Add(SqlDAL.Add_Parameter("@dtLedgerBalanceDate", _DtLedgerBalanceDate, NpgsqlTypes.NpgsqlDbType.Timestamp))


                    .Add(SqlDAL.Add_Parameter("@dtStartDate", _DtStartDate, NpgsqlTypes.NpgsqlDbType.Timestamp))


                    .Add(SqlDAL.Add_Parameter("@dtEndDate", _DtEndDate, NpgsqlTypes.NpgsqlDbType.Timestamp))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object = sqlParams.ToArray()
                Return CLng(SqlDAL.ExecuteScalar(connString, "usp_ManageBankStatementHeader", objParam))


            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function DeleteBankStatementHeader() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numStatementID", _StatementID, NpgsqlTypes.NpgsqlDbType.BigInt)) ', 9, ParameterDirection.InputOutput))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With
                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteBankStatementHeader", sqlParams.ToArray())

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GetBankStatementTransactions() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numTransactionID", _TransactionID, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetBankStatementTransactions", sqlParams.ToArray())
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ManageBankStatementTransactions() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams


                    .Add(SqlDAL.Add_Parameter("@numTransactionID", _TransactionID, NpgsqlTypes.NpgsqlDbType.BigInt)) ', 9, ParameterDirection.InputOutput))


                    .Add(SqlDAL.Add_Parameter("@numStatementID", _StatementID, NpgsqlTypes.NpgsqlDbType.BigInt))


                    .Add(SqlDAL.Add_Parameter("@monAmount", _Amount, NpgsqlTypes.NpgsqlDbType.Numeric))


                    .Add(SqlDAL.Add_Parameter("@vcCheckNumber", _CheckNumber, NpgsqlTypes.NpgsqlDbType.VarChar, 50))


                    .Add(SqlDAL.Add_Parameter("@dtDatePosted", _DtDatePosted, NpgsqlTypes.NpgsqlDbType.Timestamp))


                    .Add(SqlDAL.Add_Parameter("@vcFITransactionID", _FITransactionID, NpgsqlTypes.NpgsqlDbType.VarChar, 50))


                    .Add(SqlDAL.Add_Parameter("@vcPayeeName", _PayeeName, NpgsqlTypes.NpgsqlDbType.VarChar, 50))


                    .Add(SqlDAL.Add_Parameter("@vcMemo", _Memo, NpgsqlTypes.NpgsqlDbType.VarChar, 100))


                    .Add(SqlDAL.Add_Parameter("@vcTxType", _TxType, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@numAccountID", _AccountID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", _DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))


                End With
                SqlDAL.ExecuteNonQuery(connString, "usp_ManageBankStatementTransaction", sqlParams.ToArray())

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function MapBankStatementTransactions() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numTransMappingId", _TransMappingId, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numTransactionID", _TransactionID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@tintReferenceType", _ReferenceType, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@numReferenceID", _ReferenceID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@bitIsActive", _IsActive, NpgsqlTypes.NpgsqlDbType.Bit))

                End With
                SqlDAL.ExecuteNonQuery(connString, "USP_MapBankStatementTransactions", sqlParams.ToArray())

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function MapTransactionJournalUpdate() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@bitDeposit", _bitDeposit, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@bitCheck", _bitCheck, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@Id", _JournalUpdateId, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With
                SqlDAL.ExecuteNonQuery(connString, "USP_MapTransactionJournalUpdate", sqlParams.ToArray())

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function DeleteBankStatementTransactions() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numTransactionID", _TransactionID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With
                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteBankStatementTransactions", sqlParams.ToArray())

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ExcludeBankStatementTransactions() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numTransactionID", _TransactionID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With
                SqlDAL.ExecuteNonQuery(connString, "USP_ExcludeBankStatementTransactions", sqlParams.ToArray())

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function UnMatchBankStatementTransactions() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numTransactionID", _TransactionID, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With
                SqlDAL.ExecuteNonQuery(connString, "USP_UnMatchBankStatementTransactions", sqlParams.ToArray())

                Return True

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Sub GetCreditCardStatement(ByVal dr As DataRow)
            Try
                Dim objCcStatement1 As New Ccstatement
                Dim objCommon As New CCommon
                objCcStatement1.OFXAppId = "QWIN"
                objCcStatement1.OFXAppVersion = "1800"
                objCcStatement1.FIUrl = dr("vcFIOFXServerUrl").ToString
                objCcStatement1.FIId = dr("vcFIId").ToString
                objCcStatement1.FIOrganization = dr("vcFIOrganization").ToString
                objCcStatement1.CardNumber = dr("vcAccountNumber").ToString
                objCcStatement1.OFXUser = objCommon.Decrypt(CCommon.ToString(dr("vcUserName")))
                objCcStatement1.OFXPassword = objCommon.Decrypt(CCommon.ToString(dr("vcPassword")))

                If CCommon.ToString(dr("dtCreatedDate")) <> "" Then
                    objCcStatement1.StartDate = Convert.ToDateTime(CCommon.ToString(dr("dtCreatedDate")), System.Globalization.CultureInfo.CreateSpecificCulture("En-US")).ToString
                Else
                    objCcStatement1.StartDate = Convert.ToDateTime(DateTime.Now.AddDays(-180).ToString, System.Globalization.CultureInfo.CreateSpecificCulture("En-US")).ToString
                End If

                objCcStatement1.EndDate = Convert.ToDateTime(DateTime.Now.ToString(), System.Globalization.CultureInfo.CreateSpecificCulture("En-US")).ToString

                'If CCommon.ToString(dr("dtCreatedDate")) <> "" Then
                '    objCcStatement1.StartDate = CCommon.ToString(dr("dtCreatedDate"))
                'Else
                '    objCcStatement1.StartDate = DateTime.Now.AddDays(-30).ToString
                'End If

                'objCcStatement1.EndDate = DateTime.Now.ToString()

                objCcStatement1.GetStatement()
                Dim objEbank As New EBanking
                objEbank.StatementID = 0
                objEbank.BankDetailID = CCommon.ToLong(dr("numBankDetailID"))
                objEbank.DomainID = CCommon.ToLong(dr("numDomainID"))
                objEbank.FIStatementID = ""
                objEbank.LedgerBalance = CCommon.ToDecimal(objCcStatement1.LedgerBalance)
                objEbank.DtLedgerBalanceDate = Convert.ToDateTime(objCcStatement1.LedgerBalanceDate)

                objEbank.AvailableBalance = CCommon.ToDecimal(objCcStatement1.AvailableBalance)
                objEbank.DtAvailableBalanceDate = Convert.ToDateTime(objCcStatement1.AvailableBalanceDate)
                objEbank.DtStartDate = Convert.ToDateTime(objCcStatement1.StartDate)
                objEbank.DtEndDate = Convert.ToDateTime(objCcStatement1.EndDate)
                objCommon.DomainID = CCommon.ToLong(dr("numDomainID")) 'Session("DomainID")
                objCommon.Mode = 15
                objCommon.Str = "USD"
                objEbank.CurrencyID = CCommon.ToLong(objCommon.GetSingleFieldValue())

                Dim StatementId As Long = objEbank.ManageBankStatementHeader()
                objEbank.StatementID = StatementId

                For i As Integer = 0 To objCcStatement1.Transactions.Count - 1
                    objEbank.TransactionID = 0
                    objEbank.Amount = CCommon.ToDecimal(objCcStatement1.Transactions(i).Amount)
                    objEbank.CheckNumber = objCcStatement1.Transactions(i).CheckNumber
                    objEbank.DtDatePosted = Convert.ToDateTime(objCcStatement1.Transactions(i).DatePosted, System.Globalization.CultureInfo.CreateSpecificCulture("En-US")) 'Convert.ToDateTime(objCcStatement1.Transactions(i).DatePosted)
                    objEbank.FITransactionID = objCcStatement1.Transactions(i).FITID
                    objEbank.PayeeName = objCcStatement1.Transactions(i).PayeeName
                    objEbank.Memo = objCcStatement1.Transactions(i).Memo
                    objEbank.TxType = objCcStatement1.Transactions(i).TxType.ToString
                    objEbank.AccountID = CCommon.ToLong(dr("numAccountID"))
                    objEbank.ManageBankStatementTransactions()

                Next

            Catch ex As Exception
                WriteMessage(ex.Message)
                WriteMessage(ex.StackTrace)

            End Try

        End Sub

        Public Sub GetBankStatement(ByVal dr As DataRow)
            Try

                Dim objBankStatement1 As New Bankstatement
                'objBankStatement1.Config("OFXDateFormat=yyyyMMddHHmmss.mmm")
                Dim objCommon As New CCommon
                objBankStatement1.OFXAppId = "QWIN"
                objBankStatement1.OFXAppVersion = "1800"
                objBankStatement1.FIUrl = dr("vcFIOFXServerUrl").ToString
                objBankStatement1.FIId = dr("vcFIId").ToString
                objBankStatement1.FIOrganization = dr("vcFIOrganization").ToString
                objBankStatement1.AccountId = dr("vcAccountNumber").ToString
                objBankStatement1.AccountType = DirectCast(CCommon.ToInteger(dr("vcAccountType")), BankstatementAccountTypes)
                objBankStatement1.BankId = dr("vcBankID").ToString
                objBankStatement1.OFXUser = objCommon.Decrypt(CCommon.ToString(dr("vcUserName")))
                objBankStatement1.OFXPassword = objCommon.Decrypt(CCommon.ToString(dr("vcPassword")))

                Dim strDateFormat As String = objBankStatement1.Config("DateFormat= dd/MM/yyyy hh:mm:ss")
                Dim strOFXDateFormat As String = objBankStatement1.Config("OFXDateFormat= dd/MM/yyyy hh:mm:ss")

                strDateFormat = objBankStatement1.Config("DateFormat")
                strOFXDateFormat = objBankStatement1.Config("OFXDateFormat")


                If CCommon.ToString(dr("dtCreatedDate")) <> "" Then
                    objBankStatement1.StartDate = Convert.ToDateTime(CCommon.ToString(dr("dtCreatedDate"))).ToString
                Else
                    objBankStatement1.StartDate = Convert.ToDateTime(DateTime.Now.AddDays(-180).ToString).ToString
                End If

                objBankStatement1.EndDate = DateTime.Now.ToString


                'If CCommon.ToString(dr("dtCreatedDate")) <> "" Then
                '    objBankStatement1.StartDate = Convert.ToDateTime(CCommon.ToString(dr("dtCreatedDate")), Globalization.CultureInfo.CreateSpecificCulture("En-US")).ToString("dd/MM/yyyy hh:mm:ss")
                'Else
                '    objBankStatement1.StartDate = Convert.ToDateTime(DateTime.Now.AddDays(-180).ToString, Globalization.CultureInfo.CreateSpecificCulture("En-US")).ToString("dd/MM/yyyy hh:mm:ss")
                'End If

                'objBankStatement1.EndDate = Convert.ToDateTime(DateTime.Now.ToString(), Globalization.CultureInfo.CreateSpecificCulture("En-US")).ToString("dd/MM/yyyy hh:mm:ss")

                'If CCommon.ToString(dr("dtCreatedDate")) <> "" Then
                '    objBankStatement1.StartDate = Convert.ToDateTime(CCommon.ToString(dr("dtCreatedDate")), Globalization.CultureInfo.CreateSpecificCulture("En-US")).ToString("yyyyMMddHHmmss.mmm")
                'Else
                '    objBankStatement1.StartDate = Convert.ToDateTime(DateTime.Now.AddDays(-180).ToString, Globalization.CultureInfo.CreateSpecificCulture("En-US")).ToString("yyyyMMddHHmmss.mmm")
                'End If

                'objBankStatement1.EndDate = DateTime.Now.ToString("yyyyMMddHHmmss.mmm")

                objBankStatement1.GetStatement()
                Dim objEbank As New EBanking
                objEbank.StatementID = 0
                objEbank.BankDetailID = CCommon.ToLong(dr("numBankDetailID"))
                objEbank.FIStatementID = ""
                objEbank.LedgerBalance = CCommon.ToDecimal(objBankStatement1.LedgerBalance)
                objEbank.DomainID = CCommon.ToLong(dr("numDomainID"))
                Try
                    If Not IsNothing(objBankStatement1.LedgerBalanceDate) Then
                        objEbank.DtLedgerBalanceDate = Convert.ToDateTime(objBankStatement1.LedgerBalanceDate, System.Globalization.CultureInfo.CreateSpecificCulture("En-US"))

                        'objEbank.DtLedgerBalanceDate = Convert.ToDateTime(objBankStatement1.LedgerBalanceDate)
                    Else
                        objEbank.DtLedgerBalanceDate = DateTime.Now

                    End If


                Catch ex As Exception
                    objEbank.DtLedgerBalanceDate = DateTime.Now
                End Try

                objEbank.AvailableBalance = CCommon.ToDecimal(objBankStatement1.AvailableBalance)

                Try
                    If Not objBankStatement1.AvailableBalanceDate = Nothing Then
                        objEbank.DtAvailableBalanceDate = Convert.ToDateTime(objBankStatement1.AvailableBalanceDate)
                    Else
                        objEbank.DtAvailableBalanceDate = DateTime.Now

                    End If

                Catch ex As Exception
                    objEbank.DtAvailableBalanceDate = DateTime.Now
                End Try

                objEbank.DtStartDate = Convert.ToDateTime(objBankStatement1.StartDate)
                objEbank.DtEndDate = Convert.ToDateTime(objBankStatement1.EndDate)
                objCommon.DomainID = CCommon.ToLong(dr("numDomainID"))
                objCommon.Mode = 15
                objCommon.Str = "USD"
                objEbank.CurrencyID = CCommon.ToLong(objCommon.GetSingleFieldValue())

                Dim StatementId As Long = objEbank.ManageBankStatementHeader()
                objEbank.StatementID = StatementId

                For i As Integer = 0 To objBankStatement1.Transactions.Count - 1
                    objEbank.TransactionID = 0
                    objEbank.Amount = CCommon.ToDecimal(objBankStatement1.Transactions(i).Amount)
                    objEbank.CheckNumber = objBankStatement1.Transactions(i).CheckNumber
                    objEbank.DtDatePosted = Convert.ToDateTime(objBankStatement1.Transactions(i).DatePosted, System.Globalization.CultureInfo.CreateSpecificCulture("En-US"))
                    objEbank.FITransactionID = objBankStatement1.Transactions(i).FITID
                    objEbank.PayeeName = objBankStatement1.Transactions(i).PayeeName
                    objEbank.Memo = objBankStatement1.Transactions(i).Memo
                    objEbank.TxType = objBankStatement1.Transactions(i).TxType.ToString
                    objEbank.AccountID = CCommon.ToLong(dr("numAccountID"))
                    objEbank.ManageBankStatementTransactions()
                Next
                objBankStatement1.Dispose()
                objBankStatement1 = Nothing

            Catch ex As Exception
                WriteMessage(ex.Message)
                WriteMessage(ex.StackTrace)
                
            End Try

        End Sub

        Public Sub AsyncGetCreditCardStatement(ByVal dr As DataRow)
            Try

                Dim AsyncCall As UpdateBankAccountBalance = AddressOf GetCreditCardStatement
                AsyncCall.BeginInvoke(dr, Nothing, New Object())

            Catch ex As Exception
                Throw ex

            End Try
        End Sub

        Public Sub AsyncGetBankStatement(ByVal dr As DataRow)
            Try
                Dim AsyncCall As UpdateBankAccountBalance = AddressOf GetBankStatement
                AsyncCall.BeginInvoke(dr, Nothing, New Object())
            Catch ex As Exception
                Throw ex

            End Try

        End Sub

        Public Sub PayBill()
            Dim objBillpayment As New nsoftware.InEBank.Billpayment
            Dim dtPayee As DataTable
            Dim dt As DataTable
            Dim objCommon As New CCommon

            Try
                dt = GetBankDetails()

                If dt.Rows.Count > 0 Then
                    Dim dRow As DataRow = dt.Rows(0)
                    objBillpayment.OFXAppId = "QWIN"
                    objBillpayment.OFXAppVersion = "1700"
                    objBillpayment.FIUrl = dRow("vcFIOFXServerUrl").ToString
                    objBillpayment.FIId = dRow("vcFIId").ToString
                    objBillpayment.FIOrganization = dRow("vcFIOrganization").ToString
                    objBillpayment.OFXUser = objCommon.Decrypt(dRow("vcUserName").ToString)
                    objBillpayment.OFXPassword = objCommon.Decrypt(dRow("vcPassword").ToString)
                    objBillpayment.Payment.FromBankId = dRow("vcBankID").ToString
                    objBillpayment.Payment.FromAccountId = dRow("vcAccountNumber").ToString
                    Dim AccountTyp As AccountTypes = DirectCast(System.[Enum].Parse(GetType(AccountTypes), dRow("vcAccountType").ToString), AccountTypes)

                    objBillpayment.Payment.FromAccountType = AccountTyp
                    objBillpayment.Payment.Amount = CCommon.ToString(Amount)
                    objBillpayment.Payment.Memo = Memo
                    objBillpayment.Payment.DateDue = CCommon.ToString(DtPaymentDueDate)

                    dtPayee = GetOnlineBillPayeeDetail()

                    If dtPayee.Rows.Count > 0 Then
                        Dim dRowPayee As DataRow = dtPayee.Rows(0)
                        objBillpayment.Payee.Name = dRowPayee("vcPayeeName").ToString
                        objBillpayment.Payee.Account = dRowPayee("vcAccount").ToString
                        objBillpayment.Payee.ListId = dRowPayee("vcPayeeFIListID").ToString
                        objBillpayment.Payee.Addr1 = dRowPayee("vcAddress1").ToString
                        objBillpayment.Payee.Addr2 = dRowPayee("vcAddress2").ToString
                        objBillpayment.Payee.Addr3 = dRowPayee("vcAddress3").ToString
                        objBillpayment.Payee.City = dRowPayee("vcCity").ToString
                        objBillpayment.Payee.State = dRowPayee("vcState").ToString
                        objBillpayment.Payee.Country = dRowPayee("vcCountry").ToString
                        objBillpayment.Payee.PostalCode = dRowPayee("vcPostalCode").ToString
                        objBillpayment.Payee.Phone = dRowPayee("vcPhone").ToString
                        objBillpayment.PayBill()

                    End If

                End If


            Catch ex As Exception
                Throw ex

            End Try


        End Sub

        Public Function GetAccountTypeID(ByVal AccountType As String) As String
            Dim AccountTypeId As Integer = 0
            Select Case AccountType.Replace(" ", "").ToUpper

                Case "CHECKING"
                    Return "0"

                Case "SAVINGS"
                    Return "1"

                Case "MONEYMARKET"
                    Return "2"

                Case "LINEOFCREDIT"
                    Return "3"

                Case "CREDITCARD"
                    Return "4"

            End Select

        End Function

#End Region

        Private Sub WriteMessage(ByVal Message As String)
            Try
                Dim DirectoryPath As String = CCommon.GetDocumentPhysicalPath(_DomainID) & "TransactionLogs/"
                If Not Directory.Exists(DirectoryPath) Then
                    Directory.CreateDirectory(DirectoryPath)
                End If
                WebAPI.WebAPI.WriteMessage(Message, DirectoryPath)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class

End Namespace