﻿Option Explicit On
Option Strict On

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRM.BusinessLogic.Common
Imports System.Web
Imports System.IO
Imports System.Xml.Serialization
Imports System.Xml
Imports System.Collections.Generic
Imports Npgsql

Namespace BACRM.BusinessLogic.Accounting
    Public Class JournalEntryHeader
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _JournalId As Long
        Public Property JournalId() As Long
            Get
                Return _JournalId
            End Get
            Set(ByVal value As Long)
                _JournalId = value
            End Set
        End Property


        Private _RecurringId As Long
        Public Property RecurringId() As Long
            Get
                Return _RecurringId
            End Get
            Set(ByVal value As Long)
                _RecurringId = value
            End Set
        End Property


        Private _EntryDate As Date
        Public Property EntryDate() As Date
            Get
                Return _EntryDate
            End Get
            Set(ByVal value As Date)
                _EntryDate = value
            End Set
        End Property


        Private _Description As String
        Public Property Description() As String
            Get
                Return _Description
            End Get
            Set(ByVal value As String)
                _Description = value
            End Set
        End Property


        Private _Amount As Decimal
        Public Property Amount() As Decimal
            Get
                Return _Amount
            End Get
            Set(ByVal value As Decimal)
                _Amount = value
            End Set
        End Property



        Private _CheckId As Long
        Public Property CheckId() As Long
            Get
                Return _CheckId
            End Get
            Set(ByVal value As Long)
                _CheckId = value
            End Set
        End Property


        Private _CashCreditCardId As Long
        Public Property CashCreditCardId() As Long
            Get
                Return _CashCreditCardId
            End Get
            Set(ByVal value As Long)
                _CashCreditCardId = value
            End Set
        End Property


        Private _ChartAcntId As Long
        Public Property ChartAcntId() As Long
            Get
                Return _ChartAcntId
            End Get
            Set(ByVal value As Long)
                _ChartAcntId = value
            End Set
        End Property


        Private _OppId As Long
        Public Property OppId() As Long
            Get
                Return _OppId
            End Get
            Set(ByVal value As Long)
                _OppId = value
            End Set
        End Property


        Private _OppBizDocsId As Long
        Public Property OppBizDocsId() As Long
            Get
                Return _OppBizDocsId
            End Get
            Set(ByVal value As Long)
                _OppBizDocsId = value
            End Set
        End Property


        Private _DepositId As Long
        Public Property DepositId() As Long
            Get
                Return _DepositId
            End Get
            Set(ByVal value As Long)
                _DepositId = value
            End Set
        End Property


        Private _BizDocsPaymentDetId As Long
        Public Property BizDocsPaymentDetId() As Long
            Get
                Return _BizDocsPaymentDetId
            End Get
            Set(ByVal value As Long)
                _BizDocsPaymentDetId = value
            End Set
        End Property


        Private _IsOpeningBalance As Boolean
        Public Property IsOpeningBalance() As Boolean
            Get
                Return _IsOpeningBalance
            End Get
            Set(ByVal value As Boolean)
                _IsOpeningBalance = value
            End Set
        End Property


        Private _LastRecurringDate As DateTime
        Public Property LastRecurringDate() As DateTime
            Get
                Return _LastRecurringDate
            End Get
            Set(ByVal value As DateTime)
                _LastRecurringDate = value
            End Set
        End Property


        Private _NoTransactions As Long
        Public Property NoTransactions() As Long
            Get
                Return _NoTransactions
            End Get
            Set(ByVal value As Long)
                _NoTransactions = value
            End Set
        End Property

        Private _CategoryHDRID As Long
        Public Property CategoryHDRID() As Long
            Get
                Return _CategoryHDRID
            End Get
            Set(ByVal value As Long)
                _CategoryHDRID = value
            End Set
        End Property

        Private _ReturnID As Long
        Public Property ReturnID() As Long
            Get
                Return _ReturnID
            End Get
            Set(ByVal value As Long)
                _ReturnID = value
            End Set
        End Property

        Private _CheckHeaderID As Long
        <XmlElement("numCheckHeaderID")>
        Public Property CheckHeaderID() As Long
            Get
                Return _CheckHeaderID
            End Get
            Set(ByVal value As Long)
                _CheckHeaderID = value
            End Set
        End Property


        Private _BillID As Long
        <XmlElement("numBillID")>
        Public Property BillID() As Long
            Get
                Return _BillID
            End Get
            Set(ByVal value As Long)
                _BillID = value
            End Set
        End Property


        Private _BillPaymentID As Long
        <XmlElement("numBillPaymentID")>
        Public Property BillPaymentID() As Long
            Get
                Return _BillPaymentID
            End Get
            Set(ByVal value As Long)
                _BillPaymentID = value
            End Set
        End Property

        Private _ReferenceType As Short
        Public Property ReferenceType() As Short
            Get
                Return _ReferenceType
            End Get
            Set(ByVal value As Short)
                _ReferenceType = value
            End Set
        End Property


        Private _ReferenceID As Long
        Public Property ReferenceID() As Long
            Get
                Return _ReferenceID
            End Get
            Set(ByVal value As Long)
                _ReferenceID = value
            End Set
        End Property


        Private _ReconcileID As Long
        Public Property ReconcileID() As Long
            Get
                Return _ReconcileID
            End Get
            Set(ByVal value As Long)
                _ReconcileID = value
            End Set
        End Property

        Private _JournalReferenceNo As Long
        Public Property JournalReferenceNo() As Long
            Get
                Return _JournalReferenceNo
            End Get
            Set(ByVal value As Long)
                _JournalReferenceNo = value
            End Set
        End Property

        Private _PayrollHeaderID As Long
        Public Property PayrollHeaderID() As Long
            Get
                Return _PayrollHeaderID
            End Get
            Set(ByVal value As Long)
                _PayrollHeaderID = value
            End Set
        End Property

        Public Property IsReconcileInterest As Boolean

        Function Save() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams


                    .Add(SqlDAL.Add_Parameter("@numJournal_Id", _JournalId, NpgsqlTypes.NpgsqlDbType.BigInt, 9, ParameterDirection.InputOutput))


                    .Add(SqlDAL.Add_Parameter("@numRecurringId", _RecurringId, NpgsqlTypes.NpgsqlDbType.BigInt))


                    .Add(SqlDAL.Add_Parameter("@datEntry_Date", _EntryDate, NpgsqlTypes.NpgsqlDbType.Timestamp))


                    .Add(SqlDAL.Add_Parameter("@vcDescription", _Description, NpgsqlTypes.NpgsqlDbType.VarChar, 1000))


                    .Add(SqlDAL.Add_Parameter("@numAmount", _Amount, NpgsqlTypes.NpgsqlDbType.Numeric))


                    .Add(SqlDAL.Add_Parameter("@numCheckId", _CheckId, NpgsqlTypes.NpgsqlDbType.BigInt))


                    .Add(SqlDAL.Add_Parameter("@numCashCreditCardId", _CashCreditCardId, NpgsqlTypes.NpgsqlDbType.BigInt))


                    .Add(SqlDAL.Add_Parameter("@numChartAcntId", _ChartAcntId, NpgsqlTypes.NpgsqlDbType.BigInt))


                    .Add(SqlDAL.Add_Parameter("@numOppId", _OppId, NpgsqlTypes.NpgsqlDbType.BigInt))


                    .Add(SqlDAL.Add_Parameter("@numOppBizDocsId", _OppBizDocsId, NpgsqlTypes.NpgsqlDbType.BigInt))


                    .Add(SqlDAL.Add_Parameter("@numDepositId", _DepositId, NpgsqlTypes.NpgsqlDbType.BigInt))


                    .Add(SqlDAL.Add_Parameter("@numBizDocsPaymentDetId", _BizDocsPaymentDetId, NpgsqlTypes.NpgsqlDbType.BigInt))


                    .Add(SqlDAL.Add_Parameter("@bitOpeningBalance", _IsOpeningBalance, NpgsqlTypes.NpgsqlDbType.Bit))


                    .Add(SqlDAL.Add_Parameter("@numCategoryHDRID", _CategoryHDRID, NpgsqlTypes.NpgsqlDbType.BigInt))


                    .Add(SqlDAL.Add_Parameter("@numReturnID", _ReturnID, NpgsqlTypes.NpgsqlDbType.BigInt))


                    .Add(SqlDAL.Add_Parameter("@numCheckHeaderID", _CheckHeaderID, NpgsqlTypes.NpgsqlDbType.BigInt))


                    .Add(SqlDAL.Add_Parameter("@numBillID", _BillID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numBillPaymentID", _BillPaymentID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numReconcileID", _ReconcileID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numJournalReferenceNo", _JournalReferenceNo, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numPayrollDetailID", _PayrollHeaderID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@bitReconcileInterest", IsReconcileInterest, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With
                Dim objParam() As Object
                objParam = sqlParams.ToArray()

                SqlDAL.ExecuteNonQuery(connString, "USP_InsertJournalEntryHeader", objParam, True)

                _JournalId = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

                Return _JournalId
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Function Save(ByVal objTransaction As NpgsqlTransaction) As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(23) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numJournal_Id", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _JournalId

                arParms(1) = New Npgsql.NpgsqlParameter("@numRecurringId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _RecurringId

                arParms(2) = New Npgsql.NpgsqlParameter("@datEntry_Date", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _EntryDate

                arParms(3) = New Npgsql.NpgsqlParameter("@vcDescription", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(3).Value = _Description

                arParms(4) = New Npgsql.NpgsqlParameter("@numAmount", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(4).Value = _Amount

                arParms(5) = New Npgsql.NpgsqlParameter("@numCheckId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _CheckId

                arParms(6) = New Npgsql.NpgsqlParameter("@numCashCreditCardId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _CashCreditCardId

                arParms(7) = New Npgsql.NpgsqlParameter("@numChartAcntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _ChartAcntId

                arParms(8) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = _OppId

                arParms(9) = New Npgsql.NpgsqlParameter("@numOppBizDocsId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = _OppBizDocsId

                arParms(10) = New Npgsql.NpgsqlParameter("@numDepositId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(10).Value = _DepositId

                arParms(11) = New Npgsql.NpgsqlParameter("@numBizDocsPaymentDetId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(11).Value = _BizDocsPaymentDetId

                arParms(12) = New Npgsql.NpgsqlParameter("@bitOpeningBalance", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(12).Value = _IsOpeningBalance

                arParms(13) = New Npgsql.NpgsqlParameter("@numCategoryHDRID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(13).Value = _CategoryHDRID

                arParms(14) = New Npgsql.NpgsqlParameter("@numReturnID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(14).Value = _ReturnID

                arParms(15) = New Npgsql.NpgsqlParameter("@numCheckHeaderID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(15).Value = _CheckHeaderID

                arParms(16) = New Npgsql.NpgsqlParameter("@numBillID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(16).Value = _BillID

                arParms(17) = New Npgsql.NpgsqlParameter("@numBillPaymentID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(17).Value = _BillPaymentID

                arParms(18) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(18).Value = DomainID

                arParms(19) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(19).Value = UserCntID

                arParms(20) = New Npgsql.NpgsqlParameter("@numReconcileID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(20).Value = _ReconcileID

                arParms(21) = New Npgsql.NpgsqlParameter("@numJournalReferenceNo", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(21).Value = _JournalReferenceNo

                arParms(22) = New Npgsql.NpgsqlParameter("@numPayrollDetailID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(22).Value = _PayrollHeaderID

                arParms(23) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(23).Value = Nothing
                arParms(23).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object
                objParam = arParms.ToArray()

                If objTransaction IsNot Nothing Then
                    SqlDAL.ExecuteNonQuery(objTransaction, "USP_InsertJournalEntryHeader", objParam, True)
                Else
                    SqlDAL.ExecuteNonQuery(connString, "USP_InsertJournalEntryHeader", objParam, True)
                End If

                _JournalId = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

                Return _JournalId
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Function GetAccountTransactionInfo() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@tintReferenceType", _ReferenceType, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@numReferenceID", _ReferenceID, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetAccountTransactionInfo", sqlParams.ToArray())

                Return ds.Tables(0)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function CheckDuplicateRefIDForJournal() As Long
            Dim lngResult As Long = 0
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numJournalReferenceNo", _JournalReferenceNo, NpgsqlTypes.NpgsqlDbType.BigInt, 18))
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint, 18))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_CheckDuplicateRefIDForJournal", sqlParams.ToArray())
                If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    lngResult = CCommon.ToLong(ds.Tables(0).Rows(0)("RowCount"))
                End If

            Catch ex As Exception
                Throw ex
            End Try

            Return lngResult
        End Function

        Public Function BeginTransaction() As NpgsqlTransaction
            Try
                Dim getconnection As New BACRMBUSSLOGIC.BussinessLogic.GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim conn As New Npgsql.NpgsqlConnection(connString)
                conn.Open()
                Dim objTransaction As NpgsqlTransaction = conn.BeginTransaction(IsolationLevel.Serializable)
                Return objTransaction
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Sub CommitTransaction(objTransaction As NpgsqlTransaction)
            Try
                If Not objTransaction Is Nothing Then
                    objTransaction.Commit()
                    If Not objTransaction.Connection Is Nothing Then
                        If objTransaction.Connection.State = ConnectionState.Open Then
                            objTransaction.Connection.Close()
                        End If
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Sub RollbackTransaction(objTransaction As NpgsqlTransaction)
            Try
                If Not objTransaction Is Nothing Then
                    objTransaction.Rollback()
                    If Not objTransaction.Connection Is Nothing Then
                        If objTransaction.Connection.State = ConnectionState.Open Then
                            objTransaction.Connection.Close()
                        End If
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class

    Public Class JournalEntryCollection

        Enum JournalMode
            InsertOnly = 0
            DeleteUpdateInsert = 1
        End Enum
        Private JournalEntryList As List(Of JournalEntryNew)

        Public Sub Add(ByVal ObjJournalEntry As JournalEntryNew)
            If JournalEntryList Is Nothing Then JournalEntryList = New List(Of JournalEntryNew)
            JournalEntryList.Add(ObjJournalEntry)
        End Sub



        Public Sub Save(ByVal Mode As JournalMode, JournalId As Long, lngDomainID As Long)
            Try
                Dim strXML As String = ""

                If Not JournalEntryList Is Nothing Then
                    strXML = CCommon.SerializeToXML(JournalEntryList)
                End If

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numJournalId", JournalId, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@strRow", strXML, NpgsqlTypes.NpgsqlDbType.Text))

                    .Add(SqlDAL.Add_Parameter("@Mode", Convert.ToInt16(Mode), NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@numDomainId", lngDomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_SaveJournalDetails", sqlParams.ToArray())

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub Save(ByVal Mode As JournalMode, JournalId As Long, lngDomainID As Long, ByVal objTransaction As NpgsqlTransaction)
            Try
                Dim strXML As String = ""

                If Not JournalEntryList Is Nothing Then
                    strXML = CCommon.SerializeToXML(JournalEntryList)
                End If

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numJournalId", JournalId, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@strRow", strXML, NpgsqlTypes.NpgsqlDbType.Text))

                    .Add(SqlDAL.Add_Parameter("@Mode", Convert.ToInt16(Mode), NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@numDomainId", lngDomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With

                If Not objTransaction Is Nothing Then
                    SqlDAL.ExecuteNonQuery(objTransaction, "USP_SaveJournalDetails", sqlParams.ToArray())
                Else
                    SqlDAL.ExecuteNonQuery(connString, "USP_SaveJournalDetails", sqlParams.ToArray())
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub



    End Class

    Public Class JournalEntryNew
        'Inherits BACRM.BusinessLogic.CBusinessBase
        'Private Sub New(lngJournalId As Long, lngDomainID As Long)
        '    Me.JournalId = lngJournalId
        '    Me.DomainID = lngDomainID
        'End Sub

        Private _TransactionId As Long
        <XmlElement("numTransactionId")>
        Public Property TransactionId() As Long
            Get
                Return _TransactionId
            End Get
            Set(ByVal value As Long)
                _TransactionId = value
            End Set
        End Property

        Private _DebitAmt As Decimal
        <XmlElement("numDebitAmt")>
        Public Property DebitAmt() As Decimal
            Get
                Return _DebitAmt
            End Get
            Set(ByVal value As Decimal)
                _DebitAmt = value
            End Set
        End Property


        Private _CreditAmt As Decimal
        <XmlElement("numCreditAmt")>
        Public Property CreditAmt() As Decimal
            Get
                Return _CreditAmt
            End Get
            Set(ByVal value As Decimal)
                _CreditAmt = value
            End Set
        End Property


        Private _ChartAcntId As Long
        <XmlElement("numChartAcntId")>
        Public Property ChartAcntId() As Long
            Get
                Return _ChartAcntId
            End Get
            Set(ByVal value As Long)
                _ChartAcntId = value
            End Set
        End Property


        Private _Description As String
        <XmlElement("varDescription")>
        Public Property Description() As String
            Get
                Return _Description
            End Get
            Set(ByVal value As String)
                _Description = value
            End Set
        End Property


        Private _CustomerId As Long
        <XmlElement("numCustomerId")>
        Public Property CustomerId() As Long
            Get
                Return _CustomerId
            End Get
            Set(ByVal value As Long)
                _CustomerId = value
            End Set
        End Property

        Private _MainDeposit As Boolean
        <XmlElement("bitMainDeposit")>
        Public Property MainDeposit() As Boolean
            Get
                Return _MainDeposit
            End Get
            Set(ByVal value As Boolean)
                _MainDeposit = value
            End Set
        End Property


        Private _MainCheck As Boolean
        <XmlElement("bitMainCheck")>
        Public Property MainCheck() As Boolean
            Get
                Return _MainCheck
            End Get
            Set(ByVal value As Boolean)
                _MainCheck = value
            End Set
        End Property


        Private _MainCashCredit As Boolean
        <XmlElement("bitMainCashCredit")>
        Public Property MainCashCredit() As Boolean
            Get
                Return _MainCashCredit
            End Get
            Set(ByVal value As Boolean)
                _MainCashCredit = value
            End Set
        End Property


        Private _OppitemtCode As Long
        <XmlElement("numoppitemtCode")>
        Public Property OppitemtCode() As Long
            Get
                Return _OppitemtCode
            End Get
            Set(ByVal value As Long)
                _OppitemtCode = value
            End Set
        End Property


        Private _BizDocItems As String
        <XmlElement("chBizDocItems")>
        Public Property BizDocItems() As String
            Get
                Return _BizDocItems
            End Get
            Set(ByVal value As String)
                _BizDocItems = value
            End Set
        End Property


        Private _Reference As String
        <XmlElement("vcReference")>
        Public Property Reference() As String
            Get
                Return _Reference
            End Get
            Set(ByVal value As String)
                _Reference = value
            End Set
        End Property


        Private _PaymentMethod As Long
        <XmlElement("numPaymentMethod")>
        Public Property PaymentMethod() As Long
            Get
                Return _PaymentMethod
            End Get
            Set(ByVal value As Long)
                _PaymentMethod = value
            End Set
        End Property


        Private _Reconcile As Boolean
        <XmlElement("bitReconcile")>
        Public Property Reconcile() As Boolean
            Get
                Return _Reconcile
            End Get
            Set(ByVal value As Boolean)
                _Reconcile = value
            End Set
        End Property


        Private _CurrencyID As Long
        <XmlElement("numCurrencyID")>
        Public Property CurrencyID() As Long
            Get
                Return _CurrencyID
            End Get
            Set(ByVal value As Long)
                _CurrencyID = value
            End Set
        End Property


        Private _FltExchangeRate As Double
        <XmlElement("fltExchangeRate")>
        Public Property FltExchangeRate() As Double
            Get
                Return _FltExchangeRate
            End Get
            Set(ByVal value As Double)
                _FltExchangeRate = value
            End Set
        End Property

        Private _TaxItemID As Long
        <XmlElement("numTaxItemID")>
        Public Property TaxItemID() As Long
            Get
                Return _TaxItemID
            End Get
            Set(ByVal value As Long)
                _TaxItemID = value
            End Set
        End Property


        Private _BizDocsPaymentDetailsId As Long
        <XmlElement("numBizDocsPaymentDetailsId")>
        Public Property BizDocsPaymentDetailsId() As Long
            Get
                Return _BizDocsPaymentDetailsId
            End Get
            Set(ByVal value As Long)
                _BizDocsPaymentDetailsId = value
            End Set
        End Property


        Private _ContactID As Long
        <XmlElement("numcontactid")>
        Public Property ContactID() As Long
            Get
                Return _ContactID
            End Get
            Set(ByVal value As Long)
                _ContactID = value
            End Set
        End Property


        Private _ItemID As Long
        <XmlElement("numItemID")>
        Public Property ItemID() As Long
            Get
                Return _ItemID
            End Get
            Set(ByVal value As Long)
                _ItemID = value
            End Set
        End Property


        Private _ProjectID As Long
        <XmlElement("numProjectID")>
        Public Property ProjectID() As Long
            Get
                Return _ProjectID
            End Get
            Set(ByVal value As Long)
                _ProjectID = value
            End Set
        End Property


        Private _ClassID As Long
        <XmlElement("numClassID")>
        Public Property ClassID() As Long
            Get
                Return _ClassID
            End Get
            Set(ByVal value As Long)
                _ClassID = value
            End Set
        End Property


        Private _CommissionID As Long
        <XmlElement("numCommissionID")>
        Public Property CommissionID() As Long
            Get
                Return _CommissionID
            End Get
            Set(ByVal value As Long)
                _CommissionID = value
            End Set
        End Property


        Private _ReconcileID As Long
        <XmlElement("numReconcileID")>
        Public Property ReconcileID() As Long
            Get
                Return _ReconcileID
            End Get
            Set(ByVal value As Long)
                _ReconcileID = value
            End Set
        End Property


        Private _Cleared As Boolean
        <XmlElement("bitCleared")>
        Public Property Cleared() As Boolean
            Get
                Return _Cleared
            End Get
            Set(ByVal value As Boolean)
                _Cleared = value
            End Set
        End Property

        Private _ReferenceType As Short
        <XmlElement("tintReferenceType")>
        Public Property ReferenceType() As Short
            Get
                Return _ReferenceType
            End Get
            Set(ByVal value As Short)
                _ReferenceType = value
            End Set
        End Property


        Private _ReferenceID As Long
        <XmlElement("numReferenceID")>
        Public Property ReferenceID() As Long
            Get
                Return _ReferenceID
            End Get
            Set(ByVal value As Long)
                _ReferenceID = value
            End Set
        End Property

        Private _CampaignID As Long
        <XmlElement("numCampaignID")>
        Public Property CampaignID() As Long
            Get
                Return _CampaignID
            End Get
            Set(ByVal value As Long)
                _CampaignID = value
            End Set
        End Property
    End Class
End Namespace
