Option Explicit On
Option Strict On

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Accounting
Public Class Budget
        Inherits BACRM.BusinessLogic.CBusinessBase
        '#Region "Constructor"
        '    '**********************************************************************************
        '    ' Name         : New
        '    ' Type         : Sub
        '    ' Scope        : Public
        '    ' Returns      : N/A
        '    ' Parameters   : ByVal intUserId As Int32               
        '    ' Description  : The constructor                
        '    ' Notes        : N/A                
        '    ' Created By   : Siva 	DATE:05-Sep-07
        '    '**********************************************************************************
        '    Public Sub New(ByVal intUserId As Integer)
        '        'Constructor
        '        MyBase.New(intUserId)
        '    End Sub

        '    '**********************************************************************************
        '    ' Name         : New
        '    ' Type         : Sub
        '    ' Scope        : Public
        '    ' Returns      : N/A
        '    ' Parameters   : ByVal intUserId As Int32              
        '    ' Description  : The constructor                
        '    ' Notes        : N/A                
        '    ' Created By   : Siva 	DATE:05-Sep-07
        '    '**********************************************************************************
        '    Public Sub New()
        '        'Constructor
        '        MyBase.New()
        '    End Sub
        '#End Region


        'Define Private Variable
        Private _BudgetId As Integer
        'Private DomainId As Integer
        Private _DepartmentId As Integer
        Private _DivisionId As Integer
        Private _CostCenterId As Integer
        Private _BudgetName As String
        Private _AcntIdDetails As String = String.Empty
        Private _BudgetDetails As String = String.Empty
        Private _ProcurementBudgetId As Integer
        Private _FiscalYear As Integer
        Private _PurchaseDeal As Boolean
        Private _ProcurementBudgetDetails As String = String.Empty
        Private _MarketBudgetId As Integer
        Private _MarketCampaignBit As Boolean
        Private _MarketBudgetDetails As String = String.Empty
        Private _ItemCode As Integer
        Private _MonthlyAmt As Decimal
        Private _TotalYearlyAmt As Decimal
        Private _bitPurchaseOpp As Boolean
        Private _sintByte As Int16
        Private _BudgetDetId As Integer
        Private _sintForecast As Int16
        Private _TotalBankBalance As Decimal
        Private _Type As Integer
        Private _Id As Integer
        Private _ByteMode As Int16

        Public Property BudgetId() As Integer
            Get
                Return _BudgetId
            End Get
            Set(ByVal value As Integer)
                _BudgetId = value
            End Set
        End Property


        ''Public Property DomainId() As Integer
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal value As Integer)
        '        DomainId = value
        '    End Set
        'End Property

        Public Property DepartmentId() As Integer
            Get
                Return _DepartmentId
            End Get
            Set(ByVal value As Integer)
                _DepartmentId = value
            End Set
        End Property

        Public Property DivisionId() As Integer
            Get
                Return _DivisionId
            End Get
            Set(ByVal value As Integer)
                _DivisionId = value
            End Set
        End Property
        Public Property CostCenterId() As Integer
            Get
                Return _CostCenterId
            End Get
            Set(ByVal value As Integer)
                _CostCenterId = value
            End Set
        End Property

        Public Property BudgetName() As String
            Get
                Return _BudgetName
            End Get
            Set(ByVal value As String)
                _BudgetName = value
            End Set
        End Property

        Public Property AcntIdDetails() As String
            Get
                Return _AcntIdDetails
            End Get
            Set(ByVal value As String)
                _AcntIdDetails = value
            End Set
        End Property

        'Public Property UserCntID() As Integer
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal value As Integer)
        '        UserCntID = value
        '    End Set
        'End Property



        Public Property BudgetDetails() As String
            Get
                Return _BudgetDetails
            End Get
            Set(ByVal value As String)
                _BudgetDetails = value
            End Set
        End Property

        Public Property ProcurementBudgetId() As Integer
            Get
                Return _ProcurementBudgetId
            End Get
            Set(ByVal value As Integer)
                _ProcurementBudgetId = value
            End Set
        End Property

        Public Property FiscalYear() As Integer
            Get
                Return _FiscalYear
            End Get
            Set(ByVal value As Integer)
                _FiscalYear = value
            End Set
        End Property

        Public Property PurchaseDeal() As Boolean
            Get
                Return _PurchaseDeal
            End Get
            Set(ByVal value As Boolean)
                _PurchaseDeal = value
            End Set
        End Property

        Public Property ProcurementBudgetDetails() As String
            Get
                Return _ProcurementBudgetDetails
            End Get
            Set(ByVal value As String)
                _ProcurementBudgetDetails = value
            End Set
        End Property

        Public Property MarketBudgetId() As Integer
            Get
                Return _MarketBudgetId
            End Get
            Set(ByVal value As Integer)
                _MarketBudgetId = value
            End Set
        End Property

        Public Property MarketCampaignBit() As Boolean
            Get
                Return _MarketCampaignBit
            End Get
            Set(ByVal value As Boolean)
                _MarketCampaignBit = value
            End Set
        End Property

        Public Property MarketBudgetDetails() As String
            Get
                Return _MarketBudgetDetails
            End Get
            Set(ByVal value As String)
                _MarketBudgetDetails = value
            End Set
        End Property

        Public Property ItemCode() As Integer
            Get
                Return _ItemCode
            End Get
            Set(ByVal value As Integer)
                _ItemCode = value
            End Set
        End Property

        Public Property MonthlyAmt() As Decimal
            Get
                Return _MonthlyAmt
            End Get
            Set(ByVal value As Decimal)
                _MonthlyAmt = value
            End Set
        End Property

        Public Property TotalYearlyAmt() As Decimal
            Get
                Return _TotalYearlyAmt
            End Get
            Set(ByVal value As Decimal)
                _TotalYearlyAmt = value
            End Set
        End Property


        Public Property bitPurchaseOpp() As Boolean
            Get
                Return _bitPurchaseOpp
            End Get
            Set(ByVal value As Boolean)
                _bitPurchaseOpp = value
            End Set
        End Property

        Public Property sintByte() As Int16
            Get
                Return _sintByte
            End Get
            Set(ByVal value As Int16)
                _sintByte = value
            End Set
        End Property

        Public Property BudgetDetId() As Integer
            Get
                Return _BudgetDetId
            End Get
            Set(ByVal value As Integer)
                _BudgetDetId = value
            End Set
        End Property

        Public Property sintForecast() As Int16
            Get
                Return _sintForecast
            End Get
            Set(ByVal value As Int16)
                _sintForecast = value
            End Set
        End Property

        Public Property TotalBankBalance() As Decimal
            Get
                Return _TotalBankBalance
            End Get
            Set(ByVal value As Decimal)
                _TotalBankBalance = value
            End Set
        End Property

        Public Property Type() As Integer
            Get
                Return _Type
            End Get
            Set(ByVal value As Integer)
                _Type = value
            End Set
        End Property

        Public Property Id() As Integer
            Get
                Return _Id
            End Get
            Set(ByVal value As Integer)
                _Id = value
            End Set
        End Property

        Public Property ByteMode() As Int16
            Get
                Return _ByteMode
            End Get
            Set(ByVal value As Int16)
                _ByteMode = value
            End Set
        End Property

        Public Function GetOperationBudget() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim ds As DataSet
                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = DomainID

                arparms(1) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Integer)
                arparms(1).Value = _Type

                arparms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(2).Value = Nothing
                arparms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetOperationBudget", arparms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetHierachyDetails() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@strRow", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _AcntIdDetails

                arParms(2) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _Type

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_HierachyGridForBudget", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveOperationBudget() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}
                Dim ds As DataSet
                Dim lngBudgetId As Long

                arParms(0) = New Npgsql.NpgsqlParameter("@numBudgetId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _BudgetId

                arParms(1) = New Npgsql.NpgsqlParameter("@vcBudgetName", NpgsqlTypes.NpgsqlDbType.Varchar, 100)
                arParms(1).Value = _BudgetName

                arParms(2) = New Npgsql.NpgsqlParameter("@numDepartmentId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Value = _DepartmentId

                arParms(3) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(3).Value = _DivisionId

                arParms(4) = New Npgsql.NpgsqlParameter("@numCostCenterId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(4).Value = _CostCenterId

                arParms(5) = New Npgsql.NpgsqlParameter("@intFiscalYear", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _FiscalYear

                arParms(6) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(6).Value = DomainID

                arParms(7) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(7).Value = UserCntID

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                lngBudgetId = CLng(SqlDAL.ExecuteScalar(connString, "USP_SaveOperationBudgetHeader", arParms))
                Return lngBudgetId

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveOperationBudgetDetails() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(9) {}
                Dim ds As DataSet
                Dim lngBudgetId As Long

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numBudgetId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _BudgetId

                arParms(2) = New Npgsql.NpgsqlParameter("@strRow", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(2).Value = _BudgetDetails

                arParms(3) = New Npgsql.NpgsqlParameter("@numDepartmentId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(3).Value = _DepartmentId

                arParms(4) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(4).Value = _DivisionId

                arParms(5) = New Npgsql.NpgsqlParameter("@numCostCenterId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(5).Value = _CostCenterId

                arParms(6) = New Npgsql.NpgsqlParameter("@intFiscalYear", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = _FiscalYear

                arParms(7) = New Npgsql.NpgsqlParameter("@Id", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(7).Value = _Id

                arParms(8) = New Npgsql.NpgsqlParameter("@tinyByteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(8).Value = _ByteMode

                arParms(9) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(9).Value = _Type

                SqlDAL.ExecuteNonQuery(connString, "USP_SaveOperationBudgetDetails", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetBudgetDetails() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetBudgetDetails", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetOperatingBudgetMasterDet() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numBudgetId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _BudgetId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetOperationBudgetMasterDetails", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetOperatingBudgetDetails() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numBudgetId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _BudgetId

                arParms(2) = New Npgsql.NpgsqlParameter("@intFiscalYear", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _FiscalYear

                arParms(3) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _Type

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetOperationBudgetDetails", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function CheckDuplicateBudgetName() As Long
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numBudgetId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _BudgetId

                arParms(1) = New Npgsql.NpgsqlParameter("@vcBudgetName", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(1).Value = _BudgetName

                arParms(2) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Value = _DivisionId

                arParms(3) = New Npgsql.NpgsqlParameter("@numDepartmentId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(3).Value = _DepartmentId

                arParms(4) = New Npgsql.NpgsqlParameter("@numCostCenterId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(4).Value = _CostCenterId

                arParms(5) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(5).Value = DomainID

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_CheckDuplicateBudgetName", arParms))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetProcurementMasterDetails() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim ds As DataSet
                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(0).Value = DomainID

                arparms(1) = New Npgsql.NpgsqlParameter("@numFiscalYear", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(1).Value = _FiscalYear

                arparms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(2).Value = Nothing
                arparms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetProcurementMasterDetails", arparms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Function GetProcurementDetails() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim ds As DataSet

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(0).Value = DomainID

                arparms(1) = New Npgsql.NpgsqlParameter("@numProcurementId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(1).Value = _ProcurementBudgetId

                arparms(2) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.Integer)
                arparms(2).Value = _Type

                arparms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(3).Value = Nothing
                arparms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_HierarchyGridForProcumentBudget", arparms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveProcurementBudgetMaster() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim ds As DataSet
                Dim lngProcurementBudgetId As Long

                ''arParms(0) = New Npgsql.NpgsqlParameter("@numProcurementBudgetId", NpgsqlTypes.NpgsqlDbType.BigInt)
                ''arParms(0).Value = _ProcurementBudgetId

                arParms(0) = New Npgsql.NpgsqlParameter("@intFiscalYear", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = _FiscalYear

                arParms(1) = New Npgsql.NpgsqlParameter("@bitPurchaseDeal", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(1).Value = _PurchaseDeal

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                lngProcurementBudgetId = CLng(SqlDAL.ExecuteScalar(connString, "USP_SaveProcurementBudgetHeader", arParms))
                Return lngProcurementBudgetId

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveProcurementBudgetDetails() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}
                Dim lngBudgetId As Long

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numProcurementId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _ProcurementBudgetId

                arParms(2) = New Npgsql.NpgsqlParameter("@strRow", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(2).Value = _ProcurementBudgetDetails

                arParms(3) = New Npgsql.NpgsqlParameter("@intYear", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _FiscalYear

                arParms(4) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _Type

                arParms(5) = New Npgsql.NpgsqlParameter("@sintByte", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _ByteMode

                arParms(6) = New Npgsql.NpgsqlParameter("@numItemGroupId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(6).Value = _Id

                SqlDAL.ExecuteNonQuery(connString, "USP_SaveProcurementBudgetDetails", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetMarketMasterDetails() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim ds As DataSet

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(0).Value = DomainID

                arparms(1) = New Npgsql.NpgsqlParameter("@numFiscalYear", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(1).Value = _FiscalYear

                arparms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(2).Value = Nothing
                arparms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetMarketBudgetMasterDetails", arparms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Function GetMarketBudgetDetails() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim ds As DataSet

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(0).Value = DomainID

                arparms(1) = New Npgsql.NpgsqlParameter("@numMarketId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(1).Value = _MarketBudgetId

                arparms(2) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.Integer)
                arparms(2).Value = _Type

                arparms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(3).Value = Nothing
                arparms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_MarketBudgetDetails", arparms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function SaveMarketBudgetMaster() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim lngMarketBudgetId As Long

                ''arParms(0) = New Npgsql.NpgsqlParameter("@numProcurementBudgetId", NpgsqlTypes.NpgsqlDbType.BigInt)
                ''arParms(0).Value = _ProcurementBudgetId

                arParms(0) = New Npgsql.NpgsqlParameter("@intFiscalYear", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = _FiscalYear

                arParms(1) = New Npgsql.NpgsqlParameter("@bitMarketCampaign", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(1).Value = _MarketCampaignBit

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                lngMarketBudgetId = CLng(SqlDAL.ExecuteScalar(connString, "USP_SaveMarketBudgetHeader", arParms))
                Return lngMarketBudgetId

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function SaveMarketBudgetDetails() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}
                Dim lngBudgetId As Long

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numProcurementBudgetId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _MarketBudgetId

                arParms(2) = New Npgsql.NpgsqlParameter("@strRow", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(2).Value = _MarketBudgetDetails

                arParms(3) = New Npgsql.NpgsqlParameter("@intYear", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _FiscalYear

                arParms(4) = New Npgsql.NpgsqlParameter("@sintByte", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _ByteMode

                arParms(5) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _Type

                arParms(6) = New Npgsql.NpgsqlParameter("@numListItemID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(6).Value = _Id

                SqlDAL.ExecuteNonQuery(connString, "USP_SaveMarketBudgetDetails", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetBudgetForPurchaseOpportunity() As Long
            Try
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim lngBudgetId As Long

                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@bitPurchaseDeal", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Direction = ParameterDirection.InputOutput
                arParms(2).Value = _bitPurchaseOpp

                arParms(3) = New Npgsql.NpgsqlParameter("@monmonthlyAmt", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(3).Direction = ParameterDirection.InputOutput
                arParms(3).Value = _MonthlyAmt

                arParms(4) = New Npgsql.NpgsqlParameter("@monTotalYearlyAmt", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(4).Direction = ParameterDirection.InputOutput
                arParms(4).Value = _TotalYearlyAmt

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ProcurementBudgetForOpenDeal", objParam, True)
                _bitPurchaseOpp = Convert.ToBoolean(DirectCast(objParam, Npgsql.NpgsqlParameter())(2).Value)
                _MonthlyAmt = Convert.ToDecimal(DirectCast(objParam, Npgsql.NpgsqlParameter())(3).Value)
                _TotalYearlyAmt = Convert.ToDecimal(DirectCast(objParam, Npgsql.NpgsqlParameter())(4).Value)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetOperationBudgetDetails() As DataTable
            Try
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetOperationBudgetMasterForImpact", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetParentOperationBudgetDetails() As DataTable
            Try
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numBudgetId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _BudgetId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@intFiscalYear", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _FiscalYear

                arParms(3) = New Npgsql.NpgsqlParameter("@sintByte", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _sintByte

                arParms(4) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _Type

                ds = SqlDAL.ExecuteDataset(connString, "GetParentOperationBudgetDetails", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetOperationBudgetMasterForBudgetAnalyze() As DataTable
            Try

                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numBudgetId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _BudgetId

                arParms(1) = New Npgsql.NpgsqlParameter("@numChartAcntId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _Id

                arParms(2) = New Npgsql.NpgsqlParameter("@sintByte", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _sintByte

                arParms(3) = New Npgsql.NpgsqlParameter("@sintForecast", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _sintForecast

                arParms(4) = New Npgsql.NpgsqlParameter("@intFiscalYear", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _FiscalYear

                arParms(5) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(5).Value = UserCntID

                arParms(6) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(6).Value = DomainID

                arParms(7) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(7).Value = _Type

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetOperationBudgetMasterForBudgetAnalyze", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetProcurementDetailsForImpact() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim ds As DataSet

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(0).Value = DomainID

                arparms(1) = New Npgsql.NpgsqlParameter("@numFiscalYear", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(1).Value = _FiscalYear

                arparms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(2).Value = Nothing
                arparms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetProcurementDetailsForImpact", arparms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetMarketBudgetDetailsForImpact() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim ds As DataSet

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(0).Value = DomainID

                arparms(1) = New Npgsql.NpgsqlParameter("@numFiscalYear", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(1).Value = _FiscalYear

                arparms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(2).Value = Nothing
                arparms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetMarketBudgetDetailsForImpact", arparms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetOperationBudgetDetailsForImpact() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim ds As DataSet

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(0).Value = DomainID

                arparms(1) = New Npgsql.NpgsqlParameter("@numBudgetId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(1).Value = _BudgetId

                arparms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(2).Value = Nothing
                arparms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetOperationBudgetDetailsForImpact", arparms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function UpdateImpactBudgetDetails() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim ds As DataSet
                Dim lngBudgetId As Long

                arParms(0) = New Npgsql.NpgsqlParameter("@sintByte", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _sintByte

                arParms(1) = New Npgsql.NpgsqlParameter("@strRow", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _BudgetDetails

                SqlDAL.ExecuteNonQuery(connString, "USP_SaveImpactBudgetDetails", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetTotalBankBalance() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim ds As DataSet
                Dim lngBudgetId As Long

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@monTotalBankBal", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(1).Direction = ParameterDirection.InputOutput
                arParms(1).Value = _TotalBankBalance

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_GetTotalBankBalance", objParam, True)
                _TotalBankBalance = Convert.ToDecimal(DirectCast(objParam, Npgsql.NpgsqlParameter())(1).Value)

            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function DeleteOperationBudget() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numOperationId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(0).Value = _BudgetId

                arparms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(1).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteOperationBudget", arparms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class
End Namespace
