Option Explicit On
Option Strict On

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.Accounting
    Public Class BankReconcile
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _AcntId As Long
        Public Property AcntId() As Long
            Get
                Return _AcntId
            End Get
            Set(ByVal Value As Long)
                _AcntId = Value
            End Set
        End Property

        Private _Mode As Short
        Public Property Mode() As Short
            Get
                Return _Mode
            End Get
            Set(ByVal Value As Short)
                _Mode = Value
            End Set
        End Property

        Private _ReconcileID As Long
        Public Property ReconcileID() As Long
            Get
                Return _ReconcileID
            End Get
            Set(ByVal Value As Long)
                _ReconcileID = Value
            End Set
        End Property

        Private _StatementDate As DateTime = New Date(1753, 1, 1)
        Public Property StatementDate() As DateTime
            Get
                Return _StatementDate
            End Get
            Set(ByVal Value As DateTime)
                _StatementDate = Value
            End Set
        End Property

        Private _BeginBalance As Decimal
        Public Property BeginBalance() As Decimal
            Get
                Return _BeginBalance
            End Get
            Set(ByVal Value As Decimal)
                _BeginBalance = Value
            End Set
        End Property

        Private _EndBalance As Decimal
        Public Property EndBalance() As Decimal
            Get
                Return _EndBalance
            End Get
            Set(ByVal Value As Decimal)
                _EndBalance = Value
            End Set
        End Property

        Private _ServiceChargeAmt As Decimal
        Public Property ServiceChargeAmt() As Decimal
            Get
                Return _ServiceChargeAmt
            End Get
            Set(ByVal Value As Decimal)
                _ServiceChargeAmt = Value
            End Set
        End Property

        Private _ServiceChargeChartAcntId As Long
        Public Property ServiceChargeChartAcntId() As Long
            Get
                Return _ServiceChargeChartAcntId
            End Get
            Set(ByVal Value As Long)
                _ServiceChargeChartAcntId = Value
            End Set
        End Property

        Private _ServiceChargeDate As DateTime = New Date(1753, 1, 1)
        Public Property ServiceChargeDate() As DateTime
            Get
                Return _ServiceChargeDate
            End Get
            Set(ByVal Value As DateTime)
                _ServiceChargeDate = Value
            End Set
        End Property

        Private _InterestEarnedAmt As Decimal
        Public Property InterestEarnedAmt() As Decimal
            Get
                Return _InterestEarnedAmt
            End Get
            Set(ByVal Value As Decimal)
                _InterestEarnedAmt = Value
            End Set
        End Property

        Private _InterestEarnedChartAcntId As Long
        Public Property InterestEarnedChartAcntId() As Long
            Get
                Return _InterestEarnedChartAcntId
            End Get
            Set(ByVal Value As Long)
                _InterestEarnedChartAcntId = Value
            End Set
        End Property

        Private _InterestEarnedDate As DateTime = New Date(1753, 1, 1)
        Public Property InterestEarnedDate() As DateTime
            Get
                Return _InterestEarnedDate
            End Get
            Set(ByVal Value As DateTime)
                _InterestEarnedDate = Value
            End Set
        End Property

        Private _ReconcileComplete As Boolean
        Public Property ReconcileComplete() As Boolean
            Get
                Return _ReconcileComplete
            End Get
            Set(ByVal Value As Boolean)
                _ReconcileComplete = Value
            End Set
        End Property

        Public Property FileName As String
        Public Property FileData As String

        Public Function ManageBankReconcileMaster(Optional ByVal isCodeLevelTransacion As Boolean = False) As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@tintMode", _Mode, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@numReconcileID", _ReconcileID, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@numCreatedBy", UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@dtStatementDate", _StatementDate, NpgsqlTypes.NpgsqlDbType.Timestamp))

                    .Add(SqlDAL.Add_Parameter("@monBeginBalance", _BeginBalance, NpgsqlTypes.NpgsqlDbType.Numeric))

                    .Add(SqlDAL.Add_Parameter("@monEndBalance", _EndBalance, NpgsqlTypes.NpgsqlDbType.Numeric))

                    .Add(SqlDAL.Add_Parameter("@numChartAcntId", _AcntId, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@monServiceChargeAmount", _ServiceChargeAmt, NpgsqlTypes.NpgsqlDbType.Numeric))

                    .Add(SqlDAL.Add_Parameter("@numServiceChargeChartAcntId", _ServiceChargeChartAcntId, NpgsqlTypes.NpgsqlDbType.Bigint))

                    .Add(SqlDAL.Add_Parameter("@dtServiceChargeDate", _ServiceChargeDate, NpgsqlTypes.NpgsqlDbType.Timestamp))

                    .Add(SqlDAL.Add_Parameter("@monInterestEarnedAmount", _InterestEarnedAmt, NpgsqlTypes.NpgsqlDbType.Numeric))

                    .Add(SqlDAL.Add_Parameter("@numInterestEarnedChartAcntId", _InterestEarnedChartAcntId, NpgsqlTypes.NpgsqlDbType.Bigint))

                    .Add(SqlDAL.Add_Parameter("@dtInterestEarnedDate", _InterestEarnedDate, NpgsqlTypes.NpgsqlDbType.Timestamp))

                    .Add(SqlDAL.Add_Parameter("@bitReconcileComplete", _ReconcileComplete, NpgsqlTypes.NpgsqlDbType.Boolean))

                    .Add(SqlDAL.Add_Parameter("@vcFileName", FileName, NpgsqlTypes.NpgsqlDbType.Varchar))

                    .Add(SqlDAL.Add_Parameter("@vcFileData", FileData, NpgsqlTypes.NpgsqlDbType.Text))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_ManageBankReconcileMaster", sqlParams.ToArray())

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function CheckFinancialyear() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@dtDate", _StatementDate, NpgsqlTypes.NpgsqlDbType.Timestamp))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim Total As Integer = CInt(SqlDAL.ExecuteScalar(connString, "USP_CheckFinancialyear", sqlParams.ToArray()))

                If Total > 0 Then
                    Return True
                End If

                Return False
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageBankStatementTransactions() As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint, 9))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_ManageBankStatementTransactions", sqlParams.ToArray())

                Return ds

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Sub UpdateBankReconcileFileDataStatus(ByVal vcText As String)
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint, 18))
                    .Add(SqlDAL.Add_Parameter("@numReconcileID", ReconcileID, NpgsqlTypes.NpgsqlDbType.Bigint, 18))
                    .Add(SqlDAL.Add_Parameter("@strStatement", vcText, NpgsqlTypes.NpgsqlDbType.Varchar))

                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_BankReconcileFileData_UpdateStatus", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub DeleteBankReconcile()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint, 18))
                    .Add(SqlDAL.Add_Parameter("@numReconcileID", ReconcileID, NpgsqlTypes.NpgsqlDbType.Bigint, 18))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_BankReconcileMaster_DeleteByID", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub DeleteOldAdjustment()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint, 18))
                    .Add(SqlDAL.Add_Parameter("@numReconcileID", ReconcileID, NpgsqlTypes.NpgsqlDbType.Bigint, 18))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_Journal_DeleteOldBankReconciliationAdjustment", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub
    End Class
   
End Namespace
