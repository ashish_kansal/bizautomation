Option Explicit On
Option Strict On

Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRM.BusinessLogic.Common
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic
Imports Npgsql

Namespace BACRM.BusinessLogic.Accounting
    Public Class Checks
        Inherits BACRM.BusinessLogic.CBusinessBase

        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Siva 	DATE:03-Feb-07
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Siva 	DATE:03-Feb-07
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region

        'Define Private Vairable
        'Private DomainId As Integer
        Private _AccountType As Integer
        Private _strWhereCondition As String
        Private _AccountId As Integer
        Private _Entry_Date As Date = New Date(1753, 1, 1)
        Private _numAmount As Decimal
        Private _JournalId As Integer
        Private _CheckId As Integer
        Private _JournalDetails As String = String.Empty
        Private _Mode As Int16
        Private _DivisionId As Integer
        Private _RecurringMode As Int16
        Private _RecurringId As Integer
        Private _AuthorizativeAccounting As Int16
        Private _OppId As Integer
        Private _OppBizDocsId As Integer
        Private _DepositId As Integer
        Private _Amount As Decimal
        Private _CustomerId As Long
        Private _CheckCompanyId As Long
        Private _Memo As String = String.Empty
        Private _CheckNo As Long
        Private _byteMode As Int16
        Private _FromDate As Date = New Date(1753, 1, 1)
        Private _ToDate As Date = New Date(1753, 1, 1)
        Private _strSearch As String


        Private _Description As String


        Public Property Description() As String
            Get
                Return _Description
            End Get
            Set(ByVal value As String)
                _Description = value
            End Set
        End Property

        Public Property strSearch() As String
            Get
                Return _strSearch
            End Get
            Set(ByVal Value As String)
                _strSearch = Value
            End Set
        End Property


        Public Property ToDate() As Date
            Get
                Return _ToDate
            End Get
            Set(ByVal Value As Date)
                _ToDate = Value
            End Set
        End Property


        Public Property FromDate() As Date
            Get
                Return _FromDate
            End Get
            Set(ByVal Value As Date)
                _FromDate = Value
            End Set
        End Property


        ''Public Property DomainId() As Integer
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Integer)
        '        DomainId = Value
        '    End Set
        'End Property

        Public Property AccountType() As Integer
            Get
                Return _AccountType
            End Get
            Set(ByVal Value As Integer)
                _AccountType = Value
            End Set
        End Property

        Public Property strWhereCondition() As String
            Get
                Return _strWhereCondition
            End Get
            Set(ByVal value As String)
                _strWhereCondition = value
            End Set
        End Property

        Public Property AccountId() As Integer
            Get
                Return _AccountId
            End Get
            Set(ByVal Value As Integer)
                _AccountId = Value
            End Set
        End Property

        Public Property Entry_Date() As DateTime
            Get
                Return _Entry_Date
            End Get
            Set(ByVal Value As DateTime)
                _Entry_Date = Value
            End Set
        End Property

        Public Property numAmount() As Decimal
            Get
                Return _numAmount
            End Get
            Set(ByVal Value As Decimal)
                _numAmount = Value
            End Set
        End Property

        Public Property JournalId() As Integer
            Get
                Return _JournalId
            End Get
            Set(ByVal Value As Integer)
                _JournalId = Value
            End Set
        End Property

        Public Property CheckId() As Integer
            Get
                Return _CheckId
            End Get
            Set(ByVal Value As Integer)
                _CheckId = Value
            End Set
        End Property

        Public Property JournalDetails() As String
            Get
                Return _JournalDetails
            End Get
            Set(ByVal value As String)
                _JournalDetails = value
            End Set
        End Property

        Public Property Mode() As Int16
            Get
                Return _Mode
            End Get
            Set(ByVal Value As Int16)
                _Mode = Value
            End Set
        End Property

        Public Property DivisionId() As Integer
            Get
                Return _DivisionId
            End Get
            Set(ByVal Value As Integer)
                _DivisionId = Value
            End Set
        End Property

        Public Property RecurringMode() As Int16
            Get
                Return _RecurringMode
            End Get
            Set(ByVal Value As Int16)
                _RecurringMode = Value
            End Set
        End Property

        Public Property RecurringId() As Integer
            Get
                Return _RecurringId
            End Get
            Set(ByVal value As Integer)
                _RecurringId = value
            End Set
        End Property

        'Public Property UserCntID() As Integer
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal value As Integer)
        '        UserCntID = value
        '    End Set
        'End Property


        Public Property AuthorizativeAccounting() As Int16
            Get
                Return _AuthorizativeAccounting
            End Get
            Set(ByVal value As Int16)
                _AuthorizativeAccounting = value
            End Set
        End Property

        Public Property OppId() As Integer
            Get
                Return _OppId
            End Get
            Set(ByVal value As Integer)
                _OppId = value
            End Set
        End Property

        Public Property OppBizDocsId() As Integer
            Get
                Return _OppBizDocsId
            End Get
            Set(ByVal value As Integer)
                _OppBizDocsId = value
            End Set
        End Property

        Public Property DepositId() As Integer
            Get
                Return _DepositId
            End Get
            Set(ByVal value As Integer)
                _DepositId = value
            End Set
        End Property

        Public Property Amount() As Decimal
            Get
                Return _Amount
            End Get
            Set(ByVal value As Decimal)
                _Amount = value
            End Set
        End Property



        Public Property CustomerId() As Long
            Get
                Return _CustomerId
            End Get
            Set(ByVal value As Long)
                _CustomerId = value
            End Set
        End Property

        Public Property CheckCompanyId() As Long
            Get
                Return _CheckCompanyId
            End Get
            Set(ByVal value As Long)
                _CheckCompanyId = value
            End Set
        End Property

        Public Property Memo() As String
            Get
                Return _Memo
            End Get
            Set(ByVal value As String)
                _Memo = value
            End Set
        End Property

        Public Property CheckNo() As Long
            Get
                Return _CheckNo
            End Get
            Set(ByVal value As Long)
                _CheckNo = value
            End Set
        End Property

        Public Property byteMode() As Int16
            Get
                Return _byteMode
            End Get
            Set(ByVal value As Int16)
                _byteMode = value
            End Set
        End Property

        Private _CheckHeaderID As Long

        Public Property CheckHeaderID() As Long
            Get
                Return _CheckHeaderID
            End Get
            Set(ByVal value As Long)
                _CheckHeaderID = value
            End Set
        End Property

        Private _Type As Short
        Public Property Type() As Short
            Get
                Return _Type
            End Get
            Set(ByVal value As Short)
                _Type = value
            End Set
        End Property

        Private _ReferenceID As Long
        Public Property ReferenceID() As Long
            Get
                Return _ReferenceID
            End Get
            Set(ByVal value As Long)
                _ReferenceID = value
            End Set
        End Property

        Private _IsPrint As Boolean
        Public Property IsPrint() As Boolean
            Get
                Return _IsPrint
            End Get
            Set(ByVal value As Boolean)
                _IsPrint = value
            End Set
        End Property

        Private _dtFromDate As DateTime
        Public Property dtFromDate() As DateTime
            Get
                Return _dtFromDate
            End Get
            Set(ByVal value As DateTime)
                _dtFromDate = value
            End Set
        End Property

        Private _dtToDate As DateTime
        Public Property dtToDate() As DateTime
            Get
                Return _dtToDate
            End Get
            Set(ByVal value As DateTime)
                _dtToDate = value
            End Set
        End Property

        Public Function GetCompanyDetails() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@varCompanyName", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(1).Value = _strWhereCondition

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_Journal_CompanyDetails", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCompanyName() As String
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim strName As String

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(1).Value = _DivisionId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                strName = CStr(SqlDAL.ExecuteScalar(connString, "USP_Journal_CompanyName", arParms))
                Return strName

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCheckChartAcntDetails() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@AcntTypeId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _AccountType

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetCheckChartAcntDet", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCheckNumber() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetCheckNumber", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCheckOpeningBalance() As Decimal
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim ldecOpeningBal As Decimal

                arParms(0) = New Npgsql.NpgsqlParameter("@numAccountId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _AccountId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ldecOpeningBal = CDec(SqlDAL.ExecuteScalar(connString, "USP_GetCheckOpeningBalance", arParms))
                Return ldecOpeningBal
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveDataToJournalDetailsForChecks() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numJournalId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _JournalId

                arParms(1) = New Npgsql.NpgsqlParameter("@strRow", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _JournalDetails

                arParms(2) = New Npgsql.NpgsqlParameter("@Mode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _Mode

                arParms(3) = New Npgsql.NpgsqlParameter("@RecurringMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _RecurringMode

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(4).Value = DomainID

                ''arParms(5) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                ''arParms(5).Value = _OppId

                SqlDAL.ExecuteNonQuery(connString, "USP_SaveJournalDetails", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCheckJournalDetails() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numJournalId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _JournalId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetChecksJournalDetails", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetJournalId() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numCheckId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _CheckId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_GetCheckJournalId", arParms))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCheckDetails() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numCheckId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _CheckId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetCheckDetails", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetMainCheckDetailsId() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim lintTransactionId As Long

                arParms(0) = New Npgsql.NpgsqlParameter("@numCheckId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _CheckId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                lintTransactionId = CLng(SqlDAL.ExecuteScalar(connString, "USP_GetCheckMainDetailsId", arParms))
                Return lintTransactionId
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetRecurringDetails() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(0).Value = DomainID

                arparms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(1).Value = Nothing
                arparms(1).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetRecurringDetails", arparms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveCheckCompanyDetails() As Long
            Try
                Dim getConnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}


                arparms(0) = New Npgsql.NpgsqlParameter("@numCheckCompanyId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(0).Value = _CheckCompanyId

                arparms(1) = New Npgsql.NpgsqlParameter("@numCompanyId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(1).Value = _CustomerId

                arparms(2) = New Npgsql.NpgsqlParameter("@monAmount", NpgsqlTypes.NpgsqlDbType.Numeric)
                arparms(2).Value = _Amount

                arparms(3) = New Npgsql.NpgsqlParameter("@vcMemo", NpgsqlTypes.NpgsqlDbType.Varchar, 200)
                arparms(3).Value = Memo

                arparms(4) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(4).Value = UserCntID

                arparms(5) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(5).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_SaveCheckCompanyDetails", arparms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetCompanyCheckDetails() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(0).Value = DomainID

                arparms(1) = New Npgsql.NpgsqlParameter("@numCheckId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(1).Value = _CheckId

                arparms(2) = New Npgsql.NpgsqlParameter("@numCheckCompanyId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(2).Value = _CheckCompanyId

                arparms(3) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arparms(3).Value = _byteMode

                arparms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(4).Value = Nothing
                arparms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetCompanyCheckDetails", arparms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteCheckCompanyDetails() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(0).Value = DomainID

                arparms(1) = New Npgsql.NpgsqlParameter("@numCheckCompanyId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(1).Value = _CheckCompanyId

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteCheckCompanyDetails", arparms)
                Return True
            Catch ex As Exception
                Return False
                Throw ex
            End Try
        End Function

        Public Function UpdateCheckCompanyId() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numCheckId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(0).Value = _CheckId

                arparms(1) = New Npgsql.NpgsqlParameter("@numCheckCompanyId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(1).Value = _CheckCompanyId

                arparms(2) = New Npgsql.NpgsqlParameter("@numCompanyId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(2).Value = _CustomerId

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateCheckCompanyId", arparms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function CheckDuplicateCheckNo() As Integer
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numCheckId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(0).Value = _CheckId

                arparms(1) = New Npgsql.NpgsqlParameter("@numCheckNo", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(1).Value = _CheckNo

                arparms(2) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(2).Value = DomainID

                arparms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(3).Value = Nothing
                arparms(3).Direction = ParameterDirection.InputOutput

                Return CInt(SqlDAL.ExecuteScalar(connString, "USP_CheckDuplicateCheckNo", arparms))
            Catch ex As Exception
                Throw ex
            End Try
        End Function



        Public Function GetMoneyTrail() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(0).Value = DomainID

                arparms(1) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(1).Value = _DivisionId

                arparms(2) = New Npgsql.NpgsqlParameter("@numAccountID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(2).Value = _AccountId

                arparms(3) = New Npgsql.NpgsqlParameter("@dtFrom", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(3).Value = _FromDate

                arparms(4) = New Npgsql.NpgsqlParameter("@dtTo", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(4).Value = _ToDate

                arparms(5) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arparms(5).Value = _byteMode

                arparms(6) = New Npgsql.NpgsqlParameter("@strSearch", NpgsqlTypes.NpgsqlDbType.Varchar, 100)
                arparms(6).Value = _strSearch

                arparms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(7).Value = Nothing
                arparms(7).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetMonyTrailDetails", arparms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function UpdateCheckNo() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(0).Value = DomainID

                arparms(1) = New Npgsql.NpgsqlParameter("@numCheckNo", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(1).Value = _CheckNo

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateCheckNo", arparms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveCheckDetails() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@numCheckHeaderID", _CheckHeaderID, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@strRow", _JournalDetails, NpgsqlTypes.NpgsqlDbType.Text))

                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_SaveCheckDetails", sqlParams.ToArray())
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function FetchCheckDetails() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@numCheckHeaderID", _CheckHeaderID, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_FetchCheckDetails", sqlParams.ToArray())

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageCheckHeader() As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@tintMode", _Mode, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@numCheckHeaderID", _CheckHeaderID, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@numChartAcntId", _AccountId, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@numDivisionID", _DivisionId, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@monAmount", _Amount, NpgsqlTypes.NpgsqlDbType.Numeric))

                    .Add(SqlDAL.Add_Parameter("@numCheckNo", _CheckNo, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@tintReferenceType", _Type, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@numReferenceID", _ReferenceID, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@dtCheckDate", _FromDate, NpgsqlTypes.NpgsqlDbType.Timestamp))

                    .Add(SqlDAL.Add_Parameter("@bitIsPrint", _IsPrint, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@vcMemo", _Memo, NpgsqlTypes.NpgsqlDbType.Varchar, 1000))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_ManageCheckHeader", sqlParams.ToArray())

                Return ds

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageCheckHeader(ByVal objTransaction As NpgsqlTransaction) As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@tintMode", _Mode, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@numCheckHeaderID", _CheckHeaderID, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@numChartAcntId", _AccountId, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@numDivisionID", _DivisionId, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@monAmount", _Amount, NpgsqlTypes.NpgsqlDbType.Numeric))

                    .Add(SqlDAL.Add_Parameter("@numCheckNo", _CheckNo, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@tintReferenceType", _Type, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@numReferenceID", _ReferenceID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@dtCheckDate", _FromDate, NpgsqlTypes.NpgsqlDbType.Timestamp))

                    .Add(SqlDAL.Add_Parameter("@bitIsPrint", _IsPrint, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@vcMemo", _Memo, NpgsqlTypes.NpgsqlDbType.VarChar, 1000))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim ds As DataSet
                If Not objTransaction Is Nothing Then
                    ds = SqlDAL.ExecuteDataset(objTransaction, "USP_ManageCheckHeader", sqlParams.ToArray())
                Else
                    ds = SqlDAL.ExecuteDataset(connString, "USP_ManageCheckHeader", sqlParams.ToArray())
                End If

                Return ds

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function UpdateCheckHeadePrintCheck() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@strRow", _JournalDetails, NpgsqlTypes.NpgsqlDbType.Text))

                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateCheckHeadePrintCheck", sqlParams.ToArray())

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetMatchingCheckDetails() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@monAmount", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(0).Value = _numAmount

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _dtFromDate

                arParms(3) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(3).Value = _dtToDate

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetMatchingCheckDetails", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function SaveCheckHeader(ByVal COAccountId As Integer, ByVal DivisionId As Integer, ByVal Amount As Decimal,
                      ByVal FromDate As Date, ByVal CheckNo As Long, ByVal Memo As String, ByVal IsPrint As Boolean, ByVal strCheckDetails As String) As Long

            Try
                Dim objChecks As New Checks()
                Dim CheckHeaderID As Long = 0
                objChecks.DomainID = DomainID
                objChecks.AccountId = COAccountId
                objChecks.CheckHeaderID = CheckHeaderID
                objChecks.DivisionId = DivisionId
                objChecks.UserCntID = UserCntID
                objChecks.Amount = Amount
                objChecks.FromDate = FromDate
                objChecks.CheckNo = CheckNo
                objChecks.Memo = Memo
                objChecks.IsPrint = IsPrint

                objChecks.Type = CCommon.ToShort(enmReferenceType.CheckHeader)  'Write Check
                objChecks.Mode = 2 'Insert/Edit

                Dim ds As DataSet = objChecks.ManageCheckHeader()

                Dim dtCheckHeader As DataTable = ds.Tables(0)

                If dtCheckHeader.Rows.Count > 0 Then
                    CheckHeaderID = CCommon.ToLong(dtCheckHeader.Rows(0)("numCheckHeaderID"))
                End If
                objChecks.DomainID = DomainID
                objChecks.CheckHeaderID = CheckHeaderID
                objChecks.JournalDetails = strCheckDetails

                objChecks.SaveCheckDetails()
                Return CheckHeaderID

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function SaveCheckDataToHeader(ByVal EntryDate As Date, ByVal Description As String, ByVal Amount As Decimal, ByVal COAccountId As Long, ByVal lngCheckHeaderID As Long) As Long
            Try
                Dim objJEHeader As New JournalEntryHeader
                Dim lngJournalID As Long
                With objJEHeader
                    .JournalId = 0
                    .RecurringId = 0
                    .EntryDate = EntryDate
                    .Description = Description
                    .Amount = Amount
                    .CheckId = 0
                    .CashCreditCardId = 0
                    .ChartAcntId = COAccountId
                    .OppId = 0
                    .OppBizDocsId = 0
                    .DepositId = 0
                    .BizDocsPaymentDetId = 0
                    .IsOpeningBalance = CCommon.ToBool(0)
                    .LastRecurringDate = Date.Now
                    .NoTransactions = 0
                    .CategoryHDRID = 0
                    .ReturnID = 0
                    .CheckHeaderID = lngCheckHeaderID
                    .BillID = 0
                    .BillPaymentID = 0
                    .UserCntID = UserCntID
                    .DomainID = DomainID
                End With
                lngJournalID = objJEHeader.Save()
                Return lngJournalID
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Sub SaveCheckDataToGeneralJournalDetails(ByVal EntryDate As Date, ByVal Description As String, ByVal Amount As Decimal,
                                                         ByVal COAccountId As Long, ByVal lngCheckHeaderID As Long, ByVal CustomerId As Long, ByVal lngJournalId As Long)
            Try
                Dim objJEList As New JournalEntryCollection

                Dim objJE As New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = 0
                objJE.CreditAmt = Math.Abs(Amount)
                objJE.ChartAcntId = COAccountId
                objJE.Description = "Write Check"
                objJE.CustomerId = CustomerId
                objJE.MainDeposit = CCommon.ToBool(0)
                objJE.MainCheck = CCommon.ToBool(0)
                objJE.MainCashCredit = CCommon.ToBool(0)
                objJE.OppitemtCode = 0
                objJE.BizDocItems = ""
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 0
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = CCommon.ToBool(1)
                objJE.ReferenceType = CCommon.ToShort(enmReferenceType.CheckHeader) 'Write Check Header
                objJE.ReferenceID = lngCheckHeaderID

                objJEList.Add(objJE)

                Dim dtDetails As New DataTable

                Dim objChecks As New Checks

                objChecks.DomainID = DomainID
                objChecks.CheckHeaderID = lngCheckHeaderID

                dtDetails = objChecks.FetchCheckDetails()

                For Each dr As DataRow In dtDetails.Rows
                    objJE = New JournalEntryNew()

                    objJE.TransactionId = CCommon.ToLong(dr("numTransactionId"))
                    objJE.DebitAmt = Math.Abs(CCommon.ToDecimal(dr("monAmount")))
                    objJE.CreditAmt = 0
                    objJE.ChartAcntId = CCommon.ToLong(dr("numChartAcntId"))
                    objJE.Description = CCommon.ToString(dr("vcDescription"))
                    objJE.CustomerId = CCommon.ToLong(dr("numCustomerId"))
                    objJE.MainDeposit = CCommon.ToBool(0)
                    objJE.MainCheck = CCommon.ToBool(0)
                    objJE.MainCashCredit = CCommon.ToBool(0)
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = ""
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = 0
                    objJE.FltExchangeRate = 0
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = CCommon.ToLong(dr("numProjectID"))
                    objJE.ClassID = CCommon.ToLong(dr("numClassID"))
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = CCommon.ToBool(1)
                    objJE.ReferenceType = CCommon.ToShort(enmReferenceType.CheckDetail) 'Write Check Detail
                    objJE.ReferenceID = CCommon.ToLong(dr("numCheckDetailID"))

                    objJEList.Add(objJE)
                Next

                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, DomainID)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace
