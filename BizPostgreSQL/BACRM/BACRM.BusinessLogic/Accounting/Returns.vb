﻿Option Explicit On
Option Strict On

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.Accounting
    Public Class Returns
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _ReturnHeaderID As Long
        Public Property ReturnHeaderID As Long
            Get
                Return _ReturnHeaderID
            End Get
            Set(ByVal value As Long)
                _ReturnHeaderID = value
            End Set
        End Property

        Private _RMA As String
        Public Property RMA As String
            Get
                Return _RMA
            End Get
            Set(ByVal value As String)
                _RMA = value
            End Set
        End Property

        Private _BizDocName As String
        Public Property BizDocName As String
            Get
                Return _BizDocName
            End Get
            Set(ByVal value As String)
                _BizDocName = value
            End Set
        End Property

        Private _DivisionId As Long
        Public Property DivisionId() As Long
            Get
                Return _DivisionId
            End Get
            Set(ByVal value As Long)
                _DivisionId = value
            End Set
        End Property

        Private _ContactId As Long
        Public Property ContactId() As Long
            Get
                Return _ContactId
            End Get
            Set(ByVal value As Long)
                _ContactId = value
            End Set
        End Property

        Private _OppId As Long
        Public Property OppId() As Long
            Get
                Return _OppId
            End Get
            Set(ByVal value As Long)
                _OppId = value
            End Set
        End Property

        Private _ReturnType As Short
        Public Property ReturnType() As Short
            Get
                Return _ReturnType
            End Get
            Set(ByVal value As Short)
                _ReturnType = value
            End Set
        End Property

        Private _ReturnReason As Long
        Public Property ReturnReason() As Long
            Get
                Return _ReturnReason
            End Get
            Set(ByVal value As Long)
                _ReturnReason = value
            End Set
        End Property

        Private _ReturnStatus As Long
        Public Property ReturnStatus() As Long
            Get
                Return _ReturnStatus
            End Get
            Set(ByVal value As Long)
                _ReturnStatus = value
            End Set
        End Property

        Private _BizDocsPaymentDetId As Integer
        Public Property BizDocsPaymentDetId() As Integer
            Get
                Return _BizDocsPaymentDetId
            End Get
            Set(ByVal value As Integer)
                _BizDocsPaymentDetId = value
            End Set
        End Property

        Private _Amount As Decimal
        Public Property Amount() As Decimal
            Get
                Return _Amount
            End Get
            Set(ByVal value As Decimal)
                _Amount = value
            End Set
        End Property

        Private _AmountUsed As Decimal
        Public Property AmountUsed() As Decimal
            Get
                Return _AmountUsed
            End Get
            Set(ByVal value As Decimal)
                _AmountUsed = value
            End Set
        End Property

        Private _TotalTax As Decimal
        Public Property TotalTax() As Decimal
            Get
                Return _TotalTax
            End Get
            Set(ByVal value As Decimal)
                _TotalTax = value
            End Set
        End Property

        Private _TotalDiscount As Decimal
        Public Property TotalDiscount() As Decimal
            Get
                Return _TotalDiscount
            End Get
            Set(ByVal value As Decimal)
                _TotalDiscount = value
            End Set
        End Property

        Private _ReceiveType As Short
        Public Property ReceiveType() As Short
            Get
                Return _ReceiveType
            End Get
            Set(ByVal value As Short)
                _ReceiveType = value
            End Set
        End Property

        Private _Comments As String
        Public Property Comments() As String
            Get
                Return _Comments
            End Get
            Set(ByVal value As String)
                _Comments = value
            End Set
        End Property

        Private _strXML As String
        Public Property strXML() As String
            Get
                Return _strXML
            End Get
            Set(ByVal value As String)
                _strXML = value
            End Set
        End Property

        Private _DepositeID As Long
        Public Property DepositeID() As Long
            Get
                Return _DepositeID
            End Get
            Set(ByVal value As Long)
                _DepositeID = value
            End Set
        End Property

        Private _ReferenceID As Long
        Public Property ReferenceID() As Long
            Get
                Return _ReferenceID
            End Get
            Set(ByVal value As Long)
                _ReferenceID = value
            End Set
        End Property

        Private _Mode As Short
        Public Property Mode() As Short
            Get
                Return _Mode
            End Get
            Set(ByVal value As Short)
                _Mode = value
            End Set
        End Property

        Private _CurrencyID As Long
        Public Property CurrencyID() As Long
            Get
                Return _CurrencyID
            End Get
            Set(ByVal Value As Long)
                _CurrencyID = Value
            End Set
        End Property

        Private _AccountClass As Long
        Public Property AccountClass() As Long
            Get
                Return _AccountClass
            End Get
            Set(ByVal value As Long)
                _AccountClass = value
            End Set
        End Property

        Public Function ManageReturnHeaderItems() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numReturnHeaderID", _ReturnHeaderID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))
                    .Add(SqlDAL.Add_Parameter("@vcRMA", _RMA, NpgsqlTypes.NpgsqlDbType.VarChar, 100))
                    .Add(SqlDAL.Add_Parameter("@vcBizDocName", _BizDocName, NpgsqlTypes.NpgsqlDbType.VarChar, 100))
                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))
                    .Add(SqlDAL.Add_Parameter("@numDivisionId", _DivisionId, NpgsqlTypes.NpgsqlDbType.BigInt, 9))
                    .Add(SqlDAL.Add_Parameter("@numContactId", _ContactId, NpgsqlTypes.NpgsqlDbType.BigInt, 9))
                    .Add(SqlDAL.Add_Parameter("@numOppId", _OppId, NpgsqlTypes.NpgsqlDbType.BigInt, 9))
                    .Add(SqlDAL.Add_Parameter("@tintReturnType", _ReturnType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@numReturnReason", _ReturnReason, NpgsqlTypes.NpgsqlDbType.BigInt, 9))
                    .Add(SqlDAL.Add_Parameter("@numReturnStatus", _ReturnStatus, NpgsqlTypes.NpgsqlDbType.BigInt, 9))
                    .Add(SqlDAL.Add_Parameter("@monAmount", _Amount, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@monAmountUsed", _AmountUsed, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@monTotalTax", _TotalTax, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@monTotalDiscount", _TotalDiscount, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@tintReceiveType", _ReceiveType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@vcComments", _Comments, NpgsqlTypes.NpgsqlDbType.Text))
                    .Add(SqlDAL.Add_Parameter("@strItems", _strXML, NpgsqlTypes.NpgsqlDbType.Text))
                    .Add(SqlDAL.Add_Parameter("@numDepositeId", _DepositeID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageReturnHeaderItems", sqlParams.ToArray())

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetCustomerCredits() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@numDivisionId", _DivisionId, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@numReferenceId", _ReferenceID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@tintMode", _Mode, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@numCurrencyID", _CurrencyID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@numAccountClass", _AccountClass, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetCustomerCredits", sqlParams.ToArray())

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace


