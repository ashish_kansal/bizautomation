Option Explicit On
Option Strict On

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Accounting
    Public Class VendorPayment
        Inherits BACRM.BusinessLogic.CBusinessBase

        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Siva 	DATE:17-May-07
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Siva 	DATE:17-May-07
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub

        '#End Region

        'define private vairable
        'Private DomainId As Integer
        Private _BizDocsPaymentDetId As Integer
        Private _BizDocsId As Integer
        Private _OppId As Integer
        Private _Amount As Decimal
        Private _Entry_Date As Date = New Date(1753, 1, 1)
        Private _JournalId As Integer
        Private _RecurringId As Integer
        Private _OppBIzDocID As Integer
        Private _JournalDetails As String = String.Empty
        Private _Mode As Int16
        Private _RecurringMode As Int16
        Private _ClientTimeZoneOffset As Int32
        'Private _ThiryDaysAmountTotal As Decimal
        'Private _SixtyDaysAmountTotal As Decimal
        'Private _NinetyDaysAmountTotal As Decimal
        'Private _OverNinetyDaysAmountTotal As Decimal
        'Private _PassedDueAmountTotal As Decimal
        'Private _TotalAmountTotal As Decimal


        Private _Flag As String

        Private _CheckNo As Long


        Private _ProjectID As Long
        Public Property ProjectID() As Long
            Get
                Return _ProjectID
            End Get
            Set(ByVal value As Long)
                _ProjectID = value
            End Set
        End Property


        Private _AccountClass As Long
        Public Property AccountClass() As Long
            Get
                Return _AccountClass
            End Get
            Set(ByVal value As Long)
                _AccountClass = value
            End Set
        End Property

        Public Property CheckNo() As Long
            Get
                Return _CheckNo
            End Get
            Set(ByVal value As Long)
                _CheckNo = value
            End Set
        End Property

        Private _ReturnID As Long
        Public Property ReturnID() As Long
            Get
                Return _ReturnID
            End Get
            Set(ByVal value As Long)
                _ReturnID = value
            End Set
        End Property

        Public Property Flag() As String
            Get
                Return _Flag
            End Get
            Set(ByVal value As String)
                _Flag = value
            End Set
        End Property


        Private _DivisionID As Long
        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal value As Long)
                _DivisionID = value
            End Set
        End Property


        Private _CompanyID As Long
        Public Property CompanyID() As Long
            Get
                Return _CompanyID
            End Get
            Set(ByVal value As Long)
                _CompanyID = value
            End Set
        End Property


        'Public Property DomainId() As Integer
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal value As Integer)
        '        DomainId = value
        '    End Set
        'End Property

        Public Property BizDocsPaymentDetId() As Integer
            Get
                Return _BizDocsPaymentDetId
            End Get
            Set(ByVal value As Integer)
                _BizDocsPaymentDetId = value
            End Set
        End Property

        Public Property BizDocsId() As Integer
            Get
                Return _BizDocsId
            End Get
            Set(ByVal value As Integer)
                _BizDocsId = value
            End Set
        End Property

        Public Property OppId() As Integer
            Get
                Return _OppId
            End Get
            Set(ByVal value As Integer)
                _OppId = value
            End Set
        End Property

        Public Property Amount() As Decimal
            Get
                Return _Amount
            End Get
            Set(ByVal value As Decimal)
                _Amount = value
            End Set
        End Property

        Public Property Entry_Date() As DateTime
            Get
                Return _Entry_Date
            End Get
            Set(ByVal Value As DateTime)
                _Entry_Date = Value
            End Set
        End Property

        Public Property JournalId() As Integer
            Get
                Return _JournalId
            End Get
            Set(ByVal value As Integer)
                _JournalId = value
            End Set
        End Property

        Public Property RecurringId() As Integer
            Get
                Return _RecurringId
            End Get
            Set(ByVal value As Integer)
                _RecurringId = value
            End Set
        End Property

        'Public Property UserCntID() As Integer
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal value As Integer)
        '        UserCntID = value
        '    End Set
        'End Property

        Public Property OppBIzDocID() As Integer
            Get
                Return _OppBIzDocID
            End Get
            Set(ByVal value As Integer)
                _OppBIzDocID = value
            End Set
        End Property

        Public Property JournalDetails() As String
            Get
                Return _JournalDetails
            End Get
            Set(ByVal value As String)
                _JournalDetails = value
            End Set
        End Property

        Public Property Mode() As Int16
            Get
                Return _Mode
            End Get
            Set(ByVal Value As Int16)
                _Mode = Value
            End Set
        End Property

        Public Property RecurringMode() As Int16
            Get
                Return _RecurringMode
            End Get
            Set(ByVal Value As Int16)
                _RecurringMode = Value
            End Set
        End Property

        Public Property ClientTimeZoneOffset() As Int32
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal value As Int32)
                _ClientTimeZoneOffset = value
            End Set
        End Property

        Private _dtFromDate As DateTime
        Public Property dtFromDate() As DateTime
            Get
                Return _dtFromDate
            End Get
            Set(ByVal value As DateTime)
                _dtFromDate = value
            End Set
        End Property

        Private _dtToDate As DateTime
        Public Property dtTodate() As DateTime
            Get
                Return _dtToDate
            End Get
            Set(ByVal value As DateTime)
                _dtToDate = value
            End Set
        End Property
		
        Public Property RegularSearch As String
        Public Property IsCustomerStatement As Boolean
        Public Property BasedOn As Short '1:Due Date, 2:Invoice Date

        Public Function GetAccountReceivableAging() As DataTable
            Try
                Dim ds As New DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Using connection As New Npgsql.NpgsqlConnection(connString)
                    Dim sqlAdapter As Npgsql.NpgsqlDataAdapter

                    Dim sqlCommand As New Npgsql.NpgsqlCommand
                    sqlCommand.Connection = connection
                    sqlCommand.CommandTimeout = 1800
                    sqlCommand.CommandType = CommandType.StoredProcedure
                    sqlCommand.CommandText = "usp_getaccountreceivableaging"
                    sqlCommand.Parameters.Add(SqlDAL.Add_Parameter("v_numdomainid", DomainID, NpgsqlTypes.NpgsqlDbType.Numeric))
                    sqlCommand.Parameters.Add(SqlDAL.Add_Parameter("v_numdivisionid", IIf(_CompanyID > 0, _CompanyID, DBNull.Value), NpgsqlTypes.NpgsqlDbType.Numeric))
                    sqlCommand.Parameters.Add(SqlDAL.Add_Parameter("v_numusercntid", UserCntID, NpgsqlTypes.NpgsqlDbType.Numeric))
                    sqlCommand.Parameters.Add(SqlDAL.Add_Parameter("v_numaccountclass", _AccountClass, NpgsqlTypes.NpgsqlDbType.Numeric))
                    sqlCommand.Parameters.Add(SqlDAL.Add_Parameter("v_dtfromdate", _dtFromDate, NpgsqlTypes.NpgsqlDbType.Timestamp))
                    sqlCommand.Parameters.Add(SqlDAL.Add_Parameter("v_dttodate", _dtToDate, NpgsqlTypes.NpgsqlDbType.Timestamp))
                    sqlCommand.Parameters.Add(SqlDAL.Add_Parameter("v_clienttimezoneoffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    sqlCommand.Parameters.Add(SqlDAL.Add_Parameter("v_bitcustomerstatement", IsCustomerStatement, NpgsqlTypes.NpgsqlDbType.Boolean))
                    sqlCommand.Parameters.Add(SqlDAL.Add_Parameter("v_tintbasedon", BasedOn, NpgsqlTypes.NpgsqlDbType.Smallint))
                    sqlCommand.Parameters.Add(SqlDAL.Add_Parameter("swv_refcur", DBNull.Value, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))

                    Dim i As Int32 = 0
                    connection.Open()
                    Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                        sqlCommand.ExecuteNonQuery()

                        For Each parm As Npgsql.NpgsqlParameter In sqlCommand.Parameters
                            If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                    Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                    sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                    ds.Tables.Add(parm.Value.ToString())
                                    sqlAdapter.Fill(ds.Tables(i))
                                    i += 1
                                End If
                            End If
                        Next

                        objTransaction.Commit()
                    End Using
                    connection.Close()
                End Using

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetAccountReceivableAgingDetails() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = IIf(_DivisionID > 0, _DivisionID, DBNull.Value)

                arParms(2) = New Npgsql.NpgsqlParameter("@vcFlag", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = _Flag

                arParms(3) = New Npgsql.NpgsqlParameter("@numAccountClass ", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _AccountClass

                arParms(4) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(4).Value = _dtFromDate

                arParms(5) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(5).Value = _dtToDate

                arParms(6) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = ClientTimeZoneOffset

                arParms(7) = New Npgsql.NpgsqlParameter("@tintBasedOn", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(7).Value = BasedOn

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetAccountReceivableAging_Details", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetAccountPayableAgingDetails() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = IIf(_DivisionID > 0, _DivisionID, DBNull.Value)

                arParms(2) = New Npgsql.NpgsqlParameter("@vcFlag", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = _Flag

                arParms(3) = New Npgsql.NpgsqlParameter("@numAccountClass ", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _AccountClass

                arParms(4) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(4).Value = _dtFromDate

                arParms(5) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(5).Value = _dtToDate

                arParms(6) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = ClientTimeZoneOffset

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetAccountPayableAging_Details", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetAccountPayableAging() As DataTable
            Try
                Dim ds As New DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Using connection As New Npgsql.NpgsqlConnection(connString)
                    Dim sqlAdapter As Npgsql.NpgsqlDataAdapter

                    Dim sqlCommand As New Npgsql.NpgsqlCommand
                    sqlCommand.Connection = connection
                    sqlCommand.CommandTimeout = 1800
                    sqlCommand.CommandType = CommandType.StoredProcedure
                    sqlCommand.CommandText = "usp_getaccountpayableaging"
                    sqlCommand.Parameters.Add(SqlDAL.Add_Parameter("v_numdomainid", DomainID, NpgsqlTypes.NpgsqlDbType.Numeric))
                    sqlCommand.Parameters.Add(SqlDAL.Add_Parameter("v_numdivisionid", IIf(_CompanyID > 0, _CompanyID, DBNull.Value), NpgsqlTypes.NpgsqlDbType.Numeric))
                    sqlCommand.Parameters.Add(SqlDAL.Add_Parameter("v_numaccountclass", _AccountClass, NpgsqlTypes.NpgsqlDbType.Numeric))
                    sqlCommand.Parameters.Add(SqlDAL.Add_Parameter("v_dtfromdate", _dtFromDate, NpgsqlTypes.NpgsqlDbType.Timestamp))
                    sqlCommand.Parameters.Add(SqlDAL.Add_Parameter("v_dttodate", _dtToDate, NpgsqlTypes.NpgsqlDbType.Timestamp))
                    sqlCommand.Parameters.Add(SqlDAL.Add_Parameter("v_clienttimezoneoffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    sqlCommand.Parameters.Add(SqlDAL.Add_Parameter("swv_refcur", DBNull.Value, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))

                    Dim i As Int32 = 0
                    connection.Open()
                    Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                        sqlCommand.ExecuteNonQuery()

                        For Each parm As Npgsql.NpgsqlParameter In sqlCommand.Parameters
                            If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                    Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                    sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                    ds.Tables.Add(parm.Value.ToString())
                                    sqlAdapter.Fill(ds.Tables(i))
                                    i += 1
                                End If
                            End If
                        Next

                        objTransaction.Commit()
                    End Using
                    connection.Close()
                End Using

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function DuplicateCheckNo(ByVal numCheckNo As Integer) As Boolean
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numCheckNo", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = numCheckNo

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_DuplicateCheckNo", arParms)
                If CType(ds.Tables(0).Rows(0).Item(0), Integer) > 0 Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetAccountReceivableAging_Invoice() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(10) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = IIf(_CompanyID > 0, _CompanyID, DBNull.Value)

                arParms(2) = New Npgsql.NpgsqlParameter("@vcFlag", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = _Flag

                arParms(3) = New Npgsql.NpgsqlParameter("@numAccountClass ", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _AccountClass

                arParms(4) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(4).Value = _dtFromDate

                arParms(5) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(5).Value = _dtToDate

                arParms(6) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = ClientTimeZoneOffset
				
				arParms(7) = New Npgsql.NpgsqlParameter("@vcRegularSearch", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(7).Value = RegularSearch

                arParms(8) = New Npgsql.NpgsqlParameter("@bitCustomerStatement", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(8).Value = IsCustomerStatement

                arParms(9) = New Npgsql.NpgsqlParameter("@tintBasedOn", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(9).Value = BasedOn

                arParms(10) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(10).Value = Nothing
                arParms(10).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetAccountReceivableAging_Invoice", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetAccountPayableAging_Invoice() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@vcFlag", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(1).Value = _Flag

                arParms(2) = New Npgsql.NpgsqlParameter("@numAccountClass ", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _AccountClass

                arParms(3) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(3).Value = _dtFromDate

                arParms(4) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(4).Value = _dtToDate

                arParms(5) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = ClientTimeZoneOffset

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetAccountPayableAging_Invoice", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace