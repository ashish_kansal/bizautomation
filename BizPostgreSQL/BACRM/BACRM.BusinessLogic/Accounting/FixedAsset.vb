Option Explicit On
Option Strict On

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports Npgsql

Namespace BACRM.BusinessLogic.Accounting
    Public Class FixedAsset
        Inherits BACRM.BusinessLogic.CBusinessBase

        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Siva 	DATE:20-Mar-07
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Siva 	DATE:20-Mar-07
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region

        'Define Private Vairable
        Private _FixedAssetId As Integer
        Private _CompanyId As Integer
        Private _AssetClass As Integer
        Private _AssetName As String
        Private _AssetDescription As String
        Private _Location As String
        Private _SerialNo As String
        Private _DatePurchased As DateTime
        Private _Cost As Decimal
        Private _AppOrDepPercentage As Boolean
        Private _DepreciationPer As Decimal
        Private _AppreciationPer As Decimal
        Private _AssetAccount As Integer
        Private _Vendor As Integer
        Private _FiscalDepOrAppAmt As Decimal
        Private _CurrentValue As Decimal
        Private _Notes As String
        Private _AccountTypeId As Integer
        'Private DomainId As Integer
        Private _CompFilter As String = String.Empty

        Public Property FixedAssetId() As Integer
            Get
                Return _FixedAssetId
            End Get
            Set(ByVal Value As Integer)
                _FixedAssetId = Value
            End Set
        End Property

        Public Property CompanyId() As Integer
            Get
                Return _CompanyId
            End Get
            Set(ByVal value As Integer)
                _CompanyId = value
            End Set
        End Property

        Public Property AssetClass() As Integer
            Get
                Return _AssetClass
            End Get
            Set(ByVal Value As Integer)
                _AssetClass = Value
            End Set
        End Property

        Public Property AssetName() As String
            Get
                Return _AssetName
            End Get
            Set(ByVal Value As String)
                _AssetName = Value
            End Set
        End Property

        Public Property AssetDescription() As String
            Get
                Return _AssetDescription
            End Get
            Set(ByVal Value As String)
                _AssetDescription = Value
            End Set
        End Property

        Public Property Location() As String
            Get
                Return _Location
            End Get
            Set(ByVal Value As String)
                _Location = Value
            End Set
        End Property
        Public Property SerialNo() As String
            Get
                Return _SerialNo
            End Get
            Set(ByVal Value As String)
                _SerialNo = Value
            End Set
        End Property

        Public Property DatePurchased() As DateTime
            Get
                Return _DatePurchased
            End Get
            Set(ByVal Value As DateTime)
                _DatePurchased = Value
            End Set
        End Property

        Public Property Cost() As Decimal
            Get
                Return _Cost
            End Get
            Set(ByVal Value As Decimal)
                _Cost = Value
            End Set
        End Property

        Public Property AppOrDepPercentage() As Boolean
            Get
                Return _AppOrDepPercentage
            End Get
            Set(ByVal Value As Boolean)
                _AppOrDepPercentage = Value
            End Set
        End Property

        Public Property DepreciationPer() As Decimal
            Get
                Return _DepreciationPer
            End Get
            Set(ByVal Value As Decimal)
                _DepreciationPer = Value
            End Set
        End Property

        Public Property AppreciationPer() As Decimal
            Get
                Return _AppreciationPer
            End Get
            Set(ByVal Value As Decimal)
                _AppreciationPer = Value
            End Set
        End Property

        Public Property AssetAccount() As Integer
            Get
                Return _AssetAccount
            End Get
            Set(ByVal Value As Integer)
                _AssetAccount = Value
            End Set
        End Property

        Public Property Vendor() As Integer
            Get
                Return _Vendor
            End Get
            Set(ByVal Value As Integer)
                _Vendor = Value
            End Set
        End Property

        Public Property FiscalDepOrAppAmt() As Decimal
            Get
                Return _FiscalDepOrAppAmt
            End Get
            Set(ByVal Value As Decimal)
                _FiscalDepOrAppAmt = Value
            End Set
        End Property

        Public Property CurrentValue() As Decimal
            Get
                Return _CurrentValue
            End Get
            Set(ByVal Value As Decimal)
                _CurrentValue = Value
            End Set
        End Property

        Public Property Notes() As String
            Get
                Return _Notes
            End Get
            Set(ByVal Value As String)
                _Notes = Value
            End Set
        End Property

        'Public Property UserCntID() As Integer
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal Value As Integer)
        '        UserCntID = Value
        '    End Set
        'End Property

        Public Property AccountTypeId() As Integer
            Get
                Return _AccountTypeId
            End Get
            Set(ByVal value As Integer)
                _AccountTypeId = value
            End Set
        End Property

        ''Public Property DomainId() As Integer
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal value As Integer)
        '        DomainId = value
        '    End Set
        'End Property

        Public Property CompFilter() As String
            Get
                Return _CompFilter
            End Get
            Set(ByVal Value As String)
                _CompFilter = Value
            End Set
        End Property

        Public Function GetChartAcntDetails() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@AcntTypeId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _AccountTypeId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetCheckChartAcntDet", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ListVendor() As DataSet
            Try
                Dim Connection As New NpgsqlConnection(GetConnection.GetConnectionString)
                Dim arParams() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParams(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParams(0).Value = DomainID

                arParams(1) = New Npgsql.NpgsqlParameter("@vcCmpFilter", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParams(1).Value = _CompFilter

                arParams(2) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParams(2).Value = UserCntID

                arParams(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParams(3).Value = Nothing
                arParams(3).Direction = ParameterDirection.InputOutput

                ListVendor = SqlDAL.ExecuteDataset(Connection, "usp_GetCompany", arParams)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveFixedAssetDetails() As Long
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(14) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numFixedAssetId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _FixedAssetId

                arParms(1) = New Npgsql.NpgsqlParameter("@numAssetClass", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _AssetClass

                arParms(2) = New Npgsql.NpgsqlParameter("@varAssetName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(2).Value = _AssetName


                arParms(3) = New Npgsql.NpgsqlParameter("@varAssetDescription", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(3).Value = _AssetDescription

                arParms(4) = New Npgsql.NpgsqlParameter("@strLocation", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(4).Value = _Location

                arParms(5) = New Npgsql.NpgsqlParameter("@varSerialNo", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(5).Value = _SerialNo

                arParms(6) = New Npgsql.NpgsqlParameter("@monCost", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(6).Value = _Cost

                arParms(7) = New Npgsql.NpgsqlParameter("@dtDatePurchased", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(7).Value = _DatePurchased

                arParms(8) = New Npgsql.NpgsqlParameter("@decDepreciationPer", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(8).Value = _DepreciationPer


                arParms(9) = New Npgsql.NpgsqlParameter("@numAssetAccount", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(9).Value = _AssetAccount

                arParms(10) = New Npgsql.NpgsqlParameter("@numVendor", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(10).Value = _Vendor

                arParms(11) = New Npgsql.NpgsqlParameter("@monFiscalDepOrAppAmt", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(11).Value = _FiscalDepOrAppAmt

                arParms(12) = New Npgsql.NpgsqlParameter("@monCurrentValue", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(12).Value = _CurrentValue

                arParms(13) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(13).Value = UserCntID

                arParms(14) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(14).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_SaveFixedAssetDetails", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetFixedAssetDetails() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numFixedAssetId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _FixedAssetId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetFixedAssetDetails", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetFixedAssetDetailsForGrid() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numAssetClass", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = _AssetClass

                arparms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(1).Value = DomainID

                arparms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(2).Value = Nothing
                arparms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_FixedAssetDetails", arparms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace
