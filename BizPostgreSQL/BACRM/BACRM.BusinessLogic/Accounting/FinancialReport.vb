﻿Option Explicit On
Option Strict On

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Namespace BACRM.BusinessLogic.Accounting
    Public Class FinancialReport
        Inherits BACRM.BusinessLogic.CBusinessBase



        Private _FRID As Long
        Public Property FRID() As Long
            Get
                Return _FRID
            End Get
            Set(ByVal value As Long)
                _FRID = value
            End Set
        End Property

        Private _ReportName As String
        Public Property ReportName() As String
            Get
                Return _ReportName
            End Get
            Set(ByVal value As String)
                _ReportName = value
            End Set
        End Property

        Private _FinViewID As Long
        Public Property FinViewID() As Long
            Get
                Return _FinViewID
            End Get
            Set(ByVal value As Long)
                _FinViewID = value
            End Set
        End Property


        Private _DimensionID As Long
        Public Property DimensionID() As Long
            Get
                Return _DimensionID
            End Get
            Set(ByVal value As Long)
                _DimensionID = value
            End Set
        End Property

        Private _DateRange As Integer
        Public Property DateRange() As Integer
            Get
                Return _DateRange
            End Get
            Set(ByVal value As Integer)
                _DateRange = value
            End Set
        End Property

        ''Private DomainId As Long
        ''Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal value As Long)
        '        DomainId = value
        '    End Set
        'End Property

        'Private UserCntID As Long
        'Public Property UserContactID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal value As Long)
        '        UserCntID = value
        '    End Set
        'End Property

        Private _FRDetailID As Long
        Public Property FRDetailID() As Long
            Get
                Return _FRDetailID
            End Get
            Set(ByVal value As Long)
                _FRDetailID = value
            End Set
        End Property

        Private _Value1 As Long
        Public Property Value1() As Long
            Get
                Return _Value1
            End Get
            Set(ByVal value As Long)
                _Value1 = value
            End Set
        End Property

        Private _Value2 As Long
        Public Property Value2() As Long
            Get
                Return _Value2
            End Get
            Set(ByVal value As Long)
                _Value2 = value
            End Set
        End Property

        Private _ValueText As String
        Public Property ValueText() As String
            Get
                Return _ValueText
            End Get
            Set(ByVal value As String)
                _ValueText = value
            End Set
        End Property

        Private _FromDate As Date
        Public Property FromDate() As Date
            Get
                Return _FromDate
            End Get
            Set(ByVal Value As Date)
                _FromDate = Value
            End Set
        End Property

        Private _ToDate As Date
        Public Property ToDate() As Date
            Get
                Return _ToDate
            End Get
            Set(ByVal Value As Date)
                _ToDate = Value
            End Set
        End Property

        Private _AccountClass As Long
        Public Property AccountClass() As Long
            Get
                Return _AccountClass
            End Get
            Set(ByVal value As Long)
                _AccountClass = value
            End Set
        End Property

        Public Property ClientTimeZoneOffset As Integer

        Public Function GetFinancialReports() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numFRID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _FRID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetFinancialReport", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageFinancialReport() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numFRID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _FRID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcReportName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(1).Value = _ReportName

                arParms(2) = New Npgsql.NpgsqlParameter("@numFinancialViewID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _FinViewID

                arParms(3) = New Npgsql.NpgsqlParameter("@numFinancialDimensionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _DimensionID

                arParms(4) = New Npgsql.NpgsqlParameter("@intDateRange", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _DateRange

                arParms(5) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = DomainID

                arParms(6) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = UserCntID

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageFinancialReport", objParam, True)
                _FRID = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

                If _FRID <= 0 Then
                    Return False
                End If
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function DeleteFinancialReport() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numFRID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _FRID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteFinancialReport", arParms)

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Function GetFinancialReportsDetail() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numFRDtlID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _FRDetailID

                arParms(1) = New Npgsql.NpgsqlParameter("@numFRID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _FRID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetFinancialReportDetail", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function



        Public Function ManageFinancialReportDetail() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numFRDtlID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _FRDetailID

                arParms(1) = New Npgsql.NpgsqlParameter("@numFRID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _FRID

                arParms(2) = New Npgsql.NpgsqlParameter("@numValue1", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _Value1

                arParms(3) = New Npgsql.NpgsqlParameter("@numValue2", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _Value2

                arParms(4) = New Npgsql.NpgsqlParameter("@vcValue1", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(4).Value = _ValueText

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageFinancialReportDetail", objParam, True)
                _FRID = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

                If _FRID <= 0 Then
                    Return False
                End If
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function DeleteFinancialReportDetail() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numFRDtlID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _FRDetailID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteFinancialReportDetail", arParms)

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Function GetAccountsReports(ByVal tintReportType As Short) As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = DomainID

                arparms(1) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(1).Value = _FromDate

                arparms(2) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(2).Value = _ToDate

                arparms(3) = New Npgsql.NpgsqlParameter("@tintReportType", NpgsqlTypes.NpgsqlDbType.Integer)
                arparms(3).Value = tintReportType

                arparms(4) = New Npgsql.NpgsqlParameter("@numAccountClass", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(4).Value = _AccountClass

                arparms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(5).Value = Nothing
                arparms(5).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_AccountsReports", arparms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetTaxReports() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arparms(0).Value = DomainID

                arparms(1) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(1).Value = _FromDate

                arparms(2) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(2).Value = _ToDate

                arparms(3) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arparms(3).Value = ClientTimeZoneOffset

                arparms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(4).Value = Nothing
                arparms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_TaxReports", arparms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace