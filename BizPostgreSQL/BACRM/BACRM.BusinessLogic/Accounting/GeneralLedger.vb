Option Explicit On
Option Strict On
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Namespace BACRM.BusinessLogic.Accounting
    Public Class GeneralLedger
        Inherits BACRM.BusinessLogic.CBusinessBase

        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Siva 	DATE:23-Mar-07
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Siva 	DATE:23-Mar-07
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region


        'Define Private Vairable
        'Private DomainId As Integer
        Private _FromDate As Date
        Private _ToDate As Date
        Private _ParentAccountId As Integer
        Private _ChartAcntId As Integer
        Private _AccountTypeId As Integer
        Private _Year As Integer


        ''Public Property DomainId() As Integer
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal value As Integer)
        '        DomainId = value
        '    End Set
        'End Property

        Public Property FromDate() As Date
            Get
                Return _FromDate
            End Get
            Set(ByVal Value As Date)
                _FromDate = Value
            End Set
        End Property

        Public Property ToDate() As Date
            Get
                Return _ToDate
            End Get
            Set(ByVal Value As Date)
                _ToDate = Value
            End Set
        End Property

        Public Property ParentAccountId() As Integer
            Get
                Return _ParentAccountId
            End Get
            Set(ByVal Value As Integer)
                _ParentAccountId = Value
            End Set
        End Property

        Public Property ChartAcntId() As Integer
            Get
                Return _ChartAcntId
            End Get
            Set(ByVal value As Integer)
                _ChartAcntId = value
            End Set
        End Property

        Public Property AccountTypeId() As Integer
            Get
                Return _AccountTypeId
            End Get
            Set(ByVal value As Integer)
                _AccountTypeId = value
            End Set
        End Property

        Public Property Year() As Integer
            Get
                Return _Year
            End Get
            Set(ByVal value As Integer)
                _Year = value
            End Set
        End Property

        Private _AccountIds As String
        Public Property AccountIds() As String
            Get
                Return _AccountIds
            End Get
            Set(ByVal value As String)
                _AccountIds = value
            End Set
        End Property

        Private _DivisionID As Long
        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal value As Long)
                _DivisionID = value
            End Set
        End Property

        Private _ReconStatus As Char
        Public Property ReconStatus() As Char
            Get
                Return _ReconStatus
            End Get
            Set(ByVal value As Char)
                _ReconStatus = value
            End Set
        End Property

        Private _CurrentPage As Integer = 0
        Public Property CurrentPage() As Integer
            Get
                Return _CurrentPage
            End Get
            Set(ByVal Value As Integer)
                _CurrentPage = Value
            End Set
        End Property

        Private _PageSize As Integer = 0
        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal Value As Integer)
                _PageSize = Value
            End Set
        End Property

        Private _TotalRecords As Integer = 0
        Public Property TotalRecords() As Integer
            Get
                Return _TotalRecords
            End Get
            Set(ByVal Value As Integer)
                _TotalRecords = Value
            End Set
        End Property

        Private _ItemCode As Integer = 0
        Public Property ItemCode() As Integer
            Get
                Return _ItemCode
            End Get
            Set(ByVal Value As Integer)
                _ItemCode = Value
            End Set
        End Property

        Private _ClientZoneOffsetTime As Integer
        Public Property ClientZoneOffsetTime() As Integer
            Get
                Return _ClientZoneOffsetTime
            End Get
            Set(ByVal Value As Integer)
                _ClientZoneOffsetTime = Value
            End Set
        End Property

        Private _byteMode As Short
        Public Property byteMode() As Short
            Get
                Return _byteMode
            End Get
            Set(ByVal Value As Short)
                _byteMode = Value
            End Set
        End Property
        Private _TransactionID As Long
        Public Property TransactionID() As Long
            Get
                Return _TransactionID
            End Get
            Set(ByVal value As Long)
                _TransactionID = value
            End Set
        End Property

        Public Property AccountClass As Long
        Public Property FilterTransactionType As Short
        Public Property FilterName As String
        Public Property FilterDescription As String
        Public Property FilterSplit As String
        Public Property FilterAmount As String
        Public Property FilterNarration As String
        'Private _ClientTimeZoneOffset As Integer
        'Public Property ClientTimeZoneOffset() As Integer
        '    Get
        '        Return _ClientTimeZoneOffset
        '    End Get
        '    Set(ByVal value As Integer)
        '        _ClientTimeZoneOffset = value
        '    End Set
        'End Property

        'Obsolete
        Public Function GetJournalEntries() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(23) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = DomainID

                arparms(1) = New Npgsql.NpgsqlParameter("@vcAccountId", NpgsqlTypes.NpgsqlDbType.VarChar, 4000)
                arparms(1).Value = _AccountIds

                arparms(2) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(2).Value = _FromDate

                arparms(3) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(3).Value = _ToDate

                arparms(4) = New Npgsql.NpgsqlParameter("@vcTranType", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arparms(4).Value = ""

                arparms(5) = New Npgsql.NpgsqlParameter("@varDescription", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arparms(5).Value = ""

                arparms(6) = New Npgsql.NpgsqlParameter("@CompanyName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arparms(6).Value = ""

                arparms(7) = New Npgsql.NpgsqlParameter("@vcBizPayment", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arparms(7).Value = ""

                arparms(8) = New Npgsql.NpgsqlParameter("@vcCheqNo", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arparms(8).Value = ""

                arparms(9) = New Npgsql.NpgsqlParameter("@vcBizDocID", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arparms(9).Value = ""

                arparms(10) = New Npgsql.NpgsqlParameter("@vcTranRef", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arparms(10).Value = ""

                arparms(11) = New Npgsql.NpgsqlParameter("@vcTranDesc", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arparms(11).Value = ""

                arparms(12) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(12).Value = _DivisionID

                arparms(13) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer) 'Param added by Chintan (Since Date and Time must be displayed w.r.t. the client machine)
                arparms(13).Value = Common.CCommon.ToInteger(System.Web.HttpContext.Current.Session("ClientMachineUTCTimeOffset"))

                arparms(14) = New Npgsql.NpgsqlParameter("@charReconFilter", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arparms(14).Value = _ReconStatus

                arparms(15) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arparms(15).Value = _byteMode

                arparms(16) = New Npgsql.NpgsqlParameter("@numItemId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(16).Value = _ItemCode

                arparms(17) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arparms(17).Value = _CurrentPage

                arparms(18) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arparms(18).Value = _PageSize

                arparms(19) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arparms(19).Direction = ParameterDirection.InputOutput
                arparms(19).Value = _TotalRecords

                arparms(20) = New Npgsql.NpgsqlParameter("@TransactionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(20).Value = _TransactionID

                arparms(21) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(21).Value = Nothing
                arparms(21).Direction = ParameterDirection.InputOutput

                arparms(22) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(22).Value = Nothing
                arparms(22).Direction = ParameterDirection.InputOutput

                arparms(23) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(23).Value = Nothing
                arparms(23).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arparms.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "Usp_GetJournalEntries", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(19).Value)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetGeneralLedger_VirtualLoad() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(27) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = DomainID

                arparms(1) = New Npgsql.NpgsqlParameter("@vcAccountId", NpgsqlTypes.NpgsqlDbType.VarChar, 4000)
                arparms(1).Value = _AccountIds

                arparms(2) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(2).Value = _FromDate

                arparms(3) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(3).Value = _ToDate

                arparms(4) = New Npgsql.NpgsqlParameter("@vcTranType", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arparms(4).Value = ""

                arparms(5) = New Npgsql.NpgsqlParameter("@varDescription", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arparms(5).Value = ""

                arparms(6) = New Npgsql.NpgsqlParameter("@CompanyName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arparms(6).Value = ""

                arparms(7) = New Npgsql.NpgsqlParameter("@vcBizPayment", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arparms(7).Value = ""

                arparms(8) = New Npgsql.NpgsqlParameter("@vcCheqNo", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arparms(8).Value = ""

                arparms(9) = New Npgsql.NpgsqlParameter("@vcBizDocID", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arparms(9).Value = ""

                arparms(10) = New Npgsql.NpgsqlParameter("@vcTranRef", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arparms(10).Value = ""

                arparms(11) = New Npgsql.NpgsqlParameter("@vcTranDesc", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arparms(11).Value = ""

                arparms(12) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(12).Value = _DivisionID

                arparms(13) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer) 'Param added by Chintan (Since Date and Time must be displayed w.r.t. the client machine)
                arparms(13).Value = Common.CCommon.ToInteger(System.Web.HttpContext.Current.Session("ClientMachineUTCTimeOffset"))

                arparms(14) = New Npgsql.NpgsqlParameter("@charReconFilter", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arparms(14).Value = _ReconStatus

                arparms(15) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arparms(15).Value = _byteMode

                arparms(16) = New Npgsql.NpgsqlParameter("@numItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(16).Value = _ItemCode

                arparms(17) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arparms(17).Value = _CurrentPage

                arparms(18) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arparms(18).Value = _PageSize

                arparms(19) = New Npgsql.NpgsqlParameter("@numAccountClass", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(19).Value = AccountClass

                arparms(20) = New Npgsql.NpgsqlParameter("@tintFilterTransactionType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arparms(20).Value = FilterTransactionType

                arparms(21) = New Npgsql.NpgsqlParameter("@vcFiterName", NpgsqlTypes.NpgsqlDbType.VarChar)
                arparms(21).Value = FilterName

                arparms(22) = New Npgsql.NpgsqlParameter("@vcFiterDescription", NpgsqlTypes.NpgsqlDbType.VarChar)
                arparms(22).Value = FilterDescription

                arparms(23) = New Npgsql.NpgsqlParameter("@vcFiterSplit", NpgsqlTypes.NpgsqlDbType.VarChar)
                arparms(23).Value = FilterSplit

                arparms(24) = New Npgsql.NpgsqlParameter("@vcFiterAmount", NpgsqlTypes.NpgsqlDbType.VarChar)
                arparms(24).Value = FilterAmount

                arparms(25) = New Npgsql.NpgsqlParameter("@vcFiterNarration", NpgsqlTypes.NpgsqlDbType.VarChar)
                arparms(25).Value = FilterNarration

                arparms(26) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(26).Value = Nothing
                arparms(26).Direction = ParameterDirection.InputOutput

                arparms(27) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(27).Value = Nothing
                arparms(27).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetGeneralLedger_Virtual", arparms)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetJournalEntriesReport_VirtualLoad() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(20) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = DomainID

                arparms(1) = New Npgsql.NpgsqlParameter("@vcAccountId", NpgsqlTypes.NpgsqlDbType.VarChar, 4000)
                arparms(1).Value = _AccountIds

                arparms(2) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(2).Value = _FromDate

                arparms(3) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(3).Value = _ToDate

                arparms(4) = New Npgsql.NpgsqlParameter("@vcTranType", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arparms(4).Value = ""

                arparms(5) = New Npgsql.NpgsqlParameter("@varDescription", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arparms(5).Value = ""

                arparms(6) = New Npgsql.NpgsqlParameter("@CompanyName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arparms(6).Value = ""

                arparms(7) = New Npgsql.NpgsqlParameter("@vcBizPayment", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arparms(7).Value = ""

                arparms(8) = New Npgsql.NpgsqlParameter("@vcCheqNo", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arparms(8).Value = ""

                arparms(9) = New Npgsql.NpgsqlParameter("@vcBizDocID", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arparms(9).Value = ""

                arparms(10) = New Npgsql.NpgsqlParameter("@vcTranRef", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arparms(10).Value = ""

                arparms(11) = New Npgsql.NpgsqlParameter("@vcTranDesc", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arparms(11).Value = ""

                arparms(12) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(12).Value = _DivisionID

                arparms(13) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arparms(13).Value = Common.CCommon.ToInteger(System.Web.HttpContext.Current.Session("ClientMachineUTCTimeOffset"))

                arparms(14) = New Npgsql.NpgsqlParameter("@charReconFilter", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arparms(14).Value = _ReconStatus

                arparms(15) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arparms(15).Value = _byteMode

                arparms(16) = New Npgsql.NpgsqlParameter("@numItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(16).Value = _ItemCode

                arparms(17) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arparms(17).Value = _CurrentPage

                arparms(18) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arparms(18).Value = _PageSize

                arparms(19) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(19).Value = Nothing
                arparms(19).Direction = ParameterDirection.InputOutput

                arparms(20) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(20).Value = Nothing
                arparms(20).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetJournalEntries_Virtual", arparms)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetGeneralJournalDetailsReport() As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = DomainID

                arparms(1) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(1).Value = _FromDate

                arparms(2) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(2).Value = _ToDate

                arparms(3) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arparms(3).Value = _CurrentPage

                arparms(4) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arparms(4).Value = _PageSize

                arparms(5) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arparms(5).Value = _byteMode

                arparms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(6).Value = Nothing
                arparms(6).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetGenralJournalDetailsReport", arparms)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetGLTransactionDetailById() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = DomainID

                arparms(1) = New Npgsql.NpgsqlParameter("@numTransactionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(1).Value = _TransactionID

                arparms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(2).Value = Nothing
                arparms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetGLTransactionDetailById", arparms)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetChartAcntDetailsForTransaction() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numAccountId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = _ChartAcntId

                arparms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(1).Value = DomainID

                arparms(2) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(2).Value = _FromDate

                arparms(3) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(3).Value = _ToDate

                arparms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(4).Value = Nothing
                arparms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetChartAcntDetailsForTransaction", arparms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetChildCategory() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = DomainID

                arparms(1) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(1).Value = _FromDate

                arparms(2) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(2).Value = _ToDate

                arparms(3) = New Npgsql.NpgsqlParameter("@numParntAcntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(3).Value = _ParentAccountId

                arparms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(4).Value = Nothing
                arparms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetChartAcntChildDetails", arparms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetChartAcntBalanceAmt() As Decimal
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numAccountId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = _ChartAcntId

                arparms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(1).Value = DomainID

                arparms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(2).Value = Nothing
                arparms(2).Direction = ParameterDirection.InputOutput

                Return CDec(SqlDAL.ExecuteScalar(connString, "USP_GetOpeningBalance", arparms))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCurrentOpeningBalanceForGeneralLedgerDetails() As Decimal
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numAccountId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = _ChartAcntId

                arparms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(1).Value = DomainID

                arparms(2) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(2).Value = _FromDate

                arparms(3) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(3).Value = _ToDate

                arparms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(4).Value = Nothing
                arparms(4).Direction = ParameterDirection.InputOutput

                Return CDec(SqlDAL.ExecuteScalar(connString, "USP_GetCurrentOpeningBalanceForGeneralLedgerDetails", arparms))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCOAforItemsJournalEntries() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = DomainID

                arparms(1) = New Npgsql.NpgsqlParameter("@numItemId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(1).Value = _ItemCode

                arparms(2) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arparms(2).Value = _byteMode

                arparms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(3).Value = Nothing
                arparms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetCOAforItemsJournalEntries", arparms)
                Return ds.Tables(0)


            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetChartOfAcntDetails() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim ds As DataSet

                arparms(0) = New Npgsql.NpgsqlParameter("@numAcntTypeId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = _AccountTypeId

                arparms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(1).Value = DomainID

                arparms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(2).Value = Nothing
                arparms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetChartOfAcntDetails", arparms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetFiscalDate() As DateTime
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim ds As DataSet

                arparms(0) = New Npgsql.NpgsqlParameter("@numYear", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = _Year

                arparms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(1).Value = DomainID

                arparms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(2).Value = Nothing
                arparms(2).Direction = ParameterDirection.InputOutput

                Return CDate(SqlDAL.ExecuteScalar(connString, "USP_GetFiscalYearStartDate", arparms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetAccountDiagnosis() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur3", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur4", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur5", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur6", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur7", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur8", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur9", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur10", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur11", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur12", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur13", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur14", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur15", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur16", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur17", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur18", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_AccountDiagnosis", sqlParams.ToArray())

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class
End Namespace
