Option Explicit On
Option Strict On

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Namespace BACRM.BusinessLogic.Accounting
    Public Class CashCreditCard
        Inherits BACRM.BusinessLogic.CBusinessBase

        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Siva 	DATE:08-Feb-07
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Siva 	DATE:08-Feb-07
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region

        'Define Private Vairable
        'Private DomainId As Integer
        Private _AcntType As Integer
        Private _strWhereCondition As String
        Private _AccountId As Integer
        Private _Entry_Date As Date = New Date(1753, 1, 1)
        Private _numAmount As Decimal
        Private _JournalId As Integer
        Private _CashCreditCardId As Integer
        Private _JournalDetails As String = String.Empty
        Private _Mode As Int16
        Private _DivisionId As Integer
        Private _PurchaseId As Integer
        Private _Memo As String
        Private _bitMoneyOut As Boolean
        Private _ReferenceNo As String
        Private _bitChargeBack As Boolean
        Private _RecurringMode As Int16
        Private _RecurringId As Integer

        Private _Description As String
        Public Property Description() As String
            Get
                Return _Description
            End Get
            Set(ByVal value As String)
                _Description = value
            End Set
        End Property

        Public Property PurchaseId() As Integer
            Get
                Return _PurchaseId
            End Get
            Set(ByVal Value As Integer)
                _PurchaseId = Value
            End Set
        End Property

        ''Public Property DomainId() As Integer
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Integer)
        '        DomainId = Value
        '    End Set
        'End Property

        Public Property AcntType() As Integer
            Get
                Return _AcntType
            End Get
            Set(ByVal Value As Integer)
                _AcntType = Value
            End Set
        End Property

        Public Property strWhereCondition() As String
            Get
                Return _strWhereCondition
            End Get
            Set(ByVal value As String)
                _strWhereCondition = value
            End Set
        End Property

        Public Property AccountId() As Integer
            Get
                Return _AccountId
            End Get
            Set(ByVal Value As Integer)
                _AccountId = Value
            End Set
        End Property

        Public Property Entry_Date() As DateTime
            Get
                Return _Entry_Date
            End Get
            Set(ByVal Value As DateTime)
                _Entry_Date = Value
            End Set
        End Property

        Public Property numAmount() As Decimal
            Get
                Return _numAmount
            End Get
            Set(ByVal Value As Decimal)
                _numAmount = Value
            End Set
        End Property

        Public Property JournalId() As Integer
            Get
                Return _JournalId
            End Get
            Set(ByVal Value As Integer)
                _JournalId = Value
            End Set
        End Property

        Public Property CashCreditCardId() As Integer
            Get
                Return _CashCreditCardId
            End Get
            Set(ByVal Value As Integer)
                _CashCreditCardId = Value
            End Set
        End Property

        Public Property JournalDetails() As String
            Get
                Return _JournalDetails
            End Get
            Set(ByVal value As String)
                _JournalDetails = value
            End Set
        End Property

        Public Property Mode() As Int16
            Get
                Return _Mode
            End Get
            Set(ByVal Value As Int16)
                _Mode = Value
            End Set
        End Property

        Public Property DivisionId() As Integer
            Get
                Return _DivisionId
            End Get
            Set(ByVal Value As Integer)
                _DivisionId = Value
            End Set
        End Property

        Public Property Memo() As String
            Get
                Return _Memo
            End Get
            Set(ByVal Value As String)
                _Memo = Value
            End Set
        End Property

        Public Property bitMoneyOut() As Boolean
            Get
                Return _bitMoneyOut

            End Get
            Set(ByVal Value As Boolean)
                _bitMoneyOut = Value
            End Set
        End Property

        Public Property ReferenceNo() As String
            Get
                Return _ReferenceNo

            End Get
            Set(ByVal Value As String)
                _ReferenceNo = Value
            End Set
        End Property

        Public Property bitChargeBack() As Boolean
            Get
                Return _bitChargeBack

            End Get
            Set(ByVal Value As Boolean)
                _bitChargeBack = Value
            End Set
        End Property

        Public Property RecurringMode() As Int16
            Get
                Return _RecurringMode
            End Get
            Set(ByVal value As Int16)
                _RecurringMode = value
            End Set
        End Property

        Public Property RecurringId() As Integer
            Get
                Return _RecurringId
            End Get
            Set(ByVal value As Integer)
                _RecurringId = value
            End Set
        End Property

        'Public Property UserCntID() As Integer
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal value As Integer)
        '        UserCntID = value
        '    End Set
        'End Property

        Public Function GetCheckOpeningBalanceForCashCredit() As Decimal
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim ldecOpeningBal As Decimal

                arParms(0) = New Npgsql.NpgsqlParameter("@numAccountId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _AccountId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ldecOpeningBal = CDec(SqlDAL.ExecuteScalar(connString, "USP_GetCheckOpeningBalance", arParms))
                Return ldecOpeningBal
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetMainCashCreditCardId() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim lintTransactionId As Long

                arParms(0) = New Npgsql.NpgsqlParameter("@numCashCreditCardId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CashCreditCardId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                lintTransactionId = CLng(SqlDAL.ExecuteScalar(connString, "USP_GetCashCreditCardMainDetailsId", arParms))
                Return lintTransactionId
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCashCreditCardDetails() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numCheckId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CashCreditCardId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetCashCreditCardDetails", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCashCreditCardJournalDetails() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numJournalId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _JournalId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetCashCreditCardJournalDetails", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetJournalId() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numCashCreditCardId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CashCreditCardId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_GetJournalId", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class

End Namespace

