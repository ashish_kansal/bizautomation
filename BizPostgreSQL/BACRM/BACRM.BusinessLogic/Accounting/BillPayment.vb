﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic
Imports Npgsql

Public Class BillPayment
    Inherits BACRM.BusinessLogic.CBusinessBase


    Private _BillPaymentID As Long
    Public Property BillPaymentID() As Long
        Get
            Return _BillPaymentID
        End Get
        Set(ByVal value As Long)
            _BillPaymentID = value
        End Set
    End Property


    Private _PaymentDate As DateTime
    Public Property PaymentDate() As DateTime
        Get
            Return _PaymentDate
        End Get
        Set(ByVal value As DateTime)
            _PaymentDate = value
        End Set
    End Property


    Private _AccountID As Long
    Public Property AccountID() As Long
        Get
            Return _AccountID
        End Get
        Set(ByVal value As Long)
            _AccountID = value
        End Set
    End Property


    Private _PaymentMethod As Long
    Public Property PaymentMethod() As Long
        Get
            Return _PaymentMethod
        End Get
        Set(ByVal value As Long)
            _PaymentMethod = value
        End Set
    End Property

    Private _DivisionID As Long
    Public Property DivisionID() As Long
        Get
            Return _DivisionID
        End Get
        Set(ByVal value As Long)
            _DivisionID = value
        End Set
    End Property


    Private _ReturnHeaderID As Long
    Public Property ReturnHeaderID() As Long
        Get
            Return _ReturnHeaderID
        End Get
        Set(ByVal value As Long)
            _ReturnHeaderID = value
        End Set
    End Property

    Private _strXml As String
    Public Property strXml() As String
        Get
            Return _strXml
        End Get
        Set(ByVal Value As String)
            _strXml = Value
        End Set
    End Property
    Private _ClientTimeZoneOffset As Int32
    Public Property ClientTimeZoneOffset() As Int32
        Get
            Return _ClientTimeZoneOffset
        End Get
        Set(ByVal value As Int32)
            _ClientTimeZoneOffset = value
        End Set
    End Property

    Private _Mode As Int32
    Public Property Mode() As Int16
        Get
            Return _Mode
        End Get
        Set(ByVal Value As Int16)
            _Mode = Value
        End Set
    End Property

    Private _PaymentAmount As Decimal
    Public Property PaymentAmount() As Decimal
        Get
            Return _PaymentAmount
        End Get
        Set(ByVal Value As Decimal)
            _PaymentAmount = Value
        End Set
    End Property

    Private _CurrencyID As Long
    Public Property CurrencyID() As Long
        Get
            Return _CurrencyID
        End Get
        Set(ByVal value As Long)
            _CurrencyID = value
        End Set
    End Property
    Private _ExchangeRate As Double
    Public Property ExchangeRate() As Double
        Get
            Return _ExchangeRate
        End Get
        Set(ByVal value As Double)
            _ExchangeRate = value
        End Set
    End Property

    Private _CurrentPage As Integer
    Private _PageSize As Integer
    Private _TotalRecords As Integer

    Public Property TotalRecords() As Integer
        Get
            Return _TotalRecords
        End Get
        Set(ByVal Value As Integer)
            _TotalRecords = Value
        End Set
    End Property

    Public Property PageSize() As Integer
        Get
            Return _PageSize
        End Get
        Set(ByVal Value As Integer)
            _PageSize = Value
        End Set
    End Property

    Public Property CurrentPage() As Integer
        Get
            Return _CurrentPage
        End Get
        Set(ByVal Value As Integer)
            _CurrentPage = Value
        End Set
    End Property

    Private _AccountClass As Long
    Public Property AccountClass() As Long
        Get
            Return _AccountClass
        End Get
        Set(ByVal value As Long)
            _AccountClass = value
        End Set
    End Property

    Private _OppBizDocsID As Long
    Public Property OppBizDocsID() As Long
        Get
            Return _OppBizDocsID
        End Get
        Set(ByVal value As Long)
            _OppBizDocsID = value
        End Set
    End Property

    Private _BillID As String
    Public Property BillID() As String
        Get
            Return _BillID
        End Get
        Set(ByVal value As String)
            _BillID = value
        End Set
    End Property

    Private _numAmount As Double
    Public Property Amount() As Double
        Get
            Return _numAmount
        End Get
        Set(ByVal value As Double)
            _numAmount = value
        End Set
    End Property

    Private _SortColumn As String = ""

    Public Property SortColumn() As String
        Get
            Return _SortColumn
        End Get
        Set(ByVal Value As String)
            _SortColumn = Value
        End Set
    End Property

    Private _columnSortOrder As String = ""

    Public Property columnSortOrder() As String
        Get
            Return _columnSortOrder
        End Get
        Set(ByVal Value As String)
            _columnSortOrder = Value
        End Set
    End Property

    Private _strCondition As String = ""

    Public Property strCondition() As String
        Get
            Return _strCondition
        End Get
        Set(ByVal Value As String)
            _strCondition = Value
        End Set
    End Property


    Function GetBillPaymentDetail() As DataSet
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Dim ds As DataSet
            Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

            With sqlParams

                .Add(SqlDAL.Add_Parameter("@numBillPaymentID", _BillPaymentID, NpgsqlTypes.NpgsqlDbType.Bigint))

                .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))

                .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", _ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))

                .Add(SqlDAL.Add_Parameter("@numDivisionID", _DivisionID, NpgsqlTypes.NpgsqlDbType.Bigint))

                .Add(SqlDAL.Add_Parameter("@numCurrencyID", _CurrencyID, NpgsqlTypes.NpgsqlDbType.Bigint))

                .Add(SqlDAL.Add_Parameter("@CurrentPage", _CurrentPage, NpgsqlTypes.NpgsqlDbType.Bigint))

                .Add(SqlDAL.Add_Parameter("@PageSize", _PageSize, NpgsqlTypes.NpgsqlDbType.Bigint))

                .Add(SqlDAL.Add_Parameter("@TotRecs", _TotalRecords, NpgsqlTypes.NpgsqlDbType.Bigint, 9, ParameterDirection.InputOutput))

                .Add(SqlDAL.Add_Parameter("@numAccountClass", _AccountClass, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                .Add(SqlDAL.Add_Parameter("@SortColumn", _SortColumn, NpgsqlTypes.NpgsqlDbType.Varchar, 50))

                .Add(SqlDAL.Add_Parameter("@columnSortOrder", _columnSortOrder, NpgsqlTypes.NpgsqlDbType.Varchar, 10))

                .Add(SqlDAL.Add_Parameter("@strCondition", _strCondition, NpgsqlTypes.NpgsqlDbType.Varchar, 2000))

                .Add(SqlDAL.Add_Parameter("SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, ParameterDirection.InputOutput))

                .Add(SqlDAL.Add_Parameter("SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, ParameterDirection.InputOutput))
            End With

            Dim objParam() As Object = sqlParams.ToArray()

            ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetBillPaymentDetails", objParam, True)
            _TotalRecords = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(7).Value)

            Return ds
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function ManageBillPayment() As Boolean
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Dim ds As DataSet
            Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

            With sqlParams


                .Add(SqlDAL.Add_Parameter("@numBillPaymentID", _BillPaymentID, NpgsqlTypes.NpgsqlDbType.Bigint, 9, ParameterDirection.InputOutput))

                .Add(SqlDAL.Add_Parameter("@dtPaymentDate", _PaymentDate, NpgsqlTypes.NpgsqlDbType.Timestamp))

                .Add(SqlDAL.Add_Parameter("@numAccountID", _AccountID, NpgsqlTypes.NpgsqlDbType.Bigint))

                .Add(SqlDAL.Add_Parameter("@numPaymentMethod", _PaymentMethod, NpgsqlTypes.NpgsqlDbType.Bigint))

                .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))

                .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))

                .Add(SqlDAL.Add_Parameter("@strItems", _strXml, NpgsqlTypes.NpgsqlDbType.Text))

                .Add(SqlDAL.Add_Parameter("@tintMode", _Mode, NpgsqlTypes.NpgsqlDbType.Integer))

                .Add(SqlDAL.Add_Parameter("@monPaymentAmount", _PaymentAmount, NpgsqlTypes.NpgsqlDbType.Numeric))

                .Add(SqlDAL.Add_Parameter("@numDivisionID", _DivisionID, NpgsqlTypes.NpgsqlDbType.Bigint))

                .Add(SqlDAL.Add_Parameter("@numReturnHeaderID", _ReturnHeaderID, NpgsqlTypes.NpgsqlDbType.Bigint))

                .Add(SqlDAL.Add_Parameter("@numCurrencyID", _CurrencyID, NpgsqlTypes.NpgsqlDbType.Bigint))

                .Add(SqlDAL.Add_Parameter("@fltExchangeRate", _ExchangeRate, NpgsqlTypes.NpgsqlDbType.Numeric))

                .Add(SqlDAL.Add_Parameter("@numAccountClass", _AccountClass, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

            End With
            Dim objParam() As Object
            objParam = sqlParams.ToArray()
            SqlDAL.ExecuteNonQuery(connString, "USP_ManageBillPayment", objParam, True)
            _BillPaymentID = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function ManageBillPayment(ByVal objTransaction As NpgsqlTransaction) As Boolean
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Dim ds As DataSet
            Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(13) {}

            arParms(0) = New Npgsql.NpgsqlParameter("@numBillPaymentID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
            arParms(0).Direction = ParameterDirection.InputOutput
            arParms(0).Value = _BillPaymentID

            arParms(1) = New Npgsql.NpgsqlParameter("@dtPaymentDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
            arParms(1).Value = _PaymentDate

            arParms(2) = New Npgsql.NpgsqlParameter("@numAccountID", NpgsqlTypes.NpgsqlDbType.Bigint)
            arParms(2).Value = _AccountID

            arParms(3) = New Npgsql.NpgsqlParameter("@numPaymentMethod", NpgsqlTypes.NpgsqlDbType.Bigint)
            arParms(3).Value = _PaymentMethod

            arParms(4) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint)
            arParms(4).Value = UserCntID

            arParms(5) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
            arParms(5).Value = DomainID

            arParms(6) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.Text)
            arParms(6).Value = _strXml

            arParms(7) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Integer)
            arParms(7).Value = _Mode

            arParms(8) = New Npgsql.NpgsqlParameter("@monPaymentAmount", NpgsqlTypes.NpgsqlDbType.Numeric)
            arParms(8).Value = _PaymentAmount

            arParms(9) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.Bigint)
            arParms(9).Value = _DivisionID

            arParms(10) = New Npgsql.NpgsqlParameter("@numReturnHeaderID", NpgsqlTypes.NpgsqlDbType.Bigint)
            arParms(10).Value = _ReturnHeaderID

            arParms(11) = New Npgsql.NpgsqlParameter("@numCurrencyID", NpgsqlTypes.NpgsqlDbType.Bigint)
            arParms(11).Value = _CurrencyID

            arParms(12) = New Npgsql.NpgsqlParameter("@fltExchangeRate", NpgsqlTypes.NpgsqlDbType.Numeric)
            arParms(12).Value = _ExchangeRate

            arParms(13) = New Npgsql.NpgsqlParameter("@numAccountClass", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
            arParms(13).Value = _AccountClass

            Dim objParam() As Object
            objParam = arParms.ToArray()

            If Not objTransaction Is Nothing Then
                SqlDAL.ExecuteNonQuery(objTransaction, "USP_ManageBillPayment", objParam, True)
            Else
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageBillPayment", objParam, True)
            End If

            _BillPaymentID = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function DeleteBillPaymentHeader() As Boolean
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Dim ds As DataSet
            Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

            With sqlParams

                .Add(SqlDAL.Add_Parameter("@numBillPaymentID", _BillPaymentID, NpgsqlTypes.NpgsqlDbType.Bigint, 9, ParameterDirection.InputOutput))

                .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))

            End With
            SqlDAL.ExecuteNonQuery(connString, "USP_DeleteBillPaymentHeader", sqlParams.ToArray())

            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function CheckBizDocAmountsForPayBill() As DataSet
        Try

            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

            arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.Bigint)
            arParms(0).Value = _DivisionID

            arParms(1) = New Npgsql.NpgsqlParameter("@numOppBizDocId", NpgsqlTypes.NpgsqlDbType.Bigint)
            arParms(1).Value = _OppBizDocsID

            arParms(2) = New Npgsql.NpgsqlParameter("@numBillID", NpgsqlTypes.NpgsqlDbType.Bigint)
            arParms(2).Value = Convert.ToInt64(_BillID)

            arParms(3) = New Npgsql.NpgsqlParameter("@numAmount", NpgsqlTypes.NpgsqlDbType.Numeric)
            arParms(3).Value = _numAmount

            arParms(4) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
            arParms(4).Value = DomainID

            arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
            arParms(5).Value = Nothing
            arParms(5).Direction = ParameterDirection.InputOutput

            Dim ds As DataSet
            ds = SqlDAL.ExecuteDataset(connString, "USP_CheckAndBilledAmount", arParms)
            Return ds

        Catch ex As Exception
            Throw ex
        End Try
    End Function




End Class