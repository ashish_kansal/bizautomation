﻿Option Explicit On
Option Strict On

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Web.UI.WebControls
Namespace BACRM.BusinessLogic.Accounting
    Public Class MultiCompany
        Inherits BACRM.BusinessLogic.CBusinessBase


        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Siva 	DATE:26-Apr-07
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Siva 	DATE:26-Apr-07
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region

        'Define Private Vairable

        Dim _SubscriberID As Integer
        Public Property SubscriberID() As Integer
            Get
                Return _SubscriberID
            End Get
            Set(ByVal value As Integer)
                _SubscriberID = value
            End Set
        End Property
        Dim _NewDomainId As Integer
        Public Property NewDomainId() As Integer
            Get
                Return _NewDomainId
            End Get
            Set(ByVal value As Integer)
                _NewDomainId = value
            End Set
        End Property
        Dim _ParentDomainId As Integer
        Public Property ParentDomainId() As Integer
            Get
                Return _ParentDomainId
            End Get
            Set(ByVal value As Integer)
                _ParentDomainId = value
            End Set
        End Property
        Dim _ParentType As Integer
        Public Property ParentType() As Integer
            Get
                Return _ParentType
            End Get
            Set(ByVal value As Integer)
                _ParentType = value
            End Set
        End Property
        Private _FromDate As Date
        Public Property FromDate() As Date
            Get
                Return _FromDate
            End Get
            Set(ByVal value As Date)
                _FromDate = value
            End Set
        End Property
        Private _ToDate As Date
        Public Property ToDate() As Date
            Get
                Return _ToDate
            End Get
            Set(ByVal value As Date)
                _ToDate = value
            End Set
        End Property

        Private _PageSize As Integer
        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal Value As Integer)
                _PageSize = Value
            End Set
        End Property

        Private _CurrentPage As Integer
        Public Property CurrentPage() As Integer
            Get
                Return _CurrentPage
            End Get
            Set(ByVal Value As Integer)
                _CurrentPage = Value
            End Set
        End Property

        Private _TotalRecords As Integer
        Public Property TotalRecords() As Integer
            Get
                Return _TotalRecords
            End Get
            Set(ByVal Value As Integer)
                _TotalRecords = Value
            End Set
        End Property

        'Private DomainId As Integer
        ''Public Property DomainId() As Integer
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal value As Integer)
        '        DomainId = value
        '    End Set
        'End Property

        Private _Mode As Short
        Public Property Mode() As Short
            Get
                Return _Mode
            End Get
            Set(ByVal value As Short)
                _Mode = value
            End Set
        End Property

        Private _DomainCode As String
        Public Property DomainCode() As String
            Get
                Return _DomainCode
            End Get
            Set(ByVal value As String)
                _DomainCode = value
            End Set
        End Property

        Private _IsRollUp As Boolean
        Public Property IsRollUp() As Boolean
            Get
                Return _IsRollUp
            End Get
            Set(ByVal value As Boolean)
                _IsRollUp = value
            End Set
        End Property

        'Private UserCntID As Long
        'Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal value As Long)
        '        UserCntID = value
        '    End Set
        'End Property

        Private _DateRange As Integer
        Public Property DateRange() As Integer
            Get
                Return _DateRange
            End Get
            Set(ByVal value As Integer)
                _DateRange = value
            End Set
        End Property

        Private _BasedOn As Integer
        Public Property BasedOn() As Integer
            Get
                Return _BasedOn
            End Get
            Set(ByVal value As Integer)
                _BasedOn = value
            End Set
        End Property

        Private _ItemType As Integer
        Public Property ItemType() As Integer
            Get
                Return _ItemType
            End Get
            Set(ByVal value As Integer)
                _ItemType = value
            End Set
        End Property

        Private _ItemClass As Integer
        Public Property ItemClass() As Integer
            Get
                Return _ItemClass
            End Get
            Set(ByVal value As Integer)
                _ItemClass = value
            End Set
        End Property

        Private _ItemClassID As Integer
        Public Property ItemClassID() As Integer
            Get
                Return _ItemClassID
            End Get
            Set(ByVal value As Integer)
                _ItemClassID = value
            End Set
        End Property

        Private _DateCondition As String
        Public Property DateCondition() As String
            Get
                Return _DateCondition
            End Get
            Set(ByVal value As String)
                _DateCondition = value
            End Set
        End Property

        Private _WarehouseID As Integer
        Public Property WarehouseID() As Integer
            Get
                Return _WarehouseID
            End Get
            Set(ByVal value As Integer)
                _WarehouseID = value
            End Set
        End Property

        Public Function GetMultiSummary(ByVal ParentDomainId As Int32, ByVal FromDate As Date, _
                                        ByVal ToDate As Date, ByVal ByteMode As Integer, ByVal numSubscriberID As Integer) As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim spname As String = String.Empty
                Dim ds As New DataSet
                Dim arparms() As Npgsql.NpgsqlParameter
                If ByteMode = 1 Or ByteMode = 2 Or ByteMode = 3 Or ByteMode = 5 Or ByteMode = 6 Or ByteMode = 7 Then
                    arparms = New Npgsql.NpgsqlParameter(5) {}
                Else
                    arparms = New Npgsql.NpgsqlParameter(4) {}
                End If


                arparms(0) = New Npgsql.NpgsqlParameter("@numParentDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = ParentDomainId

                arparms(1) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(1).Value = FromDate

                arparms(2) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(2).Value = ToDate

                arparms(3) = New Npgsql.NpgsqlParameter("@numSubscriberID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(3).Value = numSubscriberID

                arparms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(4).Value = Nothing
                arparms(4).Direction = ParameterDirection.InputOutput

                If ByteMode = 1 Or ByteMode = 2 Or ByteMode = 3 Or ByteMode = 5 Or ByteMode = 6 Or ByteMode = 7 Then
                    arparms(5) = New Npgsql.NpgsqlParameter("@Rollup", NpgsqlTypes.NpgsqlDbType.Bit)
                    arparms(5).Value = _IsRollUp
                End If

                Select Case ByteMode
                    Case 1
                        spname = "USP_GetMultiAR"
                    Case 2
                        spname = "USP_GetMultiAP"
                    Case 3
                        spname = "USP_GetMultiIncomeExpenses"
                    Case 4
                        spname = "USP_GetMultiExpensesDetailed"
                    Case 5
                        spname = "USP_GetMultiCASHBANK"
                    Case 6
                        spname = "USP_GetMultiStock"
                    Case 7
                        spname = "USP_GetMultiPROFITLOSS"

                End Select

                ds = SqlDAL.ExecuteDataset(connString, spname, arparms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetMultipleTB(ByVal ParentDomainId As Int32, ByVal FromDate As Date, ByVal ToDate As Date, ByVal subScriberID As Integer) As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numParentDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = ParentDomainId

                arparms(1) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(1).Value = FromDate

                arparms(2) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(2).Value = ToDate

                arparms(3) = New Npgsql.NpgsqlParameter("@numSubscriberID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(3).Value = subScriberID

                arparms(4) = New Npgsql.NpgsqlParameter("@Rollup", NpgsqlTypes.NpgsqlDbType.Bit)
                arparms(4).Value = _IsRollUp

                arparms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(5).Value = Nothing
                arparms(5).Direction = ParameterDirection.InputOutput

                arparms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(6).Value = Nothing
                arparms(6).Direction = ParameterDirection.InputOutput

                arparms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(7).Value = Nothing
                arparms(7).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetMultiTB", arparms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetMultiplePL(ByVal ParentDomainId As Int32, ByVal FromDate As Date, ByVal ToDate As Date, ByVal numSubscriberId As Integer) As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numParentDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = ParentDomainId

                arparms(1) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(1).Value = FromDate

                arparms(2) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(2).Value = ToDate

                arparms(3) = New Npgsql.NpgsqlParameter("@numSubscriberId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(3).Value = numSubscriberId

                arparms(4) = New Npgsql.NpgsqlParameter("@Rollup", NpgsqlTypes.NpgsqlDbType.Bit)
                arparms(4).Value = _IsRollUp

                arparms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(5).Value = Nothing
                arparms(5).Direction = ParameterDirection.InputOutput

                arparms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(6).Value = Nothing
                arparms(6).Direction = ParameterDirection.InputOutput

                arparms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(7).Value = Nothing
                arparms(7).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetMultiPL", arparms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetMultipleBS(ByVal ParentDomainId As Int32, ByVal FromDate As Date, ByVal ToDate As Date, ByVal numSubscriberId As Integer) As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numParentDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = ParentDomainId

                arparms(1) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(1).Value = FromDate

                arparms(2) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arparms(2).Value = ToDate

                arparms(3) = New Npgsql.NpgsqlParameter("@numSubscriberId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(3).Value = numSubscriberId

                arparms(4) = New Npgsql.NpgsqlParameter("@Rollup", NpgsqlTypes.NpgsqlDbType.Bit)
                arparms(4).Value = _IsRollUp

                arparms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(5).Value = Nothing
                arparms(5).Direction = ParameterDirection.InputOutput

                arparms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(6).Value = Nothing
                arparms(6).Direction = ParameterDirection.InputOutput

                arparms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(7).Value = Nothing
                arparms(7).Direction = ParameterDirection.InputOutput

                arparms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur4", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(8).Value = Nothing
                arparms(8).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetMultiBS", arparms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'ByVal SubScriberID As Integer, ByVal DomainId As Int32, ByVal ByteMode As Integer, Optional ByVal DomainCode As String = ""
        Public Function GetDomain() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = DomainId

                arparms(1) = New Npgsql.NpgsqlParameter("@numType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(1).Value = _Mode

                arparms(2) = New Npgsql.NpgsqlParameter("@vcDomainCode", NpgsqlTypes.NpgsqlDbType.VarChar)
                arparms(2).Value = _DomainCode

                arparms(3) = New Npgsql.NpgsqlParameter("@numSubscriberID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(3).Value = _SubscriberID

                arparms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(4).Value = Nothing
                arparms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetDomain", arparms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Sub sb_FillComboFromDomain(ByRef objCombo As DropDownList, ByVal DomainID As Integer, ByVal ByteMode As Short, ByVal SubscriberID As Integer)
            Try
                _DomainCode = ""
                DomainId = DomainID
                _SubscriberID = SubscriberID
                _Mode = ByteMode
                objCombo.DataSource = GetSubscriberDomain()
                objCombo.DataTextField = "vcDomainName"
                objCombo.DataValueField = "numDomainID"
                objCombo.DataBind()
                objCombo.Items.Insert(0, "--Select One--")
                objCombo.Items.FindByText("--Select One--").Value = "0"
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        'ByVal DomainID As Integer, ByVal ByteMode As Integer, ByVal SubscriberID As Integer, Optional ByVal DomainCode As String = ""
        Public Function GetSubscriberDomain() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arparms(0).Value = _Mode

                arparms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(1).Value = DomainId

                arparms(2) = New Npgsql.NpgsqlParameter("@numSubscriberID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(2).Value = _SubscriberID

                arparms(3) = New Npgsql.NpgsqlParameter("@vcDomainCode", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arparms(3).Value = _DomainCode

                arparms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(4).Value = Nothing
                arparms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_ListDomain", arparms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetInventoryChart(ByVal DomainID As Integer, ByVal ByteType As Integer, _
                ByVal FromDate As Date, ByVal ToDate As Date, ByVal TopCount As Integer) As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = DomainID

                arparms(1) = New Npgsql.NpgsqlParameter("@PeriodFrom", NpgsqlTypes.NpgsqlDbType.Date)
                arparms(1).Value = FromDate

                arparms(2) = New Npgsql.NpgsqlParameter("@PeriodTo", NpgsqlTypes.NpgsqlDbType.Date)
                arparms(2).Value = ToDate

                arparms(3) = New Npgsql.NpgsqlParameter("@Type", NpgsqlTypes.NpgsqlDbType.Integer)
                arparms(3).Value = ByteType

                arparms(4) = New Npgsql.NpgsqlParameter("@Top", NpgsqlTypes.NpgsqlDbType.Integer)
                arparms(4).Value = TopCount

                arparms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(5).Value = Nothing
                arparms(5).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_INVENTORYDASHBOARD", arparms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetMultiInventory(ByVal subScriberID As Integer, ByVal DomainID As Integer, ByVal RepDomain As Integer, ByVal ByteType As Integer, _
                ByVal FromDate As Date, ByVal ToDate As Date, ByVal TopCount As Integer) As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = DomainID

                arparms(1) = New Npgsql.NpgsqlParameter("@numSubscriberID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(1).Value = subScriberID

                arparms(2) = New Npgsql.NpgsqlParameter("@PeriodFrom", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(2).Value = FromDate

                arparms(3) = New Npgsql.NpgsqlParameter("@PeriodTo", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(3).Value = ToDate

                arparms(4) = New Npgsql.NpgsqlParameter("@Type", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(4).Value = ByteType

                arparms(5) = New Npgsql.NpgsqlParameter("@TopCount", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(5).Value = TopCount

                arparms(6) = New Npgsql.NpgsqlParameter("@numRepDomain", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(6).Value = RepDomain

                arparms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(7).Value = Nothing
                arparms(7).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_MultiCompInventory", arparms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetMultiCompany(ByVal subScriberID As Integer, ByVal DomainID As Integer) As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = DomainID

                arparms(1) = New Npgsql.NpgsqlParameter("@numSubscriberID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(1).Value = subScriberID

                arparms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(2).Value = Nothing
                arparms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetMultiCompany", arparms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Public Function GetActiveFinYear(ByVal DomainID As Integer) As DataTable
        '    Try
        '        Dim getconnection As New GetConnection
        '        Dim connString As String = getconnection.GetConnectionString

        '        Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

        '        arparms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arparms(0).Value = DomainID


        '        Dim ds As DataSet
        '        ds = SqlDAL.ExecuteDataset(connString, "USP_GetActiveFinYear", arparms)
        '        Return ds.Tables(0)
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function


        Public Function GetInventoryReport() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(14) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = DomainId

                arparms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(1).Value = UserCntID

                arparms(2) = New Npgsql.NpgsqlParameter("@numDateRange", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(2).Value = _DateRange

                arparms(3) = New Npgsql.NpgsqlParameter("@numBasedOn", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(3).Value = _BasedOn

                arparms(4) = New Npgsql.NpgsqlParameter("@numItemType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(4).Value = _ItemType

                arparms(5) = New Npgsql.NpgsqlParameter("@numItemClass", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(5).Value = _ItemClass

                arparms(6) = New Npgsql.NpgsqlParameter("@numItemClassId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(6).Value = _ItemClassID

                arparms(7) = New Npgsql.NpgsqlParameter("@DateCondition", NpgsqlTypes.NpgsqlDbType.VarChar)
                arparms(7).Value = _DateCondition

                arparms(8) = New Npgsql.NpgsqlParameter("@numWareHouseId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(8).Value = _WarehouseID

                arparms(9) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Date)
                arparms(9).Value = _FromDate

                arparms(10) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Date)
                arparms(10).Value = _ToDate

                arparms(11) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arparms(11).Value = _CurrentPage

                arparms(12) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arparms(12).Value = _PageSize

                arparms(13) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(13).Direction = ParameterDirection.InputOutput
                arparms(13).Value = _TotalRecords

                arparms(14) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(14).Value = Nothing
                arparms(14).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arparms.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_InventoryReport", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(13).Value)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function InsertMultiCompany() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numNewDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = _NewDomainId

                arparms(1) = New Npgsql.NpgsqlParameter("@numParentDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(1).Value = _ParentDomainId

                arparms(2) = New Npgsql.NpgsqlParameter("@numParentType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(2).Value = _ParentType

                arparms(3) = New Npgsql.NpgsqlParameter("@numSubscriberID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(3).Value = _SubscriberID

                arparms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(4).Value = Nothing
                arparms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_InsertMultiCompany", arparms)
                If ds.Tables(0).Rows(0).Item(0).ToString = "U" Then
                    Return True
                Else
                    Return False
                End If

            Catch ex As Exception
                'Return False
                Throw ex
            End Try
        End Function
        Public Function ValidateMultiComp() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = DomainID

                arparms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(1).Value = Nothing
                arparms(1).Direction = ParameterDirection.InputOutput

                Return CBool(SqlDAL.ExecuteScalar(connString, "USP_ValidateMultiComp", arparms))
            Catch ex As Exception
                Throw ex
            End Try
        End Function


    End Class

End Namespace

