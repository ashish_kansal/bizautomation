Option Explicit On
Option Strict On

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Namespace BACRM.BusinessLogic.Accounting

    Public Class ReceivePayment
        Inherits BACRM.BusinessLogic.CBusinessBase

        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Siva 	DATE:17-May-07
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Siva 	DATE:17-May-07
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub

        '#End Region

        'define private vairable
        'Private DomainId As Integer
        Private _BizDocsPaymentDetId As Integer
        Private _BizDocsId As Integer
        Private _OppId As Integer
        Private _Amount As Decimal
        Private _Entry_Date As Date = New Date(1753, 1, 1)
        Private _JournalId As Integer
        Private _RecurringId As Integer
        Private _OppBIzDocID As Integer
        Private _JournalDetails As String = String.Empty
        Private _Mode As Integer
        Private _RecurringMode As Int16
        Private _ClientTimeZoneOffset As Int32
        Private _BizDocsPaymentDetailsId As Long
        Private _CheckNo As Long
        Public Property CheckNo() As Long
            Get
                Return _CheckNo
            End Get
            Set(ByVal value As Long)
                _CheckNo = value
            End Set
        End Property
        'Public Property DomainId() As Integer
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal value As Integer)
        '        DomainId = value
        '    End Set
        'End Property

        Public Property BizDocsPaymentDetId() As Integer
            Get
                Return _BizDocsPaymentDetId
            End Get
            Set(ByVal value As Integer)
                _BizDocsPaymentDetId = value
            End Set
        End Property

        Public Property BizDocsPaymentDetailsId() As Long
            Get
                Return _BizDocsPaymentDetailsId
            End Get
            Set(ByVal value As Long)
                _BizDocsPaymentDetailsId = value
            End Set
        End Property

        Public Property BizDocsId() As Integer
            Get
                Return _BizDocsId
            End Get
            Set(ByVal value As Integer)
                _BizDocsId = value
            End Set
        End Property

        Public Property OppId() As Integer
            Get
                Return _OppId
            End Get
            Set(ByVal value As Integer)
                _OppId = value
            End Set
        End Property

        Public Property Amount() As Decimal
            Get
                Return _Amount
            End Get
            Set(ByVal value As Decimal)
                _Amount = value
            End Set
        End Property

        Public Property Entry_Date() As DateTime
            Get
                Return _Entry_Date
            End Get
            Set(ByVal Value As DateTime)
                _Entry_Date = Value
            End Set
        End Property

        Public Property JournalId() As Integer
            Get
                Return _JournalId
            End Get
            Set(ByVal value As Integer)
                _JournalId = value
            End Set
        End Property

        Public Property RecurringId() As Integer
            Get
                Return _RecurringId
            End Get
            Set(ByVal value As Integer)
                _RecurringId = value
            End Set
        End Property

        'Public Property UserCntID() As Integer
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal value As Integer)
        '        UserCntID = value
        '    End Set
        'End Property

        Public Property OppBIzDocID() As Integer
            Get
                Return _OppBIzDocID
            End Get
            Set(ByVal value As Integer)
                _OppBIzDocID = value
            End Set
        End Property

        Public Property JournalDetails() As String
            Get
                Return _JournalDetails
            End Get
            Set(ByVal value As String)
                _JournalDetails = value
            End Set
        End Property

        Public Property Mode() As Integer
            Get
                Return _Mode
            End Get
            Set(ByVal Value As Integer)
                _Mode = Value
            End Set
        End Property

        Public Property RecurringMode() As Int16
            Get
                Return _RecurringMode
            End Get
            Set(ByVal Value As Int16)
                _RecurringMode = Value
            End Set
        End Property

        Public Property ClientTimeZoneOffset() As Int32
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal value As Int32)
                _ClientTimeZoneOffset = value
            End Set
        End Property

        Private _ReturnID As Long
        Public Property ReturnID() As Long
            Get
                Return _ReturnID
            End Get
            Set(ByVal value As Long)
                _ReturnID = value
            End Set
        End Property

        Private _PaymentType As Short
        Public Property PaymentType() As Short
            Get
                Return _PaymentType
            End Get
            Set(ByVal value As Short)
                _PaymentType = value
            End Set
        End Property

        Private _FromDate As Date
        Public Property FromDate() As Date
            Get
                Return _FromDate
            End Get
            Set(ByVal Value As Date)
                _FromDate = Value
            End Set
        End Property

        Private _ToDate As Date
        Public Property ToDate() As Date
            Get
                Return _ToDate
            End Get
            Set(ByVal Value As Date)
                _ToDate = Value
            End Set
        End Property

        Private _Type As Short
        Public Property Type() As Short
            Get
                Return _Type
            End Get
            Set(ByVal Value As Short)
                _Type = Value
            End Set
        End Property

        Private _ComissionID As Long
        Public Property ComissionID() As Long
            Get
                Return _ComissionID
            End Get
            Set(ByVal Value As Long)
                _ComissionID = Value
            End Set
        End Property

        Private _CompanyID As Long
        Public Property CompanyID() As Long
            Get
                Return _CompanyID
            End Get
            Set(ByVal value As Long)
                _CompanyID = value
            End Set
        End Property

        Public Function GetReceivePaymentDetails() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _ClientTimeZoneOffset

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetReceivePaymentDetails", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetReceivePaymentDetailsForBizDocs() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OppId

                arParms(2) = New Npgsql.NpgsqlParameter("@OppBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _OppBIzDocID

                arParms(3) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _ClientTimeZoneOffset

                arParms(4) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(4).Value = _FromDate

                arParms(5) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(5).Value = _ToDate

                arParms(6) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(6).Value = _Type

                arParms(7) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = IIf(_CompanyID > 0, _CompanyID, DBNull.Value)

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetReceivePaymentDetailsForBizDocs", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetOpportunityBizDocsIntegratedToAcnt() As Long
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numBizDocsPaymentDetId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _BizDocsPaymentDetId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_GetOpportunityBizDocsIntegratedToAcnt", arParms))
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try

        End Function
        ' This function is to reverse the transaction from account section
        ' Created by Rajaram on 28/05/2008
        Public Function UpdateOpportunityBizDocsIntegratedToAcnt() As Long
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numBizDocsPaymentDetId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _BizDocsPaymentDetId

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateOpportunityBizDocsIntegratedToAcnt", arParms)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try

        End Function
    End Class
End Namespace
