Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports MailBee.ImapMail
Imports MailBee.SmtpMail
Imports MailBee.SmtpMail.Smtp
Imports System.Configuration
Imports BACRM.BusinessLogic.Outlook
Imports System.Web.HttpContext
Imports BACRM.BusinessLogic.Common
Imports MailBee
Imports BACRM.BusinessLogic.Admin
Imports System.Collections.Generic
Imports System.Xml.Serialization

Namespace BACRM.BusinessLogic.Outlook
    Public Class COutlook
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _TotalRecords As Integer



        Public Property TotalRecords() As Integer
            Get
                Return _TotalRecords
            End Get
            Set(ByVal Value As Integer)
                _TotalRecords = Value
            End Set
        End Property
        Private _TeamType As Integer
        Public Property TeamType() As Integer
            Get
                Return _TeamType
            End Get
            Set(ByVal Value As Integer)
                _TeamType = Value
            End Set
        End Property
        Private _GroupId As Long
        Public Property GroupId() As Long
            Get
                Return _GroupId
            End Get
            Set(ByVal Value As Long)
                _GroupId = Value
            End Set
        End Property


        'Private UserCntID As Long

        'Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal Value As Long)
        '        UserCntID = Value
        '    End Set
        'End Property

        'Private DomainId As Long
        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property
        Private _RegularSearchCriteria As String
        Public Property RegularSearchCriteria() As String
            Get
                Return _RegularSearchCriteria
            End Get
            Set(ByVal Value As String)
                _RegularSearchCriteria = Value
            End Set
        End Property

        Private _CustomSearchCriteria As String
        Public Property CustomSearchCriteria() As String
            Get
                Return _CustomSearchCriteria
            End Get
            Set(ByVal Value As String)
                _CustomSearchCriteria = Value
            End Set
        End Property
        Private _CurrentPage As Integer
        Public Property CurrentPage() As Integer
            Get
                Return _CurrentPage
            End Get
            Set(ByVal Value As Integer)
                _CurrentPage = Value
            End Set
        End Property
        Private _PageSize As Integer
        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal Value As Integer)
                _PageSize = Value
            End Set
        End Property
        Private _ActivityID As Long
        Public Property ActivityID() As Long
            Get
                Return _ActivityID
            End Get
            Set(ByVal Value As Long)
                _ActivityID = Value
            End Set
        End Property
        Private _ResourceID As Long
        Public Property ResourceID() As Long
            Get
                Return _ResourceID
            End Get
            Set(ByVal Value As Long)
                _ResourceID = Value
            End Set
        End Property
        Private _ResourceName As String
        Public Property ResourceName() As String
            Get
                Return _ResourceName
            End Get
            Set(ByVal Value As String)
                _ResourceName = Value
            End Set
        End Property
        Private _columnSortOrder As String
        Public Property columnSortOrder() As String
            Get
                Return _columnSortOrder
            End Get
            Set(ByVal Value As String)
                _columnSortOrder = Value
            End Set
        End Property
        Private _columnName As String
        Public Property columnName() As String
            Get
                Return _columnName
            End Get
            Set(ByVal Value As String)
                _columnName = Value
            End Set
        End Property
        Private _Email As String
        Public Property Email() As String
            Get
                Return _Email
            End Get
            Set(ByVal Value As String)
                _Email = Value
            End Set
        End Property
        Private _ToEmail As String
        Public Property ToEmail() As String
            Get
                Return _ToEmail
            End Get
            Set(ByVal Value As String)
                _ToEmail = Value
            End Set
        End Property
        Private _strXml As String
        Public Property strXml() As String
            Get
                Return _strXml
            End Get
            Set(ByVal Value As String)
                _strXml = Value
            End Set
        End Property
        Private _AllDayEvent As Boolean

        Public Property AllDayEvent() As Boolean
            Get
                Return _AllDayEvent
            End Get
            Set(ByVal Value As Boolean)
                _AllDayEvent = Value
            End Set
        End Property
        Private _ActivityDescription As String
        Public Property ActivityDescription() As String
            Get
                Return _ActivityDescription
            End Get
            Set(ByVal Value As String)
                _ActivityDescription = Value
            End Set
        End Property
        Private _Duration As Integer
        Public Property Duration() As Integer
            Get
                Return _Duration
            End Get
            Set(ByVal Value As Integer)
                _Duration = Value
            End Set
        End Property
        Private _Location As String
        Public Property Location() As String
            Get
                Return _Location
            End Get
            Set(ByVal Value As String)
                _Location = Value
            End Set
        End Property

        Private _HtmlLink As String
        Public Property HtmlLink() As String
            Get
                Return _HtmlLink
            End Get
            Set(ByVal Value As String)
                _HtmlLink = Value
            End Set
        End Property

        Private _chrSource As Char
        Public Property chrSource() As Char
            Get
                Return _chrSource
            End Get
            Set(ByVal Value As Char)
                _chrSource = Value
            End Set
        End Property

        Private _LastReminderDateTimeUtc As DateTime
        Public Property LastReminderDateTimeUtc() As DateTime
            Get
                Return _LastReminderDateTimeUtc
            End Get
            Set(ByVal Value As DateTime)
                _LastReminderDateTimeUtc = Value
            End Set
        End Property
        Private _OriginalStartDateTimeUtc As DateTime
        Public Property OriginalStartDateTimeUtc() As DateTime
            Get
                Return _OriginalStartDateTimeUtc
            End Get
            Set(ByVal Value As DateTime)
                _OriginalStartDateTimeUtc = Value
            End Set
        End Property
        Private _StartDateTimeUtc As DateTime
        Public Property StartDateTimeUtc() As DateTime
            Get
                Return _StartDateTimeUtc
            End Get
            Set(ByVal Value As DateTime)
                _StartDateTimeUtc = Value
            End Set
        End Property
        Private _Subject As String
        Public Property Subject() As String
            Get
                Return _Subject
            End Get
            Set(ByVal Value As String)
                _Subject = Value
            End Set
        End Property
        Private _EnableReminder As Boolean
        Public Property EnableReminder() As Boolean
            Get
                Return _EnableReminder
            End Get
            Set(ByVal Value As Boolean)
                _EnableReminder = Value
            End Set
        End Property
        Private _ReminderInterval As Integer
        Public Property ReminderInterval() As Integer
            Get
                Return _ReminderInterval
            End Get
            Set(ByVal Value As Integer)
                _ReminderInterval = Value
            End Set
        End Property
        Private _ShowTimeAs As Integer
        Public Property ShowTimeAs() As Integer
            Get
                Return _ShowTimeAs
            End Get
            Set(ByVal Value As Integer)
                _ShowTimeAs = Value
            End Set
        End Property
        Private _Importance As Integer
        Public Property Importance() As Integer
            Get
                Return _Importance
            End Get
            Set(ByVal Value As Integer)
                _Importance = Value
            End Set
        End Property
        Private _Status As Integer
        Public Property Status() As Integer
            Get
                Return _Status
            End Get
            Set(ByVal Value As Integer)
                _Status = Value
            End Set
        End Property
        Private _RecurrenceKey As Integer
        Public Property RecurrenceKey() As Integer
            Get
                Return _RecurrenceKey
            End Get
            Set(ByVal Value As Integer)
                _RecurrenceKey = Value
            End Set
        End Property

        Private _SearchText As String
        Public Property SearchText() As String
            Get
                Return _SearchText
            End Get
            Set(ByVal value As String)
                _SearchText = value
            End Set
        End Property

        Private _SearchTextFrom As String
        Public Property SearchTextFrom() As String
            Get
                Return _SearchTextFrom
            End Get
            Set(ByVal value As String)
                _SearchTextFrom = value
            End Set
        End Property

        Private _SearchTextTo As String
        Public Property SearchTextTo() As String
            Get
                Return _SearchTextTo
            End Get
            Set(ByVal value As String)
                _SearchTextTo = value
            End Set
        End Property

        Private _SearchTextHasWords As String
        Public Property SearchTextHasWords() As String
            Get
                Return _SearchTextHasWords
            End Get
            Set(ByVal value As String)
                _SearchTextHasWords = value
            End Set
        End Property

        Private _SearchHasAttachment As Boolean
        Public Property SearchHasAttachment() As Boolean
            Get
                Return _SearchHasAttachment
            End Get
            Set(ByVal value As Boolean)
                _SearchHasAttachment = value
            End Set
        End Property

        Private _SearchIsAdvancedsrch As Boolean
        Public Property SearchIsAdvancedsrch() As Boolean
            Get
                Return _SearchIsAdvancedsrch
            End Get
            Set(ByVal value As Boolean)
                _SearchIsAdvancedsrch = value
            End Set
        End Property

        Private _SearchTextBody As String
        Public Property SearchTextBody() As String
            Get
                Return _SearchTextBody
            End Get
            Set(ByVal value As String)
                _SearchTextBody = value
            End Set
        End Property

        Private _SearchTextSubject As String
        Public Property SearchTextSubject() As String
            Get
                Return _SearchTextSubject
            End Get
            Set(ByVal value As String)
                _SearchTextSubject = value
            End Set
        End Property

        Private _SearchInNode As Long
        Public Property SearchInNode() As Long
            Get
                Return _SearchInNode
            End Get
            Set(ByVal value As Long)
                _SearchInNode = value
            End Set
        End Property

        Private _VarianceKey As String
        Public Property VarianceKey() As String
            Get
                Return _VarianceKey
            End Get
            Set(ByVal Value As String)
                _VarianceKey = Value
            End Set
        End Property

        Private _LookAheadWindowEndTime As DateTime
        Public Property LookAheadWindowEndTime() As DateTime
            Get
                Return _LookAheadWindowEndTime
            End Get
            Set(ByVal Value As DateTime)
                _LookAheadWindowEndTime = Value
            End Set
        End Property

        Private _EndDateUtc As DateTime
        Public Property EndDateUtc() As DateTime
            Get
                Return _EndDateUtc
            End Get
            Set(ByVal Value As DateTime)
                _EndDateUtc = Value
            End Set
        End Property
        Private _DayOfWeekMaskUtc As Integer
        Public Property DayOfWeekMaskUtc() As Integer
            Get
                Return _DayOfWeekMaskUtc
            End Get
            Set(ByVal Value As Integer)
                _DayOfWeekMaskUtc = Value
            End Set
        End Property
        Private _UtcOffset As Integer
        Public Property UtcOffset() As Integer
            Get
                Return _UtcOffset
            End Get
            Set(ByVal Value As Integer)
                _UtcOffset = Value
            End Set
        End Property
        Private _DayOfMonth As Integer
        Public Property DayOfMonth() As Integer
            Get
                Return _DayOfMonth
            End Get
            Set(ByVal Value As Integer)
                _DayOfMonth = Value
            End Set
        End Property
        Private _MonthOfYear As Integer
        Public Property MonthOfYear() As Integer
            Get
                Return _MonthOfYear
            End Get
            Set(ByVal Value As Integer)
                _MonthOfYear = Value
            End Set
        End Property
        Private _PeriodMultiple As Integer
        Public Property PeriodMultiple() As Integer
            Get
                Return _PeriodMultiple
            End Get
            Set(ByVal Value As Integer)
                _PeriodMultiple = Value
            End Set
        End Property
        Private _Period As Char
        Public Property Period() As Char
            Get
                Return _Period
            End Get
            Set(ByVal Value As Char)
                _Period = Value
            End Set
        End Property
        Private _EditType As Integer
        Public Property EditType() As Integer
            Get
                Return _EditType
            End Get
            Set(ByVal Value As Integer)
                _EditType = Value
            End Set
        End Property

        Private _numEmailHstrID As Long
        Public Property numEmailHstrID() As Long
            Get
                Return _numEmailHstrID
            End Get
            Set(ByVal Value As Long)
                _numEmailHstrID = Value
            End Set
        End Property
        Private _AttachmentType As String
        Public Property AttachmentType() As String
            Get
                Return _AttachmentType
            End Get
            Set(ByVal Value As String)
                _AttachmentType = Value
            End Set
        End Property
        Private _ItemId As String
        Public Property ItemId() As String
            Get
                Return _ItemId
            End Get
            Set(ByVal Value As String)
                _ItemId = Value
            End Set
        End Property
        Private _ItemIdOccur As String
        Public Property ItemIdOccur() As String
            Get
                Return _ItemIdOccur
            End Get
            Set(ByVal Value As String)
                _ItemIdOccur = Value
            End Set
        End Property
        Private _ChangeKey As String
        Public Property ChangeKey() As String
            Get
                Return _ChangeKey
            End Get
            Set(ByVal Value As String)
                _ChangeKey = Value
            End Set
        End Property
        Private _mode As Boolean = False

        Public Property mode() As Boolean
            Get
                Return _mode
            End Get
            Set(ByVal Value As Boolean)
                _mode = Value
            End Set
        End Property

        Private _Type As Integer

        Public Property Type() As Integer
            Get
                Return _Type
            End Get
            Set(ByVal Value As Integer)
                _Type = Value
            End Set
        End Property

        Private _Uid As Long
        Public Property Uid() As Long
            Get
                Return _Uid
            End Get
            Set(ByVal value As Long)
                _Uid = value
            End Set
        End Property

        Private _NodeId As Long

        Public Property NodeId() As Long
            Get
                Return _NodeId
            End Get
            Set(ByVal Value As Long)
                _NodeId = Value
            End Set
        End Property
        Private _ParentId As Long

        Public Property ParentId() As Long
            Get
                Return _ParentId
            End Get
            Set(ByVal Value As Long)
                _ParentId = Value
            End Set
        End Property
        Private _NodeName As String

        Public Property NodeName() As String
            Get
                Return _NodeName
            End Get
            Set(ByVal Value As String)
                _NodeName = Value
            End Set
        End Property
        Private _ModeType As Integer

        Public Property ModeType() As Integer
            Get
                Return _ModeType
            End Get
            Set(ByVal Value As Integer)
                _ModeType = Value
            End Set
        End Property
        Private _ClientTimeZoneOffset As Integer
        Public Property ClientTimeZoneOffset() As Integer
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal value As Integer)
                _ClientTimeZoneOffset = value
            End Set
        End Property

        Private _DivisionID As Long

        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property
        Private _UserRightType As Long = 0
        Public Property UserRightType As Long
            Get
                Return _UserRightType
            End Get
            Set(ByVal value As Long)
                _UserRightType = value

            End Set
        End Property
        Private _EmailStatus As Long = 0
        Public Property EmailStatus As Long
            Get
                Return _EmailStatus
            End Get
            Set(ByVal value As Long)
                _EmailStatus = value
            End Set
        End Property

        Private _bitHidden As Boolean = False
        Public Property bitHidden As Boolean
            Get
                Return _bitHidden
            End Get
            Set(value As Boolean)
                _bitHidden = value
            End Set
        End Property

        Private _Color As Short
        Public Property Color() As Short
            Get
                Return _Color
            End Get
            Set(ByVal Value As Short)
                _Color = Value
            End Set
        End Property

        Private _FromName As String

        Public Property FromName() As String
            Get
                Return _FromName
            End Get
            Set(ByVal Value As String)
                _FromName = Value
            End Set
        End Property

        Private _ToName As String

        Public Property ToName() As String
            Get
                Return _ToName
            End Get
            Set(ByVal Value As String)
                _ToName = Value
            End Set
        End Property

        Private _CCName As String

        Public Property CCName() As String
            Get
                Return _CCName
            End Get
            Set(ByVal Value As String)
                _CCName = Value
            End Set
        End Property

        Private _BCCName As String

        Public Property BCCName() As String
            Get
                Return _BCCName
            End Get
            Set(ByVal Value As String)
                _BCCName = Value
            End Set
        End Property

        Private _Body As String

        Public Property Body() As String
            Get
                Return _Body
            End Get
            Set(ByVal Value As String)
                _Body = Value
            End Set
        End Property

        Private _BodyText As String

        Public Property BodyText() As String
            Get
                Return _BodyText
            End Get
            Set(ByVal Value As String)
                _BodyText = Value
            End Set
        End Property

        Private _ReceivedOn As DateTime
        Public Property ReceivedOn() As DateTime
            Get
                Return _ReceivedOn
            End Get
            Set(ByVal Value As DateTime)
                _ReceivedOn = Value
            End Set
        End Property

        Private _vcSize As String
        Public Property vcSize() As String
            Get
                Return _vcSize
            End Get
            Set(ByVal Value As String)
                _vcSize = Value
            End Set
        End Property

        Private _strRecordsIds As String
        Public Property strRecordsIds() As String
            Get
                Return _strRecordsIds
            End Get
            Set(value As String)
                _strRecordsIds = value
            End Set
        End Property

        Private _IsRead As Boolean
        Public Property IsRead() As Boolean
            Get
                Return _IsRead
            End Get
            Set(ByVal Value As Boolean)
                _IsRead = Value
            End Set
        End Property

        Private _HasAttachments As Boolean
        Public Property HasAttachments() As Boolean
            Get
                Return _HasAttachments
            End Get
            Set(ByVal Value As Boolean)
                _HasAttachments = Value
            End Set
        End Property

        Private _AttachmentName As String
        Public Property AttachmentName() As String
            Get
                Return _AttachmentName
            End Get
            Set(ByVal Value As String)
                _AttachmentName = Value
            End Set
        End Property

        Private _NewAttachmentName As String
        Public Property NewAttachmentName() As String
            Get
                Return _NewAttachmentName
            End Get
            Set(ByVal Value As String)
                _NewAttachmentName = Value
            End Set
        End Property


        Private _intContactFilterType As Integer
        Public Property intContactFilterType() As Integer
            Get
                Return _intContactFilterType
            End Get
            Set(ByVal Value As Integer)
                _intContactFilterType = Value
            End Set
        End Property


        Private _vcFollowupStatusIds As String
        Public Property vcFollowupStatusIds() As String
            Get
                Return _vcFollowupStatusIds
            End Get
            Set(ByVal Value As String)
                _vcFollowupStatusIds = Value
            End Set
        End Property


        Private _numRelationShipId As Int64
        Public Property numRelationShipId() As Int64
            Get
                Return _numRelationShipId
            End Get
            Set(ByVal Value As Int64)
                _numRelationShipId = Value
            End Set
        End Property

        Private _numProfileId As Int64
        Public Property numProfileId() As Int64
            Get
                Return _numProfileId
            End Get
            Set(ByVal Value As Int64)
                _numProfileId = Value
            End Set
        End Property

        Private _bitGroupBySubjectThread As Boolean
        Public Property bitGroupBySubjectThread() As Boolean
            Get
                Return _bitGroupBySubjectThread
            End Get
            Set(ByVal Value As Boolean)
                _bitGroupBySubjectThread = Value
            End Set
        End Property
        Private _AttachmentSize As String
        Public Property AttachmentSize() As String
            Get
                Return _AttachmentSize
            End Get
            Set(ByVal Value As String)
                _AttachmentSize = Value
            End Set
        End Property

        Private _fromDate As DateTime
        Public Property FromDate() As DateTime
            Get
                Return _fromDate
            End Get
            Set(ByVal Value As DateTime)
                _fromDate = Value
            End Set
        End Property

        Private _toDate As DateTime
        Public Property ToDate() As DateTime
            Get
                Return _toDate
            End Get
            Set(ByVal Value As DateTime)
                _toDate = Value
            End Set
        End Property

        Private _ParentNodeText As String
        Public Property ParentNodeText() As String
            Get
                Return _ParentNodeText
            End Get
            Set(value As String)
                _ParentNodeText = value
            End Set
        End Property

        Public Property FilterDate As DateTime?
        Public Property ExcludeEmailFromNonBizContact As Boolean

        'Public Sub MarkReadInBoxItem()
        '    Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
        '    Dim getconnection As New GetConnection
        '    Dim connString As String = getconnection.GetConnectionString

        '    arParms(0) = New Npgsql.NpgsqlParameter("@numEmailHstrID", NpgsqlTypes.NpgsqlDbType.VarChar)
        '    arParms(0).Value = _numEmailHstrID

        '    SqlDAL.ExecuteNonQuery(connString, "USP_MarkReadInboxItems", arParms)
        'End Sub
        Public Sub deleteInboxItem()
            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString

            arParms(0) = New Npgsql.NpgsqlParameter("@numEmailHstrID", NpgsqlTypes.NpgsqlDbType.Varchar)
            arParms(0).Value = _numEmailHstrID

            arParms(1) = New Npgsql.NpgsqlParameter("@mode", NpgsqlTypes.NpgsqlDbType.Bit)
            arParms(1).Value = _mode

            arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
            arParms(2).Value = DomainID

            arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint)
            arParms(3).Value = UserCntID

            SqlDAL.ExecuteNonQuery(connString, "USP_DelInboxItems", arParms)
        End Sub
        Public Sub deleteInboxItemByUid()
            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString

            arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
            arParms(0).Value = DomainID

            arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint)
            arParms(1).Value = UserCntID

            arParms(2) = New Npgsql.NpgsqlParameter("@numUid", NpgsqlTypes.NpgsqlDbType.Bigint)
            arParms(2).Value = _Uid

            arParms(3) = New Npgsql.NpgsqlParameter("@numNodeId", NpgsqlTypes.NpgsqlDbType.Bigint)
            arParms(3).Value = NodeId

            SqlDAL.ExecuteNonQuery(connString, "USP_DelInboxItemsByUid", arParms)
        End Sub

        Public Function getDetailsFromEmail() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@Email", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(0).Value = _Email

                arParms(1) = New Npgsql.NpgsqlParameter("@DomainId", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetContactFromEmail", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function GetResourceByName() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@ResourceName", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(0).Value = _ResourceName

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "Resource_SelByName", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function getInbox() As DataSet
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(31) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = _PageSize

                arParms(1) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(1).Value = _CurrentPage

                arParms(2) = New Npgsql.NpgsqlParameter("@srch", NpgsqlTypes.NpgsqlDbType.Varchar, 100)
                arParms(2).Value = _SearchText

                arParms(3) = New Npgsql.NpgsqlParameter("@ToEmail", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(3).Value = _ToEmail

                arParms(4) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.Varchar, 50)
                arParms(4).Value = _columnName

                arParms(5) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.Varchar, 10)
                arParms(5).Value = _columnSortOrder

                arParms(6) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint, 10)
                arParms(6).Value = DomainID

                arParms(7) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.Bigint, 10)
                arParms(7).Value = UserCntID

                arParms(8) = New Npgsql.NpgsqlParameter("@numNodeId", NpgsqlTypes.NpgsqlDbType.Bigint, 10)
                arParms(8).Value = _NodeId

                arParms(9) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(9).Value = _ClientTimeZoneOffset

                arParms(10) = New Npgsql.NpgsqlParameter("@tintUserRightType", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(10).Value = _UserRightType

                arParms(11) = New Npgsql.NpgsqlParameter("@EmailStatus", NpgsqlTypes.NpgsqlDbType.Bigint, 10)
                arParms(11).Value = _EmailStatus

                arParms(12) = New Npgsql.NpgsqlParameter("@srchFrom", NpgsqlTypes.NpgsqlDbType.Varchar, 100)
                arParms(12).Value = _SearchTextFrom

                arParms(13) = New Npgsql.NpgsqlParameter("@srchTo", NpgsqlTypes.NpgsqlDbType.Varchar, 100)
                arParms(13).Value = _SearchTextTo

                arParms(14) = New Npgsql.NpgsqlParameter("@srchSubject", NpgsqlTypes.NpgsqlDbType.Varchar, 100)
                arParms(14).Value = _SearchTextSubject

                arParms(15) = New Npgsql.NpgsqlParameter("@srchHasWords", NpgsqlTypes.NpgsqlDbType.Varchar, 100)
                arParms(15).Value = _SearchTextHasWords

                arParms(16) = New Npgsql.NpgsqlParameter("@srchHasAttachment", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(16).Value = _SearchHasAttachment

                arParms(17) = New Npgsql.NpgsqlParameter("@srchIsAdvancedsrch", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(17).Value = _SearchIsAdvancedsrch

                arParms(18) = New Npgsql.NpgsqlParameter("@srchInNode", NpgsqlTypes.NpgsqlDbType.Bigint, 10)
                arParms(18).Value = _SearchInNode

                arParms(19) = New Npgsql.NpgsqlParameter("@FromDate", NpgsqlTypes.NpgsqlDbType.Date)
                arParms(19).Value = _fromDate

                arParms(20) = New Npgsql.NpgsqlParameter("@ToDate", NpgsqlTypes.NpgsqlDbType.Date)
                arParms(20).Value = _toDate

                arParms(21) = New Npgsql.NpgsqlParameter("@dtSelectedDate", NpgsqlTypes.NpgsqlDbType.Date)
                arParms(21).Value = FilterDate

                arParms(22) = New Npgsql.NpgsqlParameter("@bitExcludeEmailFromNonBizContact", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(22).Value = ExcludeEmailFromNonBizContact

                arParms(23) = New Npgsql.NpgsqlParameter("@vcRegularSearchCriteria", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(23).Value = RegularSearchCriteria

                arParms(24) = New Npgsql.NpgsqlParameter("@vcCustomSearchCriteria", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(24).Value = CustomSearchCriteria

                arParms(25) = New Npgsql.NpgsqlParameter("@intContactFilterType", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(25).Value = intContactFilterType

                arParms(26) = New Npgsql.NpgsqlParameter("@vcFollowupStatusIds", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(26).Value = vcFollowupStatusIds

                arParms(27) = New Npgsql.NpgsqlParameter("@numRelationShipId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(27).Value = numRelationShipId

                arParms(28) = New Npgsql.NpgsqlParameter("@numProfileId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(28).Value = numProfileId

                arParms(29) = New Npgsql.NpgsqlParameter("@bitGroupBySubjectThread", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(29).Value = bitGroupBySubjectThread

                arParms(30) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(30).Value = Nothing
                arParms(30).Direction = ParameterDirection.InputOutput

                arParms(31) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(31).Value = Nothing
                arParms(31).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_InboxItems", arParms)
                Return ds 'ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'this method will return range of last 20(ie PageSize) UIDs in Database without bothering about its tinttype and nodeid e.g. 15 to 40 UIDs in Datatable
        Public Function getInboxUidRange() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = _PageSize

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint, 10)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.Bigint, 10)
                arParms(2).Value = UserCntID

                arParms(3) = New Npgsql.NpgsqlParameter("@numNodeId", NpgsqlTypes.NpgsqlDbType.Bigint, 10)
                arParms(3).Value = NodeId

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetInboxItemRange", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Function getMail() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numEmailHstrID", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(0).Value = _numEmailHstrID

                arParms(1) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _ClientTimeZoneOffset

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetMail", arParms)
                Return ds.Tables(0)
            Catch ex As Exception

            End Try
        End Function
        Public Sub UpdateMultipleEmailFlag()
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@vchEmailHstrID", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(0).Value = strRecordsIds

                arParms(1) = New Npgsql.NpgsqlParameter("@bitRead", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(1).Value = IsRead

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateMessageFlags", arParms)
            Catch ex As Exception

            End Try
        End Sub
        Public Sub MoveCopy()
            Try

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numEmailHstrID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(0).Value = _numEmailHstrID

                arParms(1) = New Npgsql.NpgsqlParameter("@numNodeId", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(1).Value = _NodeId

                arParms(2) = New Npgsql.NpgsqlParameter("@ModeType", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(2).Value = _mode

                'SqlDAL.ExecuteNonQuery(connString, "USP_AddImportantItems", arParms)
                SqlDAL.ExecuteNonQuery(connString, "USP_MoveCopyItems", arParms)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Sub

        Public Function FetchActivity() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@OrganizerName", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(0).Value = _ResourceName

                arParms(1) = New Npgsql.NpgsqlParameter("@StartDateTimeUtc", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _StartDateTimeUtc

                arParms(2) = New Npgsql.NpgsqlParameter("@EndDateTimeUtc", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _EndDateUtc

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "Activity_SelByDate", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function getRecurrence() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@OrganizerName", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(0).Value = _ResourceName

                arParms(1) = New Npgsql.NpgsqlParameter("@StartDateTimeUtc", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _StartDateTimeUtc

                arParms(2) = New Npgsql.NpgsqlParameter("@EndDateTimeUtc", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _EndDateUtc

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "Recurrence_Sel", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function getRemainders() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@LookAheadWindowEndTime", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(0).Value = _LookAheadWindowEndTime

                arParms(1) = New Npgsql.NpgsqlParameter("@ResourceID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _ResourceID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "Reminder_Sel", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function AddActivity() As Long
            Try
                Dim lngActivityId As Integer
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(17) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@AllDayEvent", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(0).Value = _AllDayEvent

                arParms(1) = New Npgsql.NpgsqlParameter("@ActivityDescription", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _ActivityDescription

                arParms(2) = New Npgsql.NpgsqlParameter("@Duration", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _Duration

                arParms(3) = New Npgsql.NpgsqlParameter("@Location", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(3).Value = _Location

                arParms(4) = New Npgsql.NpgsqlParameter("@StartDateTimeUtc", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(4).Value = _StartDateTimeUtc

                arParms(5) = New Npgsql.NpgsqlParameter("@Subject", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(5).Value = _Subject

                arParms(6) = New Npgsql.NpgsqlParameter("@EnableReminder", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(6).Value = _EnableReminder

                arParms(7) = New Npgsql.NpgsqlParameter("@ReminderInterval", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(7).Value = _ReminderInterval

                arParms(8) = New Npgsql.NpgsqlParameter("@ShowTimeAs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(8).Value = _ShowTimeAs

                arParms(9) = New Npgsql.NpgsqlParameter("@Importance", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(9).Value = _Importance

                arParms(10) = New Npgsql.NpgsqlParameter("@Status", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(10).Value = _Status


                arParms(11) = New Npgsql.NpgsqlParameter("@RecurrenceKey", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(11).Value = _RecurrenceKey

                arParms(12) = New Npgsql.NpgsqlParameter("@ResourceName", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(12).Value = _ResourceName

                arParms(13) = New Npgsql.NpgsqlParameter("@ItemId", NpgsqlTypes.NpgsqlDbType.Varchar, 250)
                arParms(13).Value = _ItemId

                arParms(14) = New Npgsql.NpgsqlParameter("@ChangeKey", NpgsqlTypes.NpgsqlDbType.Varchar, 250)
                arParms(14).Value = _ChangeKey

                arParms(15) = New Npgsql.NpgsqlParameter("@Color", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(15).Value = _Color

                arParms(16) = New Npgsql.NpgsqlParameter("@HtmlLink", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(16).Value = _HtmlLink

                arParms(17) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(17).Value = Nothing
                arParms(17).Direction = ParameterDirection.InputOutput

                lngActivityId = SqlDAL.ExecuteScalar(connString, "Activity_Add", arParms)

                Return lngActivityId

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Function RemoveActivity() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@DataKey", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = _ActivityID

                SqlDAL.ExecuteNonQuery(connString, "Activity_Rem", arParms)

            Catch ex As Exception

            End Try
        End Function

        Public Function UpdateActivity() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(17) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@AllDayEvent", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(0).Value = _AllDayEvent

                arParms(1) = New Npgsql.NpgsqlParameter("@ActivityDescription", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _ActivityDescription

                arParms(2) = New Npgsql.NpgsqlParameter("@Duration", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _Duration

                arParms(3) = New Npgsql.NpgsqlParameter("@Location", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(3).Value = _Location

                arParms(4) = New Npgsql.NpgsqlParameter("@StartDateTimeUtc", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(4).Value = _StartDateTimeUtc

                arParms(5) = New Npgsql.NpgsqlParameter("@Subject", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(5).Value = _Subject

                arParms(6) = New Npgsql.NpgsqlParameter("@EnableReminder", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(6).Value = _EnableReminder

                arParms(7) = New Npgsql.NpgsqlParameter("@ReminderInterval", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(7).Value = _ReminderInterval

                arParms(8) = New Npgsql.NpgsqlParameter("@ShowTimeAs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(8).Value = _ShowTimeAs

                arParms(9) = New Npgsql.NpgsqlParameter("@Importance", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(9).Value = _Importance

                arParms(10) = New Npgsql.NpgsqlParameter("@Status", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(10).Value = _Status

                arParms(11) = New Npgsql.NpgsqlParameter("@RecurrenceKey", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(11).Value = _RecurrenceKey

                arParms(12) = New Npgsql.NpgsqlParameter("@DataKey", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(12).Value = _ActivityID

                arParms(13) = New Npgsql.NpgsqlParameter("@VarianceKey", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(13).Value = _VarianceKey

                arParms(14) = New Npgsql.NpgsqlParameter("@ItemIdOccur", NpgsqlTypes.NpgsqlDbType.Varchar, 250)
                arParms(14).Value = _ItemIdOccur

                arParms(15) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(15).Value = _ModeType

                arParms(16) = New Npgsql.NpgsqlParameter("@Color", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(16).Value = _Color

                arParms(17) = New Npgsql.NpgsqlParameter("@HtmlLink", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(17).Value = _HtmlLink

                SqlDAL.ExecuteNonQuery(connString, "Activity_Upd", arParms)

                Return True

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Private _Priority As Long
        Public Property Priority() As Long
            Get
                Return _Priority
            End Get
            Set(ByVal Value As Long)
                _Priority = Value
            End Set
        End Property

        Private _FollowUpStatus As Long
        Public Property FollowUpStatus() As Long
            Get
                Return _FollowUpStatus
            End Get
            Set(ByVal Value As Long)
                _FollowUpStatus = Value
            End Set
        End Property


        Private _Activity As Long
        Public Property Activity() As Long
            Get
                Return _Activity
            End Get
            Set(ByVal Value As Long)
                _Activity = Value
            End Set
        End Property

        Private _Comments As String
        Public Property Comments() As String
            Get
                Return _Comments
            End Get
            Set(ByVal Value As String)
                _Comments = Value
            End Set
        End Property
        Private _ContactID As Long
        Public Property ContactID() As Long
            Get
                Return _ContactID
            End Get
            Set(ByVal Value As Long)
                _ContactID = Value
            End Set
        End Property
        Public Function UpdateActivityFromBiz() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@DataKey", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = _ActivityID

                arParms(1) = New Npgsql.NpgsqlParameter("@Priority", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _Priority

                arParms(2) = New Npgsql.NpgsqlParameter("@Activity", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _Activity

                arParms(3) = New Npgsql.NpgsqlParameter("@FollowUpStatus", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _FollowUpStatus

                arParms(4) = New Npgsql.NpgsqlParameter("@Comments", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(4).Value = _Comments

                arParms(5) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _ContactID

                SqlDAL.ExecuteNonQuery(connString, "Activity_UpdFromBiz", arParms)

                Return True

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function AddVarience() As Integer
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(19) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@AllDayEvent", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(0).Value = _AllDayEvent

                arParms(1) = New Npgsql.NpgsqlParameter("@ActivityDescription", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _ActivityDescription

                arParms(2) = New Npgsql.NpgsqlParameter("@Duration", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _Duration

                arParms(3) = New Npgsql.NpgsqlParameter("@Location", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(3).Value = _Location

                arParms(4) = New Npgsql.NpgsqlParameter("@StartDateTimeUtc", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(4).Value = _StartDateTimeUtc

                arParms(5) = New Npgsql.NpgsqlParameter("@Subject", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(5).Value = _Subject

                arParms(6) = New Npgsql.NpgsqlParameter("@EnableReminder", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(6).Value = _EnableReminder

                arParms(7) = New Npgsql.NpgsqlParameter("@ReminderInterval", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(7).Value = _ReminderInterval

                arParms(8) = New Npgsql.NpgsqlParameter("@ShowTimeAs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(8).Value = _ShowTimeAs

                arParms(9) = New Npgsql.NpgsqlParameter("@Importance", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(9).Value = _Importance

                arParms(10) = New Npgsql.NpgsqlParameter("@Status", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(10).Value = _Status


                arParms(11) = New Npgsql.NpgsqlParameter("@RecurrenceKey", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(11).Value = _RecurrenceKey

                arParms(12) = New Npgsql.NpgsqlParameter("@VarianceKey", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(12).Value = _VarianceKey

                arParms(13) = New Npgsql.NpgsqlParameter("@OriginalStartDateTimeUtc", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(13).Value = _OriginalStartDateTimeUtc

                arParms(14) = New Npgsql.NpgsqlParameter("@ResourceName", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(14).Value = _ResourceName

                arParms(15) = New Npgsql.NpgsqlParameter("@ItemId", NpgsqlTypes.NpgsqlDbType.Varchar, 250)
                arParms(15).Value = _ItemId

                arParms(16) = New Npgsql.NpgsqlParameter("@ChangeKey", NpgsqlTypes.NpgsqlDbType.Varchar, 250)
                arParms(16).Value = _ChangeKey

                arParms(17) = New Npgsql.NpgsqlParameter("@ItemIdOccur", NpgsqlTypes.NpgsqlDbType.Varchar, 250)
                arParms(17).Value = _ItemIdOccur

                arParms(18) = New Npgsql.NpgsqlParameter("@Color", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(18).Value = _Color

                arParms(19) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(19).Value = Nothing
                arParms(19).Direction = ParameterDirection.InputOutput

                Dim ActivityIDD As Integer

                ActivityIDD = SqlDAL.ExecuteScalar(connString, "Variance_Add", arParms)

                Return ActivityIDD

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function AddRecurring() As Integer
            Dim recursionid As Integer = 0
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

            arParms(0) = New Npgsql.NpgsqlParameter("@EndDateUtc", NpgsqlTypes.NpgsqlDbType.Timestamp)
            arParms(0).Value = _EndDateUtc

            arParms(1) = New Npgsql.NpgsqlParameter("@DayOfWeekMaskUtc", NpgsqlTypes.NpgsqlDbType.Integer)
            arParms(1).Value = _DayOfWeekMaskUtc


            arParms(2) = New Npgsql.NpgsqlParameter("@UtcOffset", NpgsqlTypes.NpgsqlDbType.Integer)
            arParms(2).Value = _UtcOffset


            arParms(3) = New Npgsql.NpgsqlParameter("@DayOfMonth", NpgsqlTypes.NpgsqlDbType.Integer)
            arParms(3).Value = _DayOfMonth

            arParms(4) = New Npgsql.NpgsqlParameter("@MonthOfYear", NpgsqlTypes.NpgsqlDbType.Integer)
            arParms(4).Value = _MonthOfYear

            arParms(5) = New Npgsql.NpgsqlParameter("@PeriodMultiple", NpgsqlTypes.NpgsqlDbType.Integer)
            arParms(5).Value = _PeriodMultiple

            arParms(6) = New Npgsql.NpgsqlParameter("@Period", NpgsqlTypes.NpgsqlDbType.Char)
            arParms(6).Value = _Period

            arParms(7) = New Npgsql.NpgsqlParameter("@EditType", NpgsqlTypes.NpgsqlDbType.Integer)
            arParms(7).Value = _EditType

            arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
            arParms(8).Value = Nothing
            arParms(8).Direction = ParameterDirection.InputOutput

            recursionid = CLng(SqlDAL.ExecuteScalar(connString, "Recurrence_Add", arParms))
            Return recursionid

        End Function

        Public Function FetchVariance() As DataTable
            Try

                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(9) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@StartDateTimeUtc", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(0).Value = _StartDateTimeUtc

                arParms(1) = New Npgsql.NpgsqlParameter("@EndDateTimeUtc", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _EndDateUtc

                arParms(2) = New Npgsql.NpgsqlParameter("@OrganizerName", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(2).Value = _ResourceName

                arParms(3) = New Npgsql.NpgsqlParameter("@RecurrenceKey", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _RecurrenceKey

                arParms(4) = New Npgsql.NpgsqlParameter("@VarianceKey", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(4).Value = _VarianceKey

                ds = SqlDAL.ExecuteDataset(connString, "Variance_Sel", arParms)

                Return ds.Tables(0)

            Catch ex As Exception

            End Try

        End Function


        Sub UpdateVariance()
            Try


                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(14) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@AllDayEvent", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(0).Value = _AllDayEvent

                arParms(1) = New Npgsql.NpgsqlParameter("@ActivityDescription", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _ActivityDescription

                arParms(2) = New Npgsql.NpgsqlParameter("@Duration", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _Duration

                arParms(3) = New Npgsql.NpgsqlParameter("@Location", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(3).Value = _Location

                arParms(4) = New Npgsql.NpgsqlParameter("@StartDateTimeUtc", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(4).Value = _StartDateTimeUtc

                arParms(5) = New Npgsql.NpgsqlParameter("@Subject", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(5).Value = _Subject

                arParms(6) = New Npgsql.NpgsqlParameter("@EnableReminder", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(6).Value = _EnableReminder

                arParms(7) = New Npgsql.NpgsqlParameter("@ReminderInterval", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(7).Value = _ReminderInterval

                arParms(8) = New Npgsql.NpgsqlParameter("@ShowTimeAs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(8).Value = _ShowTimeAs

                arParms(9) = New Npgsql.NpgsqlParameter("@Importance", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(9).Value = _Importance

                arParms(10) = New Npgsql.NpgsqlParameter("@Status", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(10).Value = _Status

                arParms(11) = New Npgsql.NpgsqlParameter("@OriginalStartDateTimeUtc", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(11).Value = _OriginalStartDateTimeUtc

                arParms(12) = New Npgsql.NpgsqlParameter("@RecurrenceKey", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(12).Value = _RecurrenceKey

                arParms(13) = New Npgsql.NpgsqlParameter("@DataKey", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(13).Value = _ActivityID

                arParms(14) = New Npgsql.NpgsqlParameter("@Color", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(14).Value = _Color

                SqlDAL.ExecuteNonQuery(connString, "Variance_Upd", arParms)
            Catch ex As Exception

            End Try
        End Sub
        Sub RemoveVarience()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@Status", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = _Status


                arParms(1) = New Npgsql.NpgsqlParameter("@ActivityID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _ActivityID



                SqlDAL.ExecuteNonQuery(connString, "Variance_Rem", arParms)
            Catch ex As Exception
                Throw ex
            End Try


        End Sub
        Sub RemoveAllVarience()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@ActivityID", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(0).Value = _ActivityID

                SqlDAL.ExecuteNonQuery(connString, "Variance_RemAll", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Sub UpdateRecurrence()

            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(9) {}

            arParms(0) = New Npgsql.NpgsqlParameter("@EndDateUtc", NpgsqlTypes.NpgsqlDbType.Timestamp)
            arParms(0).Value = _EndDateUtc

            arParms(1) = New Npgsql.NpgsqlParameter("@DayOfWeekMaskUtc", NpgsqlTypes.NpgsqlDbType.Integer)
            arParms(1).Value = _DayOfWeekMaskUtc


            arParms(2) = New Npgsql.NpgsqlParameter("@UtcOffset", NpgsqlTypes.NpgsqlDbType.Integer)
            arParms(2).Value = _UtcOffset


            arParms(3) = New Npgsql.NpgsqlParameter("@DayOfMonth", NpgsqlTypes.NpgsqlDbType.Integer)
            arParms(3).Value = _DayOfMonth

            arParms(4) = New Npgsql.NpgsqlParameter("@MonthOfYear", NpgsqlTypes.NpgsqlDbType.Integer)
            arParms(4).Value = _MonthOfYear

            arParms(5) = New Npgsql.NpgsqlParameter("@PeriodMultiple", NpgsqlTypes.NpgsqlDbType.Integer)
            arParms(5).Value = _PeriodMultiple

            arParms(6) = New Npgsql.NpgsqlParameter("@Period", NpgsqlTypes.NpgsqlDbType.Char)
            arParms(6).Value = _Period

            arParms(7) = New Npgsql.NpgsqlParameter("@EditType", NpgsqlTypes.NpgsqlDbType.Integer)
            arParms(7).Value = _EditType

            arParms(8) = New Npgsql.NpgsqlParameter("@LastReminderDateTimeUtc", NpgsqlTypes.NpgsqlDbType.Timestamp)
            arParms(8).Value = _LastReminderDateTimeUtc

            arParms(9) = New Npgsql.NpgsqlParameter("@RecurrenceID", NpgsqlTypes.NpgsqlDbType.Integer)
            arParms(9).Value = _RecurrenceKey

            SqlDAL.ExecuteNonQuery(connString, "[Recurrence_Upd]", arParms)

        End Sub

        Public Sub UpdateReminderByReccurence()
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

            arParms(0) = New Npgsql.NpgsqlParameter("@LastReminderDateTimeUtc", NpgsqlTypes.NpgsqlDbType.Timestamp)
            arParms(0).Value = _LastReminderDateTimeUtc

            arParms(1) = New Npgsql.NpgsqlParameter("@RecurrenceID", NpgsqlTypes.NpgsqlDbType.Integer)
            arParms(1).Value = _RecurrenceKey

            SqlDAL.ExecuteNonQuery(connString, "Reminder_UpdByRecurrence", arParms)
        End Sub
        Public Sub UpdateReminderByActivityId()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@Status", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = _Status

                arParms(1) = New Npgsql.NpgsqlParameter("@ActivityID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _ActivityID


                SqlDAL.ExecuteNonQuery(connString, "Reminder_Upd", arParms)
            Catch ex As Exception

            End Try
        End Sub
        Public Sub UpdateReminderSnoozeByActivityId()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@LastSnoozDateTimeUtc", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(0).Value = _LastReminderDateTimeUtc

                arParms(1) = New Npgsql.NpgsqlParameter("@ActivityID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _ActivityID


                SqlDAL.ExecuteNonQuery(connString, "Reminder_Snooze", arParms)
            Catch ex As Exception

            End Try
        End Sub
        Public Sub RemoveReccurence()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@DataKey", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = _ActivityID

                arParms(1) = New Npgsql.NpgsqlParameter("@RecurrenceID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _RecurrenceKey

                SqlDAL.ExecuteNonQuery(connString, "Recurrence_Rem", arParms)
            Catch ex As Exception

            End Try
        End Sub
        Public Function GetTempCalandar() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@StartDateTimeUtc", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(0).Value = _StartDateTimeUtc


                arParms(1) = New Npgsql.NpgsqlParameter("@ItemId", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _ItemId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetTempCalandar", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function getActivityByActId() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@ActivityID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = _ActivityID

                arParms(1) = New Npgsql.NpgsqlParameter("@mode", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(1).Value = _ActivityID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetAcivitybyActId", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetUsers() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@GroupId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Value = _GroupId

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_CalendarEmployees", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try

        End Function
        Public Function GetResourceId() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetResourceId", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try

        End Function
        Public Sub InsertImapNewMails()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(18) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numUid", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Value = _Uid

                arParms(3) = New Npgsql.NpgsqlParameter("@vcSubject", NpgsqlTypes.NpgsqlDbType.Varchar, 500)
                arParms(3).Value = _Subject

                arParms(4) = New Npgsql.NpgsqlParameter("@vcBody", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(4).Value = _Body

                arParms(5) = New Npgsql.NpgsqlParameter("@vcBodyText", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(5).Value = _BodyText

                arParms(6) = New Npgsql.NpgsqlParameter("@dtReceivedOn", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(6).Value = _ReceivedOn

                arParms(7) = New Npgsql.NpgsqlParameter("@vcSize", NpgsqlTypes.NpgsqlDbType.Varchar, 50)
                arParms(7).Value = _vcSize

                arParms(8) = New Npgsql.NpgsqlParameter("@bitIsRead", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(8).Value = _IsRead

                arParms(9) = New Npgsql.NpgsqlParameter("@bitHasAttachments", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(9).Value = _HasAttachments

                arParms(10) = New Npgsql.NpgsqlParameter("@vcFromName", NpgsqlTypes.NpgsqlDbType.Varchar, 5000)
                arParms(10).Value = _FromName

                arParms(11) = New Npgsql.NpgsqlParameter("@vcToName", NpgsqlTypes.NpgsqlDbType.Varchar, 5000)
                arParms(11).Value = _ToName

                arParms(12) = New Npgsql.NpgsqlParameter("@vcCCName", NpgsqlTypes.NpgsqlDbType.Varchar, 5000)
                arParms(12).Value = _CCName

                arParms(13) = New Npgsql.NpgsqlParameter("@vcBCCName", NpgsqlTypes.NpgsqlDbType.Varchar, 5000)
                arParms(13).Value = _BCCName

                arParms(14) = New Npgsql.NpgsqlParameter("@vcAttachmentName", NpgsqlTypes.NpgsqlDbType.Varchar, 500)
                arParms(14).Value = _AttachmentName

                arParms(15) = New Npgsql.NpgsqlParameter("@vcAttachmenttype", NpgsqlTypes.NpgsqlDbType.Varchar, 500)
                arParms(15).Value = _AttachmentType

                arParms(16) = New Npgsql.NpgsqlParameter("@vcNewAttachmentName", NpgsqlTypes.NpgsqlDbType.Varchar, 500)
                arParms(16).Value = _NewAttachmentName

                arParms(17) = New Npgsql.NpgsqlParameter("@vcAttachmentSize", NpgsqlTypes.NpgsqlDbType.Varchar, 500)
                arParms(17).Value = _AttachmentSize

                arParms(18) = New Npgsql.NpgsqlParameter("@numNodeId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(18).Value = _NodeId

                SqlDAL.NoTimeOut = True
                SqlDAL.ExecuteNonQuery(connString, "USP_InsertImapNewMails", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function getImapInbox() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(10) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = _PageSize

                arParms(1) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(1).Value = _CurrentPage

                arParms(2) = New Npgsql.NpgsqlParameter("@srchSubjectBody", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(2).Value = _Subject

                arParms(3) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Direction = ParameterDirection.InputOutput
                arParms(3).Value = _TotalRecords

                arParms(4) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.Varchar, 50)
                arParms(4).Value = _columnName

                arParms(5) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.Varchar, 10)
                arParms(5).Value = _columnSortOrder

                arParms(6) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(6).Value = UserCntID

                arParms(7) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(7).Value = DomainID

                arParms(8) = New Npgsql.NpgsqlParameter("@tinttype", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(8).Value = _Type

                arParms(9) = New Npgsql.NpgsqlParameter("@ToEmail", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(9).Value = _ToEmail

                arParms(10) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(10).Value = Nothing
                arParms(10).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_ImapInboxItems", arParms)
                '_TotalRecords = arParms(4).Value
                Dim dttable As DataTable
                dttable = ds.Tables(0)
                _TotalRecords = dttable.Rows(0).Item("numEmailHstrID")
                dttable.Rows.RemoveAt(0)
                Return dttable
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function GetTree() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(9) {}
                Dim getconnection As New GetConnection

                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@NodeId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Value = _NodeId

                arParms(3) = New Npgsql.NpgsqlParameter("@NodeName", NpgsqlTypes.NpgsqlDbType.Varchar, 5000)
                arParms(3).Value = _NodeName

                arParms(4) = New Npgsql.NpgsqlParameter("@Mode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _ModeType

                arParms(5) = New Npgsql.NpgsqlParameter("@ParentId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(5).Value = _ParentId

                arParms(6) = New Npgsql.NpgsqlParameter("@ParentNodeText", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(6).Value = _ParentNodeText

                arParms(7) = New Npgsql.NpgsqlParameter("@numLastUid", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(7).Value = _Uid

                arParms(8) = New Npgsql.NpgsqlParameter("@bitHidden", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(8).Value = _bitHidden

                arParms(9) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(9).Value = Nothing
                arParms(9).Direction = ParameterDirection.InputOutput

                If _ModeType = 0 Or _ModeType = 7 Or _ModeType = 10 Then
                    ds = SqlDAL.ExecuteDataset(connString, "Usp_ManageInboxTree", arParms)
                    Return ds.Tables(0)
                Else
                    SqlDAL.ExecuteNonQuery(connString, "Usp_ManageInboxTree", arParms)
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'Added By Sachin Sadhu for Ordering MailBox ||Case No:516 ||Date:7th-dec-2013
        Public Function GetTreeOrderList() As DataSet
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection

                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_InboxTreeSort_NewSortOrder", arParms)
                Return ds


            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'Added By Sachin Sadhu for Ordering MailBox ||Case No:516 ||Date:7th-dec-2013
        Public Function UpdateTreeOrder(ByVal mSortOrder As Integer, ByVal intNodeId As Integer, ByVal intParentId As Integer, ByVal intUserCntID As Integer, ByVal intDomainID As Integer)
            Try
                Dim getConnection As New GetConnection
                Dim arParams() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}


                Dim connString As String = GetConnection.GetConnectionString


                arParams(0) = New Npgsql.NpgsqlParameter("@numSortOrder", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParams(0).Value = mSortOrder

                arParams(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParams(1).Value = intUserCntID

                arParams(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParams(2).Value = intDomainID

                arParams(3) = New Npgsql.NpgsqlParameter("@NodeId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParams(3).Value = intNodeId
                arParams(4) = New Npgsql.NpgsqlParameter("@ParentId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParams(4).Value = intParentId

                'USP_InboxTree_UpdateSortOrder()

                SqlDAL.ExecuteNonQuery(connString, "USP_InboxTreeSort_UpdateSortOrder", arParams)
            Catch ex As Exception

            End Try
        End Function
        Public Function getInboxOpenEmailByDivision(ByVal bDivision As Integer) As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint, 10)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.Bigint, 10)
                arParms(1).Value = _DivisionID

                arParms(2) = New Npgsql.NpgsqlParameter("@bDivision", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = bDivision

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_InboxOpenEmailByDivision", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ActivityFutureDate() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@OrganizerName", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(0).Value = _ResourceName

                arParms(1) = New Npgsql.NpgsqlParameter("@StartDateTimeUtc", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _StartDateTimeUtc

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "Activity_FutureDate", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Function Activity_GoogleEventId(ByVal GoogleEventId As String) As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@ActivityID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = _ActivityID

                arParms(1) = New Npgsql.NpgsqlParameter("@GoogleEventId", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(1).Value = GoogleEventId

                SqlDAL.ExecuteNonQuery(connString, "Activity_GoogleEventId", arParms)

            Catch ex As Exception

            End Try
        End Function

        Public Function GetAcivitybyGoogleEventId(ByVal GoogleEventId As String, ByVal ResourceID As Integer) As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@GoogleEventId", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(0).Value = GoogleEventId

                arParms(1) = New Npgsql.NpgsqlParameter("@ResourceID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = ResourceID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetAcivitybyGoogleEventId", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        ''' <summary>
        ''' TODO:Mofigy this method to work with comma seperated string instead of OpenXML
        ''' </summary>
        ''' <returns></returns>
        '''         ''' <remarks></remarks>
        Function ChangeEmailAsRead() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint, 10)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@strXml", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _strXml

                SqlDAL.ExecuteNonQuery(connString, "USP_ChangeEmailAsRead", arParms)

            Catch ex As Exception

            End Try
        End Function

        Function getNextPrevMailID() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint, 10)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.Bigint, 10)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numEmailHstrID", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(2).Value = _numEmailHstrID

                arParms(3) = New Npgsql.NpgsqlParameter("@numNodeId", NpgsqlTypes.NpgsqlDbType.Bigint, 10)
                arParms(3).Value = _NodeId

                arParms(4) = New Npgsql.NpgsqlParameter("@EmailStatus", NpgsqlTypes.NpgsqlDbType.Bigint, 10)
                arParms(4).Value = _EmailStatus

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_getNextPrevMailID", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Added By Rajesh it Create folder and it should get created in Gmail in realtime.||Date:27th-Nov-2014    
        Function Create_Folder(ByVal ParentFolder As String, ByVal NewFolder As String) As String
            Try
                Imap.LicenseKey = ConfigurationManager.AppSettings("ImapLicense")
                Dim objImap As New Imap
                objImap.SslProtocol = MailBee.Security.SecurityProtocol.TlsAuto
                Dim ObjOutlook As New COutlook
                Dim dtTable As DataTable

                ObjOutlook.DomainID = DomainID
                ObjOutlook.numEmailHstrID = numEmailHstrID
                ObjOutlook.ClientTimeZoneOffset = CCommon.ToInteger(Current.Session("ClientMachineUTCTimeOffset"))
                dtTable = ObjOutlook.getMail()

                Dim success As Boolean
                Dim ServerUrl As String
                Dim emailId As String
                Dim Password As String
                Dim dtCredientials As DataTable

                Dim objUserAccess As New UserAccess
                objUserAccess.UserCntID = UserCntID
                objUserAccess.DomainID = DomainID
                dtCredientials = objUserAccess.GetImapDtls()

                Dim strPassword As String
                Dim objCommon As New CCommon
                strPassword = objCommon.Decrypt(dtCredientials.Rows(0).Item("vcImapPassword").ToString)
                ServerUrl = dtCredientials.Rows(0).Item("vcImapServerUrl").ToString
                emailId = dtCredientials.Rows(0).Item("vcEmailID").ToString
                Password = strPassword

                success = objImap.Connect(ServerUrl, CInt(dtCredientials.Rows(0).Item("numPort")))
                If (success <> True) Then
                    Return False
                    Exit Function
                End If
                success = objImap.Login(emailId, Password, AuthenticationMethods.Auto, AuthenticationOptions.PreferSimpleMethods, Nothing)
                'objImap.SelectFolder(ParentFolder)

                If (success <> True) Then
                    Return False
                    Exit Function
                End If

                'If objImap.SelectFolder(ParentFolder) Then
                '    If objImap.MessageCount > 0 Then

                Return objImap.CreateFolder(NewFolder)

                '    End If
                'End If

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Added By Rajesh it Delete Folder in Gmail.
        '  http://www.afterlogic.com/mailbee-net/docs/MailBee.ImapMail.Imap.DeleteFolder.html ||Date:20th-Dec-2014    

        Function DeleteFolder(ByVal ParentFolder As String, ByVal DelFolder As String) As String
            Try

                Imap.LicenseKey = ConfigurationManager.AppSettings("ImapLicense")
                Dim objImap As New Imap
                objImap.SslProtocol = MailBee.Security.SecurityProtocol.TlsAuto
                Dim ObjOutlook As New COutlook
                Dim dtTable As DataTable

                'objImap.Log.Enabled = True

                ObjOutlook.DomainID = DomainID
                ObjOutlook.numEmailHstrID = numEmailHstrID
                ObjOutlook.ClientTimeZoneOffset = CCommon.ToInteger(Current.Session("ClientMachineUTCTimeOffset"))
                dtTable = ObjOutlook.getMail()

                Dim success As Boolean
                Dim ServerUrl As String
                Dim emailId As String
                Dim Password As String
                Dim dtCredientials As DataTable

                Dim objUserAccess As New UserAccess
                objUserAccess.UserCntID = UserCntID
                objUserAccess.DomainID = DomainID
                dtCredientials = objUserAccess.GetImapDtls()

                Dim strPassword As String
                Dim objCommon As New CCommon
                strPassword = objCommon.Decrypt(dtCredientials.Rows(0).Item("vcImapPassword").ToString)
                ServerUrl = dtCredientials.Rows(0).Item("vcImapServerUrl").ToString
                emailId = dtCredientials.Rows(0).Item("vcEmailID").ToString
                Password = strPassword

                success = objImap.Connect(ServerUrl, CInt(dtCredientials.Rows(0).Item("numPort")))
                If (success <> True) Then
                    Return False
                    Exit Function
                End If
                success = objImap.Login(emailId, Password, AuthenticationMethods.Auto, AuthenticationOptions.PreferSimpleMethods, Nothing)
                If (success <> True) Then
                    Return False
                    Exit Function
                End If

                objImap.DeleteFolder(DelFolder)

                objImap.Disconnect()

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function SetMessageFlags(p1 As String, p2 As Boolean, p3 As String, messageFlagAction As MessageFlagAction, p5 As Boolean) As Integer
            Throw New NotImplementedException
        End Function

        Public Sub ArchiveEmail(ByVal selectedEmails As String)
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numEmailHstrID", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(2).Value = selectedEmails

                SqlDAL.ExecuteNonQuery(connString, "USP_EmailHistory_Archive", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub Restore(ByVal selectedEmails As String)
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numEmailHstrID", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(2).Value = selectedEmails

                SqlDAL.ExecuteNonQuery(connString, "USP_EmailHistory_Restore", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub DeleteArchived(ByVal fromDate As Date, ByVal toDate As Date)
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@dtFrom", NpgsqlTypes.NpgsqlDbType.Date)
                arParms(2).Value = CCommon.ToSqlDate(fromDate)

                arParms(3) = New Npgsql.NpgsqlParameter("@dtTo", NpgsqlTypes.NpgsqlDbType.Date)
                arParms(3).Value = CCommon.ToSqlDate(toDate)

                SqlDAL.ExecuteNonQuery(connString, "USP_EmailHistory_DeleteArchived", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub MarkAsSpam(ByVal selectedEmails As String)
            Try
                Dim success As Boolean
                Dim ServerUrl As String
                Dim emailId As String
                Dim Password As String
                Dim strPassword As String
                Dim dtTable As DataTable
                Dim dtCredientials As DataTable

                Imap.LicenseKey = ConfigurationManager.AppSettings("ImapLicense")
                Dim objImap As New Imap
                objImap.SslProtocol = MailBee.Security.SecurityProtocol.TlsAuto
                'GET IMAP DETAILS OF USER
                Dim objUserAccess As New UserAccess
                objUserAccess.UserCntID = UserCntID
                objUserAccess.DomainID = DomainID
                dtCredientials = objUserAccess.GetImapDtls()

                Dim objCommon As New CCommon
                strPassword = objCommon.Decrypt(dtCredientials.Rows(0).Item("vcImapPassword").ToString)
                ServerUrl = dtCredientials.Rows(0).Item("vcImapServerUrl").ToString
                emailId = dtCredientials.Rows(0).Item("vcEmailID").ToString
                Password = strPassword

                'CONNECT TO IMAP WITH CREDENTIALS
                success = objImap.Connect(ServerUrl, CInt(dtCredientials.Rows(0).Item("numPort")))

                'RETURN IF ERROR OCUURED WHILE CONNECTING
                If (success <> True) Then
                    Exit Sub
                End If

                'TRY TO LOGIN WITH USER IMAP CREDENTIALS
                success = objImap.Login(emailId, Password, AuthenticationMethods.Auto, AuthenticationOptions.PreferSimpleMethods, Nothing)

                'RETURN IF ERROR OCUURED WHILE LOGIN WITH USER IMAP CREDENTIAL
                If (success <> True) Then
                    Exit Sub
                End If

                'GET UIDs OF SELECTED EMAILS
                dtTable = GetEmailHistryByIDs(selectedEmails)



                Try
                    'MOVES SELECTED EMAILS TO SPAM IN GMAIL
                    Dim lstfolders As FolderCollection
                    lstfolders = objImap.DownloadFolders()
                    Dim strFoldersFromServer As String
                    strFoldersFromServer = ""
                    For Each objFolder As Folder In lstfolders
                        Dim strParentNodeText As String
                        strParentNodeText = ""

                        If objFolder.ShortName = "Spam" Then
                            strFoldersFromServer = objFolder.Name
                        End If
                    Next
                    If Not dtTable Is Nothing AndAlso dtTable.Rows.Count > 0 Then
                        For Each newRow As DataRow In dtTable.Rows

                            If String.IsNullOrEmpty(CCommon.ToString(newRow("vcNodeName"))) Then
                                objImap.SelectFolder("Inbox")
                            Else
                                objImap.SelectFolder(CCommon.ToString(newRow("vcNodeName")))
                            End If
                            Dim objUIDResult As New MailBee.ImapMail.UidPlusResult()
                            Dim res As Boolean = objImap.MoveMessages(newRow("numUid").ToString(), True, strFoldersFromServer, objUIDResult)
                        Next
                    End If
                Catch ex As Exception

                End Try

                'DELETE SELECTED EMAIL FROM BIZ
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@vcEmailHstrID", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(0).Value = selectedEmails

                SqlDAL.ExecuteNonQuery(connString, "USP_EmailHistory_MarkAsSpam", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetEmailHistryByIDs(ByVal vcEmailHstrID As String) As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@vcEmailHstrID", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(0).Value = vcEmailHstrID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Value = UserCntID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_EmailHistory_GetByIDsString", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub RenameFolder(ByVal name As String)
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numNodeID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Value = NodeId

                arParms(3) = New Npgsql.NpgsqlParameter("@vcName", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(3).Value = name

                SqlDAL.ExecuteNonQuery(connString, "USP_InboxTree_RenameFolder", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetReminder() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@OrganizerName", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(0).Value = _ResourceName

                arParms(1) = New Npgsql.NpgsqlParameter("@StartDateTimeUtc", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _StartDateTimeUtc

                arParms(2) = New Npgsql.NpgsqlParameter("@EndDateTimeUtc", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _EndDateUtc

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetReminders", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetAlertDetail(ByVal numAlertType As Int16, ByVal contactID As Long) As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numAlertType", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = numAlertType

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DivisionID

                arParms(2) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = contactID

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = DomainID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_GetAlertPanelDetail", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    Public Class AttendeeCollection

        Private AttendeeList As List(Of Attendee)

        Public Sub Add(ByVal ObjAttendeeEntry As Attendee)
            If AttendeeList Is Nothing Then AttendeeList = New List(Of Attendee)
            AttendeeList.Add(ObjAttendeeEntry)
        End Sub

        Public Sub Save(lngActivityID As Long, lngContactID As Long, lngDomainID As Long)
            Try
                Dim strXML As String = ""

                If Not AttendeeList Is Nothing Then
                    strXML = CCommon.SerializeToXML(AttendeeList)
                End If

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@strRow", strXML, NpgsqlTypes.NpgsqlDbType.Text))
                    .Add(SqlDAL.Add_Parameter("@numActivityID", lngActivityID, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_ActivityAttendees", sqlParams.ToArray())

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
    Public Class Attendee
        Private _EmailId As String
        <XmlElement("EmailId")>
        Public Property EmailId() As String
            Get
                Return _EmailId
            End Get
            Set(ByVal value As String)
                _EmailId = value
            End Set
        End Property

        Private _ResponseStatus As String
        <XmlElement("ResponseStatus")>
        Public Property ResponseStatus() As String
            Get
                Return _ResponseStatus
            End Get
            Set(ByVal value As String)
                _ResponseStatus = value
            End Set
        End Property
    End Class
End Namespace
