'Created by anoop jayaraj
Option Explicit On
Option Strict On

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.TimeAndExpense

    Public Class TimeExpenseLeave
        Inherits BACRM.BusinessLogic.CBusinessBase


        Private _CategoryHDRID As Long = 0
        Private _TEType As Long = 0
        Private _CategoryID As Long
        Private _FromDate As Date = New Date(1753, 1, 1)
        Private _ToDate As Date = New Date(1753, 1, 1)
        Private _bitFromFullDay As Boolean
        Private _bitToFullDay As Boolean
        Private _Amount As Decimal
        Private _OppID As Long = 0
        Private _CaseID As Long = 0
        Private _ProID As Long = 0
        Private _ContractID As Long = 0
        Private _DivisionID As Long
        Private _Desc As String
        Private _CategoryType As Long
        'Private UserCntID As Long
        'Private DomainId As Long
        Private _strDate As String
        Private _Approved As Boolean
        Private _byteMode As Short
        Private _RolePercentage As Double
        Private _RoleID As Long
        Private _AmtCategory As Long
        Private _dtAmtAdded As Date = New Date(1753, 1, 1)
        Private _AmtAddHDRID As Long
        Private _OppBizDocsId As Long
        Private _ClientTimeZoneOffset As Integer
        Private _StageId As Long = 0

        Private _ItemID As Long
        Public Property ItemID() As Long
            Get
                Return _ItemID
            End Get
            Set(ByVal value As Long)
                _ItemID = value
            End Set
        End Property


        Private _bitReimburse As Boolean = False
        Public Property bitReimburse() As Boolean
            Get
                Return _bitReimburse
            End Get
            Set(ByVal Value As Boolean)
                _bitReimburse = Value
            End Set
        End Property

        Private _bitLeaveTaken As Boolean
        Public Property bitLeaveTaken() As Boolean
            Get
                Return _bitLeaveTaken
            End Get
            Set(ByVal value As Boolean)
                _bitLeaveTaken = value
            End Set
        End Property

        Public Property StageId() As Long
            Get
                Return _StageId
            End Get
            Set(ByVal Value As Long)
                _StageId = Value
            End Set
        End Property
        Public Property TEType() As Long
            Get
                Return _TEType
            End Get
            Set(ByVal Value As Long)
                _TEType = Value
            End Set
        End Property
        Public Property ClientTimeZoneOffset() As Integer
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal Value As Integer)
                _ClientTimeZoneOffset = Value
            End Set
        End Property
        Public Property OppBizDocsId() As Long
            Get
                Return _OppBizDocsId
            End Get
            Set(ByVal Value As Long)
                _OppBizDocsId = Value
            End Set
        End Property

        Public Property AmtAddHDRID() As Long
            Get
                Return _AmtAddHDRID
            End Get
            Set(ByVal Value As Long)
                _AmtAddHDRID = Value
            End Set
        End Property

        Public Property dtAmtAdded() As Date
            Get
                Return _dtAmtAdded
            End Get
            Set(ByVal Value As Date)
                _dtAmtAdded = Value
            End Set
        End Property

        Public Property AmtCategory() As Long
            Get
                Return _AmtCategory
            End Get
            Set(ByVal Value As Long)
                _AmtCategory = Value
            End Set
        End Property

        Public Property RoleID() As Long
            Get
                Return _RoleID
            End Get
            Set(ByVal Value As Long)
                _RoleID = Value
            End Set
        End Property

        Public Property RolePercentage() As Double
            Get
                Return _RolePercentage
            End Get
            Set(ByVal Value As Double)
                _RolePercentage = Value
            End Set
        End Property

        Public Property byteMode() As Short
            Get
                Return _byteMode
            End Get
            Set(ByVal Value As Short)
                _byteMode = Value
            End Set
        End Property

        Public Property Approved() As Boolean
            Get
                Return _Approved
            End Get
            Set(ByVal Value As Boolean)
                _Approved = Value
            End Set
        End Property

        Public Property strDate() As String
            Get
                Return _strDate
            End Get
            Set(ByVal Value As String)
                _strDate = Value
            End Set
        End Property

        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property

        'Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal Value As Long)
        '        UserCntID = Value
        '    End Set
        'End Property

        Public Property CategoryType() As Long
            Get
                Return _CategoryType
            End Get
            Set(ByVal Value As Long)
                _CategoryType = Value
            End Set
        End Property

        Public Property Desc() As String
            Get
                Return _Desc
            End Get
            Set(ByVal Value As String)
                _Desc = Value
            End Set
        End Property

        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property

        Public Property OppID() As Long
            Get
                Return _OppID
            End Get
            Set(ByVal Value As Long)
                _OppID = Value
            End Set
        End Property
        Public Property ProID() As Long
            Get
                Return _ProID
            End Get
            Set(ByVal Value As Long)
                _ProID = Value
            End Set
        End Property
        Public Property CaseID() As Long
            Get
                Return _CaseID
            End Get
            Set(ByVal Value As Long)
                _CaseID = Value
            End Set
        End Property
        Public Property Amount() As Decimal
            Get
                Return _Amount
            End Get
            Set(ByVal Value As Decimal)
                _Amount = Value
            End Set
        End Property
        Public Property ContractID() As Long
            Get
                Return _ContractID
            End Get
            Set(ByVal Value As Long)
                _ContractID = Value
            End Set
        End Property

        Public Property bitToFullDay() As Boolean
            Get
                Return _bitToFullDay
            End Get
            Set(ByVal Value As Boolean)
                _bitToFullDay = Value
            End Set
        End Property

        Public Property bitFromFullDay() As Boolean
            Get
                Return _bitFromFullDay
            End Get
            Set(ByVal Value As Boolean)
                _bitFromFullDay = Value
            End Set
        End Property

        Public Property ToDate() As DateTime
            Get
                Return _ToDate
            End Get
            Set(ByVal Value As DateTime)
                _ToDate = Value
            End Set
        End Property

        Public Property FromDate() As DateTime
            Get
                Return _FromDate
            End Get
            Set(ByVal Value As DateTime)
                _FromDate = Value
            End Set
        End Property

        Public Property CategoryID() As Long
            Get
                Return _CategoryID
            End Get
            Set(ByVal Value As Long)
                _CategoryID = Value
            End Set
        End Property

        Public Property CategoryHDRID() As Long
            Get
                Return _CategoryHDRID
            End Get
            Set(ByVal Value As Long)
                _CategoryHDRID = Value
            End Set
        End Property


        Private _strRows As String
        Public Property strRows() As String
            Get
                Return _strRows
            End Get
            Set(ByVal value As String)
                _strRows = value
            End Set
        End Property

        'Private _Mode As Short
        'Public Property Mode() As Short
        '    Get
        '        Return _Mode
        '    End Get
        '    Set(ByVal value As Short)
        '        _Mode = value
        '    End Set
        'End Property

        Private _ChartAcntID As Long
        Public Property ChartAcntID() As Long
            Get
                Return _ChartAcntID
            End Get
            Set(ByVal value As Long)
                _ChartAcntID = value
            End Set
        End Property

        Private _OppItemID As Long
        Public Property OppItemID() As Long
            Get
                Return _OppItemID
            End Get
            Set(ByVal value As Long)
                _OppItemID = value
            End Set
        End Property
        Private _ExpId As Long
        Public Property Expid() As Long
            Get
                Return _ExpId
            End Get
            Set(ByVal value As Long)
                _ExpId = value
            End Set
        End Property
        Private _ApprovalStatus As Long
        Public Property ApprovalStatus() As Long
            Get
                Return _ApprovalStatus
            End Get
            Set(ByVal value As Long)
                _ApprovalStatus = value
            End Set
        End Property
        Private _ServiceItemID As Long
        Public Property ServiceItemID() As Long
            Get
                Return _ServiceItemID
            End Get
            Set(ByVal value As Long)
                _ServiceItemID = value
            End Set
        End Property
        Private _CreatedBy As Long
        Public Property CreatedBy() As Long
            Get
                Return _CreatedBy
            End Get
            Set(ByVal value As Long)
                _CreatedBy = value
            End Set
        End Property

        Public Property IsAbsent As Boolean
        Public Property UserRightType As Long

        Public Function ManageTimeAndExpense() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(26) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numCategoryHDRID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _CategoryHDRID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintTEType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _TEType

                arParms(2) = New Npgsql.NpgsqlParameter("@numCategory", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _CategoryID

                arParms(3) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(3).Value = _FromDate

                arParms(4) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(4).Value = _ToDate

                arParms(5) = New Npgsql.NpgsqlParameter("@bitFromFullDay", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(5).Value = _bitFromFullDay

                arParms(6) = New Npgsql.NpgsqlParameter("@bitToFullDay", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(6).Value = _bitToFullDay

                arParms(7) = New Npgsql.NpgsqlParameter("@monAmount", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(7).Value = _Amount

                arParms(8) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = _OppID

                arParms(9) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = _DivisionID

                arParms(10) = New Npgsql.NpgsqlParameter("@txtDesc", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(10).Value = _Desc

                arParms(11) = New Npgsql.NpgsqlParameter("@numType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(11).Value = _CategoryType

                arParms(12) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(12).Value = UserCntID

                arParms(13) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(13).Value = DomainId

                arParms(14) = New Npgsql.NpgsqlParameter("@numCaseId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(14).Value = _CaseID

                arParms(15) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(15).Value = _ProID

                arParms(16) = New Npgsql.NpgsqlParameter("@numContractId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(16).Value = _ContractID

                arParms(17) = New Npgsql.NpgsqlParameter("@numOppBizDocsId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(17).Value = _OppBizDocsId

                arParms(18) = New Npgsql.NpgsqlParameter("@numStageId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(18).Value = _StageId

                arParms(19) = New Npgsql.NpgsqlParameter("@bitReimburse", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(19).Value = _bitReimburse

                arParms(20) = New Npgsql.NpgsqlParameter("@bitLeaveTaken", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(20).Direction = ParameterDirection.InputOutput
                arParms(20).Value = _bitLeaveTaken

                arParms(21) = New Npgsql.NpgsqlParameter("@numOppItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(21).Value = _OppItemID

                arParms(22) = New Npgsql.NpgsqlParameter("@numExpId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(22).Value = _ExpId

                arParms(23) = New Npgsql.NpgsqlParameter("@numApprovalComplete", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(23).Value = _ApprovalStatus

                arParms(24) = New Npgsql.NpgsqlParameter("@numServiceItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(24).Value = _ServiceItemID

                arParms(25) = New Npgsql.NpgsqlParameter("@numClassID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(25).Value = _ClassID

                arParms(26) = New Npgsql.NpgsqlParameter("@numCreatedBy", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(26).Value = _CreatedBy

                Dim objParam() As Object
                objParam = arParms.ToArray()

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageTimeAndExpense", objParam, True)
                _CategoryHDRID = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)
                _bitLeaveTaken = Convert.ToBoolean(DirectCast(objParam, Npgsql.NpgsqlParameter())(20).Value)

                Return CBool(_bitLeaveTaken)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetOppByDivID() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetOppbyCompany", arParms).Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetTimeAndExpDetails() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(11) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@strDate", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(2).Value = _strDate

                arParms(3) = New Npgsql.NpgsqlParameter("@numCategoryHDRID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _CategoryHDRID

                arParms(4) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _ClientTimeZoneOffset

                arParms(5) = New Npgsql.NpgsqlParameter("@tintTEType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _TEType

                arParms(6) = New Npgsql.NpgsqlParameter("@numCaseId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _CaseID

                arParms(7) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _ProID

                arParms(8) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = _OppID

                arParms(9) = New Npgsql.NpgsqlParameter("@numStageId", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(9).Value = _StageId

                arParms(10) = New Npgsql.NpgsqlParameter("@numCategory", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(10).Value = _CategoryID

                arParms(11) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(11).Value = Nothing
                arParms(11).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetTimeAndExpense", arParms).Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function GetTimeAndExpDetailsByDate() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(13) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@strDate", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(2).Value = _strDate

                arParms(3) = New Npgsql.NpgsqlParameter("@numCategoryHDRID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _CategoryHDRID

                arParms(4) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _ClientTimeZoneOffset

                arParms(5) = New Npgsql.NpgsqlParameter("@tintTEType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _TEType

                arParms(6) = New Npgsql.NpgsqlParameter("@numCaseId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _CaseID

                arParms(7) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _ProID

                arParms(8) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = _OppID

                arParms(9) = New Npgsql.NpgsqlParameter("@numStageId", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(9).Value = _StageId

                arParms(10) = New Npgsql.NpgsqlParameter("@numCategory", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(10).Value = _CategoryID

                arParms(11) = New Npgsql.NpgsqlParameter("@FromDate", NpgsqlTypes.NpgsqlDbType.Date)
                arParms(11).Value = _FromDate

                arParms(12) = New Npgsql.NpgsqlParameter("@ToDate", NpgsqlTypes.NpgsqlDbType.Date)
                arParms(12).Value = _ToDate

                arParms(13) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(13).Value = Nothing
                arParms(13).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetTimeAndExpenseDate", arParms).Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetTimeAndExpense_BudgetTotal() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ProID

                arParms(2) = New Npgsql.NpgsqlParameter("@numStageId", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _StageId

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetTimeAndExpense_BudgetTotal", arParms).Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        ''' <summary>
        ''' Added by chintan
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function GetTimeAndExpenseDetails() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = _byteMode

                arParms(1) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ProID

                arParms(2) = New Npgsql.NpgsqlParameter("@numStageId", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _StageId

                arParms(3) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _DivisionID

                arParms(4) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = UserCntID

                arParms(5) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = DomainID

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetTimeAndExpenseDetail", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetMonthlyTimeAndExp() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@FromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _FromDate

                arParms(3) = New Npgsql.NpgsqlParameter("@numCategoryType", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _CategoryType

                arParms(4) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _ClientTimeZoneOffset

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetTimeExpWeekly", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteTimeExpLeave() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numCategoryHDRID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CategoryHDRID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId


                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteTimExpLeave", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function UpdateLeaveDtls() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numCategoryHDRID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CategoryHDRID

                arParms(1) = New Npgsql.NpgsqlParameter("@numType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _CategoryType

                arParms(2) = New Npgsql.NpgsqlParameter("@bitApproved", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(2).Value = _Approved

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateLeaveDetails", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function ManageUserRoles() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = _byteMode

                arParms(1) = New Npgsql.NpgsqlParameter("@RolesID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _RoleID

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = UserCntID

                arParms(3) = New Npgsql.NpgsqlParameter("@fltPercentage", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(3).Value = _RolePercentage

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return (SqlDAL.ExecuteDataset(connString, "USP_ManageRoles", arParms)).Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function ManageAddAmount() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = _byteMode

                arParms(1) = New Npgsql.NpgsqlParameter("@numAmtCategory", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _AmtCategory

                arParms(2) = New Npgsql.NpgsqlParameter("@monAmount", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(2).Value = _Amount

                arParms(3) = New Npgsql.NpgsqlParameter("@dtAmtAdded", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(3).Value = _dtAmtAdded

                arParms(4) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = UserCntID

                arParms(5) = New Npgsql.NpgsqlParameter("@numAddHDRID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _AmtAddHDRID

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                Return (SqlDAL.ExecuteDataset(connString, "USP_ManageAddAmount", arParms)).Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function GetProByDivID() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _byteMode

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetProbyDivId", arParms).Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetCaseByDivID() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _byteMode

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetCasebyDivId", arParms).Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetOppByCaseID() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numCaseID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CaseID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetOppByCase", arParms).Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function GetOppByProId() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numProID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ProID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetOppByProID", arParms).Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetTimeLineAddedBizDocs() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetTimeLineAddedBizDocs", arParms).Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function GetProjectOpp() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = 0

                arParms(1) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ProID

                arParms(2) = New Npgsql.NpgsqlParameter("@numCaseId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = 0

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_ProCaseOppLinked", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetCaseOpp() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = 1

                arParms(1) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = 0

                arParms(2) = New Npgsql.NpgsqlParameter("@numCaseId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _CaseID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_ProCaseOppLinked", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Private _Entry_Date As Date = New Date(1753, 1, 1)
        Public Property Entry_Date() As DateTime
            Get
                Return _Entry_Date
            End Get
            Set(ByVal Value As DateTime)
                _Entry_Date = Value
            End Set
        End Property

        Private _JournalID As Long
        Public Property JournalID() As Long
            Get
                Return _JournalID
            End Get
            Set(ByVal value As Long)
                _JournalID = value
            End Set
        End Property

        Private _ClassID As Long
        Public Property ClassID() As Long
            Get
                Return _ClassID
            End Get
            Set(ByVal value As Long)
                _ClassID = value
            End Set
        End Property

        Public Function GetTimeAndMaterials() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ProID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetTimeAndMaterials", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetTimeEntries(ByVal employeeName As String, ByVal teams As String, ByVal payrollType As Short, ByVal sortColumn As String, ByVal sortOrder As String) As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@dtFromDate", FromDate, NpgsqlTypes.NpgsqlDbType.Date))
                    .Add(SqlDAL.Add_Parameter("@dtToDate", ToDate, NpgsqlTypes.NpgsqlDbType.Date))
                    .Add(SqlDAL.Add_Parameter("@vcEmployeeName", employeeName, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcTeams", teams, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@tintPayrollType", payrollType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@vcSortColumn", sortColumn, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcSortOrder", sortOrder, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@tintUserRightType", UserRightType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_TimeAndExpense_GetTimeEntries", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function


        Public Function GetExpenseEntries(ByVal employeeName As String, ByVal teams As String, ByVal payrollType As Short, ByVal sortColumn As String, ByVal sortOrder As String) As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@dtFromDate", FromDate, NpgsqlTypes.NpgsqlDbType.Date))
                    .Add(SqlDAL.Add_Parameter("@dtToDate", ToDate, NpgsqlTypes.NpgsqlDbType.Date))
                    .Add(SqlDAL.Add_Parameter("@vcEmployeeName", employeeName, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcTeams", teams, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@tintPayrollType", payrollType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@vcSortColumn", sortColumn, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcSortOrder", sortOrder, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@tintUserRightType", UserRightType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_TimeAndExpense_GetExpenseEntries", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetTimeSummary(ByVal employeeName As String, ByVal teams As String, ByVal payrollType As Short) As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@dtFromDate", FromDate, NpgsqlTypes.NpgsqlDbType.Date))
                    .Add(SqlDAL.Add_Parameter("@dtToDate", ToDate, NpgsqlTypes.NpgsqlDbType.Date))
                    .Add(SqlDAL.Add_Parameter("@vcEmployeeName", employeeName, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcTeams", teams, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@tintPayrollType", payrollType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_TimeAndExpense_GetTimeSummary", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetExpenseSummary(ByVal employeeName As String, ByVal teams As String, ByVal payrollType As Short) As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@dtFromDate", FromDate, NpgsqlTypes.NpgsqlDbType.Date))
                    .Add(SqlDAL.Add_Parameter("@dtToDate", ToDate, NpgsqlTypes.NpgsqlDbType.Date))
                    .Add(SqlDAL.Add_Parameter("@vcEmployeeName", employeeName, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcTeams", teams, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@tintPayrollType", payrollType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_TimeAndExpense_GetExpenseSummary", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetTimeEntriesDetail() As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@dtFromDate", FromDate, NpgsqlTypes.NpgsqlDbType.Date))
                    .Add(SqlDAL.Add_Parameter("@dtToDate", ToDate, NpgsqlTypes.NpgsqlDbType.Date))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_TimeAndExpense_GetTimeEntriesDetail", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetExpenseEntriesDetail() As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@dtFromDate", FromDate, NpgsqlTypes.NpgsqlDbType.Date))
                    .Add(SqlDAL.Add_Parameter("@dtToDate", ToDate, NpgsqlTypes.NpgsqlDbType.Date))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_TimeAndExpense_GetExpenseEntriesDetail", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function SaveUserClockInOut() As Boolean
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numApprovalComplete", ApprovalStatus, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return Convert.ToBoolean(SqlDAL.ExecuteScalar(connString, "USP_TimeAndExpense_SaveUserClockInOut", sqlParams.ToArray()))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function IsUserClockIn() As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_TimeAndExpense_IsUserClockIn", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetUserClockInOutDetail() As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@dtFromDate", FromDate, NpgsqlTypes.NpgsqlDbType.Timestamp))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_TimeAndExpense_GetUserClockInOutDetail", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub Delete()
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numCategoryHDRID", CategoryHDRID, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_TimeAndExpense_Delete", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub
    End Class

End Namespace
