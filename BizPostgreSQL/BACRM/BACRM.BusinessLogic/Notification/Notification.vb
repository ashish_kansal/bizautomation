Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Namespace BACRM.BusinessLogic.Notification
    Public Class Notification
        Inherits BACRM.BusinessLogic.CBusinessBase

        'Private UserCntID As Long = 0
        Private _CampaignID As Long = 0
        Private _DivisionID As Long = 0
        ''Private DomainId As Long = 0
        Private _interval As Long = 0
        Private _StrCommIds As String = ""

        Private _numScheduleid As Long = 0
        Public Property StrCommIds() As String
            Get
                Return _StrCommIds
            End Get
            Set(ByVal Value As String)
                _StrCommIds = Value
            End Set
        End Property
        Public Property interval() As Long
            Get
                Return _interval
            End Get
            Set(ByVal Value As Long)
                _interval = Value
            End Set
        End Property
        Public Property numScheduleid() As Long
            Get
                Return _numScheduleid
            End Get
            Set(ByVal Value As Long)
                _numScheduleid = Value
            End Set
        End Property

        Private _numConECampDTLID As Long
        Public Property numConECampDTLID() As Long
            Get
                Return _numConECampDTLID
            End Get
            Set(ByVal value As Long)
                _numConECampDTLID = value
            End Set
        End Property


        Private _DeliveryStatus As Short
        Public Property DeliveryStatus() As Short
            Get
                Return _DeliveryStatus
            End Get
            Set(ByVal value As Short)
                _DeliveryStatus = value
            End Set
        End Property


        Private _EmailLog As String
        Public Property EmailLog() As String
            Get
                Return _EmailLog
            End Get
            Set(ByVal value As String)
                _EmailLog = value
            End Set
        End Property

        Private _Mode As Short
        Public Property Mode() As Short
            Get
                Return _Mode
            End Get
            Set(ByVal value As Short)
                _Mode = value
            End Set
        End Property


        Private _OppId As Long
        Public Property OppId() As Long
            Get
                Return _OppId
            End Get
            Set(ByVal value As Long)
                _OppId = value
            End Set
        End Property

        Private _OppBizDocId As Long
        Public Property OppBizDocId() As Long
            Get
                Return _OppBizDocId
            End Get
            Set(ByVal value As Long)
                _OppBizDocId = value
            End Set
        End Property

        Private _StartDate As Date
        Public Property StartDate() As Date
            Get
                Return _StartDate
            End Get
            Set(ByVal value As Date)
                _StartDate = value
            End Set
        End Property

        Private _EndDate As Date
        Public Property EndDate() As Date
            Get
                Return _EndDate
            End Get
            Set(ByVal value As Date)
                _EndDate = value
            End Set
        End Property

        Private _OppRecurringID As Long
        Public Property OppRecurringID() As Long
            Get
                Return _OppRecurringID
            End Get
            Set(ByVal value As Long)
                _OppRecurringID = value
            End Set
        End Property

        Public Function getActItemsMail() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                arParms(0) = New Npgsql.NpgsqlParameter("@Mode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = 0

                arParms(1) = New Npgsql.NpgsqlParameter("@Time", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _interval

                arParms(2) = New Npgsql.NpgsqlParameter("@strComId", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(2).Value = ""

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_getActItemsMail", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Sub UpdateActItems()
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                arParms(0) = New Npgsql.NpgsqlParameter("@Mode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = 1

                arParms(1) = New Npgsql.NpgsqlParameter("@Time", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = 30

                arParms(2) = New Npgsql.NpgsqlParameter("@strComId", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(2).Value = StrCommIds

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                SqlDAL.ExecuteNonQuery(connString, "usp_getActItemsMail", arParms)

                'Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Sub
        Public Sub UpdateECampaignAlertStatus()
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                arParms(0) = New Npgsql.NpgsqlParameter("@numConECampDTLID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _numConECampDTLID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintDeliveryStatus", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _DeliveryStatus

                arParms(2) = New Npgsql.NpgsqlParameter("@vcEmailLog", NpgsqlTypes.NpgsqlDbType.VarChar, 3000)
                arParms(2).Value = _EmailLog

                arParms(3) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _Mode

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateECampaignAlertStatus", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function GetRecurringTransactions(ByVal RecurringType As Short) As DataSet
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@tintRecurringType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = RecurringType

                arParms(1) = New Npgsql.NpgsqlParameter("@dtStartDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _StartDate

                arParms(2) = New Npgsql.NpgsqlParameter("@dtEndDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _EndDate

                arParms(3) = New Npgsql.NpgsqlParameter("@numOppRecID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _OppRecurringID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetRecurringTransactions", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        <Obsolete()>
        Public Sub CreateRecurringOpportunityBizDocs()
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OppId

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppBizDocId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Direction = ParameterDirection.Output
                arParms(1).Value = _OppBizDocId

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_RecurringForBizDocs", objParam, True)
                _OppBizDocId = CLng(DirectCast(objParam, Npgsql.NpgsqlParameter())(1).Value)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Function getEmailAlerts() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(0).Value = Nothing
                arParms(0).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_SendApplicationMail", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function getECampaignMails() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(0).Value = Nothing
                arParms(0).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetEmailCampaignMails", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function getECampaignActionItems() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(0).Value = Nothing
                arParms(0).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetECampaignActionItems", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function getECampaignContactFollowUpList() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(0).Value = Nothing
                arParms(0).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetECampaignContactFollowUpList", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function getCustScheduledEmails() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numScheduleid", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _numScheduleid

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "Usp_GetScheduledEmails", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Sub UpdateScheduledOccurance()
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numScheduleid", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _numScheduleid

                ds = SqlDAL.ExecuteDataset(connString, "Usp_UpdateScheduledOccurance", arParms)


            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Sub
        Public Function getMileStonesAlerts() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(0).Value = Nothing
                arParms(0).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_updateProjOppStageEvent", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function getRecurringBizDocs() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(0).Value = Nothing
                arParms(0).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_scheduler", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
    End Class
End Namespace

