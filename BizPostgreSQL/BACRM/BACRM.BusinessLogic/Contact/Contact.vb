'**********************************************************************************
' <CContacts.vb>
' 
' 	CHANGE CONTROL:
'	
'	AUTHOR: Goyal 	DATE:28-Feb-05 	VERSION	CHANGES	KEYSTRING:
'**********************************************************************************


Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Case
Imports System.Web.UI.WebControls
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.Contacts

    '**********************************************************************************
    ' Module Name  : None
    ' Module Type  : CAccount
    ' 
    ' Description  : This is the business logic classe for Accounts Module
    '**********************************************************************************
    Public Class CContacts
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Constructor"
        '**********************************************************************************
        ' Name         : New
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal intUserId As Int32               
        ' Description  : The constructor                
        ' Notes        : N/A                
        ' Created By   : Goyal 	DATE:28-Feb-05
        '**********************************************************************************
        'Public Sub New(ByVal intUserId As Integer)
        '    'Constructor
        '    MyBase.New(intUserId)
        'End Sub

        ''**********************************************************************************
        '' Name         : New
        '' Type         : Sub
        '' Scope        : Public
        '' Returns      : N/A
        '' Parameters   : ByVal intUserId As Int32              
        '' Description  : The constructor                
        '' Notes        : N/A                
        '' Created By   : Goyal 	DATE:28-Feb-05
        ''**********************************************************************************
        'Public Sub New()
        '    'Constructor
        '    MyBase.New()
        'End Sub
#End Region

#Region "PRIVATE VARIABLE"

        'define private vairable
        Private _DomainID As Long = 0
        Private _ContactType As Long
        Private _ContactID As Long = 0
        Private _FirstName As String = String.Empty
        Private _LastName As String = String.Empty
        Private _Name As String = String.Empty
        Private _CompanyName As String = String.Empty
        Private _CompanyID As Long = 0
        Private _DivisionID As Long = 0
        Private _UserRights As Integer = 0
        Private _ContactNO As String = String.Empty
        Private _ShowAll As Integer = 0
        Private _CommunID As Integer = 0
        Private _CreatedBy As Long = 0
        Private _AssignedTo As String = String.Empty
        Private _UserRightType As Long = 0
        Private _SortOrder As Integer
        Private _SortOrder1 As Integer
        Private _SortChar As String = String.Empty
        Private _RelType As Long = 0
        Private _CustName As String = String.Empty
        Private _SortChar1 As String = String.Empty
        Private _TerritoryID As Long = 0
        Private _Email As String = String.Empty
        Private _SearchCriteria As String = String.Empty
        Private _FromDate As Date
        Private _ToDate As Date
        Private _strAOI As String
        Private _BillStreet As String
        Private _BillCity As String
        Private _BillState As Long
        Private _BillPostal As String
        Private _BillCountry As Long
        Private _ShipStreet As String
        Private _ShipCity As String
        Private _ShipState As Long
        Private _ShipPostal As String
        Private _ShipCountry As Long
        Private _BillingAddress As Short
        Private _Department As Long
        Private _GivenName As String
        Private _ContactPhone As String
        Private _HomePhone As String
        Private _Fax As String
        Private _CellPhone As String
        Private _Street As String
        Private _State As Long
        Private _Country As Long
        Private _PostalCode As String
        Private _AssFirstName As String
        Private _City As String
        Private _Website As String
        Private _AssLastName As String
        Private _Sex As String
        Private _AssPhone As String
        Private _CRMType As Short
        Private _CurrentPage As Integer
        Private _PageSize As Integer
        Private _TotalRecords As Integer
        Private _SortCharacter As Char
        Private _columnSortOrder As String
        Private _columnName As String
        Private _KeyWord As String
        Private _MessageFrom As String
        Private _EmailHstrID As Long
        Private _CreatedOn As Date
        Private _Position As Long
        Private _ContactPhoneExt As String
        Private _DOB As Date = "1/1/1753"
        Private _Comments As String
        Private _Category As Long
        Private _AssPhoneExt As String
        Private _AssEmail As String
        Private _OptOut As Boolean
        Private _Manager As Long
        Private _Team As Long
        Private _MessageTo As String
        Private _Subject As String
        Private _Cc As String
        Private _Bcc As String
        Private _Body As String
        Private _BodyText As String
        Private _Title As String
        Private _AltEmail As String
        Private _byteMode As Short
        Private _Type As String
        Private _RecID As Long
        Private _UserCntID As Long
        Private _bitPartner As Boolean
        Private _strContactIDs As String
        Private _strType As String
        Private _ItemId As String
        Private _ChangeKey As String
        Private _tinttype As Short
        Private _bitRead As Boolean
        Private _HasAttachment As Boolean
        Private _Size As Long
        Private _AttachmentType As String
        Private _FileName As String
        Private _AttachmentItemId As String
        Private _MessageToName As String
        Private _MessageFromName As String
        Private _CCName As String
        Private _MCategory As String
        Private _CaseID As Long
        Private _ClientTimeZoneOffset As Int32
        Private _FormId As Int32
        Private _strXml As String
        Private _bitDefault As Boolean
        Private _FilterID As Long
        Private _GroupID As Long = 0
        Private _bitInvisible As Boolean
        Private _vcImageName As String
        Private _vcLinkedinId As String
#End Region

#Region "PUBLIC PROPERTY"
        Private _BroadcastID As Long
        Public Property BroadcastID() As Long
            Get
                Return _BroadcastID
            End Get
            Set(ByVal value As Long)
                _BroadcastID = value
            End Set
        End Property
        Public Property LinkedinId() As String
            Get
                Return _vcLinkedinId
            End Get
            Set(ByVal value As String)
                _vcLinkedinId = value
            End Set
        End Property
        Private _vcLinkedinUrl As String
        Public Property LinkedinUrl() As String
            Get
                Return _vcLinkedinUrl
            End Get
            Set(ByVal value As String)
                _vcLinkedinUrl = value
            End Set
        End Property
        Public Property GroupID() As Long
            Get
                Return _GroupID
            End Get
            Set(ByVal Value As Long)
                _GroupID = Value
            End Set
        End Property

        Private _RecordID As Long
        Public Property RecordID() As Long
            Get
                Return _RecordID
            End Get
            Set(ByVal value As Long)
                _RecordID = value
            End Set
        End Property

        Public Property FilterID() As Long
            Get
                Return _FilterID
            End Get
            Set(ByVal Value As Long)
                _FilterID = Value
            End Set
        End Property

        Public Property bitDefault() As Boolean
            Get
                Return _bitDefault
            End Get
            Set(ByVal Value As Boolean)
                _bitDefault = Value
            End Set
        End Property

        Public Property strXml() As String
            Get
                Return _strXml
            End Get
            Set(ByVal Value As String)
                _strXml = Value
            End Set
        End Property

        Public Property FormId() As Int32
            Get
                Return _FormId
            End Get
            Set(ByVal Value As Int32)
                _FormId = Value
            End Set
        End Property

        Public Property ClientTimeZoneOffset() As Int32
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal Value As Int32)
                _ClientTimeZoneOffset = Value
            End Set
        End Property

        Public Property CaseID() As Long
            Get
                Return _CaseID
            End Get
            Set(ByVal Value As Long)
                _CaseID = Value
            End Set
        End Property

        Public Property MCategory() As String
            Get
                Return _MCategory
            End Get
            Set(ByVal Value As String)
                _MCategory = Value
            End Set
        End Property

        Public Property CCName() As String
            Get
                Return _CCName
            End Get
            Set(ByVal Value As String)
                _CCName = Value
            End Set
        End Property

        Public Property MessageFromName() As String
            Get
                Return _MessageFromName
            End Get
            Set(ByVal Value As String)
                _MessageFromName = Value
            End Set
        End Property

        Public Property MessageToName() As String
            Get
                Return _MessageToName
            End Get
            Set(ByVal Value As String)
                _MessageToName = Value
            End Set
        End Property

        Public Property AttachmentItemId() As String
            Get
                Return _AttachmentItemId
            End Get
            Set(ByVal Value As String)
                _AttachmentItemId = Value
            End Set
        End Property

        Public Property FileName() As String
            Get
                Return _FileName
            End Get
            Set(ByVal Value As String)
                _FileName = Value
            End Set
        End Property

        Public Property AttachmentType() As String
            Get
                Return _AttachmentType
            End Get
            Set(ByVal Value As String)
                _AttachmentType = Value
            End Set
        End Property

        Public Property Size() As Long
            Get
                Return _Size
            End Get
            Set(ByVal Value As Long)
                _Size = Value
            End Set
        End Property

        Public Property HasAttachment() As Boolean
            Get
                Return _HasAttachment
            End Get
            Set(ByVal Value As Boolean)
                _HasAttachment = Value
            End Set
        End Property

        Public Property tinttype() As Short
            Get
                Return _tinttype
            End Get
            Set(ByVal Value As Short)
                _tinttype = Value
            End Set
        End Property

        Public Property bitRead() As Boolean
            Get
                Return _bitRead
            End Get
            Set(ByVal Value As Boolean)
                _bitRead = Value
            End Set
        End Property

        Public Property ItemId() As String
            Get
                Return _ItemId
            End Get
            Set(ByVal Value As String)
                _ItemId = Value
            End Set
        End Property

        Public Property ChangeKey() As String
            Get
                Return _ChangeKey
            End Get
            Set(ByVal Value As String)
                _ChangeKey = Value
            End Set
        End Property

        Public Property strType() As String
            Get
                Return _strType
            End Get
            Set(ByVal Value As String)
                _strType = Value
            End Set
        End Property

        Public Property strContactIDs() As String
            Get
                Return _strContactIDs
            End Get
            Set(ByVal Value As String)
                _strContactIDs = Value
            End Set
        End Property

        Public Property bitPartner() As Boolean
            Get
                Return _bitPartner
            End Get
            Set(ByVal Value As Boolean)
                _bitPartner = Value
            End Set
        End Property

        Public Property UserCntID() As Long
            Get
                Return _UserCntID
            End Get
            Set(ByVal Value As Long)
                _UserCntID = Value
            End Set
        End Property

        Public Property RecID() As Long
            Get
                Return _RecID
            End Get
            Set(ByVal Value As Long)
                _RecID = Value
            End Set
        End Property

        Public Property Type() As String
            Get
                Return _Type
            End Get
            Set(ByVal Value As String)
                _Type = Value
            End Set
        End Property

        Public Property byteMode() As Short
            Get
                Return _byteMode
            End Get
            Set(ByVal Value As Short)
                _byteMode = Value
            End Set
        End Property

        Public Property AltEmail() As String
            Get
                Return _AltEmail
            End Get
            Set(ByVal Value As String)
                _AltEmail = Value
            End Set
        End Property

        Public Property Title() As String
            Get
                Return _Title
            End Get
            Set(ByVal Value As String)
                _Title = Value
            End Set
        End Property

        Public Property Body() As String
            Get
                Return _Body
            End Get
            Set(ByVal Value As String)
                _Body = Value
            End Set
        End Property

        Public Property BodyText() As String
            Get
                Return _BodyText
            End Get
            Set(ByVal Value As String)
                _BodyText = Value
            End Set
        End Property

        Public Property Bcc() As String
            Get
                Return _Bcc
            End Get
            Set(ByVal Value As String)
                _Bcc = Value
            End Set
        End Property

        Public Property Cc() As String
            Get
                Return _Cc
            End Get
            Set(ByVal Value As String)
                _Cc = Value
            End Set
        End Property

        Public Property Subject() As String
            Get
                Return _Subject
            End Get
            Set(ByVal Value As String)
                _Subject = Value
            End Set
        End Property

        Public Property MessageTo() As String
            Get
                Return _MessageTo
            End Get
            Set(ByVal Value As String)
                _MessageTo = Value
            End Set
        End Property

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Employment Status of the Contact.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Status id.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	01/23/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        ''' 

        Private _EmpStatus As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Employment Status of the Contact as a property.
        ''' </summary>
        ''' <remarks>
        '''     Helps manage the Emp Status Id.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	01/23/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        ''' 
        Public Property EmpStatus() As Long
            Get
                Return _EmpStatus
            End Get
            Set(ByVal Value As Long)
                _EmpStatus = Value
            End Set
        End Property

        Public Property Team() As Long
            Get
                Return _Team
            End Get
            Set(ByVal Value As Long)
                _Team = Value
            End Set
        End Property

        Public Property Manager() As Long
            Get
                Return _Manager
            End Get
            Set(ByVal Value As Long)
                _Manager = Value
            End Set
        End Property

        Public Property OptOut() As Boolean
            Get
                Return _OptOut
            End Get
            Set(ByVal Value As Boolean)
                _OptOut = Value
            End Set
        End Property

        Public Property AssEmail() As String
            Get
                Return _AssEmail
            End Get
            Set(ByVal Value As String)
                _AssEmail = Value
            End Set
        End Property

        Public Property AssPhoneExt() As String
            Get
                Return _AssPhoneExt
            End Get
            Set(ByVal Value As String)
                _AssPhoneExt = Value
            End Set
        End Property

        Public Property Category() As Long
            Get
                Return _Category
            End Get
            Set(ByVal Value As Long)
                _Category = Value
            End Set
        End Property

        Public Property Comments() As String
            Get
                Return _Comments
            End Get
            Set(ByVal Value As String)
                _Comments = Value
            End Set
        End Property

        Public Property DOB() As Date
            Get
                Return _DOB
            End Get
            Set(ByVal Value As Date)
                _DOB = Value
            End Set
        End Property

        Public Property ContactPhoneExt() As String
            Get
                Return _ContactPhoneExt
            End Get
            Set(ByVal Value As String)
                _ContactPhoneExt = Value
            End Set
        End Property

        Public Property Position() As Long
            Get
                Return _Position
            End Get
            Set(ByVal Value As Long)
                _Position = Value
            End Set
        End Property

        Public Property CreatedOn() As Date
            Get
                Return _CreatedOn
            End Get
            Set(ByVal Value As Date)
                _CreatedOn = Value
            End Set
        End Property

        Public Property EmailHstrID() As Long
            Get
                Return _EmailHstrID
            End Get
            Set(ByVal Value As Long)
                _EmailHstrID = Value
            End Set
        End Property

        Public Property MessageFrom() As String
            Get
                Return _MessageFrom
            End Get
            Set(ByVal Value As String)
                _MessageFrom = Value
            End Set
        End Property

        Public Property KeyWord() As String
            Get
                Return _KeyWord
            End Get
            Set(ByVal Value As String)
                _KeyWord = Value
            End Set
        End Property

        Public Property columnName() As String
            Get
                Return _columnName
            End Get
            Set(ByVal Value As String)
                _columnName = Value
            End Set
        End Property

        Public Property columnSortOrder() As String
            Get
                Return _columnSortOrder
            End Get
            Set(ByVal Value As String)
                _columnSortOrder = Value
            End Set
        End Property

        Public Property SortCharacter() As Char
            Get
                Return _SortCharacter
            End Get
            Set(ByVal Value As Char)
                _SortCharacter = Value
            End Set
        End Property

        Public Property TotalRecords() As Integer
            Get
                Return _TotalRecords
            End Get
            Set(ByVal Value As Integer)
                _TotalRecords = Value
            End Set
        End Property

        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal Value As Integer)
                _PageSize = Value
            End Set
        End Property

        Public Property CurrentPage() As Integer
            Get
                Return _CurrentPage
            End Get
            Set(ByVal Value As Integer)
                _CurrentPage = Value
            End Set
        End Property

        Public Property CRMType() As Short
            Get
                Return _CRMType
            End Get
            Set(ByVal Value As Short)
                _CRMType = Value
            End Set
        End Property

        Public Property AssPhone() As String
            Get
                Return _AssPhone
            End Get
            Set(ByVal Value As String)
                _AssPhone = Value
            End Set
        End Property

        Public Property Sex() As String
            Get
                Return _Sex
            End Get
            Set(ByVal Value As String)
                _Sex = Value
            End Set
        End Property

        Public Property AssLastName() As String
            Get
                Return _AssLastName
            End Get
            Set(ByVal Value As String)
                _AssLastName = Value
            End Set
        End Property

        Public Property Website() As String
            Get
                Return _Website
            End Get
            Set(ByVal Value As String)
                _Website = Value
            End Set
        End Property

        Public Property City() As String
            Get
                Return _City
            End Get
            Set(ByVal Value As String)
                _City = Value
            End Set
        End Property

        Public Property AssFirstName() As String
            Get
                Return _AssFirstName
            End Get
            Set(ByVal Value As String)
                _AssFirstName = Value
            End Set
        End Property

        Public Property PostalCode() As String
            Get
                Return _PostalCode
            End Get
            Set(ByVal Value As String)
                _PostalCode = Value
            End Set
        End Property

        Public Property Country() As Long
            Get
                Return _Country
            End Get
            Set(ByVal Value As Long)
                _Country = Value
            End Set
        End Property

        Public Property State() As Long
            Get
                Return _State
            End Get
            Set(ByVal Value As Long)
                _State = Value
            End Set
        End Property

        Public Property Street() As String
            Get
                Return _Street
            End Get
            Set(ByVal Value As String)
                _Street = Value
            End Set
        End Property

        Public Property CellPhone() As String
            Get
                Return _CellPhone
            End Get
            Set(ByVal Value As String)
                _CellPhone = Value
            End Set
        End Property

        Public Property Fax() As String
            Get
                Return _Fax
            End Get
            Set(ByVal Value As String)
                _Fax = Value
            End Set
        End Property

        Public Property HomePhone() As String
            Get
                Return _HomePhone
            End Get
            Set(ByVal Value As String)
                _HomePhone = Value
            End Set
        End Property

        Public Property ContactPhone() As String
            Get
                Return _ContactPhone
            End Get
            Set(ByVal Value As String)
                _ContactPhone = Value
            End Set
        End Property

        Public Property GivenName() As String
            Get
                Return _GivenName
            End Get
            Set(ByVal Value As String)
                _GivenName = Value
            End Set
        End Property

        Public Property Department() As Long
            Get
                Return _Department
            End Get
            Set(ByVal Value As Long)
                _Department = Value
            End Set
        End Property

        Public Property BillingAddress() As Short
            Get
                Return _BillingAddress
            End Get
            Set(ByVal Value As Short)
                _BillingAddress = Value
            End Set
        End Property

        Public Property ShipCountry() As Long
            Get
                Return _ShipCountry
            End Get
            Set(ByVal Value As Long)
                _ShipCountry = Value
            End Set
        End Property

        Public Property ShipPostal() As String
            Get
                Return _ShipPostal
            End Get
            Set(ByVal Value As String)
                _ShipPostal = Value
            End Set
        End Property

        Public Property ShipState() As Long
            Get
                Return _ShipState
            End Get
            Set(ByVal Value As Long)
                _ShipState = Value
            End Set
        End Property

        Public Property ShipCity() As String
            Get
                Return _ShipCity
            End Get
            Set(ByVal Value As String)
                _ShipCity = Value
            End Set
        End Property

        Public Property ShipStreet() As String
            Get
                Return _ShipStreet
            End Get
            Set(ByVal Value As String)
                _ShipStreet = Value
            End Set
        End Property

        Public Property BillCountry() As Long
            Get
                Return _BillCountry
            End Get
            Set(ByVal Value As Long)
                _BillCountry = Value
            End Set
        End Property

        Public Property BillPostal() As String
            Get
                Return _BillPostal
            End Get
            Set(ByVal Value As String)
                _BillPostal = Value
            End Set
        End Property

        Public Property BillState() As Long
            Get
                Return _BillState
            End Get
            Set(ByVal Value As Long)
                _BillState = Value
            End Set
        End Property

        Public Property BillCity() As String
            Get
                Return _BillCity
            End Get
            Set(ByVal Value As String)
                _BillCity = Value
            End Set
        End Property

        Public Property BillStreet() As String
            Get
                Return _BillStreet
            End Get
            Set(ByVal Value As String)
                _BillStreet = Value
            End Set
        End Property

        Public Property strAOI() As String
            Get
                Return _strAOI
            End Get
            Set(ByVal Value As String)
                _strAOI = Value
            End Set
        End Property

        Public Property ToDate() As Date
            Get
                Return _ToDate
            End Get
            Set(ByVal Value As Date)
                _ToDate = Value
            End Set
        End Property

        Public Property FromDate() As Date
            Get
                Return _FromDate
            End Get
            Set(ByVal Value As Date)
                _FromDate = Value
            End Set
        End Property

        Public Property SearchCriteria() As String
            Get
                Return _SearchCriteria
            End Get
            Set(ByVal Value As String)
                _SearchCriteria = Value
            End Set
        End Property

        Public Property Email() As String
            Get
                Return _Email
            End Get
            Set(ByVal Value As String)
                _Email = Value
            End Set
        End Property

        Public Property TerritoryID() As Long
            Get
                Return _TerritoryID
            End Get
            Set(ByVal Value As Long)
                _TerritoryID = Value
            End Set
        End Property

        Public Property CustName() As String
            Get
                Return _CustName
            End Get
            Set(ByVal Value As String)
                _CustName = Value
            End Set
        End Property

        Public Property UserRightType() As Long
            Get
                Return _UserRightType
            End Get
            Set(ByVal Value As Long)
                _UserRightType = Value
            End Set
        End Property

        Public Property SortOrder() As Integer
            Get
                Return _SortOrder
            End Get
            Set(ByVal Value As Integer)
                _SortOrder = Value
            End Set
        End Property

        Public Property SortOrder1() As Integer
            Get
                Return _SortOrder1
            End Get
            Set(ByVal Value As Integer)
                _SortOrder1 = Value
            End Set
        End Property

        Public Property SortChar() As String
            Get
                Return _SortChar
            End Get
            Set(ByVal Value As String)
                _SortChar = Value
            End Set
        End Property

        Public Property RelType() As Long
            Get
                Return _RelType
            End Get
            Set(ByVal Value As Long)
                _RelType = Value
            End Set
        End Property

        Public Property CreatedBy() As Long
            Get
                Return _CreatedBy
            End Get
            Set(ByVal Value As Long)
                _CreatedBy = Value
            End Set
        End Property

        Public Property DomainID() As Long
            Get
                Return _DomainID
            End Get
            Set(ByVal Value As Long)
                _DomainID = Value
            End Set
        End Property

        Public Property ContactType() As Long
            Get
                Return _ContactType
            End Get
            Set(ByVal Value As Long)
                _ContactType = Value
            End Set
        End Property

        Public Property ContactID() As Long
            Get
                Return _ContactID
            End Get
            Set(ByVal Value As Long)
                _ContactID = Value
            End Set
        End Property

        Public Property FirstName() As String
            Get
                Return _FirstName
            End Get
            Set(ByVal Value As String)
                _FirstName = Value
            End Set
        End Property

        Public Property LastName() As String
            Get
                Return _LastName
            End Get
            Set(ByVal Value As String)
                _LastName = Value
            End Set
        End Property

        Public Property Name() As String
            Get
                Return _Name
            End Get
            Set(ByVal Value As String)
                _Name = Value
            End Set
        End Property

        Public Property CompanyName() As String
            Get
                Return _CompanyName
            End Get
            Set(ByVal Value As String)
                _CompanyName = Value
            End Set
        End Property

        Public Property CompanyID() As Long
            Get
                Return _CompanyID
            End Get
            Set(ByVal Value As Long)
                _CompanyID = Value
            End Set
        End Property

        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property

        Public Property UserRights() As Integer
            Get
                Return _UserRights
            End Get
            Set(ByVal Value As Integer)
                _UserRights = Value
            End Set
        End Property

        Public Property ContactNo() As String
            Get
                Return _ContactNO
            End Get
            Set(ByVal Value As String)
                _ContactNO = Value
            End Set
        End Property

        Public Property CommunID() As Integer
            Get
                Return _CommunID
            End Get
            Set(ByVal Value As Integer)
                _CommunID = Value
            End Set
        End Property

        Public Property AssignedTo() As String
            Get
                Return _AssignedTo
            End Get
            Set(ByVal Value As String)
                _AssignedTo = Value
            End Set
        End Property

        Public Property SortChar1() As String
            Get
                Return _SortChar1
            End Get
            Set(ByVal Value As String)
                _SortChar1 = Value
            End Set
        End Property

        Private _DripCampaign As Long
        Public Property DripCampaign() As Long
            Get
                Return _DripCampaign
            End Get
            Set(ByVal value As Long)
                _DripCampaign = value
            End Set
        End Property

        Private _IsSelectiveUpdate As Boolean
        Public Property IsSelectiveUpdate() As Boolean
            Get
                Return _IsSelectiveUpdate
            End Get
            Set(ByVal value As Boolean)
                _IsSelectiveUpdate = value
            End Set
        End Property

        Private _bitOpenCommu As Boolean
        Public Property bitOpenCommu() As Boolean
            Get
                Return _bitOpenCommu
            End Get
            Set(ByVal Value As Boolean)
                _bitOpenCommu = Value
            End Set
        End Property

        Private _AddressID As Long
        Public Property AddressID() As Long
            Get
                Return _AddressID
            End Get
            Set(ByVal value As Long)
                _AddressID = value
            End Set
        End Property

        Public Enum enmAddressOf
            Contact = 1
            Organization = 2
            Opportunity = 3
        End Enum

        Private _AddressOf As enmAddressOf
        Public Property AddresOf() As enmAddressOf
            Get
                Return _AddressOf
            End Get
            Set(ByVal value As enmAddressOf)
                _AddressOf = value
            End Set
        End Property

        Public Enum enmAddressType
            None = 0
            BillTo = 1
            ShipTo = 2
        End Enum

        Private _AddressType As enmAddressType
        Public Property AddressType() As enmAddressType
            Get
                Return _AddressType
            End Get
            Set(ByVal value As enmAddressType)
                _AddressType = value
            End Set
        End Property

        Private _AddressName As String
        Public Property AddressName() As String
            Get
                Return _AddressName
            End Get
            Set(ByVal value As String)
                _AddressName = value
            End Set
        End Property

        Private _IsPrimaryAddress As Boolean
        Public Property IsPrimaryAddress() As Boolean
            Get
                Return _IsPrimaryAddress
            End Get
            Set(ByVal value As Boolean)
                _IsPrimaryAddress = value
            End Set
        End Property

        Private _PrimaryContact As Boolean
        Public Property PrimaryContact() As Boolean
            Get
                Return _PrimaryContact
            End Get
            Set(ByVal value As Boolean)
                _PrimaryContact = value
            End Set
        End Property

        Private _strFilter As String = ""
        Public Property strFilter As String
            Get
                Return _strFilter
            End Get
            Set(ByVal value As String)
                _strFilter = value
            End Set
        End Property

        Private _blnFlag As Boolean = False
        Public Property blnFlag As Boolean
            Get
                Return _blnFlag
            End Get
            Set(ByVal value As Boolean)
                _blnFlag = value
            End Set
        End Property

        Private _bitFlag As Boolean = False
        Public Property bitFlag As Boolean
            Get
                Return _bitFlag
            End Get
            Set(ByVal value As Boolean)
                _bitFlag = value
            End Set
        End Property

        Private _RegularSearchCriteria As String
        Public Property RegularSearchCriteria() As String
            Get
                Return _RegularSearchCriteria
            End Get
            Set(ByVal Value As String)
                _RegularSearchCriteria = Value
            End Set
        End Property

        Private _CustomSearchCriteria As String
        Public Property CustomSearchCriteria() As String
            Get
                Return _CustomSearchCriteria
            End Get
            Set(ByVal Value As String)
                _CustomSearchCriteria = Value
            End Set
        End Property


        Private _ViewID As Long
        Public Property ViewID() As Long
            Get
                Return _ViewID
            End Get
            Set(ByVal Value As Long)
                _ViewID = Value
            End Set
        End Property

        Private _ModuleID As Long
        Public Property ModuleID() As Long
            Get
                Return _ModuleID
            End Get
            Set(ByVal value As Long)
                _ModuleID = value
            End Set
        End Property

        Private _CampaignAudit As String
        Public Property vcCampaignAudit() As String
            Get
                Return _CampaignAudit
            End Get
            Set(ByVal Value As String)
                _CampaignAudit = Value
            End Set
        End Property

        Public Property bitInvisible As Boolean
            Get
                Return _bitInvisible
            End Get
            Set(ByVal value As Boolean)
                _bitInvisible = value
            End Set
        End Property

        Public Property vcImageName() As String
            Get
                Return _vcImageName
            End Get
            Set(ByVal Value As String)
                _vcImageName = Value
            End Set
        End Property

        Public Property IsResidential As Boolean
        Public Property IsFromECommerce As Boolean
        Public Property NonBizCompanyID As String
        Public Property NonBizContactID As String
        Public Property PStreet As String
        Public Property PCity As String
        Public Property PPostalCode As String
        Public Property PState As Long
        Public Property PCountry As Long
        Public Property TaxID As String
        Public Property IsAltContact As Boolean
        Public Property AltContact As String
        Public Property OppBizDocID As Long
#End Region

        Public Function GetCompanyName() As String
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                _CompanyName = CStr(SqlDAL.ExecuteScalar(connString, "usp_GetCompanyName", arParms))
                Return _CompanyName
            Catch ex As Exception
                Throw ex
            Finally

            End Try

        End Function

        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  RemoveRecrod()
        '     Purpose					 :  To delete data from database 
        '     Example                    :  
        '     Parameters                 :  @ItemCode                     
        '                                 
        '     Outputs					 :  It return true  if deleted otherwise return false
        '						
        '     Author Name				 :  Ajeet Singh
        '     Date Written				 :  1/11/2004
        '     Cross References 			 :  List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        'Public Function RemoveRecrod() As Boolean
        '    Dim getconnection As New GetConnection
        '    Dim connString As String = getconnection.GetConnectionString
        '    Dim Custint As Integer
        '    Try
        '        ' Set up parameters 
        '        Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

        '        ' @ItemCount Input Parameter 
        '        arParms(0) = New Npgsql.NpgsqlParameter("@CommunID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
        '        arParms(0).Value = _CommunID

        '        Custint = SqlDAL.ExecuteNonQuery(connString, "usp_DeleteCommunication", _CommunID)

        '        Return True
        '    Catch ex As Exception
        '        Return False
        '        Throw ex
        '    Finally

        '    End Try

        'End Function




        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  RemoveRecrod()
        '     Purpose					 :  To delete data from database 
        '     Example                    :  
        '     Parameters                 :  @ItemCode                     
        '                                 
        '     Outputs					 :  It return true  if deleted otherwise return false
        '						
        '     Author Name				 :  Ajeet Singh
        '     Date Written				 :  1/11/2004
        '     Cross References 			 :  List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Function DelContact() As String
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim Custint As Integer
            Dim lintCount As Integer
            Try
                ' Set up parameters 
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDivisionID", 0, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@numContactID", _ContactID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", _DomainID, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                lintCount = CInt(SqlDAL.ExecuteScalar(connString, "USP_GetAuthoritativeBizDocsCount", sqlParams.ToArray()))
                If lintCount > 0 Then Throw New Exception("AUTH_BIZDOC")

                Dim sqlParam As New List(Of Npgsql.NpgsqlParameter)
                sqlParam.Add(SqlDAL.Add_Parameter("@numContactID", _ContactID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                SqlDAL.ExecuteScalar(connString, "usp_DeleteContact", sqlParam.ToArray())
                Return ""
            Catch ex As Exception
                Select Case ex.Message
                    Case "USER"
                        Return "Selected Contact is associated to Active User, Can not be deleted."
                    Case "PRIMARY_CONTACT"
                        Return "Primary Contact Can not be Deleted ! To delete it you'll need to delete the entire organization."
                    Case "CHILD_OPP"
                        Return "Selected contact is associated with Opportunities/Orders, Can not be deleted."
                    Case "CHILD_CASE"
                        Return "Selected contact is associated with case(s), Can not be deleted."
                    Case "AUTH_BIZDOC"
                        Return "Selected contact is associated with Authoritative BizDocs, Can not be deleted."
                    Case Else
                        Throw ex
                End Select
            End Try
        End Function

        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  CheckCompanyName()
        '     Purpose					 :  This function will return company name.  
        '     Example                    :  
        '     Parameters                 :  @CompanyID                     
        '                                 
        '     Outputs					 :  It return  company name 
        '						
        '     Author Name				 :  Ajeet Singh
        '     Date Written				 :  1/11/2004
        '     Cross References 			 :  List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Function CheckCompanyName() As Boolean
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@CompanyName", _CompanyName, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@strSearchCriteria", _SearchCriteria, NpgsqlTypes.NpgsqlDbType.VarChar, 100))

                End With
                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "usp_CheckExistingCompany_new", sqlParams.ToArray())
                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                Return False
                Throw ex
            Finally

            End Try

        End Function
        Public Function GetEmailId(ByVal intCntId As Integer) As String
            Dim strMailId As String

            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = intCntId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                strMailId = CStr(SqlDAL.ExecuteScalar(connString, "usp_GetEmailIdForContact", arParms))

                Return strMailId
            Catch ex As Exception
                Throw New Exception("Error retrieving email Id", ex)
            End Try
        End Function
        Public Function GetCntInfoForEdit1() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With SqlParams

                    .ADD(SqlDAL.Add_Parameter("@numContactId", _ContactID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .ADD(SqlDAL.Add_Parameter("@numDomainID", _DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .ADD(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", _ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_EditCntInfo", sqlParams.ToArray())
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetEditContactAdd() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                sqlParams.Add(SqlDAL.Add_Parameter("@numContactId", _ContactID, NpgsqlTypes.NpgsqlDbType.Bigint))
                sqlParams.Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "usp_GetEditContactAddress", sqlParams.ToArray())
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetAOIForContact() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                sqlParams.Add(SqlDAL.Add_Parameter("@numAOIContactID", _ContactID, NpgsqlTypes.NpgsqlDbType.BigInt))

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "usp_GetAOIsForContact", sqlParams.ToArray())
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetCompnyDetailsByConID() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numcntid", _ContactID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDivisionID", _DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "usp_GetCompanydetails", sqlParams.ToArray())
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetAOIDetails() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numContactID", _ContactID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", _DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetAOI", sqlParams.ToArray())
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function SaveAOI() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numContactID", _ContactID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@strAOI", _strAOI, NpgsqlTypes.NpgsqlDbType.Text))
                
                End With
                
                SqlDAL.ExecuteNonQuery(connString, "USP_AOIManage", sqlParams.ToArray())
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetBillOrgorContAdd() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDivisionId", _DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numContactID", _ContactID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", _DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetBillAddCompany", sqlParams.ToArray())
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function UpdateCompanyAddress() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@vcBillStreet", _BillStreet, NpgsqlTypes.NpgsqlDbType.VarChar, 100))

                    .Add(SqlDAL.Add_Parameter("@vcBillCity", _BillCity, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcBilState", _BillState, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@vcBillPostCode", _BillPostal, NpgsqlTypes.NpgsqlDbType.VarChar, 15))

                    .Add(SqlDAL.Add_Parameter("@vcBillCountry", _BillCountry, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@bitSameAddr", _BillingAddress, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@vcShipStreet", _ShipStreet, NpgsqlTypes.NpgsqlDbType.VarChar, 100))

                    .Add(SqlDAL.Add_Parameter("@vcShipCity", _ShipCity, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcShipState", _ShipState, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@vcShipPostCode", _ShipPostal, NpgsqlTypes.NpgsqlDbType.VarChar, 15))

                    .Add(SqlDAL.Add_Parameter("@vcShipCountry", _ShipCountry, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDivisionID", _DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numContactId", _ContactID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@vcFirstname", _FirstName, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcLastName", _LastName, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcEmail", _Email, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcPhone", _ContactPhone, NpgsqlTypes.NpgsqlDbType.VarChar, 15))

                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateCompAdd", sqlParams.ToArray())
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function ImportContactFromOutlook() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@vcNewCompanyName", _CompanyName, NpgsqlTypes.NpgsqlDbType.VarChar, 100))

                    .Add(SqlDAL.Add_Parameter("@vcDepartment", _Department, NpgsqlTypes.NpgsqlDbType.VarChar, 100))

                    .Add(SqlDAL.Add_Parameter("@vcFirstName", _FirstName, NpgsqlTypes.NpgsqlDbType.VarChar, 50))                    

                    .Add(SqlDAL.Add_Parameter("@vcLastName", _LastName, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcGivenName", _GivenName, NpgsqlTypes.NpgsqlDbType.VarChar, 100))

                    .Add(SqlDAL.Add_Parameter("@numPhone", _ContactPhone, NpgsqlTypes.NpgsqlDbType.VarChar, 15))

                    .Add(SqlDAL.Add_Parameter("@numHomePhone", _HomePhone, NpgsqlTypes.NpgsqlDbType.VarChar, 15))

                    .Add(SqlDAL.Add_Parameter("@vcFax", _Fax, NpgsqlTypes.NpgsqlDbType.VarChar, 15))

                    .Add(SqlDAL.Add_Parameter("@numCell", _CellPhone, NpgsqlTypes.NpgsqlDbType.VarChar, 15))

                    .Add(SqlDAL.Add_Parameter("@vcEmail", _Email, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcStreet", _Street, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcCity", _City, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcState", _State, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@vcCountry", _Country, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@vcCmpWebSite", _Website, NpgsqlTypes.NpgsqlDbType.VarChar, 255))

                    .Add(SqlDAL.Add_Parameter("@strPOstCode", _PostalCode, NpgsqlTypes.NpgsqlDbType.VarChar, 10))

                    .Add(SqlDAL.Add_Parameter("@charSex", _Sex, NpgsqlTypes.NpgsqlDbType.Char, 1))

                    .Add(SqlDAL.Add_Parameter("@vcAsstFirstName", _AssFirstName, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcAsstLastName", _AssLastName, NpgsqlTypes.NpgsqlDbType.VarChar, 50))                    

                    .Add(SqlDAL.Add_Parameter("@numAsstPhone", _AssPhone, NpgsqlTypes.NpgsqlDbType.VarChar, 15))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", _DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numUserCntID", _UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numTerID", _TerritoryID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@CRMType", _CRMType, NpgsqlTypes.NpgsqlDbType.Smallint))

                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_ImportContactsFromOutLook", sqlParams.ToArray())
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function CheckEmailExistence() As String
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@vcEmail", _Email, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@numDomainId", _DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteScalar(connString, "USP_checkEmail", sqlParams.ToArray())
            Catch ex As Exception

                Throw ex
            End Try
        End Function

        Public Function GetContactList() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numUserCntID", _UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", _DomainID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@tintUserRightType", _UserRightType, NpgsqlTypes.NpgsqlDbType.Smallint))                    

                    .Add(SqlDAL.Add_Parameter("@tintSortOrder", _SortOrder, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@SortChar", _SortCharacter, NpgsqlTypes.NpgsqlDbType.Char, 1))

                    .Add(SqlDAL.Add_Parameter("@FirstName", IIf(_FirstName = "", "", Replace(_FirstName, "'", "''")), NpgsqlTypes.NpgsqlDbType.VarChar, 100))

                    .Add(SqlDAL.Add_Parameter("@LastName", IIf(_LastName = "", "", Replace(_LastName, "'", "''")), NpgsqlTypes.NpgsqlDbType.VarChar, 100))

                    .Add(SqlDAL.Add_Parameter("@CustName", IIf(_CustName = "", "", Replace(_CustName, "'", "''")), NpgsqlTypes.NpgsqlDbType.VarChar, 100))

                    .Add(SqlDAL.Add_Parameter("@CurrentPage", _CurrentPage, NpgsqlTypes.NpgsqlDbType.Integer))

                    .Add(SqlDAL.Add_Parameter("@PageSize", _PageSize, NpgsqlTypes.NpgsqlDbType.Integer))                    

                    .Add(SqlDAL.Add_Parameter("@columnName", _columnName, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@columnSortOrder", _columnSortOrder, NpgsqlTypes.NpgsqlDbType.VarChar, 10))

                    .Add(SqlDAL.Add_Parameter("@numDivisionID", _DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))                    

                    .Add(SqlDAL.Add_Parameter("@bitPartner", _bitPartner, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@intType", _ContactType, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", _ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))                    

                    .Add(SqlDAL.Add_Parameter("@vcRegularSearchCriteria", _RegularSearchCriteria, NpgsqlTypes.NpgsqlDbType.VarChar, 1000))

                    .Add(SqlDAL.Add_Parameter("@vcCustomSearchCriteria", _CustomSearchCriteria, NpgsqlTypes.NpgsqlDbType.Varchar, 1000))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim ds As DataSet
                Dim dtTable As DataTable

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetContactList1", sqlParams.ToArray())
                dtTable = ds.Tables(0)
                If dtTable.Rows.Count > 0 Then
                    _TotalRecords = CInt(dtTable.Rows(0).Item("TotalRowCount"))
                End If
                dtTable.Columns.Remove("TotalRowCount")
                ' End If

                ds.AcceptChanges()
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetContactListPortal() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numUserCntID", _UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", _DomainID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@tintUserRightType", _UserRightType, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@tintSortOrder", _SortOrder, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@SortChar", _SortCharacter, NpgsqlTypes.NpgsqlDbType.Char, 1))

                    .Add(SqlDAL.Add_Parameter("@FirstName", IIf(_FirstName = "", "", Replace(_FirstName, "'", "''")), NpgsqlTypes.NpgsqlDbType.VarChar, 100))

                    .Add(SqlDAL.Add_Parameter("@LastName", IIf(_LastName = "", "", Replace(_LastName, "'", "''")), NpgsqlTypes.NpgsqlDbType.VarChar, 100))

                    .Add(SqlDAL.Add_Parameter("@CustName", IIf(_CustName = "", "", Replace(_CustName, "'", "''")), NpgsqlTypes.NpgsqlDbType.VarChar, 100))

                    .Add(SqlDAL.Add_Parameter("@CurrentPage", _CurrentPage, NpgsqlTypes.NpgsqlDbType.Integer))

                    .Add(SqlDAL.Add_Parameter("@PageSize", _PageSize, NpgsqlTypes.NpgsqlDbType.Integer))

                    .Add(SqlDAL.Add_Parameter("@TotRecs", _TotalRecords, NpgsqlTypes.NpgsqlDbType.Integer, 0, ParameterDirection.InputOutput))

                    .Add(SqlDAL.Add_Parameter("@columnName", _columnName, NpgsqlTypes.NpgsqlDbType.VarChar, 50))                    

                    .Add(SqlDAL.Add_Parameter("@columnSortOrder", _columnSortOrder, NpgsqlTypes.NpgsqlDbType.VarChar, 10))

                    .Add(SqlDAL.Add_Parameter("@numDivisionID", _DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@bitPartner", _bitPartner, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@intType", _ContactType, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim ds As DataSet
                Dim dtTable As DataTable

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetContactList", sqlParams.ToArray())
                dtTable = ds.Tables(0)
                '_TotalRecords = CInt(arParms(10).Value)

                Return dtTable
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function GetContactEmailList() As DataTable
            Try


                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@keyWord", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(1).Value = _KeyWord

                arParms(2) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _CurrentPage

                arParms(3) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _PageSize

                arParms(4) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Direction = ParameterDirection.InputOutput
                arParms(4).Value = _TotalRecords

                arParms(5) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(5).Value = _columnName

                arParms(6) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(6).Value = _columnSortOrder

                arParms(7) = New Npgsql.NpgsqlParameter("@bitFlag ", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(7).Value = bitFlag

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetContactEmailAddress", arParms)
                TotalRecords = arParms(4).Value
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function EmployeeList() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                sqlParams.Add(SqlDAL.Add_Parameter("@numDomainID", _DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                sqlParams.Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))

                ds = SqlDAL.ExecuteDataset(connString, "USP_EmployeeList", sqlParams.ToArray())
                Return ds.Tables(0)
            Catch ex As Exception

                Throw ex
            End Try
        End Function

        Public Function EmployeeEmail(ByVal intUserID) As String
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                sqlParams.Add(SqlDAL.Add_Parameter("@numUserID", intUserID, NpgsqlTypes.NpgsqlDbType.Bigint))
                sqlParams.Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))

                Return CStr(SqlDAL.ExecuteScalar(connString, "USP_EmployeeEmailList", sqlParams.ToArray()))

            Catch ex As Exception

                Throw ex
            End Try
        End Function
        Public Function EmailDetails() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numEmailHstrID", _EmailHstrID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@bintCreatedOn", _CreatedOn, NpgsqlTypes.NpgsqlDbType.Timestamp))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With
                
                ds = SqlDAL.ExecuteDataset(connString, "USP_EmailDetails", sqlParams.ToArray())
                Return ds.Tables(0)
            Catch ex As Exception

                Throw ex
            End Try
        End Function

        Public Function EmailAttachmentDtls() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                sqlParams.Add(SqlDAL.Add_Parameter("@numEmailHstrID", _EmailHstrID, NpgsqlTypes.NpgsqlDbType.Bigint))
                sqlParams.Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetEmailAttachments", sqlParams.ToArray())
                Return ds.Tables(0)
            Catch ex As Exception

                Throw ex
            End Try
        End Function

        Public Function ManageContactInfo() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numContactId", _ContactID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@vcDepartment", _Department, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@vcFirstName", _FirstName, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcLastName", _LastName, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcEmail", _Email, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcSex", _Sex, NpgsqlTypes.NpgsqlDbType.Char, 1))

                    .Add(SqlDAL.Add_Parameter("@vcPosition", _Position, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@numPhone", _ContactPhone, NpgsqlTypes.NpgsqlDbType.VarChar, 15))

                    .Add(SqlDAL.Add_Parameter("@numPhoneExtension", _ContactPhoneExt, NpgsqlTypes.NpgsqlDbType.VarChar, 7))

                    .Add(SqlDAL.Add_Parameter("@bintDOB", IIf(_DOB.Year = 1753, DBNull.Value, _DOB), NpgsqlTypes.NpgsqlDbType.Timestamp))

                    .Add(SqlDAL.Add_Parameter("@txtNotes", _Comments, NpgsqlTypes.NpgsqlDbType.Text))

                    .Add(SqlDAL.Add_Parameter("@numUserCntID", _UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numCategory", _Category, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numCell", _CellPhone, NpgsqlTypes.NpgsqlDbType.VarChar, 15))

                    .Add(SqlDAL.Add_Parameter("@NumHomePhone", _HomePhone, NpgsqlTypes.NpgsqlDbType.VarChar, 15))

                    .Add(SqlDAL.Add_Parameter("@vcFax", _Fax, NpgsqlTypes.NpgsqlDbType.VarChar, 15))

                    .Add(SqlDAL.Add_Parameter("@vcAsstFirstName", _AssFirstName, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcAsstLastName", _AssLastName, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@numAsstPhone", _AssPhone, NpgsqlTypes.NpgsqlDbType.VarChar, 15))

                    .Add(SqlDAL.Add_Parameter("@numAsstExtn", _AssPhoneExt, NpgsqlTypes.NpgsqlDbType.VarChar, 6))

                    .Add(SqlDAL.Add_Parameter("@vcAsstEmail", _AssEmail, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@numContactType", _ContactType, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@bitOptOut", _OptOut, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@numManagerId", _Manager, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numTeam", _Team, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numEmpStatus", EmpStatus, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@vcAltEmail", _AltEmail, NpgsqlTypes.NpgsqlDbType.VarChar, 100))

                    .Add(SqlDAL.Add_Parameter("@vcTitle", _Title, NpgsqlTypes.NpgsqlDbType.VarChar, 100))                    

                    .Add(SqlDAL.Add_Parameter("@numECampaignID", _DripCampaign, NpgsqlTypes.NpgsqlDbType.BigInt))                    

                    .Add(SqlDAL.Add_Parameter("@bitSelectiveUpdate", _IsSelectiveUpdate, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@bitPrimaryContact", _PrimaryContact, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@vcTaxID", TaxID, NpgsqlTypes.NpgsqlDbType.VarChar))

                End With

                SqlDAL.ExecuteNonQuery(connString, "usp_ManageContInfo", sqlParams.ToArray())
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function InsertIntoEmailHstr() As Long
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@vcMessageTo", _MessageTo, NpgsqlTypes.NpgsqlDbType.VarChar, 1000))

                    .Add(SqlDAL.Add_Parameter("@vcMessageToName", _MessageToName, NpgsqlTypes.NpgsqlDbType.VarChar, 1000))

                    .Add(SqlDAL.Add_Parameter("@vcMessageFrom", _MessageFrom, NpgsqlTypes.NpgsqlDbType.VarChar, 1000))

                    .Add(SqlDAL.Add_Parameter("@vcMessageFromName", _MessageFromName, NpgsqlTypes.NpgsqlDbType.VarChar, 1000))

                    .Add(SqlDAL.Add_Parameter("@vcSubject", _Subject, NpgsqlTypes.NpgsqlDbType.VarChar, 500))

                    .Add(SqlDAL.Add_Parameter("@vcBody", _Body, NpgsqlTypes.NpgsqlDbType.Text))

                    .Add(SqlDAL.Add_Parameter("@vcCC", _Cc, NpgsqlTypes.NpgsqlDbType.VarChar, 2000))

                    .Add(SqlDAL.Add_Parameter("@vcCCName", _CCName, NpgsqlTypes.NpgsqlDbType.VarChar, 1000))

                    .Add(SqlDAL.Add_Parameter("@vcBCC", _Bcc, NpgsqlTypes.NpgsqlDbType.VarChar, 2000))

                    .Add(SqlDAL.Add_Parameter("@bitHasAttachment", _HasAttachment, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@vcItemId", _ItemId, NpgsqlTypes.NpgsqlDbType.VarChar, 1))

                    .Add(SqlDAL.Add_Parameter("@vcChangeKey", _ChangeKey, NpgsqlTypes.NpgsqlDbType.VarChar, 1))

                    .Add(SqlDAL.Add_Parameter("@bitIsRead", _bitRead, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@vcSize", _Size, NpgsqlTypes.NpgsqlDbType.Integer))

                    .Add(SqlDAL.Add_Parameter("@chrSource", _Type, NpgsqlTypes.NpgsqlDbType.VarChar, 10))

                    .Add(SqlDAL.Add_Parameter("@tinttype", _tinttype, NpgsqlTypes.NpgsqlDbType.Integer))

                    .Add(SqlDAL.Add_Parameter("@vcCategory", _MCategory, NpgsqlTypes.NpgsqlDbType.VarChar, 10))

                    .Add(SqlDAL.Add_Parameter("@vcAttachmentType", _AttachmentType, NpgsqlTypes.NpgsqlDbType.VarChar, 10))

                    .Add(SqlDAL.Add_Parameter("@vcFileName", _FileName, NpgsqlTypes.NpgsqlDbType.VarChar, 10))

                    .Add(SqlDAL.Add_Parameter("@vcAttachmentItemId", _AttachmentItemId, NpgsqlTypes.NpgsqlDbType.VarChar, 10))

                    .Add(SqlDAL.Add_Parameter("@dtReceived", _CreatedOn, NpgsqlTypes.NpgsqlDbType.VarChar, 40))

                    .Add(SqlDAL.Add_Parameter("@numDomainId", _DomainID, NpgsqlTypes.NpgsqlDbType.VarChar, 40))

                    .Add(SqlDAL.Add_Parameter("@vcBodyText", _BodyText, NpgsqlTypes.NpgsqlDbType.Text))

                    .Add(SqlDAL.Add_Parameter("@numUserCntID", _UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@IsReplied", _IsReplied, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@numRepliedMailID", _RepliedMailID, NpgsqlTypes.NpgsqlDbType.BigInt, 18))

                    .Add(SqlDAL.Add_Parameter("@bitInvisible", _bitInvisible, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@numOppBizDocID", OppBizDocID, NpgsqlTypes.NpgsqlDbType.Bigint))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return Common.CCommon.ToLong(SqlDAL.ExecuteScalar(connString, "USP_InsertIntoEmailHistory", sqlParams.ToArray()))
                'Return 0
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Private _IsReplied As Boolean
        Public Property IsReplied() As Boolean
            Get
                Return _IsReplied
            End Get
            Set(ByVal value As Boolean)
                _IsReplied = value
            End Set
        End Property

        Private _RepliedMailID As Long
        Public Property RepliedMailID() As Long
            Get
                Return _RepliedMailID
            End Get
            Set(ByVal value As Long)
                _RepliedMailID = value
            End Set
        End Property

        Public Function DeleteEmailHstr() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                sqlParams.Add(SqlDAL.Add_Parameter("@numEmailHstrID", _EmailHstrID, NpgsqlTypes.NpgsqlDbType.BigInt))

                SqlDAL.ExecuteNonQuery(connString, "USP_EmailHistoryDelete", sqlParams.ToArray())

                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function ManageFavorites() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@byteMode", _byteMode, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numContactID", _ContactID, NpgsqlTypes.NpgsqlDbType.Char, 2))

                    .Add(SqlDAL.Add_Parameter("@numUserCntID", _UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@cType", _Type, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageFavorites", sqlParams.ToArray())

                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function AddVisiteddetails() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numRecordID", _RecID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@chrRecordType", _Type, NpgsqlTypes.NpgsqlDbType.Char, 2))

                    .Add(SqlDAL.Add_Parameter("@numUserCntID", _UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_AddVisitedDetails", sqlParams.ToArray())

                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function getCorres() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@FromDate", _FromDate, NpgsqlTypes.NpgsqlDbType.Timestamp))

                    .Add(SqlDAL.Add_Parameter("@ToDate", _ToDate, NpgsqlTypes.NpgsqlDbType.Timestamp))

                    .Add(SqlDAL.Add_Parameter("@numContactID", _ContactID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@vcMessageFrom", _MessageFrom, NpgsqlTypes.NpgsqlDbType.VarChar, 1000))

                    .Add(SqlDAL.Add_Parameter("@tintSortOrder", _SortOrder, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@SeachKeyword", _KeyWord, NpgsqlTypes.NpgsqlDbType.VarChar, 100))

                    .Add(SqlDAL.Add_Parameter("@CurrentPage", _CurrentPage, NpgsqlTypes.NpgsqlDbType.Integer))

                    .Add(SqlDAL.Add_Parameter("@PageSize", _PageSize, NpgsqlTypes.NpgsqlDbType.Integer))

                    .Add(SqlDAL.Add_Parameter("@TotRecs", _TotalRecords, NpgsqlTypes.NpgsqlDbType.Integer, 0, ParameterDirection.InputOutput))

                    .Add(SqlDAL.Add_Parameter("@columnName", _columnName, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@columnSortOrder", _columnSortOrder, NpgsqlTypes.NpgsqlDbType.VarChar, 10))

                    .Add(SqlDAL.Add_Parameter("@filter", _SortOrder1, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDivisionID", _DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", _DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numCaseID", _CaseID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", _ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))

                    .Add(SqlDAL.Add_Parameter("@numOpenRecordID", _RecordID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@tintMode", _byteMode, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@bitOpenCommu", _bitOpenCommu, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@numUserCntID", _UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@vcTypes", Type, NpgsqlTypes.NpgsqlDbType.Varchar))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "Usp_getCorrespondance", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(8).Value)

                If ds.Tables.Count > 0 Then
                    Return ds.Tables(0)
                Else
                    Return Nothing
                End If
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Function DelCorrespondence() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numID", _EmailHstrID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@tintType", _tinttype, NpgsqlTypes.NpgsqlDbType.Smallint))

                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_DelCorrespondence", sqlParams.ToArray())

                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Function InsertItemID() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@ItemId", _ItemId, NpgsqlTypes.NpgsqlDbType.VarChar, 250))

                    .Add(SqlDAL.Add_Parameter("@ChangeKey", _ChangeKey, NpgsqlTypes.NpgsqlDbType.VarChar, 250))

                    .Add(SqlDAL.Add_Parameter("@ContactId", _ContactID, NpgsqlTypes.NpgsqlDbType.Integer))

                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_AddContactItemId", sqlParams.ToArray())

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GetColumnConfiguration() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainId", _DomainID, NpgsqlTypes.NpgsqlDbType.VarChar, 250))

                    .Add(SqlDAL.Add_Parameter("@numUserCntId", _UserCntID, NpgsqlTypes.NpgsqlDbType.VarChar, 250))

                    .Add(SqlDAL.Add_Parameter("@FormId", _FormId, NpgsqlTypes.NpgsqlDbType.Integer))

                    .Add(SqlDAL.Add_Parameter("@numType", _ContactType, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numViewID", _ViewID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetColumnConfiguration1", sqlParams.ToArray())
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Sub SaveContactColumnConfiguration()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainId", _DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numUserCntId", _UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@FormId", _FormId, NpgsqlTypes.NpgsqlDbType.Integer))

                    .Add(SqlDAL.Add_Parameter("@numType", _ContactType, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@strXml", _strXml, NpgsqlTypes.NpgsqlDbType.Text))

                    .Add(SqlDAL.Add_Parameter("@numViewID", _ViewID, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With
                SqlDAL.ExecuteNonQuery(connString, "USP_SaveContactColumnConfiguration1", sqlParams.ToArray())
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub SaveDefaultFilter()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainId", _DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numUserCntId", _UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numRelation", _ContactType, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@FilterId", _FilterID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@bitDefault", _bitDefault, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@FormId", _FormId, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@GroupId", _GroupID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@vcFilter", _strFilter, NpgsqlTypes.NpgsqlDbType.VarChar, 4000))

                    .Add(SqlDAL.Add_Parameter("@bitFlag", _blnFlag, NpgsqlTypes.NpgsqlDbType.Bit))

                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_SaveDefaultFilter", sqlParams.ToArray())
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function GetDefaultfilter() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainId", _DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numUserCntId", _UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numRelation", _ContactType, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@FormId", _FormId, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@GroupId", _GroupID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetDefaultfilter", sqlParams.ToArray())
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetContactType(ByVal ContactID As Long) As Long
            Try
                Dim dttable As DataTable

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                sqlParams.Add(SqlDAL.Add_Parameter("@numUserCntId", ContactID, NpgsqlTypes.NpgsqlDbType.Bigint))
                sqlParams.Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))

                Return BACRM.BusinessLogic.Common.CCommon.ToLong(SqlDAL.ExecuteScalar(connString, "USP_GetContactType", sqlParams.ToArray()))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Sub FillOpenRecords(ByVal lngDivisionID As Long, ByVal lngDomainID As Long, ByRef ddl As DropDownList, ByVal Type As Short)
            Dim objOpp As New MOpportunity
            objOpp.DomainID = lngDomainID
            objOpp.DivisionID = lngDivisionID
            Select Case Type
                Case 1
                    objOpp.Mode = 1 'select only sales opportunity
                    ddl.DataSource = objOpp.GetOpenRecords()
                    ddl.DataTextField = "vcPOppName"
                    ddl.DataValueField = "numOppId"
                    ddl.DataBind()
                    ddl.Items.Insert(0, "--Select One--")
                    ddl.Items.FindByText("--Select One--").Value = "0"
                Case 2
                    objOpp.Mode = 2
                    ddl.DataSource = objOpp.GetOpenRecords()
                    ddl.DataTextField = "vcPOppName"
                    ddl.DataValueField = "numOppId"
                    ddl.DataBind()
                    ddl.Items.Insert(0, "--Select One--")
                    ddl.Items.FindByText("--Select One--").Value = "0"
                Case 3
                    objOpp.Mode = 3
                    ddl.DataSource = objOpp.GetOpenRecords()
                    ddl.DataTextField = "vcPOppName"
                    ddl.DataValueField = "numOppId"
                    ddl.DataBind()
                    ddl.Items.Insert(0, "--Select One--")
                    ddl.Items.FindByText("--Select One--").Value = "0"
                Case 4
                    objOpp.Mode = 4
                    ddl.DataSource = objOpp.GetOpenRecords()
                    ddl.DataTextField = "vcPOppName"
                    ddl.DataValueField = "numOppId"
                    ddl.DataBind()
                    ddl.Items.Insert(0, "--Select One--")
                    ddl.Items.FindByText("--Select One--").Value = "0"
                Case 5
                    Dim objProject As New Project
                    objProject.DomainID = lngDomainID
                    objProject.DivisionID = lngDivisionID
                    ddl.DataTextField = "vcProjectName"
                    ddl.DataValueField = "numProId"
                    ddl.DataSource = objProject.GetOpenProject()
                    ddl.DataBind()
                    ddl.Items.Insert(0, "--Select One--")
                    ddl.Items.FindByText("--Select One--").Value = "0"
                Case 6
                    Dim ObjCases As New CCases
                    Dim dtCaseNo As DataTable
                    ObjCases.DivisionID = lngDivisionID
                    dtCaseNo = ObjCases.GetOpenCases
                    ddl.DataTextField = "vcCaseNumber"
                    ddl.DataValueField = "numCaseId1"
                    ddl.DataSource = dtCaseNo
                    ddl.DataBind()
                    ddl.Items.Insert(0, "--Select One--")
                    ddl.Items.FindByText("--Select One--").Value = 0
            End Select
        End Sub

        Public Sub UpdateOptOut()
            Try
                Dim dttable As DataTable

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numContactID", _ContactID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@vcEmailID", _Email, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@bitOptOut", _OptOut, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@numBroadCastID", _BroadcastID, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With

                SqlDAL.ExecuteScalar(connString, "USP_UpdateOptOut", sqlParams.ToArray())
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        'Private Function FindID(ByVal bk As Npgsql.NpgsqlParameter) As Boolean
        '    If bk.ID = IDToFind Then
        '        Return True
        '    Else
        '        Return False
        '    End If
        'End Function

        Public Function ManageAddress() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                'Private IDToFind As String = "@numAddressID"

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numAddressID", _AddressID, NpgsqlTypes.NpgsqlDbType.BigInt, 0, ParameterDirection.InputOutput))

                    .Add(SqlDAL.Add_Parameter("@vcAddressName", _AddressName, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcStreet", _Street, NpgsqlTypes.NpgsqlDbType.VarChar, 100))

                    .Add(SqlDAL.Add_Parameter("@vcCity", _City, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcPostalCode", _PostalCode, NpgsqlTypes.NpgsqlDbType.VarChar, 15))

                    .Add(SqlDAL.Add_Parameter("@numState", _State, NpgsqlTypes.NpgsqlDbType.VarChar, 15))

                    .Add(SqlDAL.Add_Parameter("@numCountry", _Country, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@bitIsPrimary", _IsPrimaryAddress, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@tintAddressOf", Convert.ToInt16(_AddressOf), NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@tintAddressType", Convert.ToInt16(_AddressType), NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@numRecordID", _RecordID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", _DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@bitResidential", IsResidential, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@vcPhone", ContactPhone, NpgsqlTypes.NpgsqlDbType.VarChar))

                    .Add(SqlDAL.Add_Parameter("@bitFromEcommerce", IsFromECommerce, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@numContact", ContactID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@bitAltContact", IsAltContact, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@vcAltContact", AltContact, NpgsqlTypes.NpgsqlDbType.VarChar))

                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageAddressDetails", sqlParams.ToArray())

                _AddressID = Convert.ToInt64(DirectCast(sqlParams.ToArray(), Npgsql.NpgsqlParameter())(0).Value)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GetAddressDetail() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@tintMode", _byteMode, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@numDomainId", _DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))

                    .Add(SqlDAL.Add_Parameter("@numRecordID", _RecordID, NpgsqlTypes.NpgsqlDbType.Bigint))

                    .Add(SqlDAL.Add_Parameter("@numAddressID", _AddressID, NpgsqlTypes.NpgsqlDbType.Bigint))

                    .Add(SqlDAL.Add_Parameter("@tintAddressType", Convert.ToInt16(_AddressType), NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@tintAddressOf", Convert.ToInt16(_AddressOf), NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetAddressDetails", sqlParams.ToArray())

                If ds.Tables.Count > 0 Then
                    Return ds.Tables(0)
                Else
                    Dim dtTable As New DataTable
                    Return dtTable
                End If



            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'Author:Sachin Sadhu||Date:17-Jan-2014
        'Purpose: To add facility to save Billing/Shipping address on New Record Froms
        Function GetLastDivisonID() As Integer
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim intDivisonId As Integer

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(0).Value = Nothing
                arParms(0).Direction = ParameterDirection.InputOutput

                intDivisonId = CInt(SqlDAL.ExecuteScalar(connString, "Usp_DivisionMaster_LastRecord", arParms))
                Return intDivisonId


            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'End of code by Sachin

        Public Function DeleteAddress() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numAddressID", _AddressID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", _DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With

                Return SqlDAL.ExecuteNonQuery(connString, "USP_DeleteAddressDetail", sqlParams.ToArray())
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageGoogleContact() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainId", _DomainID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@vcEmail", _Email, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcPhone", _ContactPhone, NpgsqlTypes.NpgsqlDbType.VarChar, 15))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                ds = SqlDAL.ExecuteDataset(connString, "USP_ManageGoogleContact", sqlParams.ToArray())
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetDefaultfilter111() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@TestParam1", 1, NpgsqlTypes.NpgsqlDbType.BigInt, 0))

                    .Add(SqlDAL.Add_Parameter("@TestParam2", 2, NpgsqlTypes.NpgsqlDbType.BigInt, 0, ParameterDirection.InputOutput))

                    .Add(SqlDAL.Add_Parameter("@TestParam3", "Manish", NpgsqlTypes.NpgsqlDbType.VarChar, 1000, ParameterDirection.InputOutput))

                    .Add(SqlDAL.Add_Parameter("@TestParam4", 3, NpgsqlTypes.NpgsqlDbType.BigInt, 0, ParameterDirection.InputOutput))

                End With

                ds = SqlDAL.ExecuteDataset(connString, "TEST_PROCEDURE", sqlParams.ToArray())
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Sub SaveShareRecord()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainId", _DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numRecordID", _RecordID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numModuleID", _ModuleID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@tintMode", _tinttype, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@strXml", _strXml, NpgsqlTypes.NpgsqlDbType.Text))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageShareRecord", sqlParams.ToArray())
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function GetShareRecordList() As DataSet
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainId", _DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numRecordID", _RecordID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numModuleID", _ModuleID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@tintMode", _tinttype, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                ds = SqlDAL.ExecuteDataset(connString, "USP_ManageShareRecord", sqlParams.ToArray())
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Sub UpdateContactImage()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainId", _DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numContactID", _ContactID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcImageName", _vcImageName, NpgsqlTypes.NpgsqlDbType.VarChar))
                End With
                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateContactImage", sqlParams.ToArray())
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub GetContactDetailForImport()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = ContactID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_Import_GetContactDetails", arParms)

                If ds Is Nothing Or ds.Tables.Count = 0 Or ds.Tables(0).Rows.Count = 0 Then
                    Me.ContactID = 0
                Else
                    For Each dr As DataRow In ds.Tables(0).Rows
                        If Not IsDBNull(dr("numDivisionId")) Then
                            Me.DivisionID = CType(dr("numDivisionId"), Long)
                        End If

                        If Not IsDBNull(dr("numContactID")) Then
                            Me.ContactID = CType(dr("numContactID"), Long)
                        End If

                        If Not IsDBNull(dr("vcFirstName")) Then
                            Me.FirstName = CType(dr("vcFirstName"), String)
                        End If

                        If Not IsDBNull(dr("vcLastName")) Then
                            Me.LastName = CType(dr("vcLastName"), String)
                        End If

                        If Not IsDBNull(dr("vcEmail")) Then
                            Me.Email = CType(dr("vcEmail"), String)
                        End If

                        If Not IsDBNull(dr("numContactType")) Then
                            Me.ContactType = CType(dr("numContactType"), Long)
                        End If

                        If Not IsDBNull(dr("numPhone")) Then
                            Me.ContactPhone = CType(dr("numPhone"), String)
                        End If

                        If Not IsDBNull(dr("numPhoneExtension")) Then
                            Me.ContactPhoneExt = CType(dr("numPhoneExtension"), String)
                        End If

                        If Not IsDBNull(dr("numCell")) Then
                            Me.CellPhone = CType(dr("numCell"), String)
                        End If

                        If Not IsDBNull(dr("numHomePhone")) Then
                            Me.HomePhone = CType(dr("numHomePhone"), String)
                        End If

                        If Not IsDBNull(dr("vcFax")) Then
                            Me.Fax = CType(dr("vcFax"), String)
                        End If

                        If Not IsDBNull(dr("vcCategory")) Then
                            Me.Category = CType(dr("vcCategory"), Long)
                        End If

                        If Not IsDBNull(dr("numTeam")) Then
                            Me.Team = CType(dr("numTeam"), Long)
                        End If

                        If Not IsDBNull(dr("vcPosition")) Then
                            Me.Position = CType(dr("vcPosition"), Long)
                        End If

                        If Not IsDBNull(dr("vcTitle")) Then
                            Me.Title = CType(dr("vcTitle"), String)
                        End If

                        If Not IsDBNull(dr("vcDepartment")) Then
                            Me.Department = CType(dr("vcDepartment"), Long)
                        End If

                        If Not IsDBNull(dr("txtNotes")) Then
                            Me.Comments = CType(dr("txtNotes"), String)
                        End If

                        If Not IsDBNull(dr("vcNonBizContactID")) Then
                            Me.NonBizContactID = CType(dr("vcNonBizContactID"), String)
                        End If

                        If Not IsDBNull(dr("bitPrimaryContact")) Then
                            Me.PrimaryContact = CType(dr("bitPrimaryContact"), Boolean)
                        End If
                    Next
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub SaveContactForImport()
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDivisionID", DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numContactID", ContactID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcFirstName", FirstName, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcLastName", LastName, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcEmail", Email, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numContactType", ContactType, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numPhone", ContactPhone, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numPhoneExtension", ContactPhoneExt, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numCell", CellPhone, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numHomePhone", HomePhone, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcFax", Fax, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcCategory", Category, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numTeam", Team, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcPosition", Position, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcTitle", Title, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcDepartment", Department, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@txtNotes", Comments, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@bitIsPrimaryContact", PrimaryContact, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@vcStreet", PStreet, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcCity", PCity, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numState", PState, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numCountry", PCountry, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcPostalCode", PPostalCode, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Me.ContactID = SqlDAL.ExecuteScalar(connString, "USP_Import_SaveContactDetails", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function ManageImportActionItemReference(Optional ByVal strNonBizContactID As Long = 0) As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numCompanyID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Direction = ParameterDirection.InputOutput
                arParms(1).Value = CompanyID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Direction = ParameterDirection.InputOutput
                arParms(2).Value = DivisionID

                arParms(3) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Direction = ParameterDirection.InputOutput
                arParms(3).Value = ContactID

                arParms(4) = New Npgsql.NpgsqlParameter("@numNonBizContactID", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(4).Value = NonBizContactID

                arParms(5) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _byteMode

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageImportActionItemReference", objParam, True)
                _CompanyID = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(1).Value)
                _DivisionID = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(2).Value)
                _ContactID = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(3).Value)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function RollbackImportedContact()
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numContactID", ContactID, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_AdditionalContactsInformation_RollbackImport", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function IsDuplicateEmail() As Boolean
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numContactID", ContactID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcEmail", Email, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return Convert.ToBoolean(SqlDAL.ExecuteScalar(connString, "USP_AdditionalContactsInformation_CheckIfDuplicateEmail", sqlParams.ToArray()))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetContactByEmail() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numContactID", ContactID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcEmail", Email, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_AdditionalContactsInformation_GetByEmail", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub ChangeEmailAddress()
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numContactID", ContactID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcEmail", Email, NpgsqlTypes.NpgsqlDbType.VarChar))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_AdditionalContactsInformation_ChangeEmail", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub
    End Class
End Namespace

