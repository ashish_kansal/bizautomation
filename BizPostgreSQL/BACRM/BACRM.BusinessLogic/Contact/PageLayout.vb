
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Namespace BACRM.BusinessLogic.Contacts
    Public Class CPageLayout
        Inherits BACRM.BusinessLogic.CBusinessBase


        ''Private DomainId As Long = 0
        Private _ContactType As Long
        Private _ContactID As Integer = 1
        Private _FirstName As String = String.Empty
        Private _CoType As Char
        Private _DivisionID As Integer = 0
        Private _ListId As Integer = 0
        Private _ListVAlue As Integer = 0
        Private _XmlData As String
        Private _OpportunityId As Long
        Private _ProjectId As Long
        Private _CaseID As Long = 0
        Private _ContractId As Long = 0
        Private _numRelation As Int64
        Private _numRelCntType As Int64 = 0
        Private _PageId As Int64
        Private _locId As Long
        Private _RelId As Long
        Private _RecordId As Long
        Private _SalesProcessId As Long
        Private _ClientTimeZoneOffset As Integer
        Public Property ClientTimeZoneOffset() As Integer
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal Value As Integer)
                _ClientTimeZoneOffset = Value
            End Set
        End Property
        Public Property SalesProcessId() As Long
            Get
                Return _SalesProcessId
            End Get
            Set(ByVal Value As Long)
                _SalesProcessId = Value
            End Set
        End Property
        Public Property RecordId() As Long
            Get
                Return _RecordId
            End Get
            Set(ByVal Value As Long)
                _RecordId = Value
            End Set
        End Property
        Public Property locId() As Long
            Get
                Return _locId
            End Get
            Set(ByVal Value As Long)
                _locId = Value
            End Set
        End Property
        Public Property RelId() As Long
            Get
                Return _RelId
            End Get
            Set(ByVal Value As Long)
                _RelId = Value
            End Set
        End Property
        Public Property numRelCntType() As Int64
            Get
                Return _numRelCntType
            End Get
            Set(ByVal Value As Int64)
                _numRelCntType = Value
            End Set
        End Property

        Public Property numRelation() As Int64
            Get
                Return _numRelation
            End Get
            Set(ByVal Value As Int64)
                _numRelation = Value
            End Set
        End Property
        Public Property CaseID() As Long
            Get
                Return _CaseID
            End Get
            Set(ByVal Value As Long)
                _CaseID = Value
            End Set
        End Property
        Public Property ContractID() As Long
            Get
                Return _ContractID
            End Get
            Set(ByVal Value As Long)
                _ContractID = Value
            End Set
        End Property
        Public Property ProjectId() As Long
            Get
                Return _ProjectId
            End Get
            Set(ByVal Value As Long)
                _ProjectId = Value
            End Set
        End Property
        Public Property OpportunityId() As Long
            Get
                Return _OpportunityId
            End Get
            Set(ByVal Value As Long)
                _OpportunityId = Value
            End Set
        End Property

        Public Property XmlData() As String
            Get
                Return _XmlData
            End Get
            Set(ByVal Value As String)
                _XmlData = Value
            End Set
        End Property

        Public Property DivisionID() As Integer
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Integer)
                _DivisionID = Value
            End Set
        End Property
        Public Property CoType() As Char
            Get
                Return _CoType
            End Get
            Set(ByVal Value As Char)
                _CoType = Value
            End Set
        End Property

        'Public Property UserCntID() As Integer
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal Value As Integer)
        '        UserCntID = Value
        '    End Set
        'End Property
        Public Property ContactID() As Integer
            Get
                Return _ContactID
            End Get
            Set(ByVal Value As Integer)
                _ContactID = Value
            End Set
        End Property

        Public Property ListId() As Integer
            Get
                Return _ListId
            End Get
            Set(ByVal Value As Integer)
                _ListId = Value
            End Set
        End Property
        Public Property ListValue() As Integer
            Get
                Return _ListValue
            End Get
            Set(ByVal Value As Integer)
                _ListValue = Value
            End Set
        End Property
        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property
        Public Property PageId() As Int64
            Get
                Return _PageId
            End Get
            Set(ByVal Value As Int64)
                _PageId = Value
            End Set
        End Property

        Private _FormId As Int64
        Public Property FormId() As Int64
            Get
                Return _FormId
            End Get
            Set(ByVal Value As Int64)
                _FormId = Value
            End Set
        End Property

        Private _PageType As Integer
        Public Property PageType() As Integer
            Get
                Return _PageType
            End Get
            Set(ByVal Value As Integer)
                _PageType = Value
            End Set
        End Property
        Public Function GetTableInfoDefault() As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numRecordID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _RecordId

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainId

                arParms(3) = New Npgsql.NpgsqlParameter("@charCoType", NpgsqlTypes.NpgsqlDbType.Char)
                arParms(3).Value = _CoType

                arParms(4) = New Npgsql.NpgsqlParameter("@pageId", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _PageId

                arParms(5) = New Npgsql.NpgsqlParameter("@numRelCntType", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _numRelCntType

                arParms(6) = New Npgsql.NpgsqlParameter("@numFormID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _FormId

                arParms(7) = New Npgsql.NpgsqlParameter("@tintPageType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(7).Value = _PageType

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "usp_GetTableInfoDefault", arParms)
                Return ds

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function GetTableInfoDefaultPortal() As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@@numRecordID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _RecordId

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainId

                arParms(3) = New Npgsql.NpgsqlParameter("@charCoType", NpgsqlTypes.NpgsqlDbType.Char)
                arParms(3).Value = _CoType

                arParms(4) = New Npgsql.NpgsqlParameter("@pageId", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _PageId

                arParms(5) = New Npgsql.NpgsqlParameter("@numRelCntType", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _numRelCntType

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "usp_GetTableInfoDefaultPortal", arParms)
                Return ds

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetdropListItems() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@ListID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ListId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@ListValue", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ListVAlue

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetddlListItems", arParms).Tables(0)


            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetManagers(ByVal lngDomainID As Long, ByVal lngConID As Long, ByVal lngDivID As Long) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = lngDivID

                arParms(1) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = lngConID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = lngDomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "GetManagersddl", arParms).Tables(0)

            Catch ex As Exception

                Throw ex
            End Try
        End Function

        Public Function GetListItems() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@ListData", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(0).Value = _XmlData

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetListItems", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function



        Public Function GetLeadDtls() As DataTable
            Try

                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ContactID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _ClientTimeZoneOffset

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "usp_GetLeadDetailsDTLPL", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function







        Public Function GetContractDetails() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numContractId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _ContractID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId


                ds = SqlDAL.ExecuteDataset(connString, "USP_ContractDetailsdtlPL", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        <Obsolete("Procedure contains old code needs updation")> _
        Public Function GetCustFlds() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@PageId", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = _locId

                arParms(1) = New Npgsql.NpgsqlParameter("@numRelation", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _RelId

                arParms(2) = New Npgsql.NpgsqlParameter("@numRecordId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _RecordId

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = DomainId

                ds = SqlDAL.ExecuteDataset(connString, "USP_cfwGetFieldsDTLPL", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        <Obsolete("replaced by new usercontrol")>
        Public Function SalesProcessDtlByOppId() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@ProcessID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _SalesProcessId

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@DomianID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainId

                arParms(3) = New Npgsql.NpgsqlParameter("@OpportunityID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _OpportunityId

                ds = SqlDAL.ExecuteDataset(connString, "USP_OPPMilestoneDTLPL", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        '''copied from backup file on 1st jul by Mohan... This function was missing in demo server copy.
        Public Function OpportunityDetails() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@OpportunityId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _ClientTimeZoneOffset

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_OPPDetailsDTLPL", arParms)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
    End Class

    End Namespace

