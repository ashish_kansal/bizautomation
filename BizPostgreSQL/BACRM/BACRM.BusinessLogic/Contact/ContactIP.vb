﻿'Created Anoop Jayaraj
Option Explicit On
Option Strict On


Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Namespace BACRM.BusinessLogic.Contacts
    Public Class ContactIP
        Inherits CContacts

        'Note:Declare ID Property in CContacts and Name Property in ContactIP - by chintan

        Private _Age As Integer
        Private _DocCount As Integer
        Private _Address As String
        Private _PositionName As String
        Private _DepartmentName As String
        Private _SexName As String
        Private _ContactTypeName As String
        Private _CategoryName As String
        Private _ManagerName As String
        Private _EmpStatusName As String
        Private _TeamName As String
        Private _LastFollowup As String
        Private _NextFollowup As String

        Public Property TeamName() As String
            Get
                Return _TeamName
            End Get
            Set(ByVal Value As String)
                _TeamName = Value
            End Set
        End Property

        Private _DripCampaignName As String
        Public Property DripCampaignName() As String
            Get
                Return _DripCampaignName
            End Get
            Set(ByVal value As String)
                _DripCampaignName = value
            End Set
        End Property
        Private _ContactECampaignID As Long
        Public Property ContactECampaignID() As Long
            Get
                Return _ContactECampaignID
            End Get
            Set(ByVal value As Long)
                _ContactECampaignID = value
            End Set
        End Property
        Public Property EmpStatusName() As String
            Get
                Return _EmpStatusName
            End Get
            Set(ByVal Value As String)
                _EmpStatusName = Value
            End Set
        End Property


        Public Property ManagerName() As String
            Get
                Return _ManagerName
            End Get
            Set(ByVal Value As String)
                _ManagerName = Value
            End Set
        End Property


        Public Property CategoryName() As String
            Get
                Return _CategoryName
            End Get
            Set(ByVal Value As String)
                _CategoryName = Value
            End Set
        End Property


        Public Property ContactTypeName() As String
            Get
                Return _ContactTypeName
            End Get
            Set(ByVal Value As String)
                _ContactTypeName = Value
            End Set
        End Property


        Public Property SexName() As String
            Get
                Return _SexName
            End Get
            Set(ByVal Value As String)
                _SexName = Value
            End Set
        End Property


        Public Property DepartmentName() As String
            Get
                Return _DepartmentName
            End Get
            Set(ByVal Value As String)
                _DepartmentName = Value
            End Set
        End Property


        Public Property PositionName() As String
            Get
                Return _PositionName
            End Get
            Set(ByVal Value As String)
                _PositionName = Value
            End Set
        End Property


        Public Property Address() As String
            Get
                Return _Address
            End Get
            Set(ByVal Value As String)
                _Address = Value
            End Set
        End Property

        Public Property DocCount() As Integer
            Get
                Return _DocCount
            End Get
            Set(ByVal Value As Integer)
                _DocCount = Value
            End Set
        End Property

        Public Property Age() As Integer
            Get
                Return _Age
            End Get
            Set(ByVal Value As Integer)
                _Age = Value
            End Set
        End Property

        Public Property vcPassword As String
        Public Property LastFollowup() As String
            Get
                Return _LastFollowup
            End Get
            Set(ByVal Value As String)
                _LastFollowup = Value
            End Set
        End Property

        Public Property NextFollowup() As String
            Get
                Return _NextFollowup
            End Get
            Set(ByVal Value As String)
                _NextFollowup = Value
            End Set
        End Property


        Public Function GetContactInfoEditIP() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = ContactID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = ClientTimeZoneOffset

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim dt As DataTable = SqlDAL.ExecuteDatable(connString, "usp_GetContactDTlPL", arParms)

                Dim objCommon As New Common.CCommon
                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    For Each dr As DataRow In dt.Rows
                        If Not IsDBNull(dr("vcFirstName")) Then
                            Me.FirstName = dr("vcFirstName").ToString
                        End If
                        If Not IsDBNull(dr("vcLastName")) Then
                            Me.LastName = dr("vcLastName").ToString
                        End If
                        If Not IsDBNull(dr("numPhone")) Then
                            Me.ContactPhone = dr("numPhone").ToString
                        End If
                        If Not IsDBNull(dr("numPhoneExtension")) Then
                            Me.ContactPhoneExt = dr("numPhoneExtension").ToString
                        End If
                        If Not IsDBNull(dr("vcPosition")) Then
                            Me.Position = CType(dr("vcPosition"), Long)
                        End If
                        If Not IsDBNull(dr("vcEmail")) Then
                            Me.Email = dr("vcEmail").ToString
                        End If
                        If Not IsDBNull(dr("numCell")) Then
                            Me.CellPhone = dr("numCell").ToString
                        End If
                        If Not IsDBNull(dr("vcTitle")) Then
                            Me.Title = dr("vcTitle").ToString
                        End If
                        If Not IsDBNull(dr("NumHomePhone")) Then
                            Me.HomePhone = dr("NumHomePhone").ToString
                        End If
                        If Not IsDBNull(dr("vcAltEmail")) Then
                            Me.AltEmail = dr("vcAltEmail").ToString
                        End If
                        If Not IsDBNull(dr("vcDepartment")) Then
                            Me.Department = CType(dr("vcDepartment"), Long)
                        End If
                        If Not IsDBNull(dr("bintDOB")) Then
                            Me.DOB = CType(dr("bintDOB"), Date)
                        End If
                        If Not IsDBNull(dr("Age")) Then
                            Me.Age = CType(dr("Age"), Integer)
                        End If
                        If Not IsDBNull(dr("Age")) Then
                            _Age = CType(dr("Age"), Integer)
                        End If
                        If Not IsDBNull(dr("vcFax")) Then
                            Me.Fax = CType(dr("vcFax"), String)
                        End If
                        If Not IsDBNull(dr("charSex")) Then
                            Me.Sex = CType(dr("charSex"), String)
                        End If
                        If Not IsDBNull(dr("vcContactType")) Then
                            Me.ContactType = CType(dr("vcContactType"), Long)
                        End If
                        If Not IsDBNull(dr("vcCategory")) Then
                            Me.Category = CType(dr("vcCategory"), Long)
                        End If
                        If Not IsDBNull(dr("bitOptOut")) Then
                            Me.OptOut = CType(dr("bitOptOut"), Boolean)
                        End If
                        If Not IsDBNull(dr("Manager")) Then
                            Me.Manager = CType(dr("Manager"), Long)
                        End If
                        If Not IsDBNull(dr("numEmpStatus")) Then
                            Me.EmpStatus = CType(dr("numEmpStatus"), Long)
                        End If
                        If Not IsDBNull(dr("vcTeam")) Then
                            Me.Team = CType(dr("vcTeam"), Long)
                        End If
                        If Not IsDBNull(dr("DocumentCount")) Then
                            Me.DocCount = CType(dr("DocumentCount"), Integer)
                        End If
                        If Not IsDBNull(dr("Address")) Then
                            Me.Address = CType(dr("Address"), String)
                        End If
                        If Not IsDBNull(dr("VcAsstFirstName")) Then
                            Me.AssFirstName = CType(dr("VcAsstFirstName"), String)
                        End If
                        If Not IsDBNull(dr("vcAsstLastName")) Then
                            Me.AssLastName = CType(dr("vcAsstLastName"), String)
                        End If
                        If Not IsDBNull(dr("numAsstPhone")) Then
                            Me.AssPhone = CType(dr("numAsstPhone"), String)
                        End If
                        If Not IsDBNull(dr("numAsstExtn")) Then
                            Me.AssPhoneExt = CType(dr("numAsstExtn"), String)
                        End If
                        If Not IsDBNull(dr("vcAsstEmail")) Then
                            Me.AssEmail = CType(dr("vcAsstEmail"), String)
                        End If
                        If Not IsDBNull(dr("vcPositionName")) Then
                            Me.PositionName = CType(dr("vcPositionName"), String)
                        End If
                        If Not IsDBNull(dr("vcDepartmentName")) Then
                            Me.DepartmentName = CType(dr("vcDepartmentName"), String)
                        End If
                        If Not IsDBNull(dr("vcContactTypeName")) Then
                            Me.ContactTypeName = CType(dr("vcContactTypeName"), String)
                        End If
                        If Not IsDBNull(dr("charSexName")) Then
                            Me.SexName = CType(dr("charSexName"), String)
                        End If
                        If Not IsDBNull(dr("vcCategoryName")) Then
                            Me.CategoryName = CType(dr("vcCategoryName"), String)
                        End If
                        If Not IsDBNull(dr("ManagerName")) Then
                            Me.ManagerName = CType(dr("ManagerName"), String)
                        End If
                        If Not IsDBNull(dr("numEmpStatusName")) Then
                            Me.EmpStatusName = CType(dr("numEmpStatusName"), String)
                        End If
                        If Not IsDBNull(dr("vcTeamName")) Then
                            Me.TeamName = CType(dr("vcTeamName"), String)
                        End If
                        If Not IsDBNull(dr("txtNotes")) Then
                            Me.Comments = CType(dr("txtNotes"), String)
                        End If
                        If Not IsDBNull(dr("vcECampName")) Then
                            Me.DripCampaignName = CType(dr("vcECampName"), String)
                        End If
                        If Not IsDBNull(dr("numECampaignID")) Then
                            Me.DripCampaign = CType(dr("numECampaignID"), Long)
                        End If
                        If Not IsDBNull(dr("numConEmailCampID")) Then
                            Me.ContactECampaignID = CType(dr("numConEmailCampID"), Long)
                        End If
                        If Not IsDBNull(dr("numDivisionID")) Then
                            Me.DivisionID = CType(dr("numDivisionID"), Long)
                        End If
                        If Not IsDBNull(dr("bitPrimaryContact")) Then
                            Me.PrimaryContact = CType(dr("bitPrimaryContact"), Boolean)
                        End If
                        If Not IsDBNull(dr("vcLinkedinId")) Then
                            Me.LinkedinId = CType(dr("vcLinkedinId"), String)
                        End If
                        If Not IsDBNull(dr("vcLinkedinUrl")) Then
                            Me.LinkedinUrl = CType(dr("vcLinkedinUrl"), String)
                        End If
                        If Not IsDBNull(dr("vcPassword")) Then
                            If CType(dr("vcPassword"), String).Trim() <> "" Then
                                Me.vcPassword = objCommon.Decrypt(CType(dr("vcPassword"), String))
                            Else
                                Me.vcPassword = ""
                            End If
                        End If
                        If Not IsDBNull(dr("vcTaxID")) Then
                            Me.TaxID = CType(dr("vcTaxID"), String)
                        End If
                        If Not IsDBNull(dr("vcLastFollowup")) Then
                            Me.LastFollowup = CType(dr("vcLastFollowup"), String)
                        End If
                        If Not IsDBNull(dr("vcNextFollowup")) Then
                            Me.NextFollowup = CType(dr("vcNextFollowup"), String)
                        End If
                        If Not IsDBNull(dr("ContactPhoneExt")) Then
                            Me.ContactPhoneExt = CType(dr("ContactPhoneExt"), String)
                        End If
                    Next
                End If
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

    End Class
End Namespace
