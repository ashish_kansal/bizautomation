﻿Imports BACRM.BusinessLogic.Common
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Contacts

    Public Class AddressDetails
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"

        Public Property AddressID As Long
        Public Property RecordID As Long

#End Region

#Region "Public Methods"

        Public Sub UpdateRecordID()
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numAddressID", AddressID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numRecordID", RecordID, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_AddressDetails_UpdateRecordID", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

#End Region

    End Class

End Namespace