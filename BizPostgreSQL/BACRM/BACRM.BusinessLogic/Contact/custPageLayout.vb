Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Namespace BACRM.BusinessLogic.Contacts
    Public Class CcustPageLayout
        Inherits BACRM.BusinessLogic.CBusinessBase


        ''Private DomainId As Long = 0
        Private _ColumnID As Long = 0
        Private _tintRow As Long = 0
        Private _RowID As Long = 0
        Private _ContactID As Int64 = 0
        'Private UserCntID As Int64 = 0
        Private _RowString As String = ""
        Private _CoType As Char
        Private _PageId As Int64
        Private _numRelation As Int64
        Private _numRelCntType As Int64 = 0

        Public Property numRelCntType() As Int64
            Get
                Return _numRelCntType
            End Get
            Set(ByVal Value As Int64)
                _numRelCntType = Value
            End Set
        End Property

        Public Property numRelation() As Int64
            Get
                Return _numRelation
            End Get
            Set(ByVal Value As Int64)
                _numRelation = Value
            End Set
        End Property
        Public Property PageId() As Int64
            Get
                Return _PageId
            End Get
            Set(ByVal Value As Int64)
                _PageId = Value
            End Set
        End Property
        Public Property CoType() As Char
            Get
                Return _CoType
            End Get
            Set(ByVal Value As Char)
                _CoType = Value
            End Set
        End Property
        Public Property RowString() As String
            Get
                Return _RowString
            End Get
            Set(ByVal Value As String)
                _RowString = Value
            End Set
        End Property
        Public Property tintRow() As Integer
            Get
                Return _tintRow
            End Get
            Set(ByVal Value As Integer)
                _tintRow = Value
            End Set
        End Property
        Public Property ContactID() As Int64
            Get
                Return _ContactID
            End Get
            Set(ByVal Value As Int64)
                _ContactID = Value
            End Set
        End Property
        'Public Property UserCntID() As Int64
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal Value As Int64)
        '        UserCntID = Value
        '    End Set
        'End Property
        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property
        Public Property ColumnID() As Long
            Get
                Return _ColumnID
            End Get
            Set(ByVal Value As Long)
                _ColumnID = Value
            End Set
        End Property
        Public Property RowID() As Long
            Get
                Return _rowID
            End Get
            Set(ByVal Value As Long)
                _rowID = Value
            End Set
        End Property

        Private _FormId As Int64
        Public Property FormId() As Int64
            Get
                Return _FormId
            End Get
            Set(ByVal Value As Int64)
                _FormId = Value
            End Set
        End Property

        Private _PageType As Integer
        Public Property PageType() As Integer
            Get
                Return _PageType
            End Get
            Set(ByVal Value As Integer)
                _PageType = Value
            End Set
        End Property

        Private _TabId As Integer
        Public Property TabId() As Integer
            Get
                Return _TabId
            End Get
            Set(ByVal Value As Integer)
                _TabId = Value
            End Set
        End Property
        Public Function getValues() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@intcoulmn", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _ColumnID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainId

                arParms(3) = New Npgsql.NpgsqlParameter("@Ctype", NpgsqlTypes.NpgsqlDbType.Char)
                arParms(3).Value = _CoType

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "usp_GetLayoutInfo", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function getValuesWithddl() As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@intcoulmn", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _ColumnID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainId

                arParms(3) = New Npgsql.NpgsqlParameter("@PageId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _PageId

                arParms(4) = New Npgsql.NpgsqlParameter("@numRelation", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _numRelation

                arParms(5) = New Npgsql.NpgsqlParameter("@numFormID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _FormId

                arParms(6) = New Npgsql.NpgsqlParameter("@tintPageType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(6).Value = _PageType

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "usp_GetLayoutInfoDdl", arParms)
                Return ds
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function SaveList() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@strRow", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(2).Value = _RowString

                arParms(3) = New Npgsql.NpgsqlParameter("@intcoulmn", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = ColumnID

                arParms(4) = New Npgsql.NpgsqlParameter("@numFormID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _FormId

                arParms(5) = New Npgsql.NpgsqlParameter("@numRelCntType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _numRelCntType

                arParms(6) = New Npgsql.NpgsqlParameter("@tintPageType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(6).Value = _PageType

                SqlDAL.ExecuteNonQuery(connString, "USP_SaveLayoutList", arParms)
                Return True

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function GetMaxRows() As String
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@Ctype", NpgsqlTypes.NpgsqlDbType.Char)
                arParms(2).Value = _CoType

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return CStr(SqlDAL.ExecuteScalar(connString, "USP_GetMaxRows", arParms))


            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCustomTabLayoutInfoDdl() As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@intcoulmn", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _ColumnID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@PageId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _PageId

                arParms(4) = New Npgsql.NpgsqlParameter("@numRelation", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _numRelation

                arParms(5) = New Npgsql.NpgsqlParameter("@numFormID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _FormId

                arParms(6) = New Npgsql.NpgsqlParameter("@tintPageType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(6).Value = _PageType

                arParms(7) = New Npgsql.NpgsqlParameter("@numTabId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _TabId

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "usp_GetCustomTabLayoutInfoDdl", arParms)
                Return ds
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function SaveCustomTabLayoutList() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@strRow", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(2).Value = _RowString

                arParms(3) = New Npgsql.NpgsqlParameter("@intcoulmn", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = ColumnID

                arParms(4) = New Npgsql.NpgsqlParameter("@numFormID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _FormId

                arParms(5) = New Npgsql.NpgsqlParameter("@numRelCntType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _numRelCntType

                arParms(6) = New Npgsql.NpgsqlParameter("@tintPageType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(6).Value = _PageType

                arParms(7) = New Npgsql.NpgsqlParameter("@numTabId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _TabId

                SqlDAL.ExecuteNonQuery(connString, "USP_SaveCustomTabLayoutList", arParms)
                Return True

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
    End Class
End Namespace

