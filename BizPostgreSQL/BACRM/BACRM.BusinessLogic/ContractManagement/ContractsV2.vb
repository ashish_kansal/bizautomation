﻿
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Contract
    Public Class ContractsV2
        Inherits BACRM.BusinessLogic.CBusinessBase
        Private _ContractID As Long = 0
        Private _ContractIds As String = ""
        Private _Type As Integer = 0
        Private _DivisionId As Long = 0
        Private _Incidents As Long = 0
        Private _IncidentLeft As Long = 0
        Private _Hours As Long = 0
        Private _Minutes As Long = 0
        Private _ItemClassification As String = ""
        Private _WarrantyDays As Long = 0
        Private _Notes As String = ""
        Private _CurrentPage As Integer
        Private _TotalRecords As Integer
        Private _PageSize As Integer
        Private _ClientTimeZoneOffset As Integer
        Private _RegularSearchCriteria As String = ""
        Private _CustomSearchCriteria As String = ""
        Private _SearchText As String = ""

        Public Property SearchText() As String
            Get
                Return _SearchText
            End Get
            Set(ByVal Value As String)
                _SearchText = Value
            End Set
        End Property
        Public Property CustomSearchCriteria() As String
            Get
                Return _CustomSearchCriteria
            End Get
            Set(ByVal Value As String)
                _CustomSearchCriteria = Value
            End Set
        End Property
        Public Property RegularSearchCriteria() As String
            Get
                Return _RegularSearchCriteria
            End Get
            Set(ByVal Value As String)
                _RegularSearchCriteria = Value
            End Set
        End Property


        Public Property ClientTimeZoneOffset() As Integer
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal Value As Integer)
                _ClientTimeZoneOffset = Value
            End Set
        End Property
        Public Property TotalRecords() As Integer
            Get
                Return _TotalRecords
            End Get
            Set(ByVal Value As Integer)
                _TotalRecords = Value
            End Set
        End Property
        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal Value As Integer)
                _PageSize = Value
            End Set
        End Property
        Public Property CurrentPage() As Integer
            Get
                Return _CurrentPage
            End Get
            Set(ByVal Value As Integer)
                _CurrentPage = Value
            End Set
        End Property
        Public Property Notes() As String
            Get
                Return _Notes
            End Get
            Set(ByVal Value As String)
                _Notes = Value
            End Set
        End Property
        Public Property WarrantyDays() As Long
            Get
                Return _WarrantyDays
            End Get
            Set(ByVal Value As Long)
                _WarrantyDays = Value
            End Set
        End Property
        Public Property ItemClassification() As String
            Get
                Return _ItemClassification
            End Get
            Set(ByVal Value As String)
                _ItemClassification = Value
            End Set
        End Property
        Public Property Minutes() As Long
            Get
                Return _Minutes
            End Get
            Set(ByVal Value As Long)
                _Minutes = Value
            End Set
        End Property

        Public Property Hours() As Long
            Get
                Return _Hours
            End Get
            Set(ByVal Value As Long)
                _Hours = Value
            End Set
        End Property
        Public Property IncidentLeft() As Long
            Get
                Return _IncidentLeft
            End Get
            Set(ByVal Value As Long)
                _IncidentLeft = Value
            End Set
        End Property
        Public Property Incidents() As Long
            Get
                Return _Incidents
            End Get
            Set(ByVal Value As Long)
                _Incidents = Value
            End Set
        End Property
        Public Property DivisionId() As Long
            Get
                Return _DivisionId
            End Get
            Set(ByVal Value As Long)
                _DivisionId = Value
            End Set
        End Property
        Public Property Type() As Integer
            Get
                Return _Type
            End Get
            Set(ByVal Value As Integer)
                _Type = Value
            End Set
        End Property
        Public Property ContractID() As Long
            Get
                Return _ContractID
            End Get
            Set(ByVal Value As Long)
                _ContractID = Value
            End Set
        End Property

        Public Property ContractIds() As String
            Get
                Return _ContractIds
            End Get
            Set(value As String)
                _ContractIds = value
            End Set
        End Property

        Public Function SaveContracts() As Long
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(11) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numContractId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _ContractID

                arParms(1) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.Integer, 9)
                arParms(1).Value = _Type

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@numDivisonId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = _DivisionId

                arParms(5) = New Npgsql.NpgsqlParameter("@numIncidents", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(5).Value = _Incidents

                arParms(6) = New Npgsql.NpgsqlParameter("@numIncidentLeft", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(6).Value = _IncidentLeft

                arParms(7) = New Npgsql.NpgsqlParameter("@numHours", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(7).Value = _Hours

                arParms(8) = New Npgsql.NpgsqlParameter("@numMinutes", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(8).Value = _Minutes

                arParms(9) = New Npgsql.NpgsqlParameter("@vcItemClassification", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(9).Value = _ItemClassification

                arParms(10) = New Npgsql.NpgsqlParameter("@numWarrantyDays", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(10).Value = _WarrantyDays

                arParms(11) = New Npgsql.NpgsqlParameter("@vcNotes", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(11).Value = _Notes

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageContracts", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetContractsList() As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(11) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _CurrentPage

                arParms(3) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _PageSize

                arParms(4) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Direction = ParameterDirection.InputOutput
                arParms(4).Value = _TotalRecords

                arParms(5) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _ClientTimeZoneOffset

                arParms(6) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.Integer, 9)
                arParms(6).Value = _Type

                arParms(7) = New Npgsql.NpgsqlParameter("@vcRegularSearchCriteria", NpgsqlTypes.NpgsqlDbType.VarChar, 50000)
                arParms(7).Value = _RegularSearchCriteria

                arParms(8) = New Npgsql.NpgsqlParameter("@vcCustomSearchCriteria", NpgsqlTypes.NpgsqlDbType.VarChar, 50000)
                arParms(8).Value = _CustomSearchCriteria

                arParms(9) = New Npgsql.NpgsqlParameter("@SearchText", NpgsqlTypes.NpgsqlDbType.VarChar, 50000)
                arParms(9).Value = _SearchText

                arParms(10) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(10).Value = Nothing
                arParms(10).Direction = ParameterDirection.InputOutput

                arParms(11) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(11).Value = Nothing
                arParms(11).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetContracts2", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(4).Value)

                Return ds
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetContactsDetails() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numContractId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = ContractID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _ClientTimeZoneOffset

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_ContractDetails", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetContactsLog() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numContractId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = ContractID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_ContractsLog", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetWarrantyItemClassification() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetWarrantyItemClassification", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function DeleteContracts() As Long
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@vcContractId", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(0).Value = _ContractIds

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteContracts", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace
