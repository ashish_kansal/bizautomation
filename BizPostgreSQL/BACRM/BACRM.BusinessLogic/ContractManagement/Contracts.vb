

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Contract

    Public Class CContracts
        Inherits BACRM.BusinessLogic.CBusinessBase


        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Goyal 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Goyal 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region


        Private _CategoryHDRID As Long = 0

        Private _ContractID As Long = 0
        Private _ContactID As Long = 0
        Private _DivisionId As Long = 0
        Private _ContractName As String
        Private _tintDays As Short = 0
        Private _tintIncidents As Short = 0
        Private _tintHours As Short = 0
        Private _Days As Long = 0
        Private _Incidents As Long = 0
        Private _Hours As Long = 0
        Private _StartDate As Date = New Date(1753, 1, 1)
        Private _ExpDate As Date = New Date(1753, 1, 1)
        Private _Notes As String = ""
        Private _tintEmailDays As Short = 0
        Private _tintEmailIncidents As Short = 0
        Private _tintEmailHours As Short = 0
        Private _EmailDays As Long = 0
        Private _EmailIncidents As Long = 0
        Private _EmailHours As Long = 0
        'Private UserCntID As Long = 0
        ''Private DomainId As Long = 0
        Private _SortOrder As Integer = 0
        Private _Amount As Integer = 0
        Private _bitAmount As Boolean = True
        Private _bitHrs As Boolean = True
        Private _bitDays As Boolean = True
        Private _bitInci As Boolean = True
        Private _bitEHrs As Boolean = True
        Private _bitEDays As Boolean = True
        Private _bitEInci As Boolean = True
        Private _TemplateId As Long = 0
        Private _rate As Decimal = 0
        Private _ClientTimeZoneOffset As Integer

        Public Property CategoryHDRID() As Long
            Get
                Return _CategoryHDRID
            End Get
            Set(ByVal Value As Long)
                _CategoryHDRID = Value
            End Set
        End Property

        Public Property ClientTimeZoneOffset() As Integer
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal Value As Integer)
                _ClientTimeZoneOffset = Value
            End Set
        End Property
        Public Property rate() As Decimal
            Get
                Return _rate
            End Get
            Set(ByVal Value As Decimal)
                _rate = Value
            End Set
        End Property
        Public Property TemplateId() As Long
            Get
                Return _TemplateId
            End Get
            Set(ByVal Value As Long)
                _TemplateId = Value
            End Set
        End Property
        Public Property bitEHrs() As Boolean
            Get
                Return _bitEHrs
            End Get
            Set(ByVal Value As Boolean)
                _bitEHrs = Value
            End Set
        End Property
        Public Property bitEDays() As Boolean
            Get
                Return _bitEDays
            End Get
            Set(ByVal Value As Boolean)
                _bitEDays = Value
            End Set
        End Property
        Public Property bitEInci() As Boolean
            Get
                Return _bitEInci
            End Get
            Set(ByVal Value As Boolean)
                _bitEInci = Value
            End Set
        End Property
        Public Property bitAmount() As Boolean
            Get
                Return _bitAmount
            End Get
            Set(ByVal Value As Boolean)
                _bitAmount = Value
            End Set
        End Property
        Public Property bitHrs() As Boolean
            Get
                Return _bitHrs
            End Get
            Set(ByVal Value As Boolean)
                _bitHrs = Value
            End Set
        End Property
        Public Property bitDays() As Boolean
            Get
                Return _bitDays
            End Get
            Set(ByVal Value As Boolean)
                _bitDays = Value
            End Set
        End Property
        Public Property bitInci() As Boolean
            Get
                Return _bitInci
            End Get
            Set(ByVal Value As Boolean)
                _bitInci = Value
            End Set
        End Property

        Public Property ContractID() As Long
            Get
                Return _ContractID
            End Get
            Set(ByVal Value As Long)
                _ContractID = Value
            End Set
        End Property
        Public Property ContactID() As Long
            Get
                Return _ContactID
            End Get
            Set(ByVal Value As Long)
                _ContactID = Value
            End Set
        End Property
        Public Property Amount() As Long
            Get
                Return _Amount
            End Get
            Set(ByVal Value As Long)
                _Amount = Value
            End Set
        End Property
        Public Property DivisionId() As Long
            Get
                Return _DivisionId
            End Get
            Set(ByVal Value As Long)
                _DivisionId = Value
            End Set
        End Property

        Public Property ContractName() As String
            Get
                Return _ContractName
            End Get
            Set(ByVal Value As String)
                _ContractName = Value
            End Set
        End Property

        Public Property Days() As Long
            Get
                Return _Days
            End Get
            Set(ByVal Value As Long)
                _Days = Value
            End Set
        End Property

        Public Property Incidents() As Long
            Get
                Return _Incidents
            End Get
            Set(ByVal Value As Long)
                _Incidents = Value
            End Set
        End Property

        Public Property Hours() As Long
            Get
                Return _Hours
            End Get
            Set(ByVal Value As Long)
                _Hours = Value
            End Set
        End Property

        Public Property StartDate() As Date
            Get
                Return _StartDate
            End Get
            Set(ByVal Value As Date)
                _StartDate = Value
            End Set
        End Property

        Public Property ExpDate() As Date
            Get
                Return _ExpDate
            End Get
            Set(ByVal Value As Date)
                _ExpDate = Value
            End Set
        End Property

        Public Property Notes() As String
            Get
                Return _Notes
            End Get
            Set(ByVal Value As String)
                _Notes = Value
            End Set
        End Property

        Public Property EmailDays() As Long
            Get
                Return _EmailDays
            End Get
            Set(ByVal Value As Long)
                _EmailDays = Value
            End Set
        End Property

        Public Property EmailIncidents() As Long
            Get
                Return _EmailIncidents
            End Get
            Set(ByVal Value As Long)
                _EmailIncidents = Value
            End Set
        End Property

        Public Property EmailHours() As Long
            Get
                Return _EmailHours
            End Get
            Set(ByVal Value As Long)
                _EmailHours = Value
            End Set
        End Property

        'Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal Value As Long)
        '        UserCntID = Value
        '    End Set
        'End Property

        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property
        Public Property SortOrder() As Integer
            Get
                Return _SortOrder
            End Get
            Set(ByVal Value As Integer)
                _SortOrder = Value
            End Set
        End Property
        Private _SortCharacter As Char
        Public Property SortCharacter() As Char
            Get
                Return _SortCharacter
            End Get
            Set(ByVal Value As Char)
                _SortCharacter = Value
            End Set
        End Property
        Private _columnName As String
        Public Property columnName() As String
            Get
                Return _columnName
            End Get
            Set(ByVal Value As String)
                _columnName = Value
            End Set
        End Property
        Private _columnSortOrder As String
        Public Property columnSortOrder() As String
            Get
                Return _columnSortOrder
            End Get
            Set(ByVal Value As String)
                _columnSortOrder = Value
            End Set
        End Property
        Private _TotalRecords As Integer
        Public Property TotalRecords() As Integer
            Get
                Return _TotalRecords
            End Get
            Set(ByVal Value As Integer)
                _TotalRecords = Value
            End Set
        End Property
        Private _PageSize As Integer
        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal Value As Integer)
                _PageSize = Value
            End Set
        End Property
        Private _CurrentPage As Integer
        Public Property CurrentPage() As Integer
            Get
                Return _CurrentPage
            End Get
            Set(ByVal Value As Integer)
                _CurrentPage = Value
            End Set
        End Property

        Public Function SaveContracts() As Long
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(22) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numContractId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _ContractID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _DivisionId

                arParms(2) = New Npgsql.NpgsqlParameter("@vcContractName", NpgsqlTypes.NpgsqlDbType.VarChar, 250)
                arParms(2).Value = _ContractName

                arParms(3) = New Npgsql.NpgsqlParameter("@bintStartDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(3).Value = _StartDate

                arParms(4) = New Npgsql.NpgsqlParameter("@bintExpDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(4).Value = _ExpDate

                arParms(5) = New Npgsql.NpgsqlParameter("@numIncidents", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(5).Value = _Incidents

                arParms(6) = New Npgsql.NpgsqlParameter("@numHours", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(6).Value = _Hours

                arParms(7) = New Npgsql.NpgsqlParameter("@vcNotes", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(7).Value = _Notes

                arParms(8) = New Npgsql.NpgsqlParameter("@numEmailDays", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = _EmailDays

                arParms(9) = New Npgsql.NpgsqlParameter("@numEmailIncidents", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = _EmailIncidents

                arParms(10) = New Npgsql.NpgsqlParameter("@numEmailHours", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(10).Value = _EmailHours

                arParms(11) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(11).Value = UserCntID

                arParms(12) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(12).Value = DomainId

                arParms(13) = New Npgsql.NpgsqlParameter("@numAmount", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(13).Value = _Amount


                arParms(14) = New Npgsql.NpgsqlParameter("@bitAmount", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(14).Value = _bitAmount

                arParms(15) = New Npgsql.NpgsqlParameter("@bitDays", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(15).Value = _bitDays

                arParms(16) = New Npgsql.NpgsqlParameter("@bitHours", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(16).Value = _bitHrs

                arParms(17) = New Npgsql.NpgsqlParameter("@bitInci", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(17).Value = _bitInci

                arParms(18) = New Npgsql.NpgsqlParameter("@bitEDays", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(18).Value = _bitEDays

                arParms(19) = New Npgsql.NpgsqlParameter("@bitEHours", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(19).Value = _bitEHrs

                arParms(20) = New Npgsql.NpgsqlParameter("@bitEInci", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(20).Value = _bitEInci

                arParms(21) = New Npgsql.NpgsqlParameter("@decRate", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(21).Value = _rate

                arParms(22) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(22).Value = Nothing
                arParms(22).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ContractManage", objParam, True)

                Return CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Sub ModifyContracts()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(22) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numContractId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _ContractID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _DivisionId

                arParms(2) = New Npgsql.NpgsqlParameter("@vcContractName", NpgsqlTypes.NpgsqlDbType.VarChar, 250)
                arParms(2).Value = _ContractName

                arParms(3) = New Npgsql.NpgsqlParameter("@bintStartDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(3).Value = _StartDate

                arParms(4) = New Npgsql.NpgsqlParameter("@bintExpDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(4).Value = _ExpDate

                arParms(5) = New Npgsql.NpgsqlParameter("@numIncidents", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(5).Value = _Incidents

                arParms(6) = New Npgsql.NpgsqlParameter("@numHours", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(6).Value = _Hours

                arParms(7) = New Npgsql.NpgsqlParameter("@vcNotes", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(7).Value = _Notes

                arParms(8) = New Npgsql.NpgsqlParameter("@numEmailDays", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = _EmailDays

                arParms(9) = New Npgsql.NpgsqlParameter("@numEmailIncidents", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = _EmailIncidents

                arParms(10) = New Npgsql.NpgsqlParameter("@numEmailHours", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(10).Value = _EmailHours

                arParms(11) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(11).Value = UserCntID

                arParms(12) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(12).Value = DomainId

                arParms(13) = New Npgsql.NpgsqlParameter("@numAmount", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(13).Value = _Amount

                arParms(14) = New Npgsql.NpgsqlParameter("@bitAmount", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(14).Value = _bitAmount

                arParms(15) = New Npgsql.NpgsqlParameter("@bitDays", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(15).Value = _bitDays

                arParms(16) = New Npgsql.NpgsqlParameter("@bitHours", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(16).Value = _bitHrs

                arParms(17) = New Npgsql.NpgsqlParameter("@bitInci", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(17).Value = _bitInci


                arParms(18) = New Npgsql.NpgsqlParameter("@bitEDays", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(18).Value = _bitEDays

                arParms(19) = New Npgsql.NpgsqlParameter("@bitEHours", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(19).Value = _bitEHrs

                arParms(20) = New Npgsql.NpgsqlParameter("@bitEInci", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(20).Value = _bitEInci

                arParms(21) = New Npgsql.NpgsqlParameter("@numTemplateId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(21).Value = _TemplateId

                arParms(22) = New Npgsql.NpgsqlParameter("@rateHr", NpgsqlTypes.NpgsqlDbType.Numeric, 9)
                arParms(22).Value = _rate

                SqlDAL.ExecuteNonQuery(connString, "USP_ModifyContract", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Function GetContactList() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(11) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintSortOrder", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _SortOrder

                arParms(3) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(3).Value = _SortCharacter

                arParms(4) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _CurrentPage

                arParms(5) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _PageSize

                arParms(6) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Direction = ParameterDirection.InputOutput
                arParms(6).Value = _TotalRecords

                arParms(7) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(7).Value = _columnName

                arParms(8) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(8).Value = _columnSortOrder

                arParms(9) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(9).Value = _ClientTimeZoneOffset

                arParms(10) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(10).Value = _DivisionId

                arParms(11) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(11).Value = Nothing
                arParms(11).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "usp_GetContractList", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(6).Value)
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function GetContractDtl() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numContractID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _ContractID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "usp_GetContractDtl", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function
        Public Function GetContractHrsAmount() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numContractID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _ContractID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@numCategoryHDRID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _CategoryHDRID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "usp_GetContractHrsAmount", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function
        Sub DeleteContract()
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

            arParms(0) = New Npgsql.NpgsqlParameter("@numContractID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
            arParms(0).Value = _ContractID

            arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
            arParms(1).Value = DomainId

            SqlDAL.ExecuteNonQuery(connString, "usp_DeleteContract", arParms)
        End Sub
        Public Function GetContractDdlList() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _DivisionId

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetContractDdlList", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function
        Sub SaveContractsContacts()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _ContactID

                arParms(1) = New Npgsql.NpgsqlParameter("@numContractID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _ContractID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = DomainId



                SqlDAL.ExecuteScalar(connString, "USP_SaveContractsContacts", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Function GetContactSelected() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numContractID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _ContractID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetContactSelected", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function
        Sub DeleteContacts()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _ContactID

                arParms(1) = New Npgsql.NpgsqlParameter("@numContractID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _ContractID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = DomainId

                SqlDAL.ExecuteScalar(connString, "USP_DeleteContractsContacts", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Function getContractLinkedItems() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numContractID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _ContractID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _ClientTimeZoneOffset

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GeContractLinkedItems", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function
        Public Function GetContactListFromDiv() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _DivisionId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _ClientTimeZoneOffset

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetContactListFromDiv", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetContactDetail() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numContractID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = ContractID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_ContractManagment_GetDetail", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace
