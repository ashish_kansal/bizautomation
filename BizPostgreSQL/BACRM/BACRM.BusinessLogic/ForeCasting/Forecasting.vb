
'Created  By Anoop Jayaraj
Option Explicit On 
Option Strict On
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Namespace BACRM.BusinessLogic.Forecasting

    Public Class Forecast
        Inherits BACRM.BusinessLogic.CBusinessBase

        'Private DomainId As Long
        Private _Type As Short
        Private _OppStatus As Short
        Private _ItemCode As Long
        Private _ForecastType As Integer
        Private _Year As Integer
        Private _Quarter As Integer
        Private _strForecast As String
        'Private UserCntID As Long
        Private _strTeam As String
        Private _ForecastAmt As Long
        Private _ForecastID As Long
        Private _DivisionID As Long
        Private _firstMonth As Short
        Private _secondMonth As Short
        Private _thirdMonth As Short
        Private _IntYear As Integer
        Private _byteMode As Short

        Public Property byteMode() As Short
            Get
                Return _byteMode
            End Get
            Set(ByVal Value As Short)
                _byteMode = Value
            End Set
        End Property

        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property

        Public Property ForecastID() As Long
            Get
                Return _ForecastID
            End Get
            Set(ByVal Value As Long)
                _ForecastID = Value
            End Set
        End Property

        Public Property ForecastAmt() As Long
            Get
                Return _ForecastAmt
            End Get
            Set(ByVal Value As Long)
                _ForecastAmt = Value
            End Set
        End Property

        Public Property strTeam() As String
            Get
                Return _strTeam
            End Get
            Set(ByVal Value As String)
                _strTeam = Value
            End Set
        End Property

        'Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal Value As Long)
        '        UserCntID = Value
        '    End Set
        'End Property

        Public Property strForecast() As String
            Get
                Return _strForecast
            End Get
            Set(ByVal Value As String)
                _strForecast = Value
            End Set
        End Property

        Public Property Quarter() As Integer
            Get
                Return _Quarter
            End Get
            Set(ByVal Value As Integer)
                _Quarter = Value
            End Set
        End Property

        Public Property Year() As Integer
            Get
                Return _Year
            End Get
            Set(ByVal Value As Integer)
                _Year = Value
            End Set
        End Property

        Public Property ForecastType() As Integer
            Get
                Return _ForecastType
            End Get
            Set(ByVal Value As Integer)
                _ForecastType = Value
            End Set
        End Property

        Public Property ItemCode() As Long
            Get
                Return _ItemCode
            End Get
            Set(ByVal Value As Long)
                _ItemCode = Value
            End Set
        End Property

        Public Property OppStatus() As Short
            Get
                Return _OppStatus
            End Get
            Set(ByVal Value As Short)
                _OppStatus = Value
            End Set
        End Property

        Public Property Type() As Short
            Get
                Return _Type
            End Get
            Set(ByVal Value As Short)
                _Type = Value
            End Set
        End Property

        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property

        Public Property firstMonth() As Short
            Get
                Return _firstMonth
            End Get
            Set(ByVal Value As Short)
                _firstMonth = Value
            End Set
        End Property

        Public Property secondMonth() As Short
            Get
                Return _secondMonth
            End Get
            Set(ByVal Value As Short)
                _secondMonth = Value
            End Set
        End Property

        Public Property thirdMonth() As Short
            Get
                Return _thirdMonth
            End Get
            Set(ByVal Value As Short)
                _thirdMonth = Value
            End Set
        End Property

        Public Property IntYear() As Integer
            Get
                Return _IntYear
            End Get
            Set(ByVal value As Integer)
                _IntYear = value
            End Set
        End Property


        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop Jayaraj

        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop Jayaraj
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region

        Public Function GetForeCastOpp() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(9) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _Type

                arParms(3) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _DivisionID

                arParms(4) = New Npgsql.NpgsqlParameter("@tintFirstMonth", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _firstMonth

                arParms(5) = New Npgsql.NpgsqlParameter("@tintSecondMonth", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _secondMonth

                arParms(6) = New Npgsql.NpgsqlParameter("@tintThirdMonth", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(6).Value = _thirdMonth

                arParms(7) = New Npgsql.NpgsqlParameter("@intYear", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(7).Value = _IntYear

                arParms(8) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(8).Value = _byteMode

                arParms(9) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(9).Value = Nothing
                arParms(9).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_ForeCastOPP", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetForecastItems() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(10) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _Type

                arParms(3) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _DivisionID

                arParms(4) = New Npgsql.NpgsqlParameter("@tintFirstMonth", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _firstMonth

                arParms(5) = New Npgsql.NpgsqlParameter("@tintSecondMonth", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _secondMonth

                arParms(6) = New Npgsql.NpgsqlParameter("@tintThirdMonth", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(6).Value = _thirdMonth

                arParms(7) = New Npgsql.NpgsqlParameter("@intYear", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(7).Value = _IntYear

                arParms(8) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(8).Value = _byteMode

                arParms(9) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = _ItemCode

                arParms(10) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(10).Value = Nothing
                arParms(10).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_ForeCastItems", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function SaveForeCast() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@strForecast", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(0).Value = _strForecast

                arParms(1) = New Npgsql.NpgsqlParameter("@tintForecastType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _ForecastType

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = UserCntID

                arParms(3) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _ItemCode


                arParms(4) = New Npgsql.NpgsqlParameter("@sintYear", NpgsqlTypes.NpgsqlDbType.SmallInt)
                arParms(4).Value = _Year


                arParms(5) = New Npgsql.NpgsqlParameter("@tintquarter", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _Quarter

                arParms(6) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = DomainId


                SqlDAL.ExecuteNonQuery(connString, "USP_ForecastManage", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteForeCast() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@tintForecastType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = _ForecastType

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _ItemCode


                arParms(3) = New Npgsql.NpgsqlParameter("@sintYear", NpgsqlTypes.NpgsqlDbType.SmallInt)
                arParms(3).Value = _Year


                arParms(4) = New Npgsql.NpgsqlParameter("@tintquarter", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _Quarter

                arParms(5) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = DomainId


                SqlDAL.ExecuteNonQuery(connString, "USP_ForecastDelete", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetForRepByTeam() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _ForecastType

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_ForRepByTeam", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetForRepByTerritory() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _ForecastType

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_ForRepByTerritory", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetTeamsForForRep() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _ForecastType

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetForCastTeams", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageTeamsForForRept() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID


                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _ForecastType

                arParms(3) = New Npgsql.NpgsqlParameter("@strTeam", NpgsqlTypes.NpgsqlDbType.VarChar, 4000)
                arParms(3).Value = _strTeam

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageTeamForRept", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function UpdateForCastAmt() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numForecastAmount", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(0).Value = _ForecastAmt


                arParms(1) = New Npgsql.NpgsqlParameter("@numForecastID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _ForecastID

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateForecastAmt", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function


    End Class

End Namespace

