﻿'Created by Chintan Prajapati
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Net
Imports System.IO
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.WebAPI

    Public Class WebAPI
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _strSKU As String
        Public Property SKU() As String
            Get
                Return _strSKU
            End Get
            Set(ByVal value As String)
                _strSKU = value
            End Set
        End Property

        Private _WebApiId As Integer

        Public Property WebApiId() As Integer
            Get
                Return _WebApiId
            End Get
            Set(ByVal value As Integer)
                _WebApiId = value
            End Set
        End Property

        Private _UserContactID As Long
        Public Property UserContactID() As Long
            Get
                Return _UserContactID
            End Get
            Set(ByVal value As Long)
                _UserContactID = value
            End Set
        End Property

        Private _FlagItemImport As String
        Public Property FlagItemImport() As String
            Get
                Return _FlagItemImport
            End Get
            Set(ByVal value As String)
                _FlagItemImport = value
            End Set
        End Property


        Private _ContactId As Long
        Public Property ContactId() As Long
            Get
                Return _ContactId
            End Get
            Set(ByVal value As Long)
                _ContactId = value
            End Set
        End Property

        Private _Status As Boolean
        Public Property Status() As Boolean
            Get
                Return _Status
            End Get
            Set(ByVal value As Boolean)
                _Status = value
            End Set
        End Property

        Private _CallLimitPerDay As Integer
        Public Property CallLimitPerDay() As Integer
            Get
                Return _CallLimitPerDay
            End Get
            Set(ByVal value As Integer)
                _CallLimitPerDay = value
            End Set
        End Property

        Private _EndDate As Date
        Public Property EndDate() As Date
            Get
                Return _EndDate
            End Get
            Set(ByVal value As Date)
                _EndDate = value
            End Set
        End Property

        Private _EnableAPI As Boolean
        Public Property EnableAPI() As Boolean
            Get
                Return _EnableAPI
            End Get
            Set(ByVal value As Boolean)
                _EnableAPI = value
            End Set
        End Property

        Private _EnableItemUpdate As Boolean
        Public Property EnableItemUpdate() As Boolean
            Get
                Return _EnableItemUpdate
            End Get
            Set(ByVal value As Boolean)
                _EnableItemUpdate = value
            End Set
        End Property

        Private _EnableInventoryUpdate As Boolean
        Public Property EnableInventoryUpdate() As Boolean
            Get
                Return _EnableInventoryUpdate
            End Get
            Set(ByVal value As Boolean)
                _EnableInventoryUpdate = value
            End Set
        End Property

        Private _EnableOrderImport As Boolean
        Public Property EnableOrderImport() As Boolean
            Get
                Return _EnableOrderImport
            End Get
            Set(ByVal value As Boolean)
                _EnableOrderImport = value
            End Set
        End Property


        Private _EnableTrackingUpdate As Boolean
        Public Property EnableTrackingUpdate() As Boolean
            Get
                Return _EnableTrackingUpdate
            End Get
            Set(ByVal value As Boolean)
                _EnableTrackingUpdate = value
            End Set
        End Property


        Private _APIURL As String
        Public Property APIURL() As String
            Get
                Return _APIURL
            End Get
            Set(ByVal value As String)
                _APIURL = value
            End Set
        End Property

        Private _UserKey As String
        Public Property UserKey() As String
            Get
                Return _UserKey
            End Get
            Set(ByVal value As String)
                _UserKey = value
            End Set
        End Property

        Private _SectionKey As String
        Public Property SectionKey() As String
            Get
                Return _SectionKey
            End Get
            Set(ByVal value As String)
                _SectionKey = value
            End Set
        End Property

        Private _Token As String
        Public Property Token() As String
            Get
                Return _Token
            End Get
            Set(ByVal value As String)
                _Token = value
            End Set
        End Property

        Private _WareHouseID As Long
        Public Property WareHouseID() As Long
            Get
                Return _WareHouseID
            End Get
            Set(ByVal value As Long)
                _WareHouseID = value
            End Set
        End Property


        Private _COGSChartAcntId As Long
        Public Property COGSChartAcntId() As Long
            Get
                Return _COGSChartAcntId
            End Get
            Set(ByVal value As Long)
                _COGSChartAcntId = value
            End Set
        End Property

        Private _AssetChartAcntId As Long
        Public Property AssetChartAcntId() As Long
            Get
                Return _AssetChartAcntId
            End Get
            Set(ByVal value As Long)
                _AssetChartAcntId = value
            End Set
        End Property

        Private _IncomeChartAcntId As Long
        Public Property IncomeChartAcntId() As Long
            Get
                Return _IncomeChartAcntId
            End Get
            Set(ByVal value As Long)
                _IncomeChartAcntId = value
            End Set
        End Property


        Private _DateCreated As String
        Public Property DateCreated() As String
            Get
                Return _DateCreated
            End Get
            Set(ByVal value As String)
                _DateCreated = value
            End Set
        End Property

        Private _DateModified As String
        Public Property DateModified() As String
            Get
                Return _DateModified
            End Get
            Set(ByVal value As String)
                _DateModified = value
            End Set
        End Property
        Private _str As String
        Public Property str() As String
            Get
                Return _str
            End Get
            Set(ByVal Value As String)
                _str = Value
            End Set
        End Property
        Private _Value1 As String
        Public Property Value1() As String
            Get
                Return _Value1
            End Get
            Set(ByVal value As String)
                _Value1 = value
            End Set
        End Property
        Private _Value2 As String
        Public Property Value2() As String
            Get
                Return _Value2
            End Get
            Set(ByVal value As String)
                _Value2 = value
            End Set
        End Property
        Private _Value3 As String
        Public Property Value3() As String
            Get
                Return _Value3
            End Get
            Set(ByVal value As String)
                _Value3 = value
            End Set
        End Property
        Private _Value4 As String
        Public Property Value4() As String
            Get
                Return _Value4
            End Get
            Set(ByVal value As String)
                _Value4 = value
            End Set
        End Property
        Private _Value5 As String
        Public Property Value5() As String
            Get
                Return _Value5
            End Get
            Set(ByVal value As String)
                _Value5 = value
            End Set
        End Property
        Private _Value6 As String
        Public Property Value6() As String
            Get
                Return _Value6
            End Get
            Set(ByVal value As String)
                _Value6 = value
            End Set
        End Property
        Private _Value7 As String
        Public Property Value7() As String
            Get
                Return _Value7
            End Get
            Set(ByVal value As String)
                _Value7 = value
            End Set
        End Property
        Private _Value8 As String
        Public Property Value8() As String
            Get
                Return _Value8
            End Get
            Set(ByVal value As String)
                _Value8 = value
            End Set
        End Property
        Private _Value9 As String
        Public Property Value9() As String
            Get
                Return _Value9
            End Get
            Set(ByVal value As String)
                _Value9 = value
            End Set
        End Property
        Private _Value10 As String
        Public Property Value10() As String
            Get
                Return _Value10
            End Get
            Set(ByVal value As String)
                _Value10 = value
            End Set
        End Property
        Private _Value11 As String
        Public Property Value11() As String
            Get
                Return _Value11
            End Get
            Set(ByVal value As String)
                _Value11 = value
            End Set
        End Property

        Private _Value12 As String
        Public Property Value12() As String
            Get
                Return _Value12
            End Get
            Set(ByVal value As String)
                _Value12 = value
            End Set
        End Property

        Private _OppId As Long
        Public Property OppId() As Long
            Get
                Return _OppId
            End Get
            Set(ByVal value As Long)
                _OppId = value
            End Set
        End Property

        Private _vcAPIOppId As String
        Public Property vcAPIOppId() As String
            Get
                Return _vcAPIOppId
            End Get
            Set(ByVal value As String)
                _vcAPIOppId = value
            End Set
        End Property

        Private _vcApiOppItemId As String
        Public Property vcApiOppItemId() As String
            Get
                Return _vcApiOppItemId
            End Get
            Set(ByVal value As String)
                _vcApiOppItemId = value
            End Set
        End Property


        Private _ItemCode As Long
        Public Property ItemCode() As Long
            Get
                Return _ItemCode
            End Get
            Set(ByVal value As Long)
                _ItemCode = value
            End Set
        End Property

        Private _Product_Id As String
        Public Property Product_Id() As String
            Get
                Return _Product_Id
            End Get
            Set(ByVal value As String)
                _Product_Id = value
            End Set
        End Property

        Private _WebApiOrdDetailId As Integer
        Public Property WebApiOrdDetailId() As Integer
            Get
                Return _WebApiOrdDetailId
            End Get
            Set(ByVal value As Integer)
                _WebApiOrdDetailId = value
            End Set
        End Property

        Private _WebApiOrderItemId As Integer
        Public Property WebApiOrderItemId() As Integer
            Get
                Return _WebApiOrderItemId
            End Get
            Set(ByVal value As Integer)
                _WebApiOrderItemId = value
            End Set
        End Property


        Private _WebApiDetailId As Integer
        Public Property WebApiDetailId() As Integer
            Get
                Return _WebApiDetailId
            End Get
            Set(ByVal value As Integer)
                _WebApiDetailId = value
            End Set
        End Property

        Private _API_OrderStatus As Integer
        Public Property API_OrderStatus() As Integer
            Get
                Return _API_OrderStatus
            End Get
            Set(ByVal value As Integer)
                _API_OrderStatus = value
            End Set
        End Property

        Private _API_OrderId As String
        Public Property API_OrderId() As String
            Get
                Return _API_OrderId
            End Get
            Set(ByVal value As String)
                _API_OrderId = value
            End Set
        End Property

        Private _WebApiOrdReportId As Integer
        Public Property WebApiOrdReportId() As Integer
            Get
                Return _WebApiOrdReportId
            End Get
            Set(ByVal value As Integer)
                _WebApiOrdReportId = value
            End Set
        End Property

        Private _API_ReportStatus As Integer
        Public Property API_ReportStatus() As Integer
            Get
                Return _API_ReportStatus
            End Get
            Set(ByVal value As Integer)
                _API_ReportStatus = value
            End Set
        End Property

        Private _API_OrderReportId As String
        Public Property API_OrderReportId() As String
            Get
                Return _API_OrderReportId
            End Get
            Set(ByVal value As String)
                _API_OrderReportId = value
            End Set
        End Property


        Private _RStatus As Integer
        Public Property RStatus() As Integer
            Get
                Return _RStatus
            End Get
            Set(ByVal value As Integer)
                _RStatus = value
            End Set
        End Property

        Private _RelationshipID As Long

        Public Property RelationshipID() As Long
            Get
                Return _RelationshipID
            End Get
            Set(ByVal value As Long)
                _RelationshipID = value
            End Set
        End Property
        Private _ProfileID As Long
        Public Property ProfileID() As Long
            Get
                Return _ProfileID
            End Get
            Set(ByVal value As Long)
                _ProfileID = value
            End Set
        End Property
        Private _ExpenseAccountId As Long
        Public Property ExpenseAccountId() As Long
            Get
                Return _ExpenseAccountId
            End Get
            Set(ByVal value As Long)
                _ExpenseAccountId = value
            End Set
        End Property
        Private _SalesTaxItemMapping As Long
        Public Property SalesTaxItemMapping() As Long
            Get
                Return _SalesTaxItemMapping
            End Get
            Set(ByVal value As Long)
                _SalesTaxItemMapping = value
            End Set
        End Property

        Private _DiscountItemMapping As Long
        Public Property DiscountItemMapping() As Long
            Get
                Return _DiscountItemMapping
            End Get
            Set(ByVal value As Long)
                _DiscountItemMapping = value
            End Set
        End Property

        Private _UnShipmentOrderStatus As Long
        Public Property UnShipmentOrderStatus() As Long
            Get
                Return _UnShipmentOrderStatus
            End Get
            Set(ByVal value As Long)
                _UnShipmentOrderStatus = value
            End Set
        End Property

        Private _OrderStatus As Long
        Public Property OrderStatus() As Long
            Get
                Return _OrderStatus
            End Get
            Set(ByVal value As Long)
                _OrderStatus = value
            End Set
        End Property
        Private _AssignTo As Long
        Public Property AssignTo() As Long
            Get
                Return _AssignTo
            End Get
            Set(ByVal value As Long)
                _AssignTo = value
            End Set
        End Property
        Private _RecordOwner As Long
        Public Property RecordOwner() As Long
            Get
                Return _RecordOwner
            End Get
            Set(ByVal value As Long)
                _RecordOwner = value
            End Set
        End Property
        Private _BizDocID As Long
        Public Property BizDocID() As Long
            Get
                Return _BizDocID
            End Get
            Set(ByVal value As Long)
                _BizDocID = value
            End Set
        End Property

        Private _BizDocStatusID As Long
        Public Property BizDocStatusID() As Long
            Get
                Return _BizDocStatusID
            End Get
            Set(ByVal value As Long)
                _BizDocStatusID = value
            End Set
        End Property

        Private _BizDocItemId As Long
        Public Property BizDocItemId() As Long
            Get
                Return _BizDocItemId
            End Get
            Set(ByVal value As Long)
                _BizDocItemId = value
            End Set
        End Property

        Private _OppItemId As Long
        Public Property OppItemId() As Long
            Get
                Return _OppItemId
            End Get
            Set(ByVal value As Long)
                _OppItemId = value
            End Set
        End Property


        Private _WebApiReportTypeId As Short
        Public Property WebApiReportTypeId() As Short
            Get
                Return _WebApiReportTypeId
            End Get
            Set(ByVal value As Short)
                _WebApiReportTypeId = value
            End Set
        End Property

        Private _Mode As Short
        Public Property Mode() As Short
            Get
                Return _Mode
            End Get
            Set(ByVal value As Short)
                _Mode = value
            End Set
        End Property

        Private _ImportApiOrderReqId As Long
        Public Property ImportApiOrderReqId() As Long
            Get
                Return _ImportApiOrderReqId
            End Get
            Set(ByVal value As Long)
                _ImportApiOrderReqId = value
            End Set
        End Property

        Private _OrdersFromDate As DateTime
        Public Property OrdersFromDate() As DateTime
            Get
                Return _OrdersFromDate
            End Get
            Set(ByVal value As DateTime)
                _OrdersFromDate = value
            End Set
        End Property

        Private _OrdersToDate As DateTime
        Public Property OrdersToDate() As DateTime
            Get
                Return _OrdersToDate
            End Get
            Set(ByVal value As DateTime)
                _OrdersToDate = value
            End Set
        End Property

        Private _OrderImportReqType As Short
        Public Property OrderImportReqType() As Short
            Get
                Return _OrderImportReqType
            End Get
            Set(ByVal value As Short)
                _OrderImportReqType = value
            End Set
        End Property

        Private _FbaBizDocID As Long
        Public Property FbaBizDocID() As Long
            Get
                Return _FbaBizDocID
            End Get
            Set(ByVal value As Long)
                _FbaBizDocID = value
            End Set
        End Property

        Private _FbaBizDocStatusID As Long
        Public Property FbaBizDocStatusID() As Long
            Get
                Return _FbaBizDocStatusID
            End Get
            Set(ByVal value As Long)
                _FbaBizDocStatusID = value
            End Set
        End Property


        Public Function GetWebApi() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@WebApiID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = _WebApiId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _Mode

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetWebAPI", arParms).Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        'Public Function ManageWebAPIDetails() As Boolean
        '    Try
        '        Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
        '        Dim getconnection As New GetConnection
        '        Dim connString As String = getconnection.GetConnectionString
        '        Dim ds As New DataSet

        '        arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(0).Value = DomainID

        '        arParms(1) = New Npgsql.NpgsqlParameter("@numWebApiId", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(1).Value = _WebApiId

        '        arParms(2) = New Npgsql.NpgsqlParameter("@bitStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(2).Value = _Status

        '        'arParms(1) = New Npgsql.NpgsqlParameter("@str", NpgsqlTypes.NpgsqlDbType.Text)
        '        'arParms(1).Value = _str

        '        Return SqlDAL.ExecuteNonQuery(connString, "USP_ManageWebAPIDetails", arParms)

        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function

        Public Function AddAPIopportunity() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@WebApiId", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _WebApiId

                arParms(2) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _OppId

                arParms(3) = New Npgsql.NpgsqlParameter("@vcAPIOppId", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(3).Value = _vcAPIOppId

                arParms(4) = New Npgsql.NpgsqlParameter("@numCreatedby", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = UserCntID

                SqlDAL.ExecuteNonQuery(connString, "USP_AddAPIOpportunity", arParms)
                Return True

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function AddItemAPILinking() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@intWebApiId", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _WebApiId

                arParms(2) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ItemCode

                arParms(3) = New Npgsql.NpgsqlParameter("@vcApiItemId", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(3).Value = _Product_Id

                arParms(4) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = UserCntID

                arParms(5) = New Npgsql.NpgsqlParameter("@vcSKU", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(5).Value = _strSKU

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageItemAPILinking", arParms)
                Return True

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function CheckDuplicateAPIOpportunity() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@WebApiId", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _WebApiId

                arParms(2) = New Npgsql.NpgsqlParameter("@vcAPIOppId", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(2).Value = _vcAPIOppId

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                If CInt(SqlDAL.ExecuteScalar(connString, "USP_CheckDuplicateAPIOpportunity", arParms)) > 0 Then
                    Return False
                Else
                    Return True
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetAPIDetails() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetAPIDetails", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function


        Public Function SaveAPISettings() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@WebApiId", _WebApiId, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@vcValue1", _Value1, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcValue2", _Value2, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcValue3", _Value3, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcValue4", _Value4, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcValue5", _Value5, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcValue6", _Value6, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcValue7", _Value7, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcValue8", _Value8, NpgsqlTypes.NpgsqlDbType.VarChar, 1000))

                    .Add(SqlDAL.Add_Parameter("@vcValue9", _Value9, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcValue10", _Value10, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcValue11", _Value11, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcValue12", _Value12, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@bitEnableAPI", _EnableAPI, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@numBizDocId", _BizDocID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numBizDocStatusId", _BizDocStatusID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numOrderStatus", _OrderStatus, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numFBABizDocId", _FbaBizDocID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numFBABizDocStatusId", _FbaBizDocStatusID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numUnShipmentOrderStatus", _UnShipmentOrderStatus, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numRecordOwner", _RecordOwner, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numAssignTo", _AssignTo, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numWareHouseID", _WareHouseID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numRelationshipId", _RelationshipID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numProfileId", _ProfileID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numExpenseAccountId", _ExpenseAccountId, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDiscountItemMapping", _DiscountItemMapping, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numSalesTaxItemMapping", _SalesTaxItemMapping, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@bitEnableItemUpdate", _EnableItemUpdate, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@bitEnableOrderImport", _EnableOrderImport, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@bitEnableTrackingUpdate", _EnableTrackingUpdate, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@bitEnableInventoryUpdate", _EnableInventoryUpdate, NpgsqlTypes.NpgsqlDbType.Bit))

                End With

                Dim objParam() As Object = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_SaveAPISettings", objParam, False)

                Return True

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        ''' <param name="bitMode">1 for OrdersLastUpdatedDate,0 for ProductsLastUpdatedDate</param>
        Public Function UpdateAPISettingsDate(ByVal tintMode As Integer) As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@WebApiId", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = _WebApiId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = tintMode

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateAPISettingDate", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetResponse(ByVal strRequestURL As String) As String
            Dim url As String = strRequestURL
            'Create GET Request
            Dim req As HttpWebRequest = DirectCast(WebRequest.Create(url), HttpWebRequest)
            Dim proxy As String = Nothing
            req.Method = "GET"
            req.ContentType = "application/x-www-form-urlencoded"
            req.Proxy = New WebProxy(proxy, True)
            ' ignore for local addresses
            req.CookieContainer = New CookieContainer()
            ' enable cookies
            Dim res As HttpWebResponse = DirectCast(req.GetResponse(), HttpWebResponse)

            Dim resst As Stream = res.GetResponseStream()
            Dim sr As New StreamReader(resst)
            'Read the Resonse
            Dim response As String = sr.ReadToEnd()
            Return response
        End Function
        Public Function GetItemsApi() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@WebApiID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _WebApiId

                arParms(2) = New Npgsql.NpgsqlParameter("@CreatedDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = If(_DateCreated = "", Nothing, Convert.ToDateTime(_DateCreated))

                arParms(3) = New Npgsql.NpgsqlParameter("@ModifiedDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(3).Value = If(_DateModified = "", Nothing, Convert.ToDateTime(_DateModified))

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetItems_API", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetItemsCOAApi() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@WebApiID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _WebApiId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetItemsCOA_API", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetItemsInventoryApi() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@WebApiID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _WebApiId

                arParms(2) = New Npgsql.NpgsqlParameter("@ModifiedDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = If(_DateModified = "", Nothing, Convert.ToDateTime(_DateModified))

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetItemsInventory_API", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function UrlEncode(ByVal input As String) As String
            Try
                Return System.Web.HttpUtility.UrlEncode(input)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function UrlDecode(ByVal input As String) As String
            Try
                Return System.Web.HttpUtility.UrlDecode(input)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Sub WriteMessage(ByVal Message As String, ByVal LogFilePath As String)
            Dim TimeStamp As String = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString() + " --- "
            Dim FileName As String = LogFilePath + "ErrorLog_" + DateTime.Now.ToString("ddMMyyyy") + ".txt"

            Dim nTries As Integer = 0
            While True
                Try
                    nTries = nTries + 1
                    Dim sw As New StreamWriter(FileName, True)
                    sw.WriteLine(TimeStamp + Message)
                    sw.Flush()
                    sw.Close()
                    sw = Nothing
                    Exit Sub
                Catch ex As Exception
                    If nTries >= 5 Then
                        Return
                    End If
                    Threading.Thread.Sleep(500)
                End Try

            End While


        End Sub

        Public Function ManageAPIOrderDetails() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numWebApiOrdDetailId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _WebApiOrdDetailId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcWebApiOrderId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = _API_OrderId

                arParms(3) = New Npgsql.NpgsqlParameter("@tintStatus", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _API_OrderStatus

                arParms(4) = New Npgsql.NpgsqlParameter("@numWebApiId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _WebApiId

                arParms(5) = New Npgsql.NpgsqlParameter("@numOppid", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _OppId


                SqlDAL.ExecuteNonQuery(connString, "USP_ManageWebAPIOrderDetails", arParms)
                Return True

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Function ManageAPIImportOrderRequest() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numImportApiOrderReqId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ImportApiOrderReqId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numWebApiId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _WebApiId

                arParms(3) = New Npgsql.NpgsqlParameter("@vcWebApiOrderId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = _API_OrderId

                arParms(4) = New Npgsql.NpgsqlParameter("@bitIsActive", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(4).Value = _Status

                arParms(5) = New Npgsql.NpgsqlParameter("@tintRequestType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _OrderImportReqType

                arParms(6) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(6).Value = _OrdersFromDate

                arParms(7) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(7).Value = _OrdersToDate

                arParms(8) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = _OppId

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageAPIImportOrderRequest", arParms)
                Return True

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Function GetTopWebApiOrderID() As String
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numWebApiId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _WebApiId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteScalar(connString, "USP_GetTopWebApiOrderID", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageWebAPIOrderReports() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOrdReportId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _WebApiOrdReportId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numWebApiId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _WebApiId

                arParms(3) = New Npgsql.NpgsqlParameter("@vcWebApiOrderReportId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = _API_OrderReportId

                arParms(4) = New Npgsql.NpgsqlParameter("@tintStatus", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _API_ReportStatus

                arParms(5) = New Npgsql.NpgsqlParameter("@RStatus", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _RStatus

                arParms(6) = New Npgsql.NpgsqlParameter("@ReportTypeId", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(6).Value = _WebApiReportTypeId

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageWebAPIOrderReports", arParms)
                Return True

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Function ManageWebApiItemImport() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numWebApiID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _WebApiId

                arParms(2) = New Npgsql.NpgsqlParameter("@UserContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _UserContactID

                arParms(3) = New Npgsql.NpgsqlParameter("@FlagItemImport", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = _FlagItemImport

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageApiItemImport", arParms)
                Return True

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Function GetTopAPIOrderReport() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numWebApiId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _WebApiId

                arParms(2) = New Npgsql.NpgsqlParameter("@Mode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _Mode

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetTopAPIOrderReport", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetTrackingDetails() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numWebApiId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _WebApiId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "usp_GetTrackingDetails", arParms).Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function ManageApiOrderItems() As Boolean
            Try

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@WebApiOrderItemId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _WebApiOrderItemId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numWebApiId", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _WebApiId

                arParms(3) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _OppId

                arParms(4) = New Npgsql.NpgsqlParameter("@numOppBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _OppItemId

                arParms(5) = New Npgsql.NpgsqlParameter("@vcAPIOppId", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(5).Value = _vcAPIOppId

                arParms(6) = New Npgsql.NpgsqlParameter("@vcApiOppItemId", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(6).Value = vcApiOppItemId

                arParms(7) = New Npgsql.NpgsqlParameter("@tintStatus", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(7).Value = _RStatus

                arParms(8) = New Npgsql.NpgsqlParameter("@numCreatedby", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = _RecordOwner

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageAPIOppItem", arParms)
                Return True

            Catch ex As Exception
                Throw ex

            End Try

        End Function

        Public Function GetApiOpportunities() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numWebApiId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _WebApiId

                arParms(2) = New Npgsql.NpgsqlParameter("@Mode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _Mode

                arParms(3) = New Npgsql.NpgsqlParameter("@vcApiOrderID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _vcAPIOppId

                arParms(4) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(4).Value = _OrdersFromDate

                arParms(5) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(5).Value = _OrdersToDate

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetApiOpportunities", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCflList(ByVal DomainID As Long, ByVal LocID As Short) As DataSet
            Dim dsCustFld As DataSet = Nothing
            Try
                Dim objCusField As Admin.CustomFields = New Admin.CustomFields()
                objCusField.DomainID = DomainID
                objCusField.locId = LocID
                dsCustFld = objCusField.GetCustomFieldListAndData()

            Catch ex As Exception
                dsCustFld = Nothing
                Throw ex
            End Try

            Return dsCustFld
        End Function
      

    End Class

End Namespace
