﻿'Created by Chintan Prajapati
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.IO

Namespace BACRM.BusinessLogic.WebAPI

    Public Class WebService
        Inherits BACRM.BusinessLogic.CBusinessBase
        Private _WebServiceId As Long
        Public Property WebServiceId() As Long
            Get
                Return _WebServiceId
            End Get
            Set(ByVal value As Long)
                _WebServiceId = value
            End Set
        End Property
        Private _Token As String
        Public Property Token() As String
            Get
                Return _Token
            End Get
            Set(ByVal value As String)
                _Token = value
            End Set
        End Property
        'Private DomainId As Long
        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal value As Long)
        '        DomainId = value
        '    End Set
        'End Property
        'Private UserCntID As Long
        'Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal value As Long)
        '        UserCntID = value
        '    End Set
        'End Property

        Public Function GetWebService() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@vcToken", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(0).Value = _Token

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GETWebService", arParms).Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function ManageWebService() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As New DataSet
                arParms(0) = New Npgsql.NpgsqlParameter("@WebServiceId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _WebServiceId

                arParms(1) = New Npgsql.NpgsqlParameter("@vcToken", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(1).Value = _Token

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainId

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = UserCntID

                Return CBool(SqlDAL.ExecuteNonQuery(connString, "USP_ManageWebService", arParms))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ValidateToken() As Boolean
            Try
                Dim dtWS As DataTable = GetWebService()
                If dtWS.Rows.Count > 0 Then
                    DomainId = Long.Parse(dtWS.Rows(0)("numDomainId").ToString())
                    UserCntID = GetRoutingRulesUserCntID("", "")
                End If

                If DomainId > 0 And UserCntID > 0 Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Function GetRoutingRulesUserCntID(ByVal dbFieldName As String, ByVal FieldValue As String) As Long
            Try
                Dim objAutoRoutRles As New Admin.AutoRoutingRules
                Dim dtAutoRoutingRules As New DataTable
                Dim drAutoRow As DataRow
                Dim ds As New DataSet

                objAutoRoutRles.DomainID = DomainID
                If dtAutoRoutingRules.Columns.Count = 0 Then
                    dtAutoRoutingRules.TableName = "Table"
                    dtAutoRoutingRules.Columns.Add("vcDbColumnName")
                    dtAutoRoutingRules.Columns.Add("vcDbColumnText")
                End If

                dtAutoRoutingRules.Rows.Clear()
                drAutoRow = dtAutoRoutingRules.NewRow
                drAutoRow("vcDbColumnName") = dbFieldName
                drAutoRow("vcDbColumnText") = FieldValue
                dtAutoRoutingRules.Rows.Add(drAutoRow)

                ds.Tables.Add(dtAutoRoutingRules)
                objAutoRoutRles.strValues = ds.GetXml
                ds.Tables.Remove(ds.Tables(0))

                Return objAutoRoutRles.GetRecordOwner()
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace

