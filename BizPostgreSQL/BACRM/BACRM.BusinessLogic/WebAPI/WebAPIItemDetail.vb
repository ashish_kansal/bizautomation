﻿
Namespace BACRM.BusinessLogic.WebAPI
    ''' <summary>
    ''' Class holds properties which needs to be mapped to amazon api from Biz item
    ''' </summary>
    ''' <remarks></remarks>
    Public Class WebAPIItemDetail
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _WarrantyDescription As String
        Public Property WarrantyDescription() As String
            Get
                Return _WarrantyDescription
            End Get
            Set(ByVal value As String)
                _WarrantyDescription = BACRM.BusinessLogic.Common.CCommon.Truncate(value, 2000) 'value 'BACRM.BusinessLogic.Common.CCommon.Truncate(value, 2000)
            End Set
        End Property

        Private _SearchTerms As String
        Public Property SearchTerms() As String
            Get
                Return _SearchTerms
            End Get
            Set(ByVal value As String)
                _SearchTerms = value 'BACRM.BusinessLogic.Common.CCommon.Truncate(value, 250)
            End Set
        End Property

        Private _WeightUOM As String
        Public Property WeightUOM() As String
            Get
                Return _WeightUOM
            End Get
            Set(ByVal value As String)
                _WeightUOM = value
            End Set
        End Property

        Private _DimensionUOM As String
        Public Property DimensionUOM() As String
            Get
                Return _DimensionUOM
            End Get
            Set(ByVal value As String)
                _DimensionUOM = value
            End Set
        End Property

        Private _Weight As String
        Public Property Weight() As String
            Get
                Return _Weight
            End Get
            Set(ByVal value As String)
                _Weight = value
            End Set
        End Property

        Private _Length As String
        Public Property Length() As String
            Get
                Return _Length
            End Get
            Set(ByVal value As String)
                _Length = value
            End Set
        End Property

        Private _Width As String
        Public Property Width() As String
            Get
                Return _Width
            End Get
            Set(ByVal value As String)
                _Width = value
            End Set
        End Property

        Private _Height As String
        Public Property Height() As String
            Get
                Return _Height
            End Get
            Set(ByVal value As String)
                _Height = value
            End Set
        End Property

        Private _CategoryId As String
        Public Property CategoryId() As String
            Get
                Return _CategoryId
            End Get
            Set(ByVal value As String)
                _CategoryId = value
            End Set
        End Property

        Private _EbayListingStyles As String
        Public Property EbayListingStyles() As String
            Get
                Return _EbayListingStyles
            End Get
            Set(ByVal value As String)
                _EbayListingStyles = value
            End Set
        End Property
        Private _EbayListingDuration As String
        Public Property EbayListingDuration() As String
            Get
                Return _EbayListingDuration
            End Get
            Set(ByVal value As String)
                _EbayListingDuration = value
            End Set
        End Property

        Private _StandardShippingCharge As String
        Public Property StandardShippingCharge() As String
            Get
                Return _StandardShippingCharge
            End Get
            Set(ByVal value As String)
                _StandardShippingCharge = value
            End Set
        End Property

        Private _ExpressShippingCharge As String
        Public Property ExpressShippingCharge() As String
            Get
                Return _ExpressShippingCharge
            End Get
            Set(ByVal value As String)
                _ExpressShippingCharge = value
            End Set
        End Property

        Private _GoogleProductType As String

        Public Property GoogleProductType() As String
            Get
                Return _GoogleProductType
            End Get
            Set(ByVal value As String)
                _GoogleProductType = value
            End Set
        End Property

        Private _GoogleProductCategory As String

        Public Property GoogleProductCategory() As String
            Get
                Return _GoogleProductCategory
            End Get
            Set(ByVal value As String)
                _GoogleProductCategory = value
            End Set
        End Property

        Private _GoogleProductURL As String
        Public Property GoogleProductURL() As String
            Get
                Return _GoogleProductURL
            End Get
            Set(ByVal value As String)
                _GoogleProductURL = value
            End Set
        End Property

        Private _ProductImageLink As String
        Public Property ProductImageLink() As String
            Get
                Return _ProductImageLink
            End Get
            Set(ByVal value As String)
                _ProductImageLink = value
            End Set
        End Property


        Private _MagentoProductAttributeSet As Integer
        Public Property MagentoProductAttributeSet() As Integer
            Get
                Return _MagentoProductAttributeSet
            End Get
            Set(ByVal value As Integer)
                _MagentoProductAttributeSet = value
            End Set
        End Property

        Private _MagentoProductType As String
        Public Property MagentoProductType() As String
            Get
                Return _MagentoProductType
            End Get
            Set(ByVal value As String)
                _MagentoProductType = value
            End Set
        End Property

        Private _lngItemCode As Long
        Public Property ItemCode() As Long
            Get
                Return _lngItemCode
            End Get
            Set(ByVal value As Long)
                _lngItemCode = value
            End Set
        End Property

        Private _Brand As String
        Public Property Brand() As String
            Get
                Return _Brand
            End Get
            Set(ByVal value As String)
                _Brand = value
            End Set
        End Property

        Private _ItemType As String
        Public Property ItemType() As String
            Get
                Return _ItemType
            End Get
            Set(ByVal value As String)
                _ItemType = value
            End Set
        End Property

    End Class
End Namespace