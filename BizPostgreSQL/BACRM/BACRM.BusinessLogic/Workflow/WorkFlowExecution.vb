﻿
'Author:Sachin Sadhu'
'Date:14thFeb2014
'Purpose:Execute Workflow rule actions 
'Future Enhancements: SMS alerts is under BETA release.need Domain wise Twilio registration


Option Explicit On
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents
Imports System.Web.HttpContext
Imports System.Web.UI.WebControls
Imports System.Web
Imports BACRM.BusinessLogic.Opportunities
Imports System.Web.UI.HtmlControls
Imports System.Drawing
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Account
'Imports RestSharp
Imports Twilio
Imports System.Text
Imports Winnovative.WnvHtmlConvert
Imports Winnovative
Imports System.Configuration


Namespace BACRM.BusinessLogic.Workflow
    Public Class WorkFlowExecution
        Inherits BACRM.BusinessLogic.CBusinessBase

        Sub WFExecution()
            Try
                Dim objWF As New Workflow
                Dim dtQueue As DataTable = objWF.GetWorkFlowQueue()

                If dtQueue.Rows.Count > 0 Then
                    For Each dr As DataRow In dtQueue.Rows

                        'If Pending execution then change Status to InProcess
                        If CCommon.ToShort(dr("tintProcessStatus")) = enmWFQueueProcessStatus.PendingExecution Then
                            objWF = New Workflow
                            With objWF
                                .WFQueueID = CCommon.ToLong(dr("numWFQueueID"))
                                .DomainID = CCommon.ToLong(dr("numDomainID"))
                                .tintProcessStatus = CCommon.ToShort(enmWFQueueProcessStatus.InProcess)
                                .ManageWorkFlowQueue(1)
                            End With
                        End If

                        objWF.WFID = CCommon.ToLong(dr("numWFID"))
                        objWF.DomainID = CCommon.ToLong(dr("numDomainID"))

                        Dim ds As DataSet
                        ds = objWF.GetWorkFlowMasterDetail

                        Dim objWFObject As New WorkFlowObject

                        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            Dim dtWFList As DataTable = ds.Tables(0)
                            Dim dtWFTriggerFields As DataTable = ds.Tables(1)
                            Dim dtWFConditions As DataTable = ds.Tables(2)
                            Dim dtWFActions As DataTable = ds.Tables(3)
                            Dim dtWFActionUpdateFields As DataTable = ds.Tables(4)

                            If CCommon.ToShort(dtWFList.Rows(0)("numFormID")) = 71 Then
                                objWF.FormID = 49
                            Else
                                objWF.FormID = CCommon.ToShort(dtWFList.Rows(0)("numFormID"))
                            End If

                            objWF.RecordID = CCommon.ToLong(dr("numRecordID"))
                            objWF.intDays = CCommon.ToInteger(dtWFList.Rows(0)("intDays"))
                            objWF.intActionOn = CCommon.ToInteger(dtWFList.Rows(0)("intActionOn"))
                            objWF.vcDateField = CCommon.ToString(dtWFList.Rows(0)("vcDateField"))

                            objWF.numFieldID = CCommon.ToLong(dtWFList.Rows(0)("numFieldID"))
                            objWF.boolCustom = CCommon.ToBool(dtWFList.Rows(0)("bitCustom"))

                            'added By:Sachin ;for Test
                            'purpose: problem in getting 
                            DomainID = CCommon.ToLong(dr("numDomainID"))
                            'end of code test

                            Dim strTablename As String = Nothing
                            Dim moduleId As Integer = 0
                            Select Case objWF.FormID
                                Case 68 'Organization
                                    strTablename = "DivisionMaster"
                                    moduleId = 1
                                Case 70 'Opportunities & Orders                                  
                                    strTablename = "OpportunityMaster"
                                    moduleId = 2
                                Case 49 'BizDocs
                                    strTablename = "OpportunityBizDocs"
                                    moduleId = 8
                                Case 94 'Business Process
                                    strTablename = "StagePercentageDetails"
                                Case 69 'Contacts
                                    strTablename = "AdditionalContactsInformation"
                                    moduleId = 11
                                Case 73 'Projects
                                    strTablename = "ProjectsMaster"
                                    moduleId = 4
                                Case 72 'Cases
                                    strTablename = "Cases"
                                    moduleId = 5
                                Case 124 'Tickler/Action Items
                                    strTablename = "Communication"
                                    moduleId = 6
                                Case 148 'Website Page
                                    strTablename = "DivisionMaster"
                                    moduleId = 1
                            End Select
                            Dim dtFieldsList As DataTable
                            Dim originalFormID As Long = objWF.FormID
                            If originalFormID = 148 Then
                                objWF.FormID = 68
                            End If
                            dtFieldsList = objWF.GetWorkFlowFormFieldMaster()
                            If originalFormID = 148 Then
                                objWF.FormID = 148
                            End If

                            'Check Condition
                            Dim boolCondition As Boolean = True
                            If dtWFConditions.Rows.Count > 0 Then
                                Dim Sql As New System.Text.StringBuilder
                                Dim SqlFrom As New System.Text.StringBuilder
                                Dim SqlCondition As New System.Text.StringBuilder
                                Dim CFW_Fld_Values_Opp As New ArrayList() 'Orders(70)
                                Dim CFW_Fld_Values As New ArrayList() 'Organization(68)
                                Dim CFW_FLD_Values_Pro As New ArrayList() 'Projects(73)
                                Dim CFW_FLD_Values_Case As New ArrayList() 'Cases(72)
                                Dim CFW_FLD_Values_Cont As New ArrayList() 'Contacts(69)
                                SqlFrom.Append("select count(*) FROM")
                                SqlCondition.Append(" WHERE 1=1")

                                Select Case objWF.FormID
                                    Case 68 'Organization
                                        If (objWF.vcDateField = "") Then
                                            SqlFrom.Append(" DivisionMaster left join AdditionalContactsInformation  on DivisionMaster.numDivisionID=AdditionalContactsInformation.numDivisionId left join CompanyInfo  on DivisionMaster.numCompanyId= CompanyInfo.numCompanyId ")
                                            strTablename = "DivisionMaster"
                                            SqlCondition.Append(" AND DivisionMaster.numDomainId = $1 AND DivisionMaster.numDivisionId = $2")
                                        Else
                                            SqlFrom.Append(" DivisionMaster_TempDateFields left join AdditionalContactsInformation  on DivisionMaster_TempDateFields.numDivisionID=AdditionalContactsInformation.numDivisionId ")
                                            strTablename = "DivisionMaster_TempDateFields"
                                            SqlCondition.Append(" AND DivisionMaster_TempDateFields.numDomainId = $1 AND DivisionMaster_TempDateFields.numDivisionId = $2")
                                        End If

                                    Case 70 'Opportunities & Orders
                                        'FROM OpportunityMaster o inner join DivisionMaster D on d.numdivisionID=o.numDivisionId inner join CompanyInfo c on  c.numCompanyId=d.numCompanyId where  o.numOppId=$2"
                                        If (objWF.vcDateField = "") Then
                                            SqlFrom.Append(" OpportunityMaster left JOIN OpportunityBizDocs ON OpportunityMaster.numOppId = OpportunityBizDocs.numOppId  left join DivisionMaster  on DivisionMaster.numdivisionID=OpportunityMaster.numDivisionId inner join CompanyInfo  on  CompanyInfo.numCompanyId=DivisionMaster.numCompanyId  left join StagePercentageDetails on StagePercentageDetails.numOppID=OpportunityMaster.numOppId")
                                            strTablename = "OpportunityMaster"
                                            SqlCondition.Append(" AND OpportunityMaster.numDomainId = $1 AND OpportunityMaster.numOppID = $2")
                                        Else
                                            SqlFrom.Append(" OpportunityMaster_TempDateFields left JOIN OpportunityBizDocs ON OpportunityMaster_TempDateFields.numOppId = OpportunityBizDocs.numOppId  left join DivisionMaster  on DivisionMaster.numdivisionID=OpportunityMaster_TempDateFields.numDivisionId inner join CompanyInfo  on  CompanyInfo.numCompanyId=DivisionMaster.numCompanyId  left join StagePercentageDetails on StagePercentageDetails.numOppID=OpportunityMaster_TempDateFields.numOppId")
                                            strTablename = "OpportunityMaster_TempDateFields"
                                            SqlCondition.Append(" AND OpportunityMaster_TempDateFields.numDomainId = $1 AND OpportunityMaster_TempDateFields.numOppID = $2")
                                        End If

                                    Case 49 'BizDocs
                                        If (objWF.vcDateField = "") Then
                                            SqlFrom.Append(" OpportunityBizDocs INNER JOIN OpportunityMaster ON OpportunityMaster.numOppId = OpportunityBizDocs.numOppId inner join DivisionMaster  on DivisionMaster.numdivisionID=OpportunityMaster.numDivisionId inner join CompanyInfo  on  CompanyInfo.numCompanyId=DivisionMaster.numCompanyId")
                                            strTablename = "OpportunityBizDocs"
                                            SqlCondition.Append(" AND OpportunityMaster.numDomainId = $1 AND OpportunityBizDocs.numOppBizDocsId = $2")
                                        Else
                                            SqlFrom.Append(" OpportunityBizDocs_TempDateFields INNER JOIN OpportunityMaster ON OpportunityMaster.numOppId = OpportunityBizDocs.numOppId inner join DivisionMaster  on DivisionMaster.numdivisionID=OpportunityMaster.numDivisionId inner join CompanyInfo  on  CompanyInfo.numCompanyId=DivisionMaster.numCompanyId")
                                            strTablename = "OpportunityBizDocs_TempDateFields"
                                            SqlCondition.Append(" AND OpportunityMaster.numDomainId = $1 AND OpportunityBizDocs_TempDateFields.numOppBizDocsId = $2")
                                        End If

                                    Case 94 'Business Process
                                        If (objWF.vcDateField = "") Then
                                            SqlFrom.Append(" StagePercentageDetails Inner join ProjectProgress On (ProjectProgress.numProId=StagePercentageDetails.numProjectID OR ProjectProgress.numOppId=StagePercentageDetails.numOppId) ")
                                            strTablename = "StagePercentageDetails"
                                            SqlCondition.Append(" AND StagePercentageDetails.numDomainId = $1 AND StagePercentageDetails.numStageDetailsId = $2")
                                        Else
                                            SqlFrom.Append(" StagePercentageDetails_TempDateFields Inner join ProjectProgress On (ProjectProgress.numProId=StagePercentageDetails.numProjectID OR ProjectProgress.numOppId=StagePercentageDetails.numOppId) ")
                                            strTablename = "StagePercentageDetails_TempDateFields"
                                            SqlCondition.Append(" AND StagePercentageDetails_TempDateFields.numDomainId = $1 AND StagePercentageDetails_TempDateFields.numStageDetailsId = $2")
                                        End If

                                    Case 69 'Contacts
                                        If (objWF.vcDateField = "") Then
                                            SqlFrom.Append(" AdditionalContactsInformation left join DivisionMaster  on DivisionMaster.numdivisionID=AdditionalContactsInformation.numDivisionId left join CompanyInfo  on  CompanyInfo.numCompanyId=DivisionMaster.numCompanyId")
                                            strTablename = "AdditionalContactsInformation"
                                            SqlCondition.Append(" AND AdditionalContactsInformation.numDomainId = $1 AND AdditionalContactsInformation.numContactId = $2")
                                        Else
                                            SqlFrom.Append(" AdditionalContactsInformation_TempDateFields left join DivisionMaster  on DivisionMaster.numdivisionID=AdditionalContactsInformation.numDivisionId left join CompanyInfo  on  CompanyInfo.numCompanyId=DivisionMaster.numCompanyId")
                                            strTablename = "AdditionalContactsInformation_TempDateFields"
                                            SqlCondition.Append(" AND AdditionalContactsInformation_TempDateFields.numDomainId = $1 AND AdditionalContactsInformation_TempDateFields.numContactId = $2")
                                        End If

                                    Case 73 'Projects
                                        If (objWF.vcDateField = "") Then
                                            SqlFrom.Append(" ProjectsMaster left join ProjectProgress On ProjectProgress.numProId=ProjectsMaster_TempDateFields.numProId inner join DivisionMaster  on DivisionMaster.numdivisionID=ProjectsMaster.numDivisionId left join CompanyInfo  on  CompanyInfo.numCompanyId=DivisionMaster.numCompanyId  left join StagePercentageDetails on StagePercentageDetails.numProjectID=ProjectsMaster.numProId")
                                            strTablename = "ProjectsMaster"
                                            SqlCondition.Append(" AND ProjectsMaster.numDomainId = $1 AND ProjectsMaster.numProId = $2")
                                        Else
                                            SqlFrom.Append(" ProjectsMaster_TempDateFields left join ProjectProgress On ProjectProgress.numProId=ProjectsMaster.numProId inner join DivisionMaster  on DivisionMaster.numdivisionID=ProjectsMaster.numDivisionId left join CompanyInfo  on  CompanyInfo.numCompanyId=DivisionMaster.numCompanyId  left join StagePercentageDetails on StagePercentageDetails.numProjectID=ProjectsMaster.numProId")
                                            strTablename = "ProjectsMaster_TempDateFields"
                                            SqlCondition.Append(" AND ProjectsMaster_TempDateFields.numDomainId = $1 AND ProjectsMaster_TempDateFields.numProId = $2")
                                        End If

                                    Case 72 'Cases
                                        If (objWF.vcDateField = "") Then
                                            SqlFrom.Append(" Cases left join DivisionMaster  on DivisionMaster.numdivisionID=Cases.numDivisionId left join CompanyInfo  on  CompanyInfo.numCompanyId=DivisionMaster.numCompanyId left JOIN CaseOpportunities  on Cases.numCaseId=CaseOpportunities.numCaseId  left join OpportunityMaster  on OpportunityMaster.numOppId=CaseOpportunities.numOppId")
                                            strTablename = "Cases"
                                            SqlCondition.Append(" AND Cases.numDomainId = $1 AND Cases.numCaseId = $2")
                                        Else
                                            SqlFrom.Append(" Cases_TempDateFields left join DivisionMaster  on DivisionMaster.numdivisionID=Cases.numDivisionId left join CompanyInfo  on  CompanyInfo.numCompanyId=DivisionMaster.numCompanyId left JOIN CaseOpportunities  on Cases.numCaseId=CaseOpportunities.numCaseId  left join OpportunityMaster  on OpportunityMaster.numOppId=CaseOpportunities.numOppId")
                                            strTablename = "Cases_TempDateFields"
                                            SqlCondition.Append(" AND Cases_TempDateFields.numDomainId = $1 AND Cases_TempDateFields.numCaseId = $2")
                                        End If

                                    Case 124 'Tickler/Action Items
                                        If (objWF.vcDateField = "") Then
                                            SqlFrom.Append(" Communication left join DivisionMaster  on DivisionMaster.numdivisionID=Communication.numDivisionId left join CompanyInfo  on  CompanyInfo.numCompanyId=DivisionMaster.numCompanyId")
                                            strTablename = "Communication"
                                            SqlCondition.Append(" AND Communication.numDomainId = $1 AND Communication.numCommId = $2")
                                        Else
                                            SqlFrom.Append(" Communication_TempDateFields left join DivisionMaster  on DivisionMaster.numdivisionID=Communication.numDivisionId left join CompanyInfo  on  CompanyInfo.numCompanyId=DivisionMaster.numCompanyId")
                                            strTablename = "Communication_TempDateFields"
                                            SqlCondition.Append(" AND Communication_TempDateFields.numDomainId = $1 AND Communication_TempDateFields.numCommId = $2")
                                        End If
                                    Case 148 'Website Page
                                        SqlFrom.Append(" TrackingVisitorsDTL 
                                                        INNER JOIN 
	                                                        TrackingVisitorsHDR 
                                                        ON 
	                                                        TrackingVisitorsDTL.numTracVisitorsHDRID=TrackingVisitorsHDR.numTrackingID 
                                                        INNER JOIN
	                                                        DivisionMaster
                                                        ON
	                                                        TrackingVisitorsHDR.numDivisionID = DivisionMaster.numDivisionID
                                                        INNER JOIN
	                                                        CompanyInfo
                                                        ON
	                                                        DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
                                                        INNER JOIN 
                                                            AdditionalContactsInformation 
                                                        ON 
                                                            DivisionMaster.numDivisionID=AdditionalContactsInformation.numDivisionId")
                                        strTablename = "DivisionMaster"
                                        SqlCondition.Append(" AND TrackingVisitorsDTL.numTracVisitorsDTLID = $2 
                                                                AND TrackingVisitorsHDR.numDomainID=$1 
                                                                AND LOWER(TrackingVisitorsDTL.vcPageName) IN (SELECT LOWER(specs.""vcWebsitePage"") FROM json_to_recordset('" & CCommon.ToString(dtWFList.Rows(0)("vcWebsitePage")) & "') AS specs(""vcWebsitePage"" TEXT))
                                                                AND ((SELECT EmailHistory.dtReceivedOn FROM Correspondence INNER JOIN EmailHistory ON Correspondence.numEmailHistoryID=EmailHistory.numEmailHstrID WHERE Correspondence.numDomainID=$1 AND EmailHistory.vcTo ILIKE CONCAT('%',AdditionalContactsInformation.vcEmail,'%') ORDER BY EmailHistory.dtReceivedOn DESC LIMIT 1) IS NULL OR COALESCE(DATEDIFF('day',(SELECT EmailHistory.dtReceivedOn FROM Correspondence INNER JOIN EmailHistory ON Correspondence.numEmailHistoryID=EmailHistory.numEmailHstrID WHERE Correspondence.numDomainID=$1 AND EmailHistory.vcTo ILIKE CONCAT('%',AdditionalContactsInformation.vcEmail,'%') ORDER BY EmailHistory.dtReceivedOn DESC LIMIT 1),TIMEZONE('UTC',now())),0) > " & CCommon.ToInteger(dtWFList.Rows(0)("intWebsiteDays")) & ")")

                                End Select

                                Dim tempCondition As New List(Of String)

                                For Each drCFL As DataRow In dtWFConditions.Rows
                                    Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))
                                    If drFLCheck.Length > 0 Then
                                        Dim vcCondition As String = ConditionOperator(drCFL("vcFilterOperator"), drCFL("vcFilterValue").Trim(), drFLCheck(0)("vcAssociatedControlType"), drFLCheck(0)("vcFieldDataType"), CCommon.ToBool(drFLCheck(0)("bitCustom")))

                                        If drFLCheck(0)("vcOrigDbColumnName") = "tintOppType" Then
                                            Dim strValue() As String = drCFL("vcFilterValue").Trim().Split(",")
                                            Dim tempCondition1 As New List(Of String)

                                            For i As Integer = 0 To strValue.Length - 1
                                                Select Case strValue(i)
                                                    Case 1 'Sales Order
                                                        tempCondition1.Add(String.Format("({0}.tintOppType=1 and {0}.tintOppStatus=1)", drFLCheck(0)("vcLookBackTableName")))
                                                    Case 2 'Purchase Order
                                                        tempCondition1.Add(String.Format("({0}.tintOppType=2 and {0}.tintOppStatus=1)", drFLCheck(0)("vcLookBackTableName")))
                                                    Case 3 'Sales Opportunity
                                                        tempCondition1.Add(String.Format("({0}.tintOppType=1 and {0}.tintOppStatus=0)", drFLCheck(0)("vcLookBackTableName")))
                                                    Case 4 'Purchase Opportunity
                                                        tempCondition1.Add(String.Format("({0}.tintOppType=2 and {0}.tintOppStatus=0)", drFLCheck(0)("vcLookBackTableName")))
                                                End Select
                                            Next

                                            tempCondition.Add(String.Format("{0} (" & String.Join(" OR ", tempCondition1) & ")", drCFL("vcFilterANDOR")))
                                        ElseIf drFLCheck(0)("vcOrigDbColumnName") = "bitPaidInFull" Then
                                            Dim strValue() As String = drCFL("vcFilterValue").Trim().Split(",")
                                            Dim tempCondition1 As New List(Of String)

                                            For i As Integer = 0 To strValue.Length - 1
                                                If CCommon.ToBool(strValue(i)) Then
                                                    tempCondition1.Add("COALESCE((SELECT SUM(COALESCE(monAmountPaid,0)) FROM OpportunityBizDocs BD WHERE  BD.numOppId = OpportunityMaster.numOppId AND COALESCE(bitAuthoritativeBizDocs,0)=1),0) > COALESCE(OpportunityMaster.monDealAmount,0)")
                                                Else
                                                    tempCondition1.Add("COALESCE((SELECT SUM(COALESCE(monAmountPaid,0)) FROM OpportunityBizDocs BD WHERE  BD.numOppId = OpportunityMaster.numOppId AND COALESCE(bitAuthoritativeBizDocs,0)=1),0) < COALESCE(OpportunityMaster.monDealAmount,0)")
                                                End If
                                            Next

                                            tempCondition.Add(String.Format("{0} (" & String.Join(" OR ", tempCondition1) & ")", drCFL("vcFilterANDOR")))
                                        ElseIf drFLCheck(0)("vcOrigDbColumnName") = "vcInventoryStatus" Then
                                            Dim strValue() As String = drCFL("vcFilterValue").Trim().Split(",")
                                            Dim tempCondition1 As New List(Of String)

                                            For i As Integer = 0 To strValue.Length - 1
                                                Select Case strValue(i)
                                                    Case 1 'Not Applicable
                                                        tempCondition1.Add("CHARINDEX('Not Applicable', CheckOrderInventoryStatus(OpportunityMaster.numOppID,OpportunityMaster.numDomainID)) > 0")
                                                    Case 2 'Shipped
                                                        tempCondition1.Add("CHARINDEX('Shipped', CheckOrderInventoryStatus(OpportunityMaster.numOppID,OpportunityMaster.numDomainID)) > 0")
                                                    Case 3 'Back Order
                                                        tempCondition1.Add("CHARINDEX('BO', CheckOrderInventoryStatus(OpportunityMaster.numOppID,OpportunityMaster.numDomainID)) > 0 AND CHARINDEX('Shippable', CheckOrderInventoryStatus(OpportunityMaster.numOppID,OpportunityMaster.numDomainID)) = 0")
                                                    Case 4 'Shippable
                                                        tempCondition1.Add("CHARINDEX('Shippable', CheckOrderInventoryStatus(OpportunityMaster.numOppID,OpportunityMaster.numDomainID)) > 0")
                                                End Select
                                            Next

                                            tempCondition.Add(String.Format("{0} (" & String.Join(" OR ", tempCondition1) & ")", drCFL("vcFilterANDOR")))
                                        ElseIf drFLCheck(0)("vcOrigDbColumnName") = "bitPOFromSOOrSOFromPO" Then
                                            Dim strValue() As String = drCFL("vcFilterValue").Trim().Split(",")
                                            Dim tempCondition1 As New List(Of String)

                                            If Not strValue Is Nothing AndAlso strValue.Length > 0 Then

                                                Select Case drCFL("vcFilterOperator")
                                                    Case "eq"
                                                        If strValue(0) = "1" Then
                                                            tempCondition1.Add(String.Format("1 = (CASE WHEN (SELECT COUNT(*) FROM OpportunityLinking WHERE numChildOppID={0}.numOppID) > 0 THEN 1 ELSE 0 END)", drFLCheck(0)("vcLookBackTableName")))
                                                        ElseIf strValue(0) = "0" Then
                                                            tempCondition1.Add(String.Format("1 = (CASE WHEN (SELECT COUNT(*) FROM OpportunityLinking WHERE numChildOppID={0}.numOppID) = 0 THEN 1 ELSE 0 END)", drFLCheck(0)("vcLookBackTableName")))
                                                        End If
                                                    Case "ne"
                                                        If strValue(0) = "1" Then
                                                            tempCondition1.Add(String.Format("1 = (CASE WHEN (SELECT COUNT(*) FROM OpportunityLinking WHERE numChildOppID={0}.numOppID) = 0 THEN 1 ELSE 0 END)", drFLCheck(0)("vcLookBackTableName")))
                                                        ElseIf strValue(0) = "0" Then
                                                            tempCondition1.Add(String.Format("1 = (CASE WHEN (SELECT COUNT(*) FROM OpportunityLinking WHERE numChildOppID={0}.numOppID) > 0 THEN 1 ELSE 0 END)", drFLCheck(0)("vcLookBackTableName")))
                                                        End If
                                                End Select
                                            End If

                                            tempCondition.Add(String.Format("{0} (" & String.Join(" OR ", tempCondition1) & ")", drCFL("vcFilterANDOR")))
                                        ElseIf drFLCheck(0)("vcOrigDbColumnName") = "tintSource" Then
                                            Dim strValue() As String = drCFL("vcFilterValue").Trim().Split(",")
                                            Dim tempCondition1 As New List(Of String)

                                            For i As Integer = 0 To strValue.Length - 1
                                                Dim strValue1() As String = strValue(i).Split("~")
                                                If strValue1.Length = 2 Then
                                                    tempCondition1.Add(String.Format("({0}.tintSource={1} and {0}.tintSourceType={2})", drFLCheck(0)("vcLookBackTableName"), strValue1(0), strValue1(1)))
                                                End If
                                            Next

                                            tempCondition.Add(String.Format("{0} (" & String.Join(" OR ", tempCondition1) & ")", drCFL("vcFilterANDOR")))
                                        ElseIf objWF.FormID = 70 AndAlso drFLCheck(0)("vcOrigDbColumnName") = "monDealAmount" AndAlso drFLCheck(0)("vcLookBackTableName") = "OpportunityBizDocs" Then
                                            'added by sachin ||Compare Fields
                                            If drCFL("intCompare") = 1 Then 'comparing with Fields
                                                tempCondition.Add(String.Format("{3} COALESCE((SELECT SUM(COALESCE(BD.monDealAmount,0)) FROM   OpportunityBizDocs BD inner join OpportunityMaster on  BD.numOppId = OpportunityMaster.numOppId Where  OpportunityMaster.numOppID = $2 and COALESCE(BD.bitAuthoritativeBizDocs,0)=1),0) {2}  (COALESCE((SELECT SUM(COALESCE({0}.{4},0)) FROM   OpportunityBizDocs  inner join OpportunityMaster on  OpportunityBizDocs.numOppId = OpportunityMaster.numOppId  WHERE OpportunityMaster.numOppID = $2 and  COALESCE(OpportunityBizDocs.bitAuthoritativeBizDocs,0)=1),0))",
                                                                   drFLCheck(0)("vcLookBackTableName"), drFLCheck(0)("vcOrigDbColumnName"), vcCondition, drCFL("vcFilterANDOR"), drCFL("vcFilterValue")))

                                            Else
                                                tempCondition.Add(String.Format("{3} COALESCE((SELECT SUM(COALESCE(BD.monDealAmount,0)) FROM   OpportunityBizDocs BD inner join OpportunityMaster on  BD.numOppId = OpportunityMaster.numOppId Where OpportunityMaster.numOppID = $2 and COALESCE(BD.bitAuthoritativeBizDocs,0)=1),0) {2}",
                                                                   drFLCheck(0)("vcLookBackTableName"), drFLCheck(0)("vcOrigDbColumnName"), vcCondition, drCFL("vcFilterANDOR")))
                                            End If
                                        ElseIf objWF.FormID = 49 AndAlso drFLCheck(0)("vcOrigDbColumnName") = "monAmountPaid" AndAlso drFLCheck(0)("vcLookBackTableName") = "OpportunityBizDocs" Then
                                            If (drCFL("vcFilterValue").ToString = "1") Then
                                                tempCondition.Add(String.Format("{1} (COALESCE(OpportunityBizDocs.monDealAmount,0) > 0 AND COALESCE(OpportunityBizDocs.monAmountPaid,0) >= COALESCE(OpportunityBizDocs.monDealAmount,0))",
                                                                                                                 drFLCheck(0)("vcLookBackTableName"), drCFL("vcFilterANDOR")))
                                            ElseIf (drCFL("vcFilterValue").ToString = "2") Then
                                                tempCondition.Add(String.Format("{1} (COALESCE(OpportunityBizDocs.monDealAmount,0) > 0 AND COALESCE(OpportunityBizDocs.monAmountPaid,0) < COALESCE(OpportunityBizDocs.monDealAmount,0))",
                                                                                                                drFLCheck(0)("vcLookBackTableName"), drCFL("vcFilterANDOR")))
                                            ElseIf (drCFL("vcFilterValue").ToString = "3") Then
                                                tempCondition.Add(String.Format("{1} (COALESCE(OpportunityBizDocs.monDealAmount,0) > 0 AND COALESCE(OpportunityBizDocs.monAmountPaid,0) = 0)",
                                                                                                              drFLCheck(0)("vcLookBackTableName"), drCFL("vcFilterANDOR")))
                                            End If
                                        ElseIf objWF.FormID = 70 AndAlso drFLCheck(0)("vcOrigDbColumnName") = "monAmountPaid" AndAlso drFLCheck(0)("vcLookBackTableName") = "OpportunityBizDocs" Then
                                            'added by sachin ||Compare Fields
                                            If drCFL("intCompare") = 1 Then 'comparing with Fields
                                                tempCondition.Add(String.Format("{3} COALESCE((SELECT SUM(COALESCE(BD.monAmountPaid,0)) FROM   OpportunityBizDocs BD inner join OpportunityMaster on  BD.numOppId = OpportunityMaster.numOppId Where OpportunityMaster.numOppID = $2 and COALESCE(BD.bitAuthoritativeBizDocs,0)=1),0) {2}  (COALESCE((SELECT SUM(COALESCE({0}.{4},0)) FROM   OpportunityBizDocs   inner join OpportunityMaster on  OpportunityBizDocs.numOppId = OpportunityMaster.numOppId  WHERE  OpportunityMaster.numOppID = $2 and COALESCE(OpportunityBizDocs.bitAuthoritativeBizDocs,0)=1),0))",
                                                                   drFLCheck(0)("vcLookBackTableName"), drFLCheck(0)("vcOrigDbColumnName"), vcCondition, drCFL("vcFilterANDOR"), drCFL("vcFilterValue")))

                                            Else
                                                If (drCFL("vcFilterValue").ToString = "0") Then
                                                    tempCondition.Add(String.Format("{1} 1 = (SELECT COALESCE((SELECT SUM(COALESCE(monAmountPaid,0)) FROM  OpportunityBizDocs BD WHERE  BD.numOppId = OpportunityMaster.numOppId AND OpportunityMaster.numOppID = $2 AND COALESCE(bitAuthoritativeBizDocs,0)=1),0))",
                                                                                                                      drFLCheck(0)("vcLookBackTableName"), drCFL("vcFilterANDOR")))
                                                ElseIf (drCFL("vcFilterValue").ToString = "1") Then
                                                    tempCondition.Add(String.Format("{1} 1 = (SELECT (CASE WHEN (SELECT (CASE WHEN (SUM(COALESCE(monDealAmount,0)) = SUM(COALESCE(monAmountPaid,0)) AND SUM(COALESCE(monAmountPaid,0)) <> 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE BD.numOppId =  OpportunityMaster.numOppId AND OpportunityMaster.numOppID = $2 AND COALESCE(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END))",
                                                                                                                     drFLCheck(0)("vcLookBackTableName"), drCFL("vcFilterANDOR")))
                                                ElseIf (drCFL("vcFilterValue").ToString = "2") Then
                                                    tempCondition.Add(String.Format("{1} 1 = (SELECT CASE WHEN (SELECT (CASE WHEN ((SUM(COALESCE(monDealAmount,0)) - SUM(COALESCE(monAmountPaid,0))) > 0 AND SUM(COALESCE(monAmountPaid,0)) <> 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE  BD.numOppId = OpportunityMaster.numOppId AND OpportunityMaster.numOppID = $2 AND COALESCE(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END)",
                                                                                                                    drFLCheck(0)("vcLookBackTableName"), drCFL("vcFilterANDOR")))
                                                ElseIf (drCFL("vcFilterValue").ToString = "3") Then
                                                    tempCondition.Add(String.Format("{1} 1 = (SELECT CASE WHEN (SELECT (CASE WHEN (SUM(COALESCE(monAmountPaid,0)) = 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE  BD.numOppId = OpportunityMaster.numOppId AND OpportunityMaster.numOppID = $2 AND COALESCE(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END)",
                                                                                                                  drFLCheck(0)("vcLookBackTableName"), drCFL("vcFilterANDOR")))
                                                End If
                                                'tempCondition.Add(String.Format("{3} COALESCE((SELECT SUM(COALESCE(BD.monAmountPaid,0)) FROM   OpportunityBizDocs BD inner join OpportunityMaster on  BD.numOppId = OpportunityMaster.numOppId Where  OpportunityMaster.numOppID = $2 and COALESCE(BD.bitAuthoritativeBizDocs,0)=1),0) {2}",
                                                '                   drFLCheck(0)("vcLookBackTableName"), drFLCheck(0)("vcOrigDbColumnName"), vcCondition, drCFL("vcFilterANDOR")))
                                            End If

                                        ElseIf objWF.FormID = 70 AndAlso drFLCheck(0)("vcLookBackTableName") = "OpportunityBizDocs" Then
                                            tempCondition.Add(String.Format("{3} {0}.{1}{2}",
                                                                    drFLCheck(0)("vcLookBackTableName"), drFLCheck(0)("vcOrigDbColumnName"), vcCondition, drCFL("vcFilterANDOR")))

                                        ElseIf drFLCheck(0)("vcOrigDbColumnName") = "numState" Then
                                            If drFLCheck(0)("vcDbColumnName") = "numBillState" Then
                                                tempCondition.Add(String.Format("{3} COALESCE((SELECT numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,1::SMALLINT) LIMIT 1),0) {2}",
                                                                   drFLCheck(0)("vcLookBackTableName"), drFLCheck(0)("vcOrigDbColumnName"), vcCondition, drCFL("vcFilterANDOR")))
                                            ElseIf drFLCheck(0)("vcDbColumnName") = "numShipState" Then
                                                tempCondition.Add(String.Format("{3} COALESCE((SELECT numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2::SMALLINT) LIMIT 1),0) {2}",
                                                                   drFLCheck(0)("vcLookBackTableName"), drFLCheck(0)("vcOrigDbColumnName"), vcCondition, drCFL("vcFilterANDOR")))
                                            End If

                                        ElseIf CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                            'added by sachin ||Compare Fields
                                            If drCFL("intCompare") = 1 Then 'comparing with Fields

                                                tempCondition.Add(String.Format("{3} {0}.{1}{2}{0}.{4}",
                                                                    drFLCheck(0)("vcLookBackTableName"), drFLCheck(0)("vcOrigDbColumnName"), vcCondition, drCFL("vcFilterANDOR"), drCFL("vcFilterValue")))
                                            ElseIf drFLCheck(0)("vcAssociatedControlType") = "DateField" Then

                                                If (drCFL("vcFilterOperator").ToString = "Before") Then
                                                    vcCondition = ">= TIMEZONE('UTC',now())::TIMESTAMP + make_interval(days => -1 * " & CCommon.ToInteger(drCFL("vcFilterValue")).ToString() & ")"
                                                ElseIf (drCFL("vcFilterOperator").ToString = "After") Then
                                                    vcCondition = "<= TIMEZONE('UTC',now())::TIMESTAMP + make_interval(days => -1 * " & CCommon.ToInteger(drCFL("vcFilterValue")).ToString() & ")"
                                                End If

                                                tempCondition.Add(String.Format("{3} FormatedDateFromDate({2}.""{0}"",$1){1}",
                                                                     drFLCheck(0)("vcOrigDbColumnName"), vcCondition, drFLCheck(0)("vcLookBackTableName"), drCFL("vcFilterANDOR")))

                                            Else
                                                tempCondition.Add(String.Format("{3} {0}.{1}{2}",
                                                                 drFLCheck(0)("vcLookBackTableName"), drFLCheck(0)("vcOrigDbColumnName"), vcCondition, drCFL("vcFilterANDOR")))
                                            End If
                                        Else

                                            If objWF.FormID = 70 Or objWF.FormID = 49 Then 'orders Or BizDocs 
                                                If CCommon.ToString(drFLCheck(0)("vcLookBackTableName")).ToUpper() = "CFW_FLD_VALUES" Then
                                                    CFW_Fld_Values.Add("""" & drFLCheck(0)("numFieldID") & """")
                                                Else
                                                    CFW_Fld_Values_Opp.Add("""" & drFLCheck(0)("numFieldID") & """")
                                                End If
                                            ElseIf objWF.FormID = 68 Or objWF.FormID = 148 Then 'Organization/contacts
                                                CFW_Fld_Values.Add("""" & drFLCheck(0)("numFieldID") & """")
                                            ElseIf objWF.FormID = 69 Then 'Contacts
                                                If CCommon.ToString(drFLCheck(0)("vcLookBackTableName")).ToUpper() = "CFW_FLD_VALUES" Then
                                                    CFW_Fld_Values.Add("""" & drFLCheck(0)("numFieldID") & """")
                                                Else
                                                    CFW_FLD_Values_Cont.Add("""" & drFLCheck(0)("numFieldID") & """")
                                                End If
                                            ElseIf objWF.FormID = 73 Then 'Projects
                                                If CCommon.ToString(drFLCheck(0)("vcLookBackTableName")).ToUpper() = "CFW_FLD_VALUES" Then
                                                    CFW_Fld_Values.Add("""" & drFLCheck(0)("numFieldID") & """")
                                                Else
                                                    CFW_FLD_Values_Pro.Add("""" & drFLCheck(0)("numFieldID") & """")
                                                End If
                                            ElseIf objWF.FormID = 72 Then 'cases
                                                If CCommon.ToString(drFLCheck(0)("vcLookBackTableName")).ToUpper() = "CFW_FLD_VALUES" Then
                                                    CFW_Fld_Values.Add("""" & drFLCheck(0)("numFieldID") & """")
                                                Else
                                                    CFW_FLD_Values_Case.Add("""" & drFLCheck(0)("numFieldID") & """")
                                                End If
                                            Else
                                                CFW_Fld_Values.Add("""" & drFLCheck(0)("numFieldID") & """")
                                            End If

                                            If drFLCheck(0)("vcAssociatedControlType") = "DateField" Then
                                                tempCondition.Add(String.Format("{3} FormatedDateFromDate({2}.""{0}"",$1){1}",
                                                                     drFLCheck(0)("numFieldID"), vcCondition, drFLCheck(0)("vcLookBackTableName"), drCFL("vcFilterANDOR")))

                                            Else
                                                tempCondition.Add(String.Format("{3} {2}.""{0}""{1}",
                                                                      drFLCheck(0)("numFieldID"), vcCondition, drFLCheck(0)("vcLookBackTableName"), drCFL("vcFilterANDOR")))
                                            End If
                                        End If
                                    End If
                                Next

                                SqlCondition.Append(" AND (" & String.Join(" ", tempCondition) & ")")

                                If objWF.FormID = 70 Then 'orders
                                    If CFW_Fld_Values_Opp.Count > 0 Then
                                        Dim strCFW_Fld_Values_Opp As String() = New String(CFW_Fld_Values_Opp.Count - 1) {}
                                        CFW_Fld_Values_Opp.CopyTo(strCFW_Fld_Values_Opp)

                                        SqlFrom.Append(" Left Join (Select * From crosstab('select t2.RecId,t1.fld_id,MAX(t2.Fld_Value) as Fld_Value FROM CFW_Fld_Master t1 JOIN CFW_Fld_Values_Opp As t2 On t1.Fld_ID = t2.Fld_ID where (t1.Grp_id = 2 Or t1.Grp_id = 6) and t1.numDomainID=$1 AND t1.fld_id IN (" & String.Join(",", strCFW_Fld_Values_Opp.Distinct()).Replace("""", "") & ") AND t2.RecId=$2 AND t1.fld_type <> ''Link'' group by t2.RecId,t1.fld_id order by 1') AS TEMPCFW(RecId NUMERIC(18,0)," & String.Join(" TEXT,", strCFW_Fld_Values_Opp.Distinct()) & " TEXT))")
                                        SqlFrom.Append(" as CFW_Fld_Values_Opp on CFW_Fld_Values_Opp.RecId = OpportunityMaster.numOppid ")
                                    End If

                                    If CFW_Fld_Values.Count > 0 Then
                                        Dim strCFW_Fld_Values_Opp As String() = New String(CFW_Fld_Values.Count - 1) {}
                                        CFW_Fld_Values.CopyTo(strCFW_Fld_Values_Opp)

                                        SqlFrom.Append(" LEFT JOIN LATERAL(Select * From crosstab('select t2.RecId,t1.fld_id,MAX(t2.Fld_Value) as Fld_Value FROM CFW_Fld_Master t1 JOIN CFW_Fld_Values AS t2 ON t1.Fld_ID = t2.Fld_ID where (t1.Grp_id = 1 or t1.Grp_id = 4) and t1.numDomainID=$1 AND t1.fld_id IN (" & String.Join(",", strCFW_Fld_Values_Opp.Distinct()).Replace("""", "") & ") AND t2.RecId=DivisionMaster.numDivisionID AND t1.fld_type <> ''Link'' group by t2.RecId,t1.fld_id order by 1') AS TEMPCFW(RecId NUMERIC(18,0)," & String.Join(" TEXT,", strCFW_Fld_Values_Opp.Distinct()) & " TEXT))")
                                        SqlFrom.Append(" as CFW_Fld_Values ON TRUE ")
                                    End If
                                ElseIf objWF.FormID = 49 Then 'BizDocs 
                                    If CFW_Fld_Values_Opp.Count > 0 Then
                                        Dim strCFW_Fld_Values_Opp As String() = New String(CFW_Fld_Values_Opp.Count - 1) {}
                                        CFW_Fld_Values_Opp.CopyTo(strCFW_Fld_Values_Opp)

                                        SqlFrom.Append(" LEFT JOIN LATERAL(Select * From crosstab('select t2.RecId,t1.fld_id,MAX(t2.Fld_Value) as Fld_Value FROM CFW_Fld_Master t1 JOIN CFW_Fld_Values_Opp AS t2 ON t1.Fld_ID = t2.Fld_ID where (t1.Grp_id = 2 or t1.Grp_id = 6) and t1.numDomainID=$1 AND t1.fld_id IN (" & String.Join(",", strCFW_Fld_Values_Opp.Distinct()).Replace("""", "") & ") AND t2.RecId=OpportunityMaster.numOppId AND t1.fld_type <> ''Link'' group by t2.RecId,t1.fld_id order by 1') AS TEMPCFW(RecId NUMERIC(18,0)," & String.Join(" TEXT,", strCFW_Fld_Values_Opp.Distinct()) & " TEXT))")
                                        SqlFrom.Append(" as CFW_Fld_Values_Opp  ON TRUE")
                                    End If

                                    If CFW_Fld_Values.Count > 0 Then
                                        Dim strCFW_Fld_Values_Opp As String() = New String(CFW_Fld_Values.Count - 1) {}
                                        CFW_Fld_Values.CopyTo(strCFW_Fld_Values_Opp)

                                        SqlFrom.Append(" LEFT JOIN LATERAL(Select * From crosstab('select t2.RecId,t1.fld_id,MAX(t2.Fld_Value) as Fld_Value FROM CFW_Fld_Master t1 JOIN CFW_Fld_Values AS t2 ON t1.Fld_ID = t2.Fld_ID where (t1.Grp_id = 1 or t1.Grp_id = 4) and t1.numDomainID=$1 AND t1.fld_id IN (" & String.Join(",", strCFW_Fld_Values_Opp.Distinct()).Replace("""", "") & ") AND t2.RecId=DivisionMaster.numDivisionID AND t1.fld_type <> ''Link'' group by t2.RecId,t1.fld_id order by 1') AS TEMPCFW(RecId NUMERIC(18,0)," & String.Join(" TEXT,", strCFW_Fld_Values_Opp.Distinct()) & " TEXT))")
                                        SqlFrom.Append(" as CFW_Fld_Values ON TRUE ")
                                    End If
                                ElseIf objWF.FormID = 68 Or objWF.FormID = 148 Then 'Organization
                                    If CFW_Fld_Values.Count > 0 Then
                                        Dim strCFW_Fld_Values_Opp As String() = New String(CFW_Fld_Values.Count - 1) {}
                                        CFW_Fld_Values.CopyTo(strCFW_Fld_Values_Opp)

                                        SqlFrom.Append(" LEFT JOIN (Select * From crosstab('select t2.RecId,t1.fld_id,MAX(t2.Fld_Value) as Fld_Value FROM CFW_Fld_Master t1 JOIN CFW_Fld_Values AS t2 ON t1.Fld_ID = t2.Fld_ID where (t1.Grp_id = 1 or t1.Grp_id = 4) and t1.numDomainID=$1 AND t1.fld_id IN (" & String.Join(",", strCFW_Fld_Values_Opp.Distinct()).Replace("""", "") & ") AND t2.RecId=$2 AND t1.fld_type <> ''Link'' group by t2.RecId,t1.fld_id order by 1') AS TEMPCFW(RecId NUMERIC(18,0)," & String.Join(" TEXT,", strCFW_Fld_Values_Opp.Distinct()) & " TEXT))")
                                        SqlFrom.Append(" as CFW_Fld_Values on CFW_Fld_Values.RecId = DivisionMaster.numDivisionID ")
                                    End If
                                ElseIf objWF.FormID = 69 Then 'contacts
                                    If CFW_FLD_Values_Cont.Count > 0 Then
                                        Dim strCFW_Fld_Values_Opp As String() = New String(CFW_FLD_Values_Cont.Count - 1) {}
                                        CFW_FLD_Values_Cont.CopyTo(strCFW_Fld_Values_Opp)

                                        SqlFrom.Append(" LEFT JOIN (Select * From crosstab('select t2.RecId,t1.fld_id,MAX(t2.Fld_Value) as Fld_Value FROM CFW_Fld_Master t1 JOIN CFW_FLD_Values_Cont AS t2 ON t1.Fld_ID = t2.Fld_ID where (t1.Grp_id = 1 or t1.Grp_id = 4) and t1.numDomainID=$1 AND t1.fld_id IN (" & String.Join(",", strCFW_Fld_Values_Opp.Distinct()).Replace("""", "") & ") AND t2.RecId=$2 AND t1.fld_type <> ''Link'' group by t2.RecId,t1.fld_id order by 1') AS TEMPCFW(RecId NUMERIC(18,0)," & String.Join(" TEXT,", strCFW_Fld_Values_Opp.Distinct()) & " TEXT))")
                                        SqlFrom.Append(" as CFW_FLD_Values_Cont on CFW_FLD_Values_Cont.RecId = DivisionMaster.numDivisionID ")
                                    End If

                                    If CFW_Fld_Values.Count > 0 Then
                                        Dim strCFW_Fld_Values_Opp As String() = New String(CFW_Fld_Values.Count - 1) {}
                                        CFW_Fld_Values.CopyTo(strCFW_Fld_Values_Opp)


                                        SqlFrom.Append(" LEFT JOIN LATERAL (Select * From crosstab('select t2.RecId,t1.fld_id,MAX(t2.Fld_Value) as Fld_Value FROM CFW_Fld_Values AS t2 ON t1.Fld_ID = t2.Fld_ID where (t1.Grp_id = 1 or t1.Grp_id = 4) and t1.numDomainID=$1 AND t1.fld_id IN (" & String.Join(",", strCFW_Fld_Values_Opp.Distinct()).Replace("""", "") & ") AND t2.RecId=DivisionMaster.numDivisionID AND t1.fld_type <> ''Link'' group by t2.RecId,t1.fld_id order by 1') AS TEMPCFW(RecId NUMERIC(18,0)," & String.Join(" TEXT,", strCFW_Fld_Values_Opp.Distinct()) & " TEXT))")
                                        SqlFrom.Append(" as CFW_Fld_Values ON TRUE ")
                                    End If
                                ElseIf objWF.FormID = 73 Then 'Projects
                                    If CFW_FLD_Values_Pro.Count > 0 Then
                                        Dim strCFW_Fld_Values_Opp As String() = New String(CFW_FLD_Values_Pro.Count - 1) {}
                                        CFW_FLD_Values_Pro.CopyTo(strCFW_Fld_Values_Opp)

                                        SqlFrom.Append(" LEFT JOIN (Select * From crosstab('select t2.RecId,t1.fld_id,MAX(t2.Fld_Value) as Fld_Value FROM CFW_Fld_Master t1 JOIN CFW_FLD_Values_Pro AS t2 ON t1.Fld_ID = t2.Fld_ID where (t1.Grp_id = 11 or t1.Grp_id = 6) and t1.numDomainID=$1 AND t1.fld_id IN (" & String.Join(",", strCFW_Fld_Values_Opp.Distinct()).Replace("""", "") & ") AND t2.RecId=$2 AND t1.fld_type <> ''Link'' group by t2.RecId,t1.fld_id order by 1') AS TEMPCFW(RecId NUMERIC(18,0)," & String.Join(" TEXT,", strCFW_Fld_Values_Opp.Distinct()) & " TEXT))")
                                        SqlFrom.Append(" as CFW_FLD_Values_Pro on CFW_FLD_Values_Pro.RecId = ProjectsMaster.numProId ")
                                    End If

                                    If CFW_Fld_Values.Count > 0 Then
                                        Dim strCFW_Fld_Values_Opp As String() = New String(CFW_Fld_Values.Count - 1) {}
                                        CFW_Fld_Values.CopyTo(strCFW_Fld_Values_Opp)

                                        SqlFrom.Append(" LEFT JOIN LATERAL (Select * From crosstab('select t2.RecId,t1.fld_id,MAX(t2.Fld_Value) as Fld_Value FROM CFW_Fld_Master t1 JOIN CFW_Fld_Values AS t2 ON t1.Fld_ID = t2.Fld_ID where (t1.Grp_id = 1 or t1.Grp_id = 4) and t1.numDomainID=$1 AND t1.fld_id IN (" & String.Join(",", strCFW_Fld_Values_Opp.Distinct()).Replace("""", "") & ") AND t2.RecId=DivisionMaster.numDivisionID AND t1.fld_type <> ''Link'' group by t2.RecId,t1.fld_id order by 1') AS TEMPCFW(RecId NUMERIC(18,0)," & String.Join(" TEXT,", strCFW_Fld_Values_Opp.Distinct()) & " TEXT))")
                                        SqlFrom.Append(" as CFW_Fld_Values ON TRUE ")
                                    End If
                                ElseIf objWF.FormID = 72 Then 'cases
                                    If CFW_FLD_Values_Case.Count > 0 Then
                                        Dim strCFW_Fld_Values_Opp As String() = New String(CFW_FLD_Values_Case.Count - 1) {}
                                        CFW_FLD_Values_Case.CopyTo(strCFW_Fld_Values_Opp)

                                        SqlFrom.Append(" LEFT JOIN (Select * From crosstab('select t2.RecId,t1.fld_id,MAX(t2.Fld_Value) as Fld_Value FROM CFW_Fld_Master t1 JOIN CFW_FLD_Values_Case AS t2 ON t1.Fld_ID = t2.Fld_ID where (t1.Grp_id = 3 or t1.Grp_id = 6) and t1.numDomainID=$1 AND t1.fld_id IN (" & String.Join(",", strCFW_Fld_Values_Opp.Distinct()).Replace("""", "") & ") AND t2.RecId=$2 AND t1.fld_type <> ''Link'' group by t2.RecId,t1.fld_id order by 1') AS TEMPCFW(RecId NUMERIC(18,0)," & String.Join(" TEXT,", strCFW_Fld_Values_Opp.Distinct()) & " TEXT))")
                                        SqlFrom.Append(" as CFW_FLD_Values_Case on CFW_FLD_Values_Case.RecId = Cases.numCaseId ")
                                    End If

                                    If CFW_Fld_Values.Count > 0 Then
                                        Dim strCFW_Fld_Values_Opp As String() = New String(CFW_Fld_Values.Count - 1) {}
                                        CFW_Fld_Values.CopyTo(strCFW_Fld_Values_Opp)

                                        SqlFrom.Append(" LEFT JOIN LATERAL (Select * From crosstab('select t2.RecId,t1.fld_id,MAX(t2.Fld_Value) as Fld_Value FROM CFW_Fld_Master t1 JOIN CFW_Fld_Values AS t2 ON t1.Fld_ID = t2.Fld_ID where (t1.Grp_id = 1 or t1.Grp_id = 4) and t1.numDomainID=$1 AND t1.fld_id IN (" & String.Join(",", strCFW_Fld_Values_Opp.Distinct()).Replace("""", "") & ") AND t2.RecId=DivisionMaster.numDivisionID AND t1.fld_type <> ''Link'' group by t2.RecId,t1.fld_id order by 1') AS TEMPCFW(RecId NUMERIC(18,0)," & String.Join(" TEXT,", strCFW_Fld_Values_Opp.Distinct()) & " TEXT))")
                                        SqlFrom.Append(" as CFW_Fld_Values ON TRUE ")
                                    End If
                                Else
                                    If CFW_Fld_Values_Opp.Count > 0 Then
                                        Dim strCFW_Fld_Values_Opp As String() = New String(CFW_Fld_Values_Opp.Count - 1) {}
                                        CFW_Fld_Values_Opp.CopyTo(strCFW_Fld_Values_Opp)

                                        SqlFrom.Append(" LEFT JOIN (Select * From crosstab('select t2.RecId,t1.fld_id,MAX(t2.Fld_Value) as Fld_Value FROM CFW_Fld_Master t1 JOIN CFW_Fld_Values_Opp AS t2 ON t1.Fld_ID = t2.Fld_ID where (t1.Grp_id = 2 or t1.Grp_id = 6) and t1.numDomainID=$1 AND t1.fld_id IN (" & String.Join(",", strCFW_Fld_Values_Opp.Distinct()).Replace("""", "") & ") AND t2.RecId=$2 AND t1.fld_type <> ''Link'' group by t2.RecId,t1.fld_id order by 1') AS TEMPCFW(RecId NUMERIC(18,0)," & String.Join(" TEXT,", strCFW_Fld_Values_Opp.Distinct()) & " TEXT))")
                                        SqlFrom.Append(" as CFW_Fld_Values_Opp on CFW_Fld_Values_Opp.RecId = OpportunityMaster.numOppid ")
                                    End If
                                End If

                                Sql.Append(SqlFrom)
                                Sql.Append(SqlCondition)
                                objWF.textQuery = Sql.ToString()

                                Try
                                    boolCondition = objWF.WFConditionQueryExecute(1)
                                Catch ex As Exception
                                    Dim objWF1 As New Workflow
                                    With objWF1
                                        .WFQueueID = CCommon.ToLong(dr("numWFQueueID"))
                                        .WFID = objWF.WFID
                                        .WFDescription = "Error occured:" & ex.Message & " Query:" & Sql.ToString()
                                        .ManageWorkFlowQueue(2, _bitSuccess:=False)

                                        Continue For
                                    End With
                                End Try
                            End If

                            'Execute Action
                            If objWF.intDays = 0 Then 'Instant action
                                If boolCondition Then
                                    Dim numContactId, numDivisionId, numRecOwner, numAssignedTo, numInternalPM, numExternalPM, numNextAssignedTo, numOppIDPC, numStageIDPC, numOppBizDOCIDPC, numProjectID, numCaseID, numCommID As Long
                                    Dim numPhone As String
                                    Dim dtRecordData As DataTable = objWF.GetWFRecordData

                                    numContactId = CCommon.ToLong(dtRecordData.Rows(0)("numContactId"))
                                    numDivisionId = CCommon.ToLong(dtRecordData.Rows(0)("numDivisionId"))
                                    numRecOwner = CCommon.ToLong(dtRecordData.Rows(0)("numRecOwner"))
                                    numAssignedTo = CCommon.ToLong(dtRecordData.Rows(0)("numAssignedTo"))
                                    numInternalPM = CCommon.ToLong(dtRecordData.Rows(0)("numInternalPM"))
                                    numExternalPM = CCommon.ToLong(dtRecordData.Rows(0)("numExternalPM"))
                                    numNextAssignedTo = CCommon.ToLong(dtRecordData.Rows(0)("numNextAssignedTo"))
                                    numPhone = CCommon.ToString(dtRecordData.Rows(0)("numPhone"))
                                    numOppIDPC = CCommon.ToLong(dtRecordData.Rows(0)("numOppIDPC"))
                                    numStageIDPC = CCommon.ToLong(dtRecordData.Rows(0)("numStageIDPC"))
                                    numOppBizDOCIDPC = CCommon.ToLong(dtRecordData.Rows(0)("numOppBizDOCIDPC"))
                                    numProjectID = CCommon.ToLong(dtRecordData.Rows(0)("numProjectID"))
                                    numCaseID = CCommon.ToLong(dtRecordData.Rows(0)("numCaseId"))
                                    numCommID = CCommon.ToLong(dtRecordData.Rows(0)("numCommId"))

                                    For Each drAct As DataRow In dtWFActions.Rows
                                        Dim WFDescription As String = ""
                                        Dim bitSuccess As Boolean = False

                                        Try
                                            Select Case CCommon.ToShort(drAct("tintActionType"))

                                                Case CCommon.ToShort(enmWFAction.SendAlerts) 'Send Email
                                                    Dim dtDocDetails As DataTable
                                                    Dim objDocuments As New DocumentList
                                                    With objDocuments
                                                        .GenDocID = drAct("numTemplateID")
                                                        .DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        dtDocDetails = .GetDocByGenDocID
                                                    End With

                                                    If dtDocDetails.Rows.Count > 0 Or CCommon.ToLong(drAct("numTemplateID")) = "99999" Then

                                                        Dim strBody As String = Nothing
                                                        Dim strSubject As String = Nothing

                                                        If CCommon.ToLong(drAct("numTemplateID")) = "99999" Then 'Compose Message
                                                            strBody = drAct("vcMailBody")
                                                            strSubject = drAct("vcMailSubject")
                                                        Else
                                                            strBody = dtDocDetails.Rows(0).Item("vcDocdesc")
                                                            strSubject = IIf(IsDBNull(dtDocDetails.Rows(0).Item("vcSubject")), "", dtDocDetails.Rows(0).Item("vcSubject"))
                                                        End If

                                                        Dim objEmail As New Email

                                                        If strBody.ToLower().Contains("##ContactOpt-OutLink##".ToLower()) Then
                                                            objEmail.AdditionalMergeFields.Add("ContactOpt-OutLink", "")
                                                        End If

                                                        Dim dtEmailBroadCast As New DataTable
                                                        Dim dtEmailBroadCastSendTo As New DataTable
                                                        Dim EmailToType() As String = drAct("vcEmailToType").ToString().Split(",")
                                                        Dim lngUserCntID As Long = 0
                                                        Dim tintEmailFrom As Short = CCommon.ToShort(drAct("tintEmailFrom"))

                                                        If tintEmailFrom = 1 Then
                                                            lngUserCntID = numRecOwner
                                                        ElseIf tintEmailFrom = 2 Then
                                                            lngUserCntID = numAssignedTo
                                                        End If

                                                        Dim strContactIDs As New ArrayList
                                                        Dim strContactIDWebLead As New ArrayList
                                                        If EmailToType.Contains(1) Then 'Owner of trigger record
                                                            strContactIDs.Add(numRecOwner)
                                                        End If

                                                        If EmailToType.Contains(2) Then 'Assignee of trigger record
                                                            strContactIDs.Add(numAssignedTo)
                                                        End If

                                                        If EmailToType.Contains(3) Then 'Primary contact of trigger record
                                                            strContactIDs.Add(numContactId)
                                                        End If
                                                        If EmailToType.Contains(4) Then 'Internal Project Manager
                                                            strContactIDs.Add(numInternalPM)
                                                        End If
                                                        If EmailToType.Contains(5) Then 'External Project Manager
                                                            strContactIDs.Add(numExternalPM)
                                                        End If
                                                        If EmailToType.Contains(6) Then 'Next Stage Assigned to
                                                            strContactIDs.Add(numNextAssignedTo)
                                                        End If
                                                        If EmailToType.Contains(7) Then 'Assignee Of Trigger Record(Project)
                                                            strContactIDs.Add(numNextAssignedTo)
                                                        End If

                                                        '  Bizdoc Attachment
                                                        Dim dtTable As DataTable
                                                        If CCommon.ToShort(drAct("tintIsAttachment")) = 1 Then

                                                            Dim objOppBizDocs As New OppBizDocs
                                                            objOppBizDocs.OppBizDocId = CCommon.ToLong(dr("numRecordID"))
                                                            objOppBizDocs.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                            Dim dsOppData As DataSet = objOppBizDocs.GetBizDocsDetails
                                                            Dim objCommon1 As New CCommon
                                                            Dim dsDateFormat As DataSet = objCommon1.GetDomainDateFormat(CCommon.ToLong(dr("numDomainID")))
                                                            Dim htmlCodeToConvert As String = GenerateBizDocPDF(CCommon.ToLong(dr("numRecordID")), CCommon.ToLong(dsOppData.Tables(0).Rows(0)("numOppId")), CCommon.ToLong(dr("numDomainID")), CCommon.ToLong(dtRecordData.Rows(0)("numRecOwner")), 0, dsDateFormat.Tables(0).Rows(0)("vcDateFormat").ToString())
                                                            '  Dim htmlCodeToConvert As String = GenerateBizDocPDF(CCommon.ToLong(dr("numRecordID")), CCommon.ToLong(dsOppData.Tables(0).Rows(0)("numOppId")), CCommon.ToLong(dr("numDomainID")), CCommon.ToLong(dtRecordData.Rows(0)("numRecOwner")), 0, FormattedDateFromDate(dsOppData.Tables(0).Rows(0)("dtFromDate").ToString, dsDateFormat.Tables(0).Rows(0)("vcDateFormat").ToString()))
                                                            ' Dim htmlCodeToConvert As String = getBizDocDetails(CCommon.ToLong(dr("numDomainID")), CCommon.ToLong(dr("numRecordID")), CCommon.ToLong(dsOppData.Tables(0).Rows(0)("numOppId")), CCommon.ToLong(dsOppData.Tables(0).Rows(0)("numBizDocId")))
                                                            Dim i As Integer
                                                            Dim strFileName As String = ""
                                                            Dim strFilePhysicalLocation As String = ""
                                                            Dim objHTMLToPDF As New HTMLToPDF

                                                            strFileName = objHTMLToPDF.ConvertHTML2PDF(htmlCodeToConvert, CCommon.ToLong(dr("numDomainID")))
                                                            strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(CCommon.ToLong(dr("numDomainID"))) & strFileName
                                                            Dim strBizDocName As String = CCommon.ToString(dsOppData.Tables(0).Rows(0).Item("vcBizDocID"))

                                                            Dim objCommon As New CCommon
                                                            dtTable = New DataTable
                                                            dtTable.Columns.Add("Filename")
                                                            dtTable.Columns.Add("FileLocation")
                                                            dtTable.Columns.Add("FilePhysicalPath")
                                                            dtTable.Columns.Add("FileSize", GetType(Long))
                                                            dtTable.Columns.Add("FileType")

                                                            Dim dr1 As DataRow = dtTable.NewRow
                                                            dr1("Filename") = strFileName
                                                            dr1("FileLocation") = CCommon.GetDocumentPhysicalPath(CCommon.ToLong(dr("numDomainID"))) & strFileName
                                                            dr1("FilePhysicalPath") = strFilePhysicalLocation
                                                            dr1("FileType") = ".pdf"
                                                            dr1("FileSize") = 15650
                                                            dtTable.Rows.Add(dr1)

                                                        End If
                                                        'end of code for bizdoc attachment

                                                        If strContactIDs.Count > 0 OrElse (CCommon.ToString(drAct("vcEmailSendTo")) IsNot Nothing) Then
                                                            strContactIDWebLead.Add(numContactId)
                                                            If (moduleId = 1 Or moduleId = 45) Then
                                                                dtEmailBroadCast = objEmail.GetEmailMergeData(moduleId, numContactId, CCommon.ToLong(dr("numDomainID")), 0, 0)
                                                            Else
                                                                dtEmailBroadCast = objEmail.GetEmailMergeData(moduleId, objWF.RecordID, CCommon.ToLong(dr("numDomainID")), 0, 0)
                                                            End If

                                                            ' Displaying Tracking no. as link in Email (Added by Priya)
                                                            If (moduleId = 2) Then
                                                                For Each drrow As DataRow In dtEmailBroadCast.Rows
                                                                    Dim strTrackingNo As String = ""
                                                                    If (Not String.IsNullOrEmpty(CCommon.ToString(drrow("OppOrderTrackingNo")).Trim())) Then
                                                                        Dim strTrackingNoList() As String = CCommon.ToString(drrow("OppOrderTrackingNo")).Split(New String() {"$^$"}, StringSplitOptions.None)
                                                                        Dim strIDValue() As String

                                                                        If strTrackingNoList.Length > 0 Then
                                                                            For k As Integer = 0 To strTrackingNoList.Length - 1
                                                                                strIDValue = strTrackingNoList(k).Split(New String() {"#^#"}, StringSplitOptions.None)
                                                                                If strIDValue(1).Length > 0 Then
                                                                                    strTrackingNo += String.Format("<a href='{0}' target='_blank'>{1}</a>&nbsp;&nbsp;", IIf(strIDValue(0).Length = 0, "javascript:void(0)", String.Format(strIDValue(0) + strIDValue(1), strIDValue(1))), strIDValue(1))
                                                                                End If
                                                                            Next
                                                                        End If
                                                                    End If
                                                                    drrow("OppOrderTrackingNo") = "<div style='width: 95%;word-break: break-all;'>" & strTrackingNo & "</div>"
                                                                Next
                                                            End If

                                                            dtEmailBroadCastSendTo = objEmail.GetEmailMergeData(1, String.Join(",", strContactIDs.ToArray()), CCommon.ToLong(dr("numDomainID")), 0, 0)

                                                            Dim strTo As New List(Of String)
                                                            For Each drEmail As DataRow In dtEmailBroadCastSendTo.Rows
                                                                strTo.Add(drEmail("ContactEmail"))
                                                            Next
                                                            'just for patches-testing purpose

                                                            'end of testing
                                                            If strTo.Count > 0 OrElse (CCommon.ToString(drAct("vcEmailSendTo")) IsNot Nothing) Then
                                                                For Each iKey As KeyValuePair(Of String, String) In objEmail.AdditionalMergeFields
                                                                    If dtEmailBroadCast.Columns.Contains(iKey.Key) = False Then
                                                                        dtEmailBroadCast.Columns.Add(iKey.Key)
                                                                    End If
                                                                    'Assign values to rows in merge table
                                                                    For Each row As DataRow In dtEmailBroadCast.Rows
                                                                        If iKey.Key <> "ContactEmail" Then
                                                                            row(iKey.Key) = CCommon.ToString(iKey.Value)
                                                                        End If
                                                                    Next
                                                                Next

                                                                Dim strFinalTo As String = Nothing
                                                                If CCommon.ToString(drAct("vcEmailSendTo")) = "" Or CCommon.ToString(drAct("vcEmailSendTo")) = Nothing Then
                                                                    strFinalTo = String.Join(",", strTo)
                                                                Else
                                                                    If strTo.Count > 0 Then
                                                                        strFinalTo = String.Join(",", strTo) & "," & drAct("vcEmailSendTo")
                                                                    Else
                                                                        strFinalTo = drAct("vcEmailSendTo")
                                                                    End If

                                                                End If

                                                                Dim mailStatus As Boolean
                                                                Dim errorMessage As String = ""

                                                                If CCommon.ToShort(drAct("tintIsAttachment")) = 1 Then
                                                                    'uncommnted by ss
                                                                    'objEmail.SendEmailWS(strSubject, strBody, "", dtRecordData.Rows(0).Item("vcEmailID"), String.Join(",", strTo), dtEmailBroadCast, dtTable, FromDisplayName:=dtRecordData.Rows(0)("ContactName"), DomainID:=objWF.DomainID)
                                                                    objEmail.AdditionalMergeFields.Add("Signature", dtRecordData.Rows(0)("Signature").ToString)
                                                                    mailStatus = objEmail.SendEmailWS(strSubject, strBody, "", dtRecordData.Rows(0).Item("vcEmailID"), strFinalTo, dtEmailBroadCast, dtTable, FromDisplayName:=dtRecordData.Rows(0)("ContactName"), DomainID:=objWF.DomainID, UserCntID:=lngUserCntID, tintEmailFrom:=tintEmailFrom)
                                                                    dtTable.Rows.Clear()
                                                                    dtTable.Columns.Clear()
                                                                Else
                                                                    'objEmail.SendEmail(strSubject, strBody, "", dtRecordData.Rows(0).Item("vcEmailID"), String.Join(",", strTo), dtEmailBroadCast, FromDisplayName:=dtRecordData.Rows(0)("ContactName"), DomainID:=objWF.DomainID)
                                                                    objEmail.AdditionalMergeFields.Add("Signature", dtRecordData.Rows(0)("Signature").ToString)

                                                                    mailStatus = objEmail.SendEmailWFA(strSubject, strBody, "", dtRecordData.Rows(0).Item("vcEmailID"), strFinalTo, dtEmailBroadCast, FromDisplayName:=dtRecordData.Rows(0)("ContactName"), DomainID:=objWF.DomainID, ErrorMessage:=errorMessage, UserId:=lngUserCntID, tintEmailFrom:=tintEmailFrom)

                                                                End If

                                                                ' WFDescription = "Email Sent to " & String.Join(",", strTo)
                                                                If mailStatus = True Then
                                                                    If strFinalTo = "" Or strFinalTo = Nothing Then
                                                                        WFDescription = "Unable to send a mail.Receiver Email Address doesn't exist."
                                                                    Else
                                                                        WFDescription = "Email Sent to " & strFinalTo
                                                                    End If
                                                                    bitSuccess = True
                                                                Else
                                                                    WFDescription = "Email not Sent. Error:" & errorMessage & ", Please Configure SMTP details.GO to Global Settings->General  "
                                                                    bitSuccess = False
                                                                End If

                                                            End If
                                                        End If
                                                    End If
                                                Case CCommon.ToShort(enmWFAction.AssignActionItems)
                                                    Dim dtActionItem As DataTable
                                                    Dim objActionItem As New ActionItem
                                                    With objActionItem
                                                        .RowID = drAct("numTemplateID")
                                                    End With
                                                    dtActionItem = objActionItem.LoadThisActionItemTemplateData()

                                                    If dtActionItem.Rows.Count > 0 Then
                                                        With objActionItem
                                                            .CommID = 0
                                                            .Task = CLng(dtActionItem.Rows(0)("numTaskType"))

                                                            .ContactID = numContactId
                                                            .DivisionID = numDivisionId

                                                            .Details = ""
                                                            .AssignedTo = IIf(CShort(drAct("tintTicklerActionAssignedTo")) = 1, numRecOwner, numAssignedTo)
                                                            .UserCntID = numRecOwner
                                                            .DomainID = dr("numDomainID")
                                                            If .Task = 973 Then
                                                                .BitClosed = 1
                                                            Else : .BitClosed = 0
                                                            End If
                                                            .CalendarName = ""
                                                            Dim strDate As DateTime = Date.UtcNow.AddDays(Convert.ToDouble(dtActionItem.Rows(0)("DueDays")))

                                                            .StartTime = CDate(strDate.ToString("MM/dd/yyyy 00:00:00") & "AM")
                                                            .EndTime = CDate(strDate.ToString("MM/dd/yyyy 23:59:59") & "PM")

                                                            .Activity = CLng(dtActionItem.Rows(0)("Activity"))
                                                            .Status = CLng(dtActionItem.Rows(0)("Priority"))
                                                            .Snooze = 0
                                                            .SnoozeStatus = 0
                                                            .Remainder = 0
                                                            .RemainderStatus = 0
                                                            .ClientTimeZoneOffset = 0
                                                            .bitOutlook = 0
                                                            .SendEmailTemplate = CBool(dtActionItem.Rows(0).Item("bitSendEmailTemp"))
                                                            .EmailTemplate = CLng(dtActionItem.Rows(0).Item("numEmailTemplate"))
                                                            .Hours = CShort(dtActionItem.Rows(0).Item("tintHours"))
                                                            .Alert = CBool(dtActionItem.Rows(0).Item("bitAlert"))
                                                            .ActivityId = 0
                                                            .FollowUpAnyTime = False
                                                        End With

                                                        numCommID = objActionItem.SaveCommunicationinfo()

                                                        'Added By Sachin Sadhu||Date:4thAug2014
                                                        'Purpose :To Added Ticker data in work Flow queue based on created Rules
                                                        '          Using Change tracking
                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = numCommID
                                                        objWfA.SaveWFActionItemsQueue()
                                                        'ss//end of code

                                                        'CAlerts.SendAlertToAssignee(6, numRecOwner, objActionItem.AssignedTo, numcommId, dr("numDomainID"))

                                                        WFDescription = "Action Items/Ticker Created"
                                                        bitSuccess = True
                                                    End If
                                                Case CCommon.ToShort(enmWFAction.UpdateFields)
                                                    Dim Sql As New System.Text.StringBuilder
                                                    Dim SqlFrom As New System.Text.StringBuilder
                                                    Dim SqlCondition As New System.Text.StringBuilder

                                                    'If CCommon.ToShort(dtWFList.Rows(0)("numFormID")) = 71 Then
                                                    '    objWF.FormID = 49
                                                    'Else
                                                    '    objWF.FormID = CCommon.ToShort(dtWFList.Rows(0)("numFormID"))
                                                    'End If

                                                    'Select Case CCommon.ToShort(dtWFList.Rows(0)("numFormID"))

                                                    Dim tempUpdate As New List(Of String)

                                                    Dim drWFActionUpdateFields() As DataRow = dtWFActionUpdateFields.Select("numWFActionID=" & drAct("numWFActionID"))

                                                    Select Case objWF.FormID
                                                        Case 68 'Organization
                                                            If drWFActionUpdateFields.Length > 0 Then
                                                                Dim strFiels As String = ""
                                                                For Each drCFL As DataRow In drWFActionUpdateFields
                                                                    Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))
                                                                    Dim boolInsert As Boolean = True
                                                                    If drFLCheck.Length > 0 Then
                                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                                            If drFLCheck(0)("vcLookBackTableName") = "AdditionalContactsInformation" Then 'Organization
                                                                                Sql.Append("Update AdditionalContactsInformation a set ")
                                                                                SqlCondition.Append(" FROM  DivisionMaster d where d.numDomainId = $1 AND d.numDivisionId = $2 AND d.numDivisionID=a.numDivisionId ")
                                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "CompanyInfo" Then 'Company Info
                                                                                Sql.Append("Update CompanyInfo c set ")
                                                                                SqlCondition.Append("  FROM  DivisionMaster d where d.numDomainId = $1 AND d.numDivisionId = $2 AND d.numCompanyId=c.numCompanyId ")
                                                                            ElseIf drCFL("FieldName").ToString = "Signature Type" Then
                                                                                Sql.Append("DELETE FROM DivisionMasterShippingConfiguration WHERE numDomainID=$1 AND numDivisionID=$2; ")
                                                                                SqlCondition.Append("INSERT INTO DivisionMasterShippingConfiguration (numDomainID ,numDivisionID ,vcSignatureType) VALUES ( $1,$2, '" + drCFL("vcValue").ToString + "' );")
                                                                            Else
                                                                                Sql.Append("Update DivisionMaster set ")
                                                                                SqlCondition.Append(" where DivisionMaster.numDomainId = $1 AND DivisionMaster.numDivisionId = $2")
                                                                            End If
                                                                        Else 'Update Custom Fields

                                                                            If drFLCheck(0)("vcLookBackTableName") = "CFW_FLD_Values" Then 'Organization

                                                                                boolInsert = objWF.WFCustomFieldsInsert(CCommon.ToLong(drCFL("numFieldID")), drCFL("vcValue"))
                                                                                If boolInsert Then
                                                                                    Sql.Append("Update CFW_FLD_Values set ")
                                                                                    SqlCondition.Append(" where CFW_FLD_Values.Fld_ID = " & drCFL("numFieldID") & " AND CFW_FLD_Values.RecId = $2")
                                                                                End If
                                                                            End If
                                                                        End If
                                                                    End If
                                                                    If Not drCFL("FieldName").ToString = "Signature Type" Then
                                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                                            If CCommon.ToString(drFLCheck(0)("vcOrigDbColumnName")) = "bitNoTax" Then
                                                                                tempUpdate.Add(String.Format("{0}='{1}'", drFLCheck(0)("vcOrigDbColumnName"), IIf(CCommon.ToString(drCFL("vcValue")) = "1", "0", "1")))
                                                                            Else
                                                                                tempUpdate.Add(String.Format("{0}='{1}'", drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                                            End If
                                                                        Else
                                                                            If boolInsert Then
                                                                                tempUpdate.Add(String.Format("{0}='{1}'",
                                                                                            "Fld_Value", drCFL("vcValue")))
                                                                            End If

                                                                        End If
                                                                    End If
                                                                    strFiels = strFiels & "," & drCFL("FieldName")
                                                                    SqlFrom.Append(String.Join(",", tempUpdate))
                                                                    Sql.Append(SqlFrom)
                                                                    Sql.Append(SqlCondition)
                                                                    objWF.textQuery = Sql.ToString()
                                                                    objWF.WFConditionQueryExecute(2)
                                                                    WFDescription = "Fields Updated:" & strFiels
                                                                    bitSuccess = True
                                                                    If bitSuccess = True And objWF.FormID = 68 And drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then
                                                                        'Added By Sachin Sadhu||Date:23ndMay12014
                                                                        'Purpose :To Added Organization data in work Flow queue based on created Rules
                                                                        '          Using Change tracking
                                                                        Dim objWfA As New Workflow()
                                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                        objWfA.UserCntID = numRecOwner
                                                                        objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                        objWfA.SaveWFOrganizationQueue()
                                                                        'end of code
                                                                    ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "AdditionalContactsInformation" Then
                                                                        Dim objWfA As New Workflow()
                                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                        objWfA.UserCntID = numRecOwner
                                                                        objWfA.RecordID = numContactId
                                                                        objWfA.SaveWFContactQueue()
                                                                    Else
                                                                        Dim objWfA As New Workflow()
                                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                        objWfA.UserCntID = numRecOwner
                                                                        objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                        objWfA.SaveWFOrganizationQueue()
                                                                    End If
                                                                    If CCommon.ToBool(drFLCheck(0)("bitCustom")) = True And bitSuccess = True Then ' TRIGGER Custom fields based WF rules
                                                                        Dim objWFCF As New Workflow()
                                                                        objWFCF.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                        objWFCF.UserCntID = numRecOwner
                                                                        objWFCF.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                        objWFCF.FormFieldId = CCommon.ToLong(drCFL("numFieldID"))
                                                                        objWFCF.SaveWFCFOrganizationQueue()
                                                                    End If
                                                                    Sql.Clear()
                                                                    SqlFrom.Clear()
                                                                    SqlCondition.Clear()
                                                                    tempUpdate.Clear()
                                                                Next
                                                            End If

                                                        Case 70 'Opportunities & Orders
                                                            If drWFActionUpdateFields.Length > 0 Then
                                                                Dim strFiels As String = ""
                                                                For Each drCFL As DataRow In drWFActionUpdateFields
                                                                    Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))
                                                                    Dim boolInsert As Boolean = True
                                                                    If drFLCheck.Length > 0 Then
                                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                                            If drFLCheck(0)("vcLookBackTableName") = "CompanyInfo" Then 'Organizatin
                                                                                Sql.Append("Update CompanyInfo c set ")
                                                                                SqlCondition.Append(" FROM OpportunityMaster o inner join DivisionMaster D on d.numdivisionID=o.numDivisionId where o.numOppId=$2 AND o.numDomainId = $1 AND c.numCompanyId=d.numCompanyId")
                                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization
                                                                                Sql.Append("Update DivisionMaster d set ")
                                                                                SqlCondition.Append(" FROM CompanyInfo c, OpportunityMaster o where  o.numDomainId = $1 AND o.numOppId=$2 AND d.numdivisionID=o.numDivisionId AND c.numCompanyId=d.numCompanyId")

                                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "OpportunityBizDocs" Then 'BizDocs
                                                                                Sql.Append("Update OpportunityBizDocs b set ")
                                                                                'SqlCondition.Append(" FROM OpportunityBizDocs b inner join OpportunityMaster o on o.numOppId=b.numOppId  where  b.numOppBizDocsId=$2")
                                                                                SqlCondition.Append(" FROM OpportunityMaster o where  o.numOppId=$2 AND o.numDomainId = $1 AND o.numOppId=b.numOppId")

                                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "StagePercentageDetails" Then 'StagePercentageDetails
                                                                                Sql.Append("Update StagePercentageDetails b set ")
                                                                                'SqlCondition.Append(" FROM OpportunityBizDocs b inner join OpportunityMaster o on o.numOppId=b.numOppId  where  b.numOppBizDocsId=$2")
                                                                                SqlCondition.Append(" FROM OpportunityMaster o where  o.numOppId=$2 AND o.numDomainId = $1 AND o.numOppId=b.numOppId")
                                                                            Else
                                                                                Sql.Append("Update OpportunityMaster set ") 'Orders Module
                                                                                SqlCondition.Append(" where OpportunityMaster.numDomainId = $1 AND OpportunityMaster.numOppID = $2")

                                                                            End If
                                                                        Else
                                                                            If drFLCheck(0)("vcLookBackTableName") = "CFW_Fld_Values_Opp" Then 'Orders

                                                                                boolInsert = objWF.WFCustomFieldsInsert(CCommon.ToLong(drCFL("numFieldID")), drCFL("vcValue"))
                                                                                If boolInsert Then
                                                                                    Sql.Append("Update CFW_Fld_Values_Opp set ")
                                                                                    SqlCondition.Append(" where CFW_Fld_Values_Opp.Fld_ID = " & drCFL("numFieldID") & " AND CFW_Fld_Values_Opp.RecId = $2")
                                                                                End If

                                                                            End If
                                                                        End If
                                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                                            tempUpdate.Add(String.Format("{0}='{1}'",
                                                                                                   drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                                        Else
                                                                            If boolInsert Then
                                                                                tempUpdate.Add(String.Format("{0}='{1}'",
                                                                                                "Fld_Value", drCFL("vcValue")))
                                                                            End If

                                                                        End If

                                                                        SqlFrom.Append(String.Join(",", tempUpdate))

                                                                        Sql.Append(SqlFrom)
                                                                        Sql.Append(SqlCondition)
                                                                        objWF.textQuery = Sql.ToString()
                                                                        objWF.WFConditionQueryExecute(2)
                                                                        strFiels = strFiels & "," & drCFL("FieldName")
                                                                        WFDescription = "Fields Updated:" & strFiels
                                                                        bitSuccess = True
                                                                        Sql.Clear()
                                                                        SqlFrom.Clear()
                                                                        SqlCondition.Clear()
                                                                        tempUpdate.Clear()

                                                                        If bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "OpportunityMaster" Then 'Order Module
                                                                            'Added By Sachin Sadhu||Date:23ndMay12014
                                                                            'Purpose :To Added Order data in work Flow queue based on created Rules
                                                                            '          Using Change tracking
                                                                            Dim objWfA As New Workflow()
                                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                            objWfA.UserCntID = numRecOwner
                                                                            objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                            objWfA.SaveWFOrderQueue()
                                                                            'end of code
                                                                        ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "OpportunityBizDocs" Then 'BizDocs                                                                           
                                                                            Dim objWfA As New Workflow()
                                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                            objWfA.UserCntID = numRecOwner
                                                                            objWfA.RecordID = numOppBizDOCIDPC
                                                                            objWfA.SaveWFBizDocQueue()
                                                                        ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization                                                                           
                                                                            Dim objWfA As New Workflow()
                                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                            objWfA.UserCntID = numRecOwner
                                                                            objWfA.RecordID = numDivisionId
                                                                            objWfA.SaveWFOrganizationQueue()
                                                                        ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "StagePercentageDetails" Then 'Busniess Process                                                                           
                                                                            Dim objWfA As New Workflow()
                                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                            objWfA.UserCntID = numRecOwner
                                                                            objWfA.RecordID = numStageIDPC
                                                                            objWfA.SaveWFBusinessProcessQueue()
                                                                        Else 'orders
                                                                            Dim objWfA As New Workflow()
                                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                            objWfA.UserCntID = numRecOwner
                                                                            objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                            objWfA.SaveWFOrderQueue()

                                                                        End If
                                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = True And bitSuccess = True Then ' TRIGGER Custom fields based WF rules
                                                                            Dim objWFCF As New Workflow()
                                                                            objWFCF.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                            objWFCF.UserCntID = numRecOwner
                                                                            objWFCF.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                            objWFCF.FormFieldId = CCommon.ToLong(drCFL("numFieldID"))
                                                                            objWFCF.SaveWFCFOrdersQueue()
                                                                        End If

                                                                    End If
                                                                Next
                                                            End If

                                                        Case 49 'BizDocs

                                                            If drWFActionUpdateFields.Length > 0 Then
                                                                Dim strFiels As String = ""
                                                                For Each drCFL As DataRow In drWFActionUpdateFields
                                                                    Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))

                                                                    If drFLCheck.Length > 0 Then
                                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                                            If drFLCheck(0)("vcLookBackTableName") = "OpportunityMaster" Then 'Orders
                                                                                Sql.Append("Update OpportunityMaster o set ")
                                                                                SqlCondition.Append(" FROM OpportunityBizDocs b where  b.numOppBizDocsId=$2 AND o.numOppId=b.numOppId")
                                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "CompanyInfo" Then
                                                                                Sql.Append("Update CompanyInfo c set ") 'organization
                                                                                SqlCondition.Append(" FROM OpportunityBizDocs b inner join OpportunityMaster o on o.numOppId=b.numOppId inner join DivisionMaster D on d.numdivisionID=o.numDivisionId where b.numOppBizDocsId=$2 AND c.numCompanyId=d.numCompanyId")
                                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization
                                                                                Sql.Append("Update DivisionMaster d set ")
                                                                                SqlCondition.Append("FROM CompanyInfo c, OpportunityBizDocs b inner join OpportunityMaster o on o.numOppId=b.numOppId where b.numOppBizDocsId=$2 AND d.numdivisionID=o.numDivisionId AND c.numCompanyId=d.numCompanyId")
                                                                            Else 'BizDocs module
                                                                                Sql.Append(" Update OpportunityBizDocs set   ")
                                                                                SqlCondition.Append(" where OpportunityBizDocs.numOppBizDocsId = $2")

                                                                            End If

                                                                        End If
                                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                                            tempUpdate.Add(String.Format("{0}='{1}'",
                                                                                                   drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                                        End If

                                                                        SqlFrom.Append(String.Join(",", tempUpdate))

                                                                        Sql.Append(SqlFrom)
                                                                        Sql.Append(SqlCondition)
                                                                        objWF.textQuery = Sql.ToString()
                                                                        objWF.WFConditionQueryExecute(2)

                                                                        strFiels = strFiels & "," & drCFL("FieldName")
                                                                        WFDescription = "Fields Updated:" & strFiels
                                                                        bitSuccess = True

                                                                        Sql.Clear()
                                                                        SqlFrom.Clear()
                                                                        SqlCondition.Clear()
                                                                        tempUpdate.Clear()
                                                                        If bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "OpportunityMaster" Then 'Order Module

                                                                            Dim objWfA As New Workflow()
                                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                            objWfA.UserCntID = numRecOwner
                                                                            objWfA.RecordID = numOppIDPC
                                                                            objWfA.SaveWFOrderQueue()
                                                                            'end of code
                                                                        ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "OpportunityBizDocs" Then 'BizDocs                                                                           
                                                                            Dim objWfA As New Workflow()
                                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                            objWfA.UserCntID = numRecOwner
                                                                            objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                            objWfA.SaveWFBizDocQueue()
                                                                        ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization                                                                           
                                                                            Dim objWfA As New Workflow()
                                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                            objWfA.UserCntID = numRecOwner
                                                                            objWfA.RecordID = numDivisionId
                                                                            objWfA.SaveWFOrganizationQueue()
                                                                        Else 'BizDocs
                                                                            Dim objWfA As New Workflow()
                                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                            objWfA.UserCntID = numRecOwner
                                                                            objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                            objWfA.SaveWFBizDocQueue()
                                                                        End If

                                                                    End If
                                                                Next
                                                            End If
                                                        Case 94 'Business Process
                                                            Sql.Append("Update StagePercentageDetails set ")
                                                            SqlCondition.Append(" where StagePercentageDetails.numDomainId = $1 AND StagePercentageDetails.numStageDetailsId = $2")
                                                            If drWFActionUpdateFields.Length > 0 Then
                                                                For Each drCFL As DataRow In drWFActionUpdateFields
                                                                    Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))

                                                                    If drFLCheck.Length > 0 Then
                                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                                            tempUpdate.Add(String.Format("{0}='{1}'",
                                                                                                   drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                                        End If
                                                                    End If
                                                                Next

                                                                SqlFrom.Append(String.Join(",", tempUpdate))

                                                                Sql.Append(SqlFrom)
                                                                Sql.Append(SqlCondition)
                                                                objWF.textQuery = Sql.ToString()
                                                                objWF.WFConditionQueryExecute(2)

                                                                WFDescription = "Fields Updated"
                                                                bitSuccess = True

                                                                If bitSuccess = True Then 'Business Process

                                                                    Dim objWfA As New Workflow()
                                                                    objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                    objWfA.UserCntID = numRecOwner
                                                                    objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                    objWfA.SaveWFBusinessProcessQueue()
                                                                    'end of code

                                                                End If
                                                            End If
                                                        Case 69 'Contacts                                                           
                                                            If drWFActionUpdateFields.Length > 0 Then
                                                                Dim strFiels As String = ""
                                                                For Each drCFL As DataRow In drWFActionUpdateFields
                                                                    Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))
                                                                    Dim boolInsert As Boolean = True
                                                                    If drFLCheck.Length > 0 Then
                                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                                            If drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization
                                                                                Sql.Append("Update DivisionMaster c set ")
                                                                                SqlCondition.Append(" FROM AdditionalContactsInformation o where o.numDomainId = $1 AND o.numContactId=$2 AND c.numdivisionID=o.numDivisionId")
                                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "CompanyInfo" Then 'Company Info
                                                                                Sql.Append("Update CompanyInfo c set ")
                                                                                SqlCondition.Append(" FROM AdditionalContactsInformation o inner join DivisionMaster D on d.numdivisionID=o.numDivisionId where  o.numDomainId = $1 AND o.numContactId=$2 AND c.numCompanyId=d.numCompanyId")
                                                                            Else
                                                                                Sql.Append("Update AdditionalContactsInformation set ")
                                                                                SqlCondition.Append(" where AdditionalContactsInformation.numDomainId = $1 AND AdditionalContactsInformation.numContactId = $2")
                                                                            End If
                                                                        Else
                                                                            If drFLCheck(0)("vcLookBackTableName") = "CFW_FLD_Values_Cont" Then 'Contacts

                                                                                boolInsert = objWF.WFCustomFieldsInsert(CCommon.ToLong(drCFL("numFieldID")), drCFL("vcValue"))
                                                                                If boolInsert Then
                                                                                    Sql.Append("Update CFW_FLD_Values_Cont set ")
                                                                                    SqlCondition.Append(" where CFW_FLD_Values_Cont.Fld_ID = " & drCFL("numFieldID") & " AND CFW_FLD_Values_Cont.RecId = $2")
                                                                                End If

                                                                            End If

                                                                        End If
                                                                    End If
                                                                    If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                                        tempUpdate.Add(String.Format("{0}='{1}'",
                                                                                               drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                                    Else
                                                                        If boolInsert Then
                                                                            tempUpdate.Add(String.Format("{0}='{1}'",
                                                                                            "Fld_Value", drCFL("vcValue")))
                                                                        End If
                                                                    End If
                                                                    SqlFrom.Append(String.Join(",", tempUpdate))
                                                                    Sql.Append(SqlFrom)
                                                                    Sql.Append(SqlCondition)
                                                                    objWF.textQuery = Sql.ToString()
                                                                    objWF.WFConditionQueryExecute(2)
                                                                    strFiels = strFiels & "," & drCFL("FieldName")
                                                                    WFDescription = "Fields Updated:" & strFiels
                                                                    bitSuccess = True
                                                                    Sql.Clear()
                                                                    SqlFrom.Clear()
                                                                    SqlCondition.Clear()
                                                                    tempUpdate.Clear()
                                                                    If bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "AdditionalContactsInformation" Then 'Contacts
                                                                        Dim objWfA As New Workflow()
                                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                        objWfA.UserCntID = numRecOwner
                                                                        objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                        objWfA.SaveWFContactQueue()
                                                                        'end of code                                                                
                                                                    ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'BizDocs                                                                           
                                                                        Dim objWfA As New Workflow()
                                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                        objWfA.UserCntID = numRecOwner
                                                                        objWfA.RecordID = numDivisionId
                                                                        objWfA.SaveWFOrganizationQueue()
                                                                    Else 'Contacts
                                                                        Dim objWfA As New Workflow()
                                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                        objWfA.UserCntID = numRecOwner
                                                                        objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                        objWfA.SaveWFContactQueue()
                                                                    End If
                                                                    If CCommon.ToBool(drFLCheck(0)("bitCustom")) = True And bitSuccess = True Then ' TRIGGER Custom fields based WF rules
                                                                        Dim objWFCF As New Workflow()
                                                                        objWFCF.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                        objWFCF.UserCntID = numRecOwner
                                                                        objWFCF.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                        objWFCF.FormFieldId = CCommon.ToLong(drCFL("numFieldID"))
                                                                        objWFCF.SaveWFCFContactsQueue()
                                                                    End If
                                                                Next
                                                            End If

                                                        Case 73 'Projects

                                                            If drWFActionUpdateFields.Length > 0 Then
                                                                Dim strFiels As String = ""
                                                                For Each drCFL As DataRow In drWFActionUpdateFields
                                                                    Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))
                                                                    Dim boolInsert As Boolean = True
                                                                    If drFLCheck.Length > 0 Then
                                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                                            If drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization
                                                                                Sql.Append("Update DivisionMaster c set ")
                                                                                SqlCondition.Append(" FROM ProjectsMaster o where o.numDomainId = $1 AND o.numProId=$2 AND c.numdivisionID=o.numDivisionId")
                                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "CompanyInfo" Then 'Company Info
                                                                                Sql.Append("Update CompanyInfo c set ")
                                                                                SqlCondition.Append(" FROM ProjectsMaster o inner join DivisionMaster D on d.numdivisionID=o.numDivisionId where o.numDomainId = $1 AND o.numProId=$2 AND c.numCompanyId=d.numCompanyId")
                                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "StagePercentageDetails" Then 'StagePercentageDetails
                                                                                Sql.Append("Update StagePercentageDetails b set ")
                                                                                'SqlCondition.Append(" FROM OpportunityBizDocs b inner join OpportunityMaster o on o.numOppId=b.numOppId  where  b.numOppBizDocsId=$2")
                                                                                SqlCondition.Append(" FROM ProjectsMaster o where  o.numProId=$2 AND o.numDomainId = $1 AND o.numProId=b.numProjectID")
                                                                            Else
                                                                                Sql.Append("Update ProjectsMaster set ")
                                                                                SqlCondition.Append(" where ProjectsMaster.numDomainId = $1 AND ProjectsMaster.numProId = $2")
                                                                            End If
                                                                        Else
                                                                            If drFLCheck(0)("vcLookBackTableName") = "CFW_FLD_Values_Pro" Then 'Projects

                                                                                boolInsert = objWF.WFCustomFieldsInsert(CCommon.ToLong(drCFL("numFieldID")), drCFL("vcValue"))
                                                                                If boolInsert Then
                                                                                    Sql.Append("Update CFW_FLD_Values_Pro set ")
                                                                                    SqlCondition.Append(" where CFW_FLD_Values_Pro.Fld_ID = " & drCFL("numFieldID") & " AND CFW_FLD_Values_Pro.RecId = $2")
                                                                                End If
                                                                            End If
                                                                        End If
                                                                    End If
                                                                    If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                                        tempUpdate.Add(String.Format("{0}='{1}'",
                                                                                               drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                                    Else
                                                                        If boolInsert Then
                                                                            tempUpdate.Add(String.Format("{0}='{1}'",
                                                                                            "Fld_Value", drCFL("vcValue")))
                                                                        End If
                                                                    End If
                                                                    SqlFrom.Append(String.Join(",", tempUpdate))
                                                                    Sql.Append(SqlFrom)
                                                                    Sql.Append(SqlCondition)
                                                                    objWF.textQuery = Sql.ToString()
                                                                    objWF.WFConditionQueryExecute(2)
                                                                    strFiels = strFiels & "," & drCFL("FieldName")
                                                                    WFDescription = "Fields Updated:" & strFiels
                                                                    bitSuccess = True
                                                                    Sql.Clear()
                                                                    SqlFrom.Clear()
                                                                    SqlCondition.Clear()
                                                                    tempUpdate.Clear()

                                                                    If bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "ProjectsMaster" Then 'Order Module

                                                                        Dim objWfA As New Workflow()
                                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                        objWfA.UserCntID = numRecOwner
                                                                        objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                        objWfA.SaveWFProjectsQueue()
                                                                        'end of code

                                                                    ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization                                                                           
                                                                        Dim objWfA As New Workflow()
                                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                        objWfA.UserCntID = numRecOwner
                                                                        objWfA.RecordID = numDivisionId
                                                                        objWfA.SaveWFOrganizationQueue()
                                                                    ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "StagePercentageDetails" Then 'Busniess Process                                                                           
                                                                        Dim objWfA As New Workflow()
                                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                        objWfA.UserCntID = numRecOwner
                                                                        objWfA.RecordID = numStageIDPC
                                                                        objWfA.SaveWFBusinessProcessQueue()
                                                                    Else 'Projects
                                                                        Dim objWfA As New Workflow()
                                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                        objWfA.UserCntID = numRecOwner
                                                                        objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                        objWfA.SaveWFProjectsQueue()

                                                                    End If

                                                                    If CCommon.ToBool(drFLCheck(0)("bitCustom")) = True And bitSuccess = True Then ' TRIGGER Custom fields based WF rules
                                                                        Dim objWFCF As New Workflow()
                                                                        objWFCF.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                        objWFCF.UserCntID = numRecOwner
                                                                        objWFCF.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                        objWFCF.FormFieldId = CCommon.ToLong(drCFL("numFieldID"))
                                                                        objWFCF.SaveWFCFProjectsQueue()
                                                                    End If
                                                                Next
                                                            End If

                                                        Case 72 'Cases

                                                            If drWFActionUpdateFields.Length > 0 Then
                                                                Dim strFiels As String = ""
                                                                For Each drCFL As DataRow In drWFActionUpdateFields
                                                                    Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))
                                                                    Dim boolInsert As Boolean = True
                                                                    If drFLCheck.Length > 0 Then
                                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                                            If drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization
                                                                                Sql.Append("Update DivisionMaster c set ")
                                                                                SqlCondition.Append(" FROM Cases o where o.numDomainId = $1 AND o.numCaseId=$2 AND c.numdivisionID=o.numDivisionId")
                                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "CompanyInfo" Then 'Company Info
                                                                                Sql.Append("Update CompanyInfo c set ")
                                                                                SqlCondition.Append(" FROM Cases o inner join DivisionMaster D on d.numdivisionID=o.numDivisionId where o.numCaseId=$2 AND o.numDomainId = $1 AND c.numCompanyId=d.numCompanyId")
                                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "OpportunityMaster" Then 'Orders
                                                                                Sql.Append("Update OpportunityMaster o set ")
                                                                                SqlCondition.Append(" FROM Cases c inner join CaseOpportunities cp on c.numCaseId=cp.numCaseId where c.numDomainId = $1 AND c.numCaseId = $2 AND o.numOppId=cp.numOppId")
                                                                            Else
                                                                                Sql.Append("Update Cases set ")
                                                                                SqlCondition.Append(" where Cases.numDomainId = $1 AND Cases.numCaseId = $2")
                                                                            End If
                                                                        Else
                                                                            If drFLCheck(0)("vcLookBackTableName") = "CFW_FLD_Values_Case" Then 'Cases

                                                                                boolInsert = objWF.WFCustomFieldsInsert(CCommon.ToLong(drCFL("numFieldID")), drCFL("vcValue"))
                                                                                If boolInsert Then
                                                                                    Sql.Append("Update CFW_FLD_Values_Case set ")
                                                                                    SqlCondition.Append(" where CFW_FLD_Values_Case.Fld_ID = " & drCFL("numFieldID") & " AND CFW_FLD_Values_Case.RecId = $2")
                                                                                End If
                                                                            End If
                                                                        End If
                                                                    End If
                                                                    If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                                        tempUpdate.Add(String.Format("{0}='{1}'",
                                                                                               drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                                    Else
                                                                        If boolInsert Then
                                                                            tempUpdate.Add(String.Format("{0}='{1}'",
                                                                                            "Fld_Value", drCFL("vcValue")))
                                                                        End If
                                                                    End If
                                                                    SqlFrom.Append(String.Join(",", tempUpdate))
                                                                    Sql.Append(SqlFrom)
                                                                    Sql.Append(SqlCondition)
                                                                    objWF.textQuery = Sql.ToString()
                                                                    objWF.WFConditionQueryExecute(2)
                                                                    strFiels = strFiels & "," & drCFL("FieldName")
                                                                    WFDescription = "Fields Updated:" & strFiels
                                                                    bitSuccess = True
                                                                    Sql.Clear()
                                                                    SqlFrom.Clear()
                                                                    SqlCondition.Clear()
                                                                    tempUpdate.Clear()

                                                                    If bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "OpportunityMaster" Then 'Order Module

                                                                        Dim objWfA As New Workflow()
                                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                        objWfA.UserCntID = numRecOwner
                                                                        objWfA.RecordID = numOppIDPC
                                                                        objWfA.SaveWFOrderQueue()
                                                                        'end of code
                                                                    ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "Cases" Then 'Cases                                                                           
                                                                        Dim objWfA As New Workflow()
                                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                        objWfA.UserCntID = numRecOwner
                                                                        objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                        objWfA.SaveWFCasesQueue()
                                                                    ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization                                                                           
                                                                        Dim objWfA As New Workflow()
                                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                        objWfA.UserCntID = numRecOwner
                                                                        objWfA.RecordID = numDivisionId
                                                                        objWfA.SaveWFOrganizationQueue()

                                                                    Else 'Cases
                                                                        Dim objWfA As New Workflow()
                                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                        objWfA.UserCntID = numRecOwner
                                                                        objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                        objWfA.SaveWFCasesQueue()

                                                                    End If
                                                                    If CCommon.ToBool(drFLCheck(0)("bitCustom")) = True And bitSuccess = True Then ' TRIGGER Custom fields based WF rules
                                                                        Dim objWFCF As New Workflow()
                                                                        objWFCF.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                        objWFCF.UserCntID = numRecOwner
                                                                        objWFCF.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                        objWFCF.FormFieldId = CCommon.ToLong(drCFL("numFieldID"))
                                                                        objWFCF.SaveWFCFCasesQueue()
                                                                    End If
                                                                Next
                                                            End If

                                                            'Sql.Append("Update Cases set ")
                                                            'SqlCondition.Append(" where Cases.numDomainId = $1 AND Cases.numCaseId = $2")
                                                            'If drWFActionUpdateFields.Length > 0 Then
                                                            '    For Each drCFL As DataRow In drWFActionUpdateFields
                                                            '        Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))

                                                            '        If drFLCheck.Length > 0 Then
                                                            '            If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                            '                tempUpdate.Add(String.Format("{0}='{1}'", _
                                                            '                                       drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                            '            End If
                                                            '        End If
                                                            '    Next

                                                            '    SqlFrom.Append(String.Join(",", tempUpdate))

                                                            '    Sql.Append(SqlFrom)
                                                            '    Sql.Append(SqlCondition)
                                                            '    objWF.textQuery = Sql.ToString()
                                                            '    objWF.WFConditionQueryExecute(2)

                                                            '    WFDescription = "Fields Updated"
                                                            '    bitSuccess = True

                                                            'End If
                                                        Case 124 'Actiom Items(Tickler)

                                                            If drWFActionUpdateFields.Length > 0 Then
                                                                Dim strFiels As String = ""
                                                                For Each drCFL As DataRow In drWFActionUpdateFields
                                                                    Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))

                                                                    If drFLCheck.Length > 0 Then
                                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                                            If drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization
                                                                                Sql.Append("Update DivisionMaster c set ")
                                                                                SqlCondition.Append(" FROM Communication o where o.numDomainId = $1 AND o.numCommId=$2 AND c.numdivisionID=o.numDivisionId")
                                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "CompanyInfo" Then 'Company Info
                                                                                Sql.Append("Update CompanyInfo c set ")
                                                                                SqlCondition.Append(" FROM Communication o inner join DivisionMaster D on d.numdivisionID=o.numDivisionId where o.numDomainId = $1 AND o.numCommId=$2 AND c.numCompanyId=d.numCompanyId")
                                                                            Else
                                                                                Sql.Append("Update Communication set ")
                                                                                SqlCondition.Append(" where Communication.numDomainId = $1 AND Communication.numCommId = $2")
                                                                            End If

                                                                        End If
                                                                    End If
                                                                    If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                                        tempUpdate.Add(String.Format("{0}='{1}'",
                                                                                               drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                                    End If
                                                                    SqlFrom.Append(String.Join(",", tempUpdate))
                                                                    Sql.Append(SqlFrom)
                                                                    Sql.Append(SqlCondition)
                                                                    objWF.textQuery = Sql.ToString()
                                                                    objWF.WFConditionQueryExecute(2)
                                                                    strFiels = strFiels & "," & drCFL("FieldName")
                                                                    WFDescription = "Fields Updated:" & strFiels
                                                                    bitSuccess = True
                                                                    Sql.Clear()
                                                                    SqlFrom.Clear()
                                                                    SqlCondition.Clear()
                                                                    tempUpdate.Clear()

                                                                    If bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "Communication" Then 'Order Module
                                                                        Dim objWfA As New Workflow()
                                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                        objWfA.UserCntID = numRecOwner
                                                                        objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                        objWfA.SaveWFActionItemsQueue()
                                                                        'end of code

                                                                    ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization                                                                           
                                                                        Dim objWfA As New Workflow()
                                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                        objWfA.UserCntID = numRecOwner
                                                                        objWfA.RecordID = numDivisionId
                                                                        objWfA.SaveWFOrganizationQueue()

                                                                    Else 'Tickler
                                                                        Dim objWfA As New Workflow()
                                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                        objWfA.UserCntID = numRecOwner
                                                                        objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                        objWfA.SaveWFActionItemsQueue()

                                                                    End If
                                                                Next
                                                            End If
                                                    End Select
                                                Case CCommon.ToShort(enmWFAction.DisplayAlertMessage)

                                                    ' Dim cph As ContentPlaceHolder = DirectCast(Me.Master.FindControl("TabsPlaceHolder"), ContentPlaceHolder)
                                                    objWF.vcAlertMessage = CCommon.ToString(drAct("vcAlertMessage").ToString)
                                                    objWF.WFID = CCommon.ToLong(dr("numWFID"))
                                                    objWF.intAlertStatus = 1
                                                    objWF.SaveWorkFlowAlerts()
                                                    WFDescription = "Display Alert:Alert is shown"
                                                    bitSuccess = True
                                                Case CCommon.ToShort(enmWFAction.BizDocApproval)
                                                    Try

                                                        Dim objDocuments As New DocumentList
                                                        Dim strReceiverName As String() = drAct("vcApproval").Split(",")
                                                        Dim strApprovalName As String = ""
                                                        Dim k As Integer = 0
                                                        For k = 0 To strReceiverName.Length - 1

                                                            objDocuments.GenDocID = CCommon.ToLong(dr("numRecordID"))
                                                            objDocuments.ContactID = CCommon.ToLong(strReceiverName(k))
                                                            objDocuments.CDocType = "B"
                                                            objDocuments.byteMode = 1
                                                            objDocuments.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                            objDocuments.UserCntID = numRecOwner
                                                            objDocuments.ManageApprovers()

                                                            Dim objOppBizDocs As New OppBizDocs
                                                            objOppBizDocs.OppBizDocId = CCommon.ToLong(dr("numRecordID"))
                                                            objOppBizDocs.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                            Dim dsOppData As DataSet = objOppBizDocs.GetBizDocsDetails

                                                            'Send Approval requeset via Mail
                                                            Dim ObjCmn As New CCommon
                                                            Dim objSendMail As New Email
                                                            Dim dtDocDetails As DataTable = objSendMail.GetEmailTemplateByCode("#SYS#DOC_APPROVAL_REQUEST", CCommon.ToLong(dr("numDomainID")))
                                                            Dim objUserAccess As New UserAccess
                                                            objUserAccess.ContactID = numRecOwner
                                                            objUserAccess.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                            Dim dtUser As DataTable = objUserAccess.GetExtranetUserDetails()
                                                            Dim strPortalUrl As String = "http://portal.bizautomation.com/Login.aspx"
                                                            Dim strPortalApprovalLink As String
                                                            If dtUser.Rows.Count = 1 Then

                                                                strPortalApprovalLink = CCommon.ToString(strPortalUrl) & QueryEncryption.EncryptQueryString("&from=email&u=" + CCommon.ToString(dtUser.Rows(0)("vcEmail")) + "&p=" + ObjCmn.Encrypt(CCommon.ToString(dtUser.Rows(0)("vcPassword"))))
                                                                strPortalApprovalLink = "<a href=" & strPortalApprovalLink & " target='_blank' >click here</a>"

                                                            End If

                                                            If dtDocDetails.Rows.Count > 0 Then
                                                                Dim strBody As String = dtDocDetails.Rows(0).Item("vcDocdesc")
                                                                Dim strSubject As String = IIf(IsDBNull(dtDocDetails.Rows(0).Item("vcSubject")), "", dtDocDetails.Rows(0).Item("vcSubject"))

                                                                Dim objEmail As New Email
                                                                objEmail.AdditionalMergeFields.Add("DocumentName", dsOppData.Tables(0).Rows(0).Item("vcBizDocID"))
                                                                objEmail.AdditionalMergeFields.Add("LoggedInUser", dtRecordData.Rows(0)("ContactName"))
                                                                objEmail.AdditionalMergeFields.Add("PortalDirectLoginLink", strPortalApprovalLink)
                                                                objEmail.AdditionalMergeFields.Add("Signature", dtRecordData.Rows(0)("Signature"))
                                                                If strBody.ToLower().Contains("##ContactOpt-OutLink##".ToLower()) Then
                                                                    objEmail.AdditionalMergeFields.Add("ContactOpt-OutLink", "")
                                                                End If

                                                                Dim dtEmailBroadCast As New DataTable

                                                                Dim EmailToType() As String = drAct("vcApproval").ToString().Split(",")
                                                                ' Dim strContactIDs As New ArrayList

                                                                'If EmailToType.Contains(1) Then 'Owner of trigger record
                                                                '    strContactIDs.Add(numRecOwner)
                                                                'End If

                                                                'If EmailToType.Contains(2) Then 'Assignee of trigger record
                                                                '    strContactIDs.Add(numAssignedTo)
                                                                'End If

                                                                'If EmailToType.Contains(3) Then 'Primary contact of trigger record
                                                                '    strContactIDs.Add(numContactId)
                                                                'End If


                                                                ' If strContactIDs.Count > 0 Then
                                                                dtEmailBroadCast = objEmail.GetEmailMergeData(1, String.Join(",", EmailToType.ToArray()), CCommon.ToLong(dr("numDomainID")), 0, 0)

                                                                Dim strTo As New List(Of String)
                                                                For Each drEmail As DataRow In dtEmailBroadCast.Rows
                                                                    strTo.Add(drEmail("ContactEmail"))
                                                                Next

                                                                If dtEmailBroadCast.Rows.Count > 0 Then
                                                                    strApprovalName = dtEmailBroadCast.Rows(0)("ContactFirstName").ToString
                                                                End If

                                                                If strTo.Count > 0 Then
                                                                    For Each iKey As Object In objEmail.AdditionalMergeFields.Keys
                                                                        If dtEmailBroadCast.Columns.Contains(iKey.ToString) = False Then
                                                                            dtEmailBroadCast.Columns.Add(iKey.ToString)
                                                                        End If
                                                                        'Assign values to rows in merge table
                                                                        For Each row As DataRow In dtEmailBroadCast.Rows
                                                                            If iKey.ToString <> "ContactEmail" Then
                                                                                ' row(iKey.ToString) = CCommon.ToString(iKey)
                                                                                row(iKey.ToString) = CCommon.ToString(objEmail.AdditionalMergeFields(iKey))
                                                                            End If
                                                                        Next
                                                                    Next
                                                                    objEmail.SendEmail(strSubject, strBody, "", dtRecordData.Rows(0).Item("vcEmailID"), String.Join(",", strTo), dtEmailBroadCast, FromDisplayName:=dtRecordData.Rows(0)("ContactName"), DomainID:=objWF.DomainID)

                                                                End If
                                                                ' End If
                                                            End If
                                                            'end of Request
                                                        Next

                                                        WFDescription = "BizDoc Approval Request sent to Tickler of " & strApprovalName
                                                        bitSuccess = True

                                                    Catch ex As Exception
                                                        If ex.Message = "No_EXTRANET" Then
                                                            WFDescription = "Can not add selected contact to approver list. Your option is to add contact as external users from ""Administrator->User Administration->External Users"" and try again."
                                                            bitSuccess = False
                                                            Exit Sub
                                                        Else
                                                            Throw ex
                                                        End If
                                                    End Try
                                                Case CCommon.ToShort(enmWFAction.CreateBizDoc)
                                                    Dim dtCheckBizDocs As New DataTable()
                                                    dtCheckBizDocs = CheckDuplicateBizDocs(CCommon.ToLong(dr("numRecordID")), CCommon.ToLong(dr("numDomainID")), CCommon.ToShort(drAct("numOppType")), numRecOwner, CCommon.ToLong(drAct("numBizDocTypeID")), CCommon.ToLong(drAct("numBizDocTemplateID")))

                                                    If dtCheckBizDocs.Rows.Count > 0 Then
                                                        'WFDescription = CCommon.ToString(drAct("vcBizDoc")) & "-This BizDoc  already exists for this order "
                                                        WFDescription = "This BizDoc-" & CCommon.ToString(drAct("vcBizDoc")) & "  already exists for this order "
                                                        bitSuccess = True
                                                    Else
                                                        Dim objOppBizDoc As New OppBizDocs
                                                        objOppBizDoc.OppId = CCommon.ToLong(dr("numRecordID"))
                                                        objOppBizDoc.BizDocId = CCommon.ToLong(drAct("numBizDocTypeID"))
                                                        If CCommon.ToBool(objOppBizDoc.CheckIfIemsPendingToAddByBizDocID()) Then
                                                            CreateBizDoc(CCommon.ToLong(dr("numRecordID")), CCommon.ToLong(dr("numDomainID")), CCommon.ToShort(drAct("numOppType")), numRecOwner, CCommon.ToLong(drAct("numBizDocTypeID")), CCommon.ToLong(drAct("numBizDocTemplateID")))
                                                            WFDescription = "BizDoc-" & CCommon.ToString(drAct("vcBizDoc")) & " has been created"

                                                            bitSuccess = True
                                                        Else
                                                            WFDescription = "Not items left to be added to bizdoc."
                                                            bitSuccess = True
                                                        End If

                                                    End If

                                                Case CCommon.ToShort(enmWFAction.PromoteOrganization)
                                                    Dim objLeads As New LeadsIP
                                                    objLeads.DivisionID = CCommon.ToLong(dr("numRecordID"))
                                                    objLeads.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                    Dim dtCrmType As DataTable = objLeads.GetCRMType()

                                                    Dim tintCRMType As Integer = CCommon.ToInteger(dtCrmType.Rows(0)("tintCRMType").ToString)

                                                    If tintCRMType = 0 Then ' Leads

                                                        objLeads.UserCntID = numRecOwner
                                                        objLeads.ContactID = numContactId
                                                        objLeads.DivisionID = CCommon.ToLong(dr("numRecordID"))
                                                        objLeads.PromoteLead()
                                                        WFDescription = CCommon.ToString(dr("numRecordID")) & "-Organization Promoted"
                                                        bitSuccess = True
                                                    ElseIf tintCRMType = 1 Then 'Prospects
                                                        objLeads.DivisionID = CCommon.ToLong(dr("numRecordID"))
                                                        objLeads.byteMode = 2
                                                        objLeads.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objLeads.UserCntID = numRecOwner
                                                        objLeads.DemoteOrg()
                                                        WFDescription = CCommon.ToString(dr("numRecordID")) & "-Organization Promoted"
                                                        bitSuccess = True
                                                    Else 'Account
                                                        WFDescription = CCommon.ToString(dr("numRecordID")) & "-This Organization is already in Account ,No scope for Promotion"
                                                        bitSuccess = True
                                                    End If
                                                Case CCommon.ToShort(enmWFAction.DemoteOrganization)
                                                    Dim objLeads As New LeadsIP
                                                    objLeads.DivisionID = CCommon.ToLong(dr("numRecordID"))
                                                    objLeads.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                    Dim dtCrmType As DataTable = objLeads.GetCRMType()

                                                    Dim tintCRMType As Integer = CCommon.ToInteger(dtCrmType.Rows(0)("tintCRMType").ToString)

                                                    If tintCRMType = 1 Then 'Prospect
                                                        objLeads.DivisionID = CCommon.ToLong(dr("numRecordID"))
                                                        objLeads.byteMode = 0
                                                        objLeads.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objLeads.UserCntID = numRecOwner
                                                        objLeads.DemoteOrg()
                                                        WFDescription = CCommon.ToString(dr("numRecordID")) & "-Organization demoted"
                                                        bitSuccess = True
                                                    ElseIf tintCRMType = 2 Then 'Account
                                                        Dim objAccounts As New CAccounts
                                                        Dim intCount As Integer
                                                        objAccounts.DivisionID = CCommon.ToLong(dr("numRecordID"))
                                                        intCount = objAccounts.CheckDealsWon
                                                        If intCount = 0 Then
                                                            objLeads.DivisionID = CCommon.ToLong(dr("numRecordID"))
                                                            objLeads.byteMode = 1
                                                            objLeads.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                            objLeads.UserCntID = numRecOwner
                                                            objLeads.DemoteOrg()
                                                        Else
                                                            WFDescription = "This record can not be demoted because it contains completed deals. To demote this record, delete all deals from the Deal History section, then try demoting again."
                                                            bitSuccess = True

                                                        End If
                                                    Else
                                                        WFDescription = "This record can not be demoted because It's on Lead stage"
                                                        bitSuccess = True

                                                    End If

                                                Case CCommon.ToShort(enmWFAction.SMSAlerts)
                                                    Dim objEmail As New Email
                                                    Dim SMSToType() As String = drAct("vcEmailToType").ToString().Split(",")
                                                    Dim strContactIDs As New ArrayList

                                                    If SMSToType.Contains(1) Then 'Owner of trigger record
                                                        strContactIDs.Add(numRecOwner)
                                                    End If

                                                    If SMSToType.Contains(2) Then 'Assignee of trigger record
                                                        strContactIDs.Add(numAssignedTo)
                                                    End If

                                                    If SMSToType.Contains(3) Then 'Primary contact of trigger record
                                                        strContactIDs.Add(numContactId)
                                                    End If
                                                    If SMSToType.Contains(4) Then 'Internal Project Manager
                                                        strContactIDs.Add(numInternalPM)
                                                    End If
                                                    If SMSToType.Contains(5) Then 'External Project Manager
                                                        strContactIDs.Add(numExternalPM)
                                                    End If
                                                    If SMSToType.Contains(6) Then 'Next Stage Assigned to
                                                        strContactIDs.Add(numNextAssignedTo)
                                                    End If
                                                    If SMSToType.Contains(7) Then 'Assignee Of Trigger Record(Project)
                                                        strContactIDs.Add(numNextAssignedTo)
                                                    End If
                                                    Dim dtSMSBroadCast As New DataTable
                                                    Dim ACCOUNT_SID As String = "ACf3bc46e894a584aa43c6e934eb099db1"
                                                    Dim AUTH_TOKEN As String = "5a190ab5b2830b4dea058dbcc4c0590f"

                                                    Dim client As New TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN)

                                                    If strContactIDs.Count > 0 Then
                                                        dtSMSBroadCast = objEmail.GetEmailMergeData(1, String.Join(",", strContactIDs.ToArray()), CCommon.ToLong(dr("numDomainID")), 0, 0)
                                                        Dim strTo As New List(Of String)
                                                        For Each drEmail As DataRow In dtSMSBroadCast.Rows
                                                            strTo.Add(drEmail("ContactPhone"))
                                                            Dim message = client.SendSmsMessage("+12545312324", drEmail("ContactPhone").ToString(), CCommon.ToString(drAct("vcSMSText").ToString))
                                                            If message.RestException IsNot Nothing Then                                                        ' handle the error ...
                                                                WFDescription = " Error:" & message.RestException.Message.ToString
                                                                bitSuccess = False
                                                            Else
                                                                WFDescription = "Message Has been Sent to" & drEmail("ContactPhone").ToString()
                                                                bitSuccess = True
                                                            End If
                                                        Next
                                                    End If

                                            End Select

                                        Catch ex As Exception
                                            WFDescription = "Error occured:" & ex.Message
                                            bitSuccess = False
                                        End Try

                                        Dim objWF1 As New Workflow
                                        With objWF1
                                            .WFQueueID = CCommon.ToLong(dr("numWFQueueID"))
                                            .WFID = objWF.WFID
                                            .WFDescription = WFDescription
                                            .ManageWorkFlowQueue(2, _bitSuccess:=bitSuccess)
                                        End With
                                    Next

                                    objWF = New Workflow
                                    With objWF
                                        .WFQueueID = CCommon.ToLong(dr("numWFQueueID"))
                                        .DomainID = CCommon.ToLong(dr("numDomainID"))
                                        .tintProcessStatus = CCommon.ToShort(enmWFQueueProcessStatus.Success)
                                        .ManageWorkFlowQueue(1)
                                    End With
                                Else
                                    objWF = New Workflow
                                    With objWF
                                        .WFQueueID = CCommon.ToLong(dr("numWFQueueID"))
                                        .DomainID = CCommon.ToLong(dr("numDomainID"))
                                        .tintProcessStatus = CCommon.ToShort(enmWFQueueProcessStatus.ConditionNotMatch)
                                        .ManageWorkFlowQueue(1)
                                    End With
                                End If
                            Else 'Time based action

                                Dim dtTable As DataTable = GetTimeBasedActionStatus(objWF.RecordID, strTablename, objWF.vcDateField, objWF.intDays, objWF.intActionOn, CCommon.ToInteger(objWF.boolCustom), objWF.numFieldID)

                                Dim intResult As Integer = CCommon.ToInteger(dtTable.Rows(0)("TimeMatchStatus"))
                                If boolCondition Then 'If condition matched then check for date
                                    If intResult = 1 Then 'matching condition

                                        Dim numContactId, numDivisionId, numRecOwner, numAssignedTo, numInternalPM, numExternalPM, numNextAssignedTo, numOppIDPC, numStageIDPC, numOppBizDOCIDPC As Long
                                        Dim numPhone As String
                                        Dim dtRecordData As DataTable = objWF.GetWFRecordData

                                        numContactId = CCommon.ToLong(dtRecordData.Rows(0)("numContactId"))
                                        numDivisionId = CCommon.ToLong(dtRecordData.Rows(0)("numDivisionId"))
                                        numRecOwner = CCommon.ToLong(dtRecordData.Rows(0)("numRecOwner"))
                                        numAssignedTo = CCommon.ToLong(dtRecordData.Rows(0)("numAssignedTo"))
                                        numInternalPM = CCommon.ToLong(dtRecordData.Rows(0)("numInternalPM"))
                                        numExternalPM = CCommon.ToLong(dtRecordData.Rows(0)("numExternalPM"))
                                        numNextAssignedTo = CCommon.ToLong(dtRecordData.Rows(0)("numNextAssignedTo"))
                                        numPhone = CCommon.ToString(dtRecordData.Rows(0)("numPhone"))
                                        numOppIDPC = CCommon.ToLong(dtRecordData.Rows(0)("numOppIDPC"))
                                        numStageIDPC = CCommon.ToLong(dtRecordData.Rows(0)("numStageIDPC"))
                                        numOppBizDOCIDPC = CCommon.ToLong(dtRecordData.Rows(0)("numOppBizDOCIDPC"))

                                        For Each drAct As DataRow In dtWFActions.Rows
                                            Dim WFDescription As String = ""
                                            Dim bitSuccess As Boolean = False

                                            Try
                                                Select Case CCommon.ToShort(drAct("tintActionType"))

                                                    Case CCommon.ToShort(enmWFAction.SendAlerts) 'Send Email
                                                        Dim dtDocDetails As DataTable
                                                        Dim objDocuments As New DocumentList
                                                        With objDocuments
                                                            .GenDocID = drAct("numTemplateID")
                                                            .DomainID = CCommon.ToLong(dr("numDomainID"))
                                                            dtDocDetails = .GetDocByGenDocID
                                                        End With

                                                        If dtDocDetails.Rows.Count > 0 Or CCommon.ToLong(drAct("numTemplateID")) = "99999" Then
                                                            Dim strBody As String = Nothing
                                                            Dim strSubject As String = Nothing

                                                            If CCommon.ToLong(drAct("numTemplateID")) = "99999" Then 'Compose Message
                                                                strBody = drAct("vcMailBody")
                                                                strSubject = drAct("vcMailSubject")
                                                            Else
                                                                strBody = dtDocDetails.Rows(0).Item("vcDocdesc")
                                                                strSubject = IIf(IsDBNull(dtDocDetails.Rows(0).Item("vcSubject")), "", dtDocDetails.Rows(0).Item("vcSubject"))

                                                            End If
                                                            Dim objEmail As New Email

                                                            If strBody.ToLower().Contains("##ContactOpt-OutLink##".ToLower()) Then
                                                                objEmail.AdditionalMergeFields.Add("ContactOpt-OutLink", "")
                                                            End If

                                                            Dim dtEmailBroadCast As New DataTable
                                                            Dim dtEmailBroadCastSendTo As New DataTable

                                                            Dim lngUserCntID As Long = 0
                                                            Dim tintEmailFrom As Short = CCommon.ToShort(drAct("tintEmailFrom"))

                                                            If tintEmailFrom = 1 Then
                                                                lngUserCntID = numRecOwner
                                                            ElseIf tintEmailFrom = 2 Then
                                                                lngUserCntID = numAssignedTo
                                                            End If

                                                            Dim EmailToType() As String = drAct("vcEmailToType").ToString().Split(",")
                                                            Dim strContactIDs As New ArrayList
                                                            Dim strContactIDWebLead As New ArrayList
                                                            If EmailToType.Contains(1) Then 'Owner of trigger record
                                                                strContactIDs.Add(numRecOwner)
                                                            End If

                                                            If EmailToType.Contains(2) Then 'Assignee of trigger record
                                                                strContactIDs.Add(numAssignedTo)
                                                            End If

                                                            If EmailToType.Contains(3) Then 'Primary contact of trigger record
                                                                strContactIDs.Add(numContactId)
                                                            End If
                                                            If EmailToType.Contains(4) Then 'Internal Project Manager
                                                                strContactIDs.Add(numInternalPM)
                                                            End If
                                                            If EmailToType.Contains(5) Then 'External Project Manager
                                                                strContactIDs.Add(numExternalPM)
                                                            End If
                                                            If EmailToType.Contains(6) Then 'Next Stage Assigned to
                                                                strContactIDs.Add(numNextAssignedTo)
                                                            End If
                                                            If EmailToType.Contains(7) Then 'Assignee Of Trigger Record(Project)
                                                                strContactIDs.Add(numNextAssignedTo)
                                                            End If
                                                            '  Bizdoc Attachment
                                                            Dim dtBizDocAttachment As DataTable
                                                            If CCommon.ToShort(drAct("tintIsAttachment")) = 1 Then

                                                                Dim objOppBizDocs As New OppBizDocs
                                                                objOppBizDocs.OppBizDocId = CCommon.ToLong(dr("numRecordID"))
                                                                objOppBizDocs.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                Dim dsOppData As DataSet = objOppBizDocs.GetBizDocsDetails
                                                                Dim objCommon1 As New CCommon
                                                                Dim dsDateFormat As DataSet = objCommon1.GetDomainDateFormat(CCommon.ToLong(dr("numDomainID")))
                                                                Dim htmlCodeToConvert As String = GenerateBizDocPDF(CCommon.ToLong(dr("numRecordID")), CCommon.ToLong(dsOppData.Tables(0).Rows(0)("numOppId")), CCommon.ToLong(dr("numDomainID")), CCommon.ToLong(dtRecordData.Rows(0)("numRecOwner")), 0, dsDateFormat.Tables(0).Rows(0)("vcDateFormat").ToString())
                                                                'Dim htmlCodeToConvert As String = GenerateBizDocPDF(CCommon.ToLong(dr("numRecordID")), CCommon.ToLong(dsOppData.Tables(0).Rows(0)("numOppId")), CCommon.ToLong(dr("numDomainID")), CCommon.ToLong(dtRecordData.Rows(0)("numRecOwner")), 0, FormattedDateFromDate(dsOppData.Tables(0).Rows(0)("dtFromDate").ToString, dsDateFormat.Tables(0).Rows(0)("vcDateFormat").ToString()))
                                                                ' Dim htmlCodeToConvert As String = getBizDocDetails(CCommon.ToLong(dr("numDomainID")), CCommon.ToLong(dr("numRecordID")), CCommon.ToLong(dsOppData.Tables(0).Rows(0)("numOppId")), CCommon.ToLong(dsOppData.Tables(0).Rows(0)("numBizDocId")))
                                                                Dim i As Integer
                                                                Dim strFileName As String = ""
                                                                Dim strFilePhysicalLocation As String = ""
                                                                Dim objHTMLToPDF As New HTMLToPDF

                                                                strFileName = objHTMLToPDF.ConvertHTML2PDF(htmlCodeToConvert, CCommon.ToLong(dr("numDomainID")))
                                                                strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(CCommon.ToLong(dr("numDomainID"))) & strFileName
                                                                Dim strBizDocName As String = CCommon.ToString(dsOppData.Tables(0).Rows(0).Item("vcBizDocID"))

                                                                Dim objCommon As New CCommon
                                                                dtBizDocAttachment = New DataTable
                                                                dtBizDocAttachment.Columns.Add("Filename")
                                                                dtBizDocAttachment.Columns.Add("FileLocation")
                                                                dtBizDocAttachment.Columns.Add("FilePhysicalPath")
                                                                dtBizDocAttachment.Columns.Add("FileSize", GetType(Long))
                                                                dtBizDocAttachment.Columns.Add("FileType")

                                                                Dim dr1 As DataRow = dtBizDocAttachment.NewRow
                                                                dr1("Filename") = strFileName
                                                                dr1("FileLocation") = CCommon.GetDocumentPhysicalPath(CCommon.ToLong(dr("numDomainID"))) & strFileName
                                                                dr1("FilePhysicalPath") = strFilePhysicalLocation
                                                                dr1("FileType") = ".pdf"
                                                                dr1("FileSize") = 15650
                                                                dtBizDocAttachment.Rows.Add(dr1)

                                                            End If
                                                            'end of code for bizdoc attachment

                                                            If strContactIDs.Count > 0 OrElse (CCommon.ToString(drAct("vcEmailSendTo")) IsNot Nothing) Then
                                                                strContactIDWebLead.Add(numContactId)
                                                                dtEmailBroadCast = objEmail.GetEmailMergeData(1, String.Join(",", strContactIDWebLead.ToArray()), CCommon.ToLong(dr("numDomainID")), 0, 0)

                                                                dtEmailBroadCastSendTo = objEmail.GetEmailMergeData(1, String.Join(",", strContactIDs.ToArray()), CCommon.ToLong(dr("numDomainID")), 0, 0)

                                                                Dim strTo As New List(Of String)
                                                                For Each drEmail As DataRow In dtEmailBroadCastSendTo.Rows
                                                                    strTo.Add(drEmail("ContactEmail"))
                                                                Next

                                                                If strTo.Count > 0 OrElse (CCommon.ToString(drAct("vcEmailSendTo")) IsNot Nothing) Then
                                                                    For Each iKey As KeyValuePair(Of String, String) In objEmail.AdditionalMergeFields
                                                                        If dtEmailBroadCast.Columns.Contains(iKey.Key) = False Then
                                                                            dtEmailBroadCast.Columns.Add(iKey.Key)
                                                                        End If
                                                                        'Assign values to rows in merge table
                                                                        For Each row As DataRow In dtEmailBroadCast.Rows
                                                                            If iKey.Key <> "ContactEmail" Then
                                                                                row(iKey.Key) = CCommon.ToString(iKey.Value)
                                                                            End If
                                                                        Next
                                                                    Next

                                                                    'Newly Added code
                                                                    'If (CCommon.ToString(drAct("vcEmailSendTo")) IsNot Nothing) Then
                                                                    '    Dim alEmail As New ArrayList
                                                                    '    Dim strContacts() As String = CCommon.ToString(drAct("vcEmailSendTo")).Split(",")
                                                                    '    'For j = 0 To strContacts.Length - 1

                                                                    '    'Next
                                                                    '    For index As Integer = 0 To strContacts.Length - 1
                                                                    '        alEmail.Add(strContacts(index))
                                                                    '    Next

                                                                    '    Dim iEnum As IEnumerator = alEmail.GetEnumerator()
                                                                    '    While iEnum.MoveNext()
                                                                    '        dr = dtEmailBroadCast.NewRow
                                                                    '        dr("ContactEmail") = iEnum.Current.ToString()
                                                                    '        dr("ContactFirstName") = ""
                                                                    '        dtEmailBroadCast.Rows.Add(dr)
                                                                    '    End While
                                                                    '    dtEmailBroadCast.AcceptChanges()
                                                                    'End If

                                                                    'end of Newly added code

                                                                    Dim strFinalTo As String = Nothing
                                                                    If CCommon.ToString(drAct("vcEmailSendTo")) = "" Or CCommon.ToString(drAct("vcEmailSendTo")) = Nothing Then
                                                                        strFinalTo = String.Join(",", strTo)
                                                                    Else
                                                                        If strTo.Count > 0 Then
                                                                            strFinalTo = String.Join(",", strTo) & "," & drAct("vcEmailSendTo")
                                                                        Else
                                                                            strFinalTo = drAct("vcEmailSendTo")
                                                                        End If

                                                                    End If

                                                                    Dim mailStatus As Boolean
                                                                    If CCommon.ToShort(drAct("tintIsAttachment")) = 1 Then
                                                                        'uncommnted by ss
                                                                        'objEmail.SendEmailWS(strSubject, strBody, "", dtRecordData.Rows(0).Item("vcEmailID"), String.Join(",", strTo), dtEmailBroadCast, dtTable, FromDisplayName:=dtRecordData.Rows(0)("ContactName"), DomainID:=objWF.DomainID)
                                                                        objEmail.AdditionalMergeFields.Add("Signature", dtRecordData.Rows(0)("Signature").ToString)
                                                                        mailStatus = objEmail.SendEmailWS(strSubject, strBody, "", dtRecordData.Rows(0).Item("vcEmailID"), strFinalTo, dtEmailBroadCast, dtBizDocAttachment, FromDisplayName:=dtRecordData.Rows(0)("ContactName"), DomainID:=objWF.DomainID, UserCntID:=lngUserCntID, tintEmailFrom:=tintEmailFrom)
                                                                        dtBizDocAttachment.Rows.Clear()
                                                                        dtBizDocAttachment.Columns.Clear()
                                                                    Else
                                                                        'objEmail.SendEmail(strSubject, strBody, "", dtRecordData.Rows(0).Item("vcEmailID"), String.Join(",", strTo), dtEmailBroadCast, FromDisplayName:=dtRecordData.Rows(0)("ContactName"), DomainID:=objWF.DomainID)
                                                                        objEmail.AdditionalMergeFields.Add("Signature", dtRecordData.Rows(0)("Signature").ToString)
                                                                        mailStatus = objEmail.SendEmailWFA(strSubject, strBody, "", dtRecordData.Rows(0).Item("vcEmailID"), strFinalTo, dtEmailBroadCast, FromDisplayName:=dtRecordData.Rows(0)("ContactName"), DomainID:=objWF.DomainID, UserId:=lngUserCntID, tintEmailFrom:=tintEmailFrom)

                                                                    End If

                                                                    ' WFDescription = "Email Sent to " & String.Join(",", strTo)
                                                                    If mailStatus = True Then
                                                                        If strFinalTo = "" Or strFinalTo = Nothing Then
                                                                            WFDescription = "Unable to send a mail.Receiver Email Address doesn't exist."
                                                                        Else
                                                                            WFDescription = "Email Sent to " & strFinalTo
                                                                        End If
                                                                        bitSuccess = True
                                                                    Else
                                                                        WFDescription = "Email not Sent.Please Configure SMTP details.GO to Global Settings->General  "
                                                                        bitSuccess = False
                                                                    End If

                                                                End If
                                                            End If
                                                        End If
                                                    Case CCommon.ToShort(enmWFAction.AssignActionItems)
                                                        Dim dtActionItem As DataTable
                                                        Dim objActionItem As New ActionItem
                                                        With objActionItem
                                                            .RowID = drAct("numTemplateID")
                                                        End With
                                                        dtActionItem = objActionItem.LoadThisActionItemTemplateData()

                                                        If dtActionItem.Rows.Count > 0 Then
                                                            With objActionItem
                                                                .CommID = 0
                                                                .Task = CLng(dtActionItem.Rows(0)("numTaskType"))

                                                                .ContactID = numContactId
                                                                .DivisionID = numDivisionId

                                                                .Details = ""
                                                                .AssignedTo = IIf(CShort(drAct("tintTicklerActionAssignedTo")) = 1, numRecOwner, numAssignedTo)
                                                                .UserCntID = numRecOwner
                                                                .DomainID = dr("numDomainID")
                                                                If .Task = 973 Then
                                                                    .BitClosed = 1
                                                                Else : .BitClosed = 0
                                                                End If
                                                                .CalendarName = ""
                                                                Dim strDate As DateTime = Date.UtcNow.AddDays(Convert.ToDouble(dtActionItem.Rows(0)("DueDays")))

                                                                .StartTime = CDate(strDate.ToString("MM/dd/yyyy 00:00:00") & "AM")
                                                                .EndTime = CDate(strDate.ToString("MM/dd/yyyy 23:59:59") & "PM")

                                                                .Activity = CLng(dtActionItem.Rows(0)("Activity"))
                                                                .Status = CLng(dtActionItem.Rows(0)("Priority"))
                                                                .Snooze = 0
                                                                .SnoozeStatus = 0
                                                                .Remainder = 0
                                                                .RemainderStatus = 0
                                                                .ClientTimeZoneOffset = 0
                                                                .bitOutlook = 0
                                                                .SendEmailTemplate = CBool(dtActionItem.Rows(0).Item("bitSendEmailTemp"))
                                                                .EmailTemplate = CLng(dtActionItem.Rows(0).Item("numEmailTemplate"))
                                                                .Hours = CShort(dtActionItem.Rows(0).Item("tintHours"))
                                                                .Alert = CBool(dtActionItem.Rows(0).Item("bitAlert"))
                                                                .ActivityId = 0
                                                                .FollowUpAnyTime = False
                                                            End With

                                                            Dim numcommId As Long = 0
                                                            numcommId = objActionItem.SaveCommunicationinfo()

                                                            'Added By Sachin Sadhu||Date:4thAug2014
                                                            'Purpose :To Added Ticker data in work Flow queue based on created Rules
                                                            '          Using Change tracking
                                                            Dim objWfA As New Workflow()
                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                            objWfA.UserCntID = numRecOwner
                                                            objWfA.RecordID = numcommId
                                                            objWfA.SaveWFActionItemsQueue()
                                                            'ss//end of code

                                                            'CAlerts.SendAlertToAssignee(6, numRecOwner, objActionItem.AssignedTo, numcommId, dr("numDomainID"))

                                                            WFDescription = "Action Items Created"
                                                            bitSuccess = True
                                                        End If
                                                    Case CCommon.ToShort(enmWFAction.UpdateFields)
                                                        Dim Sql As New System.Text.StringBuilder
                                                        Dim SqlFrom As New System.Text.StringBuilder
                                                        Dim SqlCondition As New System.Text.StringBuilder

                                                        'If CCommon.ToShort(dtWFList.Rows(0)("numFormID")) = 71 Then
                                                        '    objWF.FormID = 49
                                                        'Else
                                                        '    objWF.FormID = CCommon.ToShort(dtWFList.Rows(0)("numFormID"))
                                                        'End If
                                                        'Select Case CCommon.ToShort(dtWFList.Rows(0)("numFormID"))

                                                        Dim tempUpdate As New List(Of String)

                                                        Dim drWFActionUpdateFields() As DataRow = dtWFActionUpdateFields.Select("numWFActionID=" & drAct("numWFActionID"))

                                                        Select Case objWF.FormID
                                                            Case 68 'Organization
                                                                If drWFActionUpdateFields.Length > 0 Then
                                                                    Dim strFiels As String = ""
                                                                    For Each drCFL As DataRow In drWFActionUpdateFields
                                                                        Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))
                                                                        Dim boolInsert As Boolean = True
                                                                        If drFLCheck.Length > 0 Then
                                                                            If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                                                If drFLCheck(0)("vcLookBackTableName") = "AdditionalContactsInformation" Then 'Organization
                                                                                    Sql.Append("Update AdditionalContactsInformation a set ")
                                                                                    SqlCondition.Append(" FROM  DivisionMaster d where d.numDomainId = $1 AND d.numDivisionId = $2 AND d.numDivisionID=a.numDivisionId ")
                                                                                ElseIf drFLCheck(0)("vcLookBackTableName") = "CompanyInfo" Then 'Company Info
                                                                                    Sql.Append("Update CompanyInfo c set ")
                                                                                    SqlCondition.Append("  FROM  DivisionMaster d where d.numDomainId = $1 AND d.numDivisionId = $2 AND d.numCompanyId=c.numCompanyId ")
                                                                                Else
                                                                                    Sql.Append("Update DivisionMaster set ")
                                                                                    SqlCondition.Append(" where DivisionMaster.numDomainId = $1 AND DivisionMaster.numDivisionId = $2")
                                                                                End If
                                                                            Else 'Update Custom Fields

                                                                                If drFLCheck(0)("vcLookBackTableName") = "CFW_FLD_Values" Then 'Organization

                                                                                    boolInsert = objWF.WFCustomFieldsInsert(CCommon.ToLong(drCFL("numFieldID")), drCFL("vcValue"))
                                                                                    If boolInsert Then
                                                                                        Sql.Append("Update CFW_FLD_Values set ")
                                                                                        SqlCondition.Append(" where CFW_FLD_Values.Fld_ID = " & drCFL("numFieldID") & " AND CFW_FLD_Values.RecId = $2")
                                                                                    End If
                                                                                End If
                                                                            End If
                                                                        End If
                                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                                            tempUpdate.Add(String.Format("{0}='{1}'",
                                                                                                   drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                                        Else
                                                                            If boolInsert Then
                                                                                tempUpdate.Add(String.Format("{0}='{1}'",
                                                                                                "Fld_Value", drCFL("vcValue")))
                                                                            End If

                                                                        End If
                                                                        strFiels = strFiels & "," & drCFL("FieldName")
                                                                        SqlFrom.Append(String.Join(",", tempUpdate))
                                                                        Sql.Append(SqlFrom)
                                                                        Sql.Append(SqlCondition)
                                                                        objWF.textQuery = Sql.ToString()
                                                                        objWF.WFConditionQueryExecute(2)
                                                                        WFDescription = "Fields Updated:" & strFiels
                                                                        bitSuccess = True
                                                                        If bitSuccess = True And objWF.FormID = 68 And drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then
                                                                            'Added By Sachin Sadhu||Date:23ndMay12014
                                                                            'Purpose :To Added Organization data in work Flow queue based on created Rules
                                                                            '          Using Change tracking
                                                                            Dim objWfA As New Workflow()
                                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                            objWfA.UserCntID = numRecOwner
                                                                            objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                            objWfA.SaveWFOrganizationQueue()
                                                                            'end of code
                                                                        ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "AdditionalContactsInformation" Then
                                                                            Dim objWfA As New Workflow()
                                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                            objWfA.UserCntID = numRecOwner
                                                                            objWfA.RecordID = numContactId
                                                                            objWfA.SaveWFContactQueue()
                                                                        Else
                                                                            Dim objWfA As New Workflow()
                                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                            objWfA.UserCntID = numRecOwner
                                                                            objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                            objWfA.SaveWFOrganizationQueue()
                                                                        End If
                                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = True And bitSuccess = True Then ' TRIGGER Custom fields based WF rules
                                                                            Dim objWFCF As New Workflow()
                                                                            objWFCF.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                            objWFCF.UserCntID = numRecOwner
                                                                            objWFCF.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                            objWFCF.FormFieldId = CCommon.ToLong(drCFL("numFieldID"))
                                                                            objWFCF.SaveWFCFOrganizationQueue()
                                                                        End If
                                                                        Sql.Clear()
                                                                        SqlFrom.Clear()
                                                                        SqlCondition.Clear()
                                                                        tempUpdate.Clear()
                                                                    Next
                                                                End If
                                                            Case 70 'Opportunities & Orders
                                                                If drWFActionUpdateFields.Length > 0 Then
                                                                    Dim strFiels As String = ""
                                                                    For Each drCFL As DataRow In drWFActionUpdateFields
                                                                        Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))
                                                                        Dim boolInsert As Boolean = True
                                                                        If drFLCheck.Length > 0 Then
                                                                            If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                                                If drFLCheck(0)("vcLookBackTableName") = "CompanyInfo" Then 'Organizatin
                                                                                    Sql.Append("Update CompanyInfo c set ")
                                                                                    SqlCondition.Append(" FROM OpportunityMaster o inner join DivisionMaster D on d.numdivisionID=o.numDivisionId where o.numDomainId = $1 AND o.numOppId=$2 AND c.numCompanyId=d.numCompanyId")
                                                                                ElseIf drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization
                                                                                    Sql.Append("Update DivisionMaster d set ")
                                                                                    SqlCondition.Append(" FROM CompanyInfo c, OpportunityMaster o where  o.numDomainId = $1 AND o.numOppId=$2 AND d.numdivisionID=o.numDivisionId AND c.numCompanyId=d.numCompanyId")

                                                                                ElseIf drFLCheck(0)("vcLookBackTableName") = "OpportunityBizDocs" Then 'BizDocs
                                                                                    Sql.Append("Update OpportunityBizDocs b set ")
                                                                                    'SqlCondition.Append(" FROM OpportunityBizDocs b inner join OpportunityMaster o on o.numOppId=b.numOppId  where  b.numOppBizDocsId=$2")
                                                                                    SqlCondition.Append(" FROM OpportunityMaster o where  o.numDomainId = $1 AND o.numOppId=$2 AND o.numOppId=b.numOppId")

                                                                                ElseIf drFLCheck(0)("vcLookBackTableName") = "StagePercentageDetails" Then 'StagePercentageDetails
                                                                                    Sql.Append("Update StagePercentageDetails b set ")
                                                                                    'SqlCondition.Append(" FROM OpportunityBizDocs b inner join OpportunityMaster o on o.numOppId=b.numOppId  where  b.numOppBizDocsId=$2")
                                                                                    SqlCondition.Append(" FROM OpportunityMaster o where  o.numDomainId = $1 AND o.numOppId=$2 AND o.numOppId=b.numOppId")
                                                                                Else
                                                                                    Sql.Append("Update OpportunityMaster set ") 'Orders Module
                                                                                    SqlCondition.Append(" where OpportunityMaster.numDomainId = $1 AND OpportunityMaster.numOppID = $2")

                                                                                End If
                                                                            Else
                                                                                If drFLCheck(0)("vcLookBackTableName") = "CFW_Fld_Values_Opp" Then 'Orders

                                                                                    boolInsert = objWF.WFCustomFieldsInsert(CCommon.ToLong(drCFL("numFieldID")), drCFL("vcValue"))
                                                                                    If boolInsert Then
                                                                                        Sql.Append("Update CFW_Fld_Values_Opp set ")
                                                                                        SqlCondition.Append(" where CFW_Fld_Values_Opp.Fld_ID = " & drCFL("numFieldID") & " AND CFW_Fld_Values_Opp.RecId = $2")
                                                                                    End If
                                                                                End If
                                                                            End If
                                                                            If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                                                tempUpdate.Add(String.Format("{0}='{1}'",
                                                                                                       drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                                            Else
                                                                                If boolInsert Then
                                                                                    tempUpdate.Add(String.Format("{0}='{1}'",
                                                                                                    "Fld_Value", drCFL("vcValue")))
                                                                                End If
                                                                            End If

                                                                            SqlFrom.Append(String.Join(",", tempUpdate))

                                                                            Sql.Append(SqlFrom)
                                                                            Sql.Append(SqlCondition)
                                                                            objWF.textQuery = Sql.ToString()
                                                                            objWF.WFConditionQueryExecute(2)
                                                                            strFiels = strFiels & "," & drCFL("FieldName")
                                                                            WFDescription = "Fields Updated:" & strFiels
                                                                            bitSuccess = True
                                                                            Sql.Clear()
                                                                            SqlFrom.Clear()
                                                                            SqlCondition.Clear()
                                                                            tempUpdate.Clear()

                                                                            If bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "OpportunityMaster" Then 'Order Module
                                                                                'Added By Sachin Sadhu||Date:23ndMay12014
                                                                                'Purpose :To Added Order data in work Flow queue based on created Rules
                                                                                '          Using Change tracking
                                                                                Dim objWfA As New Workflow()
                                                                                objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                                objWfA.UserCntID = numRecOwner
                                                                                objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                                objWfA.SaveWFOrderQueue()
                                                                                'end of code
                                                                            ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "OpportunityBizDocs" Then 'BizDocs                                                                           
                                                                                Dim objWfA As New Workflow()
                                                                                objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                                objWfA.UserCntID = numRecOwner
                                                                                objWfA.RecordID = numOppBizDOCIDPC
                                                                                objWfA.SaveWFBizDocQueue()
                                                                            ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization                                                                           
                                                                                Dim objWfA As New Workflow()
                                                                                objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                                objWfA.UserCntID = numRecOwner
                                                                                objWfA.RecordID = numDivisionId
                                                                                objWfA.SaveWFOrganizationQueue()
                                                                            ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "StagePercentageDetails" Then 'Busniess Process                                                                           
                                                                                Dim objWfA As New Workflow()
                                                                                objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                                objWfA.UserCntID = numRecOwner
                                                                                objWfA.RecordID = numStageIDPC
                                                                                objWfA.SaveWFBusinessProcessQueue()
                                                                            Else 'orders
                                                                                Dim objWfA As New Workflow()
                                                                                objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                                objWfA.UserCntID = numRecOwner
                                                                                objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                                objWfA.SaveWFOrderQueue()

                                                                            End If
                                                                            If CCommon.ToBool(drFLCheck(0)("bitCustom")) = True And bitSuccess = True Then ' TRIGGER Custom fields based WF rules
                                                                                Dim objWFCF As New Workflow()
                                                                                objWFCF.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                                objWFCF.UserCntID = numRecOwner
                                                                                objWFCF.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                                objWFCF.FormFieldId = CCommon.ToLong(drCFL("numFieldID"))
                                                                                objWFCF.SaveWFCFOrdersQueue()
                                                                            End If
                                                                        End If
                                                                    Next
                                                                End If

                                                            Case 49 'BizDocs

                                                                If drWFActionUpdateFields.Length > 0 Then
                                                                    Dim strFiels As String = ""
                                                                    For Each drCFL As DataRow In drWFActionUpdateFields
                                                                        Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))

                                                                        If drFLCheck.Length > 0 Then
                                                                            If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                                                If drFLCheck(0)("vcLookBackTableName") = "OpportunityMaster" Then 'Orders
                                                                                    Sql.Append("Update OpportunityMaster o set ")
                                                                                    SqlCondition.Append(" FROM OpportunityBizDocs b where  b.numOppBizDocsId=$2 AND o.numOppId=b.numOppId")
                                                                                ElseIf drFLCheck(0)("vcLookBackTableName") = "CompanyInfo" Then
                                                                                    Sql.Append("Update CompanyInfo c set ") 'organization
                                                                                    SqlCondition.Append(" FROM OpportunityBizDocs b inner join OpportunityMaster o on o.numOppId=b.numOppId inner join DivisionMaster D on d.numdivisionID=o.numDivisionId where  b.numOppBizDocsId=$2 AND c.numCompanyId=d.numCompanyId")
                                                                                ElseIf drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization
                                                                                    Sql.Append("Update DivisionMaster d set ")
                                                                                    SqlCondition.Append("FROM CompanyInfo c, OpportunityBizDocs b inner join OpportunityMaster o on o.numOppId=b.numOppId where b.numOppBizDocsId=$2 AND d.numdivisionID=o.numDivisionId AND c.numCompanyId=d.numCompanyId")
                                                                                Else 'BizDocs module
                                                                                    Sql.Append(" Update OpportunityBizDocs set   ")
                                                                                    SqlCondition.Append(" where OpportunityBizDocs.numOppBizDocsId = $2")

                                                                                End If

                                                                            End If
                                                                            If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                                                tempUpdate.Add(String.Format("{0}='{1}'",
                                                                                                       drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                                            End If

                                                                            SqlFrom.Append(String.Join(",", tempUpdate))

                                                                            Sql.Append(SqlFrom)
                                                                            Sql.Append(SqlCondition)
                                                                            objWF.textQuery = Sql.ToString()
                                                                            objWF.WFConditionQueryExecute(2)

                                                                            strFiels = strFiels & "," & drCFL("FieldName")
                                                                            WFDescription = "Fields Updated:" & strFiels
                                                                            bitSuccess = True

                                                                            Sql.Clear()
                                                                            SqlFrom.Clear()
                                                                            SqlCondition.Clear()
                                                                            tempUpdate.Clear()
                                                                            If bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "OpportunityMaster" Then 'Order Module

                                                                                Dim objWfA As New Workflow()
                                                                                objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                                objWfA.UserCntID = numRecOwner
                                                                                objWfA.RecordID = numOppIDPC
                                                                                objWfA.SaveWFOrderQueue()
                                                                                'end of code
                                                                            ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "OpportunityBizDocs" Then 'BizDocs                                                                           
                                                                                Dim objWfA As New Workflow()
                                                                                objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                                objWfA.UserCntID = numRecOwner
                                                                                objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                                objWfA.SaveWFBizDocQueue()
                                                                            ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization                                                                           
                                                                                Dim objWfA As New Workflow()
                                                                                objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                                objWfA.UserCntID = numRecOwner
                                                                                objWfA.RecordID = numDivisionId
                                                                                objWfA.SaveWFOrganizationQueue()
                                                                            Else 'BizDocs
                                                                                Dim objWfA As New Workflow()
                                                                                objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                                objWfA.UserCntID = numRecOwner
                                                                                objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                                objWfA.SaveWFBizDocQueue()
                                                                            End If

                                                                        End If
                                                                    Next
                                                                End If
                                                            Case 94 'Business Process
                                                                Sql.Append("Update StagePercentageDetails set ")
                                                                SqlCondition.Append(" where StagePercentageDetails.numDomainId = $1 AND StagePercentageDetails.numStageDetailsId = $2")
                                                                If drWFActionUpdateFields.Length > 0 Then
                                                                    For Each drCFL As DataRow In drWFActionUpdateFields
                                                                        Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))

                                                                        If drFLCheck.Length > 0 Then
                                                                            If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                                                tempUpdate.Add(String.Format("{0}='{1}'",
                                                                                                       drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                                            End If
                                                                        End If
                                                                    Next

                                                                    SqlFrom.Append(String.Join(",", tempUpdate))

                                                                    Sql.Append(SqlFrom)
                                                                    Sql.Append(SqlCondition)
                                                                    objWF.textQuery = Sql.ToString()
                                                                    objWF.WFConditionQueryExecute(2)

                                                                    WFDescription = "Fields Updated"
                                                                    bitSuccess = True

                                                                    If bitSuccess = True Then 'Business Process

                                                                        Dim objWfA As New Workflow()
                                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                        objWfA.UserCntID = numRecOwner
                                                                        objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                        objWfA.SaveWFBusinessProcessQueue()
                                                                        'end of code
                                                                    End If
                                                                End If
                                                            Case 69 'Contacts                                                           
                                                                If drWFActionUpdateFields.Length > 0 Then
                                                                    Dim strFiels As String = ""
                                                                    For Each drCFL As DataRow In drWFActionUpdateFields
                                                                        Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))
                                                                        Dim boolInsert As Boolean = True
                                                                        If drFLCheck.Length > 0 Then
                                                                            If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                                                If drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization
                                                                                    Sql.Append("Update DivisionMaster c set ")
                                                                                    SqlCondition.Append(" FROM AdditionalContactsInformation o where o.numDomainId = $1 AND o.numContactId=$2 AND c.numdivisionID=o.numDivisionId")
                                                                                ElseIf drFLCheck(0)("vcLookBackTableName") = "CompanyInfo" Then 'Company Info
                                                                                    Sql.Append("Update CompanyInfo c set ")
                                                                                    SqlCondition.Append(" FROM AdditionalContactsInformation o inner join DivisionMaster D on d.numdivisionID=o.numDivisionId where  o.numDomainId = $1 AND o.numContactId=$2 AND c.numCompanyId=d.numCompanyId")
                                                                                Else
                                                                                    Sql.Append("Update AdditionalContactsInformation set ")
                                                                                    SqlCondition.Append(" where AdditionalContactsInformation.numDomainId = $1 AND AdditionalContactsInformation.numContactId = $2")
                                                                                End If
                                                                            Else
                                                                                If drFLCheck(0)("vcLookBackTableName") = "CFW_FLD_Values_Cont" Then 'Contacts

                                                                                    boolInsert = objWF.WFCustomFieldsInsert(CCommon.ToLong(drCFL("numFieldID")), drCFL("vcValue"))
                                                                                    If boolInsert Then
                                                                                        Sql.Append("Update CFW_FLD_Values_Cont set ")
                                                                                        SqlCondition.Append(" where CFW_FLD_Values_Cont.Fld_ID = " & drCFL("numFieldID") & " AND CFW_FLD_Values_Cont.RecId = $2")
                                                                                    End If
                                                                                End If

                                                                            End If
                                                                        End If
                                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                                            tempUpdate.Add(String.Format("{0}='{1}'",
                                                                                                   drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                                        Else
                                                                            If boolInsert Then
                                                                                tempUpdate.Add(String.Format("{0}='{1}'",
                                                                                                "Fld_Value", drCFL("vcValue")))
                                                                            End If
                                                                        End If
                                                                        SqlFrom.Append(String.Join(",", tempUpdate))
                                                                        Sql.Append(SqlFrom)
                                                                        Sql.Append(SqlCondition)
                                                                        objWF.textQuery = Sql.ToString()
                                                                        objWF.WFConditionQueryExecute(2)
                                                                        strFiels = strFiels & "," & drCFL("FieldName")
                                                                        WFDescription = "Fields Updated:" & strFiels
                                                                        bitSuccess = True
                                                                        Sql.Clear()
                                                                        SqlFrom.Clear()
                                                                        SqlCondition.Clear()
                                                                        tempUpdate.Clear()
                                                                        If bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "AdditionalContactsInformation" Then 'Contacts
                                                                            Dim objWfA As New Workflow()
                                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                            objWfA.UserCntID = numRecOwner
                                                                            objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                            objWfA.SaveWFContactQueue()
                                                                            'end of code                                                                
                                                                        ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'BizDocs                                                                           
                                                                            Dim objWfA As New Workflow()
                                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                            objWfA.UserCntID = numRecOwner
                                                                            objWfA.RecordID = numDivisionId
                                                                            objWfA.SaveWFOrganizationQueue()
                                                                        Else 'Contacts
                                                                            Dim objWfA As New Workflow()
                                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                            objWfA.UserCntID = numRecOwner
                                                                            objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                            objWfA.SaveWFContactQueue()
                                                                        End If
                                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = True And bitSuccess = True Then ' TRIGGER Custom fields based WF rules
                                                                            Dim objWFCF As New Workflow()
                                                                            objWFCF.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                            objWFCF.UserCntID = numRecOwner
                                                                            objWFCF.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                            objWFCF.FormFieldId = CCommon.ToLong(drCFL("numFieldID"))
                                                                            objWFCF.SaveWFCFContactsQueue()
                                                                        End If
                                                                    Next
                                                                End If

                                                            Case 73 'Projects

                                                                If drWFActionUpdateFields.Length > 0 Then
                                                                    Dim strFiels As String = ""
                                                                    For Each drCFL As DataRow In drWFActionUpdateFields
                                                                        Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))
                                                                        Dim boolInsert As Boolean = True
                                                                        If drFLCheck.Length > 0 Then
                                                                            If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                                                If drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization
                                                                                    Sql.Append("Update DivisionMaster c set ")
                                                                                    SqlCondition.Append(" FROM ProjectsMaster o  where  o.numDomainId = $1 AND o.numProId=$2 AND c.numdivisionID=o.numDivisionId")
                                                                                ElseIf drFLCheck(0)("vcLookBackTableName") = "CompanyInfo" Then 'Company Info
                                                                                    Sql.Append("Update CompanyInfo c set ")
                                                                                    SqlCondition.Append(" FROM ProjectsMaster o inner join DivisionMaster D on d.numdivisionID=o.numDivisionId where o.numDomainId = $1 AND o.numProId=$2 AND c.numCompanyId=d.numCompanyId")
                                                                                ElseIf drFLCheck(0)("vcLookBackTableName") = "StagePercentageDetails" Then 'StagePercentageDetails
                                                                                    Sql.Append("Update StagePercentageDetails b set ")
                                                                                    'SqlCondition.Append(" FROM OpportunityBizDocs b inner join OpportunityMaster o on o.numOppId=b.numOppId  where  b.numOppBizDocsId=$2")
                                                                                    SqlCondition.Append(" FROM ProjectsMaster o where  o.numDomainId = $1 AND o.numProId=$2 AND o.numProId=b.numProjectID")
                                                                                Else
                                                                                    Sql.Append("Update ProjectsMaster set ")
                                                                                    SqlCondition.Append(" where ProjectsMaster.numDomainId = $1 AND ProjectsMaster.numProId = $2")
                                                                                End If
                                                                            Else
                                                                                If drFLCheck(0)("vcLookBackTableName") = "CFW_FLD_Values_Pro" Then 'Projects

                                                                                    boolInsert = objWF.WFCustomFieldsInsert(CCommon.ToLong(drCFL("numFieldID")), drCFL("vcValue"))
                                                                                    If boolInsert Then
                                                                                        Sql.Append("Update CFW_FLD_Values_Pro set ")
                                                                                        SqlCondition.Append(" where CFW_FLD_Values_Pro.Fld_ID = " & drCFL("numFieldID") & " AND CFW_FLD_Values_Pro.RecId = $2")
                                                                                    End If
                                                                                End If
                                                                            End If
                                                                        End If
                                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                                            tempUpdate.Add(String.Format("{0}='{1}'",
                                                                                                   drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                                        Else
                                                                            If boolInsert Then
                                                                                tempUpdate.Add(String.Format("{0}='{1}'",
                                                                                                "Fld_Value", drCFL("vcValue")))
                                                                            End If
                                                                        End If
                                                                        SqlFrom.Append(String.Join(",", tempUpdate))
                                                                        Sql.Append(SqlFrom)
                                                                        Sql.Append(SqlCondition)
                                                                        objWF.textQuery = Sql.ToString()
                                                                        objWF.WFConditionQueryExecute(2)
                                                                        strFiels = strFiels & "," & drCFL("FieldName")
                                                                        WFDescription = "Fields Updated:" & strFiels
                                                                        bitSuccess = True
                                                                        Sql.Clear()
                                                                        SqlFrom.Clear()
                                                                        SqlCondition.Clear()
                                                                        tempUpdate.Clear()

                                                                        If bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "ProjectsMaster" Then 'Order Module

                                                                            Dim objWfA As New Workflow()
                                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                            objWfA.UserCntID = numRecOwner
                                                                            objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                            objWfA.SaveWFProjectsQueue()
                                                                            'end of code

                                                                        ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization                                                                           
                                                                            Dim objWfA As New Workflow()
                                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                            objWfA.UserCntID = numRecOwner
                                                                            objWfA.RecordID = numDivisionId
                                                                            objWfA.SaveWFOrganizationQueue()
                                                                        ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "StagePercentageDetails" Then 'Busniess Process                                                                           
                                                                            Dim objWfA As New Workflow()
                                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                            objWfA.UserCntID = numRecOwner
                                                                            objWfA.RecordID = numStageIDPC
                                                                            objWfA.SaveWFBusinessProcessQueue()
                                                                        Else 'Projects
                                                                            Dim objWfA As New Workflow()
                                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                            objWfA.UserCntID = numRecOwner
                                                                            objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                            objWfA.SaveWFProjectsQueue()

                                                                        End If

                                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = True And bitSuccess = True Then ' TRIGGER Custom fields based WF rules
                                                                            Dim objWFCF As New Workflow()
                                                                            objWFCF.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                            objWFCF.UserCntID = numRecOwner
                                                                            objWFCF.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                            objWFCF.FormFieldId = CCommon.ToLong(drCFL("numFieldID"))
                                                                            objWFCF.SaveWFCFProjectsQueue()
                                                                        End If
                                                                    Next
                                                                End If

                                                            Case 72 'Cases

                                                                If drWFActionUpdateFields.Length > 0 Then
                                                                    Dim strFiels As String = ""
                                                                    For Each drCFL As DataRow In drWFActionUpdateFields
                                                                        Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))
                                                                        Dim boolInsert As Boolean = True
                                                                        If drFLCheck.Length > 0 Then
                                                                            If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                                                If drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization
                                                                                    Sql.Append("Update DivisionMaster c set ")
                                                                                    SqlCondition.Append(" FROM Cases o where o.numDomainId = $1 AND o.numCaseId=$2 AND c.numdivisionID=o.numDivisionId")
                                                                                ElseIf drFLCheck(0)("vcLookBackTableName") = "CompanyInfo" Then 'Company Info
                                                                                    Sql.Append("Update CompanyInfo c set ")
                                                                                    SqlCondition.Append(" FROM Cases o inner join DivisionMaster D on d.numdivisionID=o.numDivisionId where o.numCaseId=$2 AND o.numDomainId = $1 AND c.numCompanyId=d.numCompanyId")
                                                                                ElseIf drFLCheck(0)("vcLookBackTableName") = "OpportunityMaster" Then 'Orders
                                                                                    Sql.Append("Update OpportunityMaster o set ")
                                                                                    SqlCondition.Append(" FROM Cases c inner join CaseOpportunities cp on c.numCaseId=cp.numCaseId where c.numDomainId = $1 AND c.numCaseId = $2 AND o.numOppId=cp.numOppId")
                                                                                Else
                                                                                    Sql.Append("Update Cases set ")
                                                                                    SqlCondition.Append(" where Cases.numDomainId = $1 AND Cases.numCaseId = $2")
                                                                                End If
                                                                            Else
                                                                                If drFLCheck(0)("vcLookBackTableName") = "CFW_FLD_Values_Case" Then 'Cases

                                                                                    boolInsert = objWF.WFCustomFieldsInsert(CCommon.ToLong(drCFL("numFieldID")), drCFL("vcValue"))
                                                                                    If boolInsert Then
                                                                                        Sql.Append("Update CFW_FLD_Values_Case set ")
                                                                                        SqlCondition.Append(" where CFW_FLD_Values_Case.Fld_ID = " & drCFL("numFieldID") & " AND CFW_FLD_Values_Case.RecId = $2")
                                                                                    End If


                                                                                End If
                                                                            End If
                                                                        End If
                                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                                            tempUpdate.Add(String.Format("{0}='{1}'",
                                                                                                   drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                                        Else
                                                                            If boolInsert Then
                                                                                tempUpdate.Add(String.Format("{0}='{1}'",
                                                                                                "Fld_Value", drCFL("vcValue")))
                                                                            End If
                                                                        End If
                                                                        SqlFrom.Append(String.Join(",", tempUpdate))
                                                                        Sql.Append(SqlFrom)
                                                                        Sql.Append(SqlCondition)
                                                                        objWF.textQuery = Sql.ToString()
                                                                        objWF.WFConditionQueryExecute(2)
                                                                        strFiels = strFiels & "," & drCFL("FieldName")
                                                                        WFDescription = "Fields Updated:" & strFiels
                                                                        bitSuccess = True
                                                                        Sql.Clear()
                                                                        SqlFrom.Clear()
                                                                        SqlCondition.Clear()
                                                                        tempUpdate.Clear()

                                                                        If bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "OpportunityMaster" Then 'Order Module

                                                                            Dim objWfA As New Workflow()
                                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                            objWfA.UserCntID = numRecOwner
                                                                            objWfA.RecordID = numOppIDPC
                                                                            objWfA.SaveWFOrderQueue()
                                                                            'end of code
                                                                        ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "Cases" Then 'Cases                                                                           
                                                                            Dim objWfA As New Workflow()
                                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                            objWfA.UserCntID = numRecOwner
                                                                            objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                            objWfA.SaveWFCasesQueue()
                                                                        ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization                                                                           
                                                                            Dim objWfA As New Workflow()
                                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                            objWfA.UserCntID = numRecOwner
                                                                            objWfA.RecordID = numDivisionId
                                                                            objWfA.SaveWFOrganizationQueue()

                                                                        Else 'Cases
                                                                            Dim objWfA As New Workflow()
                                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                            objWfA.UserCntID = numRecOwner
                                                                            objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                            objWfA.SaveWFCasesQueue()

                                                                        End If
                                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = True And bitSuccess = True Then ' TRIGGER Custom fields based WF rules
                                                                            Dim objWFCF As New Workflow()
                                                                            objWFCF.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                            objWFCF.UserCntID = numRecOwner
                                                                            objWFCF.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                            objWFCF.FormFieldId = CCommon.ToLong(drCFL("numFieldID"))
                                                                            objWFCF.SaveWFCFCasesQueue()
                                                                        End If
                                                                    Next
                                                                End If
                                                            Case 124 'Actiom Items(Tickler)

                                                                If drWFActionUpdateFields.Length > 0 Then
                                                                    Dim strFiels As String = ""
                                                                    For Each drCFL As DataRow In drWFActionUpdateFields
                                                                        Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))

                                                                        If drFLCheck.Length > 0 Then
                                                                            If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                                                If drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization
                                                                                    Sql.Append("Update DivisionMaster c set ")
                                                                                    SqlCondition.Append(" FROM Communication o where  o.numCommId=$2 AND o.numDomainId = $1 AND c.numdivisionID=o.numDivisionId")
                                                                                ElseIf drFLCheck(0)("vcLookBackTableName") = "CompanyInfo" Then 'Company Info
                                                                                    Sql.Append("Update CompanyInfo c set ")
                                                                                    SqlCondition.Append(" FROM Communication o inner join DivisionMaster D on d.numdivisionID=o.numDivisionId where o.numDomainId = $1 AND o.numCommId=$2 AND c.numCompanyId=d.numCompanyId")
                                                                                Else
                                                                                    Sql.Append("Update Communication set ")
                                                                                    SqlCondition.Append(" where Communication.numDomainId = $1 AND Communication.numCommId = $2")
                                                                                End If

                                                                            End If
                                                                        End If
                                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                                            tempUpdate.Add(String.Format("{0}='{1}'",
                                                                                                   drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                                        End If
                                                                        SqlFrom.Append(String.Join(",", tempUpdate))
                                                                        Sql.Append(SqlFrom)
                                                                        Sql.Append(SqlCondition)
                                                                        objWF.textQuery = Sql.ToString()
                                                                        objWF.WFConditionQueryExecute(2)
                                                                        strFiels = strFiels & "," & drCFL("FieldName")
                                                                        WFDescription = "Fields Updated:" & strFiels
                                                                        bitSuccess = True
                                                                        Sql.Clear()
                                                                        SqlFrom.Clear()
                                                                        SqlCondition.Clear()
                                                                        tempUpdate.Clear()

                                                                        If bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "Communication" Then 'Order Module
                                                                            Dim objWfA As New Workflow()
                                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                            objWfA.UserCntID = numRecOwner
                                                                            objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                            objWfA.SaveWFActionItemsQueue()
                                                                            'end of code

                                                                        ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization                                                                           
                                                                            Dim objWfA As New Workflow()
                                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                            objWfA.UserCntID = numRecOwner
                                                                            objWfA.RecordID = numDivisionId
                                                                            objWfA.SaveWFOrganizationQueue()

                                                                        Else 'Tickler
                                                                            Dim objWfA As New Workflow()
                                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                            objWfA.UserCntID = numRecOwner
                                                                            objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                                            objWfA.SaveWFActionItemsQueue()

                                                                        End If
                                                                    Next
                                                                End If
                                                        End Select
                                                    Case CCommon.ToShort(enmWFAction.DisplayAlertMessage)

                                                        ' Dim cph As ContentPlaceHolder = DirectCast(Me.Master.FindControl("TabsPlaceHolder"), ContentPlaceHolder)
                                                        objWF.vcAlertMessage = CCommon.ToString(drAct("vcAlertMessage").ToString)
                                                        objWF.WFID = CCommon.ToLong(dr("numWFID"))
                                                        objWF.intAlertStatus = 1
                                                        objWF.SaveWorkFlowAlerts()
                                                        WFDescription = "Alert is shown"
                                                        bitSuccess = True
                                                    Case CCommon.ToShort(enmWFAction.BizDocApproval)
                                                        Try
                                                            Dim objDocuments As New DocumentList
                                                            Dim strReceiverName As String() = drAct("vcApproval").Split(",")
                                                            Dim k As Integer = 0
                                                            For k = 0 To strReceiverName.Length - 1

                                                                objDocuments.GenDocID = CCommon.ToLong(dr("numRecordID"))
                                                                objDocuments.ContactID = CCommon.ToLong(strReceiverName(k))
                                                                objDocuments.CDocType = "B"
                                                                objDocuments.byteMode = 1
                                                                objDocuments.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                objDocuments.UserCntID = numRecOwner
                                                                objDocuments.ManageApprovers()

                                                                Dim objOppBizDocs As New OppBizDocs
                                                                objOppBizDocs.OppBizDocId = CCommon.ToLong(dr("numRecordID"))
                                                                objOppBizDocs.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                Dim dsOppData As DataSet = objOppBizDocs.GetBizDocsDetails

                                                                'Send Approval requeset via Mail
                                                                Dim ObjCmn As New CCommon
                                                                Dim objSendMail As New Email
                                                                Dim dtDocDetails As DataTable = objSendMail.GetEmailTemplateByCode("#SYS#DOC_APPROVAL_REQUEST", CCommon.ToLong(dr("numDomainID")))
                                                                Dim objUserAccess As New UserAccess
                                                                objUserAccess.ContactID = numRecOwner
                                                                objUserAccess.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                Dim dtUser As DataTable = objUserAccess.GetExtranetUserDetails()
                                                                Dim strPortalUrl As String = "http://portal.bizautomation.com/Login.aspx"
                                                                Dim strPortalApprovalLink As String
                                                                If dtUser.Rows.Count = 1 Then

                                                                    strPortalApprovalLink = CCommon.ToString(strPortalUrl) & QueryEncryption.EncryptQueryString("&from=email&u=" + CCommon.ToString(dtUser.Rows(0)("vcEmail")) + "&p=" + ObjCmn.Encrypt(CCommon.ToString(dtUser.Rows(0)("vcPassword"))))
                                                                    strPortalApprovalLink = "<a href=" & strPortalApprovalLink & " target='_blank' >click here</a>"

                                                                End If

                                                                If dtDocDetails.Rows.Count > 0 Then
                                                                    Dim strBody As String = dtDocDetails.Rows(0).Item("vcDocdesc")
                                                                    Dim strSubject As String = IIf(IsDBNull(dtDocDetails.Rows(0).Item("vcSubject")), "", dtDocDetails.Rows(0).Item("vcSubject"))

                                                                    Dim objEmail As New Email
                                                                    objEmail.AdditionalMergeFields.Add("DocumentName", dsOppData.Tables(0).Rows(0).Item("vcBizDocID"))
                                                                    objEmail.AdditionalMergeFields.Add("LoggedInUser", dtRecordData.Rows(0)("ContactName"))
                                                                    objEmail.AdditionalMergeFields.Add("PortalDirectLoginLink", strPortalApprovalLink)
                                                                    objEmail.AdditionalMergeFields.Add("Signature", dtRecordData.Rows(0)("Signature"))
                                                                    If strBody.ToLower().Contains("##ContactOpt-OutLink##".ToLower()) Then
                                                                        objEmail.AdditionalMergeFields.Add("ContactOpt-OutLink", "")
                                                                    End If

                                                                    Dim dtEmailBroadCast As New DataTable

                                                                    Dim EmailToType() As String = drAct("vcApproval").ToString().Split(",")
                                                                    ' Dim strContactIDs As New ArrayList

                                                                    'If EmailToType.Contains(1) Then 'Owner of trigger record
                                                                    '    strContactIDs.Add(numRecOwner)
                                                                    'End If

                                                                    'If EmailToType.Contains(2) Then 'Assignee of trigger record
                                                                    '    strContactIDs.Add(numAssignedTo)
                                                                    'End If

                                                                    'If EmailToType.Contains(3) Then 'Primary contact of trigger record
                                                                    '    strContactIDs.Add(numContactId)
                                                                    'End If

                                                                    ' If strContactIDs.Count > 0 Then
                                                                    dtEmailBroadCast = objEmail.GetEmailMergeData(1, String.Join(",", EmailToType.ToArray()), CCommon.ToLong(dr("numDomainID")), 0, 0)

                                                                    Dim strTo As New List(Of String)
                                                                    For Each drEmail As DataRow In dtEmailBroadCast.Rows
                                                                        strTo.Add(drEmail("ContactEmail"))
                                                                    Next

                                                                    If strTo.Count > 0 Then
                                                                        For Each iKey As Object In objEmail.AdditionalMergeFields.Keys
                                                                            If dtEmailBroadCast.Columns.Contains(iKey.ToString) = False Then
                                                                                dtEmailBroadCast.Columns.Add(iKey.ToString)
                                                                            End If
                                                                            'Assign values to rows in merge table
                                                                            For Each row As DataRow In dtEmailBroadCast.Rows
                                                                                If iKey.ToString <> "ContactEmail" Then
                                                                                    ' row(iKey.ToString) = CCommon.ToString(iKey)
                                                                                    row(iKey.ToString) = CCommon.ToString(objEmail.AdditionalMergeFields(iKey))
                                                                                End If
                                                                            Next
                                                                        Next

                                                                        objEmail.SendEmail(strSubject, strBody, "", dtRecordData.Rows(0).Item("vcEmailID"), String.Join(",", strTo), dtEmailBroadCast, FromDisplayName:=dtRecordData.Rows(0)("ContactName"), DomainID:=objWF.DomainID)

                                                                    End If
                                                                    ' End If
                                                                End If
                                                                'end of Request
                                                            Next

                                                            WFDescription = "BizDoc Approval Request sent to Tickler"
                                                            bitSuccess = True

                                                        Catch ex As Exception
                                                            If ex.Message = "No_EXTRANET" Then
                                                                WFDescription = "Can not add selected contact to approver list. Your option is to add contact as external users from ""Administrator->User Administration->External Users"" and try again."
                                                                bitSuccess = False
                                                                Exit Sub
                                                            Else
                                                                Throw ex
                                                            End If
                                                        End Try
                                                    Case CCommon.ToShort(enmWFAction.CreateBizDoc)
                                                        Dim dtCheckBizDocs As New DataTable()
                                                        dtCheckBizDocs = CheckDuplicateBizDocs(CCommon.ToLong(dr("numRecordID")), CCommon.ToLong(dr("numDomainID")), CCommon.ToShort(drAct("numOppType")), numRecOwner, CCommon.ToLong(drAct("numBizDocTypeID")), CCommon.ToLong(drAct("numBizDocTemplateID")))

                                                        If dtCheckBizDocs.Rows.Count > 0 Then
                                                            WFDescription = "This BizDoc-" & CCommon.ToString(drAct("vcBizDoc")) & "  already exists for this order "
                                                            bitSuccess = True
                                                        Else
                                                            CreateBizDoc(CCommon.ToLong(dr("numRecordID")), CCommon.ToLong(dr("numDomainID")), CCommon.ToShort(drAct("numOppType")), numRecOwner, CCommon.ToLong(drAct("numBizDocTypeID")), CCommon.ToLong(drAct("numBizDocTemplateID")))
                                                            WFDescription = "BizDoc-" & CCommon.ToString(drAct("vcBizDoc")) & " has been created"

                                                            bitSuccess = True
                                                        End If

                                                    Case CCommon.ToShort(enmWFAction.PromoteOrganization)
                                                        Dim objLeads As New LeadsIP
                                                        objLeads.DivisionID = CCommon.ToLong(dr("numRecordID"))
                                                        objLeads.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        Dim dtCrmType As DataTable = objLeads.GetCRMType()

                                                        Dim tintCRMType As Integer = CCommon.ToInteger(dtCrmType.Rows(0)("tintCRMType").ToString)

                                                        If tintCRMType = 0 Then ' Leads

                                                            objLeads.UserCntID = numRecOwner
                                                            objLeads.ContactID = numContactId
                                                            objLeads.DivisionID = CCommon.ToLong(dr("numRecordID"))
                                                            objLeads.PromoteLead()
                                                            WFDescription = CCommon.ToString(dr("numRecordID")) & "-Organization Promoted"
                                                            bitSuccess = True
                                                        ElseIf tintCRMType = 1 Then 'Prospects
                                                            objLeads.DivisionID = CCommon.ToLong(dr("numRecordID"))
                                                            objLeads.byteMode = 2
                                                            objLeads.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                            objLeads.UserCntID = numRecOwner
                                                            objLeads.DemoteOrg()
                                                            WFDescription = CCommon.ToString(dr("numRecordID")) & "-Organization Promoted"
                                                            bitSuccess = True
                                                        Else 'Account
                                                            WFDescription = CCommon.ToString(dr("numRecordID")) & "-This Organization is already in Account ,No scope for Promotion"
                                                            bitSuccess = True
                                                        End If
                                                    Case CCommon.ToShort(enmWFAction.DemoteOrganization)
                                                        Dim objLeads As New LeadsIP
                                                        objLeads.DivisionID = CCommon.ToLong(dr("numRecordID"))
                                                        objLeads.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        Dim dtCrmType As DataTable = objLeads.GetCRMType()

                                                        Dim tintCRMType As Integer = CCommon.ToInteger(dtCrmType.Rows(0)("tintCRMType").ToString)

                                                        If tintCRMType = 1 Then 'Prospect
                                                            objLeads.DivisionID = CCommon.ToLong(dr("numRecordID"))
                                                            objLeads.byteMode = 0
                                                            objLeads.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                            objLeads.UserCntID = numRecOwner
                                                            objLeads.DemoteOrg()
                                                            WFDescription = CCommon.ToString(dr("numRecordID")) & "-Organization demoted"
                                                            bitSuccess = True
                                                        ElseIf tintCRMType = 2 Then 'Account
                                                            Dim objAccounts As New CAccounts
                                                            Dim intCount As Integer
                                                            objAccounts.DivisionID = CCommon.ToLong(dr("numRecordID"))
                                                            intCount = objAccounts.CheckDealsWon
                                                            If intCount = 0 Then
                                                                objLeads.DivisionID = CCommon.ToLong(dr("numRecordID"))
                                                                objLeads.byteMode = 1
                                                                objLeads.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                objLeads.UserCntID = numRecOwner
                                                                objLeads.DemoteOrg()
                                                            Else
                                                                WFDescription = "This record can not be demoted because it contains completed deals. To demote this record, delete all deals from the Deal History section, then try demoting again."
                                                                bitSuccess = True
                                                            End If
                                                        Else
                                                            WFDescription = "This record can not be demoted because It's on Lead stage"
                                                            bitSuccess = True

                                                        End If

                                                    Case CCommon.ToShort(enmWFAction.SMSAlerts)
                                                        Dim objEmail As New Email
                                                        Dim SMSToType() As String = drAct("vcEmailToType").ToString().Split(",")
                                                        Dim strContactIDs As New ArrayList

                                                        If SMSToType.Contains(1) Then 'Owner of trigger record
                                                            strContactIDs.Add(numRecOwner)
                                                        End If

                                                        If SMSToType.Contains(2) Then 'Assignee of trigger record
                                                            strContactIDs.Add(numAssignedTo)
                                                        End If

                                                        If SMSToType.Contains(3) Then 'Primary contact of trigger record
                                                            strContactIDs.Add(numContactId)
                                                        End If
                                                        If SMSToType.Contains(4) Then 'Internal Project Manager
                                                            strContactIDs.Add(numInternalPM)
                                                        End If
                                                        If SMSToType.Contains(5) Then 'External Project Manager
                                                            strContactIDs.Add(numExternalPM)
                                                        End If
                                                        If SMSToType.Contains(6) Then 'Next Stage Assigned to
                                                            strContactIDs.Add(numNextAssignedTo)
                                                        End If
                                                        If SMSToType.Contains(7) Then 'Assignee Of Trigger Record(Project)
                                                            strContactIDs.Add(numNextAssignedTo)
                                                        End If
                                                        Dim dtSMSBroadCast As New DataTable
                                                        Dim ACCOUNT_SID As String = "ACf3bc46e894a584aa43c6e934eb099db1"
                                                        Dim AUTH_TOKEN As String = "5a190ab5b2830b4dea058dbcc4c0590f"

                                                        Dim client As New TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN)

                                                        If strContactIDs.Count > 0 Then
                                                            dtSMSBroadCast = objEmail.GetEmailMergeData(1, String.Join(",", strContactIDs.ToArray()), CCommon.ToLong(dr("numDomainID")), 0, 0)
                                                            Dim strTo As New List(Of String)
                                                            For Each drEmail As DataRow In dtSMSBroadCast.Rows
                                                                strTo.Add(drEmail("ContactPhone"))
                                                                Dim message = client.SendSmsMessage("+12545312324", drEmail("ContactPhone").ToString(), CCommon.ToString(drAct("vcSMSText").ToString))
                                                                If message.RestException IsNot Nothing Then                                                        ' handle the error ...
                                                                    WFDescription = message.RestException.Message.ToString
                                                                    bitSuccess = False
                                                                Else
                                                                    WFDescription = "Message Has been Sent to" & drEmail("ContactPhone").ToString()
                                                                    bitSuccess = True
                                                                End If
                                                            Next
                                                        End If

                                                End Select

                                            Catch ex As Exception
                                                WFDescription = ex.Message
                                                bitSuccess = False
                                            End Try

                                            Dim objWF1 As New Workflow
                                            With objWF1
                                                .WFQueueID = CCommon.ToLong(dr("numWFQueueID"))
                                                .WFID = objWF.WFID
                                                .WFDescription = WFDescription
                                                .ManageWorkFlowQueue(2, _bitSuccess:=bitSuccess)
                                            End With
                                        Next

                                        objWF = New Workflow
                                        With objWF
                                            .WFQueueID = CCommon.ToLong(dr("numWFQueueID"))
                                            .DomainID = CCommon.ToLong(dr("numDomainID"))
                                            .tintProcessStatus = CCommon.ToShort(enmWFQueueProcessStatus.Success)
                                            .ManageWorkFlowQueue(1)
                                        End With
                                    Else 'If date not matched then it'll be Peding Execution
                                        objWF = New Workflow
                                        With objWF
                                            .WFQueueID = CCommon.ToLong(dr("numWFQueueID"))
                                            .DomainID = CCommon.ToLong(dr("numDomainID"))
                                            .tintProcessStatus = CCommon.ToShort(enmWFQueueProcessStatus.PendingExecution)
                                            .ManageWorkFlowQueue(1)
                                        End With
                                    End If
                                Else ' If date not matched then it'll be Peding Execution
                                    objWF = New Workflow
                                    With objWF
                                        .WFQueueID = CCommon.ToLong(dr("numWFQueueID"))
                                        .DomainID = CCommon.ToLong(dr("numDomainID"))
                                        .tintProcessStatus = CCommon.ToShort(enmWFQueueProcessStatus.ConditionNotMatch)
                                        .ManageWorkFlowQueue(1)
                                    End With
                                End If

                            End If 'end of Time based Action
                        Else
                            objWF = New Workflow
                            With objWF
                                .WFQueueID = CCommon.ToLong(dr("numWFQueueID"))
                                .DomainID = CCommon.ToLong(dr("numDomainID"))
                                .tintProcessStatus = CCommon.ToShort(enmWFQueueProcessStatus.NoWFFound)
                                .ManageWorkFlowQueue(1)
                            End With
                        End If
                    Next
                End If

                DateFieldWorkFlowExecution()

                ARAgingWorkFlowExecution()

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Function ConditionOperator(ByVal OperatorType As String, ByVal FilterValue As String, ByVal vcAssociatedControlType As String, ByVal vcFieldDataType As String, bitCustom As Boolean) As String
            Try
                Dim strOperatorSign As String = "="

                If vcAssociatedControlType = "SelectBox" Or vcAssociatedControlType = "ListBox" Or vcAssociatedControlType = "CheckBox" Then
                    If vcFieldDataType = "V" Or vcFieldDataType = "Y" Then
                        Dim strValue() As String = FilterValue.Split(",")
                        For i As Integer = 0 To strValue.Length - 1
                            strValue(i) = String.Format("'{0}'", strValue(i))
                        Next

                        FilterValue = String.Join(",", strValue)
                    End If

                    Select Case OperatorType
                        Case "eq"
                            strOperatorSign = " in(" & FilterValue & ")"
                        Case "ne"
                            strOperatorSign = " not in(" & FilterValue & ")"
                    End Select
                ElseIf vcAssociatedControlType = "DateField" Then
                    Select Case OperatorType
                        Case "eq"
                            strOperatorSign = "='" & FilterValue & "'"
                        Case "ne"
                            strOperatorSign = "!='" & FilterValue & "'"
                        Case "gt"
                            strOperatorSign = ">'" & FilterValue & "'"
                        Case "ge"
                            strOperatorSign = ">='" & FilterValue & "'"
                        Case "lt"
                            strOperatorSign = "<'" & FilterValue & "'"
                        Case "le"
                            strOperatorSign = "<='" & FilterValue & "'"
                    End Select
                Else
                    Select Case OperatorType
                        Case "eq"
                            strOperatorSign = "='" & FilterValue & "'"
                        Case "ne"
                            strOperatorSign = "!='" & FilterValue & "'"
                        Case "gt"
                            strOperatorSign = ">" & FilterValue
                        Case "ge"
                            strOperatorSign = ">=" & FilterValue
                        Case "lt"
                            strOperatorSign = "<" & FilterValue
                        Case "le"
                            strOperatorSign = "<=" & FilterValue
                        Case "co"
                            strOperatorSign = " ilike '%" & FilterValue & "%'"
                        Case "sw"
                            strOperatorSign = " ilike '" & FilterValue & "%'"
                            'compare Fields||no need to append Filter value||managed in Condition code
                        Case "ceq"
                            strOperatorSign = "="
                        Case "cne"
                            strOperatorSign = "!="
                        Case "cgt"
                            strOperatorSign = ">"
                        Case "cge"
                            strOperatorSign = ">="
                        Case "clt"
                            strOperatorSign = "<"
                        Case "cle"
                            strOperatorSign = "<="

                    End Select
                End If

                Return strOperatorSign
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetTimeBasedActionStatus(ByVal numRecordID As Integer, ByVal TableName As String, ByVal columnname As String, ByVal intDays As Integer, ByVal intActionOn As Integer, ByVal intCustom As Integer, ByVal Fld_ID As Long) As DataTable
            Try
                Dim dtResult As New DataTable
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("$2", numRecordID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@TableName", TableName, NpgsqlTypes.NpgsqlDbType.Varchar, 50))
                    .Add(SqlDAL.Add_Parameter("@columnname", columnname, NpgsqlTypes.NpgsqlDbType.Varchar, 50))
                    .Add(SqlDAL.Add_Parameter("@intDays", intDays, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intActionOn", intActionOn, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("$1", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@intCustom", intCustom, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@numFormID", FormID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@Fld_ID", Fld_ID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                dtResult = SqlDAL.ExecuteDatable(connString, "USP_WorkFlow_TimeBasdAction", sqlParams.ToArray())

                Return dtResult
            Catch ex As Exception
                Throw ex
            End Try
        End Function
#Region "Action:Create BizDoc"

        Public Sub CreateBizDoc(ByVal lngOppId As Long, ByVal numDomainID As Long, ByVal OppType As Integer, ByVal numUserCntID As Long, ByVal numBizDocTypeID As Long, ByVal numBizDocTemplateID As Long)
            Try
                Dim objOppBizDocs As New OppBizDocs
                Dim OppBizDocID, lngDivId, JournalId As Long
                Dim lngCntID As Long = 0
                objOppBizDocs.OppId = lngOppId
                objOppBizDocs.OppType = OppType

                objOppBizDocs.UserCntID = numUserCntID
                objOppBizDocs.DomainID = numDomainID
                objOppBizDocs.vcPONo = "" 'txtPO.Text
                objOppBizDocs.vcComments = "" 'txtComments.Text
                'objOppBizDocs.ShipCost = 0 'IIf(IsNumeric(txtShipCost.Text), txtShipCost.Text, 0)
                'Add Billing Term from selected SalesTemplate(Sales Order)'s first Autoritative bizdoc's Billing term


                Dim dtDetails, dtBillingTerms As DataTable
                Dim objPageLayout As New CPageLayout
                objPageLayout.OpportunityId = lngOppId
                objPageLayout.DomainID = numDomainID
                dtDetails = objPageLayout.OpportunityDetails.Tables(0)
                lngDivId = dtDetails.Rows(0).Item("numDivisionID")
                lngCntID = dtDetails.Rows(0).Item("numContactID")
                Dim objAdmin As New CAdmin
                objAdmin.DivisionID = lngDivId

                Dim lintAuthorizativeBizDocsId As Long
                objOppBizDocs.DomainID = numDomainID
                objOppBizDocs.OppId = lngOppId
                lintAuthorizativeBizDocsId = objOppBizDocs.GetAuthorizativeOpportuntiy()

                Dim objJournal As New JournalEntry
                If lintAuthorizativeBizDocsId = numBizDocTypeID Then 'save bizdoc only if selected company's AR and AP account is mapped
                    If objOppBizDocs.ValidateCustomerAR_APAccounts("AR", numDomainID, lngDivId) = 0 Then

                        Exit Sub
                    End If
                End If
                If OppType = 2 Then
                    'Accounting validation->Do not allow to save PO untill Default COGs account is mapped
                    If ChartOfAccounting.GetDefaultAccount("CG", numDomainID) = 0 Then
                        'ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default COGs account from ""Administration->Domain Details->Accounting->Default Accounts"" To Save' );", True)
                        Exit Sub
                    End If
                End If

                If OppType = 2 Then
                    If ChartOfAccounting.GetDefaultAccount("PC", numDomainID) = 0 Then
                        'ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Purchase Clearing account from ""Administration->Domain Details->Accounting->Default Accounts"" To Save' );", True)
                        Exit Sub
                    End If

                    If ChartOfAccounting.GetDefaultAccount("PV", numDomainID) = 0 Then
                        'ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Purchase Price Variance account from ""Administration->Domain Details->Accounting->Default Accounts"" To Save' );", True)
                        Exit Sub
                    End If
                End If

                objOppBizDocs.FromDate = DateTime.UtcNow
                objOppBizDocs.ClientTimeZoneOffset = "0"
                objOppBizDocs.BizDocId = numBizDocTypeID
                objOppBizDocs.BizDocTemplateID = numBizDocTemplateID

                Dim objCommon As New CCommon
                objCommon.DomainID = numDomainID
                objCommon.Mode = 33
                objCommon.Str = numBizDocTypeID
                objOppBizDocs.SequenceId = objCommon.GetSingleFieldValue()

                objCommon = New CCommon
                objCommon.DomainID = numDomainID
                objCommon.Mode = 34
                objCommon.Str = lngOppId
                objOppBizDocs.RefOrderNo = objCommon.GetSingleFieldValue()

                objOppBizDocs.bitPartialShipment = True

                Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                    OppBizDocID = objOppBizDocs.SaveBizDoc()

                    If OppBizDocID = 0 Then
                        ''litMessage.Text = "A BizDoc by the same name is already created. Please select another BizDoc !"
                        Exit Sub
                    End If

                    'If OppBizDocID > 0 Then
                    '    GetWorkFlowAutomation(OppBizDocID, lngCntID, lngDivId)

                    'End If

                    'Create Journals only for authoritative bizdocs
                    '-------------------------------------------------
                    If (lintAuthorizativeBizDocsId = numBizDocTypeID Or numBizDocTypeID = 304) Then '-- 304 Deferred Invoice
                        Dim ds As New DataSet
                        Dim dtOppBiDocItems As DataTable

                        objOppBizDocs.OppBizDocId = OppBizDocID
                        ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                        dtOppBiDocItems = ds.Tables(0)

                        Dim objCalculateDealAmount As New CalculateDealAmount

                        objCalculateDealAmount.CalculateDealAmount(lngOppId, OppBizDocID, OppType, numDomainID, dtOppBiDocItems)

                        ''---------------------------------------------------------------------------------
                        JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, objOppBizDocs.FromDate, Description:=ds.Tables(1).Rows(0).Item("vcBizDocID"))
                        Dim objJournalEntries As New JournalEntry

                        If OppType = 2 Then
                            If CCommon.ToBool(ds.Tables(1).Rows(0).Item("bitPPVariance")) Then
                                objJournalEntries.SaveJournalEntriesPurchaseVariance(lngOppId, numDomainID, dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), ds.Tables(1).Rows(0).Item("fltExchangeRateBizDoc"), vcBaseCurrency:=ds.Tables(1).Rows(0).Item("vcBaseCurrency"), vcForeignCurrency:=ds.Tables(1).Rows(0).Item("vcForeignCurrency"))
                            Else
                                objJournalEntries.SaveJournalEntriesPurchase(lngOppId, numDomainID, dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"))
                            End If
                        ElseIf OppType = 1 Then
                            objCommon.DomainID = numDomainID
                            Dim dtDomaindetails As DataTable = objCommon.GetDomainDetails()

                            If numBizDocTypeID = 304 AndAlso CCommon.ToBool(dtDomaindetails.Rows(0).Item("IsEnableDeferredIncome")) Then 'Deferred Revenue
                                objJournalEntries.SaveJournalEntriesDeferredIncome(lngOppId, numDomainID, dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), objCalculateDealAmount.GrandTotal)
                            Else
                                If objOppBizDocs.IsSalesClearingAccountEntryExists() Then
                                    objJournalEntries.SaveJournalEntriesSales(lngOppId, numDomainID, dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, lngDivId, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), 0)
                                Else
                                    objJournalEntries.SaveJournalEntriesSalesNew(lngOppId, numDomainID, dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, lngDivId, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), 0)
                                End If

                                If dtDomaindetails.Rows.Count > 0 Then
                                    If CCommon.ToBool(dtDomaindetails.Rows(0).Item("bitAutolinkUnappliedPayment")) Then
                                        Dim objSalesFulfillment As New SalesFulfillmentWorkflow
                                        objSalesFulfillment.DomainID = numDomainID
                                        objSalesFulfillment.UserCntID = numUserCntID
                                        objSalesFulfillment.ClientTimeZoneOffset = "-330"
                                        objSalesFulfillment.OppId = lngOppId
                                        objSalesFulfillment.OppBizDocId = OppBizDocID

                                        objSalesFulfillment.AutoLinkUnappliedPayment()
                                    End If
                                End If
                            End If

                        End If
                    ElseIf numBizDocTypeID = 296 Then 'Fulfillment BizDoc
                        'AUTO FULFILLS bizdoc AND MAKED QTY AS SHIPPED AND RELEASES IT FROM ALLOCATION
                        Dim objOppBizDoc As New OppBizDocs
                        objOppBizDoc.DomainID = numDomainID
                        objOppBizDoc.UserCntID = numUserCntID
                        objOppBizDoc.OppId = lngOppId
                        objOppBizDoc.OppBizDocId = OppBizDocID
                        objOppBizDoc.AutoFulfillBizDoc()


                        'IMPORTANT: WE ARE MAKING ACCOUNTING CHANGES BECAUSE WE ARE CREATING FULFILLMENT ORDER IN BACKGROUP AND MARKIGN IT AS SHIPPED
                        '           OTHERWISE ACCOUNTING IS ONLY IMPACTED WHEN USER SHIPPED IT FROM SALES FULFILLMENT PAGE
                        'CREATE SALES CLEARING ENTERIES
                        'CREATE JOURNALS ONLY FOR FILFILLMENT BIZDOCS
                        '-------------------------------------------------
                        Dim ds As New DataSet
                        Dim dtOppBiDocItems As DataTable

                        objOppBizDocs.OppBizDocId = OppBizDocID
                        ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                        dtOppBiDocItems = ds.Tables(0)

                        Dim objCalculateDealAmount As New CalculateDealAmount
                        objCalculateDealAmount.CalculateDealAmount(lngOppId, OppBizDocID, OppType, numDomainID, dtOppBiDocItems)

                        'WHEN FROM COA SOMEONE DELETED JOURNALS THEN WE NEED TO CREATE NEW JOURNAL HEADER ENTRY
                        JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, Entry_Date:=Date.UtcNow.Date, Description:=CStr(ds.Tables(1).Rows(0).Item("vcBizDocID")))

                        Dim objJournalEntries As New BACRM.BusinessLogic.Accounting.JournalEntry
                        If objOppBizDocs.IsSalesClearingAccountEntryExists() Then
                            objJournalEntries.SaveJournalEnteriesSalesClearing(lngOppId, numDomainID, dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, CLng(ds.Tables(1).Rows(0).Item("numCurrencyID")), CDbl(ds.Tables(1).Rows(0).Item("fltExchangeRate")))
                        Else
                            objJournalEntries.SaveJournalEnteriesSalesClearingNew(lngOppId, numDomainID, dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, CLng(ds.Tables(1).Rows(0).Item("numCurrencyID")), CDbl(ds.Tables(1).Rows(0).Item("fltExchangeRate")))
                        End If
                        '---------------------------------------------------------------------------------
                    End If

                    objTransactionScope.Complete()
                End Using

                ''Added By Sachin Sadhu||Date:1stMay2014
                ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
                ''          Using Change tracking
                Dim objWfA As New Workflow()
                objWfA.DomainID = numDomainID
                objWfA.UserCntID = numUserCntID
                objWfA.RecordID = OppBizDocID
                objWfA.SaveWFBizDocQueue()
                'end of code

                If OppBizDocID > 0 AndAlso numBizDocTypeID = 296 Then
                    Try
                        objOppBizDocs.CreateInvoiceFromFulfillmentOrder(numDomainID, numUserCntID, lngOppId, OppBizDocID, 0)
                    Catch ex As Exception
                        'DO NOT THROW ERROR
                    End Try
                End If

                '------ Send BizDoc Alerts - Notify record owners, their supervisors, and your trading partners when a BizDoc is created, modified, or approved.
                'Commented by Sachin
                'Dim objAlert As New CAlerts
                'objAlert.SendBizDocAlerts(lngOppId, OppBizDocID, numBizDocTypeID, numDomainID, CAlerts.enmBizDoc.IsCreated)

                'Dim objAutomatonRule As New AutomatonRule
                'objAutomatonRule.ExecuteAutomationRule(49, OppBizDocID, 1)
                'End of comment
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function CheckDuplicateBizDocs(ByVal lngOppId As Long, ByVal numDomainID As Long, ByVal OppType As Integer, ByVal numUserCntID As Long, ByVal numBizDocTypeID As Long, ByVal numBizDocTemplateID As Long) As DataTable
            Try
                Dim dt As New DataTable
                Dim objOppBizDocs As New OppBizDocs
                Dim OppBizDocID, lngDivId, JournalId As Long
                Dim lngCntID As Long = 0
                objOppBizDocs.OppId = lngOppId
                objOppBizDocs.OppType = OppType
                objOppBizDocs.BizDocId = numBizDocTypeID
                objOppBizDocs.BizDocTemplateID = numBizDocTemplateID
                objOppBizDocs.DomainID = numDomainID
                objOppBizDocs.OppId = lngOppId
                dt = objOppBizDocs.CheckDuplicateBizDocs()
                Return dt
            Catch ex As Exception
                Throw ex
            End Try
        End Function
#End Region
#Region "Action:Send mail with BizDoc Attachment"
        Public Function GenerateBizDocPDF(ByVal oppBizDocId As Long,
                                      ByVal oppId As Long,
                                      ByVal domainId As Long,
                                      ByVal userContactId As Long,
                                      Optional ByVal clientTimeZoneOffset As Integer = 0,
                                      Optional ByVal dateFormat As String = "dd/mm/yyyy") As String
            Try
                Dim dtOppBiDocDtl As DataTable
                Dim objOppBizDocs As New OppBizDocs
                objOppBizDocs.OppBizDocId = oppBizDocId
                objOppBizDocs.OppId = oppId
                objOppBizDocs.DomainID = domainId
                objOppBizDocs.UserCntID = userContactId
                objOppBizDocs.ClientTimeZoneOffset = clientTimeZoneOffset
                dtOppBiDocDtl = objOppBizDocs.GetOppBizDocDtl

                If Not dtOppBiDocDtl Is Nothing AndAlso dtOppBiDocDtl.Rows.Count > 0 Then
                    Dim bizDocHtml As String
                    Dim bizDocCss As String
                    Dim masterCSS As String

                    If System.IO.File.Exists(ConfigurationManager.AppSettings("BACRMLocation") & "\CSS\NewUiStyle.css") Then
                        masterCSS = System.IO.File.ReadAllText(ConfigurationManager.AppSettings("BACRMLocation") & "\CSS\NewUiStyle.css")
                    End If

                    Dim stingBuilder As New StringBuilder

                    bizDocHtml = HttpUtility.HtmlDecode(CCommon.ToString(dtOppBiDocDtl.Rows(0)("txtBizDocTemplate")))
                    bizDocCss = HttpUtility.HtmlDecode(CCommon.ToString(dtOppBiDocDtl.Rows(0)("txtCss")))

                    If Not String.IsNullOrEmpty(bizDocHtml) Then
                        'Gets Address Detail
                        Dim dsAddress As DataSet
                        objOppBizDocs.OppId = oppId
                        objOppBizDocs.DomainID = domainId
                        objOppBizDocs.OppBizDocId = oppBizDocId
                        dsAddress = objOppBizDocs.GetOPPGetOppAddressDetails


                        stingBuilder.Append("<html><head>")
                        'stingBuilder.Append("<link rel='stylesheet' href='" & Current.Server.MapPath("~/CSS/master.css") & "' type='text/css' />")
                        stingBuilder.Append("<style type=""text/css"">")
                        If Not String.IsNullOrEmpty(masterCSS) Then
                            stingBuilder.Append(masterCSS)
                        End If
                        stingBuilder.Append("pre.WordWrap")
                        stingBuilder.Append("{")
                        stingBuilder.Append("height:auto !important;")
                        stingBuilder.Append("overflow: auto !important;")
                        stingBuilder.Append("overflow-x: auto !important; /* Use horizontal scroller if needed; for Firefox 2, not */")
                        stingBuilder.Append(" white-space: pre-wrap !important; ")
                        stingBuilder.Append("white-space: -moz-pre-wrap !important; /* Mozilla, since 1999 */")
                        stingBuilder.Append("word-wrap: break-word !important; /* Internet Explorer 5.5+ */")
                        stingBuilder.Append("font-family:Arial,Helvetica,sans-serif !important; ")
                        stingBuilder.Append("}")
                        stingBuilder.Append(".ItemHeader, .ItemStyle, .AltItemStyle {")
                        stingBuilder.Append("text-align: left;")
                        stingBuilder.Append("}")
                        If Not String.IsNullOrEmpty(bizDocCss) Then
                            stingBuilder.Append(bizDocCss)
                        End If
                        stingBuilder.Append("</style>")
                        stingBuilder.Append("</head>")
                        stingBuilder.Append("<body>")
                        stingBuilder.Append("<table id=""tblFormattedBizDoc"" border=""0"" style=""border-color:Black;border-width:1px;border-style:solid;width:100%;"">")
                        stingBuilder.Append("<tr>")
                        stingBuilder.Append("<td>")
                        stingBuilder.Append(GetBizDocHtmlAfterReplacingTags(bizDocHtml, dtOppBiDocDtl, dsAddress, domainId, oppId, oppBizDocId, dateFormat))
                        stingBuilder.Append("</td>")
                        stingBuilder.Append(" </tr>")
                        stingBuilder.Append("</table>")
                        stingBuilder.Append("</body>")
                        stingBuilder.Append("</html>")
                        Return stingBuilder.ToString()
                    Else
                        Throw New Exception("Default tmeplate for bizdoc is not generated. Go to global settings -> Order Management -> Design / Templates")
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetBizDocHtmlAfterReplacingTags(ByVal strBizDocUI As String,
                                                         ByVal dtOppBiDocDtl As DataTable,
                                                         ByVal dsAddress As DataSet,
                                                         ByVal domainId As Long,
                                                         ByVal oppId As Long,
                                                         ByVal oppBizDocId As Long,
                                                         ByVal dateFormat As String) As String

            If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("vcBizDocImagePath")) Then
                strBizDocUI = strBizDocUI.Replace("#Logo#", "<img src=""" + (CCommon.GetDocumentPhysicalPath(domainId) & CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcBizDocImagePath"))) + """>")
            Else
                strBizDocUI = strBizDocUI.Replace("#Logo#", "")
            End If

            If CCommon.ToInteger(dtOppBiDocDtl.Rows(0).Item("tintOppType")) = 1 Then
                If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("BizdocFooter")) Then
                    strBizDocUI = strBizDocUI.Replace("#FooterImage#", "<img src=""" + (CCommon.GetDocumentPhysicalPath(domainId) & CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("BizdocFooter"))) + """>")
                Else
                    strBizDocUI = strBizDocUI.Replace("#FooterImage#", "")
                End If
            Else
                If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("PurBizdocFooter")) Then
                    strBizDocUI = strBizDocUI.Replace("#FooterImage#", "<img src=""" + (CCommon.GetDocumentPhysicalPath(domainId) & CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("PurBizdocFooter"))) + """>")
                Else
                    strBizDocUI = strBizDocUI.Replace("#FooterImage#", "")
                End If
            End If

            Dim dtOppBizAddDtl As DataTable
            Dim objOppBizDocs As New OppBizDocs
            objOppBizDocs.OppBizDocId = oppBizDocId
            objOppBizDocs.OppId = oppId
            objOppBizDocs.DomainID = domainId
            dtOppBizAddDtl = objOppBizDocs.GetOppAddressDetail
            If dtOppBizAddDtl.Rows.Count <> 0 Then
                strBizDocUI = strBizDocUI.Replace("#BizDocType#", CCommon.ToString(dtOppBizAddDtl.Rows(0).Item("BizDcocName")))
                strBizDocUI = strBizDocUI.Replace("#OrderID#", CCommon.ToString(dtOppBizAddDtl.Rows(0).Item("OppName")))
            End If


            strBizDocUI = strBizDocUI.Replace("#InventoryStatus#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcInventoryStatus")))

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Customer/Vendor Information
            strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationName#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrganizationName")))
            strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationPhone#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrganizationPhone")))

            If dsAddress.Tables(0).Rows.Count = 0 Then
                strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCompanyName#", "")
                strBizDocUI = strBizDocUI.Replace("#OppOrderBillToAddress#", "")
                strBizDocUI = strBizDocUI.Replace("#OppOrderBillToStreet#", "")
                strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCity#", "")
                strBizDocUI = strBizDocUI.Replace("#OppOrderBillToPostal#", "")
                strBizDocUI = strBizDocUI.Replace("#OppOrderBillToState#", "")
                strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCountry#", "")
                strBizDocUI = strBizDocUI.Replace("#OppOrderBillToAddressName#", "")
                strBizDocUI = strBizDocUI.Replace("#BillingContact#", "")
            Else
                strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCompanyName#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcCompanyName")))
                strBizDocUI = strBizDocUI.Replace("#OppOrderBillToAddress#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcFullAddress")))
                strBizDocUI = strBizDocUI.Replace("#OppOrderBillToStreet#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcStreet")))
                strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCity#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcCity")))
                strBizDocUI = strBizDocUI.Replace("#OppOrderBillToPostal#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcPostalCode")))
                strBizDocUI = strBizDocUI.Replace("#OppOrderBillToState#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcState")))
                strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCountry#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcCountry")))
                strBizDocUI = strBizDocUI.Replace("#OppOrderBillToAddressName#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcAddressName")))
                strBizDocUI = strBizDocUI.Replace("#BillingContact#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcContact")))
            End If

            If Not dsAddress Is Nothing AndAlso dsAddress.Tables.Count > 4 AndAlso dsAddress.Tables(4).Rows.Count > 0 Then
                strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCompanyName#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcCompanyName")))
                strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToAddress#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcFullAddress")))
                strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToStreet#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcStreet")))
                strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCity#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcCity")))
                strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToPostal#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcPostalCode")))
                strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToState#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcState")))
                strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCountry#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcCountry")))
                strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToAddressName#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcAddressName")))
            Else
                strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCompanyName#", "")
                strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToAddress#", "")
                strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToStreet#", "")
                strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCity#", "")
                strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToPostal#", "")
                strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToState#", "")
                strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCountry#", "")
                strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToAddressName#", "")
            End If

            If dsAddress.Tables(1).Rows.Count = 0 Then
                strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCompanyName#", "")
                strBizDocUI = strBizDocUI.Replace("#OppOrderShipToAddress#", "")
                strBizDocUI = strBizDocUI.Replace("#OppOrderShipToStreet#", "")
                strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCity#", "")
                strBizDocUI = strBizDocUI.Replace("#OppOrderShipToPostal#", "")
                strBizDocUI = strBizDocUI.Replace("#OppOrderShipToState#", "")
                strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCountry#", "")
                strBizDocUI = strBizDocUI.Replace("#OppOrderShipToAddressName#", "")
                strBizDocUI = strBizDocUI.Replace("#ShippingContact#", "")
            Else
                strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCompanyName#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcCompanyName")))
                strBizDocUI = strBizDocUI.Replace("#OppOrderShipToAddress#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcFullAddress")))
                strBizDocUI = strBizDocUI.Replace("#OppOrderShipToStreet#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcStreet")))
                strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCity#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcCity")))
                strBizDocUI = strBizDocUI.Replace("#OppOrderShipToPostal#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcPostalCode")))
                strBizDocUI = strBizDocUI.Replace("#OppOrderShipToState#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcState")))
                strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCountry#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcCountry")))
                strBizDocUI = strBizDocUI.Replace("#OppOrderShipToAddressName#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcAddressName")))
                strBizDocUI = strBizDocUI.Replace("#ShippingContact#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcContact")))
            End If

            If Not dsAddress Is Nothing AndAlso dsAddress.Tables.Count > 5 AndAlso dsAddress.Tables(5).Rows.Count > 0 Then
                strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCompanyName#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcCompanyName")))
                strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToAddress#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcFullAddress")))
                strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToStreet#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcStreet")))
                strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCity#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcCity")))
                strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToPostal#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcPostalCode")))
                strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToState#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcState")))
                strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCountry#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcCountry")))
                strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToAddressName#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcAddressName")))
            Else
                strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCompanyName#", "")
                strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToAddress#", "")
                strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToStreet#", "")
                strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCity#", "")
                strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToPostal#", "")
                strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToState#", "")
                strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCountry#", "")
                strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToAddressName#", "")
            End If

            If CCommon.ToInteger(dtOppBiDocDtl.Rows(0).Item("tintOppType")) = 1 Then
                If strBizDocUI.IndexOf("#OppOrderChangeBillToHeader(") > 0 Then
                    'TODO: Implementation Remaining
                    'Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#OppOrderChangeBillToHeader(") + "#OppOrderChangeBillToHeader(".Length, strBizDocUI.LastIndexOf(")#") - strBizDocUI.IndexOf("#OppOrderChangeBillToHeader(") - "#OppOrderChangeBillToHeader(".Length)
                    'hplBillto.InnerHtml = strLink '"<font color=""white"">" + strLink + "</font>"
                    'strBizDocUI = strBizDocUI.Replace("#OppOrderChangeBillToHeader(" & strLink & ")#", CCommon.RenderControl(hplBillto))
                Else
                    strBizDocUI = strBizDocUI.Replace("#OppOrderChangeBillToHeader#", "Bill To")
                End If

                If strBizDocUI.IndexOf("#OppOrderChangeShipToHeader(") > 0 Then
                    'TODO: Implementation Remaining
                    'Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#OppOrderChangeShipToHeader(") + "#OppOrderChangeShipToHeader(".Length, strBizDocUI.IndexOf(")#") - strBizDocUI.IndexOf("#OppOrderChangeShipToHeader(") - "#OppOrderChangeShipToHeader(".Length)
                    'hplShipTo.InnerHtml = strLink '"<font color=""white"">" + strLink + "</font>"
                    'strBizDocUI = strBizDocUI.Replace("#OppOrderChangeShipToHeader(" & strLink & ")#", CCommon.RenderControl(hplShipTo))
                Else
                    strBizDocUI = strBizDocUI.Replace("#OppOrderChangeShipToHeader#", "Ship To")
                End If
            Else
                If strBizDocUI.IndexOf("#OppOrderChangeBillToHeader(") > 0 Then
                    Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#OppOrderChangeBillToHeader(") + "#OppOrderChangeBillToHeader(".Length, strBizDocUI.LastIndexOf(")#") - strBizDocUI.IndexOf("#OppOrderChangeBillToHeader(") - "#OppOrderChangeBillToHeader(".Length)
                    strBizDocUI = strBizDocUI.Replace("#OppOrderChangeBillToHeader(" & strLink & ")#", strLink)
                Else
                    strBizDocUI = strBizDocUI.Replace("#OppOrderChangeBillToHeader#", "Bill To")
                End If

                If strBizDocUI.IndexOf("#OppOrderChangeShipToHeader(") > 0 Then
                    Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#OppOrderChangeShipToHeader(") + "#OppOrderChangeShipToHeader(".Length, strBizDocUI.IndexOf(")#") - strBizDocUI.IndexOf("#OppOrderChangeShipToHeader(") - "#OppOrderChangeShipToHeader(".Length)
                    strBizDocUI = strBizDocUI.Replace("#OppOrderChangeShipToHeader(" & strLink & ")#", strLink)
                Else
                    strBizDocUI = strBizDocUI.Replace("#OppOrderChangeShipToHeader#", "Ship To")
                End If
            End If

            strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationComments#", dtOppBiDocDtl.Rows(0)("vcOrganizationComments").ToString())

            strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationContactName#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrgContactName")))
            strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationContactEmail#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrgContactEmail")))
            strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationContactPhone#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrgContactPhone")))
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            'Employer Information
            strBizDocUI = strBizDocUI.Replace("#EmployerOrganizationName#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("EmployerOrganizationName")))
            strBizDocUI = strBizDocUI.Replace("#EmployerOrganizationPhone#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("EmployerOrganizationPhone")))

            If dsAddress.Tables(2).Rows.Count = 0 Then
                strBizDocUI = strBizDocUI.Replace("#EmployerBillToCompanyName#", "")
                strBizDocUI = strBizDocUI.Replace("#EmployerBillToAddress#", "")
                strBizDocUI = strBizDocUI.Replace("#EmployerBillToStreet#", "")
                strBizDocUI = strBizDocUI.Replace("#EmployerBillToCity#", "")
                strBizDocUI = strBizDocUI.Replace("#EmployerBillToPostal#", "")
                strBizDocUI = strBizDocUI.Replace("#EmployerBillToState#", "")
                strBizDocUI = strBizDocUI.Replace("#EmployerBillToCountry#", "")
                strBizDocUI = strBizDocUI.Replace("#EmployerBillToAddressName#", "")
            Else
                strBizDocUI = strBizDocUI.Replace("#EmployerBillToCompanyName#", CCommon.ToString(dsAddress.Tables(2).Rows(0)("vcCompanyName")))
                strBizDocUI = strBizDocUI.Replace("#EmployerBillToAddress#", CCommon.ToString(dsAddress.Tables(2).Rows(0)("vcFullAddress")))
                strBizDocUI = strBizDocUI.Replace("#EmployerBillToStreet#", CCommon.ToString(dsAddress.Tables(2).Rows(0)("vcStreet")))
                strBizDocUI = strBizDocUI.Replace("#EmployerBillToCity#", CCommon.ToString(dsAddress.Tables(2).Rows(0)("vcCity")))
                strBizDocUI = strBizDocUI.Replace("#EmployerBillToPostal#", CCommon.ToString(dsAddress.Tables(2).Rows(0)("vcPostalCode")))
                strBizDocUI = strBizDocUI.Replace("#EmployerBillToState#", CCommon.ToString(dsAddress.Tables(2).Rows(0)("vcState")))
                strBizDocUI = strBizDocUI.Replace("#EmployerBillToCountry#", CCommon.ToString(dsAddress.Tables(2).Rows(0)("vcCountry")))
                strBizDocUI = strBizDocUI.Replace("#EmployerBillToAddressName#", CCommon.ToString(dsAddress.Tables(2).Rows(0)("vcAddressName")))
            End If

            If dsAddress.Tables(3).Rows.Count = 0 Then
                strBizDocUI = strBizDocUI.Replace("#EmployerShipToCompanyName#", "")
                strBizDocUI = strBizDocUI.Replace("#EmployerShipToAddress#", "")
                strBizDocUI = strBizDocUI.Replace("#EmployerShipToStreet#", "")
                strBizDocUI = strBizDocUI.Replace("#EmployerShipToCity#", "")
                strBizDocUI = strBizDocUI.Replace("#EmployerShipToPostal#", "")
                strBizDocUI = strBizDocUI.Replace("#EmployerShipToState#", "")
                strBizDocUI = strBizDocUI.Replace("#EmployerShipToCountry#", "")
                strBizDocUI = strBizDocUI.Replace("#EmployerShipToAddressName#", "")
            Else
                strBizDocUI = strBizDocUI.Replace("#EmployerShipToCompanyName#", CCommon.ToString(dsAddress.Tables(3).Rows(0)("vcCompanyName")))
                strBizDocUI = strBizDocUI.Replace("#EmployerShipToAddress#", CCommon.ToString(dsAddress.Tables(3).Rows(0)("vcFullAddress")))
                strBizDocUI = strBizDocUI.Replace("#EmployerShipToStreet#", CCommon.ToString(dsAddress.Tables(3).Rows(0)("vcStreet")))
                strBizDocUI = strBizDocUI.Replace("#EmployerShipToCity#", CCommon.ToString(dsAddress.Tables(3).Rows(0)("vcCity")))
                strBizDocUI = strBizDocUI.Replace("#EmployerShipToPostal#", CCommon.ToString(dsAddress.Tables(3).Rows(0)("vcPostalCode")))
                strBizDocUI = strBizDocUI.Replace("#EmployerShipToState#", CCommon.ToString(dsAddress.Tables(3).Rows(0)("vcState")))
                strBizDocUI = strBizDocUI.Replace("#EmployerShipToCountry#", CCommon.ToString(dsAddress.Tables(3).Rows(0)("vcCountry")))
                strBizDocUI = strBizDocUI.Replace("#EmployerShipToAddressName#", CCommon.ToString(dsAddress.Tables(3).Rows(0)("vcAddressName")))
            End If

            If CCommon.ToInteger(dtOppBiDocDtl.Rows(0).Item("tintOppType")) = 2 Then
                If strBizDocUI.IndexOf("#EmployerChangeBillToHeader(") > 0 Then
                    'TODO: Implementation Remaining
                    'Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#EmployerChangeBillToHeader(") + "#EmployerChangeBillToHeader(".Length, strBizDocUI.LastIndexOf(")#") - strBizDocUI.IndexOf("#EmployerChangeBillToHeader(") - "#EmployerChangeBillToHeader(".Length)
                    'hplBillto.InnerHtml = strLink '"<font color=""white"">" + strLink + "</font>"
                    'strBizDocUI = strBizDocUI.Replace("#EmployerChangeBillToHeader(" & strLink & ")#", CCommon.RenderControl(hplBillto))
                Else
                    strBizDocUI = strBizDocUI.Replace("#EmployerChangeBillToHeader#", "Bill To")
                End If

                If strBizDocUI.IndexOf("#EmployerChangeShipToHeader(") > 0 Then
                    'TODO: Implementation Remaining
                    'Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#EmployerChangeShipToHeader(") + "#EmployerChangeShipToHeader(".Length, strBizDocUI.IndexOf(")#") - strBizDocUI.IndexOf("#EmployerChangeShipToHeader(") - "#EmployerChangeShipToHeader(".Length)
                    'hplShipTo.InnerHtml = strLink '"<font color=""white"">" + strLink + "</font>"
                    'strBizDocUI = strBizDocUI.Replace("#EmployerChangeShipToHeader(" & strLink & ")#", CCommon.RenderControl(hplShipTo))
                Else
                    strBizDocUI = strBizDocUI.Replace("#EmployerChangeShipToHeader#", "Ship To")
                End If
            Else
                If strBizDocUI.IndexOf("#EmployerChangeBillToHeader(") > 0 Then
                    Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#EmployerChangeBillToHeader(") + "#EmployerChangeBillToHeader(".Length, strBizDocUI.LastIndexOf(")#") - strBizDocUI.IndexOf("#EmployerChangeBillToHeader(") - "#EmployerChangeBillToHeader(".Length)
                    strBizDocUI = strBizDocUI.Replace("#EmployerChangeBillToHeader(" & strLink & ")#", strBizDocUI)
                Else
                    strBizDocUI = strBizDocUI.Replace("#EmployerChangeBillToHeader#", "Bill To")
                End If

                If strBizDocUI.IndexOf("#EmployerChangeShipToHeader(") > 0 Then
                    Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#EmployerChangeShipToHeader(") + "#EmployerChangeShipToHeader(".Length, strBizDocUI.IndexOf(")#") - strBizDocUI.IndexOf("#EmployerChangeShipToHeader(") - "#EmployerChangeShipToHeader(".Length)
                    strBizDocUI = strBizDocUI.Replace("#EmployerChangeShipToHeader(" & strLink & ")#", strLink)
                Else
                    strBizDocUI = strBizDocUI.Replace("#EmployerChangeShipToHeader#", "Ship To")
                End If
            End If

            Dim ds As New DataSet
            Dim dtOppBiDocItems As DataTable
            objOppBizDocs.DomainID = domainId
            objOppBizDocs.OppBizDocId = oppBizDocId
            objOppBizDocs.OppId = oppId
            ds = objOppBizDocs.GetBizDocItemsWithKitChilds
            dtOppBiDocItems = ds.Tables(0)

            strBizDocUI = strBizDocUI.Replace("#AmountPaidPopUp#", "Amount Paid: ")
            strBizDocUI = strBizDocUI.Replace("#ChangeDueDate#", "Due Date")
            strBizDocUI = strBizDocUI.Replace("#AssigneeName#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("AssigneeName")))
            strBizDocUI = strBizDocUI.Replace("#AssigneeEmail#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("AssigneeEmail")))
            strBizDocUI = strBizDocUI.Replace("#AssigneePhoneNo#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("AssigneePhone")))
            strBizDocUI = strBizDocUI.Replace("#OrderRecOwner#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OnlyOrderRecOwner")))
            strBizDocUI = strBizDocUI.Replace("#ShippingCompany#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("ShipVia")))
            strBizDocUI = strBizDocUI.Replace("#ShippingService#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcShippingService")))
            strBizDocUI = strBizDocUI.Replace("#PackingSlipID#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcPackingSlip")))
            strBizDocUI = strBizDocUI.Replace("#BizDocTemplateName#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcTemplateName")))
            If dtOppBiDocItems IsNot Nothing AndAlso dtOppBiDocItems.Rows.Count > 0 Then
                If dtOppBiDocItems.Select("numItemCode <>" & CCommon.ToDouble(dtOppBiDocDtl.Rows(0)("numShippingServiceItemID")) & " AND numItemCode <> " & CCommon.ToDouble(dtOppBiDocDtl.Rows(0)("numDiscountServiceItemID"))).Length > 0 Then
                    strBizDocUI = strBizDocUI.Replace("#TotalQuantity#", String.Format("{0:#,##0.##}", CCommon.ToDecimal(dtOppBiDocItems.Select("numItemCode <>" & CCommon.ToDouble(dtOppBiDocDtl.Rows(0)("numShippingServiceItemID")) & " AND numItemCode <> " & CCommon.ToDouble(dtOppBiDocDtl.Rows(0)("numDiscountServiceItemID"))).CopyToDataTable().Compute("sum(numUnitHour)", ""))))
                Else
                    strBizDocUI = strBizDocUI.Replace("#TotalQuantity#", String.Format("{0:#,##0.##}", 0))
                End If
            Else
                strBizDocUI = strBizDocUI.Replace("#TotalQuantity#", String.Format("{0:#,##0.##}", 0))
            End If
            strBizDocUI = strBizDocUI.Replace("#TrackingNo#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcTrackingNo")))
            strBizDocUI = strBizDocUI.Replace("#PartnerSource#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcPartner")))
            strBizDocUI = strBizDocUI.Replace("#ReleaseDate#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcReleaseDate")))
            strBizDocUI = strBizDocUI.Replace("#RequiredDate#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcRequiredDate")))
            strBizDocUI = strBizDocUI.Replace("#SOComments#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcSOComments")))
            strBizDocUI = strBizDocUI.Replace("#ParcelShippingAccount#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcShippersAccountNo")))
            strBizDocUI = strBizDocUI.Replace("#OrderCreatedDate#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrderCreatedDate")))
            strBizDocUI = strBizDocUI.Replace("#VendorInvoice#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcVendorInvoice")))
            If CCommon.ToBool(dtOppBiDocDtl.Rows(0).Item("tintBillingTerms")) = True Then
                strBizDocUI = strBizDocUI.Replace("#BillingTerms#", "Net " & CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("numBillingDaysName")) & " , " & CCommon.ToString(IIf(CCommon.ToBool(dtOppBiDocDtl.Rows(0).Item("tintInterestType")) = False, "-", "+")) & CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("fltInterest")) & " %")
                strBizDocUI = strBizDocUI.Replace("#BillingTermsName#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcBillingTermsName")) & " , " & CCommon.ToString(IIf(CCommon.ToBool(dtOppBiDocDtl.Rows(0).Item("tintInterestType")) = False, "-", "+")) & CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("fltInterest")) & " %")
            Else
                strBizDocUI = strBizDocUI.Replace("#BillingTerms#", "-")
                strBizDocUI = strBizDocUI.Replace("#BillingTermsName#", "-")
            End If


            If CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("dtFromDate")).Length > 3 Then
                strBizDocUI = strBizDocUI.Replace("#BillingTermFromDate#", FormattedDateFromDate(CCommon.ToSqlDate(dtOppBiDocDtl.Rows(0).Item("dtFromDate")), dateFormat))
            End If
            Dim strDate As Date
            Dim strDueDate As String
            strDate = DateAdd(DateInterval.Day, CCommon.ToInteger(dtOppBiDocDtl.Rows(0).Item("numBillingDaysName")), dtOppBiDocDtl.Rows(0).Item("dtFromDate"))
            strDueDate = FormattedDateFromDate(strDate, dateFormat)

            'strBizDocUI = strBizDocUI.Replace("#DueDate#", FormattedDateFromDate(DateAdd(DateInterval.Day, CCommon.ToInteger(dtOppBiDocDtl.Rows(0).Item("numBillingDaysName")), CCommon.ToSqlDate(dtOppBiDocDtl.Rows(0).Item("dtFromDate"))), dateFormat))
            strBizDocUI = strBizDocUI.Replace("#DueDate#", strDueDate)
            strBizDocUI = strBizDocUI.Replace("#BizDocID#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcBizDocID")))
            strBizDocUI = strBizDocUI.Replace("#P.O.NO#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcRefOrderNo")))
            strBizDocUI = strBizDocUI.Replace("#CustomerPO##", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcCustomerPO#")))

            strBizDocUI = strBizDocUI.Replace("#Comments#", "<pre class=""WordWrap"">" & CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcComments")) & "</pre>")
            strBizDocUI = strBizDocUI.Replace("#BizDocCreatedDate#", FormattedDateFromDate(CCommon.ToSqlDate(dtOppBiDocDtl.Rows(0).Item("dtCreatedDate")), dateFormat))

            'strBizDocUI = strBizDocUI.Replace("#AmountPaidPopUp#", "Amount Paid: (Deferred)")
            If dtOppBiDocDtl.Columns.Contains("vcTotalQtybyUOM") Then
                strBizDocUI = strBizDocUI.Replace("#TotalQytbyUOM#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcTotalQtybyUOM")))
            Else
                strBizDocUI = strBizDocUI.Replace("#TotalQytbyUOM#", "")
            End If

            strBizDocUI = strBizDocUI.Replace("#BizDocStatus#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("BizDocStatus")))
            strBizDocUI = strBizDocUI.Replace("#Currency#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString()))
            If CCommon.ToBool(dtOppBiDocDtl.Rows(0).Item("bitDiscountType")) = False Then
                strBizDocUI = strBizDocUI.Replace("#Discount#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("fltDiscount")) & " %")
            Else
                strBizDocUI = strBizDocUI.Replace("#Discount#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("fltDiscount")))
            End If

            'Replace custom field values
            Dim objCustomFields As New CustomFields
            If CCommon.ToInteger(dtOppBiDocDtl.Rows(0).Item("tintOppType")) = 1 Then
                objCustomFields.locId = 2
            Else
                objCustomFields.locId = 6
            End If
            objCustomFields.RelId = 0
            objCustomFields.DomainID = domainId
            objCustomFields.RecordId = oppId
            Dim dsCustomFields As DataSet = objCustomFields.GetCustFlds
            Dim objCommon As New CCommon
            For Each drow As DataRow In dsCustomFields.Tables(0).Rows
                If CCommon.ToLong(drow("numListID")) > 0 Then
                    strBizDocUI = strBizDocUI.Replace("#" & drow("fld_label").ToString.Trim.Replace(" ", "") & "#", objCommon.GetListItemValue(CCommon.ToString(drow("Value")), domainId))
                Else
                    strBizDocUI = strBizDocUI.Replace("#" & drow("fld_label").ToString.Trim.Replace(" ", "") & "#", CCommon.ToString(drow("Value")))
                End If
            Next
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


            'party
            Dim objCommon1 As New CCommon
            Dim dsDateFormat As DataSet = objCommon1.GetDomainDateFormat(CCommon.ToLong(domainId))
            Dim strDecimalpoints As String = dsDateFormat.Tables(0).Rows(0)("tintDecimalPoints").ToString()
            Dim objCalculateDealAmount As New CalculateDealAmount
            objCalculateDealAmount.CalculateDealAmount(oppId, oppBizDocId, CShort(dtOppBiDocDtl.Rows(0).Item("tintOppType")), domainId, dtOppBiDocItems, FromBizInvoice:=True)

            Dim subTotal As String = CCommon.GetDecimalFormatService(objCalculateDealAmount.TotalAmount, 2)
            Dim taxAmt As String = CCommon.GetDecimalFormatService(objCalculateDealAmount.TotalTaxAmount - objCalculateDealAmount.TotalCRVTaxAmount, 2)
            Dim crvTaxAmt As String = CCommon.GetDecimalFormatService(objCalculateDealAmount.TotalCRVTaxAmount, 2)
            Dim lateCharge As String = CCommon.GetDecimalFormatService(objCalculateDealAmount.TotalLateCharges, 2)
            Dim disc As String = CCommon.GetDecimalFormatService(objCalculateDealAmount.TotalDiscount, 2)
            Dim creditAmount As String = CCommon.GetDecimalFormatService(IIf(objCalculateDealAmount.CreditAmount > 0, objCalculateDealAmount.CreditAmount * -1, 0), 2)
            Dim grandTotal As Double = CCommon.ToDouble(objCalculateDealAmount.GrandTotal) - CCommon.ToDouble(objCalculateDealAmount.CreditAmount)
            Dim amountPaid As Double = CCommon.ToDouble(dtOppBiDocDtl.Rows(0).Item("monAmountPaid"))

            strBizDocUI = strBizDocUI.Replace("#AmountPaid#", amountPaid.ToString())
            strBizDocUI = strBizDocUI.Replace("#BalanceDue#", CCommon.GetDecimalFormatService(grandTotal - amountPaid, 2).ToString())
            Dim lNumericToWord As New NumericToWord
            strBizDocUI = strBizDocUI.Replace("#GrandTotalinWords#", CCommon.ToString(lNumericToWord.SpellNumber(CCommon.ToString(grandTotal).Replace(",", ""))))


            If CShort(dtOppBiDocDtl.Rows(0).Item("tintOppType")) = 1 AndAlso CShort(dtOppBiDocDtl.Rows(0).Item("tintOppStatus")) = 0 AndAlso dtOppBiDocItems.Select("bitKitParent=1").Length > 0 AndAlso dtOppBiDocItems.Select("numParentOppItemID > 0").Length > 0 Then
                Dim arrKits As DataRow() = dtOppBiDocItems.Select("bitKitParent=1")

                If Not arrKits Is Nothing AndAlso arrKits.Length > 0 Then
                    Dim tempProducts = ""

                    For Each drKit As DataRow In arrKits
                        Dim arrKitItemsGroup As DataRow() = dtOppBiDocItems.Select("numoppitemtCode=" & CCommon.ToLong(drKit("numoppitemtCode")) & " OR (numParentOppItemID=0 AND bitKitParent=0) OR numParentOppItemID=" & CCommon.ToLong(drKit("numoppitemtCode")))

                        If Not arrKitItemsGroup Is Nothing AndAlso arrKitItemsGroup.Length > 0 Then
                            Dim dataView As New DataView(arrKitItemsGroup.CopyToDataTable())
                            dataView.Sort = "bitKitParent DESC, numSortOrder ASC"
                            Dim tempItems As DataTable = dataView.ToTable()

                            tempProducts = tempProducts & GetBizDocItems(dataView.ToTable(), ds.Tables(2), strDecimalpoints, True)

                            objCalculateDealAmount.CalculateDealAmount(oppId, oppBizDocId, CShort(dtOppBiDocDtl.Rows(0).Item("tintOppType")), domainId, tempItems, FromBizInvoice:=True)
                            subTotal = CCommon.GetDecimalFormatService(objCalculateDealAmount.TotalAmount, 2)
                            taxAmt = CCommon.GetDecimalFormatService(objCalculateDealAmount.TotalTaxAmount - objCalculateDealAmount.TotalCRVTaxAmount, 2)
                            crvTaxAmt = CCommon.GetDecimalFormatService(objCalculateDealAmount.TotalCRVTaxAmount, 2)
                            lateCharge = CCommon.GetDecimalFormatService(objCalculateDealAmount.TotalLateCharges, 2)
                            disc = CCommon.GetDecimalFormatService(objCalculateDealAmount.TotalDiscount, 2)
                            creditAmount = CCommon.GetDecimalFormatService(IIf(objCalculateDealAmount.CreditAmount > 0, objCalculateDealAmount.CreditAmount * -1, 0), 2)
                            grandTotal = CCommon.ToDouble(objCalculateDealAmount.GrandTotal) - CCommon.ToDouble(objCalculateDealAmount.CreditAmount)

                            tempProducts = tempProducts & "<br/>" & GetBizDocSummary(dtOppBiDocDtl, tempItems, domainId, subTotal, taxAmt, crvTaxAmt, lateCharge, disc, creditAmount, grandTotal, 2, True) & "<br/>"
                        End If
                    Next

                    strBizDocUI = strBizDocUI.Replace("#Products#", tempProducts)
                    strBizDocUI = strBizDocUI.Replace("#BizDocSummary#", "")
                Else
                    strBizDocUI = strBizDocUI.Replace("#Products#", "")
                    strBizDocUI = strBizDocUI.Replace("#BizDocSummary#", "")
                End If
            Else
                'Renders Items Table
                strBizDocUI = strBizDocUI.Replace("#Products#", GetBizDocItems(ds.Tables(0), ds.Tables(2), strDecimalpoints, False))

                'Renders Summary Table
                strBizDocUI = strBizDocUI.Replace("#BizDocSummary#", GetBizDocSummary(dtOppBiDocDtl, ds.Tables(0), domainId, subTotal, taxAmt, crvTaxAmt, lateCharge, disc, creditAmount, grandTotal, strDecimalpoints, False))
            End If

            Return strBizDocUI
        End Function
        Public Function GetBizDocItems(ByVal dtOppBiDocItems As DataTable, ByVal dtdgColumns As DataTable, ByVal strDecimalPoint As String, ByVal isGroup As Boolean) As String
            Try
                Dim stringBuilder As New StringBuilder
                stringBuilder.Append("<table style=""background-color:White;border-style:None;font-size:10px;width:100%;border-collapse:collapse;"">")
                Dim i As Integer
                Dim j As Integer

                If dtdgColumns.Rows.Count > 0 Then
                    'Generates Header Row
                    stringBuilder.Append("<tr class=""ItemHeader"">")
                    For i = 0 To dtdgColumns.Rows.Count - 1

                        If CCommon.ToString(dtdgColumns.Rows(i).Item("vcDbColumnName")) = "txtItemDesc" And dtdgColumns.Rows.Count = 2 Then
                            stringBuilder.Append("<td class=""TwoColumns"">" + CCommon.ToString(dtdgColumns.Rows(i).Item("vcFormFieldName")) + "</td>")
                        ElseIf CCommon.ToString(dtdgColumns.Rows(i).Item("vcDbColumnName")) = "txtItemDesc" And dtdgColumns.Rows.Count = 1 Then
                            stringBuilder.Append("<td class=""OneColumns"">" + CCommon.ToString(dtdgColumns.Rows(i).Item("vcFormFieldName")) + "</td>")
                        ElseIf CCommon.ToString(dtdgColumns.Rows(i).Item("vcDbColumnName")) = "txtItemDesc" And dtdgColumns.Rows.Count = 3 Then
                            stringBuilder.Append("<td class=""ThreeColumns"">" + CCommon.ToString(dtdgColumns.Rows(i).Item("vcFormFieldName")) + "</td>")
                        Else
                            stringBuilder.Append("<td>" + CCommon.ToString(dtdgColumns.Rows(i).Item("vcFormFieldName")) + "</td>")
                        End If

                        'TODO: Implement date format code
                        'If dtdgColumns.Rows(i).Item("vcFieldDataType") = "M" Then bColumn.DataFormatString = CCommon.GetDataFormatString()
                    Next
                    stringBuilder.Append("</tr>")

                    For j = 0 To dtOppBiDocItems.Rows.Count - 1
                        If j Mod 2 = 0 Then
                            stringBuilder.Append("<tr class=""ItemStyle" & If(isGroup, " kit-item", "") & """>")
                        Else
                            stringBuilder.Append("<tr class=""AltItemStyle" & If(isGroup, " kit-item", "") & """>")
                        End If

                        For Each drColumn As DataRow In dtdgColumns.Rows
                            If CCommon.ToString(drColumn("vcDbColumnName")) = "txtItemDesc" Then
                                stringBuilder.Append("<td class=""BizDocItemDesc"">")
                            ElseIf CCommon.ToString(drColumn("vcDbColumnName")) = "SerialLotNo" Or CCommon.ToString(drColumn("vcDbColumnName")) = "vcAttributes" Then
                                stringBuilder.Append("<td class=""WordWrapSerialNo"">")
                            Else
                                stringBuilder.Append("<td>")
                            End If


                            If CCommon.ToString(drColumn("vcDbColumnName")) = "monUnitSalePrice" Or CCommon.ToString(drColumn("vcDbColumnName")) = "monPrice" Or CCommon.ToString(drColumn("vcDbColumnName")) = "numCost" Then
                                stringBuilder.Append(String.Format(CCommon.GetDataFormatStringService(strDecimalPoint), dtOppBiDocItems.Rows(j)(CCommon.ToString(drColumn("vcDbColumnName")))))
                            ElseIf CCommon.ToString(drColumn("vcFieldDataType")) = "M" Then
                                stringBuilder.Append(String.Format(CCommon.GetDataFormatStringService(2), dtOppBiDocItems.Rows(j)(CCommon.ToString(drColumn("vcDbColumnName")))))
                            Else
                                stringBuilder.Append(dtOppBiDocItems.Rows(j)(CCommon.ToString(drColumn("vcDbColumnName"))))
                            End If

                            stringBuilder.Append("</td>")
                        Next
                        stringBuilder.Append("</tr>")
                    Next
                End If

                stringBuilder.Append("</table>")

                Return stringBuilder.ToString()
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function GetBizDocSummary(ByVal dtOppBiDocDtl As DataTable,
                                           ByVal dtOppBiDocItems As DataTable,
                                          ByVal domainId As Long,
                                          ByVal subTotal As String,
                                          ByVal taxAmount As String,
                                          ByVal crvTaxAmount As String,
                                          ByVal lateCharge As String,
                                          ByVal disc As String,
                                          ByVal creditAmount As String,
                                          ByVal grandTotal As Double, ByVal strDecimalPoints As String, ByVal isGroup As Boolean) As String
            Try
                Dim stringBuilder As New StringBuilder

                stringBuilder.Append("<table id=""tblBizDocSumm"" style=""page-break-inside : avoid" & If(isGroup, ";margin-right:0;margin-left:auto;", "") & """>")

                Dim objConfigWizard As New FormConfigWizard

                If CCommon.ToInteger(dtOppBiDocDtl.Rows(0).Item("tintOppType")) = 1 Then
                    objConfigWizard.FormID = 7
                Else
                    objConfigWizard.FormID = 8
                End If

                objConfigWizard.DomainID = domainId
                objConfigWizard.BizDocID = CCommon.ToLong(dtOppBiDocDtl.Rows(0).Item("numBizDocId"))
                objConfigWizard.BizDocTemplateID = CCommon.ToLong(dtOppBiDocDtl.Rows(0).Item("numBizDocTempID"))

                Dim dsNew As DataSet
                Dim dtTable As DataTable
                dsNew = objConfigWizard.GetFieldFormListForBizDocsSumm
                dtTable = dsNew.Tables(1)

                For Each dr As DataRow In dtTable.Rows

                    stringBuilder.Append("<tr>")
                    stringBuilder.Append("<td class=""normal1"">")
                    stringBuilder.Append(CCommon.ToString(dr("vcFormFieldName")) & ": ")
                    stringBuilder.Append("</td>")
                    stringBuilder.Append("<td class=""normal1"">")
                    If CCommon.ToString(dr("vcDbColumnName")) = "SubTotal" Then
                        stringBuilder.Append(dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + subTotal)
                    ElseIf CCommon.ToString(dr("vcDbColumnName")) = "ShippingAmount" Then
                        stringBuilder.Append(dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + CCommon.GetDecimalFormatService(dtOppBiDocDtl.Rows(0).Item("monShipCost"), 2))
                    ElseIf CCommon.ToString(dr("vcDbColumnName")) = "TotalSalesTax" Then
                        stringBuilder.Append(dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + taxAmount)
                    ElseIf CCommon.ToString(dr("vcDbColumnName")) = "TotalCRVTax" Then
                        stringBuilder.Append(dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + crvTaxAmount)
                    ElseIf CCommon.ToString(dr("vcDbColumnName")) = "LateCharge" Then
                        stringBuilder.Append(dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + lateCharge)
                    ElseIf CCommon.ToString(dr("vcDbColumnName")) = "Discount" Then
                        stringBuilder.Append(dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + disc)
                    ElseIf CCommon.ToString(dr("vcDbColumnName")) = "CreditApplied" Then
                        stringBuilder.Append(dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + creditAmount)
                    ElseIf CCommon.ToString(dr("vcDbColumnName")) = "GrandTotal" Then
                        stringBuilder.Append(dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + CCommon.GetDecimalFormatService(grandTotal, 2))
                    Else
                        If CCommon.ToInteger(dtOppBiDocDtl.Rows(0).Item("tintOppType")) = 1 Then
                            Dim taxAmt As Double
                            taxAmt = CCommon.ToDouble(IIf(IsDBNull(dtOppBiDocItems.Compute("SUM(""" & CCommon.ToString(dr("vcFormFieldName")) & """)", """" & CCommon.ToString(dr("vcFormFieldName")) & """>0")), 0, dtOppBiDocItems.Compute("SUM(""" & CCommon.ToString(dr("vcFormFieldName")) & """)", """" & CCommon.ToString(dr("vcFormFieldName")) & """>0")))
                            stringBuilder.Append(dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + CCommon.GetDecimalFormatService(taxAmt, 2))
                        End If
                    End If
                    stringBuilder.Append("</td>")
                    stringBuilder.Append("<tr>")
                Next

                stringBuilder.Append("</table>")

                Return stringBuilder.ToString()
            Catch ex As Exception
                Throw
            End Try
        End Function
#End Region

        Public Sub DateFieldWorkFlowExecution()
            Try
                Dim objWF As New Workflow
                Dim dtDateFieldWF As DataTable = objWF.GetDateFieldWorkFlows()

                If dtDateFieldWF.Rows.Count > 0 Then
                    For Each dr As DataRow In dtDateFieldWF.Rows

                        ''If Pending execution then change Status to InProcess
                        'If CCommon.ToShort(dr("tintProcessStatus")) = enmWFQueueProcessStatus.PendingExecution Then
                        '    objWF = New Workflow
                        '    With objWF
                        '        .WFQueueID = CCommon.ToLong(dr("numWFQueueID"))
                        '        .DomainID = CCommon.ToLong(dr("numDomainID"))
                        '        .tintProcessStatus = CCommon.ToShort(enmWFQueueProcessStatus.InProcess)
                        '        .ManageWorkFlowQueue(1)
                        '    End With
                        'End If

                        objWF.WFID = CCommon.ToLong(dr("numWFID"))
                        objWF.DomainID = CCommon.ToLong(dr("numDomainID"))

                        Dim ds As DataSet
                        ds = objWF.GetWorkFlowMasterDetail

                        Dim objWFObject As New WorkFlowObject

                        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            Dim dtWFList As DataTable = ds.Tables(0)
                            Dim dtWFTriggerFields As DataTable = ds.Tables(1)
                            Dim dtWFConditions As DataTable = ds.Tables(2)
                            Dim dtWFActions As DataTable = ds.Tables(3)
                            Dim dtWFActionUpdateFields As DataTable = ds.Tables(4)

                            If CCommon.ToShort(dtWFList.Rows(0)("numFormID")) = 71 Then
                                objWF.FormID = 49
                            Else
                                objWF.FormID = CCommon.ToShort(dtWFList.Rows(0)("numFormID"))
                            End If

                            ' objWF.RecordID = CCommon.ToLong(dr("numRecordID"))
                            objWF.intDays = CCommon.ToInteger(dtWFList.Rows(0)("intDays"))
                            objWF.intActionOn = CCommon.ToInteger(dtWFList.Rows(0)("intActionOn"))
                            objWF.vcDateField = CCommon.ToString(dtWFList.Rows(0)("vcDateField"))

                            objWF.numFieldID = CCommon.ToLong(dtWFList.Rows(0)("numFieldID"))
                            objWF.boolCustom = CCommon.ToBool(dtWFList.Rows(0)("bitCustom"))

                            'added By:Sachin ;for Test
                            'purpose: problem in getting 
                            DomainID = CCommon.ToLong(dr("numDomainID"))
                            'end of code test

                            Dim strTablename As String = Nothing
                            Dim moduleId As Integer = 0
                            Select Case objWF.FormID
                                Case 68 'Organization
                                    strTablename = "DivisionMaster"
                                    moduleId = 1
                                Case 70 'Opportunities & Orders                                  
                                    strTablename = "OpportunityMaster"
                                    moduleId = 2
                                Case 49 'BizDocs
                                    strTablename = "OpportunityBizDocs"
                                    moduleId = 8
                                Case 94 'Business Process
                                    strTablename = "StagePercentageDetails"
                                Case 69 'Contacts
                                    strTablename = "AdditionalContactsInformation"
                                    moduleId = 11
                                Case 73 'Projects
                                    strTablename = "ProjectsMaster"
                                    moduleId = 4
                                Case 72 'Cases
                                    strTablename = "Cases"
                                    moduleId = 5
                                Case 124 'Tickler/Action Items
                                    strTablename = "Communication"
                                    moduleId = 6
                                Case 148 'Website page
                                    strTablename = "DivisionMaster"
                                    moduleId = 1
                            End Select

                            ' Fetch records based on DateField Conditons As Queue
                            Dim DFCond As String = ""
                            Dim DFCondCustom As String = ""
                            Dim dtDateFieldWFQueue As DataTable
                            If dtWFConditions.Rows.Count > 0 Then
                                For Each Cond As DataRow In dtWFConditions.Rows
                                    If (CCommon.ToBool(Cond("bitCustom")) = True) Then
                                        If (Cond("vcFilterOperator").ToString = "Before") Then
                                            DFCondCustom = DFCondCustom + " AND CAST(Fld_Value AS DATE) = (CAST(TIMEZONE('UTC',now())::TIMESTAMP + make_interval(days => -1 * " & CCommon.ToInteger(Cond("vcFilterValue")).ToString() & ")" & " AS DATE))"

                                        ElseIf (Cond("vcFilterOperator").ToString = "After") Then
                                            DFCondCustom = DFCondCustom + "AND CAST(Fld_Value AS DATE) = (CAST(TIMEZONE('UTC',now())::TIMESTAMP + make_interval(days => -1 * " & CCommon.ToInteger(Cond("vcFilterValue")).ToString() & ")" & " AS DATE))"
                                        End If
                                    Else
                                        If (Cond("vcFilterOperator").ToString = "Before") Then
                                            DFCond = DFCond + " AND CAST( " & Cond("vcDbColumnName").ToString() & " AS DATE) = (CAST(TIMEZONE('UTC',now())::TIMESTAMP + make_interval(days => -1 * " & CCommon.ToInteger(Cond("vcFilterValue")).ToString() & ")" & " AS DATE))"

                                        ElseIf (Cond("vcFilterOperator").ToString = "After") Then
                                            DFCond = DFCond + "AND CAST( " & Cond("vcDbColumnName").ToString() & " AS DATE) = (CAST(TIMEZONE('UTC',now())::TIMESTAMP + make_interval(days => -1 * " & CCommon.ToInteger(Cond("vcFilterValue")).ToString() & ")" & " AS DATE))"
                                        End If
                                    End If


                                Next
                            End If

                            dtDateFieldWFQueue = objWF.GetDateFieldWorkFlowQueue(objWF.FormID, objWF.WFID, DFCond, DFCondCustom)

                            ' Execute WF for fetched records

                            If (dtDateFieldWFQueue.Rows.Count > 0) Then
                                For Each drRecord As DataRow In dtDateFieldWFQueue.Rows
                                    objWF.RecordID = CCommon.ToLong(drRecord("numRecordID"))
                                    ExecuteDateFieldWFConditionsNActions(objWF, dtWFConditions, strTablename, dtWFActions, dr, moduleId, dtWFActionUpdateFields)
                                Next
                            End If

                        Else
                            objWF = New Workflow
                            With objWF
                                .WFQueueID = 0 'CCommon.ToLong(dr("numWFQueueID"))
                                .DomainID = CCommon.ToLong(dr("numDomainID"))
                                .tintProcessStatus = CCommon.ToShort(enmWFQueueProcessStatus.NoWFFound)
                                .ManageWorkFlowQueue(1)
                            End With
                        End If
                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub ExecuteDateFieldWFConditionsNActions(ByVal objWF As Workflow, ByVal dtWFConditions As DataTable, ByVal strTablename As String, ByVal dtWFActions As DataTable, ByVal dr As DataRow, ByVal moduleId As Integer, ByVal dtWFActionUpdateFields As DataTable)

            Dim dtFieldsList As DataTable
            dtFieldsList = objWF.GetWorkFlowFormFieldMaster()
            'Check Condition
            Dim boolCondition As Boolean = True
            If dtWFConditions.Rows.Count > 0 Then
                Dim Sql As New System.Text.StringBuilder
                Dim SqlFrom As New System.Text.StringBuilder
                Dim SqlCondition As New System.Text.StringBuilder
                Dim CFW_Fld_Values_Opp As New ArrayList() 'Orders(70)
                Dim CFW_Fld_Values As New ArrayList() 'Organization(68)
                Dim CFW_FLD_Values_Pro As New ArrayList() 'Projects(73)
                Dim CFW_FLD_Values_Case As New ArrayList() 'Cases(72)
                Dim CFW_FLD_Values_Cont As New ArrayList() 'Contacts(69)
                SqlFrom.Append("Select count(*) FROM")
                SqlCondition.Append(" WHERE 1=1")

                Select Case objWF.FormID
                    Case 68 'Organization
                        If (objWF.vcDateField = "") Then
                            SqlFrom.Append(" DivisionMaster left join AdditionalContactsInformation  On DivisionMaster.numDivisionID=AdditionalContactsInformation.numDivisionId left join CompanyInfo  On DivisionMaster.numCompanyId= CompanyInfo.numCompanyId ")
                            strTablename = "DivisionMaster"
                            SqlCondition.Append(" And DivisionMaster.numDomainId = $1 And DivisionMaster.numDivisionId = $2")
                        Else
                            SqlFrom.Append(" DivisionMaster_TempDateFields left join DivisionMaster On DivisionMaster_TempDateFields.numDivisionID = DivisionMaster.numDivisionID left join AdditionalContactsInformation  On DivisionMaster.numDivisionID=AdditionalContactsInformation.numDivisionId left join CompanyInfo  On DivisionMaster.numCompanyId= CompanyInfo.numCompanyId ")
                            strTablename = "DivisionMaster_TempDateFields"
                            SqlCondition.Append(" And DivisionMaster_TempDateFields.numDomainId = $1 And DivisionMaster_TempDateFields.numDivisionId = $2")
                        End If

                    Case 70 'Opportunities & Orders
                        'FROM OpportunityMaster o inner join DivisionMaster D on d.numdivisionID=o.numDivisionId inner join CompanyInfo c on  c.numCompanyId=d.numCompanyId where  o.numOppId=$2"
                        If (objWF.vcDateField = "") Then
                            SqlFrom.Append(" OpportunityMaster left JOIN OpportunityBizDocs ON OpportunityMaster.numOppId = OpportunityBizDocs.numOppId  left join DivisionMaster on DivisionMaster.numdivisionID=OpportunityMaster.numDivisionId inner join CompanyInfo on CompanyInfo.numCompanyId=DivisionMaster.numCompanyId  left join StagePercentageDetails on StagePercentageDetails.numOppID=OpportunityMaster.numOppId")
                            strTablename = "OpportunityMaster"
                            SqlCondition.Append(" AND OpportunityMaster.numDomainId = $1 AND OpportunityMaster.numOppID = $2")
                        Else
                            SqlFrom.Append(" OpportunityMaster_TempDateFields left join OpportunityMaster On OpportunityMaster_TempDateFields.numOppID = OpportunityMaster.numOppID  left JOIN OpportunityBizDocs ON OpportunityMaster.numOppId = OpportunityBizDocs.numOppId  left join DivisionMaster on DivisionMaster.numdivisionID=OpportunityMaster.numDivisionId inner join CompanyInfo on CompanyInfo.numCompanyId=DivisionMaster.numCompanyId  left join StagePercentageDetails on StagePercentageDetails.numOppID=OpportunityMaster.numOppId ")
                            strTablename = "OpportunityMaster_TempDateFields"
                            SqlCondition.Append(" AND OpportunityMaster_TempDateFields.numDomainId = $1 AND OpportunityMaster_TempDateFields.numOppID = $2")
                        End If

                    Case 49 'BizDocs
                        If (objWF.vcDateField = "") Then
                            SqlFrom.Append(" OpportunityBizDocs INNER JOIN OpportunityMaster ON OpportunityMaster.numOppId = OpportunityBizDocs.numOppId inner join DivisionMaster  on DivisionMaster.numdivisionID=OpportunityMaster.numDivisionId inner join CompanyInfo  on  CompanyInfo.numCompanyId=DivisionMaster.numCompanyId")
                            strTablename = "OpportunityBizDocs"
                            SqlCondition.Append(" AND OpportunityMaster.numDomainId = $1 AND OpportunityBizDocs.numOppBizDocsId = $2")
                        Else
                            SqlFrom.Append(" OpportunityBizDocs_TempDateFields INNER JOIN OpportunityMaster ON OpportunityMaster.numOppId = OpportunityBizDocs.numOppId inner join DivisionMaster  on DivisionMaster.numdivisionID=OpportunityMaster.numDivisionId inner join CompanyInfo  on  CompanyInfo.numCompanyId=DivisionMaster.numCompanyId")
                            strTablename = "OpportunityBizDocs_TempDateFields"
                            SqlCondition.Append(" AND OpportunityMaster.numDomainId = $1 AND OpportunityBizDocs_TempDateFields.numOppBizDocsId = $2")
                        End If

                    Case 94 'Business Process
                        If (objWF.vcDateField = "") Then
                            SqlFrom.Append(" StagePercentageDetails Inner join ProjectProgress On (ProjectProgress.numProId=StagePercentageDetails.numProjectID OR ProjectProgress.numOppId=StagePercentageDetails.numOppId) ")
                            strTablename = "StagePercentageDetails"
                            SqlCondition.Append(" AND StagePercentageDetails.numDomainId = $1 AND StagePercentageDetails.numStageDetailsId = $2")
                        Else
                            SqlFrom.Append(" StagePercentageDetails_TempDateFields left join StagePercentageDetails On StagePercentageDetails_TempDateFields.numStageDetailsId = StagePercentageDetails.numStageDetailsId Inner join ProjectProgress On (ProjectProgress.numProId=StagePercentageDetails.numProjectID OR ProjectProgress.numOppId=StagePercentageDetails.numOppId) ")
                            strTablename = "StagePercentageDetails_TempDateFields"
                            SqlCondition.Append(" AND StagePercentageDetails_TempDateFields.numDomainId = $1 AND StagePercentageDetails_TempDateFields.numStageDetailsId = $2")
                        End If

                    Case 69 'Contacts
                        If (objWF.vcDateField = "") Then
                            SqlFrom.Append(" AdditionalContactsInformation left join DivisionMaster on DivisionMaster.numdivisionID=AdditionalContactsInformation.numDivisionId left join CompanyInfo on  CompanyInfo.numCompanyId=DivisionMaster.numCompanyId")
                            strTablename = "AdditionalContactsInformation"
                            SqlCondition.Append(" AND AdditionalContactsInformation.numDomainId = $1 AND AdditionalContactsInformation.numContactId = $2")
                        Else
                            SqlFrom.Append(" AdditionalContactsInformation_TempDateFields left join AdditionalContactsInformation On AdditionalContactsInformation_TempDateFields.numContactId = AdditionalContactsInformation.numContactId  left join DivisionMaster on DivisionMaster.numdivisionID=AdditionalContactsInformation.numDivisionId left join CompanyInfo on  CompanyInfo.numCompanyId=DivisionMaster.numCompanyId")
                            strTablename = "AdditionalContactsInformation_TempDateFields"
                            SqlCondition.Append(" AND AdditionalContactsInformation_TempDateFields.numDomainId = $1 AND AdditionalContactsInformation_TempDateFields.numContactId = $2")
                        End If

                    Case 73 'Projects
                        If (objWF.vcDateField = "") Then
                            SqlFrom.Append(" ProjectsMaster left join ProjectProgress On ProjectProgress.numProId=ProjectsMaster.numProId inner join DivisionMaster  on DivisionMaster.numdivisionID=ProjectsMaster.numDivisionId left join CompanyInfo  on  CompanyInfo.numCompanyId=DivisionMaster.numCompanyId  left join StagePercentageDetails on StagePercentageDetails.numProjectID=ProjectsMaster.numProId")
                            strTablename = "ProjectsMaster"
                            SqlCondition.Append(" AND ProjectsMaster.numDomainId = $1 AND ProjectsMaster.numProId = $2")
                        Else
                            SqlFrom.Append(" ProjectsMaster_TempDateFields left join ProjectsMaster On ProjectsMaster_TempDateFields.numProId = ProjectsMaster.numProId left join ProjectProgress On ProjectProgress.numProId=ProjectsMaster.numProId inner join DivisionMaster  on DivisionMaster.numdivisionID=ProjectsMaster.numDivisionId left join CompanyInfo  on  CompanyInfo.numCompanyId=DivisionMaster.numCompanyId  left join StagePercentageDetails on StagePercentageDetails.numProjectID=ProjectsMaster.numProId")
                            strTablename = "ProjectsMaster_TempDateFields"
                            SqlCondition.Append(" AND ProjectsMaster_TempDateFields.numDomainId = $1 AND ProjectsMaster_TempDateFields.numProId = $2")
                        End If

                    Case 72 'Cases
                        If (objWF.vcDateField = "") Then
                            SqlFrom.Append(" Cases left join DivisionMaster  on DivisionMaster.numdivisionID=Cases.numDivisionId left join CompanyInfo  on  CompanyInfo.numCompanyId=DivisionMaster.numCompanyId left JOIN CaseOpportunities  on Cases.numCaseId=CaseOpportunities.numCaseId  left join OpportunityMaster  on OpportunityMaster.numOppId=CaseOpportunities.numOppId")
                            strTablename = "Cases"
                            SqlCondition.Append(" AND Cases.numDomainId = $1 AND Cases.numCaseId = $2")
                        Else
                            SqlFrom.Append(" Cases_TempDateFields left join Cases On Cases_TempDateFields.numCaseId = Cases.numCaseId left join DivisionMaster  on DivisionMaster.numdivisionID=Cases.numDivisionId left join CompanyInfo  on  CompanyInfo.numCompanyId=DivisionMaster.numCompanyId left JOIN CaseOpportunities  on Cases.numCaseId=CaseOpportunities.numCaseId  left join OpportunityMaster  on OpportunityMaster.numOppId=CaseOpportunities.numOppId")
                            strTablename = "Cases_TempDateFields"
                            SqlCondition.Append(" AND Cases_TempDateFields.numDomainId = $1 AND Cases_TempDateFields.numCaseId = $2")
                        End If

                    Case 124 'Tickler/Action Items
                        If (objWF.vcDateField = "") Then
                            SqlFrom.Append(" Communication left join DivisionMaster  on DivisionMaster.numdivisionID=Communication.numDivisionId left join CompanyInfo  on  CompanyInfo.numCompanyId=DivisionMaster.numCompanyId")
                            strTablename = "Communication"
                            SqlCondition.Append(" AND Communication.numDomainId = $1 AND Communication.numCommId = $2")
                        Else
                            SqlFrom.Append(" Communication_TempDateFields left join Communication On Communication_TempDateFields.numCommId = Communication.numCommId left join DivisionMaster  on DivisionMaster.numdivisionID=Communication.numDivisionId left join CompanyInfo  on  CompanyInfo.numCompanyId=DivisionMaster.numCompanyId")
                            strTablename = "Communication_TempDateFields"
                            SqlCondition.Append(" AND Communication_TempDateFields.numDomainId = $1 AND Communication_TempDateFields.numCommId = $2")
                        End If

                End Select

                Dim tempCondition As New List(Of String)

                For Each drCFL As DataRow In dtWFConditions.Rows
                    Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))
                    If drFLCheck.Length > 0 Then
                        Dim vcCondition As String = ConditionOperator(drCFL("vcFilterOperator"), drCFL("vcFilterValue").Trim(), drFLCheck(0)("vcAssociatedControlType"), drFLCheck(0)("vcFieldDataType"), CCommon.ToBool(drFLCheck(0)("bitCustom")))

                        If drFLCheck(0)("vcOrigDbColumnName") = "tintOppType" Then
                            Dim strValue() As String = drCFL("vcFilterValue").Trim().Split(",")
                            Dim tempCondition1 As New List(Of String)

                            For i As Integer = 0 To strValue.Length - 1
                                Select Case strValue(i)
                                    Case 1 'Sales Order
                                        tempCondition1.Add(String.Format("({0}.tintOppType=1 and {0}.tintOppStatus=1)", drFLCheck(0)("vcLookBackTableName")))
                                    Case 2 'Purchase Order
                                        tempCondition1.Add(String.Format("({0}.tintOppType=2 and {0}.tintOppStatus=1)", drFLCheck(0)("vcLookBackTableName")))
                                    Case 3 'Sales Opportunity
                                        tempCondition1.Add(String.Format("({0}.tintOppType=1 and {0}.tintOppStatus=0)", drFLCheck(0)("vcLookBackTableName")))
                                    Case 4 'Purchase Opportunity
                                        tempCondition1.Add(String.Format("({0}.tintOppType=2 and {0}.tintOppStatus=0)", drFLCheck(0)("vcLookBackTableName")))
                                End Select
                            Next

                            tempCondition.Add(String.Format("{0} (" & String.Join(" OR ", tempCondition1) & ")", drCFL("vcFilterANDOR")))
                        ElseIf drFLCheck(0)("vcOrigDbColumnName") = "bitPaidInFull" Then
                            Dim strValue() As String = drCFL("vcFilterValue").Trim().Split(",")
                            Dim tempCondition1 As New List(Of String)

                            For i As Integer = 0 To strValue.Length - 1
                                If CCommon.ToBool(strValue(i)) Then
                                    tempCondition1.Add("COALESCE((SELECT SUM(COALESCE(monAmountPaid,0)) FROM OpportunityBizDocs BD WHERE  BD.numOppId = OpportunityMaster.numOppId AND COALESCE(bitAuthoritativeBizDocs,0)=1),0) > COALESCE(OpportunityMaster.monDealAmount,0)")
                                Else
                                    tempCondition1.Add("COALESCE((SELECT SUM(COALESCE(monAmountPaid,0)) FROM OpportunityBizDocs BD WHERE  BD.numOppId = OpportunityMaster.numOppId AND COALESCE(bitAuthoritativeBizDocs,0)=1),0) < COALESCE(OpportunityMaster.monDealAmount,0)")
                                End If
                            Next

                            tempCondition.Add(String.Format("{0} (" & String.Join(" OR ", tempCondition1) & ")", drCFL("vcFilterANDOR")))
                        ElseIf drFLCheck(0)("vcOrigDbColumnName") = "vcInventoryStatus" Then
                            Dim strValue() As String = drCFL("vcFilterValue").Trim().Split(",")
                            Dim tempCondition1 As New List(Of String)

                            For i As Integer = 0 To strValue.Length - 1
                                Select Case strValue(i)
                                    Case 1 'Not Applicable
                                        tempCondition1.Add("CHARINDEX('Not Applicable', CheckOrderInventoryStatus(OpportunityMaster.numOppID,OpportunityMaster.numDomainID)) > 0")
                                    Case 2 'Shipped
                                        tempCondition1.Add("CHARINDEX('Shipped', CheckOrderInventoryStatus(OpportunityMaster.numOppID,OpportunityMaster.numDomainID)) > 0")
                                    Case 3 'Back Order
                                        tempCondition1.Add("CHARINDEX('BO', CheckOrderInventoryStatus(OpportunityMaster.numOppID,OpportunityMaster.numDomainID)) > 0 AND CHARINDEX('Shippable', CheckOrderInventoryStatus(OpportunityMaster.numOppID,OpportunityMaster.numDomainID)) = 0")
                                    Case 4 'Shippable
                                        tempCondition1.Add("CHARINDEX('Shippable', CheckOrderInventoryStatus(OpportunityMaster.numOppID,OpportunityMaster.numDomainID)) > 0")
                                End Select
                            Next

                            tempCondition.Add(String.Format("{0} (" & String.Join(" OR ", tempCondition1) & ")", drCFL("vcFilterANDOR")))
                        ElseIf drFLCheck(0)("vcOrigDbColumnName") = "bitPOFromSOOrSOFromPO" Then
                            Dim strValue() As String = drCFL("vcFilterValue").Trim().Split(",")
                            Dim tempCondition1 As New List(Of String)

                            If Not strValue Is Nothing AndAlso strValue.Length > 0 Then

                                Select Case drCFL("vcFilterOperator")
                                    Case "eq"
                                        If strValue(0) = "1" Then
                                            tempCondition1.Add(String.Format("1 = (CASE WHEN (SELECT COUNT(*) FROM OpportunityLinking WHERE numChildOppID={0}.numOppID) > 0 THEN 1 ELSE 0 END)", drFLCheck(0)("vcLookBackTableName")))
                                        ElseIf strValue(0) = "0" Then
                                            tempCondition1.Add(String.Format("1 = (CASE WHEN (SELECT COUNT(*) FROM OpportunityLinking WHERE numChildOppID={0}.numOppID) = 0 THEN 1 ELSE 0 END)", drFLCheck(0)("vcLookBackTableName")))
                                        End If
                                    Case "ne"
                                        If strValue(0) = "1" Then
                                            tempCondition1.Add(String.Format("1 = (CASE WHEN (SELECT COUNT(*) FROM OpportunityLinking WHERE numChildOppID={0}.numOppID) = 0 THEN 1 ELSE 0 END)", drFLCheck(0)("vcLookBackTableName")))
                                        ElseIf strValue(0) = "0" Then
                                            tempCondition1.Add(String.Format("1 = (CASE WHEN (SELECT COUNT(*) FROM OpportunityLinking WHERE numChildOppID={0}.numOppID) > 0 THEN 1 ELSE 0 END)", drFLCheck(0)("vcLookBackTableName")))
                                        End If
                                End Select
                            End If

                            tempCondition.Add(String.Format("{0} (" & String.Join(" OR ", tempCondition1) & ")", drCFL("vcFilterANDOR")))
                        ElseIf drFLCheck(0)("vcOrigDbColumnName") = "tintSource" Then
                            Dim strValue() As String = drCFL("vcFilterValue").Trim().Split(",")
                            Dim tempCondition1 As New List(Of String)

                            For i As Integer = 0 To strValue.Length - 1
                                Dim strValue1() As String = strValue(i).Split("~")
                                If strValue1.Length = 2 Then
                                    tempCondition1.Add(String.Format("({0}.tintSource={1} and {0}.tintSourceType={2})", drFLCheck(0)("vcLookBackTableName"), strValue1(0), strValue1(1)))
                                End If
                            Next

                            tempCondition.Add(String.Format("{0} (" & String.Join(" OR ", tempCondition1) & ")", drCFL("vcFilterANDOR")))
                        ElseIf objWF.FormID = 70 AndAlso drFLCheck(0)("vcOrigDbColumnName") = "monDealAmount" AndAlso drFLCheck(0)("vcLookBackTableName") = "OpportunityBizDocs" Then
                            'added by sachin ||Compare Fields
                            If drCFL("intCompare") = 1 Then 'comparing with Fields
                                tempCondition.Add(String.Format("{3} COALESCE((SELECT SUM(COALESCE(BD.monDealAmount,0)) FROM   OpportunityBizDocs BD inner join OpportunityMaster on  BD.numOppId = OpportunityMaster.numOppId Where  OpportunityMaster.numOppID = $2 and COALESCE(BD.bitAuthoritativeBizDocs,0)=1),0) {2}  (COALESCE((SELECT SUM(COALESCE({0}.{4},0)) FROM   OpportunityBizDocs  inner join OpportunityMaster on  OpportunityBizDocs.numOppId = OpportunityMaster.numOppId  WHERE OpportunityMaster.numOppID = $2 and  COALESCE(OpportunityBizDocs.bitAuthoritativeBizDocs,0)=1),0))",
                                           drFLCheck(0)("vcLookBackTableName"), drFLCheck(0)("vcOrigDbColumnName"), vcCondition, drCFL("vcFilterANDOR"), drCFL("vcFilterValue")))

                            Else
                                tempCondition.Add(String.Format("{3} COALESCE((SELECT SUM(COALESCE(BD.monDealAmount,0)) FROM   OpportunityBizDocs BD inner join OpportunityMaster on  BD.numOppId = OpportunityMaster.numOppId Where OpportunityMaster.numOppID = $2 and COALESCE(BD.bitAuthoritativeBizDocs,0)=1),0) {2}",
                                           drFLCheck(0)("vcLookBackTableName"), drFLCheck(0)("vcOrigDbColumnName"), vcCondition, drCFL("vcFilterANDOR")))
                            End If
                        ElseIf objWF.FormID = 49 AndAlso drFLCheck(0)("vcOrigDbColumnName") = "monAmountPaid" AndAlso drFLCheck(0)("vcLookBackTableName") = "OpportunityBizDocs" Then
                            If (drCFL("vcFilterValue").ToString = "1") Then
                                tempCondition.Add(String.Format("{1} (COALESCE(OpportunityBizDocs.monDealAmount,0) > 0 AND COALESCE(OpportunityBizDocs.monAmountPaid,0) >= COALESCE(OpportunityBizDocs.monDealAmount,0))",
                                                                                                 drFLCheck(0)("vcLookBackTableName"), drCFL("vcFilterANDOR")))
                            ElseIf (drCFL("vcFilterValue").ToString = "2") Then
                                tempCondition.Add(String.Format("{1} (COALESCE(OpportunityBizDocs.monDealAmount,0) > 0 AND COALESCE(OpportunityBizDocs.monAmountPaid,0) < COALESCE(OpportunityBizDocs.monDealAmount,0))",
                                                                                                drFLCheck(0)("vcLookBackTableName"), drCFL("vcFilterANDOR")))
                            ElseIf (drCFL("vcFilterValue").ToString = "3") Then
                                tempCondition.Add(String.Format("{1} (COALESCE(OpportunityBizDocs.monDealAmount,0) > 0 AND COALESCE(OpportunityBizDocs.monAmountPaid,0) = 0)",
                                                                                              drFLCheck(0)("vcLookBackTableName"), drCFL("vcFilterANDOR")))
                            End If
                        ElseIf objWF.FormID = 70 AndAlso drFLCheck(0)("vcOrigDbColumnName") = "monAmountPaid" AndAlso drFLCheck(0)("vcLookBackTableName") = "OpportunityBizDocs" Then
                            'added by sachin ||Compare Fields
                            If drCFL("intCompare") = 1 Then 'comparing with Fields
                                tempCondition.Add(String.Format("{3} COALESCE((SELECT SUM(COALESCE(BD.monAmountPaid,0)) FROM   OpportunityBizDocs BD inner join OpportunityMaster on  BD.numOppId = OpportunityMaster.numOppId Where OpportunityMaster.numOppID = $2 and COALESCE(BD.bitAuthoritativeBizDocs,0)=1),0) {2}  (COALESCE((SELECT SUM(COALESCE({0}.{4},0)) FROM   OpportunityBizDocs   inner join OpportunityMaster on  OpportunityBizDocs.numOppId = OpportunityMaster.numOppId  WHERE  OpportunityMaster.numOppID = $2 and COALESCE(OpportunityBizDocs.bitAuthoritativeBizDocs,0)=1),0))",
                                           drFLCheck(0)("vcLookBackTableName"), drFLCheck(0)("vcOrigDbColumnName"), vcCondition, drCFL("vcFilterANDOR"), drCFL("vcFilterValue")))

                            Else
                                If (drCFL("vcFilterValue").ToString = "0") Then
                                    tempCondition.Add(String.Format("{1} 1 = (SELECT COALESCE((SELECT SUM(COALESCE(monAmountPaid,0)) FROM  OpportunityBizDocs BD WHERE  BD.numOppId = OpportunityMaster.numOppId AND OpportunityMaster.numOppID = $2 AND COALESCE(bitAuthoritativeBizDocs,0)=1),0))",
                                                                                              drFLCheck(0)("vcLookBackTableName"), drCFL("vcFilterANDOR")))
                                ElseIf (drCFL("vcFilterValue").ToString = "1") Then
                                    tempCondition.Add(String.Format("{1} 1 = (SELECT (CASE WHEN (SELECT (CASE WHEN (SUM(COALESCE(monDealAmount,0)) = SUM(COALESCE(monAmountPaid,0)) AND SUM(COALESCE(monAmountPaid,0)) <> 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE BD.numOppId =  OpportunityMaster.numOppId AND OpportunityMaster.numOppID = $2 AND COALESCE(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END))",
                                                                                             drFLCheck(0)("vcLookBackTableName"), drCFL("vcFilterANDOR")))
                                ElseIf (drCFL("vcFilterValue").ToString = "2") Then
                                    tempCondition.Add(String.Format("{1} 1 = (SELECT CASE WHEN (SELECT (CASE WHEN ((SUM(COALESCE(monDealAmount,0)) - SUM(COALESCE(monAmountPaid,0))) > 0 AND SUM(COALESCE(monAmountPaid,0)) <> 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE  BD.numOppId = OpportunityMaster.numOppId AND OpportunityMaster.numOppID = $2 AND COALESCE(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END)",
                                                                                            drFLCheck(0)("vcLookBackTableName"), drCFL("vcFilterANDOR")))
                                ElseIf (drCFL("vcFilterValue").ToString = "3") Then
                                    tempCondition.Add(String.Format("{1} 1 = (SELECT CASE WHEN (SELECT (CASE WHEN (SUM(COALESCE(monAmountPaid,0)) = 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE  BD.numOppId = OpportunityMaster.numOppId AND OpportunityMaster.numOppID = $2 AND COALESCE(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END)",
                                                                                          drFLCheck(0)("vcLookBackTableName"), drCFL("vcFilterANDOR")))
                                End If
                                'tempCondition.Add(String.Format("{3} COALESCE((SELECT SUM(COALESCE(BD.monAmountPaid,0)) FROM   OpportunityBizDocs BD inner join OpportunityMaster on  BD.numOppId = OpportunityMaster.numOppId Where  OpportunityMaster.numOppID = $2 and COALESCE(BD.bitAuthoritativeBizDocs,0)=1),0) {2}",
                                '                   drFLCheck(0)("vcLookBackTableName"), drFLCheck(0)("vcOrigDbColumnName"), vcCondition, drCFL("vcFilterANDOR")))
                            End If

                        ElseIf objWF.FormID = 70 AndAlso drFLCheck(0)("vcLookBackTableName") = "OpportunityBizDocs" Then
                            tempCondition.Add(String.Format("{3} {0}.{1}{2}",
                                            drFLCheck(0)("vcLookBackTableName"), drFLCheck(0)("vcOrigDbColumnName"), vcCondition, drCFL("vcFilterANDOR")))

                        ElseIf drFLCheck(0)("vcOrigDbColumnName") = "numState" Then
                            If drFLCheck(0)("vcDbColumnName") = "numBillState" Then
                                tempCondition.Add(String.Format("{3} COALESCE((SELECT numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,1::SMALLINT) LIMIT 1),0) {2}",
                                           drFLCheck(0)("vcLookBackTableName"), drFLCheck(0)("vcOrigDbColumnName"), vcCondition, drCFL("vcFilterANDOR")))
                            ElseIf drFLCheck(0)("vcDbColumnName") = "numShipState" Then
                                tempCondition.Add(String.Format("{3} COALESCE((SELECT numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2::SMALLINT) LIMIT 1),0) {2}",
                                           drFLCheck(0)("vcLookBackTableName"), drFLCheck(0)("vcOrigDbColumnName"), vcCondition, drCFL("vcFilterANDOR")))
                            End If

                        ElseIf CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                            'added by sachin ||Compare Fields
                            If drCFL("intCompare") = 1 Then 'comparing with Fields

                                tempCondition.Add(String.Format("{3} {0}.{1}{2}{0}.{4}",
                                            drFLCheck(0)("vcLookBackTableName"), drFLCheck(0)("vcOrigDbColumnName"), vcCondition, drCFL("vcFilterANDOR"), drCFL("vcFilterValue")))
                            ElseIf drFLCheck(0)("vcAssociatedControlType") = "DateField" Then

                                If (drCFL("vcFilterOperator").ToString = "Before") Then
                                    vcCondition = "= (CAST(TIMEZONE('UTC',now())::TIMESTAMP + make_interval(days => -1 * " & CCommon.ToInteger(drCFL("vcFilterValue")).ToString() & ")" & " AS Date))"
                                ElseIf (drCFL("vcFilterOperator").ToString = "After") Then
                                    vcCondition = "= (CAST(TIMEZONE('UTC',now())::TIMESTAMP + make_interval(days => -1 * " & CCommon.ToInteger(drCFL("vcFilterValue")).ToString() & ")" & " AS Date))"
                                End If

                                tempCondition.Add(String.Format("{3} CAST({2}.""{0}"" AS Date){1}",
                                             drFLCheck(0)("vcOrigDbColumnName"), vcCondition, drFLCheck(0)("vcLookBackTableName"), drCFL("vcFilterANDOR")))

                            Else
                                tempCondition.Add(String.Format("{3} {0}.{1}{2}",
                                         drFLCheck(0)("vcLookBackTableName"), drFLCheck(0)("vcOrigDbColumnName"), vcCondition, drCFL("vcFilterANDOR")))
                            End If
                        Else

                            If objWF.FormID = 70 Or objWF.FormID = 49 Then 'orders Or BizDocs 
                                If CCommon.ToString(drFLCheck(0)("vcLookBackTableName")).ToUpper() = "CFW_FLD_VALUES" Then
                                    CFW_Fld_Values.Add("""" & drFLCheck(0)("numFieldID") & """")
                                Else
                                    CFW_Fld_Values_Opp.Add("""" & drFLCheck(0)("numFieldID") & """")
                                End If
                            ElseIf objWF.FormID = 68 Or objWF.FormID = 148 Then 'Organization/contacts
                                CFW_Fld_Values.Add("""" & drFLCheck(0)("numFieldID") & """")
                            ElseIf objWF.FormID = 69 Then 'Contacts
                                If CCommon.ToString(drFLCheck(0)("vcLookBackTableName")).ToUpper() = "CFW_FLD_VALUES" Then
                                    CFW_Fld_Values.Add("""" & drFLCheck(0)("numFieldID") & """")
                                Else
                                    CFW_FLD_Values_Cont.Add("""" & drFLCheck(0)("numFieldID") & """")
                                End If
                            ElseIf objWF.FormID = 73 Then 'Projects
                                If CCommon.ToString(drFLCheck(0)("vcLookBackTableName")).ToUpper() = "CFW_FLD_VALUES" Then
                                    CFW_Fld_Values.Add("""" & drFLCheck(0)("numFieldID") & """")
                                Else
                                    CFW_FLD_Values_Pro.Add("""" & drFLCheck(0)("numFieldID") & """")
                                End If
                            ElseIf objWF.FormID = 72 Then 'cases
                                If CCommon.ToString(drFLCheck(0)("vcLookBackTableName")).ToUpper() = "CFW_FLD_VALUES" Then
                                    CFW_Fld_Values.Add("""" & drFLCheck(0)("numFieldID") & """")
                                Else
                                    CFW_FLD_Values_Case.Add("""" & drFLCheck(0)("numFieldID") & """")
                                End If
                            Else
                                CFW_Fld_Values.Add("""" & drFLCheck(0)("numFieldID") & """")
                            End If

                            If drFLCheck(0)("vcAssociatedControlType") = "DateField" Then

                                If (drCFL("vcFilterOperator").ToString = "Before") Then
                                    vcCondition = "= (CAST(TIMEZONE('UTC',now())::TIMESTAMP + make_interval(days => -1 * " & CCommon.ToInteger(drCFL("vcFilterValue")).ToString() & ")" & " AS Date))"
                                ElseIf (drCFL("vcFilterOperator").ToString = "After") Then
                                    vcCondition = "= (CAST(TIMEZONE('UTC',now())::TIMESTAMP + make_interval(days => -1 * " & CCommon.ToInteger(drCFL("vcFilterValue")).ToString() & ")" & " AS Date))"
                                End If

                                tempCondition.Add(String.Format("{3} CAST({2}.""{0}"" AS Date){1}",
                                             drFLCheck(0)("numFieldID"), vcCondition, drFLCheck(0)("vcLookBackTableName"), drCFL("vcFilterANDOR")))

                            Else
                                tempCondition.Add(String.Format("{3} {2}.""{0}""{1}",
                                              drFLCheck(0)("numFieldID"), vcCondition, drFLCheck(0)("vcLookBackTableName"), drCFL("vcFilterANDOR")))
                            End If
                        End If
                    End If
                Next

                SqlCondition.Append(" AND (" & String.Join(" ", tempCondition) & ")")

                If objWF.FormID = 70 Then 'orders
                    If CFW_Fld_Values_Opp.Count > 0 Then
                        Dim strCFW_Fld_Values_Opp As String() = New String(CFW_Fld_Values_Opp.Count - 1) {}
                        CFW_Fld_Values_Opp.CopyTo(strCFW_Fld_Values_Opp)

                        SqlFrom.Append(" LEFT JOIN (Select * From crosstab('select t2.RecId,t1.fld_id,MAX(t2.Fld_Value) as Fld_Value FROM CFW_Fld_Master t1 JOIN CFW_Fld_Values_Opp AS t2 ON t1.Fld_ID = t2.Fld_ID where (t1.Grp_id = 2 or t1.Grp_id = 6) and t1.numDomainID=$1 AND t1.fld_id IN (" & String.Join(",", strCFW_Fld_Values_Opp.Distinct()).Replace("""", "") & ") AND t2.RecId=$2 AND t1.fld_type <> ''Link'' group by t2.RecId,t1.fld_id order by 1') AS TEMPCFW(RecId NUMERIC(18,0)," & String.Join(" TEXT,", strCFW_Fld_Values_Opp.Distinct()) & " TEXT))")
                        SqlFrom.Append(" as CFW_Fld_Values_Opp on CFW_Fld_Values_Opp.RecId = OpportunityMaster.numOppid ")
                    End If

                    If CFW_Fld_Values.Count > 0 Then
                        Dim strCFW_Fld_Values_Opp As String() = New String(CFW_Fld_Values.Count - 1) {}
                        CFW_Fld_Values.CopyTo(strCFW_Fld_Values_Opp)

                        SqlFrom.Append(" LEFT JOIN LATERAL (Select * From crosstab('select t2.RecId,t1.fld_id,MAX(t2.Fld_Value) as Fld_Value FROM CFW_Fld_Master t1 JOIN CFW_Fld_Values AS t2 ON t1.Fld_ID = t2.Fld_ID where (t1.Grp_id = 1 or t1.Grp_id = 4) and t1.numDomainID=$1 AND t1.fld_id IN (" & String.Join(",", strCFW_Fld_Values_Opp.Distinct()).Replace("""", "") & ") AND t2.RecId=DivisionMaster.numDivisionID AND t1.fld_type <> ''Link'' group by t2.RecId,t1.fld_id order by 1') AS TEMPCFW(RecId NUMERIC(18,0)," & String.Join(" TEXT,", strCFW_Fld_Values_Opp.Distinct()) & " TEXT))")
                        SqlFrom.Append(" as CFW_Fld_Values ON TRUE ")
                    End If
                ElseIf objWF.FormID = 49 Then 'BizDocs 
                    If CFW_Fld_Values_Opp.Count > 0 Then
                        Dim strCFW_Fld_Values_Opp As String() = New String(CFW_Fld_Values_Opp.Count - 1) {}
                        CFW_Fld_Values_Opp.CopyTo(strCFW_Fld_Values_Opp)

                        SqlFrom.Append(" LEFT JOIN LATERAL (Select * From crosstab('select t2.RecId,t1.fld_id,MAX(t2.Fld_Value) as Fld_Value FROM CFW_Fld_Master t1 JOIN CFW_Fld_Values_Opp AS t2 ON t1.Fld_ID = t2.Fld_ID where (t1.Grp_id = 2 or t1.Grp_id = 6) and t1.numDomainID=$1 AND t1.fld_id IN (" & String.Join(",", strCFW_Fld_Values_Opp.Distinct()).Replace("""", "") & ") AND t2.RecId=OpportunityMaster.numOppId AND t1.fld_type <> ''Link'' group by t2.RecId,t1.fld_id order by 1') AS TEMPCFW(RecId NUMERIC(18,0)," & String.Join(" TEXT,", strCFW_Fld_Values_Opp.Distinct()) & " TEXT))")
                        SqlFrom.Append(" as CFW_Fld_Values_Opp ON TRUE ")
                    End If

                    If CFW_Fld_Values.Count > 0 Then
                        Dim strCFW_Fld_Values_Opp As String() = New String(CFW_Fld_Values.Count - 1) {}
                        CFW_Fld_Values.CopyTo(strCFW_Fld_Values_Opp)

                        SqlFrom.Append(" LEFT JOIN LATERAL (Select * From crosstab('select t2.RecId,t1.fld_id,MAX(t2.Fld_Value) as Fld_Value FROM CFW_Fld_Master t1 JOIN CFW_Fld_Values AS t2 ON t1.Fld_ID = t2.Fld_ID where (t1.Grp_id = 1 or t1.Grp_id = 4) and t1.numDomainID=$1 AND t1.fld_id IN (" & String.Join(",", strCFW_Fld_Values_Opp.Distinct()).Replace("""", "") & ") AND t2.RecId=DivisionMaster.numDivisionID AND t1.fld_type <> ''Link'' group by t2.RecId,t1.fld_id order by 1') AS TEMPCFW(RecId NUMERIC(18,0)," & String.Join(" TEXT,", strCFW_Fld_Values_Opp.Distinct()) & " TEXT))")
                        SqlFrom.Append(" as CFW_Fld_Values ON TRUE ")
                    End If
                ElseIf objWF.FormID = 68 Or objWF.FormID = 148 Then 'Organization
                    If CFW_Fld_Values.Count > 0 Then
                        Dim strCFW_Fld_Values_Opp As String() = New String(CFW_Fld_Values.Count - 1) {}
                        CFW_Fld_Values.CopyTo(strCFW_Fld_Values_Opp)

                        SqlFrom.Append(" LEFT JOIN (Select * From crosstab('select t2.RecId,t1.fld_id,MAX(t2.Fld_Value) as Fld_Value FROM CFW_Fld_Master t1 JOIN CFW_Fld_Values AS t2 ON t1.Fld_ID = t2.Fld_ID where (t1.Grp_id = 1 or t1.Grp_id = 4) and t1.numDomainID=$1 AND t1.fld_id IN (" & String.Join(",", strCFW_Fld_Values_Opp.Distinct()).Replace("""", "") & ") AND t2.RecId=$2 AND t1.fld_type <> ''Link'' group by t2.RecId,t1.fld_id order by 1') AS TEMPCFW(RecId NUMERIC(18,0)," & String.Join(" TEXT,", strCFW_Fld_Values_Opp.Distinct()) & " TEXT))")
                        SqlFrom.Append(" as CFW_Fld_Values on CFW_Fld_Values.RecId = DivisionMaster.numDivisionID ")
                    End If
                ElseIf objWF.FormID = 69 Then 'contacts
                    If CFW_FLD_Values_Cont.Count > 0 Then
                        Dim strCFW_Fld_Values_Opp As String() = New String(CFW_FLD_Values_Cont.Count - 1) {}
                        CFW_FLD_Values_Cont.CopyTo(strCFW_Fld_Values_Opp)

                        SqlFrom.Append(" LEFT JOIN (Select * From crosstab('select t2.RecId,t1.fld_id,MAX(t2.Fld_Value) as Fld_Value FROM CFW_Fld_Master t1 JOIN CFW_FLD_Values_Cont AS t2 ON t1.Fld_ID = t2.Fld_ID where (t1.Grp_id = 1 or t1.Grp_id = 4) and t1.numDomainID=$1 AND t1.fld_id IN (" & String.Join(",", strCFW_Fld_Values_Opp.Distinct()).Replace("""", "") & ") AND t2.RecId=$2 AND t1.fld_type <> ''Link'' group by t2.RecId,t1.fld_id order by 1') AS TEMPCFW(RecId NUMERIC(18,0)," & String.Join(" TEXT,", strCFW_Fld_Values_Opp.Distinct()) & " TEXT))")
                        SqlFrom.Append(" as CFW_FLD_Values_Cont on CFW_FLD_Values_Cont.RecId = DivisionMaster.numDivisionID ")
                    End If

                    If CFW_Fld_Values.Count > 0 Then
                        Dim strCFW_Fld_Values_Opp As String() = New String(CFW_Fld_Values.Count - 1) {}
                        CFW_Fld_Values.CopyTo(strCFW_Fld_Values_Opp)

                        SqlFrom.Append(" LEFT JOIN LATERAL (Select * From crosstab('select t2.RecId,t1.fld_id,MAX(t2.Fld_Value) as Fld_Value FROM CFW_Fld_Master t1 JOIN CFW_Fld_Values AS t2 ON t1.Fld_ID = t2.Fld_ID where (t1.Grp_id = 1 or t1.Grp_id = 4) and t1.numDomainID=$1 AND t1.fld_id IN (" & String.Join(",", strCFW_Fld_Values_Opp.Distinct()).Replace("""", "") & ") AND t2.RecId=DivisionMaster.numDivisionID AND t1.fld_type <> ''Link'' group by t2.RecId,t1.fld_id order by 1') AS TEMPCFW(RecId NUMERIC(18,0)," & String.Join(" TEXT,", strCFW_Fld_Values_Opp.Distinct()) & " TEXT))")
                        SqlFrom.Append(" as CFW_Fld_Values ON TRUE ")
                    End If
                ElseIf objWF.FormID = 73 Then 'Projects
                    If CFW_FLD_Values_Pro.Count > 0 Then
                        Dim strCFW_Fld_Values_Opp As String() = New String(CFW_FLD_Values_Pro.Count - 1) {}
                        CFW_FLD_Values_Pro.CopyTo(strCFW_Fld_Values_Opp)

                        SqlFrom.Append(" LEFT JOIN (Select * From crosstab('select t2.RecId,t1.fld_id,MAX(t2.Fld_Value) as Fld_Value FROM CFW_Fld_Master t1 JOIN CFW_FLD_Values_Pro AS t2 ON t1.Fld_ID = t2.Fld_ID where (t1.Grp_id = 11 or t1.Grp_id = 6) and t1.numDomainID=$1 AND t1.fld_id IN (" & String.Join(",", strCFW_Fld_Values_Opp.Distinct()).Replace("""", "") & ") AND t2.RecId=$2 AND t1.fld_type <> ''Link'' group by t2.RecId,t1.fld_id order by 1') AS TEMPCFW(RecId NUMERIC(18,0)," & String.Join(" TEXT,", strCFW_Fld_Values_Opp.Distinct()) & " TEXT))")
                        SqlFrom.Append(" as CFW_FLD_Values_Pro on CFW_FLD_Values_Pro.RecId = ProjectsMaster.numProId ")
                    End If

                    If CFW_Fld_Values.Count > 0 Then
                        Dim strCFW_Fld_Values_Opp As String() = New String(CFW_Fld_Values.Count - 1) {}
                        CFW_Fld_Values.CopyTo(strCFW_Fld_Values_Opp)

                        SqlFrom.Append(" LEFT JOIN LATERAL (Select * From crosstab('select t2.RecId,t1.fld_id,MAX(t2.Fld_Value) as Fld_Value FROM CFW_Fld_Master t1 JOIN CFW_Fld_Values AS t2 ON t1.Fld_ID = t2.Fld_ID where (t1.Grp_id = 1 or t1.Grp_id = 4) and t1.numDomainID=$1 AND t1.fld_id IN (" & String.Join(",", strCFW_Fld_Values_Opp.Distinct()).Replace("""", "") & ") AND t2.RecId=DivisionMaster.numDivisionID AND t1.fld_type <> ''Link'' group by t2.RecId,t1.fld_id order by 1') AS TEMPCFW(RecId NUMERIC(18,0)," & String.Join(" TEXT,", strCFW_Fld_Values_Opp.Distinct()) & " TEXT))")
                        SqlFrom.Append(" as CFW_Fld_Values ON TRUE ")
                    End If
                ElseIf objWF.FormID = 72 Then 'cases
                    If CFW_FLD_Values_Case.Count > 0 Then
                        Dim strCFW_Fld_Values_Opp As String() = New String(CFW_FLD_Values_Case.Count - 1) {}
                        CFW_FLD_Values_Case.CopyTo(strCFW_Fld_Values_Opp)

                        SqlFrom.Append(" LEFT JOIN (Select * From crosstab('select t2.RecId,t1.fld_id,MAX(t2.Fld_Value) as Fld_Value FROM CFW_Fld_Master t1 JOIN CFW_FLD_Values_Case AS t2 ON t1.Fld_ID = t2.Fld_ID where (t1.Grp_id = 3 or t1.Grp_id = 6) and t1.numDomainID=$1 AND t1.fld_id IN (" & String.Join(",", strCFW_Fld_Values_Opp.Distinct()).Replace("""", "") & ") AND t2.RecId=$2 AND t1.fld_type <> ''Link'' group by t2.RecId,t1.fld_id order by 1') AS TEMPCFW(RecId NUMERIC(18,0)," & String.Join(" TEXT,", strCFW_Fld_Values_Opp.Distinct()) & " TEXT))")
                        SqlFrom.Append(" as CFW_FLD_Values_Case on CFW_FLD_Values_Case.RecId = Cases.numCaseId ")
                    End If

                    If CFW_Fld_Values.Count > 0 Then
                        Dim strCFW_Fld_Values_Opp As String() = New String(CFW_Fld_Values.Count - 1) {}
                        CFW_Fld_Values.CopyTo(strCFW_Fld_Values_Opp)

                        SqlFrom.Append(" LEFT JOIN LATERAL (Select * From crosstab('select t2.RecId,t1.fld_id,MAX(t2.Fld_Value) as Fld_Value FROM CFW_Fld_Master t1 JOIN CFW_Fld_Values AS t2 ON t1.Fld_ID = t2.Fld_ID where (t1.Grp_id = 1 or t1.Grp_id = 4) and t1.numDomainID=$1 AND t1.fld_id IN (" & String.Join(",", strCFW_Fld_Values_Opp.Distinct()).Replace("""", "") & ") AND t2.RecId=DivisionMaster.numDivisionID AND t1.fld_type <> ''Link'' group by t2.RecId,t1.fld_id order by 1') AS TEMPCFW(RecId NUMERIC(18,0)," & String.Join(" TEXT,", strCFW_Fld_Values_Opp.Distinct()) & " TEXT))")
                        SqlFrom.Append(" as CFW_Fld_Values ON TRUE ")
                    End If
                Else
                    If CFW_Fld_Values_Opp.Count > 0 Then
                        Dim strCFW_Fld_Values_Opp As String() = New String(CFW_Fld_Values_Opp.Count - 1) {}
                        CFW_Fld_Values_Opp.CopyTo(strCFW_Fld_Values_Opp)

                        SqlFrom.Append(" LEFT JOIN LATERAL (Select * From crosstab('select t2.RecId,t1.fld_id,MAX(t2.Fld_Value) as Fld_Value FROM CFW_Fld_Master t1 JOIN CFW_Fld_Values_Opp AS t2 ON t1.Fld_ID = t2.Fld_ID where (t1.Grp_id = 2 or t1.Grp_id = 6) and t1.numDomainID=$1 AND t1.fld_id IN (" & String.Join(",", strCFW_Fld_Values_Opp.Distinct()).Replace("""", "") & ") AND t2.RecId=$2 AND t1.fld_type <> ''Link'' group by t2.RecId,t1.fld_id order by 1') AS TEMPCFW(RecId NUMERIC(18,0)," & String.Join(" TEXT,", strCFW_Fld_Values_Opp.Distinct()) & " TEXT))")

                        SqlFrom.Append(" as CFW_Fld_Values_Opp on CFW_Fld_Values_Opp.RecId = OpportunityMaster.numOppid ")
                    End If
                End If

                Sql.Append(SqlFrom)
                Sql.Append(SqlCondition)
                objWF.textQuery = Sql.ToString()

                Try
                    boolCondition = objWF.WFConditionQueryExecute(1)
                Catch ex As Exception
                    Dim objWF1 As New Workflow
                    With objWF1
                        .WFQueueID = 0 'CCommon.ToLong(dr("numWFQueueID"))
                        .WFID = objWF.WFID
                        .WFDescription = "Error occured:" & ex.Message & " Query:" & Sql.ToString()
                        .ManageWorkFlowQueue(2, _bitSuccess:=False)
                        boolCondition = False
                        ' Continue For
                    End With
                End Try
            End If

            'Execute Action
            If objWF.intDays = 0 Then 'Instant action
                If boolCondition Then
                    Dim numContactId, numDivisionId, numRecOwner, numAssignedTo, numInternalPM, numExternalPM, numNextAssignedTo, numOppIDPC, numStageIDPC, numOppBizDOCIDPC, numProjectID, numCaseID, numCommID As Long
                    Dim numPhone As String
                    Dim dtRecordData As DataTable = objWF.GetWFRecordData

                    numContactId = CCommon.ToLong(dtRecordData.Rows(0)("numContactId"))
                    numDivisionId = CCommon.ToLong(dtRecordData.Rows(0)("numDivisionId"))
                    numRecOwner = CCommon.ToLong(dtRecordData.Rows(0)("numRecOwner"))
                    numAssignedTo = CCommon.ToLong(dtRecordData.Rows(0)("numAssignedTo"))
                    numInternalPM = CCommon.ToLong(dtRecordData.Rows(0)("numInternalPM"))
                    numExternalPM = CCommon.ToLong(dtRecordData.Rows(0)("numExternalPM"))
                    numNextAssignedTo = CCommon.ToLong(dtRecordData.Rows(0)("numNextAssignedTo"))
                    numPhone = CCommon.ToString(dtRecordData.Rows(0)("numPhone"))
                    numOppIDPC = CCommon.ToLong(dtRecordData.Rows(0)("numOppIDPC"))
                    numStageIDPC = CCommon.ToLong(dtRecordData.Rows(0)("numStageIDPC"))
                    numOppBizDOCIDPC = CCommon.ToLong(dtRecordData.Rows(0)("numOppBizDOCIDPC"))
                    numProjectID = CCommon.ToLong(dtRecordData.Rows(0)("numProjectID"))
                    numCaseID = CCommon.ToLong(dtRecordData.Rows(0)("numCaseId"))
                    numCommID = CCommon.ToLong(dtRecordData.Rows(0)("numCommId"))

                    For Each drAct As DataRow In dtWFActions.Rows
                        Dim WFDescription As String = ""
                        Dim bitSuccess As Boolean = False

                        Try
                            Select Case CCommon.ToShort(drAct("tintActionType"))

                                Case CCommon.ToShort(enmWFAction.SendAlerts) 'Send Email
                                    Dim dtDocDetails As DataTable
                                    Dim objDocuments As New DocumentList
                                    With objDocuments
                                        .GenDocID = drAct("numTemplateID")
                                        .DomainID = CCommon.ToLong(dr("numDomainID"))
                                        dtDocDetails = .GetDocByGenDocID
                                    End With

                                    If dtDocDetails.Rows.Count > 0 Or CCommon.ToLong(drAct("numTemplateID")) = "99999" Then

                                        Dim strBody As String = Nothing
                                        Dim strSubject As String = Nothing

                                        If CCommon.ToLong(drAct("numTemplateID")) = "99999" Then 'Compose Message
                                            strBody = drAct("vcMailBody")
                                            strSubject = drAct("vcMailSubject")
                                        Else
                                            strBody = dtDocDetails.Rows(0).Item("vcDocdesc")
                                            strSubject = IIf(IsDBNull(dtDocDetails.Rows(0).Item("vcSubject")), "", dtDocDetails.Rows(0).Item("vcSubject"))
                                        End If

                                        Dim objEmail As New Email

                                        If strBody.ToLower().Contains("##ContactOpt-OutLink##".ToLower()) Then
                                            objEmail.AdditionalMergeFields.Add("ContactOpt-OutLink", "")
                                        End If

                                        Dim dtEmailBroadCast As New DataTable
                                        Dim dtEmailBroadCastSendTo As New DataTable
                                        Dim lngUserCntID As Long = 0
                                        Dim tintEmailFrom As Short = CCommon.ToShort(drAct("tintEmailFrom"))

                                        If tintEmailFrom = 1 Then
                                            lngUserCntID = numRecOwner
                                        ElseIf tintEmailFrom = 2 Then
                                            lngUserCntID = numAssignedTo
                                        End If

                                        Dim EmailToType() As String = drAct("vcEmailToType").ToString().Split(",")
                                        Dim strContactIDs As New ArrayList
                                        Dim strContactIDWebLead As New ArrayList
                                        If EmailToType.Contains(1) Then 'Owner of trigger record
                                            strContactIDs.Add(numRecOwner)
                                        End If

                                        If EmailToType.Contains(2) Then 'Assignee of trigger record
                                            strContactIDs.Add(numAssignedTo)
                                        End If

                                        If EmailToType.Contains(3) Then 'Primary contact of trigger record
                                            strContactIDs.Add(numContactId)
                                        End If
                                        If EmailToType.Contains(4) Then 'Internal Project Manager
                                            strContactIDs.Add(numInternalPM)
                                        End If
                                        If EmailToType.Contains(5) Then 'External Project Manager
                                            strContactIDs.Add(numExternalPM)
                                        End If
                                        If EmailToType.Contains(6) Then 'Next Stage Assigned to
                                            strContactIDs.Add(numNextAssignedTo)
                                        End If
                                        If EmailToType.Contains(7) Then 'Assignee Of Trigger Record(Project)
                                            strContactIDs.Add(numNextAssignedTo)
                                        End If

                                        '  Bizdoc Attachment
                                        Dim dtTable As DataTable
                                        If CCommon.ToShort(drAct("tintIsAttachment")) = 1 Then

                                            Dim objOppBizDocs As New OppBizDocs
                                            objOppBizDocs.OppBizDocId = CCommon.ToLong(dr("numRecordID"))
                                            objOppBizDocs.DomainID = CCommon.ToLong(dr("numDomainID"))
                                            Dim dsOppData As DataSet = objOppBizDocs.GetBizDocsDetails
                                            Dim objCommon1 As New CCommon
                                            Dim dsDateFormat As DataSet = objCommon1.GetDomainDateFormat(CCommon.ToLong(dr("numDomainID")))
                                            Dim htmlCodeToConvert As String = GenerateBizDocPDF(CCommon.ToLong(dr("numRecordID")), CCommon.ToLong(dsOppData.Tables(0).Rows(0)("numOppId")), CCommon.ToLong(dr("numDomainID")), CCommon.ToLong(dtRecordData.Rows(0)("numRecOwner")), 0, dsDateFormat.Tables(0).Rows(0)("vcDateFormat").ToString())
                                            '  Dim htmlCodeToConvert As String = GenerateBizDocPDF(CCommon.ToLong(dr("numRecordID")), CCommon.ToLong(dsOppData.Tables(0).Rows(0)("numOppId")), CCommon.ToLong(dr("numDomainID")), CCommon.ToLong(dtRecordData.Rows(0)("numRecOwner")), 0, FormattedDateFromDate(dsOppData.Tables(0).Rows(0)("dtFromDate").ToString, dsDateFormat.Tables(0).Rows(0)("vcDateFormat").ToString()))
                                            ' Dim htmlCodeToConvert As String = getBizDocDetails(CCommon.ToLong(dr("numDomainID")), CCommon.ToLong(dr("numRecordID")), CCommon.ToLong(dsOppData.Tables(0).Rows(0)("numOppId")), CCommon.ToLong(dsOppData.Tables(0).Rows(0)("numBizDocId")))
                                            Dim i As Integer
                                            Dim strFileName As String = ""
                                            Dim strFilePhysicalLocation As String = ""
                                            Dim objHTMLToPDF As New HTMLToPDF

                                            strFileName = objHTMLToPDF.ConvertHTML2PDF(htmlCodeToConvert, CCommon.ToLong(dr("numDomainID")))
                                            strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(CCommon.ToLong(dr("numDomainID"))) & strFileName
                                            Dim strBizDocName As String = CCommon.ToString(dsOppData.Tables(0).Rows(0).Item("vcBizDocID"))

                                            Dim objCommon As New CCommon
                                            dtTable = New DataTable
                                            dtTable.Columns.Add("Filename")
                                            dtTable.Columns.Add("FileLocation")
                                            dtTable.Columns.Add("FilePhysicalPath")
                                            dtTable.Columns.Add("FileSize", GetType(Long))
                                            dtTable.Columns.Add("FileType")

                                            Dim dr1 As DataRow = dtTable.NewRow
                                            dr1("Filename") = strFileName
                                            dr1("FileLocation") = CCommon.GetDocumentPhysicalPath(CCommon.ToLong(dr("numDomainID"))) & strFileName
                                            dr1("FilePhysicalPath") = strFilePhysicalLocation
                                            dr1("FileType") = ".pdf"
                                            dr1("FileSize") = 15650
                                            dtTable.Rows.Add(dr1)

                                        End If
                                        'end of code for bizdoc attachment

                                        If strContactIDs.Count > 0 OrElse (CCommon.ToString(drAct("vcEmailSendTo")) IsNot Nothing) Then
                                            strContactIDWebLead.Add(numContactId)
                                            If (moduleId = 1 Or moduleId = 45) Then
                                                dtEmailBroadCast = objEmail.GetEmailMergeData(moduleId, numContactId, CCommon.ToLong(dr("numDomainID")), 0, 0)
                                            Else
                                                dtEmailBroadCast = objEmail.GetEmailMergeData(moduleId, objWF.RecordID, CCommon.ToLong(dr("numDomainID")), 0, 0)
                                            End If

                                            ' Displaying Tracking no. as link in Email (Added by Priya)
                                            If (moduleId = 2) Then
                                                For Each drrow As DataRow In dtEmailBroadCast.Rows
                                                    Dim strTrackingNo As String = ""
                                                    If (Not String.IsNullOrEmpty(CCommon.ToString(drrow("OppOrderTrackingNo")).Trim())) Then
                                                        Dim strTrackingNoList() As String = CCommon.ToString(drrow("OppOrderTrackingNo")).Split(New String() {"$^$"}, StringSplitOptions.None)
                                                        Dim strIDValue() As String

                                                        If strTrackingNoList.Length > 0 Then
                                                            For k As Integer = 0 To strTrackingNoList.Length - 1
                                                                strIDValue = strTrackingNoList(k).Split(New String() {"#^#"}, StringSplitOptions.None)
                                                                If strIDValue(1).Length > 0 Then
                                                                    strTrackingNo += String.Format("<a href='{0}' target='_blank'>{1}</a>&nbsp;&nbsp;", IIf(strIDValue(0).Length = 0, "javascript:void(0)", String.Format(strIDValue(0) + strIDValue(1), strIDValue(1))), strIDValue(1))
                                                                End If
                                                            Next
                                                        End If
                                                    End If
                                                    drrow("OppOrderTrackingNo") = "<div style='width: 95%;word-break: break-all;'>" & strTrackingNo & "</div>"
                                                Next
                                            End If

                                            dtEmailBroadCastSendTo = objEmail.GetEmailMergeData(1, String.Join(",", strContactIDs.ToArray()), CCommon.ToLong(dr("numDomainID")), 0, 0)

                                            Dim strTo As New List(Of String)
                                            For Each drEmail As DataRow In dtEmailBroadCastSendTo.Rows
                                                strTo.Add(drEmail("ContactEmail"))
                                            Next
                                            'just for patches-testing purpose

                                            'end of testing
                                            If strTo.Count > 0 OrElse (CCommon.ToString(drAct("vcEmailSendTo")) IsNot Nothing) Then
                                                For Each iKey As KeyValuePair(Of String, String) In objEmail.AdditionalMergeFields
                                                    If dtEmailBroadCast.Columns.Contains(iKey.Key) = False Then
                                                        dtEmailBroadCast.Columns.Add(iKey.Key)
                                                    End If
                                                    'Assign values to rows in merge table
                                                    For Each row As DataRow In dtEmailBroadCast.Rows
                                                        If iKey.Key <> "ContactEmail" Then
                                                            row(iKey.Key) = CCommon.ToString(iKey.Value)
                                                        End If
                                                    Next
                                                Next

                                                Dim strFinalTo As String = Nothing
                                                If CCommon.ToString(drAct("vcEmailSendTo")) = "" Or CCommon.ToString(drAct("vcEmailSendTo")) = Nothing Then
                                                    strFinalTo = String.Join(",", strTo)
                                                Else
                                                    If strTo.Count > 0 Then
                                                        strFinalTo = String.Join(",", strTo) & "," & drAct("vcEmailSendTo")
                                                    Else
                                                        strFinalTo = drAct("vcEmailSendTo")
                                                    End If

                                                End If

                                                Dim mailStatus As Boolean
                                                Dim errorMessage As String = ""

                                                If CCommon.ToShort(drAct("tintIsAttachment")) = 1 Then
                                                    'uncommnted by ss
                                                    'objEmail.SendEmailWS(strSubject, strBody, "", dtRecordData.Rows(0).Item("vcEmailID"), String.Join(",", strTo), dtEmailBroadCast, dtTable, FromDisplayName:=dtRecordData.Rows(0)("ContactName"), DomainID:=objWF.DomainID)
                                                    objEmail.AdditionalMergeFields.Add("Signature", dtRecordData.Rows(0)("Signature").ToString)
                                                    mailStatus = objEmail.SendEmailWS(strSubject, strBody, "", dtRecordData.Rows(0).Item("vcEmailID"), strFinalTo, dtEmailBroadCast, dtTable, FromDisplayName:=dtRecordData.Rows(0)("ContactName"), DomainID:=objWF.DomainID, UserCntID:=lngUserCntID, tintEmailFrom:=tintEmailFrom)
                                                    dtTable.Rows.Clear()
                                                    dtTable.Columns.Clear()
                                                Else
                                                    'objEmail.SendEmail(strSubject, strBody, "", dtRecordData.Rows(0).Item("vcEmailID"), String.Join(",", strTo), dtEmailBroadCast, FromDisplayName:=dtRecordData.Rows(0)("ContactName"), DomainID:=objWF.DomainID)
                                                    objEmail.AdditionalMergeFields.Add("Signature", dtRecordData.Rows(0)("Signature").ToString)

                                                    mailStatus = objEmail.SendEmailWFA(strSubject, strBody, "", dtRecordData.Rows(0).Item("vcEmailID"), strFinalTo, dtEmailBroadCast, FromDisplayName:=dtRecordData.Rows(0)("ContactName"), DomainID:=objWF.DomainID, ErrorMessage:=errorMessage, UserId:=lngUserCntID, tintEmailFrom:=tintEmailFrom)

                                                End If

                                                ' WFDescription = "Email Sent to " & String.Join(",", strTo)
                                                If mailStatus = True Then
                                                    If strFinalTo = "" Or strFinalTo = Nothing Then
                                                        WFDescription = "Unable to send a mail.Receiver Email Address doesn't exist."
                                                    Else
                                                        WFDescription = "Email Sent to " & strFinalTo
                                                    End If
                                                    bitSuccess = True
                                                Else
                                                    WFDescription = "Email not Sent. Error:" & errorMessage & ", Please Configure SMTP details.GO to Global Settings->General  "
                                                    bitSuccess = False
                                                End If

                                            End If
                                        End If
                                    End If
                                Case CCommon.ToShort(enmWFAction.AssignActionItems)
                                    Dim dtActionItem As DataTable
                                    Dim objActionItem As New ActionItem
                                    With objActionItem
                                        .RowID = drAct("numTemplateID")
                                    End With
                                    dtActionItem = objActionItem.LoadThisActionItemTemplateData()

                                    If dtActionItem.Rows.Count > 0 Then
                                        With objActionItem
                                            .CommID = 0
                                            .Task = CLng(dtActionItem.Rows(0)("numTaskType"))

                                            .ContactID = numContactId
                                            .DivisionID = numDivisionId

                                            .Details = ""
                                            .AssignedTo = IIf(CShort(drAct("tintTicklerActionAssignedTo")) = 1, numRecOwner, numAssignedTo)
                                            .UserCntID = numRecOwner
                                            .DomainID = dr("numDomainID")
                                            If .Task = 973 Then
                                                .BitClosed = 1
                                            Else : .BitClosed = 0
                                            End If
                                            .CalendarName = ""
                                            Dim strDate As DateTime = Date.UtcNow.AddDays(Convert.ToDouble(dtActionItem.Rows(0)("DueDays")))

                                            .StartTime = CDate(strDate.ToString("MM/dd/yyyy 00:00:00") & "AM")
                                            .EndTime = CDate(strDate.ToString("MM/dd/yyyy 23:59:59") & "PM")

                                            .Activity = CLng(dtActionItem.Rows(0)("Activity"))
                                            .Status = CLng(dtActionItem.Rows(0)("Priority"))
                                            .Snooze = 0
                                            .SnoozeStatus = 0
                                            .Remainder = 0
                                            .RemainderStatus = 0
                                            .ClientTimeZoneOffset = 0
                                            .bitOutlook = 0
                                            .SendEmailTemplate = CBool(dtActionItem.Rows(0).Item("bitSendEmailTemp"))
                                            .EmailTemplate = CLng(dtActionItem.Rows(0).Item("numEmailTemplate"))
                                            .Hours = CShort(dtActionItem.Rows(0).Item("tintHours"))
                                            .Alert = CBool(dtActionItem.Rows(0).Item("bitAlert"))
                                            .ActivityId = 0
                                            .FollowUpAnyTime = False
                                        End With

                                        numCommID = objActionItem.SaveCommunicationinfo()

                                        'Added By Sachin Sadhu||Date:4thAug2014
                                        'Purpose :To Added Ticker data in work Flow queue based on created Rules
                                        '          Using Change tracking
                                        Dim objWfA As New Workflow()
                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                        objWfA.UserCntID = numRecOwner
                                        objWfA.RecordID = numCommID
                                        objWfA.SaveWFActionItemsQueue()
                                        'ss//end of code

                                        'CAlerts.SendAlertToAssignee(6, numRecOwner, objActionItem.AssignedTo, numcommId, dr("numDomainID"))

                                        WFDescription = "Action Items/Ticker Created"
                                        bitSuccess = True
                                    End If
                                Case CCommon.ToShort(enmWFAction.UpdateFields)
                                    Dim Sql As New System.Text.StringBuilder
                                    Dim SqlFrom As New System.Text.StringBuilder
                                    Dim SqlCondition As New System.Text.StringBuilder

                                    'If CCommon.ToShort(dtWFList.Rows(0)("numFormID")) = 71 Then
                                    '    objWF.FormID = 49
                                    'Else
                                    '    objWF.FormID = CCommon.ToShort(dtWFList.Rows(0)("numFormID"))
                                    'End If

                                    'Select Case CCommon.ToShort(dtWFList.Rows(0)("numFormID"))

                                    Dim tempUpdate As New List(Of String)

                                    Dim drWFActionUpdateFields() As DataRow = dtWFActionUpdateFields.Select("numWFActionID=" & drAct("numWFActionID"))

                                    Select Case objWF.FormID
                                        Case 68 'Organization
                                            If drWFActionUpdateFields.Length > 0 Then
                                                Dim strFiels As String = ""
                                                For Each drCFL As DataRow In drWFActionUpdateFields
                                                    Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))
                                                    Dim boolInsert As Boolean = True
                                                    If drFLCheck.Length > 0 Then
                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                            If drFLCheck(0)("vcLookBackTableName") = "AdditionalContactsInformation" Then 'Organization
                                                                Sql.Append("Update AdditionalContactsInformation a set ")
                                                                SqlCondition.Append(" FROM  DivisionMaster d where d.numDomainId = $1 AND d.numDivisionId = $2 AND d.numDivisionID=a.numDivisionId ")
                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "CompanyInfo" Then 'Company Info
                                                                Sql.Append("Update CompanyInfo c set ")
                                                                SqlCondition.Append("  FROM DivisionMaster d where d.numDomainId = $1 AND d.numDivisionId = $2 AND d.numCompanyId=c.numCompanyId ")
                                                            ElseIf drCFL("FieldName").ToString = "Signature Type" Then
                                                                Sql.Append("DELETE FROM DivisionMasterShippingConfiguration WHERE numDomainID=$1 AND numDivisionID=$2;")
                                                                SqlCondition.Append(" INSERT INTO DivisionMasterShippingConfiguration (numDomainID ,numDivisionID ,vcSignatureType) VALUES ( $1,$2, '" + drCFL("vcValue").ToString + "' );")
                                                            Else
                                                                Sql.Append("Update DivisionMaster set ")
                                                                SqlCondition.Append(" where DivisionMaster.numDomainId = $1 AND DivisionMaster.numDivisionId = $2")
                                                            End If
                                                        Else 'Update Custom Fields

                                                            If drFLCheck(0)("vcLookBackTableName") = "CFW_FLD_Values" Then 'Organization

                                                                boolInsert = objWF.WFCustomFieldsInsert(CCommon.ToLong(drCFL("numFieldID")), drCFL("vcValue"))
                                                                If boolInsert Then
                                                                    Sql.Append("Update CFW_FLD_Values set ")
                                                                    SqlCondition.Append(" where CFW_FLD_Values.Fld_ID = " & drCFL("numFieldID") & " AND CFW_FLD_Values.RecId = $2")
                                                                End If
                                                            End If
                                                        End If
                                                    End If
                                                    If Not drCFL("FieldName").ToString = "Signature Type" Then

                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                            tempUpdate.Add(String.Format("{0}='{1}'",
                                                        drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                        Else
                                                            If boolInsert Then
                                                                tempUpdate.Add(String.Format("{0}='{1}'",
                                                                    "Fld_Value", drCFL("vcValue")))
                                                            End If

                                                        End If
                                                    End If
                                                    strFiels = strFiels & "," & drCFL("FieldName")
                                                    SqlFrom.Append(String.Join(",", tempUpdate))
                                                    Sql.Append(SqlFrom)
                                                    Sql.Append(SqlCondition)
                                                    objWF.textQuery = Sql.ToString()
                                                    objWF.WFConditionQueryExecute(2)
                                                    WFDescription = "Fields Updated:" & strFiels
                                                    bitSuccess = True
                                                    If bitSuccess = True And objWF.FormID = 68 And drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then
                                                        'Added By Sachin Sadhu||Date:23ndMay12014
                                                        'Purpose :To Added Organization data in work Flow queue based on created Rules
                                                        '          Using Change tracking
                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                        objWfA.SaveWFOrganizationQueue()
                                                        'end of code
                                                    ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "AdditionalContactsInformation" Then
                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = numContactId
                                                        objWfA.SaveWFContactQueue()
                                                    Else
                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                        objWfA.SaveWFOrganizationQueue()
                                                    End If
                                                    If CCommon.ToBool(drFLCheck(0)("bitCustom")) = True And bitSuccess = True Then ' TRIGGER Custom fields based WF rules
                                                        Dim objWFCF As New Workflow()
                                                        objWFCF.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWFCF.UserCntID = numRecOwner
                                                        objWFCF.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                        objWFCF.FormFieldId = CCommon.ToLong(drCFL("numFieldID"))
                                                        objWFCF.SaveWFCFOrganizationQueue()
                                                    End If
                                                    Sql.Clear()
                                                    SqlFrom.Clear()
                                                    SqlCondition.Clear()
                                                    tempUpdate.Clear()
                                                Next
                                            End If
                                        Case 70 'Opportunities & Orders
                                            If drWFActionUpdateFields.Length > 0 Then
                                                Dim strFiels As String = ""
                                                For Each drCFL As DataRow In drWFActionUpdateFields
                                                    Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))
                                                    Dim boolInsert As Boolean = True
                                                    If drFLCheck.Length > 0 Then
                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                            If drFLCheck(0)("vcLookBackTableName") = "CompanyInfo" Then 'Organizatin
                                                                Sql.Append("Update CompanyInfo c set ")
                                                                SqlCondition.Append(" FROM OpportunityMaster o inner join DivisionMaster D on d.numdivisionID=o.numDivisionId where o.numDomainId = $1 AND o.numOppId=$2 AND c.numCompanyId=d.numCompanyId")
                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization
                                                                Sql.Append("Update DivisionMaster d set ")
                                                                SqlCondition.Append(" FROM CompanyInfo c, OpportunityMaster o where o.numDomainId = $1 AND o.numOppId=$2 AND d.numdivisionID=o.numDivisionId AND c.numCompanyId=d.numCompanyId")

                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "OpportunityBizDocs" Then 'BizDocs
                                                                Sql.Append("Update OpportunityBizDocs b set ")
                                                                'SqlCondition.Append(" FROM OpportunityBizDocs b inner join OpportunityMaster o on o.numOppId=b.numOppId  where  b.numOppBizDocsId=$2")
                                                                SqlCondition.Append(" FROM OpportunityMaster o where  o.numOppId=$2 AND o.numDomainId = $1 AND o.numOppId=b.numOppId")

                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "StagePercentageDetails" Then 'StagePercentageDetails
                                                                Sql.Append("Update StagePercentageDetails b set ")
                                                                'SqlCondition.Append(" FROM OpportunityBizDocs b inner join OpportunityMaster o on o.numOppId=b.numOppId  where  b.numOppBizDocsId=$2")
                                                                SqlCondition.Append(" FROM OpportunityMaster o where  o.numOppId=$2 AND o.numDomainId = $1 AND o.numOppId=b.numOppId")
                                                            Else
                                                                Sql.Append("Update OpportunityMaster set ") 'Orders Module
                                                                SqlCondition.Append(" where OpportunityMaster.numDomainId = $1 AND OpportunityMaster.numOppID = $2")

                                                            End If
                                                        Else
                                                            If drFLCheck(0)("vcLookBackTableName") = "CFW_Fld_Values_Opp" Then 'Orders

                                                                boolInsert = objWF.WFCustomFieldsInsert(CCommon.ToLong(drCFL("numFieldID")), drCFL("vcValue"))
                                                                If boolInsert Then
                                                                    Sql.Append("Update CFW_Fld_Values_Opp set ")
                                                                    SqlCondition.Append(" where CFW_Fld_Values_Opp.Fld_ID = " & drCFL("numFieldID") & " AND CFW_Fld_Values_Opp.RecId = $2")
                                                                End If

                                                            End If
                                                        End If
                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                            tempUpdate.Add(String.Format("{0}='{1}'",
                                                                           drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                        Else
                                                            If boolInsert Then
                                                                tempUpdate.Add(String.Format("{0}='{1}'",
                                                                        "Fld_Value", drCFL("vcValue")))
                                                            End If

                                                        End If

                                                        SqlFrom.Append(String.Join(",", tempUpdate))

                                                        Sql.Append(SqlFrom)
                                                        Sql.Append(SqlCondition)
                                                        objWF.textQuery = Sql.ToString()
                                                        objWF.WFConditionQueryExecute(2)
                                                        strFiels = strFiels & "," & drCFL("FieldName")
                                                        WFDescription = "Fields Updated:" & strFiels
                                                        bitSuccess = True
                                                        Sql.Clear()
                                                        SqlFrom.Clear()
                                                        SqlCondition.Clear()
                                                        tempUpdate.Clear()

                                                        If bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "OpportunityMaster" Then 'Order Module
                                                            'Added By Sachin Sadhu||Date:23ndMay12014
                                                            'Purpose :To Added Order data in work Flow queue based on created Rules
                                                            '          Using Change tracking
                                                            Dim objWfA As New Workflow()
                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                            objWfA.UserCntID = numRecOwner
                                                            objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                            objWfA.SaveWFOrderQueue()
                                                            'end of code
                                                        ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "OpportunityBizDocs" Then 'BizDocs                                                                           
                                                            Dim objWfA As New Workflow()
                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                            objWfA.UserCntID = numRecOwner
                                                            objWfA.RecordID = numOppBizDOCIDPC
                                                            objWfA.SaveWFBizDocQueue()
                                                        ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization                                                                           
                                                            Dim objWfA As New Workflow()
                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                            objWfA.UserCntID = numRecOwner
                                                            objWfA.RecordID = numDivisionId
                                                            objWfA.SaveWFOrganizationQueue()
                                                        ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "StagePercentageDetails" Then 'Busniess Process                                                                           
                                                            Dim objWfA As New Workflow()
                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                            objWfA.UserCntID = numRecOwner
                                                            objWfA.RecordID = numStageIDPC
                                                            objWfA.SaveWFBusinessProcessQueue()
                                                        Else 'orders
                                                            Dim objWfA As New Workflow()
                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                            objWfA.UserCntID = numRecOwner
                                                            objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                            objWfA.SaveWFOrderQueue()

                                                        End If
                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = True And bitSuccess = True Then ' TRIGGER Custom fields based WF rules
                                                            Dim objWFCF As New Workflow()
                                                            objWFCF.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                            objWFCF.UserCntID = numRecOwner
                                                            objWFCF.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                            objWFCF.FormFieldId = CCommon.ToLong(drCFL("numFieldID"))
                                                            objWFCF.SaveWFCFOrdersQueue()
                                                        End If

                                                    End If
                                                Next
                                            End If

                                        Case 49 'BizDocs

                                            If drWFActionUpdateFields.Length > 0 Then
                                                Dim strFiels As String = ""
                                                For Each drCFL As DataRow In drWFActionUpdateFields
                                                    Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))

                                                    If drFLCheck.Length > 0 Then
                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                            If drFLCheck(0)("vcLookBackTableName") = "OpportunityMaster" Then 'Orders
                                                                Sql.Append("Update OpportunityMaster o set ")
                                                                SqlCondition.Append(" FROM OpportunityBizDocs b  where  b.numOppBizDocsId=$2 AND o.numOppId=b.numOppId")
                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "CompanyInfo" Then
                                                                Sql.Append("Update CompanyInfo c set ") 'organization
                                                                SqlCondition.Append(" FROM OpportunityBizDocs b inner join OpportunityMaster o on o.numOppId=b.numOppId inner join DivisionMaster D on d.numdivisionID=o.numDivisionId where b.numOppBizDocsId=$2 AND c.numCompanyId=d.numCompanyId")
                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization
                                                                Sql.Append("Update DivisionMaster d set ")
                                                                SqlCondition.Append("FROM companyinfo c, OpportunityBizDocs b inner join OpportunityMaster o on o.numOppId=b.numOppId where  b.numOppBizDocsId=$2 AND d.numdivisionID=o.numDivisionId AND c.numCompanyId=d.numCompanyId")
                                                            Else 'BizDocs module
                                                                Sql.Append(" Update OpportunityBizDocs set   ")
                                                                SqlCondition.Append(" where OpportunityBizDocs.numOppBizDocsId = $2")

                                                            End If

                                                        End If
                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                            tempUpdate.Add(String.Format("{0}='{1}'",
                                                                           drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                        End If

                                                        SqlFrom.Append(String.Join(",", tempUpdate))

                                                        Sql.Append(SqlFrom)
                                                        Sql.Append(SqlCondition)
                                                        objWF.textQuery = Sql.ToString()
                                                        objWF.WFConditionQueryExecute(2)

                                                        strFiels = strFiels & "," & drCFL("FieldName")
                                                        WFDescription = "Fields Updated:" & strFiels
                                                        bitSuccess = True

                                                        Sql.Clear()
                                                        SqlFrom.Clear()
                                                        SqlCondition.Clear()
                                                        tempUpdate.Clear()
                                                        If bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "OpportunityMaster" Then 'Order Module

                                                            Dim objWfA As New Workflow()
                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                            objWfA.UserCntID = numRecOwner
                                                            objWfA.RecordID = numOppIDPC
                                                            objWfA.SaveWFOrderQueue()
                                                            'end of code
                                                        ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "OpportunityBizDocs" Then 'BizDocs                                                                           
                                                            Dim objWfA As New Workflow()
                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                            objWfA.UserCntID = numRecOwner
                                                            objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                            objWfA.SaveWFBizDocQueue()
                                                        ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization                                                                           
                                                            Dim objWfA As New Workflow()
                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                            objWfA.UserCntID = numRecOwner
                                                            objWfA.RecordID = numDivisionId
                                                            objWfA.SaveWFOrganizationQueue()
                                                        Else 'BizDocs
                                                            Dim objWfA As New Workflow()
                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                            objWfA.UserCntID = numRecOwner
                                                            objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                            objWfA.SaveWFBizDocQueue()
                                                        End If

                                                    End If
                                                Next
                                            End If
                                        Case 94 'Business Process
                                            Sql.Append("Update StagePercentageDetails set ")
                                            SqlCondition.Append(" where StagePercentageDetails.numDomainId = $1 AND StagePercentageDetails.numStageDetailsId = $2")
                                            If drWFActionUpdateFields.Length > 0 Then
                                                For Each drCFL As DataRow In drWFActionUpdateFields
                                                    Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))

                                                    If drFLCheck.Length > 0 Then
                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                            tempUpdate.Add(String.Format("{0}='{1}'",
                                                                           drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                        End If
                                                    End If
                                                Next

                                                SqlFrom.Append(String.Join(",", tempUpdate))

                                                Sql.Append(SqlFrom)
                                                Sql.Append(SqlCondition)
                                                objWF.textQuery = Sql.ToString()
                                                objWF.WFConditionQueryExecute(2)

                                                WFDescription = "Fields Updated"
                                                bitSuccess = True

                                                If bitSuccess = True Then 'Business Process

                                                    Dim objWfA As New Workflow()
                                                    objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                    objWfA.UserCntID = numRecOwner
                                                    objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                    objWfA.SaveWFBusinessProcessQueue()
                                                    'end of code

                                                End If
                                            End If
                                        Case 69 'Contacts                                                           
                                            If drWFActionUpdateFields.Length > 0 Then
                                                Dim strFiels As String = ""
                                                For Each drCFL As DataRow In drWFActionUpdateFields
                                                    Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))
                                                    Dim boolInsert As Boolean = True
                                                    If drFLCheck.Length > 0 Then
                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                            If drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization
                                                                Sql.Append("Update DivisionMaster c set ")
                                                                SqlCondition.Append(" FROM AdditionalContactsInformation o where o.numDomainId = $1 AND o.numContactId=$2 AND c.numdivisionID=o.numDivisionId")
                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "CompanyInfo" Then 'Company Info
                                                                Sql.Append("Update CompanyInfo c set ")
                                                                SqlCondition.Append(" FROM AdditionalContactsInformation o inner join DivisionMaster D on d.numdivisionID=o.numDivisionId where o.numDomainId = $1 AND o.numContactId=$2 AND c.numCompanyId=d.numCompanyId")
                                                            Else
                                                                Sql.Append("Update AdditionalContactsInformation set ")
                                                                SqlCondition.Append(" where AdditionalContactsInformation.numDomainId = $1 AND AdditionalContactsInformation.numContactId = $2")
                                                            End If
                                                        Else
                                                            If drFLCheck(0)("vcLookBackTableName") = "CFW_FLD_Values_Cont" Then 'Contacts

                                                                boolInsert = objWF.WFCustomFieldsInsert(CCommon.ToLong(drCFL("numFieldID")), drCFL("vcValue"))
                                                                If boolInsert Then
                                                                    Sql.Append("Update CFW_FLD_Values_Cont set ")
                                                                    SqlCondition.Append(" where CFW_FLD_Values_Cont.Fld_ID = " & drCFL("numFieldID") & " AND CFW_FLD_Values_Cont.RecId = $2")
                                                                End If

                                                            End If

                                                        End If
                                                    End If
                                                    If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                        tempUpdate.Add(String.Format("{0}='{1}'",
                                                                       drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                    Else
                                                        If boolInsert Then
                                                            tempUpdate.Add(String.Format("{0}='{1}'",
                                                                    "Fld_Value", drCFL("vcValue")))
                                                        End If
                                                    End If
                                                    SqlFrom.Append(String.Join(",", tempUpdate))
                                                    Sql.Append(SqlFrom)
                                                    Sql.Append(SqlCondition)
                                                    objWF.textQuery = Sql.ToString()
                                                    objWF.WFConditionQueryExecute(2)
                                                    strFiels = strFiels & "," & drCFL("FieldName")
                                                    WFDescription = "Fields Updated:" & strFiels
                                                    bitSuccess = True
                                                    Sql.Clear()
                                                    SqlFrom.Clear()
                                                    SqlCondition.Clear()
                                                    tempUpdate.Clear()
                                                    If bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "AdditionalContactsInformation" Then 'Contacts
                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                        objWfA.SaveWFContactQueue()
                                                        'end of code                                                                
                                                    ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'BizDocs                                                                           
                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = numDivisionId
                                                        objWfA.SaveWFOrganizationQueue()
                                                    Else 'Contacts
                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                        objWfA.SaveWFContactQueue()
                                                    End If
                                                    If CCommon.ToBool(drFLCheck(0)("bitCustom")) = True And bitSuccess = True Then ' TRIGGER Custom fields based WF rules
                                                        Dim objWFCF As New Workflow()
                                                        objWFCF.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWFCF.UserCntID = numRecOwner
                                                        objWFCF.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                        objWFCF.FormFieldId = CCommon.ToLong(drCFL("numFieldID"))
                                                        objWFCF.SaveWFCFContactsQueue()
                                                    End If
                                                Next
                                            End If

                                        Case 73 'Projects

                                            If drWFActionUpdateFields.Length > 0 Then
                                                Dim strFiels As String = ""
                                                For Each drCFL As DataRow In drWFActionUpdateFields
                                                    Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))
                                                    Dim boolInsert As Boolean = True
                                                    If drFLCheck.Length > 0 Then
                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                            If drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization
                                                                Sql.Append("Update DivisionMaster c set ")
                                                                SqlCondition.Append(" FROM ProjectsMaster o where o.numDomainId = $1 AND o.numProId=$2 AND c.numdivisionID=o.numDivisionId")
                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "CompanyInfo" Then 'Company Info
                                                                Sql.Append("Update CompanyInfo c set ")
                                                                SqlCondition.Append(" FROM ProjectsMaster o inner join DivisionMaster D on d.numdivisionID=o.numDivisionId where  o.numDomainId = $1 AND o.numProId=$2 AND c.numCompanyId=d.numCompanyId")
                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "StagePercentageDetails" Then 'StagePercentageDetails
                                                                Sql.Append("Update StagePercentageDetails b set ")
                                                                'SqlCondition.Append(" FROM OpportunityBizDocs b inner join OpportunityMaster o on o.numOppId=b.numOppId  where  b.numOppBizDocsId=$2")
                                                                SqlCondition.Append(" FROM ProjectsMaster o where  o.numDomainId = $1 AND o.numProId=$2 AND o.numProId=b.numProjectID")
                                                            Else
                                                                Sql.Append("Update ProjectsMaster set ")
                                                                SqlCondition.Append(" where ProjectsMaster.numDomainId = $1 AND ProjectsMaster.numProId = $2")
                                                            End If
                                                        Else
                                                            If drFLCheck(0)("vcLookBackTableName") = "CFW_FLD_Values_Pro" Then 'Projects

                                                                boolInsert = objWF.WFCustomFieldsInsert(CCommon.ToLong(drCFL("numFieldID")), drCFL("vcValue"))
                                                                If boolInsert Then
                                                                    Sql.Append("Update CFW_FLD_Values_Pro set ")
                                                                    SqlCondition.Append(" where CFW_FLD_Values_Pro.Fld_ID = " & drCFL("numFieldID") & " AND CFW_FLD_Values_Pro.RecId = $2")
                                                                End If
                                                            End If
                                                        End If
                                                    End If
                                                    If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                        tempUpdate.Add(String.Format("{0}='{1}'",
                                                                       drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                    Else
                                                        If boolInsert Then
                                                            tempUpdate.Add(String.Format("{0}='{1}'",
                                                                    "Fld_Value", drCFL("vcValue")))
                                                        End If
                                                    End If
                                                    SqlFrom.Append(String.Join(",", tempUpdate))
                                                    Sql.Append(SqlFrom)
                                                    Sql.Append(SqlCondition)
                                                    objWF.textQuery = Sql.ToString()
                                                    objWF.WFConditionQueryExecute(2)
                                                    strFiels = strFiels & "," & drCFL("FieldName")
                                                    WFDescription = "Fields Updated:" & strFiels
                                                    bitSuccess = True
                                                    Sql.Clear()
                                                    SqlFrom.Clear()
                                                    SqlCondition.Clear()
                                                    tempUpdate.Clear()

                                                    If bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "ProjectsMaster" Then 'Order Module

                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                        objWfA.SaveWFProjectsQueue()
                                                        'end of code

                                                    ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization                                                                           
                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = numDivisionId
                                                        objWfA.SaveWFOrganizationQueue()
                                                    ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "StagePercentageDetails" Then 'Busniess Process                                                                           
                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = numStageIDPC
                                                        objWfA.SaveWFBusinessProcessQueue()
                                                    Else 'Projects
                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                        objWfA.SaveWFProjectsQueue()

                                                    End If

                                                    If CCommon.ToBool(drFLCheck(0)("bitCustom")) = True And bitSuccess = True Then ' TRIGGER Custom fields based WF rules
                                                        Dim objWFCF As New Workflow()
                                                        objWFCF.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWFCF.UserCntID = numRecOwner
                                                        objWFCF.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                        objWFCF.FormFieldId = CCommon.ToLong(drCFL("numFieldID"))
                                                        objWFCF.SaveWFCFProjectsQueue()
                                                    End If
                                                Next
                                            End If

                                        Case 72 'Cases

                                            If drWFActionUpdateFields.Length > 0 Then
                                                Dim strFiels As String = ""
                                                For Each drCFL As DataRow In drWFActionUpdateFields
                                                    Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))
                                                    Dim boolInsert As Boolean = True
                                                    If drFLCheck.Length > 0 Then
                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                            If drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization
                                                                Sql.Append("Update DivisionMaster c set ")
                                                                SqlCondition.Append(" FROM Cases o  where o.numDomainId = $1 AND o.numCaseId=$2 AND c.numdivisionID=o.numDivisionId")
                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "CompanyInfo" Then 'Company Info
                                                                Sql.Append("Update CompanyInfo c set ")
                                                                SqlCondition.Append(" FROM Cases o inner join DivisionMaster D on d.numdivisionID=o.numDivisionId where o.numDomainId = $1 AND o.numCaseId=$2 AND c.numCompanyId=d.numCompanyId")
                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "OpportunityMaster" Then 'Orders
                                                                Sql.Append("Update OpportunityMaster o set ")
                                                                SqlCondition.Append(" FROM Cases c inner join CaseOpportunities cp on c.numCaseId=cp.numCaseId where c.numDomainId = $1 AND c.numCaseId = $2 AND o.numOppId=cp.numOppId")
                                                            Else
                                                                Sql.Append("Update Cases set ")
                                                                SqlCondition.Append(" where Cases.numDomainId = $1 AND Cases.numCaseId = $2")
                                                            End If
                                                        Else
                                                            If drFLCheck(0)("vcLookBackTableName") = "CFW_FLD_Values_Case" Then 'Cases

                                                                boolInsert = objWF.WFCustomFieldsInsert(CCommon.ToLong(drCFL("numFieldID")), drCFL("vcValue"))
                                                                If boolInsert Then
                                                                    Sql.Append("Update CFW_FLD_Values_Case set ")
                                                                    SqlCondition.Append(" where CFW_FLD_Values_Case.Fld_ID = " & drCFL("numFieldID") & " AND CFW_FLD_Values_Case.RecId = $2")
                                                                End If
                                                            End If
                                                        End If
                                                    End If
                                                    If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                        tempUpdate.Add(String.Format("{0}='{1}'",
                                                                       drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                    Else
                                                        If boolInsert Then
                                                            tempUpdate.Add(String.Format("{0}='{1}'",
                                                                    "Fld_Value", drCFL("vcValue")))
                                                        End If
                                                    End If
                                                    SqlFrom.Append(String.Join(",", tempUpdate))
                                                    Sql.Append(SqlFrom)
                                                    Sql.Append(SqlCondition)
                                                    objWF.textQuery = Sql.ToString()
                                                    objWF.WFConditionQueryExecute(2)
                                                    strFiels = strFiels & "," & drCFL("FieldName")
                                                    WFDescription = "Fields Updated:" & strFiels
                                                    bitSuccess = True
                                                    Sql.Clear()
                                                    SqlFrom.Clear()
                                                    SqlCondition.Clear()
                                                    tempUpdate.Clear()

                                                    If bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "OpportunityMaster" Then 'Order Module

                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = numOppIDPC
                                                        objWfA.SaveWFOrderQueue()
                                                        'end of code
                                                    ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "Cases" Then 'Cases                                                                           
                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                        objWfA.SaveWFCasesQueue()
                                                    ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization                                                                           
                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = numDivisionId
                                                        objWfA.SaveWFOrganizationQueue()

                                                    Else 'Cases
                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                        objWfA.SaveWFCasesQueue()

                                                    End If
                                                    If CCommon.ToBool(drFLCheck(0)("bitCustom")) = True And bitSuccess = True Then ' TRIGGER Custom fields based WF rules
                                                        Dim objWFCF As New Workflow()
                                                        objWFCF.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWFCF.UserCntID = numRecOwner
                                                        objWFCF.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                        objWFCF.FormFieldId = CCommon.ToLong(drCFL("numFieldID"))
                                                        objWFCF.SaveWFCFCasesQueue()
                                                    End If
                                                Next
                                            End If

                                            'Sql.Append("Update Cases set ")
                                            'SqlCondition.Append(" where Cases.numDomainId = $1 AND Cases.numCaseId = $2")
                                            'If drWFActionUpdateFields.Length > 0 Then
                                            '    For Each drCFL As DataRow In drWFActionUpdateFields
                                            '        Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))

                                            '        If drFLCheck.Length > 0 Then
                                            '            If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                            '                tempUpdate.Add(String.Format("{0}='{1}'", _
                                            '                                       drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                            '            End If
                                            '        End If
                                            '    Next

                                            '    SqlFrom.Append(String.Join(",", tempUpdate))

                                            '    Sql.Append(SqlFrom)
                                            '    Sql.Append(SqlCondition)
                                            '    objWF.textQuery = Sql.ToString()
                                            '    objWF.WFConditionQueryExecute(2)

                                            '    WFDescription = "Fields Updated"
                                            '    bitSuccess = True

                                            'End If
                                        Case 124 'Actiom Items(Tickler)

                                            If drWFActionUpdateFields.Length > 0 Then
                                                Dim strFiels As String = ""
                                                For Each drCFL As DataRow In drWFActionUpdateFields
                                                    Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))

                                                    If drFLCheck.Length > 0 Then
                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                            If drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization
                                                                Sql.Append("Update DivisionMaster c set ")
                                                                SqlCondition.Append(" FROM Communication o where o.numDomainId = $1 AND o.numCommId=$2 AND c.numdivisionID=o.numDivisionId")
                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "CompanyInfo" Then 'Company Info
                                                                Sql.Append("Update CompanyInfo c set ")
                                                                SqlCondition.Append(" FROM Communication o inner join DivisionMaster D on d.numdivisionID=o.numDivisionId where o.numDomainId = $1 AND o.numCommId=$2 AND c.numCompanyId=d.numCompanyId")
                                                            Else
                                                                Sql.Append("Update Communication set ")
                                                                SqlCondition.Append(" where Communication.numDomainId = $1 AND Communication.numCommId = $2")
                                                            End If

                                                        End If
                                                    End If
                                                    If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                        tempUpdate.Add(String.Format("{0}='{1}'",
                                                                       drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                    End If
                                                    SqlFrom.Append(String.Join(",", tempUpdate))
                                                    Sql.Append(SqlFrom)
                                                    Sql.Append(SqlCondition)
                                                    objWF.textQuery = Sql.ToString()
                                                    objWF.WFConditionQueryExecute(2)
                                                    strFiels = strFiels & "," & drCFL("FieldName")
                                                    WFDescription = "Fields Updated:" & strFiels
                                                    bitSuccess = True
                                                    Sql.Clear()
                                                    SqlFrom.Clear()
                                                    SqlCondition.Clear()
                                                    tempUpdate.Clear()

                                                    If bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "Communication" Then 'Order Module
                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                        objWfA.SaveWFActionItemsQueue()
                                                        'end of code

                                                    ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization                                                                           
                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = numDivisionId
                                                        objWfA.SaveWFOrganizationQueue()

                                                    Else 'Tickler
                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                        objWfA.SaveWFActionItemsQueue()

                                                    End If
                                                Next
                                            End If
                                    End Select
                                Case CCommon.ToShort(enmWFAction.DisplayAlertMessage)

                                    ' Dim cph As ContentPlaceHolder = DirectCast(Me.Master.FindControl("TabsPlaceHolder"), ContentPlaceHolder)
                                    objWF.vcAlertMessage = CCommon.ToString(drAct("vcAlertMessage").ToString)
                                    objWF.WFID = CCommon.ToLong(dr("numWFID"))
                                    objWF.intAlertStatus = 1
                                    objWF.SaveWorkFlowAlerts()
                                    WFDescription = "Display Alert:Alert is shown"
                                    bitSuccess = True
                                Case CCommon.ToShort(enmWFAction.BizDocApproval)
                                    Try

                                        Dim objDocuments As New DocumentList
                                        Dim strReceiverName As String() = drAct("vcApproval").Split(",")
                                        Dim strApprovalName As String = ""
                                        Dim k As Integer = 0
                                        For k = 0 To strReceiverName.Length - 1

                                            objDocuments.GenDocID = CCommon.ToLong(dr("numRecordID"))
                                            objDocuments.ContactID = CCommon.ToLong(strReceiverName(k))
                                            objDocuments.CDocType = "B"
                                            objDocuments.byteMode = 1
                                            objDocuments.DomainID = CCommon.ToLong(dr("numDomainID"))
                                            objDocuments.UserCntID = numRecOwner
                                            objDocuments.ManageApprovers()

                                            Dim objOppBizDocs As New OppBizDocs
                                            objOppBizDocs.OppBizDocId = CCommon.ToLong(dr("numRecordID"))
                                            objOppBizDocs.DomainID = CCommon.ToLong(dr("numDomainID"))
                                            Dim dsOppData As DataSet = objOppBizDocs.GetBizDocsDetails

                                            'Send Approval requeset via Mail
                                            Dim ObjCmn As New CCommon
                                            Dim objSendMail As New Email
                                            Dim dtDocDetails As DataTable = objSendMail.GetEmailTemplateByCode("#SYS#DOC_APPROVAL_REQUEST", CCommon.ToLong(dr("numDomainID")))
                                            Dim objUserAccess As New UserAccess
                                            objUserAccess.ContactID = numRecOwner
                                            objUserAccess.DomainID = CCommon.ToLong(dr("numDomainID"))
                                            Dim dtUser As DataTable = objUserAccess.GetExtranetUserDetails()
                                            Dim strPortalUrl As String = "http://portal.bizautomation.com/Login.aspx"
                                            Dim strPortalApprovalLink As String
                                            If dtUser.Rows.Count = 1 Then

                                                strPortalApprovalLink = CCommon.ToString(strPortalUrl) & QueryEncryption.EncryptQueryString("&from=email&u=" + CCommon.ToString(dtUser.Rows(0)("vcEmail")) + "&p=" + ObjCmn.Encrypt(CCommon.ToString(dtUser.Rows(0)("vcPassword"))))
                                                strPortalApprovalLink = "<a href=" & strPortalApprovalLink & " target='_blank' >click here</a>"

                                            End If

                                            If dtDocDetails.Rows.Count > 0 Then
                                                Dim strBody As String = dtDocDetails.Rows(0).Item("vcDocdesc")
                                                Dim strSubject As String = IIf(IsDBNull(dtDocDetails.Rows(0).Item("vcSubject")), "", dtDocDetails.Rows(0).Item("vcSubject"))

                                                Dim objEmail As New Email
                                                objEmail.AdditionalMergeFields.Add("DocumentName", dsOppData.Tables(0).Rows(0).Item("vcBizDocID"))
                                                objEmail.AdditionalMergeFields.Add("LoggedInUser", dtRecordData.Rows(0)("ContactName"))
                                                objEmail.AdditionalMergeFields.Add("PortalDirectLoginLink", strPortalApprovalLink)
                                                objEmail.AdditionalMergeFields.Add("Signature", dtRecordData.Rows(0)("Signature"))
                                                If strBody.ToLower().Contains("##ContactOpt-OutLink##".ToLower()) Then
                                                    objEmail.AdditionalMergeFields.Add("ContactOpt-OutLink", "")
                                                End If

                                                Dim dtEmailBroadCast As New DataTable

                                                Dim EmailToType() As String = drAct("vcApproval").ToString().Split(",")
                                                ' Dim strContactIDs As New ArrayList

                                                'If EmailToType.Contains(1) Then 'Owner of trigger record
                                                '    strContactIDs.Add(numRecOwner)
                                                'End If

                                                'If EmailToType.Contains(2) Then 'Assignee of trigger record
                                                '    strContactIDs.Add(numAssignedTo)
                                                'End If

                                                'If EmailToType.Contains(3) Then 'Primary contact of trigger record
                                                '    strContactIDs.Add(numContactId)
                                                'End If


                                                ' If strContactIDs.Count > 0 Then
                                                dtEmailBroadCast = objEmail.GetEmailMergeData(1, String.Join(",", EmailToType.ToArray()), CCommon.ToLong(dr("numDomainID")), 0, 0)

                                                Dim strTo As New List(Of String)
                                                For Each drEmail As DataRow In dtEmailBroadCast.Rows
                                                    strTo.Add(drEmail("ContactEmail"))
                                                Next

                                                If dtEmailBroadCast.Rows.Count > 0 Then
                                                    strApprovalName = dtEmailBroadCast.Rows(0)("ContactFirstName").ToString
                                                End If

                                                If strTo.Count > 0 Then
                                                    For Each iKey As Object In objEmail.AdditionalMergeFields.Keys
                                                        If dtEmailBroadCast.Columns.Contains(iKey.ToString) = False Then
                                                            dtEmailBroadCast.Columns.Add(iKey.ToString)
                                                        End If
                                                        'Assign values to rows in merge table
                                                        For Each row As DataRow In dtEmailBroadCast.Rows
                                                            If iKey.ToString <> "ContactEmail" Then
                                                                ' row(iKey.ToString) = CCommon.ToString(iKey)
                                                                row(iKey.ToString) = CCommon.ToString(objEmail.AdditionalMergeFields(iKey))
                                                            End If
                                                        Next
                                                    Next
                                                    objEmail.SendEmail(strSubject, strBody, "", dtRecordData.Rows(0).Item("vcEmailID"), String.Join(",", strTo), dtEmailBroadCast, FromDisplayName:=dtRecordData.Rows(0)("ContactName"), DomainID:=objWF.DomainID)

                                                End If
                                                ' End If
                                            End If
                                            'end of Request
                                        Next

                                        WFDescription = "BizDoc Approval Request sent to Tickler of " & strApprovalName
                                        bitSuccess = True

                                    Catch ex As Exception
                                        If ex.Message = "No_EXTRANET" Then
                                            WFDescription = "Can not add selected contact to approver list. Your option is to add contact as external users from ""Administrator->User Administration->External Users"" and try again."
                                            bitSuccess = False
                                            Exit Sub
                                        Else
                                            Throw ex
                                        End If
                                    End Try
                                Case CCommon.ToShort(enmWFAction.CreateBizDoc)
                                    Dim dtCheckBizDocs As New DataTable()
                                    dtCheckBizDocs = CheckDuplicateBizDocs(CCommon.ToLong(dr("numRecordID")), CCommon.ToLong(dr("numDomainID")), CCommon.ToShort(drAct("numOppType")), numRecOwner, CCommon.ToLong(drAct("numBizDocTypeID")), CCommon.ToLong(drAct("numBizDocTemplateID")))

                                    If dtCheckBizDocs.Rows.Count > 0 Then
                                        'WFDescription = CCommon.ToString(drAct("vcBizDoc")) & "-This BizDoc  already exists for this order "
                                        WFDescription = "This BizDoc-" & CCommon.ToString(drAct("vcBizDoc")) & "  already exists for this order "
                                        bitSuccess = True
                                    Else
                                        Dim objOppBizDoc As New OppBizDocs
                                        objOppBizDoc.OppId = CCommon.ToLong(dr("numRecordID"))
                                        objOppBizDoc.BizDocId = CCommon.ToLong(drAct("numBizDocTypeID"))
                                        If CCommon.ToBool(objOppBizDoc.CheckIfIemsPendingToAddByBizDocID()) Then
                                            CreateBizDoc(CCommon.ToLong(dr("numRecordID")), CCommon.ToLong(dr("numDomainID")), CCommon.ToShort(drAct("numOppType")), numRecOwner, CCommon.ToLong(drAct("numBizDocTypeID")), CCommon.ToLong(drAct("numBizDocTemplateID")))
                                            WFDescription = "BizDoc-" & CCommon.ToString(drAct("vcBizDoc")) & " has been created"

                                            bitSuccess = True
                                        Else
                                            WFDescription = "Not items left to be added to bizdoc."
                                            bitSuccess = True
                                        End If

                                    End If

                                Case CCommon.ToShort(enmWFAction.PromoteOrganization)
                                    Dim objLeads As New LeadsIP
                                    objLeads.DivisionID = CCommon.ToLong(dr("numRecordID"))
                                    objLeads.DomainID = CCommon.ToLong(dr("numDomainID"))
                                    Dim dtCrmType As DataTable = objLeads.GetCRMType()

                                    Dim tintCRMType As Integer = CCommon.ToInteger(dtCrmType.Rows(0)("tintCRMType").ToString)

                                    If tintCRMType = 0 Then ' Leads

                                        objLeads.UserCntID = numRecOwner
                                        objLeads.ContactID = numContactId
                                        objLeads.DivisionID = CCommon.ToLong(dr("numRecordID"))
                                        objLeads.PromoteLead()
                                        WFDescription = CCommon.ToString(dr("numRecordID")) & "-Organization Promoted"
                                        bitSuccess = True
                                    ElseIf tintCRMType = 1 Then 'Prospects
                                        objLeads.DivisionID = CCommon.ToLong(dr("numRecordID"))
                                        objLeads.byteMode = 2
                                        objLeads.DomainID = CCommon.ToLong(dr("numDomainID"))
                                        objLeads.UserCntID = numRecOwner
                                        objLeads.DemoteOrg()
                                        WFDescription = CCommon.ToString(dr("numRecordID")) & "-Organization Promoted"
                                        bitSuccess = True
                                    Else 'Account
                                        WFDescription = CCommon.ToString(dr("numRecordID")) & "-This Organization is already in Account ,No scope for Promotion"
                                        bitSuccess = True
                                    End If
                                Case CCommon.ToShort(enmWFAction.DemoteOrganization)
                                    Dim objLeads As New LeadsIP
                                    objLeads.DivisionID = CCommon.ToLong(dr("numRecordID"))
                                    objLeads.DomainID = CCommon.ToLong(dr("numDomainID"))
                                    Dim dtCrmType As DataTable = objLeads.GetCRMType()

                                    Dim tintCRMType As Integer = CCommon.ToInteger(dtCrmType.Rows(0)("tintCRMType").ToString)

                                    If tintCRMType = 1 Then 'Prospect
                                        objLeads.DivisionID = CCommon.ToLong(dr("numRecordID"))
                                        objLeads.byteMode = 0
                                        objLeads.DomainID = CCommon.ToLong(dr("numDomainID"))
                                        objLeads.UserCntID = numRecOwner
                                        objLeads.DemoteOrg()
                                        WFDescription = CCommon.ToString(dr("numRecordID")) & "-Organization demoted"
                                        bitSuccess = True
                                    ElseIf tintCRMType = 2 Then 'Account
                                        Dim objAccounts As New CAccounts
                                        Dim intCount As Integer
                                        objAccounts.DivisionID = CCommon.ToLong(dr("numRecordID"))
                                        intCount = objAccounts.CheckDealsWon
                                        If intCount = 0 Then
                                            objLeads.DivisionID = CCommon.ToLong(dr("numRecordID"))
                                            objLeads.byteMode = 1
                                            objLeads.DomainID = CCommon.ToLong(dr("numDomainID"))
                                            objLeads.UserCntID = numRecOwner
                                            objLeads.DemoteOrg()
                                        Else
                                            WFDescription = "This record can not be demoted because it contains completed deals. To demote this record, delete all deals from the Deal History section, then try demoting again."
                                            bitSuccess = True

                                        End If
                                    Else
                                        WFDescription = "This record can not be demoted because It's on Lead stage"
                                        bitSuccess = True

                                    End If

                                Case CCommon.ToShort(enmWFAction.SMSAlerts)
                                    Dim objEmail As New Email
                                    Dim SMSToType() As String = drAct("vcEmailToType").ToString().Split(",")
                                    Dim strContactIDs As New ArrayList

                                    If SMSToType.Contains(1) Then 'Owner of trigger record
                                        strContactIDs.Add(numRecOwner)
                                    End If

                                    If SMSToType.Contains(2) Then 'Assignee of trigger record
                                        strContactIDs.Add(numAssignedTo)
                                    End If

                                    If SMSToType.Contains(3) Then 'Primary contact of trigger record
                                        strContactIDs.Add(numContactId)
                                    End If
                                    If SMSToType.Contains(4) Then 'Internal Project Manager
                                        strContactIDs.Add(numInternalPM)
                                    End If
                                    If SMSToType.Contains(5) Then 'External Project Manager
                                        strContactIDs.Add(numExternalPM)
                                    End If
                                    If SMSToType.Contains(6) Then 'Next Stage Assigned to
                                        strContactIDs.Add(numNextAssignedTo)
                                    End If
                                    If SMSToType.Contains(7) Then 'Assignee Of Trigger Record(Project)
                                        strContactIDs.Add(numNextAssignedTo)
                                    End If
                                    Dim dtSMSBroadCast As New DataTable
                                    Dim ACCOUNT_SID As String = "ACf3bc46e894a584aa43c6e934eb099db1"
                                    Dim AUTH_TOKEN As String = "5a190ab5b2830b4dea058dbcc4c0590f"

                                    Dim client As New TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN)

                                    If strContactIDs.Count > 0 Then
                                        dtSMSBroadCast = objEmail.GetEmailMergeData(1, String.Join(",", strContactIDs.ToArray()), CCommon.ToLong(dr("numDomainID")), 0, 0)
                                        Dim strTo As New List(Of String)
                                        For Each drEmail As DataRow In dtSMSBroadCast.Rows
                                            strTo.Add(drEmail("ContactPhone"))
                                            Dim message = client.SendSmsMessage("+12545312324", drEmail("ContactPhone").ToString(), CCommon.ToString(drAct("vcSMSText").ToString))
                                            If message.RestException IsNot Nothing Then                                                        ' handle the error ...
                                                WFDescription = " Error:" & message.RestException.Message.ToString
                                                bitSuccess = False
                                            Else
                                                WFDescription = "Message Has been Sent to" & drEmail("ContactPhone").ToString()
                                                bitSuccess = True
                                            End If
                                        Next
                                    End If

                            End Select

                        Catch ex As Exception
                            WFDescription = "Error occured:" & ex.Message
                            bitSuccess = False
                        End Try

                        Dim objWF1 As New Workflow
                        With objWF1
                            .WFQueueID = CCommon.ToLong(dr("numWFQueueID"))
                            .WFID = objWF.WFID
                            .WFDescription = WFDescription
                            .ManageWorkFlowQueue(2, _bitSuccess:=bitSuccess)
                        End With
                    Next

                    objWF = New Workflow
                    With objWF
                        .WFQueueID = CCommon.ToLong(dr("numWFQueueID"))
                        .DomainID = CCommon.ToLong(dr("numDomainID"))
                        .tintProcessStatus = CCommon.ToShort(enmWFQueueProcessStatus.Success)
                        .ManageWorkFlowQueue(1)
                    End With
                Else
                    objWF = New Workflow
                    With objWF
                        .WFQueueID = CCommon.ToLong(dr("numWFQueueID"))
                        .DomainID = CCommon.ToLong(dr("numDomainID"))
                        .tintProcessStatus = CCommon.ToShort(enmWFQueueProcessStatus.ConditionNotMatch)
                        .ManageWorkFlowQueue(1)
                    End With
                End If
            Else 'Time based action

                'Dim dtTable As DataTable = GetTimeBasedActionStatus(objWF.RecordID, strTablename, objWF.vcDateField, objWF.intDays, objWF.intActionOn, CCommon.ToInteger(objWF.boolCustom), objWF.numFieldID)

                ' Dim intResult As Integer = CCommon.ToInteger(dtTable.Rows(0)("TimeMatchStatus"))
                If boolCondition Then 'If condition matched then check for date
                    'If intResult = 1 Then 'matching condition

                    Dim numContactId, numDivisionId, numRecOwner, numAssignedTo, numInternalPM, numExternalPM, numNextAssignedTo, numOppIDPC, numStageIDPC, numOppBizDOCIDPC As Long
                    Dim numPhone As String
                    Dim dtRecordData As DataTable = objWF.GetWFRecordData

                    numContactId = CCommon.ToLong(dtRecordData.Rows(0)("numContactId"))
                    numDivisionId = CCommon.ToLong(dtRecordData.Rows(0)("numDivisionId"))
                    numRecOwner = CCommon.ToLong(dtRecordData.Rows(0)("numRecOwner"))
                    numAssignedTo = CCommon.ToLong(dtRecordData.Rows(0)("numAssignedTo"))
                    numInternalPM = CCommon.ToLong(dtRecordData.Rows(0)("numInternalPM"))
                    numExternalPM = CCommon.ToLong(dtRecordData.Rows(0)("numExternalPM"))
                    numNextAssignedTo = CCommon.ToLong(dtRecordData.Rows(0)("numNextAssignedTo"))
                    numPhone = CCommon.ToString(dtRecordData.Rows(0)("numPhone"))
                    numOppIDPC = CCommon.ToLong(dtRecordData.Rows(0)("numOppIDPC"))
                    numStageIDPC = CCommon.ToLong(dtRecordData.Rows(0)("numStageIDPC"))
                    numOppBizDOCIDPC = CCommon.ToLong(dtRecordData.Rows(0)("numOppBizDOCIDPC"))

                    Dim bitSuccess As Boolean = False
                    For Each drAct As DataRow In dtWFActions.Rows
                        Dim WFDescription As String = ""

                        Try
                            Select Case CCommon.ToShort(drAct("tintActionType"))

                                Case CCommon.ToShort(enmWFAction.SendAlerts) 'Send Email
                                    Dim dtDocDetails As DataTable
                                    Dim objDocuments As New DocumentList
                                    With objDocuments
                                        .GenDocID = drAct("numTemplateID")
                                        .DomainID = CCommon.ToLong(dr("numDomainID"))
                                        dtDocDetails = .GetDocByGenDocID
                                    End With

                                    If dtDocDetails.Rows.Count > 0 Or CCommon.ToLong(drAct("numTemplateID")) = "99999" Then
                                        Dim strBody As String = Nothing
                                        Dim strSubject As String = Nothing

                                        If CCommon.ToLong(drAct("numTemplateID")) = "99999" Then 'Compose Message
                                            strBody = drAct("vcMailBody")
                                            strSubject = drAct("vcMailSubject")
                                        Else
                                            strBody = dtDocDetails.Rows(0).Item("vcDocdesc")
                                            strSubject = IIf(IsDBNull(dtDocDetails.Rows(0).Item("vcSubject")), "", dtDocDetails.Rows(0).Item("vcSubject"))

                                        End If
                                        Dim objEmail As New Email

                                        If strBody.ToLower().Contains("##ContactOpt-OutLink##".ToLower()) Then
                                            objEmail.AdditionalMergeFields.Add("ContactOpt-OutLink", "")
                                        End If

                                        Dim dtEmailBroadCast As New DataTable
                                        Dim dtEmailBroadCastSendTo As New DataTable
                                        Dim lngUserCntID As Long = 0
                                        Dim tintEmailFrom As Short = CCommon.ToShort(drAct("tintEmailFrom"))

                                        If tintEmailFrom = 1 Then
                                            lngUserCntID = numRecOwner
                                        ElseIf tintEmailFrom = 2 Then
                                            lngUserCntID = numAssignedTo
                                        End If

                                        Dim EmailToType() As String = drAct("vcEmailToType").ToString().Split(",")
                                        Dim strContactIDs As New ArrayList
                                        Dim strContactIDWebLead As New ArrayList
                                        If EmailToType.Contains(1) Then 'Owner of trigger record
                                            strContactIDs.Add(numRecOwner)
                                        End If

                                        If EmailToType.Contains(2) Then 'Assignee of trigger record
                                            strContactIDs.Add(numAssignedTo)
                                        End If

                                        If EmailToType.Contains(3) Then 'Primary contact of trigger record
                                            strContactIDs.Add(numContactId)
                                        End If
                                        If EmailToType.Contains(4) Then 'Internal Project Manager
                                            strContactIDs.Add(numInternalPM)
                                        End If
                                        If EmailToType.Contains(5) Then 'External Project Manager
                                            strContactIDs.Add(numExternalPM)
                                        End If
                                        If EmailToType.Contains(6) Then 'Next Stage Assigned to
                                            strContactIDs.Add(numNextAssignedTo)
                                        End If
                                        If EmailToType.Contains(7) Then 'Assignee Of Trigger Record(Project)
                                            strContactIDs.Add(numNextAssignedTo)
                                        End If
                                        '  Bizdoc Attachment
                                        Dim dtBizDocAttachment As DataTable
                                        If CCommon.ToShort(drAct("tintIsAttachment")) = 1 Then

                                            Dim objOppBizDocs As New OppBizDocs
                                            objOppBizDocs.OppBizDocId = CCommon.ToLong(dr("numRecordID"))
                                            objOppBizDocs.DomainID = CCommon.ToLong(dr("numDomainID"))
                                            Dim dsOppData As DataSet = objOppBizDocs.GetBizDocsDetails
                                            Dim objCommon1 As New CCommon
                                            Dim dsDateFormat As DataSet = objCommon1.GetDomainDateFormat(CCommon.ToLong(dr("numDomainID")))
                                            Dim htmlCodeToConvert As String = GenerateBizDocPDF(CCommon.ToLong(dr("numRecordID")), CCommon.ToLong(dsOppData.Tables(0).Rows(0)("numOppId")), CCommon.ToLong(dr("numDomainID")), CCommon.ToLong(dtRecordData.Rows(0)("numRecOwner")), 0, dsDateFormat.Tables(0).Rows(0)("vcDateFormat").ToString())
                                            'Dim htmlCodeToConvert As String = GenerateBizDocPDF(CCommon.ToLong(dr("numRecordID")), CCommon.ToLong(dsOppData.Tables(0).Rows(0)("numOppId")), CCommon.ToLong(dr("numDomainID")), CCommon.ToLong(dtRecordData.Rows(0)("numRecOwner")), 0, FormattedDateFromDate(dsOppData.Tables(0).Rows(0)("dtFromDate").ToString, dsDateFormat.Tables(0).Rows(0)("vcDateFormat").ToString()))
                                            ' Dim htmlCodeToConvert As String = getBizDocDetails(CCommon.ToLong(dr("numDomainID")), CCommon.ToLong(dr("numRecordID")), CCommon.ToLong(dsOppData.Tables(0).Rows(0)("numOppId")), CCommon.ToLong(dsOppData.Tables(0).Rows(0)("numBizDocId")))
                                            Dim i As Integer
                                            Dim strFileName As String = ""
                                            Dim strFilePhysicalLocation As String = ""
                                            Dim objHTMLToPDF As New HTMLToPDF

                                            strFileName = objHTMLToPDF.ConvertHTML2PDF(htmlCodeToConvert, CCommon.ToLong(dr("numDomainID")))
                                            strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(CCommon.ToLong(dr("numDomainID"))) & strFileName
                                            Dim strBizDocName As String = CCommon.ToString(dsOppData.Tables(0).Rows(0).Item("vcBizDocID"))

                                            Dim objCommon As New CCommon
                                            dtBizDocAttachment = New DataTable
                                            dtBizDocAttachment.Columns.Add("Filename")
                                            dtBizDocAttachment.Columns.Add("FileLocation")
                                            dtBizDocAttachment.Columns.Add("FilePhysicalPath")
                                            dtBizDocAttachment.Columns.Add("FileSize", GetType(Long))
                                            dtBizDocAttachment.Columns.Add("FileType")

                                            Dim dr1 As DataRow = dtBizDocAttachment.NewRow
                                            dr1("Filename") = strFileName
                                            dr1("FileLocation") = CCommon.GetDocumentPhysicalPath(CCommon.ToLong(dr("numDomainID"))) & strFileName
                                            dr1("FilePhysicalPath") = strFilePhysicalLocation
                                            dr1("FileType") = ".pdf"
                                            dr1("FileSize") = 15650
                                            dtBizDocAttachment.Rows.Add(dr1)

                                        End If
                                        'end of code for bizdoc attachment

                                        If strContactIDs.Count > 0 OrElse (CCommon.ToString(drAct("vcEmailSendTo")) IsNot Nothing) Then

                                            strContactIDWebLead.Add(numContactId)

                                            'dtEmailBroadCast = objEmail.GetEmailMergeData(1, String.Join(",", strContactIDWebLead.ToArray()), CCommon.ToLong(dr("numDomainID")), 0, 0)

                                            'dtEmailBroadCastSendTo = objEmail.GetEmailMergeData(1, String.Join(",", strContactIDs.ToArray()), CCommon.ToLong(dr("numDomainID")), 0, 0)

                                            If (moduleId = 1 Or moduleId = 45) Then
                                                dtEmailBroadCast = objEmail.GetEmailMergeData(moduleId, numContactId, CCommon.ToLong(dr("numDomainID")), 0, 0)
                                            Else
                                                dtEmailBroadCast = objEmail.GetEmailMergeData(moduleId, objWF.RecordID, CCommon.ToLong(dr("numDomainID")), 0, 0)
                                            End If

                                            ' Displaying Tracking no. as link in Email (Added by Priya)
                                            If (moduleId = 2) Then
                                                For Each drrow As DataRow In dtEmailBroadCast.Rows
                                                    Dim strTrackingNo As String = ""
                                                    If (Not String.IsNullOrEmpty(CCommon.ToString(drrow("OppOrderTrackingNo")).Trim())) Then
                                                        Dim strTrackingNoList() As String = CCommon.ToString(drrow("OppOrderTrackingNo")).Split(New String() {"$^$"}, StringSplitOptions.None)
                                                        Dim strIDValue() As String

                                                        If strTrackingNoList.Length > 0 Then
                                                            For k As Integer = 0 To strTrackingNoList.Length - 1
                                                                strIDValue = strTrackingNoList(k).Split(New String() {"#^#"}, StringSplitOptions.None)
                                                                If strIDValue(1).Length > 0 Then
                                                                    strTrackingNo += String.Format("<a href='{0}' target='_blank'>{1}</a>&nbsp;&nbsp;", IIf(strIDValue(0).Length = 0, "javascript:void(0)", String.Format(strIDValue(0) + strIDValue(1), strIDValue(1))), strIDValue(1))
                                                                End If
                                                            Next
                                                        End If
                                                    End If
                                                    drrow("OppOrderTrackingNo") = "<div style='width: 95%;word-break: break-all;'>" & strTrackingNo & "</div>"
                                                Next
                                            End If

                                            dtEmailBroadCastSendTo = objEmail.GetEmailMergeData(1, String.Join(",", strContactIDs.ToArray()), CCommon.ToLong(dr("numDomainID")), 0, 0)

                                            Dim strTo As New List(Of String)
                                            For Each drEmail As DataRow In dtEmailBroadCastSendTo.Rows
                                                strTo.Add(drEmail("ContactEmail"))
                                            Next
                                            'just for patches-testing purpose

                                            'end of testing
                                            If strTo.Count > 0 OrElse (CCommon.ToString(drAct("vcEmailSendTo")) IsNot Nothing) Then
                                                For Each iKey As KeyValuePair(Of String, String) In objEmail.AdditionalMergeFields
                                                    If dtEmailBroadCast.Columns.Contains(iKey.Key) = False Then
                                                        dtEmailBroadCast.Columns.Add(iKey.Key)
                                                    End If
                                                    'Assign values to rows in merge table
                                                    For Each row As DataRow In dtEmailBroadCast.Rows
                                                        If iKey.Key <> "ContactEmail" Then
                                                            row(iKey.Key) = CCommon.ToString(iKey.Value)
                                                        End If
                                                    Next
                                                Next

                                                'Newly Added code
                                                'If (CCommon.ToString(drAct("vcEmailSendTo")) IsNot Nothing) Then
                                                '    Dim alEmail As New ArrayList
                                                '    Dim strContacts() As String = CCommon.ToString(drAct("vcEmailSendTo")).Split(",")
                                                '    'For j = 0 To strContacts.Length - 1

                                                '    'Next
                                                '    For index As Integer = 0 To strContacts.Length - 1
                                                '        alEmail.Add(strContacts(index))
                                                '    Next

                                                '    Dim iEnum As IEnumerator = alEmail.GetEnumerator()
                                                '    While iEnum.MoveNext()
                                                '        dr = dtEmailBroadCast.NewRow
                                                '        dr("ContactEmail") = iEnum.Current.ToString()
                                                '        dr("ContactFirstName") = ""
                                                '        dtEmailBroadCast.Rows.Add(dr)
                                                '    End While
                                                '    dtEmailBroadCast.AcceptChanges()
                                                'End If

                                                'end of Newly added code

                                                Dim strFinalTo As String = Nothing
                                                If CCommon.ToString(drAct("vcEmailSendTo")) = "" Or CCommon.ToString(drAct("vcEmailSendTo")) = Nothing Then
                                                    strFinalTo = String.Join(",", strTo)
                                                Else
                                                    If strTo.Count > 0 Then
                                                        strFinalTo = String.Join(",", strTo) & "," & drAct("vcEmailSendTo")
                                                    Else
                                                        strFinalTo = drAct("vcEmailSendTo")
                                                    End If

                                                End If

                                                Dim mailStatus As Boolean
                                                If CCommon.ToShort(drAct("tintIsAttachment")) = 1 Then
                                                    'uncommnted by ss
                                                    'objEmail.SendEmailWS(strSubject, strBody, "", dtRecordData.Rows(0).Item("vcEmailID"), String.Join(",", strTo), dtEmailBroadCast, dtTable, FromDisplayName:=dtRecordData.Rows(0)("ContactName"), DomainID:=objWF.DomainID)
                                                    objEmail.AdditionalMergeFields.Add("Signature", dtRecordData.Rows(0)("Signature").ToString)
                                                    mailStatus = objEmail.SendEmailWS(strSubject, strBody, "", dtRecordData.Rows(0).Item("vcEmailID"), strFinalTo, dtEmailBroadCast, dtBizDocAttachment, FromDisplayName:=dtRecordData.Rows(0)("ContactName"), DomainID:=objWF.DomainID, UserCntID:=lngUserCntID, tintEmailFrom:=tintEmailFrom)
                                                    dtBizDocAttachment.Rows.Clear()
                                                    dtBizDocAttachment.Columns.Clear()
                                                Else
                                                    'objEmail.SendEmail(strSubject, strBody, "", dtRecordData.Rows(0).Item("vcEmailID"), String.Join(",", strTo), dtEmailBroadCast, FromDisplayName:=dtRecordData.Rows(0)("ContactName"), DomainID:=objWF.DomainID)
                                                    objEmail.AdditionalMergeFields.Add("Signature", dtRecordData.Rows(0)("Signature").ToString)
                                                    mailStatus = objEmail.SendEmailWFA(strSubject, strBody, "", dtRecordData.Rows(0).Item("vcEmailID"), strFinalTo, dtEmailBroadCast, FromDisplayName:=dtRecordData.Rows(0)("ContactName"), DomainID:=objWF.DomainID, UserId:=lngUserCntID, tintEmailFrom:=tintEmailFrom)

                                                End If

                                                If mailStatus = True Then
                                                    If strFinalTo = "" Or strFinalTo = Nothing Then
                                                        WFDescription = "Unable to send a mail.Receiver Email Address doesn't exist."
                                                    Else
                                                        WFDescription = "Email Sent to " & strFinalTo
                                                    End If
                                                    bitSuccess = True
                                                Else
                                                    WFDescription = "Email not Sent.Please Configure SMTP details.GO to Global Settings->General  "
                                                    bitSuccess = False
                                                End If

                                            End If
                                        End If
                                    End If
                                Case CCommon.ToShort(enmWFAction.AssignActionItems)
                                    Dim dtActionItem As DataTable
                                    Dim objActionItem As New ActionItem
                                    With objActionItem
                                        .RowID = drAct("numTemplateID")
                                    End With
                                    dtActionItem = objActionItem.LoadThisActionItemTemplateData()

                                    If dtActionItem.Rows.Count > 0 Then
                                        With objActionItem
                                            .CommID = 0
                                            .Task = CLng(dtActionItem.Rows(0)("numTaskType"))

                                            .ContactID = numContactId
                                            .DivisionID = numDivisionId

                                            .Details = ""
                                            .AssignedTo = IIf(CShort(drAct("tintTicklerActionAssignedTo")) = 1, numRecOwner, numAssignedTo)
                                            .UserCntID = numRecOwner
                                            .DomainID = dr("numDomainID")
                                            If .Task = 973 Then
                                                .BitClosed = 1
                                            Else : .BitClosed = 0
                                            End If
                                            .CalendarName = ""
                                            Dim strDate As DateTime = Date.UtcNow.AddDays(Convert.ToDouble(dtActionItem.Rows(0)("DueDays")))

                                            .StartTime = CDate(strDate.ToString("MM/dd/yyyy 00:00:00") & "AM")
                                            .EndTime = CDate(strDate.ToString("MM/dd/yyyy 23:59:59") & "PM")

                                            .Activity = CLng(dtActionItem.Rows(0)("Activity"))
                                            .Status = CLng(dtActionItem.Rows(0)("Priority"))
                                            .Snooze = 0
                                            .SnoozeStatus = 0
                                            .Remainder = 0
                                            .RemainderStatus = 0
                                            .ClientTimeZoneOffset = 0
                                            .bitOutlook = 0
                                            .SendEmailTemplate = CBool(dtActionItem.Rows(0).Item("bitSendEmailTemp"))
                                            .EmailTemplate = CLng(dtActionItem.Rows(0).Item("numEmailTemplate"))
                                            .Hours = CShort(dtActionItem.Rows(0).Item("tintHours"))
                                            .Alert = CBool(dtActionItem.Rows(0).Item("bitAlert"))
                                            .ActivityId = 0
                                            .FollowUpAnyTime = False
                                        End With

                                        Dim numcommId As Long = 0
                                        numcommId = objActionItem.SaveCommunicationinfo()

                                        'Added By Sachin Sadhu||Date:4thAug2014
                                        'Purpose :To Added Ticker data in work Flow queue based on created Rules
                                        '          Using Change tracking
                                        Dim objWfA As New Workflow()
                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                        objWfA.UserCntID = numRecOwner
                                        objWfA.RecordID = numcommId
                                        objWfA.SaveWFActionItemsQueue()

                                        WFDescription = "Action Items Created"
                                        bitSuccess = True
                                    End If
                                Case CCommon.ToShort(enmWFAction.UpdateFields)
                                    Dim Sql As New System.Text.StringBuilder
                                    Dim SqlFrom As New System.Text.StringBuilder
                                    Dim SqlCondition As New System.Text.StringBuilder

                                    'If CCommon.ToShort(dtWFList.Rows(0)("numFormID")) = 71 Then
                                    '    objWF.FormID = 49
                                    'Else
                                    '    objWF.FormID = CCommon.ToShort(dtWFList.Rows(0)("numFormID"))
                                    'End If
                                    'Select Case CCommon.ToShort(dtWFList.Rows(0)("numFormID"))

                                    Dim tempUpdate As New List(Of String)

                                    Dim drWFActionUpdateFields() As DataRow = dtWFActionUpdateFields.Select("numWFActionID=" & drAct("numWFActionID"))

                                    Select Case objWF.FormID
                                        Case 68 'Organization
                                            If drWFActionUpdateFields.Length > 0 Then
                                                Dim strFiels As String = ""
                                                For Each drCFL As DataRow In drWFActionUpdateFields
                                                    Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))
                                                    Dim boolInsert As Boolean = True
                                                    If drFLCheck.Length > 0 Then
                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                            If drFLCheck(0)("vcLookBackTableName") = "AdditionalContactsInformation" Then 'Organization
                                                                Sql.Append("Update AdditionalContactsInformation a set ")
                                                                SqlCondition.Append(" FROM  DivisionMaster d where d.numDomainId = $1 AND d.numDivisionId = $2 AND d.numDivisionID=a.numDivisionId ")
                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "CompanyInfo" Then 'Company Info
                                                                Sql.Append("Update CompanyInfo c set ")
                                                                SqlCondition.Append("  FROM DivisionMaster d where d.numDomainId = $1 AND d.numDivisionId = $2 AND d.numCompanyId=c.numCompanyId ")
                                                            Else
                                                                Sql.Append("Update DivisionMaster set ")
                                                                SqlCondition.Append(" where DivisionMaster.numDomainId = $1 AND DivisionMaster.numDivisionId = $2")
                                                            End If
                                                        Else 'Update Custom Fields

                                                            If drFLCheck(0)("vcLookBackTableName") = "CFW_FLD_Values" Then 'Organization

                                                                boolInsert = objWF.WFCustomFieldsInsert(CCommon.ToLong(drCFL("numFieldID")), drCFL("vcValue"))
                                                                If boolInsert Then
                                                                    Sql.Append("Update CFW_FLD_Values set ")
                                                                    SqlCondition.Append(" where CFW_FLD_Values.Fld_ID = " & drCFL("numFieldID") & " AND CFW_FLD_Values.RecId = $2")
                                                                End If
                                                            End If
                                                        End If
                                                    End If
                                                    If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                        tempUpdate.Add(String.Format("{0}='{1}'",
                                                                           drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                    Else
                                                        If boolInsert Then
                                                            tempUpdate.Add(String.Format("{0}='{1}'",
                                                                        "Fld_Value", drCFL("vcValue")))
                                                        End If

                                                    End If
                                                    strFiels = strFiels & "," & drCFL("FieldName")
                                                    SqlFrom.Append(String.Join(",", tempUpdate))
                                                    Sql.Append(SqlFrom)
                                                    Sql.Append(SqlCondition)
                                                    objWF.textQuery = Sql.ToString()
                                                    objWF.WFConditionQueryExecute(2)
                                                    WFDescription = "Fields Updated:" & strFiels
                                                    bitSuccess = True
                                                    If bitSuccess = True And objWF.FormID = 68 And drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then
                                                        'Added By Sachin Sadhu||Date:23ndMay12014
                                                        'Purpose :To Added Organization data in work Flow queue based on created Rules
                                                        '          Using Change tracking
                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                        objWfA.SaveWFOrganizationQueue()
                                                        'end of code
                                                    ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "AdditionalContactsInformation" Then
                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = numContactId
                                                        objWfA.SaveWFContactQueue()
                                                    Else
                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                        objWfA.SaveWFOrganizationQueue()
                                                    End If
                                                    If CCommon.ToBool(drFLCheck(0)("bitCustom")) = True And bitSuccess = True Then ' TRIGGER Custom fields based WF rules
                                                        Dim objWFCF As New Workflow()
                                                        objWFCF.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWFCF.UserCntID = numRecOwner
                                                        objWFCF.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                        objWFCF.FormFieldId = CCommon.ToLong(drCFL("numFieldID"))
                                                        objWFCF.SaveWFCFOrganizationQueue()
                                                    End If
                                                    Sql.Clear()
                                                    SqlFrom.Clear()
                                                    SqlCondition.Clear()
                                                    tempUpdate.Clear()
                                                Next
                                            End If
                                        Case 70 'Opportunities & Orders
                                            If drWFActionUpdateFields.Length > 0 Then
                                                Dim strFiels As String = ""
                                                For Each drCFL As DataRow In drWFActionUpdateFields
                                                    Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))
                                                    Dim boolInsert As Boolean = True
                                                    If drFLCheck.Length > 0 Then
                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                            If drFLCheck(0)("vcLookBackTableName") = "CompanyInfo" Then 'Organizatin
                                                                Sql.Append("Update CompanyInfo c set ")
                                                                SqlCondition.Append(" FROM OpportunityMaster o inner join DivisionMaster D on d.numdivisionID=o.numDivisionId where o.numDomainId = $1 AND o.numOppId=$2 AND c.numCompanyId=d.numCompanyId")
                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization
                                                                Sql.Append("Update DivisionMaster d set ")
                                                                SqlCondition.Append(" FROM CompanyInfo c, OpportunityMaster o where o.numDomainId = $1 AND o.numOppId=$2 AND d.numdivisionID=o.numDivisionId AND c.numCompanyId=d.numCompanyId")

                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "OpportunityBizDocs" Then 'BizDocs
                                                                Sql.Append("Update OpportunityBizDocs b set ")
                                                                'SqlCondition.Append(" FROM OpportunityBizDocs b inner join OpportunityMaster o on o.numOppId=b.numOppId  where  b.numOppBizDocsId=$2")
                                                                SqlCondition.Append(" FROM OpportunityMaster o where  o.numDomainId = $1 AND o.numOppId=$2 AND o.numOppId=b.numOppId")

                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "StagePercentageDetails" Then 'StagePercentageDetails
                                                                Sql.Append("Update StagePercentageDetails b set ")
                                                                'SqlCondition.Append(" FROM OpportunityBizDocs b inner join OpportunityMaster o on o.numOppId=b.numOppId  where  b.numOppBizDocsId=$2")
                                                                SqlCondition.Append(" FROM OpportunityMaster o where o.numDomainId = $1 AND o.numOppId=$2 AND o.numOppId=b.numOppId")
                                                            Else
                                                                Sql.Append("Update OpportunityMaster set ") 'Orders Module
                                                                SqlCondition.Append(" where OpportunityMaster.numDomainId = $1 AND OpportunityMaster.numOppID = $2")

                                                            End If
                                                        Else
                                                            If drFLCheck(0)("vcLookBackTableName") = "CFW_Fld_Values_Opp" Then 'Orders

                                                                boolInsert = objWF.WFCustomFieldsInsert(CCommon.ToLong(drCFL("numFieldID")), drCFL("vcValue"))
                                                                If boolInsert Then
                                                                    Sql.Append("Update CFW_Fld_Values_Opp set ")
                                                                    SqlCondition.Append(" where CFW_Fld_Values_Opp.Fld_ID = " & drCFL("numFieldID") & " AND CFW_Fld_Values_Opp.RecId = $2")
                                                                End If
                                                            End If
                                                        End If
                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                            tempUpdate.Add(String.Format("{0}='{1}'",
                                                                               drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                        Else
                                                            If boolInsert Then
                                                                tempUpdate.Add(String.Format("{0}='{1}'",
                                                                            "Fld_Value", drCFL("vcValue")))
                                                            End If
                                                        End If

                                                        SqlFrom.Append(String.Join(",", tempUpdate))

                                                        Sql.Append(SqlFrom)
                                                        Sql.Append(SqlCondition)
                                                        objWF.textQuery = Sql.ToString()
                                                        objWF.WFConditionQueryExecute(2)
                                                        strFiels = strFiels & "," & drCFL("FieldName")
                                                        WFDescription = "Fields Updated:" & strFiels
                                                        bitSuccess = True
                                                        Sql.Clear()
                                                        SqlFrom.Clear()
                                                        SqlCondition.Clear()
                                                        tempUpdate.Clear()

                                                        If bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "OpportunityMaster" Then 'Order Module
                                                            'Added By Sachin Sadhu||Date:23ndMay12014
                                                            'Purpose :To Added Order data in work Flow queue based on created Rules
                                                            '          Using Change tracking
                                                            Dim objWfA As New Workflow()
                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                            objWfA.UserCntID = numRecOwner
                                                            objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                            objWfA.SaveWFOrderQueue()
                                                            'end of code
                                                        ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "OpportunityBizDocs" Then 'BizDocs                                                                           
                                                            Dim objWfA As New Workflow()
                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                            objWfA.UserCntID = numRecOwner
                                                            objWfA.RecordID = numOppBizDOCIDPC
                                                            objWfA.SaveWFBizDocQueue()
                                                        ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization                                                                           
                                                            Dim objWfA As New Workflow()
                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                            objWfA.UserCntID = numRecOwner
                                                            objWfA.RecordID = numDivisionId
                                                            objWfA.SaveWFOrganizationQueue()
                                                        ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "StagePercentageDetails" Then 'Busniess Process                                                                           
                                                            Dim objWfA As New Workflow()
                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                            objWfA.UserCntID = numRecOwner
                                                            objWfA.RecordID = numStageIDPC
                                                            objWfA.SaveWFBusinessProcessQueue()
                                                        Else 'orders
                                                            Dim objWfA As New Workflow()
                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                            objWfA.UserCntID = numRecOwner
                                                            objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                            objWfA.SaveWFOrderQueue()

                                                        End If
                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = True And bitSuccess = True Then ' TRIGGER Custom fields based WF rules
                                                            Dim objWFCF As New Workflow()
                                                            objWFCF.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                            objWFCF.UserCntID = numRecOwner
                                                            objWFCF.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                            objWFCF.FormFieldId = CCommon.ToLong(drCFL("numFieldID"))
                                                            objWFCF.SaveWFCFOrdersQueue()
                                                        End If
                                                    End If
                                                Next
                                            End If

                                        Case 49 'BizDocs

                                            If drWFActionUpdateFields.Length > 0 Then
                                                Dim strFiels As String = ""
                                                For Each drCFL As DataRow In drWFActionUpdateFields
                                                    Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))

                                                    If drFLCheck.Length > 0 Then
                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                            If drFLCheck(0)("vcLookBackTableName") = "OpportunityMaster" Then 'Orders
                                                                Sql.Append("Update OpportunityMaster o set ")
                                                                SqlCondition.Append(" FROM OpportunityBizDocs b where b.numOppBizDocsId=$2 AND o.numOppId=b.numOppId")
                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "CompanyInfo" Then
                                                                Sql.Append("Update CompanyInfo c set ") 'organization
                                                                SqlCondition.Append(" FROM OpportunityBizDocs b inner join OpportunityMaster o on o.numOppId=b.numOppId inner join DivisionMaster D on d.numdivisionID=o.numDivisionId where  b.numOppBizDocsId=$2 AND c.numCompanyId=d.numCompanyId")
                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization
                                                                Sql.Append("Update DivisionMaster d set ")
                                                                SqlCondition.Append("FROM CompanyInfo c, OpportunityBizDocs b inner join OpportunityMaster o on o.numOppId=b.numOppId where  b.numOppBizDocsId=$2 AND d.numdivisionID=o.numDivisionId AND c.numCompanyId=d.numCompanyId")
                                                            Else 'BizDocs module
                                                                Sql.Append(" Update OpportunityBizDocs set   ")
                                                                SqlCondition.Append(" where OpportunityBizDocs.numOppBizDocsId = $2")

                                                            End If

                                                        End If
                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                            tempUpdate.Add(String.Format("{0}='{1}'",
                                                                               drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                        End If

                                                        SqlFrom.Append(String.Join(",", tempUpdate))

                                                        Sql.Append(SqlFrom)
                                                        Sql.Append(SqlCondition)
                                                        objWF.textQuery = Sql.ToString()
                                                        objWF.WFConditionQueryExecute(2)

                                                        strFiels = strFiels & "," & drCFL("FieldName")
                                                        WFDescription = "Fields Updated:" & strFiels
                                                        bitSuccess = True

                                                        Sql.Clear()
                                                        SqlFrom.Clear()
                                                        SqlCondition.Clear()
                                                        tempUpdate.Clear()
                                                        If bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "OpportunityMaster" Then 'Order Module

                                                            Dim objWfA As New Workflow()
                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                            objWfA.UserCntID = numRecOwner
                                                            objWfA.RecordID = numOppIDPC
                                                            objWfA.SaveWFOrderQueue()
                                                            'end of code
                                                        ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "OpportunityBizDocs" Then 'BizDocs                                                                           
                                                            Dim objWfA As New Workflow()
                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                            objWfA.UserCntID = numRecOwner
                                                            objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                            objWfA.SaveWFBizDocQueue()
                                                        ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization                                                                           
                                                            Dim objWfA As New Workflow()
                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                            objWfA.UserCntID = numRecOwner
                                                            objWfA.RecordID = numDivisionId
                                                            objWfA.SaveWFOrganizationQueue()
                                                        Else 'BizDocs
                                                            Dim objWfA As New Workflow()
                                                            objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                            objWfA.UserCntID = numRecOwner
                                                            objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                            objWfA.SaveWFBizDocQueue()
                                                        End If

                                                    End If
                                                Next
                                            End If
                                        Case 94 'Business Process
                                            Sql.Append("Update StagePercentageDetails set ")
                                            SqlCondition.Append(" where StagePercentageDetails.numDomainId = $1 AND StagePercentageDetails.numStageDetailsId = $2")
                                            If drWFActionUpdateFields.Length > 0 Then
                                                For Each drCFL As DataRow In drWFActionUpdateFields
                                                    Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))

                                                    If drFLCheck.Length > 0 Then
                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                            tempUpdate.Add(String.Format("{0}='{1}'",
                                                                               drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                        End If
                                                    End If
                                                Next

                                                SqlFrom.Append(String.Join(",", tempUpdate))

                                                Sql.Append(SqlFrom)
                                                Sql.Append(SqlCondition)
                                                objWF.textQuery = Sql.ToString()
                                                objWF.WFConditionQueryExecute(2)

                                                WFDescription = "Fields Updated"
                                                bitSuccess = True

                                                If bitSuccess = True Then 'Business Process

                                                    Dim objWfA As New Workflow()
                                                    objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                    objWfA.UserCntID = numRecOwner
                                                    objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                    objWfA.SaveWFBusinessProcessQueue()
                                                    'end of code
                                                End If
                                            End If
                                        Case 69 'Contacts                                                           
                                            If drWFActionUpdateFields.Length > 0 Then
                                                Dim strFiels As String = ""
                                                For Each drCFL As DataRow In drWFActionUpdateFields
                                                    Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))
                                                    Dim boolInsert As Boolean = True
                                                    If drFLCheck.Length > 0 Then
                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                            If drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization
                                                                Sql.Append("Update DivisionMaster c set ")
                                                                SqlCondition.Append(" FROM AdditionalContactsInformation o where o.numDomainId = $1 AND o.numContactId=$2 AND c.numdivisionID=o.numDivisionId")
                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "CompanyInfo" Then 'Company Info
                                                                Sql.Append("Update CompanyInfo c set ")
                                                                SqlCondition.Append(" FROM AdditionalContactsInformation o inner join DivisionMaster D on d.numdivisionID=o.numDivisionId where o.numDomainId = $1 AND o.numContactId=$2 AND c.numCompanyId=d.numCompanyId")
                                                            Else
                                                                Sql.Append("Update AdditionalContactsInformation set ")
                                                                SqlCondition.Append(" where AdditionalContactsInformation.numDomainId = $1 AND AdditionalContactsInformation.numContactId = $2")
                                                            End If
                                                        Else
                                                            If drFLCheck(0)("vcLookBackTableName") = "CFW_FLD_Values_Cont" Then 'Contacts

                                                                boolInsert = objWF.WFCustomFieldsInsert(CCommon.ToLong(drCFL("numFieldID")), drCFL("vcValue"))
                                                                If boolInsert Then
                                                                    Sql.Append("Update CFW_FLD_Values_Cont set ")
                                                                    SqlCondition.Append(" where CFW_FLD_Values_Cont.Fld_ID = " & drCFL("numFieldID") & " AND CFW_FLD_Values_Cont.RecId = $2")
                                                                End If
                                                            End If

                                                        End If
                                                    End If
                                                    If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                        tempUpdate.Add(String.Format("{0}='{1}'",
                                                                           drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                    Else
                                                        If boolInsert Then
                                                            tempUpdate.Add(String.Format("{0}='{1}'",
                                                                        "Fld_Value", drCFL("vcValue")))
                                                        End If
                                                    End If
                                                    SqlFrom.Append(String.Join(",", tempUpdate))
                                                    Sql.Append(SqlFrom)
                                                    Sql.Append(SqlCondition)
                                                    objWF.textQuery = Sql.ToString()
                                                    objWF.WFConditionQueryExecute(2)
                                                    strFiels = strFiels & "," & drCFL("FieldName")
                                                    WFDescription = "Fields Updated:" & strFiels
                                                    bitSuccess = True
                                                    Sql.Clear()
                                                    SqlFrom.Clear()
                                                    SqlCondition.Clear()
                                                    tempUpdate.Clear()
                                                    If bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "AdditionalContactsInformation" Then 'Contacts
                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                        objWfA.SaveWFContactQueue()
                                                        'end of code                                                                
                                                    ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'BizDocs                                                                           
                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = numDivisionId
                                                        objWfA.SaveWFOrganizationQueue()
                                                    Else 'Contacts
                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                        objWfA.SaveWFContactQueue()
                                                    End If
                                                    If CCommon.ToBool(drFLCheck(0)("bitCustom")) = True And bitSuccess = True Then ' TRIGGER Custom fields based WF rules
                                                        Dim objWFCF As New Workflow()
                                                        objWFCF.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWFCF.UserCntID = numRecOwner
                                                        objWFCF.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                        objWFCF.FormFieldId = CCommon.ToLong(drCFL("numFieldID"))
                                                        objWFCF.SaveWFCFContactsQueue()
                                                    End If
                                                Next
                                            End If

                                        Case 73 'Projects

                                            If drWFActionUpdateFields.Length > 0 Then
                                                Dim strFiels As String = ""
                                                For Each drCFL As DataRow In drWFActionUpdateFields
                                                    Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))
                                                    Dim boolInsert As Boolean = True
                                                    If drFLCheck.Length > 0 Then
                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                            If drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization
                                                                Sql.Append("Update DivisionMaster c set ")
                                                                SqlCondition.Append(" FROM ProjectsMaster o where  o.numDomainId = $1 AND o.numProId=$2 AND c.numdivisionID=o.numDivisionId")
                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "CompanyInfo" Then 'Company Info
                                                                Sql.Append("Update CompanyInfo c set ")
                                                                SqlCondition.Append(" FROM ProjectsMaster o inner join DivisionMaster D on d.numdivisionID=o.numDivisionId where o.numDomainId = $1 AND o.numProId=$2 AND c.numCompanyId=d.numCompanyId")
                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "StagePercentageDetails" Then 'StagePercentageDetails
                                                                Sql.Append("Update StagePercentageDetails b set ")
                                                                'SqlCondition.Append(" FROM OpportunityBizDocs b inner join OpportunityMaster o on o.numOppId=b.numOppId  where  b.numOppBizDocsId=$2")
                                                                SqlCondition.Append(" FROM ProjectsMaster o where o.numDomainId = $1 AND o.numProId=$2 AND o.numProId=b.numProjectID")
                                                            Else
                                                                Sql.Append("Update ProjectsMaster set ")
                                                                SqlCondition.Append(" where ProjectsMaster.numDomainId = $1 AND ProjectsMaster.numProId = $2")
                                                            End If
                                                        Else
                                                            If drFLCheck(0)("vcLookBackTableName") = "CFW_FLD_Values_Pro" Then 'Projects

                                                                boolInsert = objWF.WFCustomFieldsInsert(CCommon.ToLong(drCFL("numFieldID")), drCFL("vcValue"))
                                                                If boolInsert Then
                                                                    Sql.Append("Update CFW_FLD_Values_Pro set ")
                                                                    SqlCondition.Append(" where CFW_FLD_Values_Pro.Fld_ID = " & drCFL("numFieldID") & " AND CFW_FLD_Values_Pro.RecId = $2")
                                                                End If
                                                            End If
                                                        End If
                                                    End If
                                                    If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                        tempUpdate.Add(String.Format("{0}='{1}'",
                                                                           drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                    Else
                                                        If boolInsert Then
                                                            tempUpdate.Add(String.Format("{0}='{1}'",
                                                                        "Fld_Value", drCFL("vcValue")))
                                                        End If
                                                    End If
                                                    SqlFrom.Append(String.Join(",", tempUpdate))
                                                    Sql.Append(SqlFrom)
                                                    Sql.Append(SqlCondition)
                                                    objWF.textQuery = Sql.ToString()
                                                    objWF.WFConditionQueryExecute(2)
                                                    strFiels = strFiels & "," & drCFL("FieldName")
                                                    WFDescription = "Fields Updated:" & strFiels
                                                    bitSuccess = True
                                                    Sql.Clear()
                                                    SqlFrom.Clear()
                                                    SqlCondition.Clear()
                                                    tempUpdate.Clear()

                                                    If bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "ProjectsMaster" Then 'Order Module

                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                        objWfA.SaveWFProjectsQueue()
                                                        'end of code

                                                    ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization                                                                           
                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = numDivisionId
                                                        objWfA.SaveWFOrganizationQueue()
                                                    ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "StagePercentageDetails" Then 'Busniess Process                                                                           
                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = numStageIDPC
                                                        objWfA.SaveWFBusinessProcessQueue()
                                                    Else 'Projects
                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                        objWfA.SaveWFProjectsQueue()

                                                    End If

                                                    If CCommon.ToBool(drFLCheck(0)("bitCustom")) = True And bitSuccess = True Then ' TRIGGER Custom fields based WF rules
                                                        Dim objWFCF As New Workflow()
                                                        objWFCF.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWFCF.UserCntID = numRecOwner
                                                        objWFCF.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                        objWFCF.FormFieldId = CCommon.ToLong(drCFL("numFieldID"))
                                                        objWFCF.SaveWFCFProjectsQueue()
                                                    End If
                                                Next
                                            End If

                                        Case 72 'Cases

                                            If drWFActionUpdateFields.Length > 0 Then
                                                Dim strFiels As String = ""
                                                For Each drCFL As DataRow In drWFActionUpdateFields
                                                    Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))
                                                    Dim boolInsert As Boolean = True
                                                    If drFLCheck.Length > 0 Then
                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                            If drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization
                                                                Sql.Append("Update DivisionMaster c set ")
                                                                SqlCondition.Append(" FROM Cases o where  o.numDomainId = $1 AND o.numCaseId=$2 AND c.numdivisionID=o.numDivisionId")
                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "CompanyInfo" Then 'Company Info
                                                                Sql.Append("Update CompanyInfo c set ")
                                                                SqlCondition.Append(" FROM Cases o inner join DivisionMaster D on d.numdivisionID=o.numDivisionId where o.numDomainId = $1 AND o.numCaseId=$2 AND c.numCompanyId=d.numCompanyId")
                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "OpportunityMaster" Then 'Orders
                                                                Sql.Append("Update OpportunityMaster o set ")
                                                                SqlCondition.Append(" FROM Cases c inner join CaseOpportunities cp on c.numCaseId=cp.numCaseId where c.numDomainId = $1 AND c.numCaseId = $2 AND o.numOppId=cp.numOppId")
                                                            Else
                                                                Sql.Append("Update Cases set ")
                                                                SqlCondition.Append(" where Cases.numDomainId = $1 AND Cases.numCaseId = $2")
                                                            End If
                                                        Else
                                                            If drFLCheck(0)("vcLookBackTableName") = "CFW_FLD_Values_Case" Then 'Cases

                                                                boolInsert = objWF.WFCustomFieldsInsert(CCommon.ToLong(drCFL("numFieldID")), drCFL("vcValue"))
                                                                If boolInsert Then
                                                                    Sql.Append("Update CFW_FLD_Values_Case set ")
                                                                    SqlCondition.Append(" where CFW_FLD_Values_Case.Fld_ID = " & drCFL("numFieldID") & " AND CFW_FLD_Values_Case.RecId = $2")
                                                                End If


                                                            End If
                                                        End If
                                                    End If
                                                    If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                        tempUpdate.Add(String.Format("{0}='{1}'",
                                                                           drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                    Else
                                                        If boolInsert Then
                                                            tempUpdate.Add(String.Format("{0}='{1}'",
                                                                        "Fld_Value", drCFL("vcValue")))
                                                        End If
                                                    End If
                                                    SqlFrom.Append(String.Join(",", tempUpdate))
                                                    Sql.Append(SqlFrom)
                                                    Sql.Append(SqlCondition)
                                                    objWF.textQuery = Sql.ToString()
                                                    objWF.WFConditionQueryExecute(2)
                                                    strFiels = strFiels & "," & drCFL("FieldName")
                                                    WFDescription = "Fields Updated:" & strFiels
                                                    bitSuccess = True
                                                    Sql.Clear()
                                                    SqlFrom.Clear()
                                                    SqlCondition.Clear()
                                                    tempUpdate.Clear()

                                                    If bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "OpportunityMaster" Then 'Order Module

                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = numOppIDPC
                                                        objWfA.SaveWFOrderQueue()
                                                        'end of code
                                                    ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "Cases" Then 'Cases                                                                           
                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                        objWfA.SaveWFCasesQueue()
                                                    ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization                                                                           
                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = numDivisionId
                                                        objWfA.SaveWFOrganizationQueue()

                                                    Else 'Cases
                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                        objWfA.SaveWFCasesQueue()

                                                    End If
                                                    If CCommon.ToBool(drFLCheck(0)("bitCustom")) = True And bitSuccess = True Then ' TRIGGER Custom fields based WF rules
                                                        Dim objWFCF As New Workflow()
                                                        objWFCF.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWFCF.UserCntID = numRecOwner
                                                        objWFCF.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                        objWFCF.FormFieldId = CCommon.ToLong(drCFL("numFieldID"))
                                                        objWFCF.SaveWFCFCasesQueue()
                                                    End If
                                                Next
                                            End If

                                            'Sql.Append("Update Cases set ")
                                            'SqlCondition.Append(" where Cases.numDomainId = $1 AND Cases.numCaseId = $2")
                                            'If drWFActionUpdateFields.Length > 0 Then
                                            '    For Each drCFL As DataRow In drWFActionUpdateFields
                                            '        Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))

                                            '        If drFLCheck.Length > 0 Then
                                            '            If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                            '                tempUpdate.Add(String.Format("{0}='{1}'", _
                                            '                                       drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                            '            End If
                                            '        End If
                                            '    Next

                                            '    SqlFrom.Append(String.Join(",", tempUpdate))

                                            '    Sql.Append(SqlFrom)
                                            '    Sql.Append(SqlCondition)
                                            '    objWF.textQuery = Sql.ToString()
                                            '    objWF.WFConditionQueryExecute(2)

                                            '    WFDescription = "Fields Updated"
                                            '    bitSuccess = True

                                            'End If
                                        Case 124 'Actiom Items(Tickler)

                                            If drWFActionUpdateFields.Length > 0 Then
                                                Dim strFiels As String = ""
                                                For Each drCFL As DataRow In drWFActionUpdateFields
                                                    Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))

                                                    If drFLCheck.Length > 0 Then
                                                        If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                            If drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization
                                                                Sql.Append("Update DivisionMaster c set ")
                                                                SqlCondition.Append(" FROM Communication o  where  o.numDomainId = $1 AND o.numCommId=$2 AND c.numdivisionID=o.numDivisionId")
                                                            ElseIf drFLCheck(0)("vcLookBackTableName") = "CompanyInfo" Then 'Company Info
                                                                Sql.Append("Update CompanyInfo c set ")
                                                                SqlCondition.Append(" FROM Communication o inner join DivisionMaster D on d.numdivisionID=o.numDivisionId where  o.numDomainId = $1 AND o.numCommId=$2 AND c.numCompanyId=d.numCompanyId")
                                                            Else
                                                                Sql.Append("Update Communication set ")
                                                                SqlCondition.Append(" where Communication.numDomainId = $1 AND Communication.numCommId = $2")
                                                            End If

                                                        End If
                                                    End If
                                                    If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                        tempUpdate.Add(String.Format("{0}='{1}'",
                                                                           drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                    End If
                                                    SqlFrom.Append(String.Join(",", tempUpdate))
                                                    Sql.Append(SqlFrom)
                                                    Sql.Append(SqlCondition)
                                                    objWF.textQuery = Sql.ToString()
                                                    objWF.WFConditionQueryExecute(2)
                                                    strFiels = strFiels & "," & drCFL("FieldName")
                                                    WFDescription = "Fields Updated:" & strFiels
                                                    bitSuccess = True
                                                    Sql.Clear()
                                                    SqlFrom.Clear()
                                                    SqlCondition.Clear()
                                                    tempUpdate.Clear()

                                                    If bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "Communication" Then 'Order Module
                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                        objWfA.SaveWFActionItemsQueue()
                                                        'end of code

                                                    ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then 'Organization                                                                           
                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = numDivisionId
                                                        objWfA.SaveWFOrganizationQueue()

                                                    Else 'Tickler
                                                        Dim objWfA As New Workflow()
                                                        objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objWfA.UserCntID = numRecOwner
                                                        objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                        objWfA.SaveWFActionItemsQueue()

                                                    End If
                                                Next
                                            End If
                                    End Select
                                Case CCommon.ToShort(enmWFAction.DisplayAlertMessage)

                                    ' Dim cph As ContentPlaceHolder = DirectCast(Me.Master.FindControl("TabsPlaceHolder"), ContentPlaceHolder)
                                    objWF.vcAlertMessage = CCommon.ToString(drAct("vcAlertMessage").ToString)
                                    objWF.WFID = CCommon.ToLong(dr("numWFID"))
                                    objWF.intAlertStatus = 1
                                    objWF.SaveWorkFlowAlerts()
                                    WFDescription = "Alert is shown"
                                    bitSuccess = True
                                Case CCommon.ToShort(enmWFAction.BizDocApproval)
                                    Try
                                        Dim objDocuments As New DocumentList
                                        Dim strReceiverName As String() = drAct("vcApproval").Split(",")
                                        Dim k As Integer = 0
                                        For k = 0 To strReceiverName.Length - 1

                                            objDocuments.GenDocID = CCommon.ToLong(dr("numRecordID"))
                                            objDocuments.ContactID = CCommon.ToLong(strReceiverName(k))
                                            objDocuments.CDocType = "B"
                                            objDocuments.byteMode = 1
                                            objDocuments.DomainID = CCommon.ToLong(dr("numDomainID"))
                                            objDocuments.UserCntID = numRecOwner
                                            objDocuments.ManageApprovers()

                                            Dim objOppBizDocs As New OppBizDocs
                                            objOppBizDocs.OppBizDocId = CCommon.ToLong(dr("numRecordID"))
                                            objOppBizDocs.DomainID = CCommon.ToLong(dr("numDomainID"))
                                            Dim dsOppData As DataSet = objOppBizDocs.GetBizDocsDetails

                                            'Send Approval requeset via Mail
                                            Dim ObjCmn As New CCommon
                                            Dim objSendMail As New Email
                                            Dim dtDocDetails As DataTable = objSendMail.GetEmailTemplateByCode("#SYS#DOC_APPROVAL_REQUEST", CCommon.ToLong(dr("numDomainID")))
                                            Dim objUserAccess As New UserAccess
                                            objUserAccess.ContactID = numRecOwner
                                            objUserAccess.DomainID = CCommon.ToLong(dr("numDomainID"))
                                            Dim dtUser As DataTable = objUserAccess.GetExtranetUserDetails()
                                            Dim strPortalUrl As String = "http://portal.bizautomation.com/Login.aspx"
                                            Dim strPortalApprovalLink As String
                                            If dtUser.Rows.Count = 1 Then

                                                strPortalApprovalLink = CCommon.ToString(strPortalUrl) & QueryEncryption.EncryptQueryString("&from=email&u=" + CCommon.ToString(dtUser.Rows(0)("vcEmail")) + "&p=" + ObjCmn.Encrypt(CCommon.ToString(dtUser.Rows(0)("vcPassword"))))
                                                strPortalApprovalLink = "<a href=" & strPortalApprovalLink & " target='_blank' >click here</a>"

                                            End If

                                            If dtDocDetails.Rows.Count > 0 Then
                                                Dim strBody As String = dtDocDetails.Rows(0).Item("vcDocdesc")
                                                Dim strSubject As String = IIf(IsDBNull(dtDocDetails.Rows(0).Item("vcSubject")), "", dtDocDetails.Rows(0).Item("vcSubject"))

                                                Dim objEmail As New Email
                                                objEmail.AdditionalMergeFields.Add("DocumentName", dsOppData.Tables(0).Rows(0).Item("vcBizDocID"))
                                                objEmail.AdditionalMergeFields.Add("LoggedInUser", dtRecordData.Rows(0)("ContactName"))
                                                objEmail.AdditionalMergeFields.Add("PortalDirectLoginLink", strPortalApprovalLink)
                                                objEmail.AdditionalMergeFields.Add("Signature", dtRecordData.Rows(0)("Signature"))
                                                If strBody.ToLower().Contains("##ContactOpt-OutLink##".ToLower()) Then
                                                    objEmail.AdditionalMergeFields.Add("ContactOpt-OutLink", "")
                                                End If

                                                Dim dtEmailBroadCast As New DataTable

                                                Dim EmailToType() As String = drAct("vcApproval").ToString().Split(",")
                                                ' Dim strContactIDs As New ArrayList

                                                'If EmailToType.Contains(1) Then 'Owner of trigger record
                                                '    strContactIDs.Add(numRecOwner)
                                                'End If

                                                'If EmailToType.Contains(2) Then 'Assignee of trigger record
                                                '    strContactIDs.Add(numAssignedTo)
                                                'End If

                                                'If EmailToType.Contains(3) Then 'Primary contact of trigger record
                                                '    strContactIDs.Add(numContactId)
                                                'End If

                                                ' If strContactIDs.Count > 0 Then
                                                dtEmailBroadCast = objEmail.GetEmailMergeData(1, String.Join(",", EmailToType.ToArray()), CCommon.ToLong(dr("numDomainID")), 0, 0)

                                                Dim strTo As New List(Of String)
                                                For Each drEmail As DataRow In dtEmailBroadCast.Rows
                                                    strTo.Add(drEmail("ContactEmail"))
                                                Next

                                                If strTo.Count > 0 Then
                                                    For Each iKey As Object In objEmail.AdditionalMergeFields.Keys
                                                        If dtEmailBroadCast.Columns.Contains(iKey.ToString) = False Then
                                                            dtEmailBroadCast.Columns.Add(iKey.ToString)
                                                        End If
                                                        'Assign values to rows in merge table
                                                        For Each row As DataRow In dtEmailBroadCast.Rows
                                                            If iKey.ToString <> "ContactEmail" Then
                                                                ' row(iKey.ToString) = CCommon.ToString(iKey)
                                                                row(iKey.ToString) = CCommon.ToString(objEmail.AdditionalMergeFields(iKey))
                                                            End If
                                                        Next
                                                    Next

                                                    objEmail.SendEmail(strSubject, strBody, "", dtRecordData.Rows(0).Item("vcEmailID"), String.Join(",", strTo), dtEmailBroadCast, FromDisplayName:=dtRecordData.Rows(0)("ContactName"), DomainID:=objWF.DomainID)

                                                End If
                                                ' End If
                                            End If
                                            'end of Request
                                        Next

                                        WFDescription = "BizDoc Approval Request sent to Tickler"
                                        bitSuccess = True

                                    Catch ex As Exception
                                        If ex.Message = "No_EXTRANET" Then
                                            WFDescription = "Can not add selected contact to approver list. Your option is to add contact as external users from ""Administrator->User Administration->External Users"" and try again."
                                            bitSuccess = False
                                            Exit Sub
                                        Else
                                            Throw ex
                                        End If
                                    End Try
                                Case CCommon.ToShort(enmWFAction.CreateBizDoc)
                                    Dim dtCheckBizDocs As New DataTable()
                                    dtCheckBizDocs = CheckDuplicateBizDocs(CCommon.ToLong(dr("numRecordID")), CCommon.ToLong(dr("numDomainID")), CCommon.ToShort(drAct("numOppType")), numRecOwner, CCommon.ToLong(drAct("numBizDocTypeID")), CCommon.ToLong(drAct("numBizDocTemplateID")))

                                    If dtCheckBizDocs.Rows.Count > 0 Then
                                        WFDescription = "This BizDoc-" & CCommon.ToString(drAct("vcBizDoc")) & "  already exists for this order "
                                        bitSuccess = True
                                    Else
                                        CreateBizDoc(CCommon.ToLong(dr("numRecordID")), CCommon.ToLong(dr("numDomainID")), CCommon.ToShort(drAct("numOppType")), numRecOwner, CCommon.ToLong(drAct("numBizDocTypeID")), CCommon.ToLong(drAct("numBizDocTemplateID")))
                                        WFDescription = "BizDoc-" & CCommon.ToString(drAct("vcBizDoc")) & " has been created"

                                        bitSuccess = True
                                    End If

                                Case CCommon.ToShort(enmWFAction.PromoteOrganization)
                                    Dim objLeads As New LeadsIP
                                    objLeads.DivisionID = CCommon.ToLong(dr("numRecordID"))
                                    objLeads.DomainID = CCommon.ToLong(dr("numDomainID"))
                                    Dim dtCrmType As DataTable = objLeads.GetCRMType()

                                    Dim tintCRMType As Integer = CCommon.ToInteger(dtCrmType.Rows(0)("tintCRMType").ToString)

                                    If tintCRMType = 0 Then ' Leads

                                        objLeads.UserCntID = numRecOwner
                                        objLeads.ContactID = numContactId
                                        objLeads.DivisionID = CCommon.ToLong(dr("numRecordID"))
                                        objLeads.PromoteLead()
                                        WFDescription = CCommon.ToString(dr("numRecordID")) & "-Organization Promoted"
                                        bitSuccess = True
                                    ElseIf tintCRMType = 1 Then 'Prospects
                                        objLeads.DivisionID = CCommon.ToLong(dr("numRecordID"))
                                        objLeads.byteMode = 2
                                        objLeads.DomainID = CCommon.ToLong(dr("numDomainID"))
                                        objLeads.UserCntID = numRecOwner
                                        objLeads.DemoteOrg()
                                        WFDescription = CCommon.ToString(dr("numRecordID")) & "-Organization Promoted"
                                        bitSuccess = True
                                    Else 'Account
                                        WFDescription = CCommon.ToString(dr("numRecordID")) & "-This Organization is already in Account ,No scope for Promotion"
                                        bitSuccess = True
                                    End If
                                Case CCommon.ToShort(enmWFAction.DemoteOrganization)
                                    Dim objLeads As New LeadsIP
                                    objLeads.DivisionID = CCommon.ToLong(dr("numRecordID"))
                                    objLeads.DomainID = CCommon.ToLong(dr("numDomainID"))
                                    Dim dtCrmType As DataTable = objLeads.GetCRMType()

                                    Dim tintCRMType As Integer = CCommon.ToInteger(dtCrmType.Rows(0)("tintCRMType").ToString)

                                    If tintCRMType = 1 Then 'Prospect
                                        objLeads.DivisionID = CCommon.ToLong(dr("numRecordID"))
                                        objLeads.byteMode = 0
                                        objLeads.DomainID = CCommon.ToLong(dr("numDomainID"))
                                        objLeads.UserCntID = numRecOwner
                                        objLeads.DemoteOrg()
                                        WFDescription = CCommon.ToString(dr("numRecordID")) & "-Organization demoted"
                                        bitSuccess = True
                                    ElseIf tintCRMType = 2 Then 'Account
                                        Dim objAccounts As New CAccounts
                                        Dim intCount As Integer
                                        objAccounts.DivisionID = CCommon.ToLong(dr("numRecordID"))
                                        intCount = objAccounts.CheckDealsWon
                                        If intCount = 0 Then
                                            objLeads.DivisionID = CCommon.ToLong(dr("numRecordID"))
                                            objLeads.byteMode = 1
                                            objLeads.DomainID = CCommon.ToLong(dr("numDomainID"))
                                            objLeads.UserCntID = numRecOwner
                                            objLeads.DemoteOrg()
                                        Else
                                            WFDescription = "This record can not be demoted because it contains completed deals. To demote this record, delete all deals from the Deal History section, then try demoting again."
                                            bitSuccess = True
                                        End If
                                    Else
                                        WFDescription = "This record can not be demoted because It's on Lead stage"
                                        bitSuccess = True

                                    End If

                                Case CCommon.ToShort(enmWFAction.SMSAlerts)
                                    Dim objEmail As New Email
                                    Dim SMSToType() As String = drAct("vcEmailToType").ToString().Split(",")
                                    Dim strContactIDs As New ArrayList

                                    If SMSToType.Contains(1) Then 'Owner of trigger record
                                        strContactIDs.Add(numRecOwner)
                                    End If

                                    If SMSToType.Contains(2) Then 'Assignee of trigger record
                                        strContactIDs.Add(numAssignedTo)
                                    End If

                                    If SMSToType.Contains(3) Then 'Primary contact of trigger record
                                        strContactIDs.Add(numContactId)
                                    End If
                                    If SMSToType.Contains(4) Then 'Internal Project Manager
                                        strContactIDs.Add(numInternalPM)
                                    End If
                                    If SMSToType.Contains(5) Then 'External Project Manager
                                        strContactIDs.Add(numExternalPM)
                                    End If
                                    If SMSToType.Contains(6) Then 'Next Stage Assigned to
                                        strContactIDs.Add(numNextAssignedTo)
                                    End If
                                    If SMSToType.Contains(7) Then 'Assignee Of Trigger Record(Project)
                                        strContactIDs.Add(numNextAssignedTo)
                                    End If
                                    Dim dtSMSBroadCast As New DataTable
                                    Dim ACCOUNT_SID As String = "ACf3bc46e894a584aa43c6e934eb099db1"
                                    Dim AUTH_TOKEN As String = "5a190ab5b2830b4dea058dbcc4c0590f"

                                    Dim client As New TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN)

                                    If strContactIDs.Count > 0 Then
                                        dtSMSBroadCast = objEmail.GetEmailMergeData(1, String.Join(",", strContactIDs.ToArray()), CCommon.ToLong(dr("numDomainID")), 0, 0)
                                        Dim strTo As New List(Of String)
                                        For Each drEmail As DataRow In dtSMSBroadCast.Rows
                                            strTo.Add(drEmail("ContactPhone"))
                                            Dim message = client.SendSmsMessage("+12545312324", drEmail("ContactPhone").ToString(), CCommon.ToString(drAct("vcSMSText").ToString))
                                            If message.RestException IsNot Nothing Then                                                        ' handle the error ...
                                                WFDescription = message.RestException.Message.ToString
                                                bitSuccess = False
                                            Else
                                                WFDescription = "Message Has been Sent to" & drEmail("ContactPhone").ToString()
                                                bitSuccess = True
                                            End If
                                        Next
                                    End If

                            End Select

                        Catch ex As Exception
                            WFDescription = ex.Message
                            bitSuccess = False
                        End Try

                        Dim objWF1 As New Workflow
                        With objWF1
                            .WFQueueID = 0 'CCommon.ToLong(dr("numWFQueueID"))
                            .WFID = objWF.WFID
                            .WFDescription = WFDescription
                            .ManageWorkFlowQueue(2, _bitSuccess:=bitSuccess)
                        End With
                    Next

                    'Insert RecordID in WorkFlowDateFieldExecutionHistory table

                    If (bitSuccess = True) Then
                        Dim bitInsertDateFieldHistory As Boolean = objWF.SaveWorkFlowDateFieldExecutionHistory(objWF.RecordID, objWF.FormID)
                    End If

                    objWF = New Workflow
                    With objWF
                        .WFQueueID = 0 'CCommon.ToLong(dr("numWFQueueID"))
                        .DomainID = CCommon.ToLong(dr("numDomainID"))
                        .tintProcessStatus = CCommon.ToShort(enmWFQueueProcessStatus.Success)
                        .ManageWorkFlowQueue(1)
                    End With
                    'Else 'If date not matched then it'll be Peding Execution
                    '    objWF = New Workflow
                    '    With objWF
                    '        .WFQueueID = 0 'CCommon.ToLong(dr("numWFQueueID"))
                    '        .DomainID = CCommon.ToLong(dr("numDomainID"))
                    '        .tintProcessStatus = CCommon.ToShort(enmWFQueueProcessStatus.PendingExecution)
                    '        .ManageWorkFlowQueue(1)
                    '    End With
                    'End If
                Else ' If date not matched then it'll be Peding Execution
                    objWF = New Workflow
                    With objWF
                        .WFQueueID = 0 'CCommon.ToLong(dr("numWFQueueID"))
                        .DomainID = CCommon.ToLong(dr("numDomainID"))
                        .tintProcessStatus = CCommon.ToShort(enmWFQueueProcessStatus.ConditionNotMatch)
                        .ManageWorkFlowQueue(1)
                    End With
                End If

            End If 'end of Time based Action
        End Sub

        Public Sub ARAgingWorkFlowExecution()
            Try

                Dim objWF As New Workflow
                Dim dtARAgingWF As DataTable = objWF.GetARAgingWorkFlows()

                If dtARAgingWF.Rows.Count > 0 Then
                    For Each dr As DataRow In dtARAgingWF.Rows

                        objWF.WFID = CCommon.ToLong(dr("numWFID"))
                        objWF.DomainID = CCommon.ToLong(dr("numDomainID"))

                        Dim ds As DataSet
                        ds = objWF.GetWorkFlowMasterDetail

                        Dim objWFObject As New WorkFlowObject

                        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            Dim dtWFList As DataTable = ds.Tables(0)
                            Dim dtWFTriggerFields As DataTable = ds.Tables(1)
                            Dim dtWFConditions As DataTable = ds.Tables(2)
                            Dim dtWFActions As DataTable = ds.Tables(3)
                            Dim dtWFActionUpdateFields As DataTable = ds.Tables(4)

                            If CCommon.ToShort(dtWFList.Rows(0)("numFormID")) = 71 Then
                                objWF.FormID = 49
                            Else
                                objWF.FormID = CCommon.ToShort(dtWFList.Rows(0)("numFormID"))
                            End If

                            ' objWF.RecordID = CCommon.ToLong(dr("numRecordID"))
                            objWF.intDays = CCommon.ToInteger(dtWFList.Rows(0)("intDays"))
                            objWF.intActionOn = CCommon.ToInteger(dtWFList.Rows(0)("intActionOn"))
                            objWF.vcDateField = CCommon.ToString(dtWFList.Rows(0)("vcDateField"))

                            objWF.numFieldID = CCommon.ToLong(dtWFList.Rows(0)("numFieldID"))
                            objWF.boolCustom = CCommon.ToBool(dtWFList.Rows(0)("bitCustom"))

                            DomainID = CCommon.ToLong(dr("numDomainID"))

                            Dim numUserCntId As Long = CCommon.ToLong(dtWFList.Rows(0)("numCreatedBy"))

                            Dim strTablename As String = Nothing
                            Dim moduleId As Integer = 0
                            Select Case objWF.FormID
                                Case 68 'Organization
                                    strTablename = "DivisionMaster"
                                    moduleId = 1
                                Case 70 'Opportunities & Orders                                  
                                    strTablename = "OpportunityMaster"
                                    moduleId = 2
                                Case 49 'BizDocs
                                    strTablename = "OpportunityBizDocs"
                                    moduleId = 8
                                Case 94 'Business Process
                                    strTablename = "StagePercentageDetails"
                                Case 69 'Contacts
                                    strTablename = "AdditionalContactsInformation"
                                    moduleId = 11
                                Case 73 'Projects
                                    strTablename = "ProjectsMaster"
                                    moduleId = 4
                                Case 72 'Cases
                                    strTablename = "Cases"
                                    moduleId = 5
                                Case 124 'Tickler/Action Items
                                    strTablename = "Communication"
                                    moduleId = 6
                                Case 138 'A/R Aging
                                    strTablename = "DivisionMaster"
                                    moduleId = 1
                                Case 148 'A/R Aging
                                    strTablename = "DivisionMaster"
                                    moduleId = 1
                            End Select

                            ' Fetch records based on set ARAging Past Days Conditons As Queue
                            Dim dtARAgingWFQueue As DataTable
                            dtARAgingWFQueue = objWF.GetARAgingWorkFlowQueue(numUserCntId, objWF.intDays)

                            ' Execute Actions of WF for fetched records

                            If (dtARAgingWFQueue.Rows.Count > 0) Then
                                For Each drRecord As DataRow In dtARAgingWFQueue.Rows
                                    objWF.RecordID = CCommon.ToLong(drRecord("numRecordID"))
                                    ExecuteARAgingActions(objWF, dtWFActions, dr, moduleId, dtWFActionUpdateFields)
                                Next
                            End If

                        Else
                            objWF = New Workflow
                            With objWF
                                .WFQueueID = 0 'CCommon.ToLong(dr("numWFQueueID"))
                                .DomainID = CCommon.ToLong(dr("numDomainID"))
                                .tintProcessStatus = CCommon.ToShort(enmWFQueueProcessStatus.NoWFFound)
                                .ManageWorkFlowQueue(1)
                            End With
                        End If
                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub ExecuteARAgingActions(ByVal objWF As Workflow, ByVal dtWFActions As DataTable, ByVal dr As DataRow, ByVal moduleId As Integer, ByVal dtWFActionUpdateFields As DataTable)

            Dim dtFieldsList As DataTable
            dtFieldsList = objWF.GetWorkFlowFormFieldMaster()

            Dim numContactId, numDivisionId, numRecOwner, numAssignedTo, numInternalPM, numExternalPM, numNextAssignedTo, numOppIDPC, numStageIDPC, numOppBizDOCIDPC, numProjectID, numCaseID, numCommID As Long
            Dim numPhone As String
            Dim dtRecordData As DataTable = objWF.GetWFRecordData

            numContactId = CCommon.ToLong(dtRecordData.Rows(0)("numContactId"))
            numDivisionId = CCommon.ToLong(dtRecordData.Rows(0)("numDivisionId"))
            numRecOwner = CCommon.ToLong(dtRecordData.Rows(0)("numRecOwner"))
            numAssignedTo = CCommon.ToLong(dtRecordData.Rows(0)("numAssignedTo"))
            numInternalPM = CCommon.ToLong(dtRecordData.Rows(0)("numInternalPM"))
            numExternalPM = CCommon.ToLong(dtRecordData.Rows(0)("numExternalPM"))
            numNextAssignedTo = CCommon.ToLong(dtRecordData.Rows(0)("numNextAssignedTo"))
            numPhone = CCommon.ToString(dtRecordData.Rows(0)("numPhone"))
            numOppIDPC = CCommon.ToLong(dtRecordData.Rows(0)("numOppIDPC"))
            numStageIDPC = CCommon.ToLong(dtRecordData.Rows(0)("numStageIDPC"))
            numOppBizDOCIDPC = CCommon.ToLong(dtRecordData.Rows(0)("numOppBizDOCIDPC"))
            numProjectID = CCommon.ToLong(dtRecordData.Rows(0)("numProjectID"))
            numCaseID = CCommon.ToLong(dtRecordData.Rows(0)("numCaseId"))
            numCommID = CCommon.ToLong(dtRecordData.Rows(0)("numCommId"))

            Dim bitSuccess As Boolean = False
            For Each drAct As DataRow In dtWFActions.Rows
                Dim WFDescription As String = ""

                Try
                    Select Case CCommon.ToShort(drAct("tintActionType"))

                        Case CCommon.ToShort(enmWFAction.SendAlerts) 'Send Email
                            Dim dtDocDetails As DataTable
                            Dim objDocuments As New DocumentList
                            With objDocuments
                                .GenDocID = drAct("numTemplateID")
                                .DomainID = CCommon.ToLong(dr("numDomainID"))
                                dtDocDetails = .GetDocByGenDocID
                            End With

                            If dtDocDetails.Rows.Count > 0 Or CCommon.ToLong(drAct("numTemplateID")) = "99999" Then

                                Dim strBody As String = Nothing
                                Dim strSubject As String = Nothing

                                If CCommon.ToLong(drAct("numTemplateID")) = "99999" Then 'Compose Message
                                    strBody = drAct("vcMailBody")
                                    strSubject = drAct("vcMailSubject")
                                Else
                                    strBody = dtDocDetails.Rows(0).Item("vcDocdesc")
                                    strSubject = IIf(IsDBNull(dtDocDetails.Rows(0).Item("vcSubject")), "", dtDocDetails.Rows(0).Item("vcSubject"))
                                End If

                                Dim objEmail As New Email

                                If strBody.ToLower().Contains("##ContactOpt-OutLink##".ToLower()) Then
                                    objEmail.AdditionalMergeFields.Add("ContactOpt-OutLink", "")
                                End If

                                Dim dtEmailBroadCast As New DataTable
                                Dim dtEmailBroadCastSendTo As New DataTable
                                Dim lngUserCntID As Long = 0
                                Dim tintEmailFrom As Short = CCommon.ToShort(drAct("tintEmailFrom"))

                                If tintEmailFrom = 1 Then
                                    lngUserCntID = numRecOwner
                                ElseIf tintEmailFrom = 2 Then
                                    lngUserCntID = numAssignedTo
                                End If

                                Dim EmailToType() As String = drAct("vcEmailToType").ToString().Split(",")
                                Dim strContactIDs As New ArrayList
                                Dim strContactIDWebLead As New ArrayList
                                If EmailToType.Contains(1) Then 'Owner of trigger record
                                    strContactIDs.Add(numRecOwner)
                                End If

                                If EmailToType.Contains(2) Then 'Assignee of trigger record
                                    strContactIDs.Add(numAssignedTo)
                                End If

                                If EmailToType.Contains(3) Then 'Primary contact of trigger record
                                    strContactIDs.Add(numContactId)
                                End If
                                If EmailToType.Contains(4) Then 'Internal Project Manager
                                    strContactIDs.Add(numInternalPM)
                                End If
                                If EmailToType.Contains(5) Then 'External Project Manager
                                    strContactIDs.Add(numExternalPM)
                                End If
                                If EmailToType.Contains(6) Then 'Next Stage Assigned to
                                    strContactIDs.Add(numNextAssignedTo)
                                End If
                                If EmailToType.Contains(7) Then 'Assignee Of Trigger Record(Project)
                                    strContactIDs.Add(numNextAssignedTo)
                                End If

                                '  Bizdoc Attachment
                                Dim dtTable As DataTable
                                If CCommon.ToShort(drAct("tintIsAttachment")) = 1 Then

                                    Dim objOppBizDocs As New OppBizDocs
                                    objOppBizDocs.OppBizDocId = CCommon.ToLong(dr("numRecordID"))
                                    objOppBizDocs.DomainID = CCommon.ToLong(dr("numDomainID"))
                                    Dim dsOppData As DataSet = objOppBizDocs.GetBizDocsDetails
                                    Dim objCommon1 As New CCommon
                                    Dim dsDateFormat As DataSet = objCommon1.GetDomainDateFormat(CCommon.ToLong(dr("numDomainID")))
                                    Dim htmlCodeToConvert As String = GenerateBizDocPDF(CCommon.ToLong(dr("numRecordID")), CCommon.ToLong(dsOppData.Tables(0).Rows(0)("numOppId")), CCommon.ToLong(dr("numDomainID")), CCommon.ToLong(dtRecordData.Rows(0)("numRecOwner")), 0, dsDateFormat.Tables(0).Rows(0)("vcDateFormat").ToString())
                                    '  Dim htmlCodeToConvert As String = GenerateBizDocPDF(CCommon.ToLong(dr("numRecordID")), CCommon.ToLong(dsOppData.Tables(0).Rows(0)("numOppId")), CCommon.ToLong(dr("numDomainID")), CCommon.ToLong(dtRecordData.Rows(0)("numRecOwner")), 0, FormattedDateFromDate(dsOppData.Tables(0).Rows(0)("dtFromDate").ToString, dsDateFormat.Tables(0).Rows(0)("vcDateFormat").ToString()))
                                    ' Dim htmlCodeToConvert As String = getBizDocDetails(CCommon.ToLong(dr("numDomainID")), CCommon.ToLong(dr("numRecordID")), CCommon.ToLong(dsOppData.Tables(0).Rows(0)("numOppId")), CCommon.ToLong(dsOppData.Tables(0).Rows(0)("numBizDocId")))
                                    Dim i As Integer
                                    Dim strFileName As String = ""
                                    Dim strFilePhysicalLocation As String = ""
                                    Dim objHTMLToPDF As New HTMLToPDF

                                    strFileName = objHTMLToPDF.ConvertHTML2PDF(htmlCodeToConvert, CCommon.ToLong(dr("numDomainID")))
                                    strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(CCommon.ToLong(dr("numDomainID"))) & strFileName
                                    Dim strBizDocName As String = CCommon.ToString(dsOppData.Tables(0).Rows(0).Item("vcBizDocID"))

                                    Dim objCommon As New CCommon
                                    dtTable = New DataTable
                                    dtTable.Columns.Add("Filename")
                                    dtTable.Columns.Add("FileLocation")
                                    dtTable.Columns.Add("FilePhysicalPath")
                                    dtTable.Columns.Add("FileSize", GetType(Long))
                                    dtTable.Columns.Add("FileType")

                                    Dim dr1 As DataRow = dtTable.NewRow
                                    dr1("Filename") = strFileName
                                    dr1("FileLocation") = CCommon.GetDocumentPhysicalPath(CCommon.ToLong(dr("numDomainID"))) & strFileName
                                    dr1("FilePhysicalPath") = strFilePhysicalLocation
                                    dr1("FileType") = ".pdf"
                                    dr1("FileSize") = 15650
                                    dtTable.Rows.Add(dr1)

                                End If
                                'end of code for bizdoc attachment

                                If strContactIDs.Count > 0 OrElse (CCommon.ToString(drAct("vcEmailSendTo")) IsNot Nothing) Then

                                    strContactIDWebLead.Add(numContactId)
                                    If (moduleId = 1 Or moduleId = 45) Then
                                        dtEmailBroadCast = objEmail.GetEmailMergeData(moduleId, numContactId, CCommon.ToLong(dr("numDomainID")), 0, 0)
                                    Else
                                        dtEmailBroadCast = objEmail.GetEmailMergeData(moduleId, objWF.RecordID, CCommon.ToLong(dr("numDomainID")), 0, 0)
                                    End If

                                    ' Displaying Tracking no. as link in Email (Added by Priya)
                                    If (moduleId = 2) Then
                                        For Each drrow As DataRow In dtEmailBroadCast.Rows
                                            Dim strTrackingNo As String = ""
                                            If (Not String.IsNullOrEmpty(CCommon.ToString(drrow("OppOrderTrackingNo")).Trim())) Then
                                                Dim strTrackingNoList() As String = CCommon.ToString(drrow("OppOrderTrackingNo")).Split(New String() {"$^$"}, StringSplitOptions.None)
                                                Dim strIDValue() As String

                                                If strTrackingNoList.Length > 0 Then
                                                    For k As Integer = 0 To strTrackingNoList.Length - 1
                                                        strIDValue = strTrackingNoList(k).Split(New String() {"#^#"}, StringSplitOptions.None)
                                                        If strIDValue(1).Length > 0 Then
                                                            strTrackingNo += String.Format("<a href='{0}' target='_blank'>{1}</a>&nbsp;&nbsp;", IIf(strIDValue(0).Length = 0, "javascript:void(0)", String.Format(strIDValue(0) + strIDValue(1), strIDValue(1))), strIDValue(1))
                                                        End If
                                                    Next
                                                End If
                                            End If
                                            drrow("OppOrderTrackingNo") = "<div style='width: 95%;word-break: break-all;'>" & strTrackingNo & "</div>"
                                        Next
                                    End If

                                    dtEmailBroadCastSendTo = objEmail.GetEmailMergeData(1, String.Join(",", strContactIDs.ToArray()), CCommon.ToLong(dr("numDomainID")), 0, 0)

                                    Dim strTo As New List(Of String)
                                    For Each drEmail As DataRow In dtEmailBroadCastSendTo.Rows
                                        strTo.Add(drEmail("ContactEmail"))
                                    Next
                                    'just for patches-testing purpose

                                    'end of testing
                                    If strTo.Count > 0 OrElse (CCommon.ToString(drAct("vcEmailSendTo")) IsNot Nothing) Then
                                        For Each iKey As KeyValuePair(Of String, String) In objEmail.AdditionalMergeFields
                                            If dtEmailBroadCast.Columns.Contains(iKey.Key) = False Then
                                                dtEmailBroadCast.Columns.Add(iKey.Key)
                                            End If
                                            'Assign values to rows in merge table
                                            For Each row As DataRow In dtEmailBroadCast.Rows
                                                If iKey.Key <> "ContactEmail" Then
                                                    row(iKey.Key) = CCommon.ToString(iKey.Value)
                                                End If
                                            Next
                                        Next

                                        Dim strFinalTo As String = Nothing
                                        If CCommon.ToString(drAct("vcEmailSendTo")) = "" Or CCommon.ToString(drAct("vcEmailSendTo")) = Nothing Then
                                            strFinalTo = String.Join(",", strTo)
                                        Else
                                            If strTo.Count > 0 Then
                                                strFinalTo = String.Join(",", strTo) & "," & drAct("vcEmailSendTo")
                                            Else
                                                strFinalTo = drAct("vcEmailSendTo")
                                            End If

                                        End If

                                        Dim mailStatus As Boolean
                                        Dim errorMessage As String = ""

                                        If CCommon.ToShort(drAct("tintIsAttachment")) = 1 Then
                                            'uncommnted by ss
                                            'objEmail.SendEmailWS(strSubject, strBody, "", dtRecordData.Rows(0).Item("vcEmailID"), String.Join(",", strTo), dtEmailBroadCast, dtTable, FromDisplayName:=dtRecordData.Rows(0)("ContactName"), DomainID:=objWF.DomainID)
                                            objEmail.AdditionalMergeFields.Add("Signature", dtRecordData.Rows(0)("Signature").ToString)
                                            mailStatus = objEmail.SendEmailWS(strSubject, strBody, "", dtRecordData.Rows(0).Item("vcEmailID"), strFinalTo, dtEmailBroadCast, dtTable, FromDisplayName:=dtRecordData.Rows(0)("ContactName"), DomainID:=objWF.DomainID, UserCntID:=lngUserCntID, tintEmailFrom:=tintEmailFrom)
                                            dtTable.Rows.Clear()
                                            dtTable.Columns.Clear()
                                        Else
                                            'objEmail.SendEmail(strSubject, strBody, "", dtRecordData.Rows(0).Item("vcEmailID"), String.Join(",", strTo), dtEmailBroadCast, FromDisplayName:=dtRecordData.Rows(0)("ContactName"), DomainID:=objWF.DomainID)
                                            objEmail.AdditionalMergeFields.Add("Signature", dtRecordData.Rows(0)("Signature").ToString)

                                            mailStatus = objEmail.SendEmailWFA(strSubject, strBody, "", dtRecordData.Rows(0).Item("vcEmailID"), strFinalTo, dtEmailBroadCast, FromDisplayName:=dtRecordData.Rows(0)("ContactName"), DomainID:=objWF.DomainID, ErrorMessage:=errorMessage, UserId:=lngUserCntID, tintEmailFrom:=tintEmailFrom)

                                        End If

                                        ' WFDescription = "Email Sent to " & String.Join(",", strTo)
                                        If mailStatus = True Then
                                            If strFinalTo = "" Or strFinalTo = Nothing Then
                                                WFDescription = "Unable to send a mail.Receiver Email Address doesn't exist."
                                            Else
                                                WFDescription = "Email Sent to " & strFinalTo
                                            End If
                                            bitSuccess = True
                                        Else
                                            WFDescription = "Email not Sent. Error:" & errorMessage & ", Please Configure SMTP details.GO to Global Settings->General  "
                                            bitSuccess = False
                                        End If

                                    End If
                                End If
                            End If
                        Case CCommon.ToShort(enmWFAction.UpdateFields)
                            Dim Sql As New System.Text.StringBuilder
                            Dim SqlFrom As New System.Text.StringBuilder
                            Dim SqlCondition As New System.Text.StringBuilder

                            Dim tempUpdate As New List(Of String)

                            Dim drWFActionUpdateFields() As DataRow = dtWFActionUpdateFields.Select("numWFActionID=" & drAct("numWFActionID"))

                            Select Case objWF.FormID
                                Case (138) 'Organization
                                    If drWFActionUpdateFields.Length > 0 Then
                                        Dim strFiels As String = ""
                                        For Each drCFL As DataRow In drWFActionUpdateFields
                                            Dim drFLCheck() As DataRow = dtFieldsList.Select("numFieldID=" & drCFL("numFieldID") & " AND bitCustom=" & drCFL("bitCustom"))
                                            Dim boolInsert As Boolean = True
                                            If drFLCheck.Length > 0 Then
                                                If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                    If drFLCheck(0)("vcLookBackTableName") = "AdditionalContactsInformation" Then 'Organization
                                                        Sql.Append("Update AdditionalContactsInformation a set ")
                                                        SqlCondition.Append(" FROM DivisionMaster d where d.numDomainId = $1 AND d.numDivisionId = $2 AND d.numDivisionID=a.numDivisionId ")
                                                    ElseIf drFLCheck(0)("vcLookBackTableName") = "CompanyInfo" Then 'Company Info
                                                        Sql.Append("Update CompanyInfo c set ")
                                                        SqlCondition.Append("  FROM  DivisionMaster d where d.numDomainId = $1 AND d.numDivisionId = $2 AND d.numCompanyId=c.numCompanyId ")
                                                    ElseIf drCFL("FieldName").ToString = "Signature Type" Then
                                                        Sql.Append("DELETE FROM DivisionMasterShippingConfiguration WHERE numDomainID=$1 AND numDivisionID=$2; ")
                                                        Sql.Append(" INSERT INTO DivisionMasterShippingConfiguration (numDomainID ,numDivisionID ,vcSignatureType) VALUES ( $1,$2, '" + drCFL("vcValue").ToString + "' );")
                                                    Else
                                                        Sql.Append("Update DivisionMaster set ")
                                                        SqlCondition.Append(" where DivisionMaster.numDomainId = $1 AND DivisionMaster.numDivisionId = $2")
                                                    End If
                                                Else 'Update Custom Fields

                                                    If drFLCheck(0)("vcLookBackTableName") = "CFW_FLD_Values" Then 'Organization

                                                        boolInsert = objWF.WFCustomFieldsInsert(CCommon.ToLong(drCFL("numFieldID")), drCFL("vcValue"))
                                                        If boolInsert Then
                                                            Sql.Append("Update CFW_FLD_Values set ")
                                                            SqlCondition.Append(" where CFW_FLD_Values.Fld_ID = " & drCFL("numFieldID") & " AND CFW_FLD_Values.RecId = $2")
                                                        End If
                                                    End If
                                                End If
                                            End If
                                            If Not drCFL("FieldName").ToString = "Signature Type" Then

                                                If CCommon.ToBool(drFLCheck(0)("bitCustom")) = False Then
                                                    tempUpdate.Add(String.Format("{0}='{1}'",
                                                drFLCheck(0)("vcOrigDbColumnName"), drCFL("vcValue")))
                                                Else
                                                    If boolInsert Then
                                                        tempUpdate.Add(String.Format("{0}='{1}'",
                                                            "Fld_Value", drCFL("vcValue")))
                                                    End If

                                                End If
                                            End If
                                            strFiels = strFiels & "," & drCFL("FieldName")
                                            SqlFrom.Append(String.Join(",", tempUpdate))
                                            Sql.Append(SqlFrom)
                                            Sql.Append(SqlCondition)
                                            objWF.textQuery = Sql.ToString()
                                            objWF.WFConditionQueryExecute(2)
                                            WFDescription = "Fields Updated:" & strFiels
                                            bitSuccess = True
                                            If bitSuccess = True And objWF.FormID = 68 And drFLCheck(0)("vcLookBackTableName") = "DivisionMaster" Then
                                                'Added By Sachin Sadhu||Date:23ndMay12014
                                                'Purpose :To Added Organization data in work Flow queue based on created Rules
                                                '          Using Change tracking
                                                Dim objWfA As New Workflow()
                                                objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                objWfA.UserCntID = numRecOwner
                                                ' objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                objWfA.SaveWFOrganizationQueue()
                                                'end of code
                                            ElseIf bitSuccess = True And drFLCheck(0)("vcLookBackTableName") = "AdditionalContactsInformation" Then
                                                Dim objWfA As New Workflow()
                                                objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                objWfA.UserCntID = numRecOwner
                                                ' objWfA.RecordID = numContactId
                                                objWfA.SaveWFContactQueue()
                                            Else
                                                Dim objWfA As New Workflow()
                                                objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                objWfA.UserCntID = numRecOwner
                                                ' objWfA.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                objWfA.SaveWFOrganizationQueue()
                                            End If
                                            If CCommon.ToBool(drFLCheck(0)("bitCustom")) = True And bitSuccess = True Then ' TRIGGER Custom fields based WF rules
                                                Dim objWFCF As New Workflow()
                                                objWFCF.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                objWFCF.UserCntID = numRecOwner
                                                ' objWFCF.RecordID = CCommon.ToLong(dr("numRecordID"))
                                                objWFCF.FormFieldId = CCommon.ToLong(drCFL("numFieldID"))
                                                objWFCF.SaveWFCFOrganizationQueue()
                                            End If
                                            Sql.Clear()
                                            SqlFrom.Clear()
                                            SqlCondition.Clear()
                                            tempUpdate.Clear()
                                        Next
                                    End If
                            End Select
                    End Select

                Catch ex As Exception
                    WFDescription = "Error occured:" & ex.Message
                    bitSuccess = False
                End Try
            Next

            'Insert RecordID in WorkFlowARAgingExecutionHistory table

            If (bitSuccess = True) Then
                Dim bitInsertDateFieldHistory As Boolean = objWF.SaveWorkFlowARAgingExecutionHistory(objWF.RecordID, objWF.FormID)
            End If
        End Sub
    End Class

End Namespace

