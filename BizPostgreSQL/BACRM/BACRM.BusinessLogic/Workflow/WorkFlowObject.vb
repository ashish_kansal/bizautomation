﻿Option Explicit On
Option Strict On
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.Workflow
    Public Class WorkFlowObject

        Private _WFName As String
        Public Property WFName() As String
            Get
                Return _WFName
            End Get
            Set(ByVal value As String)
                _WFName = value
            End Set
        End Property

        Private _Active As Boolean
        Public Property Active() As Boolean
            Get
                Return _Active
            End Get
            Set(ByVal value As Boolean)
                _Active = value
            End Set
        End Property

        Private _WFDescription As String
        Public Property WFDescription() As String
            Get
                Return _WFDescription
            End Get
            Set(ByVal value As String)
                _WFDescription = value
            End Set
        End Property

        Private _FormID As Long
        Public Property FormID() As Long
            Get
                Return _FormID
            End Get
            Set(ByVal value As Long)
                _FormID = value
            End Set
        End Property

        Private _WFTriggerOn As Short
        Public Property WFTriggerOn() As Short
            Get
                Return _WFTriggerOn
            End Get
            Set(ByVal value As Short)
                _WFTriggerOn = value
            End Set
        End Property

        Private _TriggerFieldList As List(Of String)
        Public Property TriggerFieldList As List(Of String)
            Get
                Return _TriggerFieldList
            End Get
            Set(ByVal value As List(Of String))
                _TriggerFieldList = value
            End Set
        End Property

        Private _Conditions As List(Of ConditionObject)
        Public Property Conditions As List(Of ConditionObject)
            Get
                Return _Conditions
            End Get
            Set(ByVal value As List(Of ConditionObject))
                _Conditions = value
            End Set
        End Property

        Private _Actions As List(Of ActionObject)
        Public Property Actions As List(Of ActionObject)
            Get
                Return _Actions
            End Get
            Set(ByVal value As List(Of ActionObject))
                _Actions = value
            End Set
        End Property

    End Class

    Public Class ConditionObject
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _Column As String
        Public Property Column() As String
            Get
                Return _Column
            End Get
            Set(ByVal value As String)
                _Column = value
            End Set
        End Property

        Private _FilterValue As String
        Public Property FilterValue() As String
            Get
                Return _FilterValue
            End Get
            Set(ByVal value As String)
                _FilterValue = value
            End Set
        End Property

        Private _FilterOperator As String
        Public Property FilterOperator() As String
            Get
                Return _FilterOperator
            End Get
            Set(ByVal value As String)
                _FilterOperator = value
            End Set
        End Property

        Private _FilterANDOR As String
        Public Property FilterANDOR() As String
            Get
                Return _FilterANDOR
            End Get
            Set(ByVal value As String)
                _FilterANDOR = value
            End Set
        End Property

    End Class

    Public Class ActionObject
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _ActionType As Short
        Public Property ActionType() As Short
            Get
                Return _ActionType
            End Get
            Set(ByVal value As Short)
                _ActionType = value
            End Set
        End Property

        Private _TemplateID As Long
        Public Property TemplateID() As Long
            Get
                Return _TemplateID
            End Get
            Set(ByVal value As Long)
                _TemplateID = value
            End Set
        End Property

        Private _vcEmailToType As String
        Public Property vcEmailToType() As String
            Get
                Return _vcEmailToType
            End Get
            Set(ByVal value As String)
                _vcEmailToType = value
            End Set
        End Property

        Private _TicklerActionAssignedTo As Short
        Public Property TicklerActionAssignedTo() As Short
            Get
                Return _TicklerActionAssignedTo
            End Get
            Set(ByVal value As Short)
                _TicklerActionAssignedTo = value
            End Set
        End Property

        Private _vcAlertMessage As String
        Public Property vcAlertMessage() As String
            Get
                Return _vcAlertMessage
            End Get
            Set(ByVal value As String)
                _vcAlertMessage = value
            End Set
        End Property

        Private _ActionUpdateFields As List(Of ActionUpdateFieldsObject)
        Public Property ActionUpdateFields As List(Of ActionUpdateFieldsObject)
            Get
                Return _ActionUpdateFields
            End Get
            Set(ByVal value As List(Of ActionUpdateFieldsObject))
                _ActionUpdateFields = value
            End Set
        End Property

    End Class

    Public Class ActionUpdateFieldsObject
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _Column As String
        Public Property Column() As String
            Get
                Return _Column
            End Get
            Set(ByVal value As String)
                _Column = value
            End Set
        End Property

        Private _tintModuleType As Short
        Public Property tintModuleType() As Short
            Get
                Return _tintModuleType
            End Get
            Set(ByVal value As Short)
                _tintModuleType = value
            End Set
        End Property

        Private _vcValue As String
        Public Property vcValue() As String
            Get
                Return _vcValue
            End Get
            Set(ByVal value As String)
                _vcValue = value
            End Set
        End Property

    End Class

End Namespace